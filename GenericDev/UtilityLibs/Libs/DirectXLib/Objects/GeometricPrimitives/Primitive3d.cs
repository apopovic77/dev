using System;
using System.Collections.Generic;
using System.Text;
using D3d = Microsoft.DirectX.Direct3D;
using System.Drawing;
using GeoLib;
using System.Collections;
using Logicx.DirectX.Engine;
using Logicx.DirectX.Management;



namespace Logicx.DirectX.Objects.GeometricPrimitives
{

    public class PrimitiveTextured3d : Primitive3d
    {
        protected new D3d.CustomVertex.PositionTextured[] m_vertices_mem;
        protected D3d.Texture m_texture;

        public PrimitiveTextured3d()
        {
        }

        public PrimitiveTextured3d(D3d.CustomVertex.PositionTextured[] vertices, D3d.PrimitiveType pt, D3d.Texture texture)
        {
            m_texture = texture;
            PrimitiveType = pt;
            m_vertices_mem = vertices;
            CreateVertexBuffer();
        }

        public override void CreateVertexBuffer()
        {
            if (m_vertices_mem == null) return;

            m_vertices_count = m_vertices_mem.Length;

            //create vertex buffer
            m_VB = new D3d.VertexBuffer(
                                        typeof(D3d.CustomVertex.PositionTextured),
                                        m_vertices_count,
                                        Engine3d.Device3d,
                                        Engine3d.GetUsageFlagForVB(false),
                                        D3d.CustomVertex.PositionTextured.Format,
                                        Engine3d.GetPoolFlagForVB());

            //lock and fill vertices values
            D3d.CustomVertex.PositionTextured[] vb_vertices = (D3d.CustomVertex.PositionTextured[])m_VB.Lock(0, typeof(D3d.CustomVertex.PositionTextured), D3d.LockFlags.None, m_vertices_count);

            for (int i = 0; i < vb_vertices.Length; i++)
            {
                vb_vertices[i].X = m_vertices_mem[i].X;
                vb_vertices[i].Y = m_vertices_mem[i].Y;
                vb_vertices[i].Z = m_vertices_mem[i].Z;
                vb_vertices[i].Tu = m_vertices_mem[i].Tu;
                vb_vertices[i].Tv = m_vertices_mem[i].Tv;
            }

            //Unlock the vb before you can use it elsewhere
            m_VB.Unlock();
        }
        protected override void SetVertexFormat()
        {
            Engine3d.Device3d.VertexFormat = D3d.CustomVertex.PositionTextured.Format;
        }

        public override void Render(float elapsedTime)
        {
            Engine3d.Device3d.SetTexture(0,m_texture);
            base.Render(elapsedTime);
            Engine3d.Device3d.SetTexture(0, null);
        }

    }

    /// <summary>
    /// Damit kann man ganz einfach eine Primitive zeichnen, der Vertex-Buffer und das Rendering werden automatisch ausgef�hrt. 
    /// Man brauch nur die Vertices dem Konstruktor �bergeben, und das Object in der Engine �bergeben.
    /// </summary>
    public class Primitive3d : Object3d, IDisposable
    {


        #region Attributes
        protected D3d.VertexBuffer m_VB = null; //vertex buffer, vertices stored in graphics card mem
        protected D3d.CustomVertex.PositionColored[] m_vertices_mem; //vertices stored in mem
        public D3d.PrimitiveType PrimitiveType = Microsoft.DirectX.Direct3D.PrimitiveType.TriangleStrip;
        public Microsoft.DirectX.Direct3D.FillMode FillMode = Microsoft.DirectX.Direct3D.FillMode.Solid;
        protected Microsoft.DirectX.Matrix m_transl_matrix;
        protected bool m_dotransl = false;
        protected int m_vertices_count=0;
        public Microsoft.DirectX.Matrix Translation
        {
            set { m_transl_matrix = value; m_dotransl = true; }
        }
        #endregion


        public Primitive3d()
        {
        }

        public Primitive3d(D3d.CustomVertex.PositionColored[] vertices,D3d.PrimitiveType pt)
        {
            PrimitiveType = pt;
            m_vertices_mem = vertices;
            CreateVertexBuffer();
        }
        public override void Init()
        {
        }
        public override void Dispose()
        {
            if (m_VB != null)
                m_VB.Dispose();
            base.Dispose();
        }
        public virtual void CreateVertexBuffer()
        {
            if (m_vertices_mem == null) return;

            m_vertices_count = m_vertices_mem.Length;

            //create vertex buffer
            m_VB = new D3d.VertexBuffer(
                                        typeof(D3d.CustomVertex.PositionColored),
                                        m_vertices_count,
                                        Engine3d.Device3d,
                                        Engine3d.GetUsageFlagForVB(false),
                                        D3d.CustomVertex.PositionColored.Format,
                                        Engine3d.GetPoolFlagForVB());

            //lock and fill vertices values
            D3d.CustomVertex.PositionColored[] vb_vertices = (D3d.CustomVertex.PositionColored[])m_VB.Lock(0, typeof(D3d.CustomVertex.PositionColored), D3d.LockFlags.None, m_vertices_count);

            for (int i = 0; i < vb_vertices.Length; i++)
            {
                vb_vertices[i].X = m_vertices_mem[i].X;
                vb_vertices[i].Y = m_vertices_mem[i].Y;
                vb_vertices[i].Z = m_vertices_mem[i].Z;
                vb_vertices[i].Color = m_vertices_mem[i].Color;
            }

            //Unlock the vb before you can use it elsewhere
            m_VB.Unlock();
        }

        protected virtual void SetVertexFormat() {
            Engine3d.Device3d.VertexFormat = D3d.CustomVertex.PositionColored.Format;
        }

        public override void Render(float elapsedTime)
        {
            if (m_VB == null) return;

            Microsoft.DirectX.Matrix m = Microsoft.DirectX.Matrix.Identity;
            if (m_vScaleing != null && (m_vScaleing.X != 0 || m_vScaleing.Y != 0 || m_vScaleing.Z != 0)) m.Scale(m_vScaleing);
            if (m_dotransl) m.Multiply(m_transl_matrix);
            Engine3d.Device3d.Transform.World = m;
            Engine3d.Device3d.RenderState.FillMode = FillMode;
            Engine3d.Device3d.SetStreamSource(0, m_VB, 0);
            SetVertexFormat();

            switch (PrimitiveType) { 
                case Microsoft.DirectX.Direct3D.PrimitiveType.PointList:
                    Engine3d.Device3d.DrawPrimitives(D3d.PrimitiveType.PointList, 0, m_vertices_count);
                    break;
                case Microsoft.DirectX.Direct3D.PrimitiveType.LineStrip:
                    Engine3d.Device3d.DrawPrimitives(D3d.PrimitiveType.LineStrip, 0, m_vertices_count - 1);
                    break;
                case Microsoft.DirectX.Direct3D.PrimitiveType.TriangleStrip:
                    Engine3d.Device3d.DrawPrimitives(D3d.PrimitiveType.TriangleStrip, 0, m_vertices_count - 2);
                    break;
                case Microsoft.DirectX.Direct3D.PrimitiveType.LineList:
                    Engine3d.Device3d.DrawPrimitives(D3d.PrimitiveType.LineList, 0, Convert.ToInt32(m_vertices_count / 2));
                    break;
                default:
                    throw new Exception3d("not implemented yet");
            }
        }




    }
}
