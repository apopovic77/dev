﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LoggingLib.Configuration;
using LoggingLib.Interfaces;

namespace LoggingLib.Configuration.Activators
{
    /// <summary>
    /// Instance activator class for configured log formatters
    /// </summary>
    public class LogFormatterActivator
    {
        #region Construction and initialization
        /// <summary>
        /// Default constructor building the data finalizers depending on the configuration settings
        /// </summary>
        /// <param name="settings"></param>
        public LogFormatterActivator(LoggingConfigSection settings)
        {
            if (settings == null)
                throw new ArgumentException("Settings must not be null.", "settings");

            BuildFormatters(settings);
        }
        #endregion

        #region Operations

        /// <summary>
        /// Returns the finalizer linked to the given data type
        /// </summary>
        /// <param name="linkedType"></param>
        /// <returns></returns>
        public ILogFormatter GetFormatterForType(Type linkedType)
        {
            foreach(KeyValuePair<Type, ILogFormatter> formatterItm in _formatterList)
            {
                if(formatterItm.Key == linkedType)
                    return formatterItm.Value;
            }

            return null;
        }

        /// <summary>
        /// Returns the standard log formatter
        /// </summary>
        /// <returns></returns>
        public ILogFormatter GetStandardFormatter()
        {
            foreach (KeyValuePair<Type, ILogFormatter> formatterItm in _formatterList)
            {
                if (formatterItm.Key == null)
                    return formatterItm.Value;
            }

            return null;   
        }

        /// <summary>
        /// Build a list of data formatter instances
        /// </summary>
        /// <param name="settings"></param>
        private void BuildFormatters(LoggingConfigSection settings)
        {
            foreach(LogFormatterElement lfElement in settings.LogFormatters)
            {
                Type linkedElementType = string.IsNullOrEmpty(lfElement.LogEntryDataTypeName) ? null : Type.GetType(lfElement.LogEntryDataTypeName);
                Type formatterDataTypeName = Type.GetType(lfElement.FormatterDataTypeName);

                ILogFormatter objFormatter = Activator.CreateInstance(formatterDataTypeName) as ILogFormatter;

                if (objFormatter != null)
                {
                    KeyValuePair<Type, ILogFormatter> itm = new KeyValuePair<Type, ILogFormatter>(linkedElementType, objFormatter);
                    _formatterList.Add(itm);
                }
            }
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets a reference to the log formatter list
        /// </summary>
        public List<KeyValuePair<Type, ILogFormatter>> FormatterList
        {
            get { return _formatterList; }
        }
        #endregion

        #region Attribs
        private List<KeyValuePair<Type, ILogFormatter>> _formatterList = new List<KeyValuePair<Type, ILogFormatter>>();
        #endregion
    }
}