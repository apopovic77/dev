﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Logicx.WpfUtility.CustomControls
{
    /// <summary>
    /// Interaction logic for KontaktInformation.xaml
    /// </summary>
    public partial class KontaktInformation : UserControl
    {
        public KontaktInformation()
        {
            InitializeComponent();
        }

        #region Properties
        /// <summary>
        /// Main StackPanel - Add custom child controls if more information has to be provided
        /// </summary>
        public StackPanel MainStackPanel
        {
            get { return main_sp; }
        }
        public string SupportLabel
        {
            set
            {
                tbl_support_address_label.Text = value;
            }
        }
        public string SupportAddress
        {
            set
            {
                tbl_support_address_value.Text = value;
                hyp_support_address.NavigateUri = new Uri("mailto:"+value);
            }
        }
        public string KeyUsersLabel
        {
            set
            {
                tbl_key_users_label.Text = value;
            }
        }
        public string KeyUser1Label
        {
            set
            {
                tbl_key_user1_label.Text = value;
            }
        }
        public string KeyUser1Address
        {
            set
            {
                tbl_key_user1_value.Text = value;
                hyp_key_user1.NavigateUri = new Uri("mailto:" + value);
            }
        }
        public string KeyUser2Label
        {
            set
            {
                tbl_key_user2_label.Text = value;
            }
        }
        public string KeyUser2Address
        {
            set
            {
                tbl_key_user2_value.Text = value;
                hyp_key_user2.NavigateUri = new Uri("mailto:" + value);
            }
        }
        public string KeyUser3Label
        {
            set
            {
                tbl_key_user3_label.Text = value;
            }
        }
        public string KeyUser3Address
        {
            set
            {
                tbl_key_user3_label.Text = value;
                hyp_key_user3.NavigateUri = new Uri("mailto:" + value);
            }
        }
        public bool ShowKeyUsers
        {
            set
            {
                if (value)
                    sp_key_users.Visibility = System.Windows.Visibility.Visible;
                else
                    sp_key_users.Visibility = System.Windows.Visibility.Collapsed;
            }
        }

        public bool ShowKeyUser1
        {
            set
            {
                if (value)
                    sp_key_user1_info.Visibility = System.Windows.Visibility.Visible;
                else
                    sp_key_user1_info.Visibility = System.Windows.Visibility.Collapsed;
            }
        }
        public bool ShowKeyUser2
        {
            set
            {
                if (value)
                    sp_key_user2_info.Visibility = System.Windows.Visibility.Visible;
                else
                    sp_key_user2_info.Visibility = System.Windows.Visibility.Collapsed;
            }
        }
        public bool ShowKeyUser3
        {
            set
            {
                if (value)
                    sp_key_user3_info.Visibility = System.Windows.Visibility.Visible;
                else
                    sp_key_user3_info.Visibility = System.Windows.Visibility.Collapsed;
            }
        }
        public bool ShowSupportInfo
        {
            set
            {
                if (value)
                    sp_support_info.Visibility = System.Windows.Visibility.Visible;
                else
                    sp_support_info.Visibility = System.Windows.Visibility.Collapsed;
            }
        }
        #endregion

        private void HelpLink_RequestNavigate(object sender, RequestNavigateEventArgs e)
        {
            Hyperlink thisLink = (Hyperlink)sender;
            try
            {
                string navigateUri = thisLink.NavigateUri.ToString();
                System.Diagnostics.Process.Start(new System.Diagnostics.ProcessStartInfo(navigateUri));
            }
            catch (Exception ex)
            {}
            e.Handled = true;
        }
    }
}
