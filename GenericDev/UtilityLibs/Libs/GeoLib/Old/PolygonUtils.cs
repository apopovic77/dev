using System;
using System.Collections;
using System.Drawing;
using MathLib;

namespace GeoLib
{



    public class PolyVertex
    {
        public PolyVertex(double x, double y)
        {
            _v.X = x;
            _v.Y = y;
        }
        public PolyVertex(double x, double y, double alpha)
        {
            _v.X = x;
            _v.Y = y;
            this.alpha = alpha;
        }

        public double X
        {
            set
            {
                _v.X = value;
            }
            get
            {
                return _v.X;
            }
        }
        public double Y
        {
            set
            {
                _v.Y = value;
            }
            get
            {
                return _v.Y;
            }
        }

        public PolyVertex next, prev, nextPoly, neighbor;
        public bool intersect, entry_exit;
        public double alpha;
        public Vector2d _v;

    }   

public enum Direction
{
    Forward,
    Backward
}

public class PolygonUtils : Polygon
{

    public static Polygon CreatePolyAroundEdge(Edge e, double margin)
    {
        double margin_norm = margin + margin * 0.2;
        Vector2d v1 = e.V1;
        Vector2d v2 = e.V2;
        Vector2d norm = (v2 - v1).GetNorm();
        Vector2d orth1 = norm.GetOrthogonal1() * margin;
        Vector2d orth2 = norm.GetOrthogonal2() * margin;
        Vector2d v_0 = v1 + (norm * margin_norm).GetMirror() + orth1;
        Vector2d v_1 = v2 + (norm * margin_norm) + orth1;
        Vector2d v_2 = v2 + (norm * margin_norm) + orth2;
        Vector2d v_3 = v1 + (norm * margin_norm).GetMirror() + orth2;
        Polygon p = new Polygon();
        p.Add(v_0);
        p.Add(v_1);
        p.Add(v_2);
        p.Add(v_3);
        p.Add(v_0);
        return p;
    }



    Polygon poly, subject;
    public PolygonUtils() { }
    protected PolyVertex InterconnectVertices(Polygon poly)
    {
        Vector2d v = poly[0];
        PolyVertex pv_curr = null;
        PolyVertex pv_prev = new PolyVertex(v.X, v.Y);
        PolyVertex pv_start = pv_prev;
        for (int i = 1; i < poly.Length - 1; i++)
        {
            v = poly[i];
            pv_curr = new PolyVertex(v.X, v.Y);
            pv_curr.prev = pv_prev;
            pv_prev.next = pv_curr;
            pv_prev = pv_curr;
        }
        pv_curr.next = pv_start;
        pv_start.prev = pv_curr;
        return pv_start;
    }

    protected void PhaseOne()
    {
        p0 = InterconnectVertices(poly);
        s0 = InterconnectVertices(subject);

        DisturbVerticesOnEdges();

        for (int i = 0; i < subject.Length - 1; i++)
        {
            Vector2d si, si2;
            si = subject[i];
            int i_2 = (i == subject.Length - 2) ? 0 : i + 1;
            si2 = subject[i_2];

            for (int j = 0; j < poly.Length - 1; j++)
            {
                PolyVertex i1, i2;
                Vector2d cj, cj2;

                cj = poly[j];
                int j_2 = (j == poly.Length - 2) ? 0 : j + 1;
                cj2 = poly[j_2];
                double a = 0, b = 0;

                if (Intersect(si, si2, cj, cj2, out a, out b))
                {
                    Vector2d cutpoint;
                    MathLib.LinearAlgebra.GetCutPoint(si, (si2 - si).GetNorm(), cj, (cj2 - cj).GetNorm(), out cutpoint);
                    i1 = new PolyVertex(cutpoint.X, cutpoint.Y, a);
                    i2 = new PolyVertex(cutpoint.X, cutpoint.Y, b);
                    i1.neighbor = i2;
                    i2.neighbor = i1;
                    i1.intersect = true;
                    i2.intersect = true;

                    //sort into subject poly
                    SortIntoList(p0, i2, cj, cj2);
                    SortIntoList(s0, i1, si, si2);
                }
            }
        }
    }

    private void DisturbVerticesOnEdges()
    {
        while (true)
        {
            for (int i = 0; i < subject.Length - 1; i++)
            {
                Vector2d si, si2;
                si = subject[i];
                int i_2 = (i == subject.Length - 2) ? 0 : i + 1;
                si2 = subject[i_2];

                for (int j = 0; j < poly.Length - 1; j++)
                {
                    Vector2d cj, cj2;
                    cj = poly[j];
                    int j_2 = (j == poly.Length - 2) ? 0 : j + 1;
                    cj2 = poly[j_2];

                    bool error = false;
                    if (DisturbPointIfNecessary(ref si, ref si2, ref cj, ref cj2, out error))
                    {
                        if (error)
                            continue;
                        else
                        {
                            subject[i] = si;
                            subject[i_2] = si2;
                            poly[j] = cj;
                            poly[j_2] = cj2;
                        }
                    }
                }
            }
            break;
        }
    }

    private bool DisturbPointIfNecessary(ref Vector2d si, ref Vector2d si2, ref Vector2d cj, ref Vector2d cj2, out bool error)
    {
        error = false;
        double a, b;
        if (Intersect(si, si2, cj, cj2, out a, out b))
        {
            if (a == 0 || b == 0)
            {
                Vector2d si_temp = si, si2_temp = si2, cj_temp = cj, cj2_temp = cj2;

                //disturb si
                Random random = new Random();
                double angle_rand = random.NextDouble() * Math.PI * 2;
                double angle_rand2 = random.NextDouble() * Math.PI * 2;
                double error_disturb_percent = 0.000001;

                Vector2d n = (cj2 - cj).GetOrthogonal1().GetNorm() * (si.GetLen() * error_disturb_percent);
                n.RotateCounterClockwise(angle_rand);
                Vector2d n2 = (si2 - si).GetOrthogonal1().GetNorm() * (si.GetLen() * error_disturb_percent);
                n2.RotateCounterClockwise(angle_rand2);
                n = n + n2;
                si_temp = si + n;

                angle_rand = random.NextDouble() * Math.PI * 2;
                angle_rand2 = random.NextDouble() * Math.PI * 2;
                n = (cj2 - cj).GetOrthogonal1().GetNorm() * (si.GetLen() * error_disturb_percent);
                n.RotateCounterClockwise(angle_rand);
                //n2 = (si2-si).GetOrthogonal1().GetNorm()*(si.GetLen()*0.001);		
                n2.RotateCounterClockwise(angle_rand2);
                n = n + n2;
                si2_temp = si2 + n;

                if (Intersect(si_temp, si2_temp, cj, cj2, out a, out b))
                    if (a == 0 || b == 0)
                    {
                        error = true;
                        return true;
                    }
                PolyVertex pv_si = SearchListForElem(s0, si);
                PolyVertex pv_si2 = SearchListForElem(s0, si2);
                pv_si.X = si_temp.X; pv_si.Y = si_temp.Y;
                pv_si2.X = si2_temp.X; pv_si2.Y = si2_temp.Y;
                si = si_temp;
                si2 = si2_temp;
                return true;
            }
        }
        return false;
    }

    private PolyVertex SearchListForElem(PolyVertex pv_start, Vector2d v)
    {
        PolyVertex pv_curr = pv_start;
        do
        {
            if (pv_curr.X == v.X && pv_curr.Y == v.Y)
                return pv_curr;
            else
                pv_curr = pv_curr.next;
        } while (pv_curr != pv_start);
        throw new GeoLibException("there is no elem with this setting");
    }

    private void SortIntoList(PolyVertex pv_start, PolyVertex pv_intersect, Vector2d v_prev, Vector2d v_next)
    {
        PolyVertex pv_curr = pv_start;
        do
        {
            if (pv_curr.X == v_prev.X && pv_curr.Y == v_prev.Y)
            {
                //check ob wirklich richtig es k�nnten auch intersections drinnen sein
                if (pv_curr.next.intersect)
                {
                    //						double dist_me = (pv_intersect-v_prev).GetLen();
                    //						double dist_inext = (pv_curr.next-v_prev).GetLen();
                    //						if(dist_me > dist_inext)
                    //						{
                    //							v_prev = pv_curr.next;
                    //							continue;
                    //						}
                    if (pv_intersect.alpha > pv_curr.next.alpha)
                    {
                        //v_prev = pv_curr.next;
                        v_prev.X = pv_curr.X;
                        v_prev.Y = pv_curr.Y;
                        pv_curr = pv_curr.next;
                        continue;
                    }
                }
                pv_intersect.next = pv_curr.next;
                pv_curr.next.prev = pv_intersect;
                pv_intersect.prev = pv_curr;
                pv_curr.next = pv_intersect;
                return;
            }
            else
                pv_curr = pv_curr.next;

        } while (pv_curr != pv_start);
        throw new GeoLibException("Element konnte nicht einsortiert werden.");
    }

    private bool Intersect(Vector2d p1, Vector2d p2, Vector2d q1, Vector2d q2, out double alphaP, out double alphaQ)
    {
        alphaP = 0; alphaQ = 0;
        double wec_p1, wec_p2, wec_q1, wec_q2;
        Vector2d n = ((q2 - q1)).GetOrthogonal1().GetNorm();
        wec_p1 = ((p1 - q2)).GetSkalarProduct(n);
        wec_p2 = ((p2 - q1)).GetSkalarProduct(n);
        if (wec_p1 * wec_p2 <= 0)
        {
            n = ((p2 - p1)).GetOrthogonal1().GetNorm();
            wec_q1 = ((q1 - p1)).GetSkalarProduct(n);
            wec_q2 = ((q2 - p1)).GetSkalarProduct(n);
            if (wec_q1 * wec_q2 <= 0)
            {
                if (wec_p1 == 0 || wec_p2 == 0) alphaP = 0;
                else alphaP = wec_p1 / (wec_p1 - wec_p2);
                if (wec_q1 == 0 || wec_q2 == 0) alphaQ = 0;
                else alphaQ = wec_q1 / (wec_q1 - wec_q2);

                //intersection detected
                return true;
            }
        }
        return false;
    }

    PolyVertex p0;
    PolyVertex s0;
    protected void PhaseTwo()
    {
        //			PolyVertex p0 = (PolyVertex)arrl_poly[0],p_curr = p0;
        //			PolyVertex s0 = (PolyVertex)arrl_subject[0],s_curr = s0;
        //			PolyVertex p_curr = p0;
        //			PolyVertex s_curr = s0;
        //
        //			bool p_entry_status = true, s_entry_status = true;
        //			if(subject.PointInPolygon(p0))
        //				p_entry_status = false;
        //			if(PointInPolygon(s0))
        //				s_entry_status = false;
        //
        //			do{
        //				p_curr = p_curr.next;
        //				if(p_curr.intersect)
        //				{
        //					p_curr.entry_exit = p_entry_status;
        //					p_entry_status = !p_entry_status;
        //				}
        //			}while(p_curr != p0);
        //
        //			do
        //			{
        //				s_curr = s_curr.next;
        //				if(s_curr.intersect)
        //				{
        //					s_curr.entry_exit = s_entry_status;
        //					s_entry_status = !s_entry_status;
        //				}
        //			
        //			}while(s_curr != s0);
    }


    //suche einen start vertex f�r die findung des union polygons
    protected bool GetStartVertex(PolyVertex v_start, out PolyVertex v_curr)
    {
        v_curr = v_start;
        do
        {
            if (!subject.PointInPolygon(new Vector2d(v_curr.X,v_curr.Y)))
                return true;
            v_curr = v_curr.next;

        } while (v_curr != v_start);
        return false;
    }

    protected Polygon PhaseThree()
    {
        PolyVertex v_start;
        if (!GetStartVertex(p0, out v_start))
            if (!GetStartVertex(s0, out v_start))
                throw new GeoLibException("Das kann nicht sein es muss einen Vertex ausserhalb geben");


        Polygon p_new = new Polygon();
        PolyVertex v_curr = v_start;
        //bool getting_prevs = false;
        do
        {
            p_new.Add(new Vector2d(v_curr.X, v_curr.Y));
            if (v_curr.intersect)
            {
                //					if(v_curr.entry_exit)
                //						getting_prevs = true;
                //					else
                //						getting_prevs = false;

                v_curr = v_curr.neighbor;
            }

            //				if(!getting_prevs)				
            v_curr = v_curr.next;
            //				else
            //					v_curr = v_curr.prev;

        } while (v_start != v_curr);
        return p_new;
    }



    public Polygon Union(Polygon polygon, Polygon subject)
    {
        this.poly = polygon;
        this.subject = subject;
        PhaseOne();
        PhaseTwo();
        Polygon p = PhaseThree();
        p.Add(p[0]);
        return p;
    }




}
}
