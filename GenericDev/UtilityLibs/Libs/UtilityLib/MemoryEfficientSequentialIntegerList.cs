using System;
using System.Collections.Generic;
using System.Text;

namespace Logicx.Utilities
{

    public struct MemoryEfficientSequentialIntegerList
    {

        public MemoryEfficientSequentialIntegerList(int count_elems)
        {
            if (count_elems == 0)
            {
                _max_int_value = -1;
                curr_buffersize = 0;
                MinIndex = 0;
                MaxIndex = 0;
                List = null;
            }
            else
            {
                _max_int_value = count_elems-1;
                curr_buffersize = (int)Math.Ceiling(_max_int_value / 32f);
                if (_max_int_value % 32f == 0)
                    curr_buffersize++;
                List = new uint[curr_buffersize];
                MinIndex = 0;
                MaxIndex = _max_int_value;
            }
        }

        public List<int> ValueList
        {
            get {
                List<int> valuelist = new List<int>();
                for (int i = MinIndex; i < MaxIndex; i++)
				{
                    if (Contains(i))
                        valuelist.Add(i);
				}
                return valuelist;
            }
        }

        
        //integer value with the given value
        public void Remove(int value)
        {
            if (!Contains(value))
                return;

            int index_buffer = (int)Math.Floor(value / 32f);
            int index_bit = value % 32;

            List[index_buffer] -= (uint)(1 << index_bit);
        }

        public bool Contains(int value)
        {
            if (List == null)
                return false;

            int index_buffer = (int)Math.Floor(value / 32f);
            if (index_buffer >= List.Length)
                return false;

            int index_bit = value % 32;
            uint check_val = List[index_buffer] & (uint)(1 << index_bit);

            //integer is in list
            if (check_val == 0)
                return false;
            else
                return true;
        }

        public MemoryEfficientSequentialIntegerList Clone()
        {
            MemoryEfficientSequentialIntegerList clone = new MemoryEfficientSequentialIntegerList();

            clone._max_int_value = _max_int_value;
            clone.curr_buffersize = (int)Math.Ceiling(_max_int_value / 32f);
            if (_max_int_value % 32f == 0)
                curr_buffersize++;
            if(List != null)
                clone.List = (uint[])List.Clone();
            clone.MinIndex = MinIndex;
            clone.MaxIndex = MaxIndex;

            return clone;
        }

        public int Count
        {
            get {
                return _max_int_value+1;
            }
        }

        public void Add(int value)
        {
            if (Contains(value))
                return;

            //resize buffer list if value is bigger than max value
            if (value > _max_int_value)
            {
                _max_int_value = value;
                MaxIndex = _max_int_value;

                int buffersize = (int)Math.Ceiling(_max_int_value / 32f);
                if (_max_int_value % 32f == 0)
                    buffersize++;

                if (buffersize > curr_buffersize)
                {
                    uint[] new_list = new uint[buffersize];
                    for (int i = 0; i < curr_buffersize; i++)
                        new_list[i] = List[i];
                    List = new_list;
                    curr_buffersize = buffersize;
                }
            }

            //set value to existent
            int index_buffer = (int)Math.Floor(value / 32f);
            int index_bit = value % 32;

            List[index_buffer] += (uint)(1 << index_bit);
        }

        public override string ToString()
        {
            string res = "";
            for (int i = 0; i < Count; i++)
            {
                //enable fast iteration through list
                if (i % 32 == 0 && List[(int)Math.Floor(i / 32f)] == 0)
                {
                    i += 31;
                    continue;
                }
                if (!Contains(i))
                    continue;
                res += i+",";

                if (i > 10000)
                {
                    res += "... QUITING HERE AS LIST TO LONG";
                    break;
                }
            }
            return res;
        }

        private int curr_buffersize;
        public uint[] List;
        private int _max_int_value;
        public int MinIndex;
        public int MaxIndex;
    }
}
