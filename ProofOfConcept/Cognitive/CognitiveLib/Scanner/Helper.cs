﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using MathLib;

namespace CognitiveLib.Scanner
{


    public static class ExtensionMethods
    {
        /// <summary>
        /// returns all the start indices of the found string
        /// </summary>
        /// <param name="s_org"></param>
        /// <param name="s_cmp"></param>
        /// <returns></returns>
        public static List<int> IndexOfAll(this string s_org, string s_cmp)
        {

            int start_index = 0;
            List<int> found_start_index = new List<int>(3);
            while(true)
            {
                int index_of = s_org.IndexOf(s_cmp, start_index, StringComparison.Ordinal);
                if (index_of < 0)
                    break;

                found_start_index.Add(index_of);

                start_index = index_of+1;
                if (start_index >= s_org.Length)
                    break;
            }

            return found_start_index;
        }

        public static VectorOfPointF ToPointF(this PointF[] points)
        {
            return new VectorOfPointF(points);
        }


        public static PointF ToPointF(this Point point)
        {
            return new PointF(point.X, point.Y);
        }

        public static PointF ToPoint(this PointF point)
        {
            return new PointF(point.X, point.Y);
        }

        public static PointF[] ToPointF(this Point[] points)
        {
            PointF[] points_ret = new PointF[points.Length];

            for (int i = 0; i < points_ret.Length; i++)
            {
                points_ret[i].X = points[i].X;
                points_ret[i].Y = points[i].Y;
            }

            return points_ret;
        }

        public static Vector2f ToVector2F(this Point point)
        {
            return new Vector2f(point.X, point.Y);
        }

        public static Vector2f[] ToVector2F(this Point[] points)
        {
            Vector2f[] points_ret = new Vector2f[points.Length];

            for (int i = 0; i < points_ret.Length; i++)
            {
                points_ret[i].X = points[i].X;
                points_ret[i].Y = points[i].Y;
            }

            return points_ret;
        }

        public static Point ToPoint(this Vector2d v)
        {
            return new Point((int)Math.Round(v.X), (int)Math.Round(v.Y));
        }

        public static Vector2d ToVector2d(this Point point)
        {
            return new Vector2d(point.X, point.Y);
        }

        public static Vector2d[] ToVector2d(this Point[] points)
        {
            Vector2d[] points_ret = new Vector2d[points.Length];

            for (int i = 0; i < points_ret.Length; i++)
            {
                points_ret[i].X = points[i].X;
                points_ret[i].Y = points[i].Y;
            }

            return points_ret;
        }

        public static Point[] ToPoint(this PointF[] points)
        {
            Point[] points_ret = new Point[points.Length];

            for (int i = 0; i < points_ret.Length; i++)
            {
                points_ret[i].X = Convert.ToInt32(points[i].X);
                points_ret[i].Y = Convert.ToInt32(points[i].Y);
            }

            return points_ret;
        }
    }


    public static class LINQExtension
    {
        public static T MaxBy<T, R>(this IEnumerable<T> en, Func<T, R> evaluate) where R : IComparable<R>
        {
            return en.Select(t => new Tuple<T, R>(t, evaluate(t)))
                .Aggregate((max, next) => next.Item2.CompareTo(max.Item2) > 0 ? next : max).Item1;
        }

        public static T MinBy<T, R>(this IEnumerable<T> en, Func<T, R> evaluate) where R : IComparable<R>
        {
            return en.Select(t => new Tuple<T, R>(t, evaluate(t)))
                .Aggregate((max, next) => next.Item2.CompareTo(max.Item2) < 0 ? next : max).Item1;
        }
    }

    public class OpenCvHelper
    {
        public static IImage LoadImageFromEncodedBytes(byte[] data)
        {
            Mat dest = new Mat();
            CvInvoke.Imdecode(data, ImreadModes.AnyColor, dest);
            return dest.ToImage<Bgr, Byte>();
        }

        public static byte[] EncodedBytesFromImage(IImage image)
        {
            VectorOfByte dest = new VectorOfByte();
            CvInvoke.Imencode(".jpg", image, dest);
            return dest.ToArray();
        }

        //public static IImage CreateImageFromBytes(byte[] data, int width, int height)
        //{
        //    ////load your 3 channel bgr image here
        //    //Mat m1 = ...;

        //    ////3 channel bgr image data, if it is single channel, the size should be m1.Width * m1.Height
        //    //byte[] data = new byte[m1.Width * m1.Height * 3];

        //    GCHandle handle = GCHandle.Alloc(data, GCHandleType.Pinned);
        //    using (Mat m2 = new Mat(new Size(width,height), DepthType.Cv8U, 3, handle.AddrOfPinnedObject(), width * 3))
        //        CvInvoke.BitwiseNot(m1, m2);
        //    handle.Free();
        //}


        /// <summary>
        /// Laden ein Image per OpenCV und macht gegebenen falls ein resize
        /// </summary>
        /// <param name="path_to_image"></param>
        /// <param name="max_dim_len">dieser wert stellt sicher dass das bild nicht mehr als 2000 px an jeder seite hat, sollte es grösser sein wird ein resize auf das die max dim len durchgeführt</param>
        public static Image<Bgr, byte> LoadImage(string path_to_image, int max_dim_len = 2000)
        {
            int max_len_img = 2000;
            Image<Bgr, byte> image = new Image<Bgr, byte>(path_to_image);
            Size size = new Size();
            float aspect = image.Size.Width / (float) image.Size.Height;

            if (image.Size.Width < max_dim_len && image.Size.Height < max_dim_len)
                return image;

            if (aspect > 1)
            {
                int height = (int) (max_len_img / aspect);
                size = new Size(max_len_img, height);
            }
            else
            {
                int width = (int) ((float) aspect * (float) max_len_img);
                size = new Size(width, max_len_img);
            }

            Image<Bgr, byte> resized_img = new Image<Bgr, byte>(size);
            CvInvoke.Resize(image, resized_img, size, 0, 0, Inter.Area);
            image.Dispose();
            return resized_img;
        }

        public static PointF[] OrderRectPoints(Point[] points)
        {
            if (points.Length != 4)
                throw new ArgumentException("Order works only for four points!");
            List<Point> pointsl = points.ToList();

            //	# initialzie a list of coordinates that will be ordered
            //	# such that the first entry in the list is the top-left,
            //	# the second entry is the top-right, the third is the
            //	# bottom-right, and the fourth is the bottom-left
            //	rect = np.zeros((4, 2), dtype = "float32")
            Point[] rect_points = new Point[4];

            //	# the top-left points will have the smallest sum, whereas
            //	# the bottom-right points will have the largest sum
            //	s = pts.sum(axis = 1)
            //	rect[0] = pts[np.argmin(s)]
            rect_points[0] = points.MinBy(p => p.X + p.Y);
            pointsl.Remove(rect_points[0]);


            //	rect[2] = pts[np.argmax(s)]
            rect_points[2] = points.MaxBy(p => p.X + p.Y);
            pointsl.Remove(rect_points[2]);

            //	# now, compute the difference between the points, the
            //	# top-right points will have the smallest difference,
            //	# whereas the bottom-left will have the largest difference
            //	diff = np.diff(pts, axis = 1)
            //	rect[1] = pts[np.argmin(diff)]
            rect_points[1] = pointsl.MinBy(p => p.Y);
            pointsl.Remove(rect_points[1]);

            //	rect[3] = pts[np.argmax(diff)]
            rect_points[3] = pointsl[0];
            //	# return the ordered coordinates
            //	return rect
            return rect_points.ToPointF();
        }

        public static void FourPointTransform(IInputArray img_from, IOutputArray img_to, Point[] rect_points_int)
        {
            //	# obtain a consistent order of the points and unpack them
            //	# individually
            //	rect = order_points(pts)
            //	(tl, tr, br, bl) = rect
            PointF[] rect_pointsf = OrderRectPoints(rect_points_int);

            PointF br = rect_pointsf[2];
            PointF tl = rect_pointsf[0];
            PointF tr = rect_pointsf[1];
            PointF bl = rect_pointsf[3];

            //	# compute the width of the new image, which will be the
            //	# maximum distance between bottom-right and bottom-left
            //	# x-coordiates or the top-right and top-left x-coordinates
            float widthA = (float) Math.Sqrt(Math.Pow(br.X - bl.X, 2) + Math.Pow(br.Y - bl.Y, 2));
            float widthB = (float) Math.Sqrt(Math.Pow(tr.X - tl.X, 2) + Math.Pow(tr.Y - tl.Y, 2));
            float maxWidth = Math.Max((int) (widthA), (int) (widthB));


            //	# compute the height of the new image, which will be the
            //	# maximum distance between the top-right and bottom-right
            //	# y-coordinates or the top-left and bottom-left y-coordinates
            float heightA = (float) Math.Sqrt(Math.Pow(tr.X - br.X, 2) + Math.Pow(tr.Y - br.Y, 2));
            float heightB = (float) Math.Sqrt(Math.Pow(tl.X - bl.X, 2) + Math.Pow(tl.Y - bl.Y, 2));
            float maxHeight = Math.Max((int) (heightA), (int) (heightB));

            //	# now that we have the dimensions of the new image, construct
            //	# the set of destination points to obtain a "birds eye view",
            //	# (i.e. top-down view) of the image, again specifying points
            //	# in the top-left, top-right, bottom-right, and bottom-left
            //	# order
            PointF[] dst = new PointF[]
            {
                new PointF(),
                new PointF(maxWidth - 1, 0),
                new PointF(maxWidth - 1, maxHeight - 1),
                new PointF(0, maxHeight - 1)
            };

            //dst[1].X = dst[2].X = dst[1].X / 2;
            //dst[2].Y = dst[3].Y = dst[2].Y / 2;

            //	# compute the perspective transform matrix and then apply it
            Mat m = CvInvoke.GetPerspectiveTransform(rect_pointsf, dst);
            CvInvoke.WarpPerspective(img_from, img_to, m, new Size((int) maxWidth, (int) maxHeight));
        }

        /// <summary>
        /// this is my own hack method, i dont trust the opencv iscontouconvex method
        /// </summary>
        /// <param name="contour"></param>
        /// <returns></returns>
        public static bool IsContourConvex(VectorOfPoint contour)
        {
            List<Point> hull = Geometry.MakeConvexHull(contour.ToArray().ToList());
            return (hull.Count == contour.Size);

            //Point[] hull_arr = hull.ToArray();
            //VectorOfPoint hull_vop = new VectorOfPoint(hull_arr);
            //double area_hull = CvInvoke.ContourArea(hull_vop);
            //hull_vop.Dispose();

            //double area_contour = CvInvoke.ContourArea(contour);

            //if(Math.Abs(area_contour - area_hull) < 30f)
            //    return true;
            //else
            //    return false;
        }


        public static float GetLenFromConvexPoly(List<Point> hull)
        {
            if (hull[0] != hull[hull.Count - 1])
                throw new Exception("Poly is not closed, maybe its not a convext poly");

            return GetLen(hull);
        }

        public static float GetLen(Point p1, Point p2)
        {
            Point pdiff = new Point(p2.X - p1.X, p2.Y - p2.Y);
            return (float) Math.Sqrt(pdiff.X * pdiff.X + pdiff.Y * pdiff.Y);
        }

        public static float GetLen(List<Point> poly)
        {
            float len = 0;
            for (int i = 0; i < poly.Count - 1; i++)
            {
                len += GetLen(poly[i], poly[i + 1]);
            }
            return len;
        }

        /// <summary>
        /// returns percentage value of contour is poly lets say return value = 1 means this is 100% a contour
        /// </summary>
        /// <param name="contour"></param>
        /// <returns></returns>
        public static float IsConvexPolygonPerc(VectorOfPoint contour)
        {
            List<Point> poly = contour.ToArray().ToList();
            if (poly[0] != poly[poly.Count - 1])
                poly.Add(poly[0]);
            float len = GetLen(poly);
            List<Point> hull = Geometry.MakeConvexHull(poly);
            float len_hull = GetLenFromConvexPoly(hull);

            //basically should be impossible
            if (len < len_hull)
                return 1;

            return len_hull / len;
        }


        public static float GetSolidity(VectorOfPoint contour)
        {
            double area = CvInvoke.ContourArea(contour);
            VectorOfPoint hull_vop = new VectorOfPoint();
            CvInvoke.ConvexHull(contour, hull_vop);
            double hull_area = CvInvoke.ContourArea(hull_vop);
            float solidity = (float)area / (float)hull_area;
            hull_vop.Dispose();
            return solidity;
        }

        public static bool IsRect(VectorOfPoint contour)
        {
            if (contour.Size > 8)
                return false;
            if (contour.Size < 4)
                return false;

            List<Point> hull = Geometry.MakeConvexHull(contour.ToArray().ToList());
            float len_hull = GetLen(hull);

            Point tl = new Point(), tr = new Point(), bl = new Point(), br = new Point();
            Geometry.GetMinMaxCorners(hull, ref tl, ref tr, ref bl, ref br);
            Point[] bbox_hull = new Point[4] {tl, tr, br, bl};
            List<Point> bbox_hull_list = bbox_hull.ToList();
            bbox_hull_list.Add(tl);
            float len_rect = OpenCvHelper.GetLen(bbox_hull_list);

            float area_diff = Math.Abs(len_rect - len_hull);
            if (area_diff < 1f)
                return true;
            else
                return false;
        }
    }
}
