﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Management;
using System.Text;

namespace LoggingLib.SystemInterop
{
    /// <summary>
    /// Collects information about the computer's hardware
    /// </summary>
    public class SystemInfoCollector
    {
        public SystemInfoCollector()
        {
            _computer_name = Environment.MachineName;
            _user_domain = Environment.UserDomainName;
            _logon_server = Environment.GetEnvironmentVariable("LOGONSERVER");
            if(string.IsNullOrEmpty(_logon_server))
            {
                _logon_server = "\\\\" + _computer_name;
            }

            GetOperatingSystemInfo();
            GetDiskInfo();
            GetComputerSystemInfo();
            GetProcessorInfo();
            GetPageFileInfo();
            GetBIOSInfo();
            GetNetworkInfo();
            GetSoundInfo();
            GetVideoInfo();
        }

        #region Operations and Methods
        /// <summary>
        /// Queries WMI ang reads basic operating system information
        /// </summary>
        private void GetOperatingSystemInfo()
        {
            string wmiQueryObject = "Win32_OperatingSystem";

            _service_pack_info = "{0}.{1}";
            ManagementObjectSearcher searcher;
            int i = 0;
#if DEBUG
            ArrayList hd = new ArrayList();
#endif
            try
            {
                searcher = new ManagementObjectSearcher("SELECT * FROM " + wmiQueryObject);

                foreach (ManagementObject wmi_HD in searcher.Get())
                {
                    i++;
                    PropertyDataCollection searcherProperties = wmi_HD.Properties;
                    foreach (PropertyData sp in searcherProperties)
                    {
#if DEBUG
                        hd.Add(sp);
#endif

                        if(sp != null)
                        {
                            if(sp.Name == "Caption")
                                _operating_system = Convert.ToString(sp.Value);
                            else if(sp.Name == "ServicePackMajorVersion")
                                _service_pack_info = _service_pack_info.Replace("{0}", Convert.ToString(sp.Value));
                            else if (sp.Name == "ServicePackMinorVersion")
                                _service_pack_info = _service_pack_info.Replace("{1}", Convert.ToString(sp.Value));
                            else if (sp.Name == "SystemDrive")
                                _system_drive_letter = Convert.ToString(sp.Value);
                            else if (sp.Name == "FreePhysicalMemory")
                                _available_ram = Convert.ToDouble(sp.Value) * 1024;
                            else if (sp.Name == "OSArchitecture")
                                _os_architecture = Convert.ToString(sp.Value);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                Debug.WriteLine("GetOperatingSystemInfo(): " + ex.ToString());
#endif
            }

        }

        /// <summary>
        /// Queries WMI and reads basic disk information
        /// </summary>
        private void GetDiskInfo()
        {
            try
            {
                //create our ManagementObject, passing it the drive letter to the
                //DevideID using WQL
                ManagementObject disk = new ManagementObject("Win32_LogicalDisk.DeviceID=\"" + _system_drive_letter + "\"");
                disk.Get();

                _free_sys_disk_space = Convert.ToDouble(disk["FreeSpace"]);
            }
            catch (Exception ex)
            {
#if DEBUG
                Debug.WriteLine("GetDiskInfo(): " + ex.ToString());
#endif
            }
        }

        /// <summary>
        /// Queries WMI to get computer system information
        /// </summary>
        private void GetComputerSystemInfo()
        {
            string wmiQueryObject = "Win32_ComputerSystem";

            ManagementObjectSearcher searcher;
            int i = 0;
#if DEBUG
            ArrayList hd = new ArrayList();
#endif
            try
            {
                searcher = new ManagementObjectSearcher("SELECT * FROM " + wmiQueryObject);

                foreach (ManagementObject wmi_HD in searcher.Get())
                {
                    i++;
                    PropertyDataCollection searcherProperties = wmi_HD.Properties;
                    foreach (PropertyData sp in searcherProperties)
                    {
#if DEBUG
                        hd.Add(sp);
#endif

                        if (sp != null)
                        {
                            if (sp.Name == "TotalPhysicalMemory")
                                _total_ram = Convert.ToDouble(sp.Value);
                            else if (sp.Name == "Manufacturer")
                                _manufacturer = Convert.ToString(sp.Value);
                            else if (sp.Name == "Model")
                                _model = Convert.ToString(sp.Value);
                            else if (sp.Name == "NumberOfLogicalProcessors")
                                _number_of_logical_cpu = Convert.ToInt32(sp.Value);
                            else if (sp.Name == "NumberOfProcessors")
                                _number_of_physical_cpu = Convert.ToInt32(sp.Value);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                Debug.WriteLine("GetComputerSystemInfo(): " + ex.ToString());
#endif
            }
        }

        /// <summary>
        /// Queries WMI to get processor information
        /// </summary>
        private void GetProcessorInfo()
        {
            string wmiQueryObject = "Win32_Processor";

            ManagementObjectSearcher searcher;
            int i = 0;
#if DEBUG
            ArrayList hd = new ArrayList();
#endif
            try
            {
                searcher = new ManagementObjectSearcher("SELECT * FROM " + wmiQueryObject);

                foreach (ManagementObject wmi_HD in searcher.Get())
                {
                    i++;
                    PropertyDataCollection searcherProperties = wmi_HD.Properties;
                    foreach (PropertyData sp in searcherProperties)
                    {
#if DEBUG
                        hd.Add(sp);
#endif

                        if (sp != null)
                        {
                            if (sp.Name == "NumberOfCores")
                                _number_of_processor_cores = Convert.ToInt32(sp.Value);
                            else if (sp.Name == "ProcessorId")
                                _cpu_id = Convert.ToString(sp.Value);
                            else if (sp.Name == "CurrentClockSpeed")
                                _cpu_speed = Convert.ToInt32(sp.Value);
                            else if (sp.Name == "Caption")
                                _cpu_type = Convert.ToString(sp.Value);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                Debug.WriteLine("GetProcessorInformation(): " + ex.ToString());
#endif
            }
        }

        /// <summary>
        /// Queries WMI to get pagefile information
        /// </summary>
        private void GetPageFileInfo()
        {
            string wmiQueryObject = "Win32_PageFileSetting";

            ManagementObjectSearcher searcher;
            int i = 0;
#if DEBUG
            ArrayList hd = new ArrayList();
#endif
            try
            {
                searcher = new ManagementObjectSearcher("SELECT * FROM " + wmiQueryObject);

                foreach (ManagementObject wmi_HD in searcher.Get())
                {
                    i++;
                    PropertyDataCollection searcherProperties = wmi_HD.Properties;
                    foreach (PropertyData sp in searcherProperties)
                    {
#if DEBUG
                        hd.Add(sp);
#endif

                        if (sp != null)
                        {
                            if (sp.Name == "MaximumSize")
                                _all_page_file_size += Convert.ToDouble(sp.Value);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                Debug.WriteLine("GetPageFileInfo(): " + ex.ToString());
#endif
            }
        }

        /// <summary>
        /// Queries WMI to get BIOS information
        /// </summary>
        private void GetBIOSInfo()
        {
            string wmiQueryObject = "Win32_BIOS";

            ManagementObjectSearcher searcher;
            int i = 0;
#if DEBUG
            ArrayList hd = new ArrayList();
#endif
            try
            {
                searcher = new ManagementObjectSearcher("SELECT * FROM " + wmiQueryObject);

                foreach (ManagementObject wmi_HD in searcher.Get())
                {
                    i++;
                    PropertyDataCollection searcherProperties = wmi_HD.Properties;
                    foreach (PropertyData sp in searcherProperties)
                    {
#if DEBUG
                        hd.Add(sp);
#endif

                        if (sp != null)
                        {
                            if (sp.Name == "SerialNumber")
                                _bios_serial_number = Convert.ToString(sp.Value);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                Debug.WriteLine("GetBIOSInfo(): " + ex.ToString());
#endif
            }
        }

        /// <summary>
        /// Queries WMI to get network information
        /// </summary>
        private void GetNetworkInfo()
        {
            string wmiQueryObject = "Win32_NetworkAdapterConfiguration";

            ManagementObjectSearcher searcher;
            int i = 0;
            int cnt = 0;
#if DEBUG
            ArrayList hd = new ArrayList();
#endif
            try
            {
                searcher = new ManagementObjectSearcher("SELECT * FROM " + wmiQueryObject + " where ipenabled=TRUE");

                foreach (ManagementObject wmi_HD in searcher.Get())
                {
                    i++;
                    PropertyDataCollection searcherProperties = wmi_HD.Properties;
                    foreach (PropertyData sp in searcherProperties)
                    {
#if DEBUG
                        hd.Add(sp);
#endif

                        if (sp != null)
                        {
                            if (sp.Name == "Description")
                            {
                                if( cnt == 0)
                                {
                                    _nic1 = Convert.ToString(sp.Value);
                                    cnt++;
                                }
                                else if (cnt == 1)
                                {
                                    _nic2 = Convert.ToString(sp.Value);
                                    cnt++;
                                }
                                else if (cnt == 2)
                                {
                                    _nic3 = Convert.ToString(sp.Value);
                                    cnt++;
                                }
                                else if (cnt == 3)
                                {
                                    _nic4 = Convert.ToString(sp.Value);
                                    cnt++;
                                }
                            }

                            if (sp.Name == "IPAddress")
                            {
                                if (cnt == 0)
                                {
                                    _nic1 += " (" + Convert.ToString(sp.Value) + ")";
                                }
                                else if (cnt == 1)
                                {
                                    _nic2 += " (" + Convert.ToString(sp.Value) + ")";
                                }
                                else if (cnt == 2)
                                {
                                    _nic3 += " (" + Convert.ToString(sp.Value) + ")";
                                }
                                else if (cnt == 3)
                                {
                                    _nic4 += " (" + Convert.ToString(sp.Value) + ")";
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                Debug.WriteLine("GetNetworkInfo(): " + ex.ToString());
#endif
            }
        }


        /// <summary>
        /// Queries WMI to get sound information
        /// </summary>
        private void GetSoundInfo()
        {
            string wmiQueryObject = "Win32_SoundDevice";

            ManagementObjectSearcher searcher;
            int i = 0;
#if DEBUG
            ArrayList hd = new ArrayList();
#endif
            try
            {
                searcher = new ManagementObjectSearcher("SELECT * FROM " + wmiQueryObject + "");

                foreach (ManagementObject wmi_HD in searcher.Get())
                {
                    i++;
                    PropertyDataCollection searcherProperties = wmi_HD.Properties;
                    foreach (PropertyData sp in searcherProperties)
                    {
#if DEBUG
                        hd.Add(sp);
#endif

                        if (sp != null)
                        {
                            if (sp.Name == "Description")
                            {
                                _sound_card = Convert.ToString(sp.Value);
                                return; // only get the first sound card
                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                Debug.WriteLine("GetSoundInfo(): " + ex.ToString());
#endif
            }
        }

        /// <summary>
        /// Queries WMI to get video information
        /// </summary>
        private void GetVideoInfo()
        {
            string wmiQueryObject = "Win32_VideoController";

            ManagementObjectSearcher searcher;
            int i = 0;
#if DEBUG
            ArrayList hd = new ArrayList();
#endif
            try
            {
                searcher = new ManagementObjectSearcher("SELECT * FROM " + wmiQueryObject + "");

                foreach (ManagementObject wmi_HD in searcher.Get())
                {
                    i++;
                    PropertyDataCollection searcherProperties = wmi_HD.Properties;
                    foreach (PropertyData sp in searcherProperties)
                    {
#if DEBUG
                        hd.Add(sp);
#endif

                        if (sp != null)
                        {
                            if (sp.Name == "Description")
                            {
                                _video_card = Convert.ToString(sp.Value);
                                return; // only get the first video controller
                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                Debug.WriteLine("GetVideoInfo(): " + ex.ToString());
#endif
            }
        }
        #endregion

        #region Properties

        public string ComputerName
        {
            get { return _computer_name;  }
        }

        public string OperatingSystem
        {
            get { return string.Format("{0} ({1})", _operating_system, _os_architecture); }
        }

        public string ServicePackInfo
        {
            get { return string.IsNullOrEmpty(_service_pack_info) ? "N/A" : string.Format("Service Pack {0}", _service_pack_info); }            
        }

        public double FreeSysDiskSpace
        {
            get { return _free_sys_disk_space; }
        }

        public double TotalRAM
        {
            get { return _total_ram;  }
        }

        public double AvailableRAM
        {
            get { return _available_ram;  }
        }

        public string PCManufacturer
        {
            get { return _manufacturer;  }
        }

        public string PCModel
        {
            get { return _model; }
        }

        public int NumberOfPhysicalCPU
        {
            get { return _number_of_physical_cpu;  }
        }

        public int NumberOfLogicalCPU
        {
            get { return _number_of_logical_cpu; }
        }

        public int NumerOfProcessorCores
        {
            get { return _number_of_processor_cores; }
        }

        public string CpuId
        {
            get { return _cpu_id; }
        }

        public int CpuSpeed
        {
            get { return _cpu_speed; }
        }

        public string CpuType
        {
            get { return _cpu_type; }
        }

        public string PageFileSize
        {
            get { return (_all_page_file_size == 0.0 ? "N/A" : _all_page_file_size.ToString()); }
        }

        public string BiosSerialNumber
        {
            get { return _bios_serial_number; }
        }

        public string NIC1
        {
            get { return string.IsNullOrEmpty(_nic1) ? "N/A" : _nic1;}
        }

        public string NIC2
        {
            get { return string.IsNullOrEmpty(_nic2) ? "N/A" : _nic2; }
        }

        public string NIC3
        {
            get { return string.IsNullOrEmpty(_nic3) ? "N/A" : _nic3; }
        }

        public string NIC4
        {
            get { return string.IsNullOrEmpty(_nic4) ? "N/A" : _nic4; }
        }

        public string SoundCard
        {
            get { return _sound_card; }
        }

        public string VideoController
        {
            get { return _video_card; }
        }

        public string UserDomain
        {
            get { return _user_domain; }
        }

        public string LogonServer
        {
            get { return _logon_server; }
        }
        #endregion

        #region Attribs

        private string _computer_name = "";
        private string _operating_system = "";
        private string _service_pack_info = "";
        private string _os_architecture = "";
        private string _system_drive_letter = "C:";
        private double _free_sys_disk_space = 0.0;
        private double _total_ram = 0.0;
        private double _available_ram = 0.0;
        private string _manufacturer = "";
        private string _model = "";
        private int _number_of_logical_cpu = 0;
        private int _number_of_physical_cpu = 0;
        private int _number_of_processor_cores = 0;
        private string _cpu_id = "";
        private int _cpu_speed = 0;
        private string _cpu_type = "";
        private double _all_page_file_size = 0.0;
        private string _bios_serial_number = "";

        private string _nic1 = "";
        private string _nic2 = "";
        private string _nic3 = "";
        private string _nic4 = "";

        private string _sound_card = "";
        private string _video_card = "";

        private string _user_domain = "";
        private string _logon_server = "";
        #endregion
    }
}
