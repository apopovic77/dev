using System;
using System.Collections.Generic;
using System.Text;

namespace MathLib
{
    /// <summary>
    /// y = k * x + d
    /// </summary>
    public class LinearEquation
    {
        public LinearEquation(float k, float d) {
            _k = k;
            _d = d;
        }

        public LinearEquation(Vector2f steutzstelle_1, Vector2f steutzstelle_2)
        {
            _k = (steutzstelle_2.Y - steutzstelle_1.Y)/(steutzstelle_2.X - steutzstelle_1.X);
            _d = steutzstelle_1.Y + _k * steutzstelle_1.X;
        }

        public float GetFx(float x) {
            float fx = _k * x + _d;
            return fx;
        }

        protected float _k, _d;
    }
}
