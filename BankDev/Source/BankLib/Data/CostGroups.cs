﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankLib.Data
{
    public class CostGroups
    {
        public static Dictionary<string, string> Values = new Dictionary<string, string>()
        {
            {/*UmsatzType#*/"EUCO CENTER",                         /*GroupName*/"Essen"},
            {/*UmsatzType#*/"ENI",                         /*GroupName*/"Tanken"},
            {/*UmsatzType#*/"BANKOMAT",                         /*GroupName*/"Bankomat"},
            {/*UmsatzType#*/"",                         /*GroupName*/""},
            {/*UmsatzType#*/"",                         /*GroupName*/""},
            {/*UmsatzType#*/"",                         /*GroupName*/""},
            {/*UmsatzType#*/"",                         /*GroupName*/""},
            {/*UmsatzType#*/"",                         /*GroupName*/""},
            {/*UmsatzType#*/"",                         /*GroupName*/""},
            {/*UmsatzType#*/"",                         /*GroupName*/""},
            {/*UmsatzType#*/"",                         /*GroupName*/""},
            {/*UmsatzType#*/"",                         /*GroupName*/""},
            {/*UmsatzType#*/"",                         /*GroupName*/""},
            {/*UmsatzType#*/"",                         /*GroupName*/""},
            {/*UmsatzType#*/"",                         /*GroupName*/""},
            {/*UmsatzType#*/"",                         /*GroupName*/""},
            {/*UmsatzType#*/"",                         /*GroupName*/""},
            {/*UmsatzType#*/"",                         /*GroupName*/""},
            //{/*UmsatzTextOrg#*/"",                         /*UmsatzType*/""},
            //{/*UmsatzTextOrg#*/"",                         /*UmsatzType*/""},
            //{/*UmsatzTextOrg#*/"",                         /*UmsatzType*/""},
            //{/*UmsatzTextOrg#*/"",                         /*UmsatzType*/""},
            //{/*UmsatzTextOrg#*/"",                         /*UmsatzType*/""},
            //{/*UmsatzTextOrg#*/"",                         /*UmsatzType*/""},
            //{/*UmsatzTextOrg#*/"",                         /*UmsatzType*/""},
            //{/*UmsatzTextOrg#*/"",                         /*UmsatzType*/""},
            //{/*UmsatzTextOrg#*/"",                         /*UmsatzType*/""},
            //{/*UmsatzTextOrg#*/"",                         /*UmsatzType*/""},
            //{/*UmsatzTextOrg#*/"",                         /*UmsatzType*/""},
            //{/*UmsatzTextOrg#*/"",                         /*UmsatzType*/""},
            //{/*UmsatzTextOrg#*/"",                         /*UmsatzType*/""},
        };
    }
}

