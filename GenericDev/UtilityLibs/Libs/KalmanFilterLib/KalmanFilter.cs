using System;
using System.Collections.Generic;
using System.Text;
using MathNet.Numerics.LinearAlgebra;


namespace Logicx.KalmanFilter
{
    /// <summary>
    /// This corresponds to a general description of kalman filter needed variables. This calls is abstract and should be
    /// used as a base for createn specific applications using a kalman filter
    /// </summary>
    public abstract class KalmanFilter
    {
		
        ///// <summary>
        ///// v ... is the the white noise of the meassurment with zero mean, this noise is uncorrelated with the input and with the noise w
        ///// </summary>
        //public double StdDeviationV
        //{
        //    set {
        //        _std_deviation_v = value;
        //    }
			
        //    get {
        //        return _std_deviation_v;
        //    }
        //}
		
        ///// <summary>
        ///// w ... is the the white noise of the process with zero mean, this noise is uncorrelated with the input and with the noise v
        ///// </summary>
        //public double StdDeviationW
        //{
        //    set {
        //        _std_deviation_w = value;
        //    }
			
        //    get {
        //        return _std_deviation_w;
        //    }
        //}
		
        /// <summary>
        /// Meassurement Matrix: zj = Hxj + vj
        /// </summary>
        public Matrix H
		{
			set {
				_H = value;
			}
			
			get {
				return _H;
			}
		}
		
        /// <summary>
        /// State transition Matrix: xj = A xj-1 + Buj + wj
        /// </summary>
        public Matrix A
		{
			set {
				_A = value;
			}
			
			get {
				return _A;
			}
		}
		
        /// <summary>
        /// Input Matrix: xj = A xj-1 + Buj + wj
        /// </summary>
        public Matrix B
		{
			set {
				_B = value;
			}
			
			get {
				return _B;
			}
		}
		
        /// <summary>
        /// Error Variance of Meassurement Noise
        /// </summary>
        public Matrix R
		{
			set {
				_R = value;
			}
			
			get {
				return _R;
			}
		}
		
        /// <summary>
        /// Error Variance of Process Noise
        /// </summary>
        public Matrix Q
		{
			set {
				_Q = value;
			}
			
			get {
				return _Q;
			}
		}
		
        /// <summary>
        /// 
        /// </summary>
		public Matrix Z
		{
			get {
                return H * _x_apostiori_estimate;
			}
		}




        public void DoNextStep(Matrix U_input, Matrix Z_meassurement) { 
        

            //do assertions
            if (Q == null)
                throw new Exception("Q must be set");

            if (R == null)
                throw new Exception("R must be set");

            if (_aposteriori_errorvariance == null)
                throw new Exception("_aposteriori_errorvariance must be set");

            if (_x_apostiori_estimate == null)
                throw new Exception("_x_apostiori_estimate must be set");


            // time updateing
            _x_apriori_estimate = A * _x_apostiori_estimate + B * U_input;
            _apriori_errorvariance = A * _aposteriori_errorvariance * Matrix.Transpose(A) + Q;


            // meassurement updateing        
            Matrix K;
            K = _apriori_errorvariance * Matrix.Transpose(H) * ((Matrix)(H*_apriori_errorvariance*Matrix.Transpose(H)+R)).Inverse();
            _x_apostiori_estimate = _x_apriori_estimate + K * (Z_meassurement - H * _x_apriori_estimate);

            Matrix KH = K * H;
            _aposteriori_errorvariance = (Matrix.Identity(KH.RowCount, KH.ColumnCount) - KH) * _apriori_errorvariance;        
        }

        protected Matrix _H;
        protected Matrix _A;
        protected Matrix _B;

        protected Matrix _Q;
        protected Matrix _R;

        protected Matrix _Z;

        protected Matrix _x_apriori_estimate;
        protected Matrix _x_apostiori_estimate;

        protected Matrix _apriori_errorvariance;
        protected Matrix _aposteriori_errorvariance;

    }
}
