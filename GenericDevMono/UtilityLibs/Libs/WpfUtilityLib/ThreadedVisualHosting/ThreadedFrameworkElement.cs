﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Markup;
using System.Windows.Media;
using Logicx.Utilities;

namespace Logicx.WpfUtility.VisualHosting
{
    /// <summary>
    ///     The VisualWrapper simply integrates a raw Visual child into a tree
    ///     of FrameworkElements.
    /// </summary>
    [ContentProperty("Child")]
    public abstract class ThreadedFrameworkElement : FrameworkElement, IDisposable
    {
        public ThreadedFrameworkElement(IDebugWriter debug_writer, bool start_dispatcher_immediatelly, bool auto_unload, string thread_name)
        {
            _debug_writer = debug_writer;
            _start_dispatcher_immediatelly = start_dispatcher_immediatelly;
            _auto_unload = auto_unload;
            IsHitTestVisible = false;
            Focusable = false;

            HostVisual host_visual = new HostVisual();
            Child = host_visual;

            // Spin up a worker thread, and pass it the HostVisual that it
            // should be part of.
            StartDispatcher(thread_name);

            //wenn true dann wird dispatcher start und ende von den loaded unloaded events gesteuert
            if (!_start_dispatcher_immediatelly)
            {
                this.Loaded += new RoutedEventHandler(ThreadedFrameworkElement_Loaded);
                this.Unloaded += new RoutedEventHandler(ThreadedFrameworkElement_Unloaded);
            }
            else
            {
                // Wait for the worker thread to spin up and create the VisualTarget.
                InitResetEvent.WaitOne();
            }
        }

        public ThreadedFrameworkElement(bool start_dispatcher_immediatelly, bool auto_unload, string thread_name) : this(new ConsoleDebugWriter(), start_dispatcher_immediatelly, auto_unload, thread_name)
        {
        }

        public void Dispose()
        {
            //StopDispatcher();
            try
            {
                if (_thread.ThreadState == ThreadState.Running || _thread.ThreadState == ThreadState.Background)
                {
                    _thread.Abort();
                    _thread.Join();
                    _thread = null;
                }
                StopDispatcher();
            }catch{}
        }

        void ThreadedFrameworkElement_Unloaded(object sender, RoutedEventArgs e)
        {
            if (_auto_unload)
                Dispose();
        }

        void ThreadedFrameworkElement_Loaded(object sender, RoutedEventArgs e)
        {
            // Wait for the worker thread to spin up and create the VisualTarget.
            InitResetEvent.Set();
        }

        protected abstract FrameworkElement CreateThreadedChildContent();

        protected virtual void CreatePresentationSource(object arg)
        {
            // Create the VisualTargetPresentationSource and then signal the
            // calling thread, so that it can continue without waiting for us.
            HostVisual hostVisual = (HostVisual)arg;
            _debug_writer.WriteInfo("ThreadedFrameworkElement: Creating VisualTargetPresentationSource");
            VisualTargetPresentationSource visualTargetPS = new VisualTargetPresentationSource(hostVisual);
            _debug_writer.WriteInfo("ThreadedFrameworkElement: Setting Initialized Event");
            InitResetEvent.Set();

            _debug_writer.WriteInfo("ThreadedFrameworkElement: Start CreateThreadedChildContent");
            // Create a MediaElement and use it as the root visual for the
            // VisualTarget.
            visualTargetPS.RootVisual = CreateThreadedChildContent();
            _debug_writer.WriteInfo("ThreadedFrameworkElement: Finished CreateThreadedChildContent");

            try
            {
                if (_start_dispatcher_immediatelly)
                {
                    _debug_writer.WriteInfo("ThreadedFrameworkElement: Run Dispatcher");
                    // Run a dispatcher for this worker thread.  This is the central
                    // processing loop for WPF.
                    System.Windows.Threading.Dispatcher.Run();
                }
                else
                {
                    _debug_writer.WriteInfo("ThreadedFrameworkElement: Wait for the worker thread to spin up and create the VisualTarget");
                    // Wait for the worker thread to spin up and create the VisualTarget.
                    InitResetEvent.WaitOne();

                    // Run a dispatcher for this worker thread.  This is the central
                    // processing loop for WPF.
                    System.Windows.Threading.Dispatcher.Run();
                }
            }
            catch(Exception ex)
            {
                FileStream fs = File.OpenWrite("Debug.Txt");
                StreamWriter sw = new StreamWriter(fs);

                sw.Write(ex.ToString());

                fs.Flush();
                sw.Dispose();
                fs.Dispose();
//#if DEBUG
//                MessageBox.Show(ex.ToString());
//#endif
                throw ex;
            }
        }

        public void StopDispatcher()
        {
            if (_thread == null)
                return;
            System.Windows.Threading.Dispatcher.FromThread(_thread).InvokeShutdown();
        }

        public void StartDispatcher(string dispatcher_name)
        {
            _debug_writer.WriteInfo("ThreadedFrameworkElement: Start CreatePresentationSource");
            _thread = new Thread(new ParameterizedThreadStart(CreatePresentationSource));
            _thread.Name = dispatcher_name;
            _thread.SetApartmentState(ApartmentState.STA);
            _thread.IsBackground = true;
            _thread.Start(Child);
        }

        public void StartDispatcher()
        {
            StartDispatcher(_thread.Name);
        }

        public Visual Child
        {
            get
            {
                return _child;
            }

            set
            {
                if (_child != null)
                {
                    RemoveVisualChild(_child);
                }

                _child = value;

                if (_child != null)
                {
                    AddVisualChild(_child);
                }
            }
        }

        protected override Visual GetVisualChild(int index)
        {
            if (_child != null && index == 0)
            {
                return _child;
            }
            else
            {
                throw new ArgumentOutOfRangeException("index");
            }
        }

        protected override int VisualChildrenCount
        {
            get
            {
                return _child != null ? 1 : 0;
            }
        }

        public Thread Thread
        {
            get
            {
                return _thread;
            }
        }

        private IDebugWriter _debug_writer;
        private Visual _child;
        public AutoResetEvent InitResetEvent = new AutoResetEvent(false);
        private Thread _thread;
        private bool _start_dispatcher_immediatelly;
        private bool _auto_unload;

    }
}
