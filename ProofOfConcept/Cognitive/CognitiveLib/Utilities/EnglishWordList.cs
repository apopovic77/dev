﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CognitiveLib.Utilities
{
    public class EnglishWordList
    {
		public static bool IsEnglishWord(string word)
        {
            if (EnglishWords == null)
            {
                EnglishWords = new Dictionary<string, bool>(371000);

                const Int32 BufferSize = 128;
                using (var fileStream = File.OpenRead("Utilities\\EnglishWords.txt"))
                using (var streamReader = new StreamReader(fileStream, Encoding.UTF8, true, BufferSize))
                {
                    String line;
                    while ((line = streamReader.ReadLine()) != null)
                        EnglishWords.Add(line.ToLower(), false);
                }

            }

            return EnglishWords.ContainsKey(word);
        }

        public static Dictionary<string, bool> EnglishWords;
	}
}
