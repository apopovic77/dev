﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using LinqLib.Serialization;

namespace LoggingLib.Data
{
    /// <summary>
    /// base class for the log entry table
    /// </summary>
    [DataContract()]
    public class LogEntryBase : IdentifyableObjectBase
    {
        private bool _require_data_refresh = false;

        /// <summary>
        /// Flag identifying if a data refresh of autopopulated 
        /// properties is required
        /// </summary>
        [DataMember]
        public bool RequiresDataRefresh
        {
            get
            {
                return _require_data_refresh;
            }
            set
            {
                _require_data_refresh = value;
            }
        }
    }
}
