﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Security.Principal;

namespace WindowsLib
{
    /// <summary>
    /// Allgemeine Benutzerinformationen über den aktuell angemeldeten Windows Benutzer
    /// </summary>
    public class WinUser
    {
        public static string CurrentUserName
        {
            get
            {

                WindowsIdentity current_identity = WindowsIdentity.GetCurrent();

                if (current_identity != null)
                    return current_identity.Name;

                return "n/a";
            }
        }

        public static string CurrentUserNameSimple
        {
            get
            {
                string sRet = string.Empty;

                string sCurName = CurrentUserName;
                if(sCurName.IndexOf("\\")>= 0)
                    sRet = sCurName.Substring(sCurName.IndexOf("\\") + 1);

                return sRet;
            }
        }
    }
}
