﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Threading;
using Logicx.Utilities;
using Logicx.WpfUtility.CustomControls.OverlayManager.StatusInfo;

namespace Logicx.WpfUtility.CustomControls.OverlayManager
{
    public class OverlayBackgroundAction
    {

        public OverlayBackgroundAction(IOverlayManagerBase overlay_manager)
        {
            _overlay_manager = overlay_manager;
        }

        public void StartAction(object argument)
        {
            _last_error = null;
            CanReportStatus = ShowStatus;
            _background_action_data = new QueuedBackgroundWorker();
            _background_action_data.Name = this.GetType().Name + " Background Action";
            _background_action_data.DoWork += new QueuedBackgroundWorker.DoWorkEventHandler(background_action_DoWork);
            _background_action_data.RunWorkerCompleted += new QueuedBackgroundWorker.RunWorkerCompletedEventHandler(background_action_RunWorkerCompleted);
            _background_action_data.RunWorkerAsync(argument);
        }

        public void ReportStatus(double percent, string info)
        {
            if (!CanReportStatus)
                return;

            _overlay_manager.UpdateBlockingStatusInfo(percent, info);
        }

        public object FindResource(string resource_key)
        {
            return Application.Current.FindResource(resource_key);
        }

        public object TryFindResource(string resource_key)
        {
            return Application.Current.TryFindResource(resource_key);
        }

        /// <summary>
        /// Override this method to implement background saving
        /// </summary>
        /// <param name="argument"></param>
        /// <returns></returns>
        protected virtual object BackgroundAction_Worker(object argument)
        {

            return null;
        }

        /// <summary>
        /// Override this method to do a task after background saving
        /// </summary>
        /// <param name="argument"></param>
        /// <param name="result"></param>
        /// <param name="error"></param>
        protected virtual void BackgroundAction_Worker_Completed(object argument, object result, Exception error)
        {

        }

        private void background_action_RunWorkerCompleted(object sender, Logicx.Utilities.RunWorkerCompletedEventArgs e)
        {
            _last_error = e.Error;
            // call completed method
            BackgroundAction_Worker_Completed(_background_action_argument, e.Result, e.Error);

            if (ShowStatus)
            {
                if (_overlay_manager != null)
                {
                    Application.Current.Dispatcher.Invoke(DispatcherPriority.Normal, (NoParas)delegate
                    {

                            _overlay_manager.HideBlockingStatusInfo();
                            _overlay_manager.CurrentBlockingStatusInformation.StatusControlHidden += new EventHandler(CurrentBlockingStatusInformation_StatusControlHidden);

                    });
                }
                else
                {
                    if (ActionCompleted != null)
                        ActionCompleted(this, new EventArgs());
                }
            }
            else
            {
                if (ActionCompleted != null)
                    ActionCompleted(this, new EventArgs());
            }
        }

        void CurrentBlockingStatusInformation_StatusControlHidden(object sender, EventArgs e)
        {
            OverlayBlockingProgressStatusInfo status_control = sender as OverlayBlockingProgressStatusInfo;

            if (status_control != null)
            {
                status_control.StatusControlHidden -= CurrentBlockingStatusInformation_StatusControlHidden;
            }

            if (ActionCompleted != null)
                ActionCompleted(this, new EventArgs());
        }

        private void background_action_DoWork(object sender, Logicx.Utilities.DoWorkEventArgs e)
        {
            

            if (ShowStatus)
            {
                Application.Current.Dispatcher.Invoke(DispatcherPriority.Normal, (NoParas)delegate
                {
                    if (_overlay_manager != null)
                        _overlay_manager.ShowBlockingStatusInfo(BackgroundActionStatusInfoTitle, BackgroundActionSubTitle);
                });
                
                ReportStatus(0, "");
            }

            _background_action_argument = e.Argument;

            Thread.Sleep(100); // kleine verzögerung um dem dispatcher zeit zu geben das UI anzupassen

            if (ActionStarted != null)
                ActionStarted(this, new EventArgs());
            
            e.Result = BackgroundAction_Worker(e.Argument);
        }


        protected virtual void OnActionStarted()
        {
            if (ActionStarted != null)
                ActionStarted(this, new EventArgs());
        }

        protected virtual void OnActionCompleted()
        {
            if (ActionCompleted != null)
                ActionCompleted(this, new EventArgs());
        }

        #region Events
        public event EventHandler ActionStarted;
        public event EventHandler ActionCompleted;

        #endregion

        #region Properties
        public bool CanReportStatus
        {
            get { return _can_report_status; }
            private set { _can_report_status = value; }
        }

        public bool ShowStatus
        {
            get { return _show_status; }
            set { _show_status = value; }
        }

        public string BackgroundActionStatusInfoTitle
        {
            get;
            set;
        }

        public string BackgroundActionSubTitle
        {
            get;
            set;
        }

        public Exception LastError
        {
            get { return _last_error; }
        }

        public string DataContextName
        {
            get
            {
                return "Overlay_Background_Hash_" + this.GetHashCode();
            }
        }

        protected Dispatcher Dispatcher
        {
            get { return Application.Current.Dispatcher; }
        }
        #endregion

        #region Attributes

        private QueuedBackgroundWorker _background_action_data;
        private object _background_action_argument = null;
        private bool _can_report_status;
        private bool _show_status;

        private Exception _last_error;

        private IOverlayManagerBase _overlay_manager;

        protected delegate void NoParas();
        #endregion
    }
}
