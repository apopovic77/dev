﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Logicx.WpfUtility.WindowManagement;

namespace Logicx.WpfUtility.CustomControls.AnimatedPanel
{
    /// <summary>
    /// Interaction logic for AnimatedPanelItem.xaml
    /// </summary>
    public partial class OldDialogMenuItem : SubMenuPanelMenuItem
    {
        public OldDialogMenuItem()
        {
            InitializeComponent();
        }

        public void SetContent(UIElement elem, double viewbox_width)
        {
            SetContent(elem, viewbox_width, null);
        }

        public void SetContent(UIElement elem, double viewbox_width, string info_text)
        {
            _viewbox_child = elem;

            OnSetContent(elem, viewbox_width, info_text);
        }

        protected virtual void OnSetContent(UIElement elem, double viewbox_width, string info_text)
        {
            main_border.Child = elem;
            OnSetInfoText(info_text);
        }

        protected virtual void OnSetInfoText(string info_text)
        {
            if (string.IsNullOrEmpty(info_text))
            {
                tb_info_text.Text = "";
                tb_info_text.Visibility = Visibility.Collapsed;
            }
            else
            {
                tb_info_text.Text = info_text;
                tb_info_text.Visibility = Visibility.Visible;

            }
        }

        public virtual void SetInfoTextStyle(Style text_style)
        {
            tb_info_text.Style = text_style;
        }

        public UIElement ViewBoxChild
        {
            get
            {
                return _viewbox_child;
            }
        }

        public string InfoText
        {
            get { return _info_text; }
            set
            {
                if (_info_text != value)
                {
                    _info_text = value;
                    OnSetInfoText(_info_text);
                }
            }
        }

        public double ScaleX
        {
            get
            {
                //ScaleTransform s_tran = view_box.RenderTransform as ScaleTransform;
                //if(s_tran != null)
                //    return s_tran.ScaleX;
                //else
                    return 1;
            }
        }

        public double ScaleY
        {
            get
            {
                //ScaleTransform s_tran = view_box.RenderTransform as ScaleTransform;
                //if (s_tran != null)
                //    return s_tran.ScaleY;
                //else
                    return 1;
            }
        }

        public void ResetContent()
        {
            _viewbox_child = null;
            _info_text = string.Empty;
            Tag = null;
        }

        protected virtual void OnResetContent()
        {
            main_border.Child = null;
            tb_info_text.Text = string.Empty;
            tb_info_text.Visibility = System.Windows.Visibility.Collapsed;
        }

        #region SubMenuPanelMenuItem Implementation
        public override List<MenuItemAction> GetActions()
        {
            return null;
        }

        public override List<MenuItemAttribute> GetAttributes()
        {
            return null;
        }

        public override object GetValue(MenuItemAttribute attribute)
        {
            return null;
        }

        public override void UpdateActionState()
        {
            
        }

        public override void UpdateAttributeState()
        {
            
        }

        public override SubMenuItemType ItemTyp
        {
            get
            {
                return SubMenuItemType.Button;
            }
        }

        public override MenuItemAction DefaultAction
        {
            get { return null; }
        }

        public override List<string> GetCustomFilters()
        {
            return null;
        }

        public override bool ApplyCustomFilter(string filter_name)
        {
            return true;
        }
        #endregion

        private UIElement _viewbox_child = null;
        private string _info_text = string.Empty;
    }
}
