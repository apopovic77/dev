using System;
using System.Web.Mail;

namespace Logicx.Utilities.Email
{
	/// <summary>
	/// Summary description for CEMailMsg.
	/// </summary>
	public abstract class Email
	{
		protected String m_Content;
        protected String m_Subject;
        protected String m_from_adr;
        protected String m_from_name;
        protected String m_to;
        protected String m_SMTPServer;


		~Email()
		{
			m_Content=null;
			m_Subject=null;
			m_from_adr=null;
            m_from_name = null;
			m_to=null;
			m_SMTPServer=null;
		}

		public Email(String Subject,String Content,String FromAdr,String FromName,String To,String SMTPServer)
		{
			m_Content = Content;
			m_Subject = Subject;
			m_from_name = FromName;
            m_from_adr = FromAdr;
			m_to = To;
			m_SMTPServer = SMTPServer;
		}

		public abstract void Send();
	}
}
