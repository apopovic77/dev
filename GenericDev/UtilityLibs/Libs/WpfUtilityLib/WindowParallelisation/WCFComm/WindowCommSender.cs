using System;
using System.Collections.Generic;
using System.ServiceModel;
using NamedPipe.Communication;

namespace Logicx.WpfUtility.WindowParallelisation.WCFComm
{
    // Create this class to send a message on a
    // named pipe.
    public class WindowCommSender
    {
        public WindowCommSender(string pipe_name)
        {
            _pipe_name = pipe_name;

            EndpointAddress ep = new EndpointAddress(string.Format("{0}/{1}", WindowCommReceiver.URI, _pipe_name));
            NetNamedPipeBinding pipe_binding = new NetNamedPipeBinding();
            pipe_binding.ReceiveTimeout = pipe_binding.SendTimeout = new TimeSpan(7, 0, 0, 0);

            Proxy = ChannelFactory<IPipeService>.CreateChannel(pipe_binding, ep);
        }

        public IPipeService Proxy;
        protected string _pipe_name;
    }
}
