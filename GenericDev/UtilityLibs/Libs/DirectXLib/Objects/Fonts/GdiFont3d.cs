using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace Logicx.DirectX.Objects.Fonts
{
    public class GdiFont3d
    {
        //Public members
        private int m_font_size = 48;
        private string m_font_name = "Arial";
        private Font m_font;
        private Brush m_brush;

        /// <summary>
        /// stores Bitmaps[] for each font-size an array of bitmaps
        /// </summary>
        private Hashtable m_bitmaps = new Hashtable();

        public Bitmap GetBitmap(char c)
        {
            return (Bitmap)((Bitmap[])m_bitmaps[m_font_size])[(int)c];
        }

        /// <summary>
        /// creates bitmaps for each char of the given font
        /// </summary>
        /// <param name="font">just the windows name of the font</param>
        /// <param name="font_size">the font size im em</param>
        public GdiFont3d(string font, int font_size) 
        {
            //store paras in this obj memspace
            m_font_size = font_size;
            m_font_name = font;
            m_font = new Font(m_font_name, font_size, FontStyle.Regular, GraphicsUnit.Pixel);
            m_brush = new SolidBrush(Color.Black);


            //create the font
            //the array storing all bitmaps of the given font
            Bitmap[] bitmaps = new Bitmap[128];
            FontFamily font_family = new FontFamily(font);
            for (int c = 0; c < 128; c++)
            {
                Bitmap bm = GetBmpFromChar((char)c,0);
                bitmaps[c] = bm;
            }
            m_bitmaps.Add(font_size,bitmaps);
        }


        private Bitmap GetBmpFromChar(char c,int spacing)
        {
            int font_height = Convert.ToInt32(Math.Floor(m_font_size * 1.2));
            if (font_height % 2 != 0)
                font_height++;
            //font_height = next_po2(font_height);

            //Create a bitmap in a fixed ratio to the original drawing area.
            Bitmap bm = new Bitmap(m_font_size * 2, font_height);
            //Get the graphics object for the image. 
            Graphics g_bm = Graphics.FromImage(bm);
            //draw the string into the bmp
            g_bm.DrawString(c.ToString(), m_font, m_brush, new PointF(0, 0));
            //We no longer need this graphics object
            g_bm.Dispose();

            //get width of char
            int smalles_non_transparent_pos_x = int.MaxValue;
            int greated_non_transparent_pos_x = int.MinValue;
            for (int x = 0; x < bm.Width; x++)
                for (int y = 0; y < bm.Height; y++)
                {
                    if (bm.GetPixel(x, y).ToArgb() == 0)
                        continue;
                    else
                    {
                        if (greated_non_transparent_pos_x < x)
                            greated_non_transparent_pos_x = x;
                        if (smalles_non_transparent_pos_x > x)
                            smalles_non_transparent_pos_x = x;
                    }
                }

            //int spacing_left = 1;
            //int spacing_right = 1;
            int char_width = greated_non_transparent_pos_x - smalles_non_transparent_pos_x + 1 + spacing + spacing;
            //int char_width_po2 = next_po2(char_width);
            Bitmap bm_res = new Bitmap(char_width, font_height);
            for (int x = smalles_non_transparent_pos_x; x <= greated_non_transparent_pos_x; x++)
                for (int y = 0; y < bm.Height; y++)
                {
                    int x_pos = x - smalles_non_transparent_pos_x;
                    bm_res.SetPixel(x_pos + spacing, y, bm.GetPixel(x, y));
                }


            //for (int x = 0; x < bm_res.Width; x++)
            //    for (int y = 0; y < bm_res.Height; y++)
            //    {
            //        if (bm_res.GetPixel(x, y).ToArgb() == 0)
            //            bm_res.SetPixel(x, y, Color.White);
            //    }

            bm.Dispose();
            return bm_res;
        }

        //public Bitmap GetBmpWithNiceSurrounding(char c)
        //{

        //    Bitmap bmp_inner = GetBmpFromChar(c,0);

        //    //Create a bitmap in a fixed ratio to the original drawing area.
        //    Bitmap bm = new Bitmap(bmp_inner.Width + 6, bmp_inner.Height + 6);
        //    //Create a GraphicsPath object. 
        //    GraphicsPath pth = new GraphicsPath();
        //    //Add the string in the chosen style. 
        //    pth.AddString(((char)c).ToString(), new FontFamily(m_font_name), (int)FontStyle.Regular, m_font_size, new Point(20, 20), StringFormat.GenericTypographic);
        //    //Get the graphics object for the image. 
        //    Graphics g_bm = Graphics.FromImage(bm);
        //    //Create a matrix that shrinks the drawing output by the fixed ratio. 
        //    Matrix mx = new Matrix(1.0f / 5, 0, 0, 1.0f / 5, -(1.0f / 5), -(1.0f / 5));
        //    //Choose an appropriate smoothing mode for the halo. 
        //    g_bm.SmoothingMode = SmoothingMode.AntiAlias;
        //    //Transform the graphics object so that the same half may be used for both halo and text output. 
        //    g_bm.Transform = mx;
        //    //Using a suitable pen...
        //    Pen p = new Pen(Color.Yellow, 3);
        //    //Draw around the outline of the path
        //    g_bm.DrawPath(p, pth);
        //    //and then fill in for good measure. 
        //    g_bm.FillPath(Brushes.Yellow, pth);

        //    //copy old char onto the new bmp 


        //    //We no longer need this graphics object
        //    g_bm.Dispose();
        //    //add the created bitmap to the bitmaps array
        //    return bm;            
        //}


    }
}
