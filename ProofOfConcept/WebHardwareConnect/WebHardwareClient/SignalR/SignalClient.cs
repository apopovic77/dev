﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.SignalR.Client;
using PoCShared;

namespace WebHardwareClient.SignalR
{
    public class SignalClient
    {
        private HubConnection _hubConnection;
        private IHubProxy _printerCommsHub;

        public async Task StartClient()
        {
            var endpoint = ConfigurationManager.AppSettings["apihost"];
            var hubName = "PrinterCommsHub";

            Console.WriteLine("Starting SignalR Hub client ...");
            Console.WriteLine(   $"connecting to '{hubName}' at endpoint '{endpoint}/'");
            _hubConnection = new HubConnection(endpoint);
            _hubConnection.StateChanged += _hubConnection_StateChanged;
            _printerCommsHub = _hubConnection.CreateHubProxy(hubName);
            _printerCommsHub.On<PrinterItem, string>("printDocument", ExecutePrintCommand);

            await _hubConnection.Start();
        }

        private void _hubConnection_StateChanged(StateChange obj)
        {
            Console.WriteLine($"State changed: {obj.OldState} -> {obj.NewState}");
            if (obj.NewState == ConnectionState.Connected)
            {
                Console.WriteLine($"    joining group '{Environment.MachineName}'");
                _printerCommsHub.Invoke($"JoinGroup", Environment.MachineName);
            }
        }

        public void StopClient()
        {
            Console.WriteLine($"Leaving group '{Environment.MachineName}'");
            _printerCommsHub.Invoke($"LeaveGroup", Environment.MachineName);

            Console.WriteLine("Stopping SignalR Hub client ...");
            _hubConnection.Stop();
            _hubConnection.Dispose();

            Console.WriteLine("Client stopped and disposed");
        }

        private void ExecutePrintCommand(PrinterItem printer, string url)
        {
            Console.WriteLine($"Received print command for document {url} using printer {printer.Name}");
            string filename1 = "D:\\download.pdf";

            using (var client = new WebClient())
            {
                var uri = new Uri(url);
                string filename = System.IO.Path.GetFileName(uri.AbsolutePath);
                var downloadPath =
                    Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "WebHardwareTest");
                if (!Directory.Exists(downloadPath))
                    Directory.CreateDirectory(downloadPath);

                client.DownloadFile(url, Path.Combine(downloadPath,filename));
                filename1 = Path.Combine(downloadPath, filename);
            }

            PrintDocument document = new PrintDocument();
            document.PrinterSettings.PrintFileName = filename1; // "D:\\download.pdf";
            document.PrintController = new System.Drawing.Printing.StandardPrintController();
            document.PrinterSettings.PrinterName = printer.Name;
            document.Print();
        }

        public async void UpdatePrinterState(PrinterStatusItem printerStatus)
        {
            await _printerCommsHub.Invoke($"SetPrinterStateFromClient", printerStatus);
        }
    }
}
