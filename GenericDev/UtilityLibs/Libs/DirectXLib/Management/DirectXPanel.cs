using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

using System.Runtime.InteropServices;
using Logicx.Utilities;
using Logicx.DirectX.Engine;

namespace Logicx.DirectX.Management
{
    public partial class DirectXPanel : UserControl
    {
        private DirectXManager m_dxmanager;

        public DirectXPanel()
        {
            InitializeComponent();
        }
		
		public DirectXManager DirectXManager
		{
			set {
				m_dxmanager = value;
			}
			
			get {
				return m_dxmanager;
			}
		}
		
        public void InitDirectX(LabelDebugWriter debuginfo)
        {
            m_dxmanager = new DirectXManager(this, debuginfo);
            m_dxmanager.DxWindowWidth = this.Size.Width;
            m_dxmanager.DxWindowHeight = this.Size.Height;
            m_dxmanager.InitGraphicsDevice();
        }

        public DirectXManager GetDirectXManager() {
            return m_dxmanager;
        }


        protected override void OnPaint(PaintEventArgs e)
        {
            
        }

        private void DirectXPanel_KeyDown(object sender, KeyEventArgs e)
        {
            Engine3d.KeyDown[(int)e.KeyCode] = true;
        }

        private void DirectXPanel_KeyUp(object sender, KeyEventArgs e)
        {
            Engine3d.KeyDown[(int)e.KeyCode] = false;
        }

        private void DirectXPanel_MouseMove(object sender, MouseEventArgs e)
        {
            Engine3d.MouseposX = e.X;
            Engine3d.MouseposY = e.Y;
        }

        private void DirectXPanel_MouseDown(object sender, MouseEventArgs e)
        {
            if(e.Button == MouseButtons.Left)
                Engine3d.LeftMouseButtonDown = true;
            else if(e.Button == MouseButtons.Right)
                Engine3d.RightMouseButtonDown = true;

        }

        private void DirectXPanel_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
                Engine3d.LeftMouseButtonDown = false;
            else if (e.Button == MouseButtons.Right)
                Engine3d.RightMouseButtonDown = false;
        }
    }
}
