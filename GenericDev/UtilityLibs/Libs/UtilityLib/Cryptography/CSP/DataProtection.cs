﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.Security;

namespace Utility.Cryptography.CSP
{
    /// <summary>
    /// CryptProtectPromptFlags enumeration 
    /// </summary>
    [Flags()]
    public enum CryptProtectPromptFlags
    {
        CRYPTPROTECT_PROMPT_ON_UNPROTECT = 0x01,
        CRYPTPROTECT_PROMPT_ON_PROTECT = 0x02,
        CRYPTPROTECT_PROMPT_RESERVED = 0x04,
        CRYPTPROTECT_PROMPT_STRONG = 0x08,
        CRYPTPROTECT_PROMPT_REQUIRE_STRONG = 0x10
    }
    /// <summary>
    /// CryptProtectDataFlags enumeration
    /// </summary>
    [Flags()]
    public enum CryptProtectDataFlags
    {
        CRYPTPROTECT_UI_FORBIDDEN = 0x01,
        CRYPTPROTECT_LOCAL_MACHINE = 0x04,
        CRYPTPROTECT_CRED_SYNC = 0x08,
        CRYPTPROTECT_AUDIT = 0x10,
        CRYPTPROTECT_NO_RECOVERY = 0x20,
        CRYPTPROTECT_VERIFY_PROTECTION = 0x40,
        CRYPTPROTECT_CRED_REGENERATE = 0x80
    }

    /// <summary>
    /// FormatMessageFlags enumeration for formatting Win32 error messages
    /// </summary>
    [Flags()]
    public enum FormatMessageFlags : int
    {
        FORMAT_MESSAGE_ALLOCATE_BUFFER = 0x0100,
        FORMAT_MESSAGE_IGNORE_INSERTS = 0x0200,
        FORMAT_MESSAGE_FROM_STRING = 0x0400,
        FORMAT_MESSAGE_FROM_HMODULE = 0x0800,
        FORMAT_MESSAGE_FROM_SYSTEM = 0x1000,
        FORMAT_MESSAGE_ARGUMENT_ARRAY = 0x2000,
        FORMAT_MESSAGE_MAX_WIDTH_MASK = 0xFF,
    }

    /// <summary>
    /// DATA_BLOB struct implementation
    /// </summary>
    [ComVisibleAttribute(false)]
    [StructLayoutAttribute(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
    public struct DATA_BLOB
    {
        /// <summary>
        /// See DATA_BLOB in Plattform SDK documentation
        /// </summary>
        public int cbData;
        /// <summary>
        /// See DATA_BLOB in Plattform SDK documentation
        /// </summary>
        public IntPtr pbData;

    }

    /// <summary>
    /// CRYPTPROTECT_PROMPTSTRUCT struct implementation
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct CRYPTPROTECT_PROMPTSTRUCT
    {
        /// <summary>
        /// See CRYPTPROTECT_PROMPTSTRUCT in Plattform SDK documentation
        /// </summary>
        public int cbSize; // = Marshal.SizeOf(typeof(CRYPTPROTECT_PROMPTSTRUCT))
        /// <summary>
        /// See CRYPTPROTECT_PROMPTSTRUCT in Plattform SDK documentation
        /// </summary>
        public int dwPromptFlags; // = 0
        /// <summary>
        /// See CRYPTPROTECT_PROMPTSTRUCT in Plattform SDK documentation
        /// </summary>
        public IntPtr hwndApp; // = IntPtr.Zero
        /// <summary>
        /// See CRYPTPROTECT_PROMPTSTRUCT in Plattform SDK documentation
        /// </summary>
        public string szPrompt; // = null
    }

    /// <summary>
    /// Summary description for DataProtection.
    /// </summary>
    [Serializable()]
    public class DataProtection
    {
        #region crypt32.dll imports
        /// <summary>
        /// Search for function name in Plattform SDK documentation
        /// </summary>
        //[DllImportAttribute("crypt32.dll", CharSet=CharSet.Auto, SetLastError=true)]
        //public static extern bool CryptProtectData(ref DATA_BLOB dataIn, string szDataDescr, IntPtr pEntropy, IntPtr pvReserved, IntPtr pPromptStruct, int dwFlags, ref DATA_BLOB pDataOut);

        [DllImportAttribute("crypt32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern bool CryptProtectData(ref DATA_BLOB dataIn, string szDataDescr, IntPtr optionalEntropy, IntPtr pvReserved,
            IntPtr pPromptStruct, CryptProtectDataFlags dwFlags, ref DATA_BLOB pDataOut);
        /// <summary>
        /// Search for function name in Plattform SDK documentation
        /// </summary>
        //[DllImportAttribute("crypt32.dll", CharSet=CharSet.Auto, SetLastError=true)]
        //public static extern bool CryptUnprotectData(ref DATA_BLOB dataIn, StringBuilder ppszDataDescr, IntPtr pEntropy, IntPtr pvReserved, IntPtr pPromptStruct, int dwFlags, ref DATA_BLOB pDataOut);

        [DllImportAttribute("crypt32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern bool CryptUnprotectData(ref DATA_BLOB dataIn, StringBuilder ppszDataDescr, IntPtr optionalEntropy,
            IntPtr pvReserved, IntPtr pPromptStruct, CryptProtectDataFlags dwFlags, ref DATA_BLOB pDataOut);

        #endregion

        #region kernel32.dll imports
        /// <summary>
        /// Search for function name in Plattform SDK documentation
        /// </summary>
        [DllImportAttribute("kernel32.dll", CharSet = CharSet.Auto)]
        public static extern IntPtr LocalFree(IntPtr hMem);

        /// <summary>
        /// Search for function name in Plattform SDK documentation
        /// </summary>
        [DllImport("Kernel32.dll")]
        public static extern int FormatMessage(FormatMessageFlags flags, IntPtr source, int messageId, int languageId,
            StringBuilder buffer, int size, IntPtr arguments);

        #endregion

        /// <summary>
        /// Encrypts data using the local machine CSP
        /// </summary>
        /// <param name="data">string data to be encrypted</param>
        /// <returns>Returns a Base64 encoded encrypted string.</returns>
        public static string EncryptData(string data)
        {
            return ProtectData(data, "",
                CryptProtectDataFlags.CRYPTPROTECT_UI_FORBIDDEN | CryptProtectDataFlags.CRYPTPROTECT_LOCAL_MACHINE);
        }
        /// <summary>
        /// Encrypts data using the current user's CSP
        /// </summary>
        /// <param name="data">string data to be encrypted</param>
        /// <returns>Returns a Base64 encoded encrypted string.</returns>
        public static string EncryptUserData(string data)
        {
            return ProtectData(data, "",
                CryptProtectDataFlags.CRYPTPROTECT_UI_FORBIDDEN);
        }
        /// <summary>
        /// Encrypts data using the local machine CSP
        /// </summary>
        /// <param name="data">string data to be encrypted</param>
        /// <param name="name">A descriptive name for the data</param>
        /// <returns>Returns a Base64 encoded encrypted string.</returns>
        public static string ProtectData(string data, string name)
        {
            return ProtectData(data, name,
                CryptProtectDataFlags.CRYPTPROTECT_UI_FORBIDDEN | CryptProtectDataFlags.CRYPTPROTECT_LOCAL_MACHINE);
        }
        /// <summary>
        /// Encrypts data using the local machine CSP
        /// </summary>
        /// <param name="data">byte array which should be encrypted</param>
        /// <param name="name">A descriptive name for the data</param>
        /// <returns>Returns a byte array holding the encrypted data.</returns>
        public static byte[] ProtectData(byte[] data, string name)
        {
            return ProtectData(data, name,
                CryptProtectDataFlags.CRYPTPROTECT_UI_FORBIDDEN | CryptProtectDataFlags.CRYPTPROTECT_LOCAL_MACHINE);
        }
        /// <summary>
        /// Encrypts data
        /// </summary>
        /// <param name="data">string data to be encrypted</param>
        /// <param name="name">A descriptive name for the data</param>
        /// <param name="flags">CSP flags to be used during encryption</param>
        /// <returns>Returns a Base64 encoded encrypted string.</returns>
        public static string ProtectData(string data, string name, CryptProtectDataFlags flags)
        {
            byte[] dataIn = Encoding.Unicode.GetBytes(data);
            byte[] dataOut = ProtectData(dataIn, name, flags);

            if (dataOut != null)
                return (Convert.ToBase64String(dataOut));
            else
                return null;
        }
        /// <summary>
        /// Encrypts data
        /// </summary>
        /// <param name="data">byte array which should be encrypted</param>
        /// <param name="name">A descriptive name for the data</param>
        /// <param name="flags">CSP flags to be used during encryption</param>
        /// <returns>Returns a byte array holding the encrypted data.</returns>
        private static byte[] ProtectData(byte[] data, string name, CryptProtectDataFlags dwFlags)
        {
            byte[] cipherText = null;

            // copy data into unmanaged memory
            DATA_BLOB din = new DATA_BLOB();
            din.cbData = data.Length;
            din.pbData = Marshal.AllocHGlobal(din.cbData);

            if (din.pbData.Equals(IntPtr.Zero))
                throw new OutOfMemoryException("Unable to allocate memory for buffer.");

            Marshal.Copy(data, 0, din.pbData, din.cbData);

            DATA_BLOB dout = new DATA_BLOB();

            try
            {
                bool cryptoRetval = CryptProtectData(ref din, name, IntPtr.Zero, IntPtr.Zero, IntPtr.Zero, dwFlags, ref dout);

                if (cryptoRetval) // ERROR_SUCCESS?
                {
                    int startIndex = 0;
                    cipherText = new byte[dout.cbData];
                    Marshal.Copy(dout.pbData, cipherText, startIndex, dout.cbData);
                    LocalFree(dout.pbData);
                }
                else
                {
                    int errCode = Marshal.GetLastWin32Error();
                    StringBuilder buffer = new StringBuilder(256);
                    FormatMessage(FormatMessageFlags.FORMAT_MESSAGE_FROM_SYSTEM, IntPtr.Zero, errCode, 0, buffer, buffer.Capacity, IntPtr.Zero);
                }
            }
            finally
            {
                // free the allocated memory in use
                if (!din.pbData.Equals(IntPtr.Zero))
                    Marshal.FreeHGlobal(din.pbData);
            }

            return cipherText;
        }
        /// <summary>
        /// Descrypts a Base64 encoded string using the local machine CSP
        /// </summary>
        /// <param name="data">Base64 encoded string which holds encrypted data</param>
        /// <returns>Returns the clear-text string representation of data.</returns>
        public static string DecryptData(string data)
        {
            byte[] dataIn = Convert.FromBase64String(data);
            byte[] dataOut = UnprotectData(dataIn, CryptProtectDataFlags.CRYPTPROTECT_UI_FORBIDDEN | CryptProtectDataFlags.CRYPTPROTECT_LOCAL_MACHINE);

            if (dataOut != null)
                return Encoding.Unicode.GetString(dataOut);
            else
                return null;
        }
        /// <summary>
        /// Descrypts a Base64 encoded string using the current user's CSP
        /// </summary>
        /// <param name="data">Base64 encoded string which holds encrypted data</param>
        /// <returns>Returns the clear-text string representation of data.</returns>
        public static string DecryptUserData(string data)
        {
            byte[] dataIn = Convert.FromBase64String(data);
            byte[] dataOut = UnprotectData(dataIn, CryptProtectDataFlags.CRYPTPROTECT_UI_FORBIDDEN);

            if (dataOut != null)
                return Encoding.Unicode.GetString(dataOut);
            else
                return null;
        }
        /// <summary>
        /// Descrypts a byte array holding encrypted data
        /// </summary>
        /// <param name="data">byte array holding the encrypted data</param>
        /// <returns>Returns the decrypted byte array</returns>
        public static byte[] UnprotectData(byte[] data)
        {
            return UnprotectData(data, CryptProtectDataFlags.CRYPTPROTECT_UI_FORBIDDEN);
        }
        /// <summary>
        /// Descrypts a Base64 encoded string
        /// </summary>
        /// <param name="data">Base64 encoded string which holds encrypted data</param>
        /// <returns>Returns the clear-text string representation of data.</returns>
        public static string UnprotectData(string data)
        {
            byte[] dataIn = Convert.FromBase64String(data);
            byte[] dataOut = UnprotectData(dataIn);

            if (dataOut != null)
                return Encoding.Unicode.GetString(dataOut);
            else
                return null;
        }
        /// <summary>
        /// Descrypts a byte array holding encrypted data
        /// </summary>
        /// <param name="data">byte array holding the encrypted data</param>
        /// <param name="dwFlags">CSP flags to be used during decryption</param>
        /// <returns>Returns the decrypted byte array</returns>
        internal static byte[] UnprotectData(byte[] data, CryptProtectDataFlags dwFlags)
        {
            byte[] clearText = null;

            // copy data into unmanaged memory
            DATA_BLOB din = new DATA_BLOB();
            din.cbData = data.Length;
            din.pbData = Marshal.AllocHGlobal(din.cbData);

            if (din.pbData.Equals(IntPtr.Zero))
                throw new OutOfMemoryException("Unable to allocate memory for buffer.");

            Marshal.Copy(data, 0, din.pbData, din.cbData);

            DATA_BLOB dout = new DATA_BLOB();

            try
            {
                bool cryptoRetval = CryptUnprotectData(ref din, null, IntPtr.Zero, IntPtr.Zero, IntPtr.Zero, dwFlags, ref dout);

                if (cryptoRetval) // ERROR_SUCCESS?
                {
                    clearText = new byte[dout.cbData];
                    Marshal.Copy(dout.pbData, clearText, 0, dout.cbData);
                    LocalFree(dout.pbData);
                }
                else
                {
                    int errCode = Marshal.GetLastWin32Error();
                    StringBuilder buffer = new StringBuilder(256);
                    FormatMessage(FormatMessageFlags.FORMAT_MESSAGE_FROM_SYSTEM, IntPtr.Zero, errCode, 0, buffer, buffer.Capacity, IntPtr.Zero);
                }
            }
            finally
            {
                // free the allocated memory in use
                if (!din.pbData.Equals(IntPtr.Zero))
                    Marshal.FreeHGlobal(din.pbData);
            }

            return clearText;
        }
        /// <summary>
        /// Initializes a crypto promt structure
        /// </summary>
        /// <param name="ps">Reference of structure to initialize</param>
        internal static void InitPromptstruct(ref CRYPTPROTECT_PROMPTSTRUCT ps)
        {
            ps.cbSize = Marshal.SizeOf(typeof(CRYPTPROTECT_PROMPTSTRUCT));
            ps.dwPromptFlags = 0;
            ps.hwndApp = IntPtr.Zero;
            ps.szPrompt = null;
        }
    }
}
