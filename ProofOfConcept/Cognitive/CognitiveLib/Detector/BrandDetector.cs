﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using CognitiveLib.WebSearch;
using FSBlog.GoogleSearch.GoogleClient;

namespace CognitiveLib.Detector
{
    public struct BrandInfo
    {
        public int BrandId;
        public string BrandMainName;
        public WeightedString BrandNameInfo;
    }

    public class BrandDetectionResult
    {
        public bool IsEmpty
        {
            get
            {
                return ModelNo == null && Kategorie == null;
            }
        }

        public string ModelNo
        {
            get
            {
                if (GeizhalsSearchResults != null)
                    foreach (GeizhalsSearchResult geizhals_search_result in GeizhalsSearchResults)
                        if (geizhals_search_result.Produkte != null)
                            if (geizhals_search_result.Produkte.Count > 0)
                            {
                                string produkt = geizhals_search_result.Produkte[0];
                                return produkt;
                            }
                return null;
            }
        }

        public string Kategorie
        {
            get
            {
                if (GeizhalsSearchResults != null)
                    foreach (GeizhalsSearchResult geizhals_search_result in GeizhalsSearchResults)
                        if (geizhals_search_result.Kategorien != null)
                            if (geizhals_search_result.Kategorien.Count > 0)
                            {
                                string[] split_kat = geizhals_search_result.Kategorien[0].Split(">".ToCharArray());
                                return split_kat.Last().Trim();
                            }
                return null;
            }
        }

        public override string ToString()
        {
            return ModelNo + " " + Kategorie;
        }


        public List<GeizhalsSearchResult> GeizhalsSearchResults;
    }

    public class BrandDetector
    {

        public static StringDetectionResult DetectBrandByGoogleSearchWithModelNumber(string model_no)
        {
            string search_text = GoogleSearch.GetSearchResultAsText(model_no);

            if (string.IsNullOrEmpty(search_text))
                return new StringDetectionResult();

            StringDetectionResult sdr = DetectBrand(search_text);
            sdr.ComparerString = model_no;
            return sdr;
        }


        public static StringDetectionResult DetectBrand(string text)
        {
            if (string.IsNullOrEmpty(text))
                return new StringDetectionResult();

            StringDetectionResults string_res = StringDetector.DetectFromList(text, Brands.Select(o=>o.BrandNameInfo), StringDetector.DetectionStrategy.AllToLower);
            StringDetectionResult? ndr = string_res.BestMatchByWeight;
            return ndr ?? new StringDetectionResult();


            //var query = from s in string_res
            //            group s by s.ComparerString
            //            into g
            //            select new { DetectedComparerStrings = g.Key, Count = g.Count(), Items = g };

            //List<StringDetectionResult> sdr_list = new List<StringDetectionResult>(1);
            //foreach (var g in query.OrderByDescending(o => o.Count))
            //{
            //    StringDetectionResult sdr = g.Items.FirstOrDefault();
            //    sdr_list.Add(sdr);
            //}
            //if (sdr_list.Count == 0)
            //    return new StringDetectionResult();
            //return sdr_list[0];
        }


        private static List<BrandInfo> Brands = new List<BrandInfo>
        {
            new BrandInfo(){BrandId = 1, BrandMainName = "Acer",BrandNameInfo = new WeightedString("Acer") },
            new BrandInfo(){BrandId = 2, BrandMainName = "AEG",BrandNameInfo = new WeightedString("AEG") },
            new BrandInfo(){BrandId = 3, BrandMainName = "Aiwa",BrandNameInfo = new WeightedString("Aiwa") },
            new BrandInfo(){BrandId = 4, BrandMainName = "Akai",BrandNameInfo = new WeightedString("Akai") },
            new BrandInfo(){BrandId = 5, BrandMainName = "Alphatronics",BrandNameInfo = new WeightedString("Alphatronics", StringWeightCategory.Perfect) },
            new BrandInfo(){BrandId = 6, BrandMainName = "AOC",BrandNameInfo = new WeightedString("AOC") },
            new BrandInfo(){BrandId = 7, BrandMainName = "APC",BrandNameInfo = new WeightedString("APC") },
            new BrandInfo(){BrandId = 8, BrandMainName = "Apple",BrandNameInfo = new WeightedString("Apple") },
            new BrandInfo(){BrandId = 9, BrandMainName = "ARCAM",BrandNameInfo = new WeightedString("ARCAM") },
            new BrandInfo(){BrandId = 10, BrandMainName = "Archos",BrandNameInfo = new WeightedString("Archos", StringWeightCategory.Perfect) },
            new BrandInfo(){BrandId = 11, BrandMainName = "Asus",BrandNameInfo = new WeightedString("Asus") },
            new BrandInfo(){BrandId = 12, BrandMainName = "Benq",BrandNameInfo = new WeightedString("Benq") },
            new BrandInfo(){BrandId = 13, BrandMainName = "Blaupunkt",BrandNameInfo = new WeightedString("Blaupunkt", StringWeightCategory.Perfect) },
            new BrandInfo(){BrandId = 14, BrandMainName = "BMW",BrandNameInfo = new WeightedString("BMW") },
            new BrandInfo(){BrandId = 15, BrandMainName = "Bosch",BrandNameInfo = new WeightedString("Bosch") },
            new BrandInfo(){BrandId = 16, BrandMainName = "Boston",BrandNameInfo = new WeightedString("Boston") },
            new BrandInfo(){BrandId = 17, BrandMainName = "Braun",BrandNameInfo = new WeightedString("Braun") },
            new BrandInfo(){BrandId = 18, BrandMainName = "Brionvega",BrandNameInfo = new WeightedString("Brionvega", StringWeightCategory.Perfect) },
            new BrandInfo(){BrandId = 19, BrandMainName = "Brother",BrandNameInfo = new WeightedString("Brother") },
            new BrandInfo(){BrandId = 20, BrandMainName = "Buffalo",BrandNameInfo = new WeightedString("Buffalo") },
            new BrandInfo(){BrandId = 21, BrandMainName = "Canon",BrandNameInfo = new WeightedString("Canon") },
            new BrandInfo(){BrandId = 22, BrandMainName = "Canton",BrandNameInfo = new WeightedString("Canton") },
            new BrandInfo(){BrandId = 23, BrandMainName = "cbe",BrandNameInfo = new WeightedString("cbe") },
            new BrandInfo(){BrandId = 24, BrandMainName = "Changhong",BrandNameInfo = new WeightedString("Changhong", StringWeightCategory.Perfect) },
            new BrandInfo(){BrandId = 25, BrandMainName = "CMX",BrandNameInfo = new WeightedString("CMX") },
            new BrandInfo(){BrandId = 26, BrandMainName = "Compaq",BrandNameInfo = new WeightedString("Compaq", StringWeightCategory.Perfect) },
            new BrandInfo(){BrandId = 27, BrandMainName = "Daewoo",BrandNameInfo = new WeightedString("Daewoo", StringWeightCategory.Perfect) },
            new BrandInfo(){BrandId = 28, BrandMainName = "Dell",BrandNameInfo = new WeightedString("Dell") },
            new BrandInfo(){BrandId = 29, BrandMainName = "Delonghi",BrandNameInfo = new WeightedString("Delonghi", StringWeightCategory.Perfect) },
            new BrandInfo(){BrandId = 30, BrandMainName = "Denon",BrandNameInfo = new WeightedString("Denon") },
            new BrandInfo(){BrandId = 31, BrandMainName = "Denver",BrandNameInfo = new WeightedString("Denver") },
            new BrandInfo(){BrandId = 32, BrandMainName = "Dictaphone",BrandNameInfo = new WeightedString("Dictaphone", StringWeightCategory.Perfect) },
            new BrandInfo(){BrandId = 33, BrandMainName = "Dimotion",BrandNameInfo = new WeightedString("Dimotion") },
            new BrandInfo(){BrandId = 34, BrandMainName = "Diverse",BrandNameInfo = new WeightedString("Diverse") },
            new BrandInfo(){BrandId = 35, BrandMainName = "DK Digital",BrandNameInfo = new WeightedString("DK Digital", StringWeightCategory.Perfect) },
            new BrandInfo(){BrandId = 36, BrandMainName = "Duai",BrandNameInfo = new WeightedString("Duai") },
            new BrandInfo(){BrandId = 37, BrandMainName = "Dual",BrandNameInfo = new WeightedString("Dual") },
            new BrandInfo(){BrandId = 38, BrandMainName = "Dyson",BrandNameInfo = new WeightedString("Dyson") },
            new BrandInfo(){BrandId = 39, BrandMainName = "ELAC",BrandNameInfo = new WeightedString("ELAC") },
            new BrandInfo(){BrandId = 40, BrandMainName = "Elin",BrandNameInfo = new WeightedString("Elin") },
            new BrandInfo(){BrandId = 41, BrandMainName = "Elro",BrandNameInfo = new WeightedString("Elro") },
            new BrandInfo(){BrandId = 42, BrandMainName = "Elta",BrandNameInfo = new WeightedString("Elta") },
            new BrandInfo(){BrandId = 43, BrandMainName = "emachine",BrandNameInfo = new WeightedString("emachine") },
            new BrandInfo(){BrandId = 44, BrandMainName = "Epad",BrandNameInfo = new WeightedString("Epad") },
            new BrandInfo(){BrandId = 45, BrandMainName = "Epson",BrandNameInfo = new WeightedString("Epson") },
            new BrandInfo(){BrandId = 46, BrandMainName = "estro",BrandNameInfo = new WeightedString("estro") },
            new BrandInfo(){BrandId = 47, BrandMainName = "Finlux",BrandNameInfo = new WeightedString("Finlux") },
            new BrandInfo(){BrandId = 48, BrandMainName = "Fisher",BrandNameInfo = new WeightedString("Fisher") },
            new BrandInfo(){BrandId = 49, BrandMainName = "FraRon",BrandNameInfo = new WeightedString("FraRon") },
            new BrandInfo(){BrandId = 50, BrandMainName = "Fujifilm",BrandNameInfo = new WeightedString("Fujifilm") },
            new BrandInfo(){BrandId = 51, BrandMainName = "Fujitsu",BrandNameInfo = new WeightedString("Fujitsu") },
            new BrandInfo(){BrandId = 52, BrandMainName = "Funai",BrandNameInfo = new WeightedString("Funai") },
            new BrandInfo(){BrandId = 53, BrandMainName = "GBC",BrandNameInfo = new WeightedString("GBC") },
            new BrandInfo(){BrandId = 54, BrandMainName = "Gelhard",BrandNameInfo = new WeightedString("Gelhard") },
            new BrandInfo(){BrandId = 55, BrandMainName = "Gericom",BrandNameInfo = new WeightedString("Gericom") },
            new BrandInfo(){BrandId = 56, BrandMainName = "GM Germany",BrandNameInfo = new WeightedString("GM Germany") },
            new BrandInfo(){BrandId = 57, BrandMainName = "GooBang Doo",BrandNameInfo = new WeightedString("GooBang Doo") },
            new BrandInfo(){BrandId = 58, BrandMainName = "GPX",BrandNameInfo = new WeightedString("GPX") },
            new BrandInfo(){BrandId = 59, BrandMainName = "Grundig",BrandNameInfo = new WeightedString("Grundig") },
            new BrandInfo(){BrandId = 60, BrandMainName = "Haier",BrandNameInfo = new WeightedString("Haier") },
            new BrandInfo(){BrandId = 61, BrandMainName = "Hama",BrandNameInfo = new WeightedString("Hama") },
            new BrandInfo(){BrandId = 62, BrandMainName = "Harman/Kardon",BrandNameInfo = new WeightedString("Harman/Kardon") },
            new BrandInfo(){BrandId = 63, BrandMainName = "Hewlett Packard",BrandNameInfo = new WeightedString("Hewlett Packard") },
            new BrandInfo(){BrandId = 64, BrandMainName = "Hirschmann",BrandNameInfo = new WeightedString("Hirschmann") },
            new BrandInfo(){BrandId = 65, BrandMainName = "Hisense",BrandNameInfo = new WeightedString("Hisense") },
            new BrandInfo(){BrandId = 66, BrandMainName = "Hitachi",BrandNameInfo = new WeightedString("Hitachi") },
            new BrandInfo(){BrandId = 67, BrandMainName = "HP",BrandNameInfo = new WeightedString("HP") },
            new BrandInfo(){BrandId = 68, BrandMainName = "HSM",BrandNameInfo = new WeightedString("HSM") },
            new BrandInfo(){BrandId = 69, BrandMainName = "Ingelen",BrandNameInfo = new WeightedString("Ingelen") },
            new BrandInfo(){BrandId = 70, BrandMainName = "iRobot",BrandNameInfo = new WeightedString("iRobot") },
            new BrandInfo(){BrandId = 71, BrandMainName = "ITT",BrandNameInfo = new WeightedString("ITT") },
            new BrandInfo(){BrandId = 72, BrandMainName = "Jamo",BrandNameInfo = new WeightedString("Jamo") },
            new BrandInfo(){BrandId = 73, BrandMainName = "JBL",BrandNameInfo = new WeightedString("JBL") },
            new BrandInfo(){BrandId = 74, BrandMainName = "JVC",BrandNameInfo = new WeightedString("JVC") },
            new BrandInfo(){BrandId = 75, BrandMainName = "Kapsch",BrandNameInfo = new WeightedString("Kapsch") },
            new BrandInfo(){BrandId = 76, BrandMainName = "Kathrein",BrandNameInfo = new WeightedString("Kathrein") },
            new BrandInfo(){BrandId = 77, BrandMainName = "Kenwood",BrandNameInfo = new WeightedString("Kenwood") },
            new BrandInfo(){BrandId = 78, BrandMainName = "Klipsch",BrandNameInfo = new WeightedString("Klipsch") },
            new BrandInfo(){BrandId = 79, BrandMainName = "Laurastar",BrandNameInfo = new WeightedString("Laurastar") },
            new BrandInfo(){BrandId = 80, BrandMainName = "LD-Systems",BrandNameInfo = new WeightedString("LD-Systems") },
            new BrandInfo(){BrandId = 81, BrandMainName = "Lenni",BrandNameInfo = new WeightedString("Lenni") },
            new BrandInfo(){BrandId = 82, BrandMainName = "Lenovo",BrandNameInfo = new WeightedString("Lenovo") },
            new BrandInfo(){BrandId = 83, BrandMainName = "Lenuss",BrandNameInfo = new WeightedString("Lenuss") },
            new BrandInfo(){BrandId = 84, BrandMainName = "LG",BrandNameInfo = new WeightedString("LG") },
            new BrandInfo(){BrandId = 85, BrandMainName = "LG",BrandNameInfo = new WeightedString("LG Electronics") },
            new BrandInfo(){BrandId = 86, BrandMainName = "Liberty",BrandNameInfo = new WeightedString("Liberty") },
            new BrandInfo(){BrandId = 87, BrandMainName = "Loewe",BrandNameInfo = new WeightedString("Loewe") },
            new BrandInfo(){BrandId = 88, BrandMainName = "Magnat",BrandNameInfo = new WeightedString("Magnat") },
            new BrandInfo(){BrandId = 89, BrandMainName = "Maxdata",BrandNameInfo = new WeightedString("Maxdata") },
            new BrandInfo(){BrandId = 90, BrandMainName = "Mc Crypt",BrandNameInfo = new WeightedString("Mc Crypt") },
            new BrandInfo(){BrandId = 91, BrandMainName = "Meco",BrandNameInfo = new WeightedString("Meco") },
            new BrandInfo(){BrandId = 92, BrandMainName = "Medion",BrandNameInfo = new WeightedString("Medion") },
            new BrandInfo(){BrandId = 93, BrandMainName = "Metz",BrandNameInfo = new WeightedString("Metz") },
            new BrandInfo(){BrandId = 94, BrandMainName = "Microsoft",BrandNameInfo = new WeightedString("Microsoft") },
            new BrandInfo(){BrandId = 95, BrandMainName = "Microstar",BrandNameInfo = new WeightedString("Microstar") },
            new BrandInfo(){BrandId = 96, BrandMainName = "MSI",BrandNameInfo = new WeightedString("MSI") },
            new BrandInfo(){BrandId = 97, BrandMainName = "Nabo",BrandNameInfo = new WeightedString("Nabo") },
            new BrandInfo(){BrandId = 98, BrandMainName = "NAD",BrandNameInfo = new WeightedString("NAD") },
            new BrandInfo(){BrandId = 99, BrandMainName = "Neff",BrandNameInfo = new WeightedString("Neff") },
            new BrandInfo(){BrandId = 100, BrandMainName = "Netgear",BrandNameInfo = new WeightedString("Netgear") },
            new BrandInfo(){BrandId = 101, BrandMainName = "Next",BrandNameInfo = new WeightedString("Next") },
            new BrandInfo(){BrandId = 102, BrandMainName = "Nissan",BrandNameInfo = new WeightedString("Nissan") },
            new BrandInfo(){BrandId = 103, BrandMainName = "NUK",BrandNameInfo = new WeightedString("NUK") },
            new BrandInfo(){BrandId = 104, BrandMainName = "OK",BrandNameInfo = new WeightedString("OK") },
            new BrandInfo(){BrandId = 105, BrandMainName = "Onkyo",BrandNameInfo = new WeightedString("Onkyo") },
            new BrandInfo(){BrandId = 106, BrandMainName = "Opera",BrandNameInfo = new WeightedString("Opera") },
            new BrandInfo(){BrandId = 107, BrandMainName = "Opticum",BrandNameInfo = new WeightedString("Opticum") },
            new BrandInfo(){BrandId = 108, BrandMainName = "Orion",BrandNameInfo = new WeightedString("Orion") },
            new BrandInfo(){BrandId = 109, BrandMainName = "Palazetti",BrandNameInfo = new WeightedString("Palazetti") },
            new BrandInfo(){BrandId = 110, BrandMainName = "Panasonic",BrandNameInfo = new WeightedString("Panasonic") },
            new BrandInfo(){BrandId = 111, BrandMainName = "PEAQ-SHE",BrandNameInfo = new WeightedString("PEAQ-SHE") },
            new BrandInfo(){BrandId = 112, BrandMainName = "Peugeot",BrandNameInfo = new WeightedString("Peugeot") },
            new BrandInfo(){BrandId = 113, BrandMainName = "Philips",BrandNameInfo = new WeightedString("Philips", StringWeightCategory.Perfect) },
            new BrandInfo(){BrandId = 114, BrandMainName = "Phocus",BrandNameInfo = new WeightedString("Phocus") },
            new BrandInfo(){BrandId = 115, BrandMainName = "Phönix",BrandNameInfo = new WeightedString("Phönix") },
            new BrandInfo(){BrandId = 116, BrandMainName = "Pioneer",BrandNameInfo = new WeightedString("Pioneer") },
            new BrandInfo(){BrandId = 117, BrandMainName = "Platine",BrandNameInfo = new WeightedString("Platine") },
            new BrandInfo(){BrandId = 118, BrandMainName = "Privileg",BrandNameInfo = new WeightedString("Privileg") },
            new BrandInfo(){BrandId = 119, BrandMainName = "RCS",BrandNameInfo = new WeightedString("RCS") },
            new BrandInfo(){BrandId = 120, BrandMainName = "Rex",BrandNameInfo = new WeightedString("Rex") },
            new BrandInfo(){BrandId = 121, BrandMainName = "Rittal",BrandNameInfo = new WeightedString("Rittal") },
            new BrandInfo(){BrandId = 122, BrandMainName = "Roland",BrandNameInfo = new WeightedString("Roland") },
            new BrandInfo(){BrandId = 123, BrandMainName = "SAAB",BrandNameInfo = new WeightedString("SAAB") },
            new BrandInfo(){BrandId = 124, BrandMainName = "Samsung",BrandNameInfo = new WeightedString("Samsung") },
            new BrandInfo(){BrandId = 125, BrandMainName = "SamsungIGSPN",BrandNameInfo = new WeightedString("SamsungIGSPN") },
            new BrandInfo(){BrandId = 126, BrandMainName = "Schaub Lorenz",BrandNameInfo = new WeightedString("Schaub Lorenz") },
            new BrandInfo(){BrandId = 127, BrandMainName = "Schneider",BrandNameInfo = new WeightedString("Schneider") },
            new BrandInfo(){BrandId = 128, BrandMainName = "Senheiser",BrandNameInfo = new WeightedString("Senheiser") },
            new BrandInfo(){BrandId = 129, BrandMainName = "Shanling",BrandNameInfo = new WeightedString("Shanling") },
            new BrandInfo(){BrandId = 130, BrandMainName = "Sharp",BrandNameInfo = new WeightedString("Sharp") },
            new BrandInfo(){BrandId = 131, BrandMainName = "Sherwood",BrandNameInfo = new WeightedString("Sherwood") },
            new BrandInfo(){BrandId = 132, BrandMainName = "Siemens",BrandNameInfo = new WeightedString("Siemens") },
            new BrandInfo(){BrandId = 133, BrandMainName = "Sigma",BrandNameInfo = new WeightedString("Sigma") },
            new BrandInfo(){BrandId = 134, BrandMainName = "Silva",BrandNameInfo = new WeightedString("Silva") },
            new BrandInfo(){BrandId = 135, BrandMainName = "Singer",BrandNameInfo = new WeightedString("Singer") },
            new BrandInfo(){BrandId = 136, BrandMainName = "Somikon",BrandNameInfo = new WeightedString("Somikon") },
            new BrandInfo(){BrandId = 137, BrandMainName = "Sonstige",BrandNameInfo = new WeightedString("Sonstige") },
            new BrandInfo(){BrandId = 138, BrandMainName = "Sony",BrandNameInfo = new WeightedString("Sony") },
            new BrandInfo(){BrandId = 139, BrandMainName = "Sphinx",BrandNameInfo = new WeightedString("Sphinx") },
            new BrandInfo(){BrandId = 140, BrandMainName = "Sungoo",BrandNameInfo = new WeightedString("Sungoo") },
            new BrandInfo(){BrandId = 141, BrandMainName = "Targa",BrandNameInfo = new WeightedString("Targa") },
            new BrandInfo(){BrandId = 142, BrandMainName = "Teac",BrandNameInfo = new WeightedString("Teac") },
            new BrandInfo(){BrandId = 143, BrandMainName = "Technics",BrandNameInfo = new WeightedString("Technics") },
            new BrandInfo(){BrandId = 144, BrandMainName = "Technisat",BrandNameInfo = new WeightedString("Technisat") },
            new BrandInfo(){BrandId = 145, BrandMainName = "Techsolo",BrandNameInfo = new WeightedString("Techsolo") },
            new BrandInfo(){BrandId = 146, BrandMainName = "Teco",BrandNameInfo = new WeightedString("Teco") },
            new BrandInfo(){BrandId = 147, BrandMainName = "Tefal",BrandNameInfo = new WeightedString("Tefal") },
            new BrandInfo(){BrandId = 148, BrandMainName = "Teka",BrandNameInfo = new WeightedString("Teka") },
            new BrandInfo(){BrandId = 149, BrandMainName = "Telefunken",BrandNameInfo = new WeightedString("Telefunken") },
            new BrandInfo(){BrandId = 150, BrandMainName = "Teufel",BrandNameInfo = new WeightedString("Teufel") },
            new BrandInfo(){BrandId = 151, BrandMainName = "Tevion",BrandNameInfo = new WeightedString("Tevion") },
            new BrandInfo(){BrandId = 152, BrandMainName = "Thomson",BrandNameInfo = new WeightedString("Thomson") },
            new BrandInfo(){BrandId = 153, BrandMainName = "TIS",BrandNameInfo = new WeightedString("TIS") },
            new BrandInfo(){BrandId = 154, BrandMainName = "TomTom",BrandNameInfo = new WeightedString("TomTom") },
            new BrandInfo(){BrandId = 155, BrandMainName = "Topfield",BrandNameInfo = new WeightedString("Topfield") },
            new BrandInfo(){BrandId = 156, BrandMainName = "Toshiba",BrandNameInfo = new WeightedString("Toshiba") },
            new BrandInfo(){BrandId = 157, BrandMainName = "TP-Link",BrandNameInfo = new WeightedString("TP-Link") },
            new BrandInfo(){BrandId = 158, BrandMainName = "Triax",BrandNameInfo = new WeightedString("Triax") },
            new BrandInfo(){BrandId = 159, BrandMainName = "Trivision",BrandNameInfo = new WeightedString("Trivision") },
            new BrandInfo(){BrandId = 160, BrandMainName = "TuxEdo",BrandNameInfo = new WeightedString("TuxEdo") },
            new BrandInfo(){BrandId = 161, BrandMainName = "Universum",BrandNameInfo = new WeightedString("Universum") },
            new BrandInfo(){BrandId = 162, BrandMainName = "VU Solo",BrandNameInfo = new WeightedString("VU Solo") },
            new BrandInfo(){BrandId = 163, BrandMainName = "Wharfedale",BrandNameInfo = new WeightedString("Wharfedale") },
            new BrandInfo(){BrandId = 164, BrandMainName = "Wisi",BrandNameInfo = new WeightedString("Wisi") },
            new BrandInfo(){BrandId = 165, BrandMainName = "Yamaha",BrandNameInfo = new WeightedString("Yamaha") },
            new BrandInfo(){BrandId = 166, BrandMainName = "Zeus",BrandNameInfo = new WeightedString("Zeus") },
        };
    }
}
