﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace Logicx.DiagramLib.DiagramObjects.Objects
{
    public class ObjectUiManupulationMeta
    {
        #region internal proeprties for touch and mouse handling
        /// <summary>
        /// Gets a flag if a input (stylus) device went down on the associated object
        /// </summary>
        internal bool IsInputDeviceDown
        {
            get;
            set;
        }
        /// <summary>
        /// The Id of the stylus device which is currently manipulating the object or NULL
        /// if the object is not beeing manipulated
        /// </summary>
        internal int? RegisteredStylusId
        {
            get;
            set;
        }
        /// <summary>
        /// The absolut coordinates of the stylus device position in relation to the parent element canvas in pixel.
        /// This will be set once if the stylus device went down
        /// </summary>
        internal Point StartInputDevicePosition
        {
            get;
            set;
        }
        /// <summary>
        /// The absolut coordinates of the stylus device position in relation to the parent element canvas in pixel.
        /// This will always be set as soon as the object forces a recalculation step and processed stylus coordinates
        /// </summary>
        internal Point LastInputDevicePosition
        {
            get;
            set;
        }

        /// <summary>
        /// A double offset of the stylus device to the object's X origin
        /// </summary>
        internal double InputDeviceOffsetX
        {
            get;
            set;
        }

        /// <summary>
        /// A double offset of the stylus device to the object's Y origin
        /// </summary>
        internal double InputDeviceOffsetY
        {
            get;
            set;
        }

        internal bool SelectedWithDownAction
        {
            get;
            set;

        }

        /// <summary>
        /// Reset the Ui manipulation variables
        /// </summary>
        internal void ResetUiManipulationValues()
        {
            IsInputDeviceDown = false;
            RegisteredStylusId = null;
            LastInputDevicePosition = new Point(0, 0);
            StartInputDevicePosition = new Point(0, 0);
            InputDeviceOffsetX = double.MaxValue;
            InputDeviceOffsetY = double.MaxValue;
            SelectedWithDownAction = false;
        }
        #endregion
    }
}
