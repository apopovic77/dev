﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Google.Apis.Auth.OAuth2;
using Google.Cloud.Storage.V1;
using Google.Cloud.Vision.V1;

namespace CognitiveLib.ApiService
{
    public class GoogleService
    {
        public struct GoogleDetectLogoResponses
        {
            public GoogleDetectLogoResponse[] GoogleDetectLogoResponse;
        }

        public struct GoogleDetectLogoResponse
        {
            public string Mid;
            public string Description;
            public float Score;
            public Vertex v1;
            public Vertex v2;
            public Vertex v3;
            public Vertex v4;
            public Vertex[] Vertices;
        }


        public static object AuthExplicit(string projectId, string jsonPath)
        {
            // Explicitly use service account credentials by specifying the private key
            // file.
            GoogleCredential credential = null;
            using (var jsonStream = new FileStream(jsonPath, FileMode.Open,
                FileAccess.Read, FileShare.Read))
            {
                credential = GoogleCredential.FromStream(jsonStream);
            }
            var storage = StorageClient.Create(credential);
            // Make an authenticated API request.
            var buckets = storage.ListBuckets(projectId);
            foreach (var bucket in buckets)
            {
                Console.WriteLine(bucket.Name);
            }
            return null;
        }


        public static object AuthImplicit(string projectId)
        {
            // If you don't specify credentials when constructing the client, the
            // client library will look for credentials in the environment.
            var storage = StorageClient.Create();
            // Make an authenticated API request.
            var buckets = storage.ListBuckets(projectId);
            foreach (var bucket in buckets)
            {
                Console.WriteLine(bucket.Name);
            }
            return null;
        }


        public static IReadOnlyList<EntityAnnotation> GetFileAnotation(string path_to_file)
        { 
            // Load an image from a local file.
            var image = Image.FromFile(path_to_file);
            var client = ImageAnnotatorClient.Create();
            var response = client.DetectLogos(image);
            return response;
        }


        public static GoogleDetectLogoResponses GetFileAnotationStrongTyped(string path_to_file)
        { 
            // Load an image from a local file.
            var response = GetFileAnotation(path_to_file);
            
            List<GoogleDetectLogoResponse> response_list = new List<GoogleDetectLogoResponse>();
            foreach (var annotation in response)
            {
                GoogleDetectLogoResponse dlr = new GoogleDetectLogoResponse();
                dlr.Mid = annotation.Mid;
                dlr.Description = annotation.Description;
                dlr.Score = annotation.Score;
                dlr.v1 = annotation.BoundingPoly.Vertices[0];
                dlr.v2 = annotation.BoundingPoly.Vertices[1];
                dlr.v3 = annotation.BoundingPoly.Vertices[2];
                dlr.v4 = annotation.BoundingPoly.Vertices[3];
                dlr.Vertices = annotation.BoundingPoly.Vertices.ToArray();

                response_list.Add(dlr);
            }

            GoogleDetectLogoResponses responses = new GoogleDetectLogoResponses();
            responses.GoogleDetectLogoResponse = response_list.ToArray();
            return responses;
        }
    }
}
