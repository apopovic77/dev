﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Data;
using Logicx.WpfUtility.CustomControls.DateTimePicker;

namespace Logicx.WpfUtility.Converters
{
    public class DateTimePickerVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if(value is DateTimeVisibility && parameter is string)
            {
                DateTimeVisibility visibility = (DateTimeVisibility)value;
                string property_name = (string) parameter;

                if (visibility == DateTimeVisibility.TimeOnly)
                {
                    if (property_name == "Date")
                        return Visibility.Collapsed;
                }
                else if(visibility == DateTimeVisibility.DateOnly)
                {
                    if (property_name == "Time")
                        return Visibility.Collapsed;
                }

                return Visibility.Visible;
            }

            return Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
