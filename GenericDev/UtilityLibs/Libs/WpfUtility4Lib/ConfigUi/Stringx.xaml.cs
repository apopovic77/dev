﻿using System.Windows;
using System.Windows.Controls;

namespace Logicx.WpfUtility4.ConfigUi
{
    /// <summary>
    /// Interaction logic for Stringx.xaml
    /// </summary>
    public partial class Stringx : UserControl
    {
        public Stringx()
        {
            InitializeComponent();
        }

        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            if (e.Property.Name == "Bezeichnung")
            {
                Bezeichnung = e.NewValue.ToString().ToUpper();
            }

            base.OnPropertyChanged(e);
        }


        public string Bezeichnung
        {
            get { return (string)GetValue(BezeichnungProperty); }
            set { SetValue(BezeichnungProperty, value); }
        }
        public static readonly DependencyProperty BezeichnungProperty = DependencyProperty.Register("Bezeichnung", typeof(string), typeof(Stringx), new UIPropertyMetadata(""));

    }
}
