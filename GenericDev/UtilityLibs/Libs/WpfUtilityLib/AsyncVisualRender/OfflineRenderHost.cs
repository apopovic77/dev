﻿using System;
using System.IO;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Logicx.WpfUtility.VisualHosting;
using Logicx.WpfUtility.WpfHelpers;

namespace Logicx.WpfUtility.AsyncVisualRender
{
    public class OfflineRenderHost : ThreadedFrameworkElement
    {
        public OfflineRenderHost(int pool_index, string thread_name) : base(true, false, thread_name)
        {
            PoolIndex = pool_index;
        }

        protected override FrameworkElement CreateThreadedChildContent()
        {
            CompositionTarget.Rendering += new EventHandler(CompositionTarget_Rendering);

            _scale_transform = new ScaleTransform(1, 1);
            _transl_transform = new TranslateTransform(0,0);
            _transform_group = new TransformGroup();
            _transform_group.Children.Add(_scale_transform);
            _transform_group.Children.Add(_transl_transform);

            _canvas = new Canvas();

            InitResetEvent.Set();
            
            return _canvas;
        }

        public void SetRenderVisual(WpfRenderTask rti, AutoResetEvent reset_event)
        {
            _caller_reset_event = reset_event;
            _rti = rti;
            _visual = _rti.GetVisual();
            _visual.RenderTransform = _transform_group;
            RenderHelper.SetScaling(_visual, rti);
            _visual.Loaded += framework_element_Loaded;
            _canvas.Children.Add(_visual);
        }

        void framework_element_Loaded(object sender, RoutedEventArgs e)
        {
            _visual.Loaded -= framework_element_Loaded;
            RenderHelper.UpdateRti(_visual, _rti);
            RenderHelper.CaptureImage(_visual, _rti);

            _canvas.Children.Clear();
            _visual.RenderTransform = null;
            if (_caller_reset_event != null)
            {
                _caller_reset_event.Set();
                _caller_reset_event = null;
            }
        }

        void CompositionTarget_Rendering(object sender, EventArgs e)
        {
            if(_canvas.Children.Count == 0)
            {
                _my_reset_event.WaitOne(5000);
                _my_reset_event.Reset();
            }
        }

        public Canvas Canvas
        {
            get
            {
                return _canvas;
            }
        }

        public void UnWait()
        {
            _my_reset_event.Set();
        }

        #region Attributes
        public readonly int PoolIndex;

        private FrameworkElement _visual;
        private Canvas _canvas;
        private TransformGroup _transform_group;
        private ScaleTransform _scale_transform;
        private TranslateTransform _transl_transform;

        private AutoResetEvent _caller_reset_event;
        private AutoResetEvent _my_reset_event = new AutoResetEvent(false);

        private WpfRenderTask _rti;
        #endregion
    }
}
