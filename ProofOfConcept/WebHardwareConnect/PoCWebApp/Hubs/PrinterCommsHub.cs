﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.SignalR;
using PoCShared;

namespace PoCWebApp.Hubs
{
    public class PrinterCommsHub : Hub
    {
        public Task JoinGroup(string groupName)
        {
            return Groups.Add(Context.ConnectionId, groupName);
        }

        public Task LeaveGroup(string groupName)
        {
            return Groups.Remove(Context.ConnectionId, groupName);
        }


        public void SendPrintCommand(PrinterItem printer, string urlToDocument)
        {
            Clients.Group(printer.PrintServer).printDocument(printer, urlToDocument);
        }

        public void SetPrinterStateFromClient(PrinterStatusItem printerStatus)
        {
            //Clients.Group(printer.PrintServer).requestState(printer);
        }
    }
}