﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Media;
using System.Windows.Shapes;

namespace ShortestPathAgentsApp
{

    public class RectangleObstacle : Obstacle
    {
        public RectangleObstacle(double width, double height, double x, double y, double rotation)
        {


            Rectangle rect = new Rectangle();
            rect.Fill = Brushes.White;
            rect.Height = Math.Abs(height);
            rect.Width = Math.Abs(width);

            Shape = rect;
            Shape.Tag = this;
            Rotation = rotation;
            X = x;
            Y = y;
        }

    }
}
