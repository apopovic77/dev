using System;
using System.Collections.Generic;
using System.Text;

namespace Logicx.Utilities
{
    public interface IDebugWriter
    {
        void WriteInfo(string msg);
    }

    public interface IInfoWriter
    {
        void WriteInfo(string title, string info);
    }

    public interface IErrorWriter
    {
        void WriteInfo(int errorid, string title, string info);
    }
}
