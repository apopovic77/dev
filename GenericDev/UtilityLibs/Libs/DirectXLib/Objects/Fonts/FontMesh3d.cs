﻿using System;
using System.Drawing;
using System.Net.Mime;
using Microsoft.DirectX;
using Microsoft.DirectX.Direct3D;
using Logicx.DirectX.Engine;
using Font = Microsoft.DirectX.Direct3D.Font;

namespace Logicx.DirectX.Objects.Fonts
{
    /// <summary>
    /// font is transformed into a 3d mesh, extrusion is possible
    /// font is drawn as a real 3d model
    /// </summary>
    public class FontMesh3d : Object3d
    {
        #region Contruction

        public FontMesh3d()
        {
        }

        #endregion

        public override void Init()
        {
        }

        public string Text
        {
            set
            {
                _text = value;
                if(_font_mesh == null)
                {
                    System.Drawing.Font f = new System.Drawing.Font("Arial",10);
                    _font_mesh = Mesh.TextFromFont(Engine3d.Device3d, f, _text, 1, 1);
                    f.Dispose();
                }
            }
        }
    

        public override void Dispose()
        {
            if(_font_mesh != null)
            {
                _font_mesh.Dispose();
            }
        }

        public override void Render(float elapsedTime)
        {
            //dont do anything if there is no text do render
            if (string.IsNullOrEmpty(_text))
                return;

            Engine3d.Device3d.Transform.World = Matrix.Identity;
            _font_mesh.DrawSubset(0);
        }

        #region Properties

        #endregion

        #region Attribs
        private Mesh _font_mesh;
        public Color FontColor = Color.Black;
        public string _text;
        #endregion
    }
}