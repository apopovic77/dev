﻿using System.Windows;

namespace GenericLib
{
    public class WpfHelper
    {

        public static T FindAncestor<T>(FrameworkElement framework_element) where T : DependencyObject
        {
            if (framework_element.Parent == null)
                return null;
            if (!(framework_element.Parent is FrameworkElement))
                return null;
            if (framework_element.Parent is T)
                return (T)framework_element.Parent;
            return FindAncestor<T>((FrameworkElement)framework_element.Parent);
        }
    }
}
