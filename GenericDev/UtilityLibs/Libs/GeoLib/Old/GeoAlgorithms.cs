using System;
using System.Collections;
using MathLib;

namespace GeoLib
{
	/// <summary>
	/// 
	/// </summary>
	public class GeoAlgorithms
	{

		public static Vector2d[] GetCutpoints(Edge[] edges,Edge e_tocut){
			ArrayList cutpoints = new ArrayList();
			for(int j=0;j<edges.Length;j++)
			{
				Edge next_edge = edges[j];
				Vector2d cutpoint;
				if(LinearAlgebra.GetCutPointInSegment(next_edge,e_tocut,out cutpoint))
				{
					cutpoints.Add(cutpoint);
				}
			}
			if(cutpoints.Count>0)
			{
				object[] o_arr = cutpoints.ToArray();
				Vector2d[] v_arr = new Vector2d[o_arr.Length];
				for(int i=0;i<v_arr.Length;i++)
					v_arr[i] = (Vector2d)o_arr[i];
				return v_arr;
			}
			else
			{
				return null;
			}
		}

		/////////////////////////////////////////////////////////////////////////////////
		/////////////////////////////////////////////////////////////////////////////////
		/////////////////////////////////////////////////////////////////////////////////
		/////////////////////////////////////////////////////////////////////////////////
		//                   SCANLINE ALG IMPL
		//                   The following function only concern the impl of Scanline
		//                   Dieser ist leider nicht fertig da er buggy ist
		/////////////////////////////////////////////////////////////////////////////////
		/////////////////////////////////////////////////////////////////////////////////
		/////////////////////////////////////////////////////////////////////////////////
		/////////////////////////////////////////////////////////////////////////////////
		private class EdgesSortXAsc : IComparer  
		{
			int IComparer.Compare( Object x1, Object x2 )  
			{
				double X1 = ((Vector2d)((object[])x1)[0]).X;
				double X2 = ((Vector2d)((object[])x2)[0]).X;
				if(X1 < X2)
					return -1;
				else if(X1 == X2)
					return 0;
				else
					return 1;
			}
		}

		private class EdgesSortYDesc : IComparer  
		{
			int IComparer.Compare( Object x1, Object x2 )  
			{
				Edge e1 = (Edge)x1;
				Edge e2 = (Edge)x2;

				Vector2d v1 = e1.GetLeftVertex();
				Vector2d v2 = e2.GetLeftVertex();

				double X1 = v1.Y;
				double X2 = v2.Y;
				if(X1 < X2)
					return 1;
				else if(X1 == X2)
					return 0;
				else
					return -1;
			}
		}


//        /// <summary>
//        /// Scanline Algorithmus zur bestimmung aller schnittpunkte in einer gegebenen menge von kanten
//        /// </summary>
//        /// <param name="edges">Listen mit allen Schnittpunkten, Schnittpunkte werden als Vectoren gespeichert</param>
//        /// <returns></returns>
//        public static ArrayList ScanlineX_GetCutpoints(Edge[] edges)
//        {
//            Vector2d cutpoint;
//            ArrayList cutpoints = new ArrayList();
//            ArrayList local_scanned = new ArrayList();

//            //create scanline list
//            ArrayList scanline = new ArrayList();

//            //build scanline
//            for(int i = 0;i<edges.Length;i++)
//            {
//                object[] vert_obj = new object[3];
//                vert_obj[0] = edges[i].v1;
//                vert_obj[1] = edges[i];
//                scanline.Add(vert_obj);
				
//                vert_obj = new object[3];
//                vert_obj[0] = edges[i].v2;
//                //stelle fest ob es eine Vertikale line ist
//                if(edges[i].IsEdgeVertical())
//                {
//                    Vector2d v = (Vector2d)vert_obj[0];
//                    v.X += 0.00000000000000000001;
//                }
//                vert_obj[1] = edges[i];
//                scanline.Add(vert_obj);
//            }

//            //sort scanline
//            scanline.Sort(new EdgesSortXAsc());

//            //verarbeite die scanline in diskreten schritten
//            while(scanline.Count>0)
//            {
//                object[] curr_vertex_obj = (object[])scanline[0];
//                scanline.Remove(curr_vertex_obj);

//                Vector2d curr_vertex = (Vector2d)curr_vertex_obj[0];
//                Edge curr_edge = (Edge)curr_vertex_obj[1];
//                Edge neighbour_edge = (Edge)curr_vertex_obj[2];

//                //bist du der start vertex oder der end vertex
//                if(curr_vertex.X == curr_edge.GetLeftVertex().X)
//                {
//                    AddLocalScanned(local_scanned,curr_edge);
//                    Edge prev_edge = GetPreviousEdgeFromList(local_scanned,curr_edge);
//                    Edge next_edge = GetNextEdgeFromList(local_scanned,curr_edge);

//                    //check cut with prev edge
//                    if(LinearAlgebra.GetCutPointInSegment(prev_edge,curr_edge,out cutpoint))
//                        AddCutPointToScanlist(scanline,cutpoint,curr_edge,prev_edge);
//                    //check cut with next edge
//                    if(LinearAlgebra.GetCutPointInSegment(next_edge,curr_edge,out cutpoint))
//                        AddCutPointToScanlist(scanline,cutpoint,curr_edge,next_edge);
//                }
//                else if(neighbour_edge != null)
//                {
//                    //es handelt sich um einen schnittpunkt
//                    cutpoints.Add(curr_vertex);
//                    ChangeElemPosition(local_scanned,curr_edge,neighbour_edge);
//                    Edge bottom = GetMostTop(local_scanned,curr_edge,neighbour_edge);
//                    Edge top = curr_edge;
//                    if(bottom==curr_edge)
//                        top = neighbour_edge;
					

////					Edge prev_edge = GetPreviousEdgeFromList(local_scanned,top);
////					Edge next_edge = GetNextEdgeFromList(local_scanned,bottom);
////					//check cut with next edge
////					if(LinearAlgebra.GetCutPointInSegment(prev_edge,top,out cutpoint))
////						AddCutPointToScanlist(scanline,cutpoint,prev_edge,top);
////					//check cut with next edge
////					if(LinearAlgebra.GetCutPointInSegment(next_edge,bottom,out cutpoint))
////						AddCutPointToScanlist(scanline,cutpoint,next_edge,bottom);


//                    Edge prev_edge = GetPreviousEdgeFromList(local_scanned,top);
//                    Edge next_edge = GetNextEdgeFromList(local_scanned,bottom);
//                    //check cut with next edge
//                    if(LinearAlgebra.GetCutPointInSegment(prev_edge,top,out cutpoint))
//                        AddCutPointToScanlist(scanline,cutpoint,prev_edge,bottom);
//                    //check cut with next edge
//                    if(LinearAlgebra.GetCutPointInSegment(next_edge,bottom,out cutpoint))
//                        AddCutPointToScanlist(scanline,cutpoint,next_edge,top);
//                }
//                else
//                {
//                    //es handelt sich um den endpunkt einer kante
//                    Edge prev_edge = GetPreviousEdgeFromList(local_scanned,curr_edge);
//                    Edge next_edge = GetNextEdgeFromList(local_scanned,curr_edge);
//                    //check cut with neighbour edges
//                    //check cut with next edge
//                    if(LinearAlgebra.GetCutPointInSegment(next_edge,prev_edge,out cutpoint))
//                        AddCutPointToScanlist(scanline,cutpoint,next_edge,prev_edge);
//                    local_scanned.Remove(curr_edge);
//                }
//            }
//            return cutpoints;
//        }
//        private static void AddCutPointToScanlist(ArrayList scanline, Vector2d cutpoint,Edge edge1,Edge edge2){
//            object[] vert_obj = new object[3];
//            vert_obj[0] = cutpoint;
//            vert_obj[1] = edge1;
//            vert_obj[2] = edge2;
//            scanline.Add(vert_obj);
//            scanline.Sort(new EdgesSortXAsc());
//        }
//        private static void ChangeElemPosition(ArrayList list,Edge edge1,Edge edge2){
//            int elem1_pos = list.IndexOf(edge1);
//            int elem2_pos = list.IndexOf(edge2);

//            if(elem1_pos < 0 || elem2_pos < 0 || elem1_pos == elem2_pos)
//                return;

//            list.RemoveAt(elem1_pos);
//            list.Insert(elem1_pos,edge2);

//            list.RemoveAt(elem2_pos);
//            list.Insert(elem2_pos,edge1);
//        }
//        private static Edge GetMostTop(ArrayList list,object edge1,object edge2)
//        {
//            bool found_one = false;
//            for(int i=0;i<list.Count;i++){
//                if(list[i]==edge1 && found_one)
//                    return (Edge)edge2;
//                else
//                    found_one = true;
//                if(list[i]==edge2 && found_one)
//                    return (Edge)edge1;
//                else
//                    found_one = true;
//            }
//            throw new GeoLibException("edges unknown");
//        }

//        private static Edge GetPreviousEdgeFromList(ArrayList list,object elem)
//        {
//            int elem_pos = list.IndexOf(elem);
//            if(elem_pos < 1)
//                return null;
//            else
//                return (Edge)list[elem_pos-1];
//        }

//        private static Edge GetNextEdgeFromList(ArrayList list,object elem)
//        {
//            int elem_pos = list.IndexOf(elem);
//            if(elem_pos < 0 || elem_pos == list.Count-1)
//                return null;
//            else 
//                return (Edge)list[elem_pos+1];
//        }

//        private static void AddLocalScanned(ArrayList list,Edge edge)
//        {
//            for(int i=0;i<list.Count;i++)
//            {
//                Edge curr_edge = (Edge)list[i];

//                Vector2d v1 = curr_edge.GetLeftVertex();
//                Vector2d v2 = edge.GetLeftVertex();

//                double X1 = v1.Y;
//                double X2 = v2.Y;

//                if(X1 < X2)
//                {
//                    list.Insert(i,edge);
//                    return;
//                }
//            }
//            list.Add(edge);
//        }
	}
}
