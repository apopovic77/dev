﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.Serialization;
using System.Data.Linq;
using System.Data.Linq.Mapping;

using LinqLib.Serialization;

namespace LinqLib.Reflection
{
    public class LinqReflectionHelper
    {
        #region Expression <-> Func

        /// <summary>
        /// Gets the MethodInfo of the generic FuncToExpression function
        /// </summary>
        /// <returns></returns>
        public static MethodInfo GeFuncToExpressionMethod()
        {
            MethodInfo[] methods = typeof(LinqReflectionHelper).GetMethods();

            foreach (MethodInfo mI in methods)
            {
                if (mI.Name == "FuncToExpression")
                    return mI;
            }

            return null;
        }

        public static Expression<Func<T, bool>> FuncToExpression<T>(Func<T, bool> f)
        {
            return x => f(x);
        }
        #endregion

        #region Standard c# reflection
        /// <summary>
        /// Gets the value of an objects' property by name using reflection
        /// </summary>
        /// <param name="obj">Object instance to get the property from</param>
        /// <param name="propertyName">Name of the property (case sensitive)</param>
        /// <returns>Value of the property. null if not found</returns>
        public static object GetPropertyValue(object obj, string propertyName)
        {
            if (obj != null)
            {
                foreach (PropertyInfo property in obj.GetType().GetProperties())
                {
                    if (property.Name == propertyName)
                    {
                        return property.GetValue(obj, null);
                    }
                }
            }

            return null;
        }
        /// <summary>
        /// Gets the PropertyInfo instance of an objects' property by name using reflection
        /// </summary>
        /// <param name="dataType">Data type where the proeprty should be searched</param>
        /// <param name="propertyName">Name of the property (case sensitive)</param>
        /// <returns>PropertyInfo instance of the property or null if not found</returns>
        public static PropertyInfo GetPropertyInfo(Type dataType, string propertyName)
        {
            if (dataType != null)
            {
                foreach (PropertyInfo property in dataType.GetProperties())
                {
                    if (property.Name == propertyName)
                    {
                        return property;
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Gets a flag if the data type is nullable
        /// </summary>
        /// <param name="property"></param>
        /// <returns></returns>
        public static bool IsGenericNullableType(PropertyInfo property)
        {
            Type propertyType = property.PropertyType;

            return IsGenericNullableType(propertyType);
        }

        /// <summary>
        /// Gets a flag if the data type is nullable
        /// </summary>
        /// <param name="propertyType"></param>
        /// <returns></returns>
        public static bool IsGenericNullableType(Type propertyType)
        {
            if (propertyType.IsGenericType)
            {
                // get a type reference to the generic EntitySet<T> type of Linq
                Type genericEntitySet = typeof(System.Nullable<>);
                // make a generic type using the same DataType as the current propertie's generic data type
                Type checkType = genericEntitySet.MakeGenericType(propertyType.GetGenericArguments()[0]);

                if (checkType == propertyType)
                {
                    return true;
                }
            }

            return false;
        }

        public static Type GetInnerTypeOfGenericType(Type genericType)
        {
            if(genericType.IsGenericType)
            {
                return genericType.GetGenericArguments()[0];
            }
            return null;
        }
        #endregion

        #region Linq specific type checking and operations
        /// <summary>
        /// Resolves a data object type to the inheritance master
        /// </summary>
        /// <param name="dc"></param>
        /// <param name="tableType"></param>
        /// <returns></returns>
        public static Type ResolveTableInheritanceType(DataContext dc, Type tableType)
        {
            Type retType = tableType;

            try
            {
                ITable tmp = dc.GetTable(tableType);
            } 
            catch // InvalidOperationException id the type does not represent a direct table mapping (e.g. for an inherited type)
            {
                // if this is not possible, the type may be an inherited type
                // check if the type has a parent type
                if(tableType.BaseType != null)
                {
                    return ResolveTableInheritanceType(dc, tableType.BaseType);
                }
            }

            return retType;
        }
        /// <summary>
        /// Checks if all the table classes are inheriting from IdentifyableObjectBase
        /// class which is needed for sucessfull deserialization to maintain
        /// object associations.
        /// </summary>
        /// <param name="dc">DataContext instance to check</param>
        /// <returns>true if ALL DataContext's table classes are inheriting from IdentifyableObjectBase</returns>
        public static bool AreDataClassesIdendifyable(DataContext dc)
        {
            bool bRet = true;
            int propCnt = 0;
            // loop through all properties of the data context and find all Table<> properties
            // the generic data type of these properties holds the class name
            // of the table class. this table class must inherit from the IdentifyableObjectBase class
            foreach (PropertyInfo property in dc.GetType().GetProperties())
            {
                // if the property is a generic type (System.Linq.Table<type>)
                if (property.PropertyType.IsGenericType)
                {
                    // get the generic types of this proeprty
                    Type[] genericTypes = property.PropertyType.GetGenericArguments();

                    // get a type reference to the generic Table<T> type of Linq
                    Type genericTablet = typeof(System.Data.Linq.Table<>);
                    // make a generic type using the same DataType as the current propertie's generic data type
                    Type checkType = genericTablet.MakeGenericType(genericTypes[0]);

                    if (checkType == property.PropertyType)
                    {
                        propCnt++;

                        bool bIsIdentifyable = false;

                        Type checkType1 = genericTypes[0];

                        // loop through the type inheritance path upwards
                        // and see if the check type inherits from IdentifyableObjectBase
                        do
                        {
                            if (checkType1 == typeof(IdentifyableObjectBase))
                            {
                                bIsIdentifyable = true;
                                checkType1 = null;
                            }
                            else
                            {
                                checkType1 = checkType1.BaseType;
                            }
                        } while (checkType1 != null);

                        bRet &= bIsIdentifyable;
                    }
                }
            }

            // if no properties where checked
            if (propCnt == 0)
                bRet = false;

            return bRet;
        }

        /// <summary>
        /// Checks if all the table classes or the data context are
        /// marked with the DataContract attribute which is needed
        /// for serialization.
        /// </summary>
        /// <param name="dc">DataContext instance to check</param>
        /// <returns>true if the DataContext's table classes are marked as serializable</returns>
        public static bool IsDataContextSerializeable(DataContext dc)
        {
            bool bRet = true;
            int propCnt = 0;
            // loop through all properties of the data context and find all Table<> properties
            // the generic data type of these properties holds the class name
            // of the table class. this table class must be marked with the DataContract attribute
            foreach (PropertyInfo property in dc.GetType().GetProperties())
            {
                // if the property is a generic type (System.Linq.Table<type>)
                if (property.PropertyType.IsGenericType)
                {
                    // get the generic types of this proeprty
                    Type[] genericTypes = property.PropertyType.GetGenericArguments();

                    // get a type reference to the generic Table<T> type of Linq
                    Type genericTablet = typeof(System.Data.Linq.Table<>);
                    // make a generic type using the same DataType as the current propertie's generic data type
                    Type checkType = genericTablet.MakeGenericType(genericTypes[0]);

                    if (checkType == property.PropertyType)
                    {
                        propCnt++;

                        bool dataContractAttribute = false;

                        // check if the type of the property (table class) is marked
                        // with the datacontract attribute for serialization
                        foreach (object attribute in genericTypes[0].GetCustomAttributes(false))
                        {
                            if (attribute is DataContractAttribute)
                                dataContractAttribute = true;
                        }

                        bRet &= dataContractAttribute;
                    }
                }
            }

            // if no properties where checked
            if (propCnt == 0)
                bRet = false;

            return bRet;
        }
        /// <summary>
        /// Gets the property name of the this-key column setting of an properties association attribute
        /// </summary>
        /// <param name="property"></param>
        /// <returns>Returns the this-key property name or null if not existent</returns>
        public static string GetAssociationAttributeThisKeyName(PropertyInfo property)
        {
            // find the association attribute of the property
            foreach (object attribute in property.GetCustomAttributes(false))
            {
                AssociationAttribute assocAttrib = attribute as AssociationAttribute;

                if (assocAttrib != null)
                {
                    // return the name of the this key column
                    return assocAttrib.ThisKey;
                }
            }

            return null;
        }

        /// <summary>
        /// Gets the property name of the other-key column setting of an properties association attribute
        /// </summary>
        /// <param name="property"></param>
        /// <returns>Returns the other-key property name or null if not existent</returns>
        public static string GetAssociationAttributeOtherKeyName(PropertyInfo property)
        {
            // find the association attribute of the property
            foreach (object attribute in property.GetCustomAttributes(false))
            {
                AssociationAttribute assocAttrib = attribute as AssociationAttribute;

                if (assocAttrib != null)
                {
                    // return the name of the this key column
                    return assocAttrib.OtherKey;
                }
            }

            return null;
        }

        /// <summary>
        /// Gets all Properties of an object which are representing the primary key of a type
        /// </summary>
        /// <param name="tableType"></param>
        /// <returns></returns>
        public static List<PropertyInfo> GetPrimaryKeys(Type tableType)
        {
            List<PropertyInfo> ret = new List<PropertyInfo>();

            foreach (PropertyInfo property in tableType.GetProperties())
            {
                // get all column attributes
                foreach (object attribute in property.GetCustomAttributes(typeof(ColumnAttribute), false))
                {
                    ColumnAttribute col = attribute as ColumnAttribute;

                    if (col != null && col.IsPrimaryKey)
                        ret.Add(property);
                }
            }

            return ret;
        }

        /// <summary>
        /// Gets all Properties of an object which are representing the original value of a key.
        /// you can use the <paramref name="propFilter"/> parameter to filter original value
        /// properties if they are mapping to one of the properties defined in <paramref name="propFilter"/>.
        /// </summary>
        /// <param name="tableType"></param>
        /// <param name="propFilter"></param>
        /// <returns></returns>
        public static List<PropertyInfo> GetOriginalValueProperties(Type tableType, List<PropertyInfo> propFilter)
        {
            List<PropertyInfo> ret = new List<PropertyInfo>();

            foreach (PropertyInfo property in tableType.GetProperties())
            {
                // get all original value attributes
                foreach (object attribute in property.GetCustomAttributes(typeof(OriginalValueIndicatorAttribute), false))
                {
                    OriginalValueIndicatorAttribute col = attribute as OriginalValueIndicatorAttribute;

                    if (col != null)
                    {
                        if (propFilter != null && propFilter.Count > 0)
                        {
                            foreach (PropertyInfo piFilter in propFilter)
                            {
                                if (piFilter.Name == col.MappedPrimaryKeyPropertyName)
                                {
                                    ret.Add(property);
                                    break;
                                }
                            }
                        }
                        else
                        {
                            ret.Add(property);
                        }
                    }
                }
            }

            return ret;
        }

        /// <summary>
        /// Gets all Properties of an object which are representing an association
        /// </summary>
        /// <param name="tableType">The data type of the table</param>
        /// <param name="onlyForeignKeys">true if the method should only return foreign key associations</param>
        /// <returns>Returns a list of property info instances which are representing associtaion properties. 
        /// If <paramref name="onlyForeignKeys"/> is ture then the return value will only contain foreign key associations.</returns>
        public static List<PropertyInfo> GetAssociationProperties(Type tableType, bool onlyForeignKeys)
        {
            return GetAssociationProperties(tableType, onlyForeignKeys, false);
        }

        /// <summary>
        /// Gets all Properties of an object which are representing an association
        /// </summary>
        /// <param name="tableType">The data type of the table</param>
        /// <param name="onlyForeignKeys">true if the method should only return foreign key associations</param>
        /// /// <param name="noForeignKeys">true if NO foreign key associations should be returned</param>
        /// <returns>Returns a list of property info instances which are representing associtaion properties. 
        /// If <paramref name="onlyForeignKeys"/> is ture then the return value will only contain foreign key associations.</returns>
        public static List<PropertyInfo> GetAssociationProperties(Type tableType, bool onlyForeignKeys, bool noForeignKeys)
        {
            List<PropertyInfo> ret = new List<PropertyInfo>();

            foreach (PropertyInfo property in tableType.GetProperties())
            {
                // get all association attributes
                foreach (object attribute in property.GetCustomAttributes(typeof(AssociationAttribute), false))
                {
                    AssociationAttribute association = attribute as AssociationAttribute;

                    if (association != null)
                    {
                        if ((!onlyForeignKeys || association.IsForeignKey) &&
                            (!noForeignKeys || (noForeignKeys && !association.IsForeignKey)))
                            ret.Add(property);
                    }
                }
            }

            return ret;
        }

        public static Dictionary<string, PropertyInfo> GetThisKeyForeignKeyAssocitations(Type tableType)
        {
            Dictionary<string, PropertyInfo> ret = new Dictionary<string, PropertyInfo>();

            foreach (PropertyInfo property in tableType.GetProperties())
            {
                // get all association attributes
                foreach (object attribute in property.GetCustomAttributes(typeof(AssociationAttribute), false))
                {
                    AssociationAttribute association = attribute as AssociationAttribute;

                    if (association != null && association.IsForeignKey)
                    {
                        if(!string.IsNullOrEmpty(association.ThisKey))
                        {
                            ret[association.ThisKey] = property;
                        }
                    }
                }
            }

            return ret;
        }

        public static Dictionary<string, PropertyInfo> GetOtherForeignKeyAssocitations(Type tableType)
        {
            Dictionary<string, PropertyInfo> ret = new Dictionary<string, PropertyInfo>();

            foreach (PropertyInfo property in tableType.GetProperties())
            {
                // get all association attributes
                foreach (object attribute in property.GetCustomAttributes(typeof(AssociationAttribute), false))
                {
                    AssociationAttribute association = attribute as AssociationAttribute;

                    if (association != null && association.IsForeignKey)
                    {
                        if (!string.IsNullOrEmpty(association.OtherKey))
                        {
                            ret[association.OtherKey] = property;
                        }
                    }
                }
            }

            return ret;
        }

        public static Dictionary<PropertyInfo, string> GetForeignKeyOtherKeyAssocitations(Type tableType)
        {
            Dictionary<PropertyInfo, string> ret = new Dictionary<PropertyInfo, string>();

            foreach (PropertyInfo property in tableType.GetProperties())
            {
                // get all association attributes
                foreach (object attribute in property.GetCustomAttributes(typeof(AssociationAttribute), false))
                {
                    AssociationAttribute association = attribute as AssociationAttribute;

                    if (association != null && association.IsForeignKey)
                    {
                        if (!string.IsNullOrEmpty(association.OtherKey))
                        {
                            ret[property] = association.OtherKey;
                        }
                    }
                }
            }

            return ret;
        }

        /// <summary>
        /// Checks if a property is a foreign key column property within a table data type
        /// </summary>
        /// <param name="tableType">DataType of the table object</param>
        /// <param name="property">PropertyInfo instance describing the property</param>
        /// <returns>True if the property is a foreign key column property</returns>
        public static bool IsForeignKeyColumnProperty(Type tableType, PropertyInfo property)
        {
            // loop through all properties
            foreach (PropertyInfo pI in tableType.GetProperties())
            {
                // get all association attributes
                foreach (object attribute in pI.GetCustomAttributes(typeof(AssociationAttribute), false))
                {
                    AssociationAttribute association = attribute as AssociationAttribute;

                    if (association != null)
                    {
                        if (association.IsForeignKey && association.ThisKey == property.Name)
                            return true;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Checks if a property is a foreign key column property within a table data type
        /// </summary>
        /// <param name="tableType">DataType of the table object</param>
        /// <param name="propertyName">Name of the property to check</param>
        /// <returns>True if the property is a foreign key column property</returns>
        public static bool IsForeignKeyColumnProperty(Type tableType, string propertyName)
        {
            // loop through all properties
            foreach (PropertyInfo pI in tableType.GetProperties())
            {
                // get all association attributes
                foreach (object attribute in pI.GetCustomAttributes(typeof(AssociationAttribute), false))
                {
                    AssociationAttribute association = attribute as AssociationAttribute;

                    if (association != null)
                    {
                        if (association.IsForeignKey && association.ThisKey == propertyName)
                            return true;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Checks if a table type contains a serialized entity set property
        /// </summary>
        /// <param name="tableType"></param>
        /// <returns></returns>
        public static bool HasSerializedEntitiySetProperty(Type tableType)
        {
            bool bRet = false;

            foreach (PropertyInfo property in tableType.GetProperties())
            {
                bool dataMemberAttribute = false;
                bool associationAttribute = false;

                // check if the property is marked with the 
                // association and datamember attribute
                foreach (object attribute in property.GetCustomAttributes(false))
                {
                    if (attribute is AssociationAttribute)
                        associationAttribute = true;

                    if (attribute is DataMemberAttribute)
                        dataMemberAttribute = true;
                }

                // if both attributes are present at the current property
                if (dataMemberAttribute && associationAttribute)
                {
                    Type propertyType = property.PropertyType;

                    if (propertyType.IsGenericType)
                    {
                        // get a type reference to the generic EntitySet<T> type of Linq
                        Type genericEntitySet = typeof(System.Data.Linq.EntitySet<>);
                        // make a generic type using the same DataType as the current propertie's generic data type
                        Type checkType = genericEntitySet.MakeGenericType(propertyType.GetGenericArguments()[0]);

                        if (checkType == propertyType)
                        {
                            return true;
                        }
                    }
                }
            }

            return bRet;
        }

        /// <summary>
        /// Checks if a property is a serialzed EntitySet property
        /// </summary>
        /// <param name="checkProp"></param>
        /// <returns></returns>
        public static bool IsSerializedEntitiySetProperty(PropertyInfo checkProp)
        {
            bool dataMemberAttribute = false;
            bool associationAttribute = false;

            // check if the property is marked with the 
            // association and datamember attribute
            foreach (object attribute in checkProp.GetCustomAttributes(false))
            {
                if (attribute is AssociationAttribute)
                    associationAttribute = true;

                if (attribute is DataMemberAttribute)
                    dataMemberAttribute = true;
            }

            // if both attributes are present at the current property
            if (dataMemberAttribute && associationAttribute)
            {
                Type propertyType = checkProp.PropertyType;

                if (propertyType.IsGenericType)
                {
                    // get a type reference to the generic EntitySet<T> type of Linq
                    Type genericEntitySet = typeof(System.Data.Linq.EntitySet<>);
                    // make a generic type using the same DataType as the current propertie's generic data type
                    Type checkType = genericEntitySet.MakeGenericType(propertyType.GetGenericArguments()[0]);

                    if (checkType == propertyType)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Checks if a table type contains a serialized entity set property
        /// </summary>
        /// <param name="tableType">Type of the linq table</param>
        /// <returns>A list of property info parameters which are describing all
        /// EntitySet properties</returns>
        public static List<PropertyInfo> GetSerializedEntitiySetProperties(Type tableType)
        {
            List<PropertyInfo> ret = new List<PropertyInfo>();

            foreach (PropertyInfo property in tableType.GetProperties())
            {
                bool dataMemberAttribute = false;
                bool associationAttribute = false;

                // check if the property is marked with the 
                // association and datamember attribute
                foreach (object attribute in property.GetCustomAttributes(false))
                {
                    if (attribute is AssociationAttribute)
                        associationAttribute = true;

                    if (attribute is DataMemberAttribute)
                        dataMemberAttribute = true;
                }

                // if both attributes are present at the current property
                if (dataMemberAttribute && associationAttribute)
                {
                    Type propertyType = property.PropertyType;

                    if (propertyType.IsGenericType)
                    {
                        // get a type reference to the generic EntitySet<T> type of Linq
                        Type genericEntitySet = typeof(System.Data.Linq.EntitySet<>);
                        // make a generic type using the same DataType as the current propertie's generic data type
                        Type checkType = genericEntitySet.MakeGenericType(propertyType.GetGenericArguments()[0]);

                        if (checkType == propertyType)
                        {
                            ret.Add(property);
                        }
                    }
                }
            }

            return ret;
        }

        /// <summary>
        /// Checks if a column name is an identity culumns
        /// </summary>
        /// <param name="columName"></param>
        /// <param name="tableType"></param>
        /// <returns></returns>
        public static bool IsIdentityColumn(string columName, Type tableType)
        {
            bool bRet = false;

            foreach (PropertyInfo property in tableType.GetProperties())
            {
                // get all column attributes
                foreach (object attribute in property.GetCustomAttributes(typeof(ColumnAttribute), false))
                {
                    ColumnAttribute col = attribute as ColumnAttribute;

                    if (col != null &&
                        col.DbType.ToUpper().Contains("IDENTITY") &&
                        property.Name == columName)
                        return true;
                }
            }

            return bRet;
        }

        /// <summary>
        /// Gets the property of a DataContext which represents a given data table
        /// </summary>
        /// <param name="tableType">Type of the data table</param>
        /// <param name="dc">DataContext to use</param>
        /// <returns>Returns the property of the data context which represents the table or null if not exists</returns>
        public static PropertyInfo GetTablePropertyOfDataContext(Type tableType, DataContext dc)
        {
            // loop through all properties
            foreach (PropertyInfo property in dc.GetType().GetProperties())
            {
                // if the property is a generic type (System.Linq.Table<type>)
                if (property.PropertyType.IsGenericType)
                {
                    // get the generic types of this proeprty
                    Type[] genericTypes = property.PropertyType.GetGenericArguments();

                    // search for the type of the table
                    foreach (Type curType in genericTypes)
                    {
                        if (curType == tableType)
                            return property;
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Checks if a type represents a Linq table
        /// </summary>
        /// <param name="tableType"></param>
        /// <returns></returns>
        public static bool IsLinqTable(Type tableType)
        {
           
            // get all column attributes
            foreach (object attribute in tableType.GetCustomAttributes(typeof(TableAttribute), false))
            {
                TableAttribute atr = attribute as TableAttribute;

                if (atr != null)
                    return true;
            }

            return false;
        }

        /// <summary>
        /// Gets a key-value pair list holding the information about the physical SQL table name
        /// and the mapped data type
        /// </summary>
        /// <param name="dc">Data context to retrieve the information from</param>
        /// <returns>A list of key-value pair items where key is the name of the physical table
        /// and the value is a Type object representing the mapped data type 
        /// </returns>
        //public static List<KeyValuePair<string, Type>> GetMappedTableNames(DataContext dc)
        public static Dictionary<string, Type> GetMappedTableNames(DataContext dc)
        {
            //List<KeyValuePair<string, Type>> retList = new List<KeyValuePair<string, Type>>();
            Dictionary<string, Type> retList = new Dictionary<string, Type>();

            // loop through all properties
            foreach (PropertyInfo property in dc.GetType().GetProperties())
            {
                Type propertyType = property.PropertyType;

                // if the property is a generic type (System.Linq.Table<type>)
                if (propertyType.IsGenericType)
                {
                    // get a type reference to the generic Table<T> type of Linq
                    Type genericEntitySet = typeof(System.Data.Linq.Table<>);
                    // make a generic type using the same DataType as the current propertie's generic data type
                    Type checkType = genericEntitySet.MakeGenericType(propertyType.GetGenericArguments()[0]);

                    if (checkType == propertyType)
                    {
                        Type tableDataType = propertyType.GetGenericArguments()[0];
                        string physicalTableName = "";

                        // get all column attributes
                        foreach (object attribute in tableDataType.GetCustomAttributes(typeof(TableAttribute), false))
                        {
                            TableAttribute tableinfo = attribute as TableAttribute;

                            if (tableinfo != null && !string.IsNullOrEmpty(tableinfo.Name))
                            {
                                physicalTableName = tableinfo.Name;
                                break;
                            }
                        }

                        if(!string.IsNullOrEmpty(physicalTableName))
                        {
                            KeyValuePair<string, Type> newInfo = new KeyValuePair<string, Type>(physicalTableName, tableDataType);
                            retList.Add(physicalTableName, tableDataType);
                        }

                    }
                }
            }

            return retList;
        }

        
        /// <summary>
        /// Compares two entities using the primary key fields
        /// </summary>
        /// <param name="entity1">Entity 1</param>
        /// <param name="entity2">Entity 2</param>
        /// <returns>True if the 2 entitys have the same primary key values</returns>
        public static bool ComparePrimaryKeyFields(object entity1, object entity2)
        {
            Type entityType = entity1.GetType();

            if (entityType != entity2.GetType())
                throw new NotSupportedException("Entity 1 and 2 must be of the same type.");

            List<PropertyInfo> primaryKeyFields = GetPrimaryKeys(entityType);

            return ComparePrimaryKeyFields(entity1, entity2, primaryKeyFields);
        }

        /// <summary>
        /// Compares two entities using the primary key fields
        /// </summary>
        /// <param name="entity1">Entity 1</param>
        /// <param name="entity2">Entity 2</param>
        /// <param name="primaryKeyFields">PropertyInfo list of primary key fields</param>
        /// <returns>True if the 2 entitys have the same primary key values</returns>
        public static bool ComparePrimaryKeyFields(object entity1, object entity2, List<PropertyInfo> primaryKeyFields)
        {
            Type entityType = entity1.GetType();

            if (entityType != entity2.GetType())
                throw new NotSupportedException("Entity 1 and 2 must be of the same type.");

            if (primaryKeyFields.Count > 0)
            {
                bool bRet = true;

                foreach (PropertyInfo pI in primaryKeyFields)
                {
                    object val1 = pI.GetValue(entity1, null);
                    object val2 = pI.GetValue(entity2, null);

                    if (val1 == null)
                    {
                        if (val2 != null)
                        {
                            bRet = false;
                            break;
                        }
                    }
                    else if (val2 == null)
                    {
                        bRet = false;
                        break;
                    }
                    else
                    {
                        if (!val1.Equals(val2))
                        {
                            bRet = false;
                            break;
                        }
                    }
                }

                return bRet;
            }

            return false;
        }

        #endregion

        #region Get MethodInfo - class Enumerable
        /// <summary>
        /// Gets the methodInfo of the FIRST static, generic ToList() method of the Enumerable class
        /// </summary>
        /// <returns></returns>
        public static MethodInfo GetToListMethodInfo()
        {
            MethodInfo[] methods = typeof(Enumerable).GetMethods();

            foreach (MethodInfo mI in methods)
            {
                if (mI.Name == "ToList" && mI.GetParameters().Count() == 1)
                    return mI;
            }

            return null;
        }

        /// <summary>
        /// Gets the methodInfo of the FIRST static, generic Where() method of the Enumerable class
        /// </summary>
        /// <returns></returns>
        public static MethodInfo GetWhereMethodInfo()
        {
            MethodInfo[] methods = typeof(Enumerable).GetMethods();

            foreach (MethodInfo mI in methods)
            {
                if (mI.Name == "Where" && mI.GetParameters().Count() == 1)
                    return mI;
            }

            return null;
        }

        /// <summary>
        /// Gets the methodInfo of the static, generic Where(Func&lt;T, R&gt;) method of the Enumerable class
        /// </summary>
        /// <returns></returns>
        public static MethodInfo GetWhereWithPredicateMethodInfo()
        {
            MethodInfo[] methods = typeof(Enumerable).GetMethods();

            foreach (MethodInfo mI in methods)
            {
                if (mI.Name == "Where" && mI.GetParameters().Count() == 2)
                    return mI;
            }

            return null;
        }

        /// <summary>
        /// Gets the methodInfo of the static, generic Where(Func&lt;T, R&gt;) method of the Enumerable class
        /// </summary>
        /// <returns></returns>
        public static MethodInfo GetQueryableWhereWithPredicateMethodInfo()
        {
            MethodInfo[] methods = typeof(Queryable).GetMethods();

            foreach (MethodInfo mI in methods)
            {
                if (mI.Name == "Where" && mI.GetParameters().Count() == 2)
                    return mI;
            }

            return null;
        }

        /// <summary>
        /// Gets the methodInfo of the FIRST static, generic Count() method of the Enumerable class
        /// </summary>
        /// <returns></returns>
        public static MethodInfo GetCountMethodInfo()
        {
            MethodInfo[] methods = typeof(Enumerable).GetMethods();

            foreach (MethodInfo mI in methods)
            {
                if (mI.Name == "Count" && mI.GetParameters().Count() == 1)
                    return mI;
            }

            return null;
        }

        /// <summary>
        /// Gets the methodInfo of the static, generic Count(Func&lt;T, R&gt;) method of the Enumerable class
        /// </summary>
        /// <returns></returns>
        public static MethodInfo GetCountWithPredicateMethodInfo()
        {
            MethodInfo[] methods = typeof(Enumerable).GetMethods();

            foreach (MethodInfo mI in methods)
            {
                if (mI.Name == "Count" && mI.GetParameters().Count() == 2)
                    return mI;
            }

            return null;
        }

        /// <summary>
        /// Gets the methodInfo of the FIRST static, generic First() method of the Enumerable class
        /// </summary>
        /// <returns></returns>
        public static MethodInfo GetFirstMethodInfo()
        {
            MethodInfo[] methods = typeof(Enumerable).GetMethods();

            foreach (MethodInfo mI in methods)
            {
                if (mI.Name == "First" && mI.GetParameters().Count() == 1)
                    return mI;
            }

            return null;
        }

        /// <summary>
        /// Gets the methodInfo of the static, generic First(Func&lt;T, R&gt;) method of the Enumerable class
        /// </summary>
        /// <returns></returns>
        public static MethodInfo GetFirstWithPredicateMethodInfo()
        {
            MethodInfo[] methods = typeof(Enumerable).GetMethods();

            foreach (MethodInfo mI in methods)
            {
                if (mI.Name == "First" && mI.GetParameters().Count() == 2)
                    return mI;
            }

            return null;
        }

        /// <summary>
        /// Gets the methodInfo of the FIRST static, generic FirstOrDefault() method of the Enumerable class
        /// </summary>
        /// <returns></returns>
        public static MethodInfo GetFirstOrDefaultMethodInfo()
        {
            MethodInfo[] methods = typeof(Enumerable).GetMethods();

            foreach (MethodInfo mI in methods)
            {
                if (mI.Name == "FirstOrDefault" && mI.GetParameters().Count() == 1)
                    return mI;
            }

            return null;
        }

        /// <summary>
        /// Gets the methodInfo of the static, generic FirstOrDefault(Func&lt;T, R&gt;) method of the Enumerable class
        /// </summary>
        /// <returns></returns>
        public static MethodInfo GetFirstOrDefaultWithPredicateMethodInfo()
        {
            MethodInfo[] methods = typeof(Enumerable).GetMethods();

            foreach (MethodInfo mI in methods)
            {
                if (mI.Name == "FirstOrDefault" && mI.GetParameters().Count() == 2)
                    return mI;
            }

            return null;
        }

        /// <summary>
        /// Gets the methodInfo of the FIRST static, generic Single() method of the Enumerable class
        /// </summary>
        /// <returns></returns>
        public static MethodInfo GetSingleMethodInfo()
        {
            MethodInfo[] methods = typeof(Enumerable).GetMethods();

            foreach (MethodInfo mI in methods)
            {
                if (mI.Name == "Single" && mI.GetParameters().Count() == 1)
                    return mI;
            }

            return null;
        }

        /// <summary>
        /// Gets the methodInfo of the static, generic Single(Func&lt;T, R&gt;) method of the Enumerable class
        /// </summary>
        /// <returns></returns>
        public static MethodInfo GetSingleWithPredicateMethodInfo()
        {
            MethodInfo[] methods = typeof(Enumerable).GetMethods();

            foreach (MethodInfo mI in methods)
            {
                if (mI.Name == "Single" && mI.GetParameters().Count() == 2)
                    return mI;
            }

            return null;
        }

        /// <summary>
        /// Gets the methodInfo of the FIRST static, generic SingleOrDefault() method of the Enumerable class
        /// </summary>
        /// <returns></returns>
        public static MethodInfo GetSingleOrDefaultMethodInfo()
        {
            MethodInfo[] methods = typeof(Enumerable).GetMethods();

            foreach (MethodInfo mI in methods)
            {
                if (mI.Name == "SingleOrDefault" && mI.GetParameters().Count() == 1)
                    return mI;
            }

            return null;
        }

        /// <summary>
        /// Gets the methodInfo of the static, generic SingleOrDefault(Func&lt;T, R&gt;) method of the Enumerable class
        /// </summary>
        /// <returns></returns>
        public static MethodInfo GetSingleOrDefaultWithPredicateMethodInfo()
        {
            MethodInfo[] methods = typeof(Enumerable).GetMethods();

            foreach (MethodInfo mI in methods)
            {
                if (mI.Name == "SingleOrDefault" && mI.GetParameters().Count() == 2)
                    return mI;
            }

            return null;
        }
        #endregion

        #region Get PropertyInfo/MethodInfo - class Table<T>
        /// <summary>
        /// Gets the methodInfo of the Attach(&lt;T&gt; originalEntity) method of the Table&lt;T&gt; class
        /// </summary>
        /// <returns></returns>
        public static MethodInfo GetTableAttachOriginalMethodInfo(Type dataType)
        {
            MethodInfo[] methods = dataType.GetMethods();

            foreach (MethodInfo mI in methods)
            {
                if (mI.Name == "Attach" && mI.GetParameters().Count() == 1)
                    return mI;
            }

            return null;
        }

        /// <summary>
        /// Gets the methodInfo of the Attach(&lt;T&gt; newEntity, bool modified) method of the Table&lt;T&gt; class
        /// </summary>
        /// <returns></returns>
        public static MethodInfo GetTableAttachModifiedMethodInfo(Type dataType)
        {
            MethodInfo[] methods = dataType.GetMethods();

            foreach (MethodInfo mI in methods)
            {
                if (mI.Name == "Attach")
                {
                    ParameterInfo[] pI = mI.GetParameters();

                    if (pI.Count() == 2)
                    {
                        if (pI[1].ParameterType == typeof(bool))
                            return mI;
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Gets the methodInfo of the Attach(&lt;T&gt; newEntity, &lt;T&gt; originalEntity) method of the Table&lt;T&gt; class
        /// </summary>
        /// <returns></returns>
        public static MethodInfo GetTableAttachWithOriginalMethodInfo(Type dataType)
        {
            MethodInfo[] methods = dataType.GetMethods();

            foreach (MethodInfo mI in methods)
            {
                if (mI.Name == "Attach")
                {
                    ParameterInfo[] pI = mI.GetParameters();

                    if (pI.Count() == 2)
                    {
                        if (pI[1].ParameterType != typeof(bool))
                            return mI;
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Gets the methodInfo of the Where() method of the Table&lt;T&gt; class
        /// </summary>
        /// <returns></returns>
        public static MethodInfo GetWhereExpression(Type dataType)
        {
            MethodInfo[] methods = dataType.GetMethods();

            foreach (MethodInfo mI in methods)
            {
                if (mI.Name == "Where")
                {
                    ParameterInfo[] pI = mI.GetParameters();

                    if (pI.Count() == 1)
                    {
                        //if (pI[0].ParameterType != typeof(Express))
                            return mI;
                    }
                }
            }

            return null;
        }
        #endregion

        #region Get PropertyInfo/MethodInfo - class EntitySet<T>
        /// <summary>
        /// Gets the methodInfo of the Add() method of the EntitySet&lt;T&gt; class
        /// </summary>
        /// <returns></returns>
        public static MethodInfo GetEntitySetAddMethodInfo(Type dataType)
        {
            MethodInfo[] methods = dataType.GetMethods();

            foreach (MethodInfo mI in methods)
            {
                if (mI.Name == "Add" && mI.GetParameters().Count() == 1)
                    return mI;
            }

            return null;
        }
        #endregion

        #region Get PropertyInfo/MethodInfo - interface IEnumerable<T>
        /// <summary>
        /// Gets the methodInfo of the FIRST static, generic GetEnumerator() method of the IEnumerable&lt;T&gt; implementing type
        /// </summary>
        /// <returns></returns>
        public static MethodInfo GetEnumeratorMethodInfo(Type dataType)
        {
            MethodInfo[] methods = dataType.GetMethods();

            foreach (MethodInfo mI in methods)
            {
                if (mI.Name == "GetEnumerator" && mI.GetParameters().Count() == 0)
                    return mI;
            }

            return null;
        }
        #endregion

        #region Get PropertyInfo/MethodInfo - interface IEnumerator<T>
        /// <summary>
        /// Gets the methodInfo of the MoveNet() method of the IEnumerator&lt;T&gt; implementing type
        /// </summary>
        /// <returns></returns>
        public static MethodInfo GetIEnumeratorMoveNextMethodInfo(Type dataType)
        {
            MethodInfo[] methods = dataType.GetMethods();

            foreach (MethodInfo mI in methods)
            {
                if (mI.Name == "MoveNext" && mI.GetParameters().Count() == 0)
                    return mI;
            }

            return null;
        }

        /// <summary>
        /// Gets the propertyInfo of the Current property of the IEnumerator&lt;T&gt; implementing type
        /// </summary>
        /// <returns></returns>
        public static PropertyInfo GetIEnumeratorCurrentPropertyInfo(Type dataType)
        {
            PropertyInfo[] properties = dataType.GetProperties();

            foreach (PropertyInfo pI in properties)
            {
                if (pI.Name == "Current")
                    return pI;
            }

            return null;
        }
        #endregion

        #region Get PropertyInfo/MethodInfo - Querable
        /// <summary>
        /// Gets the methodInfo of the static, generic SingleOrDefault(Func&lt;T, R&gt;) method of the Enumerable class
        /// </summary>
        /// <returns></returns>
        public static MethodInfo GetQueryableCastMethodInfo()
        {
            MethodInfo[] methods = typeof(Queryable).GetMethods();

            foreach (MethodInfo mI in methods)
            {
                if (mI.Name == "Cast")
                    return mI;
            }

            return null;
        }
        #endregion

        #region Get MethodInfo - static class LinqReflectionHelper
        /// <summary>
        /// Gets the MethodInfo of the generic PrimaryKeyPredicate function
        /// </summary>
        /// <returns></returns>
        public static MethodInfo GetPrimaryKeyPredicate()
        {
            MethodInfo[] methods = typeof(LinqReflectionHelper).GetMethods();

            foreach (MethodInfo mI in methods)
            {
                if (mI.Name == "PKPredicate")
                    return mI;
            }

            return null;
        }


        /// <summary>
        /// Gets the MethodInfo of the generic SingleLongPrimaryKeyPredicate function
        /// </summary>
        /// <returns></returns>
        public static MethodInfo GetSingleLongPrimaryKeyPredicate()
        {
            MethodInfo[] methods = typeof(LinqReflectionHelper).GetMethods();

            foreach (MethodInfo mI in methods)
            {
                if (mI.Name == "SingleLongPKPredicate")
                    return mI;
            }

            return null;
        }


        /// <summary>
        /// Gets the MethodInfo of the generic ThisKeyForeignKeySearchPredicate function
        /// </summary>
        /// <returns></returns>
        public static MethodInfo GetThisKeyForeignKeySearchPredicate()
        {
            MethodInfo[] methods = typeof(LinqReflectionHelper).GetMethods();

            foreach (MethodInfo mI in methods)
            {
                if (mI.Name == "ThisKeyForeignKeySearchPredicate")
                    return mI;
            }

            return null;
        }

        /// <summary>
        /// Gets the MethodInfo of the generic ObjectIdentifierPredicate function
        /// </summary>
        /// <returns></returns>
        public static MethodInfo GetObjectIdentifierPredicate()
        {
            MethodInfo[] methods = typeof(LinqReflectionHelper).GetMethods();

            foreach (MethodInfo mI in methods)
            {
                if (mI.Name == "ObjectIdentifierPredicate")
                    return mI;
            }

            return null;
        }
        #endregion

        #region special predicate function declarations
        /// <summary>
        /// Returning a predicate function which may be used in a Where clausel of a linq query.<br/>
        /// The predicate function uses a list of primary key properti infos and comparing the current items 
        /// value against a data object's property value.
        /// </summary>
        /// <typeparam name="T">Type of the Linq table object</typeparam>
        /// <param name="primaryKeys">List of parameter presenting the primary keys in T</param>
        /// <param name="data">Data object which has to be of type T</param>
        /// <returns>A predicate function which may be used in where clauses of Linq queries.</returns>
        public static Func<T, bool> PKPredicate<T>(List<PropertyInfo> primaryKeys, object data)
        {
            var ret = new Func<T, bool>(t =>
            {
                if (t == null)
                    return false;

                bool bRet = true;

                foreach (PropertyInfo pK in primaryKeys)
                {
                    // check if the current primary key is a identity column
                    bool bIsIdentity = IsIdentityColumn(pK.Name, data.GetType());
                    bool bCheckVals = true;

                    // if it is an identity colum
                    if (bIsIdentity)
                    {
                        // get the value of the check item
                        long nVal = 0;
                        if(data != null)
                            nVal = (long)pK.GetValue(data, null);
                        // if the value is 0, then irgnore the value check and return false
                        // this is necessary because LinQ objects with identities will always receive
                        // 0 identity value as long as they have not been submitted
                        // therefore a value of 0 means, that this is a not-inserted object which will
                        // automatically receive a new unique identity value after the insertion
                        bCheckVals = nVal != 0;
                        if(nVal == 0)
                            bRet = false;
                    }

                    if (bCheckVals)
                    {
                        object tVal = pK.GetValue(t, null);
                        object dataVal = pK.GetValue(data, null);

                        if (tVal == null)
                        {
                            if (dataVal == null)
                                bRet &= true;
                            else
                                bRet = false;
                        }
                        else
                        {
                            bRet &= tVal.Equals(dataVal);
                        }
                    }

                    if (!bRet)
                        break;
                }

                return bRet;
            });
            return ret;
        }

        /// <summary>
        /// Returning a predicate function which may be used in a Where clausel of a linq query.<br/>
        /// The predicate function uses a list of primary key properti infos and comparing the current items 
        /// value against a data object's property value.
        /// </summary>
        /// <typeparam name="T">Type of the Linq table object</typeparam>
        /// <param name="singleKeyProperty">property name contianing a key</param>
        /// <param name="compareValue">value to search</param>
        /// <returns>A predicate function which may be used in where clauses of Linq queries.</returns>
        public static Func<T, bool> SingleLongPKPredicate<T>(PropertyInfo singleKeyProperty, long compareValue)
        {
            var ret = new Func<T, bool>(t =>
            {
                if (t == null)
                    return false;

                bool bRet = true;


                    // check if the current primary key is a identity column
                bool bIsIdentity = IsIdentityColumn(singleKeyProperty.Name, typeof(T));
                bool bCheckVals = true;

                // if it is an identity colum
                if (bIsIdentity)
                {
                    // if the value is 0, then irgnore the value check and return false
                    // this is necessary because LinQ objects with identities will always receive
                    // 0 identity value as long as they have not been submitted
                    // therefore a value of 0 means, that this is a not-inserted object which will
                    // automatically receive a new unique identity value after the insertion
                    bCheckVals = compareValue != 0;
                    if (compareValue == 0)
                        bRet = false;
                }

                if (bCheckVals)
                {
                    object tVal = singleKeyProperty.GetValue(t, null);
                    
                    if(tVal == null)
                    {
                        bRet = false;
                    }
                    else
                    {
                        if(tVal is long)
                        {
                            bRet = compareValue == (long)tVal;
                        }
                        else
                        {
                            bRet = false;
                        }
                    }
                }

                return bRet;
            });
            return ret;
        }

        /// <summary>
        /// Returning a predicate function which may be used in a Where clausel of a linq query.<br/>
        /// The predicate function uses a uses a source and destination property info object to get
        /// property values which will be compared.
        /// </summary>
        /// <typeparam name="T">Type of the Linq table object</typeparam>
        /// <param name="listDataObjProp">The property info instance which will be used to get the compare value
        /// for items of the current items collection in the linq query.</param>
        /// <param name="dataObjProp">The property info instance which will be used to get the value from the data
        /// object. This value will be compared to the one from the list item</param>
        /// <param name="data">Data object for comparision</param>
        /// <returns>A predicate function which may be used in where clauses of Linq queries.</returns>
        public static Func<T, bool> ThisKeyForeignKeySearchPredicate<T>(PropertyInfo listDataObjProp, PropertyInfo dataObjProp, object data)
        {
            var ret = new Func<T, bool>(t =>
            {
                bool bRet = true;

                bRet = (listDataObjProp.GetValue(t, null).Equals(dataObjProp.GetValue(data, null)));

                return bRet;
            });
            return ret;
        }

        /// <summary>
        /// Returning a predicate function which may be used in a Where clausel of a linq query.<br/>
        /// The predicate function checks if an object with the same ObjectIdentifier already exists
        /// </summary>
        /// <typeparam name="T">Type of the Linq table object</typeparam>
        /// <param name="data">Data object which has to be of type T</param>
        /// <returns>A predicate function which may be used in where clauses of Linq queries.</returns>
        public static Func<T, bool> ObjectIdentifierPredicate<T>(object data)
        {
            var ret = new Func<T, bool>(t =>
            {
                bool bRet = true;

                // both the collection items and the data item
                // must inherit from IdentifyableObjectBase in order to access the
                // object identifier property called ObjectId
                if (!(t is IdentifyableObjectBase) || !(data is IdentifyableObjectBase))
                {
                    return false;
                }

                bRet = (GetPropertyValue(t, "ObjectId").Equals(GetPropertyValue(data, "ObjectId")));

                return bRet;
            });

            return ret;
        }
        #endregion
    }
}
