﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;

namespace Logicx.WpfUtility
{
    public class KeyBoardInfo
    { 
        public DesktopButton DesktopButton;
        public Vector TargetPosition;
    }

    public class KeyBoard
    {
        public KeyBoard(Canvas keyboard_canvas, ToggleButton parent_button, float button_dim_len, float button_margin, Style key_button_style, Vector keyboard_pos_topleft)
        {
            _key_button_style = key_button_style;
            _button_dim_len = button_dim_len;
            _button_margin = button_margin;
            _parent_button = parent_button;
            _parent_button.Checked += new RoutedEventHandler(_parent_button_Checked);
            _parent_button.Unchecked += new RoutedEventHandler(_parent_button_Unchecked);
            _keyboard_canvas = keyboard_canvas;
            _kinfos = new List<KeyBoardInfo>();
            _kinfos_dict = new Dictionary<string, KeyBoardInfo>();
            CreateButtons(keyboard_pos_topleft);
        }

        void _parent_button_Unchecked(object sender, RoutedEventArgs e)
        {
            Vector button_start_pos = GetParentPos(_parent_button);

            _kinfos_dict["q"].DesktopButton.DefaultCloseAnim(button_start_pos);
            _kinfos_dict["w"].DesktopButton.DefaultCloseAnim(button_start_pos);
            _kinfos_dict["e"].DesktopButton.DefaultCloseAnim(button_start_pos);
            _kinfos_dict["r"].DesktopButton.DefaultCloseAnim(button_start_pos);
            _kinfos_dict["t"].DesktopButton.DefaultCloseAnim(button_start_pos);
            _kinfos_dict["z"].DesktopButton.DefaultCloseAnim(button_start_pos);
            _kinfos_dict["u"].DesktopButton.DefaultCloseAnim(button_start_pos);
            _kinfos_dict["i"].DesktopButton.DefaultCloseAnim(button_start_pos);
            _kinfos_dict["o"].DesktopButton.DefaultCloseAnim(button_start_pos);
            _kinfos_dict["p"].DesktopButton.DefaultCloseAnim(button_start_pos);
            _kinfos_dict["ü"].DesktopButton.DefaultCloseAnim(button_start_pos);
            _kinfos_dict["a"].DesktopButton.DefaultCloseAnim(button_start_pos);

            _kinfos_dict["s"].DesktopButton.DefaultCloseAnim(button_start_pos);
            _kinfos_dict["d"].DesktopButton.DefaultCloseAnim(button_start_pos);
            _kinfos_dict["f"].DesktopButton.DefaultCloseAnim(button_start_pos);
            _kinfos_dict["g"].DesktopButton.DefaultCloseAnim(button_start_pos);
            _kinfos_dict["h"].DesktopButton.DefaultCloseAnim(button_start_pos);
            _kinfos_dict["j"].DesktopButton.DefaultCloseAnim(button_start_pos);
            _kinfos_dict["k"].DesktopButton.DefaultCloseAnim(button_start_pos);
            _kinfos_dict["l"].DesktopButton.DefaultCloseAnim(button_start_pos);
            _kinfos_dict["ö"].DesktopButton.DefaultCloseAnim(button_start_pos);
            _kinfos_dict["ä"].DesktopButton.DefaultCloseAnim(button_start_pos);

            _kinfos_dict["y"].DesktopButton.DefaultCloseAnim(button_start_pos);
            _kinfos_dict["x"].DesktopButton.DefaultCloseAnim(button_start_pos);
            _kinfos_dict["c"].DesktopButton.DefaultCloseAnim(button_start_pos);
            _kinfos_dict["v"].DesktopButton.DefaultCloseAnim(button_start_pos);
            _kinfos_dict["b"].DesktopButton.DefaultCloseAnim(button_start_pos);
            _kinfos_dict["n"].DesktopButton.DefaultCloseAnim(button_start_pos);
            _kinfos_dict["m"].DesktopButton.DefaultCloseAnim(button_start_pos);

            _kinfos_dict["leer"].DesktopButton.DefaultCloseAnim(button_start_pos);
            _kinfos_dict["start"].DesktopButton.DefaultCloseAnim(button_start_pos);
            _kinfos_dict["zurück"].DesktopButton.DefaultCloseAnim(button_start_pos);

            _kinfos_dict["texteingabe"].DesktopButton.StartButtonAnim(
                button_start_pos, //pos_end
                1, //scale_start
                0, //scale_end
                1, //opacity_start
                0, //opacity_end
                100, //time_anim_start
                100, //time_anim_start_varianz
                1200, //time_anim_end
                100, //time_anim_end_varianz
                _kinfos_dict["texteingabe"].DesktopButton.HideAnimationFinishedDelegate); //completed delegate  
        }

        void _parent_button_Checked(object sender, RoutedEventArgs e)
        {
            Vector button_start_pos = GetParentPos(_parent_button);

            _kinfos_dict["q"].DesktopButton.DefaultOpenAnim(button_start_pos, q_button.TargetPosition);
            _kinfos_dict["w"].DesktopButton.DefaultOpenAnim(button_start_pos, w_button.TargetPosition);
            _kinfos_dict["e"].DesktopButton.DefaultOpenAnim(button_start_pos, e_button.TargetPosition);
            _kinfos_dict["r"].DesktopButton.DefaultOpenAnim(button_start_pos, r_button.TargetPosition);
            _kinfos_dict["t"].DesktopButton.DefaultOpenAnim(button_start_pos, t_button.TargetPosition);
            _kinfos_dict["z"].DesktopButton.DefaultOpenAnim(button_start_pos, z_button.TargetPosition);
            _kinfos_dict["u"].DesktopButton.DefaultOpenAnim(button_start_pos, u_button.TargetPosition);
            _kinfos_dict["i"].DesktopButton.DefaultOpenAnim(button_start_pos, i_button.TargetPosition);
            _kinfos_dict["o"].DesktopButton.DefaultOpenAnim(button_start_pos, o_button.TargetPosition);
            _kinfos_dict["p"].DesktopButton.DefaultOpenAnim(button_start_pos, p_button.TargetPosition);
            _kinfos_dict["ü"].DesktopButton.DefaultOpenAnim(button_start_pos, ü_button.TargetPosition);
            
            _kinfos_dict["a"].DesktopButton.DefaultOpenAnim(button_start_pos, a_button.TargetPosition);
            _kinfos_dict["s"].DesktopButton.DefaultOpenAnim(button_start_pos, s_button.TargetPosition);
            _kinfos_dict["d"].DesktopButton.DefaultOpenAnim(button_start_pos, d_button.TargetPosition);
            _kinfos_dict["f"].DesktopButton.DefaultOpenAnim(button_start_pos, f_button.TargetPosition);
            _kinfos_dict["g"].DesktopButton.DefaultOpenAnim(button_start_pos, g_button.TargetPosition);
            _kinfos_dict["h"].DesktopButton.DefaultOpenAnim(button_start_pos, h_button.TargetPosition);
            _kinfos_dict["j"].DesktopButton.DefaultOpenAnim(button_start_pos, j_button.TargetPosition);
            _kinfos_dict["k"].DesktopButton.DefaultOpenAnim(button_start_pos, k_button.TargetPosition);
            _kinfos_dict["l"].DesktopButton.DefaultOpenAnim(button_start_pos, l_button.TargetPosition);
            _kinfos_dict["ö"].DesktopButton.DefaultOpenAnim(button_start_pos, ö_button.TargetPosition);
            _kinfos_dict["ä"].DesktopButton.DefaultOpenAnim(button_start_pos, ä_button.TargetPosition);
            
            _kinfos_dict["y"].DesktopButton.DefaultOpenAnim(button_start_pos, y_button.TargetPosition);
            _kinfos_dict["x"].DesktopButton.DefaultOpenAnim(button_start_pos, x_button.TargetPosition);
            _kinfos_dict["c"].DesktopButton.DefaultOpenAnim(button_start_pos, c_button.TargetPosition);
            _kinfos_dict["v"].DesktopButton.DefaultOpenAnim(button_start_pos, v_button.TargetPosition);
            _kinfos_dict["b"].DesktopButton.DefaultOpenAnim(button_start_pos, b_button.TargetPosition);
            _kinfos_dict["n"].DesktopButton.DefaultOpenAnim(button_start_pos, n_button.TargetPosition);
            _kinfos_dict["m"].DesktopButton.DefaultOpenAnim(button_start_pos, m_button.TargetPosition);

            _kinfos_dict["leer"].DesktopButton.DefaultOpenAnim(button_start_pos, space_button.TargetPosition);
            _kinfos_dict["start"].DesktopButton.DefaultOpenAnim(button_start_pos, enter_button.TargetPosition);
            _kinfos_dict["zurück"].DesktopButton.DefaultOpenAnim(button_start_pos, back_button.TargetPosition);

            _kinfos_dict["texteingabe"].DesktopButton.DefaultOpenAnim(button_start_pos, texteingabe_button.TargetPosition);
        }

        public static Vector GetParentPos(UIElement parent)
        {
            float startx = (float)Canvas.GetLeft(parent);
            float starty = (float)Canvas.GetTop(parent);
            if (float.IsNaN(startx))
                startx = 0;
            if (float.IsNaN(starty))
                starty = 0;
            return new Vector(startx, starty);
        }

        private void CreateButtons(Vector keyboard_top_left_target)
        {
            q_button = CreateButton("q",_key_button_style);
            w_button = CreateButton("w",_key_button_style);
            e_button = CreateButton("e",_key_button_style);
            r_button = CreateButton("r",_key_button_style);
            t_button = CreateButton("t",_key_button_style);
            z_button = CreateButton("z",_key_button_style);
            u_button = CreateButton("u",_key_button_style);
            i_button = CreateButton("i",_key_button_style);
            o_button = CreateButton("o",_key_button_style);
            p_button = CreateButton("p",_key_button_style); 
            ü_button = CreateButton("ü",_key_button_style);
            a_button = CreateButton("a",_key_button_style);
            s_button = CreateButton("s",_key_button_style);
            d_button = CreateButton("d",_key_button_style);
            f_button = CreateButton("f",_key_button_style);
            g_button = CreateButton("g",_key_button_style);
            h_button = CreateButton("h",_key_button_style);
            j_button = CreateButton("j",_key_button_style);
            k_button = CreateButton("k",_key_button_style);
            l_button = CreateButton("l",_key_button_style); 
            ö_button = CreateButton("ö",_key_button_style);
            ä_button = CreateButton("ä",_key_button_style);
            y_button = CreateButton("y",_key_button_style);
            x_button = CreateButton("x",_key_button_style);
            c_button = CreateButton("c",_key_button_style);
            v_button = CreateButton("v",_key_button_style);
            b_button = CreateButton("b",_key_button_style);
            n_button = CreateButton("n",_key_button_style);
            m_button = CreateButton("m",_key_button_style);
            space_button = CreateButton("leer",_key_button_style);
            enter_button = CreateButton("start",_key_button_style);
            back_button = CreateButton("zurück",_key_button_style);

            CreateTexteingabeFeld();

            //this initialized the target pos vectors
            SetKeyBoardTopLeft(keyboard_top_left_target);
        }

        private void CreateTexteingabeFeld()
        {
            float buttom_dim_size = (_button_dim_len + _button_margin);
            float width = buttom_dim_size*11;

            texteingabe_button = CreateButton("texteingabe", _key_button_style);
            Button button = (Button)texteingabe_button.DesktopButton.Button;
            button.Width = width;
            button.Height = _texteingabe_height;
            button.BorderBrush = Brushes.Transparent;
            button.BorderThickness = new Thickness(0);
            button.IsHitTestVisible = false;
            button.Focusable = false;
            button.Style = (Style)_parent_button.FindResource("empty_button_style");
            
            _tb_back = new TextBox();
            SetStyleTb(_tb_back, width, _texteingabe_height, 10, new SolidColorBrush(Color.FromArgb(0x22, 0x00, 0x00, 0x00)));
            _tb_back.Text = "suchbegriff eingeben";

            _tb_front = new TextBox();
            SetStyleTb(_tb_front, width, _texteingabe_height, 10, new SolidColorBrush(Color.FromArgb(0xBB, 0x00, 0x00, 0x00)));
            _tb_front.Text = "";

            Grid grid = new Grid();
            grid.Children.Add(_tb_back);
            grid.Children.Add(_tb_front);
            button.Content = grid;
        }

        private void SetStyleTb(TextBox tb, float width, float height, float margin, Brush foreground)
        {
            tb.Foreground = foreground;
            tb.Background = Brushes.Transparent;
            tb.BorderBrush = Brushes.Transparent;
            tb.BorderThickness = new Thickness(0);
            tb.FocusVisualStyle = null;
            tb.IsHitTestVisible = false;
            tb.Focusable = false;
            tb.Width = width - margin*2;
            tb.Height = height - margin*2;
            tb.TextAlignment = TextAlignment.Left;
            tb.VerticalContentAlignment = VerticalAlignment.Center;
            tb.FontFamily = new FontFamily("Segoe UI");
            tb.FontSize = 40;
        }

        public void SetKeyBoardTopLeft(Vector keyboard_top_left_target)
        {
            _keyboard_top_left_target = keyboard_top_left_target;
            float button_dim_size = _button_dim_len + _button_margin;
            Vector offset = new Vector();

            int x = 0;
            int y = 0;
            q_button.TargetPosition = keyboard_top_left_target + new Vector(x++ * button_dim_size, y * button_dim_size) + offset;
            w_button.TargetPosition = keyboard_top_left_target + new Vector(x++ * button_dim_size, y * button_dim_size) + offset;
            e_button.TargetPosition = keyboard_top_left_target + new Vector(x++ * button_dim_size, y * button_dim_size) + offset;
            r_button.TargetPosition = keyboard_top_left_target + new Vector(x++ * button_dim_size, y * button_dim_size) + offset;
            t_button.TargetPosition = keyboard_top_left_target + new Vector(x++ * button_dim_size, y * button_dim_size) + offset;
            z_button.TargetPosition = keyboard_top_left_target + new Vector(x++ * button_dim_size, y * button_dim_size) + offset;
            u_button.TargetPosition = keyboard_top_left_target + new Vector(x++ * button_dim_size, y * button_dim_size) + offset;
            i_button.TargetPosition = keyboard_top_left_target + new Vector(x++ * button_dim_size, y * button_dim_size) + offset;
            o_button.TargetPosition = keyboard_top_left_target + new Vector(x++ * button_dim_size, y * button_dim_size) + offset;
            p_button.TargetPosition = keyboard_top_left_target + new Vector(x++ * button_dim_size, y * button_dim_size) + offset; 
            ü_button.TargetPosition = keyboard_top_left_target + new Vector(x++ * button_dim_size, y * button_dim_size) + offset;
            y++;
            x = 0;
            offset.X = button_dim_size/2;
            a_button.TargetPosition = keyboard_top_left_target + new Vector(x++ * button_dim_size, y * button_dim_size) + offset;
            s_button.TargetPosition = keyboard_top_left_target + new Vector(x++ * button_dim_size, y * button_dim_size) + offset;
            d_button.TargetPosition = keyboard_top_left_target + new Vector(x++ * button_dim_size, y * button_dim_size) + offset;
            f_button.TargetPosition = keyboard_top_left_target + new Vector(x++ * button_dim_size, y * button_dim_size) + offset;
            g_button.TargetPosition = keyboard_top_left_target + new Vector(x++ * button_dim_size, y * button_dim_size) + offset;
            h_button.TargetPosition = keyboard_top_left_target + new Vector(x++ * button_dim_size, y * button_dim_size) + offset;
            j_button.TargetPosition = keyboard_top_left_target + new Vector(x++ * button_dim_size, y * button_dim_size) + offset;
            k_button.TargetPosition = keyboard_top_left_target + new Vector(x++ * button_dim_size, y * button_dim_size) + offset;
            l_button.TargetPosition = keyboard_top_left_target + new Vector(x++ * button_dim_size, y * button_dim_size) + offset; 
            ö_button.TargetPosition = keyboard_top_left_target + new Vector(x++ * button_dim_size, y * button_dim_size) + offset;
            ä_button.TargetPosition = keyboard_top_left_target + new Vector(x++ * button_dim_size, y * button_dim_size) + offset;
            y++;
            x = 0;
            offset.X = 0;
            y_button.TargetPosition = keyboard_top_left_target + new Vector(x++ * button_dim_size, y * button_dim_size) + offset;
            x_button.TargetPosition = keyboard_top_left_target + new Vector(x++ * button_dim_size, y * button_dim_size) + offset;
            c_button.TargetPosition = keyboard_top_left_target + new Vector(x++ * button_dim_size, y * button_dim_size) + offset;
            v_button.TargetPosition = keyboard_top_left_target + new Vector(x++ * button_dim_size, y * button_dim_size) + offset;
            b_button.TargetPosition = keyboard_top_left_target + new Vector(x++ * button_dim_size, y * button_dim_size) + offset;
            n_button.TargetPosition = keyboard_top_left_target + new Vector(x++ * button_dim_size, y * button_dim_size) + offset;
            m_button.TargetPosition = keyboard_top_left_target + new Vector(x++ * button_dim_size, y * button_dim_size) + offset;
            y++;
            x = 0;
            space_button.TargetPosition = keyboard_top_left_target + new Vector(x++ * button_dim_size, y * button_dim_size); 
            enter_button.TargetPosition = keyboard_top_left_target + new Vector(x++ * button_dim_size, y * button_dim_size);
            back_button.TargetPosition = keyboard_top_left_target + new Vector(x++ * button_dim_size, y * button_dim_size);

            texteingabe_button.TargetPosition = keyboard_top_left_target + new Vector(0,-(30+_texteingabe_height));

            float buttom_dim_size = (_button_dim_len + _button_margin);
            float keyboard_len = buttom_dim_size * 11;

            float space_button_len = _button_dim_len * 6;
            space_button.DesktopButton.Button.Width = space_button_len;
            space_button.TargetPosition += new Vector((keyboard_len - space_button_len) / 2, 0);

            enter_button.DesktopButton.Button.Width = _button_dim_len * 2 - (button_dim_size / 2);
            enter_button.DesktopButton.Button.Height = _button_dim_len * 2;
            enter_button.TargetPosition = _keyboard_top_left_target + new Vector(keyboard_len + button_dim_size / 2, buttom_dim_size);

            back_button.DesktopButton.Button.Width = _button_dim_len * 2;
            back_button.TargetPosition = _keyboard_top_left_target + new Vector(keyboard_len, 0);
        }

        private KeyBoardInfo CreateButton(string name, Style button_style)
        {
            DesktopNormalButton button = new DesktopNormalButton(name+"_key_button", button_style, _keyboard_canvas);
            button.Button.Width = button.Button.Height = _button_dim_len;
            button.Button.Visibility = Visibility.Collapsed;
            button.Button.Content = name;
            button.Button.Click += new System.Windows.RoutedEventHandler(Key_Click);

            KeyBoardInfo kinfo = new KeyBoardInfo();
            kinfo.DesktopButton = button;

            _kinfos.Add(kinfo);
            _kinfos_dict.Add(name, kinfo);
            return kinfo;
        }

        void Key_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            Button button = sender as Button;
            if (button == null)
                return;

            string name = button.Name.Split("_".ToCharArray(), 2)[0];

            if (name == "leer")
                _tb_front.Text += " ";
            else if (name == "zurück")
            {
                if (!string.IsNullOrEmpty(_tb_front.Text))
                    _tb_front.Text = _tb_front.Text.Substring(0, _tb_front.Text.Length - 1);
            }
            else if (name == "start")
            {
                if (EnterClick != null)
                    EnterClick(this, null);
            }
            else
            {
                _tb_front.Text += name;    
            }

            if (string.IsNullOrEmpty(_tb_front.Text))
                _tb_back.Text = "suchbegriff eingeben";
            else
                _tb_back.Text = "";

            if (TextChanged != null)
                TextChanged(this, null);
        }

        public string Text
        {
            get { return _tb_front.Text; }
        }

        #region Attributes
        public event EventHandler EnterClick;
        public event EventHandler TextChanged;

        float _button_dim_len = 100;
        float _button_margin = 3;
        private ToggleButton _parent_button;
        private List<KeyBoardInfo> _kinfos;
        private Dictionary<string, KeyBoardInfo> _kinfos_dict;
        private Canvas _keyboard_canvas;

        private Style _key_button_style;
        private Vector _keyboard_top_left_target;

        private KeyBoardInfo q_button;
        private KeyBoardInfo w_button;
        private KeyBoardInfo e_button;
        private KeyBoardInfo r_button;
        private KeyBoardInfo t_button;
        private KeyBoardInfo z_button;
        private KeyBoardInfo u_button;
        private KeyBoardInfo i_button;
        private KeyBoardInfo o_button;
        private KeyBoardInfo p_button; 
        private KeyBoardInfo ü_button;
        private KeyBoardInfo a_button;
        private KeyBoardInfo s_button;
        private KeyBoardInfo d_button;
        private KeyBoardInfo f_button;
        private KeyBoardInfo g_button;
        private KeyBoardInfo h_button;
        private KeyBoardInfo j_button;
        private KeyBoardInfo k_button;
        private KeyBoardInfo l_button; 
        private KeyBoardInfo ö_button;
        private KeyBoardInfo ä_button;
        private KeyBoardInfo y_button;
        private KeyBoardInfo x_button;
        private KeyBoardInfo c_button;
        private KeyBoardInfo v_button;
        private KeyBoardInfo b_button;
        private KeyBoardInfo n_button;
        private KeyBoardInfo m_button;
        private KeyBoardInfo space_button; 
        private KeyBoardInfo enter_button;
        private KeyBoardInfo back_button;

        private KeyBoardInfo texteingabe_button;
        private TextBox _tb_back,_tb_front;
        private readonly float _texteingabe_height = 90;
        #endregion
    }
}
