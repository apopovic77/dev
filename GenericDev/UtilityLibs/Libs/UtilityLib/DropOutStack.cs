﻿using System;
using System.Collections.Generic;
using Logicx.Utilities;

namespace Logicx.Utility
{
    public class EventArgs<T> : EventArgs
    {
        public EventArgs(T arg)
        {
            Arg = arg;
        }

        public T Arg;
    }

    public class DropOutStack<T> : List<T>
    {
        public DropOutStack(int capacity)
        {
            _capacity = capacity;
        }


        public void Push(T item)
        {
            if (this.Count == _capacity)
            {
                T t = this[0];
                this.RemoveAt(0);
                if (DropOut != null)
                    DropOut(this, new EventArgs<T>(t));
            }
            this.Add(item);
        }

        public bool Empty()
        {
            return this.Count == 0;
        }

        public bool Full()
        {
            return this.Count == _capacity;
        }

        public T Get(int index)
        {
            return this[index];
        }
        public int GetCount()
        {
            return this.Count;
        }

        public T Pop()
        {
            T item = this[this.Count - 1];
            this.RemoveAt(this.Count - 1);

            return item;
        }

        public T Top()
        {
            return this[this.Count - 1];
        }

        public T Bottom()
        {
            return this[0];
        }

        public event EventHandler<EventArgs<T>> DropOut;
        private int _capacity;
    }
}
