﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Logicx.WpfUtility.CustomControls.TouchButton
{
    public class TouchButton : Button
    {
        #region Constants
        #endregion

        #region Construction and Initialization
        public TouchButton()
        {
            this.StylusDown += new System.Windows.Input.StylusDownEventHandler(TouchButton_StylusDown);
            this.StylusUp += new System.Windows.Input.StylusEventHandler(TouchButton_StylusUp);
            this.StylusLeave += new System.Windows.Input.StylusEventHandler(TouchButton_StylusLeave);
        }

        void TouchButton_StylusLeave(object sender, System.Windows.Input.StylusEventArgs e)
        {
            _stylus_down_id = int.MinValue;
            Stylus.Capture(this, CaptureMode.None);
        }

        void TouchButton_StylusUp(object sender, System.Windows.Input.StylusEventArgs e)
        {
            Stylus.Capture(this, CaptureMode.None);

            if (!CheckOrigin(e.GetPosition(this)))
                return;

            if(_stylus_down_id == e.StylusDevice.Id)
            {
                _stylus_down_id = int.MinValue;
                e.Handled = true;
                OnClick();
            }
        }

        void TouchButton_StylusDown(object sender, System.Windows.Input.StylusDownEventArgs e)
        {
            if (!CheckOrigin(e.GetPosition(this)))
            {
                Stylus.Capture(this, CaptureMode.None);
                return;
            }

            _stylus_down_id = e.StylusDevice.Id;
            //Focus();
            e.Handled = true;
        }

        private bool CheckOrigin(Point p)
        {
            if (p.X < 0 || p.Y < 0 || p.X > this.ActualWidth || p.Y > this.ActualHeight)
                return false;

            return true;
        }
        #endregion

        #region Operations
        #endregion

        #region Event Handlers
        #endregion

        #region Properties
        #endregion

        #region Dependency Properties
        #endregion

        #region Events
        #endregion

        #region Routed Events
        #endregion

        #region Attributes
        private int _stylus_down_id = int.MinValue;
        #endregion

        #region Tests
        #endregion
    }
}
