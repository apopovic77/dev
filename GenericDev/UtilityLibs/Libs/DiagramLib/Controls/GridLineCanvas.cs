﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Shapes;
using Logicx.DiagramLib.Scales;
using Logicx.DiagramLib.ViewConfig;

namespace Logicx.DiagramLib.Controls
{
    public class GridLineCanvas : Canvas
    {
        #region Constants
        #endregion

        #region Construction and Initialization
        public GridLineCanvas()
        {
            SizeChanged += new SizeChangedEventHandler(GridLineCanvas_SizeChanged);
        }
        #endregion

        #region Operations
        public void ReCreateGridLines()
        {
            //this.Children.Clear();
            RecycleGridLines();

            if (GridLineVisibilityMode == ViewConfig.GridLineVisibility.None)
                return;

            if(GridLineVisibilityMode == ViewConfig.GridLineVisibility.Vertical || GridLineVisibilityMode == ViewConfig.GridLineVisibility.All)
            {
                if(XAxisControl != null)
                {
                    // horizontal lines
                    List<double> grid_line_positions = XAxisControl.GetGridLinePositions();

                    if (grid_line_positions.Count > 0)
                    {
                        for (int i = 0; i < grid_line_positions.Count; i++)
                        {
                                Line grid_line = GetLine();
                                //Debug.WriteLine("draw vertical line on " + grid_line_positions[i]);
                                grid_line.X1 = grid_line_positions[i];
                                grid_line.X2 = grid_line_positions[i];
                                grid_line.Y1 = 0;

                                Binding height_binding = new Binding("ActualHeight");
                                height_binding.Source = this;
                                grid_line.SetBinding(Line.Y2Property, height_binding);

                                Children.Add(grid_line);
                        }
                    }
                }
            }

            if (GridLineVisibilityMode == ViewConfig.GridLineVisibility.Horizontal || GridLineVisibilityMode == ViewConfig.GridLineVisibility.All)
            {
                if (YAxisControl != null)
                {
                    // vertical lines

                    List<double> grid_line_positions = YAxisControl.GetGridLinePositions();

                    if (grid_line_positions.Count > 0)
                    {
                         for (int i = 0; i < grid_line_positions.Count; i++)
                        {
                                Line grid_line = GetLine();

                                grid_line.X1 = 0;
                                grid_line.Y1 = grid_line_positions[i];
                                grid_line.Y2 = grid_line_positions[i];

                                Binding width_binding = new Binding("ActualWidth");
                                width_binding.Source = this;
                                grid_line.SetBinding(Line.X2Property, width_binding);

                                Children.Add(grid_line);
                        }
                    }
                }
            }
        }

        private void RecycleGridLines()
        {
            _recycled_lines.AddRange(Children.Cast<Line>().ToList());
            foreach(Line curLine in _recycled_lines)
            {
                BindingOperations.ClearBinding(curLine, Line.X2Property);
                BindingOperations.ClearBinding(curLine, Line.Y2Property);
            }
            Children.Clear();
        }

        private Line GetLine()
        {
            Line retLine = null;
            if(_recycled_lines.Count > 0)
            {
                retLine = _recycled_lines[0];
                _recycled_lines.Remove(retLine);
            }
            else
            {
                retLine = new Line();

                Binding style_binding = new Binding("GridLineStyle");
                style_binding.Source = this;

                retLine.SetBinding(StyleProperty, style_binding);
            }

            return retLine;
        }
        #endregion

        #region Event Handlers
        private static void OnGridLineVisibilityChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if(d is GridLineCanvas)
            {
                ((GridLineCanvas) d).ReCreateGridLines();
            }
        }

        private void XScalingChanged(object sender, EventArgs e)
        {
            ReCreateGridLines();
        }

        private void YScalingChanged(object sender, EventArgs e)
        {
            ReCreateGridLines();
        }

        void GridLineCanvas_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (Children.Count == 0)
                ReCreateGridLines();
        }
        #endregion

        #region Properties
        public ScaleControlBase XAxisControl
        {
            get
            {
                return _x_axis_control;
            }
            set
            {
                if(_x_axis_control != value)
                {
                    if (_x_axis_control != null)
                        _x_axis_control.ScaleingChanged -= XScalingChanged;

                    _x_axis_control = value;

                    if (_x_axis_control != null)
                        _x_axis_control.ScaleingChanged += XScalingChanged;
                }
            }
        }

        public ScaleControlBase YAxisControl
        {
            get
            {
                return _y_axis_control;
            }
            set
            {
                if (_y_axis_control != value)
                {
                    if (_y_axis_control != null)
                        _y_axis_control.ScaleingChanged -= YScalingChanged;

                    _y_axis_control = value;

                    if (_y_axis_control != null)
                        _y_axis_control.ScaleingChanged += YScalingChanged;
                }
            }
        }
        #endregion

        #region Dependency Properties

        public GridLineVisibility GridLineVisibilityMode    
        {
            get { return (GridLineVisibility)GetValue(GridLineVisibilityModeProperty); }
            set { SetValue(GridLineVisibilityModeProperty, value); }
        }

        // Using a DependencyProperty as the backing store for MyProperty.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty GridLineVisibilityModeProperty =
            DependencyProperty.Register("GridLineVisibilityMode", typeof(GridLineVisibility), typeof(GridLineCanvas), new UIPropertyMetadata(GridLineVisibility.None, new PropertyChangedCallback(GridLineCanvas.OnGridLineVisibilityChanged)));

        public Style GridLineStyle
        {
            get { return (Style)GetValue(GridLineStyleProperty); }
            set { SetValue(GridLineStyleProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ScaleInfoTextStyle.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty GridLineStyleProperty =
            DependencyProperty.Register("GridLineStyle", typeof(Style), typeof(GridLineCanvas), new UIPropertyMetadata(null));
        #endregion

        #region Events
        #endregion

        #region Routed Events
        #endregion

        #region Attributes
        private ScaleControlBase _x_axis_control = null;
        private ScaleControlBase _y_axis_control = null;

        private List<Line> _recycled_lines = new List<Line>();
        #endregion

        #region Tests
        #endregion
    }
}
