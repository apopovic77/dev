﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace Awanto.WhatsAppCore.Utilities
{
    public class ConsoleLoggerProvider : ILoggerProvider
    {
        public ILogger CreateLogger(string category_name)
        {
            return new ConsoleLogger();
        }

        public void Dispose()
        { }

        private class ConsoleLogger : ILogger
        {
            public bool IsEnabled(LogLevel log_level)
            {
                return true;
            }

            public void Log<TState>(LogLevel log_level, EventId event_id, TState state, Exception exception, Func<TState, Exception, string> formatter)
            {
                //File.AppendAllText(@"C:\temp\log.txt", formatter(state, exception));
                Console.WriteLine(formatter(state, exception));
            }

            public IDisposable BeginScope<TState>(TState state)
            {
                return null;
            }
        }
    }
}
