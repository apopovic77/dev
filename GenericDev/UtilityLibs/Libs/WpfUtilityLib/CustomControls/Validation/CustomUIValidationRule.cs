﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;

namespace Logicx.WpfUtility.CustomControls.Validation
{
    /// <summary>
    /// A validation rule which markes custom Ui validation
    /// </summary>
    public class CustomUIValidationRule : ValidationRule
    {
        #region Operations
        /// <summary>
        /// Validation
        /// </summary>
        /// <param name="value"></param>
        /// <param name="cultureInfo"></param>
        /// <returns></returns>
        public override ValidationResult Validate(object value, System.Globalization.CultureInfo cultureInfo)
        {
            return new ValidationResult(true, "");
        }

        /// <summary>
        /// Gets a new rule instance
        /// </summary>
        public static ValidationRule NewRule
        {
            get { return new CustomUIValidationRule(); }
        }
        #endregion
    }
}
