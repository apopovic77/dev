﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BankLib.Data;

namespace BankLib.Gruppe
{
    public class BgZuoFactory
    {
        public static BgZuo LoadFromStream(BinaryReader reader)
        {
            string subclass_type = BinaryHelper.ReadString(reader);

            switch (subclass_type)
            {
                case "BgZuoExakt":
                {
                    BgZuoExakt bg_zuo = new BgZuoExakt();
                    bg_zuo.LoadFromStream(reader);
                    return bg_zuo;
                }
                case "BgZuoIndexOf":
                {
                    BgZuoIndexOf bg_zuo = new BgZuoIndexOf();
                    bg_zuo.LoadFromStream(reader);
                    return bg_zuo;
                }
                case "BgZuoUnknownIncome":
                {
                    BgZuoUnknownIncome bg_zuo = new BgZuoUnknownIncome();
                    bg_zuo.LoadFromStream(reader);
                    return bg_zuo;
                }
                case "BgZuoUnknownCost":
                {
                    BgZuoUnknownCost bg_zuo = new BgZuoUnknownCost();
                    bg_zuo.LoadFromStream(reader);
                    return bg_zuo;
                }
                case "BgZuoBuchungsType":
                {
                    BgZuoBuchungsType bg_zuo = new BgZuoBuchungsType();
                    bg_zuo.LoadFromStream(reader);
                    return bg_zuo;
                }
                default:
                    throw new Exception("BgZuoFactory, cannot create BgZuo, Subclass Type unknown");
            }
        }

        public static BgZuo CreateBgZuo(string buchungs_type, string umsatz_type, string comp_type, string search_string)
        {
            switch (comp_type)
            {
                case "Exact":
                case "":
                case null:
                    BgZuoExakt bg_zuo_exakt = new BgZuoExakt();
                    bg_zuo_exakt.BuchungsType = buchungs_type;
                    bg_zuo_exakt.UmsatzType = umsatz_type;
                    return bg_zuo_exakt;
                case "IndexOf":
                    if(string.IsNullOrEmpty(search_string))
                        throw new ArgumentException("cannot create index of bgzuo, es muss ein searchstring angegeben werden");
                    BgZuoIndexOf bg_zuo_index_of = new BgZuoIndexOf();
                    bg_zuo_index_of.BuchungsType = buchungs_type;
                    bg_zuo_index_of.UmsatzType = umsatz_type;
                    bg_zuo_index_of.SearchString = search_string;
                    return bg_zuo_index_of;
                default:
                    throw new Exception("BuchungsGruppenZuoTyp unbekannt"+comp_type);
                    break;
            }

        }


        public static BgZuo CreateBgZuo(RawGroupInfo rgi)
        {
            return CreateBgZuo(rgi.BuchungsType, rgi.UmsatzType, rgi.CompareType, rgi.SearchString);
        }
    }

}
