using System;
using System.Collections;
using MathLib;


namespace GeoLib
{
	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	public class Line
	{

		public Vector2d this[int index]{
		   get{
			   return (Vector2d)vertices[index];
		   }
		   set{
				vertices[index] = value;	
			}
		}



		/// <summary>
		/// gibt die anzahl der Vertexte in der Line zur�ck
		/// </summary>
		public int Length{
			get{
				return vertices.Count;			
			}
		}

		protected double m_linelength=-1;
		public double GetLineLength(){
			if(m_linelength==-1)
				m_linelength = GetLineLength(0,Length-1);			
			return m_linelength;
		}

		/// <summary>
		/// gibt die l�nge der line zur�ck
		/// </summary>
		/// <param name="start">vertex von dem aus gestartet wird</param>
		/// <param name="end">vertex bei dem aufgeh�rt wird</param>
		/// <returns></returns>
		public virtual double GetLineLength(int start, int end){
			double len = 0;
			if(start>end){
				int buffer = start;
				start = end;
				end = buffer;
			}
			for(int i=start;i<end;i++){
				Vector2d v = (Vector2d)vertices[i] - (Vector2d)vertices[i+1];
				len += v.GetLen();
			}
			return len;
		}

		public Edge[] GetEdges(){
			if(this.Length<2)
				return null;
			Edge[] edges = new Edge[this.Length-1];
			for(int i=0;i<this.Length-1;i++){
				Edge e = new Edge((Vector2d)vertices[i],(Vector2d)vertices[i+1]);
				edges[i] = e;
			}
			return edges;
	    }
		/// <summary>
		/// gibt die l�nge eines bestimmten streckenabschnitts zur�ck
		/// </summary>
		/// <param name="start"></param>
		/// <param name="end"></param>
		/// <returns></returns>
		public virtual double GetLengthOfSection(double start, double end){
			if(CheckSectionInLine(ref start,ref end))
                throw new GeoLibException("Section points for start and end must be on route!");

			//vertausche start ende wenn start weiter hinten liegt
			if(start > end)
			{
				GeoFactory.TransposeStartEndPoint(ref start,ref end);
			}

			//wenns wirklick gleich ist dann f�g einen kleinen unterschied ein
			if(start == end)
			{
				return 0;
			}			

			return end-start;
		}

		/// <summary>
		/// gibt die distanz zur�ck die man minimal braucht um eine verbindung mit der line herzustellen
		/// im regel fall ist dies der k�rzeste orthogonal vektor zu einer line
		/// </summary>
		/// <param name="v">der vektor f�r den es die distanz zu ermitteln gilt</param>
		/// <returns>die l�nge der min zur�cklegenden wegs</returns>
		public double GetShortestDistanceToLine(Vector2d xs)
		{
			//umwegcalc
			double c = 0;
			Vector2d t_vec;
			int xs_shortest_knoten = 0;
			Vector2d xs_shortest_umweg = xs - this[0];
			Vector2d xs_shortest_wegabzweigung = new Vector2d(0,0);
			double xs_len_shortest_umweg = xs_shortest_umweg.GetLen();

			for(int i=0;i<Length-1;i++)
			{
				Vector2d wegteil = this[i+1]-this[i];
				double wegteil_len = wegteil.GetLen();
				t_vec = xs-this[i];
				c = t_vec.GetSkalarProduct(wegteil) / wegteil.GetSkalarProduct();
				double curr_wegteil_len = (wegteil * c).GetLen();
				if(c>0 && curr_wegteil_len <= wegteil_len)
				{
					Vector2d v_wegbisabzweigung = wegteil * c;
					Vector2d v_umweg = t_vec - v_wegbisabzweigung;
					double v_umweg_len = v_umweg.GetLen();
					if(v_umweg_len < xs_len_shortest_umweg )
					{
						xs_shortest_umweg = v_umweg;
						xs_len_shortest_umweg = v_umweg_len;
						xs_shortest_knoten = i;
						xs_shortest_wegabzweigung = v_wegbisabzweigung;
					}
				}
			}
			double len_t_vec = 0;
			for(int i=0;i<this.Length;i++)
			{
				t_vec =  xs - this[i];
				len_t_vec = t_vec.GetLen();
				if(len_t_vec < xs_len_shortest_umweg)
				{
					xs_shortest_umweg = t_vec;
					xs_shortest_knoten = i;
					xs_len_shortest_umweg = len_t_vec;
					xs_shortest_wegabzweigung = new Vector2d(0,0);
				}
			}
			return ((this[xs_shortest_knoten]+xs_shortest_wegabzweigung) - xs).GetLen();
		}

		protected bool CheckSectionInLine(ref double start,ref double end){
			//check ob start und end > 0 sind, da sich start bzw. ende auf der route befinden m�ssen
			if(start < 0 || end < 0 || start > GetLineLength() || end > GetLineLength())
				return false;
			return true;
		}

		
		/// <summary>
		/// rationalisieren im sinne von vertex zusammenfassen, �ber eine regressinsgerade werden 
		/// rationalisierungs_faktor viele vertexe zu 2 vertexen zusammengefasst
		/// </summary>
		/// <param name="rationalisierungs_faktor"></param>
		/// <param name="rounds"></param>
		public void Rationalize(int rationalisierungs_faktor,int rounds){
			vertices = GetRationalizedVertices(vertices,rationalisierungs_faktor,rounds);
		}
		public void Rationalize(int rationalisierungs_faktor){
			vertices = GetRationalizedVertices(vertices,rationalisierungs_faktor,1);
		}

		private ArrayList GetRationalizedVertices(ArrayList vertices_in,int rationalisierungs_faktor){
			ArrayList poly_rationlisiert = new ArrayList();
			poly_rationlisiert.Add(vertices_in[0]);
			poly_rationlisiert.Add(vertices_in[1]);
			for(int i = 2;i<vertices_in.Count-2;i+=rationalisierungs_faktor)
			{
				int count_vert = rationalisierungs_faktor;
				if(i+rationalisierungs_faktor >= vertices_in.Count-2)
					count_vert = vertices_in.Count-2-i;
				Vector2d[] vertices = new Vector2d[count_vert];
				for(int j=0;j<count_vert;j++)
				{
					vertices[j] = (Vector2d)vertices_in[i+j];
				}
				try
				{
					Regressionsgerade rg = new Regressionsgerade(vertices);
					double y_beg = rg.GetY(vertices[0].X);
					double y_end = rg.GetY(vertices[vertices.Length-1].X);

					if ( Double.IsNaN(y_beg) || Double.IsNaN(y_end))
						throw new Exception("add normal");
					
					Vector2d V1_toadd = new Vector2d(vertices[0].X,y_beg);
					Vector2d V2_toadd = new Vector2d(vertices[vertices.Length-1].X,y_end);
					

					if((Vector2d)poly_rationlisiert[poly_rationlisiert.Count-1] != V1_toadd)
					{
						double MELT_IF_SMALLER = 1;
						Vector2d v_prev = (Vector2d)poly_rationlisiert[poly_rationlisiert.Count-1];
						double len = (v_prev-V1_toadd).GetLen();
						if(len < MELT_IF_SMALLER)
						{
							Vector2d v_new  = v_prev + (V1_toadd-v_prev).GetNorm()*(len/2);
							poly_rationlisiert[poly_rationlisiert.Count-1] = v_new;
						}
						else
						{
							poly_rationlisiert.Add(V1_toadd);
						}
					}
					if((Vector2d)poly_rationlisiert[poly_rationlisiert.Count-1] != V2_toadd)
					{
						poly_rationlisiert.Add(V2_toadd);
					}
				}
				catch
				{
					for(int j=0;j<count_vert;j++)
						poly_rationlisiert.Add(vertices_in[i+j]);
				}
			}
			poly_rationlisiert.Add(vertices_in[vertices_in.Count-2]);
			poly_rationlisiert.Add(vertices_in[vertices_in.Count-1]);
			
			return poly_rationlisiert;
		}

		/// <summary>
		/// legt �ber die in rationalisierungs_faktor festgelegt anzahl von vertexen eine regressiongerade und verschmilz somit
		/// von rationalisierungs_faktor vertices auf 2 vertices zusammen
		/// </summary>
		/// <param name="vertices"></param>
		/// <param name="rationalisierungs_faktor">5 bedeutet 5 werden zu zwei vertexen verschmolzen</param>
		/// <param name="rounds">man kann den algorithmus �fter dr�berlaufen lassen, man rationalisiert daurch in mehreren schritten
		/// was der untershcieden zwischen 2 mal mit rf 3 oder 1 mal mit rf 6 ist kann ich nicht sagen</param>
		/// <returns></returns>
		public ArrayList GetRationalizedVertices(ArrayList vertices,int rationalisierungs_faktor,int rounds)
		{
			rounds--;
			ArrayList rationalisiert = GetRationalizedVertices(vertices,rationalisierungs_faktor);
			for(int i=0;i<rounds;i++){
				rationalisiert = GetRationalizedVertices(rationalisiert,rationalisierungs_faktor);
			}
			return rationalisiert;
		}

		/// <summary>
		/// gibt den linien vertex sprich kontenpunkt auf welcher auf welcher sich der point befindet zur�ck
		/// </summary>
		/// <param name="pointpos_on_line"></param>
		/// <returns></returns>
		protected int GetLineVertexOfPoint(double pointpos_on_line){
			double len = 0;
			for(int i=0;i<vertices.Count-1;i++)
			{
				Vector2d v = (Vector2d)vertices[i] - (Vector2d)vertices[i+1];
				len += v.GetLen();
				if(len > pointpos_on_line)
					return i;
			}			
			return -1;
		}

		/// <summary>
		/// vertices holds all vertices of the polygon
		/// </summary>
		protected ArrayList vertices = new ArrayList();

		protected Vector2d[] vertices_arr = null;

		public Line(){}
		public Line(string linestringWKT){
			WKT = linestringWKT;
		}
		public Line(Vector2d[] v_arr){
			ResetPolygon(v_arr);
		}

		public void Add(Object[] o_arr){
			try
			{
				for(int i=0;i<o_arr.Length;i++)
					vertices.Add((Vector2d)o_arr[i]);
			}
			catch{
			}
		}

        public GeoLib.Rectangle GetBoundingBox(Vector2d mostleft,Vector2d mostright,Vector2d mosttop,Vector2d mostbottom) {

            for (int i = 1; i < this.Length; i++) { 
                if(this[i].X < mostleft.X)
                    mostleft = this[i];
                if(this[i].X > mostright.X)
                    mostright = this[i];
                if(this[i].Y > mosttop.Y)
                    mosttop = this[i];
                if(this[i].Y < mostbottom.Y)
                    mostbottom = this[i];
            }

            Rectangle box = new Rectangle(
               new Vector2d(mostleft.X,mosttop.Y),
               new Vector2d(mostright.X, mosttop.Y),
               new Vector2d(mostright.X, mostbottom.Y),
               new Vector2d(mostleft.X, mostbottom.Y)
            );


            return box;
        }
        public GeoLib.Rectangle GetBoundingBox()
        {
            Vector2d mostleft = this[0];
            Vector2d mostright = this[0];
            Vector2d mosttop = this[0];
            Vector2d mostbottom = this[0];

            return GetBoundingBox(mostleft,mostright,mosttop,mostbottom);
        }
        public GeoLib.Rectangle GetBoundingBox(GeoLib.Rectangle box_pregiven)
        {
            Vector2d mostleft = box_pregiven.Mostleft;
            Vector2d mostright = box_pregiven.Mostright;
            Vector2d mosttop = box_pregiven.Mosttop;
            Vector2d mostbottom = box_pregiven.Mostbottom;

            return GetBoundingBox(mostleft, mostright, mosttop, mostbottom);
        }
		public void Add(Vector2d v){
			vertices.Add(v);
			//reset varray to null, a new polygon is available
			vertices_arr = null;
		}
		
		public void ResetPolygon(Vector2d[] v_arr){
			for(int i=0;i<v_arr.Length;i++)
				vertices.Add(v_arr[i]);
			//reset varray to null, a new polygon is available
			vertices_arr = null;
		}
		public void Add(Vector2d[] v_arr)
		{
			if(v_arr!=null)
			{
				for(int i=0;i<v_arr.Length;i++)
					vertices.Add(v_arr[i]);
			}
		}

		public void Clear(){
			vertices.Clear();
			vertices_arr = null;
		}

        public Vector2d[] GetTriangledShapeArroundLine(double margin)
        {
            ArrayList vertices_res = new ArrayList();
            //do for all edges in line
            for (int i = 0; i < this.Length - 1; i++)
            {
                Vector2d v = this[i + 1] - this[i];

                Vector2d ortho2 = v.GetOrthogonal2().GetNorm() * margin;

                vertices_res.Add(this[i] + ortho2);
                vertices_res.Add(this[i] + ortho2.GetMirror());

                vertices_res.Add(this[i + 1] + ortho2);
                vertices_res.Add(this[i + 1] + ortho2.GetMirror());
            }

            Array arr_ret = vertices_res.ToArray(typeof(Vector2d));
            return (Vector2d[])arr_ret;
        }

		protected virtual string CreateLineStringT(){
			string s = "";
			//serialize the arraylist
			for(int i=0;i<vertices.Count;i++){
				Vector2d v = (Vector2d)vertices[i];
				//XXX das comma wird manchmal als , transformiert
				//da aber �berall der . als trennzeichen genommen wird ist das a prob
				//todo: warum wird das ding net auf . (punkt) transformiert
				s += v.X.ToString().Replace(",",".") + " " + v.Y.ToString().Replace(",",".");
				if(i < vertices.Count-1)
					s += ",";
			}
			//return serialized poly
			return s;
		}

		public void ParseLineStringT(string poly_s){
			string[] v_sarray = poly_s.Split(",".ToCharArray());
			for(int i=0;i<v_sarray.Length;i++){
				string[] xy =v_sarray[i].Split(" ".ToCharArray());
				Vector2d v = new Vector2d(0,0);
				v.X = Convert.ToDouble(xy[0].Replace(".",","));
				v.Y = Convert.ToDouble(xy[1].Replace(".",","));
				Add(v);
			}			
		}
		public Vector2d[] GetVertices(){
			CreateArray();
			return vertices_arr;
		}

		public virtual string WKT{
			get{
				//return a serialized poly surrounded with the WKT format
				string polyWKT = "LineString(";
				polyWKT += CreateLineStringT();
				polyWKT += ")";
				return polyWKT;
			}
			set{
				try{
					int beg = value.IndexOf("(")+1;
					if(this is Polygon){
						beg++;
						value = value.Substring(beg,value.Length-beg-2);
					}else{
						value = value.Substring(beg,value.Length-beg-1);
					}
					ParseLineStringT(value);
				}catch{
					vertices.Clear();
				}
				//reset varray to null, a new polygon is available
				vertices_arr = null;
			}
		}

		public string T{
			get{
				return CreateLineStringT();
			}
			set{
				try{
					ParseLineStringT(value);
				}catch(Exception e){
					vertices.Clear();
					throw e;
				}
				//reset varray to null, a new polygon is available
				vertices_arr = null;
			}
		}
		/// <summary>
		/// create vertices array for faster traversation, only if not already instantiaed
		/// </summary>
		protected void CreateArray(){
			if(vertices_arr==null){
				object[] obj_arr = vertices.ToArray();
				vertices_arr = new Vector2d[obj_arr.Length];
				for(int i=0;i<obj_arr.Length;i++)
					vertices_arr[i] = (Vector2d)obj_arr[i];
			}
		}

		public Vector2d[] GetMBR(){
			CreateArray();
			//create minimum bounding rectangle
			Vector2d a = new Vector2d(0,0); 
			a.X = vertices_arr[0].X;
			a.Y = vertices_arr[0].Y;
			Vector2d b = new Vector2d(0,0); 
			b.X = a.X;
			b.Y = b.Y;
			for(int i=1;i<vertices_arr.Length;i++){
				//minX1
				if(a.X > vertices_arr[i].X)
					a.X = vertices_arr[i].X;
				//maxY1
				if(a.Y < vertices_arr[i].Y)
					a.Y = vertices_arr[i].Y;
				//maxX2
				if(b.X < vertices_arr[i].X)
					b.X = vertices_arr[i].X;
				//minY2
				if(b.Y > vertices_arr[i].Y)
					b.Y = vertices_arr[i].Y;
			}

			//return the rect as an vector array
			//vertex[0] = minX maxY
			//vertex[1] = maxX minY
			Vector2d[] ret = new Vector2d[2];
			ret[0] = a;
			ret[1] = b;
			return ret;
		}
		public Vector2d[] GetRectAroundLine(double radius){
			CreateArray();

			//create minimum bounding rectangle
			Vector2d[] v = GetMBR();
			Vector2d dist = v[1]-v[0];

			Vector2d dist_normiert = dist.GetNorm();
			Vector2d dist_normiert_gespiegelt = dist_normiert.GetMirror();
			Vector2d orthogonall = dist_normiert_gespiegelt.GetOrthogonal1();
			Vector2d orthogonal2 = dist_normiert_gespiegelt.GetOrthogonal2();

			Vector2d[] poly = new Vector2d[5];
			poly[0].X = v[0].X +  (dist_normiert_gespiegelt.X*radius) + (orthogonall.X*radius);
			poly[0].Y = v[0].Y +  (dist_normiert_gespiegelt.Y*radius) + (orthogonall.Y*radius);

			poly[1].X = v[0].X +  (dist_normiert_gespiegelt.X*radius) + (orthogonal2.X*radius);
			poly[1].Y = v[0].Y +  (dist_normiert_gespiegelt.Y*radius) + (orthogonal2.Y*radius);

			poly[2].X = v[1].X +  (dist_normiert.X*radius) + (orthogonal2.X*radius);
			poly[2].Y = v[1].Y +  (dist_normiert.Y*radius) + (orthogonal2.Y*radius);

			poly[3].X = v[1].X +  (dist_normiert.X*radius) + (orthogonall.X*radius);
			poly[3].Y = v[1].Y +  (dist_normiert.Y*radius) + (orthogonall.Y*radius);

			poly[4].X = poly[0].X;
			poly[4].Y = poly[0].Y;

			return poly;
		}


		public Vector2d[] GetPolygonArrayAroundLineSimple(double radius){
			return GetPolygonAroundLineSimple(radius).ToArray();
		}
		public Vector2d[] GetPolygonArrayAroundLineSimple(double radius,double slope_rad){
			return GetPolygonAroundLineSimple(radius,slope_rad).ToArray();
		}

		public Polygon GetPolygonAroundLineSimple(double radius){
			return GetPolygonAroundLineSimple(radius,0);
		}

//		public Polygon GetPolygonAroundLine(double radius,double slope_rad){
//			//create an array from arraylist
//			CreateArray();
//
//			//10% gef�lle pro connection point
//			double rad_here = radius;
//			Polygon poly = new Polygon();
//
//			//first hafcircle
//			Vector vStart = vertices_arr[1]-vertices_arr[0];
//			Vector vStart_norm = vStart.GetNorm();
//			Vector vStart_mirror = vStart_norm.GetMirror();
//			Vector buffer = vStart_norm.GetOrthogonal1();
//			Vector start = (buffer * rad_here) + vertices_arr[0];
//			poly.Add(((buffer*rad_here)+vertices_arr[0]));
//			poly.Add(((((Vector)(buffer+vStart_mirror)).GetNorm()*rad_here)+vertices_arr[0]));
//			poly.Add(((vStart_mirror*rad_here)+vertices_arr[0]));
//			buffer = vStart_norm.GetOrthogonal2();
//			poly.Add(((((Vector)(buffer+vStart_mirror)).GetNorm()*rad_here)+vertices_arr[0]));
//			poly.Add(((buffer*rad_here)+vertices_arr[0]));
//			
//			//connectionPoints top
//			Vector[] connections = null;
//			if((vertices_arr.Length-2)>0)
//				connections = new Vector[vertices_arr.Length-2];
//			for(int i=1;i<vertices_arr.Length-1;i++){
//				Vector neighb1 = (vertices_arr[i-1]-vertices_arr[i]);
//				Vector neighb2 = (vertices_arr[i+1]-vertices_arr[i]);
//				rad_here = radius - ((radius*(i*slope_rad)) / 100);
//				Vector[] conns = GetConnection(neighb1,neighb2,rad_here);
//				poly.Add((conns[0]+vertices_arr[i]));
//				conns[1] = (conns[1]+vertices_arr[i]);
//				connections[i-1] = conns[1];
//			}
//
//			//last halfcircle
//			rad_here = radius - ((radius*((vertices_arr.Length-1)*slope_rad)) / 100);
//			Vector vEnd = (vertices_arr[vertices_arr.Length-1]-vertices_arr[vertices_arr.Length-2]);
//			Vector vEnd_norm = vEnd.GetNorm();
//			//Vector vStart_mirror = GetMirror(vStart_norm);
//			buffer = vEnd_norm.GetOrthogonal2();
//			poly.Add(((buffer*rad_here)+vertices_arr[vertices_arr.Length-1]));
//			poly.Add(((((Vector)(buffer+vEnd_norm)).GetNorm()*rad_here)+vertices_arr[vertices_arr.Length-1]));
//			poly.Add(((vEnd_norm*rad_here)+vertices_arr[vertices_arr.Length-1]));
//			buffer = vEnd_norm.GetOrthogonal1();
//			poly.Add((((Vector)((buffer+vEnd_norm)).GetNorm()*rad_here)+vertices_arr[vertices_arr.Length-1]));
//			poly.Add(((buffer*rad_here)+vertices_arr[vertices_arr.Length-1]));
//
//			//connectionPoint bottom
//			if(connections!=null)
//				for(int i=connections.Length-1;i>=0;i--)
//					poly.Add(connections[i]);
//
//			//close poly
//			poly.Add(start);
//			return poly;
//		}
		public Polygon GetPolygonAroundLineSimple(double radius,double slope_rad)
		{
			//create an array from arraylist
			CreateArray();

			//10% gef�lle pro connection point
			double rad_here = radius;
			Polygon poly = new Polygon();

			//first hafcircle
			Vector2d vStart = vertices_arr[1]-vertices_arr[0];
			Vector2d vStart_norm = vStart.GetNorm();
			Vector2d vStart_mirror = vStart_norm.GetMirror();
			Vector2d buffer = vStart_norm.GetOrthogonal1();
			Vector2d start = (buffer * rad_here) + vertices_arr[0];
			poly.Add(((buffer*rad_here)+vertices_arr[0]));
			poly.Add(((((Vector2d)(buffer+vStart_mirror)).GetNorm()*rad_here)+vertices_arr[0]));
			poly.Add(((vStart_mirror*rad_here)+vertices_arr[0]));
			buffer = vStart_norm.GetOrthogonal2();
			poly.Add(((((Vector2d)(buffer+vStart_mirror)).GetNorm()*rad_here)+vertices_arr[0]));
			poly.Add(((buffer*rad_here)+vertices_arr[0]));
			
			//connectionPoints top
			Vector2d[] connections = null;
			if((vertices_arr.Length-2)>0)
				connections = new Vector2d[vertices_arr.Length-2];
			for(int i=1;i<vertices_arr.Length-1;i++)
			{
				Vector2d neighb1 = (vertices_arr[i-1]-vertices_arr[i]);
				Vector2d neighb2 = (vertices_arr[i+1]-vertices_arr[i]);
				rad_here = radius - ((radius*(i*slope_rad)) / 100);
				Vector2d[] conns = GetConnection(neighb1,neighb2,rad_here);
				poly.Add((conns[0]+vertices_arr[i]));
				conns[1] = (conns[1]+vertices_arr[i]);
				connections[i-1] = conns[1];
			}

			//last halfcircle
			rad_here = radius - ((radius*((vertices_arr.Length-1)*slope_rad)) / 100);
			Vector2d vEnd = (vertices_arr[vertices_arr.Length-1]-vertices_arr[vertices_arr.Length-2]);
			Vector2d vEnd_norm = vEnd.GetNorm();
			//Vector vStart_mirror = GetMirror(vStart_norm);
			buffer = vEnd_norm.GetOrthogonal2();
			poly.Add(((buffer*rad_here)+vertices_arr[vertices_arr.Length-1]));
			poly.Add(((((Vector2d)(buffer+vEnd_norm)).GetNorm()*rad_here)+vertices_arr[vertices_arr.Length-1]));
			poly.Add(((vEnd_norm*rad_here)+vertices_arr[vertices_arr.Length-1]));
			buffer = vEnd_norm.GetOrthogonal1();
			poly.Add((((Vector2d)((buffer+vEnd_norm)).GetNorm()*rad_here)+vertices_arr[vertices_arr.Length-1]));
			poly.Add(((buffer*rad_here)+vertices_arr[vertices_arr.Length-1]));

			//connectionPoint bottom
			if(connections!=null)
				for(int i=connections.Length-1;i>=0;i--)
					poly.Add(connections[i]);

			//close poly
			poly.Add(start);
			return poly;
		}
		public static Vector2d[] GetConnection(Vector2d neighb1, Vector2d neighb2, double radius){
			Vector2d[] conns = new Vector2d[2];
			conns[0] = GetConnection1(neighb1,neighb2,radius);
			conns[1] = GetConnection2(neighb1,neighb2,radius);
			return conns;
		}

		private static Vector2d GetConnection1(Vector2d neighb1, Vector2d neighb2, double radius){
			//			Vector V1 = OperationAddition(GetNorm(neighb1),GetNorm(neighb2));
			//			V1 = GetNorm(V1);
			//
			//			// v3
			//			double len_v3 = GetSkalarProduct(V1,neighb2)/GetSkalarProduct(neighb2,neighb2);
			//			Vector v3 = OperationMul(neighb2,len_v3);
			//
			//			//r�
			//			double rad_2  = quad(radius);
			//			double len = Math.Sqrt( rad_2 / ( quad(v3.X-V1.X) + quad(v3.Y-V1.Y) ));
			//			return OperationMul(V1,len);

			Vector2d V1 = (neighb1.GetNorm()+neighb2.GetNorm());
			V1 = (V1*-1).GetNorm();
			Vector2d n1;
			//			if((V1.X < 0 && V1.Y>0) || (V1.X>0 && V1.Y<0))
			n1 = neighb1.GetOrthogonal1().GetNorm();
			//			else
			//				n1 = GetNorm(GetOrthogonal2(neighb1));

//			debug1 = (n1*radius);

			double len = (radius * n1.GetSkalarProduct())/(V1.GetSkalarProduct(n1));
			return (V1*len);
		
		}

		private static Vector2d GetConnection2(Vector2d neighb1, Vector2d neighb2, double radius){

			Vector2d V1 = (neighb1.GetNorm()+neighb2.GetNorm());
			V1 = (V1*-1).GetNorm();
			Vector2d n1;
			//			if((V1.X < 0 && V1.Y>0) || (V1.X>0 && V1.Y<0))
			n1 = neighb1.GetOrthogonal2().GetNorm();
			//			else
			//				n1 = GetNorm(GetOrthogonal2(neighb1));

//			debug1 =(n1*radius);

			double len = (radius * n1.GetSkalarProduct())/(V1.GetSkalarProduct(n1));
			return (V1*len);

		}
		private bool IsVertHort(Edge e)
		{
			Vector2d dirVec = e.V1-e.V2;
			if(dirVec.X==0 || dirVec.Y==0)
				return true;
			else 
				return false;
		}
		private int GetIndex(Vector2d v)
		{
			for(int i=0;i<this.Length;i++)
				if(this[i]==v)
					return i;
			return -1;
		}
		private ArrayList GetCutpoints(Edge[] edges,Edge edge_to_check)
		{
			ArrayList cutpoints = new ArrayList();
			Vector2d cutpoint = new Vector2d(0,0);
			for(int j=0;j<edges.Length;j++)
			{
				if (LinearAlgebra.GetCutPointInSegment(edges[j],edge_to_check,out cutpoint))
				{
					//check ob es sich um einen cut im ursprung handelt
					double error_x = Math.Abs(edge_to_check.V1.X-cutpoint.X);
					double error_y = Math.Abs(edge_to_check.V1.Y-cutpoint.Y);
					double error_tol = edge_to_check.GetLen()*Math.Pow(10,-10); // error tolerance of 10^-12 = pico
					if(IsVertHort(edges[j]) || IsVertHort(edge_to_check))
						error_tol = Math.Pow(10,-1); // error tolerance of 10^-12 = pico
					if(error_x > error_tol || error_y > error_tol)
					{
						object[] oarr = new object[3];
						oarr[0] = cutpoint;
						oarr[1] = j;
						cutpoints.Add(oarr);
					}
				}
			}
			return cutpoints;
		}

		private Vector2d GetLeftVector(Vector2d start,Vector2d cutpoint,Edge edge,out int index_of_right_vector,out int index_of_left_vector)
		{
			Vector2d main = (cutpoint - start);
			Vector2d tocompare1 = edge.V1 - start;
			//Vector tocompare2 = edge.V2 - start;
			double angle_to_rotate = main.GetAngle();
			main.RotateCounterClockwise(angle_to_rotate);
			tocompare1.RotateCounterClockwise(angle_to_rotate);
			//tocompare2.RotateCounterClockwise(angle_to_rotate);
			if(tocompare1.Y<0)
			{
				index_of_right_vector = GetIndex(edge.V2);
				index_of_left_vector = GetIndex(edge.V1);
				return edge.V1;
			}
			else
			{
				index_of_right_vector = GetIndex(edge.V1);
				index_of_left_vector = GetIndex(edge.V2);
				return edge.V2;
			}
		}
		
		public ArrayList WayDetection()
		{
			//way detection
			ArrayList cutpoints = new ArrayList();
			ArrayList waytogo = new ArrayList();
			Edge[] edges = GetEdges();
			Vector2d cutpoint=new Vector2d(0,0);
			double minlen = Double.PositiveInfinity, curlen = 0;
			int counter=0;
			int i_start=0,i_end=1,i_begin=0;
			int i_edge=0, i_difference=0;
			Vector2d v_start = this[i_start];
			Vector2d v_end = this[i_end];
			Vector2d v_begin =this[i_begin];
			do
			{
				waytogo.Add(v_start);
				cutpoints = GetCutpoints(edges,new Edge(v_start,v_end));
				if(cutpoints.Count == 0)
				{
					i_difference = i_start - i_end;
					v_start = v_end; i_start = i_end;
					i_end -= i_difference; 
					if(i_end>this.Length-1)
						i_end = this.Length-2;
					else if(i_end < 0)
						i_end = 1;
					v_end = this[i_end];

				}
				else if(cutpoints.Count > 0)
				{
					for(int i=0;i<cutpoints.Count;i++)
					{
						curlen = ((Vector2d)((object[])cutpoints[i])[0]-v_start).GetLen();
						if(curlen<minlen)
						{
							minlen = curlen;
							cutpoint = (Vector2d)((object[])cutpoints[i])[0];
							i_edge = (int)((object[])cutpoints[i])[1];
						}
					}
					//drawCutpoint(g,cutpoint);
					v_end = GetLeftVector(v_start,cutpoint,edges[i_edge],out i_start,out i_end);
					v_start = cutpoint;
					cutpoints.Clear();
					cutpoints.Clear();
					minlen = Double.PositiveInfinity;
					curlen=0;
				}
				counter++;
				if(counter > 100000)
					throw new GeoLibException("Route Calc does not stop, cannot create route");
			}while(i_start != i_begin || this[i_start]!=v_start  || waytogo.Count < this.Length );
			waytogo.Add(v_start);	
			return waytogo;
		}

		/// <summary>
		/// DER NEUESTE ALGORITHMUS
		/// der alte ist leider nur im makro modus klargekommen, 
		/// makro = wenig elemente grosse entfernungen
		/// dieser hier sollte immer funktionieren auch wenn es sich um route mit vielen 
		/// vertexen handelt
		/// </summary>
		/// <param name="margin"></param>
		/// <returns></returns>
		public Polygon GetPolygonAroundLineMicro(double margin,out PolygonType pt){
			//set per default mikro, da ich ja ein solches erstellen will
			//dies wird nur umgestellt wenn dieser alg, nicht funktioniert
			pt = PolygonType.Mikro;

			//1.l�se vertexte �ber geraden auf
			vertices = RemoveUnusedVerticesInLine(this);

			//2. erstelle das poly
			PolyCreator p = new PolyCreator(this,margin,100);

			try
			{
				//starte den algorithmus und erstelle das polygon
				p.StartCreation();
				Polygon poly = p.GetPolygon();
				return poly;
			}
			catch{
				bool isConvexHull=false;
				Polygon poly = GetPolygonAroundLineComplex(margin,out isConvexHull);
				if(isConvexHull)
					pt = PolygonType.ConvexHull;
				else
					pt = PolygonType.Makro;
				return poly;
			}
		}
		public enum PolygonType{
			Mikro=1,
			Makro,
			ConvexHull,
			GreinerHorman
		}
		private ArrayList RemoveUnusedVerticesInLine(Line l)
		{
			ArrayList newLine = new ArrayList();
			newLine.Add(l[0]);
			double ERROR_TOLERANCE = 0.001;
			for(int i=1;i<l.Length-1;i++)
			{
				Vector2d prev = l[i-1];
				Vector2d next = l[i+1];
				Vector2d curr = l[i];
				double angle = GetAbsoluteAngleDifference(prev,curr,next);
				double angle_error = Math.Abs(Math.PI-angle);
				if(angle_error>ERROR_TOLERANCE)
					newLine.Add(curr);
			}
			newLine.Add(l[l.Length-1]);
			return newLine;
		}

		private double GetAbsoluteAngleDifference(Vector2d prev, Vector2d curr, Vector2d next)
		{
			double anglediff = Math.PI*2;
			if(next == prev)
				return anglediff;
			double distance = (next-prev).GetLen();
			double error_tol = (next.GetLen()+prev.GetLen()) * Math.Pow(10,-10);
			if(distance < error_tol)
				return anglediff;
			Vector2d V1 = prev-curr;
			Vector2d V2 = next-curr;
			anglediff = V1.GetAngle()-V2.GetAngle();
			return Math.Abs(anglediff);
			//			if(anglediff > Math.PI/2)
			//				return VertexAngle.Concav;
			//			else
			//				return VertexAngle.Convex;		
		}
		

		/// <summary>
		/// sollte es im result polygon zu einer �berschneidung kommen wird davon ausgegangen dass es sich um einen fehler handelt
		/// in dem fall wird dann die convex hull zur�ck gegeben!
		/// </summary>
		/// <param name="margin"></param>
		/// <param name="pt"></param>
		/// <returns></returns>
		public Polygon GetPolygonAroundLine(double margin,out PolygonType pt){
			pt = PolygonType.GreinerHorman;
			Polygon result_polygon = PolygonUtils.CreatePolyAroundEdge(new Edge((Vector2d)vertices[0],(Vector2d)vertices[1]),margin);
			GeoLib.PolygonUtils pu = new GeoLib.PolygonUtils();
				
			int MAX_ROUNDS_TO_TRY = 3;
			for(int round=0;round<MAX_ROUNDS_TO_TRY;round++)
			{
				for(int i=1;i<vertices.Count-1;i++)
				{
					//margin = margin+(margin*0.01);
					Polygon p = PolygonUtils.CreatePolyAroundEdge(new Edge((Vector2d)vertices[i],(Vector2d)vertices[i+1]),margin);
					result_polygon = pu.Union(result_polygon,p);
				}

				//valdiate polygon as a correct polygon over the line
				if(Polygon.WithIntersection(result_polygon))
					continue;

				//second validation check 
				//all route vertices must be in polygon
				for(int i=0;i<vertices.Count-1;i++)
				{
					Vector2d v = (Vector2d)vertices[i];
					if(!result_polygon.PointInPolygon(v)){
						continue;
					}
				}
				return result_polygon;
			}
			
			pt = PolygonType.ConvexHull;
			result_polygon = GetConvexHull(margin);
			return result_polygon;
		}



		/// <summary>
		/// gibt ein poly das mit einem gewissen margin rund um den weg liegt
		/// dabei werden �berscheidungen erkannt. es kann in seltenen f�llen dazu kommen
		/// dass der alg keine berechnung durchf�hren kann, f�r diesen fall wird die convex h�lle 
		/// berechnet
		/// </summary>
		/// <param name="margin">abstand zwischen line und border minimum</param>
		/// <returns>geschlossenes polygon</returns>
		public Polygon GetPolygonAroundLineComplex(double margin)
		{
			bool isConvexHull = false;
			return GetPolygonAroundLineComplex(margin,out isConvexHull);
		}

		public Vector2d[] GetPolygonArrayAroundLineComplex(double radius)
		{
			return GetPolygonAroundLineComplex(radius).ToArray();
		}

		/// <summary>
		/// gibt ein poly das mit einem gewissen margin rund um den weg liegt
		/// dabei werden �berscheidungen erkannt. es kann in seltenen f�llen dazu kommen
		/// dass der alg keine berechnung durchf�hren kann, f�r diesen fall wird die convex h�lle 
		/// berechnet
		/// </summary>
		/// <param name="margin">abstand zwischen line und border minimum</param>
		/// <param name="isConvexHull">true wenn das zur�ckgegebene polygon der convex hull entspricht</param>
		/// <returns></returns>
		public Polygon GetPolygonAroundLineComplex(double margin,out bool isConvexHull)
		{
			try
			{
				ArrayList waytogo = WayDetection();
				isConvexHull=false;

				//create poly algorithm
				ArrayList poly = new ArrayList();
				VertexAngle v_angle;
				Vector2d v_prev;
				Vector2d v_curr = (Vector2d)waytogo[0];
				Vector2d v_next = (Vector2d)waytogo[1];
				Vector2d v;
				Vector2d[] vt;
				Vector2d cutpoint;
				double radius=margin;
				for(int i=1;i<waytogo.Count-1;i++)
				{
					v_prev = (Vector2d)waytogo[i-1];
					v_curr = (Vector2d)waytogo[i];
					v_next = (Vector2d)waytogo[i+1];
					v_angle = GetVertexAngleType(v_prev,v_curr,v_next);

					if(v_angle == VertexAngle.Edge)
					{
						v = (v_prev-v_curr).GetNorm().GetMirror()*radius;
						poly.Add(v_curr + v + v.GetOrthogonal2().GetNorm()*radius);
						poly.Add(v_curr + v + v.GetOrthogonal1().GetNorm()*radius);
					}
					else
					{
						vt = Line.GetConnection(v_prev-v_curr,v_next-v_curr,radius);
						v = GetLeftVector(v_prev,v_curr,v_curr+vt[0],v_curr+vt[1]);

						if((v_curr-v_prev).GetLen()<(v-v_prev).GetLen())
						{
							//concav winkel						
							Vector2d c1,c2;
							//concav winkel						
							//g.DrawString("concav",drawFont,drawBrush,v.XF,v.YF);
							double concav_edge_bigger_factor = 1;
							Vector2d t = (v-v_curr).GetNorm()*radius*concav_edge_bigger_factor;
							Edge e1 = new Edge(v,v+(v_curr-v_prev).GetNorm());
							Edge e2 = new Edge((v_curr+t),(v_curr+t)+t.GetOrthogonal1().GetNorm());
							LinearAlgebra.GetCutPoint(e1,e2,out c1);		

							e1 = new Edge(v,v+(v_curr-v_next).GetNorm());
							e2 = new Edge((v_curr+t),(v_curr+t)+t.GetOrthogonal2().GetNorm());
							LinearAlgebra.GetCutPoint(e1,e2,out c2);	
				
							if(concav_edge_bigger_factor == 1 || (v_prev-c2).GetLen() > (v_prev-c1).GetLen())
							{
								poly.Add(c1);
								poly.Add(c2);				
							}else
							{
								poly.Add(c2);							
								poly.Add(c1);
							}
						}
						else
						{
							//convex winkel
							if((v_prev-v).GetLen() > radius*3 && (v_next-v).GetLen() > radius*3 && ((v-v_curr).GetLen()<(v-v_prev).GetLen() || (v-v_curr).GetLen()<(v-v_next).GetLen()))
								poly.Add(v);
						}
					}
				}

				//handle last vertex
				v_prev = (Vector2d)waytogo[waytogo.Count-1-1];
				v_curr = (Vector2d)waytogo[waytogo.Count-1];

				v = (v_prev-v_curr).GetNorm().GetMirror()*radius;
				poly.Add(v_curr + v + v.GetOrthogonal2().GetNorm()*radius);
				poly.Add(v_curr + v + v.GetOrthogonal1().GetNorm()*radius);


				//berechne den geringsten abstand zu einer line
				Line tline = new Line();
				tline.Add(waytogo.ToArray());
				Line border_line = new Line();
				border_line.Add(poly.ToArray());
				Edge[] bedges = border_line.GetEdges();
				ArrayList removelist = new ArrayList();
				Vector2d t_prev;
				Vector2d t_curr = (Vector2d)poly[poly.Count-1];
				Vector2d t_next;
				Vector2d t1,t2;
				for(int i=0;i<poly.Count;i++)
				{
					if(tline.GetShortestDistanceToLine((Vector2d)poly[i])<(radius-0.00001))
					{
						t_curr = (Vector2d)poly[i];
						
						removelist.Add(t_curr);

						t_prev = (Vector2d)poly[GetNextIndex(poly,i,false)];
						t_next = (Vector2d)poly[GetNextIndex(poly,i,true)];

						t1 = (t_prev - t_curr).GetNorm()*radius*100;
						t2 = (t_next - t_curr).GetNorm()*radius*100;


						Vector2d  t1_nearest_cutpoint = new Vector2d(0,0);
						Nullable<Edge>    t1_nearest_edge=null;
						double  t1_nearest_len=Double.PositiveInfinity;
						int     t1_nearest_edge_index = -1;
						Vector2d  t2_nearest_cutpoint = new Vector2d(0,0);
						Nullable<Edge>    t2_nearest_edge=null;
						double  t2_nearest_len=Double.PositiveInfinity;
						int     t2_nearest_edge_index = -1;
						for(int j=0;j<poly.Count;j++)
						{
							if(i!=j && i+1!=j && i-1!=j)
							{
								Edge e1 = new Edge((Vector2d)poly[j],(Vector2d)poly[GetNextIndex(poly,j,true)]);
								Edge e2 = new Edge(t_curr,t_curr+t1);
								if(LinearAlgebra.GetCutPointInSegment(e1,e2,out cutpoint))
								{
									if((cutpoint-t_curr).GetLen()<t1_nearest_len)
									{
										t1_nearest_cutpoint = cutpoint;
										t1_nearest_edge = e1;
										t1_nearest_edge_index = j;
										t1_nearest_len = (cutpoint-t_curr).GetLen();
									}
								}

								e2 = new Edge(t_curr,t_curr+t2);
								if(LinearAlgebra.GetCutPointInSegment(e1,e2,out cutpoint))
								{
									if((cutpoint-t_curr).GetLen()<t2_nearest_len)
									{
										t2_nearest_cutpoint = cutpoint;
										t2_nearest_edge = e1;
										t2_nearest_edge_index = j;
										t2_nearest_len = (cutpoint-t_curr).GetLen();
									}
								}
							}
						}

						//vertexe die eingeschlossen sind aufl�sen
						if(t1_nearest_edge_index!=-1 && t2_nearest_edge_index == t1_nearest_edge_index)
						{

							//anz vertexe links
							ArrayList todelete1 = new ArrayList();
							int index = GetNextIndex(poly,i,false);
							int t1_steps = 0;
							while(poly.Count*2 > t1_steps)
							{
								todelete1.Add(poly[index]);
                                if ((Vector2d)poly[index] == t1_nearest_edge.Value.V1 || (Vector2d)poly[index] == t1_nearest_edge.Value.V2)
									break;
								t1_steps++;
								index = GetNextIndex(poly,index,false);
							}
							
							ArrayList todelete2 = new ArrayList();
							index = GetNextIndex(poly,i,true);
							int t2_steps = 0;
							while(poly.Count*2 > t1_steps)
							{
								todelete2.Add(poly[index]);
                                if ((Vector2d)poly[index] == t1_nearest_edge.Value.V1 || (Vector2d)poly[index] == t1_nearest_edge.Value.V2)
									break;
								t2_steps++;
								index = GetNextIndex(poly,index,true);
							}

							if(t1_steps<t2_steps)
								AddToList(removelist,todelete1);
							else
								AddToList(removelist,todelete2);
						}
					}
				}

				for(int i=0;i<removelist.Count;i++)
				{
					poly.Remove(removelist[i]);
				}
				//if(lastelemdeleted)
				poly.Add(poly[0]);


				//mach einen sicherheitscheck, der urspr�ngliche weg darf sich nicht mit einer border schneiden
				bool polydefect = false;
				for(int i=0;i<this.Length-1;i++)
				{
					Edge e1 = new Edge(this[i],this[i+1]);
					for(int j=0;j<poly.Count-1;j++)
					{
						Edge e2 = new Edge((Vector2d)poly[j],(Vector2d)poly[j+1]);
						if(LinearAlgebra.GetCutPointInSegment(e1,e2,out cutpoint))
						{
							polydefect = true;
							break;
						}
					}
				}


				//check ob das poly im weg liegt
				if(!polydefect)
				{
					//check ob sich start mit ende �berschneidet und ob die edges gr�sser sind als 
					double minleft_way = Double.PositiveInfinity;
					double minleft_border = Double.PositiveInfinity;
					for(int i=0;i<this.Length;i++)
						if(minleft_way > this[i].X)
							minleft_way = this[i].X;
					for(int i=0;i<poly.Count;i++)
						if(minleft_border > ((Vector2d)poly[i]).X)
							minleft_border = ((Vector2d)poly[i]).X;
					if(minleft_way < minleft_border)
						polydefect = true;
				}


				//check ob das poly sich selbst schneidet
				if(!polydefect)
				{

					for(int i=0;i<poly.Count-1;i++)
					{
						Edge e1 = new Edge((Vector2d)poly[i],(Vector2d)poly[i+1]);
						for(int j=0;j<poly.Count-1;j++)
						{
							if( i!=j && i-1!=j && i+1!=j)
							{
								Edge e2 = new Edge((Vector2d)poly[j],(Vector2d)poly[j+1]);
								if(LinearAlgebra.GetCutPointInSegment(e1,e2,out cutpoint))
								{
									polydefect = true;
									break;
								}										  
							}
						}
						if(polydefect)
							break;
					}
				}


				if(!polydefect)
				{
					//create a polygon
					Polygon p = new Polygon();
					for(int i=0;i<poly.Count;i++)
						p.Add((Vector2d)poly[i]);
					return p;
				}
			}
			catch{
			}

			isConvexHull = true;
			return GetConvexHull(margin);
			
		}

		public Polygon GetConvexHull(double margin){
			ArrayList v = new ArrayList();
			for(int i=0;i<this.Length;i++)
			{
				for(int j=1;j<=4;j++)
				{
					if(j==1)
						v.Add( new Vector2d(this[i].X - margin,this[i].Y - margin));
					else if(j==2)
						v.Add( new Vector2d(this[i].X - margin,this[i].Y + margin));
					else if(j==3)
						v.Add( new Vector2d(this[i].X + margin,this[i].Y - margin));
					else if(j==4)
						v.Add( new Vector2d(this[i].X + margin,this[i].Y + margin));
				}
			}

			int i_start = 0;
			Vector2d v_curr = (Vector2d)v[0];
			int i_curr = 0;
			for(int i=0;i<v.Count;i++)
			{
				if(v_curr.Y > ((Vector2d)v[i]).Y)
				{
					v_curr = (Vector2d)v[i];
					i_curr = i;
				}
			}



			i_start  = i_curr;
			double scan_angle =0;
			Polygon p = new Polygon();
			p.Add(v_curr);
			while(p.Length<v.Count)
			{
				double min_angle = Double.PositiveInfinity;
				int help = 0;
				for(int i=0;i<v.Count;i++)
				{
					if(i_curr != i)
					{
						double curr_angle = ((Vector2d)v[i]-(Vector2d)v[i_curr]).GetAngle(); 
						if(min_angle>curr_angle && curr_angle >= scan_angle)
						{
							help  = i;
							min_angle = curr_angle;
						}
					}
				}
				p.Add((Vector2d)v[help]);
				scan_angle = min_angle;
				i_curr = help;
				if(i_start == i_curr)
					break;
			}
			return p;
		}

		public ArrayList GetVerticesArrayList(){
			return vertices;
		}

		public Vector2d GetLeftVector(Vector2d root,Vector2d middle, Vector2d V1, Vector2d V2)
		{
			Vector2d main = middle - root;
			Vector2d tocompare1 = V1 - root;
			double angle_to_rotate = main.GetAngle();
			main.RotateCounterClockwise(angle_to_rotate);
			tocompare1.RotateCounterClockwise(angle_to_rotate);
			if(tocompare1.Y<0)
				return V1;
			else
				return V2;
		}
		private void AddToList(ArrayList body,ArrayList toadd)
		{
			for(int i=0;i<toadd.Count;i++)
				body.Add(toadd[i]);
		}
		private int GetNextIndex(ArrayList poly,int curr_index,bool next_index_inc)
		{
			if(next_index_inc)
			{
				if(curr_index == poly.Count-1)
					return 0;
				else
					return curr_index+1;
			}
			else
			{
				if(curr_index == 0)
					return poly.Count-1;
				else
					return curr_index-1;
			}
		}
		private VertexAngle GetVertexAngleType(Vector2d prev,Vector2d curr, Vector2d next)
		{
			if(next == prev)
				return VertexAngle.Edge;
			double distance = (next-prev).GetLen();
			double error_tol = (next.GetLen()+prev.GetLen()) * Math.Pow(10,-10);
			if(distance < error_tol)
				return VertexAngle.Edge;
			Vector2d V1 = prev-curr;
			Vector2d V2 = next-curr;
			double anglediff = V1.GetAngle()-V2.GetAngle();
			if(anglediff > Math.PI/2)
				return VertexAngle.Concav;
			else
				return VertexAngle.Convex;
		}

		private enum VertexAngle
		{
			Convex=1,
			Concav,
			Edge //exactly 360�
		}






















		protected class PolyCreator
		{

			public class Edge_PC
			{
				public Rectangle_PC myRectangle;
				public Vector2d v_start;
				public Vector2d v_end;
				public int id;
				public Edge_PC(Rectangle_PC rpc,Vector2d start,Vector2d end,int id)
				{
					v_start = start;
					v_end = end;
					myRectangle = rpc;
					this.id = id;

				}
				public Edge_PC(){}

				public Vector2d GetVNext(Direction dir)
				{
					if(dir == Direction.Forward)
						return v_end;
					else
						return v_start;
				}

				public Edge_PC GetNextEdge(Direction dir)
				{
					return myRectangle.GetNextEdge(this,dir);
				}
			}

			public class Rectangle_PC
			{
				double margin = 10;
				Edge_PC[] edges = new Edge_PC[4];

				public int id;
				public Rectangle_PC(double margin_aroung_line)
				{
					this.margin = margin_aroung_line;
				}
				public Edge_PC FromEdge
				{
					set
					{
						Vector2d V1 = value.v_start;
						Vector2d V2 = value.v_end;
						Vector2d norm = (V2-V1).GetNorm()*margin;
						Vector2d orth1 = norm.GetOrthogonal1();
						Vector2d orth2 = norm.GetOrthogonal2();
						Vector2d v_0 = V1+norm.GetMirror()+orth1;
						Vector2d v_1 = V2+norm+orth1;
						Vector2d v_2 = V2+norm+orth2;
						Vector2d v_3 = V1+norm.GetMirror()+orth2;
						edges[0] = new Edge_PC(this,v_0,v_1,0);
						edges[1] = new Edge_PC(this,v_1,v_2,1);
						edges[2] = new Edge_PC(this,v_2,v_3,2);
						edges[3] = new Edge_PC(this,v_3,v_0,3);
					}
				}

				public Edge_PC GetNextEdge(Edge_PC curr, Direction dir)
				{
					int curr_id = curr.id;

					if(dir == Direction.Forward)
						curr_id++;
					else
						curr_id--;

					if(curr_id >=0 && curr_id < 4)
						return edges[curr_id];
					else if(curr_id < 0)
						return edges[3];
					else
						return edges[0];
				}

				public Edge_PC GetStartEdge()
				{
					return edges[0];
				}

				public Vector2d GetStartVertex()
				{
					return edges[0].v_start;
				}

				public Edge_PC[] GetEdges()
				{
					return edges;
				}
				
			}

			public enum Direction
			{
				Forward,
				Backward
			}

			public Rectangle_PC[] mySurroundingRects;
			private Line route;
			private double margin = 0;
			private Direction dir = Direction.Forward;
			private Vector2d active_v_next,active_cutpoint;
			private Edge_PC active_edge;
			private ArrayList result_polygon = new ArrayList();


			/// <summary>
			/// 100% genauigkeit bedeutet dass alle routen vertexte �bernommen werden 
			/// bei 50% genauigkeit wird jeder zweite routen punkt weggelassen
			/// die diskretisierung ist somit h�her
			/// </summary>
			/// <param name="route"></param>
			/// <param name="margin"></param>
			/// <param name="prozentGenauigkeit"></param>
			public PolyCreator(Line route_in,double margin,int prozentGenauigkeit)
			{

				if(prozentGenauigkeit<100)
					this.route = FilterRoute(route_in,prozentGenauigkeit);
				else
					this.route = route_in;
				this.margin = margin;
				//erstelle alle Rectangles
				mySurroundingRects = new Rectangle_PC[route.Length-1];
				for(int i=0;i<mySurroundingRects.Length;i++)
				{
					Vector2d v_beg = route[i];
					Vector2d v_end = route[i+1];
					mySurroundingRects[i] = new Rectangle_PC(margin);
					Edge_PC e = new Edge_PC(mySurroundingRects[i],v_beg,v_end,-1);
					mySurroundingRects[i].FromEdge = e;
					mySurroundingRects[i].id = i;
				}

				//set startup bedingungen
				active_cutpoint =  mySurroundingRects[0].GetStartVertex();
				active_edge = mySurroundingRects[0].GetStartEdge();
				active_v_next = active_edge.v_end;

			}

			private Line FilterRoute(Line route,int prozGenauigkeit)
			{
				Line route_new = new Line();
				route_new.Add(route[0]);
				Vector2d last = route[route.Length-1];
				int count_now = route.Length;
                double d_count_now = (count_now * prozGenauigkeit) / 100;
                d_count_now = Math.Floor(d_count_now);
                int count_new = Convert.ToInt32(d_count_now);
				int auslassen = count_now-count_new;
				int jedes_wievielte_auslassen = Convert.ToInt32(Math.Ceiling((double)count_now/(double)auslassen));
				for(int i=1;i<route.Length-1;i++)
				{
					if(i%jedes_wievielte_auslassen != 0)
						route_new.Add(route[i]);
				}
				route_new.Add(last);
				return route_new;
			}
			public Polygon GetPolygon()
			{
				//create a polygon
				Polygon p = new Polygon();
				for(int i=0;i<result_polygon.Count;i++)
					p.Add((Vector2d)result_polygon[i]);
				p.Add(p[0]);
				return p;
			}
			public void StartCreation()
			{
				Vector2d cutpoint;
				Edge_PC edge;
				Direction newdir;

				Edge_PC old_edge=null;
				int counter = 0;

				while(true)
				{
					
					counter++;
					if(counter > 1000)
					{
                        throw new GeoLibException(10);
						//break;
					}
					//pbl.Invalidate();
					if(IsCutWithOtherEdge(out cutpoint,out edge,out newdir,old_edge))
					{
						result_polygon.Add(cutpoint);

						active_cutpoint = cutpoint;

						old_edge = active_edge;
						active_edge = edge;

						dir = newdir;

						active_v_next = active_edge.GetVNext(dir);
					}
					else
					{
						old_edge = null;

						result_polygon.Add(active_v_next);
						if(active_v_next == mySurroundingRects[0].GetStartVertex())
							break; //creation fertig

						active_cutpoint = active_v_next;

						active_edge = active_edge.GetNextEdge(dir);

						active_v_next = active_edge.GetVNext(dir);
					}
				}
				Console.WriteLine(counter);
			}

			private bool IsCutWithOtherEdge(out Vector2d cutpoint, out Edge_PC edge_cutted, out Direction newdir,Edge_PC old_edge)
			{
				int curr_rectangle_id = active_edge.myRectangle.id;
				Edge e1 = new Edge(active_cutpoint,active_v_next);
				ArrayList cutpoints = new ArrayList();

				cutpoint = new Vector2d(0,0);
				edge_cutted = new Edge_PC();
				newdir = Direction.Forward;

				for(int i=0;i<mySurroundingRects.Length;i++)
				{
					if(i!=curr_rectangle_id)
					{
						for(int j=0;j<4;j++)
						{
							edge_cutted = mySurroundingRects[i].GetEdges()[j];
							Edge e2 = new Edge(edge_cutted.v_start,edge_cutted.v_end);

							if(LinearAlgebra.GetCutPointInSegment(e1,e2,out cutpoint))
							{
								if(old_edge == null || edge_cutted.myRectangle.id != old_edge.myRectangle.id ||	edge_cutted.id != old_edge.id)
								{
									double lentocut = (cutpoint-e1.V1).GetLen();
									object[] oarr = new object[3];
									oarr[0] = cutpoint;
									oarr[1] = edge_cutted;
									oarr[2] = lentocut;
									cutpoints.Add(oarr);
								}
							}
						}
					}
				}

				if(cutpoints.Count==0)
					return false;

				cutpoints.Sort(new CutPointSorter());
				object[] oarr_nearest = (object[])cutpoints[0];
				edge_cutted = (Edge_PC)oarr_nearest[1];
				cutpoint = (Vector2d)oarr_nearest[0];

				Edge e_tocut = new Edge(active_cutpoint,cutpoint);
				newdir = GetNewDirection(e_tocut,edge_cutted);
				return true;
			}

			private Direction GetNewDirection(Edge e_tocut, Edge_PC edge_cutted)
			{
				Edge ecutted = new Edge(edge_cutted.v_start,edge_cutted.v_end);
				Vector2d v_left = GetLeftVector(e_tocut.V1,e_tocut.V2,ecutted);
				if(v_left == edge_cutted.v_end)
					return Direction.Forward;
				else
					return Direction.Backward;
			}

			private Vector2d GetLeftVector(Vector2d start,Vector2d cutpoint,Edge edge)
			{
				Vector2d main = (cutpoint - start);
				Vector2d tocompare1 = edge.V1 - start;
				double angle_to_rotate = main.GetAngle();
				main.RotateCounterClockwise(angle_to_rotate);
				tocompare1.RotateCounterClockwise(angle_to_rotate);
				if(tocompare1.Y<0)
					return edge.V2;
				else
					return edge.V1;
			}

			public class CutPointSorter : IComparer  
			{
				int IComparer.Compare( Object x, Object y )  
				{
					object[] oarr1 = (object[])x;
					object[] oarr2 = (object[])y;
					double len1 = (double)oarr1[2];
					double len2 = (double)oarr2[2];
					if(len1 < len2)
						return -1;
					else if(len1 == len2)
						return 0;
					else
						return 1;
				}
			}
		}
	}
}
