﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hangfire;

namespace PeServerApp
{
    class Program
    {

        public class ContainerJobActivator : JobActivator
        {
            private IContainer _container;

            public ContainerJobActivator(IContainer container)
            {
                _container = container;
            }

            public override object ActivateJob(Type type)
            {
                //return _container.Resolve(type);
                return null;
            }
        }



        static void Main(string[] args)
        {
            var container = new Container();

            GlobalConfiguration.Configuration
            .UseColouredConsoleLogProvider()
            //.UseActivator(new ContainerJobActivator(container))
            .UseSqlServerStorage(@"Server=awisrv01\awialpop;Database=Hangfire;Trusted_Connection=True;");

            using (new BackgroundJobServer())
            {
                while (true)
                {
                    Console.WriteLine("Press Enter to exit...");
                    string res = Console.ReadLine();
                    if (string.IsNullOrEmpty(res))
                        break;
                }
            }
        }
    }
}
