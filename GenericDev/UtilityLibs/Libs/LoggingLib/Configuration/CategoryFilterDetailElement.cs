﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace LoggingLib.Configuration
{
    /// <summary>
    /// category detail filter element which defines priority and severity filter for a single category
    /// </summary>
    public class CategoryFilterDetailElement : ConfigurationElement
    {
        /// <summary>
        /// The name of the category where this filter should be applied.
        /// </summary>
        [ConfigurationProperty("CategoryName", DefaultValue = "", IsKey = true, IsRequired = true)]
        public string CategoryName
        {
            get
            {
                return ((string)(base["CategoryName"]));
            }

            set
            {
                base["CategoryName"] = value;
            }
        }

        /// <summary>
        /// Semicolon separated list of priorities which should be logged.
        /// Only log entries matching the priority you specify here will be logged.
        /// Leave blank to log all priorities.
        /// </summary>
        /// <remarks>The priority filter only applies to the category specified at <see cref="CategoryName"/></remarks>
        [ConfigurationProperty("PriorityFilter", DefaultValue = "", IsRequired = true)]
        public string PriorityFilter
        {
            get
            {
                return ((string)(base["PriorityFilter"]));
            }

            set
            {
                base["PriorityFilter"] = value;
            }
        }

        /// <summary>
        /// Semicolon separated list of severities which should be logged.
        /// Only log entries matching the severity you specify here will be logged.
        /// Leave blank to log all severities.
        /// </summary>
        /// <remarks>The severity filter only applies to the category specified at <see cref="CategoryName"/></remarks>
        [ConfigurationProperty("SeverityFilter", DefaultValue = "", IsRequired = true)]
        public string SeverityFilter
        {
            get
            {
                return ((string)(base["SeverityFilter"]));
            }

            set
            {
                base["SeverityFilter"] = value;
            }
        }
    }
}
