﻿using System;
using System.Drawing;
using System.Windows.Media;
using MathLib;
using Windows7.Multitouch.Manipulation;

namespace Logicx.WpfUtility.UiManipulation
{

    public class SmoothManipulationProcessor : IDisposable
    {
        public SmoothManipulationProcessor()
        {
            _processor = new ManipulationProcessor(ProcessorManipulations.ALL);
            Register();
        }

        private void Register()
        {
            _processor.ManipulationDelta += ProcessManipulationDelta;
            _processor.ManipulationCompleted += ProcessManipulationCompleted;
            _processor.ManipulationStarted += ProcessManipulationStarted;
            CompositionTarget.Rendering += UpdatePosition;
        }

        private void DeRegister()
        {
            _processor.ManipulationDelta -= ProcessManipulationDelta;
            _processor.ManipulationCompleted -= ProcessManipulationCompleted;
            _processor.ManipulationStarted -= ProcessManipulationStarted;
            CompositionTarget.Rendering -= UpdatePosition;
        }

        public void Dispose()
        {
            DeRegister();
            _processor.Dispose();
        }

        private void UpdatePosition(object sender, EventArgs e)
        {
            if (_target_translation_delta_x == 0 &&
                _target_translation_delta_y == 0 &&
                _target_rotation_delta == 0 &&
                _calc_smooth_animation == null)
                return;

            //get smooth_transl x for this step
            float smooth_translation_delta_x = _target_translation_delta_x * SmoothTranslation;
            _target_translation_delta_x -= smooth_translation_delta_x;

            //get smooth transl y for this step
            float smooth_translation_delta_y = _target_translation_delta_y * SmoothTranslation;
            _target_translation_delta_y -= smooth_translation_delta_y;

            //get smooth rot for this step
            float smooth_rotation_delta = _target_rotation_delta * SmoothTranslation;
            _target_rotation_delta -= smooth_rotation_delta;

            //get smooth scale for this step
            float smooth_scale_delta = 1;
            if (_calc_smooth_animation != null)
            {
                smooth_scale_delta = _calc_smooth_animation.GetNextValue();
                if (smooth_scale_delta == 1)
                {
                    //reset vars, we are ready with all
                    _smooth_scale_delta_sum = 1;
                    _target_scale_delta = 1;
                    _calc_smooth_animation = null;
                }
                else
                {
                    _smooth_scale_delta_sum *= smooth_scale_delta;
                }
            }

            ManipulationDelta(sender, new ManipulationDeltaEventArgs(_x,
                                                                     _y,
                                                                     smooth_translation_delta_x,
                                                                     smooth_translation_delta_y,
                                                                     smooth_scale_delta,
                                                                     _expansion_delta,
                                                                     smooth_rotation_delta,
                                                                     _cumulative_translation_x, 
                                                                     _cumulative_translation_y,
                                                                     _cumulative_scale, 
                                                                     _cumulative_expansion,
                                                                     _cumulative_rotation));
        }

        private void ProcessManipulationStarted(object sender, ManipulationStartedEventArgs e)
        {
            ManipulationStarted(sender, e);
        }

        private void ProcessManipulationCompleted(object sender, ManipulationCompletedEventArgs e)
        {
            ManipulationCompleted(sender, e);
        }

        private void ProcessManipulationDelta(object sender, ManipulationDeltaEventArgs e)
        {
            _target_translation_delta_x += e.TranslationDelta.Width;
            _target_translation_delta_y += e.TranslationDelta.Height;

            _target_rotation_delta += e.RotationDelta;

            // smooth animation noch nicht fertig
            // fehlende scale_delta werte müssen berücksicht werden
            if (_calc_smooth_animation != null)
                _target_scale_delta = _target_scale_delta / _smooth_scale_delta_sum;

            _target_scale_delta *= e.ScaleDelta;

            if (_target_scale_delta != 1)
            {
                _calc_smooth_animation = new CalcSmoothAnimation(_target_scale_delta, SmoothScale);
                _smooth_scale_delta_sum = 1;
            }

            _x = e.Location.X;
            _y = e.Location.Y;
            _expansion_delta = e.ExpansionDelta;
            _cumulative_translation_x = e.CumulativeTranslation.Width;
            _cumulative_translation_y = e.CumulativeTranslation.Height;
            _cumulative_scale = e.CumulativeScale;
            _cumulative_expansion = e.CumulativeExpansion;
            _cumulative_rotation = e.CumulativeRotation;
        }

        /// <summary>
        /// This method raises the ManipulationCompleted() event in response
        /// </summary>
        public void CompleteManipulation()
        {
            _processor.CompleteManipulation();
        }

        /// <summary>
        /// The ProcessDown method feeds data to the manipulation processor associated with a target
        /// </summary>
        /// <param name="manipulationId">The identifier for the manipulation that you want to process</param>
        /// <param name="location">The coordinates associated with the target</param>
        public void ProcessDown(uint manipulationId, PointF location)
        {
            _processor.ProcessDown(manipulationId, location);
        }

        /// <summary>
        /// The ProcessUp method feeds data to a target's manipulation processor for touch up sequences
        /// </summary>
        /// <param name="manipulationId">The identifier for the manipulation that you want to process</param>
        /// <param name="location">The coordinates associated with the target</param>
        public void ProcessUp(uint manipulationId, PointF location)
        {
            _processor.ProcessUp(manipulationId, location);
        }

        /// <summary>
        /// The ProcessMove method feeds movement data for the target object to its manipulation processor
        /// </summary>
        /// <param name="manipulationId">The identifier for the manipulation that you want to process</param>
        /// <param name="location">The coordinates associated with the target</param>
        public void ProcessMove(uint manipulationId, PointF location)
        {
            _processor.ProcessMove(manipulationId, location);
        }

        /// <summary>
        /// Feds data to the manipulation processor associated with a target and a timestamp
        /// </summary>
        /// <param name="manipulationId">The identifier for the manipulation that you want to process</param>
        /// <param name="location">The coordinates associated with the target</param>
        /// <param name="timestamp">The timestamp of the event</param>
        public void ProcessDownWithTime(uint manipulationId, PointF location, int timestamp)
        {
            _processor.ProcessDownWithTime(manipulationId, location, timestamp);
        }

        /// <summary>
        /// Feds data to the manipulation processor associated with a target and a timestamp
        /// </summary>
        /// <param name="manipulationId">The identifier for the manipulation that you want to process</param>
        /// <param name="location">The coordinates associated with the target</param>
        /// <param name="timestamp">The timestamp of the event</param>
        public void ProcessMoveWithTime(uint manipulationId, PointF location, int timestamp)
        {
            _processor.ProcessMoveWithTime(manipulationId, location, timestamp);
        }

        /// <summary>
        /// Feds data to the manipulation processor associated with a target and a timestamp
        /// </summary>
        /// <param name="manipulationId">The identifier for the manipulation that you want to process</param>
        /// <param name="location">The coordinates associated with the target</param>
        /// <param name="timestamp">The timestamp of the event</param>
        public void ProcessUpWithTime(uint manipulationId, PointF location, int timestamp)
        {
            _processor.ProcessUpWithTime(manipulationId, location, timestamp);
        }

        #region Attributes
        private readonly ManipulationProcessor _processor;
        private float _x, _y, _target_translation_delta_x, _target_translation_delta_y, _expansion_delta, _target_rotation_delta,
                _cumulative_translation_x, _cumulative_translation_y, _cumulative_scale, _cumulative_expansion, _cumulative_rotation;
        private float _smooth_scale_delta_sum = 1, _target_scale_delta = 1;
        private CalcSmoothAnimation _calc_smooth_animation;

        /// <summary>
        /// smooth-factor for translation, has to be greater than 0 and lower or equal 1
        /// lower value mean more smoothing (smaller steps)
        /// default-value 0.2F
        /// </summary>
        public float SmoothTranslation = 0.2F;

        /// <summary>
        /// smooth-factor for rotation, has to be greater than 0 and lower or equal 1
        /// lower value mean more smoothing (smaller steps)
        /// default-value 0.08F
        /// </summary>
        public float SmoothRotation = 0.08F;

        /// <summary>
        /// smooth-factor for scale, has to be greater than 0 and lower or equal 1 
        /// lower value mean more smoothing (smaller steps) 
        /// default-value 0.2F
        /// </summary>
        public float SmoothScale = 0.2F;

        /// <summary>
        /// Calculates the rotational velocity that the target object is moving at
        /// </summary>
        public float AngularVelocity
        {
            get
            {
                return _processor.AngularVelocity;
            }
        }

        /// <summary>
        /// Calculates the rate that the target object is expanding at
        /// </summary>
        public float ExpansionVelocity
        {
            get
            {
                return _processor.ExpansionVelocity;
            }
        }

        /// <summary>
        /// Specifies the minimum scale and rotate radius
        /// </summary>
        public float MinimumScaleRotateRadius
        {
            get
            {
                return _processor.MinimumScaleRotateRadius;
            }
            set
            {
                _processor.MinimumScaleRotateRadius = value;
            }
        }

        /// <summary>
        /// The Center of the object
        /// </summary>
        public PointF PivotPoint
        {
            get
            {
                return new PointF(_processor.PivotPoint.X, _processor.PivotPoint.Y);
            }
            set
            {
                _processor.PivotPoint = value;
            }
        }

        /// <summary>
        /// The PivotRadius property is used to determine how much rotation is used in single finger manipulation
        /// </summary>
        public float PivotRadius
        {
            get { return _processor.PivotRadius; }
            set { _processor.PivotRadius = value; }
        }

        /// <summary>
        /// Calculates and returns the velocity for the target object
        /// </summary>
        public VectorF Velocity
        {
            get
            {
                return new VectorF(_processor.Velocity.X, _processor.Velocity.Y);
            }
        }
        #endregion

        #region EventHandler

        /// <summary>
        /// Fired each time the processor had figured a change in one or more of the required manipulations
        /// </summary>
        public event EventHandler<ManipulationDeltaEventArgs> ManipulationDelta = (s, e) => { };

        /// <summary>
        /// Fired on manipulation end
        /// </summary>
        public event EventHandler<ManipulationCompletedEventArgs> ManipulationCompleted = (s, e) => { };

        /// <summary>
        /// Fired when manipulation is started
        /// </summary>
        public event EventHandler<ManipulationStartedEventArgs> ManipulationStarted = (s, e) => { };
        #endregion
    }
}
