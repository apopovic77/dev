﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Media;
using Logicx.WpfUtility.ZoomingPanning;
using Microsoft.Win32;

namespace CognitiveTestApp
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            cb.ItemsSource = System.Enum.GetValues(typeof(CognitiveLib.CognitiveService.IntelligentImage.CvServiceType));
            cb.SelectedIndex = 0;

            _map_zp = new MapZP(grid, canvas);
            CompositionTarget.Rendering += CompositionTarget_Rendering;

            ////////
            //start app with default file loaded
            _last_path = _inital_dir + "\\23862SN.jpg";
            canvas.Children.Clear();
            _img = new CognitiveLib.CognitiveService.IntelligentImage(_last_path);
            canvas.Children.Add(_img);
            //set default scale for canvas
            Loaded += MainWindow_Loaded;
        }

        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            _map_zp.ScaleTransform.ScaleX = 0.2;
            _map_zp.ScaleTransform.ScaleY = 0.2;
        }

        void CompositionTarget_Rendering(object sender, EventArgs e)
        {
            _map_zp.Update();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (_img == null)
                return;

            CognitiveLib.CognitiveService.IntelligentImage.CvServiceType selected_cv_type;
            CognitiveLib.CognitiveService.IntelligentImage.CvServiceType.TryParse(cb.SelectedValue.ToString(), out selected_cv_type);

            _img.CvService = selected_cv_type;
            _img.AnalyzeImage();
            result_tbx.Text = _img.AnalysationSummary;

        }

        private void rotate_img_button_Click(object sender, RoutedEventArgs e)
        {
            if (_img == null)
                return;

            _img.Rotate();
        }

        private void imag_path_button_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            if (string.IsNullOrEmpty(_last_path))
                ofd.InitialDirectory = _inital_dir;
            else
                ofd.InitialDirectory = _last_path;
            if (!Directory.Exists((ofd.InitialDirectory)))
                ofd.InitialDirectory = _inital_dir;
            if (ofd.ShowDialog() == true)
            {
                string[] sarr = ofd.FileName.Split("\\".ToCharArray());
                image_path_tbx.Text = sarr[sarr.Length - 1];
                
                _last_path = ofd.FileName;

                canvas.Children.Clear();
                _img = new CognitiveLib.CognitiveService.IntelligentImage(ofd.FileName);
                canvas.Children.Add(_img);
            }
        }

        private string _inital_dir = @"C:\AwantoRepo\Experiments\ProofOfConcept\Cognitive\Data\images";
        private string _last_path;
        protected MapZP _map_zp;
        protected CognitiveLib.CognitiveService.IntelligentImage _img;

    }
}

