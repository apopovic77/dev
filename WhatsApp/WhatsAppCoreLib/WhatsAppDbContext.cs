﻿using System.Net.Mime;
using Awanto.WhatsAppCore.Entities;
using Awanto.WhatsAppCore.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Awanto.WhatsAppCore
{
    public class WhatsAppDbContext : DbContext
    {
        public WhatsAppDbContext(DbContextOptions<WhatsAppDbContext> options) : base(options)
        {
        }

        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    var lf = new LoggerFactory();
        //    lf.AddProvider(new ConsoleLoggerProvider());
        //    optionsBuilder.UseLoggerFactory(lf);
        //}

        protected override void OnModelCreating(ModelBuilder model_builder)
        {
            base.OnModelCreating(model_builder);

            #region n to m table Nachrichten (n)<-->(m) Medien
            model_builder.Entity<NachrichtMedium>()
                .HasKey(bc => new { bc.NachrichtId, bc.MediumId });

            model_builder.Entity<NachrichtMedium>()
                .HasOne(bc => bc.Nachricht)
                .WithMany(b => b.Medien)
                .HasForeignKey(bc => bc.NachrichtId);

            model_builder.Entity<NachrichtMedium>()
                .HasOne(bc => bc.Medium)
                .WithMany(c => c.Nachrichten)
                .HasForeignKey(bc => bc.MediumId);
            #endregion
        }

        public override int SaveChanges()
        {
            var changed_entities = ChangeTracker.Entries();

            foreach (var changed_entity in changed_entities)
            {
                if (changed_entity.Entity is EntityEntry)
                {
                    DbEntity db_entity = changed_entity.Entity as DbEntity;

                    if (db_entity != null)
                        switch (changed_entity.State)
                        {
                            case EntityState.Added:
                                db_entity.OnBeforeInsert();
                                break;

                            case EntityState.Modified:
                                db_entity.OnBeforeUpdate();
                                break;

                        }
                }
            }

            return base.SaveChanges();
        }

        public DbSet<Medium> Medien { get; set; }
        public DbSet<Bild> Bilder { get; set; }
        public DbSet<Video> Videos { get; set; }

        public DbSet<Nachricht> Nachrichten { get; set; }
        public DbSet<WhatsAppNachricht> WhatsAppNachrichten { get; set; }
        public DbSet<WhatsAppImageNachricht> WhatsAppImageNachrichten { get; set; }

        public DbSet<EventLog> EventLogEntries { get; set; }
    }

    public class WhatsAppDesignTimeDbxFactory : IDesignTimeDbContextFactory<WhatsAppDbContext>
    {
        public WhatsAppDbContext CreateDbContext(string[] args)
        {
            return new WhatsAppDbContext(WhatsAppService.GetDbContextOptions());
        }
    }

}
