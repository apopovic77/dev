using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace SettingsManagerApplication
{
    public partial class SettingsManager : Form
    {
        public struct ConfigFileInfo
        {
            public string PathToClassFile;
            public string PathToConfigFile;
        }

        string _settings_classfile_content = "";
        string _settings_configfile_content = "";
        ConfigFileInfo _settings_filename;
        //string _s1 = "//settingsmanager: construction begin";
        string _s2 = "//settingsmanager: construction end";
        //string _s3 = "//settingsmanager: savesettings begin";

        string _s3 = "//settingsmanager: register end";


        string _s4 = "//settingsmanager: savesettings end";
        string _s5 = "//settingsmanager: vardef begin";
        string _s6 = "//settingsmanager: vardef end";
        string _s7 = "<!-- settingsmanager: vardef beg -->";
        string _s8 = "<!-- settingsmanager: vardef end -->";
        bool _settings_changed = false;
        private ChooseConfigFile _choose_file;
        private static readonly ConfigFileInfo[] Default_SettingsFileName = new ConfigFileInfo[]
                                                                        {
                                                                            new ConfigFileInfo()
                                                                                {
                                                                                    PathToClassFile = @"C:\dev\GenerativeCoding\ShortestPathAgentsApp\SmartAgentConfig.cs",
                                                                                    PathToConfigFile = @"C:\dev\GenerativeCoding\ShortestPathAgentsApp\SmartAgentApp.config"
                                                                                }, 
                                                                            
                                                                        };

        


        public SettingsManager()
        {
            InitializeComponent();
            _choose_file = new ChooseConfigFile(Default_SettingsFileName);
            _choose_file.Closed += new EventHandler(_choose_file_Closed);
        }

        void _choose_file_Closed(object sender, EventArgs e)
        {
            //set path to new settings file
            _settings_filename = _choose_file.GetSelectedConfigFile();

            //open file
            ReadClassSettingsContent(File.OpenRead(_settings_filename.PathToClassFile));
            ReadConfigSettingsContent(File.OpenRead(_settings_filename.PathToConfigFile));
        }

        private void SettingsManager_Load(object sender, EventArgs e)
        {
            _choose_file.ShowDialog(this);

            if (treeView1.Nodes.Count == 0)
                return;

            treeView1.Nodes[treeView1.Nodes.Count - 1].Expand();
            treeView1.Nodes[treeView1.Nodes.Count - 1].Collapse();
            treeView1.SelectedNode = treeView1.Nodes[0];
        }

        private void ReadClassSettingsContent(Stream fs)
        {
            //read content to end from file
            StreamReader sr = new StreamReader(fs);
            _settings_classfile_content = sr.ReadToEnd();
            sr.Close();
            sr.Dispose();
            fs.Close();
            fs.Dispose();

            //parse settings and fill 
            ParseSettingsContent();
        }
        private void ReadConfigSettingsContent(Stream fs)
        {
            //read content to end from file
            StreamReader sr = new StreamReader(fs);
            _settings_configfile_content = sr.ReadToEnd();
            sr.Close();
            sr.Dispose();
            fs.Close();
            fs.Dispose();
        }


        private void loadSettingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Currently not implemented");
            return;

            //DialogResult dr = openFileDialog.ShowDialog();
            //if (dr == DialogResult.OK)
            //{
            //    //set path to new settings file
            //    _settings_filename = openFileDialog.FileName;

            //    //open file
            //    Stream fs = openFileDialog.OpenFile();

            //    ReadSettingsContent(fs);
            //}
        }

        private void ParseSettingsContent()
        {
            int index_beg = _settings_classfile_content.IndexOf(_s5);
            int index_end = _settings_classfile_content.IndexOf(_s6);




            string added_vars = _settings_classfile_content.Substring(index_beg + _s5.Length, index_end - (index_beg + _s5.Length));
            added_vars.TrimStart().TrimEnd();
            string[] arr_vars = added_vars.Split(";".ToCharArray());
            foreach (string var in arr_vars) {
                try
                {
                    if (var.Trim() == "")
                        continue;
                    string[] arr_tok = var.Trim().Split("=".ToCharArray(), 2);
                    string def = arr_tok[0];
                    string value = arr_tok[1];
                    string[] arr_def = def.Split(" ".ToCharArray());
                    string datatype = arr_def[1];
                    string name = arr_def[3];
                    if (datatype == "string")
                    {
                        value = value.Substring(2, value.Length - 3);
                    }

                    AddKnoten(datatype, name, value);
                }
                catch { }
            }
        }

        private void AddKnoten(string datatype, string name, string value)
        {
            TreeNode treeNode_datatype = new TreeNode("datatype: " + datatype);
            TreeNode treeNode_value = new TreeNode("value: " + value);
            TreeNode treeNode = new TreeNode(name, new TreeNode[] {treeNode_datatype,treeNode_value} );

            this.treeView1.Nodes.AddRange(new TreeNode[] { treeNode });
        }

        private void saveSettingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_settings_classfile_content == "")
                return;
            if (_settings_configfile_content == "")
                return;

            StreamWriter sw = File.CreateText(_settings_filename.PathToClassFile);
            sw.Write(_settings_classfile_content);
            sw.Flush();
            sw.Close();
            sw.Dispose();
            sw = null;

            sw = File.CreateText(_settings_filename.PathToConfigFile);
            sw.Write(_settings_configfile_content);
            sw.Flush();
            sw.Close();
            sw.Dispose();
            sw = null;
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void b_addsetting_Click(object sender, EventArgs e)
        {
            if (!AddSettingToClassFile())
                return;

            //try
            //{
            //    if (!AddSettingToConfigFile())
            //        return;
            //}catch
            //{
            //}

            _settings_changed = true;
            AddKnoten(GetDataType(),tb_settingsname.Text,tb_settingsdefvalue.Text);
        }

        void b_updatesetting_Click(object sender, System.EventArgs e)
        {
            throw new System.NotImplementedException();
        }

        private bool AddSettingToConfigFile()
        {
            bool bstring = rb_string.Checked;
            bool bint = rb_int.Checked;
            bool bfloat = rb_float.Checked;
            bool bv2float = rb_v2float.Checked;
            bool bbool = rb_bool.Checked;


            int index_s2 = _settings_configfile_content.IndexOf(_s8);
            string new_settings_content = _settings_configfile_content.Substring(0, index_s2);

            new_settings_content += "\r\n";
            new_settings_content += "\t<add key=\"" + tb_settingsname.Text + "_bez\" value=\"" + tb_kommentar.Text + "\" />\r\n";

            if (!bv2float)
                new_settings_content += "\t<add key=\"" + tb_settingsname.Text + "_value\" value=\"" + GetValueString().Replace("f", "").Replace(".", ",") + "\" />\r\n";
            else
            {
                new_settings_content += "\t<add key=\"" + tb_settingsname.Text + "_value1\" value=\"" + tb_settingsdefvalue.Text.Replace("f", "").Replace(".", ",") + "\" />\r\n";
                new_settings_content += "\t<add key=\"" + tb_settingsname.Text + "_value2\" value=\"" + tb_settingsdefvalue2.Text.Replace("f", "").Replace(".", ",") + "\" />\r\n";
            }

            if ((bfloat || bint || bv2float) && GetMaxValueString() != null && GetMinValueString() != null)
            {
                new_settings_content += "\t<add key=\"" + tb_settingsname.Text + "_min\" value=\"" + GetMinValueString().Replace("f", "").Replace(".", ",") + "\" />\r\n";
                new_settings_content += "\t<add key=\"" + tb_settingsname.Text + "_max\" value=\"" + GetMaxValueString().Replace("f", "").Replace(".", ",") + "\" />\r\n\t";
            }
            int index_s6 = _settings_configfile_content.IndexOf(_s8);
            new_settings_content += _settings_configfile_content.Substring(index_s6);
            
            _settings_configfile_content = new_settings_content;
            return true;
        }

        private bool AddSettingToClassFile()
        {
            bool bfloat = rb_float.Checked;
            bool bint = rb_int.Checked;
            bool bv2float = rb_v2float.Checked;
            bool bv2int = rb_v2int.Checked;

            //check if values given
            if (!AssertDatatypeWithValue())
            {
                MessageBox.Show("Cannot add setting please check value and datatype");
                return false;
            }
            if (!AssertKommentar())
            {
                MessageBox.Show("Cannot add setting pleas check kommentar");
                return false;
            }

            //check if setting is not existent
            if (!AssertSettingName())
            {
                MessageBox.Show("Cannot add setting already existent");
                return false;
            }

            int index_s10 = _settings_classfile_content.IndexOf(_s3);
            string new_settings_content = _settings_classfile_content.Substring(0, index_s10);
            new_settings_content += tb_settingsname.Text + ".PropertyChanged += Config_PropertyChanged;\r\n";
            new_settings_content += "\t\t\tConfigProperties.Add(\"" + tb_settingsname.Text + "\", " + tb_settingsname.Text + ");\r\n";


            int index_s2 = _settings_classfile_content.IndexOf(_s2);
            new_settings_content += "\t\t\t"+_settings_classfile_content.Substring(index_s10, index_s2 - index_s10);

            //add construction info
            new_settings_content += "//SettingsManager-Kommentar: " + tb_kommentar.Text + "\r\n";

            if (!bv2float && ! bv2int)
            {
                new_settings_content += "\t\t\tif (_config.AppSettings.Settings[\"" + tb_settingsname.Text + "_value\"] != null)\r\n" +
                                        "\t\t\t\t" + tb_settingsname.Text + ".Value = " + GetConvertFunctionName() + "(_config.AppSettings.Settings[\"" + tb_settingsname.Text + "_value\"].Value);\r\n";
            }
            else
            {
                new_settings_content += "\t\t\tif (_config.AppSettings.Settings[\"" + tb_settingsname.Text + "_value1\"] != null)\r\n" +
                                        "\t\t\t\t" + tb_settingsname.Text + ".ValueStart = " + GetConvertFunctionName() + "(_config.AppSettings.Settings[\"" + tb_settingsname.Text + "_value1\"].Value);\r\n";

                new_settings_content += "\t\t\tif (_config.AppSettings.Settings[\"" + tb_settingsname.Text + "_value2\"] != null)\r\n" +
                                        "\t\t\t\t" + tb_settingsname.Text + ".ValueEnd = " + GetConvertFunctionName() + "(_config.AppSettings.Settings[\"" + tb_settingsname.Text + "_value2\"].Value);\r\n";
            }

            new_settings_content += "\t\t\tif (_config.AppSettings.Settings[\"" + tb_settingsname.Text + "_bez\"] != null)\r\n" +
                                    "\t\t\t\t" + tb_settingsname.Text + ".Bezeichnung = _config.AppSettings.Settings[\"" + tb_settingsname.Text + "_bez\"].Value;\r\n";
            if ((bfloat || bint || bv2float || bv2int) && GetMaxValueString() != null && GetMinValueString() != null)
            {
                new_settings_content += "\t\t\tif (_config.AppSettings.Settings[\"" + tb_settingsname.Text + "_min\"] != null)\r\n" +
                                        "\t\t\t\t" + tb_settingsname.Text + ".Minimum = " + GetConvertFunctionName() + "(_config.AppSettings.Settings[\"" + tb_settingsname.Text + "_min\"].Value);\r\n";
                new_settings_content += "\t\t\tif (_config.AppSettings.Settings[\"" + tb_settingsname.Text + "_max\"] != null)\r\n" +
                                        "\t\t\t\t" + tb_settingsname.Text + ".Maximum = " + GetConvertFunctionName() + "(_config.AppSettings.Settings[\"" + tb_settingsname.Text + "_max\"].Value);\r\n";
            }


            int index_s4 = _settings_classfile_content.IndexOf(_s4);
            new_settings_content += "\t\t\t" + _settings_classfile_content.Substring(index_s2, index_s4 - index_s2);

            //add save info
            new_settings_content += "//SettingsManager-Kommentar: " + tb_kommentar.Text + "\r\n";

            if (!bv2float && !bv2int)
                new_settings_content += "\t\t\tAddKey(_config, \"" + tb_settingsname.Text + "_value\", " + tb_settingsname.Text + ".Value.ToString());\r\n";
            else
            {
                new_settings_content += "\t\t\tAddKey(_config, \"" + tb_settingsname.Text + "_value1\", " + tb_settingsname.Text + ".ValueStart.ToString());\r\n";
                new_settings_content += "\t\t\tAddKey(_config, \"" + tb_settingsname.Text + "_value2\", " + tb_settingsname.Text + ".ValueEnd.ToString());\r\n";
            }
            new_settings_content += "\t\t\tAddKey(_config, \"" + tb_settingsname.Text + "_bez\", " + tb_settingsname.Text + ".Bezeichnung);\r\n";
            if ((bfloat || bint || bv2float || bv2int) && GetMaxValueString() != null && GetMinValueString() != null)
            {
                new_settings_content += "\t\t\tAddKey(_config, \"" + tb_settingsname.Text + "_min\", " + tb_settingsname.Text + ".Minimum.ToString());\r\n";
                new_settings_content += "\t\t\tAddKey(_config, \"" + tb_settingsname.Text + "_max\", " + tb_settingsname.Text + ".Maximum.ToString());\r\n";
            }

            int index_s6 = _settings_classfile_content.IndexOf(_s6);
            new_settings_content += "\t\t\t" + _settings_classfile_content.Substring(index_s4, index_s6 - index_s4);

            //add var info
            //public static StringConfigValue HbmAssemblyName = new StringConfigValue();
            new_settings_content += GetDataTypeEx();

            new_settings_content += "\t\t" + _settings_classfile_content.Substring(index_s6);

            _settings_classfile_content = new_settings_content;
            return true;
        }

        private string GetValueString()
        {
            bool bstring = rb_string.Checked;
            bool bint = rb_int.Checked;
            bool bfloat = rb_float.Checked;
            bool bv2float = rb_v2float.Checked;
            bool bv2int = rb_v2int.Checked;
            bool bbool = rb_bool.Checked;

            if (bstring) 
                return "\"" + tb_settingsdefvalue.Text + "\"";
            else if (bfloat)
                return tb_settingsdefvalue.Text + "f";
            else if(bv2float)
                return tb_settingsdefvalue.Text + "f, " + tb_settingsdefvalue2.Text + "f";
            else if(bv2int)
                return tb_settingsdefvalue.Text + ", " + tb_settingsdefvalue2.Text ;
            else
                return tb_settingsdefvalue.Text;
        }

        private string GetDataTypeEx()
        {
            bool bstring = rb_string.Checked;
            bool bint = rb_int.Checked;
            bool bfloat = rb_float.Checked;
            bool bbool = rb_bool.Checked;
            bool bv2float = rb_v2float.Checked;
            bool bv2int = rb_v2int.Checked;

            if (bstring || bbool || ((bfloat || bint || bv2float || bv2int) && (GetMaxValueString() == null || GetMinValueString() == null))) return "public static " + GetDataType() + " " + tb_settingsname.Text + " = new " + GetDataType() + "(\"" + tb_settingsname.Text + "\",\"" + tb_kommentar.Text + "\"," + GetValueString() + ");\r\n";
            if (bfloat || bint || bv2float || bv2int) return "public static " + GetDataType() + " " + tb_settingsname.Text + " = new " + GetDataType() + "(\"" + tb_settingsname.Text + "\",\"" + tb_kommentar.Text + "\"," + GetValueString() + "," + GetMinValueString() + "," + GetMaxValueString() + ");\r\n";
            return "";
        }

        private string GetMaxValueString()
        {
            if (string.IsNullOrEmpty(tb_maxvalue.Text))
                return null;

            bool bfloat = rb_float.Checked;
            bool bv2float = rb_v2float.Checked;

            if (bfloat || bv2float)
                return tb_maxvalue.Text + "f";

            return tb_maxvalue.Text;
        }

        private string GetMinValueString()
        {
            if (string.IsNullOrEmpty(tb_minvalue.Text))
                return null;

            bool bfloat = rb_float.Checked;
            bool bv2float = rb_v2float.Checked;

            if (bfloat || bv2float)
                return tb_minvalue.Text + "f";
            
            return tb_minvalue.Text;
        }

        private string GetDataType()
        {
            bool bstring = rb_string.Checked;
            bool bint = rb_int.Checked;
            bool bfloat = rb_float.Checked;
            bool bbool = rb_bool.Checked;
            bool bv2float = rb_v2float.Checked;
            bool bv2int = rb_v2int.Checked;

            if (bstring) return "StringConfigValue";
            if (bint) return "IntConfigValue";
            if (bfloat) return "FloatConfigValue";
            if (bbool) return "BoolConfigValue";
            if (bv2float) return "Vector2FloatConfigValue";
            if (bv2int) return "Vector2IntConfigValue";
            return "";
        }

        private string GetConvertFunctionName()
        {
            bool bstring = rb_string.Checked;
            bool bint = rb_int.Checked;
            bool bfloat = rb_float.Checked;
            bool bbool = rb_bool.Checked;
            bool bv2float = rb_v2float.Checked;
            bool bv2int = rb_v2int.Checked;

            if (bstring) return "Convert.ToString";
            if (bint || bv2int) return "Convert.ToInt32";
            if (bfloat || bv2float) return "Convert.ToSingle";
            if (bbool) return "Convert.ToBoolean";
            return "";
        }

        private bool AssertSettingName()
        {
            if (tb_settingsname.Text == "")
                return false;
            if (_settings_classfile_content.IndexOf(tb_settingsname.Text) >= 0)
                return false;
            //if (_settings_configfile_content.IndexOf(tb_settingsname.Text) >= 0)
            //    return false;
            return true;
        }

        private bool AssertKommentar()
        {
            if (tb_kommentar.Text == "")
                return false;

            return true;
        }

        /// <summary>
        /// checks if all values given, and if value suits datatype
        /// </summary>
        /// <returns></returns>
        private bool AssertDatatypeWithValue()
        {
            string value = tb_settingsdefvalue.Text;

            bool bstring = rb_string.Checked;
            bool bint = rb_int.Checked;
            bool bfloat = rb_float.Checked;
            bool bv2float = rb_v2float.Checked;
            bool bv2int = rb_v2int.Checked;
            bool bbool = rb_bool.Checked;

            if (!bbool && !bstring && !bint && !bfloat && !bv2float && !bv2int)
            {
                return false;
            }

            if (bstring) return true;

            if (bbool && value == "true" || value == "false") return true;


            float resf;
            if (bfloat && float.TryParse(value, out resf))
                return true;

            int resi;
            if ((bint && int.TryParse(value, out resi)))
            {
                if (!string.IsNullOrEmpty(tb_minvalue.Text) && !int.TryParse(tb_minvalue.Text, out resi))
                    return false;
                if (!string.IsNullOrEmpty(tb_maxvalue.Text) && !int.TryParse(tb_maxvalue.Text, out resi))
                    return false;
                return true;
            }


            float resf1, resf2;
            if (bv2float && float.TryParse(tb_settingsdefvalue.Text, out resf1) && float.TryParse(tb_settingsdefvalue2.Text, out resf2))
            {
                if (!string.IsNullOrEmpty(tb_minvalue.Text) && !float.TryParse(tb_minvalue.Text, out resf1))
                    return false;
                if (!string.IsNullOrEmpty(tb_maxvalue.Text) && !float.TryParse(tb_maxvalue.Text, out resf1))
                    return false;
                return true;
            }

            int resi1, resi2;
            if (bv2int && int.TryParse(tb_settingsdefvalue.Text, out resi1) && int.TryParse(tb_settingsdefvalue2.Text, out resi2))
            {
                if (!string.IsNullOrEmpty(tb_minvalue.Text) && !int.TryParse(tb_minvalue.Text, out resi1))
                    return false;
                if (!string.IsNullOrEmpty(tb_maxvalue.Text) && !int.TryParse(tb_maxvalue.Text, out resi1))
                    return false;
                return true;
            }

            try {
                if (bint)
                {
                    int.Parse(value, System.Globalization.NumberStyles.HexNumber);
                    return true;
                }
            }
            catch { }

            return false;
        }

        private void SettingsManager_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (_settings_changed) {
                if (MessageBox.Show("Do you want to save before quit?","Exiting",MessageBoxButtons.YesNo) == DialogResult.Yes)
                    saveSettingsToolStripMenuItem_Click(this,e);
            }
        }
    }
}