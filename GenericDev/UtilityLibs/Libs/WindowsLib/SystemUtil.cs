﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Management;
using System.Text;

namespace WindowsLib
{
    public class SystemUtil
    {
        /// <summary>
        /// Queries WMI and reads free space information
        /// </summary>
        public static double GetDiskFreeSpace(string drive_letter)
        {
            try
            {
                //create our ManagementObject, passing it the drive letter to the
                //DevideID using WQL
                ManagementObject disk = new ManagementObject("Win32_LogicalDisk.DeviceID=\"" + drive_letter + "\"");
                disk.Get();

                return Convert.ToDouble(disk["FreeSpace"]);
            }
            catch (Exception ex)
            {
#if DEBUG
                Debug.WriteLine("GetDiskFreeSpace(): " + ex.ToString());
#endif
            }
            return (double) -1;
        }
    }
}
