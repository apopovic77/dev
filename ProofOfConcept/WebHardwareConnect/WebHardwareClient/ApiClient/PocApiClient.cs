﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebHardwareClient.ApiClient
{
    public class PocApiClient
    {
        protected PocApiClient(string endpointUrl, string apiAccessToken = "")
        {
            ApiUrl = endpointUrl;
            AccessToken = apiAccessToken;
            EnsureApiUrl();
            Util = new ApiClientUtil(this);
            ApiVersion = 4;
        }

        /// <summary>
        /// Gets the ApiUrl used to communicate with the globe
        /// </summary>
        public virtual string ApiUrl { get; internal set; }
        /// <summary>
        /// Gets/Sets an API access token
        /// </summary>
        public virtual string AccessToken { get; set; }
        /// <summary>
        /// Gets the Api version used to communicate with the globe
        /// </summary>
        public virtual int ApiVersion { get; private set; }

        public virtual object LastResponse { get; set; }

        internal virtual ApiClientUtil Util { get; set; }

        private void EnsureApiUrl()
        {
            if (!ApiUrl.EndsWith("/"))
                ApiUrl += "/";

            if (!ApiUrl.EndsWith("api/"))
                ApiUrl += "api/";
        }
    }
}
