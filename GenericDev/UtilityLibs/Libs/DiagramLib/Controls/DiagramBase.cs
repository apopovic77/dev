﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using Logicx.DiagramLib.DiagramObjects.Objects;
using Logicx.DiagramLib.Resources.Converter;
using Logicx.DiagramLib.Scales;
using Logicx.DiagramLib.UIManipulation;
using Logicx.DiagramLib.ViewConfig;
using Logicx.Utilities;
using Logicx.WpfUtility.CustomControls.TouchButton;
using Logicx.WpfUtility.PieMenu;
using Logicx.WpfUtility.UiManipulation;
using Logicx.WpfUtility.WindowManagement;
using Logicx.WpfUtility.WpfHelpers;
using Windows7.Multitouch;
using DoWorkEventArgs = Logicx.Utilities.DoWorkEventArgs;
using RunWorkerCompletedEventArgs = Logicx.Utilities.RunWorkerCompletedEventArgs;

namespace Logicx.DiagramLib.Controls
{
    /// <summary>
    /// Delegate called for querying possible context pie menu entries
    /// </summary>
    /// <param name="agrument">argument instance</param>
    /// <returns>A list of pie menu entries for argument</returns>
    public delegate List<PieMenuEntry> QueryContextPieMenuEntriesCallback(object agrument);

    /// <summary>
    /// Delegate called before showing the context pie menu to update entry states
    /// </summary>
    /// <param name="argument">argument instance</param>
    /// <param name="entries">list of entries</param>
    public delegate void UpadeContextPieStateCallback(object argument, List<PieMenuEntry> entries);

    public enum DiagramMode
    {
        Normal,
        MultiSelect
    }

    public abstract class DiagramBase<Tx, Ty> : UserControl, INotifyPropertyChanged, IDisposable
    {
        #region Constants

        private const int X_AXIS_ZINDEX = 10;
        private const int Y_AXIS_ZINDEX = 9;

        private const int GRID_LINES_ZINDEX = 1;

        private const int BACKGROUND_ELEMENTS_ZINDEX = 5;

        private const int FOREGROUND_ELEMENTS_ZINDEX = 100;

        private const int LEGEND_ZINDEX = 200;

        private const int CURSORINFORMATION_ZINDEX = 199;

        private const int HEADER_ZINDEX = 250;

        private const int RIGHT_CLICK_ZINDEX = 550;

        private const double ZOOM_END_TIMEOUT = 500;

        private const int MODE_MENU_CANVAS_ZINDEX = 570;

        #endregion

        #region Construction and Initialization

        private static bool? _is_multitouch_enabled = null;

        internal static bool IsMultiTouchEnabled
        {
            get
            {
                if (!_is_multitouch_enabled.HasValue)
                    _is_multitouch_enabled = TouchHandler.DigitizerCapabilities.IsMultiTouchReady;

                return _is_multitouch_enabled.Value;
            }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="diagram_config"></param>
        public DiagramBase(DiagramConfig<Tx, Ty> diagram_config)
        {
            _diagram_config = diagram_config;
            _chart_data_objects.CollectionChanged += new System.Collections.Specialized.NotifyCollectionChangedEventHandler(_chart_data_objects_CollectionChanged);
            _chart_background_objects.CollectionChanged += new System.Collections.Specialized.NotifyCollectionChangedEventHandler(_chart_background_objects_CollectionChanged);

            ScaleFactory.GetInstance().AddScale(diagram_config.AxisScaleConfigX);
            ScaleFactory.GetInstance().AddScale(diagram_config.AxisScaleConfigY);

            Style selection_default_style = new Style(typeof(Rectangle));
            selection_default_style.Setters.Add(new Setter(Rectangle.StrokeProperty, new SolidColorBrush(Color.FromArgb(255, 0x8d, 0xae, 0xd9))));
            selection_default_style.Setters.Add(new Setter(Rectangle.FillProperty, new SolidColorBrush(Color.FromArgb(125, 0xc2, 0xd3, 0xeb))));
            selection_default_style.Setters.Add(new Setter(Rectangle.RadiusXProperty, (double)2));
            selection_default_style.Setters.Add(new Setter(Rectangle.RadiusYProperty, (double)2));

            SelectionRectangleStyle = selection_default_style;
            //InitialzeControl();
        }

        /// <summary>
        /// Initialize control base
        /// </summary>
        protected void InitializeControl()
        {
            this.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
            //this.VerticalAlignment = System.Windows.VerticalAlignment.Stretch;
            this.VerticalAlignment = System.Windows.VerticalAlignment.Bottom;

            _diagram_base_grid = new Grid();
            //_diagram_base_grid.Background = null; //TODO: Check if mouse clicks come to the base grid

            _base_grid = new Grid();
            _base_grid.IsHitTestVisible = true;
            _base_grid.Margin = new Thickness(0);
            _diagram_base_grid.Children.Add(_base_grid);

            #region Base grid column/row
            ColumnDefinition col0 = new ColumnDefinition();
            col0.Width = GridLength.Auto;
            ColumnDefinition col1 = new ColumnDefinition();
            col1.Width = new GridLength(1, GridUnitType.Star);


            _base_grid.ColumnDefinitions.Add(col0);
            _base_grid.ColumnDefinitions.Add(col1);


            RowDefinition row0 = new RowDefinition();
            row0.Height = GridLength.Auto;
            RowDefinition row1 = new RowDefinition();
            row1.Height = new GridLength(1, GridUnitType.Star);
            RowDefinition row2 = new RowDefinition();
            row2.Height = GridLength.Auto;

            _base_grid.RowDefinitions.Add(row0);
            _base_grid.RowDefinitions.Add(row1);
            _base_grid.RowDefinitions.Add(row2);
            #endregion

            #region Header container

            _header_container = new Grid();
            _header_container.Visibility = Visibility.Collapsed;
            _header_container.VerticalAlignment = System.Windows.VerticalAlignment.Stretch;
            _header_container.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
            Grid.SetRow(_header_container, 0);
            Grid.SetColumnSpan(_header_container, 2);
            Grid.SetZIndex(_header_container, HEADER_ZINDEX);
            _base_grid.Children.Add(_header_container);
            #endregion

            #region y-axis
            if (YAxisControl != null)
            {
                YAxisControl.Orientation = Orientation.Vertical;
                YAxisControl.VerticalAlignment = System.Windows.VerticalAlignment.Stretch;
                YAxisControl.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
                YAxisControl.MinWidth = 50;
                YAxisControl.MouseMove += new MouseEventHandler(YAxisControl_MouseMove);

                Grid.SetRowSpan(YAxisControl, 2);
                Grid.SetRow(YAxisControl, 1);
                Grid.SetColumn(YAxisControl, 0);
                Grid.SetZIndex(YAxisControl, Y_AXIS_ZINDEX);
                _base_grid.Children.Add(YAxisControl);

                Binding y_axis_visibility = new Binding("YAxisScaleVisibility");
                y_axis_visibility.Source = this;
                YAxisControl.SetBinding(VisibilityProperty, y_axis_visibility);

                // style and brush bindings
                Binding y_axis_text_style_binding = new Binding("YAxisScaleTextStyle");
                y_axis_text_style_binding.Source = this;
                YAxisControl.SetBinding(TypedScaleControl<Ty>.ScaleTextStyleProperty, y_axis_text_style_binding);

                Binding y_axis_infotext_style_binding = new Binding("YAxisScaleInfoTextStyle");
                y_axis_infotext_style_binding.Source = this;
                YAxisControl.SetBinding(TypedScaleControl<Ty>.ScaleInfoTextStyleProperty, y_axis_infotext_style_binding);

                Binding y_axis_line_brush_binding = new Binding("YAxisLineBrush");
                y_axis_line_brush_binding.Source = this;
                YAxisControl.SetBinding(TypedScaleControl<Ty>.LineBrushProperty, y_axis_line_brush_binding);

                Binding y_axis_control_style_binding = new Binding("YAxisScaleControlStyle");
                y_axis_control_style_binding.Source = this;
                YAxisControl.SetBinding(StyleProperty, y_axis_control_style_binding);

                Binding zoom_factor_binding = new Binding("ZoomfactorY");
                zoom_factor_binding.Source = this;
                zoom_factor_binding.Mode = BindingMode.TwoWay;
                YAxisControl.SetBinding(TypedScaleControl<Ty>.ZoomfactorProperty, zoom_factor_binding);

                Binding scale_offset_binding = new Binding("ScrollOffsetY");
                scale_offset_binding.Source = this;
                scale_offset_binding.Mode = BindingMode.TwoWay;
                YAxisControl.SetBinding(TypedScaleControl<Ty>.ScrollOffsetProperty, scale_offset_binding);

                //YAxisControl.Background = Brushes.Yellow;
            }
            else
            {
                Canvas tmp_control = new Canvas();
                tmp_control.VerticalAlignment = System.Windows.VerticalAlignment.Stretch;
                tmp_control.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
                tmp_control.MinWidth = 50;
                tmp_control.Background = new SolidColorBrush(Color.FromArgb(0x7f, 0x6f, 0x6f, 0xf6));
                tmp_control.MouseMove += new MouseEventHandler(tmp_control_MouseMove);
                Grid.SetRowSpan(tmp_control, 2);
                Grid.SetRow(tmp_control, 1);
                Grid.SetColumn(tmp_control, 0);
                Grid.SetZIndex(tmp_control, Y_AXIS_ZINDEX);
                _base_grid.Children.Add(tmp_control);

                Binding y_axis_visibility = new Binding("YAxisScaleVisibility");
                y_axis_visibility.Source = this;
                tmp_control.SetBinding(VisibilityProperty, y_axis_visibility);
            }


            #endregion

            #region x-axis

            if (XAxisControl != null)
            {
                XAxisControl.VerticalAlignment = System.Windows.VerticalAlignment.Bottom;
                XAxisControl.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
                XAxisControl.MinHeight = 50;
                XAxisControl.MouseMove += new MouseEventHandler(XAxisControl_MouseMove);
                Grid.SetColumnSpan(XAxisControl, 2);
                Grid.SetRow(XAxisControl, 2);
                Grid.SetZIndex(XAxisControl, X_AXIS_ZINDEX);
                _base_grid.Children.Add(XAxisControl);

                Binding x_axis_visibility = new Binding("XAxisScaleVisibility");
                x_axis_visibility.Source = this;
                XAxisControl.SetBinding(VisibilityProperty, x_axis_visibility);

                // style and brush bindings
                Binding x_axis_text_style_binding = new Binding("XAxisScaleTextStyle");
                x_axis_text_style_binding.Source = this;
                XAxisControl.SetBinding(TypedScaleControl<Tx>.ScaleTextStyleProperty, x_axis_text_style_binding);

                Binding x_axis_infotext_style_binding = new Binding("XAxisScaleInfoTextStyle");
                x_axis_infotext_style_binding.Source = this;
                XAxisControl.SetBinding(TypedScaleControl<Tx>.ScaleInfoTextStyleProperty, x_axis_infotext_style_binding);

                Binding x_axis_line_brush_binding = new Binding("XAxisLineBrush");
                x_axis_line_brush_binding.Source = this;
                XAxisControl.SetBinding(TypedScaleControl<Tx>.LineBrushProperty, x_axis_line_brush_binding);

                Binding x_axis_control_style_binding = new Binding("XAxisScaleControlStyle");
                x_axis_control_style_binding.Source = this;
                XAxisControl.SetBinding(StyleProperty, x_axis_control_style_binding);

                Binding zoom_factor_binding = new Binding("ZoomfactorX");
                zoom_factor_binding.Source = this;
                zoom_factor_binding.Mode = BindingMode.TwoWay;
                XAxisControl.SetBinding(TypedScaleControl<Tx>.ZoomfactorProperty, zoom_factor_binding);

                Binding scale_offset_binding = new Binding("ScrollOffsetX");
                scale_offset_binding.Source = this;
                scale_offset_binding.Mode = BindingMode.TwoWay;
                XAxisControl.SetBinding(TypedScaleControl<Tx>.ScrollOffsetProperty, scale_offset_binding);
            }
            else
            {
                Canvas tmp_control = new Canvas();
                tmp_control.VerticalAlignment = System.Windows.VerticalAlignment.Bottom;
                tmp_control.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
                tmp_control.MinHeight = 50;
                tmp_control.Background = new SolidColorBrush(Color.FromArgb(0x7f, 0xf6, 0xf6, 0xf6));
                tmp_control.MouseMove += new MouseEventHandler(tmp_control_MouseMove);
                Grid.SetColumnSpan(tmp_control, 2);
                Grid.SetRow(tmp_control, 2);
                Grid.SetZIndex(tmp_control, X_AXIS_ZINDEX);
                _base_grid.Children.Add(tmp_control);

                Binding x_axis_visibility = new Binding("XAxisScaleVisibility");
                x_axis_visibility.Source = this;
                tmp_control.SetBinding(VisibilityProperty, x_axis_visibility);
            }

            #endregion

            #region X/Y Axis bindings

            if (YAxisControl == null)
                DiagramConfig.AxisScaleConfigX.Padding = new Thickness(50, DiagramConfig.AxisScaleConfigX.Padding.Top, DiagramConfig.AxisScaleConfigX.Padding.Right, DiagramConfig.AxisScaleConfigX.Padding.Bottom);

            if (XAxisControl == null)
                DiagramConfig.AxisScaleConfigY.Padding = new Thickness(DiagramConfig.AxisScaleConfigY.Padding.Left, DiagramConfig.AxisScaleConfigY.Padding.Top, DiagramConfig.AxisScaleConfigY.Padding.Right, 50);

            if (YAxisControl != null && XAxisControl != null)
            {
                #region X-Axis control bindings
                // multi value converter for left padding
                XAxisLeftPaddingConverter left_padding_converter = new XAxisLeftPaddingConverter();

                MultiBinding left_padding_binding = new MultiBinding();

                Binding y_axis_visibility = new Binding("YAxisScaleVisibility");
                y_axis_visibility.Source = this;

                Binding current_yaxis_width = new Binding("ActualWidth");
                current_yaxis_width.Source = YAxisControl;
                current_yaxis_width.Mode = BindingMode.OneWay;

                left_padding_binding.Bindings.Add(y_axis_visibility);
                left_padding_binding.Bindings.Add(current_yaxis_width);

                left_padding_binding.Converter = left_padding_converter;
                left_padding_binding.ConverterParameter = DiagramConfig.AxisScaleConfigX.Padding;

                BindingOperations.SetBinding(DiagramConfig.AxisScaleConfigX, AxisScaleConfig<Tx>.PaddingProperty, left_padding_binding);

                #endregion

                #region Y-Axis control bindings
                // multi value converter for bottom padding
                YAxisBottomPaddingConverter bottom_padding_converter = new YAxisBottomPaddingConverter();

                MultiBinding bottom_padding_binding = new MultiBinding();

                Binding x_axis_visibility = new Binding("XAxisScaleVisibility");
                x_axis_visibility.Source = this;

                Binding current_xaxis_height = new Binding("ActualHeight");
                current_xaxis_height.Source = XAxisControl;
                current_xaxis_height.Mode = BindingMode.OneWay;

                bottom_padding_binding.Bindings.Add(x_axis_visibility);
                bottom_padding_binding.Bindings.Add(current_xaxis_height);

                bottom_padding_binding.Converter = bottom_padding_converter;
                bottom_padding_binding.ConverterParameter = DiagramConfig.AxisScaleConfigY.Padding;

                BindingOperations.SetBinding(DiagramConfig.AxisScaleConfigY, AxisScaleConfig<Ty>.PaddingProperty, bottom_padding_binding);

                #endregion
            }

            #endregion

            #region grid-lines
            _grid_lines = new GridLineCanvas();
            _grid_lines.Background = Brushes.Transparent;
            _grid_lines.XAxisControl = XAxisControl;
            _grid_lines.YAxisControl = YAxisControl;

            Binding grid_line_style_binding = new Binding("GridLineStyle");
            grid_line_style_binding.Source = this;
            _grid_lines.SetBinding(GridLineCanvas.GridLineStyleProperty, grid_line_style_binding);

            Binding grid_line_mode_binding = new Binding("GridLineVisibility");
            grid_line_mode_binding.Source = DiagramConfig;
            _grid_lines.SetBinding(GridLineCanvas.GridLineVisibilityModeProperty, grid_line_mode_binding);

            _grid_lines.VerticalAlignment = System.Windows.VerticalAlignment.Stretch;
            _grid_lines.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
            _grid_lines.IsHitTestVisible = false;

            Grid.SetRow(_grid_lines, 1);
            Grid.SetRowSpan(_grid_lines, 2);
            Grid.SetColumnSpan(_grid_lines, 2);
            Grid.SetZIndex(_grid_lines, GRID_LINES_ZINDEX);
            _base_grid.Children.Add(_grid_lines);
            #endregion

            #region background elements
            _background_elements_canvas = new Canvas();
            _background_elements_canvas.VerticalAlignment = System.Windows.VerticalAlignment.Stretch;
            //_background_elements_canvas.VerticalAlignment = System.Windows.VerticalAlignment.Bottom;
            _background_elements_canvas.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
            _background_elements_canvas.Background = Brushes.Transparent;
            //_background_elements_canvas.IsHitTestVisible = false;
            _background_elements_canvas.ClipToBounds = true;
            _background_elements_canvas.MouseLeave += new MouseEventHandler(_background_elements_canvas_MouseLeave);
            _background_elements_canvas.MouseDown += new MouseButtonEventHandler(_background_elements_canvas_MouseDown);
            _background_elements_canvas.MouseMove += new MouseEventHandler(_background_elements_canvas_MouseMove);
            _background_elements_canvas.MouseUp += new MouseButtonEventHandler(_background_elements_canvas_MouseUp);
            _background_elements_canvas.MouseWheel += new MouseWheelEventHandler(_background_elements_canvas_MouseWheel);
            _background_elements_canvas.StylusUp += new StylusEventHandler(_background_elements_canvas_StylusUp);

            if (IsMultiTouchEnabled)
            {
                //Stylus.SetIsTapFeedbackEnabled(_background_elements_canvas,true);
                Stylus.SetIsPressAndHoldEnabled(_background_elements_canvas, false);
                _scale_touch_intertia_manipulator = new DiagramScaleTouchInertiaProcessManipulation(_background_elements_canvas, _background_elements_canvas, _background_elements_canvas, UiManipulator.EnabledAxes.All);
                //_scale_touch_intertia_manipulator.WithTranslation = false;
                _scale_touch_intertia_manipulator.UiManipulationStarted += new EventHandler<UiManipulationEventArgs>(_scale_touch_intertia_manipulator_UiManipulationStarted);
                _scale_touch_intertia_manipulator.UiManipulationInertiaCompleted += new EventHandler<UiManipulationEventArgs>(_scale_touch_intertia_manipulator_UiManipulationInertiaCompleted);
                _scale_touch_intertia_manipulator.ScalingChanged += new EventHandler<DiagramScaleTouchInertiaProcessManipulation.DiagramScaleScaleingEventArgs>(_scale_touch_intertia_manipulator_ScalingChanged);
                _scale_touch_intertia_manipulator.TranslationChanged += new EventHandler<DiagramScaleTouchInertiaProcessManipulation.DiagrammScaleMoveEventArgs>(_scale_touch_intertia_manipulator_TranslationChanged);

                _scale_touch_intertia_manipulator.ShowRightClickSimulationFeedback += new EventHandler<DiagramScaleTouchInertiaProcessManipulation.ShowRightClickInfoEventArgs>(_scale_touch_intertia_manipulator_ShowRightClickSimulationFeedback);
                _scale_touch_intertia_manipulator.HideRightClickSimulationFeedback += new EventHandler(_scale_touch_intertia_manipulator_HideRightClickSimulationFeedback);

                _scale_touch_intertia_manipulator.StylusMoveWiredThrough += new StylusEventHandler(_scale_touch_intertia_manipulator_StylusMoveWiredThrough);
                _scale_touch_intertia_manipulator.StylusUpWiredThrough += new StylusEventHandler(_scale_touch_intertia_manipulator_StylusUpWiredThrough);
            }

            _background_scrolling = new Canvas();
            _background_scrolling.VerticalAlignment = System.Windows.VerticalAlignment.Stretch;
            _background_scrolling.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
            _background_scrolling.ClipToBounds = true;
            _background_scrolling.Background = null;
            _background_scrolling.MouseLeave += new MouseEventHandler(_background_scrolling_MouseLeave);
            _background_scrolling.StylusLeave += new StylusEventHandler(_background_scrolling_StylusLeave);

            _background_scrolling.Children.Add(_background_elements_canvas);

            Canvas.SetLeft(_background_elements_canvas, 0);
            Canvas.SetTop(_background_elements_canvas, 0);

            Grid.SetRow(_background_scrolling, 1);
            Grid.SetRowSpan(_background_scrolling, 2);
            Grid.SetColumnSpan(_background_scrolling, 2);
            Grid.SetZIndex(_background_scrolling, BACKGROUND_ELEMENTS_ZINDEX);
            _base_grid.Children.Add(_background_scrolling);

            //Grid.SetRow(_background_elements_canvas, 1);
            //Grid.SetRowSpan(_background_elements_canvas, 2);
            //Grid.SetColumnSpan(_background_elements_canvas, 2);
            //Grid.SetZIndex(_background_elements_canvas, BACKGROUND_ELEMENTS_ZINDEX);
            //_base_grid.Children.Add(_background_elements_canvas);
            #endregion

            #region elements
            _elements_canvas = new Canvas();
            _elements_canvas.VerticalAlignment = System.Windows.VerticalAlignment.Stretch;
            _elements_canvas.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
            _elements_canvas.Background = Brushes.Transparent;

            _elements_canvas.ClipToBounds = true;
            _elements_canvas.IsHitTestVisible = true;

            _elements_canvas.MouseDown += new MouseButtonEventHandler(_elements_canvas_MouseDown);
            _elements_canvas.MouseUp += new MouseButtonEventHandler(_elements_canvas_MouseUp);
            _elements_canvas.MouseMove += new MouseEventHandler(_elements_canvas_MouseMove);
            _elements_canvas.StylusDown += new StylusDownEventHandler(_elements_canvas_StylusDown);
            _elements_canvas.StylusUp += new StylusEventHandler(_elements_canvas_StylusUp);
            _elements_canvas.StylusMove += new StylusEventHandler(_elements_canvas_StylusMove);

            Binding element_Canvas_style_binding = new Binding("ElementCanvasStyle");
            element_Canvas_style_binding.Source = this;
            _elements_canvas.SetBinding(Canvas.StyleProperty, element_Canvas_style_binding);

            _elements_canvas.Margin = new Thickness(0);

            _element_scrolling = new Canvas();
            _element_scrolling.VerticalAlignment = System.Windows.VerticalAlignment.Stretch;
            _element_scrolling.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
            _element_scrolling.ClipToBounds = true;
            _element_scrolling.Background = null;
            _element_scrolling.Margin = new Thickness(0);

            _element_scrolling.SizeChanged += new SizeChangedEventHandler(_element_scrolling_SizeChanged);
            _element_scrolling.StylusLeave += new StylusEventHandler(_element_scrolling_StylusLeave);
            _element_scrolling.MouseLeave += new MouseEventHandler(_element_scrolling_MouseLeave);

            _element_scrolling.Children.Add(_elements_canvas);

            Grid.SetColumn(_element_scrolling, 1);
            Grid.SetRow(_element_scrolling, 1);
            Grid.SetZIndex(_element_scrolling, FOREGROUND_ELEMENTS_ZINDEX);
            Canvas.SetLeft(_elements_canvas, 0);
            Canvas.SetTop(_elements_canvas, 0);
            _base_grid.Children.Add(_element_scrolling);

            //Grid.SetColumn(_elements_canvas, 1);
            //Grid.SetRow(_elements_canvas, 1);
            //Grid.SetZIndex(_elements_canvas, FOREGROUND_ELEMENTS_ZINDEX);
            //_base_grid.Children.Add(_elements_canvas);

            DisableElementCanvasMouseCapture();
            #endregion

            #region legende

            _legend_border = new Border();

            Binding style_binding = new Binding("LegendBorderStyle");
            style_binding.Source = this;
            _legend_border.SetBinding(StyleProperty, style_binding);

            if (LegendBorderStyle == null)
            {
                // create default style
                Style default_border_style = new Style(typeof(Border));
                default_border_style.Setters.Add(new Setter(Border.BorderThicknessProperty, new Thickness(1)));
                default_border_style.Setters.Add(new Setter(Border.BackgroundProperty, new SolidColorBrush(Color.FromArgb(0xCF, 0xE1, 0xE1, 0xE1))));
                default_border_style.Setters.Add(new Setter(Border.BorderBrushProperty, new SolidColorBrush(Color.FromArgb(0xFF, 0xa1, 0xa1, 0xa1))));
                default_border_style.Setters.Add(new Setter(Border.CornerRadiusProperty, new CornerRadius(6)));

                default_border_style.Setters.Add(new Setter(Border.MarginProperty, new Thickness(0, 10, 10, 0)));
                default_border_style.Setters.Add(new Setter(Border.HorizontalAlignmentProperty, System.Windows.HorizontalAlignment.Right));
                default_border_style.Setters.Add(new Setter(Border.VerticalAlignmentProperty, System.Windows.VerticalAlignment.Top));

                default_border_style.Setters.Add(new Setter(Border.IsHitTestVisibleProperty, false));

                LegendBorderStyle = default_border_style;
            }

            _legend_border.Visibility = System.Windows.Visibility.Collapsed;

            Grid.SetRow(_legend_border, 1);
            Grid.SetRowSpan(_legend_border, 2);
            Grid.SetColumnSpan(_legend_border, 2);
            Grid.SetZIndex(_legend_border, LEGEND_ZINDEX);

            _base_grid.Children.Add(_legend_border);
            #endregion

            #region cursor information canvas

            Binding cursor_line_style_binding = new Binding("CursorLineStyle");
            cursor_line_style_binding.Source = this;

            _cursor_information_canvas = new Canvas();
            _cursor_information_canvas.VerticalAlignment = System.Windows.VerticalAlignment.Stretch;
            _cursor_information_canvas.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
            _cursor_information_canvas.IsHitTestVisible = false;
            _cursor_information_canvas.Background = null;
            _cursor_information_canvas.Visibility = Visibility.Collapsed;
            Grid.SetRow(_cursor_information_canvas, 1);
            Grid.SetRowSpan(_cursor_information_canvas, 2);
            Grid.SetColumnSpan(_cursor_information_canvas, 2);
            Grid.SetZIndex(_cursor_information_canvas, CURSORINFORMATION_ZINDEX);


            _cursor_horizontal_line = new Line();
            _cursor_horizontal_line.X1 = 0;
            Binding width_binding = new Binding("ActualWidth");
            width_binding.Source = this;
            _cursor_horizontal_line.SetBinding(Line.X2Property, width_binding);
            _cursor_horizontal_line.Y1 = _cursor_horizontal_line.Y2 = 0;
            _cursor_horizontal_line.SnapsToDevicePixels = true;
            Canvas.SetTop(_cursor_horizontal_line, 0);
            Canvas.SetLeft(_cursor_horizontal_line, 0);


            _cursor_horizontal_line.SetBinding(StyleProperty, cursor_line_style_binding);

            _cursor_vertical_line = new Line();
            _cursor_vertical_line.Y1 = 0;
            Binding height_binding = new Binding("ActualHeight");
            height_binding.Source = this;
            _cursor_vertical_line.SetBinding(Line.Y2Property, height_binding);
            _cursor_vertical_line.X1 = _cursor_vertical_line.X2 = 0;
            _cursor_vertical_line.SnapsToDevicePixels = true;
            Canvas.SetTop(_cursor_vertical_line, 0);
            Canvas.SetLeft(_cursor_vertical_line, 0);

            _cursor_vertical_line.SetBinding(StyleProperty, cursor_line_style_binding);

            _curor_information_tb = new TextBlock();
            _curor_information_tb.FontFamily = new FontFamily("Calibri");
            _curor_information_tb.FontSize = 9;
            //_curor_information_tb.FontSize = 14;
            //_curor_information_tb.FontWeight = FontWeights.Bold;

            _cursor_information_canvas.Children.Add(_cursor_horizontal_line);
            _cursor_information_canvas.Children.Add(_cursor_vertical_line);
            _cursor_information_canvas.Children.Add(_curor_information_tb);

            if (CursorLineStyle == null)
            {
                // create default style
                Style default_line_style = new Style(typeof(Line));
                default_line_style.Setters.Add(new Setter(Line.StrokeThicknessProperty, (double)0.5));
                default_line_style.Setters.Add(new Setter(Line.StrokeProperty, new SolidColorBrush(Color.FromArgb(0xCF, 0xA1, 0xA1, 0xA1))));
                default_line_style.Setters.Add(new Setter(Line.StrokeProperty, new SolidColorBrush(Color.FromArgb(0xCF, 0xA1, 0xA1, 0xA1))));
                default_line_style.Setters.Add(new Setter(Line.IsHitTestVisibleProperty, false));
                default_line_style.Setters.Add(new Setter(Line.StrokeDashCapProperty, PenLineCap.Square));


                CursorLineStyle = default_line_style;
            }

            _base_grid.Children.Add(_cursor_information_canvas);
            #endregion

            _base_grid.MouseMove += new MouseEventHandler(_base_grid_MouseMove);

            #region loading grid

            _loading_grid = new Grid();
            _loading_grid.Visibility = Visibility.Hidden;
            _loading_grid.VerticalAlignment = System.Windows.VerticalAlignment.Stretch;
            _loading_grid.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
            _loading_grid.IsHitTestVisible = false;
            _loading_grid.Opacity = 0;

            LoadingControl loading_control = new LoadingControl();
            loading_control.HorizontalAlignment = HorizontalAlignment.Center;
            loading_control.VerticalAlignment = VerticalAlignment.Center;

            _diagram_base_grid.Children.Add(_loading_grid);

            LoadingControl = loading_control;
            #endregion

            #region reset zoom button
            _mode_menu_button = new TouchButton();

            if (XAxisControl != null)
            {
                Binding button_max_height = new Binding("ActualHeight");
                button_max_height.Source = XAxisControl;
                _mode_menu_button.SetBinding(MaxHeightProperty, button_max_height);
            }

            if (YAxisControl != null)
            {
                Binding button_width_height = new Binding("ActualWidth");
                button_width_height.Source = YAxisControl;
                _mode_menu_button.SetBinding(MaxWidthProperty, button_width_height);
                _mode_menu_button.Margin = new Thickness(0);
            }

            _mode_menu_button.Click += new RoutedEventHandler(_mode_menu_button_Click);
            Grid.SetColumn(_mode_menu_button, 0);
            Grid.SetRow(_mode_menu_button, 3);

            _toolbutton_content = new ToolButtonContent();

            Binding tool_button_stroke_binding = new Binding("ModeMenuStrokeBrush");
            tool_button_stroke_binding.Source = this;
            _toolbutton_content.SetBinding(ToolButtonContent.StrokeBrushProperty, tool_button_stroke_binding);

            Binding tool_button_background_binding = new Binding("ModeMenuBackgroundBrush");
            tool_button_background_binding.Source = this;
            _toolbutton_content.SetBinding(ToolButtonContent.BackgroundBrushProperty, tool_button_background_binding);

            Binding tool_button_line_binding = new Binding("ModeMenuLineBrush");
            tool_button_line_binding.Source = this;
            _toolbutton_content.SetBinding(ToolButtonContent.LineBrushProperty, tool_button_line_binding);


            _reset_zoom_button_content = new ResetZoomButtonContent();

            Binding reset_zoom_button_stroke_binding = new Binding("ModeMenuStrokeBrush");
            reset_zoom_button_stroke_binding.Source = this;
            _reset_zoom_button_content.SetBinding(ResetZoomButtonContent.StrokeBrushProperty, reset_zoom_button_stroke_binding);

            Binding reset_zoom_button_background_binding = new Binding("ModeMenuBackgroundBrush");
            reset_zoom_button_background_binding.Source = this;
            _reset_zoom_button_content.SetBinding(ResetZoomButtonContent.BackgroundBrushProperty, reset_zoom_button_background_binding);

            Binding reset_zoom_button_line_binding = new Binding("ModeMenuLineBrush");
            reset_zoom_button_line_binding.Source = this;
            _reset_zoom_button_content.SetBinding(ResetZoomButtonContent.LineBrushProperty, reset_zoom_button_line_binding);

            Binding reset_zoom_button_zoomactive_binding = new Binding("IsZoomActive");
            reset_zoom_button_zoomactive_binding.Source = this;
            _reset_zoom_button_content.SetBinding(ResetZoomButtonContent.IsZoomActiveProperty, reset_zoom_button_zoomactive_binding);


            _mehrfachauswahl_button_content = new MehrfachAuswahlButtonContent();

            Binding mehrfach_auswahl_button_stroke_binding = new Binding("ModeMenuStrokeBrush");
            mehrfach_auswahl_button_stroke_binding.Source = this;
            _mehrfachauswahl_button_content.SetBinding(MehrfachAuswahlButtonContent.StrokeBrushProperty, mehrfach_auswahl_button_stroke_binding);

            Binding mehrfach_auswahl_button_background_binding = new Binding("ModeMenuBackgroundBrush");
            mehrfach_auswahl_button_background_binding.Source = this;
            _mehrfachauswahl_button_content.SetBinding(MehrfachAuswahlButtonContent.BackgroundBrushProperty, mehrfach_auswahl_button_background_binding);

            Binding mehrfach_auswahl_button_line_binding = new Binding("ModeMenuLineBrush");
            mehrfach_auswahl_button_line_binding.Source = this;
            _mehrfachauswahl_button_content.SetBinding(MehrfachAuswahlButtonContent.LineBrushProperty, mehrfach_auswahl_button_line_binding);

            Binding mehrfach_auswahl_button_active_binding = new Binding("IsMehrfachAuswahlActive");
            mehrfach_auswahl_button_active_binding.Source = this;
            _mehrfachauswahl_button_content.SetBinding(MehrfachAuswahlButtonContent.IsMehrfachAuswahlActiveProperty, mehrfach_auswahl_button_active_binding);

            _mode_menu_button.Content = _mehrfachauswahl_button_content;



            _mode_menu_button.OverridesDefaultStyle = true;
            _mode_menu_button.Style = (Style)FindResource("reset_zoom_button_style");
            _mode_menu_button.Visibility = System.Windows.Visibility.Collapsed;

            Grid.SetZIndex(_mode_menu_button, HEADER_ZINDEX);

            _base_grid.Children.Add(_mode_menu_button);
            #endregion

            this.Content = _diagram_base_grid;


            #region Right-Click stylus feedback


            _drop_shadow_right_click_animation = new DropShadowEffect();
            _drop_shadow_right_click_animation.ShadowDepth = 3.7;
            _drop_shadow_right_click_animation.Opacity = 0.6;
            _drop_shadow_right_click_animation.Color = Colors.Black;
            _drop_shadow_right_click_animation.Direction = 315;

            _drop_shadow_right_click_animation_finished = new DropShadowEffect();
            _drop_shadow_right_click_animation_finished.ShadowDepth = 0;
            _drop_shadow_right_click_animation_finished.Opacity = 1;
            _drop_shadow_right_click_animation_finished.BlurRadius = 6;
            _drop_shadow_right_click_animation_finished.Color = Color.FromArgb(0xff, 0x03, 0xad, 0x1b);

            _right_click_feedback_circle = new Path();
            _right_click_feedback_circle.Visibility = System.Windows.Visibility.Collapsed;
            _right_click_feedback_circle.Data = Geometry.Parse("F1 M 36.3334,0C 36.3334,-20.0663 20.0664,-36.3334 0,-36.3334C -20.0663,-36.3334 -36.3334,-20.0663 -36.3334,0C -36.3334,20.0663 -20.0663,36.3334 0,36.3334C 20.0664,36.3334 36.3334,20.0663 36.3334,0 Z M -31.125,0C -31.125,-17.1898 -17.1899,-31.1249 -2.03451e-005,-31.1249C 17.1898,-31.1249 31.125,-17.1898 31.125,0C 31.125,17.1899 17.1898,31.125 -2.03451e-005,31.125C -17.1899,31.125 -31.125,17.1899 -31.125,0 Z ");
            _right_click_feedback_circle.Stroke = Brushes.DarkGray;
            _right_click_feedback_circle.Fill = GenerateFeedbackBrush();
            _right_click_feedback_circle.Width = 74;
            _right_click_feedback_circle.Height = 74;

            Canvas.SetLeft(_right_click_feedback_circle, 100);
            Canvas.SetTop(_right_click_feedback_circle, 100);

            _right_click_color_animation_path = new EllipseGeometry(default(Point), 0.5, 0.5);
            _right_click_color_animation_path.Freeze();

            _right_click_information_canvas = new Canvas();
            _right_click_information_canvas.VerticalAlignment = System.Windows.VerticalAlignment.Stretch;
            _right_click_information_canvas.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
            _right_click_information_canvas.IsHitTestVisible = false;
            _right_click_information_canvas.Background = null;
            _right_click_information_canvas.Visibility = Visibility.Visible;
            Grid.SetRow(_right_click_information_canvas, 1);
            Grid.SetRowSpan(_right_click_information_canvas, 2);
            Grid.SetColumnSpan(_right_click_information_canvas, 2);
            Grid.SetZIndex(_right_click_information_canvas, RIGHT_CLICK_ZINDEX);

            _right_click_information_canvas.Children.Add(_right_click_feedback_circle);


            _base_grid.Children.Add(_right_click_information_canvas);

            #endregion

            #region Mode Menu

            _mode_menu_canvas = new Canvas();
            _mode_menu_canvas.VerticalAlignment = System.Windows.VerticalAlignment.Stretch;
            _mode_menu_canvas.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
            //_mode_menu_canvas.IsHitTestVisible = false;
            _mode_menu_canvas.Background = null;
            _mode_menu_canvas.Visibility = Visibility.Visible;
            Grid.SetRow(_mode_menu_canvas, 1);
            Grid.SetRowSpan(_mode_menu_canvas, 2);
            Grid.SetColumnSpan(_mode_menu_canvas, 2);
            Grid.SetZIndex(_mode_menu_canvas, MODE_MENU_CANVAS_ZINDEX);


            _mode_menu_grid = new Grid();
            ScaleTransform sc_grid = new ScaleTransform();
            sc_grid.ScaleX = 0;
            sc_grid.ScaleY = 0;


            Binding center_y_binding = new Binding("ActualHeight");
            center_y_binding.Source = _mode_menu_grid;
            BindingOperations.SetBinding(sc_grid, ScaleTransform.CenterYProperty, center_y_binding);

            _mode_menu_grid.RenderTransform = sc_grid;

            ModeMenu mode_menu_control = new ModeMenu();
            _mode_menu_grid.Children.Add(mode_menu_control);

            mode_menu_control.ResetZoomClick += new EventHandler(mode_menu_control_ResetZoomClick);
            mode_menu_control.MehrfachAuswahlClick += new EventHandler(mode_menu_control_MehrfachAuswahlClick);
            mode_menu_control.ButtonClick += new EventHandler(mode_menu_control_ButtonClick);

            Binding mode_menu_stroke_binding = new Binding("ModeMenuStrokeBrush");
            mode_menu_stroke_binding.Source = this;
            mode_menu_control.SetBinding(ModeMenu.StrokeBrushProperty, mode_menu_stroke_binding);

            Binding mode_menu_background_binding = new Binding("ModeMenuBackgroundBrush");
            mode_menu_background_binding.Source = this;
            mode_menu_control.SetBinding(ModeMenu.BackgroundBrushProperty, mode_menu_background_binding);

            Binding mode_menu_line_binding = new Binding("ModeMenuLineBrush");
            mode_menu_line_binding.Source = this;
            mode_menu_control.SetBinding(ModeMenu.LineBrushProperty, mode_menu_line_binding);

            Binding mode_menu_textgloweffect_binding = new Binding("ModeTextGlowBitmapEffect");
            mode_menu_textgloweffect_binding.Source = this;
            mode_menu_control.SetBinding(ModeMenu.TextGlowBitmapEffectProperty, mode_menu_textgloweffect_binding);

            Binding mode_menu_iazoomactive_binding = new Binding("IsZoomActive");
            mode_menu_iazoomactive_binding.Source = this;
            mode_menu_control.SetBinding(ModeMenu.IsZoomActiveProperty, mode_menu_iazoomactive_binding);

            Binding mode_menu_mehrfachauswahlactive_binding = new Binding("IsMehrfachAuswahlActive");
            mode_menu_mehrfachauswahlactive_binding.Source = this;
            mode_menu_control.SetBinding(ModeMenu.IsMehrfachAuswahlActiveProperty, mode_menu_mehrfachauswahlactive_binding);

            _mode_menu_canvas.Children.Add(_mode_menu_grid);

            _base_grid.Children.Add(_mode_menu_canvas);
            #endregion

            #region multi selection rectangle

            _selection_rectangle = new Rectangle();
            _selection_rectangle.IsHitTestVisible = false;


            Binding selection_rectangle_styling = new Binding("SelectionRectangleStyle");
            selection_rectangle_styling.Source = this;
            _selection_rectangle.SetBinding(StyleProperty, selection_rectangle_styling);

            _selection_rectangle.Width = 0;
            _selection_rectangle.Height = 0;
            Canvas.SetLeft(_selection_rectangle, 0);
            Canvas.SetTop(_selection_rectangle, 0);
            Canvas.SetZIndex(_selection_rectangle, int.MaxValue);

            _mode_menu_canvas.Children.Add(_selection_rectangle);

            #endregion

            InitializeDiagramControl();
        }



        private RadialGradientBrush GenerateFeedbackBrush()
        {
            RadialGradientBrush _origin_feedback_circle_animation_brush = new RadialGradientBrush();
            _origin_feedback_circle_animation_brush.RadiusX = 0.5;
            _origin_feedback_circle_animation_brush.RadiusY = 0.5;
            _origin_feedback_circle_animation_brush.Center = new Point(0.5, 0.5);
            _origin_feedback_circle_animation_brush.GradientOrigin = new Point(0.5, 0);

            GradientStop white_stop1 = new GradientStop(Colors.White, 0);
            GradientStop white_stop2 = new GradientStop(Colors.White, 0.17);
            GradientStop gray_stop1 = new GradientStop(Colors.LightGray, 0.5);
            GradientStop gray_stop2 = new GradientStop(Colors.LightGray, 1);

            _origin_feedback_circle_animation_brush.GradientStops.Add(white_stop1);
            _origin_feedback_circle_animation_brush.GradientStops.Add(white_stop2);
            _origin_feedback_circle_animation_brush.GradientStops.Add(gray_stop1);
            _origin_feedback_circle_animation_brush.GradientStops.Add(gray_stop2);

            return _origin_feedback_circle_animation_brush;
        }

        protected virtual void InitializeDiagramControl()
        {

        }

        public void AddForeGroundElement(UIElement element)
        {
            _diagram_base_grid.Children.Add(element);
        }

        private Point GetModeMenuPoint()
        {
            Point ret_point = new Point();

            ret_point.Y = _mode_menu_canvas.ActualHeight;
            if (XAxisControl != null && XAxisScaleVisibility == Visibility.Visible)
                ret_point.Y -= XAxisControl.ActualHeight;

            ret_point.X = 0;
            if (YAxisControl != null && YAxisScaleVisibility == Visibility.Visible)
                ret_point.X += YAxisControl.ActualWidth;

            return ret_point;
        }
        #endregion

        #region Operations

        #region abstract operations
        /// <summary>
        /// Gets the scale control for the x-axis
        /// </summary>
        /// <returns></returns>
        protected abstract TypedScaleControl<Tx> GetXAxisControl();
        /// <summary>
        /// Gets the scale control for the y-axis
        /// </summary>
        /// <returns></returns>
        protected abstract TypedScaleControl<Ty> GetYAxisControl();

        /// <summary>
        /// Transforms a X-axis scale value to the pixel value
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        protected abstract double TransformXAxisValueToPixel(Tx value);
        /// <summary>
        /// Transforms a Y-axis scale value to the pixel value
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        protected abstract double TransformYAxisValueToPixel(Ty value);
        /// <summary>
        /// Transforms a X-axis pixel value to the scale value
        /// </summary>
        /// <param name="pixel"></param>
        /// <returns></returns>
        protected abstract Tx TransformXAxisPixelToValue(double pixel);
        /// <summary>
        /// Transforms a y-axis pixel value to the scale value
        /// </summary>
        /// <param name="pixel"></param>
        /// <returns></returns>
        protected abstract Ty TransformYAxisPixelToValue(double pixel);

        #endregion

        #region Y-Axis zoom animations

        private void OnYZoomInCompleted()
        {
            if (YAxisZoomCompleted != null)
                YAxisZoomCompleted(this, EventArgs.Empty);
        }

        protected void ZoomYEnd(double zoom_factor, List<Canvas> overlay_canvases, double canvas_target_opacity)
        {
            ZoomYEnd(zoom_factor, overlay_canvases, canvas_target_opacity, 300);
        }

        protected void ZoomYEnd(double zoom_factor, List<Canvas> overlay_canvases, double canvas_target_opacity, double duration)
        {
            double final_zoom_y = ZoomfactorY * zoom_factor;
            DoubleAnimation zoom_animation = new DoubleAnimation(ZoomfactorY, final_zoom_y, new Duration(TimeSpan.FromMilliseconds(duration)));
            Storyboard.SetTargetProperty(zoom_animation, new PropertyPath(ZoomfactorYProperty));

            foreach (var overlay_canvas in overlay_canvases)
            {
                DoubleAnimation opacity_anim = new DoubleAnimation(canvas_target_opacity, new Duration(TimeSpan.FromMilliseconds(duration)));
                Storyboard.SetTargetProperty(opacity_anim, new PropertyPath(Canvas.OpacityProperty));

                overlay_canvas.BeginAnimation(Canvas.OpacityProperty, opacity_anim);
            }

            zoom_animation.Completed += zoom_animation_Completed;
            BeginAnimation(ZoomfactorYProperty, zoom_animation);
        }

        void zoom_animation_Completed(object sender, EventArgs e)
        {
            // Reset animated property
            double current_zoom_y = ZoomfactorY;
            BeginAnimation(ZoomfactorYProperty, null);
            ZoomfactorY = current_zoom_y;

            OnYZoomInCompleted();
        }

        public virtual void XAxisZoomIn(EventArgs e)
        {
            Point position = default(Point);
            if (e is MouseWheelEventArgs)
                position = ((MouseWheelEventArgs)e).GetPosition(ElementCanvas);
            else if (e is DiagramScaleTouchInertiaProcessManipulation.DiagramScaleScaleingEventArgs)
                position = ((DiagramScaleTouchInertiaProcessManipulation.DiagramScaleScaleingEventArgs)e).DownPosition;

            XAxisZoom(position, 1.1);
        }

        public virtual void XAxisZoomOut(EventArgs e)
        {
            Point position = default(Point);
            if (e is MouseWheelEventArgs)
                position = ((MouseWheelEventArgs)e).GetPosition(ElementCanvas);
            else if (e is DiagramScaleTouchInertiaProcessManipulation.DiagramScaleScaleingEventArgs)
                position = ((DiagramScaleTouchInertiaProcessManipulation.DiagramScaleScaleingEventArgs)e).DownPosition;

            XAxisZoom(position, 0.9);
        }

        private void XAxisZoomIn(Point position)
        {
            XAxisZoom(position, 1 / 0.9);
        }

        private void XAxisZoomOut(Point position)
        {
            XAxisZoom(position, 0.9);
        }

        private void XAxisZoom(Point position, double zoom_factor)
        {
            if (CanZoomXAxis)
            {
                double scroll_offset_x_old = ScrollOffsetX;
                double z_x = Math.Round(ZoomfactorX * zoom_factor, 10);
                double move_x = position.X * (1 - zoom_factor);

                if (z_x >= 1)
                    ZoomfactorX = z_x;

                if (z_x >= 1)
                    ScrollOffsetX = scroll_offset_x_old - move_x;
            }
        }

        public virtual void DiagramZoomIn(double left, double top)
        {
            DiagramZoom(left, top, 1 / 0.9);
        }

        public virtual void DiagramZoomOut(double left, double top)
        {
            DiagramZoom(left, top, 0.9);
        }

        private void DiagramZoom(double left, double top, double zoom_factor)
        {
            if (CanWheel)
            {
                double scroll_offset_x_old = ScrollOffsetX;
                double scroll_offset_y_old = ScrollOffsetY;

                //Debug.WriteLine("MMMMMM Mouse positions " + p + " " + GetMousePosition(e));
                //Debug.WriteLine("  Scrolloffests " + scroll_offset_x_old + " " + scroll_offset_y_old);

                //Debug.WriteLine("  Mouse positions after " + p_after + " " + GetMousePosition(e));

                double z_x = Math.Round(ZoomfactorX * zoom_factor, 10);
                double z_y = Math.Round(ZoomfactorY * zoom_factor, 10);

                if (z_x >= 1)
                    ZoomfactorX = z_x;
                if (z_y >= 1)
                    ZoomfactorY = z_y;

                //Debug.WriteLine("  SCROLLOFFSETS AFTER ZOOM FACTOR APPLICATION " + ScrollOffsetX + " " + ScrollOffsetY);

                double move_x = left * (1 - zoom_factor);
                double move_y = top * (1 - zoom_factor);
                //Debug.WriteLine(" Move calculated: " + move_x + " " + move_y);
                //Debug.WriteLine("  Move: " + move_x + " " + move_y);

                if (z_x >= 1)
                    ScrollOffsetX = scroll_offset_x_old - move_x;
                if (z_y >= 1)
                    ScrollOffsetY = scroll_offset_y_old - move_y;

                //Debug.WriteLine("   Scrolloffests3 " + ScrollOffsetX + " " + ScrollOffsetY);
            }

        }

        public virtual void ResetZoom()
        {
            ZoomfactorX = 1;
            ZoomfactorY = 1;

            ScrollOffsetX = 0;

            // IsZoomActive wird automatisch auf false gesetzt nachdem die zwei zoomfaktor werte auf 1 fgestellt werden.
            UpdateModeMenuButtonState();
        }

        public virtual void DiagramWheel(object sender, MouseWheelEventArgs e)
        {
            if (CanWheel)
            {
                Point p = e.GetPosition(ElementCanvas);

                double scroll_offset_x_old = ScrollOffsetX;
                double scroll_offset_y_old = ScrollOffsetY;

                //Debug.WriteLine("MMMMMM Mouse positions " + p + " " + GetMousePosition(e));
                //Debug.WriteLine("  Scrolloffests " + scroll_offset_x_old + " " + scroll_offset_y_old);

                double factor;
                if (e.Delta > 0)
                    factor = 1 / 0.9;
                else
                    factor = 0.9;

                //Debug.WriteLine("  Mouse positions after " + p_after + " " + GetMousePosition(e));

                double z_x = Math.Round(ZoomfactorX * factor, 10);
                double z_y = Math.Round(ZoomfactorY * factor, 10);

                if (z_x >= 1)
                    ZoomfactorX = z_x;
                if (z_y >= 1)
                    ZoomfactorY = z_y;

                //Debug.WriteLine("  SCROLLOFFSETS AFTER ZOOM FACTOR APPLICATION " + ScrollOffsetX + " " + ScrollOffsetY);

                double move_x = p.X * (1 - factor);
                double move_y = p.Y * (1 - factor);
                //Debug.WriteLine(" Move calculated: " + move_x + " " + move_y);
                //Debug.WriteLine("  Move: " + move_x + " " + move_y);

                if (z_x >= 1)
                    ScrollOffsetX = scroll_offset_x_old - move_x;
                if (z_y >= 1)
                    ScrollOffsetY = scroll_offset_y_old - move_y;

                //Debug.WriteLine("   Scrolloffests3 " + ScrollOffsetX + " " + ScrollOffsetY);
            }
        }



        public virtual void DiagramTouchZoom(DiagramScaleTouchInertiaProcessManipulation.DiagramScaleScaleingEventArgs e)
        {
            if (CanWheel)
            {
                //if (e.DeltaScaling > 1)
                //    DiagramZoomIn(e.DownPosition.X, e.DownPosition.Y);
                //else
                //    DiagramZoomOut(e.DownPosition.X, e.DownPosition.Y);
                if (e.DeltaScaling != 0)
                    DiagramZoom(e.DownPosition.X, e.DownPosition.Y, 1 + e.DeltaScaling);
            }
        }

        public virtual void DiagramTouchScroll(DiagramScaleTouchInertiaProcessManipulation.DiagrammScaleMoveEventArgs e)
        {
            //Debug.WriteLine("Height: " + e.DeltaHeight + ", Width: " + e.DeltaWidth);
            if (e.DeltaHeight != 0)
                ScrollOffsetY -= e.DeltaHeight;
            if (e.DeltaWidth != 0)
                ScrollOffsetX -= e.DeltaWidth;
        }

        #endregion

        #region background loading
        protected virtual void OnAbortBackgroundLoading()
        {

        }

        public void AbortBackgroundLoading()
        {
            if (_background_loader != null)
            {
                if (_background_loader.IsBusy)
                {
                    LastLoadingAborted = true;
                    _background_loader.Abort();
                    _background_loader.Join();
                }

                _background_loader = null;

                if (Dispatcher.CheckAccess())
                {
                    try
                    {
                        if (IsLoading)
                        {
                            IsLoading = false;
                            HasLoadedData = false;
                            _base_grid.IsHitTestVisible = true;
                        }
                    }
                    catch
                    {

                    }
                }
                else
                {
                    Dispatcher.Invoke(DispatcherPriority.Normal, (NoParas)delegate
                    {
                        try
                        {
                            if (IsLoading)
                            {
                                IsLoading = false;
                                HasLoadedData = false;
                                _base_grid.IsHitTestVisible = true;
                            }
                        }
                        catch
                        {

                        }
                    });
                }

                OnAbortBackgroundLoading();
            }
        }

        public void LoadDataAsync(object argument)
        {
            if (_background_loader != null)
                AbortBackgroundLoading();

            _background_loader = new QueuedBackgroundWorker();
            _loading_argument = argument;
            _abort_background_loading = false;
            LastLoadingAborted = false;
            HasLoadedData = false;

            string config_info = XAxisControl.Config != null ? " " + XAxisControl.Config.GetType() + " " + XAxisControl.Config.GetHashCode() : "";

            _background_loader.Name = "DiagramBase: AsyncConnection Thread" + config_info;
            _background_loader.DoWork += new QueuedBackgroundWorker.DoWorkEventHandler(startLoading);
            _background_loader.RunWorkerCompleted += new QueuedBackgroundWorker.RunWorkerCompletedEventHandler(loadingCompleted);
            _background_loader.RunWorkerAsync();
        }

        public void ForceLoadData()
        {
            LoadDataAsync(_loading_argument);
        }

        public virtual void FinishDiagramActivities()
        {

        }

        public void ForceLoadData(object argument)
        {
            LoadDataAsync(argument);
        }

        private void startLoading(object sender, DoWorkEventArgs e)
        {
            bool is_loading = false;

            if (DiagramManager != null)
            {
                while (DiagramManager.IsAnimating)
                    Thread.Sleep(10);
            }

            Dispatcher.Invoke(DispatcherPriority.Normal, (NoParas)delegate
            {
                try
                {
                    is_loading = IsLoading;
                    IsLoading = true;

                    _base_grid.IsHitTestVisible = false;

                    if (SelectedObjects.Count > 0)
                        ClearCurrentSelection();

                    ResetZoom();
                    if (DiagramMode == Controls.DiagramMode.MultiSelect)
                    {
                        this.DiagramMode = Controls.DiagramMode.Normal;
                        IsMehrfachAuswahlActive = false;
                        UpdateModeMenuButtonState();
                    }
                }
                catch
                {

                }
            });


            Background_LoadDataWorker(_loading_argument, e);
        }

        private void loadingCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            _background_loader.DoWork -= startLoading;
            _background_loader.RunWorkerCompleted -= loadingCompleted;

            bool is_loading = false;

            Dispatcher.Invoke(DispatcherPriority.Normal, (NoParas)delegate
            {
                try
                {
                    is_loading = IsLoading;
                    IsLoading = false;
                    if (e.Error == null)
                        HasLoadedData = true;

                    _base_grid.IsHitTestVisible = true;
                }
                catch
                {

                }
            });

            Background_LoadDataCompleted(_loading_argument, e);

        }

        protected virtual void Background_LoadDataWorker(object argument, DoWorkEventArgs e)
        {

        }

        protected virtual void Background_LoadDataCompleted(object argument, RunWorkerCompletedEventArgs e)
        {

        }
        #endregion

        #region Filtering
        public void DiagramManagerFilterChanged(object argument)
        {
            DateTime dt_check = DateTime.Now;
            OnDiagramManagerFilterChanged(argument);

            // if the filter expression changes in the virtual method
            // the filter will automatically be applied
            if (dt_check > _last_apply_filter)
                ApplyFilter();
        }

        protected virtual void OnDiagramManagerFilterChanged(object argument)
        {

        }

        /// <summary>
        /// Applys the current filter on all items
        /// </summary>
        private void ApplyFilter()
        {
            foreach (PointObject<Tx, Ty> check_item in _chart_data_objects)
            {
                IsItemFiltered(check_item);
            }

            _last_apply_filter = DateTime.Now;
            OnNewFilterApplied();
        }

        protected virtual void OnNewFilterApplied()
        {

        }

        private void StartObjectMove()
        {
            if (BeginObjectMove != null)
                BeginObjectMove(this, null);

            OnBeginObjectMove();
        }

        protected virtual void OnBeginObjectMove()
        {

        }

        private void FinishObjectMove()
        {
            if (EndObjectMove != null)
                EndObjectMove(this, null);

            OnEndObjectMove();
        }

        protected virtual void OnEndObjectMove()
        {

        }

        /// <summary>
        /// Applys the current filter on a single item
        /// </summary>
        /// <param name="check_object"></param>
        /// <returns></returns>
        private bool IsItemFiltered(PointObject<Tx, Ty> check_object)
        {
            bool isFiltered = true;
            if (ItemFilterExpression != null)
            {
                isFiltered = ItemFilterExpression(check_object);
            }

            if (isFiltered)
            {
                if (isFiltered != check_object.IsFiltered)
                {
                    check_object.IsFiltered = isFiltered;
                    // if the visibility has been set to Hidden, then the
                    // item is currently not being displayed due to a detail check
                    if (check_object.ObjectVisual.Visibility == System.Windows.Visibility.Collapsed)
                        check_object.ObjectVisual.Visibility = Visibility.Visible;
                }
            }
            else
            {
                if (isFiltered != check_object.IsFiltered)
                {
                    check_object.IsFiltered = isFiltered;
                    check_object.ObjectVisual.Visibility = Visibility.Collapsed;
                }
            }

            return isFiltered;
        }
        #endregion

        #region Property changing
        /// <summary>
        /// Called if an internal dependency property changed its value
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(e);
            if (e.Property == LegendControlProperty)
            {
                if (e.NewValue == null)
                {
                    BindingOperations.ClearBinding(_legend_border, VisibilityProperty);
                    _legend_border.Visibility = Visibility.Collapsed;
                    if (_legend_border.Child != null)
                        _legend_border.Child = null;
                }
                else
                {
                    _legend_border.Child = e.NewValue as FrameworkElement;
                    Binding visibility_binding = new Binding("Visibility");
                    visibility_binding.Source = _legend_border.Child;
                    _legend_border.SetBinding(VisibilityProperty, visibility_binding);
                }
            }
            else if (e.Property == LoadingControlProperty)
            {
                if (e.OldValue != null)
                {
                    if (_loading_grid.Children.Contains(e.OldValue as UIElement))
                        _loading_grid.Children.Remove(e.OldValue as UIElement);
                }

                if (e.NewValue != null && e.NewValue is UIElement)
                {
                    _loading_grid.Children.Add(e.NewValue as UIElement);
                }
            }
            else if (e.Property == CursorMarkerVisibilityProperty)
            {
                UpdateCurrentCursorValues();
            }
            else if (e.Property == IsLoadingProperty)
            {
                UpdateLoadingGrid();
            }
            else if (e.Property == IsMinimizedProperty)
            {
                // Mit dem nachstehenden Code wird die Ladeanimation nicht mehr in den Miniaturansichten angezeigt!

                //if ((bool)e.NewValue)
                //{
                //    if (LoadingControl != null)
                //        LoadingControl.Opacity = 0;
                //}
                //else
                //{
                //    if (LoadingControl != null)
                //        LoadingControl.Opacity = 1;
                //}
            }
        }
        /// <summary>
        /// Send property changed event
        /// </summary>
        /// <param name="property_name"></param>
        protected void SendPropertyChanged(string property_name)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property_name));
        }
        #endregion

        public virtual void ForceRedraw()
        {
            XAxisControl.SendScaleChanged();
            YAxisControl.SendScaleChanged();
        }

        protected void EnableElementCanvasMouseCapture()
        {
            EnableElementCanvasMouseCapture(null);
        }
        /// <summary>
        /// Enables mosue event capturing and hit testing
        /// </summary>
        public void EnableElementCanvasMouseCapture(UiRedirectionMetaInfo redirection_information)
        {
            _active_ui_redirection = redirection_information;

            if (_active_stylus_ids.Keys.Count <= 0)
                return;

            Debug.WriteLine("******** ENABLE CANVAS MOUSE CAPTURE");
            if (_element_canvas_background != null)
                _elements_canvas.Background = _element_canvas_background;
            else
                _elements_canvas.Background = Brushes.Transparent;
        }
        /// <summary>
        /// Disables mouse event capturing and hit testing
        /// </summary>
        public void DisableElementCanvasMouseCapture()
        {
            _active_ui_redirection = null;

            if (_active_stylus_ids.Keys.Count > 0)
                return;

            Debug.WriteLine("******** DISABLE CANVAS MOUSE CAPTURE");

            if (_elements_canvas.Background != null)
                _element_canvas_background = _elements_canvas.Background;
            _elements_canvas.Background = null;
        }

        private bool _old_xaxis_hittest = false;
        private bool _old_yaxis_hittest = false;

        protected void EnterMoveMode()
        {
            if (BeginObjectMove != null)
                StartObjectMove();

            _ignore_actual_size_changed = true;
            _reset_ignore_actual_size_changed = false;

            if (XAxisControl != null)
            {
                _actual_scroll_height = _element_scrolling.ActualHeight;

                _old_xaxis_hittest = XAxisControl.IsHitTestVisible;
                XAxisControl.IsHitTestVisible = false;
            }

            if (YAxisControl != null)
            {
                _old_yaxis_hittest = YAxisControl.IsHitTestVisible;
                YAxisControl.IsHitTestVisible = false;
            }

            //Grid.SetColumn(_element_scrolling, 1);
            //Grid.SetRow(_element_scrolling, 1);

            Grid.SetRowSpan(_element_scrolling, 2);
        }

        protected void ExitMoveMode()
        {
            FinishObjectMove();

            if (_active_stylus_ids.Keys.Count > 0)
                return;

            if (XAxisControl != null)
            {
                XAxisControl.IsHitTestVisible = _old_xaxis_hittest;
            }

            if (YAxisControl != null)
            {
                YAxisControl.IsHitTestVisible = _old_yaxis_hittest;
            }

            //Grid.SetColumn(_element_scrolling, 1);
            //Grid.SetRow(_element_scrolling, 1);
            Grid.SetRowSpan(_element_scrolling, 1);

            _actual_scroll_height = _element_scrolling.ActualHeight;

            _reset_ignore_actual_size_changed = true;
        }

        /// <summary>
        /// Registeres eventhandlers for the objects
        /// </summary>
        /// <param name="diagram_object"></param>
        private void RegisterObjectEvents(PointObject<Tx, Ty> diagram_object)
        {
            if (diagram_object == null)
                return;

            diagram_object.PropertyChanging += new System.ComponentModel.PropertyChangingEventHandler(diagram_object_PropertyChanging);
            diagram_object.PropertyChanged += new System.ComponentModel.PropertyChangedEventHandler(diagram_object_PropertyChanged);


            FrameworkElement visual = diagram_object.ObjectVisual;

            if (visual == null)
                return;

            visual.MouseEnter += new System.Windows.Input.MouseEventHandler(visual_MouseEnter);
            visual.MouseLeave += new System.Windows.Input.MouseEventHandler(visual_MouseLeave);
            visual.MouseDown += new System.Windows.Input.MouseButtonEventHandler(visual_MouseDown);
            visual.MouseUp += new System.Windows.Input.MouseButtonEventHandler(visual_MouseUp);
            visual.MouseMove += new System.Windows.Input.MouseEventHandler(visual_MouseMove);

            visual.StylusDown += new StylusDownEventHandler(visual_StylusDown);
            visual.StylusUp += new StylusEventHandler(visual_StylusUp);
            visual.StylusLeave += new StylusEventHandler(visual_StylusLeave);
            visual.StylusMove += new StylusEventHandler(visual_StylusMove);
            visual.StylusEnter += new StylusEventHandler(visual_StylusEnter);

            //ObjectSelectionMarker cur_marker = _child_markers.SingleOrDefault(m => m.ChildVisual == diagram_object.ObjectVisual);
            if (diagram_object.AssociatedObjectMarker == null)
                return;

            diagram_object.AssociatedObjectMarker.StartResizing += new EventHandler<ObjectSelectionMarker.StartResizingEventArgs>(cur_marker_StartResizing);
        }


        /// <summary>
        /// Raises the selection changed event
        /// </summary>
        /// <param name="added_items"></param>
        /// <param name="removed_items"></param>
        private void RaiseSelectionChanged(List<PointObject<Tx, Ty>> added_items, List<PointObject<Tx, Ty>> removed_items)
        {
            SelectionChangedEventArgs e = new SelectionChangedEventArgs(SelectionChangedEvent, added_items, removed_items);
            RaiseEvent(e);
        }

        /// <summary>
        /// Raises the selection changed event
        /// </summary>
        /// <param name="added_items"></param>
        /// <param name="removed_items"></param>
        private void AfterSelectionChanged()
        {
            FinishDiagramActivities();
        }

        /// <summary>
        /// Deregisteres eventhandlers for the objects
        /// </summary>
        /// <param name="diagram_object"></param>
        private void DeRegisterObjectEvents(PointObject<Tx, Ty> diagram_object)
        {
            if (diagram_object == null)
                return;

            diagram_object.PropertyChanging -= diagram_object_PropertyChanging;
            diagram_object.PropertyChanged -= diagram_object_PropertyChanged;

            FrameworkElement visual = diagram_object.ObjectVisual;

            if (visual == null)
                return;

            visual.MouseEnter -= visual_MouseEnter;
            visual.MouseLeave -= visual_MouseLeave;
            visual.MouseDown -= visual_MouseDown;
            visual.MouseUp -= visual_MouseUp;
            visual.MouseMove -= visual_MouseMove;

            visual.StylusDown -= visual_StylusDown;
            visual.StylusUp -= visual_StylusUp;
            visual.StylusLeave -= visual_StylusLeave;
            visual.StylusMove -= visual_StylusMove;
            visual.StylusEnter -= visual_StylusEnter;

            //ObjectSelectionMarker cur_marker = _child_markers.SingleOrDefault(m => m.ChildVisual == diagram_object.ObjectVisual);
            if (diagram_object.AssociatedObjectMarker == null)
                return;

            diagram_object.AssociatedObjectMarker.StartResizing -= cur_marker_StartResizing;
        }


        /// <summary>
        /// Gets the dataobject for a given visual or null if not found
        /// </summary>
        /// <param name="visual"></param>
        /// <returns></returns>
        private PointObject<Tx, Ty> GetObjectOfVisual(FrameworkElement visual)
        {
            if (visual == null)
                return null;

            if (visual.Tag is PointObject<Tx, Ty>)
                return visual.Tag as PointObject<Tx, Ty>;

            return null;
            //return _chart_data_objects.SingleOrDefault(o => o.ObjectVisual == visual);
        }
        /// <summary>
        /// Gets the marker object of a visual
        /// </summary>
        /// <param name="visual"></param>
        /// <returns></returns>
        private ObjectSelectionMarker GetMarkerOfObject(FrameworkElement visual)
        {
            PointObject<Tx, Ty> data_object = GetObjectOfVisual(visual);
            if (data_object != null)
                return data_object.AssociatedObjectMarker;

            return null;
            //return _child_markers.SingleOrDefault(m => m.ChildVisual == visual);
        }

        /// <summary>
        /// Selects a given object
        /// </summary>
        /// <param name="target_object"></param>
        /// <param name="input_origin">True if the method was called due to an input event like Mouse, Keyboard, Touch</param>
        private void SelectObject(PointObject<Tx, Ty> target_object, bool input_origin)
        {
            bool bMultiSelect = false;
            bool bShift = false;

            if (_diagram_config.AllowMultipleSelection)
            {
                if (!input_origin || DiagramMode == Controls.DiagramMode.MultiSelect)
                {
                    bMultiSelect = true;
                }
                else
                {
                    if (_active_stylus_ids.Keys.Count > 1)
                    {
                        bMultiSelect = true;
                    }
                    else
                    {
                        // require shift and/or strg key to be pressed
                        bool bCtrl = Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl);
                        bShift = Keyboard.IsKeyDown(Key.LeftShift) || Keyboard.IsKeyDown(Key.RightShift);

                        bMultiSelect = bCtrl || bShift;
                    }
                }
            }

            List<PointObject<Tx, Ty>> current_selection = SelectedObjects;

            if (bMultiSelect)
            {
                if (target_object == null || !target_object.CanSelect)
                {
                    if (target_object == null)
                    {
                        ClearCurrentSelection();
                        RaiseSelectionChanged(new List<PointObject<Tx, Ty>>(), current_selection.ToList());
                    }

                    AfterSelectionChanged();

                    return;
                }

                // toggle selection of the current object
                target_object.IsSelected = !target_object.IsSelected;

                List<PointObject<Tx, Ty>> sel_info = new List<PointObject<Tx, Ty>>();
                sel_info.Add(target_object);
                List<PointObject<Tx, Ty>> remove_info = new List<PointObject<Tx, Ty>>();

                // handle range selection
                if (current_selection.Count() > 0 && bShift)
                {
                    foreach (var point_object in current_selection)
                    {
                        if (point_object != current_selection.Last())
                            point_object.IsSelected = false;
                    }
                    remove_info.AddRange(current_selection);

                    PointObject<Tx, Ty> last_selected = current_selection.Last();

                    ClearCurrentSelection();

                    List<PointObject<Tx, Ty>> selection_range = GetSelectionRange(target_object, last_selected);
                    foreach (var point_object in selection_range)
                    {
                        point_object.IsSelected = true;
                        if (!sel_info.Contains(point_object))
                            sel_info.Add(point_object);
                    }
                    //last_selected.IsSelected = true;
                }

                if (!target_object.IsSelected)
                {
                    RaiseSelectionChanged(new List<PointObject<Tx, Ty>>(), sel_info);
                    current_selection.Remove(target_object);
                }
                else
                {
                    RaiseSelectionChanged(sel_info, remove_info);
                    current_selection.AddRange(sel_info);
                }
            }
            else
            {
                List<PointObject<Tx, Ty>> sel_info = null;

                List<PointObject<Tx, Ty>> current_selected_objects = SelectedObjects;

                // if only the current object is selected, toggle the selection
                if (current_selected_objects.Count == 1 && current_selected_objects.Contains(target_object))
                {
                    target_object.IsSelected = !target_object.IsSelected;
                    sel_info = new List<PointObject<Tx, Ty>>();
                    sel_info.Add(target_object);

                    if (!target_object.IsSelected)
                    {
                        RaiseSelectionChanged(new List<PointObject<Tx, Ty>>(), sel_info);
                        current_selected_objects.Remove(target_object);
                    }
                    else
                    {
                        RaiseSelectionChanged(sel_info, new List<PointObject<Tx, Ty>>());
                        current_selected_objects.Add(target_object);
                    }

                    AfterSelectionChanged();

                    return;
                }

                // deselect all selected first
                ClearCurrentSelection();

                if (target_object == null || !target_object.CanSelect)
                {
                    if (target_object == null)
                    {
                        RaiseSelectionChanged(new List<PointObject<Tx, Ty>>(), current_selection.ToList());
                        ClearCurrentSelection();
                    }

                    AfterSelectionChanged();

                    return;
                }

                target_object.IsSelected = !target_object.IsSelected;
                if (target_object.IsSelected)
                    current_selected_objects.Add(target_object);

                sel_info = new List<PointObject<Tx, Ty>>();
                sel_info.Add(target_object);

                RaiseSelectionChanged(sel_info, current_selection.ToList());
            }

            AfterSelectionChanged();
        }

        protected virtual List<PointObject<Tx, Ty>> GetSelectionRange(PointObject<Tx, Ty> first, PointObject<Tx, Ty> last)
        {
            List<PointObject<Tx, Ty>> values = new List<PointObject<Tx, Ty>>();
            int first_index = _chart_data_objects.IndexOf(first);
            int last_index = _chart_data_objects.IndexOf(last);
            if (first_index < last_index)
            {
                values.AddRange(_chart_data_objects.ToList().GetRange(first_index, last_index - first_index + 1));
            }
            else
            {
                values.AddRange(_chart_data_objects.ToList().GetRange(last_index, first_index - last_index + 1));
            }

            return values;
        }

        /// <summary>
        /// Clears the currently selected objects
        /// </summary>
        private void ClearCurrentSelection()
        {
            foreach (PointObject<Tx, Ty> curObj in SelectedObjects)
                curObj.IsSelected = false;

            SelectedObjects.Clear();
        }

        private void ClearSelectionOfStylus(int stylus_id)
        {
            if (SelectedObjects == null)
                return;

            List<PointObject<Tx, Ty>> stylus_selection = SelectedObjects.Where(o => o.UiManipulationMeta != null && o.UiManipulationMeta.RegisteredStylusId != null && o.UiManipulationMeta.RegisteredStylusId.Value == stylus_id).ToList();

            if (stylus_selection.Count > 0)
            {
                foreach (PointObject<Tx, Ty> curObj in stylus_selection)
                {
                    curObj.IsSelected = false;
                    SelectedObjects.Remove(curObj);
                }
            }
        }

        /// <summary>
        /// Stops the current dragging operation
        /// </summary>
        private void StopDragging(int stylus_id)
        {
            if (_is_dragging_object)
            {
                if (_dragactive_objects_per_stylus.ContainsKey(stylus_id) && _dragactive_objects_per_stylus[stylus_id] != null)
                {
                    ObjectSelectionMarker marker = GetMarkerOfObject(_dragactive_objects_per_stylus[stylus_id].ObjectVisual);
                    if (marker != null) // possible because object can be removed from Chartdataobjects in diagram (BtMassnahmenDiagram)
                        marker.ClearValue(Canvas.ZIndexProperty);
                }

                _dragactive_objects_per_stylus[stylus_id] = null;
                _dragactive_objects_per_stylus.Remove(stylus_id);

                _is_dragging_object = _dragactive_objects_per_stylus.Keys.Count > 0;


                //_x_offset[stylus_id] = double.MaxValue;
                //_y_offset[stylus_id] = double.MaxValue;
            }

            ExitMoveMode();
            DisableElementCanvasMouseCapture();
        }

        /// <summary>
        /// Stops the current rezising operation
        /// </summary>
        private void StopResizing(int stylus_id)
        {
            if (_is_resizing_object)
            {
                _is_resizing_object = false;

                if (_sizing_marker_object != null)
                {
                    _sizing_marker_object.StopResizing();

                    _sizing_marker_object.ClearValue(Canvas.ZIndexProperty);

                    if (_old_object_resize_cursor != null)
                        _sizing_marker_object.ChildVisual.Cursor = _old_object_resize_cursor;

                    _old_object_resize_cursor = null;
                }

                _sizing_marker_object = null;
                _sizing_object = null;
                _resizing_mode = ObjectSelectionMarker.ResizingMode.None;

                if (_old_resize_cursor != null)
                    _elements_canvas.Cursor = _old_resize_cursor;

                _old_resize_cursor = null;
            }

            DisableElementCanvasMouseCapture();
        }

        private void StopResizing()
        {
            if (_is_resizing_object)
            {
                _is_resizing_object = false;

                if (_sizing_marker_object != null)
                {
                    _sizing_marker_object.StopResizing();

                    _sizing_marker_object.ClearValue(Canvas.ZIndexProperty);

                    if (_old_object_resize_cursor != null)
                        _sizing_marker_object.ChildVisual.Cursor = _old_object_resize_cursor;

                    _old_object_resize_cursor = null;
                }

                _sizing_marker_object = null;
                _sizing_object = null;
                _resizing_mode = ObjectSelectionMarker.ResizingMode.None;

                if (_old_resize_cursor != null)
                    _elements_canvas.Cursor = _old_resize_cursor;

                _old_resize_cursor = null;
            }

            DisableElementCanvasMouseCapture();
        }

        /// <summary>
        /// Updates the current X/Y value dependency properties
        /// </summary>
        private void UpdateCurrentCursorValues()
        {
            Point mouse_pos = Mouse.GetPosition(_elements_canvas);
            Tx current_xvalue = XAxisControl.TransformAxisPixelToValue(mouse_pos.X);
            Ty current_yvalue = YAxisControl.TransformAxisPixelToValue(mouse_pos.Y);

            if (!current_xvalue.Equals(CurrentCursorXValue))
                CurrentCursorXValue = current_xvalue;

            if (!current_yvalue.Equals(CurrentCursorYValue))
                CurrentCursorYValue = current_yvalue;

            if (CursorMarkerVisibility == CursorMarkerVisibility.None)
            {
                _cursor_information_canvas.Visibility = Visibility.Collapsed;
            }
            else
            {
                _cursor_information_canvas.Visibility = Visibility.Visible;
                Point mouse_cursor_canvas_pos = Mouse.GetPosition(_background_elements_canvas);

                if ((CursorMarkerVisibility & CursorMarkerVisibility.HorizontalLine) == ViewConfig.CursorMarkerVisibility.HorizontalLine)
                {
                    _cursor_horizontal_line.Visibility = Visibility.Visible;
                    Canvas.SetTop(_cursor_horizontal_line, mouse_cursor_canvas_pos.Y);
                    //_cursor_horizontal_line.Y1 = mouse_cursor_canvas_pos.Y;
                    //_cursor_horizontal_line.Y2 = mouse_cursor_canvas_pos.Y;
                }
                else
                {
                    _cursor_horizontal_line.Visibility = Visibility.Collapsed;
                }

                if ((CursorMarkerVisibility & CursorMarkerVisibility.VerticalLine) == ViewConfig.CursorMarkerVisibility.VerticalLine)
                {
                    _cursor_vertical_line.Visibility = Visibility.Visible;
                    Canvas.SetLeft(_cursor_vertical_line, mouse_cursor_canvas_pos.X);
                    //_cursor_vertical_line.X1 = mouse_cursor_canvas_pos.X;
                    //_cursor_vertical_line.X2 = mouse_cursor_canvas_pos.X;
                }
                else
                {
                    _cursor_vertical_line.Visibility = Visibility.Collapsed;
                }

                if ((CursorMarkerVisibility & CursorMarkerVisibility.ShowCoordinatesOnCursor) == ViewConfig.CursorMarkerVisibility.ShowCoordinatesOnCursor)
                {
                    _curor_information_tb.Visibility = Visibility.Visible;
                    _curor_information_tb.Text = string.Format("{0} / {1}", CurrentCursorXValue, CurrentCursorYValue);
                    Canvas.SetTop(_curor_information_tb, mouse_cursor_canvas_pos.Y + 16 - ScrollOffsetY);
                    Canvas.SetLeft(_curor_information_tb, mouse_cursor_canvas_pos.X + 16 - ScrollOffsetX);
                }
                else
                {
                    _curor_information_tb.Visibility = Visibility.Collapsed;
                }
            }
        }

        private void UpdateLoadingGrid()
        {
            //Debug.WriteLine("UpdateLoadingGrid IsLoading " + IsLoading + ", Diagramtype " + GetType());

            if (IsLoading)
            {
                double cur_val = _loading_grid.Opacity;
                _loading_grid.BeginAnimation(Grid.OpacityProperty, null);
                _loading_grid.Opacity = cur_val;

                cur_val = _base_grid.Opacity;
                _base_grid.BeginAnimation(Grid.OpacityProperty, null);
                _base_grid.Opacity = cur_val;

                _loading_grid.Visibility = Visibility.Visible;
                _loading_grid.Opacity = 1;
                _base_grid.Opacity = 0;
                _base_grid.Visibility = Visibility.Collapsed;

                SendPropertyChanged("StartLoading");
            }
            else
            {
                Storyboard sb = new Storyboard();
                DoubleAnimation fad_in_animation = new DoubleAnimation(0, 1, new Duration(TimeSpan.FromSeconds(1)));
                Storyboard.SetTargetProperty(fad_in_animation, new PropertyPath(Grid.OpacityProperty));
                sb.Children.Add(fad_in_animation);

                Storyboard sb2 = new Storyboard();
                DoubleAnimation fad_out_animation = new DoubleAnimation(1, 0, new Duration(TimeSpan.FromSeconds(1)));
                Storyboard.SetTargetProperty(fad_out_animation, new PropertyPath(Grid.OpacityProperty));
                sb2.Children.Add(fad_out_animation);

                sb2.Completed += new EventHandler(sb2_Completed);

                _loading_grid.Visibility = Visibility.Visible;
                _base_grid.Visibility = Visibility.Visible;

                _base_grid.BeginStoryboard(sb);
                _loading_grid.BeginStoryboard(sb2);
            }
        }

        void sb2_Completed(object sender, EventArgs e)
        {
            _loading_grid.Visibility = Visibility.Hidden;

            SendPropertyChanged("EndLoading");
        }

        /// <summary>
        /// Adds a new header control
        /// </summary>
        /// <param name="header_control"></param>
        public void AddHeaderControl(FrameworkElement header_control)
        {
            if (!_header_container.Children.Contains(header_control))
            {
                _header_container.Children.Add(header_control);
                _header_container.Visibility = System.Windows.Visibility.Visible;
            }
        }
        /// <summary>
        /// Removes an existing header control
        /// </summary>
        /// <param name="header_control"></param>
        public void RemoveHeaderControl(FrameworkElement header_control)
        {
            if (_header_container.Children.Contains(header_control))
            {
                _header_container.Children.Remove(header_control);
                if (_header_container.Children.Count <= 0)
                    _header_container.Visibility = System.Windows.Visibility.Collapsed;
            }
        }

        /// <summary>
        /// Resets the force cursor visibility mode
        /// </summary>
        public void ResetForceCursorVisibility()
        {
            if (_force_cursor_visibility)
            {
                _force_cursor_visibility = false;
                CursorMarkerVisibility = _old_marker_visibility;
            }
        }
        /// <summary>
        /// Activates the force cursor visibility mode
        /// </summary>
        public void ForceCursorVisibility()
        {
            if (!_force_cursor_visibility)
            {
                _old_marker_visibility = CursorMarkerVisibility;
                CursorMarkerVisibility = ViewConfig.CursorMarkerVisibility.VerticalLine | ViewConfig.CursorMarkerVisibility.HorizontalLine | ViewConfig.CursorMarkerVisibility.ShowCoordinatesOnCursor;
                _force_cursor_visibility = true;
            }
        }

        private void ShowTouchSelectionRectangle(StylusEventArgs e)
        {
            if (_scale_touch_intertia_manipulator.DeviceList.Count >= 2 && _scale_touch_intertia_manipulator.DeviceList.Contains(e.StylusDevice))
            {
                int i = 0;

                StylusDevice first_device = null, second_device = null;

                foreach (StylusDevice dev in _scale_touch_intertia_manipulator.DeviceList)
                {
                    if (i == 0)
                        first_device = dev;
                    else if (i == 1)
                        second_device = dev;

                    i++;
                }

                if (first_device != null && second_device != null)
                {
                    Point pos1 = first_device.GetPosition(_background_elements_canvas);
                    Point pos2 = second_device.GetPosition(_background_elements_canvas);

                    Point normalized_pos1 = new Point(pos1.X - ScrollOffsetX, pos1.Y - ScrollOffsetY);
                    Point normalized_pos2 = new Point(pos2.X - ScrollOffsetX, pos2.Y - ScrollOffsetY);

                    DrawSelectionRectangle(normalized_pos1, normalized_pos2);
                }
            }
            else
            {
                DrawSelectionRectangle(default(Point), default(Point));
            }
        }

        private void DrawSelectionRectangle(Point pos1, Point pos2)
        {
            if (pos1 == default(Point) && pos2 == default(Point))
            {
                _selection_rectangle.Width = 0;
                _selection_rectangle.Height = 0;
                Canvas.SetLeft(_selection_rectangle, 0);
                Canvas.SetTop(_selection_rectangle, 0);
            }
            else
            {
                double dwidth = pos2.X - pos1.X;
                double dheight = pos2.Y - pos1.Y;

                if (dwidth < 0)
                    Canvas.SetLeft(_selection_rectangle, pos2.X);
                else
                    Canvas.SetLeft(_selection_rectangle, pos1.X);

                if (dheight < 0)
                    Canvas.SetTop(_selection_rectangle, pos2.Y);
                else
                    Canvas.SetTop(_selection_rectangle, pos1.Y);

                _selection_rectangle.Width = Math.Abs(dwidth);
                _selection_rectangle.Height = Math.Abs(dheight);
            }
        }

        private void ProcessMultiSelectionRectangle()
        {
            double left = Canvas.GetLeft(_selection_rectangle);
            double top = Canvas.GetTop(_selection_rectangle);
            double width = _selection_rectangle.Width;
            double height = _selection_rectangle.Height;

            // werte die im element canvas gelten
            double normalized_left = left + ScrollOffsetX - (_mode_menu_canvas.ActualWidth - _element_scrolling.ActualWidth);
            double normalized_top = top + ScrollOffsetY; // +(_mode_menu_canvas.ActualHeight - _element_scrolling.ActualHeight);

            // TODO: elemente im bereich selektieren

            ClearCurrentSelection();

            foreach (PointObject<Tx, Ty> obj in ChartDataObjects)
            {
                bool bSelect = false;

                if (!obj.AmIAnArea)
                {
                    double dX = XAxisControl.TransformAxisValueToPixel(obj.XValue);
                    double dy = YAxisControl.TransformAxisValueToPixel(obj.YValue);

                    bSelect |= dX >= normalized_left && dX <= (normalized_left + width);
                    bSelect |= dy >= normalized_top && dy <= (normalized_top + height);
                }
                else
                {
                    double dX = XAxisControl.TransformAxisValueToPixel(obj.XValue);
                    double dy = YAxisControl.TransformAxisValueToPixel(obj.YValue);

                    double dX1 = XAxisControl.TransformAxisValueToPixel(((AreaObject<Tx, Ty>)obj).ToXValue);
                    double dy1 = YAxisControl.TransformAxisValueToPixel(((AreaObject<Tx, Ty>)obj).ToYValue);

                    float from_x = (float)((dX1 > dX) ? dX : dX1);
                    float from_y = (float)((dy1 > dy) ? dy : dy1);

                    System.Drawing.RectangleF rect1 = new System.Drawing.RectangleF((float)normalized_left, (float)normalized_top, (float)width, (float)height);
                    System.Drawing.RectangleF rect2 = new System.Drawing.RectangleF((float)from_x, (float)from_y, (float)Math.Abs(dX1 - dX), (float)Math.Abs(dy1 - dy));
                    System.Drawing.RectangleF rectIntersect = System.Drawing.RectangleF.Intersect(rect1, rect2);

                    bSelect |= rectIntersect.Width > 0 || rectIntersect.Height > 0;
                }

                if (bSelect)
                    SelectObject(obj, true);
            }
        }



        #region Touch helper
        /// <summary>
        /// Gets the id of the stylus device or int.MinValue for the mouse
        /// </summary>
        /// <param name="device"></param>
        /// <returns></returns>
        public int GetStylusId(StylusDevice device)
        {
            if (device == null) // mouse
                return int.MinValue;

            return device.Id;
        }
        /// <summary>
        /// Registeres a device id for "in action"
        /// </summary>
        /// <param name="stylus_id"></param>
        /// <param name="root_element"></param>
        public void RegisterStylusAction(int stylus_id, FrameworkElement root_element, bool force)
        {
            if (!_active_stylus_ids.ContainsKey(stylus_id) || force)
            {
                _active_stylus_ids[stylus_id] = root_element;
                //Debug.WriteLine("### REGISTER STYLUS (" + stylus_id + ")");
            }
        }
        /// <summary>
        /// Deregisteres a device action
        /// </summary>
        /// <param name="stylus_id"></param>
        public void DeregisterStylusAction(int stylus_id)
        {
            if (_active_stylus_ids.ContainsKey(stylus_id))
            {
                _active_stylus_ids.Remove(stylus_id);
                //Debug.WriteLine("### DEREGISTER STYLUS (" + stylus_id + ")");
                //Debug.WriteLine("### Stylus Keys: " + string.Join(",", _active_stylus_ids.Keys.Select(k => k.ToString()).ToArray()));
            }
        }
        /// <summary>
        /// Checks if a stylusdevice is registered for action
        /// </summary>
        /// <param name="stylus_id"></param>
        /// <returns></returns>
        public bool IsStylusActionRegistered(int stylus_id)
        {
            return _active_stylus_ids.ContainsKey(stylus_id);
        }
        /// <summary>
        /// Gets the root frameworkelement which initiates the stylus action
        /// </summary>
        /// <param name="stylus_id"></param>
        /// <returns></returns>
        public FrameworkElement GetRootFrameworkElementOfStylus(int stylus_id)
        {
            if (_active_stylus_ids.ContainsKey(stylus_id))
                return _active_stylus_ids[stylus_id];

            return null;
        }
        /// <summary>
        /// Validates the device registration and returns the device id or null if not registered or invalid device
        /// </summary>
        /// <param name="device"></param>
        /// <param name="must_be_mouse"></param>
        /// <returns></returns>
        public int? ValidateStylusDeviceRegistration(StylusDevice device, bool must_be_mouse)
        {
            if (device != null && must_be_mouse)
                return null;
            else if (device == null && !must_be_mouse)
                return null;

            int current_stylus_id = GetStylusId(device);

            // wenn für das stylus device kein aktion registriert wurde
            // return
            if (!IsStylusActionRegistered(current_stylus_id))
                return null;

            return current_stylus_id;
        }

        /// <summary>
        /// Returns the number of active and registered stylus devices (incl.) the mouse
        /// </summary>
        /// <returns></returns>
        public int GetActiveStylusCount()
        {
            return _active_stylus_ids.Count;
        }

        /// <summary>
        /// Process the move event from the mouse/or stylus device
        /// </summary>
        /// <param name="sender">original event sender</param>
        /// <param name="current_stylus_id">id of the current stylus device</param>
        /// <param name="is_mouse_device">a flag indicating if the event was triggered by the mouse</param>
        /// <param name="target_object">target chart object</param>
        /// <param name="device_position">current device position in the element canvas</param>
        /// <returns>true if the event was handled</returns>
        private bool ProcessMove(object sender, int current_stylus_id, bool is_mouse_device, PointObject<Tx, Ty> target_object, Point device_position)
        {
            //Debug.WriteLine("MMMMMMMMMMMMMMMMMMMMMMMMM OOOOOOOOOOOOOOOOOOOOOOOOOOO VVVVVVVVVVVVVVVVVVVVVVVVVVVV EEEEEEEEEEEEEEEEEEEEEEE");

            // Wenn sich das Diagram im Multi-Selecto Modus befindet
            // dann ist ein Moven von Objekten NICHT möglich
            // nur selektionen sind gestattet
            if (DiagramMode == Controls.DiagramMode.MultiSelect)
                return true;

            bool move_multiple = false;
            if (is_mouse_device)
                move_multiple = SelectedObjects.Count > 1;
            else
                move_multiple = SelectedObjects.Count > 1 && GetActiveStylusCount() == 1;

            if (!target_object.WasMoved)
                target_object.WasMoved = Math.Abs((device_position - target_object.UiManipulationMeta.StartInputDevicePosition).Length) > 10;

            if (target_object.UiManipulationMeta.IsInputDeviceDown && !(target_object.IsMoving || target_object.IsResizing)) // && !_is_dragging_object)
            {
                if (target_object.CanMove && target_object.UiManipulationMeta.RegisteredStylusId.HasValue && target_object.UiManipulationMeta.RegisteredStylusId.Value == current_stylus_id)
                {
                    _is_dragging_object = true;
                    _dragactive_objects_per_stylus[current_stylus_id] = target_object;


                    if (!target_object.IsSelected)
                    {
                        target_object.UiManipulationMeta.SelectedWithDownAction = true;
                        SelectObject(target_object, true);
                    }

                    target_object.BeginMoveOrResize(true, false);
                    EnterMoveMode();

                    ObjectSelectionMarker marker = GetMarkerOfObject(sender as FrameworkElement);
                    if (marker != null)
                    {
                        Canvas.SetZIndex(marker, 10000);
                        marker.IsMoving = true;
                    }

                    // multiple selected object move
                    if (move_multiple)
                    {
                        foreach (PointObject<Tx, Ty> point_object in SelectedObjects)
                        {
                            if (point_object.Equals(target_object) || (GetActiveStylusCount() > 1 && !point_object.HasSameStylusId(target_object)))
                                continue;

                            point_object.BeginMoveOrResize(true, false);
                            ObjectSelectionMarker marker_p = GetMarkerOfObject(point_object.ObjectVisual);
                            if (marker_p != null)
                            {
                                //Debug.WriteLine("Set Marker of multiple move object on x=" + point_object.XValue);
                                Canvas.SetZIndex(marker_p, 10000);
                                marker_p.IsMoving = true;
                            }
                        }
                    }
                }
            }

            if (target_object.IsMoving && target_object.UiManipulationMeta.RegisteredStylusId.HasValue && target_object.UiManipulationMeta.RegisteredStylusId.Value == current_stylus_id)
            {
                //Debug.WriteLine("### Dragging " + e.GetPosition(this).X + ";" + e.GetPosition(this).Y);

                Point mouse_pos = device_position;

                if (target_object.UiManipulationMeta.InputDeviceOffsetY == double.MaxValue)
                {
                    double y_value = YAxisControl.TransformAxisValueToPixel(target_object.YValue);
                    target_object.UiManipulationMeta.InputDeviceOffsetY = target_object.UiManipulationMeta.StartInputDevicePosition.Y - y_value;
                }

                if (target_object.UiManipulationMeta.InputDeviceOffsetX == double.MaxValue)
                {
                    double x_value = XAxisControl.TransformAxisValueToPixel(target_object.XValue);
                    target_object.UiManipulationMeta.InputDeviceOffsetX = target_object.UiManipulationMeta.StartInputDevicePosition.X - x_value;
                }

                Vector diff = mouse_pos - target_object.UiManipulationMeta.LastInputDevicePosition;

                if (is_mouse_device)
                {
                    if (diff.Length < 1)
                    {
                        return true;
                    }
                }
                else
                {
                    if (diff.Length < 5)
                    {
                        return true;
                    }
                }

                target_object.UiManipulationMeta.LastInputDevicePosition = mouse_pos;

                //Debug.WriteLine(string.Format("STYLUS MOVE ({0}) - {1}:{2} - {3} / Visual: {4}", current_stylus_id, mouse_pos.X, mouse_pos.Y, target_object.GetHashCode(), sender.GetHashCode()));

                Tx new_xvalue = XAxisControl.TransformAxisPixelToValue(mouse_pos.X - (target_object.UiManipulationMeta.InputDeviceOffsetX == double.MaxValue ? 0 : target_object.UiManipulationMeta.InputDeviceOffsetX));
                Ty new_yvalue = YAxisControl.TransformAxisPixelToValue(mouse_pos.Y - (target_object.UiManipulationMeta.InputDeviceOffsetY == double.MaxValue ? 0 : target_object.UiManipulationMeta.InputDeviceOffsetY));
                //Debug.WriteLine("MOVE new_xvalue=" + new_xvalue);

                if (target_object.ObjectVisual.Cursor != Cursors.No && sender != null)
                    _old_move_cursor = target_object.ObjectVisual.Cursor;

                if (!new_xvalue.Equals(target_object.XValue) || !new_yvalue.Equals(target_object.YValue))
                {
                    // calculate dockable candidates but don't check if move is possible -> required to decide moving
                    target_object.SetValues(new_xvalue, new_yvalue);
                    if (move_multiple)
                    {
                        double _drag_offset_x = mouse_pos.X -
                                                target_object.UiManipulationMeta.StartInputDevicePosition.X;
                        double _drag_offset_y = mouse_pos.Y -
                                                target_object.UiManipulationMeta.StartInputDevicePosition.Y;
                        foreach (PointObject<Tx, Ty> point_object in SelectedObjects)
                        {
                            if (point_object.Equals(target_object) || (GetActiveStylusCount() > 1 && !point_object.HasSameStylusId(target_object)))
                                continue;

                            double origin_x = XAxisControl.TransformAxisValueToPixel(point_object.LastX);
                            double origin_y = YAxisControl.TransformAxisValueToPixel(point_object.LastY);
                            Tx new_xvalue_1 = XAxisControl.TransformAxisPixelToValue(origin_x + _drag_offset_x);
                            Ty new_yvalue_1 = YAxisControl.TransformAxisPixelToValue(origin_y + _drag_offset_y);
                            point_object.SetValues(new_xvalue_1, new_yvalue_1);
                        }
                    }
                    //Debug.WriteLine("Before DecideDockableCandidates om ProcessMove");

                    DecideDockableCandidates(target_object);
                    double x_offset;
                    double y_offset;
                    CalculateMovingOffset(target_object, out x_offset, out y_offset);

                    //Debug.WriteLine("x,yoffset " + x_offset + " " + y_offset);

                    bool ignore_not_can_move_on_multi_selection;
                    bool can_move = target_object.CanMoveTo(new_xvalue, new_yvalue, x_offset, y_offset, out ignore_not_can_move_on_multi_selection);
                    bool any_can_move_and_ignore = can_move && ignore_not_can_move_on_multi_selection;

                    // multiple selected object move
                    if (move_multiple)
                    {
                        double _drag_offset_x = mouse_pos.X - target_object.UiManipulationMeta.StartInputDevicePosition.X;
                        double _drag_offset_y = mouse_pos.Y - target_object.UiManipulationMeta.StartInputDevicePosition.Y;
                        foreach (PointObject<Tx, Ty> point_object in SelectedObjects)
                        {
                            if (point_object.Equals(target_object) || (GetActiveStylusCount() > 1 && !point_object.HasSameStylusId(target_object)))
                                continue;

                            double origin_x = XAxisControl.TransformAxisValueToPixel(point_object.LastX);
                            double origin_y = YAxisControl.TransformAxisValueToPixel(point_object.LastY);
                            Tx new_xvalue_1 = XAxisControl.TransformAxisPixelToValue(origin_x + _drag_offset_x);
                            Ty new_yvalue_1 = YAxisControl.TransformAxisPixelToValue(origin_y + _drag_offset_y);
                            bool ignore_not_can_move;
                            bool can_move_multiple = point_object.CanMoveTo(new_xvalue_1, new_yvalue_1, x_offset, y_offset, out ignore_not_can_move);

                            if (!can_move_multiple)
                                can_move = false;

                            if (can_move_multiple && ignore_not_can_move)
                                any_can_move_and_ignore = true;
                        }
                    }

                    if (any_can_move_and_ignore)
                        can_move = true;

                    AdaptCanMove(target_object, can_move);

                    if (!can_move)
                    {
                        InitDockableCandidates(target_object);

                        target_object.ObjectVisual.Cursor = Cursors.No;
                        _elements_canvas.Cursor = Cursors.No;
                    }
                    else
                    {
                        target_object.ObjectVisual.Cursor = _old_move_cursor;
                        _elements_canvas.Cursor = _old_move_cursor;
                    }
                }
            }
            else if (target_object.IsResizing && _sizing_object != null)
            {
                //Debug.WriteLine("### Resizing" + e.GetPosition(this).X + ";" + e.GetPosition(this).Y);<

                if (_sizing_object.AmIAnArea)
                {
                    target_object.UiManipulationMeta.InputDeviceOffsetX = 0;
                    target_object.UiManipulationMeta.InputDeviceOffsetY = 0;

                    Point mouse_pos = device_position;

                    Vector diff = mouse_pos - target_object.UiManipulationMeta.LastInputDevicePosition;

                    if (diff.Length < 1)
                    {
                        return true;
                    }

                    target_object.UiManipulationMeta.LastInputDevicePosition = mouse_pos;

                    Tx current_from_x_value = _sizing_object.XValue;
                    Ty current_from_y_value = _sizing_object.YValue;

                    Tx current_to_x_value = ((AreaObject<Tx, Ty>)_sizing_object).ToXValue;
                    Ty current_to_y_value = ((AreaObject<Tx, Ty>)_sizing_object).ToYValue;


                    Tx target_from_x_value = current_from_x_value;
                    Ty target_from_y_value = current_from_y_value;

                    Tx target_to_x_value = current_to_x_value;
                    Ty target_to_y_value = current_to_y_value;

                    Tx new_xvalue = XAxisControl.TransformAxisPixelToValue(mouse_pos.X - (target_object.UiManipulationMeta.InputDeviceOffsetX == double.MaxValue ? 0 : target_object.UiManipulationMeta.InputDeviceOffsetX));
                    Ty new_yvalue = YAxisControl.TransformAxisPixelToValue(mouse_pos.Y - (target_object.UiManipulationMeta.InputDeviceOffsetY == double.MaxValue ? 0 : target_object.UiManipulationMeta.InputDeviceOffsetY));

                    switch (_resizing_mode)
                    {
                        case ObjectSelectionMarker.ResizingMode.HorizontalLeft:
                            {
                                if (_sizing_object.CanResizeX)
                                    target_from_x_value = new_xvalue;
                            }
                            ; break;
                        case ObjectSelectionMarker.ResizingMode.HorizontalRight:
                            {
                                if (_sizing_object.CanResizeX)
                                    target_to_x_value = new_xvalue;
                            }
                            ; break;
                        case ObjectSelectionMarker.ResizingMode.VerticalTop:
                            {
                                if (_sizing_object.CanResizeY)
                                    target_to_y_value = new_yvalue;

                            }
                            ; break;
                        case ObjectSelectionMarker.ResizingMode.VerticalBottom:
                            {
                                if (_sizing_object.CanResizeY)
                                    target_from_y_value = new_yvalue;
                            }; break;
                        case ObjectSelectionMarker.ResizingMode.BothLT:
                            {
                                if (_sizing_object.CanResizeX)
                                    target_from_x_value = new_xvalue;
                                if (_sizing_object.CanResizeY)
                                    target_to_y_value = new_yvalue;
                            }
                            ; break;
                        case ObjectSelectionMarker.ResizingMode.BothLB:
                            {
                                if (_sizing_object.CanResizeX)
                                    target_from_x_value = new_xvalue;
                                if (_sizing_object.CanResizeY)
                                    target_from_y_value = new_yvalue;

                            }
                            ; break;
                        case ObjectSelectionMarker.ResizingMode.BothRT:
                            {
                                if (_sizing_object.CanResizeX)
                                    target_to_x_value = new_xvalue;
                                if (_sizing_object.CanResizeY)
                                    target_to_y_value = new_yvalue;
                            }
                            ; break;
                        case ObjectSelectionMarker.ResizingMode.BothRB:
                            {
                                if (_sizing_object.CanResizeX)
                                    target_to_x_value = new_xvalue;

                                if (_sizing_object.CanResizeY)
                                    target_from_y_value = new_yvalue;
                            }
                            ; break;
                    }

                    if (!target_from_x_value.Equals(current_from_x_value) || !target_from_y_value.Equals(current_from_y_value) ||
                        !target_to_x_value.Equals(current_to_x_value) || !target_to_y_value.Equals(current_to_y_value))
                    {
                        bool can_resize = _sizing_object.CanResizeTo(target_from_x_value, target_from_y_value, target_to_x_value, target_to_y_value);
                        if (!can_resize)
                        {
                            _sizing_object.ObjectVisual.Cursor = Cursors.No;
                            _elements_canvas.Cursor = Cursors.No;
                        }
                        else
                        {
                            _sizing_object.ObjectVisual.Cursor = _resizing_cursor;
                            _elements_canvas.Cursor = _resizing_cursor;
                        }
                    }
                }
            }

            return true;
        }

        protected void ProcessKeyMove(bool left, bool right, bool top, bool bottom, bool ensure_x_min_move, bool ensure_y_min_move)
        {
            int count_moves = left ? 1 : 0;
            count_moves += right ? 1 : 0;
            count_moves += top ? 1 : 0;
            count_moves += bottom ? 1 : 0;

            if (!XAxisControl.CanKeyMove || !YAxisControl.CanKeyMove || SelectedObjects.Count == 0 || count_moves != 1)
                return;

            bool move_multiple = SelectedObjects.Count > 1;


            double drag_offset_x = 0;
            double drag_offset_y = 0;
            if (left)
                drag_offset_x = -5;
            else if (right)
                drag_offset_x = 5;

            if (top)
                drag_offset_y = -5;
            else if (bottom)
                drag_offset_y = 5;

            if (ensure_y_min_move && drag_offset_y != 0)
            {
                // ensure correct moving in y-direction
                double unit_size = YAxisControl.ScaleUnitSmallInPixel;
                if (drag_offset_y > 0 && drag_offset_y < unit_size)
                    drag_offset_y = unit_size;
                if (drag_offset_y < 0 && drag_offset_y > -unit_size)
                    drag_offset_y = -unit_size;
            }


            if (ensure_x_min_move && drag_offset_x != 0)
            {
                // ensure correct moving in y-direction
                double unit_size = XAxisControl.ScaleUnitSmallInPixel;
                if (drag_offset_x > 0 && drag_offset_x < unit_size)
                    drag_offset_x = unit_size;
                if (drag_offset_x < 0 && drag_offset_x > -unit_size)
                    drag_offset_x = -unit_size;
            }

            foreach (PointObject<Tx, Ty> point_object in SelectedObjects)
            {
                double origin_x = XAxisControl.TransformAxisValueToPixel(point_object.LastX);
                double origin_y = YAxisControl.TransformAxisValueToPixel(point_object.LastY);
                Tx new_xvalue_1 = XAxisControl.TransformAxisPixelToValue(origin_x + drag_offset_x);
                Ty new_yvalue_1 = YAxisControl.TransformAxisPixelToValue(origin_y + drag_offset_y);
                point_object.SetValues(new_xvalue_1, new_yvalue_1);
            }

            DecideDockableCandidates(SelectedObjects[0]);
            double x_offset;
            double y_offset;
            CalculateMovingOffset(SelectedObjects[0], out x_offset, out y_offset);

            // reset offset if movement is in different direction
            if ((x_offset < 0 && drag_offset_x > 0) || (x_offset > 0 && drag_offset_x < 0))
                x_offset = 0;

            if ((y_offset < 0 && drag_offset_y > 0) || (y_offset > 0 && drag_offset_y < 0))
                y_offset = 0;

            // reset offset if move is in other direction
            if (x_offset != 0 && !(left || right))
                x_offset = 0;

            if (y_offset != 0 && !(top || bottom))
                y_offset = 0;

            bool can_move = true;
            bool any_can_move_and_ignore = false;

            foreach (PointObject<Tx, Ty> point_object in SelectedObjects)
            {
                double origin_x = XAxisControl.TransformAxisValueToPixel(point_object.LastX);
                double origin_y = YAxisControl.TransformAxisValueToPixel(point_object.LastY);
                Tx new_xvalue_1 = XAxisControl.TransformAxisPixelToValue(origin_x + drag_offset_x);
                Ty new_yvalue_1 = YAxisControl.TransformAxisPixelToValue(origin_y + drag_offset_y);
                bool ignore_not_can_move;
                bool can_move_multiple = point_object.CanMoveTo(new_xvalue_1, new_yvalue_1, x_offset, y_offset, out ignore_not_can_move);

                if (!can_move_multiple)
                    can_move = false;

                if (can_move_multiple && ignore_not_can_move)
                    any_can_move_and_ignore = true;
            }

            if (any_can_move_and_ignore)
                can_move = true;

            AdaptCanMove(SelectedObjects[0], can_move);

            if (!can_move)
            {
                InitDockableCandidates(SelectedObjects[0]);
                foreach (PointObject<Tx, Ty> point_object in SelectedObjects)
                {
                    point_object.ResetMoveAndResize();
                }
            }
            else
            {
                double left_offset;
                double right_offset;
                double bottom_offset;

                List<PointObject<Tx, Ty>> objects_to_deselect = new List<PointObject<Tx, Ty>>();
                foreach (PointObject<Tx, Ty> point_object in SelectedObjects)
                {
                    double origin_x = XAxisControl.TransformAxisValueToPixel(point_object.LastX);
                    double origin_y = YAxisControl.TransformAxisValueToPixel(point_object.LastY);
                    Tx new_xvalue_1 = XAxisControl.TransformAxisPixelToValue(origin_x + drag_offset_x);
                    Ty new_yvalue_1 = YAxisControl.TransformAxisPixelToValue(origin_y + drag_offset_y);

                    point_object.IsDockingMoveObject(out left_offset, out right_offset, out bottom_offset);
                    point_object.MoveTo(new_xvalue_1, new_yvalue_1, x_offset, y_offset);
                }
            }
        }

        private void AdaptCanMove(PointObject<Tx, Ty> target_object, bool can_move)
        {
            foreach (PointObject<Tx, Ty> point_object in SelectedObjects)
            {
                if (GetActiveStylusCount() == 1 || (GetActiveStylusCount() > 1 && point_object.HasSameStylusId(target_object)))
                {
                    point_object.AssociatedObjectMarker.CanMove = can_move;
                }
            }

            foreach (KeyValuePair<int, FrameworkElement> framework_element in _active_stylus_ids)
            {
                KeyValuePair<int, FrameworkElement> element = framework_element;
                IEnumerable<PointObject<Tx, Ty>> other_style_objects = SelectedObjects.Where(o => o.GetStylusId() == element.Key);
                bool can_move_other = true;
                foreach (PointObject<Tx, Ty> other_style_object in other_style_objects)
                {
                    bool ignore_not_can_move_on_multiple = false;
                    if (!other_style_object.CanMoveTo(other_style_object.XValue, other_style_object.YValue, 0, 0, out ignore_not_can_move_on_multiple))
                        can_move_other = false;
                }

                foreach (PointObject<Tx, Ty> other_style_object in other_style_objects)
                {
                    other_style_object.AssociatedObjectMarker.CanMove = can_move_other;
                }
            }
        }

        protected virtual void DecideDockableCandidates(PointObject<Tx, Ty> target_object)
        {
        }

        protected virtual void InitDockableCandidates(PointObject<Tx, Ty> target_object)
        {
        }

        /// <summary>
        /// Process the up event from the mouse/or stylus device
        /// </summary>
        /// <param name="sender">original event sender</param>
        /// <param name="current_stylus_id">id of the current stylus device</param>
        /// <param name="is_mouse_device">a flag indicating if the event was triggered by the mouse</param>
        /// <param name="target_object">target chart object</param>
        /// <param name="device_position">current device position in the element canvas</param>
        /// <returns>true if the event was handled</returns>
        private bool ProcessUp(object sender, int current_stylus_id, bool is_mouse_device, PointObject<Tx, Ty> target_object, Point device_position)
        {
            //Debug.WriteLine("UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP");
            bool move_multiple = false;
            if (is_mouse_device)
                move_multiple = SelectedObjects.Count > 1;
            else
                move_multiple = SelectedObjects.Count > 1 && _active_stylus_ids.Keys.Count == 1;

            if (DiagramMode == Controls.DiagramMode.MultiSelect)
                move_multiple = false; // move disabled

            if (move_multiple)
                Debug.WriteLine("move_multiple");

            if (target_object != null)
            {
                target_object.UiManipulationMeta.IsInputDeviceDown = false;
                //_object_mouse_down[target_object] = false;

                //if (!target_object.WasMoved && target_object.IsMoving)
                //{
                //    // dieses objekt wurde nur per stylus angeklickt und nicht verschoben
                //    // diese "gerstik" wird für die mehrfachselektion verwendet
                //    // die stylus id dieses objektes auf die andere setzen wenn noch eine 2. aktiv ist

                //    if(GetActiveStylusCount()==2)
                //    {
                //        int other_id = _active_stylus_ids.Single(id => id.Key != current_stylus_id).Key;
                //        target_object.UiManipulationMeta.RegisteredStylusId = other_id;
                //        target_object.UiManipulationMeta.IsInputDeviceDown = true;
                //    }
                //}

                if (!target_object.UiManipulationMeta.RegisteredStylusId.HasValue || (target_object.UiManipulationMeta.RegisteredStylusId.HasValue && target_object.UiManipulationMeta.RegisteredStylusId.Value != current_stylus_id))
                    return false;
            }

            if (target_object != null && target_object.IsMoving)
            {
                Debug.WriteLine("### Drop Object at position: " + device_position.X + ";" + device_position.Y);

                if (GetActiveStylusCount() > 1 && !target_object.WasMoved) // deselect objects with this stylusid to enable movings of objects with other stylus ids
                {
                    //SelectObject(target_object, true);

                    // sollte in multi-move modus automatisch gehn und hier nicht mehr notwendig sein

                    //// transfer object to first stylus id to enable multi-move
                    //int first_stylus_id = _active_stylus_ids.Keys.First();
                    //target_object.UiManipulationMeta.RegisteredStylusId = first_stylus_id;
                    //target_object.UiManipulationMeta.IsInputDeviceDown = true;
                }

                Point mouse_pos = device_position;

                Tx new_xvalue = XAxisControl.TransformAxisPixelToValue(mouse_pos.X - (target_object == null ? 0 : (target_object.UiManipulationMeta.InputDeviceOffsetX == double.MaxValue ? 0 : target_object.UiManipulationMeta.InputDeviceOffsetX))); //_x_offset[current_stylus_id]);
                Ty new_yvalue = YAxisControl.TransformAxisPixelToValue(mouse_pos.Y - (target_object == null ? 0 : (target_object.UiManipulationMeta.InputDeviceOffsetY == double.MaxValue ? 0 : target_object.UiManipulationMeta.InputDeviceOffsetY))); //_y_offset[current_stylus_id]);

                // required because when moving multiple objects the values are not stored and the xoffset is not correct (especially when using the touch!).
                target_object.SetValues(new_xvalue, new_yvalue);
                double _drag_offset_x_1 = mouse_pos.X - (target_object == null ? 0 : target_object.UiManipulationMeta.StartInputDevicePosition.X); // _last_mouse_pos[current_stylus_id].X;
                double _drag_offset_y_1 = mouse_pos.Y - (target_object == null ? 0 : target_object.UiManipulationMeta.StartInputDevicePosition.Y); // _last_mouse_pos[current_stylus_id].Y;
                if (move_multiple)
                {
                    foreach (PointObject<Tx, Ty> point_object in SelectedObjects)
                    {
                        if (point_object.Equals(target_object) || (GetActiveStylusCount() > 1 && !point_object.HasSameStylusId(target_object)))
                            continue;

                        double origin_x = XAxisControl.TransformAxisValueToPixel(point_object.LastX);
                        double origin_y = YAxisControl.TransformAxisValueToPixel(point_object.LastY);
                        Tx new_xvalue_1 = XAxisControl.TransformAxisPixelToValue(origin_x + _drag_offset_x_1);
                        Ty new_yvalue_1 = YAxisControl.TransformAxisPixelToValue(origin_y + _drag_offset_y_1);
                        point_object.SetValues(new_xvalue_1, new_yvalue_1);
                    }
                }


                //Debug.WriteLine("Before DecideDockableCandidates om ProcessUP, new_xvalue "+ new_xvalue);

                DecideDockableCandidates(target_object);
                double x_offset;
                double y_offset;
                CalculateMovingOffset(target_object, out x_offset, out y_offset);

                //Debug.WriteLine("### x, yoffset " + x_offset + " " + y_offset);
                bool ignore_not_can_move_on_multi_selection;
                bool can_move = target_object.CanMoveTo(new_xvalue, new_yvalue, x_offset, y_offset, out ignore_not_can_move_on_multi_selection);
                bool any_can_move_and_ignore = can_move && ignore_not_can_move_on_multi_selection;

                //reset marker visibility
                target_object.AssociatedObjectMarker.IsMoving = false;

                if (move_multiple)
                {
                    foreach (PointObject<Tx, Ty> point_object in SelectedObjects)
                    {
                        if (point_object.Equals(target_object) || (GetActiveStylusCount() > 1 && !point_object.HasSameStylusId(target_object)))
                            continue;

                        double origin_x = XAxisControl.TransformAxisValueToPixel(point_object.LastX);
                        double origin_y = YAxisControl.TransformAxisValueToPixel(point_object.LastY);
                        Tx new_xvalue_1 = XAxisControl.TransformAxisPixelToValue(origin_x + _drag_offset_x_1);
                        Ty new_yvalue_1 = YAxisControl.TransformAxisPixelToValue(origin_y + _drag_offset_y_1);
                        bool ignore_not_can_move;
                        bool can_move_multiple = point_object.CanMoveTo(new_xvalue_1, new_yvalue_1, x_offset, y_offset, out ignore_not_can_move);

                        if (!can_move_multiple)
                            can_move = false;


                        if (ignore_not_can_move)
                            ignore_not_can_move_on_multi_selection = true;

                        if (can_move_multiple && ignore_not_can_move)
                            any_can_move_and_ignore = true;

                        //reset marker visibility
                        point_object.AssociatedObjectMarker.IsMoving = false;
                    }
                }

                if (any_can_move_and_ignore)
                    can_move = true;



                if (!can_move)
                {
                    InitDockableCandidates(target_object);

                    if (target_object.UiManipulationMeta.RegisteredStylusId.Value == current_stylus_id)
                    {
                        target_object.ResetMoveAndResize();

                        if (move_multiple)
                        {
                            foreach (PointObject<Tx, Ty> point_object in SelectedObjects)
                            {
                                if (point_object.Equals(target_object) || (GetActiveStylusCount() > 1 && !point_object.HasSameStylusId(target_object)))
                                    continue;

                                point_object.ResetMoveAndResize();
                            }
                        }
                    }
                }
                else
                {
                    double left_offset;
                    double right_offset;
                    double bottom_offset;
                    target_object.IsDockingMoveObject(out left_offset, out right_offset, out bottom_offset);

                    //Debug.WriteLine("### MY x,yoffset " + my_x_offset + " " + my_y_offset + " for object at " + target_object.XValue + target_object.YValue
                    //    + " LRB: " + left_offset + " " + right_offset + " " + bottom_offset);
                    //target_object.MoveTo(new_xvalue, new_yvalue, my_x_offset, my_y_offset);
                    target_object.MoveTo(new_xvalue, new_yvalue, x_offset, y_offset);



                    double _drag_offset_x = mouse_pos.X - (target_object == null ? 0 : target_object.UiManipulationMeta.StartInputDevicePosition.X); // _last_mouse_pos[current_stylus_id].X;
                    double _drag_offset_y = mouse_pos.Y - (target_object == null ? 0 : target_object.UiManipulationMeta.StartInputDevicePosition.Y); // _last_mouse_pos[current_stylus_id].Y;

                    if (move_multiple)
                    {
                        List<PointObject<Tx, Ty>> objects_to_deselect = new List<PointObject<Tx, Ty>>();
                        foreach (PointObject<Tx, Ty> point_object in SelectedObjects)
                        {
                            if (point_object.Equals(target_object) || (GetActiveStylusCount() > 1 && !point_object.HasSameStylusId(target_object)))
                                continue;

                            double origin_x = XAxisControl.TransformAxisValueToPixel(point_object.LastX);
                            double origin_y = YAxisControl.TransformAxisValueToPixel(point_object.LastY);
                            Tx new_xvalue_1 = XAxisControl.TransformAxisPixelToValue(origin_x + _drag_offset_x);
                            Ty new_yvalue_1 = YAxisControl.TransformAxisPixelToValue(origin_y + _drag_offset_y);

                            point_object.IsDockingMoveObject(out left_offset, out right_offset, out bottom_offset);

                            //Debug.WriteLine("### MY x,yoffset " + my_x_offset + " " + my_y_offset + " for object at " + point_object.XValue + point_object.YValue
                            //    + " LRB: " + left_offset + " " + right_offset + " " + bottom_offset);
                            //point_object.MoveTo(new_xvalue_1, new_yvalue_1, my_x_offset, my_y_offset);
                            point_object.MoveTo(new_xvalue_1, new_yvalue_1, x_offset, y_offset);
                            if (GetActiveStylusCount() > 1) // deselect objects with this stylusid to enable movings of objects with other stylus ids
                                objects_to_deselect.Add(point_object);
                            //SelectObject(point_object, false);
                        }

                        foreach (PointObject<Tx, Ty> point_object in objects_to_deselect)
                        {
                            SelectObject(point_object, false);
                        }
                    }
                }

                if (!target_object.WasMoved && target_object.UiManipulationMeta.RegisteredStylusId != null && target_object.UiManipulationMeta.RegisteredStylusId.Value == current_stylus_id
                    && !target_object.UiManipulationMeta.SelectedWithDownAction)
                    SelectObject(target_object, true);
            }
            else if (_sizing_object != null && target_object != null && target_object.IsResizing)
            {
                if (_sizing_object.AmIAnArea)
                {
                    Point mouse_pos = device_position;

                    Tx current_from_x_value = _sizing_object.XValue;
                    Ty current_from_y_value = _sizing_object.YValue;

                    Tx current_to_x_value = ((AreaObject<Tx, Ty>)_sizing_object).ToXValue;
                    Ty current_to_y_value = ((AreaObject<Tx, Ty>)_sizing_object).ToYValue;


                    Tx target_from_x_value = current_from_x_value;
                    Ty target_from_y_value = current_from_y_value;

                    Tx target_to_x_value = current_to_x_value;
                    Ty target_to_y_value = current_to_y_value;

                    Tx new_xvalue = XAxisControl.TransformAxisPixelToValue(mouse_pos.X - (target_object == null ? 0 : (target_object.UiManipulationMeta.InputDeviceOffsetX == double.MaxValue ? 0 : target_object.UiManipulationMeta.InputDeviceOffsetX))); //_x_offset[current_stylus_id]);
                    Ty new_yvalue = YAxisControl.TransformAxisPixelToValue(mouse_pos.Y - (target_object == null ? 0 : (target_object.UiManipulationMeta.InputDeviceOffsetY == double.MaxValue ? 0 : target_object.UiManipulationMeta.InputDeviceOffsetY))); //_y_offset[current_stylus_id]);

                    switch (_resizing_mode)
                    {
                        case ObjectSelectionMarker.ResizingMode.HorizontalLeft:
                            {
                                if (_sizing_object.CanResizeX)
                                    target_from_x_value = new_xvalue;
                            }
                            ; break;
                        case ObjectSelectionMarker.ResizingMode.HorizontalRight:
                            {
                                if (_sizing_object.CanResizeX)
                                    target_to_x_value = new_xvalue;
                            }
                            ; break;
                        case ObjectSelectionMarker.ResizingMode.VerticalTop:
                            {
                                if (_sizing_object.CanResizeY)
                                    target_to_y_value = new_yvalue;

                            }
                            ; break;
                        case ObjectSelectionMarker.ResizingMode.VerticalBottom:
                            {
                                if (_sizing_object.CanResizeY)
                                    target_from_y_value = new_yvalue;
                            }; break;
                        case ObjectSelectionMarker.ResizingMode.BothLT:
                            {
                                if (_sizing_object.CanResizeX)
                                    target_from_x_value = new_xvalue;
                                if (_sizing_object.CanResizeY)
                                    target_to_y_value = new_yvalue;
                            }
                            ; break;
                        case ObjectSelectionMarker.ResizingMode.BothLB:
                            {
                                if (_sizing_object.CanResizeX)
                                    target_from_x_value = new_xvalue;
                                if (_sizing_object.CanResizeY)
                                    target_from_y_value = new_yvalue;

                            }
                            ; break;
                        case ObjectSelectionMarker.ResizingMode.BothRT:
                            {
                                if (_sizing_object.CanResizeX)
                                    target_to_x_value = new_xvalue;
                                if (_sizing_object.CanResizeY)
                                    target_to_y_value = new_yvalue;
                            }
                            ; break;
                        case ObjectSelectionMarker.ResizingMode.BothRB:
                            {
                                if (_sizing_object.CanResizeX)
                                    target_to_x_value = new_xvalue;

                                if (_sizing_object.CanResizeY)
                                    target_from_y_value = new_yvalue;
                            }
                            ; break;
                    }

                    bool can_resize = _sizing_object.CanResizeTo(target_from_x_value, target_from_y_value, target_to_x_value, target_to_y_value);
                    if (!can_resize)
                    {
                        _sizing_object.ResetMoveAndResize();
                    }
                    else
                    {
                        _sizing_object.ResizeTo(target_from_x_value, target_from_y_value, target_to_x_value, target_to_y_value);
                    }
                }
            }
            else
            {
                if (target_object == null)
                {
                    DeregisterStylusAction(current_stylus_id);
                    StopDragging(current_stylus_id);
                    StopResizing(current_stylus_id);
                    return true;
                }

                if (!_is_resizing_object && !_is_dragging_object)
                {
                    SelectObject(target_object, true);
                }
            }

            DeregisterStylusAction(current_stylus_id);
            StopDragging(current_stylus_id);
            StopResizing(current_stylus_id);

            target_object.UiManipulationMeta.SelectedWithDownAction = false;

            if (target_object != null && !SelectedObjects.Contains(target_object))
                target_object.UiManipulationMeta.ResetUiManipulationValues();

            return true;
        }

        private void CalculateMovingOffset(PointObject<Tx, Ty> target_object, out double x_offset, out double y_offset)
        {
            y_offset = double.MaxValue;

            double left_most_offset = double.MaxValue;
            double right_most_offset = double.MaxValue;

            foreach (PointObject<Tx, Ty> selected_object in SelectedObjects)
            {
                if (!selected_object.HasSameStylusId(target_object))
                    continue;

                double left_offset;
                double right_offset;
                double bottom_offset;
                if (selected_object.IsDockingMoveObject(out left_offset, out right_offset, out bottom_offset))
                {
                    if (left_offset < left_most_offset)
                        left_most_offset = left_offset;
                    if (right_offset < right_most_offset)
                        right_most_offset = right_offset;
                    if (bottom_offset < y_offset)
                        y_offset = bottom_offset;
                }
            }

            if (left_most_offset < right_most_offset)
                x_offset = -left_most_offset;
            else
                x_offset = right_most_offset;

            if (x_offset == double.MaxValue)
                x_offset = 0;
            if (y_offset == double.MaxValue)
                y_offset = 0;
        }

        /// <summary>
        /// Process the down event from the mouse/or stylus device
        /// </summary>
        /// <param name="sender">original event sender</param>
        /// <param name="current_stylus_id">id of the current stylus device</param>
        /// <param name="is_mouse_device">a flag indicating if the event was triggered by the mouse</param>
        /// <param name="target_object">target chart object</param>
        /// <param name="device_position">current device position in the element canvas</param>
        /// <returns>true if the event was handled</returns>
        private bool ProcessDown(object sender, int current_stylus_id, bool is_mouse_device, PointObject<Tx, Ty> target_object, Point device_position)
        {
            RegisterStylusAction(current_stylus_id, sender as FrameworkElement, true);

            EnableElementCanvasMouseCapture();

            target_object.UiManipulationMeta.InputDeviceOffsetX = double.MaxValue;
            target_object.UiManipulationMeta.InputDeviceOffsetY = double.MaxValue;

            target_object.UiManipulationMeta.IsInputDeviceDown = true;

            target_object.UiManipulationMeta.RegisteredStylusId = current_stylus_id;

            if (DiagramMode == Controls.DiagramMode.MultiSelect && _multi_select_mode_stylus == null)
            {
                // wenn sich das diagram im multiselect modus befindet 
                // und noch kein stylus als "master stylus" abgespeichert wurde
                // -> den aktuellen stylus als stylus speichern, welche dann auch die bewegung durchführen wird
                _multi_select_mode_stylus = current_stylus_id;

                if (SelectedObjects != null && SelectedObjects.Count > 0)
                {
                    // wenn es bereits ausgewählte objekte gibt, dann werden diese auf die master stylus id gelenkt
                    foreach (PointObject<Tx, Ty> sel_object in SelectedObjects)
                    {
                        if (sel_object.UiManipulationMeta != null)
                            sel_object.UiManipulationMeta.RegisteredStylusId = _multi_select_mode_stylus;
                    }
                }
            }
            else if (DiagramMode == Controls.DiagramMode.MultiSelect && _multi_select_mode_stylus != null)
            {
                // wenn sich das diagram im multiselect modus befindet
                // und bereits ein "master stylus" gespeichert wurde
                // dann die UI aktion auf den master stylus binden
                target_object.UiManipulationMeta.RegisteredStylusId = _multi_select_mode_stylus.Value;
            }

            target_object.UiManipulationMeta.StartInputDevicePosition = device_position;

            if (DiagramMode == Controls.DiagramMode.Normal && current_stylus_id != int.MinValue)
            {
                // wenn auf ein element geklickt wird, welches sich im normal-modus NICHT
                // in der aktuellen Selektion befindet, so wird die aktuelle Selektion des Stylus gelöscht
                if (!target_object.IsSelected)
                    ClearSelectionOfStylus(current_stylus_id);
            }
            return true;
        }
        #endregion

        #region Scrolling and Zooming

        protected Point GetMousePosition(System.Windows.Input.MouseWheelEventArgs e)
        {
            return e.GetPosition(_element_scrolling);
        }

        protected Point GetMousePosition(MouseEventArgs e)
        {
            return e.GetPosition(_element_scrolling);
        }

        protected Point GetStylusPosition(StylusEventArgs e)
        {
            return e.GetPosition(_element_scrolling);
        }

        private void UpdateModeMenuButtonState()
        {

            _mode_menu_button.Visibility = Visibility.Collapsed;

            if ((_diagram_config != null && _diagram_config.AllowMultipleSelection))
            {
                if (IsZoomActive)
                {
                    _mode_menu_button.Visibility = System.Windows.Visibility.Visible;
                    _mode_menu_button.Content = _toolbutton_content;
                    _mode_menu_button.ToolTip = "Diagramwerkzeuge...";
                }
                else
                {
                    _mode_menu_button.Visibility = System.Windows.Visibility.Visible;
                    _mode_menu_button.Content = _mehrfachauswahl_button_content;
                    HideModeMenu();

                    if (IsMehrfachAuswahlActive)
                        _mode_menu_button.ToolTip = "Mehrfachauswahl deaktivieren";
                    else
                        _mode_menu_button.ToolTip = "Mehrfachauswahl aktivieren";
                }
            }
            else
            {
                if (IsZoomActive)
                {
                    _mode_menu_button.Visibility = System.Windows.Visibility.Visible;
                    _mode_menu_button.Content = _reset_zoom_button_content;
                    _mode_menu_button.ToolTip = "Zoom zurück setzen";
                }
                else
                {
                    _mode_menu_button.Visibility = Visibility.Collapsed;
                }
            }

            return;

        }

        private Image CreateBitmap(FrameworkElement framework_element)
        {
            BitmapImage bitmap = UserControlConverterFunctions.GenerateBitmapImage(framework_element);

            if (bitmap == null)
                return null;

            Image image = new Image();
            image.Source = bitmap;

            ScaleTransform scale_transform = new ScaleTransform();
            image.RenderTransform = scale_transform;

            return image;
        }

        private void AfterZoomfactorXChanged(double old_zoom_factor)
        {
            IsZoomActive = ZoomfactorY != 1.0d || ZoomfactorX != 1.0d;

            if (_ignore_zoom_changes)
                return;

            // new code - indirect zoom change using scaling with final width change
            /*
            if(_zoom_finished_timer != null)
            {
                _zoom_finished_timer.Stop();
            }
            else
            {
                _zoom_finished_timer = new System.Timers.Timer(ZOOM_END_TIMEOUT);
                _zoom_finished_timer.Elapsed += new System.Timers.ElapsedEventHandler(_zoom_finished_timer_Elapsed);
            }

            ScaleTransform currentTransform = new ScaleTransform(1, 1);
            if (_elements_canvas.RenderTransform is ScaleTransform)
            {
                currentTransform = _elements_canvas.RenderTransform as ScaleTransform;
            }

            currentTransform.ScaleX = ZoomfactorX / _root_zoom_x;

            _elements_canvas.RenderTransform = currentTransform;
            //_background_elements_canvas.LayoutTransform = currentTransform;
            _background_elements_canvas.Width = (_element_scrolling.ActualWidth * ZoomfactorX) + _background_scrolling.ActualWidth - _element_scrolling.ActualWidth;
            _zoom_finished_timer.Start();
            */


            // OLD Code - direct zoom change

            double old_width = _elements_canvas.Width;
            _elements_canvas.Width = _element_scrolling.ActualWidth * ZoomfactorX;
            _background_elements_canvas.Width = _elements_canvas.Width + _background_scrolling.ActualWidth - _element_scrolling.ActualWidth;

            double xoffset_dif = _elements_canvas.Width - old_width;

            //ScrollOffsetX += xoffset_dif;

            UpdateModeMenuButtonState();

        }



        private void AfterZoomfactorYChanged(double old_zoom_factor)
        {
            IsZoomActive = ZoomfactorY != 1.0d || ZoomfactorX != 1.0d;

            if (_ignore_zoom_changes)
                return;

            // new code - indirect zoom change using scaling with final width change
            /*
            if (_zoom_finished_timer != null)
            {
                _zoom_finished_timer.Stop();
            }
            else
            {
                _zoom_finished_timer = new System.Timers.Timer(ZOOM_END_TIMEOUT);
                _zoom_finished_timer.Elapsed += new System.Timers.ElapsedEventHandler(_zoom_finished_timer_Elapsed);
            }

            ScaleTransform currentTransform = new ScaleTransform(1, 1);
            if (_elements_canvas.RenderTransform is ScaleTransform)
            {
                currentTransform = _elements_canvas.RenderTransform as ScaleTransform;
            }

            currentTransform.ScaleY = ZoomfactorY / _root_zoom_y;

            _elements_canvas.RenderTransform = currentTransform;
            //_background_elements_canvas.LayoutTransform = currentTransform;
            _background_elements_canvas.Height = (_element_scrolling.ActualHeight * ZoomfactorY) + _background_scrolling.ActualHeight - _element_scrolling.ActualHeight;
            _zoom_finished_timer.Start();
            */

            // OLD Code - direct zoom change
            double old_height = _elements_canvas.Height;
            _elements_canvas.Height = _element_scrolling.ActualHeight * ZoomfactorY;
            _background_elements_canvas.Height = _elements_canvas.Height + _background_scrolling.ActualHeight - _element_scrolling.ActualHeight;

            double yoffset_dif = _elements_canvas.Height - old_height;


            ScrollOffsetY += yoffset_dif;

            UpdateModeMenuButtonState();

        }

        void _zoom_finished_timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            _zoom_finished_timer.Stop();
            Debug.WriteLine("### UPDATE ZOOM");
            Dispatcher.Invoke(DispatcherPriority.Normal, (NoParas)delegate
            {
                _elements_canvas.RenderTransform = null;
                _elements_canvas.Width = _element_scrolling.ActualWidth * ZoomfactorX;
                _elements_canvas.Height = _element_scrolling.ActualHeight * ZoomfactorY;

                _root_zoom_x = ZoomfactorX;
                _root_zoom_y = ZoomfactorY;

                UpdateModeMenuButtonState();
            });
        }

        private void AfterSrcollOffsetXChanged()
        {
            if (_ignore_scroll_changes)
                return;

            double max_scroll_x = _elements_canvas.Width - _element_scrolling.ActualWidth;

            if (max_scroll_x <= 0 && ScrollOffsetX > 0)
            {
                _ignore_scroll_changes = true;
                ScrollOffsetX = 0;
                _ignore_scroll_changes = false;
            }
            else
            {
                if (ScrollOffsetX > max_scroll_x)
                {
                    _ignore_scroll_changes = true;
                    ScrollOffsetX = max_scroll_x;
                    _ignore_scroll_changes = false;
                }
                else if (ScrollOffsetX < 0)
                {
                    _ignore_scroll_changes = true;
                    ScrollOffsetX = 0;
                    _ignore_scroll_changes = false;
                }
            }

            Canvas.SetLeft(_elements_canvas, ScrollOffsetX * (-1));
            Canvas.SetLeft(_background_elements_canvas, ScrollOffsetX * (-1));
        }

        private void AfterSrcollOffsetYChanged()
        {
            if (_ignore_scroll_changes)
                return;

            double max_scroll_y = _elements_canvas.Height - _element_scrolling.ActualHeight;

            if (max_scroll_y <= 0 && ScrollOffsetY > 0)
            {
                _ignore_scroll_changes = true;
                ScrollOffsetY = 0;
                _ignore_scroll_changes = false;
            }
            else
            {
                if (ScrollOffsetY > max_scroll_y)
                {
                    _ignore_scroll_changes = true;
                    ScrollOffsetY = max_scroll_y;
                    _ignore_scroll_changes = false;
                }
                else if (ScrollOffsetY < 0)
                {
                    _ignore_scroll_changes = true;
                    ScrollOffsetY = 0;
                    _ignore_scroll_changes = false;
                }
            }

            Canvas.SetTop(_elements_canvas, ScrollOffsetY * (-1));
            Canvas.SetTop(_background_elements_canvas, ScrollOffsetY * (-1));
        }
        #endregion

        #region Diagram modes
        protected virtual void OnAfterDiagramModeChanged(DiagramMode old_mode)
        {
        }

        private void AfterDiagramModeChanged(DiagramMode old_mode)
        {
            OnAfterDiagramModeChanged(old_mode);
        }

        private Storyboard GetModeMenuActiveStoryboard()
        {
            if (_modemenu_active_sb == null)
            {
                _modemenu_active_sb = new Storyboard();

                DoubleAnimation anim1 = new DoubleAnimation(0, 1, new Duration(TimeSpan.FromMilliseconds(400)));
                anim1.AccelerationRatio = 0.7;
                anim1.DecelerationRatio = 0.3;
                //Storyboard.SetTargetName(anim1, "tab_item_grid");
                Storyboard.SetTarget(anim1, _mode_menu_grid);
                Storyboard.SetTargetProperty(anim1, new PropertyPath("(RenderTransform).(ScaleTransform.ScaleX)"));

                DoubleAnimation anim2 = new DoubleAnimation(0, 1, new Duration(TimeSpan.FromMilliseconds(400)));
                anim2.AccelerationRatio = 0.7;
                anim2.DecelerationRatio = 0.3;
                //Storyboard.SetTargetName(anim2, "tab_item_grid");
                Storyboard.SetTarget(anim2, _mode_menu_grid);
                Storyboard.SetTargetProperty(anim2, new PropertyPath("(RenderTransform).(ScaleTransform.ScaleY)"));

                _modemenu_active_sb.Children.Add(anim1);
                _modemenu_active_sb.Children.Add(anim2);
            }

            return _modemenu_active_sb;
        }

        private Storyboard GetModeMenuInActiveStoryboard()
        {
            if (_modemenu_inactive_sb == null)
            {
                _modemenu_inactive_sb = new Storyboard();

                DoubleAnimation anim1 = new DoubleAnimation(1, 0, new Duration(TimeSpan.FromMilliseconds(400)));
                anim1.AccelerationRatio = 0.3;
                anim1.DecelerationRatio = 0.7;
                //Storyboard.SetTargetName(anim1, "tab_item_grid");
                Storyboard.SetTarget(anim1, _mode_menu_grid);
                Storyboard.SetTargetProperty(anim1, new PropertyPath("(RenderTransform).(ScaleTransform.ScaleX)"));

                DoubleAnimation anim2 = new DoubleAnimation(1, 0, new Duration(TimeSpan.FromMilliseconds(400)));
                anim2.AccelerationRatio = 0.3;
                anim2.DecelerationRatio = 0.7;
                //Storyboard.SetTargetName(anim2, "tab_item_grid");
                Storyboard.SetTarget(anim2, _mode_menu_grid);
                Storyboard.SetTargetProperty(anim2, new PropertyPath("(RenderTransform).(ScaleTransform.ScaleY)"));

                _modemenu_inactive_sb.Children.Add(anim1);
                _modemenu_inactive_sb.Children.Add(anim2);
            }

            return _modemenu_inactive_sb;
        }
        #endregion
        #endregion

        #region Event Handlers
        /// <summary>
        /// Called if the collection of background object changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void _chart_background_objects_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (PointObject<Tx, Ty> newItem in e.NewItems)
                {
                    newItem.ObjectVisual.Tag = newItem;

                    newItem.IsBackgroundObject = true;
                    newItem.HostingDiagram = this;
                    if (!_background_elements_canvas.Children.Contains(newItem.ObjectVisual))
                        _background_elements_canvas.Children.Add(newItem.ObjectVisual);
                }
            }

            if (e.OldItems != null)
            {
                foreach (PointObject<Tx, Ty> delItem in e.OldItems)
                {
                    if (_background_elements_canvas.Children.Contains(delItem.ObjectVisual))
                    {
                        _background_elements_canvas.Children.Remove(delItem.ObjectVisual);
                        delItem.HostingDiagram = null;
                    }
                }
            }
            else
            {
                if (e.Action == NotifyCollectionChangedAction.Reset)
                {
                    _background_elements_canvas.Children.Clear();
                }
            }
        }
        /// <summary>
        /// Called if the collection of data object changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void _chart_data_objects_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            NotifyCollectionChangedAction notify_collection_changed_action = e.Action;
            if (e.NewItems != null)
            {
                foreach (PointObject<Tx, Ty> newItem in e.NewItems)
                {
                    //ObjectSelectionMarker cur_marker = _child_markers.SingleOrDefault(m => m.ChildVisual == newItem.ObjectVisual);
                    ObjectSelectionMarker cur_marker = newItem.AssociatedObjectMarker;

                    newItem.ObjectVisual.Tag = newItem;
                    newItem.UiManipulationMeta.ResetUiManipulationValues(); // ensures that StylusId is on all objects equal!

                    if (cur_marker == null)
                    {
                        newItem.IsBackgroundObject = false;
                        ObjectSelectionMarker newMarker = new ObjectSelectionMarker(newItem.ObjectVisual, newItem.CanResizeX, newItem.CanResizeY);

                        newItem.HostingDiagram = this;

                        if (SelectionStyle != null)
                            newMarker.Style = SelectionStyle;

                        Binding selection_binding = new Binding("IsSelected");
                        selection_binding.Source = newItem;
                        newMarker.SetBinding(ObjectSelectionMarker.IsSelectedProperty, selection_binding);

                        Binding hittest_visible_binding_binding = new Binding("IsHitTestVisible");
                        hittest_visible_binding_binding.Source = newItem;
                        newMarker.SetBinding(IsHitTestVisibleProperty, hittest_visible_binding_binding);

                        Binding visibility_binding = new Binding("Visibility");
                        visibility_binding.Source = newItem.ObjectVisual;
                        visibility_binding.Mode = BindingMode.OneWay;
                        newMarker.SetBinding(VisibilityProperty, visibility_binding);

                        Binding x_discrete_binding = new Binding("IsDiscrete");
                        x_discrete_binding.Source = GetXAxisControl().Config;
                        newItem.SetBinding(PointObject<Tx, Ty>.IsXDiscreteProperty, x_discrete_binding);

                        Binding y_discrete_binding = new Binding("IsDiscrete");
                        y_discrete_binding.Source = GetYAxisControl().Config;
                        newItem.SetBinding(PointObject<Tx, Ty>.IsYDiscreteProperty, y_discrete_binding);

                        _elements_canvas.Children.Add(newMarker);
                        //_child_markers.Add(newMarker);
                        newItem.AssociatedObjectMarker = newMarker;
                        newMarker.AssociatedPointObject = newItem;

                        RegisterObjectEvents(newItem);

                        // apply current filter for this item and
                        // adjust visibility
                        IsItemFiltered(newItem);
                    }

                    //if (!_elements_canvas.Children.Contains(newItem.ObjectVisual))
                    //{
                    //    _elements_canvas.Children.Add(newItem.ObjectVisual);
                    //    RegisterObjectEvents(newItem);
                    //}

                }

            }

            if (e.OldItems != null)
            {
                foreach (PointObject<Tx, Ty> delItem in e.OldItems)
                {

                    //ObjectSelectionMarker cur_marker = _child_markers.SingleOrDefault(m => m.ChildVisual == delItem.ObjectVisual);
                    ObjectSelectionMarker cur_marker = delItem.AssociatedObjectMarker;

                    if (cur_marker != null)
                    {
                        _elements_canvas.Children.Remove(cur_marker);
                        cur_marker.ReleaseChildVisual();
                        BindingOperations.ClearBinding(cur_marker, ObjectSelectionMarker.IsSelectedProperty);
                        BindingOperations.ClearBinding(cur_marker, VisibilityProperty);
                        BindingOperations.ClearBinding(cur_marker, IsHitTestVisibleProperty);
                        BindingOperations.ClearBinding(delItem, PointObject<Tx, Ty>.IsXDiscreteProperty);
                        BindingOperations.ClearBinding(delItem, PointObject<Tx, Ty>.IsYDiscreteProperty);
                        DeRegisterObjectEvents(delItem);
                        //_child_markers.Remove(cur_marker);

                        delItem.HostingDiagram = null;
                        cur_marker.Dispose();
                    }

                    //if (_elements_canvas.Children.Contains(delItem.ObjectVisual))
                    //{
                    //    _elements_canvas.Children.Remove(delItem.ObjectVisual);
                    //    DeRegisterObjectEvents(delItem);
                    //}
                }
            }
            else
            {
                if (e.Action == NotifyCollectionChangedAction.Reset)
                {
                    List<ObjectSelectionMarker> current_markers = new List<ObjectSelectionMarker>();
                    foreach (UIElement child in _elements_canvas.Children)
                    {
                        if (child is ObjectSelectionMarker)
                        {
                            current_markers.Add(child as ObjectSelectionMarker);
                        }
                        //else
                        //{
                        //    Debug.WriteLine("No OBJECTSELECTIONMARKER " + child);
                        //}
                    }

                    foreach (ObjectSelectionMarker cur_marker in current_markers)
                    {
                        PointObject<Tx, Ty> delItem = cur_marker.AssociatedPointObject as PointObject<Tx, Ty>;

                        if (delItem != null)
                        {
                            _elements_canvas.Children.Remove(cur_marker);
                            cur_marker.ReleaseChildVisual();
                            BindingOperations.ClearBinding(cur_marker, ObjectSelectionMarker.IsSelectedProperty);
                            BindingOperations.ClearBinding(cur_marker, VisibilityProperty);
                            BindingOperations.ClearBinding(cur_marker, IsHitTestVisibleProperty);
                            BindingOperations.ClearBinding(delItem, PointObject<Tx, Ty>.IsXDiscreteProperty);
                            BindingOperations.ClearBinding(delItem, PointObject<Tx, Ty>.IsYDiscreteProperty);
                            DeRegisterObjectEvents(delItem);
                            //_child_markers.Remove(cur_marker);

                            delItem.HostingDiagram = null;
                            cur_marker.Dispose();
                        }
                        //else
                        //    Debug.WriteLine("NO MARKER " + cur_marker);
                    }
                }
            }

            //Debug.WriteLine("CHILDREN SIZE " + _elements_canvas.Children.Count);
        }

        void _selected_chart_data_objects_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (PointObject<Tx, Ty> newItem in e.NewItems)
                {
                }
            }

            if (e.OldItems != null)
            {
                foreach (PointObject<Tx, Ty> delItem in e.OldItems)
                {
                }
            }
            else
            {
                if (e.Action == NotifyCollectionChangedAction.Reset)
                {
                    foreach (PointObject<Tx, Ty> curObj in SelectedObjects)
                        curObj.IsSelected = false;
                }
            }

        }

        #region size changed and scollring events

        void _element_scrolling_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (ZoomfactorX == 1.0 && ZoomfactorY == 1.0)
            {
                // adjust width and height of element and background canvas
                _elements_canvas.Width = _element_scrolling.ActualWidth;
                _elements_canvas.Height = _element_scrolling.ActualHeight;

                _background_elements_canvas.Width = _background_scrolling.ActualWidth;
                _background_elements_canvas.Height = _background_scrolling.ActualHeight;
            }
            else
            {
                if (!_ignore_actual_size_changed)
                {
                    // TODO: center element and backgorund canvas
                    double current_offset_x = ScrollOffsetX;
                    double current_offset_y = ScrollOffsetY;

                    _elements_canvas.Width = _element_scrolling.ActualWidth * ZoomfactorX;
                    _elements_canvas.Height = _element_scrolling.ActualHeight * ZoomfactorY;

                    _background_elements_canvas.Width = _elements_canvas.Width + _background_scrolling.ActualWidth -
                                                        _element_scrolling.ActualWidth;
                    _background_elements_canvas.Height = _elements_canvas.Height + _background_scrolling.ActualHeight -
                                                         _element_scrolling.ActualHeight;

                    ScrollOffsetX = current_offset_x * ZoomfactorX;
                    ScrollOffsetY = current_offset_y * ZoomfactorY;
                }
            }

            if (_reset_ignore_actual_size_changed && _ignore_actual_size_changed)
            {
                _ignore_actual_size_changed = false;
                _reset_ignore_actual_size_changed = false;
            }

            if (!_ignore_actual_size_changed)
            {
                _actual_scroll_height = _element_scrolling.ActualHeight;
            }
        }

        #endregion

        #region object events, visual and canvas mouse events
        /// <summary>
        /// Called if the ItemsFilterExpression property changes
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        static void OnItemFilterExpressionChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is DiagramBase<Tx, Ty>)
            {
                DiagramBase<Tx, Ty> diagram = d as DiagramBase<Tx, Ty>;
                diagram.ApplyFilter();
            }
        }

        /// <summary>
        /// Called if the CursorMarkerVisiblity property changes
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        static void OnCursorMarkerVisibilityChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is DiagramBase<Tx, Ty>)
            {
                DiagramBase<Tx, Ty> diagram = d as DiagramBase<Tx, Ty>;
                if (diagram._force_cursor_visibility && !diagram._resetting_dependency_property)
                {
                    diagram._old_marker_visibility = (CursorMarkerVisibility)e.NewValue;
                    diagram._resetting_dependency_property = true;
                    diagram.SetValue(e.Property, e.OldValue);
                    diagram._resetting_dependency_property = false;
                }
            }
        }


        /// <summary>
        /// Called if the IsLoading property changes
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        static void OnIsLoadingChangedChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is DiagramBase<Tx, Ty>)
            {
                DiagramBase<Tx, Ty> diagram = d as DiagramBase<Tx, Ty>;
                diagram.SendPropertyChanged("IsLoading");
            }
        }

        /// <summary>
        /// Called if the ZoomfactorX property changes
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        static void OnZoomfactorXChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is DiagramBase<Tx, Ty>)
            {
                DiagramBase<Tx, Ty> diagram = d as DiagramBase<Tx, Ty>;
                //diagram.SendPropertyChanged("ZoomfactorX");
                diagram.AfterZoomfactorXChanged((double)e.OldValue);

            }
        }

        /// <summary>
        /// Called if the ZoomfactorY property changes
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        static void OnZoomfactorYChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is DiagramBase<Tx, Ty>)
            {
                DiagramBase<Tx, Ty> diagram = d as DiagramBase<Tx, Ty>;
                //diagram.SendPropertyChanged("ZoomfactorY");
                diagram.AfterZoomfactorYChanged((double)e.OldValue);
            }
        }

        /// <summary>
        /// Called if the ScrollOffsetX property changes
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        static void OnScrollOffsetXChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is DiagramBase<Tx, Ty>)
            {
                DiagramBase<Tx, Ty> diagram = d as DiagramBase<Tx, Ty>;
                //diagram.SendPropertyChanged("ScrollOffsetX");
                diagram.AfterSrcollOffsetXChanged();
            }
        }

        /// <summary>
        /// Called if the ScrollOffsetY property changes
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        static void OnScrollOffsetYChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is DiagramBase<Tx, Ty>)
            {
                DiagramBase<Tx, Ty> diagram = d as DiagramBase<Tx, Ty>;
                //diagram.SendPropertyChanged("ScrollOffsetY");
                diagram.AfterSrcollOffsetYChanged();
            }
        }

        /// <summary>
        /// Called if the DiagramMode property changes
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        static void OnDiagramModeChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is DiagramBase<Tx, Ty>)
            {
                DiagramBase<Tx, Ty> diagram = d as DiagramBase<Tx, Ty>;
                //diagram.SendPropertyChanged("ScrollOffsetY");
                diagram.AfterDiagramModeChanged((DiagramMode)e.OldValue);
            }
        }

        #region reset zoom click
        void _mode_menu_button_Click(object sender, RoutedEventArgs e)
        {


            if ((_diagram_config != null && _diagram_config.AllowMultipleSelection))
            {
                if (IsZoomActive)
                {
                    if (_is_mode_menu_visible)
                    {
                        HideModeMenu();
                    }
                    else
                    {
                        ShowModeMenu();
                    }
                }
                else
                {
                    ToggleMehrfachAuswahl();
                }
            }
            else
            {
                ResetZoom();
            }
        }

        private void ToggleMehrfachAuswahl()
        {
            if (this.DiagramMode == Controls.DiagramMode.Normal)
            {
                _multi_select_mode_stylus = null; // resetet die mehrfach stylus auswahl id
                this.DiagramMode = Controls.DiagramMode.MultiSelect;
                IsMehrfachAuswahlActive = true;
                UpdateModeMenuButtonState();

            }
            else
            {
                this.DiagramMode = Controls.DiagramMode.Normal;
                IsMehrfachAuswahlActive = false;
                UpdateModeMenuButtonState();
            }
        }

        void mode_menu_control_MehrfachAuswahlClick(object sender, EventArgs e)
        {
            ToggleMehrfachAuswahl();

            if (!IsMehrfachAuswahlActive)
                HideModeMenu();
        }

        void mode_menu_control_ResetZoomClick(object sender, EventArgs e)
        {
            ResetZoom();
            HideModeMenu();
        }

        void mode_menu_control_ButtonClick(object sender, EventArgs e)
        {

        }

        protected void ShowModeMenu()
        {
            if (_is_mode_menu_visible)
                return;

            Point menu_bottom_point = GetModeMenuPoint();

            Canvas.SetLeft(_mode_menu_grid, menu_bottom_point.X);
            Canvas.SetTop(_mode_menu_grid, menu_bottom_point.Y - _mode_menu_grid.ActualHeight);

            Storyboard sb_active = GetModeMenuActiveStoryboard();
            if (sb_active != null)
            {
                this.BeginStoryboard(sb_active, HandoffBehavior.Compose);
                _is_mode_menu_visible = true;
            }
        }

        protected void HideModeMenu()
        {
            if (!_is_mode_menu_visible)
                return;

            Storyboard sb_inactive = GetModeMenuInActiveStoryboard();
            if (sb_inactive != null)
            {
                this.BeginStoryboard(sb_inactive, HandoffBehavior.Compose);
                _is_mode_menu_visible = false;
            }
        }
        #endregion

        #region Background Canvas MOUSE events

        void _background_scrolling_MouseLeave(object sender, MouseEventArgs e)
        {
            _background_elements_canvas_MouseLeave(sender, e);
        }

        void _background_elements_canvas_StylusUp(object sender, StylusEventArgs e)
        {
            if (_caputre_stylus_up_of_background)
            {
                _caputre_stylus_up_of_background = false;

                Debug.WriteLine("##### BACKGROUND LEAVE");
                //StylusEventArgs raise_up_event_args = new StylusEventArgs(e.StylusDevice, 1);
                //raise_up_event_args.RoutedEvent = UIElement.StylusUpEvent;
                //_elements_canvas.RaiseEvent(raise_up_event_args);

                if (_active_ui_redirection != null && _active_ui_redirection.MouseUpRedirection != null)
                {
                    _active_ui_redirection.StylusUpRedirection(GetRootFrameworkElementOfStylus(GetStylusId(e.StylusDevice)), e);
                    return;
                }


                if (_is_dragging_object) // && _dragactive_object != null)
                {
                    visual_StylusUp(GetRootFrameworkElementOfStylus(GetStylusId(e.StylusDevice)), e);
                }
                else if (_is_resizing_object)
                {
                    visual_StylusUp(GetRootFrameworkElementOfStylus(GetStylusId(e.StylusDevice)), e);
                }
                else
                {
                    SelectObject(null, false);
                }
            }
        }

        void _background_scrolling_StylusLeave(object sender, StylusEventArgs e)
        {
            if (!IsMultiTouchEnabled || e.StylusDevice == null)
                return;

            if (_is_scrolling)
            {
                _element_scrolling.IsHitTestVisible = true;
                _is_scrolling = false;
                _last_scroll_pos = default(Point);
                _last_scroll_pos_backgrund_element = default(Point);
                Mouse.OverrideCursor = _pre_scroll_cursor;
                e.Handled = true;

                return;
            }

            if (_is_multi_selecting)
            {
                _element_scrolling.IsHitTestVisible = true;
                _is_multi_selecting = false;
                _last_scroll_pos = default(Point);
                _last_scroll_pos_backgrund_element = default(Point);
                Mouse.OverrideCursor = _pre_scroll_cursor;
                e.Handled = true;
                return;
            }
        }

        void _background_elements_canvas_MouseLeave(object sender, MouseEventArgs e)
        {
            if (IsMultiTouchEnabled && e.StylusDevice != null)
                return;

            if (_is_scrolling)
            {
                _element_scrolling.IsHitTestVisible = true;
                _is_scrolling = false;
                _last_scroll_pos = default(Point);
                _last_scroll_pos_backgrund_element = default(Point);
                Mouse.OverrideCursor = _pre_scroll_cursor;
                e.Handled = true;
                return;
            }

            if (_is_multi_selecting)
            {
                _element_scrolling.IsHitTestVisible = true;
                _is_multi_selecting = false;
                _last_scroll_pos = default(Point);
                _last_scroll_pos_backgrund_element = default(Point);
                Mouse.OverrideCursor = _pre_scroll_cursor;
                e.Handled = true;
                return;
            }
        }

        /// <summary>
        /// Called if the mouse went down on the background canvas
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void _background_elements_canvas_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (IsMultiTouchEnabled && e.StylusDevice != null)
                return;

            if (e.ChangedButton == MouseButton.Left)
            {
                _last_scroll_pos = e.GetPosition(_background_scrolling);
                _last_scroll_pos_backgrund_element = e.GetPosition(_background_elements_canvas);
                _element_scrolling.IsHitTestVisible = false;
                _pre_scroll_cursor = Mouse.OverrideCursor;
                Mouse.OverrideCursor = Cursors.ScrollAll;
                if (DiagramMode == Controls.DiagramMode.Normal)
                {
                    _is_scrolling = true;
                    _scrolling_start_x = ScrollOffsetX;
                    _scrolling_start_y = ScrollOffsetY;
                }
                else
                {
                    _is_multi_selecting = true;
                }

                e.Handled = true;
            }

        }

        /// <summary>
        /// Called if the mouse moves on the background canvas
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void _background_elements_canvas_MouseMove(object sender, MouseEventArgs e)
        {
            if (IsMultiTouchEnabled && e.StylusDevice != null)
                return;

            UpdateCurrentCursorValues();

            if (_is_scrolling)
            {
                Point cur_position = e.GetPosition(_background_scrolling);
                Vector dif = cur_position - _last_scroll_pos;
                _last_scroll_pos = cur_position;

                ScrollOffsetX += dif.X * (-1);
                ScrollOffsetY += dif.Y * (-1);
                e.Handled = true;
                return;
            }
            else if (_is_multi_selecting)
            {
                Point pos1 = _last_scroll_pos_backgrund_element;
                Point pos2 = e.GetPosition(_background_elements_canvas);

                Point normalized_pos1 = new Point(pos1.X - ScrollOffsetX, pos1.Y - ScrollOffsetY);
                Point normalized_pos2 = new Point(pos2.X - ScrollOffsetX, pos2.Y - ScrollOffsetY);

                DrawSelectionRectangle(normalized_pos1, normalized_pos2);
            }
        }

        /// <summary>
        /// Called if a mouse button was releasen on the background canvas
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void _background_elements_canvas_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (IsMultiTouchEnabled && e.StylusDevice != null)
                return;

            if (_is_scrolling)
            {
                _element_scrolling.IsHitTestVisible = true;
                _is_scrolling = false;
                _last_scroll_pos = default(Point);
                _last_scroll_pos_backgrund_element = default(Point);
                Mouse.OverrideCursor = _pre_scroll_cursor;

                if(!_is_dragging_object && !_is_resizing_object && _scrolling_start_x == ScrollOffsetX && _scrolling_start_y == ScrollOffsetY)
                {
                    SelectObject(null, false);
                }

                _scrolling_start_x = double.MinValue;
                _scrolling_start_y = double.MaxValue;

                e.Handled = true;
                return;
            }
            else if (_is_multi_selecting)
            {
                _element_scrolling.IsHitTestVisible = true;
                _is_multi_selecting = false;
                _last_scroll_pos = default(Point);
                _last_scroll_pos_backgrund_element = default(Point);
                Mouse.OverrideCursor = _pre_scroll_cursor;
                e.Handled = true;

                ProcessMultiSelectionRectangle();
                // hides the selection rectangle
                DrawSelectionRectangle(default(Point), default(Point));

                ToggleMehrfachAuswahl();

                return;
            }

            if (_active_ui_redirection != null && _active_ui_redirection.MouseUpRedirection != null)
            {
                _active_ui_redirection.MouseUpRedirection(GetRootFrameworkElementOfStylus(GetStylusId(e.StylusDevice)), e);
                return;
            }

            if (e.ChangedButton == MouseButton.Left)
            {
                if (_is_dragging_object) // && _dragactive_object != null)
                {
                    visual_MouseUp(GetRootFrameworkElementOfStylus(GetStylusId(e.StylusDevice)), e);
                }
                else if (_is_resizing_object)
                {
                    visual_MouseUp(GetRootFrameworkElementOfStylus(GetStylusId(e.StylusDevice)), e);
                }
                else
                {
                    SelectObject(null, false);
                }
            }
        }

        void _background_elements_canvas_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            if (IsMultiTouchEnabled && e.StylusDevice != null)
                return;

            DiagramWheel(sender, e);
        }

        public virtual void KeyDownPressed(object sender, KeyEventArgs e, bool in_x_scale_zoom_mode)
        {
            bool axis_zoom_mode = false;
            Point position = Mouse.GetPosition(_elements_canvas);
            if (position.X <= 0 || position.X >= _elements_canvas.ActualWidth || position.Y <= 0)
                return;

            if (position.Y >= _elements_canvas.ActualHeight)
            {
                if (position.Y < _elements_canvas.ActualHeight + XAxisControl.ActualHeight && in_x_scale_zoom_mode)
                    axis_zoom_mode = true;
                else
                    return;
            }

            bool wheel_zoom_out = false;
            bool wheel_zoom_in = false;
            KeyStates state_ctrl = Keyboard.GetKeyStates(Key.LeftCtrl);
            KeyStates state_plus = Keyboard.GetKeyStates(Key.OemPlus);
            KeyStates state_minus = Keyboard.GetKeyStates(Key.OemMinus);

            KeyStates state_num_plus = Keyboard.GetKeyStates(Key.Add);          // num-block +
            KeyStates state_num_minus = Keyboard.GetKeyStates(Key.Subtract);    // num-block -

            bool ctrl_down = (state_ctrl & KeyStates.Down) > 0;
            bool plus_down = (state_plus & KeyStates.Down) > 0 || (state_num_plus & KeyStates.Down) > 0;
            bool minus_down = (state_minus & KeyStates.Down) > 0 || (state_num_minus & KeyStates.Down) > 0;

            if (ctrl_down)
            {
                if (plus_down)
                    wheel_zoom_in = true;
                else if (minus_down)
                    wheel_zoom_out = true;
                else
                    return;
            }
            else
                return;

            if (axis_zoom_mode)
            {

                if (wheel_zoom_in)
                    XAxisZoomIn(position);
                else if (wheel_zoom_out)
                    XAxisZoomOut(position);
            }
            else
            {
                if (wheel_zoom_in)
                    DiagramZoomIn(position.X, position.Y);
                else if (wheel_zoom_out)
                    DiagramZoomOut(position.X, position.Y);
            }
        }


        #endregion

        #region Background - Canvas TOUCH events

        void _scale_touch_intertia_manipulator_HideRightClickSimulationFeedback(object sender, EventArgs e)
        {
            if (!Dispatcher.CheckAccess())
            {
                Dispatcher.Invoke(DispatcherPriority.Normal, (NoParas)delegate
                {
                    _scale_touch_intertia_manipulator_HideRightClickSimulationFeedback(sender, e);
                });
                return;
            }

            _right_click_feedback_circle.Visibility = System.Windows.Visibility.Collapsed;
        }

        void _scale_touch_intertia_manipulator_ShowRightClickSimulationFeedback(object sender, Logicx.DiagramLib.UIManipulation.DiagramScaleTouchInertiaProcessManipulation.ShowRightClickInfoEventArgs e)
        {
            if (!Dispatcher.CheckAccess())
            {
                Dispatcher.Invoke(DispatcherPriority.Normal, (NoParas)delegate
                                                                           {
                                                                               _scale_touch_intertia_manipulator_ShowRightClickSimulationFeedback(sender, e);
                                                                           });
                return;
            }

            _right_click_feedback_circle.Stroke = Brushes.DarkGray;
            _right_click_feedback_circle.Fill = GenerateFeedbackBrush();
            _right_click_feedback_circle.Effect = _drop_shadow_right_click_animation;

            double anim_dur = 0.5;

            Canvas.SetLeft(_right_click_feedback_circle, e.DownPosition.X - ScrollOffsetX);
            Canvas.SetTop(_right_click_feedback_circle, e.DownPosition.Y - ScrollOffsetY);

            _right_click_feedback_circle.Visibility = System.Windows.Visibility.Visible;

            //PathGeometry animationPath = new PathGeometry();
            //animationPath.AddGeometry(_right_click_color_animation_path);
            Storyboard myStorybard = new Storyboard();

            PathGeometry animationPath = new PathGeometry();
            PathFigure pFigure = new PathFigure();
            pFigure.StartPoint = new Point(0.5, 0);

            PolyBezierSegment pBezierSegment = new PolyBezierSegment();

            pBezierSegment.Points.Add(new Point(0.75, 0));
            pBezierSegment.Points.Add(new Point(1, 0.25));
            pBezierSegment.Points.Add(new Point(1, 0.5));
            pBezierSegment.Points.Add(new Point(1, 0.75));
            pBezierSegment.Points.Add(new Point(0.75, 1));
            pBezierSegment.Points.Add(new Point(0.5, 1));
            pBezierSegment.Points.Add(new Point(0.25, 1));
            pBezierSegment.Points.Add(new Point(0, 0.75));
            pBezierSegment.Points.Add(new Point(0, 0.5));
            pBezierSegment.Points.Add(new Point(0, 0.25));
            pBezierSegment.Points.Add(new Point(0.25, 0));
            pBezierSegment.Points.Add(new Point(0.5, 0));

            pFigure.Segments.Add(pBezierSegment);
            animationPath.Figures.Add(pFigure);


            PointAnimationUsingPath pointAnim = new PointAnimationUsingPath();
            pointAnim.DecelerationRatio = 1;
            pointAnim.PathGeometry = animationPath;
            pointAnim.Duration = new Duration(TimeSpan.FromSeconds(anim_dur));

            myStorybard.Children.Add(pointAnim);

            Storyboard.SetTargetProperty(pointAnim, new PropertyPath("Fill.GradientOrigin"));
            myStorybard.Completed += myStorybard_Completed;

            _right_click_feedback_circle.BeginStoryboard(myStorybard, HandoffBehavior.SnapshotAndReplace);
        }

        void myStorybard_Completed(object sender, EventArgs e)
        {
            _right_click_feedback_circle.Stroke = Brushes.DarkGreen;
            _right_click_feedback_circle.Fill = new SolidColorBrush(Color.FromArgb(0xff, 0x5b, 0xa7, 0x55));
            //_right_click_feedback_circle.Effect = _drop_shadow_right_click_animation_finished;
        }

        void _scale_touch_intertia_manipulator_ScalingChanged(object sender, DiagramScaleTouchInertiaProcessManipulation.DiagramScaleScaleingEventArgs e)
        {
            if (DiagramMode == Controls.DiagramMode.Normal)
            {
                DiagramTouchZoom(e);
            }
        }

        void _scale_touch_intertia_manipulator_StylusMoveWiredThrough(object sender, StylusEventArgs e)
        {
            if (DiagramMode == Controls.DiagramMode.MultiSelect)
            {
                _touch_multiselected = false;
                ShowTouchSelectionRectangle(e);
            }
        }

        void _scale_touch_intertia_manipulator_StylusUpWiredThrough(object sender, StylusEventArgs e)
        {
            if (DiagramMode == Controls.DiagramMode.MultiSelect)
            {
                if (!_touch_multiselected)
                {
                    ProcessMultiSelectionRectangle();
                    // hides the selection rectangle
                    DrawSelectionRectangle(default(Point), default(Point));
                    _touch_multiselected = true;

                    ToggleMehrfachAuswahl();
                }
            }
        }

        void _scale_touch_intertia_manipulator_TranslationChanged(object sender, DiagramScaleTouchInertiaProcessManipulation.DiagrammScaleMoveEventArgs e)
        {
            DiagramTouchScroll(e);
        }


        void _scale_touch_intertia_manipulator_UiManipulationInertiaCompleted(object sender, UiManipulationEventArgs e)
        {
            ElementCanvas.IsHitTestVisible = true;

            if (_scale_touch_intertia_manipulator.SimulateRightMouseClick)
            {
                MouseButtonEventArgs raise_up_event_args = new MouseButtonEventArgs(Mouse.PrimaryDevice, 1, MouseButton.Right, null);
                raise_up_event_args.RoutedEvent = UIElement.MouseUpEvent;

                MouseButtonEventArgs raise_down_event_args = new MouseButtonEventArgs(Mouse.PrimaryDevice, 0, MouseButton.Right, null);
                raise_down_event_args.RoutedEvent = UIElement.MouseDownEvent;

                BaseFrame parent_frame = RightMouseClickSimulationTargetFrame;
                if (parent_frame != null)
                {
                    parent_frame.RaiseEvent(raise_down_event_args);
                    parent_frame.RaiseEvent(raise_up_event_args);
                }
                else
                {
                    _background_elements_canvas.RaiseEvent(raise_down_event_args);
                    _background_elements_canvas.RaiseEvent(raise_up_event_args);
                }
            }
        }

        void _scale_touch_intertia_manipulator_UiManipulationStarted(object sender, UiManipulationEventArgs e)
        {
            ElementCanvas.IsHitTestVisible = false;
        }

        #endregion

        /// <summary>
        /// Called if the mouse moves within the base grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void _base_grid_MouseMove(object sender, MouseEventArgs e)
        {
            UpdateCurrentCursorValues();
        }

        void tmp_control_MouseMove(object sender, MouseEventArgs e)
        {
            UpdateCurrentCursorValues();
        }

        void XAxisControl_MouseMove(object sender, MouseEventArgs e)
        {
            UpdateCurrentCursorValues();
        }

        void YAxisControl_MouseMove(object sender, MouseEventArgs e)
        {
            UpdateCurrentCursorValues();
        }

        /// <summary>
        /// Calles if the mouse moves within the elements canvas
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void _elements_canvas_MouseMove(object sender, MouseEventArgs e)
        {
            if (_active_ui_redirection != null && _active_ui_redirection.MouseMoveRedirection != null)
            {
                _active_ui_redirection.MouseMoveRedirection(GetRootFrameworkElementOfStylus(GetStylusId(e.StylusDevice)), e);
                e.Handled = true;
                return;
            }

            if (_is_dragging_object) // && _dragactive_object != null)
            {
                visual_MouseMove(GetRootFrameworkElementOfStylus(GetStylusId(e.StylusDevice)), e);
                e.Handled = true;
            }
            else if (_is_resizing_object)
            {
                visual_MouseMove(GetRootFrameworkElementOfStylus(GetStylusId(e.StylusDevice)), e);
                e.Handled = true;
            }
        }
        /// <summary>
        /// Called if a mouse button went up in the elements canvas
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void _elements_canvas_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (_active_ui_redirection != null && _active_ui_redirection.MouseUpRedirection != null)
            {
                _active_ui_redirection.MouseUpRedirection(GetRootFrameworkElementOfStylus(GetStylusId(e.StylusDevice)), e);
                e.Handled = true;
                return;
            }

            if (_is_dragging_object) // && _dragactive_object != null)
            {
                visual_MouseUp(GetRootFrameworkElementOfStylus(GetStylusId(e.StylusDevice)), e);
                e.Handled = true;
            }
            else if (_is_resizing_object)
            {
                visual_MouseUp(GetRootFrameworkElementOfStylus(GetStylusId(e.StylusDevice)), e);
                e.Handled = true;
            }
            else
            {
                SelectObject(null, false);
                e.Handled = true;
                _elements_canvas.Background = null; // disable background caputre
            }
        }
        /// <summary>
        /// Called if a mpuse button went down in the element canvas
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void _elements_canvas_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (_active_ui_redirection != null && _active_ui_redirection.MouseDownRedirection != null)
            {
                _active_ui_redirection.MouseDownRedirection(GetRootFrameworkElementOfStylus(GetStylusId(e.StylusDevice)), e);
                e.Handled = true;
                return;
            }

            if (_is_dragging_object) // && _dragactive_object != null)
            {
                visual_MouseDown(GetRootFrameworkElementOfStylus(GetStylusId(e.StylusDevice)), e);
                e.Handled = true;
            }
            else if (_is_resizing_object)
            {
                visual_MouseDown(GetRootFrameworkElementOfStylus(GetStylusId(e.StylusDevice)), e);
                e.Handled = true;
            }
        }

        //TODO: Element Canvas - Touch events ???

        void _element_scrolling_MouseLeave(object sender, MouseEventArgs e)
        {
            return;

            if (IsMultiTouchEnabled && e.StylusDevice != null)
                return;

            Debug.WriteLine("##### BACKGROUND LEAVE");
            MouseButtonEventArgs raise_up_event_args = new MouseButtonEventArgs(Mouse.PrimaryDevice, 1, MouseButton.Right, null);
            raise_up_event_args.RoutedEvent = UIElement.MouseUpEvent;

            _elements_canvas.RaiseEvent(raise_up_event_args);
        }

        void _element_scrolling_StylusLeave(object sender, StylusEventArgs e)
        {
            if (!IsMultiTouchEnabled || e.StylusDevice == null)
                return;

            _caputre_stylus_up_of_background = true;
        }

        void _elements_canvas_StylusMove(object sender, StylusEventArgs e)
        {
            if (_active_ui_redirection != null && _active_ui_redirection.StylusMoveRedirection != null)
            {
                _active_ui_redirection.StylusMoveRedirection(GetRootFrameworkElementOfStylus(GetStylusId(e.StylusDevice)) ?? sender, e);
                e.Handled = true;
                return;
            }

            if (_is_dragging_object) // && _dragactive_object != null)
            {
                visual_StylusMove(GetRootFrameworkElementOfStylus(GetStylusId(e.StylusDevice)), e);
                e.Handled = true;
            }
            else if (_is_resizing_object)
            {
                visual_StylusMove(GetRootFrameworkElementOfStylus(GetStylusId(e.StylusDevice)), e);
                e.Handled = true;
            }
        }

        void _elements_canvas_StylusUp(object sender, StylusEventArgs e)
        {
            if (_active_ui_redirection != null && _active_ui_redirection.StylusUpRedirection != null)
            {
                _active_ui_redirection.StylusUpRedirection(GetRootFrameworkElementOfStylus(GetStylusId(e.StylusDevice)) ?? sender, e);
                e.Handled = true;
                return;
            }

            if (_is_dragging_object) // && _dragactive_object != null)
            {
                visual_StylusUp(GetRootFrameworkElementOfStylus(GetStylusId(e.StylusDevice)), e);
                e.Handled = true;
            }
            else if (_is_resizing_object)
            {
                visual_StylusUp(GetRootFrameworkElementOfStylus(GetStylusId(e.StylusDevice)), e);
                e.Handled = true;
            }
            else
            {
                SelectObject(null, false);
                e.Handled = true;
                _elements_canvas.Background = null; // disable background caputre
            }
        }

        void _elements_canvas_StylusDown(object sender, StylusDownEventArgs e)
        {
            if (_active_ui_redirection != null && _active_ui_redirection.StylusDownRedirection != null)
            {
                _active_ui_redirection.StylusDownRedirection(GetRootFrameworkElementOfStylus(GetStylusId(e.StylusDevice)) ?? sender, e);
                e.Handled = true;
                return;
            }

            if (_is_dragging_object) // && _dragactive_object != null)
            {
                visual_StylusDown(GetRootFrameworkElementOfStylus(GetStylusId(e.StylusDevice)), e);
                e.Handled = true;
            }
            else if (_is_resizing_object)
            {
                visual_StylusDown(GetRootFrameworkElementOfStylus(GetStylusId(e.StylusDevice)), e);
                e.Handled = true;
            }
        }

        #region object property change events
        /// <summary>
        /// A property of the diargram object has changed its value
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void diagram_object_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
        }
        /// <summary>
        /// A property of the diagram object is about to change its value
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void diagram_object_PropertyChanging(object sender, System.ComponentModel.PropertyChangingEventArgs e)
        {

        }
        #endregion

        #region object-visual mouse events
        /// <summary>
        /// Called if the mouse moves within the object visual
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void visual_MouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        {
            if (e.StylusDevice != null)
                return;

            //Debug.WriteLine("visual_MouseMove current Selection count: " + SelectedObjects.Count
            //    + " Mouse down? " + _object_mose_down + ", isdragging_object " + _is_dragging_object + ", dragactive_object " + _dragactive_object
            //    + ", isresizingobject " + _is_resizing_object + ", sizing_object " + _sizing_object);
            UpdateCurrentCursorValues();

            int? check_stylus_id = ValidateStylusDeviceRegistration(e.StylusDevice, true);
            if (!check_stylus_id.HasValue)
                return;

            int current_stylus_id = check_stylus_id.Value;

            PointObject<Tx, Ty> target_object = GetObjectOfVisual(sender as FrameworkElement);
            if (target_object == null)
                return;

            if (target_object.UiManipulationMeta.RegisteredStylusId == null || target_object.UiManipulationMeta.RegisteredStylusId.Value != current_stylus_id)
                return;

            e.Handled = ProcessMove(sender, current_stylus_id, true, target_object, e.GetPosition(_elements_canvas));
        }

        /// <summary>
        /// Called if a the mouse button went up within the object visual
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void visual_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            int? check_stylus_id = ValidateStylusDeviceRegistration(e.StylusDevice, true);
            if (!check_stylus_id.HasValue)
                return;

            int current_stylus_id = check_stylus_id.Value;

            if (e.ChangedButton == MouseButton.Left)
            {
                PointObject<Tx, Ty> target_object = GetObjectOfVisual(sender as FrameworkElement);

                _elements_canvas.Cursor = Cursors.Arrow;

                e.Handled = ProcessUp(sender, current_stylus_id, true, target_object, e.GetPosition(_elements_canvas));
            }
        }
        /// <summary>
        /// Called if the a mouse button went down within a object visual
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void visual_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (e.StylusDevice != null)
                return;

            int current_stylus_id = GetStylusId(e.StylusDevice);

            if (e.ChangedButton == MouseButton.Left)
            {
                PointObject<Tx, Ty> target_object = GetObjectOfVisual(sender as FrameworkElement);
                if (target_object == null)
                    return;

                e.Handled = ProcessDown(sender, current_stylus_id, true, target_object, e.GetPosition(_elements_canvas));
            }
        }
        /// <summary>
        /// Called if the mouse leaves an object visual
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void visual_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            if (e.StylusDevice != null)
                return;

            FrameworkElement visual = sender as FrameworkElement;

            if (_is_resizing_object || _is_dragging_object)
                return;

            visual.Cursor = Cursors.Arrow;
            _elements_canvas.Cursor = Cursors.Arrow;

            PointObject<Tx, Ty> target_object = GetObjectOfVisual(visual);
            if (target_object == null)
                return;

            e.Handled = true;
        }
        /// <summary>
        /// Called if the mouse enters an object visual
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void visual_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            if (e.StylusDevice != null)
                return;

            FrameworkElement visual = sender as FrameworkElement;

            if (_is_resizing_object || _is_dragging_object)
                return;

            PointObject<Tx, Ty> target_object = GetObjectOfVisual(visual);
            if (target_object == null)
                return;

            if (target_object.CanMove)
            {
                visual.Cursor = Cursors.SizeAll;
                _elements_canvas.Cursor = Cursors.SizeAll;
            }
            else if (target_object.CanSelect)
            {
                visual.Cursor = Cursors.Hand;
                _elements_canvas.Cursor = Cursors.Hand;
            }

            e.Handled = true;
        }
        #endregion

        #region Touch events
        /// <summary>
        /// Called if the stylus device moves
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void visual_StylusMove(object sender, StylusEventArgs e)
        {
            int? check_stylus_id = ValidateStylusDeviceRegistration(e.StylusDevice, false);
            if (!check_stylus_id.HasValue)
                return;

            int current_stylus_id = check_stylus_id.Value;

            PointObject<Tx, Ty> target_object = GetObjectOfVisual(sender as FrameworkElement);
            if (target_object == null)
                return;

            if (target_object.UiManipulationMeta.RegisteredStylusId == null || target_object.UiManipulationMeta.RegisteredStylusId.Value != current_stylus_id)
                return;

            e.Handled = ProcessMove(sender, current_stylus_id, false, target_object, e.GetPosition(_elements_canvas));
        }

        void visual_StylusEnter(object sender, StylusEventArgs e)
        {
            if (e.StylusDevice == null)
                return;

            int current_stylus_id = GetStylusId(e.StylusDevice);

            PointObject<Tx, Ty> target_object = GetObjectOfVisual(sender as FrameworkElement);
            if (target_object == null)
                return;

            if (target_object.UiManipulationMeta.RegisteredStylusId != null && target_object.UiManipulationMeta.RegisteredStylusId.Value == current_stylus_id)
                RegisterStylusAction(current_stylus_id, sender as FrameworkElement, false);

            //EnableElementCanvasMouseCapture();

            //target_object.UiManipulationMeta.InputDeviceOffsetX = double.MaxValue;
            //target_object.UiManipulationMeta.InputDeviceOffsetY = double.MaxValue;

            //target_object.UiManipulationMeta.IsInputDeviceDown = true;
            //target_object.UiManipulationMeta.RegisteredStylusId = current_stylus_id;

            //target_object.UiManipulationMeta.StartInputDevicePosition = device_position;
        }

        /// <summary>
        /// Called if the stylus device leaves the visual
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void visual_StylusLeave(object sender, StylusEventArgs e)
        {
            if (e.StylusDevice == null)
                return;

            int? check_stylus_id = ValidateStylusDeviceRegistration(e.StylusDevice, false);
            if (!check_stylus_id.HasValue)
                return;

            if (_is_resizing_object || _is_dragging_object)
                return;

            PointObject<Tx, Ty> target_object = GetObjectOfVisual(sender as FrameworkElement);

            //if (target_object != null)
            //{
            //    target_object.UiManipulationMeta.IsInputDeviceDown = false;
            //    //_object_mouse_down[target_object] = false;

            //    if (!target_object.UiManipulationMeta.RegisteredStylusId.HasValue || (target_object.UiManipulationMeta.RegisteredStylusId.HasValue && target_object.UiManipulationMeta.RegisteredStylusId.Value != check_stylus_id.Value))
            //        return;

            //    e.Handled = true;
            //    DeregisterStylusAction(check_stylus_id.Value);
            //}
        }
        /// <summary>
        /// Called if the stylus device went up
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void visual_StylusUp(object sender, StylusEventArgs e)
        {
            int? check_stylus_id = ValidateStylusDeviceRegistration(e.StylusDevice, false);
            if (!check_stylus_id.HasValue)
                return;

            int current_stylus_id = check_stylus_id.Value;

            PointObject<Tx, Ty> target_object = GetObjectOfVisual(sender as FrameworkElement);

            _elements_canvas.Cursor = Cursors.Arrow;

            e.Handled = ProcessUp(sender, current_stylus_id, false, target_object, e.GetPosition(_elements_canvas));
        }
        /// <summary>
        /// Called if a stylus device went down
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void visual_StylusDown(object sender, StylusDownEventArgs e)
        {
            if (e.StylusDevice == null)
                return;

            int current_stylus_id = GetStylusId(e.StylusDevice);

            PointObject<Tx, Ty> target_object = GetObjectOfVisual(sender as FrameworkElement);
            if (target_object == null)
                return;

            e.Handled = ProcessDown(sender, current_stylus_id, false, target_object, e.GetPosition(_elements_canvas));
        }
        #endregion

        /// <summary>
        /// Called if the marker starts reszising
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void cur_marker_StartResizing(object sender, ObjectSelectionMarker.StartResizingEventArgs e)
        {
            //EnableElementCanvasMouseCapture();

            ObjectSelectionMarker marker = sender as ObjectSelectionMarker;
            if (marker == null)
                return;

            Debug.WriteLine("### START RESIZING - MARKER");


            _is_resizing_object = true;
            _sizing_marker_object = marker;
            _old_resize_cursor = _elements_canvas.Cursor;
            _elements_canvas.Cursor = e.ResizeCursor;
            _resizing_cursor = e.ResizeCursor;
            _resizing_mode = e.Mode;
            _old_object_resize_cursor = marker.ChildVisual.Cursor;
            marker.ChildVisual.Cursor = _resizing_cursor;

            PointObject<Tx, Ty> target_object = GetObjectOfVisual(marker.ChildVisual);
            _sizing_object = target_object;


            if (target_object != null && !target_object.AmIAnArea)
            {
                StopResizing();
            }
            else
            {
                ProcessDown(marker.ChildVisual, e.StylusId, e.IsMouse, target_object, new Point(0, 0));

                _sizing_object.BeginMoveOrResize(false, true);
                if (marker != null)
                    Canvas.SetZIndex(marker, 10000);
            }
        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            if (_disposed || _disposing)
                return;

            _disposing = true;

            if (_zoom_finished_timer != null)
            {
                _zoom_finished_timer.Stop();
                _zoom_finished_timer.Dispose();
                _zoom_finished_timer = null;
            }

            Dispose(true);
            ResetZoom();

            if (this.DiagramMode == Controls.DiagramMode.MultiSelect)
            {
                this.DiagramMode = Controls.DiagramMode.Normal;
                IsMehrfachAuswahlActive = false;
                UpdateModeMenuButtonState();
            }

            //_child_markers.Clear();
            _chart_data_objects.Clear();
            _chart_background_objects.Clear();

            _chart_data_objects.CollectionChanged -= _chart_data_objects_CollectionChanged;
            _chart_background_objects.CollectionChanged -= _chart_background_objects_CollectionChanged;
            EventHelper.RemoveAllCollectionChangedEventHandlers(_chart_data_objects);
            EventHelper.RemoveAllCollectionChangedEventHandlers(_chart_background_objects);

            EventHelper.UnregisterEvents(PropertyChanged);

            if (YAxisControl != null)
                YAxisControl.MouseMove -= YAxisControl_MouseMove;
            if (XAxisControl != null)
                XAxisControl.MouseMove -= XAxisControl_MouseMove;

            _background_elements_canvas.MouseMove -= _background_elements_canvas_MouseMove;
            _background_elements_canvas.MouseUp -= _background_elements_canvas_MouseUp;
            _background_elements_canvas.MouseWheel -= _background_elements_canvas_MouseWheel;

            _elements_canvas.MouseDown -= _elements_canvas_MouseDown;
            _elements_canvas.MouseUp -= _elements_canvas_MouseUp;
            _elements_canvas.MouseMove -= _elements_canvas_MouseMove;
            _base_grid.MouseMove -= _base_grid_MouseMove;

            _header_container.Children.Clear();
            _background_elements_canvas.Children.Clear();
            _elements_canvas.Children.Clear();
            _base_grid.Children.Clear();
            _diagram_base_grid.Children.Clear();

            if (XAxisControl != null)
                XAxisControl.Dispose();

            if (YAxisControl != null)
                YAxisControl.Dispose();

            if (DiagramConfig != null)
                DiagramConfig.Dispose();

            _x_axis_control = null;
            _y_axis_control = null;
            _diagram_config = null;

            if (_scale_touch_intertia_manipulator != null)
            {
                _scale_touch_intertia_manipulator.DeRegister();
                _scale_touch_intertia_manipulator.UiManipulationStarted -= _scale_touch_intertia_manipulator_UiManipulationStarted;
                _scale_touch_intertia_manipulator.UiManipulationInertiaCompleted -= _scale_touch_intertia_manipulator_UiManipulationInertiaCompleted;
                _scale_touch_intertia_manipulator.ScalingChanged -= _scale_touch_intertia_manipulator_ScalingChanged;
                _scale_touch_intertia_manipulator.TranslationChanged -= _scale_touch_intertia_manipulator_TranslationChanged;
            }

            _disposed = true;
        }

        protected virtual void Dispose(bool disposing)
        {

        }

        #endregion
        #endregion

        #region Properties

        public DiagramManager DiagramManager { get; set; }

        /// <summary>
        /// Gets the current diagram config
        /// </summary>
        public DiagramConfig<Tx, Ty> DiagramConfig
        {
            get { return _diagram_config; }
            set
            {
                if (_diagram_config != null & value != null)
                {
                    _diagram_config.InitializeFrom(value);

                    SendPropertyChanged("DiagramConfig");
                }
            }
        }

        private BaseFrame RightMouseClickSimulationTargetFrame
        {
            get
            {

                BaseFrame parent_frame = UIHelper.TryFindParent<BaseFrame>(this);
                return parent_frame;
            }
        }

        /// <summary>
        /// Stores the current visibility mode. If true the diagram is shown minimized
        /// </summary>
        public bool IsMinimized
        {
            get { return (bool)GetValue(IsMinimizedProperty); }
            set { SetValue(IsMinimizedProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsMinimized.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsMinimizedProperty =
            DependencyProperty.Register("IsMinimized", typeof(bool), typeof(DiagramBase<Tx, Ty>), new UIPropertyMetadata(false));



        /// <summary>
        /// Gets the Scale control for the x-axis
        /// </summary>
        public TypedScaleControl<Tx> XAxisControl
        {
            get
            {
                if (_x_axis_control == null)
                    _x_axis_control = GetXAxisControl();

                return _x_axis_control;
            }
        }

        /// <summary>
        /// Gets the Scale control for the y-axis
        /// </summary>
        public TypedScaleControl<Ty> YAxisControl
        {
            get
            {
                if (_y_axis_control == null)
                    _y_axis_control = GetYAxisControl();

                return _y_axis_control;
            }
        }

        /// <summary>
        /// Gets the background element canvas
        /// </summary>
        protected Canvas BackgroundElementCanvas
        {
            get { return _background_elements_canvas; }
        }

        /// <summary>
        /// Gets the collection of dataobjects in the diagram
        /// </summary>
        public ObservableCollection<PointObject<Tx, Ty>> ChartDataObjects
        {
            get { return _chart_data_objects; }
        }

        /// <summary>
        /// Gets the collection of background objects in the diagram
        /// </summary>
        public ObservableCollection<PointObject<Tx, Ty>> ChartBackgroundObjects
        {
            get { return _chart_background_objects; }
        }

        /// <summary>
        /// Gets a list of all selected objects
        /// </summary>
        public List<PointObject<Tx, Ty>> SelectedObjects
        {
            get
            {
                //return _chart_data_objects.Where(o => o.IsSelected).ToList();
                return _selected_chart_data_objects;
            }
        }

        /// <summary>
        /// Gets a list of objects which are currently hidden 
        /// </summary>
        public List<PointObject<Tx, Ty>> HiddenObjects
        {
            get
            {
                return _chart_data_objects.Where(o => o.IsHidden).ToList();
            }
        }
        /// <summary>
        /// A flag which indicates a hosting diagram manager if the diagram can scroll left/right
        /// </summary>
        public virtual bool CanScrollDiagrams
        {
            get { return false; }
        }
        /// <summary>
        /// A flag which indicates if the diagram supports X-Axis zoom
        /// </summary>
        public virtual bool CanZoomXAxis
        {
            get { return false; }
        }
        /// <summary>
        /// A flag which indicates if the diagram supports mouse-wheel interaction
        /// </summary>
        public virtual bool CanWheel
        {
            get { return false; }
        }
        /// <summary>
        /// Signal for aborting background data loading
        /// </summary>
        protected bool AbortBackgroundLoadingSignal
        {
            get { return _abort_background_loading; }
        }
        /// <summary>
        /// Flag which indicates if the loading operation has been aborted
        /// </summary>
        protected bool LastLoadingAborted { get; set; }

        /// <summary>
        /// Gets a flag if the object has been disposed
        /// </summary>
        public bool Disposed
        {
            get { return _disposed; }
        }
        /// <summary>
        /// Gets a flag if the object is disposing
        /// </summary>
        protected bool Disposing
        {
            get { return _disposing; }
        }
        /// <summary>
        /// Flag which activates/deactivates element canvas clipping
        /// </summary>
        public bool ElementClipping
        {
            get { return _elements_canvas.ClipToBounds; }
            set { _elements_canvas.ClipToBounds = value; }
        }
        /// <summary>
        /// Gets the element hosting canvas control
        /// </summary>
        public Canvas ElementCanvas
        {
            get { return _elements_canvas; }
        }

        public bool HasLoadedData
        {
            get { return _has_loaded_data; }
            set
            {
                if (_has_loaded_data != value)
                {
                    _has_loaded_data = value;
                    SendPropertyChanged("HasLoadedData");
                }
            }
        }
        public double VisibleWidth
        {
            get
            {
                if (_element_scrolling != null)
                {
                    return _element_scrolling.ActualWidth;
                }

                return 0;
            }
        }

        public double VisibleHeight
        {
            get
            {
                if (_element_scrolling != null)
                    return _actual_scroll_height;

                return 0;
            }
        }

        /// <summary>
        /// Gets a priority number for the diagram data.
        /// This may be used in an async chainloading szenario to prioritize diagrams
        /// </summary>
        public virtual int ChainDataLoadingPriority
        {
            get { return 0; }
        }

        #endregion

        #region Dependency Properties
        /// <summary>
        /// Gets/Sets the dependency property for the X-Axis visibility
        /// </summary>
        public Visibility XAxisScaleVisibility
        {
            get { return (Visibility)GetValue(XAxisScaleVisibilityProperty); }
            set { SetValue(XAxisScaleVisibilityProperty, value); }
        }

        public static readonly DependencyProperty XAxisScaleVisibilityProperty =
            DependencyProperty.Register("XAxisScaleVisibility", typeof(Visibility), typeof(DiagramBase<Tx, Ty>), new UIPropertyMetadata(null));

        /// <summary>
        /// Gets/Sets the dependency property for the Y-Axis visibility
        /// </summary>
        public Visibility YAxisScaleVisibility
        {
            get { return (Visibility)GetValue(YAxisScaleVisibilityProperty); }
            set { SetValue(YAxisScaleVisibilityProperty, value); }
        }

        public static readonly DependencyProperty YAxisScaleVisibilityProperty =
            DependencyProperty.Register("YAxisScaleVisibility", typeof(Visibility), typeof(DiagramBase<Tx, Ty>), new UIPropertyMetadata(null));


        public Style XAxisScaleTextStyle
        {
            get { return (Style)GetValue(XAxisScaleTextStyleProperty); }
            set { SetValue(XAxisScaleTextStyleProperty, value); }
        }

        //Using a DependencyProperty as the backing store for ScaleTextStyle.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty XAxisScaleTextStyleProperty =
            DependencyProperty.Register("XAxisScaleTextStyle", typeof(Style), typeof(DiagramBase<Tx, Ty>), new UIPropertyMetadata(null));

        public Style XAxisScaleInfoTextStyle
        {
            get { return (Style)GetValue(XAxisScaleInfoTextStyleProperty); }
            set { SetValue(XAxisScaleInfoTextStyleProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ScaleInfoTextStyle.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty XAxisScaleInfoTextStyleProperty =
            DependencyProperty.Register("XAxisScaleInfoTextStyle", typeof(Style), typeof(DiagramBase<Tx, Ty>), new UIPropertyMetadata(null));

        public Brush XAxisLineBrush
        {
            get { return (Brush)GetValue(XAxisLineBrushProperty); }
            set { SetValue(XAxisLineBrushProperty, value); }
        }

        // Using a DependencyProperty as the backing store for LineBrush.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty XAxisLineBrushProperty =
            DependencyProperty.Register("XAxisLineBrush", typeof(Brush), typeof(DiagramBase<Tx, Ty>), new UIPropertyMetadata(Brushes.Black));


        public Style XAxisScaleControlStyle
        {
            get { return (Style)GetValue(XAxisScaleControlStyleProperty); }
            set { SetValue(XAxisScaleControlStyleProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ScaleInfoTextStyle.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty XAxisScaleControlStyleProperty =
            DependencyProperty.Register("XAxisScaleControlStyle", typeof(Style), typeof(DiagramBase<Tx, Ty>), new UIPropertyMetadata(null));

        public Style YAxisScaleTextStyle
        {
            get { return (Style)GetValue(YAxisScaleTextStyleProperty); }
            set { SetValue(YAxisScaleTextStyleProperty, value); }
        }

        //Using a DependencyProperty as the backing store for ScaleTextStyle.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty YAxisScaleTextStyleProperty =
            DependencyProperty.Register("YAxisScaleTextStyle", typeof(Style), typeof(DiagramBase<Tx, Ty>), new UIPropertyMetadata(null));

        public Style YAxisScaleInfoTextStyle
        {
            get { return (Style)GetValue(YAxisScaleInfoTextStyleProperty); }
            set { SetValue(YAxisScaleInfoTextStyleProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ScaleInfoTextStyle.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty YAxisScaleInfoTextStyleProperty =
            DependencyProperty.Register("YAxisScaleInfoTextStyle", typeof(Style), typeof(DiagramBase<Tx, Ty>), new UIPropertyMetadata(null));

        public Brush YAxisLineBrush
        {
            get { return (Brush)GetValue(YAxisLineBrushProperty); }
            set { SetValue(YAxisLineBrushProperty, value); }
        }

        // Using a DependencyProperty as the backing store for LineBrush.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty YAxisLineBrushProperty =
            DependencyProperty.Register("YAxisLineBrush", typeof(Brush), typeof(DiagramBase<Tx, Ty>), new UIPropertyMetadata(Brushes.Black));

        public Style YAxisScaleControlStyle
        {
            get { return (Style)GetValue(YAxisScaleControlStyleProperty); }
            set { SetValue(YAxisScaleControlStyleProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ScaleInfoTextStyle.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty YAxisScaleControlStyleProperty =
            DependencyProperty.Register("YAxisScaleControlStyle", typeof(Style), typeof(DiagramBase<Tx, Ty>), new UIPropertyMetadata(null));

        public Style GridLineStyle
        {
            get { return (Style)GetValue(GridLineStyleProperty); }
            set { SetValue(GridLineStyleProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ScaleInfoTextStyle.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty GridLineStyleProperty =
            DependencyProperty.Register("GridLineStyle", typeof(Style), typeof(DiagramBase<Tx, Ty>), new UIPropertyMetadata(null));


        public Style CursorLineStyle
        {
            get { return (Style)GetValue(CursorLineStyleProperty); }
            set { SetValue(CursorLineStyleProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ScaleInfoTextStyle.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CursorLineStyleProperty =
            DependencyProperty.Register("CursorLineStyle", typeof(Style), typeof(DiagramBase<Tx, Ty>), new UIPropertyMetadata(null));

        public Style ElementCanvasStyle
        {
            get { return (Style)GetValue(ElementCanvasStyleProperty); }
            set { SetValue(ElementCanvasStyleProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ScaleInfoTextStyle.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ElementCanvasStyleProperty =
            DependencyProperty.Register("ElementCanvasStyle", typeof(Style), typeof(DiagramBase<Tx, Ty>), new UIPropertyMetadata(null));

        public Style SelectionStyle
        {
            get { return (Style)GetValue(SelectionStyleProperty); }
            set { SetValue(SelectionStyleProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ScaleInfoTextStyle.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectionStyleProperty =
            DependencyProperty.Register("SelectionStyle", typeof(Style), typeof(DiagramBase<Tx, Ty>), new UIPropertyMetadata(null));

        public Style SelectionRectangleStyle
        {
            get { return (Style)GetValue(SelectionRectangleStyleProperty); }
            set { SetValue(SelectionRectangleStyleProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SelectionRectangleStyle.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectionRectangleStyleProperty =
            DependencyProperty.Register("SelectionRectangleStyle", typeof(Style), typeof(DiagramBase<Tx, Ty>), new UIPropertyMetadata(null));


        /// <summary>
        ///  Item filter expression
        /// </summary>
        public Func<PointObject<Tx, Ty>, bool> ItemFilterExpression
        {
            get { return (Func<PointObject<Tx, Ty>, bool>)GetValue(ItemFilterExpressionProperty); }
            set { SetValue(ItemFilterExpressionProperty, value); }
        }
        public static readonly DependencyProperty ItemFilterExpressionProperty = DependencyProperty.Register("ItemFilterExpression", typeof(Func<PointObject<Tx, Ty>, bool>), typeof(DiagramBase<Tx, Ty>), new UIPropertyMetadata(null, new PropertyChangedCallback(OnItemFilterExpressionChanged)));

        public Style LegendBorderStyle
        {
            get { return (Style)GetValue(LegendBorderStyleProperty); }
            set { SetValue(LegendBorderStyleProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ScaleInfoTextStyle.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty LegendBorderStyleProperty =
            DependencyProperty.Register("LegendBorderStyle", typeof(Style), typeof(DiagramBase<Tx, Ty>), new UIPropertyMetadata(null));


        public FrameworkElement LegendControl
        {
            get { return (FrameworkElement)GetValue(LegendControlProperty); }
            set { SetValue(LegendControlProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ScaleInfoTextStyle.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty LegendControlProperty =
            DependencyProperty.Register("LegendControl", typeof(FrameworkElement), typeof(DiagramBase<Tx, Ty>), new UIPropertyMetadata(null));


        public FrameworkElement MinimizedLegendControl
        {
            get { return (FrameworkElement)GetValue(MinimizedLegendControlProperty); }
            set { SetValue(MinimizedLegendControlProperty, value); }
        }

        // Using a DependencyProperty as the backing store for MinimizedLegendControl.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MinimizedLegendControlProperty =
            DependencyProperty.Register("MinimizedLegendControl", typeof(FrameworkElement), typeof(DiagramBase<Tx, Ty>), new UIPropertyMetadata(null));


        public FrameworkElement LoadingControl
        {
            get { return (FrameworkElement)GetValue(LoadingControlProperty); }
            set { SetValue(LoadingControlProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ScaleInfoTextStyle.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty LoadingControlProperty =
            DependencyProperty.Register("LoadingControl", typeof(FrameworkElement), typeof(DiagramBase<Tx, Ty>), new UIPropertyMetadata(null));


        public CursorMarkerVisibility CursorMarkerVisibility
        {
            get { return (CursorMarkerVisibility)GetValue(CursorMarkerVisibilityProperty); }
            set { SetValue(CursorMarkerVisibilityProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ScaleInfoTextStyle.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CursorMarkerVisibilityProperty =
            DependencyProperty.Register("CursorMarkerVisibility", typeof(CursorMarkerVisibility), typeof(DiagramBase<Tx, Ty>), new UIPropertyMetadata(CursorMarkerVisibility.None, new PropertyChangedCallback(OnCursorMarkerVisibilityChanged)));




        public Tx CurrentCursorXValue
        {
            get { return (Tx)GetValue(CurrentCursorXValueProperty); }
            set { SetValue(CurrentCursorXValuePropertyKey, value); }
        }

        public static readonly DependencyPropertyKey CurrentCursorXValuePropertyKey =
            DependencyProperty.RegisterReadOnly("CurrentCursorXValue", typeof(Tx), typeof(DiagramBase<Tx, Ty>), new UIPropertyMetadata(default(Tx)));

        public static readonly DependencyProperty CurrentCursorXValueProperty = CurrentCursorXValuePropertyKey.DependencyProperty;


        public Ty CurrentCursorYValue
        {
            get { return (Ty)GetValue(CurrentCursorYValueProperty); }
            set { SetValue(CurrentCursorYValuePropertyKey, value); }
        }

        public static readonly DependencyPropertyKey CurrentCursorYValuePropertyKey =
            DependencyProperty.RegisterReadOnly("CurrentCursorYValue", typeof(Ty), typeof(DiagramBase<Tx, Ty>), new UIPropertyMetadata(default(Ty)));

        public static readonly DependencyProperty CurrentCursorYValueProperty = CurrentCursorYValuePropertyKey.DependencyProperty;



        public bool IsLoading
        {
            get { return (bool)GetValue(IsLoadingProperty); }
            set { SetValue(IsLoadingProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsLoading.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsLoadingProperty =
            DependencyProperty.Register("IsLoading", typeof(bool), typeof(DiagramBase<Tx, Ty>), new UIPropertyMetadata(false, new PropertyChangedCallback(OnIsLoadingChangedChanged)));




        public double ZoomfactorX
        {
            get { return (double)GetValue(ZoomfactorXProperty); }
            set { SetValue(ZoomfactorXProperty, value); }
        }

        public static readonly DependencyProperty ZoomfactorXProperty =
            DependencyProperty.Register("ZoomfactorX", typeof(double), typeof(DiagramBase<Tx, Ty>), new UIPropertyMetadata((double)1.0, new PropertyChangedCallback(OnZoomfactorXChanged)));

        public double ZoomfactorY
        {
            get { return (double)GetValue(ZoomfactorYProperty); }
            set { SetValue(ZoomfactorYProperty, value); }
        }

        public static readonly DependencyProperty ZoomfactorYProperty =
            DependencyProperty.Register("ZoomfactorY", typeof(double), typeof(DiagramBase<Tx, Ty>), new UIPropertyMetadata((double)1.0, new PropertyChangedCallback(OnZoomfactorYChanged)));

        public double ScrollOffsetX
        {
            get { return (double)GetValue(ScrollOffsetXProperty); }
            set { SetValue(ScrollOffsetXProperty, value); }
        }

        public static readonly DependencyProperty ScrollOffsetXProperty =
            DependencyProperty.Register("ScrollOffsetX", typeof(double), typeof(DiagramBase<Tx, Ty>), new UIPropertyMetadata((double)0.0, new PropertyChangedCallback(OnScrollOffsetXChanged)));

        public double ScrollOffsetY
        {
            get { return (double)GetValue(ScrollOffsetYProperty); }
            set { SetValue(ScrollOffsetYProperty, value); }
        }

        public static readonly DependencyProperty ScrollOffsetYProperty =
            DependencyProperty.Register("ScrollOffsetY", typeof(double), typeof(DiagramBase<Tx, Ty>), new UIPropertyMetadata((double)0.0, new PropertyChangedCallback(OnScrollOffsetYChanged)));


        public DiagramMode DiagramMode
        {
            get { return (DiagramMode)GetValue(DiagramModeProperty); }
            set { SetValue(DiagramModeProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsLoading.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DiagramModeProperty =
            DependencyProperty.Register("DiagramMode", typeof(DiagramMode), typeof(DiagramBase<Tx, Ty>), new UIPropertyMetadata(DiagramMode.Normal, new PropertyChangedCallback(OnDiagramModeChanged)));



        public Brush ModeMenuStrokeBrush
        {
            get { return (Brush)GetValue(ModeMenuStrokeBrushProperty); }
            set { SetValue(ModeMenuStrokeBrushProperty, value); }
        }

        // Using a DependencyProperty as the backing store for StrokeBrush.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ModeMenuStrokeBrushProperty =
            DependencyProperty.Register("ModeMenuStrokeBrush", typeof(Brush), typeof(DiagramBase<Tx, Ty>), new UIPropertyMetadata(Brushes.Blue));



        public Brush ModeMenuBackgroundBrush
        {
            get { return (Brush)GetValue(ModeMenuBackgroundBrushProperty); }
            set { SetValue(ModeMenuBackgroundBrushProperty, value); }
        }

        // Using a DependencyProperty as the backing store for BackgroundBrush.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ModeMenuBackgroundBrushProperty =
            DependencyProperty.Register("ModeMenuBackgroundBrush", typeof(Brush), typeof(DiagramBase<Tx, Ty>), new UIPropertyMetadata(Brushes.Black));



        public Brush ModeMenuLineBrush
        {
            get { return (Brush)GetValue(ModeMenuLineBrushProperty); }
            set { SetValue(ModeMenuLineBrushProperty, value); }
        }

        // Using a DependencyProperty as the backing store for LineBrush.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ModeMenuLineBrushProperty =
            DependencyProperty.Register("ModeMenuLineBrush", typeof(Brush), typeof(DiagramBase<Tx, Ty>), new UIPropertyMetadata(Brushes.White));

        public BitmapEffect ModeTextGlowBitmapEffect
        {
            get { return (BitmapEffect)GetValue(ModeTextGlowBitmapEffectProperty); }
            set { SetValue(ModeTextGlowBitmapEffectProperty, value); }
        }

        // Using a DependencyProperty as the backing store for LineBrush.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ModeTextGlowBitmapEffectProperty =
            DependencyProperty.Register("ModeTextGlowBitmapEffect", typeof(BitmapEffect), typeof(DiagramBase<Tx, Ty>), new UIPropertyMetadata(null));



        public bool IsZoomActive
        {
            get { return (bool)GetValue(IsZoomActiveProperty); }
            private set { SetValue(IsZoomActivePropertyKey, value); }
        }

        // Using a DependencyProperty as the backing store for LineBrush.  This enables animation, styling, binding, etc...
        public static readonly DependencyPropertyKey IsZoomActivePropertyKey =
            DependencyProperty.RegisterReadOnly("IsZoomActive", typeof(bool), typeof(DiagramBase<Tx, Ty>), new UIPropertyMetadata(false));

        public static readonly DependencyProperty IsZoomActiveProperty = IsZoomActivePropertyKey.DependencyProperty;


        public bool IsMehrfachAuswahlActive
        {
            get { return (bool)GetValue(IsMehrfachAuswahlActiveProperty); }
            private set { SetValue(IsMehrfachAuswahlActivePropertyKey, value); }
        }

        // Using a DependencyProperty as the backing store for LineBrush.  This enables animation, styling, binding, etc...
        public static readonly DependencyPropertyKey IsMehrfachAuswahlActivePropertyKey =
            DependencyProperty.RegisterReadOnly("IsMehrfachAuswahlActive", typeof(bool), typeof(DiagramBase<Tx, Ty>), new UIPropertyMetadata(false));

        public static readonly DependencyProperty IsMehrfachAuswahlActiveProperty = IsMehrfachAuswahlActivePropertyKey.DependencyProperty;

        #endregion

        #region Events & Delegates
        public event PropertyChangedEventHandler PropertyChanged;
        public event EventHandler YAxisZoomCompleted;
        public event EventHandler BeginObjectMove;
        public event EventHandler EndObjectMove;

        #endregion

        #region Routed Events

        public event RoutedEventHandler SelectionChanged
        {
            add { AddHandler(SelectionChangedEvent, value); }
            remove { RemoveHandler(SelectionChangedEvent, value); }
        }

        public static readonly RoutedEvent SelectionChangedEvent = EventManager.RegisterRoutedEvent(
            "SelectionChanged", RoutingStrategy.Bubble, typeof(SelectionChangedEventHandler), typeof(DiagramBase<Tx, Ty>));
        #endregion

        #region Attributes

        protected delegate void NoParas();

        private Grid _diagram_base_grid = null;
        private Grid _base_grid = null;
        private Grid _loading_grid = null;

        private GridLineCanvas _grid_lines = null;
        private Canvas _background_elements_canvas = null;
        private Grid _header_container = null;

        private Canvas _elements_canvas = null;
        private DiagramConfig<Tx, Ty> _diagram_config = null;

        private Canvas _background_scrolling = null;
        private Canvas _element_scrolling = null;

        private System.Timers.Timer _zoom_finished_timer = null;
        private double _root_zoom_x = 1;
        private double _root_zoom_y = 1;

        private bool _ignore_zoom_changes = false;
        private bool _ignore_scroll_changes = false;
        private Cursor _pre_scroll_cursor = null;

        private Canvas _cursor_information_canvas = null;
        private Line _cursor_vertical_line = null;
        private Line _cursor_horizontal_line = null;
        private TextBlock _curor_information_tb = null;

        private Canvas _mode_menu_canvas = null;
        private Grid _mode_menu_grid = null;
        private bool _is_mode_menu_visible = false;
        private Storyboard _modemenu_inactive_sb = null;
        private Storyboard _modemenu_active_sb = null;

        private bool _is_multi_selecting = false;
        private int? _multi_select_mode_stylus = null;
        private Rectangle _selection_rectangle = null;
        private bool _touch_multiselected = false;

        private ToolButtonContent _toolbutton_content = null;
        private ResetZoomButtonContent _reset_zoom_button_content = null;
        private MehrfachAuswahlButtonContent _mehrfachauswahl_button_content = null;

        private Canvas _right_click_information_canvas = null;

        private TouchButton _mode_menu_button = null;

        private bool _resetting_dependency_property = false;
        private bool _force_cursor_visibility = false;
        private CursorMarkerVisibility _old_marker_visibility = CursorMarkerVisibility.None;

        //private Dictionary<int, Point> _last_mouse_pos = new Dictionary<int,Point>();
        //private Dictionary<int, double> _x_offset = new Dictionary<int,double>();
        //private Dictionary<int,double> _y_offset = new Dictionary<int,double>();

        private bool _is_dragging_object = false;
        //private Dictionary<PointObject<Tx,Ty>, bool> _object_mouse_down = new Dictionary<PointObject<Tx,Ty>,bool>();
        private Dictionary<int, PointObject<Tx, Ty>> _dragactive_objects_per_stylus = new Dictionary<int, PointObject<Tx, Ty>>();

        private bool _is_resizing_object = false;
        private ObjectSelectionMarker _sizing_marker_object = null;
        private PointObject<Tx, Ty> _sizing_object = null;
        private ObjectSelectionMarker.ResizingMode _resizing_mode = ObjectSelectionMarker.ResizingMode.None;
        private Cursor _resizing_cursor = null;

        private UiRedirectionMetaInfo _active_ui_redirection = null;

        private Brush _element_canvas_background = null;

        private Border _legend_border = null;

        private bool _is_scrolling = false;
        private double _scrolling_start_x = double.MinValue;
        private double _scrolling_start_y = double.MinValue;

        private Point _last_scroll_pos = new Point(0, 0);
        private Point _last_scroll_pos_backgrund_element = new Point(0, 0);

        private Cursor _old_move_cursor = null;
        private Cursor _old_resize_cursor = null;
        private Cursor _old_object_resize_cursor = null;

        //private List<ObjectSelectionMarker> _child_markers = new List<ObjectSelectionMarker>();
        private ObservableCollection<PointObject<Tx, Ty>> _chart_data_objects = new ObservableCollection<PointObject<Tx, Ty>>();
        private ObservableCollection<PointObject<Tx, Ty>> _chart_background_objects = new ObservableCollection<PointObject<Tx, Ty>>();

        private List<PointObject<Tx, Ty>> _selected_chart_data_objects = new List<PointObject<Tx, Ty>>();

        private TypedScaleControl<Tx> _x_axis_control = null;
        private TypedScaleControl<Ty> _y_axis_control = null;

        private QueuedBackgroundWorker _background_loader = null;
        private object _loading_argument = null;
        private bool _abort_background_loading = false;

        private DateTime _last_apply_filter = DateTime.MinValue;

        private Dictionary<int, FrameworkElement> _active_stylus_ids = new Dictionary<int, FrameworkElement>();
        private DiagramScaleTouchInertiaProcessManipulation _scale_touch_intertia_manipulator = null;

        private bool _disposed = false;
        private bool _disposing = false;

        private bool _has_loaded_data = false;

        private Path _right_click_feedback_circle = null;
        private EllipseGeometry _right_click_color_animation_path = null;

        private DropShadowEffect _drop_shadow_right_click_animation = null;
        private DropShadowEffect _drop_shadow_right_click_animation_finished = null;

        private double _actual_scroll_height = 0;

        private bool _ignore_actual_size_changed;
        private bool _reset_ignore_actual_size_changed;
        private bool _caputre_stylus_up_of_background = false;

        #endregion


    }
}
