﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using Logicx.WpfUtility.WindowManagement.Adorners;
using System.Windows.Data;
using Logicx.WpfUtility.WpfHelpers;
using Logicx.WpfUtility.Converters;

namespace Logicx.WpfUtility.WindowManagement
{
    public interface ISubMenuPanelMenuItem
    {
        ObservableCollection<SubMenuPanelMenuItem> Children { get; }
        SubMenuPanelMenuItem ParentItem { get; }
    }

    public abstract class SubMenuPanelMenuItem : Viewbox, IComparable<SubMenuPanelMenuItem>, IDisposable, ISubMenuPanelMenuItem
    {
        public enum SubMenuItemType
        {
            Button,
            ToggleButton,
            Custom
        }

        public enum SubMenuItemVisualMode
        {
            Control,
            Image
        }

        public class MenuItemActionEventArgs : EventArgs
        {
            public MenuItemActionEventArgs(MenuItemAction action)
            {
                ClickedAction = action;
            }

            public MenuItemAction ClickedAction;
        }

        public class MenuItemManagementEventArgs : EventArgs
        {
            public MenuItemManagementEventArgs(SubMenuPanelMenuItem changed_item)
            {
                ChangedItem = changed_item;
            }

            public SubMenuPanelMenuItem ChangedItem;
        }

        public class MenuItemAction : INotifyPropertyChanged
        {
            #region Construction and Initialization
            /// <summary>
            /// Constructor of the class
            /// </summary>
            /// <param name="action_name">Name of the action</param>
            public MenuItemAction(string action_name)
            {
                _action_name = action_name;
            }
            /// <summary>
            /// Constructor of the class
            /// </summary>
            /// <param name="action_name">Name of the action</param>
            /// <param name="action_menu_icon">Icon of the action for Menus</param>
            /// <param name="action_grid_icon">Icon of the action for Grids</param>
            public MenuItemAction(string action_name, FrameworkElement action_menu_icon, FrameworkElement action_grid_icon)
            {
                _action_name = action_name;
                _action_menu_icon = action_menu_icon;
                _action_grid_icon = action_grid_icon;
            }
            #endregion

            #region Operations
            /// <summary>
            /// Raise property changed event
            /// </summary>
            /// <param name="property_name"></param>
            protected void SendPropertyChanged(string property_name)
            {
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs(property_name));
            }
            #endregion

            #region Properties
            /// <summary>
            /// Gets/Sets the name of the action
            /// </summary>
            public string Name
            {
                get
                {
                    return _action_name;
                }
                set
                {
                    if (_action_name != value)
                    {
                        _action_name = value;
                        SendPropertyChanged("Name");
                    }
                }
            }
            /// <summary>
            /// Gets/Sets the icon of the action
            /// </summary>
            public FrameworkElement IconMenu
            {
                get
                {
                    return _action_menu_icon;
                }
                set
                {
                    if (_action_menu_icon != value)
                    {
                        _action_menu_icon = value;
                        SendPropertyChanged("IconMenu");
                    }
                }
            }

            /// <summary>
            /// Gets/Sets the icon of the action
            /// </summary>
            public FrameworkElement IconGrid
            {
                get
                {
                    return _action_grid_icon;
                }
                set
                {
                    if (_action_grid_icon != value)
                    {
                        _action_grid_icon = value;
                        SendPropertyChanged("IconGrid");
                    }
                }
            }

            /// <summary>
            /// Gets/Sets the visibility flag
            /// </summary>
            public bool IsVisible
            {
                get
                {
                    return _is_visible;
                }
                set
                {
                    if (_is_visible != value)
                    {
                        _is_visible = value;
                        SendPropertyChanged("IsVisible");
                    }
                }
            }

            /// <summary>
            /// Gets/Sets the enabled flag
            /// </summary>
            public bool IsEnabled
            {
                get
                {
                    return _is_enabled;
                }
                set
                {
                    if (_is_enabled != value)
                    {
                        _is_enabled = value;
                        SendPropertyChanged("IsEnabled");
                    }
                }
            }
            #endregion

            #region Events
            public event PropertyChangedEventHandler PropertyChanged;
            #endregion


            #region Attributes
            private string _action_name = "";
            private FrameworkElement _action_menu_icon = null;
            private FrameworkElement _action_grid_icon = null;
            private bool _is_visible = true;
            private bool _is_enabled = true;
            #endregion
        }

        public enum MenuItemAttributeType
        {
            Label,
            LinkButton,
            Button,
            ToggleButton,
            TextBox,
            CheckBox,
            Image,
            Icon
        }

        public class MenuItemAttribute : INotifyPropertyChanged
        {
            #region Construction and Initialization
            /// <summary>
            /// Constructor of the class
            /// </summary>
            /// <param name="attribute_name">Name of the attribute</param>
            public MenuItemAttribute(string attribute_name)
            {
                _attribute_name = attribute_name;
                ShowHeader = true;
            }

            /// <summary>
            /// Constructor of the class
            /// </summary>
            /// <param name="attribute_name">Name of the attribute</param>
            /// <param name="attribute_type">Type of the attribute column</param>
            public MenuItemAttribute(string attribute_name, MenuItemAttributeType attribute_type)
            {
                _attribute_name = attribute_name;
                _attribute_type = attribute_type;
                ShowHeader = true;
            }

            /// <summary>
            /// Constructor of the class
            /// </summary>
            /// <param name="attribute_name">Name of the attribute</param>
            /// <param name="attribute_type">Type of the attribute column</param>
            /// <param name="column_alignment">Column data alignment</param>
            public MenuItemAttribute(string attribute_name, MenuItemAttributeType attribute_type, HorizontalAlignment column_alignment)
            {
                _attribute_name = attribute_name;
                _attribute_type = attribute_type;
                _item_alignment = column_alignment;
                ShowHeader = true;
            }

            /// <summary>
            /// Constructor of the class
            /// </summary>
            /// <param name="attribute_name">Name of the attribute</param>
            /// <param name="attribute_type">Type of the attribute column</param>
            /// <param name="value_binding">Value binding (null if GetValue() should be called)</param>
            public MenuItemAttribute(string attribute_name, MenuItemAttributeType attribute_type, Binding value_binding)
            {
                _attribute_name = attribute_name;
                _attribute_type = attribute_type;
                _value_binding = value_binding;
                ShowHeader = true;
            }

            /// <summary>
            /// Constructor of the class
            /// </summary>
            /// <param name="attribute_name">Name of the attribute</param>
            /// <param name="attribute_type">Type of the attribute column</param>
            /// <param name="column_alignment">Column data alignment</param>
            /// <param name="value_binding">Value binding (null if GetValue() should be called)</param>
            public MenuItemAttribute(string attribute_name, MenuItemAttributeType attribute_type, HorizontalAlignment column_alignment, Binding value_binding)
            {
                _attribute_name = attribute_name;
                _attribute_type = attribute_type;
                _item_alignment = column_alignment;
                _value_binding = value_binding;
                ShowHeader = true;
            }

            /// <summary>
            /// Constructor of the class
            /// </summary>
            /// <param name="attribute_name">Name of the attribute</param>
            /// <param name="attribute_type">Type of the attribute column</param>
            /// <param name="column_alignment">Column data alignment</param>
            /// <param name="value_binding">Value binding (null if GetValue() should be called)</param>
            /// <param name="raise_click_action">Action which should be raised if clicked a button attribute column</param>
            public MenuItemAttribute(string attribute_name, MenuItemAttributeType attribute_type, HorizontalAlignment column_alignment, Binding value_binding, MenuItemAction raise_click_action)
            {
                _attribute_name = attribute_name;
                _attribute_type = attribute_type;
                _item_alignment = column_alignment;
                _value_binding = value_binding;
                _raise_click_action = raise_click_action;
                ShowHeader = true;
            }
            #endregion

            #region Operations
            /// <summary>
            /// Raise property changed event
            /// </summary>
            /// <param name="property_name"></param>
            protected void SendPropertyChanged(string property_name)
            {
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs(property_name));
            }
            #endregion

            #region Properties

            public double MinWidth
            {
                get { return _min_width; }
                set { _min_width = value; }
            }

            public double MaxWidth
            {
                get { return _max_width; }
                set { _max_width = value; }
            }

            public double Width
            {
                get { return _width; }
                set { _width = value; }
            }
            /// <summary>
            /// Gets/Sets the name of the attribute
            /// </summary>
            public string Name
            {
                get
                {
                    return _attribute_name;
                }
                set
                {
                    if (_attribute_name != value)
                    {
                        _attribute_name = value;
                        SendPropertyChanged("Name");
                    }
                }
            }

            /// <summary>
            /// Gets/Sets the type of the attribute
            /// </summary>
            public MenuItemAttributeType Type
            {
                get
                {
                    return _attribute_type;
                }
                set
                {
                    if (_attribute_type != value)
                    {
                        _attribute_type = value;
                        SendPropertyChanged("Type");
                    }
                }
            }

            /// <summary>
            /// Gets/Sets the alignment of the attribute
            /// </summary>
            public HorizontalAlignment HorizontalAlignment
            {
                get
                {
                    return _item_alignment;
                }
                set
                {
                    if (_item_alignment != value)
                    {
                        _item_alignment = value;
                        SendPropertyChanged("HorizontalAlignment");
                    }
                }
            }

            /// <summary>
            /// Gets/Sets the value binding of the attribute
            /// </summary>
            public Binding ValueBinding
            {
                get
                {
                    return _value_binding;
                }
                set
                {
                    if (_value_binding != value)
                    {
                        _value_binding = value;
                        SendPropertyChanged("ValueBinding");
                    }
                }
            }

            /// <summary>
            /// Gets/Sets the action which will be raised if a button column gets clicked
            /// </summary>
            public MenuItemAction RaiseClickAction
            {
                get
                {
                    return _raise_click_action;
                }
                set
                {
                    if (_raise_click_action != value)
                    {
                        _raise_click_action = value;
                        SendPropertyChanged("RaiseClickAction");
                    }
                }
            }

            /// <summary>
            /// Gets/Sets the visibility flag
            /// </summary>
            public bool IsVisible
            {
                get
                {
                    return _is_visible;
                }
                set
                {
                    if (_is_visible != value)
                    {
                        _is_visible = value;
                        SendPropertyChanged("IsVisible");
                    }
                }
            }

            /// <summary>
            /// Gets/Sets the enabled flag
            /// </summary>
            public bool IsEnabled
            {
                get
                {
                    return _is_enabled;
                }
                set
                {
                    if (_is_enabled != value)
                    {
                        _is_enabled = value;
                        SendPropertyChanged("IsEnabled");
                    }
                }
            }

            public bool ShowHeader
            {
                get;
                set;
            }
            #endregion

            #region Dependency Properties
            #endregion

            #region Events
            public event PropertyChangedEventHandler PropertyChanged;
            #endregion

            #region Routed Events
            #endregion

            #region Attributes

            private string _attribute_name = "";
            private MenuItemAttributeType _attribute_type = MenuItemAttributeType.Label;
            private HorizontalAlignment _item_alignment = HorizontalAlignment.Left;
            private Binding _value_binding = null;
            private MenuItemAction _raise_click_action = null;
            private bool _is_visible = true;
            private bool _is_enabled = true;
            private double _min_width = double.NaN;
            private double _max_width = double.NaN;
            private double _width = double.NaN;
            #endregion
        }

        public SubMenuPanelMenuItem()
        {
            this.SizeChanged += new SizeChangedEventHandler(SubMenuPanelMenuItem_SizeChanged);
            this.Loaded += new RoutedEventHandler(SubMenuPanelMenuItem_Loaded);
            this.Unloaded += new RoutedEventHandler(SubMenuPanelMenuItem_Unloaded);
            this.MouseLeave += new MouseEventHandler(SubMenuPanelMenuItem_MouseLeave);
            this.KeyDown += new KeyEventHandler(SubMenuPanelMenuItem_KeyDown);

            _children.CollectionChanged += new System.Collections.Specialized.NotifyCollectionChangedEventHandler(_children_CollectionChanged);

            this.Focusable = true;
            FocusManager.SetIsFocusScope(this, true);
            _default_item_height = this.Height;
            _default_item_width = this.Width;

            // Diese Zeile darf nicht gelöscht werden, da sich die automatisch Bilderneuerung an den gebundenen Datacontext hängt.
            // Wenn dieser nicht gesetzt wird, wird möglicherweise der DataContext vom Parent genommen und es kommt zu einem Fehlverhalten des Updateprozesses des Items
            this.DataContext = null;
        }

        internal void ResetChildStore()
        {
            _old_child = null;
            this.Loaded += new RoutedEventHandler(SubMenuPanelMenuItem_Loaded);
        }

        internal void InitChildStore()
        {
            if (_old_child == null)
                _old_child = Child;
        }

        void _children_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            // TODO: children collection changed
            HasChildren = _children.Count > 0;
            if (e.NewItems != null)
            {
                foreach (SubMenuPanelMenuItem new_item in e.NewItems)
                    new_item.ParentItem = this;
            }
        }

        public SubMenuPanelMenuItem ParentItem
        {
            get;
            private set;
        }

        public bool ForceShowChildren
        {
            get;
            set;
        }

        void SubMenuPanelMenuItem_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Down)
            {
                if (HideActionMenu)
                {
                    HideActionMenu = false;
                    UpdateChild();
                    if (_action_menu != null && _action_menu.Items.Count > 0)
                    {
                        e.Handled = true;
                        return;
                    }
                }
                _temp_focused_element = Keyboard.FocusedElement;
                Keyboard.Focus((MenuItem)_action_menu.Items[0]);
                ((MenuItem)_action_menu.Items[0]).Focus();
                IsHitTestVisible = true;
                e.Handled = true;

            }
            else if (e.Key == System.Windows.Input.Key.Up)
            {
                e.Handled = true;
            }
            else if (e.Key == System.Windows.Input.Key.Right || e.Key == System.Windows.Input.Key.Left)
            {
                if (Keyboard.FocusedElement is MenuItem)
                    e.Handled = true;
                else
                {
                    if (e.Key == System.Windows.Input.Key.Right)
                        SubMenuPanel.GoRight();
                    else
                        SubMenuPanel.GoLeft();
                }
            }
            else if (e.Key == System.Windows.Input.Key.Escape)
            {
                if (_temp_focused_element != null && _temp_focused_element is UIElement)
                    FocusHelper.Focus((UIElement)_temp_focused_element);
                e.Handled = true;
            }

        }

        void SubMenuPanelMenuItem_Unloaded(object sender, RoutedEventArgs e)
        {
            if (_action_menu != null)
            {
                foreach (MenuItem item in _action_menu.Items)
                {
                    ActiveStyleAdornerManager.Instance.DeRegister(item);
                    item.Click -= m_item_Click;
                }


                try
                {
                    BindingOperations.ClearAllBindings(_action_menu);
                }
                catch (Exception ex)
                { }

                _action_menu.Items.Clear();
                _action_menu = null;
            }

            //_old_child = null;
        }

        void SubMenuPanelMenuItem_MouseLeave(object sender, MouseEventArgs e)
        {
            IsHitTestVisible = false;
        }

        void SubMenuPanelMenuItem_Loaded(object sender, RoutedEventArgs e)
        {
            //KeyBinding key_b = new KeyBinding((RoutedUICommand)FindResource("LeftArrowCommand"), System.Windows.Input.Key.Left, ModifierKeys.None);
            //KeyBinding key_r = new KeyBinding((RoutedUICommand)FindResource("RightArrowCommand"), System.Windows.Input.Key.Right, ModifierKeys.None);

            //this.InputBindings.Add(key_b);
            //this.InputBindings.Add(key_r);                    

            this.Loaded -= SubMenuPanelMenuItem_Loaded;

            InitChildStore();
        }

        void SubMenuPanelMenuItem_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            this.SizeChanged -= SubMenuPanelMenuItem_SizeChanged;

            InitChildStore();

            if (_ori_width < 10)
                _ori_width = (int)Math.Round(ActualWidth);
            if (_ori_height < 10)
                _ori_height = (int)Math.Round(ActualHeight);

            //Size diff_size = new Size(Math.Abs(e.NewSize.Width - e.PreviousSize.Width), Math.Abs(e.NewSize.Height - e.PreviousSize.Height));
            //if (diff_size.Height < 1 && diff_size.Width < 1)
            //{
            //    IsImageDirty = false;
            //    return;
            //}

            if (!IsImageDirty)
                IsImageDirty = true;

            RenderImage();
        }

        #region Operations
        protected override void OnGotKeyboardFocus(KeyboardFocusChangedEventArgs e)
        {
            _is_focused = true;
            UpdateChild();
            base.OnGotKeyboardFocus(e);
        }

        protected override void OnLostKeyboardFocus(KeyboardFocusChangedEventArgs e)
        {
            _is_focused = false;
            UpdateChild();
            base.OnLostKeyboardFocus(e);
        }

        protected override void OnGotFocus(RoutedEventArgs e)
        {
            _is_focused = true;
            base.OnGotFocus(e);
        }

        protected override void OnLostFocus(RoutedEventArgs e)
        {
            _is_focused = false;
            base.OnLostFocus(e);
        }

        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            if (e.Property == IsImageDirtyProperty)
            {
                if ((bool)e.NewValue)
                {
                    RenderImage();
                    return;
                }
            }
            else if (e.Property == IsActiveProperty)
            {
                if (!_is_active_trigger)
                {
                    _is_active_trigger = true;
                    IsImageDirty = true;
                }
            }
            else if (e.Property == VisualModeProperty)
            {
                if (_old_child != null)
                {
                    if (((SubMenuItemVisualMode)e.NewValue) == SubMenuItemVisualMode.Image && _rendered_image != null)
                    {
                        RenderImage();
                    }
                    else if (((SubMenuItemVisualMode)e.NewValue) == SubMenuItemVisualMode.Control)
                    {
                        UpdateChild();
                    }

                    CheckRenderSize();
                }

            }
            else if (e.Property == IsSelectedProperty)
            {
                if (_old_child != null)
                    UpdateChild();
            }
            else if (e.Property == ManualIsMouseOverProperty)
            {
                if (_old_child != null)
                    UpdateChild();
            }
            else if (e.Property == HideActionMenuProperty)
            {
                if (_old_child != null)
                    UpdateChild();
            }
            else if (e.Property == IsKeyboardFocusedProperty)
            {
                if (IsKeyboardFocused == true)
                {
                    //IsSelected = true;
                    Console.WriteLine("IsSelected: " + true);
                }
                else
                {    //IsSelected = false;
                }
            }
            else if (e.Property == DataContextProperty)
            {
                if (e.OldValue != null && e.OldValue is INotifyPropertyChanged)
                    DeRegisterNotifyChanged(e.OldValue as INotifyPropertyChanged);

                if (e.NewValue != null && e.NewValue is INotifyPropertyChanged)
                    RegisterNotifyChanged(e.NewValue as INotifyPropertyChanged);
            }

            base.OnPropertyChanged(e);
        }

        /// <summary>
        /// Aus irgendeinem Grund schneidet er bei den Items etwas ab, wenn VisualMode == Control und die Item Size gesetzt wurde
        /// Als Workaround, wird hier die Größe zurückgesetzt. Die Steuerung der Größe erfolgt sowieso über die Viewbox
        /// </summary>
        public void CheckRenderSize()
        {
            if (VisualMode == SubMenuItemVisualMode.Control)
            {
                Width = Double.NaN;
                Height = Double.NaN;
            }
            else if (this.Width == Double.NaN || this.Height == Double.NaN)
            {
                Width = _default_item_width;
                Height = _default_item_height;
            }


        }

        private void DeRegisterNotifyChanged(INotifyPropertyChanged property_changed_object)
        {
            property_changed_object.PropertyChanged -= datacontext_object_PropertyChanged;
        }

        private void RegisterNotifyChanged(INotifyPropertyChanged property_changed_object)
        {
            property_changed_object.PropertyChanged += new PropertyChangedEventHandler(datacontext_object_PropertyChanged);
        }

        private void RenderImage()
        {
            if (VisualMode != SubMenuItemVisualMode.Image)
                return;

            if (!IsImageDirty)
                return;



            int img_width = (int)Math.Round(ActualWidth);
            int img_height = (int)Math.Round(ActualHeight);

            if (_rendered_image != null)
            {
                DeRegisterImageEvents();
                Child = _old_child;

                if (_is_active_trigger)
                {
                    IsActive = !IsActive;
                    IsActive = !IsActive;

                    _is_active_trigger = false;
                }

                _rendered_image = null;
            }

            if (_old_child != null)
            {
                _old_child.UpdateLayout(); // make sure all visual tree changes has been applied
                _old_child.InvalidateVisual();
            }

            if (img_width == 0 || img_height == 0)
                return;

            if (_old_child is FrameworkElement && ((FrameworkElement)_old_child).ActualWidth != 0)
            {
                img_width = (int)((FrameworkElement)_old_child).ActualWidth;
                img_height = (int)((FrameworkElement)_old_child).ActualHeight;
            }


            RenderTargetBitmap rtb = new RenderTargetBitmap(img_width, img_height, 96, 96, PixelFormats.Pbgra32);
            DrawingVisual dv = new DrawingVisual();
            using (DrawingContext dc = dv.RenderOpen())
            {
                VisualBrush vb = new VisualBrush(_old_child == null ? this : _old_child);
                //VisualBrush vb = new VisualBrush(this);
                dc.DrawRectangle(vb, null, new Rect(0, 0, img_width, img_height));

            }

#if DEBUG
            Debug.WriteLine("SubmenuPanel Item wurde neu gerendert");
#endif
            rtb.Render(dv);

            _rendered_image = new Image();
            _rendered_image.Width = img_width;
            _rendered_image.Height = img_height;
            _rendered_image.Source = rtb;
            _rendered_image.IsHitTestVisible = true;

            RegisterImageEvents();

            UpdateChild();

            if (img_width < _ori_width || img_height < _ori_height)
            {
                this.SizeChanged += new SizeChangedEventHandler(SubMenuPanelMenuItem_SizeChanged);
                return;
            }

            IsImageDirty = false;
        }

        private void UpdateChild()
        {
            if (_disable_child_update)
                return;

            if (_old_child == null)
                return;

            EnsureSelectionAndHoverControls();

            Child = null;
            if (VisualMode == SubMenuItemVisualMode.Image)
            {
                if (ManualIsMouseOver || IsSelected)
                {
                    // TODO hin und wieder kann es vorkommen, dass _rendered_image an dieser Stelle null ist. Der Fehler muss noch analysiert werden
                    if (_rendered_image == null)
                    {
                        RenderImage();
                        return;
                    }
                    _selection_hover_container.Children.Add(_rendered_image);
                    Child = _selection_hover_container;
                }
                else
                {
                    //_selection_hover_container.Children.Clear();
                    //_selection_hover_container.Children.Add(_rendered_image);
                    Child = _rendered_image;
                    this.Height = Double.NaN;
                    this.Width = Double.NaN;
                }
            }
            else
            {
                if (ManualIsMouseOver || IsSelected)
                {
                    _selection_hover_container.Children.Add(_old_child);
                    Child = _selection_hover_container;
                }
                else
                {
                    Child = _old_child;
                }

            }
        }

        private void EnsureSelectionAndHoverControls()
        {
            if (_selection_hover_container == null)
            {
                _selection_hover_container = new Grid();
                _selection_hover_container.HorizontalAlignment = HorizontalAlignment.Stretch;
                _selection_hover_container.VerticalAlignment = VerticalAlignment.Stretch;
            }

            if (_selection_hover_container != null)
                _selection_hover_container.Children.Clear();

            if (_default_selection_indicator_style == null)
            {
                _default_selection_indicator_style = new Style(typeof(Border));
                //_default_selection_indicator_style.Setters.Add(new Setter(Border.BorderBrushProperty, new SolidColorBrush(Color.FromArgb(255, 58, 241, 244))));
                //_default_selection_indicator_style.Setters.Add(new Setter(Border.BackgroundProperty, new SolidColorBrush(Color.FromArgb(32, 58, 241, 244))));


                SolidColorBrush bg_brush;
                SolidColorBrush border_brush;
                if (BaseFrame.AdornerActiveRenderBrush is SolidColorBrush)
                {
                    Color c = ((SolidColorBrush)BaseFrame.AdornerActiveRenderBrush).Color;
                    c.A = 32;
                    bg_brush = new SolidColorBrush(c);
                    c.A = 255;
                    border_brush = new SolidColorBrush(c);
                }
                else
                {
                    bg_brush = new SolidColorBrush(Color.FromArgb(32, 58, 241, 244));
                    border_brush = new SolidColorBrush(Color.FromArgb(255, 58, 241, 244));
                }

                _default_selection_indicator_style.Setters.Add(new Setter(Border.BorderBrushProperty, border_brush));
                _default_selection_indicator_style.Setters.Add(new Setter(Border.BackgroundProperty, bg_brush));
                _default_selection_indicator_style.Setters.Add(new Setter(Border.BorderThicknessProperty, new Thickness(_selection_border_thickness)));
                _default_selection_indicator_style.Setters.Add(new Setter(OpacityProperty, (double)1));
                _default_selection_indicator_style.Setters.Add(new Setter(Border.CornerRadiusProperty, new CornerRadius(8)));
                _default_selection_indicator_style.Setters.Add(new Setter(Border.PaddingProperty, new Thickness(0)));
            }

            if (_selection_hover_indicator == null)
            {
                _selection_hover_indicator = new Border();
                _selection_hover_indicator.SetValue(Grid.ZIndexProperty, 1000);
                //FocusManager.SetFocusedElement(this, _selection_hover_indicator);
            }

            if (SelectionBorderStyle != null)
                _selection_hover_indicator.Style = SelectionBorderStyle;
            else
                _selection_hover_indicator.Style = _default_selection_indicator_style;

            if (!_selection_hover_container.Children.Contains(_selection_hover_indicator))
                _selection_hover_container.Children.Add(_selection_hover_indicator);

            if (HideActionMenu)
            {
                _selection_hover_indicator.Child = null;
            }

            if (!HideActionMenu)
            {
                List<MenuItemAction> item_actions = GetActions();

                if (item_actions != null)
                {
                    // dem aktuellen Item die möglichkeit geben
                    // die action state upzudaten
                    UpdateActionState();

                    int actions_to_show = 0;

                    actions_to_show = item_actions.Count(a => a.IsVisible);

                    //if (HideDisabledAction)
                    //{
                    //    foreach (string itemAction in item_actions)
                    //    {
                    //        if (IsActionEnabled(itemAction))
                    //            actions_to_show++;
                    //    }
                    //}
                    //else
                    //    actions_to_show = item_actions.Count;


                    if (item_actions.Count > 0 && actions_to_show > 0)
                    {
                        bool use_current_menu = false;

                        if (_action_menu != null && _action_menu.Items.Count > 0)
                        {
                            List<string> cur_items = new List<string>();

                            foreach (MenuItem item in _action_menu.Items)
                            {
                                if (item.Header is string)
                                {
                                    if (!cur_items.Contains(item.Header as string))
                                        cur_items.Add(item.Header as string);
                                }
                            }

                            List<string> current_visible_items = item_actions.Where(a => a.IsVisible).Select(a => a.Name).ToList();

                            if (current_visible_items.Union(cur_items).Count() == current_visible_items.Count)
                            {
                                use_current_menu = true;
                            }
                        }

                        if (use_current_menu)
                        {
                            _selection_hover_indicator.Child = _action_menu;
                        }
                        else
                        {
                            _action_menu = new Menu();

                            _action_menu.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
                            object o = TryFindResource("action_menu_style");
                            //ActiveStyleAdornerManager.Instance.Register(_action_menu, BaseFrame.AdornerActiveRenderBrush, BaseFrame.AdornerActiveRenderPen);

                            if (o is Style)
                                _action_menu.Style = (Style)o;

                            foreach (MenuItemAction curaction in item_actions)
                            {
                                if (!curaction.IsVisible)
                                    continue;

                                bool is_action_enabled = curaction.IsEnabled; //IsActionEnabled(s);
                                //if (!is_action_enabled && HideDisabledAction)
                                //    continue;

                                MenuItem m_item = new MenuItem();
                                m_item.Template = (ControlTemplate)TryFindResource("action_menuitem_style");
                                //m_item.Background = Brushes.White;
                                //ActiveStyleAdornerManager.Instance.Register(m_item, BaseFrame.AdornerActiveRenderBrush, BaseFrame.AdornerActiveRenderPen);
                                //m_item.Background = BaseFrame.AdornerActiveRenderBrush;
                                m_item.Click += new RoutedEventHandler(m_item_Click);
                                m_item.Header = curaction.Name;
                                m_item.Tag = curaction;

                                FrameworkElement item_icon = curaction.IconMenu; // GetActionIcon(s);
                                if (item_icon != null)
                                    m_item.Icon = item_icon;

                                //if (!is_action_enabled)
                                m_item.IsEnabled = curaction.IsEnabled;

                                _action_menu.Items.Add(m_item);


                            }

                            _action_menu.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;

                            #region MenuBinding
                            Binding b = new Binding();
                            DoubleValueAdderConverter sub_margin_converter = new DoubleValueAdderConverter();
                            b.Converter = sub_margin_converter;
                            b.ConverterParameter = -2 * _selection_border_thickness;
                            b.Source = _selection_hover_container;
                            b.Path = new PropertyPath("ActualHeight");
                            b.Mode = BindingMode.OneWay;
                            _action_menu.SetBinding(Menu.MaxHeightProperty, b);

                            Binding w_b = new Binding();
                            w_b.Converter = new DoubleValueAdderConverter();
                            w_b.ConverterParameter = -2 * _selection_border_thickness;
                            w_b.Source = _selection_hover_container;
                            w_b.Path = new PropertyPath("ActualWidth");
                            w_b.Mode = BindingMode.OneWay;
                            _action_menu.SetBinding(Menu.MaxWidthProperty, w_b);
                            #endregion

                            _selection_hover_indicator.Child = _action_menu;
                        }
                    }
                }
            }
        }




        protected Border GenerateActionItemContainer(Image icon)
        {
            Border border_ret = new Border();
            border_ret.Width = 20;
            border_ret.Height = 20;
            border_ret.Padding = new Thickness(1);
            border_ret.Background = Brushes.Transparent;

            //Viewbox box_icon = new Viewbox();
            //box_icon.Width = 16;
            //box_icon.Height = 16;
            //box_icon.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
            //box_icon.VerticalAlignment = System.Windows.VerticalAlignment.Stretch;
            //box_icon.Stretch = System.Windows.Media.Stretch.Uniform;

            //box_icon.Child = icon;

            if (icon != null)
                icon.Stretch = System.Windows.Media.Stretch.Uniform;

            border_ret.Child = icon;

            return border_ret;
        }


        void m_item_Click(object sender, RoutedEventArgs e)
        {
            MenuItem m_item = sender as MenuItem;
            if (m_item != null)
            {
                if (ActionClicked != null)
                    ActionClicked(this, new MenuItemActionEventArgs(m_item.Tag as MenuItemAction));
            }

        }

        private void DeRegisterImageEvents()
        {
            _rendered_image.MouseDown -= _rendered_image_MouseDown;
            _rendered_image.MouseEnter -= _rendered_image_MouseEnter;
            _rendered_image.MouseLeave -= _rendered_image_MouseLeave;
            _rendered_image.MouseMove -= _rendered_image_MouseMove;
            _rendered_image.MouseUp -= _rendered_image_MouseUp;
            _rendered_image.MouseWheel -= _rendered_image_MouseWheel;
        }

        private void RegisterImageEvents()
        {
            if (_rendered_image == null)
                return;

            _rendered_image.MouseDown += new System.Windows.Input.MouseButtonEventHandler(_rendered_image_MouseDown);
            _rendered_image.MouseEnter += new System.Windows.Input.MouseEventHandler(_rendered_image_MouseEnter);
            _rendered_image.MouseLeave += new System.Windows.Input.MouseEventHandler(_rendered_image_MouseLeave);
            _rendered_image.MouseMove += new System.Windows.Input.MouseEventHandler(_rendered_image_MouseMove);
            _rendered_image.MouseUp += new System.Windows.Input.MouseButtonEventHandler(_rendered_image_MouseUp);
            _rendered_image.MouseWheel += new System.Windows.Input.MouseWheelEventHandler(_rendered_image_MouseWheel);
        }

        void datacontext_object_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (Dispatcher.CheckAccess())
            {
                if (VisualMode == SubMenuItemVisualMode.Image)
                    IsImageDirty = true;

                TriggerGridUpdate = !TriggerGridUpdate;
            }
            else
            {
                Dispatcher.Invoke(DispatcherPriority.Normal, (NoParas)delegate
                {
                    if (VisualMode == SubMenuItemVisualMode.Image)
                        IsImageDirty = true;

                    TriggerGridUpdate = !TriggerGridUpdate;
                })
                ;
            }
        }

        void _rendered_image_MouseWheel(object sender, System.Windows.Input.MouseWheelEventArgs e)
        {
            if (_old_child != null && VisualMode == SubMenuItemVisualMode.Image)
            {
                if (ItemTyp == SubMenuItemType.Custom)
                {
                    System.Windows.Input.MouseWheelEventArgs newArgs =
                        new System.Windows.Input.MouseWheelEventArgs(e.MouseDevice, e.Timestamp, e.Delta);
                    newArgs.RoutedEvent = MouseWheelEvent;
                    _old_child.RaiseEvent(newArgs);
                }
            }
        }

        void _rendered_image_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (_old_child != null && VisualMode == SubMenuItemVisualMode.Image)
            {
                if (ItemTyp == SubMenuItemType.Custom)
                {
                    System.Windows.Input.MouseButtonEventArgs newArgs =
                    new System.Windows.Input.MouseButtonEventArgs(e.MouseDevice, e.Timestamp, e.ChangedButton);
                    newArgs.RoutedEvent = MouseUpEvent;
                    _old_child.RaiseEvent(newArgs);
                }
                else
                {
                    if (_l_mouse_down)
                    {
                        if (ItemTyp == SubMenuItemType.Button && _old_child is Button)
                        {
                            RoutedEventArgs click_args = new RoutedEventArgs(Button.ClickEvent, _old_child);
                            ((Button)_old_child).RaiseEvent(click_args);
                        }
                        else if (ItemTyp == SubMenuItemType.ToggleButton && _old_child is ToggleButton)
                        {
                            bool curValue = ((ToggleButton)_old_child).IsChecked != null ? ((ToggleButton)_old_child).IsChecked.Value : false;
                            ((ToggleButton)_old_child).IsChecked = !curValue;
                        }
                    }

                    if (e.LeftButton != MouseButtonState.Pressed)
                        _l_mouse_down = false;
                    if (e.RightButton != MouseButtonState.Pressed)
                        _r_mouse_down = false;
                }
            }
        }

        void _rendered_image_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (_old_child != null && VisualMode == SubMenuItemVisualMode.Image)
            {
                if (ItemTyp == SubMenuItemType.Custom)
                {
                    System.Windows.Input.MouseButtonEventArgs newArgs =
                    new System.Windows.Input.MouseButtonEventArgs(e.MouseDevice, e.Timestamp, e.ChangedButton);
                    newArgs.RoutedEvent = MouseDownEvent;
                    _old_child.RaiseEvent(newArgs);
                }
                else
                {
                    if (e.LeftButton == MouseButtonState.Pressed)
                        _l_mouse_down = true;
                    if (e.RightButton == MouseButtonState.Pressed)
                        _r_mouse_down = true;
                }

            }
        }

        void _rendered_image_MouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        {
            if (_old_child != null && VisualMode == SubMenuItemVisualMode.Image)
            {
                if (ItemTyp == SubMenuItemType.Custom)
                {
                    System.Windows.Input.MouseEventArgs newArgs =
                    new System.Windows.Input.MouseEventArgs(e.MouseDevice, e.Timestamp, e.StylusDevice);
                    newArgs.RoutedEvent = MouseMoveEvent;
                    _old_child.RaiseEvent(newArgs);
                }
            }
        }

        void _rendered_image_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            if (_old_child != null && VisualMode == SubMenuItemVisualMode.Image)
            {
                if (ItemTyp == SubMenuItemType.Custom)
                {
                    System.Windows.Input.MouseEventArgs newArgs =
                    new System.Windows.Input.MouseEventArgs(e.MouseDevice, e.Timestamp, e.StylusDevice);
                    newArgs.RoutedEvent = MouseLeaveEvent;
                    _old_child.RaiseEvent(newArgs);
                }
                else
                {
                    this.Cursor = _old_cursor;

                    if (e.LeftButton != MouseButtonState.Pressed)
                        _l_mouse_down = false;
                    if (e.RightButton != MouseButtonState.Pressed)
                        _r_mouse_down = false;
                }
            }

            this.IsHitTestVisible = false;
        }

        void _rendered_image_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            if (_old_child != null && VisualMode == SubMenuItemVisualMode.Image)
            {
                if (ItemTyp == SubMenuItemType.Custom)
                {
                    System.Windows.Input.MouseEventArgs newArgs =
                    new System.Windows.Input.MouseEventArgs(e.MouseDevice, e.Timestamp, e.StylusDevice);
                    newArgs.RoutedEvent = MouseEnterEvent;
                    _old_child.RaiseEvent(newArgs);
                }
                else
                {
                    _old_cursor = this.Cursor;
                    this.Cursor = Cursors.Hand;

                    if (e.LeftButton != MouseButtonState.Pressed)
                        _l_mouse_down = false;
                    if (e.RightButton != MouseButtonState.Pressed)
                        _r_mouse_down = false;
                }
            }
        }


        #endregion

        #region abstrakte/virtuelle Methoden
        public abstract List<MenuItemAction> GetActions();
        public abstract List<MenuItemAttribute> GetAttributes();
        //public abstract bool IsActionEnabled(string action);
        public abstract void UpdateActionState();
        public abstract void UpdateAttributeState();
        public abstract object GetValue(MenuItemAttribute attribute);
        //public abstract FrameworkElement GetActionIcon(string action);

        public abstract List<string> GetCustomFilters();
        public abstract bool ApplyCustomFilter(string filter_name);
        #endregion

        public MenuItemAction GetActionByName(string action_name)
        {
            List<MenuItemAction> actions = GetActions();

            if (actions != null && actions.Count > 0)
                return actions.SingleOrDefault(a => a.Name == action_name);

            return null;
        }

        public MenuItemAttribute GetAttributeByName(string action_name)
        {
            List<MenuItemAttribute> attributes = GetAttributes();

            if (attributes != null && attributes.Count > 0)
                return attributes.SingleOrDefault(a => a.Name == action_name);

            return null;
        }

        public virtual void OnMenuItemInitialized(object argument)
        {
        }

        public virtual void OnMenuItemRemoving()
        {
        }

        #region IDisposable Members

        public void Dispose()
        {
            if (_disposed)
                return;

            DisposeInternal(true);

            _disposed = true;
        }

        private void DisposeInternal(bool disposing)
        {
            if (_disposing)
                return;

            _disposing = true;

            this.SizeChanged -= SubMenuPanelMenuItem_SizeChanged;
            this.Loaded -= SubMenuPanelMenuItem_Loaded;
            this.Unloaded -= SubMenuPanelMenuItem_Unloaded;
            this.MouseLeave -= SubMenuPanelMenuItem_MouseLeave;
            this.KeyDown -= SubMenuPanelMenuItem_KeyDown;

            _children.CollectionChanged -= _children_CollectionChanged;
            foreach (SubMenuPanelMenuItem child in _children)
            {
                child.Dispose();
            }

            Dispose(disposing);
        }

        protected virtual void Dispose(bool disposing)
        {

        }

        #endregion

        #region abstrakte Properties
        public abstract SubMenuItemType ItemTyp
        {
            get;
        }

        public abstract MenuItemAction DefaultAction
        {
            get;
        }
        #endregion

        #region Properties

        public ISubMenuPanelBase SubMenuPanel { set; get; }

        public bool Disposed
        {
            get { return _disposed; }
        }
        public IBaseFrameBase BaseFrame
        {
            set;
            get;
        }

        public bool HideDisabledAction
        {
            set;
            get;
        }

        public bool IsFocusedElementFocused
        {
            get
            {
                return _is_focused;
            }
        }

        public ObservableCollection<SubMenuPanelMenuItem> Children
        {
            get { return _children; }
        }
        public string Key = "";

        #endregion

        #region IComparable
        public virtual int CompareTo(SubMenuPanelMenuItem other)
        {
            return ToString().CompareTo(other.ToString());
        }
        #endregion

        #region Dependency Properties
        public bool IsActive
        {
            get { return (bool)GetValue(IsActiveProperty); }
            set { SetValue(IsActiveProperty, value); }
        }
        public static readonly DependencyProperty IsActiveProperty = DependencyProperty.Register("IsActive", typeof(bool), typeof(SubMenuPanelMenuItem), new UIPropertyMetadata(false));

        public bool IsSelected
        {
            get { return (bool)GetValue(IsSelectedProperty); }
            set { SetValue(IsSelectedProperty, value); }
        }
        public static readonly DependencyProperty IsSelectedProperty = DependencyProperty.Register("IsSelected", typeof(bool), typeof(SubMenuPanelMenuItem), new UIPropertyMetadata(false));

        public SubMenuItemVisualMode VisualMode
        {
            get { return (SubMenuItemVisualMode)GetValue(VisualModeProperty); }
            set { SetValue(VisualModeProperty, value); }
        }
        public static readonly DependencyProperty VisualModeProperty = DependencyProperty.Register("VisualMode", typeof(SubMenuItemVisualMode), typeof(SubMenuPanelMenuItem), new UIPropertyMetadata(SubMenuItemVisualMode.Control));

        public bool IsImageDirty
        {
            get { return (bool)GetValue(IsImageDirtyProperty); }
            set { SetValue(IsImageDirtyProperty, value); }
        }
        public static readonly DependencyProperty IsImageDirtyProperty = DependencyProperty.Register("IsImageDirty", typeof(bool), typeof(SubMenuPanelMenuItem), new UIPropertyMetadata(true));

        public bool ManualIsMouseOver
        {
            get { return (bool)GetValue(ManualIsMouseOverProperty); }
            set { SetValue(ManualIsMouseOverProperty, value); }
        }
        public static readonly DependencyProperty ManualIsMouseOverProperty = DependencyProperty.Register("ManualIsMouseOver", typeof(bool), typeof(SubMenuPanelMenuItem), new UIPropertyMetadata(false));

        public bool HideActionMenu
        {
            get { return (bool)GetValue(HideActionMenuProperty); }
            set { SetValue(HideActionMenuProperty, value); }
        }
        public static readonly DependencyProperty HideActionMenuProperty = DependencyProperty.Register("HideActionMenu", typeof(bool), typeof(SubMenuPanelMenuItem), new UIPropertyMetadata(false));

        public Style SelectionBorderStyle
        {
            get { return (Style)GetValue(SelectionBorderStyleProperty); }
            set { SetValue(SelectionBorderStyleProperty, value); }
        }
        public static readonly DependencyProperty SelectionBorderStyleProperty = DependencyProperty.Register("SelectionBorderStyle", typeof(Style), typeof(SubMenuPanelMenuItem), new UIPropertyMetadata(null));

        public bool TriggerGridUpdate
        {
            get { return (bool)GetValue(TriggerGridUpdateProperty); }
            set { SetValue(TriggerGridUpdateProperty, value); }
        }
        public static readonly DependencyProperty TriggerGridUpdateProperty = DependencyProperty.Register("TriggerGridUpdate", typeof(bool), typeof(SubMenuPanelMenuItem), new UIPropertyMetadata(false));


        public bool HasChildren
        {
            get { return (bool)GetValue(HasChildrenProperty); }
            set { SetValue(HasChildrenProperty, value); }
        }
        public static readonly DependencyProperty HasChildrenProperty = DependencyProperty.Register("HasChildren", typeof(bool), typeof(SubMenuPanelMenuItem), new UIPropertyMetadata(false));
        #endregion

        #region Attribs

        internal bool _disable_child_update = false;

        private bool _disposed = false;
        private bool _disposing = false;

        private ObservableCollection<SubMenuPanelMenuItem> _children = new ObservableCollection<SubMenuPanelMenuItem>();

        protected delegate void NoParas();
        public event EventHandler<MenuItemActionEventArgs> ActionClicked;

        private Image _rendered_image = null;
        private UIElement _old_child = null;
        private bool _is_active_trigger = false;

        private int _ori_width = 0;
        private int _ori_height = 0;

        private bool _l_mouse_down = false;
        private bool _r_mouse_down = false;
        private Cursor _old_cursor = null;

        #region selection, menus
        private Grid _selection_hover_container = null;
        private Border _selection_hover_indicator = null;
        private Style _default_selection_indicator_style = null;
        private double _selection_border_thickness = 2.0;

        private Menu _action_menu = null;
        #endregion

        private double _default_item_width = Double.NaN;
        private double _default_item_height = Double.NaN;

        private bool _is_focused;
        private object _temp_focused_element;

        #endregion


    }
}
