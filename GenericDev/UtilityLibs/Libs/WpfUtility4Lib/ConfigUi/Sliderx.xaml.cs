﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using Logicx.WpfUtility.WpfHelpers;

namespace Logicx.WpfUtility4.ConfigUi
{
    /// <summary>
    /// Interaction logic for Slider.xaml
    /// </summary>
    public partial class Sliderx : UserControl
    {
        public Sliderx()
        {
            InitializeComponent();
            Loaded += new RoutedEventHandler(Sliderx_Loaded);
        }

        void Sliderx_Loaded(object sender, RoutedEventArgs e)
        {
            UpdateTrack(Value);
        }

        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            if(e.Property.Name == "Bezeichnung")
            {
                Bezeichnung = e.NewValue.ToString().ToUpper();
            }
            else if (e.Property.Name == "Value")
            {
                UpdateTrack((double)e.NewValue);
            }

            base.OnPropertyChanged(e);
        }

        private void UpdateTrack(double value)
        {
            double w = back_rect.ActualWidth;
            double diff_max = (Maximum - Minimum);
            double curr_diff_value = value - Minimum;
            value_rect.Width = w * curr_diff_value / diff_max;
        }

        #region Dependency Properties
        public string Bezeichnung
        {
            get { return (string)GetValue(BezeichnungProperty); }
            set { SetValue(BezeichnungProperty, value); }
        }
        public static readonly DependencyProperty BezeichnungProperty = DependencyProperty.Register("Bezeichnung", typeof(string), typeof(Sliderx), new UIPropertyMetadata(""));

        public double Value
        {
            get { return (double)GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }
        public static readonly DependencyProperty ValueProperty = DependencyProperty.Register("Value", typeof(double), typeof(Sliderx), new UIPropertyMetadata((double)0));

        public double Maximum
        {
            get { return (double)GetValue(MaximumProperty); }
            set { SetValue(MaximumProperty, value); }
        }
        public static readonly DependencyProperty MaximumProperty = DependencyProperty.Register("Maximum", typeof(double), typeof(Sliderx), new UIPropertyMetadata((double)100));

        public double Minimum
        {
            get { return (double)GetValue(MinimumProperty); }
            set { SetValue(MinimumProperty, value); }
        }
        public static readonly DependencyProperty MinimumProperty = DependencyProperty.Register("Minimum", typeof(double), typeof(Sliderx), new UIPropertyMetadata((double)0));
        #endregion

        private void base_grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.MouseDevice.LeftButton != MouseButtonState.Pressed)
                return;

            e.Handled = true;

            value_rect.Fill = _active_brush;

            SetHitTestAll(this, EventBase, false);

            EventBase.MouseUp += new MouseButtonEventHandler(EventBase_MouseUp);
            EventBase.MouseMove += new MouseEventHandler(EventBase_MouseMove);
            EventBase.MouseEnter += new MouseEventHandler(EventBase_MouseEnter);
            EventBase.MouseLeave += new MouseEventHandler(EventBase_MouseLeave);
        }

        private void SetHitTestAll(FrameworkElement elem, UIElement stopping_elem, bool is_hit_test_visible)
        {
            elem.IsHitTestVisible = is_hit_test_visible;
            if (elem.Parent == null)
                return;
            if (elem.Parent == stopping_elem)
                return;
            if (!(elem.Parent is FrameworkElement))
                return;
            SetHitTestAll((FrameworkElement)elem.Parent, stopping_elem, is_hit_test_visible);
        }

        private void FinishUiUpdate()
        {
            SetHitTestAll(this, EventBase, true);

            EventBase.MouseUp -= new MouseButtonEventHandler(EventBase_MouseUp);
            EventBase.MouseMove -= new MouseEventHandler(EventBase_MouseMove);
            EventBase.MouseLeave -= new MouseEventHandler(EventBase_MouseLeave);
            EventBase.MouseEnter -= new MouseEventHandler(EventBase_MouseEnter);

            value_rect.Fill = _normal_brush;
        }
        
        void EventBase_MouseLeave(object sender, MouseEventArgs e)
        {
        }

        void EventBase_MouseUp(object sender, MouseButtonEventArgs e)
        {
            FinishUiUpdate();
        }


        void EventBase_MouseEnter(object sender, MouseEventArgs e)
        {
            if(e.MouseDevice.LeftButton == MouseButtonState.Released)
                FinishUiUpdate();
        }

        void EventBase_MouseMove(object sender, MouseEventArgs e)
        {
            Point p = e.GetPosition(back_rect);
            if (p.X <= 0)
            {
                Value = Minimum;
            }
            else if (p.X >= back_rect.ActualWidth)
            {
                Value = Maximum;
            }
            else
            {
                Value = Minimum + (p.X * (Maximum - Minimum) / back_rect.ActualWidth);
            }
        }

        private UIElement EventBase
        {
            get
            {
                Window window = UIHelper.TryFindParent<Window>(this);
                if (window != null)
                    return window;

                Page page = UIHelper.TryFindParent<Page>(this);
                if (page != null)
                    return page;

                UIElement parent = Parent as UIElement;
                return parent;
            }
        }

        

        private static readonly Brush _active_brush = new SolidColorBrush(Color.FromRgb(0x00, 0x82, 0xa4));
        private static readonly Brush _normal_brush = Brushes.Black;

    }
}
