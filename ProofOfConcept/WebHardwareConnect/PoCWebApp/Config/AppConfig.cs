﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using PoCWebApp.Data;

namespace PoCWebApp.Config
{
    public class AppConfig
    {
        public static bool AutomaticDbMigrationsEnabled
        {
            get
            {
                bool ret = false;

                try
                {
                    var setting = ConfigurationManager.AppSettings[DataLayerConstants.EnableAutoDbMigrationsConfigKey];
                    bool.TryParse(setting, out ret);

                }
                catch { }

                return ret;
            }
        }
    }
}