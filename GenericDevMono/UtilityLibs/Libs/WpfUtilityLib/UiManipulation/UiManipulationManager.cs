﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace Logicx.WpfUtility.UiManipulation
{
    public enum UiManipulationType
    {
        MouseDrag,
        TouchDrag,
        TouchProcessManipulation,
        TouchInertiaProcessManipulation,
        MouseDragTouchDrag,
        MouseDragTouchProcessManipulation,
        MouseDragTouchInertiaProcessManipulation
    }

    public sealed class UiManipulationManager
    {
        public static UiManipulationManager Instance
        {
            get
            {
                if (_instance == null)
                    lock (_syncronisation)
                        if (_instance == null)
                            _instance = new UiManipulationManager();

                return _instance;
            }
        }

        private UiManipulationManager()
        {
        }

        public void Register(UIElement ui_element, Canvas canvas, UIElement event_base, UiManipulationType ui_manipulation_type, UiManipulator.EnabledAxes enabled_axes)
        {
            lock (_syncronisation)
            {
                UiManipulator ui_manipulator;
                switch (ui_manipulation_type)
                {
                    case UiManipulationType.MouseDrag:
                        ui_manipulator = new MouseDrag(ui_element, canvas, event_base, enabled_axes);
                        RegisterManipulator(ui_manipulator);
                        break;
                    case UiManipulationType.TouchDrag:
                        ui_manipulator = new TouchDrag(ui_element, canvas, event_base, enabled_axes);
                        RegisterManipulator(ui_manipulator);
                        break;
                    case UiManipulationType.TouchProcessManipulation:
                        ui_manipulator = new TouchProcessManipulation(ui_element, canvas, event_base, enabled_axes);
                        RegisterManipulator(ui_manipulator);
                        break;
                    case UiManipulationType.TouchInertiaProcessManipulation:
                        ui_manipulator = new TouchInertiaProcessManipulation(ui_element, canvas, event_base, enabled_axes);
                        RegisterManipulator(ui_manipulator);
                        break;
                    case UiManipulationType.MouseDragTouchDrag:
                        ui_manipulator = new MouseDrag(ui_element, canvas, event_base, enabled_axes);
                        RegisterManipulator(ui_manipulator);
                        ui_manipulator = new TouchDrag(ui_element, canvas, event_base, enabled_axes);
                        RegisterManipulator(ui_manipulator);
                        break;
                    case UiManipulationType.MouseDragTouchProcessManipulation:
                        ui_manipulator = new MouseDrag(ui_element, canvas, event_base, enabled_axes);
                        RegisterManipulator(ui_manipulator);
                        ui_manipulator = new TouchProcessManipulation(ui_element, canvas, event_base, enabled_axes);
                        RegisterManipulator(ui_manipulator);
                        break;
                    case UiManipulationType.MouseDragTouchInertiaProcessManipulation:
                        ui_manipulator = new MouseDrag(ui_element, canvas, event_base, enabled_axes);
                        RegisterManipulator(ui_manipulator);
                        ui_manipulator = new TouchInertiaProcessManipulation(ui_element, canvas, event_base, enabled_axes);
                        RegisterManipulator(ui_manipulator);
                        break;
                    default:
                        throw new Exception("ui manipulation type unknown");
                }
            }
        }

        private void RegisterManipulator(UiManipulator ui_manipulator)
        {
            ui_manipulator.UiManipulationStarted += new EventHandler<UiManipulationEventArgs>(ui_manipulator_UiManipulationStarted);
            ui_manipulator.UiManipulationCompleted += new EventHandler<UiManipulationEventArgs>(ui_manipulator_UiManipulationCompleted);

            if (_registered_manipulators == null)
                _registered_manipulators = new Dictionary<UIElement, List<UiManipulator>>();

            if (!_registered_manipulators.ContainsKey(ui_manipulator.UIElement))
            {
                List<UiManipulator> manipulators = new List<UiManipulator>(2);
                manipulators.Add(ui_manipulator);
                _registered_manipulators.Add(ui_manipulator.UIElement, manipulators);
            }
            else
            {
                _registered_manipulators[ui_manipulator.UIElement].Add(ui_manipulator);
            }
        }

        public void DeRegister(UIElement ui_element)
        {
            lock (_syncronisation)
            {
                if (!_registered_manipulators.ContainsKey(ui_element))
                    return;

                foreach (UiManipulator ui_manipulator in _registered_manipulators[ui_element])
                {
                    ui_manipulator.UiManipulationStarted -= ui_manipulator_UiManipulationStarted;
                    ui_manipulator.UiManipulationCompleted -= ui_manipulator_UiManipulationCompleted;
                    ui_manipulator.Dispose();
                }

                _registered_manipulators.Remove(ui_element);
            }
        }

        #region Event Handler
        void ui_manipulator_UiManipulationCompleted(object sender, UiManipulationEventArgs e)
        {
            lock (_syncronisation)
            {
                _count_active_ui_manipulations--;
            }
        }

        void ui_manipulator_UiManipulationStarted(object sender, UiManipulationEventArgs e)
        {
            lock (_syncronisation)
            {
                _count_active_ui_manipulations++;
            }
        }
        #endregion

        #region Properties
        public bool UiManipulationActive
        {
            get
            {
                lock (_syncronisation)
                {
                    return (_count_active_ui_manipulations > 0);
                }
            }
        }
        #endregion

        #region Attributes
        static volatile UiManipulationManager _instance = null;
        static Object _syncronisation = new Object();
        private Dictionary<UIElement, List<UiManipulator>> _registered_manipulators;
        private int _count_active_ui_manipulations;
        #endregion
    }
}
