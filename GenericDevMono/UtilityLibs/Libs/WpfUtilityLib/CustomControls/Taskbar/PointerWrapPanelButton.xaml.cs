﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Logicx.WpfUtility.WpfHelpers;
using MathLib;

namespace Logicx.WpfUtility.CustomControls.Taskbar
{
    /// <summary>
    /// Interaktionslogik für PointerWrapPanelButton.xaml
    /// </summary>
    public partial class PointerWrapPanelButton : UserControl
    {
        public PointerWrapPanelButton()
        {
            InitializeComponent();


            Rectangle img = new Rectangle();
            img.Width = 100;
            img.Height = 100;
            img.Fill = Brushes.LightGray;
            pointer_wrap_panel.AddItem(img, ItemType.Normal);

            Rectangle img2 = new Rectangle();
            img2.Width = 100;
            img2.Height = 100;
            img2.Fill = Brushes.Orange;
            pointer_wrap_panel.AddItem(img2, ItemType.Normal);

            Rectangle img3 = new Rectangle();
            img3.Width = 100;
            img3.Height = 100;
            img3.Fill = Brushes.Red;
            pointer_wrap_panel.AddItem(img3, ItemType.Normal);

            Rectangle img4 = new Rectangle();
            img4.Width = 100;
            img4.Height = 100;
            img4.Fill = Brushes.DarkBlue;
            pointer_wrap_panel.AddItem(img4, ItemType.Normal);

            pointer_wrap_panel.PointToElement = this;
            pointer_wrap_panel.ButtonClicked += new EventHandler<EventArgs>(pointer_wrap_panel_ButtonClicked);
            pointer_wrap_panel.FadeOutCompleted += new EventHandler<EventArgs>(pointer_wrap_panel_FadeOutCompleted);

        }

        private void StartFadeIn()
        {
            Vector v_offset = UIHelper.GetOffsetOverall(this);
            pointer_wrap_panel.Show(new Vector2d(v_offset.X + this.ActualWidth / 2, v_offset.Y), 300, 300, RenderDirection);
        }

        private void StartFadeOut()
        {
            pointer_wrap_panel.Hide();
        }


        public PointerWrapPanel RemoveWrapPanel()
        {
            base_grid.Children.Remove(pointer_wrap_panel);
            return pointer_wrap_panel;
        }

        #region EventHandler
        private void toggle_button_Checked(object sender, RoutedEventArgs e)
        {
            StartFadeIn();
            border.Visibility = Visibility.Visible;
            path.Visibility = Visibility.Visible;
            toggle_button.Opacity = 0.2;
        }

        private void toggle_button_Unchecked(object sender, RoutedEventArgs e)
        {
            StartFadeOut();
            border.Visibility = Visibility.Hidden;
            path.Visibility = Visibility.Hidden;
            toggle_button.Opacity = 1;
        }


        void pointer_wrap_panel_FadeOutCompleted(object sender, EventArgs e)
        {

        }

        void pointer_wrap_panel_ButtonClicked(object sender, EventArgs e)
        {

        }
        #endregion

        #region Properties
        public PointerWrapPanel PointerWrapPanel
        {
            get
            {
                return pointer_wrap_panel;
            }
        }

        public ToggleButton ToggleButton
        {
            get
            {
                return toggle_button;
            }
        }

        public Style PanelButtonStyle
        {
            get { return (Style)GetValue(PanelButtonStyleProperty); }
            set { SetValue(PanelButtonStyleProperty, value); }
        }
        public static readonly DependencyProperty PanelButtonStyleProperty = DependencyProperty.Register("PanelButtonStyle", typeof(Style), typeof(PointerWrapPanelButton), new PropertyMetadata(null));
        #endregion

        public RenderDirection RenderDirection = Taskbar.RenderDirection.Top;

    }
}
