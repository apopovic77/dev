﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Data;

namespace Logicx.IaResources.Converters
{
    public class DoubleAnimationConverters : IValueConverter
    {
        /// <summary>
        /// Für Animationen dürfen nicht Nan oder Infinity Werte verwendet werden. Bei Bindungen können diese Werte aber vorkommen.
        /// Dieser Konverter überprüft ob der gebundene Werte für eine Animation zulässig ist, wenn nicht wird ein
        /// Default Wert genommen, der als Parameter übergeben werden muss
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
           try
           {
                if (value != null)
                {
                    double binding_value = (double)value;
                    double default_value = Double.Parse(parameter.ToString());

                    if (Double.IsInfinity(binding_value) || Double.IsNaN(binding_value))
                        return default_value;
                    else
                    {
                        return binding_value;
                    }
                }
           }
            catch(Exception ex)
            {
                
            }

            return 1500;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }
}
