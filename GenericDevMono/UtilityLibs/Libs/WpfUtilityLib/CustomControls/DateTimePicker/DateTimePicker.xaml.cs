﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;

namespace Logicx.WpfUtility.CustomControls.DateTimePicker
{

    public enum DateTimeVisibility
    {
        DateOnly,
        TimeOnly,
        DateAndTime
    }

    public class InternalDateTime : DependencyObject, INotifyPropertyChanged
    {
        public InternalDateTime(DateTime date_time)
        {
            Date = date_time.Date;
            Hour = date_time.Hour;
            Minute = date_time.Minute;
            Second = date_time.Second;
        }

        public void SetBinding(DependencyProperty property, BindingBase binding_base)
        {
            BindingOperations.SetBinding(this, property, binding_base);
        }

        /// <summary>
        /// Send the property changed event
        /// </summary>
        /// <param name="property_name"></param>
        protected void SendPropertyChanged(string property_name)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property_name));
        }

        public DateTime Date
        {
            get { return (DateTime)GetValue(DateProperty); }
            set
            {
                SetValue(DateProperty, value);
                SendPropertyChanged("Date");
            }
        }

        // Using a DependencyProperty as the backing store for Date.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DateProperty =
            DependencyProperty.Register("Date", typeof(DateTime), typeof(DateTimePicker), new UIPropertyMetadata(DateTime.Today));

        public int Hour
        {
            get { return (int)GetValue(HourProperty); }
            set
            {
                SetValue(HourProperty, value);
                SendPropertyChanged("Hour");
            }
        }

        // Using a DependencyProperty as the backing store for SelectedHour.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty HourProperty =
            DependencyProperty.Register("SelectedHour", typeof(int), typeof(DateTimePicker), new UIPropertyMetadata(0));

        public int Minute
        {
            get { return (int)GetValue(MinuteProperty); }
            set
            {
                SetValue(MinuteProperty, value);
                SendPropertyChanged("Minute");
            }
        }

        // Using a DependencyProperty as the backing store for Minute.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MinuteProperty =
            DependencyProperty.Register("Minute", typeof(int), typeof(DateTimePicker), new UIPropertyMetadata(0));

        public int Second
        {
            get { return (int)GetValue(SecondProperty); }
            set
            {
                SetValue(SecondProperty, value);
                SendPropertyChanged("Second");
            }
        }

        // Using a DependencyProperty as the backing store for Second.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SecondProperty =
            DependencyProperty.Register("Second", typeof(int), typeof(DateTimePicker), new UIPropertyMetadata(0));




        public DateTimeVisibility DateTimeVisibilityInternal
        {
            get { return (DateTimeVisibility)GetValue(DateTimeVisibilityInternalProperty); }
            set
            {
                SetValue(DateTimeVisibilityInternalProperty, value);
                SendPropertyChanged("DateTimeVisibilityInternal");
            }
        }

        // Using a DependencyProperty as the backing store for DateTimeVisibilityInternal.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DateTimeVisibilityInternalProperty =
            DependencyProperty.Register("DateTimeVisibilityInternal", typeof(DateTimeVisibility), typeof(DateTimePicker), new UIPropertyMetadata(DateTimeVisibility.DateOnly));




        public bool SecondsVisibleInternal
        {
            get { return (bool)GetValue(SecondsVisibleInternalProperty); }
            set
            {
                SetValue(SecondsVisibleInternalProperty, value);
                SendPropertyChanged("SecondsVisibleInternal");
            }
        }

        // Using a DependencyProperty as the backing store for SecondsVisibleInternal.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SecondsVisibleInternalProperty =
            DependencyProperty.Register("SecondsVisibleInternal", typeof(bool), typeof(DateTimePicker), new UIPropertyMetadata(false));


        public event PropertyChangedEventHandler PropertyChanged;
    }

    /// <summary>
    /// Interaction logic for DateTimePicker.xaml
    /// </summary>
    public partial class DateTimePicker : UserControl, INotifyPropertyChanged
    {
        #region Construction and Initialization
        public DateTimePicker()
        {
            _date_time = new InternalDateTime(DateTime.Now);

            Initialized += new EventHandler(DateTimePicker_Initialized);

            _date_time.PropertyChanged += new PropertyChangedEventHandler(_date_time_PropertyChanged);

            InitializeComponent();
        }

        void DateTimePicker_Initialized(object sender, EventArgs e)
        {
            stackpanel.DataContext = _date_time;
        }

        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(e);

            if (e.Property == SelectedDateTimeProperty)
            {
                _ignore_datetime_change = true;
                SetValues((DateTime)e.NewValue);
                _ignore_datetime_change = false;
            }
            else if(e.Property == DateTimeVisibilityProperty)
            {
                if (_date_time != null)
                    _date_time.DateTimeVisibilityInternal = (DateTimeVisibility)e.NewValue;
            }
            else if(e.Property == DatePickerStyleProperty)
            {
                dp.Style = (Style)e.NewValue;
            }
            else if(e.Property == TimeBorderStyleProperty)
            {
                bd_time.Style = (Style)e.NewValue;
            }
            else if(e.Property == TimeTextBoxStyleProperty)
            {
                tb_hour.Style = (Style) e.NewValue;
                tb_min.Style = (Style)e.NewValue;
                tb_sec.Style = (Style)e.NewValue;
            }
            else if(e.Property == TimeSeparatorStyleProperty)
            {
                sep_min.Style = (Style)e.NewValue;
                sep_sec.Style = (Style)e.NewValue;
            }
        }

        /// <summary>
        /// Send the property changed event
        /// </summary>
        /// <param name="property_name"></param>
        protected void SendPropertyChanged(string property_name)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property_name));
        }

        void _date_time_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Date" || e.PropertyName == "Hour" || e.PropertyName == "Minute" || e.PropertyName == "Second")
            {
                SelectedDateTime = new DateTime(_date_time.Date.Year, _date_time.Date.Month, _date_time.Date.Day,
                                                _date_time.Hour, _date_time.Minute, _date_time.Second);
            }
        }
        #endregion

        #region Event handling
        private void hour_keyDown(object sender, KeyEventArgs e)
        {
            if (tb_hour.SelectionStart == tb_hour.Text.Length && e.Key == Key.Right)
            {
                tb_min.Focus();
                e.Handled = true;
            }
        }

        private void min_keyDown(object sender, KeyEventArgs e)
        {
            if (tb_min.SelectionStart == tb_min.Text.Length && e.Key == Key.Right)
            {
                tb_sec.Focus();
                e.Handled = true;
            }
            else if (tb_min.SelectionStart == 0 && e.Key == Key.Left)
            {
                tb_hour.Focus();
                e.Handled = true;
            }
        }

        private void sec_keyDown(object sender, KeyEventArgs e)
        {
            if (tb_sec.SelectionStart == 0 && e.Key == Key.Left)
            {
                tb_min.Focus();
                e.Handled = true;
            }
        }

        #endregion

        #region Dependency Properties

        public DateTime SelectedDateTime
        {
            get { return (DateTime)GetValue(SelectedDateTimeProperty); }
            set
            {
                if (value != SelectedDateTime && !_ignore_datetime_change)
                {
                    SetValue(SelectedDateTimeProperty, value);
                }
            }
        }

        private void SetValues(DateTime value)
        {
            _date_time.Date = value.Date;
            _date_time.Hour = value.Hour;
            _date_time.Minute = value.Minute;
            _date_time.Second = value.Second;
        }

        // Using a DependencyProperty as the backing store for SelectedDateTime.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectedDateTimeProperty =
            DependencyProperty.Register("SelectedDateTime", typeof(DateTime), typeof(DateTimePicker), new UIPropertyMetadata(DateTime.MinValue, OnSelectedDateTimeChanged));

        private static void OnSelectedDateTimeChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            //Debug.WriteLine("Set SelectedDateTime from " + e.OldValue + " to " + e.NewValue);
        }



        public DateTimeVisibility DateTimeVisibility
        {
            get { return (DateTimeVisibility)GetValue(DateTimeVisibilityProperty); }
            set
            {
                SetValue(DateTimeVisibilityProperty, value);
            }
        }

        // Using a DependencyProperty as the backing store for DateTimeVisibility.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DateTimeVisibilityProperty =
            DependencyProperty.Register("DateTimeVisibility", typeof(DateTimeVisibility), typeof(DateTimePicker), new UIPropertyMetadata(DateTimeVisibility.DateOnly));



        public bool SecondsVisible
        {
            get { return (bool)GetValue(SecondsVisibleProperty); }
            set
            {
                SetValue(SecondsVisibleProperty, value);

                if (_date_time != null)
                    _date_time.SecondsVisibleInternal = value;
            }
        }

        // Using a DependencyProperty as the backing store for SecondsVisible.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SecondsVisibleProperty =
            DependencyProperty.Register("SecondsVisible", typeof(bool), typeof(DateTimePicker), new UIPropertyMetadata(false));






        public Brush TextBackgroundBrush
        {
            get { return (Brush)GetValue(TextBackgroundBrushProperty); }
            set { SetValue(TextBackgroundBrushProperty, value); }
        }

        // Using a DependencyProperty as the backing store for TextBackgroundBrush.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TextBackgroundBrushProperty =
            DependencyProperty.Register("TextBackgroundBrush", typeof(Brush), typeof(DateTimePicker), new UIPropertyMetadata(Brushes.White));




        public Style DatePickerStyle
        {
            get { return (Style)GetValue(DatePickerStyleProperty); }
            set { SetValue(DatePickerStyleProperty, value); }
        }

        // Using a DependencyProperty as the backing store for DatePickerStyle.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DatePickerStyleProperty =
            DependencyProperty.Register("DatePickerStyle", typeof(Style), typeof(DateTimePicker), new UIPropertyMetadata(null));




        public Style TimeBorderStyle
        {
            get { return (Style)GetValue(TimeBorderStyleProperty); }
            set { SetValue(TimeBorderStyleProperty, value); }
        }

        // Using a DependencyProperty as the backing store for TimeBorderStyle.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TimeBorderStyleProperty =
            DependencyProperty.Register("TimeBorderStyle", typeof(Style), typeof(DateTimePicker), new UIPropertyMetadata(null));





        public Style TimeTextBoxStyle
        {
            get { return (Style)GetValue(TimeTextBoxStyleProperty); }
            set { SetValue(TimeTextBoxStyleProperty, value); }
        }

        // Using a DependencyProperty as the backing store for TimeTextBoxStyle.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TimeTextBoxStyleProperty =
            DependencyProperty.Register("TimeTextBoxStyle", typeof(Style), typeof(DateTimePicker), new UIPropertyMetadata(null));




        public Style TimeSeparatorStyle
        {
            get { return (Style)GetValue(TimeSeparatorStyleProperty); }
            set { SetValue(TimeSeparatorStyleProperty, value); }
        }

        // Using a DependencyProperty as the backing store for TimeSeparatorStyle.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TimeSeparatorStyleProperty =
            DependencyProperty.Register("TimeSeparatorStyle", typeof(Style), typeof(DateTimePicker), new UIPropertyMetadata(null));

        
        #endregion

        #region Attributes
        private readonly InternalDateTime _date_time;
        private bool _ignore_datetime_change;
        #endregion

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
