﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using Logicx.WpfUtility.CustomControls.HelpProvider;

namespace Logicx.WpfUtility.CustomControls.OverlayManager
{
    /// <summary>
    /// Interaktionslogik für OverlayHelp.xaml
    /// </summary>
    public partial class OverlayHelp : UserControl //, IOverlayHelp
    {
        public OverlayHelp()
        {
            InitializeComponent();
            help_menu.SelectionChanged += new EventHandler(help_menu_SelectionChanged);
            help_menu.SelectionRemoved += new EventHandler(help_menu_SelectionRemoved);
        }


        
        #region EventHandler
        /// <summary>
        /// im Menu Wurde vom Benutzer ein neues Element markiert
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void help_menu_SelectionChanged(object sender, EventArgs e)
        {
            StartHelpTextChanging((IOneLevelTreeViewItem) sender);      
        }             
        
        void help_menu_SelectionRemoved(object sender, EventArgs e)
        {
            StartHelpTextChanging(null);
        }   

        private void StartHelpTextChanging(IOneLevelTreeViewItem item)
        {
            _selected_item = item;

            Storyboard sb = (Storyboard)FindResource("anim_helptext_fade_out");
            sb.Completed += new EventHandler(sb_Completed);
            sb.Begin(this);   
        }
        
        /// <summary>
        /// Wenn sich die Größe des Hilfemenüs ändert, soll eine Animation gestartet werden.
        /// TODO: Wenn die Größe der Hilfe kleiner wird funktioniert der Code noch nicht (wurde daher ausgeklammert)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void help_menu_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (!e.HeightChanged)
                return;

            double offset = 10;

            _grid_height_anim = new DoubleAnimation();
            _grid_height_anim.From = helptree_scrollviewer.ActualHeight;
            if (e.NewSize.Height+offset < helptree_scrollviewer.MaxHeight)
                _grid_height_anim.To = e.NewSize.Height+offset;
            else
                _grid_height_anim.To = helptree_scrollviewer.MaxHeight;

            _grid_height_anim.DecelerationRatio = 1;
            _grid_height_anim.Duration = new Duration(TimeSpan.FromMilliseconds(500));
            _grid_height_anim.FillBehavior = FillBehavior.HoldEnd;


            helptree_scrollviewer.BeginAnimation(FrameworkElement.HeightProperty, _grid_height_anim, HandoffBehavior.SnapshotAndReplace);
        }
        #endregion

        #region IOverlayHelp Implementation
        /// <summary>
        /// Dient zur Darstellung eines bestimmten Elementes im dargestellten Help-Tree
        /// Dazu muss aber zuerst der zugehörige Tree mit der Methode ShowHelpTree dargestellt werden
        /// Das heißt, wenn der visuelle Baum nicht erzeugt wurde macht diese Methode gar nichts.
        /// Anderenfalls navigiert es in dem Baum zu dem übergebenen Element, fokusiert dieses und stellt 
        /// eventuell vorhandene Hilfetexte dar.
        /// 
        /// Die Funktion dieser Methode ist mit der der TreeVietItem-Selection gekoppelt -> Wenn ein Element angeklickt wurde, dass nicht bereits aktiv ist, werden
        /// alle anderen Elemente deaktiviert und das TreeViewItem.Selected auf true gesetzt. Die eigentliche Darstellung der Hilfe regelt dann anschließend der EventHandler 
        /// der TreeViewItems.
        /// </summary>
        /// <param name="help_element">Das Element im dargestellten HelpTree, auf das navigiert werden soll</param>
        public void ShowHelpElement(IOneLevelTreeViewItem help_element)
        {
            Monitor.Enter(this);
            if (help_element is HelpNode)
            {
                HelpNode h_node = (HelpNode)help_element;
                ShowHelpElement(h_node);
            }

            _selected_item = help_element;

            Storyboard sb = (Storyboard)FindResource("anim_helptext_fade_out");
            sb.Completed += new EventHandler(sb_Completed);
            sb.Begin(this);
            Monitor.Exit(this);
           
        }

        void sb_Completed(object sender, EventArgs e)
        {
            ((ClockGroup)sender).Completed -= sb_Completed;

            if (_selected_item == null)
                return;

            if (_selected_item is HelpNode)
            {
                HelpNode h_node = (HelpNode)_selected_item;
                ShowHelpElement(h_node);

                Storyboard sb = (Storyboard)FindResource("anim_helptext_fade_in");
                sb.Begin(this);
            }
        }

        public void ShowHelpElement(HelpNode help_element)
        {
            helptext_scrollview.ScrollToTop();
            help_description_text_tb.Text = help_element.HelpDescription;
        }

        /// <summary>
        /// Wenn aufgrund eines fokusierten Steuerelementes in der Hilfe automatisch navigiert werden soll
        /// </summary>
        /// <param name="help_element"></param>
        public void ShowHelpElementAuto(HelpNode help_element)
        {
            if (auto_help_toggle.IsChecked != true)
            {
                //ShowHelpElement((IOneLevelTreeViewItem)help_element);
                //help_menu.JumpToMenuEntry(help_element);
                help_menu.SimulateItemMouseClick(help_element);
            }
        }

        /// <summary>
        /// Das Hilfecontrol wird mit einem bestimmten Hilfebaum initialisiert
        /// </summary>
        /// <param name="help_tree"></param>
        public void ShowHelpTree(HelpNode help_tree)
        {
            Monitor.Enter(this);          
            help_menu.SetTreeSource(help_tree);
            Monitor.Exit(this);
        }
        #endregion

        #region Attributes

        protected IOneLevelTreeViewItem _selected_item;
        protected DoubleAnimation _grid_height_anim = new DoubleAnimation();
        protected bool _pending_animation;
        protected System.Timers.Timer _pending_anim_timer;
        #endregion

    }
}
