﻿using Google.Cloud.Vision.V1;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Awanto.WhatsAppCore.Entities
{
    /// <summary>
    /// this is the n to m table for nachrichten medien
    /// </summary>
    public class NachrichtMedium
    {
        public int NachrichtId { get; set; }
        public Nachricht Nachricht { get; set; }
        public int MediumId { get; set; }
        public Medium Medium { get; set; }
    }

    public class Medium : DbEntity
    {
        [StringLength(255, MinimumLength = 2)]
        public string PathToMedia { get; set; }
        /// <summary>
        /// ein wert den man zur referenzierung verwenden kann
        /// </summary>
        public string Referenz { get; set; }

        public ICollection<NachrichtMedium> Nachrichten { get; set; }
    }

    public class Bild : Medium
    {
        public string Annotation { get; set; }
        public bool IsText { get; set; }

        /// <summary>
        /// gibt dem bild semantiken
        /// </summary>
        /// <param name="uri"></param>
        /// <returns></returns>
        public void UpdateImageAnnotation()
        {
            var image = Image.FromUri(PathToMedia);
            var client = ImageAnnotatorClient.Create();
            WebDetection annotation = client.DetectWebInformation(image);
            IsText = annotation.WebEntities.Where(e => e.Score > 0.8 && e.EntityId.ToLower() == "text").Count() > 0;
            Annotation = annotation.ToString();
        }

    }

    public class TextBild : Bild
    {
    }

    public class Video : Medium
    {

    }
}
