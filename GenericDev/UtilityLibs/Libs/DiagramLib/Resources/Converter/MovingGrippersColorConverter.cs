﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Shapes;
using Logicx.WpfUtility;

namespace Logicx.DiagramLib.Resources.Converter
{
    public class MovingGrippersColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if(value is bool)
            {
                bool can_move = (bool) value;

                if (can_move)
                    return new SolidColorBrush(ColorGenerator.HexToColor("#7FC1C1C1"));

                return new SolidColorBrush(ColorGenerator.HexToColor("#FF8D363B"));
            }

            return false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
