﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Logicx.WpfUtility.PieMenu
{
    [Serializable]
    public class PieMenuEntry : PieMenuEntryBase<PieMenuEntry>
    {
        public PieMenuEntry(Color bg_active_c, string name) : base(bg_active_c, name)
        {
        }

        public PieMenuEntry(Color bg_active_c, string name, string key) : base(bg_active_c, name, key)
        {
        }

        public PieMenuEntry(string name) : base(name)
        {
        }

        public PieMenuEntry(string name, string key) : base(name, key)
        {
        }
    }
}