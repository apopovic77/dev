using System;
using MapPoint;
using System.Collections;
using System.Windows.Forms;
using System.Drawing;
using GeoLib;
using MathLib;


namespace MapX
{
	/// <summary>
	/// Summary description for MapX_StdUtilities.
	/// >>THIS CLASS IS NOT THREADSAFE<<
	/// </summary>
	public class MapXApplication : System.IDisposable
	{

		protected double m_y_graz = 525;
		protected double m_x_graz = 597;
		protected double m_graz_lat = 47.0666667;
		protected double m_graz_long = 15.45;
		protected double m_calc_lat_per_pixel = -0.00028720626631853821;
		protected double m_calc_long_per_pixel = 0.0004332314067278283;

		
		/// <summary>
		/// der maximale fehler die f�r uns akzeptabel ist bei der bestimmtund von mapx locations
		/// ist 10 km
		/// </summary>
		protected const int MAX_DIST_ERROR_VALIDATION = 10; 


		/// <summary>
		/// step_len in KM
		/// this constant is used by, GetGeoObjectsOnRoute(...)
		/// the algorightm tends to errors if the the step len is smaller then 6000 meters
		/// this error is caused of possible turnarounds where a route seems to go backwards
		/// e.g. autobahn aufahrten, dort kann es vorkommen das man kreisf�rmig auf die autobahn auff�hrt.
		/// wird der connection point ganu in so einen kreis gelegt kommt es zu fehlern bei der berechnung
		/// der poly around line da auf einmal ein starker richtungswechsel in die falsche richtung erfolgt
		/// wegen der einhaltung des radius um die linie kommt es dann zu starken spitzen die bis ins unendliche
		/// gehen k�nnen. die abfrage d�rfte somit keine ann�herung and die route darstellen.
		/// </summary>
		public const int MIN_STEP_LEN = 6;


		/// <summary>
		/// default used window state of instantiated application
		/// </summary>
		public static GeoWindowState stdWindowState = GeoWindowState.geoWindowStateMaximize;

		/// <summary>
		/// default used geounits of instantiated application
		/// </summary>
		public static GeoUnits stdGeoUnits = GeoUnits.geoKm;

		/// <summary>
		/// the one and only app object, this ones instantiated and used for mapx calcs
		/// </summary>
		protected ApplicationClass m_app = null;

		/// <summary>
		/// gets an application object, returns a new application class if there does not already exist one, otherwise this appclass obj is being returned
		/// </summary>
		public ApplicationClass Application{get{return m_app;}}

		/// <summary>
		/// if true mapx is visible to user
		/// </summary>
		protected bool stdVisible = false;

		/// <summary>
		/// if true mapx app can be used via gui
		/// </summary>
		protected bool stdManipulateable = true;

		/// <summary>
		/// default map style set to active map
		/// </summary>
		protected GeoMapStyle stdMapStyle = GeoMapStyle.geoMapStyleData;

		/// <summary>
		/// this ballonstate is automatically used for alle pushpins shown
		/// </summary>
		protected GeoBalloonState stdBalloonState = GeoBalloonState.geoDisplayNone;

		/// <summary>
		/// holds path to the MapX Map that is to be opend when the Application object is instantiated.
		/// if this string is null no map will be opened, instead a new map is shown
		/// </summary>
		protected string stdMap = null;

		/// <summary>
		/// saves the map after quit if set to true
		/// </summary>
		protected bool stdSaveMap = false;


		/// <summary>
		/// default contructor, initializes a new application object
		/// </summary>
		public MapXApplication(){
			Init();
		}

		public MapXApplication(bool visible){
			stdVisible = visible;
			Init();
		}

		/// <summary>
		/// initializes application with default parameter
		/// </summary>
		public void Init(){
			//Create an application class
			m_app = new ApplicationClass();
			//open std map, do nothing if path is null
			if(!(stdMap == null || stdMap == ""))
				m_app.OpenMap(stdMap,false);
			//init with default settings
			m_app.Units = stdGeoUnits;
			//set app user manipulateable
			m_app.UserControl = stdManipulateable;
			//init with default windowstate
			m_app.WindowState = stdWindowState;
			//set app invisible
			m_app.Visible = stdVisible;
			//set mapstayle
			m_app.ActiveMap.MapStyle = stdMapStyle;
            
		}

		public void saveMap(){
			if(!m_app.ActiveMap.Saved)
				m_app.ActiveMap.Save();
		}


		/// <summary>
		/// closes the applicationclass obj, and set all vars to null
		/// </summary>
		private bool m_disposed = false;
		~MapXApplication() {
			Dispose(false);
		}
		public void Dispose() {
			Dispose(true);
			System.GC.SuppressFinalize(this);
		}
		public void Dispose(bool explicitCall) {
			if(!this.m_disposed) {
				if(explicitCall){
					//explicit call object are available perform custom cleanup
				}

				try{
					if(stdSaveMap)
						saveMap();
					else
						//set state to saved no matter if actually not saved
						//in this case we do want to loose data
						m_app.ActiveMap.Saved = true;
					m_app.Quit();
					m_app = null;
				}catch{}
			}
			m_disposed = true;   
		}




		public static GeoCountry GetGeoCountry(string country){
			country = country.ToLower();
			if( country == "austria" || country == "�sterreich" || country == "au" )
				return GeoCountry.geoCountryAustria;
			if( country == "germany" || country == "deutschland" || country == "de" )
				return GeoCountry.geoCountryGermany;

			throw new Exception("Konnte GeoCountry nicht finden! Vielleicht in Code fehlend! Vielleicht existiert das Land nicht!");
		}



		public GeoObject CreateGeoObjectByLocation(string city,string country,MapPoint.Map map,MapPoint.Location location){
			GeoObject geoObj =  new GeoObject();
			geoObj.name = city+" "+country;
			geoObj.city = city;
			geoObj.country = country;
			geoObj.altitude = map.Altitude;
			geoObj.pixelsize = map.PixelSize;
			geoObj.units = m_app.Units;

			GeoCoordinates geocoords = GetGeoLatLong(location);
			geoObj.longitude = geocoords.Longitude;
			geoObj.latitude = geocoords.Latitude;

			location.GoTo();
			int x = map.LocationToX(location);
			int y = map.LocationToY(location);
			//Find all geographic entities at that point
			FindResults rsContext = map.ObjectsFromPoint(x, y); 
			foreach( Location loc in rsContext ) 
			{
				if(GeoShowDataBy.geoShowByCity == loc.Type)
				{
					geoObj.name = loc.Name;
					geoObj.city = loc.Name;
				}
				else if(GeoShowDataBy.geoShowByRegion1 == loc.Type)
					geoObj.state = loc.Name;
				else if(GeoShowDataBy.geoShowByRegion2 == loc.Type)
					geoObj.region2 = loc.Name;
				else if(GeoShowDataBy.geoShowByRegion3 == loc.Type)
				{
					if(geoObj.city == "")
					{
						geoObj.city = loc.Name;
						geoObj.CityNameIsRegion3 = true;
					}
				}
				else if(GeoShowDataBy.geoShowByCountry == loc.Type)
					geoObj.country = loc.Name;
			}
			rsContext = null;
			return geoObj;
		}

		public GeoObject[] FindAllGeoObjectsByCityName(string city,string country){
			Map map = m_app.ActiveMap;
			MapPoint.FindResults rs = map.FindAddressResults(string.Empty, city, string.Empty, string.Empty, string.Empty, GetGeoCountry(country));
			Location location = null;
			
			//If it's not a good match, then don't accept it
			if(GeoFindResultsQuality.geoFirstResultGood == rs.ResultsQuality)
			{
				object index = 1;
				location = (Location)rs.get_Item(ref index);
				GeoObject geoobj = CreateGeoObjectByLocation(city,country,map,location);
				GeoObject[] geoobj_arr = new GeoObject[1];
				geoobj_arr[0] = geoobj;
				return geoobj_arr;
			}
			else
			{
				if(rs.Count == 0)
					return null;
				GeoObject[] geoobj_arr = new GeoObject[rs.Count];
				for(int i=0;i<rs.Count;i++){
					object index = i+1;
					location = (Location)rs.get_Item(ref index);
					GeoObject geoobj = CreateGeoObjectByLocation(city,country,map,location);
					geoobj_arr[i] = geoobj;
				}
				return geoobj_arr;
			}
		}

		public GeoObject FindGeoObjectByCityName(string city,string country)
		{
			Map map = m_app.ActiveMap;
			
			MapPoint.FindResults rs = map.FindAddressResults(string.Empty, city, string.Empty, string.Empty, string.Empty, GetGeoCountry(country));
			Location location = null;
			
			//If it's not a good match, then don't accept it
			if(GeoFindResultsQuality.geoFirstResultGood == rs.ResultsQuality){
				object index = 1;
				location = (Location)rs.get_Item(ref index);
				GeoObject geoobj = CreateGeoObjectByLocation(city,country,map,location);
				return geoobj;
			}else 
				throw new Exception("City nicht eindeutig, Suchergebnis nicht genau genug! bitte City genauer spezifizieren!");
		}

        public GeoObject FindGeoObjectByStreetName(string street, string city, string country)
        {
            Map map = m_app.ActiveMap;

            MapPoint.FindResults rs = map.FindAddressResults(street, city, string.Empty, string.Empty, string.Empty, GetGeoCountry(country));
            Location location = null;

            //If it's not a good match, then don't accept it
            if (GeoFindResultsQuality.geoFirstResultGood == rs.ResultsQuality)
            {
                object index = 1;
                location = (Location)rs.get_Item(ref index);
                GeoObject geoobj = CreateGeoObjectByLocation(city, country, map, location);
                return geoobj;
            }
            else
            {
                throw new Exception("City nicht eindeutig, Suchergebnis nicht genau genug! bitte City genauer spezifizieren!");
            }
        }

		/// <summary>
		/// die funktion soll eine city die aus der db kommt, aus wahrscheinlich die coords von nga besitzt in ein mapx location aufl�sen
		/// 1. wenn es diese gibt bzw. wenn es mehrere gibt soll die funktion soll intelligent sein die richtige zu finden daher die lat lon state infos
		/// 2. sollte diese funktion die city nicht aufl�sen k�nnen, muss sie also nicht in mappoint vorhanden sein in dem fall kann dann nicht 
		/// routen m�ssig danach gesucht werden. was nicht bedeuten muss das die lat lon info zu dem jeweiligen ort auch tats�chlich nicht existieren
		/// es ist davon auszugehen dass mappo�nt einfach nicht alle orte gespeichert hat.
		/// </summary>
		/// <param name="city"></param>
		/// <param name="country"></param>
		/// <param name="state"></param>
		/// <param name="latitude"></param>
		/// <param name="longitude"></param>
		/// <returns></returns>
		public Location GetLocationByCity(string city,string country,double latitude,double longitude){
			//hol einmal alles was du zu dem namen finden kannst
			MapPoint.FindResults rs = m_app.ActiveMap.FindResults(city+","+country);
			
			//wieviele eintr�ge mit city code gibt es in dem result
			int citycount =  GetCountLocationsWithCityCode(rs);
			
			//the location we hope to find
			Location location=null;

			if(citycount > 0)
			{
				bool foundCity = true;
				int city_order_id = 0;
				do
				{
					city_order_id++;
					if(city_order_id > citycount){
						foundCity = false;
						break;
					}
					//probiert mal ob es eine location mit einem city code gibt
					location = GetLocationByCity(rs,city_order_id);
				}while(!validateLocation(location,latitude,longitude));
				if(foundCity)
					return location;
			}
			
			//wenn ich noch hier bin war die city suche nicht erfolgreich
			//probiers mit anderen dingen
			
			location = null;
			location = GetLocationByDefault(rs);
			if(location != null)
			{
				if(validateLocation(location,latitude,longitude))
					return location;
			}
			location = null;
			location = GetLocationByRegion3(rs);
			if(location != null)
			{
				if(validateLocation(location,latitude,longitude))
					return location;
			}
			location = null;
			location = GetLocationByRegion2(rs);
			if(location != null)
			{
				if(validateLocation(location,latitude,longitude))
					return location;
			}
			return null;
		}

		private bool validateLocation(Location location, double latitude, double longitude)
		{
			GeoCoordinates coords = GetGeoLatLong(location);
			double dist = GeoFactory.DistanceCalculation.GetRealDistance(coords.Latitude,coords.Longitude,latitude,longitude);
			if(dist > MAX_DIST_ERROR_VALIDATION)
				return false;
			else
				return true;
		}

		private int GetCountLocationsWithCityCode(FindResults rs)
		{
			int count = 0;
			Location location;
			object index=0;
			for(int i=0; i < rs.Count;i++)
			{
				index = i+1;
				location =  (Location)rs.get_Item(ref index);
				if(location.Type == GeoShowDataBy.geoShowByCity)
					count++;
				if(i > 15)
					break;
			}
			return count;
		}

  
  
//		public class ImageConverter :System.Windows.Forms.AxHost  
//  		{  
//  			public ImageConverter(): base(null) {}  
//  			public static stdole.IPictureDisp ImageToIPicture(System.Drawing.Image image)  
//  			{
//				return (stdole.IPictureDisp)System.Windows.Forms.AxHost.GetIPictureDispFromPicture(image);  
//  			}  
//  			public static System.Drawing.Image IPictureToImage(stdole.StdPicture picture)  
//  			{ 
//				return System.Windows.Forms.AxHost.GetPictureFromIPicture(picture); 
//			}  
//  		}


		public System.Drawing.Bitmap GetImageOfCurrentMap()
		{
			try
			{
				System.Drawing.Bitmap img = null;
				// save current clipboard 
				//object save =  Clipboard.GetDataObject(); 
				Clipboard.SetDataObject(new DataObject());
				// copy map to clipboard 
				m_app.ActiveMap.CopyMap(); 
				// get picture from clipboard 
				IDataObject pict = Clipboard.GetDataObject(); 
				// check available convert formats and convert 
				string[] formats = pict.GetFormats(); 
				foreach ( string s in formats ) 
					if ( s.EndsWith(System.Windows.Forms.DataFormats.Bitmap) ) 
					{ 
						img = (System.Drawing.Bitmap)pict.GetData(System.Windows.Forms.DataFormats.Bitmap); 
						break; 
					} 

				//try to restore clipboard 

				Clipboard.SetDataObject(new DataObject());
				return img;
			}
			catch(Exception e){
				Console.WriteLine(e.StackTrace);
				throw e;
			}

//			stdole.StdPicture pp=(stdole.StdPicture)mputils.GetPictureFromObject(m_app.ActiveMap,100,100);
//			System.Drawing.Image myImg = ImageConverter.IPictureToImage(pp);  
//			myImg.Width = 1024;
//			myImg.Height = 768;
//			myImg.HorizontalResolution = 30;
//			myImg.VerticalResolution = 30;  
//			float HimeterWidth=(float)myImg.Width/myImg.HorizontalResolution*2.54;  //min 1100  
//			float HimeterHeight=(float)myImg.Height/myImg.VerticalResolution*2.54;// min 1100 
//			System.Drawing.Image myNewImg=new System.Drawing.Bitmap(myImg,744,496);

			

//			MapPoint.MapPointUtilitiesClass oo=new MapPoint.MapPointUtilitiesClass();  
//  			stdole.StdPicture pp=(stdole.StdPicture)  
//  			oo.GetPictureFromObject(objMap,HimeterWidth,HimeterHeight);  
//  			System.Drawing.Image ddd =ImageConverter.IPictureToImage(pp);  
//			ddd.Width=
//			finally image width  
//	  		ddd.Height=
//			finally image height  
//			ddd.HorizontalResolution = DpiX  
//			ddd.VerticalResolution =DpiY  
//			HimeterWidth=(float)ddd.Width/ddd.HorizontalResolution*2.54;  //min 1100  
//			HimeterHeight=(float)ddd.Height/ddd.VerticalResolution*2.54;// min 1100  
//			Image you get is always Size  
//			(Screen.PrimaryScreen.Bounds.Width,Screen.PrimaryScreen.Bounds.Height) then  
//			you have to scale it:  
//			Image NewImage=new Bitmap(ddd,744,496);  		
		}


		public GeoObject FindGeoObjectByZipCode(uint zipCode, GeoCountry country)
		{
			Map map = m_app.ActiveMap;
			MapPoint.FindResults rs = map.FindAddressResults(string.Empty, string.Empty, string.Empty, string.Empty, zipCode.ToString(), country);
			Location zipLoc = null;
			object index = 1;
			
			//If it's not a good match, then don't accept it
			if(GeoFindResultsQuality.geoFirstResultGood == rs.ResultsQuality)
				zipLoc = (Location)rs.get_Item(ref index);
			else 
				return null;

			//Must be a match to a Post Code (US ZIP Code)
			if(GeoShowDataBy.geoShowByPostal1 != zipLoc.Type && GeoShowDataBy.geoShowByPostal2 != zipLoc.Type)
				return null;


			GeoObject ret = new GeoObject();
			ret.zip = zipCode;

			//Go to the ZIP Code location on the map to hit test 
			Location actualLocation = map.Location;
			GeoCoordinates zip_loc_coords = GetGeoLatLong(zipLoc);

//			//move to graz as graz is our datum an get x y values of this location
//			FindResults rs2 =  map.FindResults("Graz, Austria");
//			MapPoint.Location map_location = (Location)rs2.get_Item(ref index);
//			map_location.GoTo();
//			double altitude = map.Altitude;
			int x=0; x = map.LocationToX(zipLoc);
			int y=0; y = map.LocationToY(zipLoc);
//			ret.x = (long)x;
//			ret.y = (long)y;
//			ret.altitude = map.Altitude;
//			ret.pixelsize = map.PixelSize;
//			double v_distance_y = ret.x - m_y_graz;
//			double v_distance_x = ret.y - m_x_graz;
//			double latitude  = m_graz_lat  + (v_distance_y * m_calc_lat_per_pixel);
//			double longitude = m_graz_long + (v_distance_x * m_calc_long_per_pixel);
			ret.longitude = zip_loc_coords.Longitude;
			ret.latitude = zip_loc_coords.Latitude;

			zipLoc.GoTo();
			x = map.LocationToX(zipLoc);
			y = map.LocationToY(zipLoc);


			//Find all geographic entities at that point
			FindResults rsContext = map.ObjectsFromPoint(x, y); 
			
			foreach( Location location in rsContext )
			{
				if(GeoShowDataBy.geoShowByCity == location.Type){
					ret.name = location.Name;
					ret.city = location.Name;
				}
				else if(GeoShowDataBy.geoShowByRegion1 == location.Type)
					ret.state = location.Name;
				else if(GeoShowDataBy.geoShowByRegion2 == location.Type)
					ret.region2 = location.Name;
				else if(GeoShowDataBy.geoShowByRegion3 == location.Type)
					if(ret.city == "")
						ret.city = location.Name;
				else if(GeoShowDataBy.geoShowByCountry == location.Type)
					ret.country = location.Name;
			}
			//set to old pos
			actualLocation.GoTo();
			map.Location = actualLocation;

			if(ret.name=="" && ret.state=="" && ret.region2=="" )
				return null;
			else
				return ret;
		}

		public void createPLZCoordsList(string country,int plz_beg,int plz_end)
		{
			System.IO.Stream s_out = System.IO.File.OpenWrite("./plzcoords_"+country+".csv");
			System.IO.StreamWriter f_out = new System.IO.StreamWriter(s_out,System.Text.Encoding.Unicode);

			Map map = m_app.ActiveMap;
			for(int i=plz_beg;i<plz_end;i++)
			{
				Console.Write(".");
				MapPoint.FindResults rs = map.FindAddressResults(string.Empty, string.Empty, string.Empty, string.Empty, i.ToString(), country);
				Location zipLoc = null;
				object index = 1;
			
				//If it's not a good match, then don't accept it
				if(GeoFindResultsQuality.geoFirstResultGood == rs.ResultsQuality)
					zipLoc = (Location)rs.get_Item(ref index);
				else 
					continue;

				//Must be a match to a Post Code (US ZIP Code)
				if(GeoShowDataBy.geoShowByPostal1 != zipLoc.Type && GeoShowDataBy.geoShowByPostal2 != zipLoc.Type)
					continue;

				GeoCoordinates coords = GetGeoLatLong(zipLoc);
				string tupel = i+";"+coords.Longitude+";"+coords.Latitude;
				Console.WriteLine(tupel);
				f_out.WriteLine(tupel);
			}
			f_out.Flush();
			f_out.Close();
			s_out.Close();
			f_out = null;
			s_out = null;
		}


		public void createPLZListFromMapX(string country,int plz_beg,int plz_end)
		{
			System.IO.Stream s_out = System.IO.File.OpenWrite("./plz"+country+".csv");
			System.IO.StreamWriter f_out = new System.IO.StreamWriter(s_out,System.Text.Encoding.Unicode);
			for(int i=plz_beg;i<plz_end;i++)
			{
				GeoObject geoobj = FindGeoObjectByZipCode(Convert.ToUInt32(i),GetGeoCountry(country));
				string[] ret = new string[10];
				if(geoobj != null)
				{
					ret[0] = geoobj.city;
					ret[1] = geoobj.region2;
					ret[2] = geoobj.state;
					ret[3] = geoobj.country;
					ret[4] = geoobj.longitude.ToString();
					ret[5] = geoobj.latitude.ToString();
					ret[6] = geoobj.x.ToString();
					ret[7] = geoobj.y.ToString();
					ret[8] = geoobj.altitude.ToString();
					ret[9] = geoobj.pixelsize.ToString();


					Console.WriteLine(i+";"+ret[0]+";"+ret[1]+";"+ret[2]+";"+ret[3]+";"+ret[4]+";"+ret[5]+";"+ret[6]+";"+ret[7]+";"+ret[8]+";"+ret[9]);
					f_out.WriteLine(i+";"+ret[0]+";"+ret[1]+";"+ret[2]+";"+ret[3]+";"+ret[4]+";"+ret[5]+";"+ret[6]+";"+ret[7]+";"+ret[8]+";"+ret[9]);
				}
			}
			f_out.Flush();
			f_out.Close();
			s_out.Close();
			f_out = null;
			s_out = null;
		}

//		/// <summary>
//		/// in order to get long lat by city reference values a needed,
//		/// std graz geo data is used for that, as this calculation is dependent on the resolution of the display
//		/// this function should be called in order to re calc this values,
//		/// if you change the resolution and you do not call this function
//		/// long lat values will be wrong
//		/// </summary>
//		public void CalcReferenceValues(){
//			double graz_long = 15.45;
//			double graz_lat  = 47.0666667;
//			double wels_long = 14.0333333;
//			double wels_lat  = 48.1666667;
//
//			object index = 1; 
//			Map map = m_app.Application.ActiveMap;
//			FindResults rs =  map.FindResults("Graz, Austria");
//			MapPoint.Location location = (Location)rs.get_Item(ref index);
//			location.GoTo();
//
//			rs =  map.FindResults("Graz, Austria");
//			MapPoint.Location graz = (Location)rs.get_Item(ref index);
//			m_x_graz = map.LocationToX(graz);
//			m_y_graz = map.LocationToY(graz);
//
//
//			rs =  map.FindResults("Wels, Austria");
//			MapPoint.Location wels = (Location)rs.get_Item(ref index);
//			int x_wels=0; x_wels = map.LocationToX(wels);
//			int y_wels=0; y_wels = map.LocationToY(wels);
//
//
//			//Vector2d distance
//			double v_distance_y = y_wels - m_y_graz;
//			double v_distance_x = x_wels - m_x_graz;
//
//			//Vector2d distance as long an lat
//			double v_distance_long = wels_long - graz_long;
//			double v_distance_lat  = wels_lat - graz_lat;
//
//			//degrees per pixel based on wels - graz coords			
//			m_calc_long_per_pixel = v_distance_long / v_distance_x;
//			m_calc_lat_per_pixel = v_distance_lat / v_distance_y;
//		}
//
//		/// <summary>
//		/// tryies to calculate long and lat by given X and Y coords
//		/// in order to get long, lat graz is used as a reference.
//		/// Consider that there might be an error about +- 20 km depending
//		/// on the distance between loc and graz
//		/// </summary>
//		/// <param name="location">location of the long,lats you are trying to get e.g. "Wien, Austria"</param>
//		/// <returns>GeoObject</returns>
//		public GeoObject GetLatLongFrom(string location){
//			
//			//use graz as the reference
//			//wichtig hierbei ist dass die aufl�sung von dem rechner die mappoint laufen auf 1600 x 1200 gestellt ist.
//			//diese daten wurden einmalig berechnet und dienen nun als datum bzw. ausgangsbasis f�r die berechnung der long lat werte
//
//			Map map = m_app.ActiveMap;
//			FindResults rs =  map.FindResults("Graz, Austria");
//			object index = 1; MapPoint.Location map_location = (Location)rs.get_Item(ref index);
//			map_location.GoTo();
//			double altitude = map.Altitude;
//
//
//			rs =  map.FindResults(location);
//			MapPoint.Location loc = (Location)rs.get_Item(ref index);
//			int x_loc=0; x_loc = map.LocationToX(loc);
//			int y_loc=0; y_loc = map.LocationToY(loc);
//				
//			//Vector2d distance
//			double v_distance_y = y_loc - m_y_graz;
//			double v_distance_x = x_loc - m_x_graz;
//			double latitude  = m_graz_lat  + (v_distance_y * m_calc_lat_per_pixel);
//			double longitude = m_graz_long + (v_distance_x * m_calc_long_per_pixel);
//
//			GeoObject geoobj = new GeoObject();
//			geoobj.altitude = altitude;
//			geoobj.latitude = latitude;
//			geoobj.longitude = longitude;
//			geoobj.x = x_loc;
//			geoobj.y = y_loc;
//			geoobj.pixelsize = map.PixelSize;
//			geoobj.name = location;
//
//			return geoobj;
//		}

		public GeoObject GetGeoObjectFromLocation(Location location){
			//decl vars
			GeoObject geoObj = new GeoObject();
			int x,y;
			
			try{
				//get x,y coords of loc
				location.GoTo();
				x = m_app.ActiveMap.LocationToX(location);
				y = m_app.ActiveMap.LocationToY(location);

				//Find all geographic entities at that point
				FindResults rsContext = m_app.ActiveMap.ObjectsFromPoint(x, y); 
				try{
					foreach( Location loc in rsContext ) {
						if(GeoShowDataBy.geoShowByCity == loc.Type){
							geoObj.name = loc.Name;
							geoObj.city = loc.Name;
						}
						else if(GeoShowDataBy.geoShowByRegion1 == loc.Type)
							geoObj.state = loc.Name;
						else if(GeoShowDataBy.geoShowByRegion2 == loc.Type)
							geoObj.region2 = loc.Name;
						else if(GeoShowDataBy.geoShowByRegion3 == loc.Type)
							if(geoObj.city == "")
								geoObj.city = loc.Name;
						else if(GeoShowDataBy.geoShowByCountry == loc.Type)
							geoObj.country = loc.Name;
					}
				}catch{}
				rsContext = null;	
				
				GeoCoordinates geoCoord = GetGeoLatLong(location);
				geoObj.latitude = geoCoord.Latitude;
				geoObj.longitude = geoCoord.Longitude;
				geoObj.x = geoCoord.X;
				geoObj.y = geoCoord.Y;

				return geoObj;
			}catch{
				return null;
			}
		}


		public GeoObject GetGeoObjectInMiddleOfRoute(string from, string to){
			string[] sarr = new string[2];
			sarr[0] = from;
			sarr[1] = to;
			return GetGeoObjectInMiddleOfRoute(sarr);
		}

		public GeoObject[] GetGeoObjectsOnRoute(string from, string to,double step_size){
			string[] sroute = new string[2];
			sroute[0]=from;
			sroute[1]=to;
			return GetGeoObjectsOnRoute(sroute,step_size);
		}

		public GeoObject[] GetGeoObjectsOnRoute(string from, string to,double step_size,StepType st){
			string[] sroute = new string[2];
			sroute[0]=from;
			sroute[1]=to;
			return GetGeoObjectsOnRoute(sroute,step_size,st);
		}

		private void AddLoc(Location loc, ArrayList loc_arr,ref int curr_loc){
			try{
				loc_arr[curr_loc] = loc;
			}catch{
				loc_arr.Add(loc);
			}
		}
		
		public enum StepType {
			LenPerStep,
			MaxStepsPerRoute
		};


		/// <summary>
		/// calls the GeoGeoObjectOnRoute where per default the step size is associated with
		/// LenPerStep, which means that every step_size meters a connection point will be defined 
		/// if the route is sufficing enough to query lat lon values
		/// </summary>
		/// <param name="route_sarr"></param>
		/// <param name="step_size"></param>
		/// <returns></returns>
		public GeoObject[] GetGeoObjectsOnRoute(string[] route_sarr,double step_size){
			return GetGeoObjectsOnRoute(route_sarr,step_size,StepType.LenPerStep);
		}

		/// <summary>
		/// returns geobjects retrevied on the route, where each geoobject is at max step_len remote
		/// </summary>
		/// <param name="route_sarr"></param>
		/// <param name="step_len">steplen in meter</param>
		/// <returns></returns>
		public GeoObject[] GetGeoObjectsOnRoute(string[] route_sarr,double step_size,StepType st){
			//check paras in, two waypoint at least necessary
			if(route_sarr.Length<2)
				return null;

			ArrayList loc_arr = new ArrayList();
			FindResults rs;
			Location location=null,location_end=null,location_start=null;
			Route route;
			route = m_app.ActiveMap.ActiveRoute;

			object index = 1;
			double distance_complete=0, elapsedDistance_curr=0, elapsedDistance_prev=0;
			route.Clear();

			//add waypoints
			for(int i=0;i<route_sarr.Length;i++){
				location = null;
				string city_name = route_sarr[i].Split(",".ToCharArray())[0];
				rs = m_app.ActiveMap.FindResults(route_sarr[i]);
				location = GetLocationByCity(rs);
				if(location == null)
					location = GetLocationByRegion2(rs);
				if(location == null)
					location = GetLocationByRegion3(rs);
				if(location == null || location.Name.IndexOf(city_name)<0)
				{
					Location tloc = GetLocationByDefault(rs);
					if(tloc != null && tloc.Name.IndexOf(city_name)>=0)
						location = tloc;
				}
				if(location == null)
					throw new Exception("cannot fetch location, as location seems not to be existent");
				route.Waypoints.Add(location,i.ToString());
				if(i==0)
					location_start = location;
			}
			location_end = location;

			//calculate and get calculated info
			route.Calculate();
			distance_complete = route.Distance;
			if(st == StepType.LenPerStep)
				step_size /= 1000;
			else
				step_size = distance_complete/step_size;

			if(step_size < MIN_STEP_LEN)
				step_size = MIN_STEP_LEN;

			//interpret directions
			loc_arr.Add(location_start);
			for(int i=0;i<route.Directions.Count;i++){
				index = i+1;
				MapPoint.Direction dir = (MapPoint.Direction)route.Directions.get_Item(ref index);
				elapsedDistance_curr = dir.ElapsedDistance;

				if(elapsedDistance_curr-elapsedDistance_prev > step_size){
					loc_arr.Add(dir.Location);
					elapsedDistance_prev = elapsedDistance_curr;
				}
			}
			loc_arr.Add(location_end);
//			route.Clear();
//			route=null;

//			Line line = new Line();


			//generate the geoObj from loc info and return it, null will be returned if nothing was found
			GeoObject[] geoObj = new GeoObject[loc_arr.Count];
			for(int i=0;i<loc_arr.Count;i++){
				geoObj[i] = GetGeoObjectFromLocation((Location)loc_arr[i]);
				geoObj[i].distance = distance_complete;
				geoObj[i].drivetime = route.DrivingTime;


//				line.Add(new Vector2d(geoObj[i].x,geoObj[i].y));
//				if(i>0)
//					Console.WriteLine("len of "+i+": "+line.GetLineLength(i-1,i));

			}
//			Console.WriteLine("Length of Line: "+line.GetLineLength());
//			Console.WriteLine();

			return geoObj;
		}

		private Location GetLocationByDefault(FindResults rs)
		{
			Location location;
			object index=0;
			for(int i=0; i < rs.Count;i++)
			{
				index = i+1;
				location =  (Location)rs.get_Item(ref index);
				if(location.Type == GeoShowDataBy.geoShowByDefault)
					return location;
			}
			return null;
		}

		private Location GetLocationByRegion3(MapPoint.FindResults rs)
		{
			Location location;
			object index=0;
			for(int i=0; i < rs.Count;i++)
			{
				index = i+1;
				location =  (Location)rs.get_Item(ref index);
				if(location.Type == GeoShowDataBy.geoShowByRegion3)
					return location;
			}
			return null;
		}

		private Location GetLocationByRegion2(MapPoint.FindResults rs)
		{
			Location location;
			object index=0;
			for(int i=0; i < rs.Count;i++)
			{
				index = i+1;
				location =  (Location)rs.get_Item(ref index);
				if(location.Type == GeoShowDataBy.geoShowByRegion2)
					return location;
			}
			return null;
		}

		private Location GetLocationByCity(MapPoint.FindResults rs)
		{
			Location location;
			object index=0;
			for(int i=0; i < rs.Count;i++){
				index = i+1;
				location =  (Location)rs.get_Item(ref index);
				if(location.Type == GeoShowDataBy.geoShowByCity)
					return location;
			}
			return null;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="rs"></param>
		/// <param name="city_order">city_order 2 bedeutet das ich nicht die city die ich im result set finde zur�ckgebe sondern die zweite</param>
		/// <returns></returns>
		private Location GetLocationByCity(MapPoint.FindResults rs,int city_order)
		{
			int curr_city_oder = 1;
			Location location;
			object index=0;
			for(int i=0; i < rs.Count;i++)
			{
				index = i+1;
				location =  (Location)rs.get_Item(ref index);
				if(location.Type == GeoShowDataBy.geoShowByCity)
				{
					if(curr_city_oder == city_order)
						return location;
					else
						curr_city_oder++;						
				}
			}
			return null;
		}

//
//
//		public GeoObject[] GetGeoObjectsOnRoute(string[] route_sarr,double step_len){
//			//check paras in, two waypoint at least necessary
//			if(route_sarr.Length<2)
//				return null;
//
//			//var decl
//			double distance_complete, distance_tohere, distance_error_best=9999999999999, distance_error_here;
//			object index = 1;
//			int curr_loc = 0, curr_step=1, count_route_directions = 0,step_size=0;
//			ArrayList loc_arr;
//			FindResults rs;
//			Location location;
//			Route route = m_app.ActiveMap.ActiveRoute;
//
//			//add waypoints
//			for(int i=0;i<route_sarr.Length;i++){
//				rs = m_app.ActiveMap.FindResults(route_sarr[i]);
//				location =  (Location)rs.get_Item(ref index);
//				route.Waypoints.Add(location,i.ToString());
//			}
//
//			//calculate and get calculated info
//			route.Calculate();
//			count_route_directions = route.Directions.Count-2;
//			distance_complete = route.Distance;
//			step_len = (step_len/1000.0);
//			step_size = Convert.ToInt32(Math.Floor(distance_complete/step_len));
////			if(step_size > count_route_directions){
////				step_size = count_route_directions-1;
////				step_len = distance_complete/step_size;
////			}
//			loc_arr = new ArrayList();
//			loc_arr.Capacity = step_size;
//
//			//interpret directions
//			for(int i=0;i<route.Directions.Count;i++){
//				index = i+1;
//				Direction dir = (Direction)route.Directions.get_Item(ref index);
//				distance_tohere = dir.ElapsedDistance;
//				distance_error_here = ( step_len*(curr_step) )-dir.ElapsedDistance;
//
//				if(distance_error_best  > distance_error_here && distance_error_here > 0){
//					if(distance_error_best-distance_error_here>step_len || (curr_loc==0 && curr_step==1)){
//						distance_error_best = distance_error_here;
//						AddLoc(dir.Location,loc_arr,ref curr_loc);
//					}
//				}else{
//					if(Math.Abs(distance_error_here)<distance_error_best){
//						if(Math.Abs(distance_error_here)+distance_error_best > step_len){
//							loc_arr.Add(dir.Location);
//							curr_loc++;
////						}else{
////							AddLoc(dir.Location,loc_arr,ref curr_loc);
//						}
//						curr_step++;
//						distance_error_best += step_len;//( step_len*(curr_step) )-dir.ElapsedDistance;
//						//curr_loc++;
//					}else{
//						loc_arr.Add(dir.Location);
//						do{
//							if(distance_error_here>0)
//								break;
//							curr_step++;
//							distance_error_here = ( step_len*curr_step )-dir.ElapsedDistance;
//
//						}while(true);
//						distance_error_best = distance_error_here;
//						curr_loc++;
//					}
//				}
//			}
//			
//			route.Clear();
//			route=null;
//
//			Line line = new Line();
//
//
//			//generate the geoObj from loc info and return it, null will be returned if nothing was found
//			GeoObject[] geoObj = new GeoObject[loc_arr.Count];
//			for(int i=0;i<loc_arr.Count;i++){
//				geoObj[i] = GetGeoObjectFromLocation((Location)loc_arr[i]);
//				geoObj[i].distance = distance_complete;
//
//
//				line.Add(new Vector2d(geoObj[i].x,geoObj[i].y));
//				if(i>0)
//					Console.WriteLine("len of "+i+": "+line.GetLineLength(i-1,i));
//
//			}
//			Console.WriteLine("Length of Line: "+line.GetLineLength());
//			Console.WriteLine();
//
//			return geoObj;
//		}

		/// <summary>
		/// calculates a route normally with the specified waypoints, at least two waypoints a needed to calc a route, otherwise null 
		/// will be returned.
		/// der ort der auf halber strecke gefunden wird, wird als geoObj zur�ck gegeben.
		/// </summary>
		/// <param name="route_sarr">waypoints, first string(city) specifies the starting point</param>
		/// <returns>a GeoObject generated by Mappoint</returns>
		public GeoObject GetGeoObjectInMiddleOfRoute(string[] route_sarr){
			//check paras in, two waypoint at least necessary
			if(route_sarr.Length<2)
				return null;

			//var decl
			double distance_complete, distance_tohere, distance_half, distance_error_best=9999999999999, distance_error_here;
			object index = 1;
			FindResults rs;
			Location location, loc_near_middle=null;
			Route route = m_app.ActiveMap.ActiveRoute;
			route.Clear();

			//add waypoints
			for(int i=0;i<route_sarr.Length;i++){
				rs = m_app.ActiveMap.FindResults(route_sarr[i]);
				location =  (Location)rs.get_Item(ref index);
				route.Waypoints.Add(location,i.ToString());
			}

			//calculate and get calculated info
			route.Calculate();
			distance_complete = route.Distance;
			distance_half = distance_complete/2;

			//interpret directions
			for(int i=0;i<route.Directions.Count;i++){
				index = i+1;
				MapPoint.Direction dir = (MapPoint.Direction)route.Directions.get_Item(ref index);
				distance_tohere = dir.ElapsedDistance;
				distance_error_here = Math.Abs(distance_half-dir.ElapsedDistance);
				if(distance_error_best  > distance_error_here){
					distance_error_best = distance_error_here;
					loc_near_middle = dir.Location;
				}
			}
			
			route=null;

			//generate the geoObj from loc info and return it, null will be returned if nothing was found
			GeoObject geoObj = GetGeoObjectFromLocation(loc_near_middle);
			geoObj.distance = distance_complete;
			return geoObj;
		}
		public GeoCoordinates GetGeoLatLong(Location loc_a){
			Location loc, pointX, pointY;
			double myLat, myLong, zLat, zLong;
			double distX,distY,distZ;
			double Measure;
			double prev1_lat=1,prev2_lat=2,prev1_long=3,prev2_long=4;
			object index = 1;
			int x = 0;
			double altitude=0;

			//myLat and myLong are a known point.  In this case, Wichita, Kansas
			myLat = m_graz_lat;
			myLong = m_graz_long;
			//Initially, set zLat and zLong equal to the known point
			zLat = myLat;
			zLong = myLong;
			//Measure is used to adjust the starting distance. In this case, start off with 750 miles.
			//Measure is made smaller in the Do While loop below but it is a good idea to start off
			//with a large number
			Measure = 0.01471 * 750;

			//Create two points: oLoc as the reference point and your address for which you need lat/long
			loc = m_app.ActiveMap.GetLocation(myLat, myLong, altitude);
			//loc_a = (Location)m_app.ActiveMap.FindResults(location_s).get_Item(ref index);
			//m_app.ActiveMap.AddPushpin(loc_a, "Push1");

			//Create a loop that will continue until your desired precision.  As indicated below
			//this loop will repeat until the lat/long is found to be within 700 feet.
			//0.00195018939 --> This value is determined using the following formula:
			//   .01471/5280*Number of Feet for Precision
			//  Ex:  .01471 / 5290 * 20 = .00005572
			//  Trivia: There are 5,280 feet in a mile.
			while(Measure>0.00195018939){
				if(prev1_lat == prev2_lat && prev1_lat == zLat &&
				   prev1_long == prev2_long && prev1_long == zLong	)
					break;
				prev2_lat = prev1_lat; prev2_long = prev1_long; 
				prev1_lat = zLat; prev1_long = zLong;
				x++;

				//Create two other reference points: PointX is one Measure off oLoc's Lattitude
				//                                   PointY is one Measure off oLoc's Longitude
				pointX = m_app.ActiveMap.GetLocation( (zLat+Measure), zLong, altitude);
				pointY = m_app.ActiveMap.GetLocation(zLat, zLong + Measure, altitude);

				//Measure the distances from each of the three reference points to our main address (oLocA)
				distX = loc_a.DistanceTo(pointX);
				distY = loc_a.DistanceTo(pointY);
				distZ = loc_a.DistanceTo(loc);

				//Determine which reference point is closer to oLocA, our main address
				if(distX < distY && distX < distZ){
					//Make the master reference point oLoc equal to PointX since PointX was the closest to
					//our main address
					loc = m_app.ActiveMap.GetLocation(zLat + Measure, zLong, altitude);
					//Uncomment the next line if you want to see how the algorithm found the lat/long
					//m_app.ActiveMap.AddPushpin(m_app.ActiveMap.GetLocation(zLat + Measure, zLong, altitude), x.ToString());
					zLat = zLat + Measure;  //Don't forget to add the Measure to zLat for the next iteration
				}
				if(distY < distX && distY < distZ){
					loc = m_app.ActiveMap.GetLocation(zLat, zLong + Measure, altitude);
					zLong = zLong + Measure;
					//Set oPush = oMap.AddPushpin(oMap.GetLocation(zLat, zLong + Measure), x)
				}
				if( distZ < distX && distZ < distY ){
					//The main reference point is closer than PointX or PointY.
					//PointX and PointY were too far away by ADDING a Measure, so here we need
					//to subtract a measure from both the Latitude and the Longitude
					loc = m_app.ActiveMap.GetLocation(zLat - Measure, zLong - Measure, altitude);
					zLat = zLat - Measure;
					zLong = zLong - Measure;
					//Set oPush = oMap.AddPushpin(oMap.GetLocation(zLat - Measure, zLong - Measure), x)
				}

				//Here is where Measure gets adjusted.  Check to see if the distance between the new
				//reference point is smaller than Measure.  If so, reduce Measure by half.
				//Don't forget that Measure is in degrees of Lat/Long while the distance will be
				//in miles.  To convert degrees to miles, multiply by .01471, the number of degrees
				//in a mile.
				if( loc_a.DistanceTo(loc) < (Measure / 0.01471) )
					Measure = Measure / 2;
			}
			GeoCoordinates geo = new GeoCoordinates(zLat,zLong);
			loc = null;
			loc_a = null;
			pointX = null;
			pointY = null;
			return geo;
			//			Console.WriteLine("The Lat/Long of your address is " + zLat + ", " + zLong);
			//			Console.WriteLine("It was found in " + x + " iterations.");
		}

		public GeoCoordinates GetGeoLatLong(string location_s){
			object index = 1;
			return GetGeoLatLong((Location)m_app.ActiveMap.FindResults(location_s).get_Item(ref index));
		}











/************************
 * *******************
 * ROUTE DETECTION AND POLY CREATION
 * ************************************/

//		public static Polygon.PolygonType CreateRouteAndSurroundingPolygon(){
//		
//		}


        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="start_lat"></param>
        ///// <param name="start_lon"></param>
        ///// <param name="end_lat"></param>
        ///// <param name="end_lon"></param>
        ///// <param name="city_from"></param>
        ///// <param name="city_to"></param>
        ///// <param name="country_from"></param>
        ///// <param name="country_to"></param>
        ///// <param name="distance_vertices_in_routeline">abstand der vertexe in der route selbst in PIXEL vorgeschlagen wird hier [3 - 5] Pixel abh�ngig von der altitude des gemachten images</param>
        ///// <param name="margin_polygon_around_routeline">abstand poly around routeline, dieser wert ist wieder in KM angegeben</param>
        ///// <param name="raster_count_interpolationlist">f�r die umrechnugn von pixel auf lat lon wird eine lat lon list erstellt rastercount gibt an wie gros diese matrix sein soll. vorgeschlagen wird !!  [5]</param>
        ///// <param name="routeline_latlon"></param>
        ///// <param name="poly_aroundline_latlon"></param>
        ///// <returns>gibt an umwelche art von polygon es sich bei dem erstellen poly around line handelt, grunds�tzlich ist m�glich greinerhorman oder convexhull</
        //public Polygon.PolygonType CreateRouteAndSurroundingPolygon(
        //    double start_lat,
        //    double start_lon,
        //    double end_lat,
        //    double end_lon,
        //    int distance_vertices_in_routeline,
        //    int margin_polygon_around_routeline_in_KM,
        //    int raster_count_interpolationlist,
        //    int poly_rationalisierungsfaktor,
        //    int poly_rationalisierungsrounds,
        //    int route_rationalizationfactor,
        //    int max_vertices_in_routeline_per_10km,
        //    out Line routeline_latlon,
        //    out Polygon poly_aroundline_latlon,
        //    out Line routeline_pixel,
        //    out Polygon poly_aroundline_pixel,
        //    out double exact_distance,
        //    out double exact_drivetime
        //    )
        //{

        //    Point start_point,end_point;
        //    int bmp_width,bmp_height;
        //    double size_per_pixel;
        //    Polygon.PolygonType pt;
        //    ArrayList x_interpolationraster,y_interpolationraster;


        //    routeline_pixel = DiscretizeRoute(  start_lon,
        //                                        start_lat,
        //                                        end_lon,
        //                                        end_lat,
        //                                        distance_vertices_in_routeline,
        //                                        route_rationalizationfactor,
        //                                        max_vertices_in_routeline_per_10km,
        //                                        raster_count_interpolationlist,
        //                                        out start_point,
        //                                        out end_point,
        //                                        out bmp_width,
        //                                        out bmp_height,
        //                                        out size_per_pixel,
        //                                        out x_interpolationraster,
        //                                        out y_interpolationraster,
        //                                        out exact_distance,
        //                                        out exact_drivetime);

        //    pt = CreateSurroundingPolygon(	margin_polygon_around_routeline_in_KM,
        //                                    size_per_pixel,
        //                                    routeline_pixel,
        //                                    poly_rationalisierungsfaktor,
        //                                    poly_rationalisierungsrounds,
        //                                    out poly_aroundline_pixel);


        //    TransformPixelToGeoCoords(  x_interpolationraster,
        //                                y_interpolationraster,
        //                                routeline_pixel,
        //                                poly_aroundline_pixel,
        //                                start_point,
        //                                end_point,
        //                                bmp_width,
        //                                bmp_height,
        //                                out routeline_latlon,
        //                                out poly_aroundline_latlon);

        //    return pt;
        //}

        //public static void TransformPixelToGeoCoords(ArrayList x_interpolationraster,ArrayList y_interpolationraster,Line routeline_pixel,Polygon poly_aroundline_pixel,Point start_point,Point end_point,int bmp_width,int bmp_height,out Line routeline_latlon,out Polygon poly_aroundline_latlon)
        //{
        //    //ArrayList v_x,v_y;
        //    poly_aroundline_latlon = new Polygon();
        //    routeline_latlon = new Line();

        //    for(int i=0;i<routeline_pixel.Length;i++)
        //    {
        //        Vector2d v1 = routeline_pixel[i];
        //        double lon = GetInterpoliert(v1.X,x_interpolationraster);
        //        double lat = GetInterpoliert(v1.Y,y_interpolationraster);
        //        Vector2d v1_latlon = new Vector2d(lon,lat);
        //        routeline_latlon.Add(v1_latlon);
        //    }

        //    for(int i=0;i<poly_aroundline_pixel.Length;i++)
        //    {
        //        Vector2d v1 = poly_aroundline_pixel[i];
        //        double lon = GetInterpoliert(v1.X,x_interpolationraster);
        //        double lat = GetInterpoliert(v1.Y,y_interpolationraster);
        //        Vector2d v1_latlon = new Vector2d(lon,lat);
        //        poly_aroundline_latlon.Add(v1_latlon);
        //    }
        //}

        //public static Polygon.PolygonType CreateSurroundingPolygon(int margin_polygon_around_routeline_in_KM,double size_per_pixel,Line routeline_pixel,int rationalisierungsfaktor,int rationalisierungsrounds,out Polygon poly_aroundline_pixel)
        //{

        //    Polygon.PolygonType pt;

        //    //umrechnung km in pixel margin
        //    int margin_polygon_around_routeline_in_PIXEL = Convert.ToInt32(Math.Ceiling(margin_polygon_around_routeline_in_KM / size_per_pixel));

        //    //erstelle das polygon auf pixel ebene
        //    poly_aroundline_pixel = routeline_pixel.GetPolygonAroundLine(margin_polygon_around_routeline_in_PIXEL,out pt);
        //    if(pt != Polygon.PolygonType.ConvexHull && poly_aroundline_pixel.Length > 100)
        //    {
        //        poly_aroundline_pixel.Rationalize(rationalisierungsfaktor,rationalisierungsrounds);
        //        Polygon p_temp = PolygonUtils.CreatePolyAroundEdge(new Edge(routeline_pixel[routeline_pixel.Length-2],routeline_pixel[routeline_pixel.Length-1]),margin_polygon_around_routeline_in_PIXEL);
        //        PolygonUtils pu = new PolygonUtils();
        //        poly_aroundline_pixel = pu.Union(poly_aroundline_pixel,p_temp);
        //    }

        //    return pt;

        //}

        ///// <summary>
        ///// legt �ber die gr�ne routen linie einen pixelraster und verbindet die n�hesten vertexe. dadruch bekomme
        ///// ich eine route diskretisiert
        ///// </summary>
        ///// <param name="start_lon"></param>
        ///// <param name="start_lat"></param>
        ///// <param name="end_lon"></param>
        ///// <param name="end_lat"></param>
        ///// <param name="city_from"></param>
        ///// <param name="city_to"></param>
        ///// <param name="country_from"></param>
        ///// <param name="country_to"></param>
        ///// <param name="distance_vertices_in_routeline"></param>
        ///// <param name="start_point"></param>
        ///// <param name="end_point"></param>
        ///// <returns></returns>
        //public Line DiscretizeRoute(double start_lon, double start_lat, double end_lon, double end_lat,int distance_vertices_in_routeline,int route_rationalizationfactor,int max_vertices_in_routeline_per_10km,int raster_count_interpolationlist,out Point start_point,out Point end_point,out int bmp_width,out int bmp_height,out double size_per_pixel,out ArrayList x_interpolationraster,out ArrayList y_interpolationraster,out double exact_distance,out double exact_drivetime)
        //{

        //    Line routeline_pixel = new Line();
        //    uint c5 = 0xFF000000;
        //    uint c_route = 0xFF00CF00;
        //    uint c_route2 = 0xFF007800;
        //    uint c_route3 = 0xFF006600;
        //    uint c_route4 = 0xFF00E400;
        //    uint c_green = 0xFF00FF00;
        //    uint c_red = 0xFFFF0000;
        //    start_point = new Point(0,0);
        //    end_point=new Point(0,0);

			
        //    MapPoint.Location location_from = Application.ActiveMap.GetLocation(start_lat,start_lon,0);
        //    MapPoint.Location location_to = Application.ActiveMap.GetLocation(end_lat,end_lon,0);

        //    MapPoint.Route route;
        //    route = Application.ActiveMap.ActiveRoute;
        //    route.Clear();
        //    route.Waypoints.Add(location_from,"1");
        //    route.Waypoints.Add(location_to,"2");

        //    //hier kann es sein das eine com exception geworfen wird
        //    route.Calculate();

        //    //m_app.ItineraryVisible = false;

        //    exact_distance = route.Distance;
        //    exact_drivetime = route.DrivingTime;

        //    System.Drawing.Bitmap bmp =  GetImageOfCurrentMap();
        //    bmp_width = bmp.Width;
        //    bmp_height = bmp.Height;			
			
        //    size_per_pixel = Application.ActiveMap.PixelSize;
        //    if(MapPoint.GeoUnits.geoKm != Application.Units)
        //        size_per_pixel *= 1.6;

        //    for(int y=0;y<bmp_height;y++)
        //        for(int x=0;x<bmp_width;x++)
        //        {
        //            Color color = bmp.GetPixel(x,y);
        //            int rgb = color.ToArgb();

        //            if(rgb == (int)c_green)
        //            {

        //                if(x+2<bmp_width-1 && y+1<bmp_height-1)
        //                {
        //                    int x1,x2,x3,x4,x5,x6,x7,x8;
        //                    x1 = bmp.GetPixel(x+1,y).ToArgb();
        //                    x2 = bmp.GetPixel(x+2,y).ToArgb();
							
        //                    x3 = bmp.GetPixel(x,y+1).ToArgb();
        //                    x4 = bmp.GetPixel(x+1,y+1).ToArgb();
        //                    x5 = bmp.GetPixel(x+2,y+1).ToArgb();

        //                    x6 = bmp.GetPixel(x,y+2).ToArgb();
        //                    x7 = bmp.GetPixel(x+1,y+2).ToArgb();
        //                    x8 = bmp.GetPixel(x+2,y+2).ToArgb();
							

        //                    if(x1 == (int)c_green && x2 == (int)c_green && x3 == (int)c_green && x4 == (int)c5 && x5 == (int)c_green && x6 == (int)c_green && x7 == (int)c_green && x8 == (int)c_green)
        //                    {
        //                        bmp.SetPixel(x,y,Color.FromArgb((int)c_route));
        //                        bmp.SetPixel(x+1,y,Color.FromArgb((int)c_route));
        //                        bmp.SetPixel(x+2,y,Color.FromArgb((int)c_route));
								
        //                        bmp.SetPixel(x,y+1,Color.FromArgb((int)c_route));
        //                        bmp.SetPixel(x+1,y+1,Color.FromArgb((int)c_route));
        //                        bmp.SetPixel(x+2,y+1,Color.FromArgb((int)c_route));

        //                        bmp.SetPixel(x,y+2,Color.FromArgb((int)c_route));
        //                        bmp.SetPixel(x+1,y+2,Color.FromArgb((int)c_route));
        //                        bmp.SetPixel(x+2,y+2,Color.FromArgb((int)c_route));

        //                        rgb = (int)c_route;
        //                        start_point = new Point(x+1,y+1);
        //                    }
        //                }						
        //            }

        //            if(rgb == (int)c_red)
        //            {
        //                if(x+2<bmp_width-1 && y+1<bmp_height-1)
        //                {
        //                    int x1,x2,x3,x4,x5,x6,x7,x8;
        //                    x1 = bmp.GetPixel(x+1,y).ToArgb();
        //                    x2 = bmp.GetPixel(x+2,y).ToArgb();
							
        //                    x3 = bmp.GetPixel(x,y+1).ToArgb();
        //                    x4 = bmp.GetPixel(x+1,y+1).ToArgb();
        //                    x5 = bmp.GetPixel(x+2,y+1).ToArgb();

        //                    x6 = bmp.GetPixel(x,y+2).ToArgb();
        //                    x7 = bmp.GetPixel(x+1,y+2).ToArgb();
        //                    x8 = bmp.GetPixel(x+2,y+2).ToArgb();
							

        //                    if(x1 == (int)c_red && x2 == (int)c_red && x3 == (int)c_red && x4 == (int)c5 && x5 == (int)c_red && x6 == (int)c_red && x7 == (int)c_red && x8 == (int)c_red)
        //                    {
        //                        bmp.SetPixel(x,y,Color.FromArgb((int)c_route));
        //                        bmp.SetPixel(x+1,y,Color.FromArgb((int)c_route));
        //                        bmp.SetPixel(x+2,y,Color.FromArgb((int)c_route));
								
        //                        bmp.SetPixel(x,y+1,Color.FromArgb((int)c_route));
        //                        bmp.SetPixel(x+1,y+1,Color.FromArgb((int)c_route));
        //                        bmp.SetPixel(x+2,y+1,Color.FromArgb((int)c_route));

        //                        bmp.SetPixel(x,y+2,Color.FromArgb((int)c_route));
        //                        bmp.SetPixel(x+1,y+2,Color.FromArgb((int)c_route));
        //                        bmp.SetPixel(x+2,y+2,Color.FromArgb((int)c_route));

        //                        rgb = (int)c_route;
        //                        end_point = new Point(x+1,y+1);
        //                    }
        //                }						
        //            }
        //        }


        //    //erstelle die interpolations liste f�r die umrechnugn von pixel auf lat lon
        //    CreateInterpolationList(raster_count_interpolationlist,start_point,end_point,bmp_width,bmp_height,out x_interpolationraster,out y_interpolationraster);

			
        //    //leg einen raster 
        //    double d_loops_x = Math.Floor((double)(bmp_width / distance_vertices_in_routeline));
        //    int loops_x = Convert.ToInt32(d_loops_x);
        //    double d_loops_y = Math.Floor((double)(bmp_height / distance_vertices_in_routeline));
        //    int loops_y = Convert.ToInt32(d_loops_y);

        //    int xc=0,yc=0;

        //    ArrayList arrl_vertices = new ArrayList();
        //    arrl_vertices.Add(start_point);
        //    arrl_vertices.Add(end_point);

        //    for(int scanline_y=0;scanline_y<loops_y;scanline_y++)
        //    {
        //        yc = scanline_y*distance_vertices_in_routeline;
        //        bool scan_outside = true;
        //        int start_x = 0;
        //        int end_x = 0;

        //        //scan horizontal
        //        for(xc=0;xc<bmp_width;xc++)
        //        {
        //            Color color = bmp.GetPixel(xc,yc);
        //            int rgb = color.ToArgb();

        //            if(scan_outside)
        //            {
        //                if(rgb==(int)c_route || rgb==(int)c_route2|| rgb==(int)c_route3|| rgb==(int)c_route4)
        //                {
        //                    start_x = xc;
        //                    scan_outside=false;
        //                }
        //                else
        //                {
        //                    continue;
        //                }
        //            }
        //            else
        //            {
        //                if(rgb==(int)c_route || rgb==(int)c_route2|| rgb==(int)c_route3|| rgb==(int)c_route4)
        //                {
        //                    continue;
        //                }
        //                else
        //                {
        //                    end_x = xc;
        //                    double d_middle = Math.Floor((double)(end_x - start_x) / 2);
        //                    int middle = Convert.ToInt32(d_middle);
        //                    scan_outside=true;
        //                    if(middle>3)
        //                    {
        //                        arrl_vertices.Add(new Point(start_x+middle,yc));
        //                        bmp.SetPixel(start_x+middle,yc,Color.Magenta);
        //                    }
        //                }
        //            }
        //        }
        //    }

        //    for(int scanline_x=0;scanline_x<loops_x;scanline_x++)
        //    {
        //        xc = scanline_x*distance_vertices_in_routeline;
        //        bool scan_outside = true;
        //        //scan vertical
        //        int start_y = 0;
        //        int end_y = 0;

        //        for(yc=0;yc<bmp_height;yc++)
        //        {
        //            Color color = bmp.GetPixel(xc,yc);
        //            int rgb = color.ToArgb();

        //            if(scan_outside)
        //            {
        //                if(rgb==(int)c_route || rgb==(int)c_route2|| rgb==(int)c_route3|| rgb==(int)c_route4)
        //                {
        //                    start_y = yc;
        //                    scan_outside=false;
        //                }
        //                else
        //                {
        //                    continue;
        //                }
        //            }
        //            else
        //            {
        //                if(rgb==(int)c_route || rgb==(int)c_route2|| rgb==(int)c_route3|| rgb==(int)c_route4)
        //                {
        //                    continue;
        //                }
        //                else
        //                {
        //                    end_y = yc;
        //                    double d_middle = Math.Floor((double)(end_y - start_y) / 2);
        //                    int middle = Convert.ToInt32(d_middle);
        //                    scan_outside=true;
        //                    if(middle>3)
        //                    {
        //                        arrl_vertices.Add(new Point(xc,start_y+middle));
        //                        bmp.SetPixel(xc,start_y+middle,Color.Blue);
        //                    }
        //                }
        //            }
        //        }
        //    }

        //    //erstelle das sortier arraylist
        //    ArrayList ordered_vertices = new ArrayList();
        //    for(int i=1;i<arrl_vertices.Count;i++)
        //    {
        //        object[] objarr = new object[2];
        //        objarr[0] = arrl_vertices[i];
        //        objarr[1] = arrl_vertices[0];
        //        ordered_vertices.Add(objarr);
        //    }
        //    ArrayList line = new ArrayList();
        //    line.Add(arrl_vertices[0]);
        //    while(true)
        //    {
        //        ordered_vertices.Sort(new LowestDistanceSorter());
        //        object[] objarr = (object[])ordered_vertices[0];
        //        Point curr = (Point)objarr[1];
        //        Point next = (Point)objarr[0];
        //        line.Add(next);
        //        ordered_vertices.RemoveAt(0);
        //        if(next == end_point)
        //            break;
        //        for(int i=0;i<ordered_vertices.Count;i++)
        //        {
        //            objarr = (object[])ordered_vertices[i];
        //            objarr[1] = next;
        //            ordered_vertices[i] = objarr;
        //        }
        //    }

        //    //erstelle die line auf pixel ebene
        //    routeline_pixel.Add(new Vector2d(start_point.X,start_point.Y));
        //    for(int i=1;i<line.Count;i+=2)
        //    {
        //        Point p = (Point)line[i];
        //        routeline_pixel.Add(new Vector2d(p.X,p.Y));
        //    }
        //    Vector2d last_v = new Vector2d(end_point.X,end_point.Y);
        //    if(routeline_pixel[routeline_pixel.Length-1] != last_v && routeline_pixel[routeline_pixel.Length-2] != last_v)
        //        routeline_pixel.Add(last_v);


        //    int min_vertices = 20;
        //    int max_vertices = 120;
        //    int max_vertices_in_routeline = Convert.ToInt32(Math.Round(max_vertices_in_routeline_per_10km * exact_distance / 10));
        //    if(max_vertices_in_routeline < min_vertices)
        //        max_vertices_in_routeline = min_vertices;
        //    if(max_vertices_in_routeline > max_vertices)
        //        max_vertices_in_routeline = max_vertices;

        //    while(routeline_pixel.Length>max_vertices_in_routeline)
        //    {
        //        routeline_pixel.Rationalize(route_rationalizationfactor,1);
        //    }

        //    return routeline_pixel;
        //}

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="raster_count">�ber das gesamt bmp werden raste_count vertices diagonal angeordnet, daraus werden dann die lat lons per prozent berechnet
        ///// je gr�sser die rasterung des genauer die px to lat lon umrechnung</param>
        ///// <param name="width"></param>
        ///// <param name="height"></param>
        ///// <param name="v_x"></param>
        ///// <param name="v_y"></param>
        //private void CreateInterpolationList(int raster_count, Point start_point,Point end_point,int width, int height, out ArrayList v_x, out ArrayList v_y)
        //{
        //    MapPoint.Location loc_of_xy;
        //    GeoCoordinates geo_of_xy;

        //    int margin_x = 100;
        //    int margin_y = 50;

        //    int diff_x = width-margin_x-margin_x;
        //    int diff_y = height-margin_y-margin_y;

        //    int part_x = diff_x/(raster_count-1);
        //    int part_y = diff_y/(raster_count-1);
						
        //    Point[] xy=  new Point[raster_count];
        //    xy[0] = new Point(margin_x,margin_y);
        //    for(int i=1;i<raster_count-1;i++)
        //    {
        //        xy[i] = new Point(margin_x + part_x*i, margin_y + part_y*i);
        //    }
        //    xy[xy.Length-1] = new Point(width-margin_x,height-margin_y);

        //    v_x = new ArrayList();
        //    for(int i=0;i<raster_count;i++)
        //    {
        //        loc_of_xy = Application.ActiveMap.XYToLocation(xy[i].X,height/2);
        //        geo_of_xy = GetGeoLatLong(loc_of_xy);
        //        v_x.Add(new Vector2d(xy[i].X,geo_of_xy.Longitude));
				
        //    }

        //    v_y = new ArrayList();
        //    for(int i=0;i<raster_count;i++)
        //    {
        //        loc_of_xy = Application.ActiveMap.XYToLocation(width/2,xy[i].Y);
        //        geo_of_xy = GetGeoLatLong(loc_of_xy);
        //        v_y.Add(new Vector2d(xy[i].Y,geo_of_xy.Latitude));
				
        //    }

        //    v_x.Sort(new Vector2dSortXAsc());
        //    v_y.Sort(new Vector2dSortXAsc());


        //}

		private static double GetInterpoliert(double x_in, ArrayList stutzstellen)
		{
			double ystart=((Vector2d)stutzstellen[stutzstellen.Count-2]).Y;
			double yend=((Vector2d)stutzstellen[stutzstellen.Count-1]).Y;
			double xstart=((Vector2d)stutzstellen[stutzstellen.Count-2]).X;
			double xend=((Vector2d)stutzstellen[stutzstellen.Count-1]).X;

			for(int i=0;i<stutzstellen.Count-1;i++)
			{
				Vector2d st = (Vector2d)stutzstellen[i];
				Vector2d st_next = (Vector2d)stutzstellen[i+1];
				if(x_in < st.X)
				{
					ystart = st.Y;
					yend  = st_next.Y;
					xstart = st.X;
					xend = st_next.X;
					break;
				}
			}

			//draw stuff with mapx
			double xdiff = xend-xstart;
			double ydiff = yend-ystart;

			double diff_curr = x_in-xstart;
				
			double proz_x = (100*diff_curr)/xdiff;
			double interpoliert = (ydiff * proz_x)/100;

			return interpoliert+ystart;
		}

		private class LowestDistanceSorter: IComparer  
		{
			int IComparer.Compare( object obj1, object obj2 )  
			{
				object[] obj_arr1 = (object[])obj1;
				object[] obj_arr2 = (object[])obj2;

				Point root = (Point)obj_arr1[1];
				Point p1 = (Point)obj_arr1[0];
				Point p2 = (Point)obj_arr2[0];
				
				Point diff_p1 = new Point(p1.X-root.X,p1.Y-root.Y);
				Point diff_p2 = new Point(p2.X-root.X,p2.Y-root.Y);
				
				double len_diff_p1 = Math.Sqrt(diff_p1.X*diff_p1.X+diff_p1.Y*diff_p1.Y);
				double len_diff_p2 = Math.Sqrt(diff_p2.X*diff_p2.X+diff_p2.Y*diff_p2.Y);

				if(len_diff_p1 < len_diff_p2)
					return -1;
				else if(len_diff_p1 == len_diff_p2)
					return 0;
				else
					return 1;
			}
		}
	}
}







//
//
//
//
//
//		public void getTimeZone()
//		{
//			Console.WriteLine("testing da timezone");
//			//string place = "Wels";
//			//Find the locatoin first
//			//			MapPoint.FindResults findResults = axMappointControl1.ActiveMap.FindResults(place);
//			//			if(findResults != null && findResults.Count > 0){
//			//				object index = 1;
//			//				MapPoint.Location location = findResults.get_Item(ref index) as MapPoint.Location;
//			//				//Zoom into it
//			//				location.GoTo();
//			//				//Set low altitudes
//			//				axMappointControl1.ActiveMap.Altitude = 2;
//			//				//Now get points from the location
//			//				MapPoint.FindResults points = axMappointControl1.ActiveMap.ObjectsFromPoint (
//			//															axMappointControl1.ActiveMap.LocationToX(location),
//			//															axMappointControl1.ActiveMap.LocationToY(location)
//			//																							);
//			//				if(points != null && points.Count > 0)
//			//				{
//			//					for(int i=1;i<=points.Count;i++)
//			//					{
//			//						object index2 = i;
//			//						//Get location
//			//						MapPoint.Location loc
//			//							= points.get_Item(ref index2) as MapPoint.Location;
//			//
//			//						//Look for GMT in the name of the location
//			//						if(loc.Name.IndexOf("GMT") > 0)
//			//						{
//			//							MessageBox.Show(loc.Name);
//			//							break;
//			//						}
//			//					}
//			//				}
//			//			}
//		}
//
//		public void searchTest(string city)
//		{
//		
//			//Define an application instance
//			ApplicationClass app = null;
//
//			//Define a location instance
//			Location location = null;
//
//			//Define a FindResults instance
//			FindResults frs = null;
//
//			try
//
//			{
//				//Create an application class
//				app = new ApplicationClass();
//
//				//Now get the location
//				//frs = app.ActiveMap.FindAddressResults(string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, GeoCountry.geoCountryAustria);
//				frs = app.ActiveMap.FindResults("Austria");//(string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, GeoCountry.geoCountryAustria);
//				
//
//				//Check if the find query is succesfull
//				if(frs != null && frs.Count > 0)
//				{
//					for(int i=1;i<=frs.Count;i++)
//					{
//						object index = i;
//						location = frs.get_Item(ref index) as Location;
//
//						//Male the MapPoint 2004 application visible
//						//and go to that location
//						//app.Visible = true;
//						//location.GoTo();
//
//						//Do your processing with the location
//						//MessageBox.Show(location.StreetAddress.Value);
//						Console.Out.WriteLine(location.Name);
//					}
//				}
//				else
//				{
//					Console.Out.WriteLine("MessageCount was null");
//				}
//			}
//			catch(Exception ex)
//			{
//				string message = ex.Message;
//				Console.Out.WriteLine("error: "+message);
//			}
//			finally
//			{
//				if(app != null)
//				{
//					try
//					{
//						app.Quit();
//					}
//					catch
//					{
//						//This means your app has already quit!
//					}
//					finally
//					{
//						app = null;
//					}
//				}
//			}		
//		}
//
//		public void exportTest(string country)
//		{
//			//			if(country == "Austria"){
//			//			}else if(country == "Sweden"){
//			//			}
//
//			//geoCountryAustria Austria
//			//geoCountryBelgium Belgium
//			//geoCountryDenmark Denmark
//			//geoCountryFinland Finland
//			//geoCountryFrance France
//			//geoCountryGermany Germany
//			//geoCountryItaly Italy
//			//geoCountryLuxembourg Luxembourg
//			//geoCountryNetherlands Netherlands
//			//geoCountryNorway Norway
//			//geoCountryPortugal Portugal
//			//geoCountrySpain Spain
//			//geoCountrySweden Sweden
//			//geoCountrySwitzerland Switzerland
//			//geoCountryUnitedKingdom United Kingdom
//
//
//			//Define an application instance
//			ApplicationClass app = null;
//			//Define a location instance
//			Location location = null;
//			//Define a FindResults instance
//			FindResults frs = null;
//
//			try
//			{
//				//Create an application class
//				app = new ApplicationClass();
//
//				for(int i=1000;i<5000;i++)
//				{
//					//frs = app.ActiveMap.FindAddressResults(string.Empty, string.Empty, string.Empty, string.Empty, i.ToString(), GeoCountry.geoCountryAustria);
//					frs = app.ActiveMap.FindResults(i+",Austria");
//					//Check if the find query is succesfull
//					if(frs != null && frs.Count > 0)
//					{
//						for(int j=1;j<=frs.Count;j++)
//						{
//							object index = j;
//							//							location = (Location)frs.get_Item(ref index);
//							//							app.Visible = true;
//							//							location.GoTo();
//							//							Console.Out.WriteLine("PLZ["+i+"] "+location.StreetAddress.City);
//							location = (Location)frs.get_Item(ref index);
//							Console.Out.WriteLine("PLZ["+i+"] "+location.Name);
//						}
//					}
//					else
//					{
//						//Console.Out.WriteLine("MessageCount was null for plz "+i);
//					}
//
//
//				}
//
//			}
//			catch(Exception ex)
//			{
//				string message = ex.Message;
//				Console.Out.WriteLine("error: "+message);
//			}
//			finally
//			{
//				if(app != null)
//				{
//					try
//					{
//						app.Quit();
//					}
//					catch
//					{
//						//This means your app has already quit!
//					}
//					finally
//					{
//						app = null;
//					}
//				}
//			}		
//		}
//

