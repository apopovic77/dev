﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace Logicx.WpfUtility.CustomControls.Validation
{
    /// <summary>
    /// Implements event arguments for custom validation event
    /// </summary>
    public class CustomValidationEventArgs : RoutedEventArgs
    {
        #region construction and initialization
        /// <summary>
        /// Constructor of the class
        /// </summary>
        public CustomValidationEventArgs()
        {
            base.RoutedEvent = ErrorProvider.CustomValidationEvent;
            _result = new ValidationResult(true, "");
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets/Sets the validation result
        /// </summary>
        public ValidationResult Result
        {
            get { return _result; }
            set { _result = value; }
        }
        #endregion

        #region Attribs
        private ValidationResult _result = null;
        #endregion
    }
}
