﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Logicx.WpfUtility.CustomControls.Combobox;
using Logicx.WpfUtility.WindowManagement.Adorners;
using Logicx.WpfUtility.WpfHelpers;

namespace Logicx.WpfUtility.WindowManagement
{
    public class OptionsEventArgs : EventArgs
    {
        public OptionsEventArgs(string option_name)
        {
            OptionName = option_name;
        }

        public string OptionName;
    }

    /// <summary>
    /// Interaction logic for WindowTitleBar.xaml
    /// </summary>
    public partial class WindowTitleBar : UserControl
    {
        public WindowTitleBar()
        {
            InitializeComponent();

            searchfield_stackpanel.MouseDown += new System.Windows.Input.MouseButtonEventHandler(searchfield_stackpanel_MouseDown);

            Loaded += new RoutedEventHandler(WindowTitleBar_Loaded);
            SizeChanged += new SizeChangedEventHandler(WindowTitleBar_SizeChanged);
        }

        void WindowTitleBar_Loaded(object sender, RoutedEventArgs e)
        {
            ActiveStyleAdornerManager.Instance.Register(options_button, BaseFrame.AdornerActiveRenderBrush, BaseFrame.AdornerActiveRenderPen);
            ActiveStyleAdornerManager.Instance.Register(minimize_button, BaseFrame.AdornerActiveRenderBrush, BaseFrame.AdornerActiveRenderPen);
            ActiveStyleAdornerManager.Instance.Register(maximize_button, BaseFrame.AdornerActiveRenderBrush, BaseFrame.AdornerActiveRenderPen);
            ActiveStyleAdornerManager.Instance.Register(maximize_widthtb_button, BaseFrame.AdornerActiveRenderBrush, BaseFrame.AdornerActiveRenderPen);
            ActiveStyleAdornerManager.Instance.Register(close_button, BaseFrame.AdornerActiveRenderBrush, BaseFrame.AdornerActiveRenderPen);
            ActiveStyleAdornerManager.Instance.Register(search_button, BaseFrame.AdornerActiveRenderBrush, BaseFrame.AdornerActiveRenderPen);

            ClearUnusedSeparators();
        }

        public void AddTitle(UIElement title_elem, HorizontalAlignment horizontal_alignment)
        {
            if(horizontal_alignment == HorizontalAlignment.Left)
                title_placeholder_left.Children.Add(title_elem);
            else
                title_placeholder_center.Children.Add(title_elem);
        }
      
        public void AddOptionMenuItem(string name, string text, UIElement icon)
        {
            AddOptionMenuItem(name, text, text, icon);
        }

        public void AddOptionMenuItem(string name, string text, string tooltip, UIElement icon)
        {
            MenuItem mi = null;
            foreach (var item in options_menu.Items)
            {
                if (!(item is MenuItem))
                    continue;
                MenuItem menu_item = (MenuItem)item;
                if(menu_item.Tag == name)
                {
                    mi = menu_item;
                    break;
                }
            }

            if (mi == null)
            {
                mi = new MenuItem();
                mi.Tag = name;
                options_menu.Items.Insert(0, mi);
                mi.Click += new RoutedEventHandler(mi_Click);

            }

            mi.Header = new TextBlock() { Text = text, VerticalAlignment = VerticalAlignment.Center, HorizontalAlignment = System.Windows.HorizontalAlignment.Left, TextAlignment = TextAlignment.Left };
            mi.ToolTip = tooltip;
            mi.Icon = icon;
            mi.Visibility = Visibility.Visible;

            ClearUnusedSeparators();
        }


        

        public void AddOptionMenuSeparator()
        {
            options_menu.Items.Insert(0, new Separator());
        }
    

        public void HideOptionMenuItem(string name)
        {
            foreach (var item in options_menu.Items)
            {
                if (!(item is MenuItem))
                    continue;

                MenuItem menu_item = (MenuItem)item;
                if (menu_item.Tag == name)
                {
                    menu_item.Visibility = Visibility.Collapsed;
                }
            }
            
            ClearUnusedSeparators();
        }

        private void ClearUnusedSeparators()
        {
            Separator first_sap = null;            
            bool found_visible_elements = false;
            bool found_elements = false;
            for (int i = 0; i < options_menu.Items.Count; i++)
            {
                if (options_menu.Items[i] is Separator)
                {
                    if (first_sap == null)
                    {
                        if (i != 0)
                        {
                            first_sap = (Separator) options_menu.Items[i];
                            first_sap.Visibility = Visibility.Visible;
                            found_visible_elements = false;
                            found_elements = false;
                        }
                        else
                        {
                            ((Separator)options_menu.Items[i]).Visibility = Visibility.Collapsed;
                        }
                    }
                    else
                    {
                        if (!found_visible_elements || !found_elements)
                        {
                            ((Separator)options_menu.Items[i]).Visibility = Visibility.Collapsed;
                            found_visible_elements = false;
                            found_elements = false;
                        }
                        else
                        {
                            first_sap.Visibility = Visibility.Visible; 
                            first_sap = (Separator)options_menu.Items[i];
                            first_sap.Visibility = Visibility.Collapsed;                                                 
                            found_visible_elements = false;
                            found_elements = false;
                        }
                    }
                }
                else if (options_menu.Items[i] is FrameworkElement)
                {
                    if (((FrameworkElement)options_menu.Items[i]).Visibility == Visibility.Visible)
                        found_visible_elements = true;

                    found_elements = true;
                }
            }
        }

        public void CheckInitialWindowState()
        {
                if (_base_frame.InitialWindowMode == Logicx.WpfUtility.WindowManagement.BaseFrame.InitialWindowModeEnum.FullScreen)
                    this.maximize_button.IsChecked = true;
                else if (_base_frame.InitialWindowMode == Logicx.WpfUtility.WindowManagement.BaseFrame.InitialWindowModeEnum.TaskBarVisible)
                    maximize_widthtb_button.IsChecked = true;
        }

        internal void SetMaximizedCheck(bool is_checked)
        {
            _ignore_check_changed = true;
            maximize_widthtb_button.IsChecked = is_checked;
            _ignore_check_changed = false;
        }

        internal void SeFullscreenCheck(bool is_checked)
        {
            _ignore_check_changed = true;
            maximize_button.IsChecked = is_checked;
            _ignore_check_changed = false;
        }

        #region Properties
        public BaseFrame BaseFrame
        {
            get
            {
                return _base_frame;
            }
            set
            {
                _base_frame = value;
            }
        }

        public AcCbx SearchAcCbx
        {
            get
            {
                return search_cbx;
            }
        }

        public bool WithCustomTextSearch { get; set; }
        #endregion

        #region Dependency Properties
        public Thickness BorderThickness
        {
            get { return (Thickness)GetValue(BorderThicknessProperty); }
            set { SetValue(BorderThicknessProperty, value); }
        }
        public static readonly DependencyProperty BorderThicknessProperty = DependencyProperty.Register("BorderThickness", typeof(Thickness), typeof(WindowTitleBar), new UIPropertyMetadata(new Thickness(0,0,0,1)));

        

        public UIElement AdditionalOnlineHelpIcon
        {
            get { return (UIElement)GetValue(AdditionalOnlineHelpIconProperty); }
            set { SetValue(AdditionalOnlineHelpIconProperty, value); }
        }

        // Using a DependencyProperty as the backing store for AdditionalOnlineHelpIcon.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty AdditionalOnlineHelpIconProperty =
            DependencyProperty.Register("AdditionalOnlineHelpIcon", typeof(UIElement), typeof(WindowTitleBar), new UIPropertyMetadata(null));



        public bool AdditionalOnlineHelpVisible
        {
            get { return (bool)GetValue(AdditionalOnlineHelpVisibleProperty); }
            set { SetValue(AdditionalOnlineHelpVisibleProperty, value); }
        }

        // Using a DependencyProperty as the backing store for AdditionalOnlineHelpVisible.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty AdditionalOnlineHelpVisibleProperty =
            DependencyProperty.Register("AdditionalOnlineHelpVisible", typeof(bool), typeof(WindowTitleBar), new UIPropertyMetadata(false));



        public string AdditionalOnlineHelpText
        {
            get { return (string)GetValue(AdditionalOnlineHelpTextProperty); }
            set { SetValue(AdditionalOnlineHelpTextProperty, value); }
        }

        // Using a DependencyProperty as the backing store for AdditionalOnlineHelpText.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty AdditionalOnlineHelpTextProperty =
            DependencyProperty.Register("AdditionalOnlineHelpText", typeof(string), typeof(WindowTitleBar), new UIPropertyMetadata("Zusätzliche Online-Hilfe"));

        public string AdditionalOnlineHelpToolTipText
        {
            get { return (string)GetValue(AdditionalOnlineHelpToolTipTextProperty); }
            set { SetValue(AdditionalOnlineHelpToolTipTextProperty, value); }
        }

        // Using a DependencyProperty as the backing store for AdditionalOnlineHelpToolTipText.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty AdditionalOnlineHelpToolTipTextProperty =
            DependencyProperty.Register("AdditionalOnlineHelpToolTipText", typeof(string), typeof(WindowTitleBar), new UIPropertyMetadata("Zusätzliche Online-Hilfe"));

        public string CenterTitleText
        {
            get { return (string)GetValue(CenterTitleTextProperty); }
            set { SetValue(CenterTitleTextProperty, value); }
        }

        public static readonly DependencyProperty CenterTitleTextProperty =
            DependencyProperty.Register("CenterTitleText", typeof(string), typeof(WindowTitleBar), new UIPropertyMetadata(string.Empty));

        public string CenterTitleDescriptionText
        {
            get { return (string)GetValue(CenterTitleDescriptionTextProperty); }
            set { SetValue(CenterTitleDescriptionTextProperty, value); }
        }

        public static readonly DependencyProperty CenterTitleDescriptionTextProperty =
            DependencyProperty.Register("CenterTitleDescriptionText", typeof(string), typeof(WindowTitleBar), new UIPropertyMetadata(string.Empty));

        public bool IsSearchFieldVisible
        {
            get { return (bool)GetValue(IsSearchFieldVisibleProperty); }
            set { SetValue(IsSearchFieldVisibleProperty, value); }
        }
        public static readonly DependencyProperty IsSearchFieldVisibleProperty = DependencyProperty.Register("IsSearchFieldVisible", typeof(bool), typeof(WindowTitleBar), new UIPropertyMetadata(false));

        public string SearchButtonToolTip
        {
            get { return (string)GetValue(SearchButtonToolTipProperty); }
            set { SetValue(SearchButtonToolTipProperty, value); }
        }
        public static readonly DependencyProperty SearchButtonToolTipProperty = DependencyProperty.Register("SearchButtonToolTip", typeof(string), typeof(WindowTitleBar), new UIPropertyMetadata(""));



        public System.Windows.Visibility MaximizeWidthTbButtonVisible
        {
            get { return (System.Windows.Visibility)GetValue(MaximizeWidthTbButtonVisibleProperty); }
            set { SetValue(MaximizeWidthTbButtonVisibleProperty, value); }
        }

        public static readonly DependencyProperty MaximizeWidthTbButtonVisibleProperty =
            DependencyProperty.Register("MaximizeWidthTbButtonVisible", typeof(System.Windows.Visibility), typeof(WindowTitleBar), new UIPropertyMetadata(System.Windows.Visibility.Visible));

        
        #endregion

        #region Event Handler

        private void close_button_Click(object sender, RoutedEventArgs e)
        {
            e.Handled = true;
            if (CloseClick != null)
                CloseClick(this, new EventArgs());
        }

        private void fullscreen_button_CheckStateChanged(object sender, RoutedEventArgs e)
        {
            if (_ignore_check_changed)
                return;

            e.Handled = true;
            if (FullScreenClick != null)
                FullScreenClick(sender, new EventArgs());

            // hier wurde bewußt auf eine Bindung verzichtet, da dadurch die Reihenfolge der zugehörigen Eventhandler nicht gewährleistet werden konnte
            maximize_widthtb_button.IsChecked = maximize_button.IsChecked;
            
        }

        private void maximize_button_CheckStateChanged(object sender, RoutedEventArgs e)
        {
            if (_ignore_check_changed)
                return;

            e.Handled = true;
            if (MaximizeClick != null)
                MaximizeClick(sender, new EventArgs());

            // hier wurde bewußt auf eine Bindung verzichtet, da dadurch die Reihenfolge der zugehörigen Eventhandler nicht gewährleistet werden konnte
            maximize_button.IsChecked = maximize_widthtb_button.IsChecked;
        }

        private void minimize_button_Click(object sender, RoutedEventArgs e)
        {
            e.Handled = true;
            if (MinimizeClick != null)
                MinimizeClick(this, new EventArgs());
        }

        private void close_mi_Click(object sender, RoutedEventArgs e)
        {
            e.Handled = true;
            if (CloseClick != null)
                CloseClick(this, new EventArgs());
        }

        private void onlinehelp_mi_Click(object sender, RoutedEventArgs e)
        {
            e.Handled = true;
            if (OnlineHelpClick != null)
                OnlineHelpClick(this, new EventArgs());
        }

        private void additional_onlinehelp_mi_Click(object sender, RoutedEventArgs e)
        {
            e.Handled = true;
            if(AdditionalOnlineHelpClick != null)
                AdditionalOnlineHelpClick(this, new EventArgs());
        }

        private void about_mi_Click(object sender, RoutedEventArgs e)
        {
            e.Handled = true;
            if (AboutClick != null)
                AboutClick(this, new EventArgs());
        }

        void mi_Click(object sender, RoutedEventArgs e)
        {
            e.Handled = true;
            if (OptionsClick != null)
                OptionsClick(this, new OptionsEventArgs(((MenuItem)sender).Tag.ToString()));
        }


        void searchfield_stackpanel_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            e.Handled = true;
        }



        /// <summary>
        /// starte navigation zum anlagen element
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            if (search_cbx.SelectedItem == null && !WithCustomTextSearch)
                return;

            if (SearchElementSelected != null)
            {
                if (search_cbx.SelectedItem == null && WithCustomTextSearch)
                    SearchElementSelected(search_cbx.Text, new EventArgs());
                else
                    SearchElementSelected(search_cbx.SelectedItem, new EventArgs());
            }
        }


        void WindowTitleBar_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            search_cbx.Width = e.NewSize.Width*0.22;
        }
        #endregion

        public event EventHandler SearchElementSelected;
        public event EventHandler CloseClick;
        public event EventHandler MinimizeClick;
        public event EventHandler MaximizeClick;
        public event EventHandler FullScreenClick;
        
        public event EventHandler OnlineHelpClick;
        public event EventHandler AdditionalOnlineHelpClick;
        public event EventHandler AboutClick;
        public event EventHandler<OptionsEventArgs> OptionsClick;

        private bool _ignore_check_changed = false;
        private Brush _render_brush;
        private Pen _render_pen;
        private BaseFrame _base_frame;
    }
}
