﻿using System;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LinqLib.Serialization
{
    /// <summary>
    /// base class for all serializable data objects of the LinQ DBML
    /// </summary>
    [DataContract()]
    public class IdentifyableObjectBase
    {
        private Guid _serialized_object_id = Guid.Empty;

        /// <summary>
        /// Object identifier property assigning every object a unique GUID
        /// </summary>
        [DataMember(Order = 0)]
        public Guid SerializedObjectId
        {
            get
            {
                if (_serialized_object_id == Guid.Empty)
                    _serialized_object_id = Guid.NewGuid();

                return _serialized_object_id;
            }
            set
            {
                _serialized_object_id = value;
            }
        }
    }
}
