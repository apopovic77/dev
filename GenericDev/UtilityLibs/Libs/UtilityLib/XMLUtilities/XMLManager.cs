﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Logicx.Utility.XMLUtilities
{
    public class XMLManager
    {

        public static T DeSerialize<T>(string path_to_file)
        {
            string xml = DeSerializeFromFile(path_to_file);
            T obj = DeserializeFromString<T>(xml);
            return obj;
        }
        public static void Serialize<T>(string path_to_file, T object_to_serialize)
        {
            SerializeToFile(path_to_file, object_to_serialize);
        }

        public static void SerializeList<T>(List<T> list_to_serialize, string path_to_file)
        {
            string xml_list = "";
            foreach (var obj in list_to_serialize)
            {
                string s = SerializeToString(obj);
                xml_list += s.Substring(39) + "--?-???-?--";
            }

            //write to file
            if (File.Exists(path_to_file))
                File.Delete(path_to_file);

            using (FileStream fs = File.OpenWrite(path_to_file))
            {
                using (StreamWriter sw = new StreamWriter(fs))
                {
                    sw.Write(xml_list);
                    sw.Flush();
                }
            }
        }

        public static List<T> DeSerializeList<T>(string path_to_file)
        {

            Type type_obj = typeof(T);

            //write to file
            if (!File.Exists(path_to_file))
                throw new Exception("XMLMANAGER: cannot deserialize list, file not existing");

            using (FileStream fs = File.OpenRead(path_to_file))
            {
                using (StreamReader sr = new StreamReader(fs))
                {
                    List<T> objs = new List<T>();
                    while(true)
                    {
                        string line = sr.ReadLine();
                        if (line == null)
                            break;


                        T obj = DeserializeFromString<T>(line);
                        objs.Add(obj);
                    }
                    return objs;
                }
            }









            //Type type_obj = typeof(T);

            ////write to file
            //if (!File.Exists(path_to_file))
            //    throw new Exception("XMLMANAGER: cannot deserialize list, file not existing");

            //string s_contentfile = "";

            //using (FileStream fs = File.OpenRead(path_to_file))
            //{
            //    using (StreamReader sr = new StreamReader(fs))
            //    {
            //        s_contentfile = sr.ReadToEnd();
            //    }
            //}



            //List<T> objs = new List<T>();
            //while (true)
            //{
            //    int pos_next_obj = s_contentfile.IndexOf("<" + type_obj.Name, 1);
            //    if (pos_next_obj < 0)
            //        break;

            //    string xml_obj = s_contentfile.Substring(0, pos_next_obj);
            //    s_contentfile = s_contentfile.Remove(0, pos_next_obj);

            //    T obj = DeserializeFromString<T>(xml_obj);
            //    objs.Add(obj);
            //}
            //return objs;
        }



        #region Serialization

        #region Deserialization
        /// <summary>
        /// returns an xml stream
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        protected static string DeSerializeFromFile(string path)
        {
            FileStream fs = File.OpenRead(path);
            StreamReader sw = new StreamReader(fs);
            string xml = sw.ReadToEnd();
            sw.Dispose();
            sw = null;
            fs.Dispose();
            fs = null;
            return xml;
        }
        public static T DeserializeFromString<T>(String pXmlizedString)
        {
            Type type_obj = typeof(T);
            XmlSerializer xs = new XmlSerializer(type_obj);
            MemoryStream memoryStream = new MemoryStream(StringToUTF8ByteArray(pXmlizedString));
            XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, Encoding.UTF8);
            return (T)xs.Deserialize(memoryStream);
        }
        #endregion

        #region Serialization
        protected static void SerializeToFile(string path, object obj_to_serialize)
        {
            string xml = SerializeToString(obj_to_serialize);
            if (File.Exists(path))
            {
                if ((File.GetAttributes(path) & FileAttributes.ReadOnly) == FileAttributes.ReadOnly)
                    File.SetAttributes(path, FileAttributes.Normal);
                File.Delete(path);
            }

            try
            {
                WriteStreamToFile(path, xml);
            }
            catch (DirectoryNotFoundException ex)
            {
                CreateNotExistingSubForders(path);
                WriteStreamToFile(path, xml);
            }
        }

        public static void CreateNotExistingSubForders(string path)
        {
            int last_index = 0;
            if (path.ToLower().StartsWith("c:\\"))
                last_index = 3;
            while (true)
            {
                int new_index = path.Substring(last_index + 1).IndexOf("\\");
                if (new_index < 0)
                    break;
                last_index = new_index + last_index + 1;
                string path_to_check = path.Substring(0, last_index);

                DirectoryInfo di = new DirectoryInfo(path_to_check);
                if (!di.Exists)
                    di.Create();
            }
        }


        protected static void WriteStreamToFile(string path, string xml)
        {
            FileStream fs = File.OpenWrite(path);
            StreamWriter sw = new StreamWriter(fs);
            sw.Write(xml);
            sw.Flush();
            sw.Dispose();
            sw = null;
            fs.Dispose();
            fs = null;
        }

        public static String SerializeToString(object obj_to_serialize)
        {

            String XmlizedString = null;
            MemoryStream memoryStream = new MemoryStream();
            XmlSerializer xs = new XmlSerializer(obj_to_serialize.GetType());
            XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, Encoding.UTF8);
            xmlTextWriter.Formatting = Formatting.Indented;
            xmlTextWriter.Indentation = 4;
            xs.Serialize(xmlTextWriter, obj_to_serialize);
            memoryStream = (MemoryStream)xmlTextWriter.BaseStream;
            XmlizedString = UTF8ByteArrayToString(memoryStream.ToArray());
            return XmlizedString;

        }
        #endregion




        /// <summary>
        /// To convert a Byte Array of Unicode values (UTF-8 encoded) to a complete String.
        /// </summary>
        /// <param name="characters">Unicode Byte Array to be converted to String</param>
        /// <returns>String converted from Unicode Byte Array</returns>
        private static String UTF8ByteArrayToString(Byte[] characters)
        {
            UTF8Encoding encoding = new UTF8Encoding();
            String constructedString = encoding.GetString(characters);
            return (constructedString);
        }

        /// <summary>
        /// Converts the String to UTF8 Byte array and is used in De serialization
        /// </summary>
        /// <param name="pXmlString"></param>
        /// <returns></returns>
        private static Byte[] StringToUTF8ByteArray(String pXmlString)
        {
            UTF8Encoding encoding = new UTF8Encoding();
            Byte[] byteArray = encoding.GetBytes(pXmlString);
            return byteArray;
        }
        #endregion



    }
}
