﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Awanto.WhatsAppCore.Entities
{
    public enum KommunikationsKanal
    {
        NotDefined,
        EMail,
        Whatsapp,
        Sms
    }


    public class Nachricht : DbEntity
    {

        public void SetGesendetAm(KommunikationsKanal kanal, DateTime dt)
        {
            if (!KommunikationsKanaele.Contains(kanal))
                throw new Exception("Gesendet am nicht möglich diese Nachricht ist nicht für diesen Kommunikationskanal bestimmt.");

            _RawGesendetAm += Convert.ToInt32(kanal) + $",{dt.Year}-{dt.Month}-{dt.Day}" + dt.ToLongTimeString() + ";";
        }
        public DateTime? GetGesendetAm(KommunikationsKanal kanal)
        {
            if (!KommunikationsKanaele.Contains(kanal))
                throw new Exception("Gesendet am nicht möglich diese Nachricht ist nicht für diesen Kommunikationskanal bestimmt.");
            if (_RawGesendetAm == null)
                return null;
            string[] sarr = _RawGesendetAm.Split(";");
            foreach (var s in sarr)
            {
                string[] gesam = s.Split(",");
                KommunikationsKanal curr_kanal = Enum.Parse<KommunikationsKanal>(gesam[0]);
                if (curr_kanal == kanal)
                {
                    return DateTime.Parse(gesam[1].Replace("T", " "));
                }
            }

            return null;
        }

        #region Not Mapped

        [NotMapped]
        public string PathToMedia
        {
            get
            {
                if (Medien == null || Medien.Count == 0)
                    return null;
                return Medien.First().Medium.PathToMedia;
            }
        }
        //[NotMapped]
        //public List<String> PathsToMedia
        //{
        //    get { return _RawPathsToMedia.Split('|').ToList(); }
        //    set { _RawPathsToMedia = String.Join("|", value); }
        //}
        [NotMapped]
        public KommunikationsKanal KommunikationsKanal
        {
            get
            {
                List<KommunikationsKanal> kanaele = KommunikationsKanaele;
                if (kanaele == null || kanaele.Count == 0)
                    return KommunikationsKanal.NotDefined;
                return kanaele[0];
            }
            set
            {
                KommunikationsKanaele = new List<KommunikationsKanal>(1) { value };
            }
        }
        [NotMapped]
        public List<KommunikationsKanal> KommunikationsKanaele
        {
            get
            {
                string[] kanele = _RawKommunikationsKanaele.Split('|');
                return kanele.Select(i =>
                    (KommunikationsKanal)Enum.ToObject(typeof(KommunikationsKanal), Convert.ToInt32(i))).ToList();
            }
            set
            {
                _RawKommunikationsKanaele = String.Join("|", value.Select(e => Convert.ToInt32(e).ToString()).ToList());
            }
        }

        #endregion

        public override string ToString()
        {
            return $"{AbsenderAdresse}->{EmpfaengerAdresse} {Body}";
        }


        public ICollection<Nachricht> Antworten { get; set; }
        public Nachricht AntwortAuf { get; set; }

        [Required]
        public String _RawKommunikationsKanaele { get; set; }
        public String _RawGesendetAm { get; set; }

        public string Subject { get; set; }

        [Required]
        public string Body { get; set; }


        [Required]
        public string EmpfaengerAdresse { get; set; }
        [Required]
        public string AbsenderAdresse { get; set; }

        public ICollection<NachrichtMedium> Medien { get; set; }

    }


    public class WhatsAppNachricht : Nachricht
    {
        public WhatsAppNachricht()
        {
            KommunikationsKanal = KommunikationsKanal.Whatsapp;
        }


    }

    public class WhatsAppImageNachricht : Nachricht
    {
        public WhatsAppImageNachricht()
        {
            KommunikationsKanal = KommunikationsKanal.Whatsapp;
        }
    }

    public class WhatsAppVotingNachricht : WhatsAppNachricht
    {

        public static string GetVoteMsg(string frage, string empfänger)
        {
            string msg = "Frage ?\n\n_" + frage +
                         "?_\n\n*JA* - http://go.laisschule.at/vote?pid=11&ans=YES \n *NEIN* - http://go.laisschule.at/vote?pid=11&ans=NO";
            return msg;
        }
    }
}

