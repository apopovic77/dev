﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Controls.Primitives;

namespace Logicx.WpfUtility.CustomControls.Slider
{
    /// <summary>
    /// Interaction logic for YearMonthSlider.xaml
    /// </summary>
    public partial class YearMonthSlider : UserControl
    {
        public YearMonthSlider()
        {
            InitializeComponent();
            slider.TickFrequency = 1;
            Loaded += new RoutedEventHandler(YearSlider_Loaded);
        }

        public YearMonthSlider(double startYear, double endYear)
        {
            InitializeComponent();
            SliderInitialized = false;

            _start_year = startYear;
            _end_year = endYear+1;
           
            Minimum = _start_year;
            Maximum = _end_year;
            slider.TickFrequency = 1;
            InitStartStopDate();
            
            CurrentDate = _startDate;            

            slider.Value = 0;

            MinValue = Minimum;
            MaxValue = Maximum;
            
            Loaded += new RoutedEventHandler(YearSlider_Loaded);
            slider.MouseDown += new MouseButtonEventHandler(YearMonthSlider_MouseDown);
            slider.MouseUp += new MouseButtonEventHandler(YearMonthSlider_MouseUp);
            
        }

        void YearMonthSlider_MouseUp(object sender, MouseButtonEventArgs e)
        {
            IsSliding = false;
            SendSliderReady();
        }

        void YearMonthSlider_MouseDown(object sender, MouseButtonEventArgs e)
        {
            IsSliding = true;
        }

        private void thumb_DragCompleted(object sender, DragCompletedEventArgs e)
        {
            SendSliderReady();
            IsSliding = false;
        }

        #region Operations        
        public void SetSliderValue(DateTime sliderDate)
        {
            // Beide Datum Werte in einer Liste speichern und sortieren 
            List<DateTime> period = new List<DateTime>() { _startDate, sliderDate };
            period.Sort(DateTime.Compare);

            // Monate zählen
            int months;
            for (months = 0; period[0].AddMonths(months + 1).CompareTo(period[1]) <= 0; months++) ;

            //Slider Value zurücksetzen und monate neu hinzuzählen
            Value = 0;
            Value += months;
        }
        private void InitStartStopDate()
        {
            try
            {
                _startDate = new DateTime(Convert.ToInt32(_start_year), 1, 1);
                _stopDate = new DateTime(Convert.ToInt32(_end_year), 12, 31);
            }
            catch (Exception)
            { 
            }
        }
        private void UpdateValuePosition()
        {
            try
            {
                Thumb t = (Thumb) slider.Template.FindName("thumb", slider);
                Point relativePoint = t.TransformToAncestor(root_grid).Transform(new Point(0, 0));
                slider_value.Margin = new Thickness(relativePoint.X - 45, 10, 0, 0);
                Value = slider.Value;
            }
            catch (Exception)
            {
            }
        }

        public void InitTickBar()
        {
            try
            {
                ExtendedTickBar tickbar = (ExtendedTickBar)slider.Template.FindName("tickbar", slider);
                tickbar.StartYear = 0;
                tickbar.EndYear = 0;
                tickbar.StartYear = _start_year;
                tickbar.EndYear = _end_year;
            }
            catch (Exception)
            { 
            }
        }

        public void SetEnabledState(bool enabled)
        {
            slider.IsEnabled = enabled;

            if (!enabled)
            {
                tbl_header_text.Foreground = Brushes.Gray;
                ExtendedTickBar tickbar = (ExtendedTickBar)slider.Template.FindName("tickbar", slider);
                tickbar.TickBrush = Brushes.Gray;
                slider_value.Foreground = Brushes.Gray;
            }
            else 
            {
                tbl_header_text.Foreground = Brushes.Black;
                slider_value.Foreground = Brushes.Black;
                ExtendedTickBar tickbar = (ExtendedTickBar)slider.Template.FindName("tickbar", slider);
                tickbar.TickBrush = Brushes.Black;
            }
        }
        #endregion

        #region Properites
        public bool SliderInitialized
        {
            get;
            set;
        }

        public bool IsSliding
        {
            get;
            set;
        }
        #endregion

        #region Dependency Properties



        public double MinValue
        {
            get { return (double)GetValue(MinValueProperty); }
            set { SetValue(MinValueProperty, value); }
        }

        public static readonly DependencyProperty MinValueProperty =
            DependencyProperty.Register("MinValue", typeof(double), typeof(YearMonthSlider), new UIPropertyMetadata((double)0));


        public double MaxValue
        {
            get { return (double)GetValue(MaxValueProperty); }
            set { SetValue(MaxValueProperty, value); }
        }

        public static readonly DependencyProperty MaxValueProperty =
            DependencyProperty.Register("MaxValue", typeof(double), typeof(YearMonthSlider), new UIPropertyMetadata((double)0));




        public double Minimum
        {
            get { return (double)GetValue(MinimumProperty); }
            set { SetValue(MinimumProperty, value); }
        }

        public static readonly DependencyProperty MinimumProperty =
            DependencyProperty.Register("Minimum", typeof(double), typeof(YearMonthSlider), new UIPropertyMetadata((double)0));


        public double Maximum
        {
            get { return (double)GetValue(MaximumProperty); }
            set { SetValue(MaximumProperty, value); }
        }

        public static readonly DependencyProperty MaximumProperty =
            DependencyProperty.Register("Maximum", typeof(double), typeof(YearMonthSlider), new UIPropertyMetadata((double)0));


        public bool IsSnapToTickEnabled
        {
            get { return (bool)GetValue(IsSnapToTickEnabledProperty); }
            set { SetValue(IsSnapToTickEnabledProperty, value); }
        }

        public static readonly DependencyProperty IsSnapToTickEnabledProperty =
            DependencyProperty.Register("IsSnapToTickEnabled", typeof(bool), typeof(YearMonthSlider), new UIPropertyMetadata(false));


        public double Value
        {
            get { return (double)GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }

        public static readonly DependencyProperty ValueProperty =
            DependencyProperty.Register("Value", typeof(double), typeof(YearMonthSlider), new UIPropertyMetadata((double)0));


        public double TickFrequency
        {
            get { return (double)GetValue(TickFrequencyProperty); }
            set { SetValue(TickFrequencyProperty, value); }
        }

        public static readonly DependencyProperty TickFrequencyProperty =
            DependencyProperty.Register("TickFrequency", typeof(double), typeof(YearMonthSlider), new UIPropertyMetadata((double)0));


        public string HeaderText
        {
            get { return (string)GetValue(HeaderTextProperty); }
            set { SetValue(HeaderTextProperty, value); }
        }

        public static readonly DependencyProperty HeaderTextProperty =
            DependencyProperty.Register("HeaderText", typeof(string), typeof(YearMonthSlider), new UIPropertyMetadata("Betrachtungszeitpunkt"));


        public DateTime CurrentDate
        {
            get { return (DateTime)GetValue(CurrentDateProperty); }
            set { SetValue(CurrentDateProperty, value); }
        }

        public static readonly DependencyProperty CurrentDateProperty =
            DependencyProperty.Register("CurrentDate", typeof(DateTime), typeof(YearMonthSlider), new UIPropertyMetadata(DateTime.Now));


        #endregion

        #region Event Handlers
        void YearSlider_Loaded(object sender, RoutedEventArgs e)
        {
            MinValue = Minimum;
            MaxValue = Maximum;

            Maximum = (_end_year - _start_year) * 12;
            Minimum = 0;

            double start_value = ((double)DateTime.Now.Year - _start_year)*12 + (double)DateTime.Now.Month; 
            Value = start_value-1;

            InitTickBar();
            
            SliderInitialized = true;
            SendSliderIsInitialized();

        }

        private void RepeatButton_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            UpdateValuePosition();
        }

        private void slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            try
            {
                if (_old_slide_value < Value)
                    CurrentDate = _startDate.AddMonths((int)Value);
                else
                    CurrentDate = _startDate.AddMonths((int)Value);

                //if (e.OldValue < Value)
                //    CurrentDate = _startDate.AddMonths((int) Value);
                //else
                //    CurrentDate = _startDate.AddMonths((int) Value);

                _old_slide_value = slider.Value;

                UpdateValuePosition();

                SendValueChanged();

                if (!_reinitialized)
                {
                    InitTickBar();
                    _reinitialized = true;
                }
            }
            catch(Exception)
            {
            
            }        
        }

        #endregion

        #region Event raising
        private void SendValueChanged()
        {
            if (SliderValueChanged != null)
                SliderValueChanged(this, EventArgs.Empty);
        }

        private void SendSliderReady()
        {
            if (SliderReady != null)
                SliderReady(this, EventArgs.Empty);
        }

        private void SendSliderIsInitialized()
        {
            if (SliderIsInitialized != null)
                SliderIsInitialized(this, EventArgs.Empty);
        }

        #endregion

        #region Events
        public event EventHandler SliderValueChanged;
        public event EventHandler SliderReady;
        public event EventHandler SliderIsInitialized;

        #endregion

        #region Attributes
        double _old_slide_value;
        DateTime _startDate;
        DateTime _stopDate;
        private double _start_year;
        private double _end_year;
        bool _reinitialized = false;
        #endregion
    }
}
