﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Logicx.DiagramLib.Controls;
using Logicx.DiagramLib.Scales;
using Logicx.DiagramLib.ViewConfig;

namespace Logicx.DiagramLib.Diagrams
{
    public class DiagramIntDateTime : DiagramBase<int,DateTime>
    {
        #region Constants
        #endregion

        #region Construction and Initialization
        public DiagramIntDateTime(DiagramConfig<int,DateTime> diagram_config) : base(diagram_config)
        {
            
        }
        #endregion

        #region Operations

        protected override Scales.TypedScaleControl<int> GetXAxisControl()
        {
            DiagramConfig.AxisScaleConfigX.Orientation = System.Windows.Controls.Orientation.Horizontal;

            if (_isc == null)
            {
                _isc = new IntScaleControl(DiagramConfig.AxisScaleConfigX, "");
                _isc.IsHitTestVisible = false;
            }

            return _isc;
        }

        protected override Scales.TypedScaleControl<DateTime> GetYAxisControl()
        {
            DiagramConfig.AxisScaleConfigY.Orientation = System.Windows.Controls.Orientation.Vertical;

            if (_tsc == null)
            {
                _tsc = new TimeScaleControl(DiagramConfig.AxisScaleConfigY);
                _tsc.IsHitTestVisible = false;
            }

            return _tsc;
        }

        
        protected override double TransformXAxisValueToPixel(int value)
        {
            return 0;
        }

        protected override double TransformYAxisValueToPixel(DateTime value)
        {
            return 0;
        }

        protected override int TransformXAxisPixelToValue(double pixel)
        {
            return 0;
        }

        protected override DateTime TransformYAxisPixelToValue(double pixel)
        {
            return DateTime.MinValue;
        }

        #endregion

        #region Event Handlers
        #endregion

        #region Properties
        #endregion

        #region Dependency Properties
        #endregion

        #region Events
        #endregion

        #region Routed Events
        #endregion

        #region Attributes

        private IntScaleControl _isc;
        private TimeScaleControl _tsc;

        #endregion

        #region Tests

        #endregion


    }
}
