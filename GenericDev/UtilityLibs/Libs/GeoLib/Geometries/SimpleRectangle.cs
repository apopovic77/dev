using System;
using System.Collections.Generic;
using System.Text;
using MathLib;

namespace Logicx.Geo.Geometries
{
    public class SimpleRectangle
    {
        public SimpleRectangle(Vector2f corner1, Vector2f corner2, Vector2f corner3, Vector2f corner4)
		{
            m_corner1 = corner1;
            m_corner2 = corner2;
            m_corner3 = corner3;
            m_corner4 = corner4;
		}
        public SimpleRectangle(Vector2f corner_topleft, float width, float height)
		{
            m_corner1 = corner_topleft;
            m_corner2 = new Vector2f(corner_topleft.X + width, corner_topleft.Y);
            m_corner3 = new Vector2f(corner_topleft.X + width, corner_topleft.Y - height);
            m_corner4 = new Vector2f(corner_topleft.X, corner_topleft.Y - height);
		}
        public SimpleRectangle(Vector2f corner_topleft, float width, float height, bool invert_y_axis)
        {
            _invert_y_axis = invert_y_axis;
            if (!invert_y_axis)
            {
                m_corner1 = corner_topleft;
                m_corner2 = new Vector2f(corner_topleft.X + width, corner_topleft.Y);
                m_corner3 = new Vector2f(corner_topleft.X + width, corner_topleft.Y - height);
                m_corner4 = new Vector2f(corner_topleft.X, corner_topleft.Y - height);
            }
            else
            {
                m_corner1 = corner_topleft;
                m_corner2 = new Vector2f(corner_topleft.X + width, corner_topleft.Y);
                m_corner3 = new Vector2f(corner_topleft.X + width, corner_topleft.Y + height);
                m_corner4 = new Vector2f(corner_topleft.X, corner_topleft.Y + height);
            }
        }
        public SimpleRectangle(Vector2f corner_topleft, Vector2f dimensions)
        {
            m_corner1 = corner_topleft;
            m_corner2 = new Vector2f(corner_topleft.X + dimensions.X, corner_topleft.Y);
            m_corner3 = new Vector2f(corner_topleft.X + dimensions.X, corner_topleft.Y - dimensions.Y);
            m_corner4 = new Vector2f(corner_topleft.X, corner_topleft.Y - dimensions.Y);
        }

        public SimpleRectangle(Vector2f corner_topleft, Vector2f dimensions, bool invert_y_axis)
        {
            _invert_y_axis = invert_y_axis;
            if(!invert_y_axis)
            {
                m_corner1 = corner_topleft;
                m_corner2 = new Vector2f(corner_topleft.X + dimensions.X, corner_topleft.Y);
                m_corner3 = new Vector2f(corner_topleft.X + dimensions.X, corner_topleft.Y - dimensions.Y);
                m_corner4 = new Vector2f(corner_topleft.X, corner_topleft.Y - dimensions.Y);
            }
            else
            {
                m_corner1 = corner_topleft;
                m_corner2 = new Vector2f(corner_topleft.X + dimensions.X, corner_topleft.Y);
                m_corner3 = new Vector2f(corner_topleft.X + dimensions.X, corner_topleft.Y + dimensions.Y);
                m_corner4 = new Vector2f(corner_topleft.X, corner_topleft.Y + dimensions.Y);
            }
        }

        private void AssertDifferentCornerValues(Vector2f corner)
        {
            if (m_corner1 != null && corner != m_corner1)
                if (m_corner2 != null && corner != m_corner2)
                    if (m_corner3 != null && corner != m_corner3)
                        if (m_corner4 != null && corner != m_corner4)
                            return;
            throw new Exception("Rectangle is no Rectangle if two corners have the same value");
        }

        public Vector2f CentroidPosition
        {
            get{ 
                Vector2f diag_vec = m_corner3 - m_corner2;
                float len = diag_vec.GetLen();
                return m_corner1 + ((m_corner3 - m_corner1).GetNorm() * (len / 2f));
            }
        }

        public float Area
        {
            get {
                return (m_corner1 - m_corner2).GetLen() * (m_corner1 - m_corner4).GetLen();
            }
        }

        public float Width
        {
            get
            {
                return (m_corner1 - m_corner2).GetLen();
            }
        }
        public float Height
        {
            get
            {
                return (m_corner1 - m_corner4).GetLen();
            }
        }


        public Vector2f Mostleft
        {
            get
            {
                Vector2f mostleft = m_corner1;
                if (m_corner2.X < mostleft.X)
                    mostleft = m_corner2;
                if (m_corner3.X < mostleft.X)
                    mostleft = m_corner3;
                if (m_corner4.X < mostleft.X)
                    mostleft = m_corner4;
                return mostleft;
            }
        }

        public Vector2f Mostright
        {
            get
            {
                Vector2f mostright = m_corner1;
                if (m_corner2.X > mostright.X)
                    mostright = m_corner2;
                if (m_corner3.X > mostright.X)
                    mostright = m_corner3;
                if (m_corner4.X > mostright.X)
                    mostright = m_corner4;
                return mostright;
            }
        }
        public Vector2f Mosttop
        {
            get
            {
                Vector2f mosttop = m_corner1;
                if (m_corner2.Y > mosttop.Y)
                    mosttop = m_corner2;
                if (m_corner3.Y > mosttop.Y)
                    mosttop = m_corner3;
                if (m_corner4.Y > mosttop.Y)
                    mosttop = m_corner4;
                return mosttop;
            }
        }
        public Vector2f Mostbottom
        {
            get
            {
                Vector2f mostbottom = m_corner1;
                if (m_corner2.Y < mostbottom.Y)
                    mostbottom = m_corner2;
                if (m_corner3.Y < mostbottom.Y)
                    mostbottom = m_corner3;
                if (m_corner4.Y < mostbottom.Y)
                    mostbottom = m_corner4;
                return mostbottom;
            }
        }

        /// <summary>
        /// Verschiebt das Rectangle entlag der X Achse
        /// </summary>
        /// <param name="transl_x"></param>
        public void TranslateX(float transl_x)
        {
            m_corner1.X += transl_x;
            m_corner2.X += transl_x;
            m_corner3.X += transl_x;
            m_corner4.X += transl_x;
        }

        /// <summary>
        /// Verschiebt das Rectangle entlang der Y Achse
        /// </summary>
        /// <param name="transl_y"></param>
        public void TranslateY(float transl_y)
        {
            m_corner1.Y += transl_y;
            m_corner2.Y += transl_y;
            m_corner3.Y += transl_y;
            m_corner4.Y += transl_y;
        }

        /// <summary>
        /// Verschiebt das Rectangle entlang der Y Achse
        /// </summary>
        /// <param name="y"></param>
        public void TranslateToY(float y)
        {
            float height = Height;
            if (_invert_y_axis)
            {
                m_corner1.Y = y;
                m_corner2.Y = y;
                m_corner3.Y = y + height;
                m_corner4.Y = y + height;
            }
            else
            {
                m_corner1.Y = y;
                m_corner2.Y = y;
                m_corner3.Y = y - height;
                m_corner4.Y = y - height;
            }
        }
        

        public bool PointInRect(Vector2f v)
        {
            if (v.X > Mostleft.X && v.X < Mostright.X &&
               v.Y < Mosttop.Y && v.Y > Mostbottom.Y)
                return true;
            else
                return false;
        }

        private Vector2f m_corner1;
        private Vector2f m_corner2;
        private Vector2f m_corner3;
        private Vector2f m_corner4;

        public Vector2f Corner1 { set { AssertDifferentCornerValues(m_corner1); m_corner1 = value; } get { return m_corner1; } }
        public Vector2f Corner2 { set { AssertDifferentCornerValues(m_corner2); m_corner2 = value; } get { return m_corner2; } }
        public Vector2f Corner3 { set { AssertDifferentCornerValues(m_corner3); m_corner3 = value; } get { return m_corner3; } }
        public Vector2f Corner4 { set { AssertDifferentCornerValues(m_corner4); m_corner4 = value; } get { return m_corner4; } }

        protected bool _invert_y_axis = false;


    }
}
