﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Logicx.WpfUtility.PieMenu
{
    /// <summary>
    /// Interface zur Unterscheidung eines TogglePieMenuEntry von einem normalen PieMenuEntry
    /// </summary>
    public interface ITogglePieMenuEntry
    {
        bool IsChecked { set; get; }    
    }

    [Serializable]
    //TODO Änderungen in dieser Klasse müssen auch in der entsprechenden Klasse im IaApi Lib durchgeführt werden, da diese Klasse hier eine Kopie derjenigen im IaApi ist 
    public class TogglePieMenuEntry : PieMenuEntry, INotifyPropertyChanged, ITogglePieMenuEntry
    {
        //TODO Änderungen in dieser Klasse müssen auch in der entsprechenden Klasse im IaApi Lib durchgeführt werden, da diese Klasse hier eine Kopie derjenigen im IaApi ist 
        public TogglePieMenuEntry(Color bg_active_c, Color bg_checked_c, Color bg_checked_border_c, string name)
            : base(bg_active_c, name)
        {
            _bg_togglepie_color = bg_checked_c;
            _border_pie_checked_color = bg_checked_border_c;

            Init();
        }
        //TODO Änderungen in dieser Klasse müssen auch in der entsprechenden Klasse im IaApi Lib durchgeführt werden, da diese Klasse hier eine Kopie derjenigen im IaApi ist 
        public TogglePieMenuEntry(Color bg_active_c, Color bg_checked_c, Color bg_checked_border_c, string name, string key)
            : base(bg_active_c, name, key)
        {
            _bg_togglepie_color = bg_checked_c;
            _border_pie_checked_color = bg_checked_border_c;

            Init();
        }
        //TODO Änderungen in dieser Klasse müssen auch in der entsprechenden Klasse im IaApi Lib durchgeführt werden, da diese Klasse hier eine Kopie derjenigen im IaApi ist 
        public TogglePieMenuEntry(string name)
            : base(name)
        {
            _bg_togglepie_color = DefaultColorTogglePieBg;
            _bg_togglepie_active_color = DefaultColorTogglePieBgActive;
            _border_pie_checked_color = DefaultColorBorderPieChecked;

            Init();
        }
        //TODO Änderungen in dieser Klasse müssen auch in der entsprechenden Klasse im IaApi Lib durchgeführt werden, da diese Klasse hier eine Kopie derjenigen im IaApi ist 
        public TogglePieMenuEntry(string name, string key)
            : base(name, key)
        {
            _bg_togglepie_color = DefaultColorTogglePieBg;
            _bg_togglepie_active_color = DefaultColorTogglePieBgActive;
            _border_pie_checked_color = DefaultColorBorderPieChecked;

            Init();
        }
        //TODO Änderungen in dieser Klasse müssen auch in der entsprechenden Klasse im IaApi Lib durchgeführt werden, da diese Klasse hier eine Kopie derjenigen im IaApi ist 
        protected void Init()
        {
            _pie_checked_bg_brush = new SolidColorBrush(_bg_togglepie_color);
            _pie_checked_stroke_brush = new SolidColorBrush(_border_pie_checked_color);
        }
        //TODO Änderungen in dieser Klasse müssen auch in der entsprechenden Klasse im IaApi Lib durchgeführt werden, da diese Klasse hier eine Kopie derjenigen im IaApi ist 
        protected override void DoUpdatePieStyle()
        {
            Path p = Shape;
            Path p_text = ShapeText;

            if (IsChecked)
            {
                p.Fill = _pie_checked_bg_brush;
                p.Stroke = _pie_checked_stroke_brush;
            }
            else
            {
                //p.Fill = _pie_bg_brush;
                p.Fill = new SolidColorBrush(_bg_pie_color);
                p.Stroke = _pie_stroke_brush;
            }
        }
        //TODO Änderungen in dieser Klasse müssen auch in der entsprechenden Klasse im IaApi Lib durchgeführt werden, da diese Klasse hier eine Kopie derjenigen im IaApi ist 
        private void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(info));
        }
        //TODO Änderungen in dieser Klasse müssen auch in der entsprechenden Klasse im IaApi Lib durchgeführt werden, da diese Klasse hier eine Kopie derjenigen im IaApi ist 
        protected override void pie_item_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            Path pie;
            if (sender is Path)
                pie = (Path)sender;
            else if (Shape != null)
                pie = Shape;
            else
                return;

            if (!IsEnabled)
                return;

            Color c_target_anim = _bg_pie_color;
            if (IsChecked)
                c_target_anim = _bg_togglepie_active_color;

            ColorAnimation ca_mouse_leave = new ColorAnimation(c_target_anim, new TimeSpan(0, 0, 0, 1));
            ca_mouse_leave.DecelerationRatio = 1;
            pie.Fill.BeginAnimation(SolidColorBrush.ColorProperty, ca_mouse_leave);
        }
        //TODO Änderungen in dieser Klasse müssen auch in der entsprechenden Klasse im IaApi Lib durchgeführt werden, da diese Klasse hier eine Kopie derjenigen im IaApi ist 
        protected override void pie_item_MouseEnter(object sender, MouseEventArgs e)
        {
            Path pie;
            if (sender is Path)
                pie = (Path)sender;
            else if (Shape != null)
                pie = Shape;
            else
                return;

            if (!IsEnabled)
                return;

            ColorAnimation ca_mouse_enter = new ColorAnimation(_bg_togglepie_active_color, new TimeSpan(0, 0, 0, 0, 250));
            ca_mouse_enter.DecelerationRatio = 1;
            pie.Fill.BeginAnimation(SolidColorBrush.ColorProperty, ca_mouse_enter);
        }
        //TODO Änderungen in dieser Klasse müssen auch in der entsprechenden Klasse im IaApi Lib durchgeführt werden, da diese Klasse hier eine Kopie derjenigen im IaApi ist 
        public bool IsChecked
        {
            set
            {
                _is_checked = value;
                if (_is_checked)
                    if (Checked != null)
                        Checked(this, new EventArgs());
                if (!_is_checked)
                    if (UnChecked != null)
                        UnChecked(this, new EventArgs());

                UpdatePieStyle();

                NotifyPropertyChanged(Name);
                NotifyPropertyChanged("IsChecked");
            }
            get
            {
                return _is_checked;
            }
        }
        //TODO Änderungen in dieser Klasse müssen auch in der entsprechenden Klasse im IaApi Lib durchgeführt werden, da diese Klasse hier eine Kopie derjenigen im IaApi ist 
        public bool DefaultCheckedState
        {
            set;
            get;
        }
        //TODO Änderungen in dieser Klasse müssen auch in der entsprechenden Klasse im IaApi Lib durchgeführt werden, da diese Klasse hier eine Kopie derjenigen im IaApi ist 
        public event PropertyChangedEventHandler PropertyChanged;
        //TODO Änderungen in dieser Klasse müssen auch in der entsprechenden Klasse im IaApi Lib durchgeführt werden, da diese Klasse hier eine Kopie derjenigen im IaApi ist 
        public event EventHandler Checked;
        //TODO Änderungen in dieser Klasse müssen auch in der entsprechenden Klasse im IaApi Lib durchgeführt werden, da diese Klasse hier eine Kopie derjenigen im IaApi ist 
        public event EventHandler UnChecked;
        //TODO Änderungen in dieser Klasse müssen auch in der entsprechenden Klasse im IaApi Lib durchgeführt werden, da diese Klasse hier eine Kopie derjenigen im IaApi ist 
        protected bool _is_checked;
        //TODO Änderungen in dieser Klasse müssen auch in der entsprechenden Klasse im IaApi Lib durchgeführt werden, da diese Klasse hier eine Kopie derjenigen im IaApi ist 
        protected Color _bg_togglepie_color;
        //TODO Änderungen in dieser Klasse müssen auch in der entsprechenden Klasse im IaApi Lib durchgeführt werden, da diese Klasse hier eine Kopie derjenigen im IaApi ist 
        protected Color _bg_togglepie_active_color;
        //TODO Änderungen in dieser Klasse müssen auch in der entsprechenden Klasse im IaApi Lib durchgeführt werden, da diese Klasse hier eine Kopie derjenigen im IaApi ist 
        protected Color _border_pie_checked_color;
        //TODO Änderungen in dieser Klasse müssen auch in der entsprechenden Klasse im IaApi Lib durchgeführt werden, da diese Klasse hier eine Kopie derjenigen im IaApi ist 
        protected Brush _pie_checked_bg_brush;
        //TODO Änderungen in dieser Klasse müssen auch in der entsprechenden Klasse im IaApi Lib durchgeführt werden, da diese Klasse hier eine Kopie derjenigen im IaApi ist 
        protected Brush _pie_checked_stroke_brush;
        //TODO Änderungen in dieser Klasse müssen auch in der entsprechenden Klasse im IaApi Lib durchgeführt werden, da diese Klasse hier eine Kopie derjenigen im IaApi ist 
        public static Color DefaultColorTogglePieBg = Color.FromArgb(0x55, 0xff, 0, 0);
        //TODO Änderungen in dieser Klasse müssen auch in der entsprechenden Klasse im IaApi Lib durchgeführt werden, da diese Klasse hier eine Kopie derjenigen im IaApi ist 
        public static Color DefaultColorTogglePieBgActive = Color.FromArgb(0x55, 0xff, 0, 0);
        //TODO Änderungen in dieser Klasse müssen auch in der entsprechenden Klasse im IaApi Lib durchgeführt werden, da diese Klasse hier eine Kopie derjenigen im IaApi ist 
        public static Color DefaultColorBorderPieChecked = Colors.White;
    }
}
