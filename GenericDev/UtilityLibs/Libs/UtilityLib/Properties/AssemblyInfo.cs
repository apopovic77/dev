﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Logicx.UtilitiesLib")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("logicx consulting & workflow integration gmbh")]
[assembly: AssemblyProduct("Logicx.UtilitiesLib")]
[assembly: AssemblyCopyright("Copyright © 2007-2011 by logicx")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("be0764f0-162e-48f5-9363-eccc85ad1d48")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers 
// [assembly: AssemblyVersion("2.0.0.10")]
[assembly: AssemblyVersion("2.0.0.19")]
[assembly: AssemblyFileVersion("2.0.0.19")]
