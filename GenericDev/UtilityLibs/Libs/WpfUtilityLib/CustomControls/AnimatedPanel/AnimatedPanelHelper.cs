using System.Diagnostics;

namespace Logicx.WpfUtility.CustomControls.AnimatedPanel
{
	#region #using Directives

	using System;
	using System.Collections.Generic;
	using System.Windows;
	using System.Windows.Media;
	using System.Windows.Media.Animation;
	using System.Windows.Threading;

	#endregion

	internal class AnimatedPanelHelper
	{
        public static void ArrangeChild(UIElement panel, UIElement element, Rect finalRect, Duration duration)
        {
            ArrangeChild(panel, element, finalRect, duration, true);
        }

		public static void ArrangeChild(UIElement panel, UIElement element, Rect finalRect, Duration duration, bool new_element_animation)
		{
		    Duration dur = duration;
			var trans = element.RenderTransform as TranslateTransform;                
            
            // keep translation point and store it in its Tag property for rendering later.
            var translatePosition = finalRect.TopLeft;
            element.Arrange(new Rect(finalRect.Size));
            translatePosition.Y *= -1;

			// create new Translate Transform
			if (trans == null)
			{
				trans = new TranslateTransform();
				element.RenderTransform = trans;
                // wenn keine Startanimation gew�nscht ist, wird das Element an die Endposition gesetzt
                if (!new_element_animation)
                {
                    dur = new Duration(new TimeSpan(0));
                    trans.X = translatePosition.X;
                    trans.Y = translatePosition.Y;
                }
			}
            
			// tell the child element to arrange itself
			//element.Arrange(new Rect(finalRect.Size));

			panel.Dispatcher.BeginInvoke(new Action<FrameworkElement, KeyValuePair<Point, Duration>>(BeginAnimation), DispatcherPriority.Render, element,
			                             new KeyValuePair<Point, Duration>(translatePosition, dur));
		}

		private static void BeginAnimation(FrameworkElement element, KeyValuePair<Point, Duration> translatePositionAndDuration)
		{
			var xKeyFrame = new SplineDoubleKeyFrame
			                	{
			                		KeyTime = KeyTime.FromPercent(1.0),
			                		Value = translatePositionAndDuration.Key.X
			                	};
			var yKeyFrame = new SplineDoubleKeyFrame
			                	{
			                		KeyTime = KeyTime.FromPercent(1.0),
			                		Value = translatePositionAndDuration.Key.Y
			                	};
			var animationX = new DoubleAnimationUsingKeyFrames
			                 	{
			                 		Duration = translatePositionAndDuration.Value,
			                 		AccelerationRatio = 0.5,
			                 		DecelerationRatio = 0.5,
			                 		KeyFrames = new DoubleKeyFrameCollection
			                 		            	{
			                 		            		xKeyFrame,
			                 		            		xKeyFrame
			                 		            	}
			                 	};
			var animationY = new DoubleAnimationUsingKeyFrames
			                 	{
			                 		Duration = translatePositionAndDuration.Value,
			                 		AccelerationRatio = 0.5,
			                 		DecelerationRatio = 0.5,
			                 		KeyFrames = new DoubleKeyFrameCollection
			                 		            	{
			                 		            		yKeyFrame,
			                 		            		yKeyFrame
			                 		            	}
			                 	};

			// hook up X and Y translate transformations
			element.RenderTransform.BeginAnimation(TranslateTransform.XProperty, animationX, HandoffBehavior.Compose);
			element.RenderTransform.BeginAnimation(TranslateTransform.YProperty, animationY, HandoffBehavior.Compose);
		}
	}
}