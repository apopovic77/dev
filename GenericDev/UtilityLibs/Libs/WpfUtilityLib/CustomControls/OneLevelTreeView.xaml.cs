﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Logicx.WpfUtility.CustomControls.HelpProvider;

namespace Logicx.WpfUtility.CustomControls
{
    
    /// <summary>
    /// Interaktionslogik für OneLevelTreeView.xaml
    /// </summary>
    public partial class OneLevelTreeView : UserControl
    {
        public OneLevelTreeView()
        {
            InitializeComponent();
        }

        #region Public Methods
        /// <summary>
        /// zum Setzen des darzustellenden Baumes
        /// </summary>
        /// <param name="root"></param>
        public void SetTreeSource(IOneLevelTreeViewItem root)
        {
            Monitor.Enter(this);
            ClearTreeData();

            // begin recursive tree creation
            CreateNode(root, _root_level);
            main_grid.Children.Add(_root_level);
            CheckBackButtonNavigation();
            Monitor.Exit(this);
        }

        /// <summary>
        /// Dienst zum Navigieren im aktuellen gesetzten Baum
        /// </summary>
        /// <param name="to_select_item"></param>
        public void JumpToMenuEntry(IOneLevelTreeViewItem to_select_item)
        {
            if (to_select_item == null || _element_in_container.Count == 0)
                return;

            if (_element_in_container.ContainsKey(to_select_item))
            {
                if (!SetElementActive(to_select_item))
                {
                    JumpToMenuEntry(_element_in_container[to_select_item]);
                    SetElementActive(to_select_item);
                }
            }

        }

        public void SimulateItemMouseClick(IOneLevelTreeViewItem to_select_item)
        {
            if(to_select_item == null)
                return;

            if(_element_to_item.ContainsKey(to_select_item))
            {
                cur_child_MouseUp(_element_to_item[to_select_item], null);
            }
            
        }
        #endregion 
                
        #region Tree Creation
        /// <summary>
        /// recursive Methode die den Baum durchwandert, und alle Kinder einer Ebene in ein Stackpanel darstellt.
        /// </summary>
        /// <param name="tree"></param>
        /// <param name="levelcontainer"></param>
        protected void CreateNode(IOneLevelTreeViewItem tree, StackPanel levelcontainer)
        {
            for (int i = 0; i < tree.ChildrenList.Count; i++)
            {
                IOneLevelTreeViewItem cur_h_node = tree.ChildrenList[i];
                OneLevelTreeViewItem cur_child = new OneLevelTreeViewItem(cur_h_node);
                _element_to_item.Add(cur_h_node, cur_child);
                cur_child.MouseUp += new MouseButtonEventHandler(cur_child_MouseUp);
                levelcontainer.Children.Add(cur_child);
                StackPanel next_level = new StackPanel();
                
                // Speicherung der Baumstruktur und zugehörige Stackpanels in den dictonaries
                _element_to_lower_level.Add(cur_child, next_level);
                _element_in_container.Add(cur_h_node, levelcontainer);
                _container_to_root_container.Add(next_level, levelcontainer);

                // Parent Knoten in StackPanel aufnehmen. Dieser soll zu Begin markiert sein
                OneLevelTreeViewItem root_elem = new OneLevelTreeViewItem(cur_h_node);
                _container_root_elements.Add(root_elem);
                root_elem.MouseUp += new MouseButtonEventHandler(cur_child_MouseUp);
                next_level.Children.Add(root_elem);
                root_elem.Margin = new Thickness(0, 10, 0, 10);
                root_elem.SetActive();

                // Rekursion für Kind beginnt
                CreateNode(tree.ChildrenList[i], next_level);
            }
        }
        #endregion

        #region Navigation Helper Methods
        /// <summary>
        /// Löscht alle Informationen im Baum, die bei einer Baumerzeugung erstellt wurden
        /// </summary>
        protected void ClearTreeData()
        {
            main_grid.Children.Clear();
            _element_to_item.Clear();
            _element_in_container.Clear();
            _element_to_lower_level.Clear();
            _container_to_root_container.Clear();
            _container_root_elements.Clear();
        }   

        /// <summary>
        /// navigiert zu einem bestimmten Inhalt im Baum.
        /// Die Kinder werden alle in einem StackPanel verwaltet und dort dargestellt. 
        /// Wenn also zu einem anderen Knoten gesprungen wird, wird der gesamte StackPanel ausgetauscht
        /// </summary>
        /// <param name="new_content"></param>
        protected void JumpToMenuEntry(StackPanel new_content)
        {
            if (main_grid.Children.Contains(new_content))
                return;

            // mögliche aktivierte Elemente werden deaktiviert
            SetAllItemsInactive(new_content, true);
            // der neue darzustellende Inhalt wird unsichtbar über den alten Inhalt gelegt
            new_content.Visibility = Visibility.Collapsed;
            main_grid.Children.Add(new_content);

            // der alte Inhalt wird mit einer Animation ausgeblendet
            Storyboard sb = (Storyboard)FindResource("anim_myMainGrid_fade_out");
            sb.Completed += new EventHandler(sb_Completed);
            sb.Begin(this);
        }

        /// <summary>
        /// Sucht im akutellen Grid, das übergebene Element und schaut ob es dort enthalten ist
        /// Wenn nicht wird false zurückgegeben. Anderenfalls wird true zurückgegeben und das Element wird aktiviert
        /// </summary>
        /// <param name="to_select_item"></param>
        /// <returns></returns>
        protected bool SetElementActive(IOneLevelTreeViewItem to_select_item)
        {
            int index = -1;
            if (main_grid.Children.Count == 1 && main_grid.Children[0] == _element_in_container[to_select_item])
                index = 0;
            else if(main_grid.Children.Count == 2 && main_grid.Children[1] == _element_in_container[to_select_item])
                index = 1;

            if(index == -1)
                return false;

            foreach(UIElement ui_elem in ((StackPanel)main_grid.Children[index]).Children)
            {
                if (ui_elem is OneLevelTreeViewItem)
                {
                    if (((OneLevelTreeViewItem) ui_elem).GetRefTreeViewItem == to_select_item)
                        ((OneLevelTreeViewItem) ui_elem).SetActive();
                    else
                        ((OneLevelTreeViewItem) ui_elem).SetInactive();
                }
            }
            return true;
        }

        /// <summary>
        /// Alle Elemente eines StackPanels werden deaktiviert
        /// </summary>
        /// <param name="sp"></param>
        private void SetAllItemsInactive(StackPanel sp, bool let_root_active)
        {
            foreach(UIElement ui in sp.Children)
            {
                if (ui is OneLevelTreeViewItem)
                {
                    if (_container_root_elements.Contains((OneLevelTreeViewItem)ui) && let_root_active)
                        ((OneLevelTreeViewItem)ui).SetActive();
                    else
                        ((OneLevelTreeViewItem) ui).SetInactive();
                }
            }
        }        
        
        /// <summary>
        /// überprüft, ob der Zurückbutton für die aktuelle Ebene benötigt wird, und blendet diesen gegebenenfalls ein
        /// </summary>
        private void CheckBackButtonNavigation()
        {
            if (main_grid.Children.Count > 0 && main_grid.Children[0] == _root_level)
                back_navi.Visibility = Visibility.Collapsed;
            else
                back_navi.Visibility = Visibility.Visible;
        }        

        /// <summary>
        /// Sucht zu einem bestimmten StackPanel das Element, das auf diesen zeigt.
        /// </summary>
        /// <param name="ref_panel"></param>
        private OneLevelTreeViewItem FindSelectedValue(StackPanel ref_panel)
        {
            try
            {
                if (ref_panel != null && _container_to_root_container.ContainsKey(ref_panel))
                {
                    StackPanel parent_sp = _container_to_root_container[ref_panel];
                    if (_element_to_lower_level.ContainsValue(parent_sp))
                    {
                        KeyValuePair<OneLevelTreeViewItem, StackPanel> ref_tree_item = _element_to_lower_level.Where(v => v.Value == parent_sp).First();
                        return ref_tree_item.Key;
                    }
                }
            }catch(Exception ex)
            {}

            return null;
        }
        #endregion

        #region AnimationCompleted Events
        /// <summary>
        /// die Animation, den alten Inhalt auszublenden wurde erfolgreich beendet.
        /// Nun wird der alte Inhalt aus dem Grid entfernt und der neue mit einer Animation dargestellt.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void sb_Completed(object sender, EventArgs e)
        {
            ((ClockGroup)sender).Completed -= sb_Completed;
            if (main_grid.Children.Count > 1)
            {
                SetAllItemsInactive((StackPanel) main_grid.Children[0], false);
                main_grid.Children.RemoveAt(0);
                main_grid.Children[0].Visibility = Visibility.Visible;
                CheckBackButtonNavigation();

                Storyboard fade_in = (Storyboard)FindResource("anim_myMainGrid_fade_in");
                fade_in.Completed += new EventHandler(fade_in_Completed);
                fade_in.Begin(this);

                if (((StackPanel)main_grid.Children[0]).ActualHeight == 0)
                {
                    ((StackPanel) main_grid.Children[0]).LayoutUpdated += new EventHandler(OneLevelTreeView_LayoutUpdated);
                    _last_height = main_grid.ActualHeight;
                }
                else
                    StartHeightAnimation(((StackPanel)main_grid.Children[0]).ActualHeight);
            }
            else if(main_grid.Opacity==0)
            {
                Storyboard fade_in = (Storyboard)FindResource("anim_myMainGrid_fade_in");
                fade_in.Completed += new EventHandler(fade_in_Completed);
                fade_in.Begin(this);
            }
        }

        void OneLevelTreeView_LayoutUpdated(object sender, EventArgs e)
        {
            if (((StackPanel)main_grid.Children[0]) != null)
            {
                ((StackPanel)main_grid.Children[0]).LayoutUpdated -= OneLevelTreeView_LayoutUpdated;
                StartHeightAnimation(_last_height, ((StackPanel)main_grid.Children[0]).ActualHeight); 
            }
        }

        void StartHeightAnimation(double oldheight, double newheight)
        {
            if (newheight == 0)
                return;
            DoubleAnimation _grid_height_anim = new DoubleAnimation();
            _grid_height_anim.From = oldheight;
           
            _grid_height_anim.To = newheight;

            //_grid_height_anim.DecelerationRatio = 1;
            ////TODO: Die Zeit sollte abhängig von den dargestellten Elementen sein
            _grid_height_anim.Duration = new Duration(TimeSpan.FromMilliseconds(500));
            _grid_height_anim.FillBehavior = FillBehavior.HoldEnd;


            main_grid.BeginAnimation(FrameworkElement.HeightProperty, _grid_height_anim, HandoffBehavior.SnapshotAndReplace);
        }

        void StartHeightAnimation(double newheight)
        {
            StartHeightAnimation(main_grid.ActualHeight, newheight);
        }
        /// <summary>
        /// Die Animation, den neuen Inhalt einzublenden wurde erfolgreich beendet
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void fade_in_Completed(object sender, EventArgs e)
        {
            ((ClockGroup)sender).Completed -= fade_in_Completed;
        }
        #endregion
            
        #region EventHandler
        /// <summary>
        /// Der Navigation-Back Button wurde gedrückt
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (main_grid.Children.Count > 0 && _container_to_root_container.ContainsKey((StackPanel)main_grid.Children[0]))
            {
                StackPanel cur_level = (StackPanel)main_grid.Children[0];
                StackPanel next_level = _container_to_root_container[cur_level];
                //main_grid.Children.RemoveAt(0);
                //main_grid.Children.Add(next_level);
                //CheckBackButtonNavigation();
                JumpToMenuEntry(next_level);
                OneLevelTreeViewItem found_item = FindSelectedValue(next_level);
                if (found_item != null)
                    SendSelectionChanged(found_item);
                else
                    SendSelectionRemoved();
            }
        }        
        
        /// <summary>
        /// Auf ein Menüitem wurde geklickt. Wenn das Element Kinder hat, werden diese anschließend angezeigt.
        /// Anderenfalls wird ein Event geworfen, das ein Blattknoten erreicht wurde
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void cur_child_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (sender == null)
                return;

            if(sender is OneLevelTreeViewItem && !((OneLevelTreeViewItem)sender).IsSetActive)
            {
                if( _element_to_lower_level.ContainsKey((OneLevelTreeViewItem)sender))
                {
                    // zu welchem Element gesprungen werden soll
                    StackPanel next_level = _element_to_lower_level[(OneLevelTreeViewItem) sender];

                    // Element hat kinder. Es wird zu neuem Menüeintrag gesprungen. Hier wird ab größer 1 abgefragt, da in jedem StackPanel, 
                    // außer dem Root-Verzeichnis der zugehörige Menüeintrag drin ist
                    if (next_level.Children.Count > 1)
                    {
                        StackPanel new_content = _element_to_lower_level[(OneLevelTreeViewItem) sender];
                        JumpToMenuEntry(new_content);
                        SendSelectionChanged((OneLevelTreeViewItem) sender);
                    }
                    // Element hat keine Kinder. Element wird aktiviert
                    else                    
                    {
                        if (_element_in_container.ContainsKey(((OneLevelTreeViewItem)sender).GetRefTreeViewItem))
                        {
                            StackPanel ref_panel = _element_in_container[((OneLevelTreeViewItem) sender).GetRefTreeViewItem];
                            if(main_grid.Children[0] != ref_panel)
                                JumpToMenuEntry((ref_panel));
                        }
                        SetAllItemsInactive((StackPanel) main_grid.Children[0], false);
                        ((OneLevelTreeViewItem) sender).SetActive();
                        SendSelectionChanged((OneLevelTreeViewItem) sender);
                    }
                }
                else if(_container_root_elements.Contains((OneLevelTreeViewItem)sender))
                {
                    SetAllItemsInactive((StackPanel)main_grid.Children[0], false);
                    ((OneLevelTreeViewItem)sender).SetActive();
                    SendSelectionChanged((OneLevelTreeViewItem)sender);
                }
            }
        }
        #endregion

        #region InformListeners
        private void SendSelectionChanged(OneLevelTreeViewItem selectedTreeViewItem)
        {
            if (selectedTreeViewItem != null && SelectionChanged != null)
                SelectionChanged(selectedTreeViewItem.GetRefTreeViewItem, new EventArgs());
        }

        private void SendSelectionRemoved()
        {
            if (SelectionRemoved != null)
                SelectionRemoved(this, new EventArgs());

        }
        #endregion

        #region Attributes
        public event EventHandler SelectionChanged;
        public event EventHandler SelectionRemoved;
       
        protected DoubleAnimation _grid_height_anim = new DoubleAnimation();
        protected StackPanel _root_level = new StackPanel();

        protected Dictionary<IOneLevelTreeViewItem, StackPanel> _element_in_container = new Dictionary<IOneLevelTreeViewItem, StackPanel>();
        protected Dictionary<OneLevelTreeViewItem, StackPanel> _element_to_lower_level = new Dictionary<OneLevelTreeViewItem, StackPanel>();
        protected Dictionary<StackPanel, StackPanel> _container_to_root_container = new Dictionary<StackPanel, StackPanel>();
        protected Dictionary<IOneLevelTreeViewItem, OneLevelTreeViewItem> _element_to_item = new Dictionary<IOneLevelTreeViewItem, OneLevelTreeViewItem>();
        protected List<OneLevelTreeViewItem> _container_root_elements = new List<OneLevelTreeViewItem>();

        protected double _last_height;
        #endregion

    }
}
