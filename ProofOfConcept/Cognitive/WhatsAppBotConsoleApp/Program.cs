﻿using System;
using Awanto.WhatsAppCore.Services;

namespace WhatsAppBotConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            RunBot();
            Console.WriteLine("Cognitive bot Running!");
            Console.ReadLine();
            _bot.Stop();
        }

        static void RunBot()
        {
            _bot = new CognitiveWhatsAppBot();
            _bot.Start();
        }

        private static CognitiveWhatsAppBot _bot;
    }
}
