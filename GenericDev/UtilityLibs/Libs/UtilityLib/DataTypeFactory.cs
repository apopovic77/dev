using System;
using System.Drawing;
using System.Text.RegularExpressions;


namespace Logicx.Utilities
{
	/// <summary>
	/// 
	/// </summary>
	public class DataTypeFactory
    {
        #region Hex2Color
        /// <summary>
        /// Convert a hex string to a .NET Color object.
        /// </summary>
        /// <param name="hexColor">a hex string: "FFFFFF", "#000000"</param>
        public static Color Hex2Color(string hexColor)
        {
            string hc = ExtractHexDigits(hexColor);
            if (hc.Length != 6)
            {
                // you can choose whether to throw an exception
                //throw new ArgumentException("hexColor is not exactly 6 digits.");
                return Color.Empty;
            }
            string r = hc.Substring(0, 2);
            string g = hc.Substring(2, 2);
            string b = hc.Substring(4, 2);
            Color color = Color.Empty;
            try
            {
                int ri
                   = Int32.Parse(r, System.Globalization.NumberStyles.HexNumber);
                int gi
                   = Int32.Parse(g, System.Globalization.NumberStyles.HexNumber);
                int bi
                   = Int32.Parse(b, System.Globalization.NumberStyles.HexNumber);
                color = Color.FromArgb(ri, gi, bi);
            }
            catch
            {
                // you can choose whether to throw an exception
                //throw new ArgumentException("Conversion failed.");
                return Color.Empty;
            }
            return color;
        }
        /// <summary>
        /// Extract only the hex digits from a string.
        /// </summary>
        public static string ExtractHexDigits(string input)
        {
            // remove any characters that are not digits (like #)
            Regex isHexDigit
               = new Regex("[abcdefABCDEF\\d]+", RegexOptions.Compiled);
            string newnum = "";
            foreach (char c in input)
            {
                if (isHexDigit.IsMatch(c.ToString()))
                    newnum += c.ToString();
            }
            return newnum;
        }
        #endregion  
		public static String Double2String(double value)
		{
			return value.ToString().Replace(",",".");
		}
//	public static object getS3DataObject(string value,long dt_id){
//		switch((int)dt_id){
//			case 1:
//			case 2:
//				return value;
//			case 3:
//				return new long Long(value);
//			case 4:
//				return new Float(value);
//			case 5:
//				return new Bool(value);
//			default:
//				throw new TranspException(37);
//		}
//	}
	
//	public static Object getS3DataObject(String value) throws S3DBException{
//		int dt_id = getS3DataType(value);
//		switch((int)dt_id){
//			case 1:
//			case 2:
//				return value;
//			case 3:
//				return new Long(value);
//			case 4:
//				return new Float(value);
//			case 5:
//				return new Boolean(value);
//			default:
//				throw new S3DBException(37);
//		}
//	}
	
//	/** tries to detect der correct data type of the value given by string
//	 * returns on successful detection the correct datatype */
//	public static int getS3DataType(String value){
//		try{
//			if(value.toLowerCase().equals("true"))
//				return 5;
//			if(value.toLowerCase().equals("false"))
//				return 5;
//		}catch(Exception e){}
//		try{
//			if(value.indexOf(".")>0){
//				Float f = new Float(value);
//				return 4;
//			}
//		}catch(Exception e){}
//		try{
//			Long l = new Long(value);
//			return 3;
//		}catch(Exception e){}
//		return 2;
//	}

		public static String GetFormatedTime(TimeSpan t)
		{
            return String.Format("{0:00}:{1:00}:{2:00}",t.Hours,t.Minutes,t.Seconds);
		}

        public static string GetFormatedTimeHours(TimeSpan t) {
            return String.Format("{0:0.0}h", t.TotalHours);
        }
        public static string GetFormatedTimeMins(TimeSpan t)
        {
            return String.Format("{0:0.0}min", t.TotalMinutes);
        }

		public static string DateTime2MySQLDateTimeString(DateTime dt){
			return String.Format("{0}-{1:00}-{2:00} {3:00}:{4:00}:{5:00}.{6}",dt.Year,dt.Month,dt.Day,dt.Hour,dt.Minute,dt.Second,dt.Millisecond);
		}
		public static string DateTime2MySQLDateString(DateTime dt)
		{
			return String.Format("{0}-{1:00}-{2:00}",dt.Year,dt.Month,dt.Day);
		}
	
		public static bool GetBool(object value){
			if(value is DBNull || value == null)
				return false;

			if(value is bool)
				return (bool)value;

			if(value is string){
				if((string)value == "true")
					return true;
				if((string)value == "false")
					return false;
				if((string)value == "0")
					return false;
				if((string)value == "1")
					return true;
			}

			if(value is Decimal){
				value = Convert.ToInt32(value);
			}

			if(value is int){
				int iValue = (int)value;
				if(iValue == 0)
					return false;
				else if (iValue == 1)
					return true;
			}


            throw new UtilityException("cannot convert to boolean");

		}

	public static uint GetUInt32(object value){
		try{
			return Convert.ToUInt32(value);
		}catch{
			return 0;
		}
	}
		public static int GetInt(object value)
		{
			try
			{
				return Convert.ToInt32(value);
			}
			catch
			{
				return 0;
			}
		}

        public static string GetString(object value)
        {
            try
            {
                return Convert.ToString(value);
            }
            catch
            {
                return "";
            }
        }

	public static double GetDouble(object value){
		try{
			return Convert.ToDouble(value);
		}catch{
			return 0;
		}
	}
        public static float GetFloat(object value)
        {
            try
            {
                return Convert.ToSingle(value);
            }
            catch
            {
                return 0;
            }
        }
	public static ulong GetUInt64(object value){
		try{
			return Convert.ToUInt64(value);
		}catch{
			return 0;
		}
	}

        public static object GetObject(object value) {
            return value;
        }

	public static ulong GetULong(object value){
//		if(value is MySql.Data.MySqlClient. BigDecimal)
//			return ((BigDecimal)value).longValue();
//		else 

        if (value is Int16 || value is UInt16 || value is Int32 || value is UInt32)
        {
            try
            {
                ulong ret = 0;
                ret = Convert.ToUInt64(value);
                return ret;
            }
            catch { }
        }else if(value is ulong)
		{
			return (ulong)value;
		}
		else if(value is long)
		{
			return Convert.ToUInt64(value);
		}
		else if(value is Int64)
		{
			return Convert.ToUInt64(value);	
		}
		else if(value is long)
			return Convert.ToUInt64(value);
		else if(value is Decimal)
			return Convert.ToUInt64(value);
		else if(value is string)
		{
			if((string)value == "0")
				return 0;
			try
			{
				ulong l = Convert.ToUInt64(value);
				return l;
			}
			catch
			{
				//throw new TranspException(39);
				return 0;
			}
		}

		return 0;
	}
//	public static int getInt(Object value){
//		if(value instanceof BigDecimal){
//			return ((BigDecimal)value).intValue();
//		}
//		else if(value instanceof Long){
//			return ((Long)value).intValue();
//		}
//		else if(value instanceof Integer){
//			return ((Integer)value).intValue();
//		}
//		else if(value instanceof String){
//			return Integer.parseInt(value.toString());
//		}else
//			return 0;
//	}
//	
//	public static String getString(long value){
//		try{
//			if(value!=0)
//				return String.valueOf(value);
//		}catch(Exception e){}
//		return "null";
//
//	}

}
}
