﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BankLib.Data;

namespace BankOverviewApp
{
    /// <summary>
    /// Interaction logic for FilterControl.xaml
    /// </summary>
    public partial class FilterControl : UserControl
    {
        public FilterControl()
        {
            InitializeComponent();


            List<Konto> konten = new List<Konto>(Konten.AlleKonten);
            konten.Insert(0, new Konto() { Owner = "Alle" });
            kto_cb.SelectionChanged += kto_listbox_SelectionChanged;
            kto_cb.DataContext = konten;
            kto_cb.SelectedValue = konten[0];

            zeitraum_cb.DataContext = GetZeitraumListe();
            zeitraum_cb.SelectionChanged += zeitraum_cb_SelectionChanged;
            zeitraum_cb.SelectedIndex = 0;
        }


        public object GetZeitraumListe()
        {

            

            //Konto sel_konto = Konten.AlleKonten[0];
            //if (kto_cb.SelectedValue != null)
            //    sel_konto = ((Konto)kto_cb.SelectedValue);



            //var monate = from b in Buchungen.AlleBuchungen
            //             //where b.KtoNummer == sel_konto.KtoNummer
            //             select new { Month = b.BuchungsDatum.Month, Year = b.BuchungsDatum.Year };

            List<string> zeitraum_liste = Buchungen.GetBuchungsMonate().Select(b => b.ToString("MMM") + " " + b.Year).ToList();
            zeitraum_liste.Insert(0, "Alle Buchungen");
            return zeitraum_liste;
        }


        public DateTime? GetSelectedMonYear()
        {
            if (zeitraum_cb.SelectedValue == null || zeitraum_cb.SelectedIndex == 0)
                return null;
            return DateTime.ParseExact((string)zeitraum_cb.SelectedValue, "MMM yyyy", null);
        }


        void zeitraum_cb_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //FilterSortGrid();

            DateTime? date = GetSelectedMonYear();
            if (date != null)
            {
                moninfo_basestack.Visibility = Visibility.Visible;
                moninfo_stack.Children.Clear();
                KontenOverview kto_overview_mon = new KontenOverview(date.Value);
                moninfo_stack.Children.Add(kto_overview_mon);
                monyearchoosen_tb.Text = date.Value.ToString("MMMM yyyy");
            }
            else
            {
                moninfo_stack.Children.Clear();
                moninfo_basestack.Visibility = Visibility.Collapsed;
            }

            OnFilterSortChanged();
        }

        void kto_listbox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            OnFilterSortChanged();
        }

        public List<Buchung> GetFilteredBuchungen()
        {
            DateTime? date = GetSelectedMonYear();

            if (SelectedKonto.Owner != "Alle")
            {
                if (date == null)
                    return Buchungen.AlleBuchungen.Where(b => b.KtoNummer == SelectedKonto.KtoNummer).ToList();
                else
                    return Buchungen.AlleBuchungen.Where(b => b.KtoNummer == SelectedKonto.KtoNummer && b.Datum.Month == date.Value.Month && b.Datum.Year == date.Value.Year).ToList();
            }
            else
            {
                if (date == null)
                    return Buchungen.AlleBuchungen.ToList();
                else
                    return Buchungen.AlleBuchungen.Where(b => b.Datum.Month == date.Value.Month && b.Datum.Year == date.Value.Year).ToList();
            }
        }

        public Konto SelectedKonto
        {
            get
            {
                return ((Konto) kto_cb.SelectedValue);
            }
        }

        protected virtual void OnFilterSortChanged()
        {
            EventHandler handler = FilterSortChanged;
            if (handler != null) handler(this, EventArgs.Empty);
        }

        public event EventHandler FilterSortChanged;
    }
}
