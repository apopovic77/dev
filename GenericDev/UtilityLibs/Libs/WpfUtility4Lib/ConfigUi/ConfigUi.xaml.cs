﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Logicx.Utility;

namespace Logicx.WpfUtility4.ConfigUi
{
    /// <summary>
    /// Interaction logic for ConfigUi.xaml
    /// </summary>
    public partial class ConfigUi : UserControl
    {
        public ConfigUi(List<SystemConfig> system_configs, string config_filter_prefix = "cui_", SplitType split_type = SplitType.Underscore)
        {
            InitializeComponent();

            _split_type = split_type;
            _filter_prefix = config_filter_prefix;
            _system_configs = system_configs;

            InitControl();
        }

        private void InitControl()
        {
            foreach (SystemConfig system_config in _system_configs)
            {
                InitControl(system_config);
            }
        }

        public enum SplitType
        {
            Underscore,
            FirstUpperCamelCaseChar
        }

        private void InitControl(SystemConfig system_config)
        {
            List<string> cui_configs;
            if (string.IsNullOrEmpty(_filter_prefix))
                cui_configs = system_config.ConfigProperties.Keys.ToList();
            else
                cui_configs = system_config.ConfigProperties.Keys.Where(k => k.StartsWith("cui_")).ToList();
            cui_configs.Sort();

            List<string[]> split_arr = new List<string[]>();
            if (_split_type == SplitType.Underscore)
            {
                foreach (string cui_config in cui_configs)
                {
                    string[] split_key = cui_config.Split("_".ToCharArray(), 3);
                    split_arr.Add(split_key);
                }
            }
            else
            {
                foreach (string cui_config in cui_configs)
                {
                    var query_index = from ch in cui_config.Substring(1).ToArray()
                        where Char.IsUpper(ch)
                        select cui_config.Substring(1).IndexOf(ch);
                    int index = query_index.FirstOrDefault();
                    if (index == 0)
                    {
                        split_arr.Add(new[] { cui_config });
                    }
                    else
                    {
                        string[] split_key = new string[] { cui_config .Substring(0,index+1), cui_config.Substring(index+1)};
                        split_arr.Add(split_key);
                    }
                }
            }
            var query = from k in split_arr
                        group k by k[1]
                        into k_group
                        select k_group;

            Expander exp = new Expander();
            exp.Header = new TextBlock() { FontSize = 12, FontFamily = new FontFamily("Arial Narrow"), Foreground = Brushes.Black, Background = Brushes.White, Margin = new Thickness(0, 5, 0, 5), Text = system_config.GetType().Name.ToUpper(), HorizontalAlignment = System.Windows.HorizontalAlignment.Left };
            StackPanel stack = new StackPanel();
            exp.Content = stack;
            base_stack.Children.Add(exp);


            foreach (IGrouping<string, string[]> group_key in query)
            {
                string group_name = group_key.Key;
                ConfigUiGroup config_ui_group = new ConfigUiGroup(system_config.GetType().Name, group_name);
                foreach (string[] config_prop_split in group_key)
                {
                    string full_config_prop_name = GetFullConfigPropName(config_prop_split);

                    if (system_config.ConfigProperties[full_config_prop_name] is BoolConfigValue)
                        config_ui_group.AddConfigProperty((BoolConfigValue)system_config.ConfigProperties[full_config_prop_name]);
                    else if (system_config.ConfigProperties[full_config_prop_name] is FloatConfigValue)
                        config_ui_group.AddConfigProperty((FloatConfigValue)system_config.ConfigProperties[full_config_prop_name]);
                    else if (system_config.ConfigProperties[full_config_prop_name] is IntConfigValue)
                        config_ui_group.AddConfigProperty((IntConfigValue)system_config.ConfigProperties[full_config_prop_name]);
                    else if (system_config.ConfigProperties[full_config_prop_name] is StringConfigValue)
                        config_ui_group.AddConfigProperty((StringConfigValue)system_config.ConfigProperties[full_config_prop_name]);
                    else if (system_config.ConfigProperties[full_config_prop_name] is Vector2FloatConfigValue)
                        config_ui_group.AddConfigProperty((Vector2FloatConfigValue)system_config.ConfigProperties[full_config_prop_name]);
                }
                stack.Children.Add(config_ui_group);
            }
        }

        private string GetFullConfigPropName(string[] config_prop_split)
        {
            string res = "";
            foreach (string s in config_prop_split)
            {
                res += s;
                if(_split_type == SplitType.Underscore)
                    res += "_";
            }
            if (_split_type == SplitType.Underscore)
                res = res.Remove(res.Length - 1);
            return res;
        }

        private SplitType _split_type = SplitType.Underscore;
        private string _filter_prefix;
        private List<SystemConfig> _system_configs;
    }
}
