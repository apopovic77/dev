﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Text;
using LoggingLib.Configuration;
using LoggingLib.SystemInterop;

namespace LoggingLib.Data.Finalizers
{
    /// <summary>
    /// Implements a data finalizer which makes sure that only one system info log entry exists 
    /// </summary>
    public class LimitedUserActionDataFinalizer : DataFinalizerBase
    {
        #region Construction and initialization
        /// <summary>
        /// default constructor
        /// </summary>
        public LimitedUserActionDataFinalizer()
        {
            LoggingConfigSection settings = LogManager.Settings;
 
            if(settings.DataFinalizerConfigSettings["limitedUserActionCount"] != null &&
                !string.IsNullOrEmpty(settings.DataFinalizerConfigSettings["limitedUserActionCount"].Value))
            {
                try
                {
                    _max_number_of_user_action_entries = Int32.Parse(settings.DataFinalizerConfigSettings["limitedUserActionCount"].Value);
                }
                catch
                {
                    
                }
            }
        }
        #endregion

        /// <summary>
        /// Will be called before the the item will be inserted in the data context
        /// </summary>
        /// <param name="logEntry">The log entry for this finalizer</param>
        /// <param name="dataContext">Data context which is about to submit its changes back to the database</param>
        /// <returns>Returns true if pre-update operations succeed. If this method returns false, the SubmitChanges() will not be executed.</returns>
        public override bool BeforeInsertItemInDataContext(LogEntry logEntry, LoggingDataContext dataContext)
        {
            bool bRet = true;

            // no action before inster - allow all log entries to be inserted
            // limitation will be applied after submit

            return bRet;
        }

        /// <summary>
        /// Will be called after the submitchanges() method invokation of the given data context
        /// </summary>
        /// <param name="dataContext">Data context which submitted its changes to the database</param>
        /// <returns>Returns true if post-update operations sueceed. If not false will be returned.</returns>
        /// <remarks>You may use the return value of false, for indicating a propblem on the data context and 
        /// rollback or reset a current transaction scope.</remarks>
        public override bool AfterSubmitChanges(LoggingDataContext dataContext)
        {
            bool bRet = true;
            long nChangeCnt = 0;

            lock (dataContext)
            {
                // LinQ query to retrieve all user action log entries in the current data context
                // filter by computer name and current user name
                var query1 = from sysinfolog in dataContext.LogEntries
                             join cat in dataContext.CategoryLogEntries on sysinfolog.LogId equals cat.LogId
                             where cat.CategoryName == LogManagerConstants.SYSCATEGORY_USERACTION &&
                                   sysinfolog.MachineName == ApplicationStatisticHelper.MachineName &&
                                   sysinfolog.DomainLoginName == ApplicationStatisticHelper.CurrentUserName &&
                                   sysinfolog is UserActionLogEntry
                             select sysinfolog as UserActionLogEntry;

                // order results
                var query = from sysloginfo in query1
                            orderby sysloginfo.LogTimestamp descending
                            select sysloginfo;
                try
                {
                    List<UserActionLogEntry> logEntries = query.ToList();
                    long nCnt = 0;

                    foreach (UserActionLogEntry curEntry in logEntries)
                    {
                        nCnt++;

                        if(nCnt > _max_number_of_user_action_entries)
                        {
                            // delete all old entries
                            if (curEntry.CategoryLogEntries != null)
                            {
                                nChangeCnt += curEntry.CategoryLogEntries.Count;
                                dataContext.CategoryLogEntries.DeleteAllOnSubmit(curEntry.CategoryLogEntries);
                            }

                            dataContext.LogEntries.DeleteOnSubmit(curEntry);

                            nChangeCnt++;
                        }
                    }
                }
                catch
                {
                    bRet = false;
                }
            }

            SetChangesMade(nChangeCnt);
            return bRet;
        }

        #region Attribs

        private int _max_number_of_user_action_entries = 500;
        #endregion
    }
}
