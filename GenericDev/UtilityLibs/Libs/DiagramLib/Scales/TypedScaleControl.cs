﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Logicx.DiagramLib.ViewConfig;

namespace Logicx.DiagramLib.Scales
{
    public abstract class TypedScaleControl<T> : ScaleControlBase
    {

        public TypedScaleControl()
        {
            SizeChanged += new SizeChangedEventHandler(TypedScaleControl_SizeChanged);
        }

        void TypedScaleControl_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            base.BaseControlSizeChanged(sender, e);
        }


        /// <summary>
        /// Transforms an axis scale value to the pixel value
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public abstract double TransformAxisValueToPixel(T value);
        public abstract double TransformAxisValueToPixel(T value, bool margin_sensitive);

        /// <summary>
        /// Transforms an axis pixel value to the scale value
        /// </summary>
        /// <param name="pixel"></param>
        /// <returns></returns>
        public abstract T TransformAxisPixelToValue(double pixel);
        public abstract T TransformAxisPixelToValue(double pixel, bool margin_sensitive);


        public override double GenericTransformAxisValueToPixel(object value, bool margin_sensitive)
        {
            return TransformAxisValueToPixel((T)Convert.ChangeType(value, typeof(T)), margin_sensitive);
        }

        public override object GenericTransformAxisPixelToValue(double pixel, bool margin_sensitive)
        {
            return TransformAxisPixelToValue(pixel, margin_sensitive);
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            if (Config != null)
                Config.Dispose();
        }

        public virtual AxisScaleConfig<T> Config
        {
            get { return _config; }
            set
            {
                if (_config != value)
                {
                    _config = value;
                    base.Padding = _config.Padding;
                }
            }
        }

        public abstract bool CanKeyMove
        {
            get;
        }

        public abstract double ScaleUnitSmallInPixel
        {
            get;
        }

        private AxisScaleConfig<T> _config;
    }
}
