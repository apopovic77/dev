using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Mail;
using System.Net.Mime;
using DotNetOpenMail;
using System.IO;

namespace Logicx.Utilities.Email
{
    /// <summary>
    /// Summary description for TransportalHtmlMail
    /// </summary>
    public class HtmlMail : Email
    {
        private EmailMessage mail = new EmailMessage();

        public HtmlMail(string subject, string body, string destination, string from_adr,string from_name, string smtpserver)
            : base(subject, body, from_adr,from_name, destination, smtpserver)
        {
        }

        public string TextBody = "Email requires an html viewable Client";
        public bool ContentTypeOfAttachedBodyIsHtml = true;

        public void AddAttachement(string mime_id,string path,string content_type) {
            //create inline image
            FileInfo relatedFileInfo = new FileInfo(path);
            FileAttachment relatedFileAttachment = new FileAttachment(relatedFileInfo, mime_id);
            relatedFileAttachment.ContentType = content_type;
            //add inline image
            mail.AddRelatedAttachment(relatedFileAttachment);
        }

        public override void Send()
        {
            if (m_from_name != "")
                mail.FromAddress = new EmailAddress(m_from_adr, m_from_name);
            else
                mail.FromAddress = new EmailAddress(m_from_adr);

            mail.AddToAddress(new EmailAddress(m_to));

            mail.Subject = m_Subject;

            mail.TextPart = new TextAttachment(TextBody);
            mail.HtmlPart = new HtmlAttachment(GetMailBody());

            //Add the transportal logo as an attachement
            AddAttachement("01", @"logicx_logo.gif", "image/gif");

            //send mail
            mail.Send(new SmtpServer(base.m_SMTPServer));
        }


        public static string GetMailBody(string content, string subject,bool ContentTypeIsHtml)
        {
            string html = "";
            html += "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n";
            html += "<html xmlns=\"http://www.w3.org/1999/xhtml\">\n";
            html += "<head>\n";
            html += "    <title>Logicx Mail Service</title>\n";
            html += "    <meta http-equiv=Content-Type content=\"text/html; charset=utf-8\">\n";
            html += "    <style>\n";
            html += "    <!--\n";
            html += "        body{\n";
            html += "            margin:0px;\n";
            html += "            padding:0px;\n";
            html += "        }\n";
            html += "        body.mailstyle{\n";
            html += "            margin:0px;\n";
            html += "            padding:0px;\n";
            html += "        }\n";
            html += "        p.formatted{\n";
            html += "           font-family:Arial;\n";
            html += "           font-size:9pt;\n";
            html += "        }\n";
            html += "        div.emailbox{\n";
            html += "            margin:0px;\n";
            html += "            padding:0px;\n";
            html += "            width:100%;\n";
            html += "            height:100%;\n";
            html += "        }\n";
            html += "        div.subject{\n";
            html += "            margin:0px;\n";
            html += "            padding:0px;\n";
            html += "            width:100%;\n";
            html += "        }       \n";
            html += "        td{";
            html += "            font-family:Arial;";
            html += "            font-size:9pt;";
            html += "        }    ";
            html += "        td.header{\n";
            html += "            background-color: #636466; \n";
            html += "            letter-spacing: 0.3em;\n";
            html += "        } \n";
            html += "        td.subjecttd{\n";
            html += "            height:29px;\n";
            html += "            background-color:#fbfaf8;\n";
            html += "            width:100%;  \n";
            html += "            margin:0px;\n";
            html += "            padding-left:30px;                     \n";
            html += "        }\n";
            html += "    -->\n";
            html += "    </style> \n";
            html += "</head>\n";
            html += "<body class=\"mailstyle\">\n";
            html += "    <div class=\"emailbox\">\n";
            html += "        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" height=\"100%\">\n";
            html += "            <tr>\n";
            html += "                <td width=\"100%\" height=\"76\" class=\"header\" align=\"right\">\n";
            html += "                    <font color=\"#d1d3d4\" face=\"Arial\" size=\"16\"><b><img src=\"cid:01\" alt=\"transportal&#174;\" /></b></font>\n";
            html += "                </td>\n";
            html += "            </tr>\n";
            html += "            <tr>\n";
            html += "                <td class=\"subjecttd\">\n";
            html += "                    <div class=\"subject\">\n";
            html += "                        <font color=\"#5F5F5F\" face=\"Arial\" size=\"3\"><b>" + subject + "</b></font>\n";
            html += "                    </div>\n";
            html += "                </td>\n";
            html += "            </tr>\n";
            html += "            <tr>\n";
            html += "                <td style=\"height: 100%; padding: 30px 30px 30px 30px;\">\n";
            html += ((ContentTypeIsHtml) ? "" : "<pre>") + content + ((ContentTypeIsHtml) ? "" : "</pre>");
            html += "                </td>\n";
            html += "            </tr>\n";
            html += "        </table>\n";
            html += "    </div>\n";
            html += "</body>\n";
            html += "</html>\n";
            return html;
        }

        public string GetMailBody()
        {
            return GetMailBody(m_Content, m_Subject,ContentTypeOfAttachedBodyIsHtml);
        }

    }
}