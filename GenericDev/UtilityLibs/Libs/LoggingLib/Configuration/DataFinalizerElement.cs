﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace LoggingLib.Configuration
{
    /// <summary>
    /// data finalizer configuration element
    /// </summary>
    public class DataFinalizerElement : ConfigurationElement
    {
        /// <summary>
        /// The full qualified type name of a log entry data type. The data finalizer defined 
        /// will be applied to log entries of this type.
        /// </summary>
        [ConfigurationProperty("LogEntryDataTypeName", DefaultValue = "", IsRequired = true, IsKey=true)]
        public string LogEntryDataTypeName
        {
            get
            {
                return ((string)(base["LogEntryDataTypeName"]));
            }

            set
            {
                base["LogEntryDataTypeName"] = value;
            }
        }

        /// <summary>
        /// The full qualified type name of a data finalizer class which will be loaded and called
        /// before and after SubmitChanges() of the logging data context.
        /// </summary>
        [ConfigurationProperty("FinalizerDataTypeName", DefaultValue = "", IsRequired = true, IsKey = true)]
        public string FinalizerDataTypeName
        {
            get
            {
                return ((string)(base["FinalizerDataTypeName"]));
            }

            set
            {
                base["FinalizerDataTypeName"] = value;
            }
        }
    }
}
