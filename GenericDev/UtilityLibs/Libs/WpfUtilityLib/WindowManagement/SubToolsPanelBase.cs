﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using Logicx.WpfUtility.CustomControls.OverlayManager;

namespace Logicx.WpfUtility.WindowManagement
{
    public interface ISubToolsPanelBase
    {
        
    }

    public class SubToolsPanelBase<B, S, T, U, O, SU> : UserControl, ISubToolsPanelBase
        where S : SubMenuPanelBase<B, S, T, U, O, SU>, new()
        where B : BaseFrameBase<B, S, T, U, O, SU>
        where T : WindowTaskBarBase<B, S, T, U, O, SU>, new()
        where U : WindowTitleBarBase<B, S, T, U, O, SU>, new()
        where O : OverlayManagerBase<B, S, T, U, O, SU>, new()
        where SU : SubMenuPanelMenuItem
    {

    }
}
