﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Navigation;

namespace BankLib.Data
{
    public class Buchungen
    {
        #region Buchung Database Mgmt
        private static int DoUpdateAllFromWeb(ref string error_msg)
        {
            int added_buchungen = 0;

            DateTime from_date = DateTime.Now;

            BuchungsStatusInfo last_status_info = new BuchungsStatusInfo();
            if (last_status_info.LoadFromFile())
            {
                from_date = last_status_info.LastWebUpdate;
                from_date = from_date.AddDays(ForceWebUpdateDaysBack);
            }
            else
            {
                //wenn es kein status file mehr gibt dann mach das update fürs letzte jahr
                from_date = new DateTime(from_date.Year, from_date.Month, 1);
                from_date = from_date.AddMonths(-13);
            }

            Konto kto = Konten.AlleKonten.Where(d => d.Owner == "Martina").Single();
            added_buchungen += Buchungen.LoadFromWebNewBuchungenAndSave(kto, from_date, ref error_msg);

            kto = Konten.AlleKonten.Where(d => d.Owner == "Alex").Single();
            added_buchungen += Buchungen.LoadFromWebNewBuchungenAndSave(kto, from_date, ref error_msg);

            return added_buchungen;
        }



        public static int UpdateAllFromWeb(ref string error_msg)
        {
            BuchungsStatusInfo last_status = new BuchungsStatusInfo();
            last_status.LoadFromFile();

            AppHelper.WriteFileLog("\r\n\r\nStarting Update Konten "+DateTime.Now);

            //do work here
            int added_buchungen = DoUpdateAllFromWeb(ref error_msg);

            if (string.IsNullOrEmpty(error_msg))
            {
                AppHelper.WriteFileLog("\r\n" + DateTime.Now + " Success added " + added_buchungen);
            }
            else
            {
                AppHelper.WriteFileLog("\r\nERROR\r\n" + error_msg);
            }

            BuchungsStatusInfo curr_status = new BuchungsStatusInfo();
            curr_status.LastWebUpdate = DateTime.Now;
            curr_status.CountBuchungen = Buchungen.AlleBuchungen.Count;


            if (last_status.CountBuchungen != curr_status.CountBuchungen)
            {

                string msg = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">" +
                             "<html>" +
                             "<head>" +
                             "<meta http-equiv=\"content-type\" content=\"text/html; charset=ISO-8859-1\">" +
                             "</head>" +
                             "<body bgcolor=\"#ffffff\" text=\"#000000\">" +
                             //"Success added " + added_buchungen + "<br>" +
                             BuchungsHelper.GetBuchungsStatusEmailBody(last_status, curr_status) +
                             "<div class=\"moz-signature\"><i><br>" +
                             "<br>" +
                             "Regards<br>" +
                             "BankDataSyncService<br>" +
                             "</i></div>" +
                             "</body>" +
                             "</html>";
                AppHelper.SendEmail("Neuer Kontostand", Konten.AlleKonten, "apopovic.aut@gmail.com", msg);
            }


            curr_status.SaveToFile();
            return added_buchungen;
        }

        //public static int SaveBuchungenToFile()
        //{
        //    int saved_buchungen = 0;
        //    for (int i = 0; i < Konten.AlleKonten.Count; i++)
        //    {
        //        Konto akt_k = Konten.AlleKonten[i];

        //        var date_groups_prep = 
        //            from b in AlleBuchungen
        //            where b.Owner == akt_k.Owner
        //            select new { Buchung = b, SortDate = new DateTime(b.ValutaDatum.Year,b.ValutaDatum.Month,1)};

        //        var date_groups = from c in date_groups_prep
        //            group c by c.SortDate
        //            into g
        //            select g;

        //        foreach (var date_group in date_groups)
        //        {

        //            string path_to_buchungsfile = GetBuchungsFileName(akt_k, date_group.Key.Year, date_group.Key.Month);
        //            saved_buchungen += SaveBuchungenToFile(date_group.Select(d => d.Buchung).ToList(), path_to_buchungsfile);
        //        }

        //    }
        //    return saved_buchungen;
        //}


        public static int SaveBuchungenToFile()
        {
            int sum_saved = 0;
            sum_saved += SaveBuchungenToFile(AlleBuchungen,"");
            sum_saved += SaveBuchungenToFile(TransparenteBuchungen, PREFIX_TRANSPARENT);
            return sum_saved;
        }

        private static int SaveBuchungenToFile(List<Buchung> blist, string prefix_path)
        {
            int saved_buchungen = 0;
            for (int i = 0; i < Konten.AlleKonten.Count; i++)
            {
                Konto akt_k = Konten.AlleKonten[i];

                var date_groups_prep =
                    from b in blist
                    where b.Owner == akt_k.Owner
                    select new { Buchung = b, SortDate = new DateTime(b.ValutaDatum.Year, b.ValutaDatum.Month, 1) };

                var date_groups = from c in date_groups_prep
                                  group c by c.SortDate
                                      into g
                                      select g;

                foreach (var date_group in date_groups)
                {

                    string path_to_buchungsfile = GetBuchungsFileName(prefix_path, akt_k, date_group.Key.Year, date_group.Key.Month);
                    saved_buchungen += WriteBuchungenToFile(date_group.Select(d => d.Buchung).ToList(), path_to_buchungsfile);
                }

            }
            return saved_buchungen;
        }

        private static string GetBuchungsFileName(string prefix, Konto konto, int year, int mon)
        {
            string path_to_buchungsfile = AppHelper.PathToBankData + prefix + "k" + konto.Owner + ".y" + year + ".m" + mon+".dat";
            return path_to_buchungsfile;
        }

        private static int WriteBuchungenToFile(List<Buchung> buchungen, string path_to_buchungen_file )
        {
            if (buchungen.Count == 0)
                return 0;

            if (File.Exists(path_to_buchungen_file))
                File.Delete(path_to_buchungen_file);

            using (BinaryWriter writer = new BinaryWriter(File.Open(path_to_buchungen_file, FileMode.CreateNew)))
            {
                buchungen = buchungen.OrderBy(b => b.BuchungsDatum).ThenBy(b => b.ValutaDatum).ToList();
                writer.Write(buchungen.Count);
                for (int i = 0; i < buchungen.Count; i++)
                {
                    buchungen[i].WriteToStream(writer);
                }
            }
            return buchungen.Count;
        }

        public static void RemoveBuchungenFromFile()
        {
            throw new NotImplementedException();
        }

        public static int LoadBuchungenFromFile()
        {
            int sum_loaded = 0;

            AlleBuchungen = new List<Buchung>();
            TransparenteBuchungen = new List<Buchung>();
            
            sum_loaded += LoadBuchungenFromFile(AlleBuchungen, "");
            sum_loaded += LoadBuchungenFromFile(TransparenteBuchungen, PREFIX_TRANSPARENT);

            return sum_loaded;
        }

        public static int LoadBuchungenFromFile(List<Buchung> blist, string prefix )
        {
            int count_buchungen_loaded = 0;

            string[] file_paths = Directory.GetFiles(AppHelper.PathToBankData, prefix+"k*.dat");
            foreach (string file_path in file_paths)
            {
                count_buchungen_loaded += LoadBuchungenFromFile(file_path, blist);
            }

            return count_buchungen_loaded;
        }

        public static int LoadBuchungenFromFile(string path_to_buchungs_file, List<Buchung> buchungen)
        {
            if (!File.Exists(path_to_buchungs_file))
                return -1;


            using (BinaryReader reader = new BinaryReader(File.Open(path_to_buchungs_file, FileMode.Open)))
            {
                int b_count = reader.ReadInt32();
                for (int i = 0; i < b_count; i++)
                {
                    Buchung b = new Buchung();
                    b.LoadFromStream(reader);
                    buchungen.Add(b);
                }
                return b_count;
            }
        }


        public static int LoadFromWebNewBuchungenAndSave(Konto kto, DateTime from_date, ref string error_msg)
        {
            float? giro_kto_stand = null;
            List<Buchung> buchungen = null;
            WebImporter.Selenium sel = new WebImporter.Selenium(kto, from_date);
            try
            {
                if (sel.Connect(ref error_msg, out giro_kto_stand))
                {
                    kto.GiroKtoStandWeb = giro_kto_stand;
                    do
                    {
                        buchungen = sel.ExtractBuchungen(false);
                    } while (sel.NavWeiter());
                }
            }
            finally
            {
                sel.Disconnect();
            }
            


            int added_buchungen=0;
            if (buchungen != null)
            {
                Console.WriteLine("=======================================");
                Console.WriteLine("===EXTRACTED " + buchungen.Count + "====");
                Console.WriteLine("=======================================");

                buchungen = buchungen.OrderBy(b => b.ValutaDatum).ToList();
                added_buchungen = Buchungen.AddBuchungen(buchungen, kto);
            }
            return added_buchungen;
        }


        private static int AddBuchungen(List<Buchung> to_be_added_buchungen, Konto kto)
        {
            int b_loaded = LoadBuchungenFromFile();

            DateTime last_import_date = new DateTime();
            Buchung last_buchung = AlleBuchungen.Where(b => b.KtoNummer == kto.KtoNummer).OrderByDescending(b => b.ValutaDatum).FirstOrDefault();
            if (last_buchung != null)
                last_import_date = last_buchung.ValutaDatum;

            int added_buchungen = 0;
            int check_exact_min_entries = 5;
            int check_exact_min_daysback = -2;
            DateTime date_check = DateTime.Now;
            date_check = date_check.AddDays(check_exact_min_daysback);

            for (int i = 0; i < to_be_added_buchungen.Count; i++)
            {
                Buchung b = to_be_added_buchungen[i];

                //if (i < check_exact_min_entries || (date_check <= b.BuchungsDatum))
                //{
                    if (AddBuchungen(b))
                        added_buchungen++;
                    continue;
                //}


                ////add buchung to local db if buchung is newer
                //if (b.ValutaDatum > last_import_date)
                //{
                //    AlleBuchungen.Add(b);
                //    added_buchungen++;
                //}
            }



            int b_saved = SaveBuchungenToFile();
            
            return added_buchungen;
        }

        /// <summary>
        /// if buchung is not inserted in the list AlleBuchungen, then the given buchung will be added to the list
        /// </summary>
        /// <param name="b"></param>
        /// <returns></returns>
        private static bool AddBuchungen(Buchung b)
        {
            if (b.MetaInfoGroupAdd == "transparent")
                return AddBuchung(b, TransparenteBuchungen);

            return AddBuchung(b, AlleBuchungen);
        }

        private static bool AddBuchung(Buchung b, List<Buchung> blist )
        {
            //2te VARIANTE -> hier können auch alte to_be_added_buchungen eingefügt werden
            bool is_drin = false;
            for (int i = blist.Count - 1; i >= 0; i--)
            {
                Buchung inserted_b = blist[i];

                if (b.IsSameBuchung(inserted_b))
                {
                    is_drin = true;
                    break;
                }
            }

            if (is_drin)
                return false;

            blist.Add(b);
            return true;
        }


        public static float GetAusgaben(Konto konto, int month, int year)
        {
            return AlleBuchungen.Where(b => b.KtoNummer == konto.KtoNummer && b.ValutaDatum.Month == month && b.ValutaDatum.Year == year && b.Betrag < 0)
                .Sum(b => b.Betrag);
        }

        public static float GetEinnahmen(Konto konto, int month, int year)
        {
            return AlleBuchungen.Where(b => b.KtoNummer == konto.KtoNummer && b.ValutaDatum.Month == month && b.ValutaDatum.Year == year && b.Betrag > 0)
                .Sum(b => b.Betrag);
        }

        public static int AddBuchungenFromCsvFile(string owner, string kto_nr, bool kreditcard, string path_to_csv)
        {
            int added_buchungen = -1;
            try
            {
                DateTime last_import_date = new DateTime();
                Buchung last_buchung = AlleBuchungen.Where(b=>b.KtoNummer == kto_nr).OrderByDescending(b => b.BuchungsDatum).FirstOrDefault();
                if (last_buchung != null)
                    last_import_date =  last_buchung.BuchungsDatum;

                using (FileStream fs = File.OpenRead(path_to_csv))
                {
                    using (StreamReader sr = new StreamReader(fs,Encoding.Default))
                    {
                        bool first_line = true;
                        added_buchungen = 0;
                        while(true)
                        {
                            string line = sr.ReadLine();
                            if (line == null)
                                break;
                            Buchung b = new Buchung();
                            if (first_line)
                            {
                                first_line = false;
                                continue;
                            }
                            b.Owner = owner;
                            b.Kreditkarte = kreditcard;
                            try
                            {
                                b.LoadFromCsvLine(line);
                            }
                            catch
                            {
                                Console.WriteLine("\n\nCannot import buchungsline\n"+line);
                                continue;
                            }

                            ////add buchung to local db if buchung is newer
                            //if (b.BuchungsDatum > last_import_date)
                            //{
                            //    AlleBuchungen.Add(b);
                            //    added_buchungen++;
                            //}


                            ////2te VARIANTE -> hier können auch alte to_be_added_buchungen eingefügt werden
                            //bool is_drin = false;
                            //foreach (Buchung inserted_b in AlleBuchungen)
                            //{
                            //    if (b.IsSameBuchung(inserted_b))
                            //    {
                            //        is_drin = true;
                            //        break;
                            //    }
                            //}
                            //if (!is_drin)
                            //{
                            //    AlleBuchungen.Add(b);
                            //    added_buchungen++;
                            //}
                        }
                    }
                }
            }
            catch
            {
            }
            return added_buchungen;
        }
        #endregion

        public static List<Buchung> GetNewBuchungen(int last_buchung_index)
        {
            if (last_buchung_index <= 0)
                return new List<Buchung>(AlleBuchungen);

            if (last_buchung_index >= AlleBuchungen.Count - 1)
                return new List<Buchung>();
            try
            {
                return AlleBuchungen.GetRange(last_buchung_index, AlleBuchungen.Count - last_buchung_index);

            }
            catch
            {
                AppHelper.WriteFileLog(string.Format("Count Alle Buchungen:{2} StartIndex:{0} EndIndex:{1} LastBuchungsIndex:{3}", last_buchung_index + 1, AlleBuchungen.Count - last_buchung_index - 1, AlleBuchungen.Count, last_buchung_index));
                return AlleBuchungen.GetRange(last_buchung_index, AlleBuchungen.Count - last_buchung_index);
            }
        }

        public static List<Buchung> GetBuchungen(Konto kto = null, int month = 0, int year = 0)
        {
            var query = from b in AlleBuchungen
                        select b;
            if (kto != null && kto.KtoNummer != null)
            {
                query = from b in query
                        where b.KtoNummer == kto.KtoNummer
                        select b;
            }

            if (month != 0)
            {
                query = from b in query
                        where b.Datum.Month == month
                        select b;
            }

            if (year != 0)
            {
                query = from b in query
                        where b.Datum.Year == year
                        select b;
            }

            return query.ToList();
        }


        public static List<Buchung> GetNewBuchungen(Konto kto, int last_buchung_index)
        {
            List<Buchung> new_buchungen = new List<Buchung>();

            if (last_buchung_index < 0)
                last_buchung_index = 0;

            for (int i = last_buchung_index; i < AlleBuchungen.Count; i++)
            {
                if (AlleBuchungen[i].KtoNummer == kto.KtoNummer)
                    new_buchungen.Add(AlleBuchungen[i]);
            }
            return new_buchungen;
        }

        public static List<DateTime> GetBuchungsMonate()
        {
            if (_cache_buchungs_monate != null)
                return _cache_buchungs_monate;

            var monate = from b in Buchungen.AlleBuchungen
                select new DateTime(b.BuchungsDatum.Year, b.BuchungsDatum.Month,1);

            _cache_buchungs_monate = monate.Distinct().OrderByDescending(b => b.Year).ThenByDescending(b => b.Month).ToList();
            return _cache_buchungs_monate;
        }

        private static List<DateTime> _cache_buchungs_monate;
        public static List<Buchung> AlleBuchungen = new List<Buchung>();
        private const string PREFIX_TRANSPARENT = "transparent.";
        public static List<Buchung> TransparenteBuchungen = new List<Buchung>();
        public static int ForceWebUpdateDaysBack = -10;

    }
}
