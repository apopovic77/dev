﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MathLib;

namespace Logicx.WpfUtility.GraphPlotter
{
    /// <summary>
    /// Interaktionslogik für GraphPlot.xaml
    /// </summary>
    public partial class GraphPlot : UserControl
    {
        public GraphPlot()
        {
            InitializeComponent();

            _graphDB = new Dictionary<string, List<Vector2f>>();
            _pathDB = new Dictionary<string, Path>();
            _pathColorsDB = new Dictionary<string, Brush[]>();
            _filledpathDB = new Dictionary<Path, Path>();
            _pathStrokeThicknessDB = new Dictionary<string, double>(); 


            SkalaWidth = 50;
            MinAbstandYAchse = 50;
            YAchseBeschriftung = "n.def.";
        }

        #region PublicMethods
        public void Update()
        {
            if (!_last_x_start_update.HasValue)
                return;
            Update(_last_x_start_update.Value, _last_x_end_update.Value);
        }

        public void Update(float von, float bis)
        {
            if (_graphDB.Count == 0)
                return;

            _last_x_start_update = von;
            _last_x_end_update = bis;

            double max_Y_Value = double.MinValue;
            double min_Y_Value = double.MaxValue;
            double max_X_Value = double.MinValue;
            Vector2f untererPunkt;
            untererPunkt.X = float.NaN;
            untererPunkt.Y = float.NaN;
            Vector2f obererPunkt;
            obererPunkt.X = float.NaN;
            obererPunkt.Y = float.NaN;
           
            /* Max und Min Werte des gesamten Graphen werden berechnet.
             * Es sollten hier eigentlich nur die Min und Max - Werte des dargestellten Graphen berechnet werden. 
             * Muss noch implementiert werden. Noch kein Ansatz */
            for (int i = 0; i < _graphDB.Count; i++)
            {
                List<Vector2f> mydata = _graphDB.ElementAt(i).Value;
                
                var intervall = from p in mydata
                                where p.X >= von && p.X <= bis
                                select p;
                var unterewerte = from p in mydata
                                  where p.X < von
                                  select p;
                var oberewerte = from p in mydata
                                 where p.X > bis
                                 select p;

                /* Es muss eventuell ein Vorgängerknoten bzw. ein Nachfolgerknoten der zu überprüfenden Werte hinzugefügt werden,
                 * da im vorgegebenen Intervall eventuell gar kein Knoten liegen kann, da Start- und Endknoten außerhalb des 
                 * Intervalls liegen */
                if(unterewerte.Count() > 0)
                    untererPunkt = unterewerte.ElementAt(unterewerte.Count()-1);
                if(oberewerte.Count() > 0)
                    obererPunkt = oberewerte.ElementAt(0);

                float curmax;
                float curmin;
                float curmaxx;
                if(intervall.Count() > 0)
                {
                    curmax = intervall.Max(w => w.Y);
                    curmin = intervall.Min(w => w.Y);
                    curmaxx = intervall.Max(w => w.X);
                }
                else
                {
                    curmax = float.MinValue;
                    curmin = float.MaxValue;
                    curmaxx = float.MinValue;
                }

                if(max_Y_Value < curmax)
                    max_Y_Value = curmax;
                if(min_Y_Value > curmin)
                    min_Y_Value = curmin;
                if (max_X_Value < curmaxx)
                    max_X_Value = curmaxx;
                
                if(untererPunkt.X != float.NaN)
                {
                    if (untererPunkt.Y > max_Y_Value)
                        max_Y_Value = untererPunkt.Y;
                    if (untererPunkt.Y < min_Y_Value)
                        min_Y_Value = untererPunkt.Y;
                }
                if (obererPunkt.X != float.NaN)
                {
                    if (obererPunkt.Y > max_Y_Value)
                        max_Y_Value = obererPunkt.Y;
                    if (obererPunkt.Y < min_Y_Value)
                        min_Y_Value = obererPunkt.Y;
                }
            }

            if (IsMaxYActive)
                max_Y_Value = MaxY;
            if (IsMinYActive)
                min_Y_Value = MinY;

            /* Wenn max und min gleich sind, handelt es sich um eine Gerade parallel zur x-Achse
             * in diesem Fall fügen wir eine neue obere und untere Schranke ein, damit die Grade genau in der Mitte der Graphik liegt */
            if (max_Y_Value == min_Y_Value)
            {
                max_Y_Value += 0.5;
                min_Y_Value -= 0.5;
            }

            if (!IsMaxYActive)
                max_Y_Value = BestMaxRound(max_Y_Value, min_Y_Value);
            if (!IsMinYActive)
                min_Y_Value = BestMinRound(min_Y_Value, max_Y_Value);

            /* Canvas, welches die Pfade darstellen soll, wird neu gesetzt */
            myContent.Children.Clear();
            myContent.Width = _internRenderedWidth;
            myContent.Height = _internRenderedHeight;

            if(IsYAxisClosed && !IsYAxisFrozen)
            {
                double roundval = GetRoundStepSize(min_Y_Value, max_Y_Value);
                double diff = max_Y_Value - min_Y_Value;
                double steps = diff / roundval;
                if (steps - (int)steps > 0)
                    steps = (int) steps + 1;
                max_Y_Value = min_Y_Value + ((int)steps)*roundval;
                PrintSkala_Iterativ(min_Y_Value, max_Y_Value, roundval);
                _last_ymax_value_calculated = max_Y_Value;
                _last_ymin_value_calculated = min_Y_Value;
            }
            else if(IsYAxisFrozen)
            {
                PrintSkala_Iterativ(_last_ymin_value_calculated, _last_ymax_value_calculated);
                max_Y_Value = _last_ymax_value_calculated;
                min_Y_Value = _last_ymin_value_calculated;
            }
            else
            {
                PrintSkala_Iterativ(min_Y_Value, max_Y_Value);            
                _last_ymax_value_calculated = max_Y_Value;
                _last_ymin_value_calculated = min_Y_Value;
            }
                


            /* Skalierung basierend auf den Max und Min Wert wird berechnet */
            double scaley = _internRenderedHeight/(max_Y_Value-min_Y_Value);
            double scalex = _internRenderedWidth/(bis-von);

            for (int i = 0; i < _graphDB.Count; i++)
            {
                List<Vector2f> mydata = _graphDB.ElementAt(i).Value;
               
                string curkey = _graphDB.ElementAt(i).Key;

                StreamGeometry streamgeometry = new StreamGeometry();
                StreamGeometry streamgeometry_for_fill = new StreamGeometry();
                using (StreamGeometryContext ctx = streamgeometry.Open())
                {
                    using (StreamGeometryContext ctx2 = streamgeometry_for_fill.Open())
                    {
                        int pos = 0;
                        Vector2f lastpoint;
                        lastpoint.X = 0;
                        foreach (Vector2f vect in mydata)
                        {
                            if (pos == 0)
                            {
                                Point start_point = new Point(vect.X*scalex, (-vect.Y + min_Y_Value)*scaley);
                                ctx.BeginFigure(start_point, false /* is filled */, false /* is not closed? */);

                                Point start_point_for_fill = new Point(0, 0);
                                ctx2.BeginFigure(start_point_for_fill, true, true);
                                ctx2.LineTo(start_point, false, false);
                                pos++;
                            }
                            else
                            {
                                lastpoint = vect;
                                ctx.LineTo(new Point(vect.X*scalex, (-vect.Y + min_Y_Value)*scaley), true
                                           /* is stroked */, false /* is smooth join */);
                                ctx2.LineTo(new Point(vect.X * scalex, (-vect.Y + min_Y_Value) * scaley), false
                                            /* is stroked */, false /* is smooth join */);
                            }
                        }

                        ctx2.LineTo(new Point(lastpoint.X * scalex, 0), false, false);
                        //ctx2.LineTo(new Point(0, 0), false, false);

                        streamgeometry.Freeze();
                        streamgeometry_for_fill.Freeze();

                        /* Wenn der Pfad schon einmal erstellt wurde, wird die Datenliste nur aktuallisiert */
                        if (_pathDB.ContainsKey(curkey))
                        {
                            _pathDB[curkey].Data = streamgeometry;
                            _filledpathDB[_pathDB[curkey]].Data = streamgeometry_for_fill;
                        }
                        else
                        {
                            Path p = new Path();
                            p.MouseEnter += new MouseEventHandler(p_MouseEnter);
                            p.Stroke = _pathColorsDB[curkey][0];
                            p.StrokeThickness = _pathStrokeThicknessDB[curkey];
                            p.Data = streamgeometry;

                            Path p2 = new Path();
                            p2.MouseEnter += new MouseEventHandler(p_MouseEnter);
                            p2.IsHitTestVisible = true;
                            p2.Opacity = 0.95;
                            p2.Fill = _pathColorsDB[curkey][1];
                            p2.Data = streamgeometry_for_fill;
                            p2.Tag = p;
                            _filledpathDB.Add(p, p2);
                            _pathDB.Add(curkey, p);
                        }
                    }
                }
            }

            //mySkalaKmBis.Text = max_Y_Value.ToString();
            //mySkalaKmVon.Text = min_Y_Value.ToString();

            /* Jeder Pfad wird dem Canvas hinzugefügt und um den jeweiligen Ausschnitt verschoben */
            for (int i = 0; i < _pathDB.Count; i++)
            {
                TransformGroup mygroup = new TransformGroup();
                mygroup.Children.Add(new TranslateTransform(-(von*scalex), _internRenderedHeight));
                //mygroup.Children.Add(new ScaleTransform(RenderedWidth/max_X_Value, RenderedHeight/max_Y_Value));
                _pathDB.ElementAt(i).Value.RenderTransform = mygroup;
                _filledpathDB[_pathDB.ElementAt(i).Value].RenderTransform = mygroup;

                myContent.Children.Add(_filledpathDB[_pathDB.ElementAt(i).Value]);
                myContent.Children.Add(_pathDB.ElementAt(i).Value);
            }
        }


        public GraphLegend GetLegend()
        {
            GraphLegend mylegend = new GraphLegend();
            for (int i = 0; i < _pathColorsDB.Count; i++)
            {
                string text = _pathColorsDB.ElementAt(i).Key;
                Brush mybrush = _pathColorsDB.ElementAt(i).Value[0];
                Brush mybrush2 = _pathColorsDB.ElementAt(i).Value[1];
                GraphLegendEntry curentry = new GraphLegendEntry(this, mylegend);
                curentry.myEntryName.Text = text;
                curentry.myEntryColor.Fill = mybrush;
                curentry.GraphFillBrush = mybrush2;
                mylegend.myLegendContainer.Children.Add(curentry);
            }
            _legend = mylegend;
            return mylegend;
        }

        public void PutLineToFront(string name)
        {
            if (_pathDB.ContainsKey(name))
            {
                Path pathtofront = _pathDB[name];
                PutLineToFront(pathtofront);
            }
        }

        /// <summary>
        /// Es wird eine Vektorliste, die als durchgehender Pfad gezeichnet werden soll, hinzugefügt.
        /// Die Eindeutigkeit des Names muss gewährleistet werden. Der Rückgabewert false, zeigt einen bereits gleichen vorhandenen Namen an
        /// </summary>
        /// <param name="name"></param>
        /// <param name="vectordata"></param>
        /// <param name="strokeColor"></param>
        /// <param name="fillColor"></param>
        public bool AddLine(string name, List<Vector2f> vectordata, Brush strokeColor, double stroke_thickness, Brush fillColor)
        {
            if (vectordata == null)
                return false;

            if (_graphDB.ContainsKey(name))
                return false;

            _graphDB.Add(name, vectordata);
            Brush[] mybrushes = {strokeColor, fillColor};
            _pathColorsDB.Add(name,mybrushes);
            _pathStrokeThicknessDB.Add(name, stroke_thickness);
            return true;
        }

        public bool AddLine(string name, List<Vector2f> vectordata, SolidColorBrush strokeColor, double stroke_thickness, bool isfilled)
        {
            if (vectordata == null)
                return false;

            if (_graphDB.ContainsKey(name))
                return false;

            _graphDB.Add(name, vectordata);

            Brush[] mybrushes;
            if (isfilled)
            {
                double h, s, l;
                ColorGenerator.ColorToHSL(strokeColor.Color, out h, out s, out l);
                l *= 1.2;
                if (l > 1)
                    l = 1;
                if (l == 0)
                    l = 0.4;
                Brush fillColor = new SolidColorBrush(ColorGenerator.ColorFromHSL(h, s, l));
                mybrushes = new Brush[]{strokeColor, fillColor};
            }
            else
            {
                mybrushes = new Brush[]{ strokeColor, null };
            }
            _pathColorsDB.Add(name, mybrushes);
            _pathStrokeThicknessDB.Add(name, stroke_thickness);
            return true;
        }

        public void DeleteLine(string name)
        {
            if (_graphDB.ContainsKey(name))
            {
                if (_pathDB.ContainsKey(name))
                {
                    Path temppath = _pathDB[name];
                    _filledpathDB.Remove(temppath);
                    _pathDB.Remove(name);
                }
                _graphDB.Remove(name);
                _pathColorsDB.Remove(name);
            }
        }

        public void SetLeftInfoText(double left)
        {
            //Canvas.SetLeft(mySkala,left);
            _infotext_left_offset = left;
        }
        
        #endregion
        
        #region Zeichne_Y_Achse
        private void PrintSkala_Rekursiv(double miny, double maxy)
        {
            //Elemente der alten Skala werden gelöscht
            mySkala.Children.Clear();

            /* Vertikale Linie der Skala wird gezeichnet */
            Rectangle myrect = new Rectangle();
            myrect.HorizontalAlignment = HorizontalAlignment.Right;
            myrect.Width = 1;
            myrect.Fill = _skala_grid_brush;
            mySkala.Children.Add(myrect);

            // Das Werteminimum wird für die Skala immer abgerundet, bzw. das Wertemaximum immer aufgerundet
            int min = (int)Math.Floor(miny);
            int max = (int)Math.Ceiling(maxy);
            
            // Die Labels für die minimalen und maximalen Werte werden gezeichnet
            AddSkalaEntry(min.ToString(), _internRenderedHeight);
            AddSkalaEntry(max.ToString(), 0);

            // Die restlichen Labels der Skala werden rekursiv gezeichnet
            AddSkalaRekursiv(0, _internRenderedHeight, min, max);
        }

        private void PrintSkala_Iterativ(double miny, double maxy, double roundval)
        {
            mySkala.Children.Clear();

            /* Vertikale Linie der Skala wird gezeichnet */
            Rectangle myrect = new Rectangle();
            myrect.HorizontalAlignment = HorizontalAlignment.Right;
            myrect.VerticalAlignment = VerticalAlignment.Top;
            myrect.Height = _internRenderedHeight;
            myrect.Width = 1;
            myrect.Fill = _skala_grid_brush;
            mySkala.Children.Add(myrect);
            
            double pixelpervalue = _internRenderedHeight/(maxy - miny);
            double lauf = miny;

            while(lauf <= maxy)
            {                
                double lauf2 = lauf + roundval;
                lauf2 = Math.Round(lauf2, 10);
                if((IsYAxisClosed || IsMaxYActive) && lauf2 > maxy)
                {
                    AddSkalaEntry(maxy.ToString(), _internRenderedHeight - (maxy - miny) * pixelpervalue);
                }
                else
                    AddSkalaEntry(lauf.ToString(), _internRenderedHeight-(lauf-miny)*pixelpervalue);
                lauf = lauf2;
            }
        }

        private void PrintSkala_Iterativ(double miny, double maxy)
        {
            double roundval = GetRoundStepSize(miny, maxy);
            if (roundval < 0)
                roundval *= -1;

            PrintSkala_Iterativ(miny, maxy, roundval);
        }

        private double GetRoundStepSize(double min, double max)
        {
            double diff = max - min;
            int anzahlWerte = (int)(_internRenderedHeight/MinAbstandYAchse)+1;

            if (anzahlWerte == 1)
                return diff;

            double zwischenwertbereich = diff/anzahlWerte;

            if (zwischenwertbereich >= 5 && zwischenwertbereich <= 100)
                return ((int)(zwischenwertbereich / 5)) * 5;
            else if (zwischenwertbereich > 100 && zwischenwertbereich <= 1000)
                return ((int)(zwischenwertbereich / 10)) * 10;
            else if (zwischenwertbereich > 1000 && zwischenwertbereich <= 10000)
                return ((int)(zwischenwertbereich / 100)) * 100;
            else if (zwischenwertbereich > 10000 && zwischenwertbereich <= 100000)
                return ((int)(zwischenwertbereich / 1000)) * 1000;
            else if (zwischenwertbereich > 100000 && zwischenwertbereich <= 1000000)
                return ((int)(zwischenwertbereich / 10000)) * 10000;
            else if (zwischenwertbereich > 10000000)
                return ((int) (zwischenwertbereich/100000))*100000;
            else if (zwischenwertbereich < 5 && zwischenwertbereich >= 1)
                //return (int)zwischenwertbereich;
                return 1;
            else
            {
                int nullennachdemkomma = 1;
                double tempwert = zwischenwertbereich * 10;
                while (Math.Abs(tempwert) <= 1)
                {
                    nullennachdemkomma++;
                    tempwert *= 10;
                }

                return Math.Floor(tempwert)/Math.Pow(10, nullennachdemkomma);
                return 0.5 / ((nullennachdemkomma + 1) * 10);
            }
        }

        private void AddSkalaRekursiv(double pixelvon, double pixelbis, double wertvon, double wertbis)
        {
            /* Rundung wird hier durchgeführt, um Rundungsfehler aus einem älteren Rekursionsschritt zu kaschieren */
            if(Math.Abs(pixelbis - pixelvon)<2*(MinAbstandYAchse) || (wertvon - wertbis)==0)
            {
                /* Nicht genug Platz für ein neues Label */
                return;
            }
            else
            {
                double pixelmitte = (pixelbis + pixelvon)/2;
                double wertmitte = (wertvon + wertbis)/2;
                double gerundetemitte = BestRound(wertvon, wertbis);

                /* Wenn die Rundung einen der Werte(wertvon oder wertbis) berechnet, wird einfach der Mittelwert genommen */
                if (gerundetemitte == wertvon || gerundetemitte == wertbis)
                    gerundetemitte = wertmitte;

                double offset = wertmitte - gerundetemitte;
                double wertpropixel = (pixelbis - pixelvon)/(wertbis - wertvon);
                //wertmitte = gerundetemitte;

                double neuerpixelmitte = pixelmitte + (-offset*wertpropixel);
                AddSkalaEntry(gerundetemitte.ToString(), _internRenderedHeight - neuerpixelmitte);

                //Füge wenn möglich oberhalb der Mitte einen Wert hinzu
                AddSkalaRekursiv(pixelmitte, pixelbis, wertmitte, wertbis);
                //Füge wenn möglich unterhalb der Mitte einen Wert hinzu
                AddSkalaRekursiv(pixelvon, pixelmitte, wertvon, wertmitte);
            }
        }

        /// <summary>
        /// Auf der Skala wird ein bestimmter Text aufgedruckt und ein zugehöriger Marker
        /// </summary>
        /// <param name="text"></param>
        /// <param name="offsettop"></param>
        /// <param name="offsetright"></param>
        private void AddSkalaEntry(string text, double offsettop/*, double offsetright*/)
        {
            TextBlock mintext = new TextBlock();
            mintext.FontFamily = _skala_font_fam;
            mintext.FontSize = _skala_font_size;
            mintext.Foreground = _skala_font_foreground_brush;
            mintext.Text = text;
            mintext.HorizontalAlignment = HorizontalAlignment.Right;
            mintext.VerticalAlignment = VerticalAlignment.Top;
            mintext.Margin = new Thickness(0, offsettop, _skala_value_offset_right,0);
            mySkala.Children.Add(mintext);

            Rectangle minrect = new Rectangle();
            minrect.HorizontalAlignment = HorizontalAlignment.Right;
            minrect.VerticalAlignment = VerticalAlignment.Top;
            minrect.Width = _vertical_skala_line_width;
            minrect.Height = 1;
            minrect.Fill = _skala_grid_brush;
            minrect.Margin = new Thickness(0, offsettop, 0, 0);
            mySkala.Children.Add(minrect);

            if(ShowGrid)
            {
                Rectangle verticalLine = new Rectangle();
                //verticalLine.SnapsToDevicePixels = true;
                verticalLine.SetValue(Canvas.ZIndexProperty, 2);
                verticalLine.Opacity = 0.5;
                verticalLine.IsHitTestVisible = false;
                verticalLine.Height = 0.5;
                verticalLine.Width = _internRenderedWidth;
                verticalLine.VerticalAlignment = VerticalAlignment.Top;
                verticalLine.Fill = _skala_grid_brush;
                verticalLine.Margin = new Thickness(0, offsettop, 0, 0);
                myContent.Children.Add(verticalLine);
            }
        }

        /// <summary>
        /// Funktion versucht basierend auf zwei Intervallgrenzen den optimalen Mittelwert zu finden.
        /// Es soll dabei abhängig von den Wertunterschieden ein angemessenes Rundungsintervall gesucht werden
        /// </summary>
        /// <param name="wertvon"></param>
        /// <param name="wertbis"></param>
        /// <returns></returns>
        private double BestRound(double wertvon, double wertbis)
        {
            double wertmitte = (wertvon + wertbis)/2;
            double diff = wertbis - wertvon;

            /* Wenn die Differenz der Wert größer als 1 ist, wird auf Einerstellen gerundet */
            if (diff >= 1 /*&& diff < 10*/)
                wertmitte = Math.Round(wertmitte);
                /* Versuch auf 5er Faktoren zu runden */
                /*else if(diff >= 10)
            {
                //wertmitte = Math.Round(wertmitte/10)*10;
                wertmitte = (Math.Ceiling(wertmitte/10)*10 + Math.Floor(wertmitte/10)*10)/2;
            }*/
                /* Sehr kleine Werte werden abhängig von den Nachkommastellen ohne vorangestellte 0en gerundet */
            else if(diff < 1)
            {
                int nullennachdemkomma = 0;
                double tempwert = wertmitte*10;
                while(Math.Abs(tempwert)<=1)
                {
                    nullennachdemkomma++;
                    tempwert *= 10;
                }

                wertmitte = Math.Round(wertmitte, nullennachdemkomma + 1);
            }

            return wertmitte;
        }

        private double BestMaxRound(double oldmax, double currentmin)
        {
            double diff = oldmax - currentmin;
            if (diff > 1)
                return Math.Ceiling(oldmax);
            else
            {
                int nullennachdemkomma = 1;
                double tempwert = diff * 10;
                while (Math.Abs(tempwert) <= 1)
                {
                    nullennachdemkomma++;
                    tempwert *= 10;
                }
                double output = Math.Ceiling((oldmax-(int)oldmax) * (Math.Pow(10, nullennachdemkomma))) / Math.Pow(10, nullennachdemkomma) + (int)oldmax;
                return output;
            }
        }

        private double BestMinRound(double oldmin, double currentmax)
        {
            double diff = currentmax - oldmin;
            if (diff > 1)
                return Math.Floor(oldmin);
            else
            {
                int nullennachdemkomma = 1;
                double tempwert = diff * 10;
                while (Math.Abs(tempwert) <= 1)
                {
                    nullennachdemkomma++;
                    tempwert *= 10;
                }
                double output = Math.Floor((oldmin-(int)oldmin) * (Math.Pow(10, nullennachdemkomma))) / Math.Pow(10, nullennachdemkomma) + (int) oldmin;
                return output;
            }
        }
        #endregion

        #region EventHandler
        /// <summary>
        /// Es wird mit der Maus über ein Pfad-Element gefahren. In diesem Fall soll der Pfad im Z-Index eine Ebene nach vorne kommen, und die restlichen eine zurück
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void p_MouseEnter(object sender, MouseEventArgs e)
        {
            e.Handled = true;
            Path p = (Path)sender;
            if (p.Tag == null)
                PutLineToFront(p);
            else
                PutLineToFront((Path)p.Tag);
        }


        void p_MouseLeave(object sender, MouseEventArgs e)
        {
            if (!IsGraphFrozen)
            {
                if (_changed_z_index)
                {
                    _changed_z_index = false;
                    return;
                }

                for (int i = 0; i < myContent.Children.Count; i++)
                {
                    myContent.Children[i].SetValue(Canvas.ZIndexProperty, 0);
                }
            }
        }

        private bool _changed_z_index;

        private void PutLineToFront(Path curPath)
        {
            if (!IsGraphFrozen)
            {
                string key = "";
                for (int i = 0; i < _pathDB.Count; i++)
                {
                    if (_pathDB.ElementAt(i).Value == curPath)
                        key = _pathDB.ElementAt(i).Key;
                }

                if (_legend != null)
                    _legend.ActivateEntry(key);

                Path filledpath = _filledpathDB[curPath];
                for (int i = 0; i < myContent.Children.Count; i++)
                {
                    if (myContent.Children[i] == curPath || myContent.Children[i] == filledpath)
                    {
                        myContent.Children[i].SetValue(Canvas.ZIndexProperty, 1);
                    }
                    else if ((int) myContent.Children[i].GetValue(Canvas.ZIndexProperty) <= 1)
                    {
                        myContent.Children[i].SetValue(Canvas.ZIndexProperty, 0);
                    }
                }

                _changed_z_index = true;
            }
        }
        
        private void Grid_MouseMove(object sender, MouseEventArgs e)
        {
            myLineContainer.Children.Clear();
            TextBlock infotext = new TextBlock();
            infotext.Foreground = _skala_font_foreground_brush;
            infotext.IsHitTestVisible = false;
            infotext.HorizontalAlignment = HorizontalAlignment.Left;
            

            Rectangle mypointer = new Rectangle();
            mypointer.Fill = _vertline_brush;
            mypointer.IsHitTestVisible = false;
            mypointer.HorizontalAlignment = HorizontalAlignment.Left;
            double hy = e.GetPosition(myContent).Y;
            double hx = e.GetPosition(myContent).X;
            
            //mypointer.Width = hx;
            mypointer.Width = _internRenderedWidth;
            mypointer.Height = 0.5;

            infotext.Text = CalcYAxisValue(hy).ToString() + YAchseEinheit;
            mypointer.Margin = new Thickness(0, hy+5, 0, 0);
            infotext.Margin = new Thickness(_infotext_left_offset, hy + 5, 0, 0);

            if (hy + 5 <= _internRenderedHeight && hy + 5 >= 0 && hx <= _internRenderedWidth && hx >= 0)
            {
                myLineContainer.Children.Add(mypointer);
                myLineContainer.Children.Add(infotext);
            }
        }                
        
        private void myGraph_MouseLeave(object sender, MouseEventArgs e)
        {
            myLineContainer.Children.Clear();

            _changed_z_index = false;
            p_MouseLeave(null,null);

            if (_legend != null)
                _legend.ActivateEntry("");
        }

        private double CalcYAxisValue(double diffy)
        {
            try
            {
                return Math.Round(_last_ymax_value_calculated - ((_last_ymax_value_calculated - _last_ymin_value_calculated)/_internRenderedHeight*diffy),2);
            } catch(Exception ex)
            {
                return 0;
            }
        }
        
        #endregion

        #region TestFunktionen
        
        // TestFunktion um Werte eines möglichen Graphen zu berechnen
        public void TestAddLine()
        {
            List<Vector2f> mylist = new List<Vector2f>();
            Random r = new Random();

            for (int i = 0; i < 200; i++)
            {
                mylist.Add(new Vector2f(i, (float) r.NextDouble()*100+50));
            }
            AddLine("Test1", mylist,new SolidColorBrush(Color.FromArgb(255,255,0,0)),3, true);

            List<Vector2f> mylist2 = new List<Vector2f>();
            mylist2.Add(new Vector2f(0, 0));
            mylist2.Add(new Vector2f(300, 0));

            AddLine("Test2", mylist2, Brushes.Orange, 3, true);

            List<Vector2f> mylist3 = new List<Vector2f>();
            mylist3.Add(new Vector2f(0, 50));
            mylist3.Add(new Vector2f(100, 50));

            AddLine("Test3", mylist3, Brushes.Navy, 3, true);

            List<Vector2f> mylist4 = new List<Vector2f>();
            mylist4.Add(new Vector2f(0, 50));
            mylist4.Add(new Vector2f(100, 10));

            AddLine("Test4", mylist4, Brushes.Black, 3, true);
        }
       
        #endregion

        #region Properties
        /// <summary>
        /// Wenn auf true, werden die letzten berechneten Werte für die Y-Achse herangezogen und während des UpdateProzesses nicht erneut berechnet
        /// </summary>
        public bool IsYAxisFrozen
        {
            set;
            get;
        }
        /// <summary>
        /// Wenn true, wird MinY für Y-Skala verwendet, anderenfalls wird Wert dynamisch berechnet
        /// </summary>
        public bool IsMinYActive
        {
            set;
            get;
        }
        /// <summary>
        /// Wenn true, wird MaxY für Y-Skala verwendet, anderenfalls wird Wert dynamisch berechnet
        /// </summary>
        public bool IsMaxYActive
        {
            set;
            get;
        }
        /// <summary>
        /// Max Y-Wert für manuelle Anpassung der Y-Achse
        /// </summary>
        public float MaxY
        {
            set;
            get;
        }
        /// <summary>
        /// Min Y-Wert für manuelle Anpassungen der Y-Achse
        /// </summary>
        public float MinY
        {
            set;
            get;
        }
        /// <summary>
        /// Anzahl der aktuell enthaltenen Linien im Graphen
        /// </summary>
        public int CountGraphLines
        {
            get
            {
                return _graphDB.Count;
            }
        }
        public double RenderedWidth
        {
            get
            {
                return _internRenderedWidth + SkalaWidth;
            }

            set
            {
                _internRenderedWidth = value - SkalaWidth;

            }
        }
        public double RenderedHeight
        {
            set
            {
                _internRenderedHeight = value;
            }
        }
        /* Wenn true, wird die Skala der y-Achse so angepasst, dass nur ein Auschnitt der Kurve gezeigt wird, indem auch y-Werte vorhanden sind
         * Wenn false, beginnt die y-Achse ganz normal bei 0 und es wird der gesamte 1. Sektor der Zahlenebene dargestellt */
        //public Boolean ShowOnlySignificantArea { set; get; }

        /* Zeigt an, ob der Abschluss der Y-Achse jeweils mit einem Marker gekennzeichnet wurde. */
        public Boolean IsYAxisClosed { set; get; }
        /// <summary>
        /// Abstand in Pixel der zwischen den Werten der Achsenbeschriftungen der nicht unterschritten werden darf
        /// </summary>
        public double MinAbstandYAchse { set; get; }

        /// <summary>
        /// Wenn auf true werden vertikale Linien im Diagramm dargestellt.
        /// </summary>
        public Boolean ShowGrid { set; get; }

        public string YAchseEinheit
        {
            set; get;
        }

        /// <summary>
        /// Beschriftung der Y Achse
        /// </summary>
        public string YAchseBeschriftung
        {
            set
            {
                mySkalaBeschriftung.Text = value;
            }
            get
            {
                return mySkalaBeschriftung.Text;
            }
        }

        /// <summary>
        /// True, wenn die Darstellung des Graphen nicht mehr geändert werden soll, dh es kommt zu keiner Veränderung der Z-Indizes mehr
        /// </summary>
        public bool IsGraphFrozen { set; get; }

        public static DependencyProperty SkalaWidthProperty = DependencyProperty.Register("SkalaWidth", typeof(double), typeof(GraphPlot));
        public double SkalaWidth
        {
            get { return (double)GetValue(SkalaWidthProperty); }
            set { SetValue(SkalaWidthProperty, value); }
        }


        public Brush GridLineBrush
        {
            get { return _skala_grid_brush; }
            set { _skala_grid_brush = value; }
        }

        public double SkalaFontSize
        {
            get { return _skala_font_size; }
            set { _skala_font_size = value; }
        }

        public FontFamily SkalaFontFamily
        {
            get { return _skala_font_fam; }
            set { _skala_font_fam = value; }
        }

        public Brush SkalaFontForegroundBrush
        {
            get { return _skala_font_foreground_brush; }
            set { _skala_font_foreground_brush = value; }
        }

        public double SkalaValueOffsetRight
        {
            get { return _skala_value_offset_right; }
            set { _skala_value_offset_right = value; }
        }

        public double SkalaVerticalLineWidth
        {
            get { return _vertical_skala_line_width; }
            set { _vertical_skala_line_width = value; }
        }
        public Brush VerticalLineBrush
        {
            get { return _vertline_brush; }
            set { _vertline_brush = value; }
        }
        #endregion

        #region Attributes

        /// <summary>
        /// Zum Abspeichern der Vektoren der jeweiligen Pfade
        /// </summary>
        protected Dictionary<string, List<Vector2f>> _graphDB;
        /// <summary>
        /// Zum Abspeichern der zu zeichnenden Pfade
        /// </summary>
        protected Dictionary<string, Path> _pathDB;
        /// <summary>
        /// Zum Abspeichern der Farben der jeweiligen Pfade
        /// </summary>
        protected Dictionary<string, Brush[]> _pathColorsDB;
        protected Dictionary<string, double> _pathStrokeThicknessDB;

        protected Dictionary<Path, Path> _filledpathDB;

        protected double _internRenderedWidth;
        protected double _internRenderedHeight;

        protected GraphLegend _legend;

        protected double _last_ymin_value_calculated;
        protected double _last_ymax_value_calculated;

        protected float? _last_x_start_update;
        protected float? _last_x_end_update;

        protected double _vertical_skala_line_width = 4;
        protected double _skala_value_offset_right = 2;
        protected FontFamily _skala_font_fam = new FontFamily("Arial");
        protected double _skala_font_size=10;
        protected Brush _skala_font_foreground_brush = Brushes.Black;
        protected Brush _skala_grid_brush = Brushes.Black;
        protected Brush _vertline_brush = Brushes.Black;

        protected double _infotext_left_offset;
        #endregion
    }
}