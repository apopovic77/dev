﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using BankLib.Data;


namespace BankLib.Gruppe
{
    public enum BuchungsGruppenZuordnungsTyp
    {
        Exact,
        IndexOf
    }

    public class BuchungsGruppe
    {
        internal void WriteToStream(BinaryWriter writer)
        {
            BinaryHelper.WriteString(writer, Name);
            BinaryHelper.WriteString(writer, Beschreibung);

            writer.Write(_children.Count);
            foreach (BuchungsGruppe buchungs_gruppe in _children)
                buchungs_gruppe.WriteToStream(writer);

            writer.Write(_bg_zuos.Count);
            foreach (BgZuo bg_zuo in _bg_zuos)
                bg_zuo.WriteToStream(writer);
        }

        public void LoadFromStream(BinaryReader reader)
        {
            Name = BinaryHelper.ReadString(reader);
            Beschreibung = BinaryHelper.ReadString(reader);

            int count_children = reader.ReadInt32();
            _children.Clear();
            for (int i = 0; i < count_children; i++)
            {
                BuchungsGruppe bg = new BuchungsGruppe();
                bg.LoadFromStream(reader);
                bg.Parent = this;
                _children.Add(bg);
            }

            int count_bgzuo = reader.ReadInt32();
            _bg_zuos.Clear();
            for (int i = 0; i < count_bgzuo; i++)
            {
                BgZuo bgzuo = BgZuoFactory.LoadFromStream(reader);
                bgzuo.Parent = this;
                _bg_zuos.Add(bgzuo);
            }
        }


        public void AddChild(BuchungsGruppe bg)
        {
            _sum_betrag_dirty = true;
            if(bg.Parent != null)
                throw new ArgumentException("BuchungsGruppe hat bereits einen Parent gesetzt kann nicht geadded werden, bitte zuerst aus der Struktur removen.");
            bg.Parent = this;
            _children.Add(bg);
        }

        public void AddBgZuo(BgZuo bgzuo)
        {
            _sum_betrag_dirty = true;
            if (bgzuo == null)
                return;
            if (bgzuo.Parent != null)
                throw new ArgumentException("BuchungsGruppe hat bereits einen Parent gesetzt kann nicht geadded werden, bitte zuerst aus der Struktur removen.");
            bgzuo.Parent = this;
            _bg_zuos.Add(bgzuo);
        }

        public void AddBgZuo(List<BgZuo> list)
        {
            _sum_betrag_dirty = true;
            foreach (BgZuo bg_zuo in list)
                AddBgZuo(bg_zuo);
        }

        public List<BuchungsGruppe> Children()
        {
            return _children;
        }



        #region Extract BG Zuos Flat List
        public Dictionary<int, List<BgZuo>> ExtractBgZuosFlatList()
        {
            if (_cache_bgzuos != null)
                return _cache_bgzuos;

            Dictionary<int, List<BgZuo>> bgzuos;
            _cache_bgzuos = bgzuos = new Dictionary<int, List<BgZuo>>();
            AddBgZuos(bgzuos, this);
            return bgzuos;
        }

        private void AddBgZuos(Dictionary<int, List<BgZuo>> bgzuos, BuchungsGruppe bg_top)
        {
            if (bg_top.BgZuos() != null)
            {
                foreach (BgZuo bg_zuo in bg_top.BgZuos())
                {
                    if (!bgzuos.ContainsKey(bg_zuo.RulePriority))
                        bgzuos.Add(bg_zuo.RulePriority, new List<BgZuo>(3));
                    bgzuos[bg_zuo.RulePriority].Add(bg_zuo);
                }
            }

            foreach (BuchungsGruppe bg in bg_top.Children())
                AddBgZuos(bgzuos, bg);
        }
        #endregion


        public float GetSumBetrag(List<Buchung> buchungen, string owner = null, int month = 0, int year = 0)
        {
            Dictionary<int, List<BgZuo>> all_bgzuos = ExtractBgZuosFlatList();
            
            float sum_betrag = 0;

            foreach (Buchung b in buchungen)
            {
                //iterate through bg_zuo rule priority
                foreach (int key in all_bgzuos.Keys.OrderBy(i => i))
                {
                    //iterate through bg_zuo from rule_priority [key]
                    foreach (BgZuo bg_zuo in all_bgzuos[key])
                    {
                        if (bg_zuo.IsZuo(b))
                            sum_betrag += b.Betrag * -1;
                    }
                }
            }

            return sum_betrag;
        }


        public BuchungsGruppe Clone()
        {
            BuchungsGruppe bg_clone = new BuchungsGruppe();

            bg_clone._sum_betrag = _sum_betrag;
            bg_clone._sum_betrag_dirty = _sum_betrag_dirty;

            _children = new List<BuchungsGruppe>(3);
            foreach (BuchungsGruppe bg in _children)
                bg_clone._children.Add(bg.Clone());    

            bg_clone._bg_zuos = new List<BgZuo>(3);
            bg_clone._bg_zuos.AddRange(_bg_zuos);
            return bg_clone;
        }



        public List<BgZuo> BgZuos()
        {
            return _bg_zuos;
        }

        public override string ToString()
        {
            return String.Format("Name {0} Count Children {1}", Name, _children.Count);
        }

        public bool HasChildren()
        {
            if (_children.Count == 0)
                return false;
            return true;
        }

        public string Name { get; set; }
        public string Beschreibung { get; set; }

        public BuchungsGruppe Parent { get; set; }

        public float SumBetrag {
            get
            {
                return _sum_betrag;
            }
            set
            {
                _sum_betrag = value;
                _sum_betrag_dirty = false;
            }
        }

        private float _sum_betrag = 0;
        private bool _sum_betrag_dirty = false;

        private Dictionary<int, List<BgZuo>> _cache_bgzuos;
        private List<BuchungsGruppe> _children = new List<BuchungsGruppe>(3);
        private List<BgZuo> _bg_zuos = new List<BgZuo>(3);


    }
}
