﻿using System;
using System.IO;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace CognitiveLib.CognitiveService
{
    public class ImageUtils
    {
        /// <summary>
        /// returns a byte array in JPEG Format, does not matter what type the image has been before
        /// </summary>
        /// <param name="path"></param>
        /// <param name="angle"></param>
        /// <returns></returns>
        public static byte[] LoadRotateGetJpgBytes(string path, int angle)
        {
            //Open the source image and create the bitmap for the rotatated image
            using (System.Drawing.Bitmap sourceImage = new System.Drawing.Bitmap(path))
            using (System.Drawing.Bitmap rotateImage = new System.Drawing.Bitmap(sourceImage.Width, sourceImage.Height))
            {
                //Set the resolution for the rotation image
                rotateImage.SetResolution(sourceImage.HorizontalResolution, sourceImage.VerticalResolution);
                //Create a graphics object
                using (System.Drawing.Graphics gdi = System.Drawing.Graphics.FromImage(rotateImage))
                {
                    //Rotate the image
                    gdi.TranslateTransform((float)sourceImage.Width / 2, (float)sourceImage.Height / 2);
                    gdi.RotateTransform(angle);
                    gdi.TranslateTransform(-(float)sourceImage.Width / 2, -(float)sourceImage.Height / 2);
                    gdi.DrawImage(sourceImage, new System.Drawing.Point(0, 0));
                }

                //Save to a file
                MemoryStream imgStream = new MemoryStream();
                rotateImage.Save(imgStream, System.Drawing.Imaging.ImageFormat.Jpeg);
                return imgStream.ToArray();
            }
        }

        public static byte[] LoadImageBytes(string path_to_png, out string pixel_format)
        {
            pixel_format = Path.GetExtension(path_to_png).ToLower();
            System.Drawing.Bitmap b = new System.Drawing.Bitmap(path_to_png);
            using (Stream imgStream = new MemoryStream())
            {
                System.Drawing.ImageConverter converter = new System.Drawing.ImageConverter();
                return (byte[])converter.ConvertTo(b, typeof(byte[]));
            }
        }

        public static byte[] LoadAndRotateImage(string path_to_png)
        {
            System.Drawing.Bitmap b = new System.Drawing.Bitmap(path_to_png);
            ////Increase the length/width
            //b = new System.Drawing.Bitmap(b, (int)(b.Width * 1.6), (int)(b.Height * 1.7));
            //rotate it, flip it 
            b.RotateFlip(System.Drawing.RotateFlipType.Rotate90FlipX);

            //Now, for reasons I can't explain, the RoateFlip method converts the format of the bitmap.
            //The size of it becomes lower so I assume its compressing it somehow?
            //As such, I am having to Save it and Reload it, to get it back into a 'real' bitmap format.
            using (Stream imgStream = new MemoryStream())
            {
                b.Save(imgStream, System.Drawing.Imaging.ImageFormat.Png);
                b = new System.Drawing.Bitmap(imgStream);

                //convert it to a byte array to fire at a printer.
                System.Drawing.ImageConverter converter = new System.Drawing.ImageConverter();
                return (byte[])converter.ConvertTo(b, typeof(byte[]));
            }
        }

        public static System.Drawing.Bitmap GetBitmap(string path_to_img)
        {
            System.Drawing.Bitmap b = new System.Drawing.Bitmap(path_to_img);
            return b;
        }

        public static ImageSource GetBitmapImgSource(string path)
        {
            try
            {
                FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read);
                byte[] b = new byte[fs.Length];
                fs.Read(b, 0, b.Length);
                fs.Close();
                fs.Dispose();

                string file_ext = Path.GetExtension(path).ToLower();

                return GetBitmapImgSource(b, file_ext);
            }
            catch
            {
                return null;
            }
        }

        public static ImageSource GetBitmapImgSource(byte[] data, string img_ext)
        {
            try
            {
                MemoryStream ms = new MemoryStream(data);
                ImageSource bitmap = null;
                BitmapImage bimg = null;

                switch (img_ext)
                {
                    case ".png":
                    case "png":
                    case "PNG":
                    case "Png":
                        {
                            PngBitmapDecoder decoder = new PngBitmapDecoder(ms,
                                BitmapCreateOptions.PreservePixelFormat,
                                BitmapCacheOption.OnLoad);
                            bitmap = decoder.Frames[0];

                            break;
                        }
                    case ".jpg":
                    case ".jpeg":
                    case "JPEG":
                    case "jpeg":
                    case "JPG":
                        {
                            JpegBitmapDecoder decoder = new JpegBitmapDecoder(ms,
                                BitmapCreateOptions.PreservePixelFormat,
                                BitmapCacheOption.OnLoad);
                            bitmap = decoder.Frames[0];
                            break;
                        }
                    default:
                        throw new Exception("not implemented yet image file type");
                }

                bitmap.Freeze();
                ms.Dispose();
                return bitmap;
            }
            catch(Exception ex)
            {
                return null;
            }
        }
    }
}

