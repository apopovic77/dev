﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BankLib.Data;

namespace BankLib.Gruppe
{
    public class RawGroupInfo
    {
        public static string GetKey(string buchungs_type, string umsatz_type)
        {
            string key = buchungs_type + ";" + umsatz_type;
            return key;
        }

        public static string GetKey(RawGroupInfo b)
        {
            string key = b.BuchungsType + ";" + b.UmsatzType;
            return key;
        }

        public static string GetKey(Buchung b)
        {
            string key = b.BuchungsType + ";" + b.UmsatzType;
            return key;
        }

        public string Key
        {
            get { return BuchungsType + ";" + UmsatzType; }
        }

        public void WriteToStream(BinaryWriter writer)
        {
            BinaryHelper.WriteString(writer, Name);
            BinaryHelper.WriteString(writer, SearchString);
            BinaryHelper.WriteString(writer, CompareType);
            BinaryHelper.WriteString(writer, BuchungsType);
            BinaryHelper.WriteString(writer, UmsatzType);
        }

        public void LoadFromStream(BinaryReader reader)
        {
            Name = BinaryHelper.ReadString(reader);
            SearchString = BinaryHelper.ReadString(reader);
            CompareType = BinaryHelper.ReadString(reader);
            BuchungsType = BinaryHelper.ReadString(reader);
            UmsatzType = BinaryHelper.ReadString(reader);
        }

        public string Name;
        public string BuchungsType;
        public string UmsatzType;
        public string CompareType;
        public string SearchString;
    }

}
