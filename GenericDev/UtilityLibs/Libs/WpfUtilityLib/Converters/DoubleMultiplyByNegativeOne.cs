﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;

namespace Logicx.WpfUtility.Converters
{
    [ValueConversion(typeof(object), typeof(string))]
    public class DoubleMultiplyByNegativeOne : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            double d_val = (double)value;
            return d_val *-1;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            double d_val = (double)value;
            return d_val *-1;
        }
    }
}
