﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;
using Logicx.Geo.Geometries;
using MathLib;

namespace Logicx.WpfUtility.WpfHelpers
{
    public static class DataTypeConverterFunctions
    {
        public static Polygon SimplePolygon2Polygon(SimplePolygon poly)
        {
            Polygon paintpoly = new Polygon();
            foreach (Vector2f vct in poly.Vertices)
            {
                paintpoly.Points.Add(new Point(vct.X, vct.Y));
            }

            return paintpoly;
        }

        public static Path SimplePolygon2Path(SimplePolygon poly, out BoundingBox bounding_box)
        {
            Path p = new Path();
            p.Data = SimplePolygon2StreamGeometry(poly,out bounding_box);
            return p;
        }

        public static StreamGeometry SimplePolygon2StreamGeometry(SimplePolygon poly, out BoundingBox bounding_box)
        {
            return SimplePolygon2StreamGeometry(poly.Vertices, out bounding_box);
        }

        public static StreamGeometry SimplePolygon2StreamGeometry(SimplePolygon poly)
        {
            BoundingBox bounding_box;
            return SimplePolygon2StreamGeometry(poly.Vertices, out bounding_box);
        }

        public static StreamGeometry SimplePolygon2StreamGeometry(List<Vector2f> poly_verts, out BoundingBox bounding_box)
        {
            float minx = float.MaxValue, maxx = float.MinValue, miny = float.MaxValue, maxy = float.MinValue;
            StreamGeometry streamgeometry = new StreamGeometry();
            using (StreamGeometryContext ctx = streamgeometry.Open())
            {
                Vector2f v = poly_verts[0];
                Point start_point = new Point(v.X, v.Y);
                if (v.X < minx) minx = v.X;
                if (v.X > maxx) maxx = v.X;
                if (v.Y < miny) miny = v.Y;
                if (v.Y > maxy) maxy = v.Y;
                ctx.BeginFigure(start_point, true /* is filled */, false /* is not closed? */);
                for (int i = 0; i < poly_verts.Count; i++)
                {
                    v = poly_verts[i];
                    if (v.X < minx) minx = v.X;
                    if (v.X > maxx) maxx = v.X;
                    if (v.Y < miny) miny = v.Y;
                    if (v.Y > maxy) maxy = v.Y;
                    ctx.LineTo(new Point(v.X, v.Y), true /* is stroked */, false/* is smooth join */);
                }
            }
            streamgeometry.FillRule = FillRule.Nonzero;

            //correct error bounding box
            Vector2d new_tl = new Vector2d(minx, miny);
            Vector2d new_br = new Vector2d(maxx, maxy);
            bounding_box = new BoundingBox(new_tl, new_br);

            return streamgeometry;
        }

        public static StreamGeometry SimplePolygon2StreamGeometry(List<Vector2d> poly_verts, out BoundingBox bounding_box)
        {
            double minx = double.MaxValue, maxx = double.MinValue, miny = double.MaxValue, maxy = double.MinValue;
            StreamGeometry streamgeometry = new StreamGeometry();
            using (StreamGeometryContext ctx = streamgeometry.Open())
            {
                Vector2d v = poly_verts[0];
                Point start_point = new Point(v.X, v.Y);
                if (v.X < minx) minx = v.X;
                if (v.X > maxx) maxx = v.X;
                if (v.Y < miny) miny = v.Y;
                if (v.Y > maxy) maxy = v.Y;
                ctx.BeginFigure(start_point, true /* is filled */, false /* is not closed? */);
                for (int i = 0; i < poly_verts.Count; i++)
                {
                    v = poly_verts[i];
                    if (v.X < minx) minx = v.X;
                    if (v.X > maxx) maxx = v.X;
                    if (v.Y < miny) miny = v.Y;
                    if (v.Y > maxy) maxy = v.Y;
                    ctx.LineTo(new Point(v.X, v.Y), true /* is stroked */, false/* is smooth join */);
                }
            }
            streamgeometry.FillRule = FillRule.Nonzero;

            //correct error bounding box
            Vector2d new_tl = new Vector2d(minx, miny);
            Vector2d new_br = new Vector2d(maxx, maxy);
            bounding_box = new BoundingBox(new_tl, new_br);

            return streamgeometry;
        }
    }
}
