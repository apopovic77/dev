﻿using System;
using System.Collections.Generic;
using System.Linq;
using GeneticProgrammingLib;
using GeneticProgrammingLib.FindSentence;

namespace GeneticProgrammingApp
{
    class Program
    {
        static void Main(string[] args)
        {
            string target_sentence = "to be or not to be";
            Console.WriteLine("Start search for " + target_sentence);
            FindSentenceGeneticAlgorithm fsga = new FindSentenceGeneticAlgorithm(target_sentence);
            fsga.CreateNewRandomPopulation();
            fsga.Run(false);
            Console.WriteLine("Done");
            Console.ReadLine();
        }
    }
}
