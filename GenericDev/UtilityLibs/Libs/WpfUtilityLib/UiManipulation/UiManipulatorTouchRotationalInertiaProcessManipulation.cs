﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace Logicx.WpfUtility.UiManipulation
{
    public class UiManipulatorTouchRotationalInertiaProcessManipulation : UiManipulation.TouchInertiaProcessManipulation
    {
        public UiManipulatorTouchRotationalInertiaProcessManipulation(UIElement ui_element, Canvas canvas, UIElement event_base, EnabledAxes enabled_axes, Rect element_size)
            : this(ui_element, canvas, event_base, enabled_axes, element_size, true)
        {
        }

        public UiManipulatorTouchRotationalInertiaProcessManipulation(UIElement ui_element, Canvas canvas, UIElement event_base, EnabledAxes enabled_axes, Rect element_size, bool initialize_canvas_position)
            : base(ui_element, canvas, event_base, enabled_axes, initialize_canvas_position)
        {
            _with_scaling = false;
            _with_rotation = false;

            _element_size = element_size;
        }

        

        public Rect ElementSize
        {
            get { return _element_size; }
            set { _element_size = value; }
        }

        public override void ProcessManipulationDelta(object sender, Windows7.Multitouch.Manipulation.ManipulationDeltaEventArgs e)
        {
            double left = Canvas.GetLeft(_ui_element);
            double top = Canvas.GetTop(_ui_element);

            base.ProcessManipulationDelta(sender,e);

            double next_left = Canvas.GetLeft(_ui_element);
            double next_top = Canvas.GetTop(_ui_element);

            UiManipulatorRotationalMouseDrag.GetRotationalPosition(_enabled_axes, _element_size, ref next_left, ref next_top);

            Canvas.SetLeft(_ui_element, next_left);
            Canvas.SetTop(_ui_element, next_top);
        }

        private Rect _element_size;
    }
}
