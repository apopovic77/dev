using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using Logicx.Utilities;

namespace Logicx.Utilities
{
    public class StartupSplashInfo : LabelDebugWriter 
    {
        private Form m_parent;
        public StartupSplashInfo(Label label, Form parent) :base(label)
        {
            m_label_output = label;
            m_parent = parent;
        }
        public override void Flush()
        {
            base.Flush();
            m_parent.Refresh();
        }
        public override void WriteInfo(string msg)
        {
            base.WriteInfo(msg);
            Flush();
        }
    }

    public class LabelDebugWriter : ConsoleDebugWriter, IDebugWriter
    {
        protected Label m_label_output;

        public LabelDebugWriter(Label label) {
            m_label_output = label;
            AutoFlush = false;
        }


		/// <summary>
		/// Delegate used for thread-safety reasons.
		/// </summary>
		/// <param name="dt"></param>
		private delegate void WriteInfoHandler(string dt);

        public virtual void WriteInfo(string msg)
        {
			if (this.m_label_output.InvokeRequired)
			{
				this.m_label_output.Invoke(new WriteInfoHandler(WriteInfo), new object[]{msg});
			}
			else
			{
				if (_withdateinfo)
					msg = DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() + ": " + msg;
				m_msgs.Add(msg);
				if (m_msgs.Count >= MaxMessagesToShow)
					m_msgs.RemoveAt(0);

				if (AutoFlush)
					Flush();
			}
        }

		/// <summary>
		/// Delegate used for thread-safety reasons.
		/// </summary>
		/// <param name="dt"></param>
		private delegate void FlushHandler();

        public virtual void Flush()
		{
			if (this.m_label_output.InvokeRequired)
			{
				this.m_label_output.Invoke(new FlushHandler(Flush), new object[]{});
			}
			else
			{
				m_label_output.Text = "";
				foreach (string msg2 in m_msgs)
				{
					m_label_output.Text += msg2 + "\n";
				}        
			}
        }
    }

}
