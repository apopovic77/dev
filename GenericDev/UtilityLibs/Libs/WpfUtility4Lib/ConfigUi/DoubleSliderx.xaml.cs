﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using Logicx.WpfUtility.WpfHelpers;

namespace Logicx.WpfUtility4.ConfigUi
{
    /// <summary>
    /// Interaction logic for DoubleSliderx.xaml
    /// </summary>
    public partial class DoubleSliderx : UserControl
    {
        public DoubleSliderx()
        {
            InitializeComponent();
            Loaded += new RoutedEventHandler(DoubleSliderx_Loaded);
        }

        void DoubleSliderx_Loaded(object sender, RoutedEventArgs e)
        {
            UpdateSliderDim(ValueStart, ValueEnd);
        }

        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            if (e.Property.Name == "Bezeichnung")
            {
                Bezeichnung = e.NewValue.ToString().ToUpper();
            }
            else if(e.Property.Name == "ValueStart")
            {
                UpdateSliderDim((double)e.NewValue, ValueEnd);
            }
            else if (e.Property.Name == "ValueEnd")
            {
                UpdateSliderDim(ValueStart, (double)e.NewValue);
            }
            base.OnPropertyChanged(e);
        }

        private void UpdateSliderDim(double value_start, double value_end)
        {
            double w = back_rect.ActualWidth;
            double diff_max = (Maximum - Minimum);
            double curr_diff_value_start = value_start - Minimum;
            double curr_diff_value_end = value_end - Minimum;
            double pos_start = curr_diff_value_start / diff_max;
            double pos_end = curr_diff_value_end / diff_max;
            value_rect.Width = w * (pos_end - pos_start);
            value_rect.Margin = new Thickness(w * pos_start, 0, 0, 0);

            start_thumb.Margin = new Thickness(w * pos_start, 0, 0, 0);
            end_thumb.Margin = new Thickness(w * pos_end - end_thumb.ActualWidth, 0, 0, 0);
        }

        private void FinishUiUpdate()
        {
            IsHitTestVisible = true;

            EventBase.MouseUp -= new MouseButtonEventHandler(EventBase_MouseUp);
            EventBase.MouseMove -= new MouseEventHandler(EventBase_MouseMove);
            EventBase.MouseLeave -= new MouseEventHandler(EventBase_MouseLeave);
            EventBase.MouseEnter -= new MouseEventHandler(EventBase_MouseEnter);

            start_thumb.Fill = _normal_brush;
            end_thumb.Fill = _normal_brush;

            _ui_manipulation_active = false;
        }

        private void start_thumb_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.MouseDevice.LeftButton != MouseButtonState.Pressed)
                return;

            e.Handled = true;

            Point p_offset = e.GetPosition(start_thumb);
            double w = back_rect.ActualWidth;
            double diff_max = (Maximum - Minimum);
            _start_value_offset = diff_max * p_offset.X / w;
            _x_offset = p_offset.X;

            start_thumb.Fill = _active_brush;
            _ui_manipulation_active = true;

            IsHitTestVisible = false;

            EventBase.MouseUp += new MouseButtonEventHandler(EventBase_MouseUp);
            EventBase.MouseMove += new MouseEventHandler(EventBase_MouseMove);
            EventBase.MouseEnter += new MouseEventHandler(EventBase_MouseEnter);
            EventBase.MouseLeave += new MouseEventHandler(EventBase_MouseLeave);

            _start_thumb_moving = true;
        }

        private void end_thumb_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.MouseDevice.LeftButton != MouseButtonState.Pressed)
                return;

            e.Handled = true;

            Point p_offset = e.GetPosition(end_thumb);
            double w = back_rect.ActualWidth;
            double diff_max = (Maximum - Minimum);
            _start_value_offset = diff_max * p_offset.X / w;
            _x_offset = p_offset.X;

            end_thumb.Fill = _active_brush;
            _ui_manipulation_active = true;

            IsHitTestVisible = false;

            EventBase.MouseUp += new MouseButtonEventHandler(EventBase_MouseUp);
            EventBase.MouseMove += new MouseEventHandler(EventBase_MouseMove);
            EventBase.MouseEnter += new MouseEventHandler(EventBase_MouseEnter);
            EventBase.MouseLeave += new MouseEventHandler(EventBase_MouseLeave);

            _start_thumb_moving = false;
        }

        private void start_thumb_MouseEnter(object sender, MouseEventArgs e)
        {
            if (_ui_manipulation_active)
                return;
            start_thumb.Fill = _active_brush;
        }

        private void end_thumb_MouseEnter(object sender, MouseEventArgs e)
        {
            if (_ui_manipulation_active)
                return;
            end_thumb.Fill = _active_brush;
        }

        private void start_thumb_MouseLeave(object sender, MouseEventArgs e)
        {
            if (_ui_manipulation_active)
                return;
            start_thumb.Fill = _normal_brush;
        }

        private void end_thumb_MouseLeave(object sender, MouseEventArgs e)
        {
            if (_ui_manipulation_active)
                return;
            end_thumb.Fill = _normal_brush;
        }

        void EventBase_MouseLeave(object sender, MouseEventArgs e)
        {
        }

        void EventBase_MouseUp(object sender, MouseButtonEventArgs e)
        {
            FinishUiUpdate();
        }

        void EventBase_MouseEnter(object sender, MouseEventArgs e)
        {
            if (e.MouseDevice.LeftButton == MouseButtonState.Released)
                FinishUiUpdate();
        }

        void EventBase_MouseMove(object sender, MouseEventArgs e)
        {
            Point p = e.GetPosition(back_rect);

            if (_start_thumb_moving)
            {
                double w = back_rect.ActualWidth;
                double diff_max = (Maximum - Minimum);
                double thumb_value_dim = diff_max * end_thumb.ActualWidth / w;

                if (p.X <= 0)
                {
                    ValueStart = Minimum;
                }
                else if (p.X >= back_rect.ActualWidth)
                {
                    ValueStart = ValueEnd - thumb_value_dim*2;
                }
                else
                {

                    double value_start = Minimum + ((p.X - _x_offset) * (Maximum - Minimum) / back_rect.ActualWidth);
                    if (value_start > ValueEnd - thumb_value_dim * 2)
                        ValueStart = ValueEnd - thumb_value_dim * 2;
                    else
                        ValueStart = value_start;
                }
            }
            else
            {
                double w = back_rect.ActualWidth;
                double diff_max = (Maximum - Minimum);
                double thumb_value_dim = diff_max * end_thumb.ActualWidth / w;

                if (p.X <= 0)
                {
                    ValueEnd = ValueStart + thumb_value_dim*2;
                }
                else if (p.X >= back_rect.ActualWidth)
                {
                    ValueEnd = Maximum;
                }
                else
                {
                    double value_end = Minimum + ((p.X + (end_thumb.ActualWidth - _x_offset)) * (Maximum - Minimum) / back_rect.ActualWidth);
                    if (value_end < ValueStart + thumb_value_dim * 2)
                        value_end = ValueStart + thumb_value_dim * 2;
                    else
                        ValueEnd = value_end;
                }
            }


        }

        private UIElement EventBase
        {
            get
            {
                Window window = UIHelper.TryFindParent<Window>(this);
                if (window != null)
                    return window;

                Page page = UIHelper.TryFindParent<Page>(this);
                if (page != null)
                    return page;

                UIElement parent = Parent as UIElement;
                return parent;
            }
        }

        #region Dependency Properties
        public string Bezeichnung
        {
            get { return (string)GetValue(BezeichnungProperty); }
            set { SetValue(BezeichnungProperty, value); }
        }
        public static readonly DependencyProperty BezeichnungProperty = DependencyProperty.Register("Bezeichnung", typeof(string), typeof(DoubleSliderx), new UIPropertyMetadata(""));

        public double ValueStart
        {
            get { return (double)GetValue(ValueStartProperty); }
            set { SetValue(ValueStartProperty, value); }
        }
        public static readonly DependencyProperty ValueStartProperty = DependencyProperty.Register("ValueStart", typeof(double), typeof(DoubleSliderx), new UIPropertyMetadata((double)0));

        public double ValueEnd
        {
            get { return (double)GetValue(ValueEndProperty); }
            set { SetValue(ValueEndProperty, value); }
        }
        public static readonly DependencyProperty ValueEndProperty = DependencyProperty.Register("ValueEnd", typeof(double), typeof(DoubleSliderx), new UIPropertyMetadata((double)0));

        
        public double Maximum
        {
            get { return (double)GetValue(MaximumProperty); }
            set { SetValue(MaximumProperty, value); }
        }
        public static readonly DependencyProperty MaximumProperty = DependencyProperty.Register("Maximum", typeof(double), typeof(DoubleSliderx), new UIPropertyMetadata((double)100));

        public double Minimum
        {
            get { return (double)GetValue(MinimumProperty); }
            set { SetValue(MinimumProperty, value); }
        }
        public static readonly DependencyProperty MinimumProperty = DependencyProperty.Register("Minimum", typeof(double), typeof(DoubleSliderx), new UIPropertyMetadata((double)0));
        #endregion


        private static readonly Brush _active_brush = new SolidColorBrush(Color.FromRgb(0x00, 0x82, 0xa4));
        private static readonly Brush _normal_brush = Brushes.Black;
        private bool _start_thumb_moving;
        private bool _ui_manipulation_active;
        private double _start_value_offset;
        private double _x_offset;


    }
}
