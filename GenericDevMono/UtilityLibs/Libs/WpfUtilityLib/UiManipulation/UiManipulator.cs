﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace Logicx.WpfUtility.UiManipulation
{
    public class UiManipulationEventArgs : EventArgs
    {
        public UiManipulationEventArgs(UIElement ui_element, Point current_pos)
        {
            UIElement = ui_element;
            CurrentPosition = current_pos;
        }

        public UIElement UiElement
        {
            get { return UIElement; }
        }

        public Point ActualPosition
        {
            get { return CurrentPosition; }
        }

        private UIElement UIElement;
        private Point CurrentPosition;
    }



    public abstract class UiManipulator : IDisposable
    {
        public enum EnabledAxes
        {
            All,
            XAxis,
            YAxis
        }

        public UiManipulator(UIElement ui_element, Canvas canvas, UIElement event_base, EnabledAxes enabled_axes) : this(ui_element, canvas, event_base, enabled_axes, true)
        {
            
        }

        public UiManipulator(UIElement ui_element, Canvas canvas, UIElement event_base, EnabledAxes enabled_axes, bool initialize_canvas_position)
        {
            _event_base = event_base;
            _ui_element = ui_element;
            _canvas = canvas;
            _enabled_axes = enabled_axes;

            if (initialize_canvas_position)
            {
                if (double.IsNaN(Canvas.GetLeft(_ui_element)))
                    Canvas.SetLeft(_ui_element, 0);
                if (double.IsNaN(Canvas.GetTop(_ui_element)))
                    Canvas.SetTop(_ui_element, 0);
            }

            Register();
        }

        public abstract void Register();
        public abstract void DeRegister();

        protected void SetCanvasBackground()
        {
            _org_canvas_brush = _canvas.Background;
            if (_org_canvas_brush == null)
                _canvas.Background = Brushes.Transparent;
        }
        protected void UnsetCanvasBackground()
        {
            _canvas.Background = _org_canvas_brush;
            _org_canvas_brush = null;
        }

        #region Raising Events
        protected void RailsUiElementPositionChanged(UIElement ui_element, Point current_pos)
        {
            if (UiElementPositionChanged != null)
                UiElementPositionChanged(this, new UiManipulationEventArgs(ui_element, current_pos));
        }
        protected void RaisUiManipulationStarted(UIElement ui_element)
        {
            if (UiManipulationStarted != null)
            {
                Point ui_pos_next = new Point(Canvas.GetLeft(_ui_element), Canvas.GetTop(_ui_element));
                UiManipulationStarted(this, new UiManipulationEventArgs(ui_element, ui_pos_next));
            }
        }
        protected void RaiseUiManipulationCompleted(UIElement ui_element)
        {
            if (UiManipulationCompleted != null)
            {
                Point ui_pos_next = new Point(Canvas.GetLeft(_ui_element), Canvas.GetTop(_ui_element));
                UiManipulationCompleted(this, new UiManipulationEventArgs(ui_element, ui_pos_next));
            }
        }
        #endregion

        public void Dispose()
        {
            DeRegister();
        }

        public UIElement UIElement
        {
            get { return _ui_element; }
        }

        public EnabledAxes EnableAxes
        {
            get { return _enabled_axes; }
            set { _enabled_axes = value; }
        }

        protected UIElement _ui_element;
        protected Canvas _canvas;
        protected Brush _org_canvas_brush;
        protected EnabledAxes _enabled_axes;
        protected UIElement _event_base;

        public event EventHandler<UiManipulationEventArgs> UiElementPositionChanged;
        public event EventHandler<UiManipulationEventArgs> UiManipulationStarted;
        public event EventHandler<UiManipulationEventArgs> UiManipulationCompleted;
    }
}
