﻿using System;
using System.Collections.Generic;
using Logicx.Geo.Geometries;
using MathLib;

namespace Logicx.Geo.SpatialIndex
{
    /// <summary>
    /// Interface für QuadTree nodes (left, top, right, bottom, contained object)
    /// </summary>
    public interface IQuadTreeNode
    {
        double GetBoundingBoxTopLeftX();
        double GetBoundingBoxTopLeftY();
        double GetBoundingBoxBottomRightX();
        double GetBoundingBoxBottomRightY();
        
        /// <summary>
        /// Instance of an element in the quadtree. 
        /// </summary>
        object Instance { get; }
    }


    public class QuadTree<T> where T : IQuadTreeNode
    {
        protected class QuadTreeNode
        {
            #region Initialization
            public QuadTreeNode(BoundingBox indexed_area, int depth)
            {
                IndexedArea = indexed_area;
                Depth = depth;
            }

            /// <summary>
            /// -----
            /// |0|1|
            /// -----
            /// |3|2|
            /// -----
            /// </summary>
            private void CreateSubNodes()
            {
                SubNodes = new QuadTreeNode[4];

                int subnode_depth = Depth + 1;

                BoundingBox b0, b1, b2, b3;
                GetSubNodeBoundingBoxes(out b0, out b1, out b2, out b3);
                SubNodes[0] = new QuadTreeNode(b0, subnode_depth);
                SubNodes[1] = new QuadTreeNode(b1, subnode_depth);
                SubNodes[2] = new QuadTreeNode(b2, subnode_depth);
                SubNodes[3] = new QuadTreeNode(b3, subnode_depth);
            }

            private void GetSubNodeBoundingBoxes(out BoundingBox b0, out BoundingBox b1, out BoundingBox b2, out BoundingBox b3)
            {
                double subnode_side_len = IndexedArea.Width / 2;
                Vector2d center_this = IndexedArea.Center;

                b0 = new BoundingBox(new Vector2d(center_this.X - subnode_side_len, center_this.Y - subnode_side_len), center_this);
                b1 = new BoundingBox(new Vector2d(center_this.X, center_this.Y - subnode_side_len), new Vector2d(center_this.X + subnode_side_len, center_this.Y));
                b2 = new BoundingBox(center_this, new Vector2d(center_this.X + subnode_side_len, center_this.Y + subnode_side_len));
                b3 = new BoundingBox(new Vector2d(center_this.X - subnode_side_len, center_this.Y), new Vector2d(center_this.X, center_this.Y + subnode_side_len));
            }

            #endregion

            #region INSERTION
            public void Insert(T geowpfobj, BoundingBox bounding_box, int max_depth)
            {
                if (Depth == max_depth)
                {
                    //rect must be stored in this node
                    if (Objects == null)
                        Objects = new List<T>();

                    Objects.Add(geowpfobj);
                    return;
                }

                if (SubNodes == null)
                    CreateSubNodes();

                double subnode_side_len = IndexedArea.Width / 2;
                Vector2d this_center = IndexedArea.Center;

                if ( /*RECT SIZE FIT  */ bounding_box.Width > subnode_side_len || bounding_box.Height > subnode_side_len ||
                    /*XLINE INTERSECT*/ (bounding_box.TopLeft.Y < this_center.Y && bounding_box.BottomRight.Y > this_center.Y) ||
                    /*YLINE INTERSECT*/ (bounding_box.TopLeft.X < this_center.X && bounding_box.BottomRight.X > this_center.X)
                  )
                {
                    //rect must be stored in this node
                    if (Objects == null)
                        Objects = new List<T>();

                    Objects.Add(geowpfobj);
                    return;
                }

                if (bounding_box.TopLeft.X <= this_center.X && bounding_box.BottomRight.X <= this_center.X &&
                    bounding_box.TopLeft.Y <= this_center.Y && bounding_box.BottomRight.Y <= this_center.Y)
                {
                    SubNodes[0].Insert(geowpfobj, bounding_box, max_depth);
                    return;
                }
                if (bounding_box.TopLeft.X >= this_center.X && bounding_box.BottomRight.X >= this_center.X &&
                    bounding_box.TopLeft.Y <= this_center.Y && bounding_box.BottomRight.Y <= this_center.Y)
                {
                    SubNodes[1].Insert(geowpfobj, bounding_box, max_depth);
                    return;
                }
                if (bounding_box.TopLeft.X >= this_center.X && bounding_box.BottomRight.X >= this_center.X &&
                    bounding_box.TopLeft.Y >= this_center.Y && bounding_box.BottomRight.Y >= this_center.Y)
                {
                    SubNodes[2].Insert(geowpfobj, bounding_box, max_depth);
                    return;
                }
                if (bounding_box.TopLeft.X <= this_center.X && bounding_box.BottomRight.X <= this_center.X &&
                    bounding_box.TopLeft.Y >= this_center.Y && bounding_box.BottomRight.Y >= this_center.Y)
                {
                    SubNodes[3].Insert(geowpfobj, bounding_box, max_depth);
                    return;
                }

                throw new Exception("this cannot be check alogrithm");
            }
            #endregion

            #region Removal
            public bool Remove(T obj)
            {
                if (RemoveFromObjects(obj))
                    return true;

                if (SubNodes == null)
                    return false;

                if (SubNodes[0].Remove(obj))
                    return true;
                if (SubNodes[1].Remove(obj))
                    return true;
                if (SubNodes[2].Remove(obj))
                    return true;
                if (SubNodes[3].Remove(obj))
                    return true;
                return false;
            }
            private bool RemoveFromObjects(T obj)
            {
                if (Objects == null) return false;
                for (int i = 0; i < Objects.Count; i++)
                {
                    if (obj.Instance == Objects[i].Instance)
                    {
                        Objects.RemoveAt(i);
                        if (Objects.Count == 0)
                        {
                            Objects = null;
                        }
                        return true;
                    }
                }
                return false;
            }
            #endregion

            #region Collision Detection
            public bool IsColliding(BoundingBox query_area)
            {
                /////////////
                //check for fully overlapping query area
                //if so, we can return all subnodes immediatelly
                if (query_area.TopLeft.X <= IndexedArea.TopLeft.X && query_area.BottomRight.X >= IndexedArea.BottomRight.X &&
                    query_area.TopLeft.Y <= IndexedArea.TopLeft.Y && query_area.BottomRight.Y >= IndexedArea.BottomRight.Y)
                {
                    if (Objects == null && SubNodes == null)
                        return false;
                    else
                        return true;
                }

                //////////////
                //check this objects for overlapping
                if (Objects != null)
                {
                    for (int i = 0; i < Objects.Count; i++)
                    {
                        T obj_i = Objects[i];
                        double topleft_x = obj_i.GetBoundingBoxTopLeftX();
                        double topleft_y = obj_i.GetBoundingBoxTopLeftY();
                        double bottomright_x = obj_i.GetBoundingBoxBottomRightX();
                        double bottomright_y = obj_i.GetBoundingBoxBottomRightY();
                        if (!(query_area.BottomRight.X <= topleft_x || query_area.TopLeft.X >= bottomright_x ||
                             query_area.BottomRight.Y <= topleft_y || query_area.TopLeft.Y >= bottomright_y))
                        {
                            return true;
                        }
                    }
                }

                //////////////
                //check subnodes for overlapping objects
                if (SubNodes == null) return false;
                BoundingBox b0, b1, b2, b3;
                GetSubNodeBoundingBoxes(out b0, out b1, out b2, out b3);
                if (!(query_area.BottomRight.X <= b0.TopLeft.X || query_area.TopLeft.X >= b0.BottomRight.X ||
                      query_area.BottomRight.Y <= b0.TopLeft.Y || query_area.TopLeft.Y >= b0.BottomRight.Y))
                    if (SubNodes[0].IsColliding(query_area))
                        return true;
                if (!(query_area.BottomRight.X <= b1.TopLeft.X || query_area.TopLeft.X >= b1.BottomRight.X ||
                      query_area.BottomRight.Y <= b1.TopLeft.Y || query_area.TopLeft.Y >= b1.BottomRight.Y))
                    if (SubNodes[1].IsColliding(query_area))
                        return true;
                if (!(query_area.BottomRight.X <= b2.TopLeft.X || query_area.TopLeft.X >= b2.BottomRight.X ||
                      query_area.BottomRight.Y <= b2.TopLeft.Y || query_area.TopLeft.Y >= b2.BottomRight.Y))
                    if (SubNodes[2].IsColliding(query_area))
                        return true;
                if (!(query_area.BottomRight.X <= b3.TopLeft.X || query_area.TopLeft.X >= b3.BottomRight.X ||
                      query_area.BottomRight.Y <= b3.TopLeft.Y || query_area.TopLeft.Y >= b3.BottomRight.Y))
                    if (SubNodes[3].IsColliding(query_area))
                        return true;

                return false;
            }
            #endregion

            #region Query
            public void Query(BoundingBox query_area, List<T> found_objs)
            {
                /////////////
                //check for fully overlapping query area
                //if so, we can return all subnodes immediatelly
                if (query_area.TopLeft.X <= IndexedArea.TopLeft.X && query_area.BottomRight.X >= IndexedArea.BottomRight.X &&
                    query_area.TopLeft.Y <= IndexedArea.TopLeft.Y && query_area.BottomRight.Y >= IndexedArea.BottomRight.Y)
                {
                    GetAllSubObjects(found_objs);
                    return;
                }


                //////////////
                //check this objects for overlapping
                if (Objects != null)
                {
                    for (int i = 0; i < Objects.Count; i++)
                    {
                        T obj_i = Objects[i];
                        double topleft_x = obj_i.GetBoundingBoxTopLeftX();
                        double topleft_y = obj_i.GetBoundingBoxTopLeftY();
                        double bottomright_x = obj_i.GetBoundingBoxBottomRightX();
                        double bottomright_y = obj_i.GetBoundingBoxBottomRightY();
                        if (!(query_area.BottomRight.X <= topleft_x || query_area.TopLeft.X >= bottomright_x ||
                             query_area.BottomRight.Y <= topleft_y || query_area.TopLeft.Y >= bottomright_y))
                        {
                            found_objs.Add(obj_i);
                        }
                    }
                }



                //////////////
                //check subnodes for overlapping objects
                if (SubNodes == null) return;
                BoundingBox b0, b1, b2, b3;
                GetSubNodeBoundingBoxes(out b0, out b1, out b2, out b3);
                if (!(query_area.BottomRight.X <= b0.TopLeft.X || query_area.TopLeft.X >= b0.BottomRight.X ||
                      query_area.BottomRight.Y <= b0.TopLeft.Y || query_area.TopLeft.Y >= b0.BottomRight.Y))
                    SubNodes[0].Query(query_area, found_objs);
                if (!(query_area.BottomRight.X <= b1.TopLeft.X || query_area.TopLeft.X >= b1.BottomRight.X ||
                      query_area.BottomRight.Y <= b1.TopLeft.Y || query_area.TopLeft.Y >= b1.BottomRight.Y))
                    SubNodes[1].Query(query_area, found_objs);
                if (!(query_area.BottomRight.X <= b2.TopLeft.X || query_area.TopLeft.X >= b2.BottomRight.X ||
                      query_area.BottomRight.Y <= b2.TopLeft.Y || query_area.TopLeft.Y >= b2.BottomRight.Y))
                    SubNodes[2].Query(query_area, found_objs);
                if (!(query_area.BottomRight.X <= b3.TopLeft.X || query_area.TopLeft.X >= b3.BottomRight.X ||
                      query_area.BottomRight.Y <= b3.TopLeft.Y || query_area.TopLeft.Y >= b3.BottomRight.Y))
                    SubNodes[3].Query(query_area, found_objs);
            }

            private void GetAllSubObjects(List<T> found_objs)
            {
                if (Objects != null)
                    for (int i = 0; i < Objects.Count; i++)
                    {
                        found_objs.Add(Objects[i]);
                    }

                if (SubNodes == null)
                    return;

                SubNodes[0].GetAllSubObjects(found_objs);
                SubNodes[1].GetAllSubObjects(found_objs);
                SubNodes[2].GetAllSubObjects(found_objs);
                SubNodes[3].GetAllSubObjects(found_objs);
            }

            #endregion

            #region Attribs
            protected int Depth;
            public BoundingBox IndexedArea;
            public List<T> Objects;
            public QuadTreeNode[] SubNodes;
            #endregion
        }
        
        #region Initialization
        public QuadTree(BoundingBox indexed_area)
        {
            EnsureIndexedAreaIsQuad(ref indexed_area);
            _root = new QuadTreeNode(indexed_area,1);
        }

        public QuadTree(BoundingBox indexed_area, int max_depth)
        {
            EnsureIndexedAreaIsQuad(ref indexed_area);
            _root = new QuadTreeNode(indexed_area, 1);
            _max_depth = max_depth;
        }

        /// <summary>
        /// ensure indexed area is a quad
        /// </summary>
        private void EnsureIndexedAreaIsQuad(ref BoundingBox indexed_area)
        {
            if (indexed_area.Width == indexed_area.Height)
                return;

            Vector2d center = indexed_area.Center;
            double long_side = indexed_area.Width;
            if (indexed_area.Height > indexed_area.Width)
                long_side = indexed_area.Height;

            double half = long_side / 2;
            indexed_area.TopLeft = new Vector2d(center.X - half, center.Y - half);
            indexed_area.BottomRight = new Vector2d(center.X + half, center.Y + half);
        }
        #endregion

        public void Insert(T obj, BoundingBox bounding_box)
        {
            _root.Insert(obj, bounding_box, _max_depth);
        }

        public List<T> Query(BoundingBox query_area)
        {
            List<T> found_objs = new List<T>();
            _root.Query(query_area, found_objs);
            return found_objs;
        }

        public bool IsColliding(BoundingBox query_area)
        {
            return _root.IsColliding(query_area);
        }

        public void Remove(T obj)
        {
            _root.Remove(obj);
        }

        public void ClearTree()
        {
            _root = new QuadTreeNode(_root.IndexedArea,1);
        }


        #region Attribs
        protected int _max_depth = 20;
        protected QuadTreeNode _root;
        #endregion
    }
}
