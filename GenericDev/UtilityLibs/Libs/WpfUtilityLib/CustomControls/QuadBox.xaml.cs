﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Logicx.WpfUtility.CustomControls
{
    /// <summary>
    /// Interaction logic for QuadBox.xaml
    /// </summary>
    public partial class QuadBox : UserControl
    {
        public QuadBox()
        {
            InitializeComponent();
        }


        #region Properties
        public double TextBoxWidth
        {
            get { return (double)GetValue(TextBoxWidthProperty); }
            set { SetValue(TextBoxWidthProperty, value); }
        }
        public static readonly DependencyProperty TextBoxWidthProperty = DependencyProperty.Register("TextBoxWidth", typeof(double), typeof(QuadBox), new UIPropertyMetadata(null));
        public double LabelWidth
        {
            get { return (double)GetValue(LabelWidthProperty); }
            set { SetValue(LabelWidthProperty, value); }
        }
        public static readonly DependencyProperty LabelWidthProperty = DependencyProperty.Register("LabelWidth", typeof(double), typeof(QuadBox), new UIPropertyMetadata(null));



        public double LabelFontSize
        {
            get { return (double)GetValue(LabelFontSizeProperty); }
            set { SetValue(LabelFontSizeProperty, value); }
        }
        public static readonly DependencyProperty LabelFontSizeProperty = DependencyProperty.Register("LabelFontSize", typeof(double), typeof(QuadBox), new UIPropertyMetadata((double)10));



        public object TextboxValue1
        {
            get { return (object)GetValue(TextboxValue1Property); }
            set { SetValue(TextboxValue1Property, value); }
        }
        public static readonly DependencyProperty TextboxValue1Property = DependencyProperty.Register("TextboxValue1", typeof(object), typeof(QuadBox), new UIPropertyMetadata(null));

        public object TextboxValue2
        {
            get { return (object)GetValue(TextboxValue2Property); }
            set { SetValue(TextboxValue2Property, value); }
        }
        public static readonly DependencyProperty TextboxValue2Property = DependencyProperty.Register("TextboxValue2", typeof(object), typeof(QuadBox), new UIPropertyMetadata(null));

        public object TextboxValue3
        {
            get { return (object)GetValue(TextboxValue3Property); }
            set { SetValue(TextboxValue3Property, value); }
        }
        public static readonly DependencyProperty TextboxValue3Property = DependencyProperty.Register("TextboxValue3", typeof(object), typeof(QuadBox), new UIPropertyMetadata(null));

        public object TextboxValue4
        {
            get { return (object)GetValue(TextboxValue4Property); }
            set { SetValue(TextboxValue4Property, value); }
        }
        public static readonly DependencyProperty TextboxValue4Property = DependencyProperty.Register("TextboxValue4", typeof(object), typeof(QuadBox), new UIPropertyMetadata(null));


        public string LabelText1
        {
            get { return (string)GetValue(LabelText1Property); }
            set { SetValue(LabelText1Property, value); }
        }
        public static readonly DependencyProperty LabelText1Property = DependencyProperty.Register("LabelText1", typeof(string), typeof(QuadBox), new UIPropertyMetadata(""));
        public string LabelText2
        {
            get { return (string)GetValue(LabelText2Property); }
            set { SetValue(LabelText2Property, value); }
        }
        public static readonly DependencyProperty LabelText2Property = DependencyProperty.Register("LabelText2", typeof(string), typeof(QuadBox), new UIPropertyMetadata(""));
        public string LabelText3
        {
            get { return (string)GetValue(LabelText3Property); }
            set { SetValue(LabelText3Property, value); }
        }
        public static readonly DependencyProperty LabelText3Property = DependencyProperty.Register("LabelText3", typeof(string), typeof(QuadBox), new UIPropertyMetadata(""));
        public string LabelText4
        {
            get { return (string)GetValue(LabelText4Property); }
            set { SetValue(LabelText4Property, value); }
        }
        public static readonly DependencyProperty LabelText4Property = DependencyProperty.Register("LabelText4", typeof(string), typeof(QuadBox), new UIPropertyMetadata(""));
        #endregion

        #region operations
        protected virtual void OnDataChanged(EventArgs e)
        {
            if (DataChanged != null)
                DataChanged(this, e);
        }
        #endregion

        #region event handler
        void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            textBox1.TextChanged += new TextChangedEventHandler(TextBox_TextChanged);
            textBox2.TextChanged += new TextChangedEventHandler(TextBox_TextChanged);
            textBox3.TextChanged += new TextChangedEventHandler(TextBox_TextChanged);
            textBox4.TextChanged += new TextChangedEventHandler(TextBox_TextChanged);    
        }

        void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            OnDataChanged(new EventArgs());
        }
        #endregion

        public event EventHandler DataChanged;
    }
}
