using System;
using MathLib;

namespace GeoLib
{
	/// <summary>
	/// 
	/// </summary>
	public class Rectangle
	{
		private Vector2d m_corner1;
		private Vector2d m_corner2;
		private Vector2d m_corner3;
		private Vector2d m_corner4;

        public Vector2d Corner1 { set { AssertDifferentCornerValues(m_corner1); m_corner1 = value; } get { return m_corner1; } }
        public Vector2d Corner2 { set { AssertDifferentCornerValues(m_corner2); m_corner2 = value; } get { return m_corner2; } }
        public Vector2d Corner3 { set { AssertDifferentCornerValues(m_corner3); m_corner3 = value; } get { return m_corner3; } }
        public Vector2d Corner4 { set { AssertDifferentCornerValues(m_corner4); m_corner4 = value; } get { return m_corner4; } }

        private void AssertDifferentCornerValues(Vector2d corner) {
            if (m_corner1 != null && corner != m_corner1)
                if (m_corner2 != null && corner != m_corner2)
                    if (m_corner3 != null && corner != m_corner3)
                        if (m_corner4 != null && corner != m_corner4)
                            return;
            throw new GeoLibException("Rectangle is no Rectangle if two corners have the same value");
        }


        public Vector2d Mostleft { get {
            Vector2d mostleft = m_corner1;
            if (m_corner2.X < mostleft.X)
                mostleft = m_corner2;
            if (m_corner3.X < mostleft.X)
                mostleft = m_corner3;
            if (m_corner4.X < mostleft.X)
                mostleft = m_corner4;
            return mostleft;
            }
        } 

        public Vector2d Mostright { get { 
            Vector2d mostright = m_corner1;
            if (m_corner2.X > mostright.X)
                mostright = m_corner2;
            if (m_corner3.X > mostright.X)
                mostright = m_corner3;
            if (m_corner4.X > mostright.X)
                mostright = m_corner4;
            return mostright;
        } }
        public Vector2d Mosttop { get { 
            Vector2d mosttop = m_corner1;
            if (m_corner2.Y > mosttop.Y)
                mosttop = m_corner2;
            if (m_corner3.Y > mosttop.Y)
                mosttop = m_corner3;
            if (m_corner4.Y > mosttop.Y)
                mosttop = m_corner4;
            return mosttop;
        } }
        public Vector2d Mostbottom
        {
            get
            {
                Vector2d mostbottom = m_corner1;
                if (m_corner2.Y < mostbottom.Y)
                    mostbottom = m_corner2;
                if (m_corner3.Y < mostbottom.Y)
                    mostbottom = m_corner3;
                if (m_corner4.Y < mostbottom.Y)
                    mostbottom = m_corner4;
                return mostbottom;
            }
        }

        public bool IsRectIntersectGiven(GeoLib.Rectangle rect_to_check)
        {
            MathLib.Vector2d mostleft = rect_to_check.Mostleft;
            MathLib.Vector2d mostright = rect_to_check.Mostright;
            MathLib.Vector2d mosttop = rect_to_check.Mosttop;
            MathLib.Vector2d mostbottom = rect_to_check.Mostbottom;

            //check ob ein rechteck ECK innerhalb der region liegt
            if (this.Corner1.X >= mostleft.X &&
                this.Corner1.X <= mostright.X &&
                this.Corner1.Y <= mosttop.Y &&
                this.Corner1.Y >= mostbottom.Y)
                return true;
            if (this.Corner2.X >= mostleft.X &&
                this.Corner2.X <= mostright.X &&
                this.Corner2.Y <= mosttop.Y &&
                this.Corner2.Y >= mostbottom.Y)
                return true;
            if (this.Corner3.X >= mostleft.X &&
                this.Corner3.X <= mostright.X &&
                this.Corner3.Y <= mosttop.Y &&
                this.Corner3.Y >= mostbottom.Y)
                return true;
            if (this.Corner4.X >= mostleft.X &&
                this.Corner4.X <= mostright.X &&
                this.Corner4.Y <= mosttop.Y &&
                this.Corner4.Y >= mostbottom.Y)
                return true;

            //check ob das rect komplett ausserhalb liegt
            if (this.Mostleft.X <= mostleft.X &&
                this.Mostright.X >= mostright.X &&
                this.Mosttop.Y >= mosttop.Y &&
                this.Mostbottom.Y <= mostbottom.Y)
                return true;

            //mach den check auch umgekehrt
            mostleft = this.Mostleft;
            mostright = this.Mostright;
            mosttop = this.Mosttop;
            mostbottom = this.Mostbottom;

            if (rect_to_check.Corner1.X >= mostleft.X &&
                rect_to_check.Corner1.X <= mostright.X &&
                rect_to_check.Corner1.Y <= mosttop.Y &&
                rect_to_check.Corner1.Y >= mostbottom.Y)
                return true;
            if (rect_to_check.Corner2.X >= mostleft.X &&
                rect_to_check.Corner2.X <= mostright.X &&
                rect_to_check.Corner2.Y <= mosttop.Y &&
                rect_to_check.Corner2.Y >= mostbottom.Y)
                return true;
            if (rect_to_check.Corner3.X >= mostleft.X &&
                rect_to_check.Corner3.X <= mostright.X &&
                rect_to_check.Corner3.Y <= mosttop.Y &&
                rect_to_check.Corner3.Y >= mostbottom.Y)
                return true;
            if (rect_to_check.Corner4.X >= mostleft.X &&
                rect_to_check.Corner4.X <= mostright.X &&
                rect_to_check.Corner4.Y <= mosttop.Y &&
                rect_to_check.Corner4.Y >= mostbottom.Y)
                return true;

            //check ob das rect komplett ausserhalb liegt
            if (rect_to_check.Mostleft.X <= mostleft.X &&
                rect_to_check.Mostright.X >= mostright.X &&
                rect_to_check.Mosttop.Y >= mosttop.Y &&
                rect_to_check.Mostbottom.Y <= mostbottom.Y)
                return true;

            MathLib.Edge e1 = new Edge(Corner1, Corner2);
            MathLib.Edge e2 = new Edge(Corner2, Corner3);
            MathLib.Edge e3 = new Edge(Corner3, Corner4);
            MathLib.Edge e4 = new Edge(Corner4, Corner2);

            MathLib.Edge e_tocheck_1 = new Edge(Corner1, Corner2);
            MathLib.Edge e_tocheck_2 = new Edge(Corner2, Corner3);
            MathLib.Edge e_tocheck_3 = new Edge(Corner3, Corner4);
            MathLib.Edge e_tocheck_4 = new Edge(Corner4, Corner2);

            Vector2d v;
            if (LinearAlgebra.GetCutPointInSegment(e1, e_tocheck_1, out v)) return true;
            if (LinearAlgebra.GetCutPointInSegment(e1, e_tocheck_2, out v)) return true;
            if (LinearAlgebra.GetCutPointInSegment(e1, e_tocheck_3, out v)) return true;
            if (LinearAlgebra.GetCutPointInSegment(e1, e_tocheck_4, out v)) return true;

            if (LinearAlgebra.GetCutPointInSegment(e2, e_tocheck_1, out v)) return true;
            if (LinearAlgebra.GetCutPointInSegment(e2, e_tocheck_2, out v)) return true;
            if (LinearAlgebra.GetCutPointInSegment(e2, e_tocheck_3, out v)) return true;
            if (LinearAlgebra.GetCutPointInSegment(e2, e_tocheck_4, out v)) return true;

            if (LinearAlgebra.GetCutPointInSegment(e3, e_tocheck_1, out v)) return true;
            if (LinearAlgebra.GetCutPointInSegment(e3, e_tocheck_2, out v)) return true;
            if (LinearAlgebra.GetCutPointInSegment(e3, e_tocheck_3, out v)) return true;
            if (LinearAlgebra.GetCutPointInSegment(e3, e_tocheck_4, out v)) return true;

            if (LinearAlgebra.GetCutPointInSegment(e4, e_tocheck_1, out v)) return true;
            if (LinearAlgebra.GetCutPointInSegment(e4, e_tocheck_2, out v)) return true;
            if (LinearAlgebra.GetCutPointInSegment(e4, e_tocheck_3, out v)) return true;
            if (LinearAlgebra.GetCutPointInSegment(e4, e_tocheck_4, out v)) return true;

            return false;
        }

		private Polygon m_poly;

        public Rectangle(Vector2d corner1, Vector2d corner2, Vector2d corner3, Vector2d corner4)
		{
            m_corner1 = corner1;
            m_corner2 = corner2;
            m_corner3 = corner3;
            m_corner4 = corner4;
		}
        public Rectangle(Vector2d corner_topleft, float width,float height)
		{
            m_corner1 = corner_topleft;
            m_corner2 = new Vector2d(corner_topleft.X+width,corner_topleft.Y);
            m_corner3 = new Vector2d(corner_topleft.X+width,corner_topleft.Y-height);
            m_corner4 = new Vector2d(corner_topleft.X,corner_topleft.Y-height);
		}

		public Polygon GetPolygon(){
			if(m_poly != null)
				return m_poly;
			m_poly = new Polygon();
			m_poly.Add(m_corner1);
			m_poly.Add(m_corner2);
			m_poly.Add(m_corner4);
			m_poly.Add(m_corner3);
			return m_poly;
		}

		public bool Contains(Vector2d point){
			Polygon p = GetPolygon();
			return p.PointInPolygon(point.X,point.Y);
		}
	}
}
