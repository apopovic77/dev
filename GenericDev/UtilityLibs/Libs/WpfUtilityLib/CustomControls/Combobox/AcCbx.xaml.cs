﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Logicx.WpfUtility.CustomControls.Listbox;
using Logicx.WpfUtility.WpfHelpers;
using System.Windows.Threading;

namespace Logicx.WpfUtility.CustomControls.Combobox
{
    public enum AutoCompleteFilterType
    {
        StartsWith = 1,
        IndexOf = 2,
        SplitIndexOf = 3
    }

    public struct AcItem
    {
        public string Key
        {
            get
            {
                if (_cached_key != null)
                    return _cached_key;
                if (Value == null)
                    return null;
                if (Value is string)
                    return (string)Value;
                return Value.ToString();
            }
            set
            {
                _cached_key = value;
            }
        }
        public object Value { get; set; }

        #region Equals Comparison
        public static bool operator ==(AcItem v1, AcItem v2)
        {
            // If both are null, or both are same instance, return true.
            if (System.Object.ReferenceEquals(v1, v2))
                return true;

            // If one is null, but not both, return false.
            if (((object)v1 == null) || ((object)v2 == null))
            {
                return false;
            }

            return (v1.Value == v2.Value);
        }

        public static bool operator !=(AcItem v1, AcItem v2)
        {
            return !(v1 == v2);
        }

        public override bool Equals(Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            AcItem v = (AcItem)obj;
            if ((Object)v == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (Value == v.Value);
        }
        #endregion

        private string _cached_key;
    }

    /// <summary>
    /// Interaction logic for BackgroundLoadingComboBox.xaml
    /// </summary>
    public partial class AcCbx : UserControl, IDisposable
    {
        public AcCbx()
        {
            InitializeComponent();
            lb.SelectedValue = null;
        }



        ~AcCbx()
        {
            Dispose(true);
        }


        #region Util Helper Methods
        public static AcCbx AutocompleteBoxWithKeyboardFocus()
        {
            IInputElement elem = Keyboard.FocusedElement;

            if (elem is AcCbx)
                return elem as AcCbx;

            if(elem is DependencyObject)
            {
                DependencyObject parent = UIHelper.TryFindParent<AcCbx>(elem as DependencyObject);

                if (parent is AcCbx)
                    return parent as AcCbx;
            }

            return null;
        }

        /// <summary>
        /// Ensuresa correct proeprty info path structure for a given data item
        /// </summary>
        /// <param name="data_item"></param>
        private void EnsureDisplayMemberPath(object data_item)
        {
            if (data_item != null && _prop_info == null)
            {
                if (_display_paths == null)
                {
                    if(lb.DisplayMemberPath != null)
                        _prop_info = new PropertyInfo[] { data_item.GetType().GetProperty(lb.DisplayMemberPath) };
                }
                else
                {
                    _prop_info = new PropertyInfo[_display_paths.Length];
                    object curr_item = data_item;
                    for (int i = 0; i < _display_paths.Length; i++)
                    {
                        string display_path = _display_paths[i];
                        _prop_info[i] = curr_item.GetType().GetProperty(display_path);
                        if (i < _display_paths.Length - 1)
                            curr_item = _prop_info[i].GetValue(curr_item, null);
                    }
                }
            }    
        }

        /// <summary>
        /// Ensuresa correct proeprty info path structure for a given data item
        /// </summary>
        /// <param name="data_item"></param>
        private void EnsureValueMemberPath(object data_item)
        {
            if (data_item != null && _value_prop_info == null && !(data_item is string))
            {
                if (_value_paths == null)
                {
                    if(lb.SelectedValuePath != null)
                        _value_prop_info = new PropertyInfo[] { data_item.GetType().GetProperty(lb.SelectedValuePath) };
                }
                else
                {
                    _value_prop_info = new PropertyInfo[_value_paths.Length];
                    object curr_item = data_item;
                    for (int i = 0; i < _value_paths.Length; i++)
                    {
                        string value_path = _value_paths[i];
                        _value_prop_info[i] = curr_item.GetType().GetProperty(value_path);
                        if (i < _value_paths.Length - 1)
                            curr_item = _value_prop_info[i].GetValue(curr_item, null);
                    }
                }
            }
        }

        /// <summary>
        /// returns the item value
        /// normal ist ja das object selber der value
        /// wenn aber eine eine display member path angegeben wurde
        /// müssen die properties zum ziel aufgelöst werden
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        private object GetItemValue(object item)
        {
            EnsureDisplayMemberPath(item);
            EnsureValueMemberPath(item);

            if (_with_value_member_path && _value_prop_info != null)
            {
                if (_value_paths == null)
                    return _value_prop_info[0].GetValue(item, null);
                else
                {
                    object curr_item = item;
                    for (int i = 0; i < _value_paths.Length; i++)
                    {
                        string value_path = _value_paths[i];
                        if (i < _value_paths.Length - 1)
                            curr_item = _value_prop_info[i].GetValue(curr_item, null);
                    }
                    return _value_prop_info[_value_prop_info.Length - 1].GetValue(curr_item, null);
                }
            }

            if (!_with_display_member_path)
                return item;

            if (_display_paths == null)
                return _prop_info[0].GetValue(item, null);
            else
            {
                object curr_item = item;
                for (int i = 0; i < _display_paths.Length; i++)
                {
                    string display_path = _display_paths[i];
                    if (i < _display_paths.Length - 1)
                        curr_item = _prop_info[i].GetValue(curr_item, null);
                }
                return _prop_info[_prop_info.Length - 1].GetValue(curr_item, null);
            }
        }

        /// <summary>
        /// returns the item text
        /// normal ist ja das object selber der value
        /// wenn aber eine eine display member path angegeben wurde
        /// müssen die properties zum ziel aufgelöst werden
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        private object GetItemText(object item)
        {
            EnsureDisplayMemberPath(item);

            if (!_with_display_member_path)
                return item;

            if (_display_paths == null)
                return _prop_info[0].GetValue(item, null);
            else
            {
                object curr_item = item;
                for (int i = 0; i < _display_paths.Length; i++)
                {
                    string display_path = _display_paths[i];
                    if (i < _display_paths.Length - 1)
                        curr_item = _prop_info[i].GetValue(curr_item, null);
                }
                return _prop_info[_prop_info.Length - 1].GetValue(curr_item, null);
            }
        }

        private object FindItemByValue(object value)
        {
            foreach (object item in _ac_items)
            {
                if (item == null)
                    continue;

                object check_value = GetItemValue(item);

                if (object.Equals(check_value, value))
                    return item;
            }

            return null;
        }

        private void ScrollToBeginPos()
        {
            if (ScrollViewer != null)
                ScrollViewer.ScrollToTop();
        }


        private void SetFocusToDropDown()
        {
            lb.Focus();
            Keyboard.Focus(lb);

            if (SelectedAcItem == null)
            {
                if (VirtualizingStackPanel != null && VirtualizingStackPanel.Children.Count > 0)
                {
                    if (VirtualizingStackPanel.Children[0] != null)
                    {
                        VirtualizingStackPanel.Children[0].Focus();
                        if (VirtualizingStackPanel.Children.Count > 0 && VirtualizingStackPanel.Children[0] != null)
                        {
                            Keyboard.Focus(VirtualizingStackPanel.Children[0]);
                        }
                    }
                }
                return;
            }

            AcItem? sel_item = SelectedAcItem;
            foreach (ListBoxItem lb_item in VirtualizingStackPanel.Children)
            {
                if (lb_item.Content == sel_item.Value.Value)
                {
                    lb_item.Focus();
                    Keyboard.Focus(lb_item);
                    return;
                }
            }

            if (SelectedAcItem == null)
            {
                VirtualizingStackPanel.Children[0].Focus();
                Keyboard.Focus(VirtualizingStackPanel.Children[0]);
                return;
            }
        }
        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            Dispose(true);
        }

        private void Dispose(bool disposing)
        {
            if (_disposing)
                return;

            if(!_disposed)
            {
                _disposing = disposing;

                //Dispatcher.Invoke(DispatcherPriority.Normal, (NoParas)delegate
                //{
                //    BindingOperations.ClearAllBindings(tb);
                //});  

                UnregisterEvents(TextChanged);
                UnregisterEvents(SelectionChanged);
                
                lb.SelectionChanged -= lb_SelectionChanged;
                tb.TextChanged -= tb_TextChanged;
                tb.LostFocus -= tb_LostFocus;
                lb.MouseUp -= lb_MouseUp;


                if (Dispatcher.CheckAccess())
                {
                    BindingOperations.ClearAllBindings(tb);
                    BindingOperations.ClearAllBindings(lb);
                    BindingOperations.ClearAllBindings(this);
                }





                if (Dispatcher.CheckAccess())
                {
                    if (MainWindow != null)
                        MainWindow.PreviewMouseDown -= MainWindow_PreviewMouseDown;
                }

                EventHelper.RemoveAllTextContainerChangedEventHanders(tb); 
                EventHelper.RemoveAllTextContainerChangeEventHanders(tb);
                _disposed = true;
            }
        }

        private void UnregisterEvents(SelectionChangedEventHandler event_handler)
        {
            if (event_handler == null)
                return;

            foreach (SelectionChangedEventHandler eventDelegate in event_handler.GetInvocationList())
                event_handler -= eventDelegate;

        }

        private void UnregisterEvents(TextChangedEventHandler event_handler)
        {
            if (event_handler == null)
                return;

            foreach (TextChangedEventHandler eventDelegate in event_handler.GetInvocationList())
                event_handler -= eventDelegate;

        }
        #endregion
        /// <summary>
        /// reset the ac box to inital layout
        /// which means popup closed all values in listbox
        /// </summary>
        private void Reset()
        {
            lb.SelectionChanged -= lb_SelectionChanged;
            tb.TextChanged -= tb_TextChanged;

            ScrollToBeginPos();
            lb.SelectedItem = null;
            _last_txt_def_manually = null;
            _ac_items_filtered = _ac_items;
            _ac_filter_string = "";
            //lb.ItemsSource = GetFilteredDisplayItems();
            ItemsSourceFiltered = GetFilteredDisplayItems();
            tb.Text = "";
            SetPopup();

            lb.SelectionChanged += lb_SelectionChanged;
            tb.TextChanged += tb_TextChanged;
        }

        /// <summary>
        /// nimmt den aktuellen wert in der textbox
        /// und filtert alle werte die passen
        /// die vergleichsmethode kann über das property 
        /// FilterType gesetzt werden
        /// </summary>
        private void DoFiltering(string filter_string)
        {
            if (_ac_items == null || _ac_items.Count() == 0)
                return;

            if (string.IsNullOrEmpty(filter_string))
                return;

            UpdateFilteredItems(filter_string);
            //lb.ItemsSource = GetFilteredDisplayItems();
            ItemsSourceFiltered = GetFilteredDisplayItems();
        }
        private void UpdateFilteredItems(string filter_string)
        {
            if (filter_string == null)
                return;

            filter_string = filter_string.ToLower();

            if (filter_string == _ac_filter_string)
                return;
            _ac_filter_string = filter_string;

            if (FilterType == AutoCompleteFilterType.StartsWith)
            {
                _ac_items_filtered = _ac_items.Where(item => item.Key.StartsWith(filter_string)).ToList();

            }
            else if (FilterType == AutoCompleteFilterType.IndexOf)
            {
                _ac_items_filtered = _ac_items.Where(item => item.Key.IndexOf(filter_string) >= 0).ToList();
            }
            else if (FilterType == AutoCompleteFilterType.SplitIndexOf)
            {
                _ac_items_filtered = _ac_items;
                string[] split_filter = filter_string.Split(" ".ToCharArray());
                foreach (string filter_i in split_filter)
                {
                    _ac_items_filtered = _ac_items_filtered.Where(item => item.Key.IndexOf(filter_i) >= 0).ToList();
                }
            }
            else
            {
                throw new Exception("FilterType unknown");
            }
        }

        

        private IEnumerable GetFilteredDisplayItems()
        {
            if (_ac_items_filtered == null)
                return null;
            return _ac_items_filtered.Select(a => a.Value);
        }


        /// <summary>
        /// diese methode kontrolliert das gesicht des popoup fenster
        /// diese methode sollte alle möglichkeiten abfangen und demensprechend
        /// die popup visibility richtig setzen
        /// </summary>
        private void SetPopup()
        {
            if (lb.ItemsSource == null || _ignore_set_popup)
            {
                button.IsChecked = false;
                button.Visibility = Visibility.Collapsed;
                return;
            }

            if (!(_ac_items_filtered != null && _ac_items_filtered.Count > 0))
            {
                button.IsChecked = false;
                button.Visibility = Visibility.Collapsed;
                return;
            }

            if (!(Keyboard.FocusedElement == tb || Keyboard.FocusedElement == button))
                return;

            if (_ac_items_filtered != null && _ac_items_filtered.Count == 1 && lb.SelectedIndex >= 0)
            {
                button.IsChecked = false;
                button.Visibility = Visibility.Collapsed;
                return;
            }

            button.Visibility = Visibility.Visible;
            button.IsChecked = true;
        }

        private void CommitSuggestedValue()
        {
            if (_suggested_item.Value != null)
            {
                tb.Tag = _suggested_item;
                //tb.Text = GetItemValue(_suggested_item.Value).ToString();
                tb.Text = GetItemText(_suggested_item.Value).ToString();
                _suggested_item = new AcItem();
            }
            button.IsChecked = false;
            tb.Select((_last_txt_def_manually != null) ? _last_txt_def_manually.Length : 0, tb.Text.Length);
            tb.Focus();
            Keyboard.Focus(tb);
        }

        private void RollbackSuggestedValue()
        {
            lb.SelectedItem = null;
            tb.Text = _last_txt_def_manually;
            _suggested_item = new AcItem();
            if (!string.IsNullOrEmpty(tb.Text))
                tb.SelectionStart = tb.Text.Length;
            button.IsChecked = false;
            tb.Focus();
        }

        private void SetFocusState()
        {
            IsFocused = InnerElementHasFocus;
            if (IsFocused)
                border_focus.Visibility = Visibility.Visible;
            else
                border_focus.Visibility = Visibility.Collapsed;
        }

        #region Eventhandler
        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            if (e.Property.Name == "ItemsSource")
            {
                OnItemsSourceChanged(e);
            }
            else if (e.Property.Name == "WithDropDownButton")
            {
                if (WithDropDownButton)
                {
                    button.Opacity = 1;
                    button.IsHitTestVisible = true;
                    //button.Focusable = true;
                }
                else
                {
                    button.Opacity = 0;
                    button.IsHitTestVisible = false;
                    //button.Focusable = false;
                }
            }
            else if (e.Property.Name == "Padding")
            {
                Thickness padding = (Thickness)e.NewValue;
                tb.Margin = padding;
                return;
            }
            else if (e.Property.Name == "DropDownButtonStyle")
            {
                if (e.NewValue == null)
                    button.Style = null;
                else
                    button.Style = (Style)e.NewValue;
            }
            else if (e.Property.Name == "BorderStyle")
            {
                if (e.NewValue == null)
                    border.Style = (Style)FindResource("accb_background_border_style");
                else
                    border.Style = (Style)e.NewValue;
            }
            else if (e.Property.Name == "BorderFocusStyle")
            {
                if (e.NewValue == null)
                    border_focus.Style = (Style)FindResource("accb_border_focus_style");
                else
                    border_focus.Style = (Style)e.NewValue;
            }
            else if (e.Property.Name == "TextBoxStyle")
            {
                if (e.NewValue == null)
                    tb.Style = null;
                else
                    tb.Style = (Style)e.NewValue;
            }
            else if (e.Property.Name == "ListBoxStyle")
            {
                if (e.NewValue == null)
                    lb.Style = null;
                else
                    lb.Style = (Style)e.NewValue;
            }
            else if (e.Property.Name == "ItemContainerStyle")
            {
                if (e.NewValue == null)
                    lb.ItemContainerStyle = null;
                else
                    lb.ItemContainerStyle = (Style)e.NewValue;
            }
            else if (e.Property.Name == "ItemTemplate")
            {
                if (e.NewValue == null)
                    lb.ItemTemplate = null;
                else
                    lb.ItemTemplate = (DataTemplate)e.NewValue;
            }
            else if (e.Property.Name == "DisplayMemberPath")
            {
                lb.DisplayMemberPath = DisplayMemberPath;

                _with_display_member_path = !string.IsNullOrEmpty(lb.DisplayMemberPath);
                if (_with_display_member_path)
                {
                    _display_paths = lb.DisplayMemberPath.Split(".".ToCharArray());
                    if (_display_paths.Length <= 1)
                        _display_paths = null;
                }
            }
            else if (e.Property.Name == "SelectedValuePath")
            {
                lb.SelectedValuePath = SelectedValuePath;
            }
            else if (e.Property.Name == "IsReadOnly")
            {
                bool read_only = (bool)e.NewValue;

                if (read_only)
                {
                    this.IsHitTestVisible = false;
                    this.Focusable = false;
                    this.IsTabStop = false;
                    tb.IsHitTestVisible = false;
                    tb.Focusable = false;
                    tb.IsTabStop = false;
                    button.IsHitTestVisible = false;
                    //button.Focusable = false;
                    button.IsTabStop = false;
                    button.Visibility = Visibility.Collapsed;
                }
                else
                {
                    this.IsHitTestVisible = true;
                    this.Focusable = true;
                    this.IsTabStop = true;
                    tb.IsHitTestVisible = true;
                    tb.Focusable = true;
                    tb.IsTabStop = true;
                    button.IsHitTestVisible = true;
                    //button.Focusable = true;
                    button.IsTabStop = true;
                    button.Visibility = Visibility.Visible;
                    OnPropertyChanged(new DependencyPropertyChangedEventArgs(AcCbx.WithDropDownButtonProperty, null, null));
                }
            }
            else if (e.Property.Name == "IsMultilineTextedit")
            {
                if ((bool)e.NewValue == true)
                {
                    tb.AcceptsReturn = true;
                    tb.AllowDrop = true;
                }
                else
                {
                    tb.AcceptsReturn = false;
                    tb.AllowDrop = false;
                }
            }
            //else if (e.Property.Name == "IsEnabled")
            //{
            //    if (!_help_flag)
            //    {
            //        IsReadOnly = !(bool)e.NewValue;
            //        _help_flag = true;
            //        IsEnabled = true;
            //        _help_flag = false;
            //    }
            //}
            else if (e.Property.Name == "SelectedIndex")
            {
                if (!_help_flag)
                    lb.SelectedIndex = (int)e.NewValue;
            }
            else if (e.Property.Name == "SelectedValue")
            {
                if (!_help_flag)
                {
                    
                    base.OnPropertyChanged(e);

                    _ignore_select = true;
                    lb.SelectedValue = e.NewValue;
                    
                    // Klaus 09.07.2010
                    // Wenn SelectedValue-Bindings manuell gelöscht und neu gesetzt werden (im Code)
                    // dann ändert sich zwar die selected value aber text nicht
                    // die folgenden zeilen snychronisieren den text mit der selected value
                    try
                    {
                        if (e.NewValue != null)
                        {
                            string text_tmp = string.Empty;
                            if (!string.IsNullOrEmpty(SelectedValuePath))
                            {
                                if (lb.SelectedItem == null)
                                {
                                    object item_value = GetItemText(e.NewValue);

                                    if (item_value != null)
                                        text_tmp = item_value.ToString();
                                }
                                else
                                {
                                    object item_value = GetItemText(lb.SelectedItem);

                                    if (item_value != null)
                                        text_tmp = item_value.ToString();
                                }
                            }
                            else
                            {
                                object item_value = GetItemText(e.NewValue);

                                if (item_value != null)
                                    text_tmp = item_value.ToString();
                            }

                            if (Text != text_tmp)
                            {
                                _help_flag = true;
                                tb.Text = text_tmp;
                                Text = text_tmp;
                                _help_flag = false;
                            }
                        }
                    }
                    catch (Exception)
                    {
                    }

                    _ignore_select = false;
                }
            }
            else if (e.Property.Name == "Text")
            {
                if(!_help_flag)
                    tb.Text = (string)e.NewValue;
            }
            else if (e.Property.Name == "PopupWidth")
            {
                if(((double)e.NewValue) < 0)
                {
                    popup.ClearValue(WidthProperty);
                    // auto width base_grid binding
                    Binding widthBinding = new Binding("ActualWidth");
                    widthBinding.ElementName = "base_grid";
                    widthBinding.Mode = BindingMode.OneWay;
                    popup.SetBinding(WidthProperty, widthBinding);
                }
                else if(((double)e.NewValue) == 0)
                {
                    try
                    {
                        BindingOperations.ClearBinding(popup, WidthProperty);
                    }catch
                    {
                        
                    }
                    popup.ClearValue(WidthProperty);
                }
                else
                {
                    try
                    {
                        BindingOperations.ClearBinding(popup, WidthProperty);
                    }
                    catch
                    {

                    }
                    popup.ClearValue(WidthProperty);
                    popup.Width = (double) e.NewValue;
                }
            }
            else
                base.OnPropertyChanged(e);
        }

        private void tb_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(tb.Text))
                {
                    lb.SelectionChanged -= lb_SelectionChanged;
                    ScrollToBeginPos();
                    _suggested_item = new AcItem();

                    bool bFireSelectionChanged = false;

                    if (SelectedItem != null  && lb.SelectedItem != null)
                        bFireSelectionChanged = true;

                    if (SelectedItem != null)
                        SelectedItem = null;


                    lb.SelectedItem = null;

                    _help_flag = true;
                    Text = tb.Text;
                    _help_flag = false;

                    if (bFireSelectionChanged)
                    {
                        if (SelectionChanged != null)
                            SelectionChanged(this, new SelectionChangedEventArgs(SlimListBoxEx.SelectionChangedEvent, new List<object>(), new List<object>()));
                    }
                    else
                    {
                        if (TextChanged != null)
                            TextChanged(this, e);
                    }

                    _ac_items_filtered = _ac_items;
                    //lb.ItemsSource = GetFilteredDisplayItems();
                    ItemsSourceFiltered = GetFilteredDisplayItems();
                    _last_txt_def_manually = null;
                    _ac_filter_string = "";
                    SetPopup();
                    lb.SelectionChanged += lb_SelectionChanged;

                    return;
                }

                if (tb.Tag != null && ((AcItem)tb.Tag).Key == tb.Text.ToLower())
                {
                    return;
                }

                lb.SelectionChanged -= lb_SelectionChanged;
                DoFiltering(tb.Text);
                lb.SelectionChanged += lb_SelectionChanged;
                SetPopup();

                if (!WithCustomValue && (_ac_items_filtered==null || _ac_items_filtered.Count == 0))
                {
                    char last_char = tb.Text[tb.Text.Length - 1];
                    if (last_char == ' ')
                    {
                        //remove last char
                        tb.Text = tb.Text.Substring(0, tb.Text.Length - 1);
                        return;
                    }
                }
                

                ScrollToBeginPos();

                string last_last_txt_def_manually = _last_txt_def_manually;
                _last_txt_def_manually = tb.Text.ToLower();
                tb.Tag = null;

                //do auto completion
                if (WithAutoCompletion || (_ac_items_filtered != null && _ac_items_filtered.Count == 1))
                {
                    if (_ac_items_filtered != null && _ac_items_filtered.Count > 0)
                    {
                        bool has_selected = false;

                        int last_last_txt_def_len = (last_last_txt_def_manually != null)
                                                        ? last_last_txt_def_manually.Length
                                                        : 0;
                        int last_txt_def_len = (_last_txt_def_manually != null) ? _last_txt_def_manually.Length : 0;
                        //bool txt_deletion = last_last_txt_def_len >= last_txt_def_len;
                        
                        // KlAUS: Die Keyboard.IsKeyDown() Abfragen ergaben TRUE wenn tasten schnell hintereinander gedrückt wurden
                        // z.b.  schnell BACKSPACE + '2'

                        if (!(_last_key == Key.Delete || _last_key == Key.Back)) // || Keyboard.IsKeyDown(Key.Back) || Keyboard.IsKeyDown(Key.Delete)))
                        {
                                if (WithAutoCompletion || (!WithAutoCompletion && tb.Text.ToLower() == GetItemText(_ac_items_filtered[0].Value).ToString().ToLower()))
                            {
                                has_selected = true;
                                if (!(!WithAutoCompletion && tb.Text.ToLower() == GetItemText(_ac_items_filtered[0].Value).ToString().ToLower()))
                                    lb.SelectionChanged -= lb_SelectionChanged;
                                tb.TextChanged -= tb_TextChanged;
                                //tb.Text = GetItemValue(_ac_items_filtered[0].Value).ToString();
                                tb.Text = GetItemText(_ac_items_filtered[0].Value).ToString();
                                lb.SelectedIndex = 0;
                                tb.TextChanged += tb_TextChanged;
                                if (!(!WithAutoCompletion && tb.Text.ToLower() == GetItemText(_ac_items_filtered[0].Value).ToString().ToLower()))
                                    lb.SelectionChanged += lb_SelectionChanged;
                                tb.Select(last_txt_def_len, tb.Text.Length);                                
                            }
                        }

                        if (!WithAutoCompletion && has_selected)
                            SetPopup();
                    }
                }

            }
            finally
            {
                _help_flag = true;
                Text = tb.Text;
                _help_flag = false;

                if (TextChanged != null)
                    TextChanged(this, e);
            }
        }

        private void button_Checked(object sender, RoutedEventArgs e)
        {
            e.Handled = true;
            lb.MouseUp += lb_MouseUp;
            if (MainWindow != null)
                MainWindow.PreviewMouseDown += MainWindow_PreviewMouseDown;
        }

        private void button_Unchecked(object sender, RoutedEventArgs e)
        {
            e.Handled = true;
            lb.MouseUp -= lb_MouseUp;
            if (MainWindow != null)
                MainWindow.PreviewMouseDown -= MainWindow_PreviewMouseDown;
        }

        void lb_MouseUp(object sender, MouseButtonEventArgs e)
        {
            button.IsChecked = false;
            CommitSuggestedValue();
            e.Handled = true;
        }

        void MainWindow_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (popup.IsMouseOver || button.IsMouseOver)
                return;
            button.IsChecked = false;
            CommitSuggestedValue();
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            if (!button.IsChecked.Value)
            {
                tb.Focus();
                Keyboard.Focus(tb);
                return;
            }
            else
            {
                lb.Focus();
                Keyboard.Focus(lb);
            }

            if (tb.Tag == null)
                return;

            if (SelectedAcItem != null)
            {
                AcItem sel_item = SelectedAcItem.Value;
                _suggested_item = sel_item;
            }

            SetFocusToDropDown();
        }

        private void lb_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (_disposing)
                return;

            try
            {

                _help_flag = true;
                SelectedIndex = lb.SelectedIndex;
                _help_flag = false;

                if (SelectedAcItem == null)
                {
                    try
                    {
                        tb.Text = "";
                        SelectedValue = null;
                    }
                    catch(Exception ex)
                    {
                        Debug.WriteLine(ex);
                    }
                    return;
                }

                AcItem item = SelectedAcItem.Value;
                _suggested_item = item;
                tb.Tag = item;

                bool selected_text = string.IsNullOrEmpty(tb.Text);
                //tb.Text = GetItemValue(item.Value).ToString();
                tb.Text = GetItemText(item.Value).ToString();

                if (selected_text)
                    tb.SelectAll();

                if (!_ignore_select)
                {
                    _help_flag = true;
                    SelectedValue = lb.SelectedValue;
                    _help_flag = false;
                }
                
            }
            finally
            {

                if (SelectionChanged != null)
                    SelectionChanged(this, e);
            }
        }

        private void OnItemsSourceChanged(DependencyPropertyChangedEventArgs e)
        {
            _help_flag = true;
            SelectedIndex = -1;
            _help_flag = false;

            _with_display_member_path = false;
            _prop_info = null;
            _display_paths = null;

            object _old_sel_value = SelectedValue;

            if (e.NewValue != null)
                _ignore_set_popup = true; // nicht autom alles anzeigen bei itemssource change
            SelectedValue = null;
            
            if (ItemsSource == null)
            {
                _ac_items = null;
                _ac_items_filtered = null;
                Reset();
                _ignore_set_popup = false;
                return;
            }

            if (_ac_items != null)
                Reset();

            _ignore_set_popup = false;

            bool found_old_sel_value = false;

            //detect type of object added
            //generare Ac Items List
            _ac_items = new List<AcItem>();
            if (ItemsSource is IEnumerable)
            {
                _with_display_member_path = !string.IsNullOrEmpty(lb.DisplayMemberPath);
                if (_with_display_member_path)
                {
                    _display_paths = lb.DisplayMemberPath.Split(".".ToCharArray());
                    if (_display_paths.Length <= 1)
                        _display_paths = null;
                }

                if (lb.SelectedValuePath != null)
                {
                    _with_value_member_path = !string.IsNullOrEmpty(lb.SelectedValuePath);
                    if (_with_value_member_path)
                    {
                        _value_paths = lb.SelectedValuePath.Split(".".ToCharArray());
                        if (_value_paths.Length <= 1)
                            _value_paths = null;
                    }
                }
                else
                {
                    _with_value_member_path = false;
                }


                foreach (object item in (IEnumerable)ItemsSource)
                {
                    if (item == null)
                        continue;

                    if (item == _old_sel_value)
                        found_old_sel_value=true;
                    else if(_with_value_member_path && GetItemValue(item) == _old_sel_value)
                        found_old_sel_value = true;
                    else if (_old_sel_value is string)
                    {
                        if (GetItemValue(item) as string == _old_sel_value as string)
                            found_old_sel_value = true;
                    }

                    AcItem ac_item = new AcItem();
                    ac_item.Value = item;

                    if (!_with_display_member_path)
                    {
                        ac_item.Key = item.ToString().ToLower();
                    }
                    else
                    {
                        //object value = GetItemValue(item);
                        object value = GetItemText(item);
                        ac_item.Key = value.ToString().ToLower();
                    }

                    _ac_items.Add(ac_item);
                }

                _ac_items_filtered = _ac_items;
                _ac_filter_string = "";
                //lb.ItemsSource = GetFilteredDisplayItems();

                ItemsSourceFiltered = GetFilteredDisplayItems();
                if (_ac_items_filtered!=null && _ac_items_filtered.Count > 0)
                    button.Visibility = Visibility.Visible;
            }

            DateTime time8 = DateTime.Now;

            if (found_old_sel_value)
                SelectedValue = _old_sel_value;
        }


        private void lb_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter || e.Key == Key.Return || e.Key == Key.Tab)
            {
                CommitSuggestedValue();
            }
            else if (e.Key == Key.Escape || e.Key == Key.Back)
            {
                RollbackSuggestedValue();
            }
            e.Handled = true;
        }

        private void lb_KeyUp(object sender, KeyEventArgs e)
        {
            //if (e.Key == Key.Enter || e.Key == Key.Return || e.Key == Key.Tab)
            //{
            //    CommitSuggestedValue();
            //}
            //else if (e.Key == Key.Escape || e.Key == Key.Back)
            //{
            //    RollbackSuggestedValue();
            //}
            //e.Handled = true;
        }

        private void tb_KeyDown(object sender, KeyEventArgs e)
        {
            _last_key = e.Key;
        }

        private void tb_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Down || e.Key == Key.Up)
            {
                tb.LostFocus -= tb_LostFocus;
                if (lb.ItemsSource == null || (_ac_items_filtered != null && _ac_items_filtered.Count == 0))
                    return;

                if (!button.IsChecked.Value)
                    button.IsChecked = true;

                SetFocusToDropDown();
                tb.LostFocus += tb_LostFocus;
            }
            else if (e.Key == Key.Enter || e.Key == Key.Return)
            {
                if (button.IsChecked.Value)
                {
                    e.Handled = true;
                    if (lb.SelectedItem != null)
                    {
                        CommitSuggestedValue();
                    }
                    else
                    {
                        button.IsChecked = false;
                    }
                }
            }
            _last_key = e.Key;
        }

        private void tb_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            //es muss keine preview check druchgeführt werden wenn custom values erlaubt sind
            if (WithCustomValue)
                return;

            string save_filter_string = _ac_filter_string;
            List<AcItem> save_ac_items = _ac_items_filtered;

            //überprüfe ob ein andere bereich markiert ist
            bool text_selected = !string.IsNullOrEmpty(tb.SelectedText);
            string start_text = _last_txt_def_manually;
            if (text_selected)
                start_text = tb.Text.Substring(0, tb.SelectionStart);

            UpdateFilteredItems(start_text + e.Text);
            bool text_change_allowed = _ac_items_filtered != null && _ac_items_filtered.Count > 0;
            if (!text_change_allowed)
            {
                _ac_items_filtered = save_ac_items;
                _ac_filter_string = save_filter_string;
                e.Handled = true;
            }
        }

        private void tb_LostFocus(object sender, RoutedEventArgs e)
        {
            if (Keyboard.FocusedElement == lb)
                return;

            //if (Keyboard.FocusedElement is ListBoxItem)
            //    if (UIHelper.TryFindParent<ListBox>((ListBoxItem)Keyboard.FocusedElement) == lb)
            //        return;
            if (Keyboard.FocusedElement is ListBoxItem)
                if (UIHelper.TryFindParent<SlimListBoxEx>((ListBoxItem)Keyboard.FocusedElement) == lb)
                    return;

            if (Keyboard.FocusedElement is ToggleButton && Keyboard.FocusedElement == button)
                return;

            if (!WithCustomValue && !MustSelectEntry)
            {
                if (!string.IsNullOrEmpty(tb.Text))
                    if (SelectedAcItem == null || tb.Text.ToLower() != SelectedAcItem.Value.Key)
                    {
                        if (lb.Items != null && lb.Items.Count > 0)
                        {
                            // suche das passende item zum text
                            AcItem match_item = _ac_items.SingleOrDefault(i => i.Key.ToLower() == tb.Text.ToLower());
                            int item_idx = -1;

                            if (match_item != null)
                                item_idx = lb.Items.IndexOf(match_item.Value);

                            if (item_idx >= 0)
                            {
                                lb.SelectedIndex = item_idx;
                            }
                            else
                            {
                                lb.SelectedIndex = 0;
                                tb.TextChanged -= tb_TextChanged;
                                //if(lb.SelectedItem != null)
                                //tb.Text = GetItemValue(lb.SelectedItem).ToString();
                                tb.Text = GetItemText(lb.SelectedItem).ToString();

                                tb.TextChanged += tb_TextChanged;
                                tb.Select((_last_txt_def_manually == null) ? 0 : _last_txt_def_manually.Length, tb.Text.Length);
                            }
                        }
                        else
                        {
                            Reset();
                        }
                    }
            }

            if (!AllowNullSelection)
            {
                if(lb.SelectedItem == null && lb.Items != null && lb.Items.Count > 0)
                {
                    lb.SelectedIndex = 0;    
                }
                else if (SelectedAcItem != null)
                {
                    if ((MustSelectEntry || !AllowNullSelection) && (SelectedValue == null || SelectedItem == null))
                    {
                        if(!_disposing || _disposed)
                            lb_SelectionChanged(lb, null);
                    }
                }
                
            }

            if(MustSelectEntry)
                if (!string.IsNullOrEmpty(tb.Text))
                    if(SelectedAcItem == null || tb.Text.ToLower() != SelectedAcItem.Value.Key)
                        Reset();

            if (button.IsChecked.Value)
            {
                button.IsChecked = false;
            }

            SetFocusState();
        }

        public new void Focus()
        {
            tb.Focus();
        }

        public void SelectAll()
        {
            if (tb != null)
            {
                //tb.Focus();
                tb.SelectAll();
            }
        }

        #region Focus State Mgmt
        private void tb_GotFocus(object sender, RoutedEventArgs e)
        {
            SetFocusState();
        }

        private void button_LostFocus(object sender, RoutedEventArgs e)
        {
            SetFocusState();
        }

        private void button_GotFocus(object sender, RoutedEventArgs e)
        {
            SetFocusState();
        }

        private void lb_LostFocus(object sender, RoutedEventArgs e)
        {
            SetFocusState();
        }

        private void lb_GotFocus(object sender, RoutedEventArgs e)
        {
            SetFocusState();
        }
        #endregion
        #endregion

        #region Dependency Properties
        public object ItemsSource
        {
            get { return (object)GetValue(ItemsSourceProperty); }
            set { SetValue(ItemsSourceProperty, value); }
        }
        public static readonly DependencyProperty ItemsSourceProperty = DependencyProperty.Register("ItemsSource", typeof(object), typeof(AcCbx), new UIPropertyMetadata(null));

        public object ItemsSourceFiltered
        {
            get { return (object)GetValue(ItemsSourceFilteredProperty); }
            private set { SetValue(ItemsSourceFilteredKeyProperty, value); }
        }
        public static readonly DependencyPropertyKey ItemsSourceFilteredKeyProperty = DependencyProperty.RegisterReadOnly("ItemsSourceFiltered", typeof(object), typeof(AcCbx), new UIPropertyMetadata(null));
        private static readonly DependencyProperty ItemsSourceFilteredProperty = ItemsSourceFilteredKeyProperty.DependencyProperty;

        public AutoCompleteFilterType FilterType
        {
            get { return (AutoCompleteFilterType)GetValue(FilterTypeProperty); }
            set { SetValue(FilterTypeProperty, value); }
        }
        public static readonly DependencyProperty FilterTypeProperty = DependencyProperty.Register("FilterType", typeof(AutoCompleteFilterType), typeof(AcCbx), new UIPropertyMetadata(AutoCompleteFilterType.IndexOf));


        /// <summary>
        /// Wenn true dann wird der Text des ersten Elements in der Vorschlagsliste übernommen
        /// </summary>
        public bool WithAutoCompletion
        {
            get { return (bool)GetValue(WithAutoCompletionProperty); }
            set { SetValue(WithAutoCompletionProperty, value); }
        }
        public static readonly DependencyProperty WithAutoCompletionProperty = DependencyProperty.Register("WithAutoCompletion", typeof(bool), typeof(AcCbx), new UIPropertyMetadata(false));


        /// <summary>
        /// wenn false darf kein neuer wert definiert werden
        /// es muss ein wert eingeben werden der auch in der list vorhanden ist
        /// </summary>
        public bool WithCustomValue
        {
            get { return (bool)GetValue(WithCustomValueProperty); }
            set { SetValue(WithCustomValueProperty, value); }
        }
        public static readonly DependencyProperty WithCustomValueProperty = DependencyProperty.Register("WithCustomValue", typeof(bool), typeof(AcCbx), new UIPropertyMetadata(true));


        /// <summary>
        /// wenn auf false wird kein drop down button angezeigt
        /// es wird nur dann das drop down angezeigt wenn durch die texteingabe
        /// filter values zurückegeben werden
        /// </summary>
        public bool WithDropDownButton
        {
            get { return (bool)GetValue(WithDropDownButtonProperty); }
            set { SetValue(WithDropDownButtonProperty, value); }
        }
        public static readonly DependencyProperty WithDropDownButtonProperty = DependencyProperty.Register("WithDropDownButton", typeof(bool), typeof(AcCbx), new UIPropertyMetadata(true));

        /// <summary>
        /// es wird der text in der textbox zurückgeben
        /// </summary>
        public object SelectedValue
        {
            get { return (object)GetValue(SelectedValueProperty); }
            set { SetValue(SelectedValueProperty, value); }
        }
        public static readonly DependencyProperty SelectedValueProperty = DependencyProperty.Register("SelectedValue", typeof(object), typeof(AcCbx), new UIPropertyMetadata(null));

        /// <summary>
        /// es wird der text in der textbox zurückgeben
        /// </summary>
        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }
        public static readonly DependencyProperty TextProperty = DependencyProperty.Register("Text", typeof(string), typeof(AcCbx), new UIPropertyMetadata(null));


        /// <summary>
        /// the string value of the item beiing displayed is cached for better indexof comparission perforamnce
        /// </summary>
        public bool WithCaching
        {
            get { return (bool)GetValue(WithCachingProperty); }
            set { SetValue(WithCachingProperty, value); }
        }
        public static readonly DependencyProperty WithCachingProperty = DependencyProperty.Register("WithCaching", typeof(bool), typeof(AcCbx), new UIPropertyMetadata(true));

        public new Thickness Padding
        {
            get { return (Thickness)GetValue(PaddingProperty); }
            set { SetValue(PaddingProperty, value); }
        }
        public new static readonly DependencyProperty PaddingProperty = DependencyProperty.Register("Padding", typeof(Thickness), typeof(AcCbx), new UIPropertyMetadata(new Thickness(0)));

        /// <summary>
        /// der style für den togglebutton
        /// </summary>
        public Style DropDownButtonStyle
        {
            get { return (Style)GetValue(DropDownButtonStyleProperty); }
            set { SetValue(DropDownButtonStyleProperty, value); }
        }
        public static readonly DependencyProperty DropDownButtonStyleProperty = DependencyProperty.Register("DropDownButtonStyle", typeof(Style), typeof(AcCbx), new UIPropertyMetadata(null));

        /// <summary>
        /// der style für die listbox im dropdown field
        /// </summary>
        public Style ListBoxStyle
        {
            get { return (Style)GetValue(ListBoxStyleProperty); }
            set { SetValue(ListBoxStyleProperty, value); }
        }
        public static readonly DependencyProperty ListBoxStyleProperty = DependencyProperty.Register("ListBoxStyle", typeof(Style), typeof(AcCbx), new UIPropertyMetadata(null));

        /// <summary>
        /// Wenn eine Style für die Texteingabe box gesetzt werden soll
        /// dann muss das über dieses Border Style gemacht werden
        /// das TextBox element selbst hat als Style alles auf transparent eingestellt
        /// </summary>
        public Style BorderStyle
        {
            get { return (Style)GetValue(BorderStyleProperty); }
            set { SetValue(BorderStyleProperty, value); }
        }
        public static readonly DependencyProperty BorderStyleProperty = DependencyProperty.Register("BorderStyle", typeof(Style), typeof(AcCbx), new UIPropertyMetadata(null));

        public Style BorderFocusStyle
        {
            get { return (Style)GetValue(BorderFocusStyleProperty); }
            set { SetValue(BorderFocusStyleProperty, value); }
        }
        public static readonly DependencyProperty BorderFocusStyleProperty = DependencyProperty.Register("BorderFocusStyle", typeof(Style), typeof(AcCbx), new UIPropertyMetadata(null));

        public new bool IsFocused
        {
            get { return (bool)GetValue(IsFocusedProperty); }
            set { SetValue(IsFocusedProperty, value); }
        }
        public static readonly DependencyProperty IsFocusedProperty = DependencyProperty.Register("IsFocused", typeof(bool), typeof(AcCbx), new UIPropertyMetadata(false));


        public bool IsReadOnly
        {
            get { return (bool)GetValue(IsReadOnlyProperty); }
            set { SetValue(IsReadOnlyProperty, value); }
        }
        public static readonly DependencyProperty IsReadOnlyProperty = DependencyProperty.Register("IsReadOnly", typeof(bool), typeof(AcCbx), new UIPropertyMetadata(false));


        public string DisplayMemberPath
        {
            get { return (string)GetValue(DisplayMemberPathProperty); }
            set { SetValue(DisplayMemberPathProperty, value); }
        }
        public static readonly DependencyProperty DisplayMemberPathProperty = DependencyProperty.Register("DisplayMemberPath", typeof(string), typeof(AcCbx), new UIPropertyMetadata(null));


        public string SelectedValuePath
        {
            get { return (string)GetValue(SelectedValuePathProperty); }
            set { SetValue(SelectedValuePathProperty, value); }
        }
        public static readonly DependencyProperty SelectedValuePathProperty = DependencyProperty.Register("SelectedValuePath", typeof(string), typeof(AcCbx), new UIPropertyMetadata(null));


        public bool IsMultilineTextedit
        {
            get { return (bool)GetValue(IsMultilineTexteditProperty); }
            set { SetValue(IsMultilineTexteditProperty, value); }
        }
        public static readonly DependencyProperty IsMultilineTexteditProperty = DependencyProperty.Register("IsMultilineTextedit", typeof(bool), typeof(AcCbx), new UIPropertyMetadata(false));


        public BitmapEffect FocusBitmapEffect
        {
            get { return (BitmapEffect)GetValue(FocusBitmapEffectProperty); }
            set { SetValue(FocusBitmapEffectProperty, value); }
        }
        public static readonly DependencyProperty FocusBitmapEffectProperty = DependencyProperty.Register("FocusBitmapEffect", typeof(BitmapEffect), typeof(AcCbx), new UIPropertyMetadata(null));

        public Effect FocusEffect
        {
            get { return (Effect)GetValue(FocusEffectProperty); }
            set { SetValue(FocusEffectProperty, value); }
        }
        public static readonly DependencyProperty FocusEffectProperty = DependencyProperty.Register("FocusEffect", typeof(Effect), typeof(AcCbx), new UIPropertyMetadata(null));

        public Style TextBoxStyle
        {
            get { return (Style)GetValue(TextBoxStyleProperty); }
            set { SetValue(TextBoxStyleProperty, value); }
        }
        public static readonly DependencyProperty TextBoxStyleProperty = DependencyProperty.Register("TextBoxStyle", typeof(Style), typeof(AcCbx), new UIPropertyMetadata(null));

        public Style ItemContainerStyle
        {
            get { return (Style)GetValue(ItemContainerStyleProperty); }
            set { SetValue(ItemContainerStyleProperty, value); }
        }
        public static readonly DependencyProperty ItemContainerStyleProperty = DependencyProperty.Register("ItemContainerStyle", typeof(Style), typeof(AcCbx), new UIPropertyMetadata(null));

        public DataTemplate ItemTemplate
        {
            get { return (DataTemplate)GetValue(ItemTemplateProperty); }
            set { SetValue(ItemTemplateProperty, value); }
        }
        public static readonly DependencyProperty ItemTemplateProperty = DependencyProperty.Register("ItemTemplate", typeof(DataTemplate), typeof(AcCbx), new UIPropertyMetadata(null));

        public int SelectedIndex
        {
            get { return (int)GetValue(SelectedIndexProperty); }
            set { SetValue(SelectedIndexProperty, value); }
        }
        public static readonly DependencyProperty SelectedIndexProperty = DependencyProperty.Register("SelectedIndex", typeof(int), typeof(AcCbx), new UIPropertyMetadata((int)-1));


        /// <summary>
        /// wenn man einen comobobx ähnlichen betrieb haben möchte
        /// was bedeutet das fix immer irgendwas ausgewählt ist
        /// dann kann mir hier auf false stellen
        /// wenn null gewählt wird und die comobox den focus verliert 
        /// dann wird automatisch der erst möglich eintrag gewählt
        /// </summary>
        public bool AllowNullSelection
        {
            get { return (bool)GetValue(AllowNullSelectionProperty); }
            set { SetValue(AllowNullSelectionProperty, value); }
        }
        public static readonly DependencyProperty AllowNullSelectionProperty = DependencyProperty.Register("AllowNullSelection", typeof(bool), typeof(AcCbx), new UIPropertyMetadata(true));


        public string WatermarkText
        {
            get { return (string)GetValue(WatermarkTextProperty); }
            set { SetValue(WatermarkTextProperty, value); }
        }
        public static readonly DependencyProperty WatermarkTextProperty = DependencyProperty.Register("WatermarkText", typeof(string), typeof(AcCbx), new UIPropertyMetadata(string.Empty));


        public bool MustSelectEntry
        {
            get { return (bool)GetValue(MustSelectEntryProperty); }
            set { SetValue(MustSelectEntryProperty, value); }
        }

        public static readonly DependencyProperty MustSelectEntryProperty = DependencyProperty.Register("MustSelectEntry", typeof(bool), typeof(AcCbx), new UIPropertyMetadata(false));


        public double PopupWidth
        {
            get { return (double)GetValue(PopupWidthProperty); }
            set { SetValue(PopupWidthProperty, value); }
        }

        public static readonly DependencyProperty PopupWidthProperty = DependencyProperty.Register("PopupWidth", typeof(double), typeof(AcCbx), new UIPropertyMetadata((double)-1));




        public int ListBoxMinWidth
        {
            get { return (int)GetValue(ListBoxMinWidthProperty); }
            set { SetValue(ListBoxMinWidthProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ListBoxMinWidth.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ListBoxMinWidthProperty =
            DependencyProperty.Register("ListBoxMinWidth", typeof(int), typeof(AcCbx), new UIPropertyMetadata(0));

        
        #endregion

        #region Properties
        public bool IsPopupOpen
        {
            get
            {
                return popup.IsOpen;
            }
        }
        public ItemCollection Items
        {
            get
            {
                return lb.Items;
            }
        }
        protected Window MainWindow
        {
            get
            {
                return UIHelper.TryFindParent<Window>(this);
            }
        }
        protected VirtualizingStackPanel VirtualizingStackPanel
        {
            get
            {
                VirtualizingStackPanel virt_sp = UIHelper.FindVisualChildByType<VirtualizingStackPanel>(lb);
                return virt_sp;
            }
        }
        protected ScrollViewer ScrollViewer
        {
            get
            {
                ScrollViewer scroll_viewer = UIHelper.FindVisualChildByType<ScrollViewer>(lb);
                return scroll_viewer;
            }
        }
        protected bool InnerElementHasFocus
        {
            get
            {
                return tb.IsFocused || button.IsFocused || lb.IsFocused;
            }
        }

        protected AcItem? SelectedAcItem
        {
            get
            {
                if (lb.SelectedItem == null || lb.SelectedIndex < 0)
                    return null;
                return _ac_items_filtered[lb.SelectedIndex];
            }
        }
        public object SelectedItem
        {
            get
            {
                return lb.SelectedItem;
            }
            set
            {
                lb.SelectedItem = value;
            }
        }

        public int CaretIndex
        {
            get { return tb.CaretIndex; }
            set { tb.CaretIndex = value; }
        }

        public string LastTxtEntered
        {
            get { return _last_txt_def_manually; }
            set { _last_txt_def_manually = value; }
        }
        #endregion



        #region Attributes
        private bool _disposed = false;
        private bool _disposing = false;
        private bool _ignore_select = false;

        private string[] _display_paths;
        private bool _with_display_member_path = false;
        private PropertyInfo[] _prop_info = null;

        private string[] _value_paths;
        private bool _with_value_member_path = false;
        private PropertyInfo[] _value_prop_info = null;

        private Key _last_key;
        private List<AcItem> _ac_items;
        private List<AcItem> _ac_items_filtered;
        private string _ac_filter_string;
        private bool _help_flag;

        private bool _ignore_set_popup = false;

        private string _last_txt_def_manually;
        private AcItem _suggested_item;
        public event SelectionChangedEventHandler SelectionChanged;
        public event TextChangedEventHandler TextChanged;

        private delegate void NoParas();
        #endregion

    }
}