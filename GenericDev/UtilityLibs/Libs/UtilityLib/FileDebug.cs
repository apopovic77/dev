﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Logicx.Utility
{
    public class FileDebug
    {
        public static void WriteLine(string file_id, string msg)
        {
            if(_files == null)
                _files = new Dictionary<string, StreamWriter>();
            if(!_files.ContainsKey(file_id))
            {
                FileStream fs = File.OpenWrite("FileDebug_" + file_id + ".txt");
                StreamWriter sw = new StreamWriter(fs);
                _files.Add(file_id, sw);
            }

            _files[file_id].WriteLine(msg);
            _files[file_id].Flush();
        }

        public static void Dispose()
        {
            if (_files == null)
                return;
            foreach (StreamWriter stream_writer in _files.Values)
            {
                stream_writer.Flush();
                stream_writer.Dispose();
            }
        }


        private static Dictionary<string, StreamWriter> _files;
    }
}
