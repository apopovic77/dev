﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Logicx.WpfUtility.CustomControls.OverlayManager
{
    /// <summary>
    /// Interaction logic for OverlayLoadingInformation.xaml
    /// </summary>
    public partial class OverlayLoadingInformation : UserControl
    {
        #region Construction and Initialization
        /// <summary>
        /// Static constructor
        /// </summary>
        static OverlayLoadingInformation()
        {
            FrameworkPropertyMetadata metadataInfoTitle = new FrameworkPropertyMetadata("Bite warten ...");
            InfoTitleProperty = DependencyProperty.Register("InfoTitle",
                                                            typeof(string), typeof(OverlayContent), metadataInfoTitle);

            FrameworkPropertyMetadata metadataInfoSubTitle = new FrameworkPropertyMetadata("Daten werden geladen");
            InfoSubTitleProperty = DependencyProperty.Register("InfoSubTitle",
                                                            typeof(string), typeof(OverlayContent), metadataInfoSubTitle);
            
        }

        public OverlayLoadingInformation()
        {
            InitializeComponent();
        }
        #endregion

        #region Properties
        /// <summary>
        /// Informations titel
        /// </summary>
        public string InfoTitle
        {
            get
            {
                return (string)GetValue(InfoTitleProperty);
            }
            set
            {
                SetValue(InfoTitleProperty, value);
            }
        }

        /// <summary>
        /// Informations sub titel
        /// </summary>
        public string InfoSubTitle
        {
            get
            {
                return (string)GetValue(InfoSubTitleProperty);
            }
            set
            {
                SetValue(InfoSubTitleProperty, value);
            }
        }
        #endregion


        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            double cWidth = ActualWidth;
            double cHeight = ActualHeight;

            if (cWidth == 0 || double.IsNaN(cWidth) || double.IsNaN(cHeight) || cHeight == 0)
                return;

            if (this.Parent != null && this.Parent is FrameworkElement)
            {
                double parentWidth = ((FrameworkElement)Parent).ActualWidth;
                double parentHeight = ((FrameworkElement)Parent).ActualHeight;

                if (parentWidth == 0 || double.IsNaN(parentWidth) || double.IsNaN(parentHeight) || parentHeight == 0)
                    return;

                parentWidth = parentWidth/2;
                parentHeight = parentHeight/2;

                if (parentWidth > cWidth)
                    ResizeToWidth(parentWidth);
                else if (parentHeight > cHeight)
                    ResizeToHeight(parentHeight);
            }
        }

        private void ResizeToHeight(double new_height)
        {
            if (new_height >= MIN_HEIGHT)
                view_box.Height = new_height;
            else
                view_box.Height = MIN_HEIGHT;
        }
        private void ResizeToWidth(double new_width)
        {
            if (new_width >= MIN_WIDTH)
                view_box.Width = new_width;
            else
                view_box.Width = MIN_WIDTH;
        }

        #region Attribs

        #region Dependency properties
        public static readonly DependencyProperty InfoTitleProperty;
        public static readonly DependencyProperty InfoSubTitleProperty;
        #endregion

        private const double MIN_HEIGHT = 100;
        private const double MIN_WIDTH = 100;

        #endregion
    }
}
