﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using CognitiveLib.Scanner;
using CognitiveLib.WebSearch;
using FSBlog.GoogleSearch.GoogleClient;
using Logicx.Utility;
using Utilities;

namespace CognitiveLib.Detector
{
    public class TypeNoDetectionResult
    {
        public WeightedString TypeNo
        {
            get
            {
                if (TypeNoCandidates != null && TypeNoCandidates.Count > 0)
                    return TypeNoCandidates.MaxBy(o => o.Weight);
                return new WeightedString();
            }
        }
        public List<WeightedString> TypeNoCandidates;
    }
    public class DeviceTypeNoDetector : DeviceCodeDetector
    {
        public static TypeNoDetectionResult Detect(string text)
        {
            if (string.IsNullOrEmpty(text))
                throw new Exception("cannot detect, no text given");

            // die ordnung der liste spielt eine rolle
            List<WeightedString> look_for = new List<WeightedString>(5);
            look_for.Add(new WeightedString("Type No"));
            look_for.Add(new WeightedString("TypeNo"));

            TypeNoDetectionResult sres = new TypeNoDetectionResult();
            sres.TypeNoCandidates = GetDeviceCodeCandidates(text, look_for, DeviceCodeDetector.GetBlackList(new List < string > { "Code", "No" }));
            return sres;
        }
    }
}
