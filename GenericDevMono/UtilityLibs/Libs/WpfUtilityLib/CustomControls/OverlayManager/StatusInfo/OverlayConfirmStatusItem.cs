﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace Logicx.WpfUtility.CustomControls.OverlayManager.StatusInfo
{
    public class OverlayConfirmStatusItem: OverlayStatusItem
    {
        public class ButtonClickEventArgs : EventArgs
        {
            public ButtonClickEventArgs(string name)
            {
                Name = name;
            }

            public string Name;
        }

        public OverlayConfirmStatusItem()
        {
            ConfirmInfo = new ConfirmInfo();
            ConfirmInfo.Margin = new Thickness(10, 0, 0, 0);
            border.Child = ConfirmInfo;

            Closeable = false;
            WithHelpButton = false;
            WithAbortButton = false;
        }

        public void AddButton(string name, string text)
        {
            if(ContainsButtonByName(name))
                return;

            OverlayDialogButton button = new OverlayDialogButton();
            button.Tag = name;
            button.Content = text;
            button.Margin = new Thickness(5,0,5,0);
            button.Click += new RoutedEventHandler(button_Click);
            button.HorizontalAlignment = HorizontalAlignment.Right;
            button.Style = ConfirmButtonStyle;
            ConfirmInfo.button_stack.Children.Add(button);
        }

        protected bool ContainsButtonByName(string name)
        {
            foreach(FrameworkElement elem in ConfirmInfo.button_stack.Children)
            {
                if (elem.Tag.Equals(name))
                    return true;
            }

            return false;
        }

        void button_Click(object sender, RoutedEventArgs e)
        {
            if(StatusConfirmed != null)
                StatusConfirmed(this, new ButtonClickEventArgs((string)((Button)sender).Tag));
        }


        public ConfirmInfo ConfirmInfo
        {
            get; set;
        }

        public event EventHandler<ButtonClickEventArgs> StatusConfirmed; 
    }
}
