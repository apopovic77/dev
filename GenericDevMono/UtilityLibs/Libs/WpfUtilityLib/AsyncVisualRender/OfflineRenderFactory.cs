﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Threading;
using System.Windows;
using System.Windows.Media;
using System.Windows.Threading;
using Logicx.Utilities;
using Logicx.Utility;

namespace Logicx.WpfUtility.AsyncVisualRender
{
    public class WpfVisualFactory
    {
        public static FrameworkElement CreateVisual(string full_class_name, object[] parameters)
        {
            FrameworkElement frame_element = null;
            Type type = Type.GetType(full_class_name);
            int max_try_instancing = 5;
            int try_instancing = 0;
            while (true)
            {
                if (try_instancing > max_try_instancing)
                    break;

                try_instancing++;
                try
                {
                    //object[] paras;
                    //if (parameters != null)
                    //{
                    //    paras = new object[parameters.Length + 1];
                    //    paras[0] = texture_generateable;
                    //    for (int i = 0; i < parameters.Length; i++)
                    //        paras[i + 1] = parameters[i];
                    //}
                    //else
                    //{
                    //    paras = new object[1];
                    //    paras[0] = texture_generateable;
                    //}

                    if(parameters != null)
                        frame_element = Activator.CreateInstance(type, parameters) as FrameworkElement;
                    else
                        frame_element = Activator.CreateInstance(type) as FrameworkElement; 

                    if (frame_element != null)
                        break;
                }
                catch (Exception ex)
                {
                    Console.Error.Write("cannot create instance - retry " + try_instancing);
                }
            }

            if (frame_element == null)
                throw new Exception("cannot create instance");

            return frame_element;
        }
    }  

    [DataContract]
    public class WpfRenderTask : IDisposable
    {
        /// <summary>
        /// ein string mit dem eindeutig identifizieren kann um welches objekt es sich handelt
        /// damit könnte man die offline visual gen dann selber durchführen
        /// </summary>
        [DataMember]
        public object VisualId;
        [DataMember]
        public object[] VisualConstructionParameter;

        /// <summary>
        /// hier kann man das Wpf Visual mitgeben welches man rendern möchte
        /// wenn dieses nicht mitgegeben wird wird versuch ein objekt selbst
        /// über die visual factory zu instanzieren
        /// wenn keine klassen name mitgegeben wurde kann dies nicht gemacht werden
        /// </summary>
        public FrameworkElement WpfVisual;

        /// <summary>
        /// wenn dieser pfad gesetzt wurde kann selbständig ein instanz erstellt werden
        /// die gerendert werden soll
        /// </summary>
        public virtual string WpfVisualFullClassName
        {
            get { return _wpf_visual_full_class_name; }
            set { _wpf_visual_full_class_name = value; }
        }
        private string _wpf_visual_full_class_name;
        
        public virtual FrameworkElement GetVisual(object[] parameter)
        {
            if(string.IsNullOrEmpty(WpfVisualFullClassName))
                throw new Exception("visual task kann nicht rendern, visual nicht vorhanden, setzten via WpfVisual bzw. wenn keine instanz vohanden ist dann via WpfVisualFullClassName");
            return WpfVisualFactory.CreateVisual(WpfVisualFullClassName, parameter);
        }

        public virtual FrameworkElement GetVisual()
        {
            if (WpfVisual != null)
                return WpfVisual;
            return GetVisual(VisualConstructionParameter);
        }


        #region Attributes
        [DataMember]
        public float Left;
        [DataMember]
        public float Top;

        /// <summary>
        /// hiermit kann man angeben wie gross er das bild rendern soll
        /// der zu verwendende Scale wird automatisch berechnet
        /// falls ein scale angegeben wurde passiert ??????
        /// </summary>
        [DataMember]
        public float? DesiredLen;
        /// <summary>
        /// hiermit gibt man an wie gross das gerenderte bild sein soll
        /// </summary>
        [DataMember]
        public float? DesiredLenMax;
        [DataMember]
        public float? DesiredLenMaxWidth;
        [DataMember]
        public float? DesiredLenMaxHeight;
        /// <summary>
        /// width des controls bei scale = 1
        /// </summary>
        [DataMember]
        public float ActualWidth;
        /// <summary>
        /// height des controls bei scale = 1
        /// </summary>
        [DataMember]
        public float ActualHeight;
        /// <summary>
        /// height des gerenderten bildes und damit die with des Data Streams
        /// </summary>
        [DataMember]
        public float RenderHeight;
        /// <summary>
        /// width des gerenderten bildes und damit die height des Data Streams
        /// </summary>
        [DataMember]
        public float RenderWidth;

        [DataMember]
        public int PoolIndex;
        [DataMember]
        public byte[] Data;
        public OfflineRenderFactory.OfflineRenderResultDelegate ResultDelegate;
        /// <summary>
        /// zeitpunkt zu dem der rendertask aus der queue genommen wurde
        /// </summary>
        public DateTime StartTime;
        /// <summary>
        /// Significance
        /// </summary>
        [DataMember]
        public float Significance;

        /// <summary>
        /// wenn dieser pfad gesetzt wird dann wird das render ergebnis
        /// nicht als byte[] Data zurückgegeben sonder direkt auf die hdd 
        /// abgelegt
        /// </summary>
        [DataMember] 
        public string RenderToDiskPath;
        public bool RenderToDisk
        {
            get {
                return !string.IsNullOrEmpty(RenderToDiskPath);
            }
        }

        #endregion

        public void Dispose()
        {
            Data = null;
            Data = new byte[1];
        }

        public int RenderWidthPx
        {
            get { return (int)Math.Round(RenderWidth); }
        }
        public int RenderHeightPx
        {
            get { return (int)Math.Round(RenderHeight); }
        }

        public PixelFormat PixelFormat = PixelFormats.Pbgra32;

        /// <summary>
        /// berechnet die gewünschte grösse auf basis des Disired Size Wertes
        /// dabei wird die aspect ratio des original controls berücksichtig
        /// </summary>
        public float DesiredWidth 
        { 
            get
            {
                if (ActualWidth == 0 || ActualHeight == 0)
                    return 0;

                if (!DesiredLen.HasValue)
                    return ActualWidth;

                float aspect_ratio = ActualWidth / ActualHeight;

                if (aspect_ratio > 1)
                {
                    return DesiredLen.Value;
                }
                else
                {
                    return DesiredLen.Value * aspect_ratio;
                }
            }
        }

        /// <summary>
        /// berechnet die gewünschte grösse auf basis des Disired Size Wertes
        /// dabei wird die aspect ratio des original controls berücksichtig
        /// </summary>
        public float DesiredHeight
        {
            get
            {
                if (ActualWidth == 0 || ActualHeight == 0)
                    return 0;

                if (!DesiredLen.HasValue)
                    return ActualHeight;

                float aspect_ratio = ActualWidth / ActualHeight;

                if (aspect_ratio > 1)
                {
                    return DesiredLen.Value / aspect_ratio;
                }
                else
                {
                    return DesiredLen.Value;
                }
            }
        }
    }

    public class AsyncRenderResultEventArgs : EventArgs
    {
        public AsyncRenderResultEventArgs(WpfRenderTask rti)
        {
            WpfRenderTask = rti;
        }

        public WpfRenderTask WpfRenderTask;
    }

    public class OfflineRenderFactory
    {
        private OfflineRenderFactory() { }

        public static OfflineRenderFactory Instance
        {
            get
            {
                // DoubleLock
                if (_instance == null)
                {
                    lock (_instance_creation_lock)
                    {
                        //gen instance
                        _instance = new OfflineRenderFactory();

                        _lock = new Mutex(false, "offline render factory lock");
                        _queued_render_reset_event = new AutoResetEvent(false);


                        _host_pool = new Dictionary<int, OfflineRenderHost>(HOST_POOL_SIZE);
                        _free_hosts = new List<OfflineRenderHost>(HOST_POOL_SIZE);
                        _occupied_hosts = new List<OfflineRenderHost>(HOST_POOL_SIZE);
                        _reset_event = new Dictionary<int, AutoResetEvent>(HOST_POOL_SIZE);
                        _render_tasks = new DropOutStack<WpfRenderTask>(MAX_RENDER_TASKS);
                        for (int i = 0; i < HOST_POOL_SIZE; i++)
                        {
                            OfflineRenderHost render_host = new OfflineRenderHost(i, "Offline Render Host " + i);
                            _host_pool.Add(i, render_host);
                            _reset_event.Add(i, new AutoResetEvent(false));
                            _free_hosts.Add(render_host);
                        }
                    }
                }
                return _instance;
            }
        }


        public static void Dispose()
        {
            lock (_instance_creation_lock)
            {
                if (_instance == null)
                    return;

                _free_hosts.Clear();
                _occupied_hosts.Clear();
                for (int i = 0; i < HOST_POOL_SIZE; i++)
                {
                    _host_pool[i].UnWait();
                    _host_pool[i].Dispose();
                }

                try
                {
                    if (_thread_render_queued != null)
                    {
                        _thread_render_queued.Abort();
                        _thread_render_queued = null;
                    }
                }
                catch{}

                _instance = null;
            }
        }

        public void RenderAsync(WpfRenderTask rti)
        {
            QueuedBackgroundWorker bw_worker = new QueuedBackgroundWorker("bw render async");
            bw_worker.DoWork += new QueuedBackgroundWorker.DoWorkEventHandler(RenderAsync_DoWork);
            bw_worker.RunWorkerAsync(rti);
        }

        private void RenderAsync_DoWork(object sender, DoWorkEventArgs e)
        {
            OfflineRenderHost render_host = GetNextHost();
            WpfRenderTask rti = (WpfRenderTask)e.Argument;

            Render(render_host, rti);
            ReleaseRenderHost(render_host);

            if (rti.ResultDelegate != null)
                rti.ResultDelegate(this, new AsyncRenderResultEventArgs(rti));
        }

        /// <summary>
        /// rendert das control synchron in einem parallelen thread
        /// </summary>
        public void Render(WpfRenderTask wpf_render_task)
        {
            wpf_render_task.ActualWidth = 0;
            wpf_render_task.ActualHeight = 0;

            OfflineRenderHost render_host = GetNextHost();
            Render(render_host, wpf_render_task);
            ReleaseRenderHost(render_host);
        }

        public void ClearQueue()
        {
            Lock();
            _render_tasks.Clear();
            UnLock();
        }

        public void RenderQueued(WpfRenderTask wpf_render_task)
        {
            Lock();
                wpf_render_task.StartTime = DateTime.Now;
                _render_tasks.Push(wpf_render_task);
            UnLock();


            if (_thread_render_queued == null)
            {
                _thread_render_queued = new QueuedBackgroundWorker("bw render queued");
                _thread_render_queued.DoWork += new QueuedBackgroundWorker.DoWorkEventHandler(RenderQueued_BgDoWork);
                _thread_render_queued.RunWorkerAsync();
            }
            else
            {
                _queued_render_reset_event.Set();
            }
        }

        void RenderQueued_BgDoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                while (true)
                {
                    WpfRenderTask wpf_render_task;
                    Lock();
                    if (_render_tasks.Count == 0)
                    {
                        UnLock();
                        _queued_render_reset_event.WaitOne();
                        _queued_render_reset_event.Reset();
                        continue;
                    }
                    else
                    {
                        wpf_render_task = _render_tasks.Pop();
                        if ((DateTime.Now - wpf_render_task.StartTime).TotalMilliseconds > RenderTaskObsoleteMsec)
                        {
                            UnLock();
                            continue;
                        }

                        UnLock();
                        Render(wpf_render_task);

                        if (wpf_render_task.ResultDelegate != null)
                            wpf_render_task.ResultDelegate(this, new AsyncRenderResultEventArgs(wpf_render_task));
                    }
                }
            }
            catch
            {
                _thread_render_queued = null;
            }
        }

        private bool Render(OfflineRenderHost render_host, WpfRenderTask rti)
        {
            try
            {
                Exception error = null;
                _objects_rendered++;

                rti.PoolIndex = render_host.PoolIndex;

                render_host.UnWait();
                render_host.Canvas.Dispatcher.Invoke(DispatcherPriority.Normal, (NoParas)delegate
                {
                    try
                    {
                        render_host.SetRenderVisual(rti, _reset_event[render_host.PoolIndex]);
                    }
                    catch (Exception ex)
                    {
                        error = ex;
                    }
                });

                if (error == null)
                {
                    _reset_event[render_host.PoolIndex].WaitOne();
                    _reset_event[render_host.PoolIndex].Reset();
                }

                return (error == null);
            }
            catch(Exception ex)
            {
                return false;
            }
        }

        private void ReleaseRenderHost(OfflineRenderHost render_host)
        {
            Lock();
                _free_hosts.Add(render_host);
                _occupied_hosts.Remove(render_host);
            UnLock();
        }

        public void Lock()
        {
            _lock.WaitOne();
        }
        public void UnLock()
        {
            _lock.ReleaseMutex();
        }

        private OfflineRenderHost GetNextHost()
        {
            while (true)
            {
                lock (this)
                {
                    int rindex = _free_hosts.Count - 1;
                    if (rindex >= 0)
                    {
                        OfflineRenderHost render_host = _free_hosts[rindex];
                        _free_hosts.RemoveAt(rindex);
                        _occupied_hosts.Add(render_host);
                        return render_host;
                    }
                }
                Thread.Sleep(50);
            }
        }

        public static bool SetHostPoolSize(int desired_host_pool_size)
        {
            lock(_instance_creation_lock)
            {
                if (_instance != null)
                    return false;

                HOST_POOL_SIZE = desired_host_pool_size;
                return true;
            }
        }

        public delegate void OfflineRenderResultDelegate(object sender, AsyncRenderResultEventArgs result);

        private static int _objects_rendered = 0;

        private static QueuedBackgroundWorker _thread_render_queued;
        private static AutoResetEvent _queued_render_reset_event;
        private static Mutex _lock;

        /// <summary>
        /// dont acces without locking
        /// </summary>
        public DropOutStack<WpfRenderTask> RenderTasks
        {
            get { return _render_tasks; }
        }

        /// <summary>
        /// wenn ein task dran kommt dann wird geschaut wieviel zeit in der zwischenzeit vergangen ist
        /// wenn die dauer länger als die angegebene zeit ist dann wird das item verschmissen und nicht gerendert
        /// </summary>
        public float RenderTaskObsoleteMsec = float.MaxValue;
        private static DropOutStack<WpfRenderTask> _render_tasks;
        private static List<OfflineRenderHost> _free_hosts;
        private static List<OfflineRenderHost> _occupied_hosts;
        private static Dictionary<int, OfflineRenderHost> _host_pool;
        private static Dictionary<int, AutoResetEvent> _reset_event;
        private static int HOST_POOL_SIZE = 1;
        private const int MAX_RENDER_TASKS = 100000;
        

        private static object _instance_creation_lock = new object();
        private static volatile OfflineRenderFactory _instance = null;
        
        protected delegate void NoParas();


    }
}
