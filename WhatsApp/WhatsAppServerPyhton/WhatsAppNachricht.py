﻿import datetime
import pymssql
import sys
from WhatsAppConfig import WhatsAppConfig


class WhatsAppNachricht():
    def __init__(self, eadr='', aadr='', body='', id=0, url=''):
        self.EmpfaengerAdresse = eadr
        self.AbsenderAdresse = aadr # = False
        self.Body = body
        self.Id = id
        self.GesendetAm = "2," + datetime.datetime.now().strftime("%Y-%m-%d %X") + ";";
        self.Url = url

    def Insert(self):
        connection, cursor = self.OpenConnection()
        discriminator = 'WhatsAppNachricht'
        if not (self.Url == None or self.Url == ""):
            discriminator = 'WhatsAppImageNachricht'

        sql = u"insert into nachrichten " \
        u"(Discriminator,AbsenderAdresse, EmpfaengerAdresse, Body, ErstelltVon,_RawKommunikationsKanaele, ErstelltAm,GeaendertAm,_RawGesendetAm) " \
        u"VALUES ( '"+discriminator+"','"+self.AbsenderAdresse+"', '"+self.EmpfaengerAdresse+"','"+self.Body+"','Python', 2, '"+ datetime.datetime.now().strftime("%Y-%m-%d")+"', '"+ datetime.datetime.now().strftime("%Y-%m-%d")+"','"+self.GesendetAm+"')"
        cursor.execute(sql.encode('ascii', "backslashreplace"))

        if not (self.Url == None or self.Url == ""):
            #get the id of the nachricht inserted
            cursor.execute("select Id from nachrichten where EmpfaengerAdresse = '"+self.EmpfaengerAdresse+"' and AbsenderAdresse = '"+self.AbsenderAdresse+"' and _RawGesendetAm = '"+self.GesendetAm+"' ")
            nachricht_id = 0
            for row in cursor:
                nachricht_id = row[0]
                break
            sql = u"insert into Medien " \
                    u"(Discriminator,PathToMedia, ErstelltVon, ErstelltAm,GeaendertAm, IsText) " \
                    u"VALUES ( 'Bild','"+self.Url+"', 'Python', '"+ datetime.datetime.now().strftime("%Y-%m-%d %X")+"','"+ datetime.datetime.now().strftime("%Y-%m-%d %X")+"', 0)"
            cursor.execute(sql.encode('ascii', "backslashreplace"))

            cursor.execute("select Id from Medien where PathToMedia = '"+self.Url+"'")
            medium_id = 0
            for row in cursor:
                medium_id = row[0]
                break

            sql = u"insert into NachrichtMedium (NachrichtId,MediumId) VALUES ( "+str(nachricht_id)+", "+str(medium_id)+" )"
            cursor.execute(sql.encode('ascii', "backslashreplace"))

        connection.commit() 
        self.CloseConnection(connection,cursor)

    def Update(self):
        if(self.Id <= 0):
            raise Exception('Id is null cannot update')

        connection, cursor = self.OpenConnection()

        sql = u"update nachrichten set " \
                u" AbsenderAdresse = '"+self.AbsenderAdresse+"'" \
                u" ,EmpfaengerAdresse = '"+self.EmpfaengerAdresse+"'" \
                u" ,Body = '"+self.Body+"'" \
                u" ,_RawGesendetAm = '"+self.GesendetAm+"' where id = "+str(self.Id) 

        cursor.execute(sql)
        connection.commit() 
        self.CloseConnection(connection,cursor)

    def UpdateGesendetAm(self):
        if(self.Id <= 0):
            raise Exception('Id is null cannot update')
        connection, cursor = self.OpenConnection()
        sql = u"update nachrichten set " \
                u" _RawGesendetAm = '2,"+datetime.datetime.now().strftime("%Y-%m-%d %X")+";' where id = "+str(self.Id) 
        cursor.execute(sql)
        connection.commit() 
        self.CloseConnection(connection,cursor)

    def ParseFromAndParticipant(self, sfrom, sparticipant):
        if sparticipant == None or sparticipant == "":
            self.AbsenderAdresse = sfrom
            self.EmpfaengerAdresse = WhatsAppConfig.WHATSAPP_TELNO
            return
        self.AbsenderAdresse = sparticipant
        self.EmpfaengerAdresse = sfrom


    def OpenConnection(self):
        connection = pymssql.connect(WhatsAppConfig.DB_SERVER, WhatsAppConfig.DB_USER, WhatsAppConfig.DB_PASS, WhatsAppConfig.DB_NAME)
        cursor = connection.cursor()

        return connection, cursor

    def CloseConnection(self, connection, cursor):
        if(connection == None):
            return

        cursor.close()
        del cursor
        cursor = None

        connection.close()
        del connection
        connection = None



