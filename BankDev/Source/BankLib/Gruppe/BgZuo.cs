﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BankLib.Data;

namespace BankLib.Gruppe
{

    public abstract class BgZuo
    {
        internal virtual void WriteToStream(BinaryWriter writer)
        {
            BinaryHelper.WriteString(writer, Beschreibung);
        }

        internal virtual void LoadFromStream(BinaryReader reader)
        {
            Beschreibung = BinaryHelper.ReadString(reader);
        }

        //public string Key
        //{
        //    get
        //    {
        //        return BuchungsGruppen.GetKey(this);
        //    }
        //}

        public float SumBetrag
        {
            get
            {
                if (Buchungen == null)
                    return 0;

                return Buchungen.Sum(b => b.Betrag);
            }
        }


        /// <summary>
        /// Diese Methode checkt ob die Buchung der Buchungsgruppe Zugeordnet werden kann
        /// </summary>
        /// <param name="b"></param>
        /// <returns></returns>
        public bool IsZuo(Buchung b)
        {
            return CheckZuo(b);
        }


        public bool AddBuchungIfFit(Buchung b)
        {


            if (CheckZuo(b))
            {
                AssertBuchungsList();
                Buchungen.Add(b);
                return true;
            }

            return false;
        }

        private void AssertBuchungsList()
        {
 	        if(Buchungen == null)
                Buchungen = new List<Buchung>();
        }

        protected abstract bool CheckZuo(Buchung b);
        /// <summary>
        /// gib einen wert zurück der für die sortierung der zuo genommen wird default wert soll 1 sein
        /// </summary>
        internal virtual int RulePriority {
            get
            {
                return 1;
            }
        }

        public override string ToString()
        {
            return String.Format("[BgZuoBase] Name {0}", Beschreibung);
        }

        //public string Name { get; set; }
        public string Beschreibung { get; set; }
        public BuchungsGruppe Parent { get; set; }
        public List<Buchung> Buchungen { get; set; }

    }
}
