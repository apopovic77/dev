﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IronOcr;

namespace CognitiveLib.Ocr
{
    public class IronOcr
    {
        public static string GetText(string path_to_file)
        {
            var Ocr = new AutoOcr();
            var Result = Ocr.Read(path_to_file);
            return Result.Text;

        }
    }
}
