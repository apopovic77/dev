﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Windows.Threading;
using Logicx.DiagramLib.Util;

namespace Logicx.DiagramLib.Scales
{
    class IntScale : ScaleType
    {

        #region Constants

        private const long MIN_INT_UNIT = 1;
        private const long MIN_INT_UNITS_MIDDLE = 20;

        private const short TEXT_LOD_0 = 0;
        private const short TEXT_LOD_1 = 1;
        #endregion

        #region Construction and Initialization
        #endregion

        #region Operations

        public override void Init(double size, bool generate_always)
        {
            short old_text_level = _text_lod;

            int start = _int_scale_control.Config.Start;
            int end = _int_scale_control.Config.End;

            int num_units = (int)((end - start + 1) / MIN_INT_UNIT);

            if (size / num_units >= MIN_INT_UNITS_MIDDLE)
                _text_lod = TEXT_LOD_1;
            else
                _text_lod = TEXT_LOD_0;

            if (generate_always || old_text_level != _text_lod)
            {
                int increment = 1;
                if (_text_lod == TEXT_LOD_0)
                    increment = 5;
                else if (_text_lod == TEXT_LOD_1)
                    increment = 1;

                int mod = start % increment;
                mod = mod < 0 ? increment + mod : mod;
                _unit_offset = increment == 1 ? 0 : mod;

                _int_scale_control.ClearChildren();
                GenerateData();
                AddChildren();
            }
        }

        private void GenerateData()
        {
            _long_texts.Clear();
            _description_text = null;

            GenerateDayAndSubData();
        }

        private void GenerateDayAndSubData()
        {
            int start = _int_scale_control.Config.Start;
            int end = _int_scale_control.Config.End;

            _description_text = GeometryUtils.GenerateTextBlock(_int_scale_control.Config.Description, TextStyleType.InfoText, typeof(IntScaleControl), _int_scale_control);
            if (_int_scale_control.Config.Orientation == Orientation.Vertical)
                _description_text.LayoutTransform = new RotateTransform(270);

            int increment = 1;
            if (_text_lod == TEXT_LOD_0)
                increment = 5;
            else if (_text_lod == TEXT_LOD_1)
                increment = 1;

            int mod = start % increment;
            mod = mod < 0 ? increment + mod : mod;
            int first = mod == 0 ? start : start + increment - mod;
            first += increment;
            end += increment;

            string fixed_text = _int_scale_control.IntValuesAscendingString;
            if (increment != 1)
            {
                int int_value;
                if (int.TryParse(_int_scale_control.IntValuesAscendingString, out int_value))
                    fixed_text = (int_value * increment).ToString();
            }

            for (int i = first; i <= end; i += increment)
            {
                string text = _int_scale_control.IntValuesAscendingString.Length == 0 ? (i * increment).ToString() : fixed_text;
                TextBlock b = GeometryUtils.GenerateTextBlock(text, TextStyleType.ScaleText, typeof(IntScaleControl), _int_scale_control);
                _long_texts.Add(b);
            }
        }

        private void AddChildren()
        {
            List<UIElement> elements = new List<UIElement>();

            elements.Add(_description_text);
            elements.AddRange(_long_texts.Cast<UIElement>());

            _int_scale_control.AddToChildren(elements);
        }
        protected delegate void NoParas();

        public override void AlignData(Orientation orientation)
        {
            int start = _int_scale_control.Config.Start;
            int end = _int_scale_control.Config.End;

            int num_units = (int)((end - start + 1) / MIN_INT_UNIT);

            int index = 0;
            int increment = 1;

            if (_text_lod == TEXT_LOD_0)
                increment = 5;
            else if (_text_lod == TEXT_LOD_1)
                increment = 1;

            int mod_start = start % increment;
            int offset = increment == 1 ? 0 : mod_start <= 0 ? Math.Abs(mod_start) : increment - mod_start;
            double unit_size = GetInternalUnitSize(_int_scale_control.Config.Orientation, increment);
            //Debug.WriteLine("start: " + start + ", increment=" + increment + " mod=" + mod_start + ", offset=" + offset + ", unitsize=" + unit_size + ", correction=" + (offset * unit_size / increment));
            int real_start = start + offset;
            int mod_end = end % increment;
            mod_end = increment == 1 ? 0 : Math.Abs(mod_end);
            int real_end = end - mod_end;
            int number = (real_end - real_start) / increment + 1;

            double centered_position = _int_scale_control.GetVisibleCenteredPosition();

            if (orientation == Orientation.Horizontal)
            {
                FormattedText ft_desc = new FormattedText(_description_text.Text, Thread.CurrentThread.CurrentCulture, FlowDirection.LeftToRight, new Typeface(TextBlock.FontStyleProperty.Name), 12, Brushes.Black);
                //Canvas.SetLeft(_description_text, _int_scale_control.Config.Padding.Left + (_int_scale_control.GetRenderWidth() - ft_desc.Width) / 2);
                //Canvas.SetBottom(_description_text, _int_scale_control.Config.Padding.Bottom);
                //Canvas.SetLeft(_description_text, (_int_scale_control.GetRenderWidth() - ft_desc.Width) / 2);
                Canvas.SetLeft(_description_text, centered_position - ft_desc.Width / 2);
                Canvas.SetBottom(_description_text, 0);

                double distance = _int_scale_control.GetRenderWidth() / num_units;
                //double current_position = _int_scale_control.Config.Padding.Left + offset * distance + unit_size / 2;
                double current_position = offset * distance + unit_size / 2;

                if (_long_texts.Count == 0)
                    return;

                for (int i = 0; i < number; i++)
                {
                    TextBlock text_block = _long_texts.ToArray()[index++];

                    Canvas.SetLeft(text_block, current_position + 5);
                    //Canvas.SetTop(text_block, _int_scale_control.Config.Padding.Top + 10);
                    Canvas.SetTop(text_block, 10);

                    current_position += distance * increment;
                }
            }
            else
            {
                //Canvas.SetLeft(_description_text, _int_scale_control.Config.Padding.Left);
                Canvas.SetLeft(_description_text, 0);
                FormattedText ft_desc = new FormattedText(_description_text.Text, Thread.CurrentThread.CurrentCulture, FlowDirection.LeftToRight, new Typeface(TextBlock.FontStyleProperty.Name), 12, Brushes.Black);
                //Canvas.SetBottom(_description_text, _int_scale_control.Config.Padding.Bottom + (_int_scale_control.GetRenderHeight() - ft_desc.Width) / 2);
                //Canvas.SetBottom(_description_text, (_int_scale_control.GetRenderHeight() - ft_desc.Width) / 2);
                Canvas.SetTop(_description_text, centered_position - ft_desc.Width / 2);

                double maxsize = GetMaxDescriptionSize(start, end);

                double distance = _int_scale_control.GetRenderHeight() / num_units;
                //double current_position = _int_scale_control.Config.Padding.Bottom + unit_size / 2 + offset * unit_size / increment;
                double current_position = unit_size / 2 + offset * unit_size / increment;
                //Debug.WriteLine("First text start position " + current_position);
                if (_long_texts.Count == 0)
                    return;

                for (int i = 0; i < number; i++)
                {
                    TextBlock text_block = _long_texts.ToArray()[index++];
                    FormattedText ft = new FormattedText(text_block.Text, Thread.CurrentThread.CurrentCulture, FlowDirection.LeftToRight, new Typeface(TextBlock.FontStyleProperty.Name), 10, Brushes.Black);

                    Canvas.SetBottom(text_block, current_position - ft.Height / 2);
                    //Canvas.SetLeft(text_block, _int_scale_control.Config.Padding.Left + 20 + maxsize - ft.Width / 2);
                    Canvas.SetLeft(text_block, 20 + maxsize - ft.Width / 2);

                    current_position += distance * increment;
                }

            }
        }

        private static double GetMaxDescriptionSize(int first, int last)
        {
            double max = 0;
            for (int i = first; i < last; i++)
            {
                FormattedText ft = new FormattedText(i.ToString(), Thread.CurrentThread.CurrentCulture, FlowDirection.LeftToRight, new Typeface(TextBlock.FontStyleProperty.Name), 10, Brushes.Black);
                if (ft.Width > max)
                    max = ft.Width;
            }

            return max;
        }

        public override double GetUnitSize(Orientation orientation)
        {
            int num_units = (int)((_int_scale_control.Config.End - _int_scale_control.Config.Start + 1) / MIN_INT_UNIT);

            if (orientation == Orientation.Horizontal)
                return _int_scale_control.GetRenderWidth() / num_units;

            return _int_scale_control.GetRenderHeight() / num_units;
        }

        public double GetInternalUnitSize(Orientation orientation, int increment)
        {
            double num_units = (double)(_int_scale_control.Config.End - _int_scale_control.Config.Start + 1) / increment;

            if (orientation == Orientation.Horizontal)
                return _int_scale_control.GetRenderWidth() / num_units;

            return _int_scale_control.GetRenderHeight() / num_units;
        }

        public override double GetUnitOffset(Orientation orientation)
        {
            return _unit_offset;
        }

        public override double GetUnitOffset()
        {
            throw new NotImplementedException();
        }

        public override List<double> GetGridLinePositions()
        {
            List<double> positions = new List<double>();

            double unit_size = GetUnitSize(_int_scale_control.Config.Orientation);
            double limit = _int_scale_control.GetRenderWidth();

            int increment = 1;

            if (_text_lod == TEXT_LOD_0)
                increment = 5;
            else if (_text_lod == TEXT_LOD_1)
                increment = 1;

            if (_int_scale_control.Config.Orientation == Orientation.Horizontal)
            {
                //double current_pos = GetUnitOffset(_int_scale_control.Config.Orientation) * unit_size + _int_scale_control.Config.Padding.Left;
                double current_pos = GetUnitOffset(_int_scale_control.Config.Orientation) * unit_size;
                while (current_pos < limit)
                {
                    //if (current_pos >= _int_scale_control.Config.Padding.Left)
                    if (current_pos >= 0)
                        positions.Add(current_pos);
                    current_pos += unit_size * increment;
                }
            }
            else
            {
                if (unit_size == 0)
                    return positions;

                // from bottom to top 
                int mod_end = (_int_scale_control.Config.End + 1) % increment;
                mod_end = increment == 1 ? 0 : Math.Abs(mod_end);
                double current_pos = mod_end * unit_size;

                double scroll_offset = _int_scale_control.GetVisibleLeftmostPosition();
                double bottom_limit = _int_scale_control.GetRenderHeight();
                //Debug.WriteLine("Start pos=" + current_pos + ", scroll_offset=" + scroll_offset + ", scroll_diff=" + (unit_size - scroll_offset));
                current_pos -= scroll_offset;
                while (current_pos <= bottom_limit)
                {
                    positions.Add(current_pos);

                    current_pos += unit_size * increment;
                }
            }

            return positions;
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            _int_scale_control = null;
            _description_text = null;
            _long_texts.Clear();
        }
        #endregion

        #region Event Handlers
        #endregion

        #region Properties

        internal override Object Control
        {
            get
            {
                return _int_scale_control;
            }
            set
            {
                if (value == null)
                    throw new ArgumentException("Control must not be null!");

                if (value is IntScaleControl)
                {
                    _int_scale_control = (IntScaleControl)value;
                }
                else
                    throw new ArgumentException("Type " + value.GetType());
            }
        }

        #endregion

        #region Dependency Properties
        #endregion

        #region Events
        #endregion

        #region Routed Events
        #endregion

        #region Attributes

        private IntScaleControl _int_scale_control;

        private short _text_lod = -1;
        private double _unit_offset;

        private readonly List<TextBlock> _long_texts = new List<TextBlock>();
        private TextBlock _description_text;

        #endregion

        #region Tests
        #endregion

    }
}
