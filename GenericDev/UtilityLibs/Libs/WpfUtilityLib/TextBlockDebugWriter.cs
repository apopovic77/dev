﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows.Threading;
using Logicx.Utilities;

namespace Logicx.WpfUtility
{
    public class TextBlockDebugWriter : ConsoleDebugWriter, IDebugWriter
    {
        public TextBlockDebugWriter(TextBlock label)
        {
            _textblock_output = label;
            AutoFlush = false;
        }


        /// <summary>
        /// Delegate used for thread-safety reasons.
        /// </summary>
        /// <param name="dt"></param>
        private delegate void WriteInfoHandler(string dt);

        public virtual void WriteInfo(string msg)
        {
            lock (this)
            {
                if (_withdateinfo)
                    msg = DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() + ": " + msg;

                if (m_msgs.Count >= MaxMessagesToShow)
                    m_msgs.RemoveAt(0);

                m_msgs.Add(msg);
            }
            if (AutoFlush)
               Flush();

        }

        /// <summary>
        /// Delegate used for thread-safety reasons.
        /// </summary>
        /// <param name="dt"></param>
        private delegate void FlushHandler();
        public virtual void Flush()
        {
            if (_textblock_output.Dispatcher.Thread == System.Threading.Thread.CurrentThread)
                DoFlush();
            else
                _textblock_output.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Render, new FlushHandler(DoFlush));
        }
        private void DoFlush()
        {
            lock (this)
            {
                _textblock_output.Inlines.Clear();
                foreach (string msg2 in m_msgs)
                {
                    string prev_add_str = ((_textblock_output.Inlines.Count == 0) ? "" : "\n");
                    _textblock_output.Inlines.Add(new Run(prev_add_str + msg2));
                }
            }
        }

        protected TextBlock _textblock_output;
    }
}
