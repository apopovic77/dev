namespace LoggingLib.Data
{
    using System.Runtime.Serialization;

    partial class LoggingDataContext
    {
    }

    /// <summary>
    /// Partial class description which marks the class for inheriting from
    /// <see cref="LinqLib.Serialization.IdentifyableObjectBase">IdentifyableObjectBase</see> 
    /// for adding a unique identifier which is needed for deserialization.
    /// </summary>
    partial class Category : LinqLib.Serialization.IdentifyableObjectBase
    {
        
    }

    /// <summary>
    /// Partial class description which marks the class for inheriting from
    /// <see cref="LinqLib.Serialization.IdentifyableObjectBase">IdentifyableObjectBase</see> 
    /// for adding a unique identifier which is needed for deserialization.
    /// </summary>
    partial class LogEntry : LogEntryBase
    {
        /// <summary>
        /// Gets or sets the priority value with a mapping to the Priority enumeration
        /// </summary>
        public Priority Priority
        {
            get
            {
                return (LoggingLib.Priority) PriorityInt;
            }
            set
            {
                PriorityInt = (int)value;
            }
        }

        /// <summary>
        /// Gets or sets the severity value with a mapping to the Severity enumeration
        /// </summary>
        public Severity Severity
        {
            get
            {
                return (LoggingLib.Severity)SeverityInt;
            }
            set
            {
                SeverityInt = (int)value;
            }
        }

        /// <summary>
        /// Gets/Sets an array of category names which are not attached to the data context object
        /// </summary>
        /// <remarks>This property will be used internally to add categories to a log entry
        /// very fast. The time consumable attachment of category objects from the data context
        /// will be done in the worker thread. The worker thread uses this list to determine
        /// which categories to attach.</remarks>
        [DataMember]
        public string[] UnattachedCategtories
        {
            get { return _unattached_categories; }
            set { _unattached_categories = value; }
        }


        private string[] _unattached_categories = new string[0];
    }

    /// <summary>
    /// Partial class description which marks the class for inheriting from
    /// <see cref="LinqLib.Serialization.IdentifyableObjectBase">IdentifyableObjectBase</see> 
    /// for adding a unique identifier which is needed for deserialization.
    /// </summary>
    partial class CategoryLogEntry : LinqLib.Serialization.IdentifyableObjectBase
    {
        
    }

    /// <summary>
    /// Partial class description which marks the class for inheriting from
    /// <see cref="LinqLib.Serialization.IdentifyableObjectBase">IdentifyableObjectBase</see> 
    /// for adding a unique identifier which is needed for deserialization.
    /// </summary>
    partial class SQLServer : LinqLib.Serialization.IdentifyableObjectBase
    {

    }

    /// <summary>
    /// Partial class description which marks the class for inheriting from
    /// <see cref="LinqLib.Serialization.IdentifyableObjectBase">IdentifyableObjectBase</see> 
    /// for adding a unique identifier which is needed for deserialization.
    /// </summary>
    partial class InstalledSoftware : LinqLib.Serialization.IdentifyableObjectBase
    {

    }
}
