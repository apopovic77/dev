﻿using System;
using System.Collections.Generic;
using System.Drawing;
using MathLib;

namespace Logicx.Geo.Algorithms
{
    public class PolyUtilities
    {
        #region POLYLINESIMPLIFICATION
        public static Vector2f[] PolylineSimplification(float tol, Vector2f[] line)
        {
            Vector2f[] line_out;
            PolylineSimplifier.PolySimplify(tol, line, out line_out);
            return line_out;
        }

        public static Vector2f[] PolylineComplexation(int interpolated_values_count, Vector2f[] org_vector_ords)
        {
            interpolated_values_count = Convert.ToInt32(Math.Abs(interpolated_values_count));

            List<Vector2f> res_ords = new List<Vector2f>();
            res_ords.Add(org_vector_ords[0]);
            for (int i = 0; i < org_vector_ords.Length-2; i++)
            {
                Vector2f org_v1 = org_vector_ords[i];
                Vector2f org_v2 = org_vector_ords[i+1];
                Vector2f org_v3 = org_vector_ords[i+2];

                Vector2f kat1 = (org_v1 - org_v2);
                Vector2f kat2 = (org_v3 - org_v2);

                Vector2f p_mid1 = org_v2 + kat1.GetNorm() * (kat1.GetLen() / 2f);
                Vector2f p_mid2 = org_v2 + kat2.GetNorm() * (kat2.GetLen() / 2f);

                QuadBezierCurve bezier = new QuadBezierCurve((Vector2d)p_mid1, (Vector2d)org_v2, (Vector2d)p_mid2);
                for (float j = 0; j < interpolated_values_count+2; j++)
                {
                    float bez_t = j/interpolated_values_count;
                    Vector2f bez_pos_v = (Vector2f)bezier.Calc(bez_t);
                    res_ords.Add(bez_pos_v);
                }
            }
            res_ords.Add(org_vector_ords[org_vector_ords.Length - 1]);
            return org_vector_ords;
        }

        #region OPEN SOURCE ALG
        /// <summary>
        /// Copyright 2002, softSurfer (www.softsurfer.com)
        /// This code may be freely used and modified for any purpose
        /// providing that this copyright notice is included with it.
        /// SoftSurfer makes no warranty for this code, and cannot be held
        /// liable for any real or imagined damage resulting from its use.
        /// Users of this code must verify correctness for their application.
        /// </summary>
        private class PolylineSimplifier
        {
            // poly_simplify():
            //    Input:  tol = approximation tolerance
            //            V[] = polyline array of vertex points
            //            n   = the number of points in V[]
            //    Output: sV[]= simplified polyline vertices (max is n)
            //    Return: m   = the number of points in sV[]

            /// <summary>
            /// 
            /// </summary>
            /// <param name="tol">approximation tolerance</param>
            /// <param name="V">polyline array of vertex points</param>
            /// <param name="sV"></param>
            /// <returns></returns>
            public static int PolySimplify(float tol, Vector2f[] V, out Vector2f[] sV_out)
            {
                if (V.Length == 0) { sV_out = null; return 0; }
                List<Vector2f> sV = new List<Vector2f>();
                int n = V.Length;
                int i, k, m, pv;            // misc counters
                float tol2 = tol * tol;       // tolerance squared
                Vector2f[] vt = new Vector2f[n];      // vertex buffer
                int[] mk = new int[n];  // marker buffer

                // STAGE 1.  Vertex Reduction within tolerance of prior vertex cluster
                vt[0] = V[0];              // start at the beginning
                for (i = k = 1, pv = 0; i < n; i++)
                {
                    if (d2(V[i], V[pv]) < tol2)
                        continue;
                    vt[k++] = V[i];
                    pv = i;
                }
                if (pv < n - 1)
                    vt[k++] = V[n - 1];      // finish at the end

                // STAGE 2.  Douglas-Peucker polyline simplification
                mk[0] = mk[k - 1] = 1;       // mark the first and last vertices
                SimplifyDP(tol, vt, 0, k - 1, mk);

                // copy marked vertices to the output simplified polyline
                for (i = 0; i < k; i++)
                {
                    if (mk[i] == 1)
                        sV.Add(vt[i]);
                }
                sV_out = sV.ToArray();
                return sV.Count;         // m vertices in simplified polyline
            }

            // simplifyDP():
            //  This is the Douglas-Peucker recursive simplification routine
            //  It just marks vertices that are part of the simplified polyline
            //  for approximating the polyline subchain v[j] to v[k].
            //    Input:  tol = approximation tolerance
            //            v[] = polyline array of vertex points
            //            j,k = indices for the subchain v[j] to v[k]
            //    Output: mk[] = array of markers matching vertex array v[]
            protected static void SimplifyDP(float tol, Vector2f[] v, int j, int k, int[] mk)
            {
                if (k <= j + 1) // there is nothing to simplify
                    return;

                // check for adequate approximation by segment S from v[j] to v[k]
                int maxi = j;          // index of vertex farthest from S
                float maxd2 = 0;         // distance squared of farthest vertex
                float tol2 = tol * tol;  // tolerance squared
                Vector2f sp0 = v[j];
                Vector2f sp1 = v[k];  // segment from v[j] to v[k]
                Vector2f u = sp1 - sp0;   // segment direction vector
                float cu = dot(u, u);      // segment length squared

                // test each vertex v[i] for max distance from S
                // compute using the Feb 2001 Algorithm's dist_Point_to_Segment()
                // Note: this works in any dimension (2D, 3D, ...)
                Vector2f w;
                Vector2f Pb;                // base of perpendicular from v[i] to S
                float b, cw, dv2;        // dv2 = distance v[i] to S squared

                for (int i = j + 1; i < k; i++)
                {
                    // compute distance squared
                    w = v[i] - sp0;
                    cw = dot(w, u);
                    if (cw <= 0)
                        dv2 = d2(v[i], sp0);
                    else if (cu <= cw)
                        dv2 = d2(v[i], sp1);
                    else
                    {
                        b = cw / cu;

                        /*ORG LINE*/
                        //Pb = sp0 + b * u;

                        //VER ALEX
                        Pb = sp0 + (u * b);

                        dv2 = d2(v[i], Pb);
                    }
                    // test with current max distance squared
                    if (dv2 <= maxd2)
                        continue;
                    // v[i] is a new max vertex
                    maxi = i;
                    maxd2 = dv2;
                }
                if (maxd2 > tol2)        // error is worse than the tolerance
                {
                    // split the polyline at the farthest vertex from S
                    mk[maxi] = 1;      // mark v[maxi] for the simplified polyline
                    // recursively simplify the two subpolylines at v[maxi]
                    SimplifyDP(tol, v, j, maxi, mk);  // polyline v[j] to v[maxi]
                    SimplifyDP(tol, v, maxi, k, mk);  // polyline v[maxi] to v[k]
                }
                // else the approximation is OK, so ignore intermediate vertices
                return;
            }

            // dot product (2D) which allows vector operations in arguments
            private static float dot(Vector2f u, Vector2f v)
            {
                return u.X * v.X + u.Y * v.Y;
            }
            // norm2 = squared length of vector
            private static float norm2(Vector2f v)
            {
                return dot(v, v);
            }
            // norm = length of vector
            private static float norm(Vector2f v)
            {
                return (float)Math.Sqrt(norm2(v));
            }
            // distance squared = norm2 of difference
            private static float d2(Vector2f u, Vector2f v)
            {
                return norm2(u - v);
            }
            // distance = norm of difference
            private static float d(Vector2f u, Vector2f v)
            {
                return norm(u - v);
            }


        }
        #endregion
        
        #endregion

        #region Convex Hull
        // Copyright 2001, softSurfer (www.softsurfer.com)
        // This code may be freely used and modified for any purpose
        // providing that this copyright notice is included with it.
        // SoftSurfer makes no warranty for this code, and cannot be held
        // liable for any real or imagined damage resulting from its use.
        // Users of this code must verify correctness for their application.

        // Assume that a class is already given for the object:
        //    Point with coordinates {float x, y;}
        //===================================================================

        // isLeft(): tests if a point is Left|On|Right of an infinite line.
        //    Input:  three Vector2ds P0, P1, and P2
        //    Return: >0 for P2 left of the line through P0 and P1
        //            =0 for P2 on the line
        //            <0 for P2 right of the line
        //    See: the January 2001 Algorithm on Area of Triangles
        private static double isLeft( Vector2d P0, Vector2d P1, Vector2d P2 )
        {
            return (P1.X - P0.X)*(P2.Y - P0.Y) - (P2.X - P0.X)*(P1.Y - P0.Y);
        }
        //===================================================================
         

        // GetConvexHull(): Andrew's monotone chain 2D convex hull algorithm
        //     Input:  P[] = an array of 2D Vector2ds
        //                   presorted by increasing x- and y-coordinates
        //             n = the number of Vector2ds in P[]
        //     Output: H[] = an array of the convex hull vertices (max is n)
        //     Return: the number of Vector2ds in H[]
        public static List<Vector2d> GetConvexHull( List<Vector2d> P )
        {
            int n = P.Count;
            Vector2d[] H = new Vector2d[P.Count + 1];
            // the output array H[] will be used as the stack
            int bot = 0, top = (-1); // indices for bottom and top of the stack
            int i; // array scan index

            // Get the indices of Vector2ds with min x-coord and min|max y-coord
            int minmin = 0, minmax;
            double xmin = P[0].X;
            for (i = 1; i < n; i++)
                if (P[i].X != xmin) break;
            minmax = i - 1;
            if (minmax == n - 1)
            {
                // degenerate case: all x-coords == xmin
                H[++top] = P[minmin];
                if (P[minmax].Y != P[minmin].Y) // a nontrivial segment
                    H[++top] = P[minmax];
                H[++top] = P[minmin]; // add polygon endVector2d

                return GenList(top + 1, H);
            }

            // Get the indices of Vector2ds with max x-coord and min|max y-coord
            int maxmin, maxmax = n - 1;
            double xmax = P[n - 1].X;
            for (i = n - 2; i >= 0; i--)
                if (P[i].X != xmax) break;
            maxmin = i + 1;

            // Compute the lower hull on the stack H
            H[++top] = P[minmin]; // push minmin Vector2d onto stack
            i = minmax;
            while (++i <= maxmin)
            {
                // the lower line joins P[minmin] with P[maxmin]
                if (isLeft(P[minmin], P[maxmin], P[i]) >= 0 && i < maxmin)
                    continue; // ignore P[i] above or on the lower line

                while (top > 0) // there are at least 2 Vector2ds on the stack
                {
                    // test if P[i] is left of the line at the stack top
                    if (isLeft(H[top - 1], H[top], P[i]) > 0)
                        break; // P[i] is a new hull vertex
                    else
                        top--; // pop top Vector2d off stack
                }
                H[++top] = P[i]; // push P[i] onto stack
            }

            // Next, compute the upper hull on the stack H above the bottom hull
            if (maxmax != maxmin) // if distinct xmax Vector2ds
                H[++top] = P[maxmax]; // push maxmax Vector2d onto stack
            bot = top; // the bottom Vector2d of the upper hull stack
            i = maxmin;
            while (--i >= minmax)
            {
                // the upper line joins P[maxmax] with P[minmax]
                if (isLeft(P[maxmax], P[minmax], P[i]) >= 0 && i > minmax)
                    continue; // ignore P[i] below or on the upper line

                while (top > bot) // at least 2 Vector2ds on the upper stack
                {
                    // test if P[i] is left of the line at the stack top
                    if (isLeft(H[top - 1], H[top], P[i]) > 0)
                        break; // P[i] is a new hull vertex
                    else
                        top--; // pop top Vector2d off stack
                }
                H[++top] = P[i]; // push P[i] onto stack
            }
            if (minmax != minmin)
                H[++top] = P[minmin]; // push joining endVector2d onto stack

            return GenList(top + 1, H);
        }

        private static List<Vector2d> GenList(int number_of_Vector2ds_in_H, Vector2d[] H)
        {
            List<Vector2d> res_Vector2ds = new List<Vector2d>(number_of_Vector2ds_in_H);
            for (int j = 0; j < number_of_Vector2ds_in_H; j++)
            {
                res_Vector2ds.Add(H[j]);
            }
            return res_Vector2ds;
        }
        #endregion

        #region POINT IN POLYGON
public static bool PointInPolygon(Vector2d p, Vector2d[] polygon)
        {
            bool c = false;
            for (int i = 0, j = polygon.Length - 1; i < polygon.Length; j = i++)
            {
                if ((((polygon[i].Y <= p.Y) && (p.Y < polygon[j].Y)) || ((polygon[j].Y <= p.Y) && (p.Y < polygon[i].Y))) && (p.X < (polygon[j].X - polygon[i].X) * (p.Y - polygon[i].Y) / (polygon[j].Y - polygon[i].Y) + polygon[i].X))
                    c = !c;
            }
            return c;
        }
        public static bool PointInPolygon(Vector2d p, List<Vector2d> polygon)
        {
            bool c = false;
            for (int i = 0, j = polygon.Count - 1; i < polygon.Count; j = i++)
            {
                if ((((polygon[i].Y <= p.Y) && (p.Y < polygon[j].Y)) || ((polygon[j].Y <= p.Y) && (p.Y < polygon[i].Y))) && (p.X < (polygon[j].X - polygon[i].X) * (p.Y - polygon[i].Y) / (polygon[j].Y - polygon[i].Y) + polygon[i].X))
                    c = !c;
            }
            return c;
        }
        #endregion
    }

}
