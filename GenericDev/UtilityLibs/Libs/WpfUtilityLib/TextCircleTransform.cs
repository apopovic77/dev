﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using MathLib;

namespace Logicx.WpfUtility
{

    /// <summary>
    /// USGAE:
    /// </summary>
    /// <example> This sample shows how to call the GetZero method.
    /// <code>
    /// TextCircleTransform circletransform = new TextCircleTransform(80, "Alexander", 20, 40, FontWeights.Normal, "Calibri",3);
    /// Path p = circletransform.Path;
    /// p.Stroke = Brushes.White;
    /// p.StrokeThickness = 1;
    /// p.Fill = Brushes.Green;
    /// p.BitmapEffect = new DropShadowBitmapEffect();
    /// canvas.Children.Add(p);
    /// </code>
    /// </example>
    public class TextCircleTransform
    {
        #region CHAR CIRCLE TRANSFORM
        protected class CharCircleTransform
        {
            public CharCircleTransform(float circle_radius, string text, float padding, int font_size, FontWeight font_weight, string font_fam)
            {
                _text = text;
                _padding = padding;
                _font_fam = font_fam;
                _font_size = font_size;
                _font_weight = font_weight;
                _circle_radius = circle_radius;

                // Create a formatted _text string.
                FormattedText formatted_text = new FormattedText(
                    _text,
                    CultureInfo.GetCultureInfo("de-at"),
                    FlowDirection.LeftToRight,
                    new Typeface(_font_fam),
                    _font_size,
                    Brushes.Black);

                // Set the font weight to Bold for the formatted _text.
                formatted_text.SetFontWeight(_font_weight);
                _overhang_leading = (float)formatted_text.OverhangLeading;
                _width = (float)(formatted_text.Width - formatted_text.OverhangLeading - formatted_text.OverhangTrailing);
                //_width = (float)formatted_text.Width - (float)(formatted_text.OverhangLeading );
                // Build a geometry out of the formatted _text.
                Geometry geometry = formatted_text.BuildGeometry(new Point(-formatted_text.OverhangLeading, 0));
                PathGeometry geom_horz = geometry.GetFlattenedPathGeometry();

                //we can now set the _ursprung
                _ursprung = new Vector2f((float) geom_horz.Bounds.BottomLeft.X, (float) geom_horz.Bounds.BottomLeft.Y + _circle_radius);

                ////check bound contraints
                //if (_circle_radius*2f < geometry.Bounds.Width)
                //    throw new Exception("geom must be smaller than circle durchmesser on which to be bended");
                ////check bound contraints
                //if (_circle_radius*2f < _padding)
                //    throw new Exception("padding must be smaller than circle durchmesser on which to be bended");

                //bring den _text mit einer rotation in die richtige position
                float rot_vec = GetTextBendRotVec(geometry);

                //jetzt müsste sich das ende vom _text also der bottomright vec genau am circle umfang treffen
                geometry.Transform = new RotateTransform(Vector2f.RadToDeg(rot_vec), geometry.Bounds.BottomLeft.X, geometry.Bounds.BottomLeft.Y);

                _text_bottom_left = new Vector2f((float) geom_horz.Bounds.BottomLeft.X, (float) geom_horz.Bounds.BottomLeft.Y);
                _text_baseline = new Vector2f(1f, 0f);
                _text_baseline.Rotate(-rot_vec);
                _text_baseline = _text_baseline.GetNorm();

                // Create a set of polygons by flattening the Geometry object.
                _path_geometry = geometry.GetFlattenedPathGeometry();
                //i need to clone because otherweise i cant change points, because the geom is freezed, the clone not
                _path_geometry = _path_geometry.Clone();

                //letzter schritt transformiere alle punkte im pfad
                BendGeomRadial(_path_geometry);
            }

            private void BendGeomRadial(PathGeometry geom)
            {
                for (int i = 0; i < geom.Figures.Count; i++)
                {
                    PathFigure pf = geom.Figures[i];
                    pf.StartPoint = GetBendedVectorPos(pf.StartPoint);

                    for (int j = 0; j < geom.Figures[i].Segments.Count; j++)
                    {
                        PolyLineSegment line = geom.Figures[i].Segments[j] as PolyLineSegment;

                        for (int k = 0; k < line.Points.Count; k++)
                        {
                            line.Points[k] = GetBendedVectorPos(line.Points[k]);
                        }
                    }
                }
            }

            private Point GetBendedVectorPos(Point p)
            {
                //p.X -= _overhang_leading;
                Vector2f v_dist = new Vector2f((float)p.X, (float)p.Y) - _text_bottom_left;
                float len_ortho = v_dist.GetSkalarProduct(_text_baseline);
                Vector2f v_ortho = _text_baseline * len_ortho;
                Vector2f v_len = v_dist - v_ortho;
                float dist_baseline = v_len.GetLen();

                //set min_x max_x values
                if (len_ortho < _min_x)
                    _min_x = len_ortho;
                if (len_ortho > _max_x)
                    _max_x = len_ortho;

                Vector2f v = new Vector2f((float)p.X - _ursprung.X, (float)p.Y - _ursprung.Y);
                v = v.GetNorm() * (_circle_radius + dist_baseline);
                //v.RotateCounterClockwise(-((float)Math.Asin((_padding/2f)/_circle_radius)*2));
                v.Rotate(-_padding / _circle_radius);
                p.X = _ursprung.X + v.X;
                p.Y = _ursprung.Y + v.Y;
                return p;
            }

            private float GetTextBendRotVec(Geometry geom)
            {
                //float h1 = (float)Math.Sqrt(-Math.Pow(geom.Bounds.Width / 2, 2) + Math.Pow(_circle_radius, 2));
                float alpha = (float)Math.PI / 2f - (float)Math.Asin((geom.Bounds.Width / 2) / _circle_radius);
                float beta = (float)Math.PI / 2f - alpha;
                return beta;
            }

            #region Properties

            public FontWeight FontWeight
            {
                get { return _font_weight; }
                set { _font_weight = value; }
            }

            public int FontSize
            {
                get { return _font_size; }
                set { _font_size = value; }
            }

            public string FontFamily
            {
                get { return _font_fam; }
                set { _font_fam = value; }
            }

            public string Text
            {
                get { return _text; }
                set { _text = value; }
            }

            public Vector2f TextPosBottomLeft
            {
                get { return _text_bottom_left; }
                set { _text_bottom_left = value; }
            }


            public Vector2f Ursprung
            {
                get { return _ursprung; }
                set { _ursprung = value; }
            }

            public float CircleRadius
            {
                get { return _circle_radius; }
                set { _circle_radius = value; }
            }

            public float Padding
            {
                get { return _padding; }
                set { _padding = value; }
            }

            public float Width
            {
                get {
                    return _max_x - _min_x; // return _width; 
                }
            }

            public PathGeometry PathGeometry
            {
                get { return _path_geometry; }
                set { _path_geometry = value; }
            }

            #endregion

            #region Attribs
            
            protected float _width;
            protected float _overhang_leading;
            protected float _padding;
            protected float _circle_radius = 200f;
            protected Vector2f _ursprung;
            protected Vector2f _text_baseline;
            protected Vector2f _text_bottom_left;
            protected string _text = "";
            protected string _font_fam = "Arial";
            protected int _font_size;
            protected FontWeight _font_weight = FontWeights.Normal;
            protected PathGeometry _path_geometry;
            protected float _min_x = float.MaxValue;
            protected float _max_x = float.MinValue;
            #endregion
        }
        #endregion

        public TextCircleTransform(float circle_radius, string text, float padding, int font_size, FontWeight font_weight, string font_fam, float kerning)
        {
            _kerning = kerning;
            _text = text;
            _padding = padding;
            _font_fam = font_fam;
            _font_size = font_size;
            _font_weight = font_weight;
            _circle_radius = circle_radius;

            _path_geometry = new PathGeometry();
            _path_geometry_list = new List<PathGeometry>();

            char[] letters = _text.ToCharArray();
            float curr_padding = _padding;
            for (int i = 0; i < letters.Length; i++)
            {
                char letter = letters[i];
                if(letter == ' ')
                {
                    curr_padding += _kerning + _kerning;
                    continue;
                }
                CharCircleTransform t = new CharCircleTransform(circle_radius, letter.ToString(), curr_padding, font_size, font_weight, font_fam);
                curr_padding += t.Width + _kerning;
                int c = t.PathGeometry.Figures.Count;
                _path_geometry_list.Add(t.PathGeometry);
                foreach (PathFigure figure in t.PathGeometry.Figures)
                    _path_geometry.Figures.Add(figure);
            }
            _path_geometry.Freeze();
        }


        #region Properties
        public List<PathGeometry> PathGeometryList
        {
            get { return _path_geometry_list; }
            set { _path_geometry_list = value; }
        }
        public PathGeometry PathGeometry
        {
            get { return _path_geometry; }
            set { _path_geometry = value; }
        }
        public Path Path
        {
            get
            {
                Path p = new Path();
                p.Data = _path_geometry;
                return p;
            }
        }
        #endregion


        #region Attribs
        protected float _kerning = 2;
        protected float _padding;
        protected float _circle_radius = 200f;
        protected string _text = "";
        protected string _font_fam = "Arial";
        protected int _font_size;
        protected FontWeight _font_weight = FontWeights.Normal;
        protected List<PathGeometry> _path_geometry_list;
        protected PathGeometry _path_geometry;
        #endregion
    }
}
