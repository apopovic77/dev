﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Logicx.WpfUtility.GraphPlotter
{
    /// <summary>
    /// Interaktionslogik für GraphLegend.xaml
    /// </summary>
    public partial class GraphLegend : UserControl
    {
        public GraphLegend()
        {
            InitializeComponent();
            IsFrozen = false;
        }

        public Orientation Orientation
        {
            set
            {
                myLegendContainer.Orientation = value;
            }
        }

        public void SetEntryTextForeground(Brush brush)
        {
            foreach (GraphLegendEntry ent in myLegendContainer.Children)
            {
                ent.myEntryName.Foreground = brush;
            }
        }

        public void ActivateEntry(string name)
        {
            foreach(GraphLegendEntry ent in myLegendContainer.Children)
            {
                if (ent.myEntryName.Text.Equals(name))
                    ent.ActivateEntry();
                else
                    ent.DeactivateEntry();
            }
        }

        public bool IsFrozen { set; get; }
    }
}