﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using Logicx.WpfUtility.WindowManagement;

namespace Logicx.WpfUtility.CustomControls.OverlayManager
{
    public enum OverlayWindowLayer
    {
        Normal, // normale fenster
        Active, // active fenster
        Modal,  // modale dialoge
        Presentation // fenster präsentationen
    }

    

    /// <summary>
    ///  Manages zindezes
    /// </summary>
    internal class OverlayLayerManager<B, S, T, U, O, SU>
        where S : SubMenuPanelBase<B, S, T, U, O, SU>, new()
        where B : BaseFrameBase<B, S, T, U, O, SU>
        where T : WindowTaskBarBase<B, S, T, U, O, SU>, new()
        where U : WindowTitleBarBase<B, S, T, U, O, SU>, new()
        where O : OverlayManagerBase<B, S, T, U, O, SU>, new()
        where SU : SubMenuPanelMenuItem
    {
        #region Constants
        /// <summary>
        /// Normaler Layer
        /// DIALOG1/PNG1 - DIALOG2/PNG2 ...
        /// </summary>
        private const int NORMAL_LAYER_ZINDEX = 1;
        /// <summary>
        /// Active Layer
        /// DIALOG1/PNG1 - DIALOG2/PNG2 ...
        /// </summary>
        private const int ACTIVE_LAYER_ZINDEX = 1000001;
        /// <summary>
        /// Modaler Layer
        /// MODALCANVAS1/DIALOG1/PNG1 - MODALCANVAS2/DIALOG2/PNG2 ...
        /// </summary>
        private const int MODAL_LAYER_ZINDEX = 2000001;
        /// <summary>
        /// Presentation Layer
        /// DIALOG1/PNG1 - DIALOG2/PNG2 ...
        /// </summary>
        private const int PRESENTATION_LAYER_ZINDEX = 10000001;
        #endregion

        public OverlayLayerManager(OverlayManagerBase<B, S, T, U, O, SU> manager)
        {
            _overlay_manager = manager;
        }

        #region Operations
        public void RecalculateLayers(OverlayWindowMeta metaData)
        {
            bool alreadyPartOfWindowList = _overlay_manager.DialogWindowList.Contains(metaData);

            int nWindowCount = 0;
            foreach (OverlayWindowMeta dlgMeta in _overlay_manager.DialogWindowList.OrderBy(x => x.DialogLayerZindex[OverlayWindowLayer.Normal]).ToList())
            {
                 // && metaData.IsActive)
                {
                    // die position des fensters im normalen layer berechnen
                    dlgMeta.DialogLayerZindex[OverlayWindowLayer.Normal] = NORMAL_LAYER_ZINDEX + (nWindowCount * 2);
                    dlgMeta.ScreenshotLayerZindex[OverlayWindowLayer.Normal] = dlgMeta.DialogLayerZindex[OverlayWindowLayer.Normal] + 1;
                    // die position des fensters im aktiv layer berechnen
                    dlgMeta.DialogLayerZindex[OverlayWindowLayer.Active] = ACTIVE_LAYER_ZINDEX + (nWindowCount * 2);
                    dlgMeta.ScreenshotLayerZindex[OverlayWindowLayer.Active] = dlgMeta.DialogLayerZindex[OverlayWindowLayer.Active] + 1;
                    // die position des fensters im modal layer berechnen
                    dlgMeta.DialogLayerZindex[OverlayWindowLayer.Modal] = MODAL_LAYER_ZINDEX + (nWindowCount * 3) + 1;
                    dlgMeta.ScreenshotLayerZindex[OverlayWindowLayer.Modal] = dlgMeta.DialogLayerZindex[OverlayWindowLayer.Modal] + 1;
                    // die position des fensters im presentation layer berechnen
                    dlgMeta.DialogLayerZindex[OverlayWindowLayer.Presentation] = PRESENTATION_LAYER_ZINDEX + (nWindowCount * 4) + 1;
                    dlgMeta.ScreenshotLayerZindex[OverlayWindowLayer.Presentation] = dlgMeta.DialogLayerZindex[OverlayWindowLayer.Presentation] + 1;
                    nWindowCount++;

                    if (dlgMeta != metaData || (dlgMeta==metaData && !metaData.IsActive))
                        PlaceWindowInLayer(dlgMeta);
                }
            }

            if (metaData.IsActive)
            {
                // das aktuelle fenster im normalen layer an oberste stelle rücken
                metaData.DialogLayerZindex[OverlayWindowLayer.Normal] = NORMAL_LAYER_ZINDEX + (nWindowCount * 2);
                metaData.ScreenshotLayerZindex[OverlayWindowLayer.Normal] = metaData.DialogLayerZindex[OverlayWindowLayer.Normal] + 1;
                // das aktuelle fenster im aktiv layer an oberste stelle rücken
                metaData.DialogLayerZindex[OverlayWindowLayer.Active] = ACTIVE_LAYER_ZINDEX + (nWindowCount * 2);
                metaData.ScreenshotLayerZindex[OverlayWindowLayer.Active] = metaData.DialogLayerZindex[OverlayWindowLayer.Active] + 1;
                // die position des fensters im modal layer berechnen
                metaData.DialogLayerZindex[OverlayWindowLayer.Modal] = MODAL_LAYER_ZINDEX + (nWindowCount * 3) + 1;
                metaData.ScreenshotLayerZindex[OverlayWindowLayer.Modal] = metaData.DialogLayerZindex[OverlayWindowLayer.Modal] + 1;
                // die position des fensters im presentation layer berechnen
                metaData.DialogLayerZindex[OverlayWindowLayer.Presentation] = PRESENTATION_LAYER_ZINDEX + (nWindowCount * 4) + 1;
                metaData.ScreenshotLayerZindex[OverlayWindowLayer.Presentation] = metaData.DialogLayerZindex[OverlayWindowLayer.Presentation] + 1;

                PlaceWindowInLayer(metaData);
            }

            AdjustCustomControlZIndex();
        }

        internal int AdjustCustomControlZIndex()
        {
            return AdjustCustomControlZIndex( MODAL_LAYER_ZINDEX - 1);
        }

        internal int AdjustCustomControlZIndex(int zIndexPosition)
        {
            if (_overlay_manager.CustomControlHost != null)
                Canvas.SetZIndex(_overlay_manager.CustomControlHost, zIndexPosition);

            foreach (DependencyObject ctrl in _overlay_manager.CustomControls)
            {
                Canvas.SetZIndex(ctrl as UIElement, zIndexPosition);
            }

            return zIndexPosition;
        }

        public OverlayWindowLayer PlaceWindowInLayer(OverlayWindowMeta metaData)
        {
            if (metaData == null)
                return OverlayWindowLayer.Normal;

            return PlaceWindowInLayer(metaData, metaData.Layer);
        }
        public OverlayWindowLayer PlaceWindowInLayer(OverlayWindowMeta metaData, OverlayWindowLayer newLayer)
        {
            if (metaData == null)
                return OverlayWindowLayer.Normal;

            OverlayWindowLayer oldLayer = metaData.Layer;
            metaData.Layer = newLayer;

            if (oldLayer != newLayer)
                metaData.FallbackLayer = oldLayer;

            if (metaData.Layer == OverlayWindowLayer.Normal || metaData.Layer == OverlayWindowLayer.Active)
            {
                Canvas.SetZIndex(metaData.Dialog, metaData.DialogLayerZindex[OverlayWindowLayer.Normal]);
                Canvas.SetZIndex(metaData.DialogWindowScreenshot, metaData.ScreenshotLayerZindex[OverlayWindowLayer.Normal]);

                if (metaData.IsActive)
                {
                    Canvas.SetZIndex(metaData.Dialog, metaData.DialogLayerZindex[OverlayWindowLayer.Active]);
                    Canvas.SetZIndex(metaData.DialogWindowScreenshot, metaData.ScreenshotLayerZindex[OverlayWindowLayer.Active]);
                }
            }
            else if (metaData.Layer == OverlayWindowLayer.Modal)
            {
                Canvas.SetZIndex(metaData.Dialog, metaData.DialogLayerZindex[OverlayWindowLayer.Modal]);
                Canvas.SetZIndex(metaData.DialogWindowScreenshot, metaData.ScreenshotLayerZindex[OverlayWindowLayer.Modal]);
            }
            else if (metaData.Layer == OverlayWindowLayer.Presentation)
            {
                Canvas.SetZIndex(metaData.Dialog, metaData.DialogLayerZindex[OverlayWindowLayer.Presentation]);
                Canvas.SetZIndex(metaData.DialogWindowScreenshot, metaData.ScreenshotLayerZindex[OverlayWindowLayer.Presentation]);
            }

            return oldLayer;
        }

        public void SetLastActiveDialogPerLayer(OverlayWindowLayer layer, OverlayWindowMeta dlg)
        {
            _last_active_dialog_per_layer[layer] = dlg;
        }

        public OverlayWindowMeta GetLastActiveDialogOfLayer(OverlayWindowLayer layer)
        {
            if (!_last_active_dialog_per_layer.ContainsKey(layer))
                return null;

            return _last_active_dialog_per_layer[layer];
        }
        #endregion

        #region properties
        public static int MaxNormalDialogLayer
        {
            get { return ACTIVE_LAYER_ZINDEX - 2; }
        }

        public static int MaxActiveDialogLayer
        {
            get { return MODAL_LAYER_ZINDEX - 2; }
        }

        public static int MaxModalDialogLayer
        {
            get { return PRESENTATION_LAYER_ZINDEX - 2; }
        }
        #endregion

        #region Attribs

        private OverlayManagerBase<B, S, T, U, O, SU> _overlay_manager;
        private Dictionary<OverlayWindowLayer, OverlayWindowMeta> _last_active_dialog_per_layer = new Dictionary<OverlayWindowLayer, OverlayWindowMeta>();
        #endregion
    }
}
