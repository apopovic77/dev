﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PoCShared;

namespace WebHardwareClient.ApiClient
{
    public class PrinterApiClient : PocApiClient
    {
        public const string UriApiPrinters = "printer";
        public const string UriApiPrintersId = "printer/{0}";

        public PrinterApiClient(string endpointUrl, string apiAccessToken = "") : base (endpointUrl, apiAccessToken)
        { }

        public async Task<List<PrinterItem>> Get(int? id, bool scopeToLocalPrinters = true)
        {
            string uri = UriApiPrinters;
            string param = "";

            if (id != null)
                uri = string.Format(UriApiPrintersId, id.Value);

            if (scopeToLocalPrinters)
                param  = "printServer=" + Environment.MachineName;

            return await Util.CallGetApiAsync<List<PrinterItem>>(uri, param);
        }

        public async Task<PrinterItem> Create(PrinterItem printer)
        {
            var addedPrinter = await Util.CallPostApiAsync<PrinterItem>(UriApiPrinters, printer);
            if (addedPrinter != null && addedPrinter.Id > 0)
                printer.Id = 0;

            return addedPrinter;
        }

        public async Task<PrinterItem> Update(PrinterItem printer)
        {
            string uri = UriApiPrinters;

            if (printer.Id > 0)
                uri = string.Format(UriApiPrintersId, printer.Id);
            else
            {
                throw new InvalidOperationException("Id must be set (existing printer)");
            }

            var updatedPrinter = await Util.CallPutApiAsync<PrinterItem>(uri, printer);
            return updatedPrinter;
        }

        public async Task<PrinterItem> Delete(PrinterItem printer)
        {
            string uri = UriApiPrinters;

            if (printer.Id > 0)
                uri = string.Format(UriApiPrintersId, printer.Id);
            else
            {
                throw new InvalidOperationException("Id must be set (existing printer)");
            }

            var deletedPrinter = await Util.CallDeleteApiAsync<PrinterItem>(uri);
            if (deletedPrinter != null)
                deletedPrinter.Id = 0;

            return deletedPrinter;
        }
    }
}
