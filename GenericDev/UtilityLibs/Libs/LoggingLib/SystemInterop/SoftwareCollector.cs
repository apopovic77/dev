﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Win32;

namespace LoggingLib.SystemInterop
{
    public class SoftwareCollector
    {
        #region nested SoftwareItem class
        /// <summary>
        /// Nested class holding information about a software item
        /// </summary>
        public class SoftwareItem
        {
            #region Construction and initialization
            /// <summary>
            /// Default constructor
            /// </summary>
            public SoftwareItem()
            {
            }
            /// <summary>
            /// Init constructor
            /// </summary>
            /// <param name="title"></param>
            /// <param name="publisher"></param>
            /// <param name="version"></param>
            public SoftwareItem(string title, string publisher, string version)
            {
                _title = title;
                _publisher = publisher;
                _version = version;
            }
            #endregion

            #region Properties
            /// <summary>
            /// Gets/Sets the title of the software
            /// </summary>
            public string Title
            {
                get { return _title; }
                set { _title = value;  }
            }
            /// <summary>
            /// gets/Sets the publisher of the software
            /// </summary>
            public string Publisher
            {
                get { return _publisher; }
                set { _publisher = value; }
            }
            /// <summary>
            /// Gets/Sets the version of the software
            /// </summary>
            public string Version
            {
                get { return _version; }
                set { _version = value; }
            }
            #endregion

            #region Operations
            /// <summary>
            /// Compares two software items
            /// </summary>
            /// <param name="obj"></param>
            /// <returns></returns>
            public override bool Equals(object obj)
            {
                SoftwareItem cmpItem = obj as SoftwareItem;

                if (cmpItem == null)
                    return false;

                if (cmpItem.Title.Trim().Equals(Title.Trim()) &&
                    cmpItem.Publisher.Trim().Equals(Publisher.Trim()) &&
                    cmpItem.Version.Trim().Equals(Version.Trim()))
                {
                    return true;
                }

                return false;
            }

            /// <summary>
            /// Gets the hash code of the object
            /// </summary>
            /// <returns>Returns the hash code</returns>
            public override int GetHashCode()
            {
                return Title.GetHashCode() ^ Publisher.GetHashCode() ^ Version.GetHashCode();
            }
            #endregion

            #region Attribs
            private string _title = "";
            private string _publisher = "";
            private string _version = "";
            #endregion
        }
        #endregion

        #region Construction and initialization
        /// <summary>
        /// Default constructor
        /// </summary>
        public SoftwareCollector()
        {
            ScanSoftware();
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets the list of scanned software products
        /// </summary>
        public List<SoftwareItem> SoftwareItems
        {
            get { return _scanned_software; }
        }
        #endregion

        #region Operations and methods
        private void ScanSoftware()
        {
            RegistryKey hklm = Registry.LocalMachine;
            RegistryKey uninstallkey = hklm.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall");

            if (uninstallkey != null)
            {
                foreach (string softwareKeyName in uninstallkey.GetSubKeyNames())
                {
                    RegistryKey swKey = uninstallkey.OpenSubKey(softwareKeyName);

                    if(swKey != null)
                    {
                        SoftwareItem swI = new SoftwareItem();
                        swI.Title = Convert.ToString(swKey.GetValue("DisplayName"));
                        swI.Version = Convert.ToString(swKey.GetValue("DisplayVersion"));
                        swI.Publisher = Convert.ToString(swKey.GetValue("Publisher"));

                        if (!string.IsNullOrEmpty(swI.Title.Trim()))
                        {
                            // avoid duplicate entries
                            if(!_scanned_software.Contains(swI))
                                _scanned_software.Add(swI);
                        }
                    }
                }
            }
        }
        #endregion

        #region Attribs
        private List<SoftwareItem> _scanned_software = new List<SoftwareItem>();
        #endregion
    }
}
