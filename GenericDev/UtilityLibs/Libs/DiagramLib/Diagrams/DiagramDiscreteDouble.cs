﻿using System;
using Logicx.DiagramLib.Controls;
using Logicx.DiagramLib.Scales;
using Logicx.DiagramLib.ViewConfig;

namespace Logicx.DiagramLib.Diagrams
{
    public class DiagramDiscreteDouble : DiagramBase<Object, double>
    {
        #region Constants
        #endregion

        #region Construction and Initialization
        public DiagramDiscreteDouble(DiagramConfig<Object, double> diagram_config, bool double_scale_in_percent)
            : base(diagram_config)
        {
            _double_scale_in_percent = double_scale_in_percent;
        }
        #endregion

        #region Operations

        protected override Scales.TypedScaleControl<Object> GetXAxisControl()
        {
            DiagramConfig.AxisScaleConfigX.Orientation = System.Windows.Controls.Orientation.Horizontal;

            if (_discrete_sct == null)
            {
                _discrete_sct = new DiscreteScaleControl<object>(DiagramConfig.AxisScaleConfigX);
                _discrete_sct.IsHitTestVisible = false;
            }

            return _discrete_sct;
        }

        protected override Scales.TypedScaleControl<double> GetYAxisControl()
        {
            DiagramConfig.AxisScaleConfigY.Orientation = System.Windows.Controls.Orientation.Vertical;

            if (_double_sct == null)
            {
                _double_sct = new DoubleScaleControl(DiagramConfig.AxisScaleConfigY, _double_scale_in_percent);
                _double_sct.IsHitTestVisible = false;
            }

            return _double_sct;
        }

        protected override double TransformXAxisValueToPixel(Object value)
        {
            if (XAxisControl != null)
                return XAxisControl.TransformAxisValueToPixel(value);

            return 0;
        }

        protected override double TransformYAxisValueToPixel(double value)
        {
            if (YAxisControl != null)
                return YAxisControl.TransformAxisValueToPixel(value);

            return 0;
        }

        protected override Object TransformXAxisPixelToValue(double pixel)
        {
            if (XAxisControl != null)
                return XAxisControl.TransformAxisPixelToValue(pixel);

            return DateTime.MinValue;
        }

        protected override double TransformYAxisPixelToValue(double pixel)
        {
            if (YAxisControl != null)
                return YAxisControl.TransformAxisPixelToValue(pixel);

            return 0;
        }
        #endregion

        #region Event Handlers
        #endregion

        #region Properties
        #endregion

        #region Dependency Properties
        #endregion

        #region Events
        #endregion

        #region Routed Events
        #endregion

        #region Attributes

        private DiscreteScaleControl<Object> _discrete_sct;
        private DoubleScaleControl _double_sct;
        private bool _double_scale_in_percent;

        #endregion


        #region Tests

        #endregion


    }
}
