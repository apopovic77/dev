﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using Logicx.WpfUtility.WpfHelpers;

namespace Logicx.WpfUtility.CustomControls.OverlayManager
{
    /// <summary>
    /// Button flags
    /// </summary>
    [Flags]
    public enum WizardButtons
    {
        Help = 1,
        Back = 2,
        Next = 4
    }
    /// <summary>
    /// Wizard navigation direction
    /// </summary>
    public enum NavigationSource
    {
        Next,   
        Back,
        Direct
    }

    public enum WizardResult
    {
        Finished,
        Canceled
    }

    public class OverlayWizard : OverlayContent //, IOverlayContent
    {
        #region Nested classes
        /// <summary>
        /// Info class holding call stack information
        /// </summary>
        private class CallStackInfo
        {
            public CallStackInfo(OverlayWizardPage p, NavigationSource s)
            {
                page = p;
                source = s;
            }

            public OverlayWizardPage page;
            public NavigationSource source;
        }
        #endregion

        #region construction
        /// <summary>
        /// Constructor of the class
        /// </summary>
        public OverlayWizard()
        {
            _page_host_grid = new Grid();
            _page_host_grid.Name = "wizard_pages_grid";
            _page_host_grid.RowDefinitions.Add(new RowDefinition() {Height = new GridLength(1,GridUnitType.Auto)});

            _button_prev = new OverlayDialogButton("< Zurück", OverlayButtonAlignment.Right, Key.Back);
            _button_next = new OverlayDialogButton("Weiter >", OverlayButtonAlignment.Right, Key.Enter);

            _button_help = new OverlayDialogButton("Hilfe", OverlayButtonAlignment.Left, Key.F1);

            AddButton(_button_prev);
            AddButton(_button_next);
            AddButton(_button_help);
        }
        #endregion

        #region Wizard Operations

        #region Page management
        /// <summary>
        /// Adds a new wizard page to the wizard's page collection
        /// </summary>
        /// <param name="page">OverlayWizardPage page</param>
        public void AddPage(OverlayWizardPage page)
        {
            if (page == null)
                throw new ArgumentNullException("page");

            if (!_wizard_pages.Contains(page))
            {
                _wizard_pages.Add(page);
                page.SetParentWizard(this);
            }

        }
        /// <summary>
        /// Removes a wizard page from the page collection
        /// </summary>
        /// <param name="page"></param>
        public void RemovePage(OverlayWizardPage page)
        {
            if (_wizard_pages.Contains(page))
            {
                _wizard_pages.Remove(page);
                page.SetParentWizard(null);
            }
        }
        /// <summary>
        /// Inserts a new wizard page into the page collection
        /// </summary>
        /// <param name="idx">Index to insert</param>
        /// <param name="page">Wizard page instance</param>
        public void Insert(int idx, OverlayWizardPage page)
        {
            if (page == null)
                throw new ArgumentNullException("page");

            if (!_wizard_pages.Contains(page))
            {
                _wizard_pages.Insert(idx, page);
                page.SetParentWizard(this);
            }
        }
        /// <summary>
        /// Removes the wizard page at a given index
        /// </summary>
        /// <param name="idx"></param>
        public void RemoveAt(int idx)
        {
            if (_wizard_pages.Count > idx)
            {
                OverlayWizardPage page = _wizard_pages[idx];
                page.SetParentWizard(null);
                _wizard_pages.RemoveAt(idx);
            }
        }
        /// <summary>
        /// Removes all wizard pages
        /// </summary>
        public void Clear()
        {
            foreach(OverlayWizardPage page in _wizard_pages)
                page.SetParentWizard(null);

            _wizard_pages.Clear();
        }

        /// <summary>
        /// Returns the next wizard page in the pages chain
        /// </summary>
        /// <returns></returns>
        public OverlayWizardPage GetNextPageInChain()
        {
            return GetNextPageInChain(_active_page);
        }
        /// <summary>
        /// Returns the next wizard page in the pages chain
        /// </summary>
        /// <param name="currentPage">Current page</param>
        /// <returns></returns>
        public OverlayWizardPage GetNextPageInChain(OverlayWizardPage currentPage)
        {
            int idx = _wizard_pages.IndexOf(currentPage);

            if (idx >= 0)
            {
                idx++;
                if (idx < _wizard_pages.Count)
                {
                    return _wizard_pages[idx];
                }
            }

            return null;
        }

        /// <summary>
        /// Returns the previous wizard page in the pages chain
        /// </summary>
        /// <returns></returns>
        public OverlayWizardPage GetPrevPageInChain()
        {
            return GetPrevPageInChain(_active_page);
        }
        /// <summary>
        /// Returns the previous wizard page in the pages chain
        /// </summary>
        /// <param name="currentPage">Current page</param>
        /// <returns></returns>
        public OverlayWizardPage GetPrevPageInChain(OverlayWizardPage currentPage)
        {
            int idx = _wizard_pages.IndexOf(currentPage);

            if (idx >= 0)
            {
                idx--;
                if (idx >= 0 && idx < _wizard_pages.Count)
                {
                    return _wizard_pages[idx];
                }
            }

            return null;
        }

        /// <summary>
        /// gets the age at a given index
        /// </summary>
        /// <param name="idx"></param>
        /// <returns></returns>
        public OverlayWizardPage GetPage(int idx)
        {
            if (idx >= 0 && idx < _wizard_pages.Count)
                return _wizard_pages[idx];

            return null;
        }

        /// <summary>
        /// Gets the page of a given type
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public OverlayWizardPage GetPage(Type type)
        {
            foreach (OverlayWizardPage page in _wizard_pages)
                if (page.GetType() == type)
                    return page;

            return null;
        }

        /// <summary>
        /// Show wizard page
        /// </summary>
        /// <param name="idx"></param>
        private void ShowPage(int idx)
        {
            ShowPage(_wizard_pages[idx]);
        }
        /// <summary>
        /// Show wizard page
        /// </summary>
        /// <param name="page"></param>
        private void ShowPage(OverlayWizardPage page)
        {
            if (page == null)
                return;

            _page_host_grid.Children.Clear();

            if (_active_page != null)
                _active_page.AfterHidePage();

            _active_page = page;

            
            _page_host_grid.Children.Add(page);
            
            if(ErrorProvider != null)
                ErrorProvider.CheckForUnlinkedControls();

            _active_page.AfterShowPage();

            DialogSubTitle = _active_page.PageTitle;
        }

        public void ClickNext()
        {
            OverlayButtonClicked(_button_next);
        }

        public void ClickBack()
        {
            OverlayButtonClicked(_button_prev);
        }
        
        public bool CloseWizard(bool forceClose)
        {
            bool bRet = true;
            _wizard_result = WizardResult.Canceled;

            if (OverlayManager != null)
            {
                if (!OverlayManager.HideContent(this,forceClose))
                {
                    _wizard_result = WizardResult.Canceled;
                    bRet = false;
                }
            }

            return bRet;
        }
        #endregion

        #region Page call stack
        private void PutCallStackInfo(OverlayWizardPage page, NavigationSource src)
        {
            _page_call_stack.Add(new CallStackInfo(page, src));
        }

        private CallStackInfo GetLastCallStackInfo()
        {
            if(_page_call_stack.Count > 0)
            {
                return _page_call_stack[_page_call_stack.Count - 1];
            }

            return null;    
        }
        #endregion

        #region Wizard button management

        /// <summary>
        /// Show all buttons
        /// </summary>
        public void ShowAllButtons()
        {
            _button_help.Visibility = Visibility.Visible;
            _button_prev.Visibility = Visibility.Visible;
            _button_next.Visibility = Visibility.Visible;
        }

        /// <summary>
        /// Hide all buttons
        /// </summary>
        public void HideAllButtons()
        {
            _button_help.Visibility = Visibility.Collapsed;
            _button_prev.Visibility = Visibility.Collapsed;
            _button_next.Visibility = Visibility.Collapsed;
        }

        /// <summary>
        /// Show one or more wizard buttons and hide the others
        /// </summary>
        /// <param name="buttons">Flag combination of wizard buttons which should be shown</param>
        public void ShowButtons(WizardButtons buttons)
        {
            _button_help.Visibility = (buttons & WizardButtons.Help) > 0 ? Visibility.Visible : Visibility.Collapsed;
            _button_prev.Visibility = (buttons & WizardButtons.Back) > 0 ? Visibility.Visible : Visibility.Collapsed;
            _button_next.Visibility = (buttons & WizardButtons.Next) > 0 ? Visibility.Visible : Visibility.Collapsed;
        }

        /// <summary>
        /// Show single wizard button
        /// </summary>
        /// <param name="button">Button which should be shown</param>
        public void ShowButton(WizardButtons button)
        {
            switch (button)
            {
                case WizardButtons.Next:
                    _button_next.Visibility = Visibility.Visible; break;
                case WizardButtons.Back:
                    _button_prev.Visibility = Visibility.Visible; break;
                case WizardButtons.Help:
                    _button_help.Visibility = Visibility.Visible; break;
            }
        }
        /// <summary>
        /// Hide single wizard button
        /// </summary>
        /// <param name="button">Button which should be hidden</param>
        public void HideButton(WizardButtons button)
        {
            switch (button)
            {
                case WizardButtons.Next:
                    _button_next.Visibility = Visibility.Collapsed; break;
                case WizardButtons.Back:
                    _button_prev.Visibility = Visibility.Collapsed; break;
                case WizardButtons.Help:
                    _button_help.Visibility = Visibility.Collapsed; break;
            }
        }
        /// <summary>
        /// Enable one or more wizard buttons and disables the rest
        /// </summary>
        /// <param name="buttons">Flag combination of buttons to enable</param>
        public void EnableButtons(WizardButtons buttons)
        {
            _button_help.IsEnabled = (buttons & WizardButtons.Help) > 0;
            _button_prev.IsEnabled = (buttons & WizardButtons.Back) > 0;
            _button_next.IsEnabled = (buttons & WizardButtons.Next) > 0;
        }
        /// <summary>
        /// Enable single wizard button
        /// </summary>
        /// <param name="button"></param>
        public void EnableButton(WizardButtons button)
        {
            switch (button)
            {
                case WizardButtons.Next:
                    _button_next.IsEnabled = true; break;
                case WizardButtons.Back:
                    _button_prev.IsEnabled = true; break;
                case WizardButtons.Help:
                    _button_help.IsEnabled = true; break;
            }
        }
        /// <summary>
        /// Disable single wizard button
        /// </summary>
        /// <param name="button"></param>
        public void DisableButton(WizardButtons button)
        {
            switch (button)
            {
                case WizardButtons.Next:
                    _button_next.IsEnabled = false; break;
                case WizardButtons.Back:
                    _button_prev.IsEnabled = false; break;
                case WizardButtons.Help:
                    _button_help.IsEnabled = false; break;
            }
        }
        /// <summary>
        /// Set the text of a wizard button
        /// </summary>
        /// <param name="button"></param>
        /// <param name="text"></param>
        public void SetButtonText(WizardButtons button, string text)
        {
            switch (button)
            {
                case WizardButtons.Next:
                    _button_next.Content = text; break;
                case WizardButtons.Back:
                    _button_prev.Content = text; break;
                case WizardButtons.Help:
                    _button_help.Content = text; break;
            }
        }
        /// <summary>
        /// Gets the text of a wizard button
        /// </summary>
        /// <param name="button"></param>
        /// <returns></returns>
        public string GetButtonText(WizardButtons button)
        {
            switch (button)
            {
                case WizardButtons.Next:
                    return (string)_button_next.Content;
                case WizardButtons.Back:
                    return (string)_button_prev.Content;
                case WizardButtons.Help:
                    return (string)_button_help.Content;
            }

            return "";
        }

        /// <summary>
        /// Setzt den Next button auf Finish
        /// </summary>
        public void SetFinishedButton()
        {
            SetFinishedButton("Fertigstellen");
        }
        /// <summary>
        /// Setzt den Next button auf Finish state mit custom text
        /// </summary>
        /// <param name="finishedText"></param>
        public void SetFinishedButton(string finishedText)
        {
            if (!_next_button_is_finish)
            {
                _next_button_text = GetButtonText(WizardButtons.Next);
                SetButtonText(WizardButtons.Next, finishedText);
                EnableButton(WizardButtons.Next);
                _next_button_is_finish = true;
            }
        }
        /// <summary>
        /// Setzt den Next button auf next state
        /// </summary>
        public void UnsetFinishedButton()
        {
            if (_next_button_is_finish)
            {
                SetButtonText(WizardButtons.Next, _next_button_text);
                _next_button_is_finish = false;
            }
        }

        #endregion

        #endregion

        #region Properties
        /// <summary>
        /// gets/sets the wizard custom data object
        /// </summary>
        public object WizardData
        {
            get { return _wizardData; }
            set { _wizardData = value; }
        }

        /// <summary>
        /// gets/Sets a flag indicating if the wizard data has changed
        /// </summary>
        public bool HasWizardDataChanged
        {
            get
            {
                return HasChanged;
            }
            set
            {
                if (value)
                    SetHasChanged();
                else
                    ClearHasChanged();
            }
        }
        #endregion

        #region Virtuelle methoden welche überschrieben werden können
        /// <summary>
        /// Wird aufgerufen, wenn der Benutzer einen der OverlayButtons klicked
        /// </summary>
        /// <param name="button"></param>
        public override bool OverlayButtonClicked(OverlayDialogButton button)
        {
            if( button == _button_next)
            {
                if(_next_button_is_finish)
                {
                    // finish    
                    if (_active_page == null)
                    {
#if DEBUG
                        Debug.WriteLine("_active_page = null - unsuspected behavior!");
#endif
                        throw new ApplicationException("Wizard Fehler: Unbekannte Seite.");
                    }

                    CallStackInfo lastInfo = GetLastCallStackInfo();
                    bool bcanFinish = false;

                    if (lastInfo == null)
                        bcanFinish = _active_page.Finish(null, NavigationSource.Direct);
                    else
                        bcanFinish = _active_page.Finish(lastInfo.page, lastInfo.source);

                    if (bcanFinish)
                    {
                        _wizard_result = WizardResult.Finished;
                        // FINISH - aktuellen dialog schliessen (nicht erzwingen)
                        if (OverlayManager != null)
                            if (!OverlayManager.HideContent(this, false))
                                _wizard_result = WizardResult.Canceled;
                    }

                } 
                else
                {
                    // next

                    if (_active_page == null)
                    {
#if DEBUG
                        Debug.WriteLine("_active_page = null - unsuspected behavior!");
#endif
                        throw new ApplicationException("Wizard Fehler: Unbekannte Seite.");
                    }

                    CallStackInfo lastInfo = GetLastCallStackInfo();
                    OverlayWizardPage nextPage = null;

                    if (lastInfo == null)
                        nextPage = _active_page.Next(null, NavigationSource.Direct);
                    else
                        nextPage = _active_page.Next(lastInfo.page, lastInfo.source);

                    if (nextPage != null)
                    {
                        // standard button behavior
                        int idx = _wizard_pages.IndexOf(nextPage);

                        if (_wizard_pages.Count > idx + 1)
                            EnableButton(WizardButtons.Next);
                        else
                        {
                            if (_wizard_pages.Count == idx + 1)
                                SetFinishedButton();
                            else
                                DisableButton(WizardButtons.Next);
                        }

                        if(idx > 0)
                        {
                            if (_wizard_pages.Count > 1)
                                EnableButton(WizardButtons.Back);
                        } 
                        else
                        {
                            DisableButton(WizardButtons.Back);
                        }

                        PutCallStackInfo(_active_page, NavigationSource.Next);
                        ShowPage(nextPage);

                        _active_page = nextPage;
                    }
                }
            }
            else if(button == _button_prev)
            {
                if(_active_page == null)
                {
#if DEBUG
                    Debug.WriteLine("_active_page = null - unsuspected behavior!");
#endif
                    throw new ApplicationException("Wizard Fehler: Unbekannte Seite.");
                }

                // prev
                CallStackInfo lastInfo = GetLastCallStackInfo();
                OverlayWizardPage nextPage = null;

                if (lastInfo == null)
                    nextPage = _active_page.Prev(null, NavigationSource.Direct);
                else
                    nextPage = _active_page.Prev(lastInfo.page, lastInfo.source);

                if(nextPage != null)
                {
                    if (_next_button_is_finish)
                        UnsetFinishedButton();
    
                    // standard button behavior
                    int idx = _wizard_pages.IndexOf(nextPage);
                    if (idx <= 0)
                    {
                        if(_wizard_pages.Count > idx+1)
                            EnableButton(WizardButtons.Next);

                        DisableButton(WizardButtons.Back);
                    }

                    PutCallStackInfo(_active_page, NavigationSource.Back);
                    ShowPage(nextPage);

                    _active_page = nextPage;
                }
            }
            else if(button == _button_help)
            {
                if(_active_page != null)
                {
                    _active_page.Help();
                }
            }

            return true;
        }

        /// <summary>
        /// Wurd aufgerufen bevor das Control angezeigt wird
        /// </summary>
        /// <param name="firstTime">true wenn dieser Content in der Dialoginstanz das erste Mal angezeigt wird</param>
        /// <returns>false wenn das control nicht angezeigt werden soll.</returns>
        public override bool BeforeShow(bool firstTime)
        {
            if(this.Content == null)
                this.Content = _page_host_grid;

            if (_active_page == null)
            {
                ShowPage(0);
                DisableButton(WizardButtons.Back);
            }

            return true;
        }
        /// <summary>
        /// Wird aufgerufen nachdem das Control angezeigt wurde
        /// </summary>
        public override void AfterShow()
        {
            
        }

        /// <summary>
        /// Wird aufgerufen bevor das control geschlossen wird
        /// </summary>
        /// <param name="forceClose">True wenn das Control auf jeden Fall geschlossen wird.</param>
        /// <returns>false wenn das Control nicht geschlossen werden kann.</returns>
        /// <remarks>Wenn <see cref="forceClose"/> True ist, dann wird der 
        /// Rückgabewert ignoriert und das Control auf jeden Fall geschlossen.</remarks>
        public override bool BeforeClose(bool forceClose)
        {
            bool bRet = true;
            if(!forceClose)
            {
                if (HasWizardDataChanged && (_wizard_result == WizardResult.Canceled) && !SaveChangesQuestionDisabled)
                {
                    IOverlayManagerBase oManager = OverlayManager as IOverlayManagerBase;

                    if (oManager != null)
                    {
                        oManager.ShowMessageBox("Änderungen gehen eventuell verloren!",
                                                "Möchten Sie den Dialog schliessen und Ihre Änderungen verwerfen?",
                                                MessageBoxButton.YesNo, MessageBoxImage.Question, SaveChangedAnswered);
                        bRet = false;
                    }
                }
            }

            return bRet;
        }

        private void SaveChangedAnswered(object sender, MessageBoxResultArgs e)
        {
            if(e.Result == MessageBoxResult.Yes)
            {
                 IOverlayManagerBase oManager = OverlayManager;

                 if (oManager != null)
                 {
                     oManager.HideContent(this, true);
                 }
            }
        }

        /// <summary>
        /// Wird aufgerufen nachdem das Control geschlossen wurde
        /// </summary>
        public override void AfterClose()
        {
            foreach (OverlayWizardPage page in _wizard_pages)
                page.Cancel();

            OnWizardFinished();

            base.AfterClose();
        }

        /// <summary>
        /// Wurd aufgerufen bevor das Control gedruckt wird wird
        /// </summary>
        /// <returns>false wenn das control nicht gedruckt werden soll.</returns>
        public override bool BeforePrint()
        {
            return true;
        }
        /// <summary>
        /// wird aufgerufen nachdem das Control gedruckt wurde
        /// </summary>
        public override void AfterPrint()
        {

        }

        /// <summary>
        /// Called if the wizard finishes
        /// </summary>
        protected virtual void OnWizardFinished()
        {
            if(WizardFinished != null)
                WizardFinished(this, new EventArgs());
        }

        /// <summary>
        /// Override in inherited wizards to enable/disable help support
        /// </summary>
        /// <returns></returns>
        protected virtual bool HelpSupported()
        {
            return false;
        }
        #endregion

        #region IOverlayContent Members

        public bool Initialize()
        {
            return true;
        }

        public bool Show(bool firstTime)
        {
            return true;
        }

        public bool Close(bool forceClose)
        {
            return true;
        }

        public bool Print()
        {
            return true;
        }

        public bool Help()
        {
            return true;
        }

        public bool CanPrint
        {
            get { return false; }
        }

        public bool CanClose
        {
            get
            {
                return true;
            }
        }

        public bool CanHelp
        {
            get
            {
                return HelpSupported();
            }
        }

        public bool AutoResized
        {
            get { return true; }
        }
        #endregion

        #region Properties
        /// <summary>
        /// gets/sets the page hosting grid control
        /// </summary>
        public Grid PageHostGrid
        {
            get { return _page_host_grid; }
            set { _page_host_grid = value; }
        }
        /// <summary>
        /// Gets the wizard result
        /// </summary>
        public WizardResult Result
        {
            get { return _wizard_result; }
        }

        /// <summary>
        /// Disables the save changes question
        /// </summary>
        public bool SaveChangesQuestionDisabled { get;set; }
        #endregion

        #region Attribs

        public event EventHandler WizardFinished;

        private OverlayDialogButton _button_prev = null;
        private OverlayDialogButton _button_next = null;
        private OverlayDialogButton _button_help = null;
        private string _next_button_text = "";
        private bool _next_button_is_finish = false;

        private Grid _page_host_grid = null;

        private List<OverlayWizardPage> _wizard_pages = new List<OverlayWizardPage>();
        private OverlayWizardPage _active_page = null;

        private object _wizardData = null;

        // call stack list
        private List<CallStackInfo> _page_call_stack = new List<CallStackInfo>();

        private WizardResult _wizard_result = WizardResult.Canceled;
        #endregion
    }
}
