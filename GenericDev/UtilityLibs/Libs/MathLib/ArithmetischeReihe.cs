﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MathLib
{
    public class ArithmetischeReihe
    {
        public static double GetPartialSumme(int n, double a0, double d)
        {
            double an = a0 + d*(n - 1);
            return n * (a0 + an) * 0.5;
        }

        public static float GetPartialSumme(int n, float a0, float d)
        {
            float an = a0 + d * (n - 1);
            return n * (a0 + an) * 0.5f;
        }
    }
}
