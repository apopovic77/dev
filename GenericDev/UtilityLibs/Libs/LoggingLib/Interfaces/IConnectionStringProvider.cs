﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LoggingLib.Interfaces
{
    /// <summary>
    /// interface description for providing a connection string
    /// </summary>
    public interface IConnectionStringProvider
    {
        /// <summary>
        /// Gets the connection string 
        /// </summary>
        string ConnectionString { get; }
    }
}