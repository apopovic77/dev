﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Logicx.WpfUtility.CustomControls.Validation
{
    /// <summary>
    /// Extended AdornedElementPlaceHolder which supports the TemplatedAdornerEx template
    /// </summary>
    public class AdornedElementPlaceholderEx : AdornedElementPlaceholder
    {
        #region Properties
        /// <summary>
        /// New AdornedElement implementation
        /// </summary>
        public new UIElement AdornedElement
        {
            get
            {
                if (this.TemplatedAdornerEx != null)
                {
                    return this.TemplatedAdornerEx.AdornedElement;
                }
                
                return base.AdornedElement;
            }
        }

        /// <summary>
        /// Gets the associated templated adorner
        /// </summary>
        private TemplatedAdornerEx TemplatedAdornerEx
        {
            get
            {
                if (this._templatedAdornerEx == null)
                {
                    FrameworkElement templatedParent = base.TemplatedParent as FrameworkElement;
                    if (templatedParent != null)
                    {
                        this._templatedAdornerEx = VisualTreeHelper.GetParent(templatedParent) as TemplatedAdornerEx;
                        if ((this._templatedAdornerEx != null) && (this._templatedAdornerEx.ReferenceElement == null))
                        {
                            this._templatedAdornerEx.ReferenceElement = this;
                        }
                    }
                }
                return this._templatedAdornerEx;
            }
        }
        #endregion

        #region Operations
        /// <summary>
        /// Measure override
        /// </summary>
        /// <param name="constraint"></param>
        /// <returns></returns>
        protected override Size MeasureOverride(Size constraint)
        {
            if (base.TemplatedParent == null)
            {
                throw new InvalidOperationException("Must be in template");
            }
            if (AdornedElement == null)
            {
                return new Size(0.0, 0.0);
            }
            Size renderSize = AdornedElement.RenderSize;
            UIElement child = Child;
            if (child != null)
            {
                child.Measure(renderSize);
            }
            return renderSize;
        }

        /// <summary>
        /// Arrange override
        /// </summary>
        /// <param name="arrangeBounds"></param>
        /// <returns></returns>
        protected override Size ArrangeOverride(Size arrangeBounds)
        {
            UIElement child = this.Child;
            if (child != null)
            {
                child.Arrange(new Rect(arrangeBounds));
            }
            return arrangeBounds;
        }
        #endregion

        #region atribs
        private TemplatedAdornerEx _templatedAdornerEx;
        #endregion
    }
}
