﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;

namespace Logicx.WpfUtility.CustomControls.HelpProvider
{
    public class HelpEventArgs : EventArgs
    {
        public HelpEventArgs(Visual req_help_visual)
        {
            RequestedHelpVisual = req_help_visual;
        }

        public readonly Visual RequestedHelpVisual;
    }
}
