﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Logicx.WpfUtility.Converters;
using Logicx.WpfUtility.CustomControls.OverlayManager;
using Logicx.WpfUtility.CustomControls.TouchButton;
using Logicx.WpfUtility.WindowManagement.Adorners;
using Logicx.WpfUtility.WpfHelpers;

namespace Logicx.WpfUtility.WindowManagement
{ 
    public interface IWindowTaskBarBase
    {
        double ActualHeight { get; }
        //IBaseFrameBase BaseFrame { get; }

        //ISubMenuPanelBase GetSubMenuPanelByTitle(string p);

        FrameworkElement GetSubMenuContainerByTitle(string p);

        Point GetElementPosition(FrameworkElement elem, Visual ref_visual);
    }

    public class WindowTaskBarBase<B, S, T, U, O, SU> : UserControl, IWindowTaskBarBase
        where S : SubMenuPanelBase<B, S, T, U, O, SU>, new()
        where B : BaseFrameBase<B, S, T, U, O, SU>
        where T : WindowTaskBarBase<B, S, T, U, O, SU>, new()
        where U : WindowTitleBarBase<B, S, T, U, O, SU>, new()
        where O : OverlayManagerBase<B, S, T, U, O, SU>, new()
        where SU : SubMenuPanelMenuItem
    {

        public class TaskbarButtonClickEventArgs : EventArgs
        {
            public TaskbarButtonClickEventArgs(string title)
            {
                ButtonTitle = title;
            }

            public string ButtonTitle;
        }

        public enum ItemType
        {
            FrameworkElement,
            Button,
            ToggleButton,
            SubMenuButton,
            TabButton
            //    ,
            //SubToolsButton
        }

        public enum ItemPlacmentPosition
        {
            Left,
            Center,
            Right
        }


        public WindowTaskBarBase()
        {
            string name = Assembly.GetAssembly(typeof(DoubleDevideByTwo)).GetName().Name;
    
            String xaml_string = @"<UserControl 
xmlns=""http://schemas.microsoft.com/winfx/2006/xaml/presentation""
xmlns:x=""http://schemas.microsoft.com/winfx/2006/xaml""            
xmlns:Taskbar=""clr-namespace:Logicx.WpfUtility.CustomControls.Taskbar;assembly=WpfUtilityLib"" 
xmlns:Combobox=""clr-namespace:Logicx.WpfUtility.CustomControls.Combobox;assembly=WpfUtilityLib"" 
xmlns:WindowManagement=""clr-namespace:Logicx.WpfUtility.WindowManagement;assembly=WpfUtilityLib"" xmlns:Converters=""clr-namespace:Logicx.WpfUtility.Converters;assembly=WpfUtilityLib"">
<UserControl.Triggers>
    <!--<EventTrigger RoutedEvent=""WindowManagement:WindowTaskBar.TapActive"" >
        <EventTrigger.Actions>
            <BeginStoryboard HandoffBehavior=""Compose"">
                <Storyboard>
                    <DoubleAnimation Storyboard.TargetName=""tab_item_grid""                                                     
                                                        Storyboard.TargetProperty=""(RenderTransform).(ScaleTransform.ScaleX)"" 
                                                        From=""0"" To=""1"" Duration=""00:00:00.400""  AccelerationRatio=""0.7"" DecelerationRatio=""0.3""/>
                    <DoubleAnimation Storyboard.TargetName=""tab_item_grid""                                                     
                                                        Storyboard.TargetProperty=""(RenderTransform).(ScaleTransform.ScaleY)"" 
                                                        From=""0"" To=""1"" Duration=""00:00:00.400"" AccelerationRatio=""0.7"" DecelerationRatio=""0.3"" Completed=""DoubleAnimation_Completed""  />
                </Storyboard>
            </BeginStoryboard>
        </EventTrigger.Actions>
    </EventTrigger>
    <EventTrigger RoutedEvent=""WindowManagement:WindowTaskBar.TapInActive"">
        <EventTrigger.Actions>
            <BeginStoryboard HandoffBehavior=""Compose"">
                <Storyboard>
                    <DoubleAnimation Storyboard.TargetName=""tab_item_grid""                                                     
                                                        Storyboard.TargetProperty=""(RenderTransform).(ScaleTransform.ScaleX)"" 
                                                        From=""1"" To=""0"" Duration=""00:00:00.400""  AccelerationRatio=""0.3"" DecelerationRatio=""0.7"" />
                    <DoubleAnimation Storyboard.TargetName=""tab_item_grid""                                                     
                                                        Storyboard.TargetProperty=""(RenderTransform).(ScaleTransform.ScaleY)"" 
                                                        From=""1"" To=""0"" Duration=""00:00:00.400""  AccelerationRatio=""0.3"" DecelerationRatio=""0.7"" Completed=""DoubleAnimation_Completed_1"" />
                </Storyboard>
            </BeginStoryboard>
        </EventTrigger.Actions>
    </EventTrigger>-->
</UserControl.Triggers>

<UserControl.Resources>
        
    <Converters:DoubleDevideByTwo x:Key=""divide_by_two""></Converters:DoubleDevideByTwo>

    <Style TargetType=""Button"" x:Key=""submenu_container_button_style"">
        <Setter Property=""OverridesDefaultStyle"" Value=""true""/>
        <Setter Property=""Control.Template"">
            <Setter.Value>
                <ControlTemplate TargetType=""Button"">
                    <Grid>
                        <Grid.ColumnDefinitions>
                            <ColumnDefinition Width=""Auto"" />
                        </Grid.ColumnDefinitions>
                        <Grid.RowDefinitions>
                            <RowDefinition Height=""Auto"" />
                        </Grid.RowDefinitions>
                        <ContentPresenter HorizontalAlignment=""Center"" VerticalAlignment=""Center"" ContentTemplate=""{TemplateBinding ContentControl.ContentTemplate}"" Content=""{TemplateBinding ContentControl.Content}"" />
                        
                        <Ellipse Name=""counter_ellipse"" Width=""17"" Height=""17"" Stretch=""Fill"" HorizontalAlignment=""Right"" VerticalAlignment=""Top"" Margin=""0,2,2,0"">
                            <Ellipse.Fill>
                                <RadialGradientBrush RadiusX=""1"" RadiusY=""1"" Center=""0,0.499998"" GradientOrigin=""0,0.499998"">
                                    <RadialGradientBrush.GradientStops>
                                        <GradientStop Color=""#FFFF0000"" Offset=""0""/>
                                        <GradientStop Color=""#FFFFC800"" Offset=""0.986425""/>
                                    </RadialGradientBrush.GradientStops>
                                </RadialGradientBrush>
                            </Ellipse.Fill>
                        </Ellipse>
                        <TextBlock Name=""text_count_elems"" Text=""{Binding InfoCounter}"" Foreground=""White""  FontSize=""14"" FontWeight=""Bold"" TextAlignment=""Center"" Width=""17"" Height=""17"" HorizontalAlignment=""Right"" VerticalAlignment=""Top"" Margin=""0,2,2,0"" />
                            
                        <Ellipse Name=""indicator_ellipse"" Width=""17"" Height=""17"" Stretch=""Fill"" HorizontalAlignment=""Left"" VerticalAlignment=""Top"" Margin=""0,2,2,0"" Visibility=""Hidden"" >
                            <Ellipse.Fill>
                                <RadialGradientBrush RadiusX=""1"" RadiusY=""1"" Center=""0,0.499998"" GradientOrigin=""0,0.499998"">
                                    <RadialGradientBrush.GradientStops>
                                        <GradientStop Color=""#FFFF0000"" Offset=""0""/>
                                        <GradientStop Color=""#FFFFC800"" Offset=""0.986425""/>
                                    </RadialGradientBrush.GradientStops>
                                </RadialGradientBrush>
                            </Ellipse.Fill>
                        </Ellipse>
                        <TextBlock Name=""text_indicator"" Text=""!"" Foreground=""White""  FontSize=""14"" FontWeight=""Bold"" TextAlignment=""Center"" Width=""17"" Height=""17"" HorizontalAlignment=""Left"" VerticalAlignment=""Top"" Margin=""0,2,2,0""  Visibility=""Hidden"" />
                            
                    </Grid>
                    <ControlTemplate.Triggers>
                      <DataTrigger Binding=""{Binding InfoCounter}"" Value=""{x:Null}"" >
                            <Setter TargetName=""counter_ellipse"" Property=""Visibility"" Value=""Hidden"" />
                            <Setter TargetName=""text_count_elems"" Property=""Visibility"" Value=""Hidden"" />
                        </DataTrigger>
                        <DataTrigger Binding=""{Binding InfoCounter}"" Value=""0"" >
                            <Setter TargetName=""counter_ellipse"" Property=""Visibility"" Value=""Hidden"" />
                            <Setter TargetName=""text_count_elems"" Property=""Visibility"" Value=""Hidden"" />
                        </DataTrigger>
                        <DataTrigger Binding=""{Binding InfoIndicator}"" Value=""true"" >
                            <Setter TargetName=""indicator_ellipse"" Property=""Visibility"" Value=""Visible"" />
                            <Setter TargetName=""text_indicator"" Property=""Visibility"" Value=""Visible"" />
                        </DataTrigger>
                    </ControlTemplate.Triggers>
                </ControlTemplate>
            </Setter.Value>
        </Setter>
        <Setter Property=""FocusVisualStyle""  Value=""{x:Null}""/>
    </Style>
        
    <Style TargetType=""ToggleButton"" x:Key=""submenu_container_togglebutton_style"">
        <Setter Property=""Background"" Value=""Transparent"" />
        <Setter Property=""BorderThickness"" Value=""0"" />
        <Setter Property=""Template"">
            <Setter.Value>
                <ControlTemplate TargetType=""ToggleButton"">
                    <Grid>
                        <Grid.ColumnDefinitions>
                            <ColumnDefinition Width=""Auto"" />
                        </Grid.ColumnDefinitions>
                        <Grid.RowDefinitions>
                            <RowDefinition Height=""Auto"" />
                        </Grid.RowDefinitions>
                        <ContentPresenter Name=""content""  />
                        <Border Name=""border"" Visibility=""Hidden"" 
                                    Background=""{Binding RelativeSource={RelativeSource AncestorType={x:Type UserControl}, AncestorLevel=2},Path=BaseFrame.AdornerActiveRenderBrush}""
                                    BorderBrush=""{Binding RelativeSource={RelativeSource AncestorType={x:Type UserControl}, AncestorLevel=2},Path=BaseFrame.HooverStrokeBrush}""
                                    BorderThickness=""0.1,0,0.1,0.1"">
                            <Path Width=""17.3922"" Height=""27.3334"" Stretch=""Fill"" StrokeLineJoin=""Round"" 
                                    Fill=""{Binding RelativeSource={RelativeSource AncestorType={x:Type UserControl}, AncestorLevel=2},Path=BaseFrame.AdornerActiveRenderBrush}""
                                    Stroke=""{Binding RelativeSource={RelativeSource AncestorType={x:Type UserControl}, AncestorLevel=2},Path=BaseFrame.HooverStrokeBrush}""
                                    StrokeThickness=""0.5""
                                    Data=""F1 M 534.618,535.378L 544.285,535.378L 544.285,551.378L 547.647,551.378L 539.451,561.711L 531.255,551.378L 534.618,551.378L 534.618,535.378 Z ""/>
                        </Border>

                        <Ellipse Name=""counter_ellipse"" Width=""17"" Height=""17"" Stretch=""Fill"" HorizontalAlignment=""Right"" VerticalAlignment=""Top"" Margin=""0,2,2,0"">
                            <Ellipse.Fill>
                                <RadialGradientBrush RadiusX=""1"" RadiusY=""1"" Center=""0,0.499998"" GradientOrigin=""0,0.499998"">
                                    <RadialGradientBrush.GradientStops>
                                        <GradientStop Color=""#FFFF0000"" Offset=""0""/>
                                        <GradientStop Color=""#FFFFC800"" Offset=""0.986425""/>
                                    </RadialGradientBrush.GradientStops>
                                </RadialGradientBrush>
                            </Ellipse.Fill>
                        </Ellipse>
                        <TextBlock Name=""text_count_elems"" Text=""{Binding ItemCount}"" Foreground=""White""  FontSize=""14"" FontWeight=""Bold"" TextAlignment=""Center"" Width=""17"" Height=""17"" HorizontalAlignment=""Right"" VerticalAlignment=""Top"" Margin=""0,2,2,0"" />
                            
                        <Ellipse Name=""indicator_ellipse"" Width=""17"" Height=""17"" Stretch=""Fill"" HorizontalAlignment=""Left"" VerticalAlignment=""Top"" Margin=""0,2,2,0"" Visibility=""Hidden"" >
                            <Ellipse.Fill>
                                <RadialGradientBrush RadiusX=""1"" RadiusY=""1"" Center=""0,0.499998"" GradientOrigin=""0,0.499998"">
                                    <RadialGradientBrush.GradientStops>
                                        <GradientStop Color=""#FFFF0000"" Offset=""0""/>
                                        <GradientStop Color=""#FFFFC800"" Offset=""0.986425""/>
                                    </RadialGradientBrush.GradientStops>
                                </RadialGradientBrush>
                            </Ellipse.Fill>
                        </Ellipse>
                        <TextBlock Name=""text_indicator"" Text=""!"" Foreground=""White""  FontSize=""14"" FontWeight=""Bold"" TextAlignment=""Center"" Width=""17"" Height=""17"" HorizontalAlignment=""Left"" VerticalAlignment=""Top"" Margin=""0,2,2,0""  Visibility=""Hidden"" />
                            
                    </Grid>
                    <ControlTemplate.Triggers>
                        <DataTrigger Binding=""{Binding ItemCount}"" Value=""0"" >
                            <Setter TargetName=""counter_ellipse"" Property=""Visibility"" Value=""Hidden"" />
                            <Setter TargetName=""text_count_elems"" Property=""Visibility"" Value=""Hidden"" />
                        </DataTrigger>
                        <DataTrigger Binding=""{Binding WithShowItemCount}"" Value=""false"" >
                            <Setter TargetName=""counter_ellipse"" Property=""Visibility"" Value=""Hidden"" />
                            <Setter TargetName=""text_count_elems"" Property=""Visibility"" Value=""Hidden"" />
                        </DataTrigger>
                        <DataTrigger Binding=""{Binding ShowInfoIndicator}"" Value=""true"" >
                            <Setter TargetName=""indicator_ellipse"" Property=""Visibility"" Value=""Visible"" />
                            <Setter TargetName=""text_indicator"" Property=""Visibility"" Value=""Visible"" />
                        </DataTrigger>
                        <Trigger Property=""IsChecked"" Value=""true"">
                            <Setter TargetName=""border"" Property=""Visibility"" Value=""Visible"" />
                            <Setter TargetName=""content"" Property=""Opacity"" Value=""0.5"" />
                        </Trigger>
                    </ControlTemplate.Triggers>
                </ControlTemplate>
            </Setter.Value>
        </Setter>
    </Style>

    <Style TargetType=""ToggleButton"" x:Key=""tab_togglebutton_style"">
        <Setter Property=""Background"" Value=""Transparent"" />
        <Setter Property=""BorderThickness"" Value=""0"" />
        <Setter Property=""Template"">
            <Setter.Value>
                <ControlTemplate TargetType=""ToggleButton"">
                    <Grid>
                        <Grid.ColumnDefinitions>
                            <ColumnDefinition Width=""Auto"" />
                        </Grid.ColumnDefinitions>
                        <Grid.RowDefinitions>
                            <RowDefinition Height=""Auto"" />
                        </Grid.RowDefinitions>
                        <ContentPresenter Name=""content""  />
                        <Border Name=""border"" Visibility=""Hidden"" 
                                    Background=""{Binding RelativeSource={RelativeSource AncestorType={x:Type UserControl}, AncestorLevel=2},Path=BaseFrame.AdornerActiveRenderBrush}""
                                    BorderBrush=""{Binding RelativeSource={RelativeSource AncestorType={x:Type UserControl}, AncestorLevel=2},Path=BaseFrame.HooverStrokeBrush}""
                                    BorderThickness=""0.1,0,0.1,0.1"">
                            <Path Width=""17.3922"" Height=""27.3334"" Stretch=""Fill"" StrokeLineJoin=""Round"" 
                                    Fill=""{Binding RelativeSource={RelativeSource AncestorType={x:Type UserControl}, AncestorLevel=2},Path=BaseFrame.AdornerActiveRenderBrush}""
                                    Stroke=""{Binding RelativeSource={RelativeSource AncestorType={x:Type UserControl}, AncestorLevel=2},Path=BaseFrame.HooverStrokeBrush}""
                                    StrokeThickness=""0.5""
                                    Data=""F1 M 534.618,535.378L 544.285,535.378L 544.285,551.378L 547.647,551.378L 539.451,561.711L 531.255,551.378L 534.618,551.378L 534.618,535.378 Z ""/>
                        </Border>

                        <Ellipse Name=""counter_ellipse"" Width=""17"" Height=""17"" Stretch=""Fill"" HorizontalAlignment=""Right"" VerticalAlignment=""Top"" Margin=""0,2,2,0"">
                            <Ellipse.Fill>
                                <RadialGradientBrush RadiusX=""1"" RadiusY=""1"" Center=""0,0.499998"" GradientOrigin=""0,0.499998"">
                                    <RadialGradientBrush.GradientStops>
                                        <GradientStop Color=""#FFFF0000"" Offset=""0""/>
                                        <GradientStop Color=""#FFFFC800"" Offset=""0.986425""/>
                                    </RadialGradientBrush.GradientStops>
                                </RadialGradientBrush>
                            </Ellipse.Fill>
                        </Ellipse>
                        <TextBlock Name=""text_count_elems"" Text=""{Binding InfoCounter}"" Foreground=""White""  FontSize=""14"" FontWeight=""Bold"" TextAlignment=""Center"" Width=""17"" Height=""17"" HorizontalAlignment=""Right"" VerticalAlignment=""Top"" Margin=""0,2,2,0"" />
                            
                        <Ellipse Name=""indicator_ellipse"" Width=""17"" Height=""17"" Stretch=""Fill"" HorizontalAlignment=""Left"" VerticalAlignment=""Top"" Margin=""0,2,2,0"" Visibility=""Hidden"" >
                            <Ellipse.Fill>
                                <RadialGradientBrush RadiusX=""1"" RadiusY=""1"" Center=""0,0.499998"" GradientOrigin=""0,0.499998"">
                                    <RadialGradientBrush.GradientStops>
                                        <GradientStop Color=""#FFFF0000"" Offset=""0""/>
                                        <GradientStop Color=""#FFFFC800"" Offset=""0.986425""/>
                                    </RadialGradientBrush.GradientStops>
                                </RadialGradientBrush>
                            </Ellipse.Fill>
                        </Ellipse>
                        <TextBlock Name=""text_indicator"" Text=""!"" Foreground=""White""  FontSize=""14"" FontWeight=""Bold"" TextAlignment=""Center"" Width=""17"" Height=""17"" HorizontalAlignment=""Left"" VerticalAlignment=""Top"" Margin=""0,2,2,0""  Visibility=""Hidden"" />
                         
                    </Grid>
                    <ControlTemplate.Triggers>
                      <DataTrigger Binding=""{Binding InfoCounter}"" Value=""{x:Null}"" >
                            <Setter TargetName=""counter_ellipse"" Property=""Visibility"" Value=""Hidden"" />
                            <Setter TargetName=""text_count_elems"" Property=""Visibility"" Value=""Hidden"" />
                        </DataTrigger>
                        <DataTrigger Binding=""{Binding InfoCounter}"" Value=""0"" >
                            <Setter TargetName=""counter_ellipse"" Property=""Visibility"" Value=""Hidden"" />
                            <Setter TargetName=""text_count_elems"" Property=""Visibility"" Value=""Hidden"" />
                        </DataTrigger>
                        <DataTrigger Binding=""{Binding InfoIndicator}"" Value=""true"" >
                            <Setter TargetName=""indicator_ellipse"" Property=""Visibility"" Value=""Visible"" />
                            <Setter TargetName=""text_indicator"" Property=""Visibility"" Value=""Visible"" />
                        </DataTrigger>
                        <Trigger Property=""IsChecked"" Value=""true"">
                            <Setter TargetName=""border"" Property=""Visibility"" Value=""Visible"" />
                            <Setter TargetName=""content"" Property=""Opacity"" Value=""0.5"" />
                        </Trigger>
                    </ControlTemplate.Triggers>
                </ControlTemplate>
            </Setter.Value>
        </Setter>
    </Style>
        
</UserControl.Resources>

<StackPanel>

    <Grid>
        <Grid Name=""submenu_base_grid"">
        </Grid>
        <Grid Name=""subtools_base_grid"">
        </Grid>
    </Grid>

    <Border Name=""base_border"" BorderThickness=""0,1,0,0"" BorderBrush=""{Binding RelativeSource={RelativeSource AncestorType={x:Type UserControl}, AncestorLevel=2},Path=BorderBrush}"" Height=""{Binding RelativeSource={RelativeSource AncestorType={x:Type UserControl}, AncestorLevel=2},Path=BarHeight}"">

        <Grid Name=""base_grid"">
            <Canvas Name=""item_canv"" Width=""{Binding ElementName=item_panel_grid, Path=ActualWidth}"" Height=""{Binding ElementName=item_panel_grid, Path=ActualHeight}"" HorizontalAlignment=""Center"">
                <Grid Name=""item_panel_grid"" Width=""{Binding ElementName=base_grid, Path=ActualWidth}"" Height=""{Binding RelativeSource={RelativeSource AncestorType={x:Type UserControl}, AncestorLevel=2},Path=BarHeight}"">
                    <Grid.ColumnDefinitions>
                        <ColumnDefinition Width=""Auto""/>
                        <ColumnDefinition Width=""*""/>
                        <ColumnDefinition Width=""Auto""/>
                    </Grid.ColumnDefinitions>

                    <StackPanel Margin=""5,0,5,0"" Grid.Column=""0"" Orientation=""Horizontal"" HorizontalAlignment=""Left"" Name=""item_panel_stack_left"" Height=""{Binding RelativeSource={RelativeSource AncestorType={x:Type UserControl}, AncestorLevel=2},Path=BarHeight}"">
                    </StackPanel>

                    <StackPanel Grid.Column=""1"" Orientation=""Horizontal"" HorizontalAlignment=""Center"" Name=""item_panel_stack_center"" Height=""{Binding RelativeSource={RelativeSource AncestorType={x:Type UserControl}, AncestorLevel=2},Path=BarHeight}"">
                    </StackPanel>

                    <StackPanel Margin=""5,0,5,0"" Grid.Column=""2"" Orientation=""Horizontal"" HorizontalAlignment=""Right"" Name=""item_panel_stack_right"" Height=""{Binding RelativeSource={RelativeSource AncestorType={x:Type UserControl}, AncestorLevel=2},Path=BarHeight}"">
                    </StackPanel>
                </Grid>
                    

                <Grid Name=""tab_item_grid"">
                    <Grid.RenderTransform>
                        <ScaleTransform ScaleX=""0"" ScaleY=""0"" CenterX=""{Binding ElementName=tab_item_grid, Path=ActualWidth, Converter={StaticResource divide_by_two}}"" CenterY=""{Binding ElementName=tab_item_grid, Path=ActualHeight}""></ScaleTransform>
                    </Grid.RenderTransform>   
                </Grid>
            </Canvas>
                
            <Grid Name=""user_info_grid"" HorizontalAlignment=""Right"" VerticalAlignment=""Bottom"" Height=""{Binding RelativeSource={RelativeSource AncestorType={x:Type UserControl}, AncestorLevel=2},Path=BarHeight}"" Visibility=""Hidden"">
                <StackPanel HorizontalAlignment=""Right"" VerticalAlignment=""Center""  Margin=""3"" >
                    <TextBlock Name=""app_info_line1"" TextAlignment=""Right"" FontSize=""10"" Width=""Auto"" Visibility=""Collapsed"" Foreground=""Gray"" Margin=""0""></TextBlock>
                    <TextBlock Name=""app_info_line2"" TextAlignment=""Right"" FontSize=""10"" Width=""Auto"" Visibility=""Collapsed"" Foreground=""Gray"" Margin=""0""></TextBlock>
                    <TextBlock TextAlignment=""Right"" FontSize=""4"" Width=""Auto"" Margin=""0""> </TextBlock>
                    <TextBlock Name=""user_info_line1"" TextAlignment=""Right"" FontSize=""14"" FontWeight=""Bold"" Width=""Auto"" Visibility=""Collapsed"" Foreground=""{Binding RelativeSource={RelativeSource AncestorType={x:Type UserControl}, AncestorLevel=2},Path=BaseFrame.DefaultForegroundBrush}"" Margin=""0""></TextBlock>
                    <TextBlock Name=""user_info_line2"" TextAlignment=""Right"" FontSize=""10"" Width=""Auto"" Visibility=""Collapsed"" Foreground=""{Binding RelativeSource={RelativeSource AncestorType={x:Type UserControl}, AncestorLevel=2},Path=BaseFrame.DefaultForegroundBrush}"" Margin=""0""></TextBlock>
                </StackPanel>
            </Grid>
        </Grid>
    </Border>

</StackPanel>

</UserControl>";

            try
            {
                this.Content = XamlReader.Parse(xaml_string);
            }catch(Exception ex)
            {
                
            }

            _items = new List<TaskbarItemBase<B, S, T, U, O, SU>>();

            this.TapActive += WindowTaskBar_TapActive;
            this.TapInActive += WindowTaskBar_TapInActive;
        }

        public Border BaseBorder
        {
            get
            {
                return (Border)((UserControl)Content).FindName("base_border");
            }
        }

        public Grid TabItemGrid
        {
            get
            {
                return (Grid)((UserControl)Content).FindName("tab_item_grid");
            }
        }

        public Grid SubMenuBaseGrid
        {
            get
            {
                return (Grid)((UserControl)Content).FindName("submenu_base_grid");
            }
        }

        public Canvas ItemCanv
        {
            get
            {
                return (Canvas)((UserControl)Content).FindName("item_canv");
            }
        }

        public Grid UserInfoGrid
        {
            get
            {
                return (Grid)((UserControl)Content).FindName("user_info_grid");
            }
        }

        public TextBlock AppInfoLine1
        {
            get
            {
                return (TextBlock)((UserControl)Content).FindName("app_info_line1");
            }
        }


        public TextBlock AppInfoLine2
        {
            get
            {
                return (TextBlock)((UserControl)Content).FindName("app_info_line2");
            }
        }

        public TextBlock UserInfoLine1
        {
            get
            {
                return (TextBlock)((UserControl)Content).FindName("user_info_line1");
            }
        }

        public TextBlock UserInfoLine2
        {
            get
            {
                return (TextBlock)((UserControl)Content).FindName("user_info_line2");
            }
        }

        public StackPanel ItemPanelStackCenter
        {
            get
            {
                return (StackPanel)((UserControl)Content).FindName("item_panel_stack_center");
            }
        }

        public StackPanel ItemPanelStackLeft
        {
            get
            {
                return (StackPanel)((UserControl)Content).FindName("item_panel_stack_left");
            }
        }

        public StackPanel ItemPanelStackRight
        {
            get
            {
                return (StackPanel)((UserControl)Content).FindName("item_panel_stack_right");
            }
        }

        #region Add and Remove Items
        public void AddItem(WindowTaskBarBase<B,S,T,U,O,SU>.ItemType item_type, FrameworkElement framework_element, string title, Style container_border_style, Style text_style)
        {
            AddItem(item_type, framework_element, title, container_border_style, text_style, ItemPanelStackCenter.Children.Count, WindowTaskBarBase<B, S, T, U, O, SU>.ItemPlacmentPosition.Center);
        }

        public void AddItem(WindowTaskBarBase<B, S, T, U, O, SU>.ItemType item_type, FrameworkElement framework_element, string title, Style container_border_style, Style text_style, int insert_position)
        {
            AddItem(item_type, framework_element, title, container_border_style, text_style, insert_position, WindowTaskBarBase<B, S, T, U, O, SU>.ItemPlacmentPosition.Center);
        }


        public void AddItem(WindowTaskBarBase<B, S, T, U, O, SU>.ItemType item_type, FrameworkElement framework_element, string title, Style container_border_style, Style text_style, WindowTaskBarBase<B, S, T, U, O, SU>.ItemPlacmentPosition position)
        {
            int nposition = 0;

            switch (position)
            {
                case WindowTaskBarBase<B, S, T, U, O, SU>.ItemPlacmentPosition.Left:
                    nposition = ItemPanelStackLeft.Children.Count;
                    break;
                case WindowTaskBarBase<B, S, T, U, O, SU>.ItemPlacmentPosition.Center:
                    nposition = ItemPanelStackCenter.Children.Count;
                    break;
                case WindowTaskBarBase<B, S, T, U, O, SU>.ItemPlacmentPosition.Right:
                    nposition = ItemPanelStackRight.Children.Count;
                    break;
            }
            AddItem(item_type, framework_element, title, container_border_style, text_style, nposition, position);
        }

        public void AddItem(WindowTaskBarBase<B, S, T, U, O, SU>.ItemType item_type, FrameworkElement framework_element, string title, Style container_border_style, Style text_style, int insert_position, WindowTaskBarBase<B, S, T, U, O, SU>.ItemPlacmentPosition position)
        {
            TaskbarItemBase<B, S, T, U, O, SU> item = new TaskbarItemBase<B, S, T, U, O, SU>(framework_element, title);
            _items.Add(item);
            item.ItemBorder.Tag = item;
            item.ItemType = item_type;
            item.ContainerBorderStyle = container_border_style;
            item.TextStyle = text_style;

            FrameworkElement taskbar_container = GetTaskBarContainer(item_type, item);
            item.Container = taskbar_container;

            if (taskbar_container.Tag is S)
            {
                ((S)taskbar_container.Tag).TaskbarItemName = title;
            }

            switch (position)
            {
                case WindowTaskBarBase<B, S, T, U, O, SU>.ItemPlacmentPosition.Left:
                    ItemPanelStackLeft.Children.Insert(insert_position, taskbar_container);
                    break;
                case WindowTaskBarBase<B, S, T, U, O, SU>.ItemPlacmentPosition.Center:
                    ItemPanelStackCenter.Children.Insert(insert_position, taskbar_container);
                    break;
                case WindowTaskBarBase<B, S, T, U, O, SU>.ItemPlacmentPosition.Right:
                    ItemPanelStackRight.Children.Insert(insert_position, taskbar_container);
                    break;
            }


            if (ItemAdded != null)
                ItemAdded(this, new EventArgs());

            ActiveStyleAdornerManager.Instance.Register(item, BaseFrame.AdornerActiveRenderBrush, BaseFrame.AdornerActiveRenderPen);

            //if (WithSpreadMenuManagement)
            //{
            //    if (framework_element is SingleSpreadMenu)
            //    {
            //        SingleSpreadMenu menu = (SingleSpreadMenu)framework_element;
            //        menu.ToggleButton.Checked += new RoutedEventHandler(ToggleButton_Checked);
            //        menu.ToggleButton.Unchecked += new RoutedEventHandler(ToggleButton_Unchecked);
            //    }
            //    else if (framework_element is PointerWrapPanelButton)
            //    {
            //        PointerWrapPanelButton menu = (PointerWrapPanelButton)framework_element;
            //        menu.ToggleButton.Checked += new RoutedEventHandler(ToggleButton_Checked);
            //        menu.ToggleButton.Unchecked += new RoutedEventHandler(ToggleButton_Unchecked);
            //    }
            //}
        }

        public void SetApplicationInfo(string app_info1, string app_info2)
        {
            if (!string.IsNullOrEmpty(app_info1))
            {
                AppInfoLine1.Text = app_info1;
                AppInfoLine1.Visibility = Visibility.Visible;
                UserInfoGrid.Visibility = Visibility.Visible;
            }
            else
            {
                AppInfoLine1.Visibility = Visibility.Collapsed;
            }

            if (!string.IsNullOrEmpty(app_info2))
            {
                AppInfoLine2.Text = app_info2;
                AppInfoLine2.Visibility = Visibility.Visible;
                UserInfoGrid.Visibility = Visibility.Visible;
            }
            else
            {
                AppInfoLine2.Visibility = Visibility.Collapsed;
            }
        }

        public void SetUserInfo(string user_info1, string user_info2)
        {
            if (!string.IsNullOrEmpty(user_info1))
            {
                UserInfoLine1.Text = user_info1;
                UserInfoLine1.Visibility = Visibility.Visible;
                UserInfoGrid.Visibility = Visibility.Visible;
            }
            else
            {
                UserInfoLine1.Visibility = Visibility.Collapsed;
            }

            if (!string.IsNullOrEmpty(user_info2))
            {
                UserInfoLine2.Text = user_info2;
                UserInfoLine2.Visibility = Visibility.Visible;
                UserInfoGrid.Visibility = Visibility.Visible;
            }
            else
            {
                UserInfoLine2.Visibility = Visibility.Collapsed;
            }
        }

        private FrameworkElement GetTaskBarContainer(WindowTaskBarBase<B, S, T, U, O, SU>.ItemType item_type, FrameworkElement base_elem)
        {
            switch (item_type)
            {
                case WindowTaskBarBase<B, S, T, U, O, SU>.ItemType.FrameworkElement:
                    return base_elem;
                case WindowTaskBarBase<B, S, T, U, O, SU>.ItemType.Button:
                    TouchButton b_container = new TouchButton();
                    b_container.Style = (Style)((UserControl)Content).FindResource("submenu_container_button_style");
                    b_container.Content = base_elem;
                    b_container.DataContext = base_elem;
                    b_container.Click += new RoutedEventHandler(b_container_Click);
                    return b_container;
                case WindowTaskBarBase<B, S, T, U, O, SU>.ItemType.ToggleButton:
                    TouchToggleButton tb_container = new TouchToggleButton();
                    tb_container.Content = base_elem;
                    tb_container.DataContext = base_elem;
                    tb_container.Checked += new RoutedEventHandler(tb_container_Checked);
                    tb_container.Unchecked += new RoutedEventHandler(tb_container_Unchecked);
                    return tb_container;
                case WindowTaskBarBase<B, S, T, U, O, SU>.ItemType.SubMenuButton:
                    TouchToggleButton stb_container = new TouchToggleButton();
                    stb_container.Style = (Style)((UserControl)Content).FindResource("submenu_container_togglebutton_style");
                    stb_container.Content = base_elem;
                    stb_container.Tag = CreateSubMenuPanel(stb_container);
                    stb_container.DataContext = stb_container.Tag;
                    stb_container.Checked += new RoutedEventHandler(stb_container_Checked);
                    stb_container.Unchecked += new RoutedEventHandler(stb_container_Unchecked);
                    return stb_container;
                case WindowTaskBarBase<B, S, T, U, O, SU>.ItemType.TabButton:
                    TouchToggleButton tab_container = new TouchToggleButton();
                    tab_container.Style = (Style)((UserControl)Content).FindResource("tab_togglebutton_style");
                    tab_container.Content = base_elem;
                    tab_container.Tag = TabItemGrid;
                    tab_container.DataContext = base_elem;
                    tab_container.Checked += new RoutedEventHandler(tab_container_Checked);
                    tab_container.Unchecked += new RoutedEventHandler(tab_container_Unchecked);
                    return tab_container;
                //case WindowTaskBarBase<B, S, T, U, O, SU>.ItemType.SubToolsButton:
                //    TouchToggleButton tabtools_container = new TouchToggleButton();
                //    tabtools_container.Style = (Style)((UserControl)Content).FindResource("tab_togglebutton_style");
                //    tabtools_container.Content = base_elem;
                //    tabtools_container.Tag = CreateSubToolsPanel(tabtools_container);;
                //    tabtools_container.Checked += new RoutedEventHandler(tab_container_Checked);
                //    tabtools_container.Unchecked += new RoutedEventHandler(tab_container_Unchecked);
                //    return tabtools_container;

                default:
                    throw new Exception("unknown item type");
            }
        }

        public void RemoveItem(FrameworkElement framework_element)
        {
            TaskbarItemBase<B, S, T, U, O, SU> item = framework_element.Tag as TaskbarItemBase<B, S, T, U, O, SU>;
            if (item == null)
                return;

            if (!ItemPanelStackLeft.Children.Contains(item) &&
                !ItemPanelStackCenter.Children.Contains(item) &&
                !ItemPanelStackRight.Children.Contains(item))
                return;

            ActiveStyleAdornerManager.Instance.DeRegister(item);

            //start hide anim
            throw new NotImplementedException();

        }

        public FrameworkElement AddSeparator()
        {
            return AddSeparator(WindowTaskBar.ItemPlacmentPosition.Center);
        }

        public FrameworkElement AddSeparator(WindowTaskBar.ItemPlacmentPosition position)
        {
            Rectangle rect = new Rectangle();
            rect.Width = 1;
            rect.Height = 25;
            rect.Margin = new Thickness(10, 0, 10, 12);
            rect.Fill = BaseFrame.DefaultForegroundBrush;
            rect.VerticalAlignment = VerticalAlignment.Center;

            switch (position)
            {
                case WindowTaskBar.ItemPlacmentPosition.Left: ItemPanelStackLeft.Children.Add(rect); break;
                case WindowTaskBar.ItemPlacmentPosition.Center: ItemPanelStackCenter.Children.Add(rect); break;
                case WindowTaskBar.ItemPlacmentPosition.Right: ItemPanelStackRight.Children.Add(rect); break;
            }


            return rect;
        }

        public void HidePanel(string title)
        {
            ToggleButton tbutton = GetTabContainerByTitle(title) as ToggleButton;

            if (tbutton != null)
            {
                tbutton.IsChecked = false;
            }
            else
            {
                tbutton = GetSubMenuContainerByTitle(title) as ToggleButton;

                if (tbutton != null)
                {
                    tbutton.IsChecked = false;
                }
                else
                {
                    S sub_menu_panel = GetSubMenuPanelByTitle(title);
                    if (sub_menu_panel != null)
                        sub_menu_panel.StartFadeOut();
                }
            }
        }

        public void ShowPanel(string title)
        {
            DateTime now = DateTime.Now;
            Debug.WriteLine("SHOW " + now.Second + ":" + now.Millisecond);

            S sub_menu_panel = GetSubMenuPanelByTitle(title);

            S curr_active_sub_menu_panel = ActiveSubMenuPanel;

            if (curr_active_sub_menu_panel != sub_menu_panel)
            {
                ToggleButton tbutton = GetTabContainerByTitle(title) as ToggleButton;

                if (tbutton == null)
                {
                    tbutton = GetSubMenuContainerByTitle(title) as ToggleButton;

                    if (tbutton != null)
                    {
                        tbutton.IsChecked = true;
                    }
                    else
                    {
                        if (curr_active_sub_menu_panel == null)
                        {
                            if (sub_menu_panel != null)
                                sub_menu_panel.StartFadeIn();
                        }
                        else
                        {
                            _tobeshow_panel = sub_menu_panel;
                            curr_active_sub_menu_panel.FadeOutCompleted += new EventHandler(curr_active_sub_menu_panel_FadeOutCompleted);
                            curr_active_sub_menu_panel.StartFadeOut();
                        }
                    }
                }
            }
        }

        void curr_active_sub_menu_panel_FadeOutCompleted(object sender, EventArgs e)
        {
            ((S)sender).FadeOutCompleted -= curr_active_sub_menu_panel_FadeOutCompleted;
            if (_tobeshow_panel != null)
            {
                _tobeshow_panel.StartFadeIn();
                _tobeshow_panel = null;
            }
        }

        public void HideItem(string title)
        {
            TaskbarItemBase<B, S, T, U, O, SU> item = GetTaskbarItemByTitle(title);

            if (item != null)
                item.Visibility = System.Windows.Visibility.Collapsed;

            if (item != null && item.Container != null)
                item.Container.Visibility = System.Windows.Visibility.Collapsed;

            S panel = GetSubMenuPanelByTitle(title);
            if (panel != null)
            {
                panel.StartFadeOut();
            }
        }

        public void ShowItem(string title)
        {
            TaskbarItemBase<B, S, T, U, O, SU> item = GetTaskbarItemByTitle(title);

            if (item != null)
                item.Visibility = System.Windows.Visibility.Visible;
            if (item != null && item.Container != null)
                item.Container.Visibility = System.Windows.Visibility.Visible;
        }

        public List<S> GetSubMenuPanels()
        {
            List<TaskbarItemBase<B, S, T, U, O, SU>> taskbar_items = _items.Where(i => i.ItemType == WindowTaskBarBase<B, S, T, U, O, SU>.ItemType.SubMenuButton).ToList();

            if (taskbar_items == null || taskbar_items.Count <= 0)
                return new List<S>();

            List<S> retList = new List<S>();
            foreach (TaskbarItemBase<B, S, T, U, O, SU> item in taskbar_items)
            {
                if (item.Container.Tag is S && !retList.Contains((S)item.Container.Tag))
                    retList.Add((S)item.Container.Tag);
            }

            return retList;
        }
        public S GetSubMenuPanelByFramworkElement(FrameworkElement element)
        {
            TaskbarItemBase<B, S, T, U, O, SU> taskbar_item = _items.Where(i => i.FrameworkElement == element && i.ItemType == WindowTaskBarBase<B, S, T, U, O, SU>.ItemType.SubMenuButton).SingleOrDefault();

            if (taskbar_item == null)
                return null;

            S sub_menu_panel = (S)taskbar_item.Container.Tag;
            return sub_menu_panel;
        }

        public TaskbarItemBase<B, S, T, U, O, SU> GetTaskbarItemByTitle(string title)
        {
            TaskbarItemBase<B, S, T, U, O, SU> taskbar_item = _items.Where(i => i.Title == title && (i.ItemType == WindowTaskBarBase<B, S, T, U, O, SU>.ItemType.SubMenuButton || i.ItemType == WindowTaskBarBase<B, S, T, U, O, SU>.ItemType.Button)).SingleOrDefault();

            return taskbar_item;
        }

        public string GetTitleForSubmenuPanel(S panel)
        {
            TaskbarItemBase<B, S, T, U, O, SU> taskbar_item = _items.Where(i => i.Container != null && i.Container.Tag == panel).SingleOrDefault();
            if (taskbar_item == null)
                return string.Empty;

            return taskbar_item.Title;
        }

        public S GetSubMenuPanelByTitle(string title)
        {
            TaskbarItemBase<B, S, T, U, O, SU> taskbar_item = _items.Where(i => i.Title == title && i.ItemType == WindowTaskBarBase<B, S, T, U, O, SU>.ItemType.SubMenuButton).SingleOrDefault();

            if (taskbar_item == null)
                return null;

            S sub_menu_panel = (S)taskbar_item.Container.Tag;
            return sub_menu_panel;
        }

        public FrameworkElement GetSubMenuContainerByTitle(string title)
        {
            TaskbarItemBase<B, S, T, U, O, SU> taskbar_item = _items.Where(i => i.Title == title && i.ItemType == WindowTaskBarBase<B, S, T, U, O, SU>.ItemType.SubMenuButton).SingleOrDefault();

            if (taskbar_item == null)
                return null;

            return taskbar_item.Container;
        }
        
        public Canvas GetTabItemCanvasByTitle(string title)
        {
            TaskbarItemBase<B, S, T, U, O, SU> taskbar_item = _items.Where(i => i.Title == title && i.ItemType == WindowTaskBarBase<B, S, T, U, O, SU>.ItemType.TabButton).SingleOrDefault();

            if (taskbar_item == null)
                return null;

            Canvas elem_list = (Canvas)taskbar_item.Container.Tag;
            return elem_list;
        }

        public FrameworkElement GetTabContainerByTitle(string title)
        {
            TaskbarItemBase<B, S, T, U, O, SU> taskbar_item = _items.Where(i => i.Title == title && i.ItemType == WindowTaskBarBase<B, S, T, U, O, SU>.ItemType.TabButton).SingleOrDefault();

            if (taskbar_item == null)
                return null;

            return taskbar_item.Container;
        }

        public void AddTabItemGridByTitle(string title, FrameworkElement g)
        {
            TaskbarItemBase<B, S, T, U, O, SU> taskbar_item = _items.Where(i => i.Title == title && i.ItemType == WindowTaskBarBase<B, S, T, U, O, SU>.ItemType.TabButton).SingleOrDefault();

            if (taskbar_item == null)
                return;

            TabItemGrid.Children.Clear();
            TabItemGrid.Children.Add(g);
            TabItemGrid.Tag = g;
        }


        #endregion

        #region Add and Remove SubTool Panels
        protected object CreateSubToolsPanel(ToggleButton toggle_button)
        {
            return null;
        }

        #endregion

        #region Add and Remove SubMenu Panels
        protected S CreateSubMenuPanel(ToggleButton toggle_button)
        {
            S sub_menu_panel = new S();
            sub_menu_panel.BaseFrame = BaseFrame;
            sub_menu_panel.ToggleButtonReference = toggle_button;
            sub_menu_panel.FadeOutCompleted += new EventHandler(sub_menu_panel_FadeOutCompleted);
            sub_menu_panel.SetBinding(BorderBrushProperty, new Binding("WindowBorderBrush") { Source = BaseFrame });
            sub_menu_panel.SetBinding(SubMenuPanelBase<B, S, T, U, O, SU>.SubMenuIconBgBrushProperty, new Binding("DefaultForegroundBrush") { Source = BaseFrame });
            sub_menu_panel.SetBinding(SubMenuPanelBase<B, S, T, U, O, SU>.PanelBackgroundProperty, new Binding("SubMenuPanelBackground") { Source = BaseFrame });
            return sub_menu_panel;
        }

        protected void AddSubMenuPanel(S sub_menu_panel)
        {

            if (SubMenuBaseGrid.Children.Count > 0)
            {
                S curr_active_sub_menu_panel = (S)SubMenuBaseGrid.Children[0];
                if (curr_active_sub_menu_panel != sub_menu_panel)
                {
                    curr_active_sub_menu_panel.ToggleButtonReference.IsChecked = false;
                    _tobeadded_panel = sub_menu_panel;
                }
                else
                    sub_menu_panel.StartFadeIn();
            }
            else
            {
                SubMenuBaseGrid.Children.Add(sub_menu_panel);
                SubMenuBaseGrid.SetBinding(HeightProperty, new Binding("ActualHeight") { Source = sub_menu_panel.SubmenuBorder });
                sub_menu_panel.StartFadeIn();
            }
        }

        //protected void AddSubMenuPanel()
        //{
        //}

        //protected void AddSubMenuPanel()
        //{
        //}
        #endregion

        #region Helper Methods
        public Point GetElementPosition(FrameworkElement elem)
        {
            return GetElementPosition(elem, BaseFrame.WindowBorder);
        }

        public Point GetElementPosition(FrameworkElement elem, Visual ref_visual)
        {
            this.UpdateLayout();
            GeneralTransform transform = elem.TransformToAncestor(ref_visual);

            Point rootPoint = transform.Transform(new Point(0, 0));

            Thickness margin = this.Margin;

            rootPoint.X -= margin.Left / 2;
            rootPoint.X += margin.Right / 2;

            rootPoint.Y += margin.Bottom / 2;
            rootPoint.Y -= margin.Top / 2;

            return rootPoint;
        }
        #endregion

        #region Event Handler

        #region SubMenu Panel
        void sub_menu_panel_FadeOutCompleted(object sender, EventArgs e)
        {
            SubMenuBaseGrid.Children.Remove((S)sender);
            BindingOperations.ClearAllBindings(SubMenuBaseGrid);

            if (_tobeadded_panel != null)
            {
                SubMenuBaseGrid.Children.Add(_tobeadded_panel);
                SubMenuBaseGrid.SetBinding(HeightProperty, new Binding("ActualHeight") { Source = _tobeadded_panel.SubmenuBorder });
                _tobeadded_panel.StartFadeIn();
                _tobeadded_panel = null;
            }
        }

        void stb_container_Unchecked(object sender, RoutedEventArgs e)
        {
            ToggleButton toggle_button = (ToggleButton)sender;
            S sub_menu_panel = (S)toggle_button.Tag;
            sub_menu_panel.StartFadeOut();
        }

        void stb_container_Checked(object sender, RoutedEventArgs e)
        {
            ToggleButton toggle_button = (ToggleButton)sender;
            S sub_menu_panel = (S)toggle_button.Tag;
            AddSubMenuPanel(sub_menu_panel);
        }
        #endregion

        #region TabContainer
        void tab_container_Unchecked(object sender, RoutedEventArgs e)
        {
            TabItemGrid.Visibility = Visibility.Visible;
            RaiseTapInActiveEvent();
        }

        void tab_container_Checked(object sender, RoutedEventArgs e)
        {
            ToggleButton toggle_button = (ToggleButton)sender;
            Point r_point = UIHelper.GetElementPosition(toggle_button, ItemCanv);

            //tab_item_grid.Visibility = Visibility.Visible;
            RaiseTapActiveEvent();
            //Canvas.SetLeft(tab_item_grid, r_point.X - toggle_button.ActualWidth + 15);
            //Canvas.SetTop(tab_item_grid, r_point.Y - ((FrameworkElement)tab_item_grid.Tag).Height);

            Canvas.SetLeft(TabItemGrid, r_point.X - TabItemGrid.ActualWidth / 2 + toggle_button.ActualWidth / 2);
            Canvas.SetTop(TabItemGrid, r_point.Y - TabItemGrid.ActualHeight);

        }
        #endregion

        void tb_container_Unchecked(object sender, RoutedEventArgs e)
        {
        }

        void tb_container_Checked(object sender, RoutedEventArgs e)
        {
        }

        void b_container_Click(object sender, RoutedEventArgs e)
        {
            Button b_container = (Button)sender;
            TaskbarItemBase<B, S, T, U, O, SU> item = (TaskbarItemBase<B, S, T, U, O, SU>)b_container.Content;
            if (MenuButtonClick != null)
                MenuButtonClick(sender, new WindowTaskBarBase<B, S, T, U, O, SU>.TaskbarButtonClickEventArgs(item.Title));
        }

        #endregion

        #region Properties
        public S ActiveSubMenuPanel
        {
            get
            {
                if (SubMenuBaseGrid.Children.Count == 0)
                    return null;

                return (S)SubMenuBaseGrid.Children[0];
            }
        }
        public double BarHeight
        {
            get { return (double)GetValue(BarHeightProperty); }
            set { SetValue(BarHeightProperty, value); }
        }
        public static readonly DependencyProperty BarHeightProperty = DependencyProperty.Register("BarHeight", typeof(double), typeof(WindowTaskBarBase<B, S, T, U, O, SU>), new UIPropertyMetadata((double)70));

        public BaseFrameBase<B, S, T, U, O, SU> BaseFrame
        {
            get { return _base_frame; }
            set
            {
                _base_frame = value;
            }
        }

        #endregion

        #region RoutedEvents
        public static readonly RoutedEvent TapActiveEvent = EventManager.RegisterRoutedEvent("TapActive", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(WindowTaskBarBase<B, S, T, U, O, SU>));

        // Provide CLR accessors for the event
        public event RoutedEventHandler TapActive
        {
            add { AddHandler(TapActiveEvent, value); }
            remove { RemoveHandler(TapActiveEvent, value); }
        }

        // This method raises the Tap event
        void RaiseTapActiveEvent()
        {
            RoutedEventArgs newEventArgs = new RoutedEventArgs(WindowTaskBarBase<B, S, T, U, O, SU>.TapActiveEvent);
            RaiseEvent(newEventArgs);
        }

        public static readonly RoutedEvent TapInActiveEvent = EventManager.RegisterRoutedEvent("TapInActive", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(WindowTaskBarBase<B, S, T, U, O, SU>));

        // Provide CLR accessors for the event
        public event RoutedEventHandler TapInActive
        {
            add { AddHandler(TapInActiveEvent, value); }
            remove { RemoveHandler(TapInActiveEvent, value); }
        }

        // This method raises the Tap event
        void RaiseTapInActiveEvent()
        {
            RoutedEventArgs newEventArgs = new RoutedEventArgs(WindowTaskBarBase<B, S, T, U, O, SU>.TapInActiveEvent);
            RaiseEvent(newEventArgs);
        }
        #endregion

        #region Attributes
        protected List<TaskbarItemBase<B, S, T, U, O, SU>> _items;
        public event EventHandler ItemAdded;
        protected BaseFrameBase<B, S, T, U, O, SU> _base_frame;
        private S _tobeadded_panel;
        private S _tobeshow_panel;

        public event EventHandler<WindowTaskBarBase<B, S, T, U, O, SU>.TaskbarButtonClickEventArgs> MenuButtonClick;
        private Storyboard _tap_active_sb = null;
        private Storyboard _tap_inactive_sb = null;

        #endregion

        #region Test Custom Routed Event

        private Storyboard GetTapActiveStoryboard()
        {
            if (_tap_active_sb == null)
            {
                _tap_active_sb = new Storyboard();

                DoubleAnimation anim1 = new DoubleAnimation(0, 1, new Duration(TimeSpan.FromMilliseconds(400)));
                anim1.AccelerationRatio = 0.7;
                anim1.DecelerationRatio = 0.3;
                Storyboard.SetTargetName(anim1, "tab_item_grid");
                Storyboard.SetTargetProperty(anim1, new PropertyPath("(RenderTransform).(ScaleTransform.ScaleX)"));

                DoubleAnimation anim2 = new DoubleAnimation(0, 1, new Duration(TimeSpan.FromMilliseconds(400)));
                anim2.AccelerationRatio = 0.7;
                anim2.DecelerationRatio = 0.3;
                Storyboard.SetTargetName(anim2, "tab_item_grid");
                Storyboard.SetTargetProperty(anim2, new PropertyPath("(RenderTransform).(ScaleTransform.ScaleY)"));

                _tap_active_sb.Children.Add(anim1);
                _tap_active_sb.Children.Add(anim2);
            }

            return _tap_active_sb;
        }

        private Storyboard GetTapInActiveStoryboard()
        {
            if (_tap_inactive_sb == null)
            {
                _tap_inactive_sb = new Storyboard();

                DoubleAnimation anim1 = new DoubleAnimation(1, 0, new Duration(TimeSpan.FromMilliseconds(400)));
                anim1.AccelerationRatio = 0.3;
                anim1.DecelerationRatio = 0.7;
                Storyboard.SetTargetName(anim1, "tab_item_grid");
                Storyboard.SetTargetProperty(anim1, new PropertyPath("(RenderTransform).(ScaleTransform.ScaleX)"));

                DoubleAnimation anim2 = new DoubleAnimation(1, 0, new Duration(TimeSpan.FromMilliseconds(400)));
                anim2.AccelerationRatio = 0.3;
                anim2.DecelerationRatio = 0.7;
                Storyboard.SetTargetName(anim2, "tab_item_grid");
                Storyboard.SetTargetProperty(anim2, new PropertyPath("(RenderTransform).(ScaleTransform.ScaleY)"));

                _tap_inactive_sb.Children.Add(anim1);
                _tap_inactive_sb.Children.Add(anim2);
            }

            return _tap_inactive_sb;
        }

        void WindowTaskBar_TapInActive(object sender, RoutedEventArgs e)
        {
            Storyboard sb_inactive = GetTapInActiveStoryboard();
            if (sb_inactive != null)
                ((UserControl)Content).BeginStoryboard(sb_inactive, HandoffBehavior.Compose);
        }

        void WindowTaskBar_TapActive(object sender, RoutedEventArgs e)
        {
            Storyboard sb_active = GetTapActiveStoryboard();
            if (sb_active != null)
            {
                ((UserControl)Content).BeginStoryboard(sb_active, HandoffBehavior.Compose);
            }
        }
        #endregion

        public double ActualHeight
        {
            get { return ((UserControl)Content).ActualHeight; }
        }
    }
}
