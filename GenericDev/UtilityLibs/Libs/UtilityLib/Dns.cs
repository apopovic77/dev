using System;
using System.Collections.Generic;
using System.Text;

namespace Logicx.Utilities
{
    public class Dns
    {
        public static string GetLocalIp() {
            System.Net.IPAddress[] arr_ip = System.Net.Dns.GetHostAddresses(System.Net.Dns.GetHostName());
            return arr_ip[0].ToString();
        }
    }
}
