﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Logicx.DiagramLib.Controls;
using Logicx.DiagramLib.Resources.Converter;

namespace Logicx.DiagramLib.DiagramObjects.Objects
{
    public abstract class VerticalStrechedAreaObject<Tx, Ty> : AreaObject<Tx, Ty>
    {
        
        #region Construction and Initialization
        /// <summary>
        /// Constructor of the class
        /// </summary>
        /// <param name="from_x_value"></param>
        /// <param name="to_x_value"></param>
        public VerticalStrechedAreaObject(Tx from_x_value, Tx to_x_value)
            : base(from_x_value, default(Ty), to_x_value, default(Ty))
        {
        }
        #endregion

        #region Operations
        protected override void OnUpdateVisualBindings()
        {
            base.OnUpdateVisualBindings();
            if (ObjectVisual == null)
                return;

            // always begins at 0
            BindingOperations.ClearBinding(ObjectVisual, Canvas.TopProperty);
            Canvas.SetTop(ObjectVisual, 0);


            // always uses full height
            BindingOperations.ClearBinding(ObjectVisual, FrameworkElement.HeightProperty);

            Binding full_height = new Binding("ActualHeight");
            //full_height.RelativeSource = new RelativeSource(RelativeSourceMode.FindAncestor, typeof (DiagramBase<Tx, Ty>), 1);
            full_height.RelativeSource = new RelativeSource(RelativeSourceMode.FindAncestor, typeof(Canvas), 1);
            ObjectVisual.SetBinding(FrameworkElement.HeightProperty, full_height);
        }
        #endregion

        #region Properties
        public override bool CanResizeY
        {
            get { return false; }
        }
        #endregion
    }
}
