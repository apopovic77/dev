﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CognitiveLib.Detector;
using Dynamsoft.Barcode;

namespace CognitiveLib.Scanner
{
    public class BarcodeScanResult : List<WeightedString>
    {
        public BarcodeScanResult(int count) : base(count)
        {

        }
        public List<WeightedString> Barcodes
        {
            get { return this; }
        }

        public override string ToString()
        {
            string res = "";
            foreach (WeightedString s in Barcodes)
            {
                res += s.ToString() + "\n";
            }
            res = res.Remove(res.Length - 1);
            return res;
        }
    }

    public class BarcodeScanner
    {

        public static BarcodeScanResult ReadBarcodes(string path_to_image)
        {
            try
            {
                Bitmap bmp = new Bitmap(path_to_image);

                BarcodeReader br = new BarcodeReader("t0068MgAAAFAAoOBQRFwoOhBtLrspTMLxmAowGqjKzHsOi71IxtJ+36VT3R6lBvpJrfFh7owLxRiq0IJvF8hEg0vBXkBzGKo=;");

                BarcodeResult[] ary_result = br.DecodeBitmap(bmp);

                if (ary_result != null)
                {
                    BarcodeScanResult barcodes = new BarcodeScanResult(ary_result.Length);
                    for (var i = 0; i < ary_result.Length; i++)
                    {
                        barcodes.Add(new WeightedString(ary_result[i].BarcodeText, 100) { Attribute = "Dynasoft Barcode Scanner" });
                    }
                    return barcodes;
                }
            }
            finally
            {
                
            }
            return null;
        }
    }
}
