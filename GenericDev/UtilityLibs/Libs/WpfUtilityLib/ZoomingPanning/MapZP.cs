﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Logicx.WpfUtility.GraphPlotter;
using Logicx.WpfUtility.WpfHelpers;
using MathLib;
using Windows7.Multitouch;
using Windows7.Multitouch.WPF;
using Windows7.Multitouch.Manipulation;
using ManipulationCompletedEventArgs = Windows7.Multitouch.Manipulation.ManipulationCompletedEventArgs;
using ManipulationDeltaEventArgs = Windows7.Multitouch.Manipulation.ManipulationDeltaEventArgs;
using ManipulationStartedEventArgs = Windows7.Multitouch.Manipulation.ManipulationStartedEventArgs;

namespace Logicx.WpfUtility.ZoomingPanning
{
    public class MapZP
    {
        #region Custom Structs
        protected struct JumpAnimation
        {
            public bool IsActive;
            public bool step1_completed;
            public bool step2_completed;
            public Vector2f TargetIa;
            public double TransMiddleX;
            public double TransMiddleY;
            public double TransTargetX;
            public double TransTargetY;
            public double MiddleScale;
            public double TargetScale;
            public float Secs;
            /// <summary>
            /// zeitspanne, in der die Animation in der Hälfte verweilen soll
            /// </summary>
            public float WaitTime;
            public bool ThrowMiddlePointEvent;
        }
        #endregion


        #region Custom EventArg Classes
        public class RegionSelectedEventArgs : EventArgs
        {
            public RegionSelectedEventArgs(float km_start, float km_end)
            {
                KmStart = km_start;
                KmEnd = km_end;
            }

            public float KmStart, KmEnd;
        }
        public class ZoomChangedEventArgs : EventArgs
        {
            public ZoomChangedEventArgs(double to_zoom)
            {
                ToZoom = to_zoom;
            }

            public double ToZoom;
        }
        public class WheelZoomEventArgs : EventArgs
        {
            public WheelZoomEventArgs(double from_zoom, double to_zoom)
            {
                ToZoom = to_zoom;
                FromZoom = from_zoom;
            }

            public double ToZoom;
            public double FromZoom;
        }
        public class JumpMiddlePointEventArgs : EventArgs
        {
            public JumpMiddlePointEventArgs(Vector2f target_ia)
            {
                TargetIa = target_ia;
            }      
           
            public Vector2f TargetIa;
        }
        #endregion

        public MapZP(FrameworkElement mapbase, Canvas map)
        {
            _mapbase = mapbase;
            _map = map;
            
            //scale and _translate_transform transforms
            ScaleTransform = new ScaleTransform(1, 1);

            TranslateTransform = new TranslateTransform(0, 0);
            TransformGroup group = new TransformGroup();
            group.Children.Add(ScaleTransform);
            group.Children.Add(TranslateTransform);
            _map.RenderTransform = group;

            _mapbase.Loaded += MapBase_Loaded;
        }



        private const int TICKS_PER_SECOND = 60;
        private const int SKIP_TICKS = 1000 / TICKS_PER_SECOND;
        private const int MAX_FRAMESKIP = 5;
        private static Stopwatch _stopwatch = Stopwatch.StartNew();
        long last_frame_realtime = -10000000;
        long next_game_tick = _stopwatch.ElapsedMilliseconds;

        /// <summary>
        /// Main function that adjusts zooming an panning
        /// </summary>
        public bool Update()
        {
            bool view_changed = false;
            int loops = 0;
            while (_stopwatch.ElapsedMilliseconds > next_game_tick && loops < MAX_FRAMESKIP)
            {
                view_changed |= UpdateMap();
                next_game_tick += SKIP_TICKS;
                loops++;
            }
            return view_changed;
        }

        public bool UpdateMap()
        {
            bool view_changed = false;
                   
            if (JumpAnimationActive)
            {
                if (TranslationAnimationActive || MapTranslationActive || ZoomAnimationActive || MultiTouchTranslationActive || MultiTouchZoomActive)
                {
                    JumpAnimationActive = false;
                    _jump_animation_watch.Stop();
                }
                else
                {
                    double elapsed_sec = _jump_animation_watch.Elapsed.TotalSeconds;
                    if (elapsed_sec < _jump_animation_seconds)
                    {
                        if (_jump_to_point)
                        {
                            double curr_t = (elapsed_sec / _jump_animation_seconds);
                            double iscale = _jump_scale_animation_spline.interpolate(curr_t);
                            double scale;
                            scale = _jump_start_scale + (_jump_target_scale - _jump_start_scale) * iscale;

                            TranslateTransform.X = _jump_start_transx + (_jump_end_transx - _jump_start_transx) * iscale;
                            TranslateTransform.Y = _jump_start_transy + (_jump_end_transy - _jump_start_transy) * iscale;
                          
                            ScaleTransform.ScaleX = scale;
                            ScaleTransform.ScaleY = scale;
                        }
                    }
                    else
                    {
                        if(_jump_with_stop.IsActive)
                        {
                            if(!_jump_with_stop.step1_completed)
                            {
                                if (_jump_with_stop.ThrowMiddlePointEvent == false)
                                {
                                    double curr_t = 1;
                                    double iscale = _jump_scale_animation_spline.interpolate(curr_t);
                                    double scale;
                                    scale = _jump_start_scale + (_jump_target_scale - _jump_start_scale) * iscale;

                                    TranslateTransform.X = _jump_start_transx + (_jump_end_transx - _jump_start_transx) * iscale;
                                    TranslateTransform.Y = _jump_start_transy + (_jump_end_transy - _jump_start_transy) * iscale;
                             
                                    ScaleTransform.ScaleX = scale;
                                    ScaleTransform.ScaleY = scale;

                                    RaiseJumpAnimationMiddlePointReached();
                                    _jump_with_stop.ThrowMiddlePointEvent = true;
                                }

                                // Die Wartezeit in der mittleren Position ist abgelaufen und die Animation kann fortgesetzt werden
                                if (elapsed_sec > _jump_animation_seconds + _jump_with_stop.WaitTime)
                                {
                                    JumpAnimationActive = false;
                                    _jump_animation_watch.Stop();

                                    

                                    StartJumpToPointAnimation(new Vector2f(), _jump_with_stop.TransTargetX, _jump_with_stop.TransTargetY, _jump_with_stop.TargetScale, _jump_with_stop.Secs / 2);
                                    _jump_with_stop.step1_completed = true;
                                }
                            }
                            else if(!_jump_with_stop.step2_completed)
                            {
                                RaiseJumpAnimationMiddlePointWaitingCompleted();

                                double curr_t = 1;
                                double iscale = _jump_scale_animation_spline.interpolate(curr_t);
                                double scale;
                                scale = _jump_start_scale + (_jump_target_scale - _jump_start_scale) * iscale;

                                TranslateTransform.X = _jump_start_transx + (_jump_end_transx - _jump_start_transx) * iscale;
                                TranslateTransform.Y = _jump_start_transy + (_jump_end_transy - _jump_start_transy) * iscale;
                                ScaleTransform.ScaleX = scale;
                                ScaleTransform.ScaleY = scale;

                                JumpAnimationActive = false;
                                _jump_animation_watch.Stop();

                                _jump_with_stop.step2_completed = true;
                                _jump_with_stop.IsActive = false;
                            }
                        }
                        else
                        {
                            if (_jump_to_point)
                            {
                                double curr_t = 1;
                                double iscale = _jump_scale_animation_spline.interpolate(curr_t);
                                double scale;
                                scale = _jump_start_scale + (_jump_target_scale - _jump_start_scale) * iscale;

                                TranslateTransform.X = _jump_start_transx + (_jump_end_transx - _jump_start_transx)*iscale;
                                TranslateTransform.Y = _jump_start_transy + (_jump_end_transy - _jump_start_transy) * iscale;
                                
                                ScaleTransform.ScaleX = scale;
                                ScaleTransform.ScaleY = scale;
                            }

                            JumpAnimationActive = false;
                            _jump_animation_watch.Stop();

                            if (JumpAnimationFinished != null)
                                JumpAnimationFinished(this, new EventArgs());

                            if(JumpToPointAnimationFinished != null)
                                JumpToPointAnimationFinished(this, new EventArgs());
                        }

                    }
                }
            }

            if (_selection_active)
            {
                double sel_rect_width = LastMousePos.X - _sel_rect_pos.X;
                double sel_rect_height = LastMousePos.Y - _sel_rect_pos.Y;

                if (sel_rect_width < 0)
                {
                    sel_rect_width *= -1;
                    Canvas.SetLeft(_sel_rect, _sel_rect_pos.X - sel_rect_width);
                }

                if (sel_rect_height < 0)
                {
                    sel_rect_height *= -1;
                    Canvas.SetTop(_sel_rect, _sel_rect_pos.Y - sel_rect_height);
                }

                _sel_rect.Width = sel_rect_width;
                _sel_rect.Height = sel_rect_height;
                _sel_rect.MinWidth = sel_rect_width;
                _sel_rect.MinHeight = sel_rect_height;
                _sel_rect.MaxWidth = sel_rect_width;
                _sel_rect.MaxHeight = sel_rect_height;

            }
            else if (MapTranslationActive)
            {
                if (LastTranslationMousePos != LastMousePos)
                {
                    _count_translation_idle = 0;

                    double transl_x = TranslateTransform.X + (LastMousePos.X - LastTranslationMousePos.X);
                    double transl_y = TranslateTransform.Y + (LastMousePos.Y - LastTranslationMousePos.Y);

                    PushLastTranslVec();

                    TranslateTransform.X = transl_x;
                    if (!IgnoreYTranslation)
                        TranslateTransform.Y = transl_y;

                    if (WithBorderCheckWhileTranslationActive)
                        CheckTranslationBorders();

                    LastTranslationMousePos = LastMousePos;

                    view_changed = true;
                }
                else
                {
                    _count_translation_idle++;
                }
            }
            else if (MultiTouchTranslationActive || _multitouch_zoomtranslation_active)
            {
                if (_last_last_transl_stylus_pos != _last_transl_stylus_pos)
                {
                    _count_translation_idle = 0;

                    double transl_x = _last_transl_stylus_pos.X - _last_last_transl_stylus_pos.X;
                    double transl_y = _last_transl_stylus_pos.Y - _last_last_transl_stylus_pos.Y;
                    
                    if (!MultiTouchZoomActive)
                    {
                        transl_x *= _smooth;
                        transl_y *= _smooth;
                    }

                    double tmp_transl_x = transl_x;
                    double tmp_transl_y = transl_y;

                    PushLastTranslVec();

                    TranslateTransform.X += transl_x;
                    TranslateTransform.Y += transl_y;

                    if (WithBorderCheckWhileTranslationActive)
                        CheckTranslationBorders();

                    _last_last_transl_stylus_pos.X += tmp_transl_x;
                    _last_last_transl_stylus_pos.Y += tmp_transl_y;
                    
                    view_changed = true;
                }
                else
                {
                    _count_translation_idle++;
                }
            }
            else if (_manual_zoom_active)
            {
                double rel_mouse_move = (LastMousePos.Y - LastTranslationMousePos.Y);
                double scale_target = ScaleTransform.ScaleX + rel_mouse_move * _manual_zoom_speed_factor;

                double mouse_pos_x = -TranslateTransform.X + _manual_zoom_mouse_pos_start.X;
                double mouse_pos_y = -TranslateTransform.Y + _manual_zoom_mouse_pos_start.Y;
                double mouse_pos_after_scale_x = (mouse_pos_x * scale_target) / ScaleTransform.ScaleX;
                double mouse_pos_after_scale_y = (mouse_pos_y * scale_target) / ScaleTransform.ScaleX;
                double transl_x = mouse_pos_x - mouse_pos_after_scale_x;
                double transl_y = mouse_pos_y - mouse_pos_after_scale_y;

                _transl_target_x = TranslateTransform.X + transl_x;
                _transl_target_y = TranslateTransform.Y + transl_y;

                TranslateTransform.X = _transl_target_x;
                TranslateTransform.Y = _transl_target_y;
                ScaleTransform.ScaleX = scale_target;
                ScaleTransform.ScaleY = scale_target;

                LastTranslationMousePos = LastMousePos;
            }
            else if (ZoomAnimationActive)
            {
                //view_changed = true;
                double old_scale = ScaleTransform.ScaleX;

                ScaleAnimationStepSize -= _stepsize_scale_deccl;
                _tranlate_step_x_size -= _stepsize_transl_x_deccl;
                _tranlate_step_y_size -= _stepsize_transl_y_deccl;

                //perform scale
                ScaleTransform.ScaleX += ScaleAnimationStepSize;
                ScaleTransform.ScaleY += ScaleAnimationStepSize;

                //perform translation operation
                double transl_x = TranslateTransform.X + _tranlate_step_x_size;
                double transl_y = TranslateTransform.Y + _tranlate_step_y_size;


                //check for translation borders
                //CheckTranslationBorders(ref transl_x, ref transl_y);

                TranslateTransform.X = transl_x;
                TranslateTransform.Y = transl_y;

                //Console.WriteLine("Zoom: "+_curr_zoom_anim_frame + "frames all: "+_zoom_animation_frames);
                //raise curr zoom anim frame counter
                _curr_zoom_anim_frame++;
                //beende die animation wenn wir fertig damit sind
                if (_zoom_animation_frames <= _curr_zoom_anim_frame)
                {
                    ZoomAnimationActive = false;
                    ScaleTransform.ScaleX = _scale_target;
                }
            }


            if (!MapTranslationActive && !MultiTouchTranslationActive)
            {
                //border check
                bool view_changed_because_of_border_check = CheckTranslationBorders();
                if (view_changed_because_of_border_check)
                {
                    view_changed = true;
                    _init_transl_animation = false;
                }
            }

            //if (!MultiTouchTranslationActive)
            //{
            //    //border check
            //    bool view_changed_because_of_border_check = CheckTranslationBorders();
            //    if (view_changed_because_of_border_check)
            //    {
            //        view_changed = true;
            //        _init_transl_animation = false;
            //    }
            //}

            if (_init_transl_animation)
            {
                //set anim parameters for translation
                _init_transl_animation = false;
                if (_count_translation_idle < 5)
                {
                    DateTime calc_time = DateTime.Now;
                    Vector v_transl = new Vector(TranslateTransform.X, TranslateTransform.Y);
                    _transl_animation_velocity = v_transl - GetAvgTranslation(calc_time);

                    if (_transl_animation_velocity.Length > 0)
                    {
                        _transl_animation_velocity.Normalize();
                        _transl_animation_velocity *= GetAvgTranslationVelocity(calc_time);
                    }
                    
                    //_transl_animation_velocity *= _slide;

                    TranslationAnimationActive = true;
                }
                else
                {
                    RaiseTranslationCompleted();
                    RaiseMovementAnimationCompleted();
                    TranslationAnimationActive = false;
                }
            }

            if (TranslationAnimationActive)
            {
                TranslateTransform.X += _transl_animation_velocity.X;
                TranslateTransform.Y += _transl_animation_velocity.Y;
                _transl_animation_velocity *= _transl_animation_damping;
                view_changed = true;

                if (Math.Round(_transl_animation_velocity.X) == 0 && Math.Round(_transl_animation_velocity.Y) == 0)
                    TranslationAnimationActive = false;
            }

            if (Double.IsNaN(TranslateTransform.X))
                TranslateTransform.X = 0;
            if (Double.IsNaN(TranslateTransform.Y))
                TranslateTransform.Y = 0;
            return view_changed;
        }


        private bool CheckTranslationBorders()
        {
            bool view_changed_because_of_border_check = false;

            if (!WithBorderCheck)
                return view_changed_because_of_border_check;

            double transl_x = TranslateTransform.X;
            double transl_y = TranslateTransform.Y;         

            double border_check_x_max = BorderCheckerXMax;
            double border_check_y_max = BorderCheckerYMax;
            double border_check_x_min = _mapbase.ActualWidth - (BorderCheckerWidth * ScaleTransform.ScaleX);
            double border_check_y_min = _mapbase.ActualHeight - (BorderCheckerHeight * ScaleTransform.ScaleX);

            double target_transl_x = transl_x;
            double target_transl_y = transl_y;
            
            if (transl_x >= border_check_x_max)
            {
                target_transl_x = border_check_x_max;
            }
            else if (transl_x < border_check_x_min)
            {
                target_transl_x = border_check_x_min;
            }
            
            if (transl_y >= border_check_y_max)
            {
                target_transl_y = border_check_y_max;
            }
            else if (transl_y < border_check_y_min)
            {
                target_transl_y = border_check_y_min;
            }    

            if (ZoomAnimationActive)
            {
                if (!ZoomInActive)
                {
                    TranslateTransform.X = target_transl_x;
                    TranslateTransform.Y = target_transl_y;
                }
                return true;
            }


            if (WithBorderCheckWhileTranslationActive)
            {
                TranslateTransform.X = target_transl_x;
                TranslateTransform.Y = target_transl_y;
                return true;
            }


            if (target_transl_x != transl_x)
            {
                if (Math.Abs(target_transl_x - transl_x) < 0.000001)
                    TranslateTransform.Y = target_transl_y;
                else
                {
                    TranslateTransform.X = (transl_x + ((target_transl_x - transl_x) * BorderCheckCorrSppedFactor));
                    view_changed_because_of_border_check = true;
                }
            }
            if (target_transl_y != transl_y)
            {
                if (Math.Abs(target_transl_y - transl_y) < 0.000001)
                    TranslateTransform.Y = target_transl_y;
                else
                {
                    TranslateTransform.Y = (transl_y + ((target_transl_y - transl_y) * BorderCheckCorrSppedFactor));
                    view_changed_because_of_border_check = true;
                }
            }

            return view_changed_because_of_border_check;
        }

        #region Translation Animation
        private void ResetLastTranslVec()
        {
            _init_transl_animation = false;
            TranslationAnimationActive = false;
            _transl_animation_velocity = new Vector();
            for (int i = 0; i < _last_translations.Length; i++)
            {
                _last_translations[i] = new Vector();
                _last_translations_timestamp[i] = new DateTime();
            }
        }
        private void PushLastTranslVec()
        {
            Vector curr_transl = new Vector(TranslateTransform.X, TranslateTransform.Y);
            DateTime curr_transl_time = DateTime.Now;
            for (int i = _last_translations.Length - 1; i >= 1; i--)
            {
                _last_translations[i] = _last_translations[i - 1];
                _last_translations_timestamp[i] = _last_translations_timestamp[i - 1];
            }
            _last_translations[0] = curr_transl;
            _last_translations_timestamp[0] = curr_transl_time;
        }
        private double GetAvgTranslationVelocity(DateTime calc_time)
        {
            double sum_vel = 0;
            int count_transl = 0;

            for (int i = _last_translations.Length - 1; i >= 1; i--)
            {
                if ((_last_translations[i].X == 0 && _last_translations[i].Y == 0) || ((calc_time - _last_translations_timestamp[i]).TotalMilliseconds > _last_translations_time_threshold))
                    continue;
                sum_vel += (_last_translations[i] - _last_translations[i - 1]).Length;
                count_transl++;
            }
            if (sum_vel == 0)
                return 0;
            double avg_vel = sum_vel / count_transl;
            return avg_vel;
        }
        private Vector GetAvgTranslation(DateTime calc_time)
        {
            Vector sum_last = new Vector();
            int count_transl = 0;

            for (int i = _last_translations.Length - 1; i >= 1; i--)
            {
                if ((_last_translations[i].X == 0 && _last_translations[i].Y == 0) || ((calc_time - _last_translations_timestamp[i]).TotalMilliseconds > _last_translations_time_threshold))
                    continue;
                sum_last += _last_translations[i];
                count_transl++;
            }
            if (count_transl == 0)
                return new Vector();

            Vector avg_last = sum_last/count_transl;
            return avg_last;
        }
        #endregion

        public void Zoom(int steps)
        {
            if (steps == 0)
                return;

            LeftMouseDown = false;
            RightMouseDown = false;
            _selection_active = false;
            _region_selection_active = false;
            _map_translation_active = false;

            int anim_duration_frames = AnimDurationFramesPerZoomStep;
            double old_scale;
            if (!ZoomAnimationActive)
                old_scale = ScaleTransform.ScaleX;
            else
            {
                old_scale = target_scale_anim;
                anim_duration_frames += AnimDurationFramesPerZoomStep - _curr_zoom_anim_frame;
            }

            double to_scale;
            bool wheel_zoom_out;

            double curr_map_width = _map.ActualWidth / old_scale;
            double new_map_width = curr_map_width;

            //check with or without scale
            double curr_alt = (_map.ActualWidth / ScaleTransform.ScaleX / 2) / Math.Tan(_fov / 2);
            if (curr_alt < WithZoomAnimationAtAlt)
                WithZoomAnimation = true;
            else
                WithZoomAnimation = false;

            if (steps < 0)
            {
                //zoom in 
                double wheel_scroll_count = Math.Abs(steps);
                for (int i = 0; i < wheel_scroll_count; i++)
                    new_map_width /= 2 - ZoomReductionFactor;
                wheel_zoom_out = false;
            }
            else
            {
                //zoom out
                double wheel_scroll_count = Math.Abs(steps);
                for (int i = 0; i < wheel_scroll_count; i++)
                    new_map_width *= 2 - ZoomReductionFactor;
                wheel_zoom_out = true;
            }

            if (!ZoomAnimationActive)
                to_scale = ScaleTransform.ScaleX * new_map_width / curr_map_width;
            else
                to_scale = target_scale_anim * new_map_width / curr_map_width;



            CheckScaleLimits(ref to_scale);

            if (ZoomChanged != null)
            {
                ZoomChanged(this, new ZoomChangedEventArgs(to_scale));
            }

            target_scale_anim = to_scale;
            StartWheelZoom(LinearWheelZoomAnimation, anim_duration_frames, to_scale, wheel_zoom_out);

        }

        private void CheckScaleLimits(ref double scale)
        {
            if (scale < BorderMinZoom)
                scale = BorderMinZoom;
            if (scale > BorderMaxZoom)
                scale = BorderMaxZoom;
        }

        public void RegisterEventHandler()
        {
            if (_handler_registered)
                return;

            RegisterMouseEventHandler();
            RegisterTouchEventHandler();

            _handler_registered = true;
        }

        public void DeRegisterEventHandler()
        {
            _explicit_deregister_call = true;

            if (!_handler_registered)
                return;

            DeRegisterMouseEventHandler();
            DeRegisterTouchEventHandler();

            _handler_registered = false;
        }

        #region Mouse Management
        private void DeRegisterMouseEventHandler()
        {
            _mapbase.MouseDown -= MouseDown;
            _mapbase.MouseMove -= MouseMove;
            _mapbase.MouseUp -= MouseUp;
            _mapbase.MouseWheel -= MouseWheel;
            _mapbase.MouseEnter -= MouseEnter;
            _mapbase.MouseLeave -= MouseLeave;
            _mapbase.KeyDown -= KeyDown;
        }
        private void RegisterMouseEventHandler()
        {
            _mapbase.MouseDown += MouseDown;
            _mapbase.MouseMove += MouseMove;
            _mapbase.MouseUp += MouseUp;
            _mapbase.MouseWheel += MouseWheel;
            _mapbase.MouseEnter += MouseEnter;
            _mapbase.MouseLeave += MouseLeave;
            _mapbase.KeyDown += KeyDown;
        }

        public void MouseMove(object sender, MouseEventArgs e)
        {
            //Wenn der Event von einem Stylus kommt, Abbruch
            //if (e.StylusDevice != null)
            //    return;

            LastMousePos = e.GetPosition(_mapbase);
            LastMousePos.X += MouseOffsetX;
            LastMousePos.Y += MouseOffsetY;
        }

        public void MouseDown(object sender, MouseButtonEventArgs e)
        {
            //Wenn der Event von einem Stylus kommt, Abbruch
            //if (e.StylusDevice != null)
            //    return;

            MapTranslationActive = false;
            //TranslationAnimationActive = false;
            ZoomAnimationActive = false;

            if (e.RightButton == MouseButtonState.Pressed)
                RightMouseDown = true;
            else if (e.LeftButton == MouseButtonState.Pressed)
                LeftMouseDown = true;

            Point pos = e.GetPosition(_mapbase);
            pos.X += MouseOffsetX;
            pos.Y += MouseOffsetY;


            if (RightMouseDown && _with_selection_rect)
            {
                ZoomRectangleStart(pos);
            }
            else if (RightMouseDown)
            {
                ManualZoomStart(pos);
            }

            if (LeftMouseDown)
            {
                MapTranslationStart(pos);
            }
        }

        private void ManualZoomStart(Point pos)
        {
            _manual_zoom_active = true;
            ZoomAnimationActive = true;
            LastTranslationMousePos = pos;
            _manual_zoom_mouse_pos_start = pos;
        }

        public void MapTranslationStart(Point pos)
        {
            ResetLastTranslVec();
            MapTranslationActive = true;
            ZoomAnimationActive = false;
            LastTranslationMousePos = pos;
            if (TranslationChanged != null)
                TranslationChanged(this, null);
        }


        public void ZoomRectangleStart(Point pos)
        {
            _selection_active = true;
            //selection rectangle handling
            _sel_rect_pos = pos;
            _sel_rect.SetValue(Canvas.TopProperty, _sel_rect_pos.Y);
            _sel_rect.SetValue(Canvas.LeftProperty, _sel_rect_pos.X);
            _sel_rect.Width = 0;
            _sel_rect.Height = 0;
            _sel_rect.Visibility = Visibility.Visible;
        }


        public void MouseUp(object sender, MouseButtonEventArgs e)
        {
            //Wenn der Event von einem Stylus kommt, Abbruch
            //if (e.StylusDevice != null)
            //    return;

            if (LeftMouseDown)
            {
                ZoomAnimationActive = false;
                _init_transl_animation = true;
            }

            if (_selection_active && (_sel_rect.ActualWidth > 10 && _sel_rect.ActualHeight > 10))
            {

                _sel_rect_pos.X = Canvas.GetLeft(_sel_rect);
                _sel_rect_pos.Y = Canvas.GetTop(_sel_rect);

                double sel_rect_width = _sel_rect.ActualWidth;
                double sel_rect_height = _sel_rect.ActualHeight;

                //start zoom animation
                ZoomAnimationActive = true;
                ZoomInActive = true;
                _curr_zoom_anim_frame = 0;


                Point scale_center = new Point(_sel_rect_pos.X + sel_rect_width / 2, _sel_rect_pos.Y + sel_rect_height / 2);
                LastMousePos = scale_center;
                Vector translation_offset = new Point(_map.ActualWidth / 2, _map.ActualHeight / 2) - scale_center;

                double old_scale = ScaleTransform.ScaleX;

                double to_scale = old_scale * _map.ActualWidth / sel_rect_width;
                if (sel_rect_width < sel_rect_height)
                    to_scale = old_scale * _map.ActualHeight / sel_rect_height;

                CheckScaleLimits(ref to_scale);

                StartWheelZoom(false, AnimDurationFramesPerZoomStep * 2, to_scale, ZoomInActive, translation_offset);



                if (ZoomChanged != null)
                {
                    ZoomChanged(this, new ZoomChangedEventArgs(to_scale));
                }
            }

            if (_with_selection_rect)
                _sel_rect.Visibility = Visibility.Hidden;

            if (_manual_zoom_active)
            {
                ZoomAnimationActive = false;
                _manual_zoom_active = false;
            }

            LeftMouseDown = false;
            RightMouseDown = false;

            _selection_active = false;
            _region_selection_active = false;
            _map_translation_active = false;

        }


        public void MouseWheel(object sender, MouseWheelEventArgs e)
        {
            Zoom(Convert.ToInt32(Math.Ceiling((double)e.Delta / 120)));
        }

        public void KeyDown(object sender, KeyEventArgs e)
        {
            KeyStates state_ctrl = Keyboard.GetKeyStates(Key.LeftCtrl);
            KeyStates state_plus = Keyboard.GetKeyStates(Key.OemPlus);          // +
            KeyStates state_minus = Keyboard.GetKeyStates(Key.OemMinus);        // -

            KeyStates state_num_plus = Keyboard.GetKeyStates(Key.Add);          // num-block +
            KeyStates state_num_minus = Keyboard.GetKeyStates(Key.Subtract);    // num-block -

            bool ctrl_down = (state_ctrl & KeyStates.Down)>0;
            bool plus_down = (state_plus & KeyStates.Down) > 0 || (state_num_plus & KeyStates.Down) > 0;
            bool minus_down = (state_minus & KeyStates.Down) > 0 || (state_num_minus & KeyStates.Down) > 0;

            if (ctrl_down)
            {
                if (plus_down)
                    Zoom(1);
                else if (minus_down)
                    Zoom(-1);            
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <param name="n">how many time steps</param>
        /// <param name="accel">value between -1 and +1
        /// -1 means that the anim starts fast and ends slow
        /// +1 means that the animation stars slow and ends fast</param>
        /// <param name="change_per_step"></param>
        /// <param name="change_the_change_per_step"></param>
        private void AnimStepCalc(double start, double end, int n, double accel, out double change_per_step, out double change_the_change_per_step)
        {
            double n_double = n;
            double max_start_val = (end - start) * 2.0 / n_double;

            if (accel > 0)
            {
                change_per_step = max_start_val - (max_start_val * accel);
            }
            else
            {
                accel = Math.Abs(accel);
                change_per_step = (max_start_val * accel);
            }

            change_the_change_per_step = ((end - start - n_double * change_per_step) / ((n_double * (n_double + 1.0)) / 2.0)) * -1.0;

        }

        public void StartJumpToPointAnimation(Vector2f targetia, double trans_x, double trans_y, double scale, float sec)
        {
            if (JumpAnimationActive)
                return;

            _jump_to_point = true;
           
            _jump_start_scale = ScaleTransform.ScaleX;
            _jump_target_scale = scale;
            _jump_start_transx = TranslateTransform.X;
            _jump_start_transy = TranslateTransform.Y;
            _jump_end_transx = trans_x;
            _jump_end_transy = trans_y;


            if (ScaleTransform.ScaleX > scale)
            {
                _jump_scale_animation_spline = new Spline(4);
                _jump_scale_animation_spline.X = new double[] {0, 0.2, 0.6, 1};
                _jump_scale_animation_spline.Y = new double[] {0, 0.1, 0.8, 1};
                _jump_scale_animation_spline.CreateSpline();
            }
            else
            {
                _jump_scale_animation_spline = new Spline(4);
                _jump_scale_animation_spline.X = new double[] { 0, 0.4, 0.9, 1 };
                _jump_scale_animation_spline.Y = new double[] { 0, 0.2, 0.9, 1 };
                _jump_scale_animation_spline.CreateSpline();
            }

            if (targetia.X != 0 && targetia.Y != 0 && JumpToPointAnimationStarted != null)
                JumpToPointAnimationStarted(this, new JumpMiddlePointEventArgs(targetia));

            JumpAnimationActive = true;
            _jump_animation_seconds = sec;
            _jump_animation_watch.Reset();
            _jump_animation_watch.Start();
        }

        public void StartJumpToPointAnimationWithZoomOut(Vector2f target_ia, double trans_x_middle, double trans_y_middle, double scale_middle, double trans_x_target, double trans_y_target, double scale_target, float sec)
        {
            if(JumpAnimationActive)
                return;

            StartJumpToPointAnimation(new Vector2f(), trans_x_middle, trans_y_middle, scale_middle, sec / 2);
              
            _jump_with_stop.TargetIa = target_ia;
            _jump_with_stop.TargetScale = scale_target;
            _jump_with_stop.TransTargetX = trans_x_target;
            _jump_with_stop.TransTargetY = trans_y_target;
            _jump_with_stop.Secs = sec;
            _jump_with_stop.IsActive = true;
            _jump_with_stop.step1_completed = false;
            _jump_with_stop.step2_completed = false;
            _jump_with_stop.ThrowMiddlePointEvent = false;
            _jump_with_stop.WaitTime = 2;
            JumpAnimationActive = true;
        }
       
        public void StartWheelZoom(bool linear_zoom_anim, int animation_duration_frames, double to_scale, bool wheel_zoom_out)
        {
            StartWheelZoom(linear_zoom_anim, animation_duration_frames, to_scale, wheel_zoom_out, new Vector(0, 0));
        }

        public void StartWheelZoom(bool linear_zoom_anim, int animation_duration_frames, double to_scale, bool wheel_zoom_out, Vector translation_offset)
        {
            ZoomInActive = wheel_zoom_out;

            //start zoom animation
            JumpAnimationActive = false;
            TranslationAnimationActive = false;
            MapTranslationActive = false;
            ZoomAnimationActive = true;
            _curr_zoom_anim_frame = 0;

            double old_scale = ScaleTransform.ScaleX;
            if (!WithZoomAnimation) animation_duration_frames = 1;


            if (!linear_zoom_anim && WithZoomAnimation)
            {
                AnimStepCalc(old_scale, to_scale, animation_duration_frames, -1, out ScaleAnimationStepSize, out _stepsize_scale_deccl);
            }
            else
            {
                ScaleAnimationStepSize = (to_scale - old_scale) / animation_duration_frames;
                _stepsize_scale_deccl = 0;
            }



            //ScaleStepSize = (to_scale - old_scale) / AnimDurationFrames;
            //_stepsize_scale_deccl = 0;

            double mouse_pos_x = -TranslateTransform.X + (LastMousePos.X);
            double mouse_pos_y = -TranslateTransform.Y + (LastMousePos.Y);
            double mouse_pos_after_scale_x = (mouse_pos_x * to_scale) / old_scale;
            double mouse_pos_after_scale_y = (mouse_pos_y * to_scale) / old_scale;
            double transl_x = mouse_pos_x - mouse_pos_after_scale_x;
            double transl_y = mouse_pos_y - mouse_pos_after_scale_y;
            _tranlate_step_x_size = transl_x / animation_duration_frames;
            _tranlate_step_y_size = transl_y / animation_duration_frames;
            _stepsize_transl_x_deccl = 0;
            _stepsize_transl_y_deccl = 0;

            _transl_target_x = TranslateTransform.X + transl_x + translation_offset.X;
            _transl_target_y = TranslateTransform.Y + transl_y + translation_offset.Y;

            if (!linear_zoom_anim)
            {
                AnimStepCalc(TranslateTransform.X, _transl_target_x, animation_duration_frames, -1, out _tranlate_step_x_size, out _stepsize_transl_x_deccl);
                AnimStepCalc(TranslateTransform.Y, _transl_target_y, animation_duration_frames, -1, out _tranlate_step_y_size, out _stepsize_transl_y_deccl);
            }
            else
            {
                _tranlate_step_x_size = (_transl_target_x - TranslateTransform.X) / animation_duration_frames;
                _tranlate_step_y_size = (_transl_target_y - TranslateTransform.Y) / animation_duration_frames;
                _stepsize_transl_x_deccl = 0;
                _stepsize_transl_y_deccl = 0;
            }
            _zoom_animation_frames = animation_duration_frames;
            _scale_target = to_scale;
        }



        public void MouseEnter(object sender, MouseEventArgs e)
        {
            //Wenn der Event von einem Stylus kommt, Abbruch
            //if (e.StylusDevice != null)
            //    return;

            if (LeftMouseDown && e.LeftButton == MouseButtonState.Released)
            {
                LeftMouseDown = false;
                MapTranslationActive = false;
            }
            if (_with_selection_rect && (_selection_active || _region_selection_active) && e.RightButton == MouseButtonState.Released)
            {
                _sel_rect.Visibility = Visibility.Hidden;
                LeftMouseDown = false;
                RightMouseDown = false;
                _selection_active = false;
                _region_selection_active = false;
                MapTranslationActive = false;
            }

            foreach (UIElement u in _map.Children)
            {
                MouseEventArgs ee = new MouseEventArgs(Mouse.PrimaryDevice, 0);
                ee.RoutedEvent = Mouse.MouseEnterEvent;

                u.RaiseEvent(ee);
            }
        }

        public void MouseLeave(object sender, MouseEventArgs e)
        {
            //Wenn der Event von einem Stylus kommt, Abbruch
            //if (e.StylusDevice != null)
            //    return;

            if (_with_selection_rect)
                _sel_rect.Visibility = Visibility.Hidden;
            ZoomAnimationActive = false;
            LeftMouseDown = false;
            RightMouseDown = false;
            _selection_active = false;
            _region_selection_active = false;
            _manual_zoom_active = false;
            if (MapTranslationActive)
                _init_transl_animation = true;
            else
                _init_transl_animation = false;
            _map_translation_active = false;
        }
        #endregion


        #region Touch Management
        private void RegisterTouchEventHandler()
        {
            if (TouchHandler.DigitizerCapabilities.IsMultiTouchReady)
            {

                Window window = UIHelper.TryFindParent<Window>(_mapbase);
                if (window == null)
                    return;

                Factory.EnableStylusEvents(window);

                ManipulationProcessor = new ManipulationProcessor(ProcessorManipulations.ALL);
                
                //window.StylusDown += StylusDown;
                //window.StylusUp += StylusUp;
                //window.StylusMove += StylusMove;
                //window.StylusLeave += StylusLeave;
                _mapbase.StylusDown += StylusDown;
                _mapbase.StylusUp += StylusUp;
                _mapbase.StylusMove += StylusMove;
                _mapbase.StylusLeave += StylusLeave;

                ManipulationProcessor.ManipulationStarted += ProcessManipulationStarted;
                ManipulationProcessor.ManipulationDelta += ProcessManipulationDelta;
                ManipulationProcessor.ManipulationCompleted += ProcessManipulationCompleted;

            }
        }

        private void DeRegisterTouchEventHandler()
        {
            if (!TouchHandler.DigitizerCapabilities.IsMultiTouchReady)
                return;

            Window window = UIHelper.TryFindParent<Window>(_mapbase);
            if (window == null)
                return;

            //window.StylusDown -= StylusDown;
            //window.StylusUp -= StylusUp;
            //window.StylusMove -= StylusMove;
            //window.StylusLeave -= StylusLeave;

            _mapbase.StylusDown -= StylusDown;
            _mapbase.StylusUp -= StylusUp;
            _mapbase.StylusMove -= StylusMove;
            _mapbase.StylusLeave -= StylusLeave;

            ManipulationProcessor.ManipulationStarted -= ProcessManipulationStarted;
            ManipulationProcessor.ManipulationDelta -= ProcessManipulationDelta;
            ManipulationProcessor.ManipulationCompleted -= ProcessManipulationCompleted;
            ManipulationProcessor = null;

        }
        private void MapBase_Loaded(object sender, RoutedEventArgs e)
        {
            _mapbase.Loaded -= MapBase_Loaded;
            if (!_explicit_deregister_call)
                RegisterEventHandler();
        }

        public void StylusDown(object sender, StylusDownEventArgs e)
        {
            if (e.StylusDevice == null)
                return;

            Point pos = e.GetPosition(_mapbase);

            MultiTouchTranslationActive = false;
            TranslationAnimationActive = false;
            
            if (!MapTranslationActive && !MultiTouchZoomActive && !_multitouch_zoomtranslation_active)
            {
                MultiTouchTranslationStart(_last_transl_stylus_pos);
            }
            _multi_touch_input_count++;
            ProcessDown(e.StylusDevice.Id, pos);
            //e.Handled = true;
        }

        public void StylusUp(object sender, StylusEventArgs e)
        {
            if (e.StylusDevice == null)
                return;

            Point pos = e.GetPosition(_mapbase);
            
            if (!MultiTouchZoomActive && !_multitouch_zoomtranslation_active)
            {
                _init_transl_animation = true;
            }

            if (_multi_touch_input_count > 0)
                _multi_touch_input_count--;

            if (_multi_touch_input_count <= 0)
            {
                _multi_touch_input_count = 0;
                _multitouch_zoomtranslation_active = false;
                MultiTouchZoomActive = false;
                _multitouch_translation_active = false;
            }
            ProcessUp(e.StylusDevice.Id, pos);
            //e.Handled = true;
        }

        public void StylusMove(object sender, StylusEventArgs e)
        {
            if (e.StylusDevice == null)
                return;

            Point pos = e.GetPosition(_mapbase);
            UpdateLastStylusPos(e.StylusDevice.Id, pos);
            ProcessMove(e.StylusDevice.Id, pos);
            //e.Handled = true;
        }

        public void UpdateLastStylusPos(int stylus_id, Point pos)
        {
            if (!LastStylusPos.ContainsKey(stylus_id))
                LastStylusPos.Add(stylus_id, pos);
            else
                LastStylusPos[stylus_id] = pos;
        }
        public void UpdateLastStylusPos(StylusEventArgs e)
        {
            Point pos = e.GetPosition(_mapbase);
            int stylus_id = e.StylusDevice.Id;

            if (!LastStylusPos.ContainsKey(stylus_id))
                LastStylusPos.Add(stylus_id, pos);
            else
                LastStylusPos[stylus_id] = pos;
        }

        public void StylusLeave(object sender, StylusEventArgs e)
        {
            if (e.StylusDevice == null)
                return;

            if (MultiTouchZoomActive)
            {
                StylusUp(sender, e);
            }

            if (_multi_touch_input_count > 0)
                _multi_touch_input_count--;

            if (_multi_touch_input_count <= 0)
            {
                _multi_touch_input_count = 0;
                _multitouch_translation_active = false;
            }

        }

        public void MultiTouchTranslationStart(Point pos)
        {
            ResetLastTranslVec();
            MultiTouchTranslationActive = true;
            _last_last_transl_stylus_pos = pos;
            if (TranslationChanged != null)
                TranslationChanged(this, null);
        }

        public void ProcessManipulationStarted(object sender, ManipulationStartedEventArgs e)
        {}

        public void ProcessManipulationDelta(object sender, ManipulationDeltaEventArgs e)
        {
            if (ScaleTransform.ScaleX * e.ScaleDelta > BorderMinZoom && ScaleTransform.ScaleX * e.ScaleDelta < BorderMaxZoom && e.ScaleDelta != 1)
            {
                if (!MultiTouchZoomActive)
                    MultiTouchZoomActive = true;

                if (MultiTouchTranslationActive)
                    MultiTouchTranslationActive = false;

                _multitouch_zoomtranslation_active = true;

                double to_scale = ScaleTransform.ScaleX * e.ScaleDelta;
                double old_scale = ScaleTransform.ScaleX;
                
                double stylus_pos_x = -TranslateTransform.X + (e.Location.X);
                double stylus_pos_y = -TranslateTransform.Y + (e.Location.Y);
                double stylus_pos_after_scale_x = (stylus_pos_x * to_scale) / old_scale;
                double stylus_pos_after_scale_y = (stylus_pos_y * to_scale) / old_scale;
                double transl_x = stylus_pos_x - stylus_pos_after_scale_x;
                double transl_y = stylus_pos_y - stylus_pos_after_scale_y;

                ScaleTransform.ScaleX *= e.ScaleDelta;
                ScaleTransform.ScaleY *= e.ScaleDelta;
                _last_transl_stylus_pos.X += transl_x;
                _last_transl_stylus_pos.Y += transl_y;
            }

            _last_transl_stylus_pos.X += e.TranslationDelta.Width;
            _last_transl_stylus_pos.Y += e.TranslationDelta.Height;

            
            //RotateTransform.Angle += e.RotationDelta*180/Math.PI;);)
        }

        public void ProcessManipulationCompleted(object sender, ManipulationCompletedEventArgs e)
        {}

        public void ProcessUp(int id, Point location)
        {
            if (ManipulationProcessor == null)
                return;

            ManipulationProcessor.ProcessUp((uint)id, location.ToDrawingPointF());
        }

        public void ProcessDown(int id, Point location)
        {
            if (ManipulationProcessor == null)
                return;

            ManipulationProcessor.ProcessDown((uint)id, location.ToDrawingPointF());
        }

        public void ProcessMove(int id, Point location)
        {
            if (ManipulationProcessor == null)
                return;

            ManipulationProcessor.ProcessMove((uint)id, location.ToDrawingPointF());
        }
        
        #endregion

        public void SetSelectionRectangleEnabled(Canvas sel_rect_canvas, Brush bg_brush)
        {
            _selregion_canvas = sel_rect_canvas;

            //create a rectangle and add it to the selregion canvas
            _sel_rect = new Border();
            _sel_rect.BorderBrush = new SolidColorBrush(ColorGenerator.HexToColor("#858585"));
            _sel_rect.BorderThickness = new Thickness(1);
            _sel_rect.Visibility = Visibility.Hidden;
            //_sel_rect.Fill = (Brush)FindResource("default_bg_brush");
            _sel_rect.Background = bg_brush;
            _sel_rect.Opacity = 0.4;
            _sel_rect.Width = 0;
            _sel_rect.Height = 0;
            _sel_rect.IsHitTestVisible = false;
            _sel_rect.Focusable = false;
            _selregion_canvas.Children.Add(_sel_rect);
            Canvas.SetTop(_sel_rect, 0);
            Canvas.SetLeft(_sel_rect, 0);

            _with_selection_rect = true;
        }

        #region Properties
        public double ActualMapHeight
        {
            get
            {
                return _map.ActualHeight;
            }
        }

        public double ActualMapWidth
        {
            get
            {
                return _map.ActualWidth;
            }
            
        }

        public bool IsMapMovementActive
        {
            get
            {
                return MapTranslationActive || ZoomAnimationActive || JumpAnimationActive ||TranslationAnimationActive || MultiTouchTranslationActive || MultiTouchZoomActive;

            }
        }
        public bool MapTranslationActive
        {
            get
            {
                return _map_translation_active;
            }
            set
            {
                bool movement_changed = (_map_translation_active != value);
                _map_translation_active = value;

                if (movement_changed && !_map_translation_active)
                {
#if DEBUG
                    Console.WriteLine("completed map_transl");
#endif
                    RaiseMovementAnimationCompleted();
                }

                if (movement_changed && _map_translation_active)
                {
#if DEBUG
                    Console.WriteLine("started map_transl");
#endif
                    RaiseMovementAnimationStarted();
                }
            }
        }
        public bool ZoomAnimationActive
        {
            get
            {
                return _zoom_animation_active;
            }
            set
            {
                bool movement_changed = (_zoom_animation_active != value);
                _zoom_animation_active = value;

                if (movement_changed && !_zoom_animation_active)
                {
#if DEBUG
                    Console.WriteLine("completed zoom " + DateTime.Now.ToShortTimeString());
#endif
                    RaiseZoomAnimationCompleted();
                    RaiseMovementAnimationCompleted();
                }

                if (movement_changed && _zoom_animation_active)
                {
#if DEBUG
                    Console.WriteLine("started zoom");
#endif
                    RaiseZoomAnimationStarted();
                    RaiseMovementAnimationStarted();
                }
            }
        }

        public bool JumpAnimationActive
        {
            get
            {
                return _jump_animation_active;
            }
            set
            {
                bool movement_changed = (_jump_animation_active != value);
                _jump_animation_active = value;

                if (movement_changed && !_jump_animation_active)
                {
#if DEBUG
                    Console.WriteLine("completed jump");
#endif
                    RaiseJumpAnimationCompleted();
                    RaiseMovementAnimationCompleted();
                    RaiseJumpToPointAnimationFinished();
                }

                if (movement_changed && _jump_animation_active)
                {
#if DEBUG
                    Console.WriteLine("started jump");
#endif
                    RaiseJumpAnimationStarted();
                    RaiseMovementAnimationStarted();
                }

            }
        }
        public bool TranslationAnimationActive
        {
            get
            {
                return _translation_animation_active;
            }
            set
            {
                bool movement_changed = (_translation_animation_active != value);
                _translation_animation_active = value;

                if (movement_changed && !_translation_animation_active)
                {
#if DEBUG
                    Console.WriteLine("completed translanim");
#endif
                    RaiseTranslationCompleted();
                    RaiseMovementAnimationCompleted();
                }

//                if (movement_changed && _translation_animation_active)
//                {
//#if DEBUG
//                    Console.WriteLine("started translanim");
//#endif
//                    RaiseTranslationStarted();
//                    RaiseMovementAnimationStarted();
//                }
            }
        }

        public bool MultiTouchTranslationActive
        {
            get
            {
                return _multitouch_translation_active;
            }
            set
            {
                bool movement_changed = (_multitouch_translation_active != value);
                _multitouch_translation_active = value;

                if (movement_changed && !_multitouch_translation_active)
                {
#if DEBUG
                    Console.WriteLine("completed multitouch-transl");
#endif
                    RaiseMovementAnimationCompleted();
                }

                if (movement_changed && _multitouch_translation_active)
                {
#if DEBUG
                    Console.WriteLine("started multitouch-transl");
#endif
                    RaiseMovementAnimationStarted();
                }
            }
        }
        public bool MultiTouchZoomActive
        {
            get
            {
                return _multitouch_zoom_active;
            }
            set
            {
                bool movement_changed = (_multitouch_zoom_active != value);
                _multitouch_zoom_active = value;

                if (movement_changed && !_multitouch_zoom_active)
                {
#if DEBUG
                    Console.WriteLine("completed multitouch-zoom");
#endif
                    RaiseMovementAnimationCompleted();
                }

                if (movement_changed && _multitouch_zoom_active)
                {
#if DEBUG
                    Console.WriteLine("started multitouch-zoom");
#endif
                    RaiseMovementAnimationStarted();
                }
            }
        }

        public bool IsRectangleSelectionActive
        {
            get
            {
                return _selection_active;
            }
        }
               
        protected bool _selection_active
        {
            get
            {
                return _rect_selection_active;
            }
            set
            {
                bool sel_state_changed = (_rect_selection_active != value);
                _rect_selection_active = value;

                if (sel_state_changed && !_rect_selection_active)
                {
                    RaiseRectangularZoomSelectionCompleted();
                }

                if (sel_state_changed && _rect_selection_active)
                {
                    RaiseRectangularZoomSelectionStarted();
                }
            }
        }
        #endregion

        #region Event Raising
        private void RaiseZoomAnimationCompleted()
        {
            if (ZoomAnimationCompleted != null)
                ZoomAnimationCompleted(this, null);
        }

        private void RaiseZoomAnimationStarted()
        {
            if (ZoomAnimationStarted != null)
                ZoomAnimationStarted(this, null);
        }

        private void RaiseJumpAnimationCompleted()
        {
            if (JumpAnimationCompleted != null)
                JumpAnimationCompleted(this, null);
        }

        private void RaiseJumpAnimationStarted()
        {
            if (JumpAnimationStarted != null)
                JumpAnimationStarted(this, null);
        }

        private void RaiseJumpToPointAnimationFinished()
        {
            if (JumpToPointAnimationFinished != null)
                JumpToPointAnimationFinished(this, new EventArgs());
        }


        private void RaiseJumpAnimationMiddlePointReached()
        {
            if (JumpAnimationMiddlePointReached != null)
                JumpAnimationMiddlePointReached(this, new JumpMiddlePointEventArgs(_jump_with_stop.TargetIa));
        }

        private void RaiseJumpAnimationMiddlePointWaitingCompleted()
        {
            if (JumpAnimationMiddlePointWaitingCompleted != null)
                JumpAnimationMiddlePointWaitingCompleted(this, null);
        }

        private void RaiseTranslationCompleted()
        {
            if (TranslationAnimationCompleted != null)
                TranslationAnimationCompleted(this, null);
        }

        private void RaiseTranslationStarted()
        {
            if (TranslationAnimationStarted != null)
                TranslationAnimationStarted(this, null);
        }

        private void RaiseMovementAnimationCompleted()
        {
            if (MovementAnimationCompleted != null)
                MovementAnimationCompleted(this, null);
        }

        private void RaiseMovementAnimationStarted()
        {
            if (MovementAnimationStarted != null)
                MovementAnimationStarted(this, null);
        }


        private void RaiseRectangularZoomSelectionCompleted()
        {
            if (RectangularZoomSelectionCompleted != null)
                RectangularZoomSelectionCompleted(this, null);
        }

        private void RaiseRectangularZoomSelectionStarted()
        {
            if (RectangularZoomSelectionStarted != null)
                RectangularZoomSelectionStarted(this, null);
        }
        #endregion

        #region Attributes
        public event EventHandler MovementAnimationCompleted;
        public event EventHandler MovementAnimationStarted;
        public event EventHandler ZoomAnimationCompleted;
        public event EventHandler ZoomAnimationStarted;
        public event EventHandler JumpAnimationCompleted;
        public event EventHandler JumpAnimationStarted;
        public event EventHandler<JumpMiddlePointEventArgs> JumpToPointAnimationStarted;
        public event EventHandler JumpToPointAnimationFinished;
        public event EventHandler TranslationAnimationCompleted;
        public event EventHandler TranslationAnimationStarted;
        public event EventHandler<JumpMiddlePointEventArgs> JumpAnimationMiddlePointReached;
        public event EventHandler JumpAnimationMiddlePointWaitingCompleted;
        public event EventHandler RectangularZoomSelectionStarted;
        public event EventHandler RectangularZoomSelectionCompleted;

        protected NewtonInterpolation _jump_scale_animation;
        protected NewtonInterpolation _jump_translate_x_animation;
        protected NewtonInterpolation _jump_translate_y_animation;

        //protected CubicBezierCurve _jump_scale_animation_bezier;
        protected Spline _jump_scale_animation_spline;
        protected Spline _jump_transx_animation_spline;
        protected Spline _jump_transy_animation_spline;
        protected CubicBezierCurve _jump_scale_animation_bezier2;
        protected QuadBezierCurve _jump_scale_animation_bezier_quad;
        //protected CubicBezierCurve _jump_translate_x_animation_bezier;
        //protected CubicBezierCurve _jump_translate_y_animation_bezier;

        protected Vector2d _jump_target_ia;
        protected Vector2d _jump_start_ia;
        
        protected double _jump_start_transx;
        protected double _jump_start_transy;

        protected double _jump_target_transx;
        protected double _jump_target_transy;

        protected double _jump_offset_x;
        protected double _jump_offset_y;
        protected double _jump_global_offset_x;
        protected double _jump_global_offset_y;

        protected float _jump_animation_seconds;
        protected int _jump_animation_frames;
        protected Vector2f _jump_mouse_pos;
        protected double _jump_target_scale;
        protected double _jump_start_a;
        protected Stopwatch _jump_animation_watch = new Stopwatch();
        protected bool _jump_animation_active;
        public event EventHandler JumpAnimationFinished;
        protected double _jump_start_scale;
        protected double _jump_end_transx;
        protected double _jump_end_transy;
        protected Vector2d _jump_transl_dir;
        protected Vector2d _jump_transl_start;
        protected Vector2d _jump_transl_end;
        protected bool _jump_to_point;
        protected JumpAnimation _jump_with_stop;
        protected double _map_width;
        protected double _map_height;

        protected FrameworkElement _mapbase;
        protected Canvas _map;

        public Point LastMousePos;
        public bool LeftMouseDown;
        public bool RightMouseDown;

        protected bool _with_selection_rect;

        public bool WithBorderCheck;
        public double BorderCheckerXMax = 0;
        public double BorderCheckerYMax = 0;
        public double BorderCheckerWidth = 0;
        public double BorderCheckerHeight = 0;
        public double BorderCheckCorrSppedFactor = 0.3;
        
        public double BorderMinZoom = 0.001;
        public double BorderMaxZoom = double.MaxValue;

        //sel region attribs
        protected Canvas _selregion_canvas;
        protected bool _rect_selection_active = false;
        protected Border _sel_rect;
        protected Point _sel_rect_pos;
        protected Point _sel_rect_center_pos;
        protected bool _region_selection_active = false;

        //map _translate_transform attribs
        protected bool _map_translation_active = false;
        public Point LastTranslationMousePos;
        public bool IgnoreYTranslation;

        //map zooming animation
        public double WithZoomAnimationAtAlt = double.PositiveInfinity;
        protected readonly double _fov = 90;
        public double ZoomReductionFactor = 0.5;
        public bool WithZoomAnimation = true;
        protected double target_scale_anim = 0;
        protected bool _zoom_animation_active;
        public bool ZoomInActive;
        protected int _curr_zoom_anim_frame = 0;
        public int AnimDurationFramesPerZoomStep = 20;
        public long LastZoomAnimFinished;
        public bool LinearWheelZoomAnimation = false;
        public int _zoom_animation_frames;

        //zoom panning animation helper vars
        protected double _scale_target = 0;
        protected double _transl_target_x = 0;
        protected double _transl_target_y = 0;
        public double ScaleAnimationStepSize = 0;
        protected double _tranlate_step_x_size;
        protected double _tranlate_step_y_size;
        protected double _stepsize_scale_deccl = 0;
        protected double _stepsize_transl_x_deccl = 0;
        protected double _stepsize_transl_y_deccl = 0;

        //manual zoom
        protected bool _manual_zoom_active = false;
        protected double _manual_zoom_speed_factor = 0.0001;
        protected Point _manual_zoom_mouse_pos_start;

        public bool WithBorderCheckWhileTranslationActive;
        public double MouseOffsetX;
        public double MouseOffsetY;
        public ScaleTransform ScaleTransform;
        public TranslateTransform TranslateTransform;

        protected bool _init_transl_animation;
        public bool _translation_animation_active;
        protected Vector _transl_animation_velocity;
        protected double _transl_animation_damping = 0.94;
        protected Vector[] _last_translations = new Vector[10];
        protected DateTime[] _last_translations_timestamp = new DateTime[10];
        protected double _last_translations_time_threshold = 200; //milliseconds
        protected int _count_translation_idle;

        public event EventHandler TranslationChanged;

        public event ZoomChangedEventHandler ZoomChanged;
        public delegate void ZoomChangedEventHandler(object sender, ZoomChangedEventArgs zoom_changed_evargs);
        public delegate void WheelZoomChangedEventHandler(object sender, WheelZoomEventArgs wheel_zoom_evargs);

        #region Touch Attributes

        public ManipulationProcessor ManipulationProcessor;
        protected bool _multitouch_animation_active;
        protected bool _multitouch_translation_active;
        protected bool _multitouch_zoom_active;
        protected bool _multitouch_zoomtranslation_active;
        protected int _multi_touch_input_count;

        public Dictionary<int, Point> LastStylusPos = new Dictionary<int, Point>();
        private Point _last_transl_stylus_pos;
        private Point _last_last_transl_stylus_pos;

        public double StylusOffsetX;
        public double StylusOffsetY;

        protected double _smooth = 0.1;
        protected double _slide = 3;

        protected bool _handler_registered;
        protected bool _explicit_deregister_call;
        #endregion
               
      
        #endregion
    }
}