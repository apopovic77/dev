using System;
using System.Collections.Generic;
using System.Text;
using Logicx.Geo.Geometries;
using D3d = Microsoft.DirectX.Direct3D;
using System.Drawing;
using GeoLib;

namespace Logicx.DirectX.Objects.GeometricPrimitives
{
    public class Rectangle3d : Primitive3d
    {
        public Rectangle3d(SimpleRectangle rect)  {
            this.PrimitiveType = Microsoft.DirectX.Direct3D.PrimitiveType.TriangleStrip;
            m_vertices_mem = new Microsoft.DirectX.Direct3D.CustomVertex.PositionColored[4];

            m_vertices_mem[0].X = rect.Corner4.X;
            m_vertices_mem[0].Y = rect.Corner4.Y;
            m_vertices_mem[0].Z = 0;
            m_vertices_mem[0].Color = Color.Black.ToArgb();

            m_vertices_mem[1].X = rect.Corner1.X;
            m_vertices_mem[1].Y = rect.Corner1.Y;
            m_vertices_mem[1].Z = 0;
            m_vertices_mem[1].Color = Color.Black.ToArgb();

            m_vertices_mem[2].X = rect.Corner3.X;
            m_vertices_mem[2].Y = rect.Corner3.Y;
            m_vertices_mem[2].Z = 0;
            m_vertices_mem[2].Color = Color.Black.ToArgb();

            m_vertices_mem[3].X = rect.Corner2.X;
            m_vertices_mem[3].Y = rect.Corner2.Y;
            m_vertices_mem[3].Z = 0;
            m_vertices_mem[3].Color = Color.Black.ToArgb();

            CreateVertexBuffer();

            FillMode = Microsoft.DirectX.Direct3D.FillMode.WireFrame;
        }
        public Rectangle3d(MathLib.Vector2f upperLeft,float size_Horizontal, float size_Vertical)
        {
            this.PrimitiveType = Microsoft.DirectX.Direct3D.PrimitiveType.TriangleStrip;
            m_vertices_mem = new Microsoft.DirectX.Direct3D.CustomVertex.PositionColored[4];

            m_vertices_mem[0].X = upperLeft.X;
            m_vertices_mem[0].Y = upperLeft.Y - size_Vertical;
            m_vertices_mem[0].Z = 0;
            m_vertices_mem[0].Color = Color.Black.ToArgb();

            m_vertices_mem[1].X = upperLeft.X;
            m_vertices_mem[1].Y = upperLeft.Y;
            m_vertices_mem[1].Z = 0;
            m_vertices_mem[1].Color = Color.Black.ToArgb();

            m_vertices_mem[2].X = upperLeft.X + size_Horizontal;
            m_vertices_mem[2].Y = upperLeft.Y - size_Vertical;
            m_vertices_mem[2].Z = 0;
            m_vertices_mem[2].Color = Color.Black.ToArgb();

            m_vertices_mem[3].X = upperLeft.X + size_Horizontal;
            m_vertices_mem[3].Y = upperLeft.Y;
            m_vertices_mem[3].Z = 0;
            m_vertices_mem[3].Color = Color.Black.ToArgb();

            CreateVertexBuffer();

            FillMode = Microsoft.DirectX.Direct3D.FillMode.WireFrame;
        }
    }
}
