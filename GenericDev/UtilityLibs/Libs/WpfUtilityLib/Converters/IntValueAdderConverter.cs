﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;

namespace Logicx.WpfUtility.Converters
{
    [ValueConversion(typeof(int), typeof(int))]
    public class IntValueAdderConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            int d_val = (int)value;
            int para_val;
            if (parameter is int)
                return d_val + (int)parameter;
            else if (parameter is string && int.TryParse((string)parameter, out para_val))
                return d_val + para_val;
            return d_val;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            int d_val = (int)value;
            int para_val;
            if (parameter is int)
                return d_val - (int)parameter;
            else if (parameter is string && int.TryParse((string)parameter, out para_val))
                return d_val - para_val;
            return d_val;
        }
    }
}
