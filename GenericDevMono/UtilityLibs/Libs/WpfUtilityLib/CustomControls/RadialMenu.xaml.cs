﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Logicx.WpfUtility;

namespace Logicx.WpfUtility.CustomControls
{
    /// <summary>
    /// Interaction logic for RadialMenu.xaml
    /// </summary>
    public partial class RadialMenu : UserControl
    {
        public class RadialMenuClickEventArgs : EventArgs
        {
            public RadialMenuClickEventArgs(int layer_click)
            {
                LayerClicked = layer_click;
            }

            public int LayerClicked;
        }

        public struct RadialMenuEntry
        {
            public RadialMenuEntry(string text)
            {
                Text = text;
                ColorHover   = ColorGenerator.HexToColor("#00ff00");
                ColorDefault = ColorGenerator.HexToColor("#eaeaea");
                ColorPressed = ColorGenerator.HexToColor("#00ff00");
            }

            public string Text;
            public Color ColorHover;
            public Color ColorPressed;
            public Color ColorDefault;
        }

        public RadialMenu()
        {
            InitializeComponent();

            _menu_button_grids = new List<Grid>();
            _menu_entries = new List<RadialMenuEntry>();
        }

        #region Menu functions
        public void PushMenuEntry(RadialMenuEntry entry)
        {
            PushMenuEntry(entry,false);
        }

        public void PushMenuEntry(RadialMenuEntry entry, bool show)
        {
            int layer = _menu_button_grids.Count + 1;
            
            Grid grid_b = new Grid();
            grid_b.RenderTransform = new ScaleTransform() { ScaleX = 1, ScaleY = 1, CenterX = GetRadius(layer), CenterY = GetRadius(layer) };
            grid_b.HorizontalAlignment = HorizontalAlignment.Center;
            grid_b.VerticalAlignment = VerticalAlignment.Center;
            grid_b.Width = grid_b.Height = GetRadius(layer) * 2;
            
            Button b = CreateMenuButton(entry, layer);
            grid_b.Children.Add(b);
            b.Click += new RoutedEventHandler(b_Click);

            Path p = CreateButtonText(entry, layer);
            if (p != null)
            {
                p.Margin = new Thickness(GetRadius(layer) - 3 + (5 * (layer-1)), GetLayerThickness() / 2 - _text_font_size/2f-2, 0, 0);
                p.Style = (Style)FindResource("radial_menu_text_style");
                p.HorizontalAlignment = HorizontalAlignment.Left;
                p.VerticalAlignment = VerticalAlignment.Top;
                grid_b.Children.Add(p);
            }


            //das erste wird per default angezeigt
            if (menu_grid.Children.Count == 0 )
                menu_grid.Children.Add(grid_b);
            else if (show && menu_grid.Children.Count == _menu_button_grids.Count)
                menu_grid.Children.Insert(0,grid_b);
            _menu_button_grids.Add(grid_b);
            _menu_entries.Add(entry);

            


            AdjustCanvasGridPos();
        }
        public void ResetSubMenuEntries()
        {
            for (int i = _menu_button_grids.Count - 1; i > 0; i--)
            {
                if (menu_grid.Children.Contains(_menu_button_grids[i]))
                    menu_grid.Children.Remove(_menu_button_grids[i]);
                _menu_button_grids.RemoveAt(i);
                _menu_entries.RemoveAt(i);

            }
            AdjustCanvasGridPos();
        }
        public void PopMenuEntry()
        {
            if (_menu_button_grids.Count > 0)
            {
                int index = _menu_button_grids.Count - 1;
                Grid g = _menu_button_grids[index];
                if (menu_grid.Children.Contains(g))
                    menu_grid.Children.Remove(g);
                _menu_button_grids.RemoveAt(index);
                _menu_entries.RemoveAt(index);

                AdjustCanvasGridPos();
            }
        }

        public void setRootElemContent(UserControl l_elem) {
            ((Grid)_menu_button_grids[0]).Children.Add(l_elem);
            l_elem.Focusable = false;
            l_elem.MouseDown += new MouseButtonEventHandler(l_elem_MouseDown);
        }

        void l_elem_MouseDown(object sender, MouseButtonEventArgs e) {
            UserControl l_elem = (UserControl)sender;
            Grid l_grid = (Grid)l_elem.Parent;
            Button l_but = (Button)l_grid.Children[0];
            b_Click(l_but, e);
        }

        private void AdjustCanvasGridPos()
        {
            //versetzte das grid im canvas
            menu_grid.Width = GetRadius(menu_grid.Children.Count)*2;
            menu_grid.Height = GetRadius(menu_grid.Children.Count) * 2;
            Canvas.SetTop(menu_grid, GetRadius(_menu_button_grids.Count) - GetRadius(menu_grid.Children.Count));
            Canvas.SetLeft(menu_grid, -GetRadius(menu_grid.Children.Count));
            main_canvas.Height = GetRadius(_menu_button_grids.Count) * 2;
            main_canvas.Width = GetRadius(menu_grid.Children.Count) + 10;
        }

        private void CloseMenuEntries(int layer)
        {
            int count_menuentry_open = menu_grid.Children.Count;
            lock (this){
                _count_to_close = count_menuentry_open - layer - 1;
            }
            
            for (int i = count_menuentry_open-1; i >= layer; i--)
            {
                double scale_form = 1;
                double scale_to = 0.5;

                Grid grid_b = _menu_button_grids[i];
                ((ScaleTransform)grid_b.RenderTransform).ScaleX = scale_form;
                ((ScaleTransform)grid_b.RenderTransform).ScaleY = scale_form;

                Storyboard story_scale = new Storyboard();
                story_scale.Completed += new EventHandler(story_scale_minus_Completed);

                DoubleAnimation scale_x = new DoubleAnimation();
                scale_x.Duration = new Duration(TimeSpan.FromMilliseconds(500));
                scale_x.From = scale_form;
                scale_x.To = scale_to;
                scale_x.DecelerationRatio = 1;
                scale_x.SetValue(Storyboard.TargetPropertyProperty, new PropertyPath("(0).(1)", Canvas.RenderTransformProperty, ScaleTransform.ScaleXProperty));


                DoubleAnimation scale_y = new DoubleAnimation();
                scale_y.Duration = new Duration(TimeSpan.FromMilliseconds(500));
                scale_y.From = scale_form;
                scale_y.To = scale_to;
                scale_y.DecelerationRatio = 1;
                scale_y.SetValue(Storyboard.TargetPropertyProperty, new PropertyPath("(0).(1)", Canvas.RenderTransformProperty, ScaleTransform.ScaleYProperty));

                story_scale.Children.Add(scale_x);
                story_scale.Children.Add(scale_y);

                grid_b.BeginStoryboard(story_scale);
            }
            
        }

        private void story_scale_minus_Completed(object sender, EventArgs e)
        {
            lock (this)
            {
                if (_count_to_close >= 0)
                    menu_grid.Children.RemoveAt(_count_to_close);
                _count_to_close--;

                if (MenuClick != null)
                    MenuClick(new RadialMenuClickEventArgs(_count_to_close + 2));
            }
        }

        
        private void OpenNextMenuEntry(int layer)
        {
            double scale_form = 0.5;
            double scale_to = 1;

            Grid grid_b = _menu_button_grids[layer];
            ((ScaleTransform) grid_b.RenderTransform).ScaleX = scale_form;
            ((ScaleTransform) grid_b.RenderTransform).ScaleY = scale_form;

            menu_grid.Children.Insert(0, grid_b);


            Storyboard story_scale = new Storyboard();

            DoubleAnimation scale_x = new DoubleAnimation();
            scale_x.Duration = new Duration(TimeSpan.FromMilliseconds(500));
            scale_x.From = scale_form;
            scale_x.To = scale_to;
            scale_x.DecelerationRatio = 1;
            scale_x.SetValue(Storyboard.TargetPropertyProperty, new PropertyPath("(0).(1)", Canvas.RenderTransformProperty, ScaleTransform.ScaleXProperty));


            DoubleAnimation scale_y = new DoubleAnimation();
            scale_y.Duration = new Duration(TimeSpan.FromMilliseconds(500));
            scale_y.From = scale_form;
            scale_y.To = scale_to;
            scale_y.DecelerationRatio = 1;
            scale_y.SetValue(Storyboard.TargetPropertyProperty, new PropertyPath("(0).(1)", Canvas.RenderTransformProperty, ScaleTransform.ScaleYProperty));

            story_scale.Children.Add(scale_x);
            story_scale.Children.Add(scale_y);

            grid_b.BeginStoryboard(story_scale);
        }



        #endregion

        #region Button Creation
        protected float GetRadius(int layer)
        {
            return _circle_radius + (GetLayerThickness() * (layer - 1));
        }
        protected float GetLayerThickness()
        {
            return _circle_radius*0.7f;
        }

        protected Button CreateMenuButton(RadialMenuEntry entry, int layer)
        {
            Button b = new Button();
            b.Width = GetRadius(layer) * 2;
            b.Height = GetRadius(layer) * 2;
            b.HorizontalAlignment = HorizontalAlignment.Center;
            b.VerticalAlignment = VerticalAlignment.Center;
            b.Style = (Style)FindResource("radialmenu_circle_style");

            return b;
        }

        private Path CreateButtonText(RadialMenuEntry entry,int layer)
        {
            if (string.IsNullOrEmpty(entry.Text) || layer == 1)
                return null;
            TextCircleTransform tct = new TextCircleTransform(GetRadius(layer) * 0.71f + ((layer-1) * 2), entry.Text, _text_default_padding, _text_font_size, _text_font_weight, _text_font_fam, _text_kerning);
            return tct.Path;
        }
        #endregion

        #region Event Handlers
        protected void b_Click(object sender, RoutedEventArgs e)
        {
            Button b = sender as Button;
            Grid grid_b = b.Parent as Grid;

            int count_menuentry_open = menu_grid.Children.Count;
            int layer_click = _menu_button_grids.IndexOf(grid_b)+1;

            if (count_menuentry_open == layer_click && _menu_button_grids.Count > count_menuentry_open)
            {
                //open next layer if there is another one
                OpenNextMenuEntry(layer_click);

                if (MenuClick != null)
                    MenuClick(new RadialMenuClickEventArgs(layer_click));
            }
            else
            {
                if (layer_click > 1 || count_menuentry_open > 1)
                    //close the layers
                    CloseMenuEntries(layer_click);
            }

            AdjustCanvasGridPos();

        }

        #endregion

        #region Attribs
        public event RadialMenuClickEventHandler MenuClick;
        public delegate void RadialMenuClickEventHandler(RadialMenuClickEventArgs menu_click_evargs);

        //die circle ebene des root buttons
        protected float _circle_radius = 60;
        protected List<RadialMenuEntry> _menu_entries;
        protected List<Grid> _menu_button_grids;

        //helper var
        private int _count_to_close = -1;
        protected int _text_default_padding = 10;
        //protected string _text_font_fam = "Calibri";
        protected string _text_font_fam = "Arial";
        protected FontWeight _text_font_weight = FontWeights.Bold;
        protected int _text_font_size = 16;//22;
        protected float _text_kerning = 3f;
        #endregion


    }
}
