﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CoreLab.Oracle;
using Logicx.Geo.Coordinates;
using MathLib;


namespace Logicx.OracleSpatial
{
    public class SdoGeometryManager
    {
        public static SdoGeometry ReadGeometryObject(OracleObject obj, bool convert_to_ia)
        {
            if (obj.IsNull)
                return new SdoGeometry();

            SdoPoint3D point = new SdoPoint3D();
            if (!(obj["SDO_POINT"] == DBNull.Value || ((OracleObject)obj["SDO_POINT"]).IsNull))
            {
                OracleObject pointObj = (OracleObject)obj["SDO_POINT"];
                if (pointObj.GetOracleValue("Z") != DBNull.Value)
                {
                    //if (convert_to_ia)
                    //    throw new Exception("Cannot transform 3d coordinate to utm");
                    //else
                        point = new SdoPoint3D(Convert.ToSingle(pointObj["X"]), Convert.ToSingle(pointObj["Y"]), Convert.ToSingle(pointObj["Z"]));
                }
                else
                {
                    if (convert_to_ia)
                    {
                        float lon_x = Convert.ToSingle(pointObj["X"]);
                        float lat_y = Convert.ToSingle(pointObj["Y"]);
                        CoordinateTransformation.LonLatToIAXY(ref lon_x,ref lat_y);
                        point = new SdoPoint3D(lon_x, lat_y, 0);
                    }
                    else
                        point = new SdoPoint3D(Convert.ToSingle(pointObj["X"]), Convert.ToSingle(pointObj["Y"]), 0);
                }
            }

            //float[] elemInfo;
            SdoElemInfo[] sdo_elem_infos;
            OracleArray arr;
            if (obj["SDO_ELEM_INFO"] != DBNull.Value)
            {
                arr = (OracleArray)obj["SDO_ELEM_INFO"];
                sdo_elem_infos = new SdoElemInfo[arr.Count/3];
                for (int i = 0; i < arr.Count; i += 3)
                {
                    sdo_elem_infos[i / 3].SdoStartingOffset = (Convert.ToInt32(arr[i]) - 1) / 2;
                    sdo_elem_infos[i / 3].SdoEtype = Convert.ToInt32(arr[i + 1]);
                    sdo_elem_infos[i / 3].SdoInterpretation = Convert.ToInt32(arr[i + 2]);
                }
            }
            else
                sdo_elem_infos = new SdoElemInfo[0];

            Vector2f[] ordinates;
            if (obj["SDO_ORDINATES"] != DBNull.Value)
            {
                arr = (OracleArray)obj["SDO_ORDINATES"];
                ordinates = new Vector2f[arr.Count/2];
                if (convert_to_ia)
                {
                    for (int i = 0; i < arr.Count; i += 2)
                    {
                        float lon_x = Convert.ToSingle(arr[i]);
                        float lat_y = Convert.ToSingle(arr[i + 1]);
                        CoordinateTransformation.LonLatToIAXY(ref lon_x, ref lat_y);
                        ordinates[i/2] = new Vector2f(lon_x,lat_y);
                    }
                }else
                    for (int i = 0; i < arr.Count; i+=2)
                        ordinates[i / 2] = new Vector2f(Convert.ToSingle(arr[i]), Convert.ToSingle(arr[i+1]));
            }
            else
                ordinates = new Vector2f[0];

            if (obj.GetOracleValue("SDO_GTYPE") == DBNull.Value)
                return new SdoGeometry(Convert.ToInt32(obj["SDO_GTYPE"]), Convert.ToSingle(obj["SDO_SRID"]), point, sdo_elem_infos, ordinates);
            else
                return new SdoGeometry(Convert.ToInt32(obj["SDO_GTYPE"]), 0, point, sdo_elem_infos, ordinates);
        }

        public static void UpdateDataBase(SdoGeometry figure, OracleConnection oraConnection, string TableName, string FieldName, float ID, bool convert_ia_to_latlon)
        {

            OracleObject sdoObj;
            OracleObject sdoPointObj;
            OracleArray info;
            OracleArray ordinates;
            try
            {
                oraConnection.Open();
                sdoObj = new OracleObject("MDSYS.SDO_GEOMETRY", oraConnection);
                sdoPointObj = new OracleObject("MDSYS.SDO_POINT_TYPE", oraConnection);
                info = new OracleArray("MDSYS.SDO_ELEM_INFO_ARRAY", oraConnection);
                ordinates = new OracleArray("MDSYS.SDO_ORDINATE_ARRAY", oraConnection);
            }
            catch (OracleException ex)
            {
                oraConnection.Close();
                throw ex;
            }
            if (figure == null)
                sdoObj = null;
            else
            {
                sdoObj["SDO_GTYPE"] = figure.SdoGType;
                if (figure.SdoSrid != 0)
                    sdoObj["SDO_SRID"] = figure.SdoSrid;
                else
                    sdoObj["SDO_SRID"] = null;

                if (figure.SdoPoint.X == 0 && figure.SdoPoint.Y == 0 && figure.SdoPoint.Z == 0)
                {
                    sdoPointObj["X"] = figure.SdoPoint.X;
                    sdoPointObj["Y"] = figure.SdoPoint.Y;
                    sdoPointObj["Z"] = figure.SdoPoint.Z;
                    sdoObj["SDO_POINT"] = sdoPointObj;
                }
                else
                    sdoObj["SDO_POINT"] = null;

                if (figure.SdoElemInfos == null)
                    sdoObj["SDO_ELEM_INFO"] = null;
                else
                {
                    foreach (SdoElemInfo sdo_elem_info in figure.SdoElemInfos)
                    {
                        info.Add(sdo_elem_info.SdoStartingOffset + 1);
                        info.Add(sdo_elem_info.SdoEtype);
                        info.Add(sdo_elem_info.SdoInterpretation);
                    }
                    sdoObj["SDO_ELEM_INFO"] = info;
                }

                if (figure.SdoOrdinates == null)
                    sdoObj["SDO_ORDINATES"] = null;
                else
                {
                    foreach (Vector2f v in figure.SdoOrdinates)
                    {
                        float ia_x = v.X;
                        float ia_y = v.Y;
                        if(convert_ia_to_latlon)
                            CoordinateTransformation.IAXYToLonLat(ref ia_x,ref ia_y);
                        ordinates.Add(ia_x);
                        ordinates.Add(ia_y);
                    }
                    sdoObj["SDO_ORDINATES"] = ordinates;
                }
            }
            OracleCommand command = new OracleCommand();
            command.Connection = oraConnection;
            command.CommandText = string.Format("UPDATE {0} SET {1} = :OBJ WHERE ID = :IDENT", TableName, FieldName);
            command.Parameters.Add("OBJ", OracleDbType.Object).Value = sdoObj;
            command.Parameters.Add("IDENT", OracleDbType.Number).Value = ID;
            try
            {
                command.ExecuteNonQuery();
            }
            finally
            {
                command.Dispose();
            }
        }
    }
}
