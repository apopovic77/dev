﻿using CognitiveLib.Typenschild;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CognitiveLib.WebSearch;

namespace TypeDetectorTestApp
{
    public class Program
    {
        static void Main(string[] args)
        {
            //this var must be set, otherwise google auth does not work
            Environment.SetEnvironmentVariable("GOOGLE_APPLICATION_CREDENTIALS", "google.cloudservice.auth.json");
            
            //initiate right from the beginning the instance
            GeizhalsSelenium sel = GeizhalsSelenium.Instance();


            string path_to_images = @"C:\AwantoRepo\Experiments\ProofOfConcept\Cognitive\Data\images";
            string[] files = Directory.GetFiles(path_to_images, "*.jpg");
            foreach (string path_to_img in files)
            {
                TypeDetectionResult res = DeviceInfoDetector.Detect(path_to_img);
                Console.WriteLine("====================");
                Console.WriteLine("Brand: " + res.Brand.BestStringMatchByCount);
                Console.WriteLine("BrandByModel: " + res.ModelNoDR.Brand);
                Console.WriteLine("Code: " + res.ModelNoDR.ModelNo);
            }

            GeizhalsSelenium.Dispose();
        }
    }
}
