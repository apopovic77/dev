using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using Logicx.DirectX.Objects.Fonts;
using Microsoft.DirectX;
using Microsoft.DirectX.Direct3D;
using System.Windows.Forms;
using System.Drawing;
using System.Runtime.InteropServices;
using Logicx.DirectX.Management;
using DirectXLib.Engine;
using Logicx.DirectX.Utilities;
using Logicx.DirectX.Objects;
using MathLib;
using Logicx.DirectX.Objects.Hud;

namespace Logicx.DirectX.Engine
{
    public class Engine3d : IDisposable
    {
        #region Construction & Initialization
        public Engine3d(Control parent,DirectXManager dxmanager) 
        {
           
            _parent = parent;
            
            DirectXManager = dxmanager;
            DirectXManager.DxDeviceLost += new EventHandler(DirectXDeviceLost);
            DirectXManager.DxDeviceReset += new EventHandler(DirectXDeviceReset);
            DirectXManager.DxResizing += new CancelEventHandler(DirectXResizing);
            DirectXManager.DxDisposing += new EventHandler(DirectXDisposing);

            Camera = new Camera(new Vector3(0, 0, -100f));
            _3dobjects_general = new List<Object3d>();


            Hud = new Hud();
            Font = new FontCache3d();
            
        }

        public void InitRunLoop() 
        {
            Application.Idle += new EventHandler(Application_Idle);
        }

        public virtual void InitEngine() {
            
            Hud.Init();
            Font.Init();

            Camera.SetProjectionMatrix();
            Camera.MoveCamera();

            //3. INIT STD BEHAVIOUR
            Device3d.TextureState[0].ColorOperation = TextureOperation.Modulate;
            Device3d.TextureState[0].ColorArgument1 = TextureArgument.TextureColor;
            Device3d.TextureState[0].ColorArgument2 = TextureArgument.Diffuse;
            Device3d.TextureState[0].AlphaOperation = TextureOperation.SelectArg1;
            Device3d.TextureState[0].AlphaArgument1 = TextureArgument.TextureColor;
            Device3d.SamplerState[0].MinFilter = TextureFilter.Linear;
            Device3d.SamplerState[0].MagFilter = TextureFilter.Linear;
            Device3d.SamplerState[0].MipFilter = TextureFilter.Linear;
            Device3d.SamplerState[0].AddressU = TextureAddress.Clamp;
            Device3d.SamplerState[0].AddressV = TextureAddress.Clamp;
            Device3d.RenderState.DitherEnable = true;

            Device3d.RenderState.AlphaBlendEnable = true;
            Device3d.RenderState.SourceBlend = Blend.SourceAlpha;
            Device3d.RenderState.DestinationBlend = Blend.InvSourceAlpha;
            Device3d.RenderState.BlendOperation = BlendOperation.Add;

            //Device3d.RenderState.CullMode = Cull.CounterClockwise;
            Device3d.RenderState.CullMode = Cull.None;

            Device3d.RenderState.ZBufferEnable = false;
            Device3d.RenderState.ZBufferWriteEnable = true;
            Device3d.RenderState.ZBufferFunction = Compare.LessEqual;

            //4. init light stuff
            // Licht
            Device3d.RenderState.Lighting = true;
            Device3d.RenderState.Ambient = Color.DimGray;
            Device3d.Lights[0].Type = LightType.Directional;
            Device3d.Lights[0].Diffuse = Color.White;
            Device3d.Lights[0].Direction = new Vector3(0, 0, 1);
            Device3d.Lights[0].Enabled = true;
            Device3d.RenderState.SpecularEnable = true;
        }
        #endregion

        #region Render Loop Kernel
        protected virtual void RenderingNextFrame()
        {
            HandleInput();
            RenderObjects(_3dobjects_general);
            Hud.Render(m_elapsedTime);
            Font.Render(m_elapsedTime);
        }

        private void RenderObjects(List<Logicx.DirectX.Objects.Object3d> objects3d)
        {
            lock (objects3d)
            {
                for (int i = 0; i < objects3d.Count; i++)
                {
                    Logicx.DirectX.Objects.Object3d obj = objects3d[i];
                    obj.Render(m_elapsedTime);

                    //check if object still alive
                    if (!obj.IsAlive())
                    {
                        objects3d.RemoveAt(i);
                        i--;
                        continue;
                    }
                }
            }
        }

        /// <summary>
        /// entry function for next render frame, this function is responsible for the painting of the 3d content
        /// </summary>
        public void RenderNextFrame()
        {
            if (AppStillIdle)
            {
                if ((Device3d != null) && (!Device3d.Disposed))
                {
                    UpdateRenderStats();

                    if (m_elapsedTime == 0)
                    {
                        //dont render next frame if no time elapsed since last render
                        _parent.Invalidate(new Rectangle(new Point(0, 0), new Size(10, 10)));
                        return;
                    }

                    try
                    {
                        Device3d.Clear(ClearFlags.ZBuffer | ClearFlags.Target, m_render_clear_color, 1.0f, 0);
                        Device3d.BeginScene();

                        RenderingNextFrame();

                        Device3d.EndScene();
                        Device3d.Present();
                    }
                    // F�ngt Ausnahmen ab, die bei einer Gr��en�nderung des
                    // Fensters auftreten k�nnen.
                    catch 
                    { 
                    }


                    
                }
                //System.Threading.Thread.Sleep(m_sleep_between_renderframe);
            }
            if (AppStillIdle)
            {
                _parent.Invalidate(new Rectangle(new Point(0, 0), new Size(10, 10)));
            }
        }

        protected void UpdateRenderStats()
        {

            ////////////
            //update stats
            // Get the app's time, in seconds. Skip rendering if no time elapsed
            m_renderframe++;
            float fAppTime = DXUtilities3d.Timer(TIMER.GETAPPTIME);
            float fElapsedAppTime = DXUtilities3d.Timer(TIMER.GETELAPSEDTIME);
            if (0.0f == fElapsedAppTime) return;
            m_appTime = fAppTime;
            m_elapsedTime = fElapsedAppTime;
            // Keep track of the frame count
            float time = DXUtilities3d.Timer(TIMER.GETABSOLUTETIME);
            ++m_frames;
            // Update the scene stats once per second
            if (time - m_lastTime > 3.0f)
            {
                m_framesPerSecond = m_frames / (time - m_lastTime);
                m_lastTime = time;
                m_frames = 0;
            }
            ////////////
        }

        #region app free for rendering determination

        /// <summary>
        /// Starts the render loop when ever the application is idle. The render loop will continue until 
        /// there is a message to process.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual void Application_Idle(object sender, EventArgs e)
        {

            if (AppStillIdle)
            {
                _parent.Invalidate(new Rectangle(new Point(0, 0), new Size(10, 10)));
            }
        }


        /// <summary>
        /// Checks to see if the application is idel or if there is a message 
        /// waiting to be processed.
        /// </summary>
        public virtual bool AppStillIdle
        {
            get
            {
                Message msg;
                return !PeekMessage(out msg, IntPtr.Zero, 0, 0, 0);
            }
        }

        /// <summary>
        /// Required by <c>PeekMessage</c>
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        protected struct Message
        {
            public IntPtr hWnd;
            public int msg;
            public IntPtr wParam;
            public IntPtr lParam;
            public uint time;
            public System.Drawing.Point p;
        }

        /// <summary>
        /// Checks if there is a message waiting to be processed.
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="hWnd"></param>
        /// <param name="messageFilterMin"></param>
        /// <param name="messageFilterMax"></param>
        /// <param name="flags"></param>
        /// <returns><c>true</c> if there is a message waiting to be processed.</returns>
        [System.Security.SuppressUnmanagedCodeSecurity] // We won't use this maliciously
        [DllImport("User32.dll", CharSet = CharSet.Auto)]
        protected static extern bool PeekMessage(out Message msg, IntPtr hWnd, uint messageFilterMin, uint messageFilterMax, uint flags);
        #endregion
 
        #endregion

        #region Handling Input
        protected virtual void HandleInput()
        {
            //translate the map if left mousebutton held down
            if (LeftMouseButtonDown && KeyDown[18] && !KeyDown[17])
            {
                TranslateCamZZero();
            }
            //zoom view
            else if (RightMouseButtonDown)
            {
                ZoomCam();
            }
            //rotate view
            else if (LeftMouseButtonDown && KeyDown[17])
            {
                RotateCam();

            }

            //get mousepos in 3d coords
            GetMousePos3DZZero();

            //setz die jetzige pos als neue aktuelle last pos
            m_mouse_lastpos = new Point(Convert.ToInt32(MouseposX), Convert.ToInt32(MouseposY));
        }

        private void ZoomCam()
        {
            //berechne die relative Bewegung der Mouse
            Point rel_move_mouse = new Point();
            rel_move_mouse.X = m_mouse_lastpos.X - Convert.ToInt32(MouseposX);
            rel_move_mouse.Y = Convert.ToInt32(MouseposY) - m_mouse_lastpos.Y;

            //calc cam moveup speed, for 2d landscape viewing formula = y = kx + d --> alt_change_vel = 50/5900 * z + 0
            float altitude_change_velocity = (50f / 5900f) * Camera.Position.Z;
            float cam_altitude = rel_move_mouse.Y * altitude_change_velocity;

            Vector3 camdir = Camera.GetCameraDirectionVector();
            camdir.X *= cam_altitude;
            camdir.Y *= cam_altitude;
            camdir.Z *= cam_altitude;

            Camera.Position.X -= camdir.X;
            Camera.Position.Y -= camdir.Y;
            Camera.Position.Z -= camdir.Z;

            float min_zoom_in = -10;

            //grenze den bewegungspielraum ein
            if (Camera.Position.Z >= min_zoom_in)
                Camera.Position.Z = min_zoom_in; //man kann nicht weiter reinzoomen
            else if (Camera.Position.Z < -1000000)
                Camera.Position.Z = -1000000;

            Camera.MoveCamera();
        }

        private void RotateCam()
        {
            //berechne die relative Bewegung der Mouse
            Point rel_move_mouse = new Point();
            rel_move_mouse.X = m_mouse_lastpos.X - Convert.ToInt32(MouseposX);
            rel_move_mouse.Y = Convert.ToInt32(MouseposY) - m_mouse_lastpos.Y;

            //ver�nder die pos der camera um genau die relative bewegung der mouse
            Camera.AdjustRoll(rel_move_mouse.Y * 0.2f);
            Camera.AdjustHeading(rel_move_mouse.X * 0.2f);

            Camera.MoveCamera();
        }

        private void TranslateCamZZero()
        {
            //berechne die relative Bewegung der Mouse
            Point rel_move_mouse = new Point();
            rel_move_mouse.X = m_mouse_lastpos.X - Convert.ToInt32(MouseposX);
            rel_move_mouse.Y = Convert.ToInt32(MouseposY) - m_mouse_lastpos.Y;

            //cam movement in 2d Perspective
            float screen_half_width = Convert.ToSingle((Math.Tan(Camera.FieldOfView * 0.64f) * Camera.Position.Z));
            float screen_half_height = Convert.ToSingle((Math.Tan(Camera.FieldOfView * 0.64f / Camera.AspectRatio * 1.037f) * Camera.Position.Z));
            float cam_move_x = screen_half_width * rel_move_mouse.X / (DirectXManager.DxWindowWidth / 2);
            float cam_move_y = screen_half_height * rel_move_mouse.Y / (DirectXManager.DxWindowHeight / 2);
            if (Camera.Attitude.Roll != -PI_half)
            {
                cam_move_x *= 2;
                cam_move_y *= 2;
            }

            //ver�nder die pos der camera um genau die relative bewegung der mouse
            Camera.MoveRelCamera(-cam_move_x, -cam_move_y, 0);
        }
        private void GetMousePos3DZZero()
        {
            //get current mouse utm
            float roll_rad = Engine3d.Camera.Attitude.Roll;
            float heading_rad = Engine3d.Camera.Attitude.Heading;

            //p on screen where mouse is located in world coords
            //diese formel hab ich aus einem buch kap. picking
            float px = ((2f * MouseposX / DirectXManager.DxWindowWidth) - 1) * (1 / Camera.Projection.M11);
            float py = ((-2f * MouseposY / DirectXManager.DxWindowHeight) + 1) * (1 / Camera.Projection.M22);
            float pz = 1;

            //blick direkt richtung z entlang
            if (Engine3d.Camera.Attitude.Roll == -PI_half)
            {
                Vector3 p = new Vector3(px, py, pz);
                p.TransformCoordinate(Matrix.RotationZ(heading_rad - Convert.ToSingle(Math.PI / 2)));
                //berechne den dpunkt auf z = 0
                p.Normalize();
                Vector3 v_dpunkt = Vector3.Empty;
                float len_dir_vec_bis_zum_durchstosspunkt = -Engine3d.Camera.Position.Z / p.Z;
                v_dpunkt.X = Engine3d.Camera.Position.X + p.X * len_dir_vec_bis_zum_durchstosspunkt;
                v_dpunkt.Y = Engine3d.Camera.Position.Y + p.Y * len_dir_vec_bis_zum_durchstosspunkt;
                v_dpunkt.Z = 0.0f;

                //leg die mousepos info fest
                Mousepos3dX = v_dpunkt.X;
                Mousepos3dY = v_dpunkt.Y;

                Mousepos3d = new Vector2f(Mousepos3dX, Mousepos3dY);
            }
            else
            {
                float alpha_x = Convert.ToSingle(Math.Atan(px));
                float alpha_y = Convert.ToSingle(Math.Atan(py));

                Vector3 v_inertial = new Vector3(0, 0, 1);
                Vector3 v_p = new Vector3(px, py, pz);
                v_inertial.TransformCoordinate(Matrix.RotationX(-roll_rad - (float)Math.PI / 2));
                v_p.TransformCoordinate(Matrix.RotationX(-roll_rad - (float)Math.PI / 2));
                Vector3 v_ortho = new Vector3(0, -v_inertial.Z, v_inertial.Y);
                v_p.TransformCoordinate(Matrix.RotationZ(+heading_rad - (float)Math.PI / 2));

                //berechne den dpunkt auf z = 0
                v_p.Normalize();
                Vector3 v_dpunkt = Vector3.Empty;
                float len_dir_vec_bis_zum_durchstosspunkt = -Engine3d.Camera.Position.Z / v_p.Z;
                v_dpunkt.X = Engine3d.Camera.Position.X + v_p.X * len_dir_vec_bis_zum_durchstosspunkt;
                v_dpunkt.Y = Engine3d.Camera.Position.Y + v_p.Y * len_dir_vec_bis_zum_durchstosspunkt;
                v_dpunkt.Z = 0.0f;
                //leg die mousepos info fest
                Mousepos3dX = v_dpunkt.X;
                Mousepos3dY = v_dpunkt.Y;

                Mousepos3d = new Vector2f(Mousepos3dX, Mousepos3dY);
            }
        }

        #endregion

        #region 3d Object Handling
        public static void AddObject(Object3d obj)
        {
            lock (_3dobjects_general)
            {
                _3dobjects_general.Add(obj);
            }
        }
        public static void ClearObjects()
        {
            lock (_3dobjects_general)
            {
                _3dobjects_general.Clear();
            }
        }
        #endregion

        #region DirectX Manager EventHandling
        /// <summary>
        /// Whenever the user resizes the form, the device will become �lost�. This means that all the resources will be discarded.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual void DirectXDeviceLost(object sender, EventArgs e)
        {
        }
        /// <summary>
        /// When the app regains focus (eg. Immediately after the resizing action) the device will be reset
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual void DirectXDeviceReset(object sender, EventArgs e)
        {
            InitEngine();
        }
        protected virtual void DirectXDisposing(object sender, EventArgs e)
        {
        }
        protected virtual void DirectXResizing(object sender, CancelEventArgs e)
        {
            m_resize_happend = true;
            if (!m_allow_dynmic_resizing)
                e.Cancel = true;
            return;
        }
        #endregion

        #region Destruktor
        private bool m_disposed = false;
        ~Engine3d()
        {
            Dispose(false);
        }
        public void Dispose()
        {
            Dispose(true);
            System.GC.SuppressFinalize(this);
        }
        public virtual void Dispose(bool explicitCall)
        {
            if (!this.m_disposed)
            {
                if (explicitCall)
                {
                    //explicit call object are available perform custom cleanup
                }
            }
            m_disposed = true;
        }
        #endregion

        #region Attribs
        #region Engine Attribs
        public static Camera Camera;
        public static Device Device3d { get { return DirectXManager.Device3d; } }
        public static DirectXManager DirectXManager;
        #endregion

        public static Hud Hud;

        #region Render Loop Attribs
        protected bool m_pause = false;
        protected int m_sleep_between_renderframe = 10;
        protected int m_render_clear_color = Color.Black.ToArgb();
        protected bool m_allow_dynmic_resizing = false;
        protected bool m_resize_happend = false; // sets true if there was a resize
        protected bool _appisidle = true;
        protected Control _parent;
        #endregion

        #region Input Attribs
        public static bool[] KeyDown = new bool[256];
        public static bool LeftMouseButtonDown = false;
        public static bool RightMouseButtonDown = false;
        protected Point m_mouse_lastpos;
        public static float MouseposX = 0;
        public static float MouseposY = 0;
        public static float Mousepos3dX = 0;
        public static float Mousepos3dY = 0;
        public static Vector2f Mousepos3d;
        #endregion

        protected static List<Logicx.DirectX.Objects.Object3d> _3dobjects_general;
        public static FontCache3d Font;

        #region Helper Value Vars
        protected readonly float PI = Convert.ToSingle(Math.PI);
        protected readonly float PI_half = Convert.ToSingle(Math.PI / 2);
        protected readonly float PI_quad = Convert.ToSingle(Math.PI / 4);
        #endregion

        #region Render Stats Attribs
        public static float AppTime { get { return m_appTime; } }
        protected static float m_appTime;
        protected float m_lastTime = 0.0f;
        protected float m_elapsedTime;
        protected int m_frames = 0;
        protected static uint m_renderframe = 0;
        public static float FPS { get { return m_framesPerSecond; } }
        protected static float m_framesPerSecond;
        public static uint RenderFrame { get { return m_renderframe; } }
        #endregion
        #endregion

        #region 3d Config Helper Methods
        public static Usage GetUsageFlagForVB(bool withReading)
        {
            if (DirectXManager.GraphicsSettings.VertexProcessingType == VertexProcessingType.PureHardware)
            {
                if (withReading)
                    return Usage.Dynamic;
                else
                    return Usage.WriteOnly;
            }
            else
            {
                return Usage.SoftwareProcessing;
            }
        }

        public static Pool GetPoolFlagForVB()
        {
            if (DirectXManager.GraphicsSettings.VertexProcessingType == VertexProcessingType.PureHardware)
            {
                return Pool.Default;
            }
            else
            {
                return Pool.SystemMemory;
            }
        }
        #endregion
    }
}
