﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Text.RegularExpressions;
using System.Windows.Input;

namespace Logicx.WpfUtility
{
    public class TextBoxFiltered : TextBox
    {

        public TextBoxFiltered()
        {
            if (IsContextMenuAvailable)
                InitContextMenu();
        }

        private void InitContextMenu()
        {
            try
            {
                _cut = new MenuItem();
                Image icon1 = new Image();
                icon1.Source = new System.Windows.Media.ImageSourceConverter().ConvertFromString("pack://application:,,,/WpfUtilityLib;component/Resources/Graphics/cut.png") as System.Windows.Media.ImageSource;
                _cut.Icon = icon1;
                _cut.Header = "Ausschneiden";
                _cut.Click += new RoutedEventHandler(cut_Click);

                _copy = new MenuItem();
                Image icon2 = new Image();
                icon2.Source = new System.Windows.Media.ImageSourceConverter().ConvertFromString("pack://application:,,,/WpfUtilityLib;component/Resources/Graphics/page_copy.png") as System.Windows.Media.ImageSource;
                _copy.Icon = icon2;
                _copy.Header = "Kopieren";
                _copy.Click += new RoutedEventHandler(copy_Click);

                _paste = new MenuItem();
                Image icon3 = new Image();
                icon3.Source = new System.Windows.Media.ImageSourceConverter().ConvertFromString("pack://application:,,,/WpfUtilityLib;component/Resources/Graphics/page_paste.png") as System.Windows.Media.ImageSource;
                _paste.Icon = icon3;
                _paste.Header = "Einfügen";
                _paste.Click += new RoutedEventHandler(paste_Click);
                
                ContextMenu = new ContextMenu();
                ContextMenu.Items.Add(_cut);
                ContextMenu.Items.Add(_copy);
                ContextMenu.Items.Add(_paste);
            }
            catch (Exception)
            {
            }

            MouseRightButtonUp += new System.Windows.Input.MouseButtonEventHandler(TextBoxFiltered_MouseRightButtonUp);
        }

        void paste_Click(object sender, RoutedEventArgs e)
        {
            Paste();
        }

        void copy_Click(object sender, RoutedEventArgs e)
        {
            Copy();
        }

        void cut_Click(object sender, RoutedEventArgs e)
        {
            Cut();
        }


        void TextBoxFiltered_MouseRightButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (ContextMenu != null)
            {
                ContextMenu.IsOpen = true;

                if (SelectedText.Length > 0)
                {
                    _cut.IsEnabled = true;
                    ((Image)_cut.Icon).Opacity = 1;
                    _copy.IsEnabled = true;
                    ((Image)_copy.Icon).Opacity = 1;
                }
                else
                {
                    _cut.IsEnabled = false;
                    ((Image)_cut.Icon).Opacity = 0.5;
                    _copy.IsEnabled = false;
                    ((Image)_copy.Icon).Opacity = 0.5;
                }

                _paste.IsEnabled = Clipboard.ContainsText();
                ((Image)_paste.Icon).Opacity = Clipboard.ContainsText() ? 1: 0.5;
            }
        }

        protected override void OnLostFocus(System.Windows.RoutedEventArgs e)
        {
            base.Text = Regex.Replace(base.Text, FilterExpression, ReplaceString);
            base.OnLostFocus(e);           
        }

        #region Properties
        public string FilterExpression
        {
            get
            {
                return _filter_expression;
            }
            set
            {
                _filter_expression = value;
            }
        }

        public string ReplaceString
        {
            get
            {
                return _replace_string;
            }
            set
            {
                _replace_string = value;
            }
        }

        #endregion

        #region Dependency Properties


        public bool IsContextMenuAvailable
        {
            get { return (bool)GetValue(IsContextMenuAvailableProperty); }
            set { SetValue(IsContextMenuAvailableProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsContextMenuAvailable.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsContextMenuAvailableProperty =
            DependencyProperty.Register("IsContextMenuAvailable", typeof(bool), typeof(TextBoxFiltered), new UIPropertyMetadata(true));


        #endregion

        private string _filter_expression = @"[\u0000-\u0008]|[\u000B-\u000C]|[\u000E-\u0019]|[\u007F-\u009F]";//@"[\u0000-\u0008] [\u0011-\u0012] [\u0014-\u0029] [\u0031]"; //@"[^\u0000-\u007F]";
        private string _replace_string = " ";

        private MenuItem _cut;
        private MenuItem _copy;
        private MenuItem _paste;
    }
}
