﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Logicx.Utilities;
using Logicx.WpfUtility.ZoomingPanning;

namespace Logicx.WpfUtility.CustomControls
{
    /// <summary>
    /// Interaction logic for ImageViewer.xaml
    /// </summary>
    public partial class ImageViewer : UserControl
    {
        public ImageViewer()
        {
            InitializeComponent();

            _map_zp = new MapZP(this,canvas);
            _map_zp.DeRegisterEventHandler();

            RegisterRenderFrame();
        }

        void RenderFrame(object sender, EventArgs e)
        {
            _map_zp.Update();
        }

        public void RemoveImage()
        {
            image.Source = null;
            Canvas.SetTop(image, 0);
            Canvas.SetLeft(image, 0);
        }

        public void SetImage(ImageSource img_source, double img_width, double img_height)
        {
            image.Source = img_source;
            image.Width = img_width;
            image.Height = img_height;

            RenderOptions.SetBitmapScalingMode(image, BitmapScalingMode.LowQuality);
            RenderOptions.SetCacheInvalidationThresholdMinimum(image, 0.25);
            RenderOptions.SetCacheInvalidationThresholdMaximum(image, 4.0);
            RenderOptions.SetCachingHint(image, CachingHint.Cache);

            DoCenterImageOnScreen(img_width, img_height);
        }

        public void RegisterRenderFrame()
        {
            if (!_renderframe_registered)
            {
                CompositionTarget.Rendering += RenderFrame;
                _map_zp.RegisterEventHandler();
            }
            _renderframe_registered = true;

        }
        public void UnRegisterRenderFrame()
        {
            if (_renderframe_registered)
            {
                CompositionTarget.Rendering -= RenderFrame;
                _map_zp.DeRegisterEventHandler();
            }
            _renderframe_registered = false;
        }

        /// <summary>
        /// positionsiert das image unabhängig von der dim des bildes so dass der ganze image viewer bereich ausgenutzt wird
        /// </summary>
        private void DoCenterImageOnScreen(double img_width, double img_height)
        {
            //get img viewer dimensions
            double dim_canvas = canvas.ActualWidth/canvas.ActualHeight;
            double dim_image = img_width/img_height;

            _initial_scale = 1;
            if(dim_canvas < dim_image)
            {
                _initial_scale = canvas.ActualWidth / img_width;
                Canvas.SetTop(image, (canvas.ActualHeight / _initial_scale - img_height) / 2);
            }
            else if (dim_canvas > dim_image)
            {
                _initial_scale = canvas.ActualHeight / img_height;
                Canvas.SetLeft(image, (canvas.ActualWidth / _initial_scale - img_width) / 2);
            }
            else
            {
                _initial_scale = canvas.ActualWidth / img_width;
            }

            _map_zp.BorderMinZoom = _initial_scale;
            _map_zp.BorderMaxZoom = double.PositiveInfinity;
            _map_zp.ScaleTransform.ScaleX = _initial_scale;
            _map_zp.ScaleTransform.ScaleY = _initial_scale;

            //center austria map on screen
            _map_zp.TranslateTransform.X = 0;
            _map_zp.TranslateTransform.Y = 0;

            //update border boundery for map_zp
            _map_zp.WithBorderCheck = true;
            _map_zp.BorderCheckerXMax = 0;
            _map_zp.BorderCheckerYMax = 0;
            _map_zp.BorderCheckerWidth = canvas.ActualWidth / _initial_scale;
            _map_zp.BorderCheckerHeight = canvas.ActualHeight / _initial_scale;
        }

        public void ZoomCenter(int steps)
        {
            _map_zp.LastMousePos = new Point(canvas.ActualWidth/2, canvas.ActualHeight/2);
            _map_zp.Zoom(steps);
        }


        #region EventHandlers
        #region Mouse Events
        protected override void OnMouseMove(MouseEventArgs e)
        {
            _map_zp.MouseMove(this, e);
        }
        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            if(IgnoreRightMouseButtonDown && e.RightButton != MouseButtonState.Pressed)
                _map_zp.MouseDown(this, e);
        }
        protected override void OnMouseUp(MouseButtonEventArgs e)
        {
            _map_zp.MouseUp(this, e);
        }
        protected override void OnMouseWheel(MouseWheelEventArgs e)
        {
            if(!IgnoreMouseWheel)
                _map_zp.MouseWheel(this, e);
        }
        protected override void OnMouseEnter(MouseEventArgs e)
        {
            _map_zp.MouseEnter(this, e);

            //_map_zp.LeftMouseDown = true;
            //Point pos = e.GetPosition(canvas);
            //_map_zp.MapTranslationStart(pos);
        }
        protected override void OnMouseLeave(MouseEventArgs e)
        {
            _map_zp.MouseLeave(this, e);
        }
        #endregion
        #endregion

        #region Properties
        public bool IgnoreRightMouseButtonDown { set; get; }
        public bool IgnoreMouseWheel { set; get; }
        public Canvas GetRootCanvas
        {
            get
            {
                return canvas;
            }
        }
        public Image GetImage
        {
            get
            {
                return image;
            }
        }
        protected MapZP MapZP
        {
            get
            {
                return _map_zp;
            }
        }
        #endregion

        #region Attribs
        protected bool _renderframe_registered;
        protected MapZP _map_zp;
        protected double _initial_scale = 0;
        #endregion
    }
}