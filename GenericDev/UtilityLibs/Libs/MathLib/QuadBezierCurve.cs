﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MathLib
{
    public class QuadBezierCurve
    {
        Vector2d _p0, _p1, _p2;

        public QuadBezierCurve(Vector2d p0, Vector2d p1, Vector2d p2)
        {
            _p0 = p0;
            _p1 = p1;
            _p2 = p2;
        }

        public Vector2d Calc(double t)
        {
            Vector2d point_res = _p0 * ((1f - t) * (1f - t)) +
                                 _p1 * (2f * t * (1f - t)) +
                                 _p2 * (t * t);            
            return point_res;
        }
    }
}
