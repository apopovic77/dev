﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace LoggingLib.Configuration
{
    /// <summary>
    /// Category element which defines category names
    /// </summary>
    public class CategoryElement : ConfigurationElement
    {
        /// <summary>
        /// The name of the category. This is a unique key value.
        /// A category name cannot be used twice or more often.
        /// </summary>
        [ConfigurationProperty("Name", DefaultValue = "", IsKey = true, IsRequired = true)]
        public string Name
        {
            get
            {
                return ((string)(base["Name"]));
            }

            set
            {
                base["Name"] = value;
            }
        }
    }
}
