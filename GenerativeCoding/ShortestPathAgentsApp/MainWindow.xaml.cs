﻿ using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using GeneticProgrammingLib;
 using Logicx.Utility;
 using Logicx.WpfUtility.ZoomingPanning;
 using Logicx.WpfUtility4.ConfigUi;
 using MathLib;
using ShortestPathAgentsApp.GeneticAlgorithm;

namespace ShortestPathAgentsApp
{
    public class Wpf
    {
        public static Canvas Canvas;
        public static TextBlock DebugTb { get; set; }
        public static WriteableBitmap Bmp;
    }

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Image i;
        private WriteableBitmap writeableBitmap;
        public MainWindow()
        {
            InitializeComponent();

            //SmartAgentConfig.CreateInstance("SmartAgentApp.config");
            SmartAgentConfig.CreateInstance();
            List<SystemConfig> configs = new List<SystemConfig>(1);
            configs.Add(SmartAgentConfig.Instance);

            ConfigUi config_ui = new ConfigUi(configs,"",ConfigUi.SplitType.FirstUpperCamelCaseChar);
            config_ui.Margin = new Thickness(30);
            grid.Children.Add(config_ui);

            WindowState = WindowState.Maximized;
            
            this.Loaded += MainWindow_Loaded;
            
           
        }

        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            Wpf.Canvas = canvas;
            Wpf.DebugTb = debug_db;
            Random _r = new Random();

            i = new Image();
            RenderOptions.SetBitmapScalingMode(i, BitmapScalingMode.NearestNeighbor);
            RenderOptions.SetEdgeMode(i, EdgeMode.Aliased);
            writeableBitmap = new WriteableBitmap(
                (int)ActualWidth,
                (int)ActualHeight,
                96,
                96,
                PixelFormats.Bgra32,
                null);

            i.Source = writeableBitmap;
            Wpf.Bmp = writeableBitmap;
            i.Stretch = Stretch.None;
            canvas.Children.Add(i);


            Vector2d target_pos = new Vector2d(_r.Next(1, (int)ActualWidth), _r.Next(1, (int)ActualHeight));
            //Vector2d initial_pos = new Vector2d(900, _r.Next(1, (int)ActualHeight));
            //Vector2d initial_pos = new Vector2d(ActualWidth - 100, ActualHeight / 2);
            Vector2d initial_pos = new Vector2d(ActualWidth/2, ActualHeight / 2);

            //RectangleObstacle robstacle = new RectangleObstacle(300,20,300,300, 0);
            //canvas.Children.Add(robstacle.Shape);

            RectangleObstacle top = new RectangleObstacle(this.ActualWidth, 10, 0, 0, 0);
            canvas.Children.Add(top.Shape);

            RectangleObstacle bottom = new RectangleObstacle(this.ActualWidth, 10, 0, ActualHeight, 0);
            canvas.Children.Add(bottom.Shape);

            RectangleObstacle left = new RectangleObstacle(10, ActualHeight, 0, 0, 0);
            canvas.Children.Add(left.Shape);

            RectangleObstacle right = new RectangleObstacle(10, ActualHeight, ActualWidth, 0, 0);
            canvas.Children.Add(right.Shape);

            Random r = new Random();
            for (int i = 0; i < 0; i++)
            {
                Vector2d topleft = new Vector2d(r.NextDouble() * ActualWidth, r.NextDouble() * ActualHeight);
                double len = r.NextDouble() * 200 + 10;
                double thickness = 10;
                bool vertical = (r.NextDouble() > 0.5);
                if (vertical)
                {
                    RectangleObstacle obs = new RectangleObstacle(10, len, topleft.X, topleft.Y, 0);
                    canvas.Children.Add(obs.Shape);
                }
                else
                {
                    RectangleObstacle obs = new RectangleObstacle(len, 10, topleft.X, topleft.Y, 0);
                    canvas.Children.Add(obs.Shape);
                }


            }

            //RectangleObstacle obs1 = new RectangleObstacle(10, 300, 500, 100, 0);
            //canvas.Children.Add(obs1.Shape);

            _target_obstacle = new RectangleObstacle(10, 10, target_pos.X, target_pos.Y, 0);
            _target_obstacle.Shape.Fill = Brushes.White;
            _target_obstacle.Shape.MouseDown += Shape_MouseDown;
            canvas.Children.Add(_target_obstacle.Shape);

            genetic_algorithm = new FindPathGeneticAlgorithm(target_pos, initial_pos);
            genetic_algorithm.CreateNewRandomPopulation();
            //genetic_algorithm.Run(true);



            overall_max_fitness = 0;
            best_dna = null;
            generations = 0;



            //_map_zp = new MapZP(grid, canvas);

            for (int i = 0; i < 0; i++)
            {
                genetic_algorithm.UpdateFrame();

                //remove old population from canvas if existing
                if (genetic_algorithm.OldPopulation != null)
                {
                    foreach (SmartAgent dna in genetic_algorithm.OldPopulation)
                        dna.Dispose();
                    genetic_algorithm.OldPopulation = null;
                }

                //show changes
                foreach (SmartAgent dna in genetic_algorithm.Population)
                    if (!dna.IsDead)
                        dna.Render(false, false);

            }
            CompositionTarget.Rendering += CompositionTarget_Rendering;

        }

        private MapZP _map_zp;
        private long last_update;
        private void CompositionTarget_Rendering(object sender, EventArgs e)
        {


            if (_stop_watch == null)
            {
                _stop_watch = new Stopwatch();
                _stop_watch.Start();
            }
            
            long elapsed = _stop_watch.ElapsedMilliseconds;

            //_map_zp.Update();
            if (elapsed - last_update > FindPathGeneticAlgorithm.IntervalMsec)
            {
                last_update = elapsed;
                //genetic_algorithm.UpdateFrame();
            }

            genetic_algorithm.UpdateFrame();



            lock (genetic_algorithm.Population)
            {
                //remove old population from canvas if existing
                if (genetic_algorithm.OldPopulation != null)
                {
                    foreach (SmartAgent dna in genetic_algorithm.OldPopulation)
                        dna.Dispose();
                    genetic_algorithm.OldPopulation = null;

                    for (int i = canvas.Children.Count - 1; i > 0; i--)
                    {
                        if (canvas.Children[i] is Line)
                            canvas.Children.RemoveAt(i);
                    }

                    Int32Rect rect = new Int32Rect(0, 0, Wpf.Bmp.PixelWidth, Wpf.Bmp.PixelHeight);
                    int bytesPerPixel = Wpf.Bmp.Format.BitsPerPixel / 8; // typically 4 (BGR32)
                    byte[] empty = new byte[rect.Width * rect.Height * bytesPerPixel]; // cache this one
                    int emptyStride = rect.Width * bytesPerPixel;
                    Wpf.Bmp.WritePixels(rect, empty, emptyStride, 0);

                    System.GC.Collect();
                    return;
                }

                //show changes
                foreach (SmartAgent dna in genetic_algorithm.Population)
                    if (!dna.IsDead)
                        dna.Render();

                //if (genetic_algorithm.OverallBestDna != null)
                //{
                //    SmartAgent best_agent = (SmartAgent)genetic_algorithm.OverallBestDna;
                //    Canvas.SetZIndex(best_agent.Shape, 10);
                //    best_agent.Shape.Fill = Brushes.Red;
                //}
            }
        }

        float overall_max_fitness = 0;
        Dna<Vector2d> best_dna = null;
        int generations = 0;

        private int _frame;
        private FindPathGeneticAlgorithm genetic_algorithm;
        private Stopwatch _stop_watch;
        List<FrameworkElement> obstacles = new List<FrameworkElement>();

        private void Canvas_OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            _start_create_block_mousedown = new Vector2d(e.GetPosition(grid).X, e.GetPosition(grid).Y);
        }

        private void Canvas_OnMouseUp(object sender, MouseButtonEventArgs e)
        {
            if (_start_create_block_mousedown.HasValue)
            {
                Vector2d mouseup = new Vector2d(e.GetPosition(grid).X, e.GetPosition(grid).Y);
                RectangleObstacle right = new RectangleObstacle(mouseup.X - _start_create_block_mousedown.Value.X, mouseup.Y - _start_create_block_mousedown.Value.Y, _start_create_block_mousedown.Value.X, _start_create_block_mousedown.Value.Y, 0);
                canvas.Children.Add(right.Shape);
                _start_create_block_mousedown = null;
                return;
            }


            if (_target_obstacle_move_start.HasValue)
            {
                Canvas.SetLeft(_target_obstacle.Shape, e.GetPosition(grid).X);
                Canvas.SetTop(_target_obstacle.Shape, e.GetPosition(grid).Y);
                _target_obstacle_move_start = null;
                SmartAgent.TargetPos = new Vector2d(e.GetPosition(grid).X + _target_obstacle.Shape.ActualWidth / 2, e.GetPosition(grid).Y + _target_obstacle.Shape.ActualHeight / 2);
            }

        }

        private void Shape_MouseDown(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
            _target_obstacle_move_start = new Vector2d(e.GetPosition(grid).X, e.GetPosition(grid).Y);
        }

        private RectangleObstacle _target_obstacle;
        private Vector2d? _start_create_block_mousedown;


        private Vector2d? _target_obstacle_move_start = new Vector2d();


    }
}
