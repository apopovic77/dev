﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Challenge
{
    public partial class MainPage : UserControl
    {
        public MainPage()
        {
            InitializeComponent();

            CompositionTarget.Rendering += CompositionTarget_Rendering;
        }

        void CompositionTarget_Rendering(object sender, EventArgs e)
        {

            _r++;
            LayoutRoot.Background = new SolidColorBrush(Color.FromArgb(255,_r,_g,_b));
        }
        byte _r, _g, _b;

    }
}
