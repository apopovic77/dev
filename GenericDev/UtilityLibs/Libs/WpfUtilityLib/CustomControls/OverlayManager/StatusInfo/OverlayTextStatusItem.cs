﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Logicx.Utilities;

namespace Logicx.WpfUtility.CustomControls.OverlayManager.StatusInfo
{
    public class OverlayTextStatusItem : OverlayStatusItem
    {
        public OverlayTextStatusItem() : base()
        {
            _title = new TextBlock();
            _title.Margin = new Thickness(2);
            _title.Text = "";
            _title.FontSize = 20;
            _title.Foreground = Brushes.WhiteSmoke;
            _title.FontWeight = FontWeights.Bold;
            _title.FontFamily = new FontFamily("Calibri");
            _title.HorizontalAlignment = HorizontalAlignment.Left;
            _title.VerticalAlignment = VerticalAlignment.Center;

            _text = new TextBlock();
            _text.Margin = new Thickness(2);
            _text.Text = "";
            _text.FontSize = 18;
            _text.Foreground = Brushes.White;
            _text.FontFamily = new FontFamily("Calibri");
            _text.HorizontalAlignment = HorizontalAlignment.Left;
            _text.VerticalAlignment = VerticalAlignment.Center;

            StackPanel sp_text = new StackPanel();
            sp_text.Children.Add(_title);
            sp_text.Children.Add(_text);
            sp_text.Margin = new Thickness(10);
            sp_text.HorizontalAlignment = HorizontalAlignment.Left;
            sp_text.VerticalAlignment = VerticalAlignment.Center;

            border.Child = sp_text;

            Closeable = true;
        }

        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(e);

            if(e.Property.Name == "Title")
            {
                _title.Text = (string)e.NewValue;
            }
            else if(e.Property.Name == "Text")
            {
                _text.Text = (string)e.NewValue;
            }
        }

        #region Properties
        public string Title
        {
            get { return (string)GetValue(TitleProperty); }
            set { SetValue(TitleProperty, value); }
        }
        public static readonly DependencyProperty TitleProperty = DependencyProperty.Register("Title", typeof(string), typeof(OverlayTextStatusItem), new UIPropertyMetadata(""));

        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }
        public static readonly DependencyProperty TextProperty = DependencyProperty.Register("Text", typeof(string), typeof(OverlayTextStatusItem), new UIPropertyMetadata(""));
        #endregion

        #region Attributes
        protected TextBlock _title;
        protected TextBlock _text;
        #endregion

    }
}
