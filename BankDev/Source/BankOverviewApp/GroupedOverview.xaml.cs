﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.DataVisualization.Charting;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BankLib.Data;
using BankLib.Gruppe;

namespace BankOverviewApp
{
    /// <summary>
    /// Interaction logic for GroupedOverview.xaml
    /// </summary>
    public partial class GroupedOverview : UserControl
    {
        public GroupedOverview()
        {
            InitializeComponent();

            _filter_ctrl = new FilterControl();
            _filter_ctrl.FilterSortChanged += _filter_ctrl_FilterSortChanged;
            header_stack.Children.Insert(0, _filter_ctrl);
        }

        private void CreateTreeView(BuchungsGruppe top)
        {
            if (top == null)
                return;

            CreateTreeView(_tree_view.Items, top);
        }

        private void CreateTreeView(ItemCollection item_collection, BuchungsGruppe buchungs_gruppe)
        {
            if (!buchungs_gruppe.HasChildren())
                return;

            foreach (BuchungsGruppe bg_child in buchungs_gruppe.Children())
            {
                TreeViewItem tv_child = new TreeViewItem();
                tv_child.Tag = bg_child;
                tv_child.IsExpanded = GetIsExpanded(bg_child.Name);
                tv_child.Expanded += tv_child_Expanded;
                tv_child.Collapsed += tv_child_Collapsed;
                item_collection.Add(tv_child);
                tv_child.Header = GetNiceTreeHeader(bg_child);
                AddBgZuoToTree(tv_child, bg_child);
                AddBuchungenToTree(tv_child, bg_child);
                CreateTreeView(tv_child.Items, bg_child);
            }
        }


        private bool GetIsExpanded(string node_name)
        {
            if (!_is_expanded.ContainsKey(node_name))
                return false;

            return _is_expanded[node_name];
        }

        private void SetIsExpanded(TreeViewItem tv, bool is_expanded)
        {
            BuchungsGruppe bg_tagged = tv.Tag as BuchungsGruppe;

            string node_name_tagged = tv.Tag as string;
            string node_name = (bg_tagged != null) ? bg_tagged.Name : node_name_tagged;

            if (node_name == null)
                return;

            if (!_is_expanded.ContainsKey(node_name))
                _is_expanded.Add(node_name, is_expanded);
            _is_expanded[node_name] = is_expanded;
        }

        void tv_child_Collapsed(object sender, RoutedEventArgs e)
        {
            TreeViewItem tv = (TreeViewItem) sender;
            SetIsExpanded(tv, tv.IsExpanded);
        }

        void tv_child_Expanded(object sender, RoutedEventArgs e)
        {
            TreeViewItem tv = (TreeViewItem)sender;
            SetIsExpanded(tv, true);
        }


        private void CollapseAll()
        {
            ColExpTree(_tree_view.Items, false);
        }

        private void ExpandAll()
        {
            ColExpTree(_tree_view.Items, true);
        }

        private void ColExpTree(ItemCollection item_collection, bool is_expanded)
        {
            foreach (TreeViewItem tree_view_item in item_collection)
            {
                if (tree_view_item.Tag is string)
                {
                    string node_name_tagged = (string)tree_view_item.Tag;
                    tree_view_item.IsExpanded = GetIsExpanded(node_name_tagged);
                    continue;
                }

                if (tree_view_item.Items.Count <= 2)
                {
                    BuchungsGruppe bg = tree_view_item.Tag as BuchungsGruppe;
                    if(bg != null)
                        tree_view_item.IsExpanded = GetIsExpanded(bg.Name);
                    continue;
                }

                tree_view_item.IsExpanded = is_expanded;
                ColExpTree(tree_view_item.Items, is_expanded);
            }
        }

        private object GetNiceTreeHeader(BuchungsGruppe bg_child)
        {
            TextBlock tb_name = new TextBlock();
            tb_name.Width = 100;
            tb_name.Margin = new Thickness(0,0,10,0);
            tb_name.Text = bg_child.Name;

            TextBlock tb_betrag = new TextBlock();
            tb_betrag.Text = bg_child.SumBetrag.ToString("C2", CultureInfo.CreateSpecificCulture("de-DE"));
            tb_betrag.Foreground = (bg_child.SumBetrag < 0) ? Brushes.Red : Brushes.Black; 
            tb_betrag.TextAlignment = TextAlignment.Right;
            tb_betrag.Width = 80;

            StackPanel sp = new StackPanel();
            sp.Orientation = Orientation.Horizontal;

            sp.Children.Add(tb_name);
            sp.Children.Add(tb_betrag);

            return sp;
        }

        private void AddBgZuoToTree(TreeViewItem tv, BuchungsGruppe bg)
        {
            if (bg.BgZuos().Count == 0)
                return;

            TreeViewItem tv_child_rules = new TreeViewItem();
            tv_child_rules.Tag = bg.Name + "Rules";
            tv_child_rules.IsExpanded = GetIsExpanded(bg.Name + "Rules");
            tv_child_rules.Expanded += tv_child_Expanded;
            tv_child_rules.Collapsed += tv_child_Collapsed;
            tv.Items.Add(tv_child_rules);
            TextBlock tb = new TextBlock();
            tb.Text = "Rules";
            tb.FontStyle = FontStyles.Italic;
            tb.Foreground = Brushes.Gray;
            tv_child_rules.Header = tb;

            foreach (BgZuo bg_zuo in bg.BgZuos())
            {
                TreeViewItem tv_child = new TreeViewItem();
                tv_child.IsExpanded = false;
                tv_child_rules.Items.Add(tv_child);
                tv_child.Header = bg_zuo.ToString();
            }
        }


        private void AddBuchungenToTree(TreeViewItem tv, BuchungsGruppe bg)
        {
            TreeViewItem tv_child_buchungen = new TreeViewItem();
            tv_child_buchungen.Tag = bg.Name + "Buchungen";
            tv_child_buchungen.IsExpanded = GetIsExpanded(bg.Name + "Buchungen");
            tv_child_buchungen.Expanded += tv_child_Expanded;
            tv_child_buchungen.Collapsed += tv_child_Collapsed;
            TextBlock tb = new TextBlock();
            tb.Text = "Buchungen";
            tb.FontStyle = FontStyles.Italic;
            tb.Foreground = Brushes.Gray;
            tv_child_buchungen.Header = tb;

            List<Buchung> alle_buchungen = new List<Buchung>();
            foreach (BgZuo bg_zuo in bg.BgZuos())
            {
                if (bg_zuo.Buchungen == null)
                    continue;
                
                foreach (Buchung b in bg_zuo.Buchungen)
                    alle_buchungen.Add(b);
            }

            foreach (Buchung b in alle_buchungen.OrderByDescending(b=>b.Datum))
            {
                TreeViewItem tv_child = new TreeViewItem();
                tv_child.IsExpanded = false;
                tv_child.Tag = b;
                tv_child_buchungen.Items.Add(tv_child);
                tv_child.Header = GetNiceBuchungTextBlock(b);
            }
           

            if(tv_child_buchungen.Items.Count > 0)
                tv.Items.Add(tv_child_buchungen);
        }

        private TextBlock GetNiceBuchungTextBlock(Buchung b)
        {
            TextBlock tb = new TextBlock();
            string formatted_betrag = b.BetragFormatted;
            string tabs = "\t";
            if (formatted_betrag.Length < 9)
                tabs += "\t";
            if (formatted_betrag.Length < 5)
                tabs += "\t";
            tb.Text = b.Owner + "\t" + b.Datum.ToShortDateString() + "\t" + formatted_betrag + tabs + b.UmsatzType;
            tb.Foreground = Brushes.Gray;
            return tb;
        }

        private void UpdateTreeView()
        {
            if (BuchungsGruppen.BgTop == null)
                BuchungsGruppen.LoadGruppenFromFile();

            List<Buchung> not_added_buchungen = BuchungsGruppen.AddBuchungen(_filter_ctrl.GetFilteredBuchungen(), true);
            
            _tree_view.Items.Clear();
            CreateTreeView(BuchungsGruppen.BgTop);

        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            UpdateTreeView();
        }


        void _filter_ctrl_FilterSortChanged(object sender, EventArgs e)
        {
            UpdateTreeView();
        }

        private void ExpandAll_OnClick(object sender, RoutedEventArgs e)
        {
            ExpandAll();
        }

        private void CollapseAll_OnClick(object sender, RoutedEventArgs e)
        {
            CollapseAll();
        }



        private void _tree_view_OnSelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            TreeViewItem tvi = (TreeViewItem)_tree_view.SelectedItem;
            if (tvi == null)
            {
                RemoveOldChartFromGrid();
                return;
            }

            BuchungsGruppe bg = tvi.Tag as BuchungsGruppe;
            if (bg == null)
            {
                RemoveOldChartFromGrid();
                return;
            }

            List<KeyValuePair<string, int>> value_list = new List<KeyValuePair<string, int>>();
            foreach (DateTime date_time in Buchungen.GetBuchungsMonate())
            {
                List<Buchung> filtered_buchungen = Buchungen.GetBuchungen(_filter_ctrl.SelectedKonto, date_time.Month, date_time.Year);
                value_list.Add(new KeyValuePair<string, int>(date_time.ToString("MMM") + " " + date_time.Year, (int)Math.Round(bg.GetSumBetrag(filtered_buchungen))));
            }


            RemoveOldChartFromGrid();

            Chart chart = CreateChart(bg.Name, value_list);
            grid.Children.Add(chart);
        }

        private void RemoveOldChartFromGrid()
        {
            //remove old chart
            for (int i = grid.Children.Count - 1; i >= 0; i--)
            {
                if (grid.Children[i] is Chart)
                {
                    grid.Children.RemoveAt(i);
                    break;
                }
            }
        }



        private Chart CreateChart(string title, IEnumerable items_source)
        {
            //Setting data for column chart
            ColumnSeries series = new ColumnSeries();
            series.DependentValuePath = "Value";
            series.IndependentValuePath = "Key";
            series.ItemsSource = items_source;

            Chart chart = new Chart();
            chart.Title = title;
            chart.Series.Add(series);
            Grid.SetRow(chart, 1);
            Grid.SetColumn(chart,1);

            return chart;



            //// Setting data for pie chart
            //pieChart.DataContext = valueList;

            ////Setting data for area chart
            //areaChart.DataContext = valueList;

            ////Setting data for bar chart
            //barChart.DataContext = valueList;

            ////Setting data for line chart
            //lineChart.DataContext = valueList;
        }


        private FilterControl _filter_ctrl;
        private Dictionary<string, bool> _is_expanded = new Dictionary<string, bool>();


    }
}
