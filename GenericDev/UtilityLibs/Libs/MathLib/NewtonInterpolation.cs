﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MathLib
{
    public class NewtonInterpolation
    {
        public NewtonInterpolation(Vector2f p1, Vector2f p2, Vector2f p3, Vector2f p4)
        {
            _newton_values = new double[4];
            _newton_x = new double[4];
            CalcNewtonPolynom(p1,p2,p3,p4);
        }

        public NewtonInterpolation(Vector2d p1, Vector2d p2, Vector2d p3, Vector2d p4)
        {
            _newton_values = new double[4];
            _newton_x = new double[4];
            CalcNewtonPolynom(p1, p2, p3, p4);
        }

        

        public void CalcNewtonPolynom(Vector2f p1, Vector2f p2, Vector2f p3, Vector2f p4)
        {
            _newton_values[0] = p1.Y;
            _newton_x[0] = p1.X;

            double[] val1 = new double[3];
            val1[0] = (p2.Y - p1.Y) / (p2.X - p1.X);
            val1[1] = (p3.Y - p2.Y) / (p3.X - p2.X);
            val1[2] = (p4.Y - p3.Y) / (p4.X - p3.X);
            _newton_values[1] = val1[0];
            _newton_x[1] = p2.X;

            double[] val2 = new double[2];
            val2[0] = (val1[1] - val1[0]) / (p3.X - p1.X);
            val2[1] = (val1[2] - val1[1]) / (p4.X - p2.X);
            _newton_values[2] = val2[0];
            _newton_x[2] = p3.X;

            _newton_values[3] = (val2[1] - val2[0]) / (p4.X - p1.X);
            _newton_x[3] = p4.X;
        }

        public void CalcNewtonPolynom(Vector2d p1, Vector2d p2, Vector2d p3, Vector2d p4)
        {
            _newton_values[0] = p1.Y;
            _newton_x[0] = p1.X;

            double[] val1 = new double[3];
            val1[0] = (p2.Y - p1.Y) / (p2.X - p1.X);
            val1[1] = (p3.Y - p2.Y) / (p3.X - p2.X);
            val1[2] = (p4.Y - p3.Y) / (p4.X - p3.X);
            _newton_values[1] = val1[0];
            _newton_x[1] = p2.X;

            double[] val2 = new double[2];
            val2[0] = (val1[1] - val1[0]) / (p3.X - p1.X);
            val2[1] = (val1[2] - val1[1]) / (p4.X - p2.X);
            _newton_values[2] = val2[0];
            _newton_x[2] = p3.X;

            _newton_values[3] = (val2[1] - val2[0]) / (p4.X - p1.X);
            _newton_x[3] = p4.X;
        }

        public double GetNewtonPolynomValue(double x)
        {
            return (_newton_values[0] + _newton_values[1] * (x - _newton_x[0]) + _newton_values[2] * (x - _newton_x[0]) * (x - _newton_x[1]) + _newton_values[3] * (x - _newton_x[0]) * (x - _newton_x[1]) * (x - _newton_x[2]));
        }

        public override string ToString()
        {
            //return _newton_values[0] +"+"+ _newton_values[1]+"*(x - "+_newton_x[0]+") + "+_newton_values[2]+"*(x - "+_newton_x[0]+")*(x - "+_newton_x[1]+") + "+_newton_values[3]+"*(x - "+_newton_x[0]+")*(x - "+_newton_x[1]+")*(x - "+_newton_x[2]+")";


            return String.Format("{0} + {1} * (x - {4}) + {2} * (x - {4}) * (x - {5}) + {3} * (x - {4}) * (x - {5}) * (x - {6})",
                string.Format("{0:0.0000000000000000000000}", (float)_newton_values[0]).ToString().Replace(",", "."),
                string.Format("{0:0.0000000000000000000000}", (float)_newton_values[1]).ToString().Replace(",", "."),
                string.Format("{0:0.0000000000000000000000}", (float)_newton_values[2]).ToString().Replace(",", "."),
                string.Format("{0:0.0000000000000000000000}", (float)_newton_values[3]).ToString().Replace(",", "."),
                string.Format("{0:0.0000000000000000000000}", (float)_newton_x[0]).ToString().Replace(",", "."),
                string.Format("{0:0.0000000000000000000000}", (float)_newton_x[1]).ToString().Replace(",", "."),
                string.Format("{0:0.0000000000000000000000}", (float)_newton_x[2]).ToString().Replace(",", "."));
        }

        #region Attributes
        private double[] _newton_values;
        private double[] _newton_x;
        #endregion
    }
}
