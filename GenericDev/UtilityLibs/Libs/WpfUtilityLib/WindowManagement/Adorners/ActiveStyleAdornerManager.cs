﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Media;

namespace Logicx.WpfUtility.WindowManagement.Adorners
{
    public class ActiveStyleAdornerManager
    {
        public enum AdornedElemStyle
        {
            Default = 1
        }

        private class AdornedElemData
        {
            public Adorner Adorner;
            public Brush RenderBrush;
            public Pen RenderPen;
        }

        public static ActiveStyleAdornerManager Instance
        {
            get
            {
                if (_instance == null)
                    lock (_syncronisation)
                        if (_instance == null)
                            _instance = new ActiveStyleAdornerManager();

                return _instance;
            }
        }

        private ActiveStyleAdornerManager()
        {
        }

        public void Register(FrameworkElement elem, Brush render_brush, Pen render_pen)
        {
            AdornedElemData adorned_elem_data = new AdornedElemData();
            adorned_elem_data.RenderBrush = render_brush;
            adorned_elem_data.RenderPen = render_pen;

            _registered_elements.Add(elem, adorned_elem_data);
            elem.MouseEnter += new System.Windows.Input.MouseEventHandler(elem_MouseEnter);
            elem.MouseLeave += new System.Windows.Input.MouseEventHandler(elem_MouseLeave);
            elem.MouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(elem_MouseLeftButtonDown);
            elem.MouseLeftButtonUp += new System.Windows.Input.MouseButtonEventHandler(elem_MouseLeftButtonUp);
        }

        public void DeRegister(FrameworkElement elem)
        {
            _registered_elements.Remove(elem);
            elem.MouseEnter -= elem_MouseEnter;
            elem.MouseLeave -= elem_MouseLeave;
            elem.MouseLeftButtonDown -= elem_MouseLeftButtonDown;
            elem.MouseLeftButtonUp -= elem_MouseLeftButtonUp;
        }

        void elem_MouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
        }

        void elem_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
        }

        void elem_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            FrameworkElement elem = (FrameworkElement)sender;
            AdornerLayer layer = AdornerLayer.GetAdornerLayer(elem);
            if(layer!=null)
                layer.Remove(_registered_elements[elem].Adorner);
        }

        void elem_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            FrameworkElement elem = (FrameworkElement) sender;
            AdornerLayer layer = AdornerLayer.GetAdornerLayer(elem);

            if(layer==null)
                return;

            layer.IsHitTestVisible = false;
            layer.Focusable = false;

            if(_registered_elements[elem].Adorner == null)
            {
                _registered_elements[elem].Adorner = new ActiveStyleAdorner(elem, _registered_elements[elem].RenderBrush, _registered_elements[elem].RenderPen);
            }

            layer.Add(_registered_elements[elem].Adorner);
        }

        #region Attributes
        static volatile ActiveStyleAdornerManager _instance = null;
        static Object _syncronisation = new Object();
        private Dictionary<FrameworkElement, AdornedElemData> _registered_elements = new Dictionary<FrameworkElement, AdornedElemData>();
        #endregion
    }
}
