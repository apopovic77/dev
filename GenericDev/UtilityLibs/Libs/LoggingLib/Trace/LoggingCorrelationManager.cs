﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace LoggingLib.Trace
{
    public class ActivityInfo
    {
        #region Construction and initialzation
        /// <summary>
        /// Internal constructor of the activity info class
        /// </summary>
        /// <param name="activity_id"></param>
        /// <param name="activity_description"></param>
        /// <param name="scope_identifier"></param>
        internal ActivityInfo(Guid activity_id, string activity_description, object scope_identifier)
        {
            _activity_id = activity_id;
            _activity_description = activity_description;
            _scope_identifier = scope_identifier;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets the GUID of the current activity
        /// </summary>
        public Guid ActivityId
        {
            get { return _activity_id;}
        }
        /// <summary>
        /// Gets/Sets the description of the current activity
        /// </summary>
        public string ActivityDescrpition
        {
            get { return _activity_description; }
            set { _activity_description = value; }
        }
        /// <summary>
        /// Internal property which returns the scope identifier object
        /// </summary>
        /// <remarks>The scope identifier may be a Thread, a string or any other kind oj object</remarks>
        public object ScopeIdentifier
        {
            get { return _scope_identifier; }
        }
        #endregion

        #region Attributes
        private Guid _activity_id = Guid.Empty;
        private string _activity_description = string.Empty;
        private object _scope_identifier = null;
        #endregion
    }

    /// <summary>
    /// Implements a correlation manager class with scope and description support
    /// </summary>
    public class LoggingCorrelationManager
    {
        #region Construction and initialization
        #endregion

        #region Operations

        #region logical operation management
        /// <summary>
        /// Starts a thread-bound logical operation
        /// </summary>
        /// <returns>Returns the ActivityInfo of the new operation</returns>
        public static ActivityInfo StartLogicalOperation()
        {
            return StartLogicalOperation("");
        }
        /// <summary>
        /// Starts a thread-bound logical operation
        /// </summary>
        /// <param name="activity_description">A description text for the operation</param>
        /// <returns>Returns the ActivityInfo of the new operation</returns>
        public static ActivityInfo StartLogicalOperation(string activity_description)
        {
            return StartLogicalOperation(Thread.CurrentThread, "");
        }
        /// <summary>
        /// Starts a custom scoped logical operation
        /// </summary>
        /// <param name="scope_identifier">an object used to identify the correlation scope</param>
        /// <param name="activity_description">A description text for the operation</param>
        /// <returns>Returns the ActivityInfo of the new operation</returns>
        public static ActivityInfo StartLogicalOperation(object scope_identifier, string activity_description)
        {
            if (scope_identifier == null)
                throw new ArgumentException("scope_identifier must not be a null value");

            // create a new activity identifier
            LoggingLib.Trace.ActivityInfo new_activity = new ActivityInfo(Guid.NewGuid(), activity_description, scope_identifier);

            if (!_scoped_activities.ContainsKey(scope_identifier))
                _scoped_activities[scope_identifier] = new Stack<ActivityInfo>();

            // push the new activity info onto the stack
            _scoped_activities[scope_identifier].Push(new_activity);

            return new_activity;
        }

        /// <summary>
        /// Stops the current logical operation in the current thread
        /// </summary>
        public static void StopLogicalOperation()
        {
            StopLogicalOperation(Thread.CurrentThread);
        }
        /// <summary>
        /// Stops the current logical operation in the identified scope
        /// </summary>
        /// <param name="scope_identifier">the scope identifier object</param>
        public static void StopLogicalOperation(object scope_identifier)
        {
            if (scope_identifier == null)
                throw new ArgumentException("scope_identifier must not be a null value");

            // remove the current activity info from the stack
            if (_scoped_activities.ContainsKey(scope_identifier))
                _scoped_activities[scope_identifier].Pop();

        }
        #endregion 

        #region scoped activity
        /// <summary>
        /// Gets the current activity information object for a specified correlation scope
        /// </summary>
        /// <param name="scope_identifier">the correlation scope identifier</param>
        /// <returns>Returns the current activity information object of the correlation scope or null if not existent</returns>
        public static ActivityInfo GetActivityInfoForScope(object scope_identifier)
        {
            if (_scoped_activities.ContainsKey(scope_identifier))
            {
                Stack<ActivityInfo> activity_stack = _scoped_activities[scope_identifier];
                ActivityInfo[] activities = activity_stack.ToArray();
                if (activities.Length > 0)
                    return activities[0];
            }

            return null;
        }

        /// <summary>
        /// Gets the activity stack for a specified correlation scope
        /// </summary>
        /// <param name="scope_identifier">the correlation scope identifier</param>
        /// <returns>Returns the current activity stack of the correlation scope or null if not existent</returns>
        public static Stack<ActivityInfo> GetActivityStackForScope(object scope_identifier)
        {
            if (_scoped_activities.ContainsKey(scope_identifier))
                return _scoped_activities[scope_identifier];

            return null;
        }
        #endregion

        #endregion

        #region Properties
        /// <summary>
        /// Gets the current thread's acitivity info object
        /// </summary>
        /// <remarks>If the current thread does not contain a logical operation stack<br/>
        /// the property will return null</remarks>
        public static ActivityInfo ActivityInfo
        {
            get
            {
                object scope_key = Thread.CurrentThread as object;

                if(_scoped_activities.ContainsKey(scope_key))
                {
                    Stack<ActivityInfo> activity_stack = _scoped_activities[scope_key];
                    ActivityInfo[] activities = activity_stack.ToArray();
                    if (activities.Length > 0)
                        return activities[0];
                }

                return null;
            }
        }

        /// <summary>
        /// Gets the current thread's acitivity id
        /// </summary>
        /// <remarks>If the current thread does not contain a logical operation stack<br/>
        /// the property will return <see cref="System.Guid.Empty"/></remarks>
        public static Guid ActivityId
        {
            get
            {
                LoggingLib.Trace.ActivityInfo activity_info = ActivityInfo;

                if (activity_info != null)
                    return activity_info.ActivityId;

                return Guid.Empty;
            }
        }

        /// <summary>
        /// Gets the current thread's acitivity description
        /// </summary>
        /// <remarks>If the current thread does not contain a logical operation stack<br/>
        /// the property will return <see cref="System.String.Empty"/></remarks>
        public static string ActivityDescription
        {
            get
            {
                LoggingLib.Trace.ActivityInfo activity_info = ActivityInfo;

                if (activity_info != null)
                    return activity_info.ActivityDescrpition;

                return string.Empty;
            }
        }

        #endregion

        #region Attribures
        private static Dictionary<object, Stack<ActivityInfo>> _scoped_activities = new Dictionary<object, Stack<ActivityInfo>>();
        #endregion
    }
}
