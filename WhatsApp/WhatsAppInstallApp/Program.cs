﻿using System;
using Awanto.WhatsAppCore;
using Awanto.WhatsAppCore.Services;

namespace WhatsAppInstallApp
{
    class Program
    {
        static void Main(string[] args)
        {
            WhatsAppDbContext dbx = new WhatsAppDbContext(WhatsAppService.GetDbContextOptions());
            WhatsAppDbSeeder seeder = new WhatsAppDbSeeder(dbx);
            seeder.Seed().RunSynchronously();
            Console.ReadLine();
        }
    }
}
