﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Logicx.WpfUtility.CustomControls.OverlayManager
{
    /// <summary>
    /// Interaction logic for OverlayQuestion.xaml
    /// </summary>
    public partial class OverlayQuestion // : IOverlayContent
    {
        public OverlayQuestion()
        {
            InitializeComponent();
        }

        #region IOverlayContent interface implementation
        public bool Initialize()
        {
            DialogTitle = "Frage";
            DialogSubTitle = "";

            _button_yes = new OverlayDialogButton("Ja", OverlayButtonAlignment.Right, Key.Enter);
            _button_no = new OverlayDialogButton("Nein", OverlayButtonAlignment.Right);
            _button_cancel = new OverlayDialogButton("Abbrechen", OverlayButtonAlignment.Right, Key.Escape);
            AddButton(_button_yes);
            AddButton(_button_no);
            AddButton(_button_cancel);

            
            return true;
        }

        public bool Show(bool firstTime)
        {
            return true;
        }

        public bool Close(bool forceClose)
        {
            return true;
        }

        public bool Print()
        {
            return true;
        }

        public bool Help()
        {
            return true;
        }

        /// <summary>
        /// Eigenschaft welche definiert ob der content das Drucken unterstützt oder nicht
        /// </summary>
        public bool CanPrint
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Eigenschaft welche dem Hostdialog mitteilt ob der Content geschlossen werden kann.
        /// </summary>
        public bool CanClose
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Eigenschaft welche dem Hostdialog mitteilt, ob der Content eine Hilfe verfügt
        /// </summary>
        public bool CanHelp
        {
            get
            {
                return false;
            }
        }

        public bool AutoResized
        {
            get { return true; }
        }
        #endregion

        #region Properties
        /// <summary>
        /// show or hide cancel button
        /// </summary>
        public bool ShowCancel
        {
            get { return _show_cancel; }
            set
            {
                if (_show_cancel != value)
                {
                    _show_cancel = value;
                    if (!_show_cancel)
                        RemoveButton(_button_cancel);
                    else
                        AddButton(_button_cancel);
                }
            }
        }
        /// <summary>
        /// Gets/Sets the question text
        /// </summary>
        public string QuestionText
        {
            get { return textblock_question.Text; }
            set { textblock_question.Text = value; }
        }
        #endregion

        #region Virtuelle methoden welche überschrieben werden können

        /// <summary>
        /// Wird aufgerufen, wenn der Benutzer einen der OverlayButtons klicked
        /// </summary>
        /// <param name="button"></param>
        public override bool OverlayButtonClicked(OverlayDialogButton button)
        {
            if (button == _button_yes)
            {
                // YES - aktuellen dialog schliessen (nicht erzwingen)
                if (YesClicked != null)
                {
                    YesClicked(this, new EventArgs());
                }

                if (OverlayManager != null)
                    //OverlayManager.HideActiveDialogOnly(false);
                    OverlayManager.HideContent(this, false);
            }

            if (button == _button_no)
            {
                // no - aktuellen dialog schliessen (nicht erzwingen)
                if (NoClicked != null)
                {
                    NoClicked(this, new EventArgs());
                }

                if (OverlayManager != null)
                    //OverlayManager.HideActiveDialogOnly(false);
                    OverlayManager.HideContent(this, false);
            }

            if (button == _button_cancel)
            {
                // abbrechen - aktuellen dialog schliessen (erzwingen)
                if (CancelClicked != null)
                {
                    CancelClicked(this, new EventArgs());
                }

                if (OverlayManager != null)
                     OverlayManager.HideContent(this, true);

            }

            return true;
        }

        /// <summary>
        /// Wurd aufgerufen bevor das Control angezeigt wird
        /// </summary>
        /// <param name="firstTime">true wenn dieser Content in der Dialoginstanz das erste Mal angezeigt wird</param>
        /// <returns>false wenn das control nicht angezeigt werden soll.</returns>
        public override bool BeforeShow(bool firstTime)
        {
            return true;
        }
        /// <summary>
        /// Wird aufgerufen nachdem das Control angezeigt wurde
        /// </summary>
        public override void AfterShow()
        {

        }

        /// <summary>
        /// Wird aufgerufen bevor das control geschlossen wird
        /// </summary>
        /// <param name="forceClose">True wenn das Control auf jeden Fall geschlossen wird.</param>
        /// <returns>false wenn das Control nicht geschlossen werden kann.</returns>
        /// <remarks>Wenn <see cref="forceClose"/> True ist, dann wird der 
        /// Rückgabewert ignoriert und das Control auf jeden Fall geschlossen.</remarks>
        public override bool BeforeClose(bool forceClose)
        {
            return true;
        }
        /// <summary>
        /// Wird aufgerufen nachdem das Control geschlossen wurde
        /// </summary>
        public override void AfterClose()
        {

        }

        /// <summary>
        /// Wurd aufgerufen bevor das Control gedruckt wird wird
        /// </summary>
        /// <returns>false wenn das control nicht gedruckt werden soll.</returns>
        public override bool BeforePrint()
        {
            return true;
        }
        /// <summary>
        /// wird aufgerufen nachdem das Control gedruckt wurde
        /// </summary>
        public override void AfterPrint()
        {

        }
        #endregion

        #region Attribs
        public event EventHandler YesClicked;
        public event EventHandler NoClicked;
        public event EventHandler CancelClicked;

        private OverlayDialogButton _button_yes = null;
        private OverlayDialogButton _button_no = null;
        private OverlayDialogButton _button_cancel = null;

        private bool _show_cancel = true;

        #endregion
    }
}
