using System;
using System.Collections.Generic;
using System.Text;
using MathLib;
using System.Drawing;
using Logicx.Utilities;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using System.Drawing.Text;

namespace Logicx.Drawing.Objects3d
{
    public abstract class Object2d
    {
        public Object2d(Control parent) 
        {
            _parent = parent;

            Position.X = 0;
            Position.Y = 0;
            Size.Width = 100;
            Size.Height = 100;
        }

        public void Update() 
        {
            Bitmap bmp = new Bitmap(Size.Width+4,Size.Height+4);
            Graphics g_bmp = Graphics.FromImage(bmp);
            g_bmp.SmoothingMode = SmoothingMode.HighQuality;
            g_bmp.InterpolationMode = InterpolationMode.HighQualityBilinear;
            g_bmp.TextRenderingHint = TextRenderingHint.AntiAliasGridFit;
            DoRender(g_bmp);
            g_bmp.Flush();
            g_bmp.Dispose();

            if (_render_buffer != null)
                _render_buffer.Dispose();
            _render_buffer = bmp;
        }

        protected abstract void DoRender(System.Drawing.Graphics g);

        public void Render(System.Drawing.Graphics g) 
        {
            //if we have something in the backbuffer than draw 
            if (_render_buffer != null)
                g.DrawImage(_render_buffer, Position);
            else
                DoRender(g);
        }

        #region Properties
        #endregion

        #region Attributes
        public Point Position;
        public Size Size;
        protected Control _parent;
        protected Bitmap _render_buffer;
        #endregion
    }
}
