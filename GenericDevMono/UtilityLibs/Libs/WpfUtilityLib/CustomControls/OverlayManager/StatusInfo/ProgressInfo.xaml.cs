﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Timers;
using System.Windows.Threading;

namespace Logicx.WpfUtility.CustomControls.OverlayManager.StatusInfo
{
    public class ProgressBarHighlightConverter : IMultiValueConverter
    {
        // Methods
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            Type type = typeof(double);
            if ((((values == null) || (values.Length != 3)) || ((values[0] == null) || (values[1] == null))) || (((values[2] == null) || !typeof(Brush).IsAssignableFrom(values[0].GetType())) || (!type.IsAssignableFrom(values[1].GetType()) || !type.IsAssignableFrom(values[2].GetType()))))
            {
                return null;
            }

            Brush brush = (Brush)values[0];
            double d = (double)values[1];
            double num2 = (double)values[2];

            if ((((d <= 0) || double.IsInfinity(d)) || (double.IsNaN(d) || (num2 <= 0))) || (double.IsInfinity(num2) || double.IsNaN(num2)))
            {
                return null;
            }

            DrawingBrush brush2 = new DrawingBrush();
            double width = d * 2;
            brush2.Viewport = brush2.Viewbox = new Rect(-d, 0, width, num2);
            brush2.ViewportUnits = brush2.ViewboxUnits = BrushMappingMode.Absolute;
            brush2.TileMode = TileMode.None;
            brush2.Stretch = Stretch.None;

            DrawingGroup group = new DrawingGroup();
            DrawingContext context = group.Open();
            context.DrawRectangle(brush, null, new Rect(-d, 0, d, num2));
            TimeSpan keyTime = TimeSpan.FromSeconds(width / 200);
            TimeSpan span2 = TimeSpan.FromSeconds(1);

            DoubleAnimationUsingKeyFrames animation = new DoubleAnimationUsingKeyFrames
            {
                BeginTime = new TimeSpan?(TimeSpan.Zero),
                Duration = new Duration(keyTime + span2),
                RepeatBehavior = RepeatBehavior.Forever
            };

            animation.KeyFrames.Add(new LinearDoubleKeyFrame(width, keyTime));
            TranslateTransform transform = new TranslateTransform();
            transform.BeginAnimation(TranslateTransform.XProperty, animation);
            brush2.Transform = transform;
            context.Close();
            brush2.Drawing = group;
            return brush2;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
    /// <summary>
    /// Interaction logic for OverlayBlockingProgressStatusInfo.xaml
    /// </summary>
    public partial class ProgressInfo : UserControl, IDisposable
    {
        static ProgressInfo()
        {
            FrameworkPropertyMetadata metadataColorBgBrushBrush = new FrameworkPropertyMetadata(new SolidColorBrush(Color.FromArgb(0x33, 0x6B, 0x6B, 0x6B)));
            ColorBgBrushBrushProperty = DependencyProperty.Register("ColorBgBrushBrush",
                                                            typeof(Brush), typeof(ProgressInfo), metadataColorBgBrushBrush);

            FrameworkPropertyMetadata metadataTextBrush = new FrameworkPropertyMetadata(new SolidColorBrush(Color.FromArgb(0xFF, 0x77, 0x77, 0x77)));
            TextBrushProperty = DependencyProperty.Register("TextBrush",
                                                            typeof(Brush), typeof(ProgressInfo), metadataTextBrush);

            FrameworkPropertyMetadata metadataLongOperationIndicatorTextBrush = new FrameworkPropertyMetadata(new SolidColorBrush(Color.FromArgb(0xFF, 0xB5, 0xE0, 0xF0)));
            LongOperationIndicatorTextBrushProperty = DependencyProperty.Register("LongOperationIndicatorTextBrush",
                                                            typeof(Brush), typeof(ProgressInfo), metadataLongOperationIndicatorTextBrush);
        }

        public ProgressInfo()
        {
            InitializeComponent();
            Reset();
        }


        #region Destruktor
        /// <summary>
        /// Implement IDisposable.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            // This object will be cleaned up by the Dispose method.
            // Therefore, you should call GC.SupressFinalize to
            // take this object off the finalization queue 
            // and prevent finalization code for this object
            // from executing a second time.
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Dispose(bool disposing) executes in two distinct scenarios. 
        /// If disposing equals true, the method has been called directly 
        /// or indirectly by a user's code. Managed and unmanaged resources 
        /// can be disposed. 
        /// If disposing equals false, the method has been called by the 
        /// runtime from inside the finalizer and you should not reference  
        /// other objects. Only unmanaged resources can be disposed.
        /// </summary>
        /// <param name="disposing">disposing flag</param>
        private void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if (!_disposed)
            {
                // If disposing equals true, dispose all managed 
                // and unmanaged resources.
                if (disposing)
                {
                    // Dispose managed resources.
                    if (_elapsed_timer != null)
                    {
                        _elapsed_timer.Stop();
                        _elapsed_timer.Elapsed -= _elapsed_timer_Elapsed;
                        _elapsed_timer = null;
                    }

                    if (_long_operation_flash_timer != null)
                    {
                        _long_operation_flash_timer.Stop();
                        _long_operation_flash_timer.Elapsed -=  _long_operation_flash_timer_Elapsed;
                        _long_operation_flash_timer = null;
                    }
                }
            }
            _disposed = true;
        }
        #endregion

        #region Operations
        public void Reset()
        {
            CurrentAction = " ";
            CurrentActionProgress = 0.0;
            time_elapsed_tb.Text = "00:00";
            _time_elapsed = 0;
            _dtstart = DateTime.Now;

            if (_elapsed_timer != null)
            {
                _elapsed_timer.Stop();
                //_elapsed_timer.Elapsed -= _elapsed_timer_Elapsed;
                _elapsed_timer = null;
            }

            _elapsed_timer = new Timer(500);
            //_elapsed_timer.Elapsed += new ElapsedEventHandler(_elapsed_timer_Elapsed);
            //_elapsed_timer.Start();
        }

        private void _elapsed_timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (_elapsed_timer == null)
                return;

            _elapsed_timer.Stop();

            Dispatcher.Invoke(DispatcherPriority.Normal, (NoParas)delegate
            {
                TimeSpan ts = DateTime.Now - _dtstart;
                TimeSpan tslastAction = DateTime.Now - _last_action;
                time_elapsed_tb.Text = ts.Minutes.ToString("00") + ":" + ts.Seconds.ToString("00");
                _time_elapsed = ts.TotalMilliseconds;

                if (_enable_logoperation_indicator)
                {
                    if (tslastAction.TotalMilliseconds > _longoperation_indicator_trigger && _long_operation_flash_timer == null)
                    {
                        // 500ms text flash animation
                        _long_operation_flash_timer = new Timer(FLASHTIMEOUT_PERIOD);
                        //_long_operation_flash_timer.Elapsed += new ElapsedEventHandler(_long_operation_flash_timer_Elapsed);
                        //_long_operation_flash_timer.Start();
                    }
                }
            });

            OnTimerTicked();
            //if(_elapsed_timer != null)
                //_elapsed_timer.Start();
        }

        private void _long_operation_flash_timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (_enable_logoperation_indicator)
            {
                Dispatcher.Invoke(DispatcherPriority.Normal, (NoParas)delegate
                {
                    SetCurrentActionText(!_longoperation_indicator_visible);
                });
            }
        }


        private void ResetLongOperationTrigger()
        {
            _last_action = DateTime.Now;

            if (_long_operation_flash_timer != null)
            {
                _long_operation_flash_timer.Stop();
                //_long_operation_flash_timer.Elapsed -= new ElapsedEventHandler(_long_operation_flash_timer_Elapsed);
                _long_operation_flash_timer = null;
            }

            SetCurrentActionText(false);

        }

        private void SetCurrentActionText(bool bShowLongOperationIndicator)
        {
            if (bShowLongOperationIndicator)
            {
                _longoperation_indicator_visible = true;
                if (_longoperation_indicator_text.IndexOf("\n") >= 0)
                {
                    //current_action_info_tb.Text = _longoperation_indicator_text.Substring(0, _longoperation_indicator_text.IndexOf("\n"));
                    current_action_tb.Text = _longoperation_indicator_text.Substring(_longoperation_indicator_text.IndexOf("\n") + 1); ;
                }
                else
                {
                    current_action_tb.Text = _longoperation_indicator_text;
                }

                //current_action_info_tb.Foreground = (Brush)FindResource("orange_solid_color_brush");
                //current_action_tb.Foreground = (Brush)FindResource("orange_solid_color_brush");
            }
            else
            {
                _longoperation_indicator_visible = false;
                //current_action_info_tb.Text = "Aktuelle Aktion";
                current_action_tb.Text = CurrentAction;

                //current_action_info_tb.Foreground = (Brush)FindResource("GrayTextBrush");
                //current_action_tb.Foreground = (Brush)FindResource("GrayTextBrush");
            }
        }

        protected void OnTimerTicked()
        {
            if(TimerTicked != null)
            {
                TimerTicked(this, new EventArgs());
            }
        }

        public void StartHidingAnimation()
        {
            try
            {
                if (_hiding_sb != null)
                    return;

                _hiding_sb = (Storyboard)FindResource("anim_startcontent_fade_out");

                _hiding_sb.Completed += new EventHandler(_hiding_sb_Completed);
                _hiding_sb.Begin();

            }
            catch (Exception)
            {
                Visibility = Visibility.Hidden;
            }

        }
        #endregion

        #region EventHandler
        void _hiding_sb_Completed(object sender, EventArgs e)
        {
            if(_hiding_sb != null)
                _hiding_sb.Stop();

            _hiding_sb = null;

            this.Visibility = System.Windows.Visibility.Hidden;
            this.Opacity = 1;

            if (StatusControlHidden != null)
                StatusControlHidden(this, new EventArgs());
        }
        #endregion

        #region Events
        public event EventHandler TimerTicked;
        public event EventHandler StatusControlHidden;
        #endregion

        #region Dependency Properties
        public Brush ColorBgBrushBrush
        {
            get { return (Brush)GetValue(ColorBgBrushBrushProperty); }
            set { SetValue(ColorBgBrushBrushProperty, value); }
        }

        public Brush TextBrush
        {
            get { return (Brush)GetValue(TextBrushProperty); }
            set { SetValue(TextBrushProperty, value); }
        }

        public Brush LongOperationIndicatorTextBrush
        {
            get { return (Brush)GetValue(LongOperationIndicatorTextBrushProperty); }
            set { SetValue(LongOperationIndicatorTextBrushProperty, value); }
        }

        public static readonly DependencyProperty ColorBgBrushBrushProperty;
        public static readonly DependencyProperty TextBrushProperty;
        public static readonly DependencyProperty LongOperationIndicatorTextBrushProperty;
        #endregion

        #region Properties
        public string InformationTitle
        {
            get { return info_title_tb.Text; }
            set { info_title_tb.Text = value; }
        }
        public string SubInformationTitle
        {
            get { return subinfo_title_tb.Text; }
            set { subinfo_title_tb.Text = value; }
        }

        public string CurrentAction
        {
            get { return _current_action; }
            set
            {
                if (value == null)
                    return;

                string tmp = value;

                if (tmp.StartsWith("["))
                {
                    tmp = tmp.Substring(tmp.IndexOf("]") + 1);
                }

                _current_action = tmp;
                current_action_tb.Text = _current_action;
                ResetLongOperationTrigger();
            }
        }

        public double CurrentActionProgress
        {
            get { return action_progress.Value; }
            set
            {
                action_progress.Value = value;
                action_progress_tb.Text = value.ToString("0.0") + "%";
                ResetLongOperationTrigger();
            }
        }

        public bool EnableLongOperationIndicator
        {
            get { return _enable_logoperation_indicator; }
            set { _enable_logoperation_indicator = value; }
        }

        public double LongOperationTrigger
        {
            get { return _longoperation_indicator_trigger; }
            set { _longoperation_indicator_trigger = value; }
        }

        public string LongOperationText
        {
            get { return _longoperation_indicator_text; }
            set { _longoperation_indicator_text = value; }
        }

        public Visibility TimeIndicatorVisibility
        {
            get { return time_elapsed_tb.Visibility; }
            set { time_elapsed_tb.Visibility = value; }
        }

        public double TimeElapsed
        {
            get { return _time_elapsed; }
        }

        public bool IsLongOperationIndicatorActive
        {
            get { return _long_operation_flash_timer != null; }
        }

        public bool ShowPercentage
        {
            get { return _show_percentage; }
            set
            {
                if (_show_percentage != value)
                {
                    _show_percentage = value;

                    if (_show_percentage)
                    {
                        action_progress.Visibility = Visibility.Visible;
                        action_progress_tb.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        action_progress.Visibility = Visibility.Collapsed;
                        action_progress_tb.Visibility = Visibility.Collapsed;
                    }
                }
            }
        }

        public bool ShowCurrentAction
        {
            get { return _show_current_action; }
            set
            {
                if (_show_current_action != value)
                {
                    _show_current_action = value;

                    if (_show_current_action)
                    {
                        current_action_tb.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        current_action_tb.Visibility = Visibility.Collapsed;
                    }
                }
            }
        }

        #endregion

        #region Attributes
        private Storyboard _hiding_sb = null;

        private bool _transparent_background = true;
        
        private bool _show_percentage = true;
        private bool _show_current_action = true;

        private DateTime _dtstart = DateTime.Now;
        private DateTime _last_action = DateTime.Now;
        private Timer _elapsed_timer = null;
        private bool _disposed = false;
        private string _current_action = "";
        // long operation info
        // wenn sich eine gewisse zeitlang kein status ändert, wird durch flashen des
        // info textes angegeben, dass im hintergrund gearbeitet wird
        private Timer _long_operation_flash_timer = null;
        private bool _enable_logoperation_indicator = false;
        private double _longoperation_indicator_trigger = 8000; // ms
        private string _longoperation_indicator_text = "Die aktuelle Aktion wird im Hintergrund abgeschlossen.\nBitte das Fenster nicht schliessen auch wenn keine Statusänderungen sichtbar sind!";
        private bool _longoperation_indicator_visible = false;
        private double _time_elapsed = 0;

        private const double FLASHTIMEOUT_PERIOD = 1600;
        protected delegate void NoParas();
        #endregion

    }
}
