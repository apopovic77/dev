﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Net.Mime;
using Awanto.WhatsAppCore.Entities;
using Awanto.WhatsAppCore.Utilities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Console;

namespace Awanto.WhatsAppCore.Services
{
    public class WhatsAppBot : IDisposable
    {
        public WhatsAppBot(string bot_id = "WhatsAppBot", ILogger logger = null)
        {
            _bot_id = bot_id;
            _logger = logger;


            if (logger == null)
                _logger = new ConsoleLogger("WhatsAppBotConsoleLogger", null, false);
        }

        public void Start()
        {
            if (_sql_dependency != null)
                return;

            CheckForNewInquiry();
            StartNotificationRetrieval();
            _logger.LogInformation("Starting Successfully Notfication Retrieval for WhatsAppBot");
        }

        public void Stop()
        {
            StopNotificationRetrieval();
            _logger.LogInformation("Stoping Notfication Retrieval for WhatsAppBot");
        }

        protected virtual WhatsAppNachricht CheckForBotQuery(Nachricht nachricht)
        {
            try
            {
                if (string.IsNullOrEmpty(nachricht.Body))
                    return null;

                string body = nachricht.Body.ToLower();

                switch (body)
                {
                    case "time":
                        return CreateAntwortNachricht(nachricht, DateTime.Now.ToString());
                    case "ip":
                        return CreateAntwortNachricht(nachricht, "dont know");
                        //case "zitat":
                        //    return CreateAntwortNachricht(nachricht, ZitateService.GetRandomZitat());
                        //case "log":
                        //    return CreateAntwortNachricht(nachricht, EventLogService.GetLastEventLogEntries(DateTime.Now - new TimeSpan(24, 0, 0), new[] { "Debug" }));
                        //case "debug":
                        //    return CreateAntwortNachricht(nachricht, EventLogService.GetLastDebugEventLogEntries(DateTime.Now - new TimeSpan(24, 0, 0)));
                        //case "engel":
                        //case "tageskarte":
                        //    MediaTypeNames.Text engel_text = EngelKarteService.GetRandomEngelKarte();
                        //    return CreateAntwortNachricht(nachricht, engel_text.Überschrift + "\n\n*" + engel_text.Txt + "*\n\n" + engel_text.Referenz.Split(";".ToCharArray(), 2)[1]);
                        //case "pitemp":
                        //    return CreateAntwortNachricht(nachricht,
                        //        LaisCode.Core.Utility.SshCommand.ExecSshCommand("/home/pi/scripts/pitemp.sh"));
                }
                //if (body.StartsWith("picmd"))
                //{
                //    return CreateAntwortNachricht(nachricht, LaisCode.Core.Utility.SshCommand.ExecSshCommand(nachricht.Body.Split(" ".ToCharArray(), 2)[1]));
                //}
            }
            catch (Exception ex)
            {
                _logger.LogDebug("CheckForBotQuery: " + ex.ToString());
            }
            return null;
        }


        protected WhatsAppNachricht CreateAntwortNachricht(Nachricht n_in, string body_antwort)
        {
            WhatsAppNachricht bot_antwort = new WhatsAppNachricht();
            bot_antwort.AntwortAuf = n_in;
            if (n_in.Antworten == null)
                n_in.Antworten = new List<Nachricht>();
            n_in.Antworten.Add(bot_antwort);
            bot_antwort.AbsenderAdresse = WhatsAppService.GetPhoneNr();
            bot_antwort.EmpfaengerAdresse = n_in.AbsenderAdresse;
            bot_antwort.Body = "" + body_antwort + "";
            return bot_antwort;
        }


        protected void CheckForNewInquiry()
        {
            lock (this)
            {
                WhatsAppDbContext db = null;
                try
                {
                    db = new WhatsAppDbContext(WhatsAppService.GetDbContextOptions());


                    List<Nachricht> nachrichten = db.Nachrichten.Include(n => n.Antworten).Include(n => n.Medien).ThenInclude(nm => nm.Medium).Where(
                        n =>
                            n._RawGesendetAm != null && n._RawGesendetAm.IndexOf("2,") >= 0 &&
                            n.GetGesendetAm(KommunikationsKanal.Whatsapp) + new TimeSpan(0, 0, 10, 0) >= DateTime.Now &&
                            (n.Antworten == null ||
                             n.Antworten.Count(antwort => antwort.Subject == "WhatsAppBot Antwort") == 0) &&
                            n.AbsenderAdresse != WhatsAppService.GetPhoneNr() &&
                            n.Id > _last_nachricht_id_processed
                    ).ToList();


                    foreach (Nachricht nachricht in nachrichten)
                    {
                        _last_nachricht_id_processed = nachricht.Id;

                        WhatsAppNachricht bot_antwort = CheckForBotQuery(nachricht);
                        if (bot_antwort == null)
                            continue;

                        Console.WriteLine("New Question arrived!");

                        db.WhatsAppNachrichten.Add(bot_antwort);
                    }


                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    _logger.LogDebug("Exception in WhatsAppBot " + ex.ToString());
                    Console.Error.WriteLine(ex.ToString());
                }
                finally
                {
                    try
                    {
                        if (db != null)
                            db.Dispose();
                    }
                    catch
                    {
                    }
                }
            }
        }

        //protected void CheckForNewImageWork()
        //{
        //    lock (this)
        //    {
        //        WhatsAppDbContext db = null;
        //        try
        //        {
        //            db = new WhatsAppDbContext(WhatsAppService.GetDbContextOptions());

        //            List<WhatsAppNachricht> nachrichten = db.WhatsAppNachrichten.Include(n => n.Antworten).Where(
        //                n =>
        //                    n._RawGesendetAm != null && n._RawGesendetAm.IndexOf("2,") >= 0 &&
        //                    n.GetGesendetAm(KommunikationsKanal.Whatsapp) + new TimeSpan(0, 0, 10, 0) >= DateTime.Now &&
        //                    (n.Antworten == null ||
        //                     n.Antworten.Count(antwort => antwort.Subject == "WhatsAppBot Antwort") == 0) &&
        //                    n.AbsenderAdresse != WhatsAppService.GetPhoneNr() &&
        //                    n.Id > _last_nachricht_id_processed
        //            ).ToList();


        //            foreach (Nachricht nachricht in nachrichten)
        //            {
        //                _last_nachricht_id_processed = nachricht.Id;

        //                WhatsAppNachricht bot_antwort = CheckForBotQuery(nachricht);
        //                if (bot_antwort == null)
        //                    continue;

        //                Console.WriteLine("New Question arrived!");

        //                db.WhatsAppNachrichten.Add(bot_antwort);
        //            }


        //            db.SaveChanges();
        //        }
        //        catch (Exception ex)
        //        {
        //            _logger.LogDebug("Exception in WhatsAppBot " + ex.ToString());
        //            Console.Error.WriteLine(ex.ToString());
        //        }
        //        finally
        //        {
        //            try
        //            {
        //                if (db != null)
        //                    db.Dispose();
        //            }
        //            catch
        //            {
        //            }
        //        }
        //    }
        //}



        #region DB Notifications
        public virtual void StartNotificationRetrieval()
        {
            lock (this)
            {
                if (_sql_dependency != null)
                    return;
                _logger.LogDebug("SqlDependency: Attemping to start");
                _sql_dependency = new SqlDependencyEx(
                    WhatsAppService.GetConnectionString(),
                    WhatsAppService.GetDatabaseName(),
                    WhatsAppService.GetDatabaseUserName(),
                    "Nachrichten",
                    "dbo",
                    SqlDependencyEx.NotificationTypes.Insert,
                    false,
                    _bot_id+ "Nachrichten",
                    WhatsAppService.IsSqlExpressDb);
                {
                    _sql_dependency.WithInstallListener = true;
                    _sql_dependency.TableChanged += SqlDependency_TableChanged;
                    _sql_dependency.Start();
                    Console.WriteLine("starting sql dependency successfully");
                    _logger.LogDebug("SqlDependency: starting sql dependency successfully Nachrichten");
                }

                //if (_listen_for_new_medien)
                //{
                //    if (_sql_dependency_new_medien != null)
                //        return;
                //    _logger.LogDebug("SqlDependency: Attemping to start");
                //    _sql_dependency_new_medien = new SqlDependencyEx(
                //        WhatsAppService.GetConnectionString(),
                //        WhatsAppService.GetDatabaseName(),
                //        WhatsAppService.GetDatabaseUserName(),
                //        "NachrichtMedium",
                //        "dbo",
                //        SqlDependencyEx.NotificationTypes.Insert,
                //        false,
                //        _bot_id + "NachrichtMedium",
                //        WhatsAppService.IsSqlExpressDb);
                //    {
                //        _sql_dependency_new_medien.WithInstallListener = true;
                //        _sql_dependency_new_medien.TableChanged += _sql_dependency_new_medien_TableChanged;
                //        _sql_dependency_new_medien.Start();
                //        _logger.LogDebug("SqlDependency: starting sql dependency successfully NachrichtMedien");
                //    }
                //}
            }
        }

        //private void _sql_dependency_new_medien_TableChanged(object sender, SqlDependencyEx.TableChangedEventArgs e)
        //{
        //    lock (this)
        //    {
        //        Console.WriteLine("receiving table changed ->Medien");
        //        _logger.LogDebug("SqlDependency: WhatsAppBot receiving table changed Medien");
        //        CheckForNewImageWork();
        //    }
        //}

        private void SqlDependency_TableChanged(object sender, SqlDependencyEx.TableChangedEventArgs e)
        {
            lock (this)
            {
                Console.WriteLine("receiving table changed ->Nachrichten");
                _logger.LogDebug("SqlDependency: WhatsAppBot receiving table changed Nachrichten");
                CheckForNewInquiry();
            }
        }


        private void StopNotificationRetrieval()
        {
            lock (this)
            {
                if (_sql_dependency != null)
                {
                    _sql_dependency.Dispose();
                    _sql_dependency = null;
                }
                //if (_sql_dependency_new_medien != null)
                //{
                //    _sql_dependency_new_medien.Dispose();
                //    _sql_dependency_new_medien = null;
                //}
            }
        }


        #endregion

        #region Disposing
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                Stop();
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion

        protected SqlDependencyEx _sql_dependency;
        //protected SqlDependencyEx _sql_dependency_new_medien;
        private int _last_nachricht_id_processed;
        protected ILogger _logger;
        protected string _bot_id;

    }
}
