using System;
using System.Collections.Generic;
using System.Text;
using MathLib;

namespace Logicx.Geo.Geometries
{
    public class SimplePolygon : SimpleLine
    {
        public SimplePolygon()
        {
            _index_mode = IndexMode.List;
        }

        public SimplePolygon(string vertices) :base(vertices)
        {
            _index_mode = IndexMode.List;
            AssertPolygonVertices();
        }
        public SimplePolygon(List<Vector2f> vertices) :base(vertices)
        {
            _index_mode = IndexMode.List;
            AssertPolygonVertices();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="vertices"></param>
        /// <param name="start_seg">Richtungsvektor des linken Endes des Polygons</param>
        /// <param name="end_seg">Richtungsvektor des rechten Endes des Polygons</param>
        public SimplePolygon(List<Vector2f> vertices, Vector2f start_seg, Vector2f end_seg) : base(vertices)
        {
            _index_mode = IndexMode.List;
            StartSegmentVector = start_seg;
            EndSegmentVector = end_seg;
            AssertPolygonVertices();
        }
        public SimplePolygon(BoundingBox bounding_box)
        {
            _index_mode = IndexMode.List;
            _vertices_list = new List<Vector2f>();

            Vector2f topleft = (Vector2f)bounding_box.TopLeft;
            Vector2f bottomright = (Vector2f)bounding_box.BottomRight;
            Vector2f topright = new Vector2f(bottomright.X, topleft.Y);
            Vector2f bottomleft = new Vector2f(topleft.X, bottomright.Y);

            _vertices_list.Add(topleft);
            _vertices_list.Add(topright);
            _vertices_list.Add(bottomright);
            _vertices_list.Add(bottomleft);
            _vertices_list.Add(topleft);

            AssertPolygonVertices();
        }

		private void AssertPolygonVertices()
		{
            if (Count <= 3)
                throw new Exception("Vertices Count must be greater than 3");
            if (this[0] != this[Count - 1])
            {
                Add(this[0]);
            }
                //throw new Exception("first and last vertex must have same values");
		}

        public bool PointInPolygon(float x, float y)
        {
            return PointInPolygon((double)x, (double)y);
        }

        public bool PointInPolygon(double x, double y)
        {
            if(Count == 0)
                return false;

            bool c = false;
            for (int i = 0, j = Count - 1; i < Count; j = i++)
            {
                if ((((this[i].Y <= y) && (y < this[j].Y)) || ((this[j].Y <= y) && (y < this[i].Y))) && (x < (this[j].X - this[i].X) * (y - this[i].Y) / (this[j].Y - this[i].Y) + this[i].X))
                    c = !c;
            }
            return c;
        }

        /// <summary>
        /// translation
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public void Translate(Vector2f v)
        {
            for (int i = 0; i < Count; i++)
                this[i] += v;
        }

        public static SimplePolygon operator +(SimplePolygon poly, Vector2f v)
        {
            if (poly == null)
                return null;

            List<Vector2f> new_verts = new List<Vector2f>(poly.Count);
            for (int i = 0; i < poly.Count; i++)
                new_verts.Add(poly[i] + v);
            return new SimplePolygon(new_verts);
        }

        public static SimplePolygon operator -(SimplePolygon poly, Vector2f v)
        {
            if (poly == null)
                return null;

            List<Vector2f> new_verts = new List<Vector2f>(poly.Count);
            for (int i = 0; i < poly.Count; i++)
                new_verts.Add(poly[i] - v);
            return new SimplePolygon(new_verts);
        }  

        public Vector2f CentroidPosition
        {
            get 
            {
                throw new Exception("currently not implemented");
            }
        }
        public float Area
        {
            get
            {
                throw new Exception("currently not implemented");
            }
        }

        public bool PointInPolygon(Vector2f v)
        {
            return PointInPolygon(v.X, v.Y);
        }

        public bool PointInPolygon(Vector2d v)
        {
            return PointInPolygon(v.X, v.Y);
        }

        public void ProjectPolygon(Vector2f axis, SimplePolygon polygon, ref float min, ref float max) {
            // To project a point on an axis use the dot product

            float dotProduct = axis.GetSkalarProduct(polygon[0]);
            min = dotProduct;
            max = dotProduct;
            for (int i = 0; i < polygon.Count; i++) {
                dotProduct = polygon[i].GetSkalarProduct(axis);
                if (dotProduct < min) {
                    min = dotProduct;
                } else {
                    if (dotProduct> max) {
                        max = dotProduct;
                    }
                }
            }
        }

        /// <summary>
        /// das is so ziemlich der langsamste isintersecting check den ich kenne
        /// ausserdem bin auch nicht sicher ob 
        /// </summary>
        /// <param name="curr_visual_bounding_poly"></param>
        /// <returns></returns>
        public bool IsIntersecting(SimplePolygon curr_visual_bounding_poly)
        {
            int edgeCountA = Count-1;
            int edgeCountB = curr_visual_bounding_poly.Count-1;
            Edge edge;

            for (int edgeIndex = 0; edgeIndex < edgeCountA + edgeCountB; edgeIndex++)
            {
                if (edgeIndex < edgeCountA)
                {
                    edge = new Edge((Vector2d) this[edgeIndex], (Vector2d) this[edgeIndex + 1]);
                }
                else
                {
                    edge = new Edge((Vector2d) curr_visual_bounding_poly[edgeIndex - edgeCountA],
                                    (Vector2d) curr_visual_bounding_poly[edgeIndex - edgeCountA + 1]);
                }

                // ===== 1. Find if the polygons are currently intersecting =====


                // Find the axis perpendicular to the current edge

                Vector2f axis = (Vector2f)(edge.V2 - edge.V1).GetOrthogonal1().GetNorm();

                // Find the projection of the polygon on the current axis

                float minA = 0;
                float minB = 0;
                float maxA = 0;
                float maxB = 0;
                ProjectPolygon(axis, this, ref minA, ref maxA);
                ProjectPolygon(axis, curr_visual_bounding_poly, ref minB, ref maxB);

                // Check if the polygon projections are currentlty intersecting

                if (!(IntervalDistance(minA, maxA, minB, maxB) > 0)) 
                    return true;
            }

    


            /*for(int i=0; i < _vertices.Count-1; i++)
            {
                Edge edge_to_check = new Edge((Vector2d) (this[i]), (Vector2d) (this[i + 1]));
                for(int e=0; e < curr_visual_bounding_poly.Count-1; e++)
                {
                    LinearAlgebra.GetCutPointInSegment(edge_to_check,
                }
                
            }
            for (int i = 0; i < _vertices.Count; i++)
                if (curr_visual_bounding_poly.PointInPolygon(this[i]))
                    return true;

            for (int i = 0; i < curr_visual_bounding_poly.Count; i++)
                if (PointInPolygon(curr_visual_bounding_poly[i]))
                    return true;
             */

            return false;
        }

        private float IntervalDistance(float minA, float maxA, float minB, float maxB) {
            if (minA < minB) {
                return minB - maxA;
            } else {
                return minA - maxB;
            }
        }



        public BoundingBox GetBoundingBox()
        {
            float most_top = float.MinValue;
            float most_left = float.MaxValue;
            float most_right = float.MinValue;
            float most_bottom = float.MaxValue;

            for (int i = 0; i < Count - 1; i++)
            {
                Vector2f v = this[i];
                if (v.Y > most_top)
                    most_top = v.Y;
                if (v.Y < most_bottom)
                    most_bottom = v.Y;
                if (v.X < most_left)
                    most_left = v.X;
                if (v.X > most_right)
                    most_right = v.X;
            }

            return new BoundingBox(new Vector2d(most_left, most_bottom), new Vector2d(most_right, most_top));
        }

        /// <summary>
        /// rotiert alle vertexe im polygon
        /// </summary>
        /// <param name="rot_center"></param>
        /// <param name="angle_vec"></param>
        public void Rotate(Vector2f rot_center, Vector2f angle_vec)
        {
            float angle = angle_vec.GetAngle();

            for (int i = 0; i < Count; i++)
            {
                Vector2f v_curr_to_rot = this[i] - rot_center;
                v_curr_to_rot.Rotate(angle);
                this[i] = rot_center + v_curr_to_rot;
            }
        }
        
        public Vector2f StartSegmentVector
        {
            set; get;
        }

        public Vector2f EndSegmentVector
        {
            set;
            get;
        }
    }
}
