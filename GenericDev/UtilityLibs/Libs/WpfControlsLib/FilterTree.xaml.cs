﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Logicx.Utility.XMLUtilities;

namespace Logicx.WpfUtility.CustomControls
{
    /// <summary>
    /// Interaction logic for FilterTree.xaml
    /// </summary>
    public partial class FilterTree : UserControl
    {
        public FilterTree()
        {
            InitializeComponent();
        }

        #region Tree Creation
        protected TreeViewItem CreateTreeNode(string node_name, string node_header)
        {
            return CreateTreeNode(true, node_name, node_header);
        }
        protected TreeViewItem CreateTreeNode(bool is_checked, string node_name, string node_header)
        {
            CheckBox chkbx = new CheckBox();
            chkbx.IsChecked = is_checked;
            chkbx.Click += chkbx_treenode_Click;
            chkbx.Name = node_name + "_chkbx";

            TextBlock tb = new TextBlock();
            tb.MouseDown += treeviewitem_tb_MouseDown;
            tb.Name = node_name + "_tb";
            tb.Text = node_header;
            tb.Style = _tb_style;

            StackPanel sp = new StackPanel();
            sp.Orientation = Orientation.Horizontal;
            sp.Children.Add(chkbx);
            sp.Children.Add(tb);

            TreeViewItem ti = new TreeViewItem();
            ti.Name = node_name;
            ti.Tag = node_header;
            ti.Header = sp;
            return ti;
        }
        #endregion

        #region Custom Serialization
        [Serializable]
        public struct CustomFilterTreeNode
        {
            public bool IsChecked;
            public string NodeName;
            public string NodeHeader;
            public List<CustomFilterTreeNode> SubNodes;
        }
        public string GetSerializedFilterTree()
        {
            //create a custom filter tree
            CustomFilterTreeNode root_node = new CustomFilterTreeNode();
            root_node.NodeName = "ROOT";
            root_node.NodeHeader = "ROOT";
            root_node.SubNodes = new List<CustomFilterTreeNode>();
            for (int i = 0; i < Tree.Items.Count; i++)
            {
                TreeViewItem ti_sub = (TreeViewItem)Tree.Items[i];
                CustomFilterTreeNode subnode = GetCustomFilterNode(ti_sub);
                root_node.SubNodes.Add(subnode);
            }

            string serialized_filter_tree = XMLManager.SerializeToString(root_node);
            return serialized_filter_tree;
        }
        private CustomFilterTreeNode GetCustomFilterNode(TreeViewItem ti)
        {
            CustomFilterTreeNode node = new CustomFilterTreeNode();
            node.NodeName = ti.Name;
            node.NodeHeader = (ti.Tag!=null)?ti.Tag.ToString():"";
            node.IsChecked = GetIsCheckedTreeNode(ti);
            if (ti.Items.Count == 0)
                return node;
            node.SubNodes = new List<CustomFilterTreeNode>();
            for (int i = 0; i < ti.Items.Count; i++)
            {
                TreeViewItem ti_sub = (TreeViewItem)ti.Items[i];
                CustomFilterTreeNode subnode = GetCustomFilterNode(ti_sub);
                node.SubNodes.Add(subnode);
            }
            return node;
        }

        public void SetSerializedFilterTree(string serialized_filter_tree)
        {
            Tree.Items.Clear();
            if (string.IsNullOrEmpty(serialized_filter_tree))
                return;

            CustomFilterTreeNode root_node = XMLManager.DeserializeFromString<CustomFilterTreeNode>(serialized_filter_tree);
            for (int i = 0; i < root_node.SubNodes.Count; i++)
            {
                TreeViewItem ti = CreateTreeViewItem(root_node.SubNodes[i]);
                Tree.Items.Add(ti);
            }
        }
        private TreeViewItem CreateTreeViewItem(CustomFilterTreeNode node)
        {
            TreeViewItem ti = CreateTreeNode(node.IsChecked, node.NodeName, node.NodeHeader);
            if (node.SubNodes == null) return ti;
            for (int i = 0; i < node.SubNodes.Count; i++)
            {
                TreeViewItem ti_sub = CreateTreeViewItem(node.SubNodes[i]);
                ti.Items.Add(ti_sub);
            }
            return ti;
        }

        #endregion

        #region Util Methods
        protected bool GetBoolIsChecked(CheckBox chkbx)
        {
            if (chkbx.IsChecked.Value)
                return true;
            else
                return false;
        }
        /// <summary>
        /// iterates through all tree view item nodes and return the item if it holds the checkbox
        /// </summary>
        /// <param name="chkbx"></param>
        /// <returns></returns>
        protected TreeViewItem GetTreeNode(CheckBox chkbx)
        {
            return (TreeViewItem)((StackPanel)chkbx.Parent).Parent;
        }
        protected TreeViewItem GetTreeNode(TextBlock tb)
        {
            return (TreeViewItem)((StackPanel)tb.Parent).Parent;
        }
        public TreeViewItem GetTreeNodeWithName(string name)
        {
            foreach (TreeViewItem ti in filter_tree.Items)
            {
                if (ti.Name != null && ti.Name == name)
                    return ti;

                TreeViewItem ti_with_name = GetTreeNodeWithName(ti, name);
                if (ti_with_name != null)
                    return ti_with_name;
            }
            return null;
        }
        private TreeViewItem GetTreeNodeWithName(TreeViewItem ti_parent,string name)
        {
            foreach (TreeViewItem ti in ti_parent.Items)
            {
                if (ti.Name != null && ti.Name == name)
                    return ti;

                TreeViewItem ti_with_name = GetTreeNodeWithName(ti, name);
                if (ti_with_name != null)
                    return ti_with_name;
            }
            return null;
        }

        public List<TreeViewItem> GetTreeNodesWithTag(string tag)
        {
            List<TreeViewItem> tree_nodes = new List<TreeViewItem>();
            foreach (TreeViewItem ti in filter_tree.Items)
            {
                if (ti.Tag != null && ti.Tag.ToString() == tag)
                    tree_nodes.Add(ti);

                GetTreeNodesWithTag(ti, tag, tree_nodes);
            }
            return tree_nodes;
        }
        private void GetTreeNodesWithTag(TreeViewItem ti_parent, string tag, List<TreeViewItem> tree_nodes)
        {
            foreach (TreeViewItem ti in ti_parent.Items)
            {
                if (ti.Tag != null && ti.Tag.ToString() == tag)
                    tree_nodes.Add(ti);

                GetTreeNodesWithTag(ti, tag, tree_nodes);
            }
        }

        public TreeViewItem GetTreeNodeWithTag(string tag)
        {
            foreach (TreeViewItem ti in filter_tree.Items)
            {
                if (ti.Tag != null && ti.Tag.ToString() == tag)
                    return ti;

                TreeViewItem ti_with_tag = GetTreeNodeWithTag(ti, tag);
                if (ti_with_tag != null)
                    return ti_with_tag;
            }
            return null;
        }
        private TreeViewItem GetTreeNodeWithTag(TreeViewItem ti_parent, string tag)
        {
            foreach (TreeViewItem ti in ti_parent.Items)
            {
                if (ti.Tag != null && ti.Tag.ToString() == tag)
                    return ti;

                TreeViewItem ti_with_tag = GetTreeNodeWithTag(ti, tag);
                if (ti_with_tag != null)
                    return ti_with_tag;
            }
            return null;
        }


        protected CheckBox GetCheckBox(TreeViewItem ti)
        {
            StackPanel sp = (StackPanel)ti.Header;
            CheckBox chkbx = (CheckBox)sp.Children[0];
            return chkbx;
        }


        public void SetParentsCheckedState(TreeViewItem ti)
        {
            if (ti.Parent == null)
                return;

            if (ti.Parent is TreeViewItem)
            {
                TreeViewItem ti_parent = (TreeViewItem)ti.Parent;

                bool corr_checked_val = GetCorrectCheckedState(ti_parent);
                //if (corr_checked_val == GetIsCheckedTreeNode(ti_parent))
                //    return;
                SetIsCheckedTreeNode(ti_parent, corr_checked_val);
                SetParentsCheckedState(ti_parent);
            }
        }

        public void SetIsCheckedOnNodesWithTag(string tag_name, bool is_checked)
        {
            foreach (TreeViewItem ti in filter_tree.Items)
                SetIsCheckedOnNodesWithTag(ti, tag_name, is_checked);
        }
        private void SetIsCheckedOnNodesWithTag(TreeViewItem ti_parent, string tag_name, bool is_checked)
        {
            if (ti_parent.Tag != null && ti_parent.Tag.ToString() == tag_name)
                SetIsCheckedTreeNode(ti_parent,is_checked);

            foreach (TreeViewItem ti in ti_parent.Items)
                SetIsCheckedOnNodesWithTag(ti, tag_name, is_checked);
        }

        public void SetIsCheckedOnNodesWithNameContains(string name_part, bool is_checked)
        {
            foreach (TreeViewItem ti in filter_tree.Items)
                SetIsCheckedOnNodesWithNameContains(ti, name_part, is_checked);
        }
        private void SetIsCheckedOnNodesWithNameContains(TreeViewItem ti_parent, string name_part, bool is_checked)
        {
            if (ti_parent.Name != null && ti_parent.Name.ToString().IndexOf(name_part) >= 0)
                SetIsCheckedTreeNode(ti_parent, is_checked);

            foreach (TreeViewItem ti in ti_parent.Items)
                SetIsCheckedOnNodesWithNameContains(ti, name_part, is_checked);
        }


        protected bool IsSubNodeChecked(TreeViewItem ti)
        {
            if (GetIsCheckedTreeNode(ti))
                return true;

            foreach (TreeViewItem ti_child in ti.Items)
                if (IsSubNodeChecked(ti_child))
                    return true;

            return false;
        }

        protected bool GetCorrectCheckedState(TreeViewItem ti)
        {
            foreach (TreeViewItem ti_child in ti.Items)
            {
                if (!GetIsCheckedTreeNode(ti_child))
                    return false;
            }
            return true;
        }

        protected void SetIsCheckedSubTreeNodes(TreeViewItem ti, bool is_checked)
        {
            SetIsCheckedTreeNode(ti, is_checked);
            foreach (TreeViewItem ti_sub in ti.Items)
                SetIsCheckedSubTreeNodes(ti_sub, is_checked);
        }

        public static void SetIsCheckedTreeNode(TreeViewItem ti, bool is_checked)
        {
            StackPanel sp = (StackPanel)ti.Header;
            CheckBox chkbx = (CheckBox)sp.Children[0];
            chkbx.IsChecked = is_checked;
        }
        public static bool GetIsCheckedTreeNode(TreeViewItem ti)
        {
            StackPanel sp = (StackPanel)ti.Header;
            CheckBox chkbx = (CheckBox)sp.Children[0];

            if (chkbx.IsChecked.Value)
                return true;
            else
                return false;
        }

        public void CheckAllTreeItems()
        {
            foreach (TreeViewItem ti in filter_tree.Items)
            {
                SetIsCheckedTreeNode(ti, true);
                CheckAllTreeItems(ti);
            }
        }
        private void CheckAllTreeItems(TreeViewItem ti_root)
        {
            foreach (TreeViewItem ti in ti_root.Items)
                CheckAllTreeItems(ti);
            SetIsCheckedTreeNode(ti_root, true);
        }

        #endregion

        #region Event Handling
        private void treeviewitem_tb_MouseDown(object sender, MouseButtonEventArgs e)
        {
            TextBlock tb = (TextBlock)sender;
            TreeViewItem ti = GetTreeNode(tb);
            SetIsCheckedSubTreeNodes(ti, !GetIsCheckedTreeNode(ti));
            SetParentsCheckedState(ti);
        }

        private void chkbx_treenode_Click(object sender, RoutedEventArgs e)
        {
            CheckBox chkbx = (CheckBox)sender;
            SetIsCheckedSubTreeNodes(GetTreeNode(chkbx), GetBoolIsChecked(chkbx));
            SetParentsCheckedState(GetTreeNode(chkbx));
        }
        #endregion

        #region Properties
        public TreeView Tree
        {
            get
            {
                return filter_tree;
            }
        }
        #endregion

        #region Attribs
        protected Style _tb_style;
        #endregion

    }
}