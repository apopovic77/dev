﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Media.TextFormatting;
using CognitiveLib.Scanner;
using Logicx.Utility;
using Utilities;

namespace CognitiveLib.Detector
{
    public class WeightedStrings : List<WeightedString>
    {
        public void SortListByWeights()
        {
            Sort((x, y) => y.Weight.CompareTo(x.Weight));
        }
    }

    public enum StringWeightCategory
    {
        Low,
        Middle,
        Good,
        Perfect
    }

    public struct DetectionResult
    {

    }

    public class WeightedString
    {
        public WeightedString()
        {
        }

        public WeightedString(string str, StringWeightCategory weight_category)
        {
            Attribute = null;
            _custom_weight = null;
            _custom_weight_cat = null;
            WeightFactor = 1;
            _added_weights = null;

            String = str;
            WeightCategory = weight_category;
        }

        public WeightedString(string str, float weight_factor)
        {
            _added_weights = null;
            Attribute = null;
            _custom_weight = null;
            _custom_weight_cat = null;
            WeightFactor = 1;

            String = str;
            WeightFactor = weight_factor;
        }

        public WeightedString(string str)
        {
            _added_weights = null;
            Attribute = null;
            _custom_weight = null;
            _custom_weight_cat = null;
            WeightFactor = 1;

            String = str;
        }

        //public static float GetNameWeight(string str, float weight_factor = 1)
        //{
        //    return str.Length * str.Length * weight_factor;
        //}

        public void AddWeight(WeightedString ws)
        {
            if(_added_weights == null)
                _added_weights = new List<WeightedString>(1);

            _added_weights.Add(ws);
        }

        public void AddWeight(float weight, string attribute)
        {
            if (_added_weights == null)
                _added_weights = new List<WeightedString>(1);

            WeightedString ws = new WeightedString();
            ws.SetCustomWeight(weight);
            ws.Attribute = attribute;

            _added_weights.Add(ws);
        }

        /// <summary>
        /// normalerweise errechnet sich das weight von dem string durch eine funktion
        /// diesen wert kann man überschreiben in dem man durch diese methode selbst einen wert definiert
        /// </summary>
        /// <param name="weight"></param>
        public void SetCustomWeight(float weight)
        {
            _custom_weight = weight;
        }

        /// <summary>
        /// welchen detection wert hat der angegbene name
        /// der Marken Name "OK" hat beispielsweise einen schlechten detection wert 
        /// </summary>
        public StringWeightCategory WeightCategory
        {
            get
            {
                if (Weight <= 1)
                    return StringWeightCategory.Low;
                else if (Weight > 50)
                    return StringWeightCategory.Middle;
                else if (Weight > 1000)
                    return StringWeightCategory.Good;
                else
                    return StringWeightCategory.Perfect;
            }
            set
            {
                _custom_weight_cat = value;
                switch (_custom_weight_cat.Value)
                {
                    case StringWeightCategory.Low:
                        WeightFactor = 1;
                        break;
                    case StringWeightCategory.Middle:
                        WeightFactor = 2;
                        break;
                    case StringWeightCategory.Good:
                        WeightFactor = 3;
                        break;
                    case StringWeightCategory.Perfect:
                        WeightFactor = 4;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        /// <summary>
        /// return k * x^2
        /// k = weight_factor
        /// x = string.length
        /// </summary>
        public float Weight
        {
            get
            {
                if (_custom_weight.HasValue)
                    return _custom_weight.Value;

                if (!WeightFactor.HasValue)
                    WeightFactor = 1;

                float weight = 0f;
                if(_added_weights != null)
                    foreach (WeightedString ws in _added_weights)
                        weight += ws.Weight;

                if (string.IsNullOrEmpty(String))
                    return weight;

                return String.Length * String.Length * WeightFactor.Value + weight;
            }
        }


        //public int CompareTo(WeightedString other)
        //{
        //    return this.Weight.CompareTo(other.Weight);
        //}

        public bool IsEmpty
        {
            get
            {
                return string.IsNullOrEmpty(String) && string.IsNullOrEmpty(Attribute) && Weight == 0f;
            }
        }

        public override string ToString()
        {
            if (string.IsNullOrEmpty(String) && string.IsNullOrEmpty(Attribute) && Weight == 0f)
                return null;

            string res = $"{Weight}";
            if (!string.IsNullOrEmpty(String))
                res += $", {String}";

            string attributes = Attribute;
            if(_added_weights != null)
                foreach (WeightedString weighted_string in _added_weights)
                    if(!string.IsNullOrEmpty(weighted_string.Attribute))
                        attributes += "-" + weighted_string.Attribute;

            if (!string.IsNullOrEmpty(attributes))
                res += ", "+attributes;
            return res;
        }


        public WeightedString ReferenceString;

        public string String;
        /// <summary>
        /// ein string der etwas über den String beschreibt oder aussagt
        /// </summary>
        public string Attribute;
        /// <summary>
        /// a custom weight factor that can be set so make the weight of the string stronger
        /// </summary>
        public float? WeightFactor;

        /// <summary>
        /// man kann dem String auch anderen weighted strings anhängen, damit erhöht sich das gewicht des strings
        /// sollte dieses string in einer kette enstanden sein spielen die anderen strings eine rolle und deren gewicht
        /// diese wirken sich dann aus auf den aktuellen weight
        /// </summary>
        private List<WeightedString> _added_weights;

        private float? _custom_weight;
        private StringWeightCategory? _custom_weight_cat;
    }


    public class StringDetectionResults : List<StringDetectionResult>
    {
        public bool IsEmpty
        {
            get
            {
                return (Count == 0);
            }
        }

        public override string ToString()
        {
            StringDetectionResult? bst_match_by_count = BestMatchByCount;
            string res = "Weight "+ bst_match_by_count.Value.DetectionWeight+ " DetectedCount "+ Count+";";
            if (bst_match_by_count.HasValue)
                res = "BestMatch " + bst_match_by_count.Value.BestStringMatchByCount + ";" + res;
            foreach (var s in this)
            {
                foreach (StringDetectionInfo dts in s.DetectedStrings)
                {
                    res += " " + dts.DetectedString;
                }
            }
            return res;
        }

        public StringDetectionResult? BestMatchByWeight
        {
            get
            {
                if (this.Count == 0)
                    return null;
                return this.MaxBy(o => o.DetectionWeight);
            }
        }

        public StringDetectionResult? BestMatchByCount
        {
            get
            {
                var query = from s in this
                            group s by s.ComparerString
                            into g
                            select new { DetectedComparerStrings = g.Key, Count = g.Count(), Items = g };

                StringDetectionResult sdr = query.OrderByDescending(o => o.Count).Select(o => o.Items.FirstOrDefault()).FirstOrDefault();
                return sdr;
            }
        }

        public int BestMatchCount
        {
            get
            {
                var query = from s in this
                    group s by s.ComparerString
                    into g
                    select new { DetectedComparerStrings = g.Key, Count = g.Count(), Items = g };

                return  query.OrderByDescending(o => o.Count).Select(o => o.Count).FirstOrDefault();
            }
        }

        public string RawSourceText;
        public IEnumerable<WeightedString> LookForList;
    }

    public struct StringDetectionInfo 
    {
        public WeightedString LookFor;
        public WeightedString DetectedString;
        public float MatchAccuracy;
        public int DetectedStringIndex;

        public float DetectionWeight
        {
            get
            {
                return LookFor.Weight + DetectedString.Weight;
                //float weight = LookFor.Weight;
                //if (weight == 0 || Math.Abs(weight) < 0.000001)
                //    weight = WeightedString.GetNameWeight(DetectedString);
                //return weight;
            }
        }

        public override string ToString()
        {
            return DetectedString + " Weight " + DetectionWeight + " StringIndex " + DetectedStringIndex + " Accuracy" + MatchAccuracy;
        }
    }

    public struct StringDetectionResult
    {
        public void AssertListsNotNull()
        {
            if (DetectedStrings != null)
                return;
            DetectedStrings = new List<StringDetectionInfo>(1);
        }

        public bool IsEmpty
        {
            get
            {
                return (DetectedStrings == null || DetectedStrings.Count == 0);
            }
        }

        public override string ToString()
        {
            if (DetectedStrings == null)
                return "";
            string res = "Weight "+DetectionWeight+" DetectedCount " + DetectedStrings.Count + ";";
            res += BestStringMatchByCount;
            return res;
        }

        public int CountStringsDetected
        {
            get
            {
                if (DetectedStrings != null)
                    return DetectedStrings.Count;
                return 0;
            }
        }


        public WeightedString BestStringMatchByWeight
        {
            get
            {
                if (DetectedStrings == null)
                    return new WeightedString(null);
                var query = from s in DetectedStrings
                    group s by s.DetectedString
                    into g
                    select new { DetectedComparerStrings = g.Key, Count = g.Count(), Items = g };

                var query2 = query.MaxBy(o => o.Items.Sum(p => p.DetectionWeight)).Items.FirstOrDefault();
                return query2.DetectedString;
                //var best_candiate = query.OrderByDescending(o => o.Count).Select(o => o.Items.FirstOrDefault()).FirstOrDefault();
                //return best_candiate.DetectedString;
            }
        }

        public WeightedString BestStringMatchByCount
        {
            get
            {
                if (DetectedStrings == null)
                    return new WeightedString(null);
                var query = from s in DetectedStrings
                            group s by s.DetectedString
                            into g
                            select new { DetectedComparerStrings = g.Key, Count = g.Count(), Items = g };
                var best_candiate = query.OrderByDescending(o => o.Count).Select(o => o.Items.FirstOrDefault()).FirstOrDefault();
                return best_candiate.DetectedString;
            }
        }
        //public int BestStringMatchCount
        //{
        //    get
        //    {
        //        var query = from s in DetectedStrings
        //            group s by s.DetectedString
        //            into g
        //            select new { DetectedComparerStrings = g.Key, Count = g.Count(), Items = g };
        //        int best_candiate_count = query.OrderByDescending(o => o.Count).Select(o => o.Count).FirstOrDefault();
        //        return best_candiate_count;
        //    }
        //}

        public float DetectionWeight
        {
            get { return CountStringsDetected * DetectedStrings[0].DetectionWeight; }
        }

        public string RawSourceText;
        public List<StringDetectionInfo> DetectedStrings;
        public string ComparerString;

    }

    public class StringDetector
    {
        public enum DetectionStrategy
        {
            Exact,
            AllToLower,
            Nearby
        }

        public static string SepLines(string text, List<string> sep_words_list)
        {
            foreach (string sep_word in sep_words_list)
            {
                StringDetectionResult? sdr = DetectByStringLower(text, new WeightedString(sep_word));
                if (!sdr.HasValue)
                    continue;
                foreach (StringDetectionInfo sdi in sdr.Value.DetectedStrings.OrderByDescending(o=>o.DetectedStringIndex))
                    text = text.Insert(sdi.DetectedStringIndex, "\n");
            }
            return text;
        }

        public static string UnSepLines(string text, List<string> sep_words_list)
        {
            foreach (string sep_word in sep_words_list)
            {
                StringDetectionResult? sdr = DetectByStringLower(text, new WeightedString(sep_word));
                if (!sdr.HasValue)
                    continue;
                foreach (StringDetectionInfo sdi in sdr.Value.DetectedStrings.OrderByDescending(o => o.DetectedStringIndex))
                {
                    int index = sdi.DetectedStringIndex + sdi.DetectedString.String.Length;
                    int index_end = text.Length-1;
                    while (true)
                    {
                        char c = text[index];
                        if (char.IsLetterOrDigit(c))
                            break;
                        if (char.GetUnicodeCategory(c) == UnicodeCategory.LineSeparator || char.IsControl(c))
                            text = text.Remove(index, 1);
                        index++;
                        if (index > index_end)
                            break;
                    }
                }
            }
            return text;
        }



        public static StringDetectionResult? DetectByStringLower(string raw_text, WeightedString look_for)
        {
            look_for.String = look_for.String.ToLower();
            return DetectByStringExact(raw_text.ToLower(), look_for);
        }

        public static StringDetectionResult? DetectByStringExact(string text, WeightedString look_for)
        {
            StringDetectionResult res = new StringDetectionResult();
            res.RawSourceText = text;

            List<int> found_indices = text.IndexOfAll(look_for.String);
            for (int i = 0; i < found_indices.Count; i++)
            {
                SimilarStringComparer ssc = new SimilarStringComparer();
                float accuracy = ssc.IsEqual(look_for.String, text.Substring(found_indices[0], look_for.String.Length));

                res.AssertListsNotNull();

                StringDetectionInfo dt_info = new StringDetectionInfo();
                dt_info.LookFor = look_for;
                dt_info.DetectedString = new WeightedString(look_for.String, look_for.WeightCategory);
                dt_info.DetectedStringIndex = found_indices[i];
                dt_info.MatchAccuracy = accuracy;

                res.DetectedStrings.Add(dt_info);
            }

            if (res.IsEmpty)
                return null;
            return res;
        }

        public static StringDetectionResults DetectFromList(string raw_text, IEnumerable<WeightedString> look_for_list, DetectionStrategy strategy)
        {
            StringDetectionResults res = new StringDetectionResults();
            res.LookForList = look_for_list;

            if (strategy == DetectionStrategy.AllToLower)
                    raw_text = raw_text.ToLower();

            foreach (WeightedString look_for in look_for_list)
            {
                
                switch (strategy)
                {
                    case DetectionStrategy.Exact:
                        StringDetectionResult? curr_res_exact = DetectByStringExact(raw_text, look_for);
                        if (curr_res_exact.HasValue)
                            res.Add(curr_res_exact.Value);
                        break;
                    case DetectionStrategy.AllToLower:
                        WeightedString tolower_name_info = look_for;
                        tolower_name_info.String = look_for.String.ToLower();
                        StringDetectionResult? curr_res_lower = DetectByStringExact(raw_text, tolower_name_info);
                        if (curr_res_lower.HasValue)
                        {
                                //StringDetectionResult cur_res = new StringDetectionResult();
                                //cur_res.RawSourceText = raw_text;
                                //cur_res.ComparerString = name_info.Name;
                                //cur_res.DetectedStrings = cur_res.DetectedStrings;
                                //res.Add(cur_res);
                            res.Add(curr_res_lower.Value);
                        }
                        
                        break;
                    case DetectionStrategy.Nearby:
                        break;
                    default:
                        throw new ArgumentOutOfRangeException(nameof(strategy), strategy, null);
                }
            }

            return res;
        }


        public static List<string> DetectWords(string text, int start_index, int char_distance, int word_distance, int min_wordlen = 0, int max_word_len = int.MaxValue)
        {
            char[] split_chars = " .,:/\n\r".ToCharArray();

            string part_front = text.Substring(start_index, char_distance);
            string[] parts_front = part_front.Split(split_chars);

            start_index -= char_distance;
            if (start_index < 0)
            {
                char_distance += start_index;
                start_index = 0;
            }


            string part_back = text.Substring(start_index, char_distance);
            string[] parts_back = part_back.Split(split_chars);

            List<string> words = new List<string>();
            for (int i = 0; i < parts_back.Length; i++)
            {
                if (string.IsNullOrEmpty(parts_back[i]))
                    continue;
                if(parts_back[i].Length >= min_wordlen && parts_back[i].Length <= max_word_len)
                    words.Add(parts_back[i]);
            }

            int back_words_added = words.Count;
            int count_to_remove = back_words_added - word_distance;
            for (int i = 0; i < count_to_remove; i++)
                words.RemoveAt(0);

            int insert_index = 0;
            for (int i = 0;  i < parts_front.Length; i++)
            {
                if (string.IsNullOrEmpty(parts_front[i]))
                    continue;
                if (!(parts_front[i].Length >= min_wordlen && parts_front[i].Length <= max_word_len))
                    continue;
                words.Insert(insert_index, parts_front[i]);
                insert_index++;
                if (words.Count / 2 >= word_distance)
                    break;
            }

            return words;
        }

        public static bool IsAlphaNumericWord(string word, int min_wordlen = 0, int max_word_len = int.MaxValue, int min_numbers_count = 1, int min_letters_count = 1)
        {
            Regex regex = new Regex(@"([A-Za-z0-9-/]*[0-9]+)\w*");
            Match m = regex.Match(word);
            if (!regex.IsMatch(word))
                return false;

            int count_numbers = StringDetector.GetCountNumbersInString(word);
            int count_letters = StringDetector.GetCountLettersInString(word);

            if (count_letters < min_letters_count)
                return false;

            if (count_numbers < min_numbers_count)
                return false;

            int count_digits = count_letters + count_numbers;

            if (!(count_digits >= min_wordlen && count_digits <= max_word_len))
                return false;

            return true;
        }

        public static List<string> DetectAlphaNumericWords(string text, int start_index, int char_distance, int min_wordlen = 0, int max_word_len = int.MaxValue, int min_numbers_count = 1, int min_letters_count = 1)
        {
            start_index -= char_distance;

            if (start_index < 0)
            {
                char_distance = char_distance + start_index + char_distance;
                start_index = 0;
            }
            char_distance *= 2;
            if (char_distance + start_index > text.Length)
                char_distance = text.Length - start_index;

            string sub_text = text.Substring(start_index, char_distance);

            List<string> words = GetAlphaNumericWords(sub_text);
            List<string> detected_words = new List<string>(1);

            foreach (string s in words)
            {
                if (!IsAlphaNumericWord(s, min_wordlen, max_word_len, min_numbers_count, min_letters_count))
                    continue;

                if(!detected_words.Contains(s))
                    detected_words.Add(s);
            }
            return detected_words;
        }


        private static List<string> GetAlphaNumericWords(string text)
        {
            List<string> words = new List<string>(1);

            Regex regex = new Regex(@"([A-Za-z0-9-/]*[0-9]+)\w*");
            Match m = regex.Match(text);

            while (true)
            {
                if (!m.Success)
                    break;
                words.Add(m.Value);
                m = m.NextMatch();
            }
            return words;
        }

        public static int GetCountLettersInString(string s)
        {
            int digits_count = 0;
            foreach (char c in s)
            {
                if (Char.IsLetter(c))
                    digits_count++;
            }
            return digits_count;
        }

        public static int GetCountNumbersInString(string s)
        {
            int digits_count = 0;
            foreach (char c in s)
            {
                if (Char.IsNumber(c) || Char.IsDigit(c))
                    digits_count++;
            }
            return digits_count;
        }

        public static int GetCountSeparatorsPunctuationsInString(string s)
        {
            int digits_count = 0;
            foreach (char c in s)
            {
                if (Char.IsPunctuation(c) || Char.IsSeparator(c))
                    digits_count++;
            }

            return digits_count;
        }
    }
}
