﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Newtonsoft.Json;

namespace CognitiveLib.WebSearch
{
    public class GoogleCustomSearch
    {
        private static string _google_key_awanto_cognitive = "AIzaSyCJi3wYbhT1uTVYKnTv6VgL8a4_6c4rC98";

        public static dynamic GetResponseString(string query)
        {
            if (string.IsNullOrEmpty(query))
                return null;

            Uri google_custom_search = new Uri(string.Format("https://www.googleapis.com/customsearch/v1?key={1}&cx=017576662512468239146:omuauf_lfve&q={0}", HttpUtility.UrlEncode(query), _google_key_awanto_cognitive));
            WebClient client = new WebClient();
            Stream data = client.OpenRead(google_custom_search);
            StreamReader reader = new StreamReader(data);
            string s = reader.ReadToEnd();
            Console.WriteLine(s);
            data.Close();
            reader.Close();

            return JsonConvert.DeserializeObject(s);
        }
    }
}
