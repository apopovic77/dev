﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsLib
{
    public class ProcessUtil
    {
        public static Process CurrentProcess
        {
            get { return Process.GetCurrentProcess(); }
        }

        public static bool IsRunningInTerminalSession
        {
            get
            {
                try
                {
                    return SystemInformation.TerminalServerSession;
                    //Process proc = CurrentProcess;
                    //if (proc != null)
                    //    return proc.SessionId > 1;
                }
                catch
                {
                    
                }

                return false;
            }
        }
    }
}
