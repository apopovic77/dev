﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Data;
using Logicx.DiagramLib.Scales;

namespace Logicx.DiagramLib.Resources.Converter
{
    public class MinSizeValueToPositionConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (values.Length == 4)
            {
                object x_value = values[0];

                if (values[1] == DependencyProperty.UnsetValue || values[2] == DependencyProperty.UnsetValue || values[3] == DependencyProperty.UnsetValue)
                    return 0;

                double scalue_unit_size = (double)values[1];
                ScaleControlBase scale_control = values[2] as ScaleControlBase;
                double actual_height = (double)values[3];

                if (x_value != null && scale_control != null)
                {
                    double val;
                    double top;
                    if (scale_control is DiscreteScaleControl<Object>)
                    {
                        double unit_size = scale_control.GetUnitSize();
                        double x = scale_control.GenericTransformAxisValueToPixel(x_value, false);
                        top = x - DiscreteScale.UNIT_SIZE_FACTOR * unit_size / 2;
                        val = Math.Abs(actual_height - top);
                    }
                    else
                    {
                        top = scale_control.GenericTransformAxisValueToPixel(x_value, false);
                        if(double.IsInfinity(top))
                            scale_control.GenericTransformAxisValueToPixel(x_value, false);
                        val = Math.Abs(actual_height - top);
                    }

                    if (parameter != null && parameter is double && val < (double)parameter)
                        top -= (double)parameter - val;

                    //if (double.IsNaN(top))
                    //    Debug.WriteLine("NAN IN MINSIZETOPOSITIONCONVERTER");
                    //if (double.IsInfinity(top))
                    //{
                    //    Debug.WriteLine("INFINITY IN MINSIZETOPOSITIONCONVERTER");
                    //    return 0;
                    //}
                    return top;
                }
            }

            return 0;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }
}
