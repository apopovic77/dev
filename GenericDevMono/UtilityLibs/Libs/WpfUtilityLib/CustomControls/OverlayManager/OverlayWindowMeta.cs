﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using Logicx.WpfUtility.CustomControls.OverlayManager.StatusInfo;
using MathLib;

namespace Logicx.WpfUtility.CustomControls.OverlayManager
{
    public enum OverlayAnimationType
    {
        None,
        Custom,
        LoadingStart,
        LoadingEnd,
        ModalToMinimize,
        ModalToMaximize,
        MinimizedToModal,
        MaximizedToModal,
        NormalToMinimize,
        MinimizedToNormal,
        MinimizeToMaximize,
        NormalToMaximize,
        MaximizeToNormal,
        MaximizeToMinimize,
        FadeInOpacity,
        FadOutOpacity,
        CustomActivate,
        CustomDeactivate
    }

    public enum OverlayAnimationTriggerSource
    {
        Undefined,
        Custom,
        Loading,
        ActiveStateChange,
        WindowStateChange
    }

    public class OverlayWindowMeta : IDisposable
    {

        #region nested classes
        public class OverlayAnimationProperties
        {
            public double FinalLeft = double.NaN;
            public double FinalTop = double.NaN;
            public double FinalOpacity = double.NaN;
            public double FinalScalex = double.NaN;
            public double FinalScaley = double.NaN;
        }

        public class OverlayAnimationCompletedArgs : EventArgs
        {
            public OverlayAnimationCompletedArgs(OverlayAnimationType anim_type, FrameworkElement animation_target)
            {
                _animation_type = anim_type;
                _animation_target = animation_target;
            }

            public OverlayAnimationCompletedArgs(OverlayAnimationType anim_type, FrameworkElement animation_target, object argument)
                : this(anim_type, animation_target)
            {
                _argument = argument;
            }

            public OverlayAnimationCompletedArgs(OverlayAnimationType anim_type, FrameworkElement animation_target, object argument, OverlayAnimationTriggerSource source) :
                this(anim_type, animation_target, argument)
            {
                _trigger_source = source;
            }

            public OverlayAnimationType AnimationType
            {
                get { return _animation_type; }
            }

            public FrameworkElement AnimationTarget
            {
                get { return _animation_target; }
            }

            public object Argument
            {
                get { return _argument; }
            }

            public OverlayAnimationTriggerSource TriggerSource
            {
                get { return _trigger_source; }
            }

            private OverlayAnimationType _animation_type = OverlayAnimationType.None;
            private object _argument = null;
            private FrameworkElement _animation_target = null;
            private OverlayAnimationTriggerSource _trigger_source = OverlayAnimationTriggerSource.Undefined;
        }

        public class OverlayMetaDialogComparer : EqualityComparer<OverlayDialog>
        {
            public override bool Equals(OverlayDialog x, OverlayDialog y)
            {
                if (x == null && y == null)
                    return true;

                if (x == null && y != null)
                    return false;

                return x.Equals(y);
            }

            public override int GetHashCode(OverlayDialog obj)
            {
                if (obj == null)
                    return 0;

                return obj.GetHashCode();
            }
        }
        #endregion

        public OverlayWindowMeta()
        {
            AnimationTriggerSource = OverlayAnimationTriggerSource.Undefined;
            Layer = OverlayWindowLayer.Normal;
            FallbackLayer = OverlayWindowLayer.Normal;
            DialogLayerZindex = new Dictionary<OverlayWindowLayer, int>();
            DialogLayerZindex[Layer] = 1;
            ScreenshotLayerZindex = new Dictionary<OverlayWindowLayer, int>();
            ScreenshotLayerZindex[Layer] = 2;

        }

        public OverlayDialog Dialog
        {
            get { return _dialog; }
            set
            {
                DependencyPropertyDescriptor descrLeft = DependencyPropertyDescriptor.FromProperty(Canvas.LeftProperty, typeof(OverlayDialog));
                DependencyPropertyDescriptor descrTop = DependencyPropertyDescriptor.FromProperty(Canvas.TopProperty, typeof(OverlayDialog));
                if (_dialog != null)
                {
                    descrLeft.RemoveValueChanged(_dialog, CanvasLeftTopChanged);
                    descrTop.RemoveValueChanged(_dialog, CanvasLeftTopChanged);
                }

                _dialog = value;

                if (_dialog != null)
                {
                    CanvasLeftTopChanged(_dialog, null);

                    descrLeft.AddValueChanged(_dialog, CanvasLeftTopChanged);
                    descrTop.AddValueChanged(_dialog, CanvasLeftTopChanged);
                }
            }
        }

        public OverlayPictureHost DialogWindowScreenshot
        {
            get { return _normal_screenshot; }
            //set { _normal_screenshot = value; }
        }

        public OverlayPictureHost DialogThumbScreenshot
        {
            get { return _thumb_screenshot; }
            //set { _thumb_screenshot = value; }
        }

        public Point DialogNormalCanvasPosition
        {
            get { return _normal_canvas_position; }
        }

        public bool SetActiveAfterStateChange { get; set; }

        public bool InitializationFinshed { get; set; }
        public bool LoadingStartAnimationFinished { get; set; }
        public bool LoadingEndAnimationFinished { get; set; }

        public bool DialogMoved { get; set; }
        public Dictionary<OverlayWindowLayer, int> DialogLayerZindex { get; set; }
        public Dictionary<OverlayWindowLayer, int> ScreenshotLayerZindex { get; set; }

        internal List<OverlayWindowMeta> ReactivateAfterModalClose { get; set; }
        internal List<OverlayWindowMeta> NormalizeAfterModalClose { get; set; }
        internal List<OverlayWindowMeta> ModalAfterModalClose { get; set; }

        public bool IgnoreMouseInputs
        {
            set
            {
                _ignore_mouse_inputs = value;
                if(_ignore_mouse_inputs)
                {
                    this.Dialog.MouseUp += new MouseButtonEventHandler(Dialog_MouseUp);
                    this.Dialog.MouseDown += new MouseButtonEventHandler(Dialog_MouseDown);
                    this.DialogWindowScreenshot.MouseDown += new MouseButtonEventHandler(DialogWindowScreenshot_MouseDown);
                    this.DialogWindowScreenshot.MouseUp += new MouseButtonEventHandler(DialogWindowScreenshot_MouseUp);
                }
                else
                {
                    this.Dialog.MouseUp -= Dialog_MouseUp;
                    this.Dialog.MouseDown -= Dialog_MouseDown;
                    this.DialogWindowScreenshot.MouseDown -= DialogWindowScreenshot_MouseDown;
                    this.DialogWindowScreenshot.MouseUp -= DialogWindowScreenshot_MouseUp;
                }
            }
            get
            {
                return _ignore_mouse_inputs;
            }
        }

        public bool IsActive
        {
            get
            {
                return _is_active;
            }
            set
            {
                if (_is_active != value)
                {
                    _is_active = value;
                    if (Dialog != null)
                    {
                        if (value)
                        {
                            Dialog.SetActive();
                        }
                        else
                        {
                            Dialog.SetInActive();
                        }
                    }
                }
            }
        }
        public bool IsDragActive { get; set; }
        public MouseDevice DragMouseDevice { get; set; }

        public OverlayWindowLayer Layer { get; set; }
        public OverlayWindowLayer FallbackLayer { get; set; }

        public Vector2f DragOffset { get; set; }

        public double WidthBeforeAnimtion
        {
            set
            {
                //if(_width_before_anim == 0)
                _width_before_anim = value;
            }
            get
            {
                return _width_before_anim;
            }
        }
        public double HeightBeforeAnimation
        {
            set
            {
                //if(_height_before_anim == 0)
                _height_before_anim = value;
            }
            get
            {
                return _height_before_anim;
            }
        }

        public OverlayAnimationTriggerSource AnimationTriggerSource { get; set; }

        public OverlayShowParams ShowParams { set; get; }

        public StackPanel TaskbarPlaceholder;

        public FrameworkElement ClickedWindowTaskBarElement { set; get; }

        #region Equality comparer
        public OverlayMetaDialogComparer DialogComparer
        {
            get
            {
                if (_dialog_comparer == null)
                    _dialog_comparer = new OverlayMetaDialogComparer();

                return _dialog_comparer;
            }
        }
        #endregion

        #region Operations
        public void CalculateDragOffset(MouseButtonEventArgs e, Canvas mainCanvas)
        {
            double canv_left = Canvas.GetLeft(Dialog);
            double canv_top = Canvas.GetTop(Dialog);

            Point canvasMousePos = e.GetPosition(mainCanvas);

            DragOffset = new Vector2f((float)canvasMousePos.X - (float)canv_left, (float)canvasMousePos.Y - (float)canv_top);
        }

        public void ResetAnimatedProperties()
        {
            Debug.WriteLine("||XXXX||||  ResetAnimatedProeprties()");

            if (IsAnimationRunning || Dialog == null)
            {
                _reset_animations_requested = true;
                Debug.WriteLine("||XXXX||||  Animation reset QUEUED");
                return;
            }

            // reset animated proeprties
            double dialog_cur_left = Canvas.GetLeft(Dialog);
            double dialog_cur_top = Canvas.GetTop(Dialog);
            int dialog_zindex = Canvas.GetZIndex(Dialog);
            double dialog_cur_opacity = Dialog.Opacity;

            Dialog.BeginAnimation(Canvas.LeftProperty, null);
            Canvas.SetLeft(Dialog, dialog_cur_left);

            Dialog.BeginAnimation(Canvas.TopProperty, null);
            Canvas.SetTop(Dialog, dialog_cur_top);

            Dialog.BeginAnimation(Canvas.ZIndexProperty, null);
            Canvas.SetZIndex(Dialog, dialog_zindex);

            Dialog.BeginAnimation(FrameworkElement.OpacityProperty, null);
            Dialog.Opacity = dialog_cur_opacity;

            ScaleTransform s_tran = DialogWindowScreenshot.RenderTransform as ScaleTransform;
            if (s_tran != null)
            {
                double old_sx = s_tran.ScaleX;
                double old_sy = s_tran.ScaleY;
                s_tran.BeginAnimation(ScaleTransform.ScaleXProperty, null);
                s_tran.BeginAnimation(ScaleTransform.ScaleYProperty, null);
                s_tran.ScaleX = old_sx;
                s_tran.ScaleY = old_sy;
            }

            double screen_cur_left = Canvas.GetLeft(DialogWindowScreenshot);
            double screen_cur_top = Canvas.GetTop(DialogWindowScreenshot);
            int screen_zindex = Canvas.GetZIndex(DialogWindowScreenshot);
            double screen_cur_opacity = DialogWindowScreenshot.Opacity;

            DialogWindowScreenshot.BeginAnimation(Canvas.LeftProperty, null);
            Canvas.SetLeft(DialogWindowScreenshot, screen_cur_left);

            DialogWindowScreenshot.BeginAnimation(Canvas.TopProperty, null);
            Canvas.SetTop(DialogWindowScreenshot, screen_cur_top);

            DialogWindowScreenshot.BeginAnimation(Canvas.ZIndexProperty, null);
            Canvas.SetZIndex(DialogWindowScreenshot, screen_zindex);

            DialogWindowScreenshot.BeginAnimation(FrameworkElement.OpacityProperty, null);
            DialogWindowScreenshot.Opacity = screen_cur_opacity;
        }

        public void WaitForAnimationFinished()
        {
            int max_wait_time = 1000;
            int step_size = 100;
            int current_waittime = 0;

            //while(current_waittime < max_wait_time)
            //{
            //    if(!IsAnimationRunning)
            //        return;

            //    Thread.Sleep(step_size);
            //    current_waittime += step_size;
            //}

            // Alle Animationen beenden
            if (IsAnimationRunning)
            {
                List<string> running_storyboards_keys = _running_storyboards.Keys.ToList();
                if (_running_storyboards.Count > 0)
                {
                    foreach (string sb_name in running_storyboards_keys)
                    {
                        StopAnimation(sb_name);
                    }
                }
                //int count = _running_storyboards.Count;
                //for (int i = 0; i < count; i++)
                //{
                //    StopAnimation(_running_storyboards[0]);
                //}


                _running_storyboards.Clear();
                _final_animation_properties.Clear();
                ResetAnimatedProperties();
            }

        }

        private bool IsAnimationTypeRunning(string type)
        {
            if (_running_storyboards.Count > 0)
            {
                foreach (string s in _running_storyboards.Keys)
                {
                    if (s.StartsWith(type))
                        return true;
                }
            }
            return false;
        }

        private string GetAnimationNameByType(string type)
        {
            if (_running_storyboards.Count > 0)
            {
                foreach (string s in _running_storyboards.Keys)
                {
                    if (s.StartsWith(type))
                        return s;
                }
            }
            return "";
        }

        private void MarkAnimationRunning(string storyboard_name, Storyboard sb, OverlayAnimationProperties animation_prop)
        {
            if (!_running_storyboards.ContainsKey(storyboard_name))
            {
                //_running_storyboards.Add(storyboard_name);
                _running_storyboards[storyboard_name] = sb;
            }

            if (!_final_animation_properties.ContainsKey(storyboard_name))
                _final_animation_properties[storyboard_name] = animation_prop;

            if (_animation_targets.ContainsKey(storyboard_name))
            {
                if (_animation_targets[storyboard_name] != null && _animation_targets[storyboard_name] is OverlayPictureHost)
                {
                    ((OverlayPictureHost)_animation_targets[storyboard_name]).SetIsAnimating(true);
                }
            }
        }

        private void RemoveAnimationRunning(string storyboard_name)
        {
            if (_running_storyboards.ContainsKey(storyboard_name))
            {
                if (_running_storyboards[storyboard_name] != null)
                {
                    _running_storyboards[storyboard_name].Completed -= CustomStoryboardCompleted;
                }
                _running_storyboards.Remove(storyboard_name);
            }

            if (_animation_targets.ContainsKey(storyboard_name))
            {
                if (_animation_targets[storyboard_name] != null && _animation_targets[storyboard_name] is OverlayPictureHost)
                {
                    ((OverlayPictureHost)_animation_targets[storyboard_name]).SetIsAnimating(false);
                }
            }

            if (!IsAnimationRunning && _reset_animations_requested)
            {
                ResetAnimatedProperties();
                _reset_animations_requested = false;

            }

            SetFinalPositionAfterAnimation(storyboard_name);
        }

        private void SetFinalPositionAfterAnimation(string storyboard_name)
        {
            if (_final_animation_properties.ContainsKey(storyboard_name))
            {
                OverlayAnimationProperties prop = _final_animation_properties[storyboard_name];

                ScaleTransform s_tran = DialogWindowScreenshot.RenderTransform as ScaleTransform;
                if (s_tran != null)
                {
                    if (prop != null && !Double.IsNaN(prop.FinalScalex) && !Double.IsInfinity(prop.FinalScalex) && !Double.IsNaN(prop.FinalScaley) && !Double.IsInfinity(prop.FinalScaley))
                    {
                        s_tran.ScaleX = prop.FinalScalex;
                        s_tran.ScaleY = prop.FinalScaley;
                    }
                }

                if (prop != null && !Double.IsNaN(prop.FinalLeft) && !Double.IsInfinity(prop.FinalLeft))
                    Canvas.SetLeft(DialogWindowScreenshot, prop.FinalLeft);

                if (prop != null && !Double.IsNaN(prop.FinalTop) && !Double.IsInfinity(prop.FinalTop))
                    Canvas.SetTop(DialogWindowScreenshot, prop.FinalTop);

                if (prop != null && !Double.IsNaN(prop.FinalOpacity) && !Double.IsInfinity(prop.FinalOpacity))
                    DialogWindowScreenshot.Opacity = prop.FinalOpacity;

                _final_animation_properties.Remove(storyboard_name);
            }
        }

        private void StopAnimation(string s)
        {
            Storyboard temp_sb = new Storyboard();
            temp_sb.Name = s;

            if (s.StartsWith(OverlayAnimationType.Custom.ToString()))
                CustomStoryboardCompleted(temp_sb, new EventArgs());
            else if (s.StartsWith(OverlayAnimationType.CustomActivate.ToString()))
                CustomActivateStoryboardCompleted(temp_sb, new EventArgs());
            else if (s.StartsWith(OverlayAnimationType.CustomDeactivate.ToString()))
                CustomDeactivateStoryboardCompleted(temp_sb, new EventArgs());
            else if (s.StartsWith(OverlayAnimationType.FadeInOpacity.ToString()))
                FadeInOpacityStoryboardCompleted(temp_sb, new EventArgs());
            else if (s.StartsWith(OverlayAnimationType.FadOutOpacity.ToString()))
                FadOutOpacityStoryboardCompleted(temp_sb, new EventArgs());
            else if (s.StartsWith(OverlayAnimationType.LoadingEnd.ToString()))
                LoadingEndStoryboardCompleted(temp_sb, new EventArgs());
            else if (s.StartsWith(OverlayAnimationType.LoadingStart.ToString()))
                LoadingStartStoryboardCompleted(temp_sb, new EventArgs());
            else if (s.StartsWith(OverlayAnimationType.MaximizedToModal.ToString()))
                MaximizedToModalStoryboardCompleted(temp_sb, new EventArgs());
            else if (s.StartsWith(OverlayAnimationType.MaximizeToMinimize.ToString()))
                MaximizeToMinimizeStoryboardCompleted(temp_sb, new EventArgs());
            else if (s.StartsWith(OverlayAnimationType.MaximizeToNormal.ToString()))
                MaximizeToNormalStoryboardCompleted(temp_sb, new EventArgs());
            else if (s.StartsWith(OverlayAnimationType.MinimizedToModal.ToString()))
                MinimizedToModalStoryboardCompleted(temp_sb, new EventArgs());
            else if (s.StartsWith(OverlayAnimationType.MinimizedToNormal.ToString()))
                MinimizedToNormalStoryboardCompleted(temp_sb, new EventArgs());
            else if (s.StartsWith(OverlayAnimationType.MinimizeToMaximize.ToString()))
                MinimizeToMaximizeStoryboardCompleted(temp_sb, new EventArgs());
            else if (s.StartsWith(OverlayAnimationType.ModalToMaximize.ToString()))
                ModalToMaximizeStoryboardCompleted(temp_sb, new EventArgs());
            else if (s.StartsWith(OverlayAnimationType.ModalToMinimize.ToString()))
                ModalToMinimizeStoryboardCompleted(temp_sb, new EventArgs());
            else if (s.StartsWith(OverlayAnimationType.NormalToMaximize.ToString()))
                NormalToMaximizeStoryboardCompleted(temp_sb, new EventArgs());
            else if (s.StartsWith(OverlayAnimationType.NormalToMinimize.ToString()))
                NormalToMinimizeStoryboardCompleted(temp_sb, new EventArgs());

        }

        public bool IsAnimationRunning
        {
            get
            {
                return _running_storyboards.Count > 0;
            }
        }
        #region Animations
        //public void StartLoadingAnimation()
        //{
        //    if (Dialog.DialogContent.LoadingStoryboard != null)
        //    {

        //        // ladeanimation wurde definiert
        //        LoadingAnimationFinished = false;
        //        Dialog.DialogContent.LoadingStoryboard.Name = OverlayAnimationType.Loading.ToString() + "_" + Dialog.DialogContent.LoadingStoryboard.GetHashCode().ToString();

        //        _animation_arguments[Dialog.DialogContent.LoadingStoryboard.Name] = null;
        //        _animation_trigger_source[Dialog.DialogContent.LoadingStoryboard.Name] = AnimationTriggerSource;

        //        Dialog.DialogContent.LoadingStoryboard.Completed += new EventHandler(LoadingStoryboardCompleted);
        //        DialogWindowScreenshot.BeginStoryboard(Dialog.DialogContent.LoadingStoryboard);
        //    }
        //    else
        //    {
        //        LoadingAnimationFinished = true;
        //        if (OverlayAnimationFinished != null)
        //            OverlayAnimationFinished(this, new OverlayAnimationCompletedArgs(OverlayAnimationType.Loading, null, AnimationTriggerSource));
        //    }
        //}

        public void StartLoadingStartAnimation()
        {
            if (Dialog.DialogContent.LoadingStartStoryboard != null)
            {
                DialogWindowScreenshot.Visibility = Visibility.Visible;
                // ladeanimation wurde definiert
                LoadingStartAnimationFinished = false;
                Dialog.DialogContent.LoadingStartStoryboard.Name = OverlayAnimationType.LoadingStart.ToString() + "_" +
                    Dialog.DialogContent.LoadingStartStoryboard.GetHashCode().ToString();

                _animation_arguments[Dialog.DialogContent.LoadingStartStoryboard.Name] = null;
                _animation_trigger_source[Dialog.DialogContent.LoadingStartStoryboard.Name] = AnimationTriggerSource;
                _animation_targets[Dialog.DialogContent.LoadingEndStoryboard.Name] = DialogWindowScreenshot;

                Dialog.DialogContent.LoadingStartStoryboard.Completed += new EventHandler(LoadingStartStoryboardCompleted);
                MarkAnimationRunning(Dialog.DialogContent.LoadingStartStoryboard.Name, Dialog.DialogContent.LoadingStartStoryboard, Dialog.DialogContent.LoadingStartStoryboardFinalPos);
                DialogWindowScreenshot.BeginStoryboard(Dialog.DialogContent.LoadingStartStoryboard);

            }
            else
            {
                LoadingStartAnimationFinished = true;
                if (OverlayAnimationFinished != null)
                    OverlayAnimationFinished(this, new OverlayAnimationCompletedArgs(OverlayAnimationType.LoadingStart, null, AnimationTriggerSource));
            }
        }

        public void StartLoadingEndAnimation()
        {
            if (Dialog != null && Dialog.DialogContent.LoadingEndStoryboard != null)
            {
                DialogWindowScreenshot.Visibility = Visibility.Visible;
                // ladeanimation wurde definiert
                LoadingEndAnimationFinished = false;
                Dialog.DialogContent.LoadingEndStoryboard.Name = OverlayAnimationType.LoadingEnd.ToString() + "_" +
                    Dialog.DialogContent.LoadingEndStoryboard.GetHashCode().ToString();

                _animation_arguments[Dialog.DialogContent.LoadingEndStoryboard.Name] = null;
                _animation_trigger_source[Dialog.DialogContent.LoadingEndStoryboard.Name] = AnimationTriggerSource;
                _animation_targets[Dialog.DialogContent.LoadingEndStoryboard.Name] = DialogWindowScreenshot;

                Dialog.DialogContent.LoadingEndStoryboard.Completed += new EventHandler(LoadingEndStoryboardCompleted);
                MarkAnimationRunning(Dialog.DialogContent.LoadingEndStoryboard.Name, Dialog.DialogContent.LoadingEndStoryboard, Dialog.DialogContent.LoadingEndStoryboardFinalPos);
                DialogWindowScreenshot.BeginStoryboard(Dialog.DialogContent.LoadingEndStoryboard);
            }
            else
            {
                LoadingEndAnimationFinished = true;
                if (OverlayAnimationFinished != null)
                    OverlayAnimationFinished(this, new OverlayAnimationCompletedArgs(OverlayAnimationType.LoadingEnd, null, AnimationTriggerSource));
            }
        }

        public void StartAnimation(Storyboard sb, OverlayAnimationType type, OverlayAnimationProperties anim_prop)
        {
            StartAnimation(sb, DialogWindowScreenshot, type, null, AnimationTriggerSource, anim_prop);
        }
        public void StartAnimation(Storyboard sb, OverlayAnimationType type, object argument, OverlayAnimationTriggerSource source, OverlayAnimationProperties anim_prop)
        {
            StartAnimation(sb, DialogWindowScreenshot, type, argument, source, anim_prop);
        }

        public void StartAnimation(Storyboard sb, FrameworkElement target, OverlayAnimationType type, OverlayAnimationProperties anim_prop)
        {
            StartAnimation(sb, target, type, null, AnimationTriggerSource, anim_prop);
        }
        public void StartAnimation(Storyboard sb, FrameworkElement target, OverlayAnimationType type, object argument, OverlayAnimationTriggerSource source, OverlayAnimationProperties anim_prop)
        {
            if (target == null || sb == null)
                return;


            if (IsAnimationTypeRunning(type.ToString()))
            {
                return;
                Storyboard temp_sb = new Storyboard();
                temp_sb.Name = GetAnimationNameByType(OverlayAnimationType.FadOutOpacity.ToString());
                switch (type)
                {
                    case OverlayAnimationType.Custom:
                        CustomStoryboardCompleted(temp_sb, new EventArgs());
                        break;
                    case OverlayAnimationType.CustomActivate:
                        CustomActivateStoryboardCompleted(temp_sb, new EventArgs());
                        break;
                    case OverlayAnimationType.MinimizedToNormal:
                        MinimizedToNormalStoryboardCompleted(temp_sb, new EventArgs());
                        break;

                }

                DialogWindowScreenshot.BeginAnimation(FrameworkElement.OpacityProperty, null);
                DialogWindowScreenshot.Opacity = 1;
            }

            // generate unique name
            sb.Name = type.ToString() + "_" + sb.GetHashCode().ToString();
#if DEBUG
            Debug.WriteLine("** Starting animation on storyboard: " + sb.GetHashCode().ToString() + " ('" + sb.Name + "')");
            Debug.WriteLine("***** Dialog: " + GetHashCode().ToString());
#endif

            _animation_arguments[sb.Name] = argument;
            _animation_trigger_source[sb.Name] = source;
            _animation_targets[sb.Name] = target;

            ScaleTransform s_tran = target.RenderTransform as ScaleTransform;

            switch (type)
            {
                case OverlayAnimationType.LoadingStart:
                    sb.Completed += new EventHandler(LoadingStartStoryboardCompleted);
                    break;
                case OverlayAnimationType.LoadingEnd:
                    sb.Completed += new EventHandler(LoadingEndStoryboardCompleted);
                    break;
                case OverlayAnimationType.NormalToMinimize:
                    sb.Completed += new EventHandler(NormalToMinimizeStoryboardCompleted);
                    break;
                case OverlayAnimationType.NormalToMaximize:
                    sb.Completed += new EventHandler(NormalToMaximizeStoryboardCompleted);
                    break;
                case OverlayAnimationType.MinimizedToNormal:
                    sb.Completed += new EventHandler(MinimizedToNormalStoryboardCompleted);
                    break;
                case OverlayAnimationType.MinimizeToMaximize:
                    sb.Completed += new EventHandler(MinimizeToMaximizeStoryboardCompleted);
                    break;
                case OverlayAnimationType.MaximizeToNormal:
                    sb.Completed += new EventHandler(MaximizeToNormalStoryboardCompleted);
                    break;
                case OverlayAnimationType.MaximizeToMinimize:
                    sb.Completed += new EventHandler(MaximizeToMinimizeStoryboardCompleted);
                    break;
                case OverlayAnimationType.MinimizedToModal:
                    sb.Completed += new EventHandler(MinimizedToModalStoryboardCompleted);
                    break;
                case OverlayAnimationType.MaximizedToModal:
                    sb.Completed += new EventHandler(MaximizedToModalStoryboardCompleted);
                    break;
                case OverlayAnimationType.ModalToMaximize:
                    sb.Completed += new EventHandler(ModalToMaximizeStoryboardCompleted);
                    break;
                case OverlayAnimationType.ModalToMinimize:
                    sb.Completed += new EventHandler(ModalToMinimizeStoryboardCompleted);
                    break;
                case OverlayAnimationType.Custom:
                    sb.Completed += new EventHandler(CustomStoryboardCompleted);
                    break;
                case OverlayAnimationType.FadeInOpacity:
                    {
                        if (_active_storyboard != null)
                        {
                            _active_storyboard.Stop();
                            FadOutOpacityStoryboardCompleted(_active_storyboard, new EventArgs());

                            if (Dialog.DialogContent.GoInactiveBehaviuor == OverlayContent.GoInactiveBehaviuorEnum.OpacityAnimationDialog)
                            {
                                Dialog.BeginAnimation(FrameworkElement.OpacityProperty, null);
                                Dialog.Opacity = 1;
                            }
                            else
                            {
                                DialogWindowScreenshot.BeginAnimation(FrameworkElement.OpacityProperty, null);
                                DialogWindowScreenshot.Opacity = 1;
                            }
                        }
                        if (IsAnimationTypeRunning(OverlayAnimationType.FadOutOpacity.ToString()))
                        {
                            Storyboard temp_sb = new Storyboard();
                            temp_sb.Name = GetAnimationNameByType(OverlayAnimationType.FadOutOpacity.ToString());
                            FadOutOpacityStoryboardCompleted(temp_sb, new EventArgs());

                            if (Dialog.DialogContent.GoInactiveBehaviuor == OverlayContent.GoInactiveBehaviuorEnum.OpacityAnimationDialog)
                            {
                                Dialog.BeginAnimation(FrameworkElement.OpacityProperty, null);
                                Dialog.Opacity = 1;
                            }
                            else
                            {
                                DialogWindowScreenshot.BeginAnimation(FrameworkElement.OpacityProperty, null);
                                DialogWindowScreenshot.Opacity = 1;
                            }
                        }

                        _active_storyboard = sb;
                        sb.Completed += new EventHandler(FadeInOpacityStoryboardCompleted);
                    }
                    break;
                case OverlayAnimationType.FadOutOpacity:
                    {
                        if (_active_storyboard != null)
                        {
                            _active_storyboard.Stop();
                            FadeInOpacityStoryboardCompleted(_active_storyboard, new EventArgs());
                            if (Dialog.DialogContent.GoInactiveBehaviuor == OverlayContent.GoInactiveBehaviuorEnum.OpacityAnimationDialog)
                            {
                                Dialog.BeginAnimation(FrameworkElement.OpacityProperty, null);
                                Dialog.Opacity = 1;
                            }
                            else
                            {
                                DialogWindowScreenshot.BeginAnimation(FrameworkElement.OpacityProperty, null);
                                DialogWindowScreenshot.Opacity = 1;
                            }
                        }
                        if (IsAnimationTypeRunning(OverlayAnimationType.FadeInOpacity.ToString()))
                        {
                            Storyboard temp_sb = new Storyboard();
                            temp_sb.Name = GetAnimationNameByType(OverlayAnimationType.FadeInOpacity.ToString());
                            FadeInOpacityStoryboardCompleted(temp_sb, new EventArgs());
                            if (Dialog.DialogContent.GoInactiveBehaviuor == OverlayContent.GoInactiveBehaviuorEnum.OpacityAnimationDialog)
                            {
                                Dialog.BeginAnimation(FrameworkElement.OpacityProperty, null);
                                Dialog.Opacity = 1;
                            }
                            else
                            {
                                DialogWindowScreenshot.BeginAnimation(FrameworkElement.OpacityProperty, null);
                                DialogWindowScreenshot.Opacity = 1;
                            }
                        }

                        _active_storyboard = sb;
                        sb.Completed += new EventHandler(FadOutOpacityStoryboardCompleted);
                    }
                    break;
                case OverlayAnimationType.CustomActivate:
                    {
                        if (_active_storyboard != null)
                            _active_storyboard.Stop();

                        _active_storyboard = sb;
                        sb.Completed += new EventHandler(CustomActivateStoryboardCompleted);
                    }
                    break;
                case OverlayAnimationType.CustomDeactivate:
                    {
                        if (_active_storyboard != null)
                        {
                            _active_storyboard.Stop();
                            FadeInOpacityStoryboardCompleted(_active_storyboard, new EventArgs());
                            DialogWindowScreenshot.BeginAnimation(FrameworkElement.OpacityProperty, null);
                            DialogWindowScreenshot.Opacity = 1;
                        }

                        _active_storyboard = sb;
                        sb.Completed += new EventHandler(CustomDeactivateStoryboardCompleted);
                    }
                    break;
            }
            MarkAnimationRunning(sb.Name,sb, anim_prop);
            sb.Begin(target);
        }
        #endregion

        public void ShowLoadingInfoControl()
        {
            if (Dialog.DialogContent.LoadingInformationControl != null)
            {
                //DialogWindowScreenshot.SetInformationControl(Dialog.DialogContent.LoadingInformationControl);
                SetWindowScreenControl(Dialog.DialogContent.LoadingInformationControl);
            }
        }


        public void SetWindowScreenControl(UIElement control)
        {
            DialogWindowScreenshot.SetInformationControl(control);
        }

        public void RemoveWindowScreenControl()
        {
            DialogWindowScreenshot.RemoveInformationControl(true);
        }
        #endregion

        #region Event handler
        void CanvasLeftTopChanged(object sender, EventArgs e)
        {
            if (sender == null || !(sender is UIElement))
                return;

            double left = Canvas.GetLeft(sender as UIElement);
            double top = Canvas.GetTop(sender as UIElement);

            _normal_canvas_position.X = left;
            _normal_canvas_position.Y = top;
        }

        #region animations
        void LoadingStartStoryboardCompleted(object sender, EventArgs e)
        {
            StoryboardCompletedActions(sender, OverlayAnimationType.LoadingStart);
            ////            if (sender is Storyboard)
            ////                ((Storyboard)sender).Completed -= new EventHandler(LoadingStartStoryboardCompleted);
            ////            else
            ////                ((ClockGroup)sender).Completed -= new EventHandler(LoadingStartStoryboardCompleted);
            ////#if DEBUG
            ////            Debug.WriteLine("** Animation of storyboard: " + ((ClockGroup)sender).GetHashCode().ToString() + " ('" + ((ClockGroup)sender).Timeline.Name + "') completed");
            ////            System.Diagnostics.Debug.WriteLine("*** LOADING START Storyboard Finished");
            ////            Debug.WriteLine("***** Dialog: " + GetHashCode().ToString());
            ////#endif
            ////            string sbName = "";
            ////            if (sender is Storyboard)
            ////                sbName = ((Storyboard)sender).Name;
            ////            else
            ////                sbName = ((ClockGroup)sender).Timeline.Name;
            ////            RemoveAnimationRunning(sbName);

            ////            LoadingStartAnimationFinished = true;
            ////            if (OverlayAnimationFinished != null)
            ////                OverlayAnimationFinished(this, new OverlayAnimationCompletedArgs(OverlayAnimationType.LoadingStart, 
            ////                    _animation_targets[sbName], _animation_arguments[sbName], _animation_trigger_source[sbName]));

            ////            if (Dialog != null && Dialog.DialogContent != null)
            ////                Dialog.DialogContent.OnOverlayAnimationFinished(this, new OverlayAnimationCompletedArgs(OverlayAnimationType.LoadingStart,
            ////                    _animation_targets[sbName], _animation_arguments[sbName], _animation_trigger_source[sbName]));

            ////            _animation_arguments.Remove(sbName);
            ////            _animation_trigger_source.Remove(sbName);
            ////            _animation_targets.Remove(sbName);

        }

        void LoadingEndStoryboardCompleted(object sender, EventArgs e)
        {
            LoadingEndAnimationFinished = true;
            StoryboardCompletedActions(sender, OverlayAnimationType.LoadingEnd);
            //            if (sender is Storyboard)
            //                ((Storyboard)sender).Completed -= new EventHandler(LoadingEndStoryboardCompleted);
            //            else
            //                ((ClockGroup)sender).Completed -= new EventHandler(LoadingEndStoryboardCompleted);
            //#if DEBUG
            //            Debug.WriteLine("** Animation of storyboard: " + ((ClockGroup)sender).GetHashCode().ToString() + " ('" + ((ClockGroup)sender).Timeline.Name + "') completed");
            //            System.Diagnostics.Debug.WriteLine("*** LOADING END Storyboard Finished");
            //            Debug.WriteLine("***** Dialog: " + GetHashCode().ToString());
            //#endif
            //            string sbName = "";
            //            if (sender is Storyboard)
            //                sbName = ((Storyboard)sender).Name;
            //            else
            //                sbName = ((ClockGroup)sender).Timeline.Name;
            //            RemoveAnimationRunning(sbName);

            //            LoadingEndAnimationFinished = true;
            //            if (OverlayAnimationFinished != null)
            //                OverlayAnimationFinished(this, new OverlayAnimationCompletedArgs(OverlayAnimationType.LoadingEnd,
            //                    _animation_targets[sbName], _animation_arguments[sbName], _animation_trigger_source[sbName]));

            //            if (Dialog != null && Dialog.DialogContent != null)
            //                Dialog.DialogContent.OnOverlayAnimationFinished(this, new OverlayAnimationCompletedArgs(OverlayAnimationType.LoadingEnd,
            //                    _animation_targets[sbName], _animation_arguments[sbName], _animation_trigger_source[sbName]));

            //            _animation_arguments.Remove(sbName);
            //            _animation_trigger_source.Remove(sbName);
            //            _animation_targets.Remove(sbName);

        }

        void MinimizedToModalStoryboardCompleted(object sender, EventArgs e)
        {
            StoryboardCompletedActions(sender, OverlayAnimationType.MinimizedToModal);
            //            if (sender is Storyboard)
            //                ((Storyboard)sender).Completed -= new EventHandler(MinimizedToModalStoryboardCompleted);
            //            else
            //                ((ClockGroup)sender).Completed -= new EventHandler(MinimizedToModalStoryboardCompleted);

            //#if DEBUG
            //            Debug.WriteLine("** Animation of storyboard: " + ((ClockGroup)sender).GetHashCode().ToString() + " ('" + ((ClockGroup)sender).Timeline.Name + "') completed");
            //            System.Diagnostics.Debug.WriteLine("*** MinimizedToModal Storyboard Finished");
            //            Debug.WriteLine("***** Dialog: " + GetHashCode().ToString());
            //#endif
            //            string sbName = "";
            //            if (sender is Storyboard)
            //                sbName = ((Storyboard)sender).Name;
            //            else
            //                sbName = ((ClockGroup)sender).Timeline.Name;
            //            RemoveAnimationRunning(sbName);

            //            if (OverlayAnimationFinished != null)
            //                OverlayAnimationFinished(this, new OverlayAnimationCompletedArgs(OverlayAnimationType.MinimizedToModal,
            //                    _animation_targets[sbName], _animation_arguments[sbName], _animation_trigger_source[sbName]));

            //            if (Dialog != null && Dialog.DialogContent != null)
            //                Dialog.DialogContent.OnOverlayAnimationFinished(this, new OverlayAnimationCompletedArgs(OverlayAnimationType.MinimizedToModal,
            //                    _animation_targets[sbName], _animation_arguments[sbName], _animation_trigger_source[sbName]));

            //            _animation_arguments.Remove(sbName);
            //            _animation_trigger_source.Remove(sbName);
            //            _animation_targets.Remove(sbName);
        }

        void MaximizedToModalStoryboardCompleted(object sender, EventArgs e)
        {
            StoryboardCompletedActions(sender, OverlayAnimationType.MaximizedToModal);
            //            if (sender is Storyboard)
            //                ((Storyboard)sender).Completed -= new EventHandler(MaximizedToModalStoryboardCompleted);
            //            else
            //                ((ClockGroup)sender).Completed -= new EventHandler(MaximizedToModalStoryboardCompleted);

            //#if DEBUG
            //            Debug.WriteLine("** Animation of storyboard: " + ((ClockGroup)sender).GetHashCode().ToString() + " ('" + ((ClockGroup)sender).Timeline.Name + "') completed");
            //            System.Diagnostics.Debug.WriteLine("*** MaximizedToModal Storyboard Finished");
            //            Debug.WriteLine("***** Dialog: " + GetHashCode().ToString());
            //#endif
            //            string sbName = "";
            //            if (sender is Storyboard)
            //                sbName = ((Storyboard)sender).Name;
            //            else
            //                sbName = ((ClockGroup)sender).Timeline.Name;
            //            RemoveAnimationRunning(sbName);

            //            if (OverlayAnimationFinished != null)
            //                OverlayAnimationFinished(this, new OverlayAnimationCompletedArgs(OverlayAnimationType.MaximizedToModal,
            //                    _animation_targets[sbName], _animation_arguments[sbName], _animation_trigger_source[sbName]));

            //            if (Dialog != null && Dialog.DialogContent != null)
            //                Dialog.DialogContent.OnOverlayAnimationFinished(this, new OverlayAnimationCompletedArgs(OverlayAnimationType.MaximizedToModal,
            //                    _animation_targets[sbName], _animation_arguments[sbName], _animation_trigger_source[sbName]));

            //            _animation_arguments.Remove(sbName);
            //            _animation_trigger_source.Remove(sbName);
            //            _animation_targets.Remove(sbName);
        }

        void ModalToMaximizeStoryboardCompleted(object sender, EventArgs e)
        {
            StoryboardCompletedActions(sender, OverlayAnimationType.ModalToMaximize);
            //            if (sender is Storyboard)
            //                ((Storyboard)sender).Completed -= new EventHandler(ModalToMaximizeStoryboardCompleted);
            //            else
            //                ((ClockGroup)sender).Completed -= new EventHandler(ModalToMaximizeStoryboardCompleted);

            //#if DEBUG
            //            Debug.WriteLine("** Animation of storyboard: " + ((ClockGroup)sender).GetHashCode().ToString() + " ('" + ((ClockGroup)sender).Timeline.Name + "') completed");
            //            System.Diagnostics.Debug.WriteLine("*** ModalToMaximize Storyboard Finished");
            //            Debug.WriteLine("***** Dialog: " + GetHashCode().ToString());
            //#endif
            //            string sbName = "";
            //            if (sender is Storyboard)
            //                sbName = ((Storyboard)sender).Name;
            //            else
            //                sbName = ((ClockGroup)sender).Timeline.Name;
            //            RemoveAnimationRunning(sbName);

            //            if (OverlayAnimationFinished != null)
            //                OverlayAnimationFinished(this, new OverlayAnimationCompletedArgs(OverlayAnimationType.ModalToMaximize,
            //                    _animation_targets[sbName], _animation_arguments[sbName], _animation_trigger_source[sbName]));

            //            if (Dialog != null && Dialog.DialogContent != null)
            //                Dialog.DialogContent.OnOverlayAnimationFinished(this, new OverlayAnimationCompletedArgs(OverlayAnimationType.ModalToMaximize,
            //                    _animation_targets[sbName], _animation_arguments[sbName], _animation_trigger_source[sbName]));

            //            _animation_arguments.Remove(sbName);
            //            _animation_trigger_source.Remove(sbName);
            //            _animation_targets.Remove(sbName);
        }

        void ModalToMinimizeStoryboardCompleted(object sender, EventArgs e)
        {
            StoryboardCompletedActions(sender, OverlayAnimationType.ModalToMinimize);
            //            if (sender is Storyboard)
            //                ((Storyboard)sender).Completed -= new EventHandler(ModalToMinimizeStoryboardCompleted);
            //            else
            //                ((ClockGroup)sender).Completed -= new EventHandler(ModalToMinimizeStoryboardCompleted);

            //#if DEBUG
            //            Debug.WriteLine("** Animation of storyboard: " + ((ClockGroup)sender).GetHashCode().ToString() + " ('" + ((ClockGroup)sender).Timeline.Name + "') completed");
            //            System.Diagnostics.Debug.WriteLine("*** ModalToMinimize Storyboard Finished");
            //            Debug.WriteLine("***** Dialog: " + GetHashCode().ToString());
            //#endif
            //            string sbName = "";
            //            if (sender is Storyboard)
            //                sbName = ((Storyboard)sender).Name;
            //            else
            //                sbName = ((ClockGroup)sender).Timeline.Name;
            //            RemoveAnimationRunning(sbName);

            //            if (OverlayAnimationFinished != null)
            //                OverlayAnimationFinished(this, new OverlayAnimationCompletedArgs(OverlayAnimationType.ModalToMinimize,
            //                    _animation_targets[sbName], _animation_arguments[sbName], _animation_trigger_source[sbName]));

            //            if (Dialog != null && Dialog.DialogContent != null)
            //                Dialog.DialogContent.OnOverlayAnimationFinished(this, new OverlayAnimationCompletedArgs(OverlayAnimationType.ModalToMinimize,
            //                    _animation_targets[sbName], _animation_arguments[sbName], _animation_trigger_source[sbName]));

            //            _animation_arguments.Remove(sbName);
            //            _animation_trigger_source.Remove(sbName);
            //            _animation_targets.Remove(sbName);
        }

        void NormalToMinimizeStoryboardCompleted(object sender, EventArgs e)
        {
            StoryboardCompletedActions(sender, OverlayAnimationType.NormalToMinimize);
            //            if (sender is Storyboard)
            //                ((Storyboard)sender).Completed -= new EventHandler(NormalToMinimizeStoryboardCompleted);
            //            else
            //                ((ClockGroup)sender).Completed -= new EventHandler(NormalToMinimizeStoryboardCompleted);

            //#if DEBUG
            //            Debug.WriteLine("** Animation of storyboard: " + ((ClockGroup)sender).GetHashCode().ToString() + " ('" + ((ClockGroup)sender).Timeline.Name + "') completed");
            //            System.Diagnostics.Debug.WriteLine("*** NormalToMinimize Storyboard Finished");
            //            Debug.WriteLine("***** Dialog: " + GetHashCode().ToString());
            //#endif
            //            string sbName = "";
            //            if (sender is Storyboard)
            //                sbName = ((Storyboard)sender).Name;
            //            else
            //                sbName = ((ClockGroup)sender).Timeline.Name;
            //            RemoveAnimationRunning(sbName);

            //            if (OverlayAnimationFinished != null)
            //                OverlayAnimationFinished(this, new OverlayAnimationCompletedArgs(OverlayAnimationType.NormalToMinimize,
            //                    _animation_targets[sbName], _animation_arguments[sbName], _animation_trigger_source[sbName]));

            //            if (Dialog != null && Dialog.DialogContent != null)
            //                Dialog.DialogContent.OnOverlayAnimationFinished(this, new OverlayAnimationCompletedArgs(OverlayAnimationType.NormalToMinimize,
            //                    _animation_targets[sbName], _animation_arguments[sbName], _animation_trigger_source[sbName]));

            //            _animation_arguments.Remove(sbName);
            //            _animation_trigger_source.Remove(sbName);
            //            _animation_targets.Remove(sbName);
        }

        void NormalToMaximizeStoryboardCompleted(object sender, EventArgs e)
        {
            StoryboardCompletedActions(sender, OverlayAnimationType.NormalToMaximize);
            //            if (sender is Storyboard)
            //                ((Storyboard)sender).Completed -= new EventHandler(NormalToMaximizeStoryboardCompleted);
            //            else
            //                ((ClockGroup)sender).Completed -= new EventHandler(NormalToMaximizeStoryboardCompleted);

            //#if DEBUG
            //            Debug.WriteLine("** Animation of storyboard: " + ((ClockGroup)sender).GetHashCode().ToString() + " ('" + ((ClockGroup)sender).Timeline.Name + "') completed"); Debug.WriteLine("** Animation of storyboard: " + ((ClockGroup)sender).GetHashCode().ToString() + " completed");
            //            System.Diagnostics.Debug.WriteLine("*** NormalToMaximize Storyboard Finished");
            //            Debug.WriteLine("***** Dialog: " + GetHashCode().ToString());
            //#endif

            //            string sbName = "";
            //            if (sender is Storyboard)
            //                sbName = ((Storyboard)sender).Name;
            //            else
            //                sbName = ((ClockGroup)sender).Timeline.Name;
            //            RemoveAnimationRunning(sbName);

            //            if (OverlayAnimationFinished != null)
            //                OverlayAnimationFinished(this, new OverlayAnimationCompletedArgs(OverlayAnimationType.NormalToMaximize,
            //                    _animation_targets[sbName], _animation_arguments[sbName], _animation_trigger_source[sbName]));

            //            if (Dialog != null && Dialog.DialogContent != null)
            //                Dialog.DialogContent.OnOverlayAnimationFinished(this, new OverlayAnimationCompletedArgs(OverlayAnimationType.NormalToMaximize,
            //                    _animation_targets[sbName], _animation_arguments[sbName], _animation_trigger_source[sbName]));

            //            _animation_arguments.Remove(sbName);
            //            _animation_trigger_source.Remove(sbName);
            //            _animation_targets.Remove(sbName);
        }

        void MinimizedToNormalStoryboardCompleted(object sender, EventArgs e)
        {
            StoryboardCompletedActions(sender, OverlayAnimationType.MinimizedToNormal);
            //            if (sender is Storyboard)
            //                ((Storyboard)sender).Completed -= new EventHandler(MinimizedToNormalStoryboardCompleted);
            //            else
            //                ((ClockGroup)sender).Completed -= new EventHandler(MinimizedToNormalStoryboardCompleted);

            //#if DEBUG
            //            Debug.WriteLine("** Animation of storyboard: " + ((ClockGroup)sender).GetHashCode().ToString() + " ('" + ((ClockGroup)sender).Timeline.Name + "') completed");
            //            System.Diagnostics.Debug.WriteLine("*** MinimizedToNormal Storyboard Finished");
            //            Debug.WriteLine("***** Dialog: " + GetHashCode().ToString());
            //#endif

            //            string sbName = "";
            //            if (sender is Storyboard)
            //                sbName = ((Storyboard)sender).Name;
            //            else
            //                sbName = ((ClockGroup)sender).Timeline.Name;
            //            RemoveAnimationRunning(sbName);

            //            if (OverlayAnimationFinished != null)
            //                OverlayAnimationFinished(this, new OverlayAnimationCompletedArgs(OverlayAnimationType.MinimizedToNormal,
            //                    _animation_targets[sbName], _animation_arguments[sbName], _animation_trigger_source[sbName]));

            //            if (Dialog != null && Dialog.DialogContent != null)
            //                Dialog.DialogContent.OnOverlayAnimationFinished(this, new OverlayAnimationCompletedArgs(OverlayAnimationType.MinimizedToNormal,
            //                    _animation_targets[sbName], _animation_arguments[sbName], _animation_trigger_source[sbName]));

            //            _animation_arguments.Remove(sbName);
            //            _animation_trigger_source.Remove(sbName);
            //            _animation_targets.Remove(sbName);
        }

        void MinimizeToMaximizeStoryboardCompleted(object sender, EventArgs e)
        {
            StoryboardCompletedActions(sender, OverlayAnimationType.MinimizeToMaximize);
            //            if (sender is Storyboard)
            //                ((Storyboard)sender).Completed -= new EventHandler(MinimizeToMaximizeStoryboardCompleted);
            //            else
            //                ((ClockGroup)sender).Completed -= new EventHandler(MinimizeToMaximizeStoryboardCompleted);

            //#if DEBUG
            //            Debug.WriteLine("** Animation of storyboard: " + ((ClockGroup)sender).GetHashCode().ToString() + " ('" + ((ClockGroup)sender).Timeline.Name + "') completed");
            //            System.Diagnostics.Debug.WriteLine("*** MinimizeToMaximize Storyboard Finished");
            //            Debug.WriteLine("***** Dialog: " + GetHashCode().ToString());
            //#endif
            //            string sbName = "";
            //            if (sender is Storyboard)
            //                sbName = ((Storyboard)sender).Name;
            //            else
            //                sbName = ((ClockGroup)sender).Timeline.Name;
            //            RemoveAnimationRunning(sbName);

            //            if (OverlayAnimationFinished != null)
            //                OverlayAnimationFinished(this, new OverlayAnimationCompletedArgs(OverlayAnimationType.MinimizeToMaximize,
            //                    _animation_targets[sbName], _animation_arguments[sbName], _animation_trigger_source[sbName]));

            //            if (Dialog != null && Dialog.DialogContent != null)
            //                Dialog.DialogContent.OnOverlayAnimationFinished(this, new OverlayAnimationCompletedArgs(OverlayAnimationType.MinimizeToMaximize,
            //                    _animation_targets[sbName], _animation_arguments[sbName], _animation_trigger_source[sbName]));

            //            _animation_arguments.Remove(sbName);
            //            _animation_trigger_source.Remove(sbName);
            //            _animation_targets.Remove(sbName);
        }

        void MaximizeToNormalStoryboardCompleted(object sender, EventArgs e)
        {
            StoryboardCompletedActions(sender, OverlayAnimationType.MaximizeToNormal);
            //            if (sender is Storyboard)
            //                ((Storyboard)sender).Completed -= new EventHandler(MaximizeToNormalStoryboardCompleted);
            //            else
            //                ((ClockGroup)sender).Completed -= new EventHandler(MaximizeToNormalStoryboardCompleted);

            //#if DEBUG
            //            if(sender is Storyboard)
            //                Debug.WriteLine("** Animation of storyboard: " + ((Storyboard)sender).GetHashCode().ToString() + " ('" + ((Storyboard)sender).Name + "') completed");
            //            else
            //                Debug.WriteLine("** Animation of storyboard: " + ((ClockGroup)sender).GetHashCode().ToString() + " ('" + ((ClockGroup)sender).Timeline.Name + "') completed");
            //            System.Diagnostics.Debug.WriteLine("*** MaximizeToNormal Storyboard Finished");
            //            Debug.WriteLine("***** Dialog: " + GetHashCode().ToString());
            //#endif
            //            string sbName = "";
            //            if (sender is Storyboard)
            //                sbName = ((Storyboard)sender).Name;
            //            else
            //                sbName = ((ClockGroup)sender).Timeline.Name;
            //            RemoveAnimationRunning(sbName);

            //            if (OverlayAnimationFinished != null)
            //                OverlayAnimationFinished(this, new OverlayAnimationCompletedArgs(OverlayAnimationType.MaximizeToNormal,
            //                    _animation_targets[sbName], _animation_arguments[sbName], _animation_trigger_source[sbName]));

            //            if (Dialog != null && Dialog.DialogContent != null)
            //                Dialog.DialogContent.OnOverlayAnimationFinished(this, new OverlayAnimationCompletedArgs(OverlayAnimationType.MaximizeToNormal,
            //                    _animation_targets[sbName], _animation_arguments[sbName], _animation_trigger_source[sbName]));

            //            _animation_arguments.Remove(sbName);
            //            _animation_trigger_source.Remove(sbName);
            //            _animation_targets.Remove(sbName);
        }

        void MaximizeToMinimizeStoryboardCompleted(object sender, EventArgs e)
        {
            StoryboardCompletedActions(sender, OverlayAnimationType.MaximizeToMinimize);
            //            if (sender is Storyboard)
            //                ((Storyboard)sender).Completed -= new EventHandler(MaximizeToMinimizeStoryboardCompleted);
            //            else
            //                ((ClockGroup)sender).Completed -= new EventHandler(MaximizeToMinimizeStoryboardCompleted);

            //#if DEBUG
            //            if(sender is Storyboard)
            //                Debug.WriteLine("** Animation of storyboard: " + ((Storyboard)sender).GetHashCode().ToString() + " ('" + ((Storyboard)sender).Name + "') completed");
            //            else
            //                Debug.WriteLine("** Animation of storyboard: " + ((ClockGroup)sender).GetHashCode().ToString() + " ('" + ((ClockGroup)sender).Timeline.Name + "') completed");
            //            System.Diagnostics.Debug.WriteLine("*** MaximizeToMinimize Storyboard Finished");
            //            Debug.WriteLine("***** Dialog: " + GetHashCode().ToString());
            //#endif
            //            string sbName = "";
            //            if (sender is Storyboard)
            //                sbName = ((Storyboard)sender).Name;
            //            else
            //                sbName = ((ClockGroup)sender).Timeline.Name;
            //            RemoveAnimationRunning(sbName);

            //            if (OverlayAnimationFinished != null)
            //                OverlayAnimationFinished(this, new OverlayAnimationCompletedArgs(OverlayAnimationType.MaximizeToMinimize,
            //                    _animation_targets[sbName], _animation_arguments[sbName], _animation_trigger_source[sbName]));

            //            if (Dialog != null && Dialog.DialogContent != null)
            //                Dialog.DialogContent.OnOverlayAnimationFinished(this, new OverlayAnimationCompletedArgs(OverlayAnimationType.MaximizeToMinimize,
            //                    _animation_targets[sbName], _animation_arguments[sbName], _animation_trigger_source[sbName]));

            //            _animation_arguments.Remove(sbName);
            //            _animation_trigger_source.Remove(sbName);
        }

        void FadeInOpacityStoryboardCompleted(object sender, EventArgs e)
        {
            _active_storyboard = null;
            StoryboardCompletedActions(sender, OverlayAnimationType.FadeInOpacity);

            //            if (sender is Storyboard)
            //                ((Storyboard) sender).Completed -= new EventHandler(FadeInOpacityStoryboardCompleted);
            //            else
            //                ((ClockGroup) sender).Completed -= new EventHandler(FadeInOpacityStoryboardCompleted);

            //#if DEBUG
            //            if (sender is Storyboard)
            //                Debug.WriteLine("** Animation of storyboard: " + ((Storyboard) sender).GetHashCode().ToString() + " ('" + ((Storyboard) sender).Name + "') completed");
            //            else if (sender is Storyboard)
            //                Debug.WriteLine("** Animation of storyboard: " + ((ClockGroup) sender).GetHashCode().ToString() + " ('" + ((ClockGroup) sender).Timeline.Name + "') completed");
            //            System.Diagnostics.Debug.WriteLine("*** FadeInOpacity Storyboard Finished");
            //            Debug.WriteLine("***** Dialog: " + GetHashCode().ToString());
            //#endif
            //            string sbName = "";
            //            if (sender is Storyboard)
            //                sbName = ((Storyboard) sender).Name;
            //            else
            //                sbName = ((ClockGroup) sender).Timeline.Name;
            //            RemoveAnimationRunning(sbName);

            //            if (_animation_arguments.ContainsKey(sbName) && _animation_trigger_source.ContainsKey(sbName) && _animation_targets.ContainsKey(sbName))
            //            {
            //                if (OverlayAnimationFinished != null)
            //                    OverlayAnimationFinished(this, new OverlayAnimationCompletedArgs(OverlayAnimationType.FadeInOpacity, _animation_targets[sbName], _animation_arguments[sbName], _animation_trigger_source[sbName]));

            //                if (Dialog != null && Dialog.DialogContent != null)
            //                    Dialog.DialogContent.OnOverlayAnimationFinished(this, new OverlayAnimationCompletedArgs(OverlayAnimationType.FadeInOpacity,_animation_targets[sbName],_animation_arguments[sbName],_animation_trigger_source[sbName]));
            //            }

            //            if(_animation_arguments.ContainsKey(sbName))
            //                _animation_arguments.Remove(sbName);

            //            if(_animation_trigger_source.ContainsKey(sbName))
            //                _animation_trigger_source.Remove(sbName);

            //            if(_animation_targets.ContainsKey(sbName))
            //                _animation_targets.Remove(sbName);

        }

        void FadOutOpacityStoryboardCompleted(object sender, EventArgs e)
        {
            _active_storyboard = null;
            StoryboardCompletedActions(sender, OverlayAnimationType.FadOutOpacity);
            //            if (sender is Storyboard)
            //                ((Storyboard)sender).Completed -= new EventHandler(FadOutOpacityStoryboardCompleted);
            //            else
            //                ((ClockGroup)sender).Completed -= new EventHandler(FadOutOpacityStoryboardCompleted);

            //#if DEBUG
            //            if(sender is Storyboard)
            //                Debug.WriteLine("** Animation of storyboard: " + ((Storyboard)sender).GetHashCode().ToString() + " ('" + ((Storyboard)sender).Name + "') completed");
            //            else
            //                Debug.WriteLine("** Animation of storyboard: " + ((ClockGroup)sender).GetHashCode().ToString() + " ('" + ((ClockGroup)sender).Timeline.Name + "') completed");
            //            System.Diagnostics.Debug.WriteLine("*** FadOutOpacity Storyboard Finished");
            //            Debug.WriteLine("***** Dialog: " + GetHashCode().ToString());
            //#endif
            //            string sbName = "";
            //            if (sender is Storyboard)
            //                sbName = ((Storyboard)sender).Name;
            //            else
            //                sbName = ((ClockGroup)sender).Timeline.Name;
            //            RemoveAnimationRunning(sbName);

            //            if (_animation_arguments.ContainsKey(sbName) && _animation_trigger_source.ContainsKey(sbName) && _animation_targets.ContainsKey(sbName))
            //            {
            //                if (OverlayAnimationFinished != null)
            //                    OverlayAnimationFinished(this, new OverlayAnimationCompletedArgs(OverlayAnimationType.FadOutOpacity, _animation_targets[sbName], _animation_arguments[sbName], _animation_trigger_source[sbName]));

            //                if (Dialog != null && Dialog.DialogContent != null)
            //                    Dialog.DialogContent.OnOverlayAnimationFinished(this, new OverlayAnimationCompletedArgs(OverlayAnimationType.FadOutOpacity,_animation_targets[sbName],_animation_arguments[sbName],_animation_trigger_source[sbName]));
            //            }

            //            if (_animation_arguments.ContainsKey(sbName))
            //                _animation_arguments.Remove(sbName);

            //            if (_animation_trigger_source.ContainsKey(sbName))
            //                _animation_trigger_source.Remove(sbName);

            //            if (_animation_targets.ContainsKey(sbName))
            //                _animation_targets.Remove(sbName);

        }

        void CustomActivateStoryboardCompleted(object sender, EventArgs e)
        {
            _active_storyboard = null;
            StoryboardCompletedActions(sender, OverlayAnimationType.CustomActivate);
            //            if (sender is Storyboard)
            //                ((Storyboard)sender).Completed -= new EventHandler(CustomActivateStoryboardCompleted);
            //            else
            //                ((ClockGroup)sender).Completed -= new EventHandler(CustomActivateStoryboardCompleted);

            //#if DEBUG
            //            if(sender is Storyboard)
            //                Debug.WriteLine("** Animation of storyboard: " + ((Storyboard)sender).GetHashCode().ToString() + " ('" + ((Storyboard)sender).Name + "') completed");
            //            else
            //                Debug.WriteLine("** Animation of storyboard: " + ((ClockGroup)sender).GetHashCode().ToString() + " ('" + ((ClockGroup)sender).Timeline.Name + "') completed");
            //            System.Diagnostics.Debug.WriteLine("*** CustomActivate Storyboard Finished");
            //#endif
            //            string sbName = "";
            //            if (sender is Storyboard)
            //                sbName = ((Storyboard)sender).Name;
            //            else
            //                sbName = ((ClockGroup)sender).Timeline.Name;
            //            RemoveAnimationRunning(sbName);

            //            if (_animation_arguments.ContainsKey(sbName) && _animation_trigger_source.ContainsKey(sbName) && _animation_targets.ContainsKey(sbName))
            //            {
            //                if (OverlayAnimationFinished != null)
            //                    OverlayAnimationFinished(this, new OverlayAnimationCompletedArgs(OverlayAnimationType.CustomActivate, _animation_targets[sbName], _animation_arguments[sbName], _animation_trigger_source[sbName]));

            //                if (Dialog != null && Dialog.DialogContent != null)
            //                    Dialog.DialogContent.OnOverlayAnimationFinished(this, new OverlayAnimationCompletedArgs(OverlayAnimationType.CustomActivate, _animation_targets[sbName], _animation_arguments[sbName], _animation_trigger_source[sbName]));
            //            }

            //            if (_animation_arguments.ContainsKey(sbName))
            //                _animation_arguments.Remove(sbName);

            //            if (_animation_trigger_source.ContainsKey(sbName))
            //                _animation_trigger_source.Remove(sbName);

            //            if (_animation_targets.ContainsKey(sbName))
            //                _animation_targets.Remove(sbName);

        }

        void CustomDeactivateStoryboardCompleted(object sender, EventArgs e)
        {
            _active_storyboard = null;
            StoryboardCompletedActions(sender, OverlayAnimationType.CustomDeactivate);
            //            if (sender is Storyboard)
            //                ((Storyboard)sender).Completed -= new EventHandler(CustomDeactivateStoryboardCompleted);
            //            else
            //                ((ClockGroup)sender).Completed -= new EventHandler(CustomDeactivateStoryboardCompleted);

            //#if DEBUG
            //            if(sender is Storyboard)
            //                Debug.WriteLine("** Animation of storyboard: " + ((Storyboard)sender).GetHashCode().ToString() + " ('" + ((Storyboard)sender).Name + "') completed");
            //            else
            //                Debug.WriteLine("** Animation of storyboard: " + ((ClockGroup)sender).GetHashCode().ToString() + " ('" + ((ClockGroup)sender).Timeline.Name + "') completed");
            //            System.Diagnostics.Debug.WriteLine("*** CustomDeactivate Storyboard Finished");
            //            Debug.WriteLine("***** Dialog: " + GetHashCode().ToString());
            //#endif
            //            string sbName = "";
            //            if (sender is Storyboard)
            //                sbName = ((Storyboard)sender).Name;
            //            else
            //                sbName = ((ClockGroup)sender).Timeline.Name;
            //            RemoveAnimationRunning(sbName);

            //            if (_animation_arguments.ContainsKey(sbName) && _animation_trigger_source.ContainsKey(sbName) && _animation_targets.ContainsKey(sbName))
            //            {
            //                if (OverlayAnimationFinished != null)
            //                    OverlayAnimationFinished(this, new OverlayAnimationCompletedArgs(OverlayAnimationType.CustomDeactivate, _animation_targets[sbName], _animation_arguments[sbName], _animation_trigger_source[sbName]));

            //                if (Dialog != null && Dialog.DialogContent != null)
            //                    Dialog.DialogContent.OnOverlayAnimationFinished(this, new OverlayAnimationCompletedArgs(OverlayAnimationType.CustomDeactivate, _animation_targets[sbName], _animation_arguments[sbName], _animation_trigger_source[sbName]));
            //            }


            //            if (_animation_arguments.ContainsKey(sbName))
            //                _animation_arguments.Remove(sbName);

            //            if (_animation_trigger_source.ContainsKey(sbName))
            //                _animation_trigger_source.Remove(sbName);

            //            if (_animation_targets.ContainsKey(sbName))
            //                _animation_targets.Remove(sbName);

        }

        void CustomStoryboardCompleted(object sender, EventArgs e)
        {
            StoryboardCompletedActions(sender, OverlayAnimationType.Custom);
            //            if (sender is Storyboard)
            //                ((Storyboard)sender).Completed -= new EventHandler(CustomStoryboardCompleted);
            //            else
            //                ((ClockGroup)sender).Completed -= new EventHandler(CustomStoryboardCompleted);

            //#if DEBUG
            //            if(sender is Storyboard)
            //                Debug.WriteLine("** Animation of storyboard: " + ((Storyboard)sender).GetHashCode().ToString() + " ('" + ((Storyboard)sender).Name + "') completed");
            //            else
            //                Debug.WriteLine("** Animation of storyboard: " + ((ClockGroup)sender).GetHashCode().ToString() + " ('" + ((ClockGroup)sender).Timeline.Name + "') completed");
            //            System.Diagnostics.Debug.WriteLine("*** Custom Storyboard Finished");
            //            Debug.WriteLine("***** Dialog: " + GetHashCode().ToString());
            //#endif
            //            string sbName = "";
            //            if (sender is Storyboard)
            //                sbName = ((Storyboard)sender).Name;
            //            else
            //                sbName = ((ClockGroup)sender).Timeline.Name;

            //            RemoveAnimationRunning(sbName);

            //            if (_animation_arguments.ContainsKey(sbName) && _animation_trigger_source.ContainsKey(sbName) && _animation_targets.ContainsKey(sbName))
            //            {
            //                if (OverlayAnimationFinished != null)
            //                    OverlayAnimationFinished(this, new OverlayAnimationCompletedArgs(OverlayAnimationType.Custom, _animation_targets[sbName], _animation_arguments[sbName], _animation_trigger_source[sbName]));

            //                if (Dialog != null && Dialog.DialogContent != null)
            //                    Dialog.DialogContent.OnOverlayAnimationFinished(this, new OverlayAnimationCompletedArgs(OverlayAnimationType.Custom, _animation_targets[sbName], _animation_arguments[sbName], _animation_trigger_source[sbName]));
            //            }

            //            if (_animation_arguments.ContainsKey(sbName))
            //                _animation_arguments.Remove(sbName);

            //            if (_animation_trigger_source.ContainsKey(sbName))
            //                _animation_trigger_source.Remove(sbName);

            //            if (_animation_targets.ContainsKey(sbName))
            //                _animation_targets.Remove(sbName);
        }

        private void StoryboardCompletedActions(object sender, OverlayAnimationType type)
        {

            if (sender is Storyboard)
                ((Storyboard)sender).Completed -= CustomStoryboardCompleted;
            else
                ((ClockGroup)sender).Completed -= CustomStoryboardCompleted;

#if DEBUG
            if (sender is Storyboard)
                Debug.WriteLine("** Animation of storyboard: " + ((Storyboard)sender).GetHashCode().ToString() + " ('" + ((Storyboard)sender).Name + "') completed");
            else
                Debug.WriteLine("** Animation of storyboard: " + ((ClockGroup)sender).GetHashCode().ToString() + " ('" + ((ClockGroup)sender).Timeline.Name + "') completed");
            System.Diagnostics.Debug.WriteLine("*** " + type.ToString() + " Finished");
            Debug.WriteLine("***** Dialog: " + GetHashCode().ToString());
#endif
            string sbName = "";
            if (sender is Storyboard)
                sbName = ((Storyboard)sender).Name;
            else
                sbName = ((ClockGroup)sender).Timeline.Name;

            RemoveAnimationRunning(sbName);
            SetFinalPositionAfterAnimation(sbName);

            if (_animation_arguments.ContainsKey(sbName) && _animation_trigger_source.ContainsKey(sbName) && _animation_targets.ContainsKey(sbName))
            {
                if (OverlayAnimationFinished != null)
                    OverlayAnimationFinished(this, new OverlayAnimationCompletedArgs(type,
                        _animation_targets[sbName],
                        _animation_arguments[sbName],
                        _animation_trigger_source[sbName]));

                if (Dialog != null && Dialog.DialogContent != null)
                    Dialog.DialogContent.OnOverlayAnimationFinished(this,
                        new OverlayAnimationCompletedArgs(type, _animation_targets[sbName], _animation_arguments[sbName], _animation_trigger_source[sbName]));
            }

            if (_animation_arguments.ContainsKey(sbName))
                _animation_arguments.Remove(sbName);

            if (_animation_trigger_source.ContainsKey(sbName))
                _animation_trigger_source.Remove(sbName);

            if (_animation_targets.ContainsKey(sbName))
                _animation_targets.Remove(sbName);
        }
        #endregion
        #endregion

        #region Ignore Mouse Inputs Event Handler
        void DialogWindowScreenshot_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (_ignore_mouse_inputs)
                e.Handled = true;
        }

        void DialogWindowScreenshot_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (_ignore_mouse_inputs)
                e.Handled = true;
        }

        void Dialog_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (_ignore_mouse_inputs)
                e.Handled = true;
        }

        void Dialog_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (_ignore_mouse_inputs)
                e.Handled = true;
        }
        #endregion

        #region IDisposable Implementation
        public void Dispose()
        {
            DependencyPropertyDescriptor descrLeft = DependencyPropertyDescriptor.FromProperty(Canvas.LeftProperty, typeof(OverlayDialog));
            DependencyPropertyDescriptor descrTop = DependencyPropertyDescriptor.FromProperty(Canvas.TopProperty, typeof(OverlayDialog));
            if (Dialog != null)
            {
                if (descrLeft != null)
                    descrLeft.RemoveValueChanged(Dialog, CanvasLeftTopChanged);
                if (descrTop != null)
                    descrTop.RemoveValueChanged(Dialog, CanvasLeftTopChanged);
            }

            IgnoreMouseInputs = false;
            Dialog.Dispose();
            Dialog = null;
        }
        #endregion

        #region Events

        //public event EventHandler LoadingAnimationCompleted;
        public event EventHandler<OverlayAnimationCompletedArgs> OverlayAnimationFinished;
        #endregion

        #region Attribs

        private bool _ignore_mouse_inputs;

        private double _width_before_anim = 0;
        private double _height_before_anim = 0;
        private OverlayMetaDialogComparer _dialog_comparer = null;
        private OverlayPictureHost _normal_screenshot = new OverlayPictureHost { Visibility = Visibility.Hidden };
        private OverlayPictureHost _thumb_screenshot = new OverlayPictureHost { Visibility = Visibility.Hidden, Width = 100, Height = 100 };
        private Point _normal_canvas_position = new Point(0, 0);
        private OverlayDialog _dialog = null;
        private bool _is_active = false;

        private Dictionary<string, OverlayAnimationTriggerSource> _animation_trigger_source = new Dictionary<string, OverlayAnimationTriggerSource>();
        private Dictionary<string, object> _animation_arguments = new Dictionary<string, object>();
        private Dictionary<string, FrameworkElement> _animation_targets = new Dictionary<string, FrameworkElement>();

        private Dictionary<string, Storyboard> _running_storyboards = new Dictionary<string, Storyboard>();
        private Dictionary<string, OverlayAnimationProperties> _final_animation_properties = new Dictionary<string, OverlayAnimationProperties>();
        private bool _reset_animations_requested = false;

        private Storyboard _active_storyboard = null;
        #endregion
    }
}
