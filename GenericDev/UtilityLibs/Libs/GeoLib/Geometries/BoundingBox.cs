﻿using System;
using MathLib;

namespace Logicx.Geo.Geometries
{
    /// <summary>
    /// Eine 2D Bounding-Box (für 2D picking/collision checking), Verwendung z.B. im QuadTree
    /// </summary>
    [Serializable]
    public struct BoundingBox
    {
        public BoundingBox(Vector2d topleft, Vector2d bottomright)
        {
            TopLeft = topleft;
            BottomRight = bottomright;
        }
        public static BoundingBox FromVertices(Vector2f[] ordinates)
        {
            float minx = float.MaxValue, maxx = float.MinValue, miny = float.MaxValue, maxy = float.MinValue;
          
            for (int i = 0; i < ordinates.Length; i++)
            {
                Vector2f v = ordinates[i];
                if (v.X < minx)
                    minx = v.X;
                if (v.X > maxx)
                    maxx = v.X;
                if (v.Y < miny)
                    miny = v.Y;
                if (v.Y > maxy)
                    maxy = v.Y;

            }
            return new BoundingBox(new Vector2d(minx, miny), new Vector2d(maxx, maxy));
        }
        public double Width
        {
            get { return BottomRight.X - TopLeft.X; } 
        }
        public double Height
        {
            get { return BottomRight.Y - TopLeft.Y; }
        }
        public Vector2d Center
        {
            get { return new Vector2d((TopLeft.X + BottomRight.X) / 2, (TopLeft.Y + BottomRight.Y) / 2); }
        }
        public double Area
        {
            get { return Width*Height; }
        }

        public static BoundingBox operator +(BoundingBox bounding_box, Vector2d v)
        {
            Vector2d topleft = bounding_box.TopLeft + v;
            Vector2d bottomright = bounding_box.BottomRight + v;
            return new BoundingBox(topleft, bottomright);
        }        

        public void Translate(double x, double y)
        {
            TopLeft = new Vector2d(TopLeft.X + x, TopLeft.Y + y);
            BottomRight = new Vector2d(BottomRight.X + x, BottomRight.Y + y);
        }

        #region Operator Overloadings
        public static bool operator ==(BoundingBox b1, BoundingBox b2)
        {
            // If both are null, or both are same instance, return true.
            if (System.Object.ReferenceEquals(b1, b2))
                return true;

            // If one is null, but not both, return false.
            if (((object)b1 == null) || ((object)b2 == null))
            {
                return false;
            }

            return (b1.TopLeft == b2.TopLeft && b1.BottomRight == b2.BottomRight);
        }

        public static bool operator !=(BoundingBox b1, BoundingBox b2)
        {
            return !(b1 == b2);
        }
        #endregion

        #region Point Iside Checks
        /// <summary>
        /// USE THIS METHOD FOR WPF OR GDI SCREEN COORDS
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public bool PointInside(Vector2d v)
        {
            if (v.X > TopLeft.X && v.X < BottomRight.X &&
               v.Y > TopLeft.Y && v.Y < BottomRight.Y)
                return true;
            else
                return false;
        }
        /// <summary>
        /// USE THIS METHOD FOR WPF OR GDI SCREEN COORDS
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public bool PointInside(Vector2f v)
        {
            if (v.X > TopLeft.X && v.X < BottomRight.X &&
               v.Y > TopLeft.Y && v.Y < BottomRight.Y)
                return true;
            else
                return false;
        }
        #endregion

        #region Collision Detection
        public bool IsCollision(BoundingBox ref_box)
        {
            if(ref_box.PointInside(TopLeft))
                return true;
            if(ref_box.PointInside(BottomRight))
                return true;
            if(ref_box.PointInside(TopLeft + new Vector2d(Width,0)))
                return true;
            if(ref_box.PointInside(TopLeft + new Vector2d(0, -Height)))
                return true;

            return false;
        }
        #endregion

        public Vector2d TopLeft;
        public Vector2d BottomRight;
    }
}
