﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Logicx.IaApi.Interaction.Overlay.Management;

namespace Logicx.DiagramLib.Controls.MenuItemControls
{
    /// <summary>
    /// Interaction logic for SubMenuPanelMenuDiagramItem.xaml
    /// </summary>
    public partial class SubMenuPanelMenuDiagramItem : SubMenuPanelMenuItem
    {
        #region Constants
        public const string OPEN_ACTION = "Öffnen";

        public const string BEZEICHNUNG_ATTRIBUTE = "Bezeichnung";
        public const string HOME_ATTRIBUTE = "Home-Ansicht";
        #endregion

        #region Construction and Initialization

        public SubMenuPanelMenuDiagramItem(String title, UserControl control, UserControl minimized_info, bool title_left_aligned)
            : this(title, control, minimized_info)
        {
            if (title_left_aligned)
                title_tb.HorizontalAlignment = HorizontalAlignment.Left;
        }

        public SubMenuPanelMenuDiagramItem(String title, UserControl control, UserControl minimized_info)
        {
            //VisualMode = SubMenuItemVisualMode.Image;
            InitializeComponent();

            _control = control;
            if (_control != null)
            {
                viewbox.Child = _control;
            }

            _minimized_info = minimized_info;

            if(_minimized_info != null)
            {
                minimized_info_control.Content = _minimized_info;
                minimized_info_control.Visibility = Visibility.Visible;
            }
            title_tb.Text = title;

            MenuItemAction action = new MenuItemAction(OPEN_ACTION,
                               GenerateActionItemContainer(new Image { Source = (ImageSource)FindResource("open_action_explorer_imagesource"), Width = 14 }),
                               GenerateActionItemContainer(new Image { Source = (ImageSource)FindResource("open_action_explorer_imagesource"), Width = 14 }));
            _actions.Add(action);

            _attributes.Add(new MenuItemAttribute(BEZEICHNUNG_ATTRIBUTE, MenuItemAttributeType.LinkButton, HorizontalAlignment.Left, null, DefaultAction));
        }

        #endregion

        #region Operations
        public override string ToString()
        {
            return string.Empty;
        }

        public void ResetControls()
        {
            UserControl = null;
            minimized_info_control.Content = null;
            minimized_info_control.Visibility = Visibility.Collapsed;
        }

        #region SubMenuPanelMenuItem Implementation

        public override List<MenuItemAction> GetActions()
        {
            return null; // _actions;
        }

        public override List<MenuItemAttribute> GetAttributes()
        {
            return null; // _attributes;
        }

        public override void UpdateActionState()
        {

        }

        public override void UpdateAttributeState()
        {

        }

        public override object GetValue(MenuItemAttribute attribute)
        {
            if (attribute == null || attribute.Name == null)
                return null;

            if (attribute.Name.ToLower() == BEZEICHNUNG_ATTRIBUTE.ToLower())
                return title_tb.Text;

            return null;
        }

        public override List<string> GetCustomFilters()
        {
            return new List<string>();
        }

        public override bool ApplyCustomFilter(string filter_name)
        {
            return true;
        }

        public override SubMenuItemType ItemTyp
        {
            get
            {
                return SubMenuItemType.ToggleButton;
            }
        }

        public override MenuItemAction DefaultAction
        {
            get { return null; } // _actions[0]; }
        }

        #endregion
        #endregion

        #region Properties
        public UserControl UserControl
        {
            get { return _control; }
            set
            {
                if (value == null)
                    viewbox.Child = null;
                else
                {
                    viewbox.Child = value;
                    _control = value;
                }
            }
        }
        #endregion

        #region Attributes
        private UserControl _control;
        private UserControl _minimized_info;


        private List<MenuItemAction> _actions = new List<MenuItemAction>();
        private List<MenuItemAttribute> _attributes = new List<MenuItemAttribute>();
        #endregion

        #region Tests
        #endregion

    }
}
