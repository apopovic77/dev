﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApplication2
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            CompositionTarget.Rendering += CompositionTarget_Rendering;
        }

        void CompositionTarget_Rendering(object sender, EventArgs e)
        {
            left_pos += 0.01;
            Canvas.SetLeft(img, left_pos);
            i++;

            bool change_img = i % change_img_freq == 0;
            if (change_img)
            {
                curr_image++;
                if (curr_image > 4)
                    curr_image = 1;
                img.Source = (ImageSource)FindResource("bild"+curr_image);
            }
        }


        double left_pos = 0;
        int i = 0;

        int curr_image = 1;
        int count_images = 6;
        private int change_img_freq = 10;


        private void Slider_OnValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            change_img_freq = Convert.ToInt32(slider.Value);
        }
    }
}
