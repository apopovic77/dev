using System;
using System.Collections;
using System.Collections.Generic;

namespace MathLib
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public struct Vector2d
    {
        public enum RotationDirection
        {
            Clockwise,
            CounterClockwise,
            Undefined
        }

        public const double TwoPi = Math.PI * 2.0f;
        public const double RadAngle180 = Math.PI;
        public const double RadAngle360 = TwoPi;

        public static double NormalizeAngle(double var)
        {
            if (var >= 0.0f && var < RadAngle360) return var;

            if (var < 0.0f)
                var = RadAngle360 + (var % RadAngle360);
            else
                var = var % RadAngle360;

            return var;
        }
        /// <summary>
        /// gibt die rotations richtung an die man f�r die cirucal dist ben�tigt, damit sich die rotation in die richtige richtung dreht
        /// </summary>
        /// <param name="val1"></param>
        /// <param name="val2"></param>
        /// <returns></returns>
        public static RotationDirection TurnDir(double val1, double val2)
        {
            val1 = NormalizeAngle(val1);
            val2 = NormalizeAngle(val2);
            
            if (val1 == val2) 
                return RotationDirection.Undefined;

            if (Math.Abs(val1 - val2) > RadAngle180)
            {
                if (val1 > val2) return RotationDirection.Clockwise;
                else return RotationDirection.CounterClockwise;
            }
            else
            {
                if (val1 > val2) return RotationDirection.CounterClockwise;
                else return RotationDirection.Clockwise;
            }
        }

        /// <summary>
        /// gibt den k�rzensten abstand zwischen zwei winkel zur�ck
        /// </summary>
        /// <param name="v1"></param>
        /// <param name="v2"></param>
        /// <returns></returns>
        public static double CircularDist(double v1, double v2)
        {
            double vTemp = System.Math.Abs(NormalizeAngle(v1) - NormalizeAngle(v2));
            if (vTemp * 2.0f <= RadAngle360)
                return vTemp;
            else
                return RadAngle360 - vTemp;
        }



        public static Vector2d GetVector2dFromString(string geo_string)
        {
            string[] arr_lonlat = geo_string.Split(" ".ToCharArray());
            string lon = arr_lonlat[0];
            string lat = arr_lonlat[1];

            double flat = Convert.ToSingle(lat.Replace(".", ","));
            double flon = Convert.ToSingle(lon.Replace(".", ","));

            return new MathLib.Vector2d(flon, flat);
        }

        public static explicit operator Vector2f(Vector2d v)
        {
            return new Vector2f(Convert.ToSingle(v.X),Convert.ToSingle(v.Y));
        }


        public static Vector2d[] ArrayList2VectorArray(ArrayList al)
        {
            object[] oarr = al.ToArray();
            Vector2d[] varr = new Vector2d[oarr.Length];
            for (int i = 0; i < varr.Length; i++)
            {
                varr[i] = (Vector2d)oarr[i];
            }
            return varr;
        }


        public double Determinante(Vector2d v2)
        {
            return (this.X * v2.Y) - (v2.X * this.Y);
        }

        /// <summary>
        /// der �bergebene vektor wird als richtungsvektor gesehen
        /// der winkel, verh�ltnis wird zur�ckgegeben
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public double GetAngle()
        {
            if (X == 0 && Y == 0)
                return 0;
            if (X == 0 && Y > 0)
                return (double)(Math.PI / 2);
            if (X == 0 && Y < 0)
                return (double)(Math.PI + Math.PI / 2);
            if (Y == 0 && X > 0)
                return 0;
            if (Y == 0 && X < 0)
                return (double)(Math.PI);
            if (X > 0 && Y > 0)
                return (double)(Math.Atan(Y / X));
            if (X < 0 && Y > 0)
                return (double)(Math.PI + Math.Atan(Y / X));
            if (X < 0 && Y < 0)
                return (double)(Math.PI + Math.Atan(Y / X));
            if (X > 0 && Y < 0)
                return (double)(Math.PI + Math.PI + Math.Atan(Y / X));
            return 0;
        }
        public static Vector2d GetVector2dFromAngleDegree(double angle)
        {
            double rad = angle * Math.PI / 180;
            Vector2d v = new Vector2d(1, 0);
            v.RotateClockwise(rad);
            return v;
        }


        public static double GetAngleBetween(Vector2d a, Vector2d b)
        {
            double angle =  Math.Acos(a.GetSkalarProduct(b)/(a.GetLen()*b.GetLen()));
            return angle;
        }

        public double GetAngleBetween(Vector2d v2)
        {
            return GetAngleBetween(this, v2);
        }

        public double GetAngleDegree()
        {
            return GetAngle() * 180 / Math.PI;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="angle">Radianten bitte!</param>
        public void RotateCounterClockwise(double angle)
        {
            double betrag = GetLen();
            double phase = GetAngle();

            phase -= angle;

            X = Math.Cos(phase) * betrag;
            Y = Math.Sin(phase) * betrag;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="angle">Radianten bitte!</param>
        public void RotateClockwise(double angle)
        {
            double betrag = GetLen();
            double phase = GetAngle();

            phase += angle;

            X = Math.Cos(phase) * betrag;
            Y = Math.Sin(phase) * betrag;
        }

        public Vector2d(double x, double y)
        {
            X = x;
            Y = y;
        }


        public override string ToString()
        {
            return "X=" + X + "; y=" + Y + ";";
        }

        public static bool operator ==(Vector2d v1, Vector2d v2)
        {
            // If both are null, or both are same instance, return true.
            if (System.Object.ReferenceEquals(v1, v2))
                return true;

            // If one is null, but not both, return false.
            if (((object)v1 == null) || ((object)v2 == null))
            {
                return false;
            }

            return (v1.X == v2.X && v1.Y == v2.Y);
        }

        public static bool operator !=(Vector2d v1, Vector2d v2)
        {
            return !(v1 == v2);
        }
        public static Vector2d operator +(Vector2d a, Vector2d b)
        {
            Vector2d c = new Vector2d(0, 0);
            c.X = a.X + b.X;
            c.Y = a.Y + b.Y;
            return c;
        }
        public static Vector2d operator +(Vector2d a, double b)
        {
            Vector2d c = new Vector2d(0, 0);
            c.X = a.X + b;
            c.Y = a.Y + b;
            return c;
        }
        public static Vector2d operator -(Vector2d a, double b)
        {
            Vector2d c = new Vector2d(0, 0);
            c.X = a.X - b;
            c.Y = a.Y - b;
            return c;
        }
        public static Vector2d operator *(Vector2d a, double skalar)
        {
            return new Vector2d(a.X * skalar, a.Y * skalar);
        }
        public static Vector2d operator /(Vector2d a, double skalar)
        {
            return new Vector2d(a.X / skalar, a.Y / skalar);
        }
        public static Vector2d operator -(Vector2d a, Vector2d b)
        {
            Vector2d c = new Vector2d(a.X - b.X, a.Y - b.Y);
            return c;
        }

        public static double DegToRad(double deg)
        {
            return (deg * (double)Math.PI) / 180;
        }
        public static double RadToDeg(double rad)
        {
            return (rad * 180) / (double)Math.PI;
        }
        public static float DegToRad(float deg)
        {
            return (deg * (float)Math.PI) / 180;
        }
        public static float RadToDeg(float rad)
        {
            return (rad * 180) / (float)Math.PI;
        }

        public bool IsNull()
        {
            return X == 0 && Y == 0;
        }

        public double GetLen()
        {
            return (double)Math.Sqrt(X * X + Y * Y);
        }
        public Vector2d GetNorm()
        {
            Vector2d b = new Vector2d(0, 0);

            double len = GetLen();
            double len_kehrwert = (1 / len);
            if (len == 0)
                len_kehrwert = 0;
            b.X = X * len_kehrwert;
            b.Y = Y * len_kehrwert;
            return b;
        }
        public double GetSkalarProduct(Vector2d b)
        {
            return X * b.X + Y * b.Y;
        }
        public double GetSkalarProduct()
        {
            return X * X + Y * Y;
        }
        public Vector2d GetMirror()
        {
            Vector2d b = new Vector2d(0, 0);
            b.X = (-X);
            b.Y = (-Y);
            return b;
        }
        public Vector2d GetOrthogonal1()
        {
            Vector2d b = new Vector2d(0, 0);
            b.X = (-Y);
            b.Y = (X);
            return b;
        }
        public Vector2d GetOrthogonal2()
        {
            Vector2d b = new Vector2d(0, 0);
            b.X = (Y);
            b.Y = (-X);
            return b;
        }

        public override bool Equals(Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            Vector2d v = (Vector2d)obj;
            if ((Object)v == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (X == v.X) && (Y == v.Y);
        }

        public override int GetHashCode()
        {
            return X.GetHashCode() ^ Y.GetHashCode();
        }

        public double X;
        public double Y;


        public static List<Vector2d> GetVector2dListFromString(string geom)
        {
            List<Vector2d> v_list = new List<Vector2d>();
            string[] sarr_geom = geom.Split(",".ToCharArray());
            foreach (string s_v in sarr_geom)
            {
                Vector2d v = Vector2d.GetVector2dFromString(s_v);
                v_list.Add(v);
            }
            return v_list;
        }

        public static Vector2d[] GetVector2dArrayFromString(string geom)
        {
            List<Vector2d> v_list = GetVector2dListFromString(geom);
            return v_list.ToArray();
        }

        public static Vector2d GetRandomDirectionVector(Random r)
        {
            return new Vector2d(r.NextDouble() * 2 - 1, r.NextDouble() * 2 - 1).GetNorm();
        }
    }
}
