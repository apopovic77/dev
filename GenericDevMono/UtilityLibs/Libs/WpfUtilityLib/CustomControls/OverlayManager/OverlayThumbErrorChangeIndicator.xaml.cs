﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Logicx.WpfUtility.CustomControls.ChangeInfo;
using Logicx.WpfUtility.CustomControls.Validation;

namespace Logicx.WpfUtility.CustomControls.OverlayManager
{
    /// <summary>
    /// Interaction logic for OverlayThumbErrorChangeIndicator.xaml
    /// </summary>
    public partial class OverlayThumbErrorChangeIndicator : UserControl
    {
        #region Dependency Properties
        /// <summary>
        /// Mit diesem Property kann die anzahl der fehler gesetzt werden
        /// </summary>
        public int ErrorCount
        {
            get { return (int)GetValue(ErrorCountProperty); }
            set { SetValue(ErrorCountProperty, value); }
        }

        public static readonly DependencyProperty ErrorCountProperty =
            DependencyProperty.Register("ErrorCount", typeof(int), typeof(OverlayThumbErrorChangeIndicator), new UIPropertyMetadata(0));


        /// <summary>
        /// Mit diesem Property kann die sichtbarkeit des fehlerindicators gesetzt werden
        /// </summary>
        public bool ShowError
        {
            get { return (bool)GetValue(ShowErrorProperty); }
            set { SetValue(ShowErrorProperty, value); }
        }

        public static readonly DependencyProperty ShowErrorProperty =
            DependencyProperty.Register("ShowError", typeof(bool), typeof(OverlayThumbErrorChangeIndicator), new UIPropertyMetadata(false));

        /// <summary>
        /// Mit diesem Property kann die sichtbarkeit des fehlerindicators gesetzt werden
        /// </summary>
        public bool ShowChangeInfo
        {
            get { return (bool)GetValue(ShowChangeInfoProperty); }
            set { SetValue(ShowChangeInfoProperty, value); }
        }

        public static readonly DependencyProperty ShowChangeInfoProperty =
            DependencyProperty.Register("ShowChangeInfo", typeof(bool), typeof(OverlayThumbErrorChangeIndicator), new UIPropertyMetadata(false));

        #endregion

        public OverlayThumbErrorChangeIndicator()
        {
            InitializeComponent();

        }
    }
}
