﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Media3D;
using System.Windows.Shapes;
using System.Windows.Threading;
using Logicx.Geo.Geometries;
using Logicx.WpfUtility.Converters;
using Logicx.WpfUtility.CustomControls.Combobox;
using Logicx.WpfUtility.CustomControls.OverlayManager;
using Logicx.WpfUtility.CustomControls.TouchButton;
using Logicx.WpfUtility.Extensions;
using Logicx.WpfUtility.WindowManagement.Adorners;
using Logicx.WpfUtility.WpfHelpers;
using Logicx.WpfUtility.ZoomingPanning;
using MathLib;
using Microsoft.Windows.Controls;
using Microsoft.Windows.Controls.Primitives;
using PixelLab.Wpf;

namespace Logicx.WpfUtility.WindowManagement
{
    public class SubMenuPanel : SubMenuPanelBase<BaseFrame, SubMenuPanel, WindowTaskBar, WindowTitleBar, OverlayManager, SubMenuPanelMenuItem>
    {
        
    }
}
