﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.XPath;

namespace PublishVersionUpdate
{
    class Program
    {
        private static string _project_file = "";
        private static string _version_string = "";
        private static Version _version;

        static void Main(string[] args)
        {
            PrintInfo();

            if (args == null || args.Length != 2)
            {
                PrintUsage();
            }
            else
            {
                if(!File.Exists(args[0]))
                {
                    PrintError(1, "Project file not found '" + args[0] + "'");
                    Console.WriteLine(" ");
                    PrintUsage();
                }
                else
                {
                    _project_file = args[0];

                    try
                    {
                        _version_string = args[1];
                        _version = new Version(_version_string);
                    }
                    catch(Exception ex)
                    {
                        PrintError(2, "Version string not supported. " + ex.Message);
                        Console.WriteLine(" ");
                        PrintUsage();
                        return;
                    }

                    UpdatePublishVersion();
                }
            }
        }

        private static void UpdatePublishVersion()
        {
            try
            {
                XmlDocument xml_doc = new XmlDocument();
                xml_doc.Load(_project_file);

                if (xml_doc.GetElementsByTagName("ApplicationVersion").Count > 0)
                {
                    XmlNode node_version = xml_doc.GetElementsByTagName("ApplicationVersion")[0];

                    if (node_version != null)
                        node_version.InnerXml = _version_string;
                }
                
                if (xml_doc.GetElementsByTagName("ApplicationRevision").Count > 0)
                {
                    XmlNode node_revision = xml_doc.GetElementsByTagName("ApplicationRevision")[0];

                    if (node_revision != null)
                        node_revision.InnerXml = _version.Revision.ToString();
                }
                
                File.Copy(_project_file, _project_file + ".bak", true);
                xml_doc.Save(_project_file);
            }
            catch(Exception ex)
            {
                PrintError(3, "Error reading/writing CSProject file. " + ex.Message);
                Console.WriteLine(" ");
                PrintUsage();
                return;
            }
        }

        public static void PrintError(long id, string message)
        {
            Console.WriteLine(">>> ERROR " + id.ToString());
            Console.WriteLine("   " + message);
        }

        static void PrintInfo()
        {
            Console.WriteLine("Logicx Publish-Version update 1.0");
            Console.WriteLine(" ");
        }
        static void PrintUsage()
        {
            Console.WriteLine(" ");
            Console.WriteLine("Usage:  publishversionupdate.exe <project.csproj> <publish version>");
            Console.WriteLine(" ");
        }
    }
}
