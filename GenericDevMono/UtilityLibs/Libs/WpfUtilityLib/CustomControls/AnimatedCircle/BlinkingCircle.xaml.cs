﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Logicx.WpfUtility.CustomControls.AnimatedCircle
{
    /// <summary>
    /// Interaction logic for BlinkingCircle.xaml
    /// </summary>
    public partial class BlinkingCircle : UserControl
    {
        public BlinkingCircle()
        {
            InitializeComponent();
            //this.Loaded += new RoutedEventHandler(BlinkingCircle_Loaded);
        }

        void BlinkingCircle_Loaded(object sender, RoutedEventArgs e)
        {
            RadialGradientBrush br = (RadialGradientBrush)FindResource("pos_gradient_brush");

            main_ellipse.Fill = br;

            PointAnimation pa = new PointAnimation();
            pa.From = new Point(0.1, 0.9);
            pa.To = new Point(0.9, 0.1); ;
            pa.Duration = TimeSpan.FromSeconds(1);
            pa.RepeatBehavior = RepeatBehavior.Forever;
            pa.AutoReverse = true;

            if (br != null)
            {
                // create a modifyable clone
                br = br.Clone();

                // set the modifyable brush as the new background brush
                main_ellipse.Fill = br;

                br.BeginAnimation(RadialGradientBrush.GradientStopsProperty, pa);
            }
        }
    }
}
