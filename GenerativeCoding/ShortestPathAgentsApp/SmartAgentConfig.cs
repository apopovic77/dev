using System;
using Logicx.Utility;

namespace ShortestPathAgentsApp
{
    /// <summary>
    /// If you want to create a new Configuration-File
    /// this class can be copied and renamed. It has
    /// all neccessary methods and tags that are used
    /// within the SettingsManagerApplication
    /// </summary>
    public class SmartAgentConfig : SystemConfig
    {
        private SmartAgentConfig(string path_to_config_file)
            : base(path_to_config_file)
        {
        }

        protected override void RegisterConfigProperties()
        {
            //settingsmanager: register begin

            AgentVelocity.PropertyChanged += Config_PropertyChanged;
			ConfigProperties.Add("AgentVelocity", AgentVelocity);
			AgentRotationVelocityFactor.PropertyChanged += Config_PropertyChanged;
			ConfigProperties.Add("AgentRotationVelocityFactor", AgentRotationVelocityFactor);
			AgentTimeToLive.PropertyChanged += Config_PropertyChanged;
			ConfigProperties.Add("AgentTimeToLive", AgentTimeToLive);
			EvolutionPopulationSize.PropertyChanged += Config_PropertyChanged;
			ConfigProperties.Add("EvolutionPopulationSize", EvolutionPopulationSize);
			EvolutionMutationRate.PropertyChanged += Config_PropertyChanged;
			ConfigProperties.Add("EvolutionMutationRate", EvolutionMutationRate);
			//settingsmanager: register end
        }

        protected override void ExecLoadValues()
        {
            //settingsmanager: construction begin

            //SettingsManager-Kommentar: no comment
			if (_config.AppSettings.Settings["AgentVelocity_value"] != null)
				AgentVelocity.Value = Convert.ToSingle(_config.AppSettings.Settings["AgentVelocity_value"].Value);
			if (_config.AppSettings.Settings["AgentVelocity_bez"] != null)
				AgentVelocity.Bezeichnung = _config.AppSettings.Settings["AgentVelocity_bez"].Value;
			//SettingsManager-Kommentar: no comment
			if (_config.AppSettings.Settings["AgentRotationVelocityFactor_value"] != null)
				AgentRotationVelocityFactor.Value = Convert.ToSingle(_config.AppSettings.Settings["AgentRotationVelocityFactor_value"].Value);
			if (_config.AppSettings.Settings["AgentRotationVelocityFactor_bez"] != null)
				AgentRotationVelocityFactor.Bezeichnung = _config.AppSettings.Settings["AgentRotationVelocityFactor_bez"].Value;
			if (_config.AppSettings.Settings["AgentRotationVelocityFactor_min"] != null)
				AgentRotationVelocityFactor.Minimum = Convert.ToSingle(_config.AppSettings.Settings["AgentRotationVelocityFactor_min"].Value);
			if (_config.AppSettings.Settings["AgentRotationVelocityFactor_max"] != null)
				AgentRotationVelocityFactor.Maximum = Convert.ToSingle(_config.AppSettings.Settings["AgentRotationVelocityFactor_max"].Value);
			//SettingsManager-Kommentar: no comment
			if (_config.AppSettings.Settings["AgentTimeToLive_value"] != null)
				AgentTimeToLive.Value = Convert.ToInt32(_config.AppSettings.Settings["AgentTimeToLive_value"].Value);
			if (_config.AppSettings.Settings["AgentTimeToLive_bez"] != null)
				AgentTimeToLive.Bezeichnung = _config.AppSettings.Settings["AgentTimeToLive_bez"].Value;
			if (_config.AppSettings.Settings["AgentTimeToLive_min"] != null)
				AgentTimeToLive.Minimum = Convert.ToInt32(_config.AppSettings.Settings["AgentTimeToLive_min"].Value);
			if (_config.AppSettings.Settings["AgentTimeToLive_max"] != null)
				AgentTimeToLive.Maximum = Convert.ToInt32(_config.AppSettings.Settings["AgentTimeToLive_max"].Value);
			//SettingsManager-Kommentar: no comment
			if (_config.AppSettings.Settings["EvolutionPopulationSize_value"] != null)
				EvolutionPopulationSize.Value = Convert.ToInt32(_config.AppSettings.Settings["EvolutionPopulationSize_value"].Value);
			if (_config.AppSettings.Settings["EvolutionPopulationSize_bez"] != null)
				EvolutionPopulationSize.Bezeichnung = _config.AppSettings.Settings["EvolutionPopulationSize_bez"].Value;
			if (_config.AppSettings.Settings["EvolutionPopulationSize_min"] != null)
				EvolutionPopulationSize.Minimum = Convert.ToInt32(_config.AppSettings.Settings["EvolutionPopulationSize_min"].Value);
			if (_config.AppSettings.Settings["EvolutionPopulationSize_max"] != null)
				EvolutionPopulationSize.Maximum = Convert.ToInt32(_config.AppSettings.Settings["EvolutionPopulationSize_max"].Value);
			//SettingsManager-Kommentar: no comment
			if (_config.AppSettings.Settings["EvolutionMutationRate_value"] != null)
				EvolutionMutationRate.Value = Convert.ToSingle(_config.AppSettings.Settings["EvolutionMutationRate_value"].Value);
			if (_config.AppSettings.Settings["EvolutionMutationRate_bez"] != null)
				EvolutionMutationRate.Bezeichnung = _config.AppSettings.Settings["EvolutionMutationRate_bez"].Value;
			if (_config.AppSettings.Settings["EvolutionMutationRate_min"] != null)
				EvolutionMutationRate.Minimum = Convert.ToSingle(_config.AppSettings.Settings["EvolutionMutationRate_min"].Value);
			if (_config.AppSettings.Settings["EvolutionMutationRate_max"] != null)
				EvolutionMutationRate.Maximum = Convert.ToSingle(_config.AppSettings.Settings["EvolutionMutationRate_max"].Value);
			//settingsmanager: construction end 
        }

        protected override void ExecSaveSettings()
        {
            //settingsmanager: savesettings begin

            //SettingsManager-Kommentar: no comment
			AddKey(_config, "AgentVelocity_value", AgentVelocity.Value.ToString());
			AddKey(_config, "AgentVelocity_bez", AgentVelocity.Bezeichnung);
			//SettingsManager-Kommentar: no comment
			AddKey(_config, "AgentRotationVelocityFactor_value", AgentRotationVelocityFactor.Value.ToString());
			AddKey(_config, "AgentRotationVelocityFactor_bez", AgentRotationVelocityFactor.Bezeichnung);
			AddKey(_config, "AgentRotationVelocityFactor_min", AgentRotationVelocityFactor.Minimum.ToString());
			AddKey(_config, "AgentRotationVelocityFactor_max", AgentRotationVelocityFactor.Maximum.ToString());
			//SettingsManager-Kommentar: no comment
			AddKey(_config, "AgentTimeToLive_value", AgentTimeToLive.Value.ToString());
			AddKey(_config, "AgentTimeToLive_bez", AgentTimeToLive.Bezeichnung);
			AddKey(_config, "AgentTimeToLive_min", AgentTimeToLive.Minimum.ToString());
			AddKey(_config, "AgentTimeToLive_max", AgentTimeToLive.Maximum.ToString());
			//SettingsManager-Kommentar: no comment
			AddKey(_config, "EvolutionPopulationSize_value", EvolutionPopulationSize.Value.ToString());
			AddKey(_config, "EvolutionPopulationSize_bez", EvolutionPopulationSize.Bezeichnung);
			AddKey(_config, "EvolutionPopulationSize_min", EvolutionPopulationSize.Minimum.ToString());
			AddKey(_config, "EvolutionPopulationSize_max", EvolutionPopulationSize.Maximum.ToString());
			//SettingsManager-Kommentar: no comment
			AddKey(_config, "EvolutionMutationRate_value", EvolutionMutationRate.Value.ToString());
			AddKey(_config, "EvolutionMutationRate_bez", EvolutionMutationRate.Bezeichnung);
			AddKey(_config, "EvolutionMutationRate_min", EvolutionMutationRate.Minimum.ToString());
			AddKey(_config, "EvolutionMutationRate_max", EvolutionMutationRate.Maximum.ToString());
			//settingsmanager: savesettings end
        }


        //settingsmanager: vardef begin 

        public static FloatConfigValue AgentVelocity = new FloatConfigValue("AgentVelocity", "AgentVelocity", 10f, 0.001f, 100f);
		public static FloatConfigValue AgentRotationVelocityFactor = new FloatConfigValue("AgentRotationVelocityFactor", "AgentRotationVelocityFactor", 0.1f,0f,1f);
		public static IntConfigValue AgentTimeToLive = new IntConfigValue("AgentTimeToLive", "AgentTimeToLive", 10,0,200);
		public static IntConfigValue EvolutionPopulationSize = new IntConfigValue("EvolutionPopulationSize", "EvolutionPopulationSize", 50,1,800);
		public static FloatConfigValue EvolutionMutationRate = new FloatConfigValue("EvolutionMutationRate", "EvolutionMutationRate", 0.2f,0f,1f);
		//settingsmanager: vardef end 


        #region Singleton Instance
        public static void CreateInstance(string path_to_config_file)
        {
            lock (_instance_creation_lock)
            {
                _instance = new SmartAgentConfig(path_to_config_file);
            }
        }
        public static void CreateInstance()
        {
            CreateInstance(GetPathToConfigFile());
        }





        public static SmartAgentConfig Instance
        {
            get
            {
                lock (_instance_creation_lock)
                {
                    if (_instance == null)
                        throw new Exception("Instance not existing, you need to create the instance via CreateInstance()");
                    return _instance;
                }
            }
        }

        private static object _instance_creation_lock = new object();
        private static SmartAgentConfig _instance;
        #endregion
    }
}

