﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LinqLib.Linq2Sql.Progress
{
    /// <summary>
    /// Class identifying the submission state
    /// </summary>
    public class SubmissionStatistics
    {
        #region Properties
        /// <summary>
        /// Gets/sets the start time of the submission
        /// </summary>
        public DateTime Started { get; set; }
        /// <summary>
        /// Gets/sets the end time of the submission
        /// </summary>
        public DateTime Ended { get; set; }
        /// <summary>
        /// Gets/sets the time needed for the submission
        /// </summary>
        public TimeSpan TimeNeeded { get; set; }
        /// <summary>
        /// Gets/sets a flag if the submission succeed
        /// </summary>
        public bool Succeed { get; set; }
        /// <summary>
        /// Gets/sets the last error occured during the submission
        /// </summary>
        public Exception LastError { get; set; }
        /// <summary>
        /// Gets/sets the number of inserts done during the submission
        /// </summary>
        public int NumberOfInserts { get; set; }
        /// <summary>
        /// Gets/sets the number of update done during the submission
        /// </summary>
        public int NumberOfUpdates { get; set; }
        /// <summary>
        /// Gets/sets the number of deletes done during the submission
        /// </summary>
        public int NumberOfDeletes { get; set; }
        /// <summary>
        /// Gets/sets the complete submission history which holds all SQL commands used during the submission
        /// </summary>
        public string SubmissionHistory { get; set; }
        #endregion

        public string ToTimeInfoString()
        {
            string sRet = "";
            sRet = "Start: " + Started.ToString("HH:mm:ss") + ", End: " + Ended.ToString("HH:mm:ss") + ", Time: " + TimeNeeded.ToString();
            return sRet;
        }

        public string ToActionInfoString()
        {
            string sRet = "";
            sRet = string.Format("Inserts: {0}, Updates: {1}, Deletes: {2}", NumberOfInserts, NumberOfUpdates, NumberOfDeletes);
            return sRet;
        }

        public string ToSuccessInfoString()
        {
            string sRet = "";
            if (Succeed)
                sRet = "Succeed";
            else
                sRet = "Failed (" + (LastError != null ? LastError.Message : "") + ")";
            return sRet;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(ToSuccessInfoString());
            sb.AppendLine(ToTimeInfoString());
            sb.AppendLine(ToActionInfoString());

            return sb.ToString();
        }

        public string ToFullString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(ToSuccessInfoString());
            sb.AppendLine(ToTimeInfoString());
            sb.AppendLine(ToActionInfoString());
            sb.AppendLine(SubmissionHistory);

            return sb.ToString();
            
        }
    }

    /// <summary>
    /// Event argument class for the submission completed event
    /// </summary>
    public class SubmissionCompletedEventArgs : EventArgs
    {
        #region Construction and initialization
        /// <summary>
        /// Constructor of the class
        /// </summary>
        /// <param name="statistics">statistics of the last submission</param>
        public SubmissionCompletedEventArgs(SubmissionStatistics statistics)
        {
            _statistics = statistics;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets the statistics of the last submission
        /// </summary>
        public SubmissionStatistics Statistics
        {
            get
            {
                return _statistics;
            }
        }
        #endregion

        #region Attributes
        private SubmissionStatistics _statistics = new SubmissionStatistics();
        #endregion
    }
}
