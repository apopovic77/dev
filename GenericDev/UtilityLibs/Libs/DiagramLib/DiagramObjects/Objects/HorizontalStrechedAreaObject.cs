﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Logicx.DiagramLib.Controls;

namespace Logicx.DiagramLib.DiagramObjects.Objects
{
    public abstract class HorizontalStrechedAreaObject<Tx, Ty> : AreaObject<Tx, Ty>
    {
        #region Construction and Initialization
        /// <summary>
        /// Constructor of the class
        /// </summary>
        /// <param name="from_y_value"></param>
        /// <param name="to_y_value"></param>
        public HorizontalStrechedAreaObject(Ty from_y_value, Ty to_y_value)
            : base(default(Tx), from_y_value, default(Tx), to_y_value)
        {
        }
        #endregion

        #region Operations
        protected override void OnUpdateVisualBindings()
        {
            base.OnUpdateVisualBindings();
            if (ObjectVisual == null)
                return;

            // always begins at 0
            BindingOperations.ClearBinding(ObjectVisual, Canvas.LeftProperty);
            Canvas.SetLeft(ObjectVisual, 0);


            // always uses full height
            BindingOperations.ClearBinding(ObjectVisual, FrameworkElement.WidthProperty);

            Binding full_width = new Binding("ActualWidth");
            //full_width.RelativeSource = new RelativeSource(RelativeSourceMode.FindAncestor, typeof(DiagramBase<Tx, Ty>), 1);
            full_width.RelativeSource = new RelativeSource(RelativeSourceMode.FindAncestor, typeof(Canvas), 1);
            ObjectVisual.SetBinding(FrameworkElement.WidthProperty, full_width);
        }
        #endregion

        #region Properties
        public override bool CanResizeX
        {
            get { return false; }
        }
        #endregion
    }
}
