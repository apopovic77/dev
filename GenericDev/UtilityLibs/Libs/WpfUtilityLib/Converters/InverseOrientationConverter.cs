﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Data;

namespace Logicx.WpfUtility.Converters
{
    [ValueConversion(typeof(Orientation), typeof(Orientation))]
    public class InverseOrientationConverter : IValueConverter
    {
        #region IValueConverter Members
        public object Convert(object value, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            if (targetType != typeof(Orientation))
                throw new InvalidOperationException("The target must be a Orientation");

            if (((Orientation)value) == Orientation.Vertical)
                return Orientation.Horizontal;

            return Orientation.Vertical;
        }

        public object ConvertBack(object value, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            if (targetType != typeof(Orientation))
                throw new InvalidOperationException("The target must be a Orientation");

            if (((Orientation)value) == Orientation.Vertical)
                return Orientation.Horizontal;

            return Orientation.Vertical;
        }
        #endregion
    }
}
