﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Logicx.WpfUtility.CustomControls
{
    /// <summary>
    /// Interaction logic for ColorPicker.xaml
    /// </summary>
    public partial class ColorPicker : UserControl
    {
        public class ColorChangedEventArgs :EventArgs
        {
            public ColorChangedEventArgs(Color c)
            {
                Color = c;
            }

            public Color Color;
        }

        public ColorPicker()
        {
            InitializeComponent();
            this.DataContext = this;
            _hexcolortb_change_enabled = true;
        }

        private Color GetCurrentColor()
        {
            int h_val, s_val, l_val;

            if (!Int32.TryParse(h_tb.Text, out h_val))
                h_val = 0;
            if (!Int32.TryParse(s_tb.Text, out s_val))
                s_val = 0;
            if (!Int32.TryParse(l_tb.Text, out l_val))
                l_val = 0;

            if (h_val == 0 && s_val == 0 && l_val == 0 || l_val == 255)
                return Colors.Transparent;
            else
                return ColorGenerator.ColorFromHSL((double)h_val / 255, (double)s_val / 255, (double)l_val / 255);
        }

        private void SetColor()
        {
            Color curr_color = GetCurrentColor();
            color_border.Background = new SolidColorBrush(curr_color);
            if (_last_active_color != curr_color)
                if (ColorChanged != null)
                    ColorChanged(this, new ColorChangedEventArgs(curr_color));
        }

        #region EventHandlers
        private void hexcolor_tb_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!_hexcolortb_change_enabled) return;
            
            Color c = ColorGenerator.HexToColor(hexcolor_tb.Text);
            double h, s, l;
            ColorGenerator.ColorToHSL(c, out h,out s, out l);
            int i_h = Convert.ToInt32(255 * h);
            int i_s = Convert.ToInt32(255 * s);
            int i_l = Convert.ToInt32(255 * l);

            _text_change_enabled = false;
            _slider_change_enabled = false;

            h_slider.Value = i_h;
            s_slider.Value = i_s;
            l_slider.Value = i_l;

            h_tb.Text = i_h.ToString();
            s_tb.Text = i_s.ToString();
            l_tb.Text = i_l.ToString();

            _text_change_enabled = true;
            _slider_change_enabled = true;

            SetColor();
        }

        private void hsl_slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (!_slider_change_enabled) return;

            _text_change_enabled = false;
            
            //set value to textboxes
            h_tb.Text = Math.Round(h_slider.Value).ToString();
            s_tb.Text = Math.Round(s_slider.Value).ToString();
            l_tb.Text = Math.Round(l_slider.Value).ToString();

            SetHexColorValue();
            
            _text_change_enabled = true;
            
            SetColor();
        }

        private void SetHexColorValue()
        {
            _hexcolortb_change_enabled = false;
            hexcolor_tb.Text = ColorGenerator.ColorToHex(GetCurrentColor());
            _hexcolortb_change_enabled = true;
        }

        private void hsl_tb_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!_text_change_enabled) return;
            int h_val, s_val, l_val;

            if (!Int32.TryParse(h_tb.Text, out h_val))
            {
                h_val = 0;
                h_tb.Text = "0";
            }
            if (!Int32.TryParse(s_tb.Text, out s_val))
            {
                s_val = 0;
                s_tb.Text = "0";
            }
            if (!Int32.TryParse(l_tb.Text, out l_val))
            {
                l_val = 0;
                l_tb.Text = "0";
            }
            
            if (h_val > 255)
            {
                h_val = 255;
                h_tb.Text = "255";
            }
            if (s_val > 255)
            {
                s_val = 255;
                s_tb.Text = "255";
            }
            if (l_val > 255)
            {
                l_val = 255;
                l_tb.Text = "255";
            }

            _slider_change_enabled = false;
            h_slider.Value = h_val;
            s_slider.Value = s_val;
            l_slider.Value = l_val;
            _slider_change_enabled = true;

            SetColor();
        }
        #endregion

        #region Dependency Properties
        public string ColorHex
        {
            get { return (string)GetValue(ColorHexProperty); }
            set { SetValue(ColorHexProperty, value); }
        }
        public static readonly DependencyProperty ColorHexProperty = DependencyProperty.Register("ColorHex", typeof(string), typeof(ColorPicker), new UIPropertyMetadata("#000000"));
        #endregion

        #region Properties
        //public int Year
        //{
        //    set
        //    {
        //        _year = value;
        //        year.Text = value.ToString();
        //    }
        //    get
        //    {
        //        return _year;
        //    }
        //}
        public Color Color
        {
            set
            {
                _hexcolortb_change_enabled = true;
                hexcolor_tb.Text = ColorGenerator.ColorToHex(value);
            }
            get
            {
                return GetCurrentColor();
            }
        }
        public SolidColorBrush BackgroundBrush
        {
            set
            {
                this.Background = value;
            }
        }
        #endregion

        #region Attributes
        protected int _year = -1;
        protected bool _text_change_enabled = false;
        protected bool _slider_change_enabled = true;
        protected bool _hexcolortb_change_enabled = false;

        protected Color _last_active_color;

        public event ColorChangedEventHandler ColorChanged;
        public delegate void ColorChangedEventHandler(object sender, ColorChangedEventArgs colorchanged_evargs);
        #endregion

    }
}
