using System;
using System.Collections.Generic;
using System.Text;

namespace GeoLib
{
    class GeoLibException : Exception
    {
        public static String[] eMessages = {   /*0*/				 "no id available, query not successful, specify an id an requery!",
											   /*1*/				 "dbconnection missing! data cannot be synchronized with db! ",
											   /*2*/				 "no data found! line cannot be fetched!",
											   /*3*/				 "no data found! scalar cannot be fetched!",
											   /*4*/				 "datatype not valid, value is of antoher datatype --> action cannot be performed",
											   /*5*/				 "datatype missing cannot perfom action",
											   /*6*/				 "Value not set, cannot insert Parameter!",
											   /*7*/				 "DataType cannot be evaluated, Value cannot be set!",
											   /*8*/				 "Messagestatus unknown!",
											   /*9*/				 "no id available, update cannot be done!",
											   /*10*/				 "PolyCreation failed lifelock erkannt, algorithmus bricht nicht ab!",
                                               /*11*/				 "Route Discretisation erfolglos, wahrscheinlich sind keine routeninformationen vorhanden!",
                                               /*12*/                "remot host not available",
                                               /*13*/                 "no more free object this cant be, bug in code",
                                               /*14*/                "route discretisation cannot be performed via mapx, as no remote object is available",
                                               /*15*/                "UserX konnte nicht angelegt werden auf der PushX Db"
										   };

        private int m_errorCode = -1;
        public GeoLibException(int errorCode)
            : base(eMessages[errorCode])
        {
            m_errorCode = errorCode;
        }
        public GeoLibException(string errormsg)
            : base(errormsg)
        {
        }

        public int ErrorCode
        {
            get
            {
                return m_errorCode;
            }
        }
    }
}

