﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows.Data;

namespace Logicx.WpfUtility.Converters
{
    [ValueConversion(typeof(double), typeof(double))]
    public class DoubleValueAdderConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            double d_val = (double)value;
            double para_val;
            if (parameter is double)
            {
                double ret = d_val + (double) parameter;
                return ret < 0 ? 0 : ret;
            }
            else if (parameter is string && double.TryParse((string)parameter, out para_val))
            {
                double ret = d_val + para_val;
                return ret < 0 ? 0 : ret;
            }
 
            return d_val;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            double d_val = (double)value;
            double para_val;
            if (parameter is double)
                return d_val - (double)parameter;
            else if (parameter is string && double.TryParse((string)parameter, out para_val))
                return d_val - para_val;
            return d_val;
        }
    }
}
