﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using Logicx.DirectX.Objects;
using Microsoft.DirectX.Direct3D;
using Font=Microsoft.DirectX.Direct3D.Font;
using Logicx.DirectX.Engine;
using Microsoft.DirectX;

namespace DirectXLib.Objects.Fonts
{
    public class FontWorld2d : Font2d
    {
          #region Contruction

        public FontWorld2d(string text, string font_fam, int font_size, FontStyle fontweight)
            : base(text, font_fam, font_size, fontweight)
        {
        }

        #endregion

        public override void Render(float elapsedTime)
        {
            //dont do anything if there is no text do render
            if (string.IsNullOrEmpty(Text))
                return;

            if (WithAutoFontScaling)
            {
                //get scaling factor for font
                FontScalingFactor = GetScalingValuesForFontRendering();
            }

            //calc font dimensions
            float font_width_ges = Text.Length * _font_width_smallest * FontScalingFactor;
            float font_height_ges = _font_height_smallest * FontScalingFactor;

            //set world coord system back to identity
            Engine3d.Device3d.Transform.World = Microsoft.DirectX.Matrix.Identity
                                                *
                                                Microsoft.DirectX.Matrix.Scaling(FontScalingFactor, FontScalingFactor, FontScalingFactor)
                                                *
                                                Microsoft.DirectX.Matrix.RotationYawPitchRoll(0.0f, Convert.ToSingle(Math.PI), 0.0f)
                                                *
                                                Microsoft.DirectX.Matrix.Translation(Position.X - (font_width_ges / 2.0f), Position.Y + (font_height_ges / 2.0f), Position.Z);

            //begin with sprite and draw font
            _sprite.Begin(SpriteFlags.ObjectSpace | SpriteFlags.SortTexture | SpriteFlags.AlphaBlend);
            _active_font.Font3d.DrawText(_sprite, Text, 0, 0, Color);
            _sprite.End();




        }

        private float GetScalingValuesForFontRendering()
        {
            float cam_z = Engine3d.Camera.Position.Z;

            float val_at_z_top = 38;
            float val_at_z_bottom = 1;
            float z_top = -41900;
            float z_bottom = -476;

            float k = (val_at_z_top - val_at_z_bottom) / (z_top - z_bottom);
            float d = val_at_z_bottom - k * z_bottom;

            return k * cam_z + d;
        }


        #region Properties

        #endregion

        #region Attribs
        private const int _font_height_smallest = 12;
        private readonly int _font_width_smallest = Convert.ToInt32(Math.Floor(_font_height_smallest * 0.40));
        public bool WithAutoFontScaling = false;
        public float FontScalingFactor = 1f;
        #endregion
    }
}
