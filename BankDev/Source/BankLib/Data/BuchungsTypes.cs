﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankLib.Data
{

    public class BuchungsTypes
    {
        public static Dictionary<string, string> Values = new Dictionary<string, string>()
        {
            {/*BuchungsTextOrg*/"Abschluss",                    /*BuchungsType*/"Abschluss"},
            {/*BuchungsTextOrg*/"Bankomat",                     /*BuchungsType*/"Bankomat"},
            {/*BuchungsTextOrg*/"Bankomat Int.",                /*BuchungsType*/"Bankomat"},
            {/*BuchungsTextOrg*/"Bankomat Int.EU",              /*BuchungsType*/"Bankomat"},
            {/*BuchungsTextOrg*/"Barauszahlung",                /*BuchungsType*/"Abbuchung"},
            {/*BuchungsTextOrg*/"Bareinzahlung",                /*BuchungsType*/"Gutschrift"},
            {/*BuchungsTextOrg*/"Datenträger-Umsatz",           /*BuchungsType*/"Datenträger-Umsatz"},
            {/*BuchungsTextOrg*/"Einzugsermächtigung",          /*BuchungsType*/"Abbuchung"},
            {/*BuchungsTextOrg*/"EPS-Auftrag",                  /*BuchungsType*/"Abbuchung"},
            {/*BuchungsTextOrg*/"Internetauftrag",              /*BuchungsType*/"Abbuchung"},
            {/*BuchungsTextOrg*/"POS",                          /*BuchungsType*/"Bankomat Kassa"},
            {/*BuchungsTextOrg*/"POS Int.Zahl.Auftr.EU",        /*BuchungsType*/"Bankomat Kassa"},
            {/*BuchungsTextOrg*/"POS Int.Zahl.Auftrag",         /*BuchungsType*/"Bankomat Kassa"},
            {/*BuchungsTextOrg*/"POS Zahlungsauftrag",          /*BuchungsType*/"Bankomat Kassa"},
            {/*BuchungsTextOrg*/"SB Konto Übertrag",            /*BuchungsType*/"SB Konto Übertrag"},
            {/*BuchungsTextOrg*/"SB-Quick Laden",               /*BuchungsType*/"Abbuchung"},
            {/*BuchungsTextOrg*/"Selbstbedienung",              /*BuchungsType*/"Bankomat"},
            {/*BuchungsTextOrg*/"SEPA-Gutschrift",              /*BuchungsType*/"Gutschrift"},
            {/*BuchungsTextOrg*/"SEPA-Lastschrift",             /*BuchungsType*/"Abbuchung"},
            {/*BuchungsTextOrg*/"SEPA-Zahlung INET",            /*BuchungsType*/"Abbuchung"},
        };
    }
}

