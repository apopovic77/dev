﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using Logicx.Geo.Geometries;
using Logicx.WpfUtility.Converters;
using Logicx.WpfUtility.CustomControls.Combobox;
using Logicx.WpfUtility.Extensions;
using Logicx.WpfUtility.UiManipulation;
using Logicx.WpfUtility.WindowManagement.Adorners;
using Logicx.WpfUtility.WpfHelpers;
using Logicx.WpfUtility.ZoomingPanning;
using MathLib;
using Microsoft.Windows.Controls;
using Microsoft.Windows.Controls.Primitives;
using PixelLab.Wpf;
using System.Timers;
using Logicx.WpfUtility.CustomControls.TouchButton;

namespace Logicx.WpfUtility.WindowManagement
{
    public interface ISubMenuPanelBase
    {
    
    }

    /// <summary>
    /// Interaction logic for SubMenuPanel.xaml
    /// </summary>
    public partial class SubMenuPanel : UserControl, ISubMenuPanelBase
    {
        #region nested types
        public enum LayoutMode
        {
            Kachel,
            Line,
            Grid
        }

        

        #endregion

        #region Construction & Init
        public SubMenuPanel()
        {
            _root_bread_crumb_item = new SubMenuPanelBreadCrumbItem(this, string.Empty);
            _breadcrumb_path.Add(_root_bread_crumb_item);
            
            InitializeComponent();
            WithCustomButton1 = false;
            WithCustomButton = false;

            CurrentBreadCrumbItem = _root_bread_crumb_item;
            lb_breadcrumb.ItemsSource = _breadcrumb_path;

            _root_items_readonly = new ReadOnlyObservableCollection<SubMenuPanelMenuItem>(_root_items);
            _items.CollectionChanged += new System.Collections.Specialized.NotifyCollectionChangedEventHandler(_items_CollectionChanged);
            //zum verzögerten registrieren der adorner und dem animated panel :-(
            Loaded += new RoutedEventHandler(Menu_Loaded);
            this.Unloaded += new RoutedEventHandler(Menu_Unloaded);

            KeyDown += new KeyEventHandler(SubMenuPanel_KeyDown);
            KeyUp += new KeyEventHandler(SubMenuPanel_KeyUp);


            BigItemSize = new Size(200, 100);
            SmallItemSize = new Size(100, 50);

            _active_layout_mode = LayoutMode.Line;
            orderlayout1_button.IsChecked = false;
            orderlayout2_button.IsChecked = true;
            orderlayout3_button.IsChecked = false;

            InitDefaultValues();
            UpdateBreadCrumbVisibility();
        }

        private void InitDefaultValues()
        {
            WithCustomButton = false;
        }

        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            if(e.Property == ButtonHighlightBackgroundProperty)
            {
                if(HighlightSearchButton)
                {
                    search_button.Background = e.NewValue as Brush;
                }
            }
            else if(e.Property == HighlightSearchButtonProperty)
            {
                if((bool)e.NewValue)
                {
                    search_button.Background = ButtonHighlightBackground;
                }
                else
                {
                    search_button.Background = Brushes.Transparent;
                }
            }
            base.OnPropertyChanged(e);
        }

        void RegisterLayoutCheckedEvents()
        {
            orderlayout1_button.Checked += new RoutedEventHandler(orderlayout1_button_Checked);
            orderlayout1_button.Unchecked += new RoutedEventHandler(orderlayout1_button_Unchecked);
            orderlayout2_button.Checked += new RoutedEventHandler(orderlayout2_button_Checked);
            orderlayout2_button.Unchecked += new RoutedEventHandler(orderlayout2_button_Unchecked);
            orderlayout3_button.Checked += new RoutedEventHandler(orderlayout3_button_Checked);
            orderlayout3_button.Unchecked += new RoutedEventHandler(orderlayout3_button_Unchecked);
        }

        void UnRegisterLayoutCheckedEvents()
        {
            orderlayout1_button.Checked -=new RoutedEventHandler(orderlayout1_button_Checked);
            orderlayout1_button.Unchecked -=new RoutedEventHandler(orderlayout1_button_Unchecked);
            orderlayout2_button.Checked -=new RoutedEventHandler(orderlayout2_button_Checked);
            orderlayout2_button.Unchecked -=new RoutedEventHandler(orderlayout2_button_Unchecked);
            orderlayout3_button.Checked -=new RoutedEventHandler(orderlayout3_button_Checked);
            orderlayout3_button.Unchecked -=new RoutedEventHandler(orderlayout3_button_Unchecked);
        }

        void UpdateBreadCrumbVisibility()
        {
            List<SubMenuPanelBreadCrumbItem> rem_items = new List<SubMenuPanelBreadCrumbItem>();
            bool found_current_item = false;

            foreach(SubMenuPanelBreadCrumbItem check_item in _breadcrumb_path)
            {
                if(check_item == CurrentBreadCrumbItem)
                {
                    found_current_item = true;
                }
                else if(found_current_item)
                {
                    rem_items.Add(check_item);
                }
            }

            foreach(SubMenuPanelBreadCrumbItem rem_item in rem_items)
            {
                _breadcrumb_path.Remove(rem_item);
            }

            if(CurrentBreadCrumbItem == _root_bread_crumb_item || _breadcrumb_path.Count <= 1)
            {
                lb_breadcrumb.Visibility = Visibility.Collapsed;
            }
            else
            {
                lb_breadcrumb.Visibility = Visibility.Visible;
            }

            for(int i=0; i < _breadcrumb_path.Count; i++)
            {
                if (i == _breadcrumb_path.Count - 1)
                    _breadcrumb_path[i].IsLastItem = true;
                else
                    _breadcrumb_path[i].IsLastItem = false;
            }
        }

        void Menu_Unloaded(object sender, RoutedEventArgs e)
        {
            ActiveStyleAdornerManager.Instance.DeRegister(arrowleft_button);
            ActiveStyleAdornerManager.Instance.DeRegister(arrowright_button);
            ActiveStyleAdornerManager.Instance.DeRegister(arrowup_button);
            ActiveStyleAdornerManager.Instance.DeRegister(arrowdown_button);
            ActiveStyleAdornerManager.Instance.DeRegister(orderlayout1_button);
            ActiveStyleAdornerManager.Instance.DeRegister(orderlayout2_button);
            ActiveStyleAdornerManager.Instance.DeRegister(orderlayout3_button);
            ActiveStyleAdornerManager.Instance.DeRegister(pin_button);
            ActiveStyleAdornerManager.Instance.DeRegister(newelem_button);
            ActiveStyleAdornerManager.Instance.DeRegister(custom_button);
            ActiveStyleAdornerManager.Instance.DeRegister(custom_button1);
            ActiveStyleAdornerManager.Instance.DeRegister(search_button);

            this.MouseLeave -= _animating_tile_panel_MouseLeave;
            this.MouseEnter -= _animating_tile_panel_MouseEnter;
            this.MouseMove -= _animating_tile_panel_MouseMove;
            this.SizeChanged -= SubMenuPanel_SizeChanged;

            this.Height = 0;

            if (Window != null)
                Window.SizeChanged -= Window_SizeChanged;
        }

        void Menu_Loaded(object sender, RoutedEventArgs e)
        {
            ActiveStyleAdornerManager.Instance.Register(arrowleft_button, BaseFrame.AdornerActiveRenderBrush, BaseFrame.AdornerActiveRenderPen);
            ActiveStyleAdornerManager.Instance.Register(arrowright_button, BaseFrame.AdornerActiveRenderBrush, BaseFrame.AdornerActiveRenderPen);
            ActiveStyleAdornerManager.Instance.Register(arrowup_button, BaseFrame.AdornerActiveRenderBrush, BaseFrame.AdornerActiveRenderPen);
            ActiveStyleAdornerManager.Instance.Register(arrowdown_button, BaseFrame.AdornerActiveRenderBrush, BaseFrame.AdornerActiveRenderPen);
            ActiveStyleAdornerManager.Instance.Register(orderlayout1_button, BaseFrame.AdornerActiveRenderBrush, BaseFrame.AdornerActiveRenderPen);
            ActiveStyleAdornerManager.Instance.Register(orderlayout2_button, BaseFrame.AdornerActiveRenderBrush, BaseFrame.AdornerActiveRenderPen);
            ActiveStyleAdornerManager.Instance.Register(orderlayout3_button, BaseFrame.AdornerActiveRenderBrush, BaseFrame.AdornerActiveRenderPen);
            ActiveStyleAdornerManager.Instance.Register(pin_button, BaseFrame.AdornerActiveRenderBrush, BaseFrame.AdornerActiveRenderPen);
            ActiveStyleAdornerManager.Instance.Register(newelem_button, BaseFrame.AdornerActiveRenderBrush, BaseFrame.AdornerActiveRenderPen);
            ActiveStyleAdornerManager.Instance.Register(custom_button, BaseFrame.AdornerActiveRenderBrush, BaseFrame.AdornerActiveRenderPen);
            ActiveStyleAdornerManager.Instance.Register(custom_button1, BaseFrame.AdornerActiveRenderBrush, BaseFrame.AdornerActiveRenderPen);
            ActiveStyleAdornerManager.Instance.Register(search_button, BaseFrame.AdornerActiveRenderBrush, BaseFrame.AdornerActiveRenderPen);

            _animating_tile_panel = UIHelper.FindVisualChildByType<AnimatingTilePanel>(animating_tile_panel_items_control);

            if (_animating_tile_panel == null && animating_tile_panel_items_control.IsLoaded == false)
            {
                animating_tile_panel_items_control.Loaded += new RoutedEventHandler(animating_tile_panel_items_control_Loaded);
                return;                
            }
            else if (_animating_tile_panel == null)
                return;

            InitControlProperties();
        }

        private void InitControlProperties()
        {
            if(_animating_tile_panel == null)
                _animating_tile_panel = UIHelper.FindVisualChildByType<AnimatingTilePanel>(animating_tile_panel_items_control);

            if (_animating_tile_panel == null)
                return;

            this.MouseLeave += new MouseEventHandler(_animating_tile_panel_MouseLeave);
            this.MouseEnter += new MouseEventHandler(_animating_tile_panel_MouseEnter);
            this.MouseMove += new MouseEventHandler(_animating_tile_panel_MouseMove);

            this.SizeChanged += new SizeChangedEventHandler(SubMenuPanel_SizeChanged);

            if (Window != null)
                Window.SizeChanged += Window_SizeChanged;

            this.Height = double.NaN;

            if (_map_zp != null)
            {
                StartFadeIn();
                return;
            }

            _map_zp = new MapZP(submenu_base_grid, submenu_base_canvas);

            _map_zp.DeRegisterEventHandler();
            _map_zp.WithBorderCheckWhileTranslationActive = false;
            _map_zp.WithBorderCheck = true;
            _map_zp.TranslationAnimationStarted += new EventHandler(_map_zp_TranslationAnimationStarted);
            _map_zp.TranslationAnimationCompleted += new EventHandler(_map_zp_TranslationAnimationCompleted);
            _map_zp.ZoomAnimationCompleted += new EventHandler(_map_zp_ZoomAnimationCompleted);

            _map_zp.BorderMaxZoom = 1;
            _map_zp.BorderMinZoom = 1;
            _map_zp.BorderCheckerHeight = PanelHeight - sub_header_stack.Height;
            _map_zp.BorderCheckerWidth = BigItemSize.Width * _items.Count;

            _scale_x_reset = new DoubleAnimation(1, new Duration(new TimeSpan(0, 0, 0, 0, 500))) { DecelerationRatio = 1 };
            _scale_y_reset = new DoubleAnimation(1, new Duration(new TimeSpan(0, 0, 0, 0, 500))) { DecelerationRatio = 1 };
            _transl_x_reset = new DoubleAnimation(0, new Duration(new TimeSpan(0, 0, 0, 0, 500))) { DecelerationRatio = 1 };
            _transl_y_reset = new DoubleAnimation(0, new Duration(new TimeSpan(0, 0, 0, 0, 500))) { DecelerationRatio = 1 };

            _scale_x_reset.Completed += new EventHandler(_scale_reset_x_Completed);
            _scale_y_reset.Completed += new EventHandler(_scale_reset_y_Completed);
            _transl_x_reset.Completed += new EventHandler(_transl_reset_x_Completed);
            _transl_y_reset.Completed += new EventHandler(_transl_reset_y_Completed);


            BigItemSize = BigItemSize;
            SmallItemSize = SmallItemSize;
            animating_tile_panel_items_control.DataContext = _items_filtered;
            submenu_items_datagrid.DataContext = _items_filtered;

            //_animating_tile_panel.SetBinding(AnimatingTilePanel.ItemHeightProperty, (string)null);
            //_animating_tile_panel.SetBinding(AnimatingTilePanel.ItemWidthProperty, (string)null);
            Binding height_binding = new Binding("ActualHeight");
            height_binding.Source = submenu_base_canvas;
            // Edit AlexO: Ich hab einen Konverter eingebunden, weil wenn die ItemHeightProperty auf 0 gesetzt wird, wirft das _animating_tile_panel eine BindingException
            // Statt null gibt der Konverter einen sehr kleinen Wert zurück
            height_binding.Converter = new DoubleNotNullConverter();
            if (_animating_tile_panel != null)
            {
                _animating_tile_panel.SetBinding(AnimatingTilePanel.ItemHeightProperty, height_binding);
                _animating_tile_panel.ItemWidth = BigItemSize.Width;
            }

            RegisterLayoutCheckedEvents();

            StartFadeIn();

            //if(BaseFrame != null)
            //    BaseFrame.KeyUp += new KeyEventHandler(BaseFrame_KeyUp);
        }

        
        void animating_tile_panel_items_control_Loaded(object sender, RoutedEventArgs e)
        {
            animating_tile_panel_items_control.Loaded -= animating_tile_panel_items_control_Loaded;
            InitControlProperties();
        }

        void SubMenuPanel_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            UpdateMapZPBorders();
        }

        void _map_zp_ZoomAnimationCompleted(object sender, EventArgs e)
        {
            UpdateMapZPBorders();
        }

        void BaseFrame_KeyUp(object sender, KeyEventArgs e)
        {
            //KeyStates state_m = Keyboard.GetKeyStates(Key.M);
            //KeyStates state_plus = Keyboard.GetKeyStates(Key.OemPlus);
            //KeyStates state_minus = Keyboard.GetKeyStates(Key.OemMinus);

            //KeyStates state_num_plus = Keyboard.GetKeyStates(Key.Add);          // num-block +
            //KeyStates state_num_minus = Keyboard.GetKeyStates(Key.Subtract);    // num-block -

            //bool m_down = (state_m & KeyStates.Down) > 0;
            //bool plus_down = (state_plus & KeyStates.Down) > 0 || (state_num_plus & KeyStates.Down) > 0;
            //bool minus_down = (state_minus & KeyStates.Down) > 0 || (state_num_minus & KeyStates.Down) > 0;

            //m_down = true;
            //plus_down = true;
            //minus_down = true;
            //if (!plus_down && !minus_down)
            //    return;

            //if (m_down)
            //{
            //    int i = 0;
            //    bool found_focused = false;
            //    for (i = 0; i < Items.Count; i++)
            //    {
            //        if (Items[i].IsFocused)
            //            found_focused = true;
            //    }

            //    if (!found_focused)
            //        i = 0;

            //    if (plus_down)
            //    {
            //        if (i < Items.Count - 1)
            //        {
            //            Items[i + 1].Focus();
            //        }
            //    }
            //    else if (minus_down)
            //    {
            //        if (i > 0)
            //            Items[i - 1].Focus();
            //    }
            //    else
            //        return;
            //}
            //else
            //{
            //    return;
            //}
        }

        void SubMenuPanel_KeyUp(object sender, KeyEventArgs e)
        {   
            if(e.Key == Key.Left && _active_layout_mode == LayoutMode.Line)
            {
                //KeyLeftClicked();
                //arrowleft_button_Click(arrowleft_button, null);
            }
            else if(e.Key == Key.Right && _active_layout_mode == LayoutMode.Line)
            {
                //KeyRightClicked();
                //arrowright_button_Click(arrowright_button, null);
            }
            else if (e.Key == Key.Down && _active_layout_mode != LayoutMode.Grid)
            {
                arrowdown_button_Click(arrowdown_button, null);
            }
            else if (e.Key == Key.Up && _active_layout_mode != LayoutMode.Grid)
            {
                arrowup_button_Click(arrowup_button, null);
            }

            e.Handled = true;
        }

        #region KeyNavigation
        private void KeyLeftClicked()
        {
            SubMenuPanelMenuItem selected_item = FindSelectedMenuItem();

            if (selected_item == null && _items_filtered.Count > 0)
                _items_filtered[_items_filtered.Count - 1].IsSelected = true;
            else if(selected_item != null)
            {
                int index_of_element = _items_filtered.IndexOf(selected_item);
                if(index_of_element != -1)
                {
                    selected_item.IsSelected = false;
                    if (index_of_element > 0)
                        //_items_filtered[index_of_element - 1].IsSelected = true;
                        _items_filtered[index_of_element - 1].Focus();
                    else
                        //_items_filtered[_items_filtered.Count - 1].IsSelected = true;
                        _items_filtered[_items_filtered.Count - 1].Focus();
                    
                }
            }

            MoveSelectedItemIntoView();
        }

        private void KeyRightClicked()
        {
            SubMenuPanelMenuItem selected_item = FindSelectedMenuItem();

            if(selected_item == null && _items_filtered.Count>0)
            {
                _items_filtered[0].IsSelected = true;
            }
            else if(selected_item != null)
            {
                int index_of_element = _items_filtered.IndexOf(selected_item);
                if(index_of_element != -1)
                {
                    if(_items_filtered.Count-1 > index_of_element)
                    {
                        selected_item.IsSelected = false;
                        //_items_filtered[index_of_element + 1].IsSelected = true;
                        _items_filtered[index_of_element + 1].Focus();
                    }
                    else
                    {
                        selected_item.IsSelected = false;
                        //_items_filtered[0].IsSelected = true;
                        _items_filtered[0].Focus();
                    }
                }
            }

            MoveSelectedItemIntoView();
        }

        private SubMenuPanelMenuItem FindSelectedMenuItem()
        {
            SubMenuPanelMenuItem selected_item = null;
            foreach (SubMenuPanelMenuItem subMenuPanelMenuItem in _items_filtered)
            {
                if (selected_item == null && (subMenuPanelMenuItem.IsSelected || subMenuPanelMenuItem.ManualIsMouseOver))
                {
                    selected_item = subMenuPanelMenuItem;
                    subMenuPanelMenuItem.ManualIsMouseOver = false;
                    continue;
                }

                subMenuPanelMenuItem.ManualIsMouseOver = false;
                subMenuPanelMenuItem.IsSelected = false;
            }

            return selected_item;
        }

        private void MoveSelectedItemIntoView()
        {
            MoveItemIntoView(FindSelectedMenuItem());
        }

        private void MoveItemIntoView(SubMenuPanelMenuItem item)
        {
            if(item == null)
                return;

            Point item_pos = UIHelper.GetElementPosition(item, this);
            
            float view_width = (float)submenu_base_grid.ActualWidth;

            if(item_pos.X > view_width)
            {
                _map_zp.TranslateTransform.X -= item_pos.X;
            }
            else if (item_pos.X < 0)
            {
                _map_zp.TranslateTransform.X += Math.Abs(item_pos.X);
            }

            item.InvalidateArrange();
        }
        #endregion

        void SubMenuPanel_KeyDown(object sender, KeyEventArgs e)
        {
            //KeyStates state_m = Keyboard.GetKeyStates(Key.M);
            //KeyStates state_plus = Keyboard.GetKeyStates(Key.OemPlus);
            //KeyStates state_minus = Keyboard.GetKeyStates(Key.OemMinus);

            //KeyStates state_num_plus = Keyboard.GetKeyStates(Key.Add);          // num-block +
            //KeyStates state_num_minus = Keyboard.GetKeyStates(Key.Subtract);    // num-block -

            //bool m_down = (state_m & KeyStates.Down) > 0;
            //bool plus_down = (state_plus & KeyStates.Down) > 0 || (state_num_plus & KeyStates.Down) > 0;
            //bool minus_down = (state_minus & KeyStates.Down) > 0 || (state_num_minus & KeyStates.Down) > 0;

            //if(!plus_down && !minus_down)
            //    return;

            //if (m_down)
            //{            
            //    int i = 0;
            //    for(i=0; i < Items.Count; i++)
            //    {
            //        if(Items[i].IsFocused)
            //            break;
            //    }

            //    if (plus_down)
            //    {
            //        if(i < Items.Count-1)
            //        {
            //            Items[i + 1].Focus();
            //        }
            //    }
            //    else if (minus_down)
            //    {
            //        if(i >0)
            //            Items[i - 1].Focus();
            //    }
            //    else
            //        return;
            //}
            //else
            //{
            //    return;
            //}
        }
        #endregion

        #region FadeIn
        public void StartFadeIn()
        {
            if(!IsLoaded)
                return;

            BaseFrame.DisableMouseClicks();
            _animation_active = true;
            _submenu_fadein_da = new DoubleAnimation(0, PanelHeight, new Duration(new TimeSpan(0, 0, 0, 0, 500)));
            _submenu_fadein_da.AccelerationRatio = 0.5;
            _submenu_fadein_da.DecelerationRatio = 0.5;
            _submenu_fadein_da.Completed += new EventHandler(_submenu_fadein_Completed);
            submenu_border.BeginAnimation(HeightProperty, _submenu_fadein_da, HandoffBehavior.Compose);

            CompositionTarget.Rendering += new EventHandler(CompositionTarget_Rendering);
            _map_zp.RegisterEventHandler();
        }
        #endregion

        public void StartFadeOut()
        {
            BaseFrame.DisableMouseClicks();
            _animation_active = true;
            _submenu_fadeout_da = new DoubleAnimation(0, new Duration(new TimeSpan(0, 0, 0, 0, 500)));
            _submenu_fadeout_da.AccelerationRatio = 0.5;
            _submenu_fadeout_da.DecelerationRatio = 0.5;
            _submenu_fadeout_da.Completed += new EventHandler(_submenu_fadeout_da_Completed);
            submenu_border.BeginAnimation(HeightProperty, _submenu_fadeout_da, HandoffBehavior.Compose);

            CompositionTarget.Rendering -= CompositionTarget_Rendering;
            if(_map_zp != null)
                _map_zp.DeRegisterEventHandler();
        }

     
        #region Add and Remove Items
        public void AddItem(SubMenuPanelMenuItem item)
        {
            if (item == null)
                throw new ArgumentNullException("item");

            if (item.Disposed)
                throw new ObjectDisposedException("item");

            FrameworkElement container = null;

            if(item.ItemTyp == SubMenuItemType.Button)
            {
                //Button b = new Button();
                TouchButton b = new TouchButton();
                FocusManager.SetFocusedElement(item, b);
                b.Style = (Style)FindResource("NavigationButton");
                UIElement old_child = item.Child;
                item.Child = null;
                b.Content = old_child;
                item.Child = b;              
                b.Click += new RoutedEventHandler(b_Click);
                b.Tag = item;
                container = b;
                item.InitChildStore();
            }
            else if(item.ItemTyp == SubMenuItemType.ToggleButton)
            {
                ToggleButton t = new ToggleButton();
                FocusManager.SetFocusedElement(item, t);                
                t.Style = (Style)FindResource("NavigationToggleButton");
                UIElement old_child = item.Child;
                item.Child = null;
                t.Content = old_child;
                item.Child = t;               
                t.Tag = item;
                t.Checked += new RoutedEventHandler(t_Checked);
                t.Unchecked += new RoutedEventHandler(t_Unchecked);
                container = t;
                item.InitChildStore();
            }

            item.CheckRenderSize();

            //to send here the container as an argument is a hack
            //grund, mit try find parent hab ich das teil nicht gefunden
            //auch nicht nach dem loaded event
            item.OnMenuItemInitialized(container);
            //item.IsHitTestVisible = false;
            //item.Focusable = false;

            item.IsHitTestVisible = false;
            item.BaseFrame = BaseFrame;
            item.SubMenuPanel = this;

            _items.Add(item);

            item.ActionClicked += new EventHandler<MenuItemActionEventArgs>(item_ActionClicked);
            item.GotKeyboardFocus += new KeyboardFocusChangedEventHandler(item_GotKeyboardFocus);
            item.LostKeyboardFocus += new KeyboardFocusChangedEventHandler(item_LostKeyboardFocus);
            //_raw_elems.Add(item);
            if (ItemCountFilterExpression == null)
            {
                ItemCount = ItemCount + 1;
            }
            else
            {
                // if a IteCountFilterExpression has been set
                // check if the current item 
                if(ItemCountFilterExpression(item))
                {
                    ItemCount = ItemCount + 1;
                }
            }

            item._disable_child_update = true;
            if (ShowActionsByLeftMouse)
                item.HideActionMenu = true;
            else
                item.HideActionMenu = false;
            item._disable_child_update = false;

            GenerateDataGridColumns();
            
            AdjustCustomFilters(item);

            if (!WithSearchField || string.IsNullOrEmpty(search_cbx.Text) || IsFiltered(item))
            {
                _items_filtered.Add(item);
                _items_filtered.Sort((x, y) => x.CompareTo(y));
            }

            if (ItemAdded != null)
                ItemAdded(this, new MenuItemManagementEventArgs(item));

            UpdateMapZPBorders();
        }
        
        public void RemoveItem(FrameworkElement item)
        {
            SubMenuPanelMenuItem m_item = _items.Where(i => i == item).SingleOrDefault();

            if(m_item == null)
                return;

            ResetItemState(ref m_item);

            m_item.OnMenuItemRemoving();

            _items.Remove(m_item);

            m_item.ActionClicked -= item_ActionClicked;
            m_item.GotKeyboardFocus -= item_GotKeyboardFocus;
            m_item.LostKeyboardFocus -= item_LostKeyboardFocus;

            if (ItemCountFilterExpression == null)
            {
                ItemCount = ItemCount - 1;
            }
            else
            {
                // if a IteCountFilterExpression has been set
                // check if the current item 
                if (ItemCountFilterExpression(m_item))
                {
                    ItemCount = ItemCount + 1;
                }
            }
            

            if (m_item.ItemTyp == SubMenuItemType.Button)
            {
                if (m_item.Child is Button)
                {
                    ((Button) m_item.Child).Click -= b_Click;
                    UIElement old_child = ((Button)m_item.Child).Content as UIElement;
                    BindingOperations.ClearAllBindings(((Button)m_item.Child));
                    ((Button)m_item.Child).Content = null;
                    m_item.Child = old_child;
                    m_item.ResetChildStore();
                }
            }
            else if (m_item.ItemTyp == SubMenuItemType.ToggleButton)
            {
                if (m_item.Child is ToggleButton)
                {
                    ((ToggleButton) m_item.Child).Checked -= t_Checked;
                    ((ToggleButton) m_item.Child).Unchecked -= t_Unchecked;

                    UIElement old_child = ((ToggleButton)m_item.Child).Content as UIElement;
                    BindingOperations.ClearAllBindings(((ToggleButton)m_item.Child));
                    ((ToggleButton)m_item.Child).Content = null;
                    m_item.Child = old_child;
                    m_item.ResetChildStore();

                    
                }
            }

            if(_items_filtered.Contains(m_item))
                _items_filtered.Remove(m_item);

            if (ItemRemoved != null)
                ItemRemoved(this, new MenuItemManagementEventArgs(m_item));

            //m_item.Dispose();
        }

        private void ResetItemState(ref SubMenuPanelMenuItem item)
        {
            item.ManualIsMouseOver = false;
            item.IsActive = false;
            item.IsSelected = false;
            item.HideActionMenu = false;
        }
        public void RemoveAllItemsOfCurrentBreadcrumbLayer()
        {
            _custom_filter_values.Clear();
            search_cbx.SelectedValue = null;
            search_cbx.ItemsSource = null;

            List<SubMenuPanelMenuItem> copy_list = new List<SubMenuPanelMenuItem>();
            copy_list.AddRange(_items);

            foreach (SubMenuPanelMenuItem rem_item in copy_list)
                RemoveItem(rem_item);

            ItemCount = 0;
        }

        public void RemoveAllItems()
        {
            _ignore_collection_change = true;
            RemoveAllItemsOfCurrentBreadcrumbLayer();

            List<SubMenuPanelMenuItem> copy_list = new List<SubMenuPanelMenuItem>();
            copy_list.AddRange(_root_items);

            foreach (SubMenuPanelMenuItem rem_item in copy_list)
            {
                rem_item.Dispose();
            }

            _root_items.Clear();
            CurrentBreadCrumbItem = _root_bread_crumb_item;
            UpdateBreadCrumbVisibility();
            _ignore_collection_change = false;
        }

        private bool IsFiltered(SubMenuPanelMenuItem item)
        {
            if (search_cbx.SelectedItem != null)
            {
                return item.ApplyCustomFilter(search_cbx.SelectedItem as string);
            }
            else
            {
                string[] split_filter = search_cbx.Text.Split(" ".ToCharArray());
                string lower_item_string = item.ToString().ToLower();
                foreach (string filter_i in split_filter)
                {
                    string lower_filter_i = filter_i.ToLower();
                    if (lower_item_string.IndexOf(lower_filter_i) >= 0)
                        return true;
                }
            }

            return false;
        }

        public void ResetHoverElementState()
        {
            _mouse_down = false;
            if (_last_hover_element != null)
                _last_hover_element.ManualIsMouseOver = false;
        }

        private void AdjustCustomFilters(SubMenuPanelMenuItem item)
        {
            List<string> item_custom_filter = item.GetCustomFilters();
            if (item_custom_filter != null && item_custom_filter.Count > 0)
            {
                _custom_filter_values = _custom_filter_values.Union(item.GetCustomFilters()).ToList();
                search_cbx.SelectedValue = null;
                search_cbx.ItemsSource = _custom_filter_values;
            }
        }
        #endregion

        #region GetElement
        public FrameworkElement GetItemByElement(FrameworkElement elem)
        {
            var query = from view in _items
                        where view == elem
                        select view;

            if (query.Count() == 1)
                return query.First();

            return null;
        }

        public SubMenuPanelMenuItem FindMenuItemByKey(string key)
        {
            SubMenuPanelMenuItem item = _items.Where(i => i.Key.Equals(key)).SingleOrDefault();
            return item;
        }
        #endregion


        void CompositionTarget_Rendering(object sender, EventArgs e)
        {
            _map_zp.UpdateMap();
        }


        #region Layout Switching
        private void SwitchToLine()
        {
            _autohide_timer_after_action = false;
            _ignore_autohide_after_action = true; // nach der animation wird nicht der auto-hide timer ausgelöst sollte sich die mouse ausserhalb befinden

            submenu_items_datagrid.Visibility = Visibility.Collapsed;
            submenu_base_grid.Visibility = Visibility.Visible;

            _animating_tile_panel.SetBinding(AnimatingTilePanel.ItemHeightProperty, (string)null);
            _animating_tile_panel.SetBinding(AnimatingTilePanel.ItemWidthProperty, (string)null);

            if (submenu_base_canvas.ActualHeight > 0)
            {
                DoubleAnimation upsize_itemheight_da = new DoubleAnimation(BigItemSize.Height, new Duration(new TimeSpan(0, 0, 0, 0, 500)));
                upsize_itemheight_da.DecelerationRatio = 1;
                _animating_tile_panel.BeginAnimation(AnimatingTilePanel.ItemHeightProperty, upsize_itemheight_da);

                DoubleAnimation upsize_itemwidth_da = new DoubleAnimation(BigItemSize.Width, new Duration(new TimeSpan(0, 0, 0, 0, 500)));
                upsize_itemwidth_da.DecelerationRatio = 1;
                _animating_tile_panel.BeginAnimation(AnimatingTilePanel.ItemWidthProperty, upsize_itemwidth_da);

                UpdateAndAnimatePanelToHeight(PanelHeight);

                DoubleAnimation rescale_da = new DoubleAnimation(SmallItemSize.Width, BigItemSize.Width, new Duration(new TimeSpan(0, 0, 0, 0, 500)));
                rescale_da.DecelerationRatio = 1;
                foreach (Viewbox view_box in _items)
                    view_box.BeginAnimation(WidthProperty, rescale_da);
            }
            else
            {
                _animating_tile_panel.SetBinding(AnimatingTilePanel.ItemHeightProperty, new Binding("ActualHeight") { Source = submenu_base_canvas });
                _animating_tile_panel.ItemWidth = BigItemSize.Width;
            }


            _animating_tile_panel.SetBinding(WidthProperty, (string)null);
            _animating_tile_panel.Width = BigItemSize.Width * _items.Count;

            UpdateLineMapZP();

            submenu_base_canvas.Height = double.NaN;

            ResetMapZp();
            EnbaleLeftRightArrows();
            UpdateMapZPBorders();
        }

        private void SwitchToKachel()
        {
            _autohide_timer_after_action = false;
            _ignore_autohide_after_action = true; // nach der animation wird nicht der auto-hide timer ausgelöst sollte sich die mouse ausserhalb befinden

            submenu_items_datagrid.Visibility = Visibility.Collapsed;
            submenu_base_grid.Visibility = Visibility.Visible;

            _animating_tile_panel.SetBinding(AnimatingTilePanel.ItemHeightProperty, (string)null);
            _animating_tile_panel.SetBinding(AnimatingTilePanel.ItemWidthProperty, (string)null);

            if (submenu_base_canvas.ActualHeight > 0)
            {
                DoubleAnimation downsize_itemheight_da = new DoubleAnimation(SmallItemSize.Height, new Duration(new TimeSpan(0, 0, 0, 0, 500)));
                downsize_itemheight_da.DecelerationRatio = 1;
                _animating_tile_panel.BeginAnimation(AnimatingTilePanel.ItemHeightProperty, downsize_itemheight_da);

                DoubleAnimation downsize_itemwidth_da = new DoubleAnimation(SmallItemSize.Width, new Duration(new TimeSpan(0, 0, 0, 0, 500)));
                downsize_itemwidth_da.DecelerationRatio = 1;
                _animating_tile_panel.BeginAnimation(AnimatingTilePanel.ItemWidthProperty, downsize_itemwidth_da);

                if(submenu_border.ActualHeight < PanelHeight)
                    UpdateAndAnimatePanelToHeight(PanelHeight);

                DoubleAnimation rescale_da = new DoubleAnimation(BigItemSize.Width, SmallItemSize.Width, new Duration(new TimeSpan(0, 0, 0, 0, 500)));
                rescale_da.DecelerationRatio = 1;
                foreach (Viewbox view_box in _items)
                    view_box.BeginAnimation(WidthProperty, rescale_da);
            }
            else
            {
                _animating_tile_panel.ItemHeight = SmallItemSize.Height * 3;
                _animating_tile_panel.ItemWidth = SmallItemSize.Width;
            }

            _animating_tile_panel.SetBinding(WidthProperty, new Binding("ActualWidth") { Source = submenu_base_canvas });


            int items_per_row = (int)Math.Floor(submenu_base_canvas.ActualWidth / SmallItemSize.Width);
            int rows = (int)Math.Ceiling((double)_items_filtered.Count / (double)items_per_row);
            double panel_canvas_height = (SmallItemSize.Height * rows);
            submenu_base_canvas.Height = panel_canvas_height;

            UpdateKachelMapZP();

            ResetMapZp();
            EnableUpDownArrows();
            UpdateMapZPBorders();
        }

        private void SwitchToGrid()
        {
            EnableUpDownArrows();
            // Grid disabled
            //return;

            submenu_items_datagrid.Visibility = Visibility.Visible;
            if (submenu_items_datagrid.Items.Count > 0)
            {
                if (submenu_items_datagrid.SelectedItems.Count == 0)
                    submenu_items_datagrid.SelectedIndex = 0;
            }
            submenu_base_grid.Visibility = Visibility.Collapsed;

            _autohide_timer_after_action = false;
            _ignore_autohide_after_action = true; // nach der animation wird nicht der auto-hide timer ausgelöst sollte sich die mouse ausserhalb befinden

            UpdateAndAnimatePanelToHeight(PanelHeight);
        }

        private void GenerateDataGridColumns()
        {
            if (_grid_columns_generated)
                return;

            if (_items != null && _items.Count > 0)
            {
                SubMenuPanelMenuItem newest_item = _items[_items.Count - 1];

                List<MenuItemAttribute> attribs = newest_item.GetAttributes();

                if(attribs != null && attribs.Count > 0)
                {
                    int nColCount = 0;
                    foreach (MenuItemAttribute cur_attribute in attribs)
                    {
                        DataGridTemplateColumn template_col = null;
                        //if(nColCount == 0)
                        //{
                        //    template_col = GenerateLinkTemplateColumn(cur_attribute);
                        //}
                        //else
                        //{
                        //    template_col = GenerateTextTemplateColumn(cur_attribute);    
                        //}

                        if(cur_attribute.Type == MenuItemAttributeType.LinkButton)
                        {
                            template_col = GenerateLinkTemplateColumn(cur_attribute);
                        }
                        else if (cur_attribute.Type == MenuItemAttributeType.Button)
                        {
                            template_col = GenerateButtonTemplateColumn(cur_attribute);
                        }
                        else if (cur_attribute.Type == MenuItemAttributeType.ToggleButton)
                        {
                            template_col = GenerateToggleButtonTemplateColumn(cur_attribute);
                        }
                        else if(cur_attribute.Type == MenuItemAttributeType.Label)
                        {
                            template_col = GenerateTextTemplateColumn(cur_attribute);    
                        }
                        else if (cur_attribute.Type == MenuItemAttributeType.CheckBox)
                        {
                            template_col = GenerateCheckBoxTemplateColumn(cur_attribute);
                        }
                        else if (cur_attribute.Type == MenuItemAttributeType.TextBox)
                        {
                            template_col = GenerateTextBoxTemplateColumn(cur_attribute);
                        }
                        else if (cur_attribute.Type == MenuItemAttributeType.Image)
                        {
                            template_col = GenerateImageTemplateColumn(cur_attribute);
                        }
                        else if (cur_attribute.Type == MenuItemAttributeType.Icon)
                        {
                            template_col = GenerateIconTemplateColumn(cur_attribute);
                        }

                        
                        if (template_col != null)
                            submenu_items_datagrid.Columns.Add(template_col);

                        nColCount++;
                    }

                    _grid_columns_generated = true;

                    GenerateActionsColumns(newest_item);
                    //submenu_items_datagrid.Columns.Add(actions_col);
                    //submenu_items_datagrid.Items.Refresh();
                }
            }
        }

        private DataGridTemplateColumn GenerateLinkTemplateColumn(MenuItemAttribute attribute)
        {
            DataGridTemplateColumn template_col = new DataGridTemplateColumn();
            template_col.IsReadOnly = true;
            template_col.Header = attribute.Name;
            template_col.Header = attribute.ShowHeader ? attribute.Name : "";

            if (!double.IsNaN(attribute.MaxWidth))
                template_col.MaxWidth = attribute.MaxWidth;
            if (!double.IsNaN(attribute.MinWidth))
                template_col.MinWidth = attribute.MinWidth;
            if (!double.IsNaN(attribute.Width))
                template_col.Width = attribute.Width;
            //template_col.SortMemberPath = attribute_name;

            if(attribute.HorizontalAlignment == System.Windows.HorizontalAlignment.Right)
            {
                template_col.HeaderStyle = GetRightAlignColumnHeaderStyle();
            }
            else if(attribute.HorizontalAlignment == System.Windows.HorizontalAlignment.Center)
            {
                template_col.HeaderStyle = GetCenterAlignColumnHeaderStyle();
            }
            else
            {
                template_col.HeaderStyle = GetLeftAlignColumnHeaderStyle();
            }

            MultiBinding tb_content_binding = new MultiBinding();
            tb_content_binding.Converter = new SubMenuPanelGridValueConverter();
            tb_content_binding.ConverterParameter = attribute;
            tb_content_binding.Bindings.Add(new Binding());
            tb_content_binding.Bindings.Add(new Binding("TriggerGridUpdate"));

            Binding command_binding = new Binding();

            FrameworkElementFactory elementFactory = new FrameworkElementFactory(typeof(PanelGridButton));

            elementFactory.SetValue(Button.NameProperty, "standard_action_button");
            elementFactory.SetValue(Button.StyleProperty, (Style)FindResource("dg_link_button_style"));
            elementFactory.SetValue(Button.MarginProperty, new Thickness(0, 2, 0, 2));
            elementFactory.SetValue(Button.PaddingProperty, new Thickness(0));
            elementFactory.SetValue(Button.HorizontalContentAlignmentProperty, attribute.HorizontalAlignment);
            elementFactory.SetValue(Button.HorizontalAlignmentProperty, attribute.HorizontalAlignment);
            elementFactory.SetValue(TextElement.FontWeightProperty, FontWeights.Bold);

            elementFactory.SetValue(TagProperty, attribute);

            MultiBinding btn_enabled_binding = new MultiBinding();
            btn_enabled_binding.Converter = new SubMenuPanelGridActionEnabledConverter();
            btn_enabled_binding.ConverterParameter = attribute;
            btn_enabled_binding.Bindings.Add(new Binding());
            btn_enabled_binding.Bindings.Add(new Binding("TriggerGridUpdate"));

            MultiBinding btn_visibility_binding = new MultiBinding();
            btn_visibility_binding.Converter = new SubMenuPanelGridActionVisibilityConverter();
            btn_visibility_binding.ConverterParameter = attribute;
            btn_visibility_binding.Bindings.Add(new Binding());
            btn_visibility_binding.Bindings.Add(new Binding("TriggerGridUpdate"));

            elementFactory.SetBinding(IsEnabledProperty, btn_enabled_binding);
            elementFactory.SetBinding(VisibilityProperty, btn_visibility_binding);


            elementFactory.AddHandler(Button.ClickEvent, new RoutedEventHandler(datagrid_button_column_click));
            elementFactory.AddHandler(PanelGridButton.InternalKeyUpEvent, new KeyEventHandler(grid_button_KeyUp));
            elementFactory.AddHandler(PanelGridButton.InternalKeyDownEvent, new KeyEventHandler(grid_button_KeyDown));
            
            elementFactory.SetBinding(Button.ContentProperty, tb_content_binding);
            elementFactory.SetBinding(Button.CommandParameterProperty, command_binding);

            DataTemplate data_template = new DataTemplate();
            data_template.VisualTree = elementFactory;

            template_col.CellTemplate = data_template;

            return template_col;
        }

        private DataGridTemplateColumn GenerateButtonTemplateColumn(MenuItemAttribute attribute)
        {
            DataGridTemplateColumn template_col = new DataGridTemplateColumn();
            template_col.IsReadOnly = true;
            template_col.Header = attribute.Name;
            template_col.Header = attribute.ShowHeader ? attribute.Name : "";

            //template_col.SortMemberPath = attribute_name;

            if (attribute.HorizontalAlignment == System.Windows.HorizontalAlignment.Right)
            {
                template_col.HeaderStyle = GetRightAlignColumnHeaderStyle();
            }
            else if (attribute.HorizontalAlignment == System.Windows.HorizontalAlignment.Center)
            {
                template_col.HeaderStyle = GetCenterAlignColumnHeaderStyle();
            }
            else
            {
                template_col.HeaderStyle = GetLeftAlignColumnHeaderStyle();
            }

            MultiBinding tb_content_binding = new MultiBinding();
            tb_content_binding.Converter = new SubMenuPanelGridValueConverter();
            tb_content_binding.ConverterParameter = attribute;
            tb_content_binding.Bindings.Add(new Binding());
            tb_content_binding.Bindings.Add(new Binding("TriggerGridUpdate"));

            Binding command_binding = new Binding();

            FrameworkElementFactory elementFactory = new FrameworkElementFactory(typeof(PanelGridButton));

            elementFactory.SetValue(Button.NameProperty, "standard_action_button");
            elementFactory.SetValue(Button.StyleProperty, null);
            elementFactory.SetValue(Button.MarginProperty, new Thickness(2));
            elementFactory.SetValue(Button.PaddingProperty, new Thickness(0));
            elementFactory.SetValue(Button.HorizontalContentAlignmentProperty, attribute.HorizontalAlignment);
            elementFactory.SetValue(Button.HorizontalAlignmentProperty, attribute.HorizontalAlignment);
            //elementFactory.SetValue(TextElement.FontWeightProperty, FontWeights.Bold);

            elementFactory.SetValue(TagProperty, attribute);

            MultiBinding btn_enabled_binding = new MultiBinding();
            btn_enabled_binding.Converter = new SubMenuPanelGridActionEnabledConverter();
            btn_enabled_binding.ConverterParameter = attribute;
            btn_enabled_binding.Bindings.Add(new Binding());
            btn_enabled_binding.Bindings.Add(new Binding("TriggerGridUpdate"));

            MultiBinding btn_visibility_binding = new MultiBinding();
            btn_visibility_binding.Converter = new SubMenuPanelGridActionVisibilityConverter();
            btn_visibility_binding.ConverterParameter = attribute;
            btn_visibility_binding.Bindings.Add(new Binding());
            btn_visibility_binding.Bindings.Add(new Binding("TriggerGridUpdate"));

            elementFactory.SetBinding(IsEnabledProperty, btn_enabled_binding);
            elementFactory.SetBinding(VisibilityProperty, btn_visibility_binding);


            elementFactory.AddHandler(Button.ClickEvent, new RoutedEventHandler(datagrid_button_column_click));
            elementFactory.AddHandler(PanelGridButton.InternalKeyUpEvent, new KeyEventHandler(grid_button_KeyUp));
            elementFactory.AddHandler(PanelGridButton.InternalKeyDownEvent, new KeyEventHandler(grid_button_KeyDown));

            elementFactory.SetBinding(Button.ContentProperty, tb_content_binding);
            elementFactory.SetBinding(Button.CommandParameterProperty, command_binding);

            DataTemplate data_template = new DataTemplate();
            data_template.VisualTree = elementFactory;

            template_col.CellTemplate = data_template;

            return template_col;
        }

        private DataGridTemplateColumn GenerateToggleButtonTemplateColumn(MenuItemAttribute attribute)
        {
            DataGridTemplateColumn template_col = new DataGridTemplateColumn();
            template_col.IsReadOnly = true;
            template_col.Header = attribute.Name;
            template_col.Header = attribute.ShowHeader ? attribute.Name : "";

            //template_col.SortMemberPath = attribute_name;

            if (attribute.HorizontalAlignment == System.Windows.HorizontalAlignment.Right)
            {
                template_col.HeaderStyle = GetRightAlignColumnHeaderStyle();
            }
            else if (attribute.HorizontalAlignment == System.Windows.HorizontalAlignment.Center)
            {
                template_col.HeaderStyle = GetCenterAlignColumnHeaderStyle();
            }
            else
            {
                template_col.HeaderStyle = GetLeftAlignColumnHeaderStyle();
            }

            MultiBinding tb_content_binding = new MultiBinding();
            tb_content_binding.Converter = new SubMenuPanelGridValueConverter();
            tb_content_binding.ConverterParameter = attribute;
            tb_content_binding.Bindings.Add(new Binding());
            tb_content_binding.Bindings.Add(new Binding("TriggerGridUpdate"));

            Binding command_binding = new Binding();

            FrameworkElementFactory elementFactory = new FrameworkElementFactory(typeof(ToggleButton));

            elementFactory.SetValue(NameProperty, "toggle_action_button");
            elementFactory.SetValue(Button.StyleProperty, null);
            elementFactory.SetValue(MarginProperty, new Thickness(2));
            elementFactory.SetValue(PaddingProperty, new Thickness(0));
            elementFactory.SetValue(Button.HorizontalContentAlignmentProperty, attribute.HorizontalAlignment);
            elementFactory.SetValue(Button.HorizontalAlignmentProperty, attribute.HorizontalAlignment);

            elementFactory.SetValue(TagProperty, attribute);

            MultiBinding btn_enabled_binding = new MultiBinding();
            btn_enabled_binding.Converter = new SubMenuPanelGridActionEnabledConverter();
            btn_enabled_binding.ConverterParameter = attribute;
            btn_enabled_binding.Bindings.Add(new Binding());
            btn_enabled_binding.Bindings.Add(new Binding("TriggerGridUpdate"));

            MultiBinding btn_visibility_binding = new MultiBinding();
            btn_visibility_binding.Converter = new SubMenuPanelGridActionVisibilityConverter();
            btn_visibility_binding.ConverterParameter = attribute;
            btn_visibility_binding.Bindings.Add(new Binding());
            btn_visibility_binding.Bindings.Add(new Binding("TriggerGridUpdate"));

            elementFactory.SetBinding(IsEnabledProperty, btn_enabled_binding);
            elementFactory.SetBinding(VisibilityProperty, btn_visibility_binding);

            if (attribute.ValueBinding == null)
            {
                elementFactory.AddHandler(ToggleButton.ClickEvent, new RoutedEventHandler(datagrid_button_column_click));
            }
            else
            {
                elementFactory.SetBinding(ToggleButton.IsCheckedProperty, attribute.ValueBinding);
            }

            //elementFactory.AddHandler(PanelGridButton.InternalKeyUpEvent, new KeyEventHandler(grid_button_KeyUp));
            //elementFactory.AddHandler(PanelGridButton.InternalKeyDownEvent, new KeyEventHandler(grid_button_KeyDown));

            elementFactory.SetBinding(ContentProperty, tb_content_binding);
            elementFactory.SetBinding(ToggleButton.CommandParameterProperty, command_binding);

            DataTemplate data_template = new DataTemplate();
            data_template.VisualTree = elementFactory;

            template_col.CellTemplate = data_template;

            return template_col;
        }

        private DataGridTemplateColumn GenerateCheckBoxTemplateColumn(MenuItemAttribute attribute)
        {
            DataGridTemplateColumn template_col = new DataGridTemplateColumn();
            template_col.IsReadOnly = true;
            template_col.Header = attribute.Name;
            template_col.Header = attribute.ShowHeader ? attribute.Name : "";

            //template_col.SortMemberPath = attribute_name;

            if (attribute.HorizontalAlignment == System.Windows.HorizontalAlignment.Right)
            {
                template_col.HeaderStyle = GetRightAlignColumnHeaderStyle();
            }
            else if (attribute.HorizontalAlignment == System.Windows.HorizontalAlignment.Center)
            {
                template_col.HeaderStyle = GetCenterAlignColumnHeaderStyle();
            }
            else
            {
                template_col.HeaderStyle = GetLeftAlignColumnHeaderStyle();
            }

            //Binding tb_content_binding = new Binding();
            //tb_content_binding.Converter = new SubMenuPanelGridValueConverter();
            //tb_content_binding.ConverterParameter = attribute;

            Binding command_binding = new Binding();

            FrameworkElementFactory elementFactory = new FrameworkElementFactory(typeof(CheckBox));

            elementFactory.SetValue(NameProperty, "checkbox_action_button");
            elementFactory.SetValue(MarginProperty, new Thickness(2));
            elementFactory.SetValue(PaddingProperty, new Thickness(0));
            elementFactory.SetValue(HorizontalContentAlignmentProperty, attribute.HorizontalAlignment);
            elementFactory.SetValue(HorizontalAlignmentProperty, attribute.HorizontalAlignment);

            elementFactory.SetValue(TagProperty, attribute);

            MultiBinding btn_enabled_binding = new MultiBinding();
            btn_enabled_binding.Converter = new SubMenuPanelGridActionEnabledConverter();
            btn_enabled_binding.ConverterParameter = attribute;
            btn_enabled_binding.Bindings.Add(new Binding());
            btn_enabled_binding.Bindings.Add(new Binding("TriggerGridUpdate"));

            MultiBinding btn_visibility_binding = new MultiBinding();
            btn_visibility_binding.Converter = new SubMenuPanelGridActionVisibilityConverter();
            btn_visibility_binding.ConverterParameter = attribute;
            btn_visibility_binding.Bindings.Add(new Binding());
            btn_visibility_binding.Bindings.Add(new Binding("TriggerGridUpdate"));

            elementFactory.SetBinding(IsEnabledProperty, btn_enabled_binding);
            elementFactory.SetBinding(VisibilityProperty, btn_visibility_binding);

            if (attribute.ValueBinding == null)
            {
                elementFactory.AddHandler(CheckBox.ClickEvent, new RoutedEventHandler(datagrid_button_column_click));
            }
            else
            {
                elementFactory.SetBinding(CheckBox.IsCheckedProperty, attribute.ValueBinding);
            }

            //elementFactory.AddHandler(PanelGridButton.InternalKeyUpEvent, new KeyEventHandler(grid_button_KeyUp));
            //elementFactory.AddHandler(PanelGridButton.InternalKeyDownEvent, new KeyEventHandler(grid_button_KeyDown));

            //elementFactory.SetBinding(ContentProperty, tb_content_binding);
            elementFactory.SetBinding(CheckBox.CommandParameterProperty, command_binding);

            DataTemplate data_template = new DataTemplate();
            data_template.VisualTree = elementFactory;

            template_col.CellTemplate = data_template;

            return template_col;
        }
        private DataGridTemplateColumn GenerateTextTemplateColumn(MenuItemAttribute attribute)
        {
            DataGridTemplateColumn template_col = new DataGridTemplateColumn();
            template_col.IsReadOnly = true;
            template_col.Header = attribute.ShowHeader ? attribute.Name : "";

            if (!double.IsNaN(attribute.MaxWidth))
                template_col.MaxWidth = attribute.MaxWidth;
            if (!double.IsNaN(attribute.MinWidth))
                template_col.MinWidth = attribute.MinWidth;
            if (!double.IsNaN(attribute.Width))
                template_col.Width = attribute.Width;
            //template_col.SortMemberPath = attribute_name;

            if (attribute.HorizontalAlignment == System.Windows.HorizontalAlignment.Right)
            {
                template_col.HeaderStyle = GetRightAlignColumnHeaderStyle();
            }
            else if (attribute.HorizontalAlignment == System.Windows.HorizontalAlignment.Center)
            {
                template_col.HeaderStyle = GetCenterAlignColumnHeaderStyle();
            }
            else
            {
                template_col.HeaderStyle = GetLeftAlignColumnHeaderStyle();
            }

            MultiBinding tb_text_binding = new MultiBinding();
            tb_text_binding.Converter = new SubMenuPanelGridValueConverter();
            tb_text_binding.ConverterParameter = attribute;
            tb_text_binding.Bindings.Add(new Binding());
            tb_text_binding.Bindings.Add(new Binding("TriggerGridUpdate"));

            FrameworkElementFactory elementFactory = new FrameworkElementFactory(typeof (TextBlock));
            elementFactory.SetValue(TextBlock.MarginProperty, new Thickness(2, 2, 15, 2));
            if (attribute.HorizontalAlignment == System.Windows.HorizontalAlignment.Right)
            {
                elementFactory.SetValue(TextBlock.TextAlignmentProperty, TextAlignment.Right);
            }
            else if (attribute.HorizontalAlignment == System.Windows.HorizontalAlignment.Center)
            {
                elementFactory.SetValue(TextBlock.TextAlignmentProperty, TextAlignment.Center);
            }
            else
            {
                elementFactory.SetValue(TextBlock.TextAlignmentProperty, TextAlignment.Left);
            }
            
            elementFactory.SetValue(HorizontalAlignmentProperty, HorizontalAlignment.Stretch);

            if (attribute.ValueBinding == null)
            {
                elementFactory.SetBinding(TextBlock.TextProperty, tb_text_binding);
            }
            else
            {
                elementFactory.SetBinding(TextBlock.TextProperty, attribute.ValueBinding);
            }
            

            MultiBinding btn_enabled_binding = new MultiBinding();
            btn_enabled_binding.Converter = new SubMenuPanelGridActionEnabledConverter();
            btn_enabled_binding.ConverterParameter = attribute;
            btn_enabled_binding.Bindings.Add(new Binding());
            btn_enabled_binding.Bindings.Add(new Binding("TriggerGridUpdate"));

            MultiBinding btn_visibility_binding = new MultiBinding();
            btn_visibility_binding.Converter = new SubMenuPanelGridActionVisibilityConverter();
            btn_visibility_binding.ConverterParameter = attribute;
            btn_visibility_binding.Bindings.Add(new Binding());
            btn_visibility_binding.Bindings.Add(new Binding("TriggerGridUpdate"));

            elementFactory.SetBinding(IsEnabledProperty, btn_enabled_binding);
            elementFactory.SetBinding(VisibilityProperty, btn_visibility_binding);

            DataTemplate data_template = new DataTemplate();
            data_template.VisualTree = elementFactory;

            template_col.CellTemplate = data_template;

            return template_col;
        }

        private DataGridTemplateColumn GenerateImageTemplateColumn(MenuItemAttribute attribute)
        {
            DataGridTemplateColumn template_col = new DataGridTemplateColumn();
            template_col.IsReadOnly = true;
            template_col.Header = attribute.Name;
            template_col.Header = attribute.ShowHeader ? attribute.Name : "";

            //template_col.SortMemberPath = attribute_name;

            if (attribute.HorizontalAlignment == System.Windows.HorizontalAlignment.Right)
            {
                template_col.HeaderStyle = GetRightAlignColumnHeaderStyle();
            }
            else if (attribute.HorizontalAlignment == System.Windows.HorizontalAlignment.Center)
            {
                template_col.HeaderStyle = GetCenterAlignColumnHeaderStyle();
            }
            else
            {
                template_col.HeaderStyle = GetLeftAlignColumnHeaderStyle();
            }

            MultiBinding tb_content_binding = new MultiBinding();
            tb_content_binding.Converter = new SubMenuPanelGridValueConverter();
            tb_content_binding.ConverterParameter = attribute;
            tb_content_binding.Bindings.Add(new Binding());
            tb_content_binding.Bindings.Add(new Binding("TriggerGridUpdate"));

            Binding command_binding = new Binding();

            FrameworkElementFactory elementFactory = new FrameworkElementFactory(typeof(Image));

            elementFactory.SetValue(Image.NameProperty, "standard_image");
            elementFactory.SetValue(Image.StyleProperty, null);
            elementFactory.SetValue(Image.MarginProperty, new Thickness(2));
            elementFactory.SetValue(Image.HorizontalAlignmentProperty, attribute.HorizontalAlignment);
            //elementFactory.SetValue(TextElement.FontWeightProperty, FontWeights.Bold);

            elementFactory.SetValue(TagProperty, attribute);

            MultiBinding btn_enabled_binding = new MultiBinding();
            btn_enabled_binding.Converter = new SubMenuPanelGridActionEnabledConverter();
            btn_enabled_binding.ConverterParameter = attribute;
            btn_enabled_binding.Bindings.Add(new Binding());
            btn_enabled_binding.Bindings.Add(new Binding("TriggerGridUpdate"));

            MultiBinding btn_visibility_binding = new MultiBinding();
            btn_visibility_binding.Converter = new SubMenuPanelGridActionVisibilityConverter();
            btn_visibility_binding.ConverterParameter = attribute;
            btn_visibility_binding.Bindings.Add(new Binding());
            btn_visibility_binding.Bindings.Add(new Binding("TriggerGridUpdate"));

            elementFactory.SetBinding(IsEnabledProperty, btn_enabled_binding);
            elementFactory.SetBinding(VisibilityProperty, btn_visibility_binding);

            if (attribute.ValueBinding == null)
            {
                elementFactory.SetBinding(Image.SourceProperty, tb_content_binding);
            }
            else
            {
                elementFactory.SetBinding(Image.SourceProperty, attribute.ValueBinding);
            }

            DataTemplate data_template = new DataTemplate();
            data_template.VisualTree = elementFactory;

            template_col.CellTemplate = data_template;

            return template_col;
        }

        private DataGridTemplateColumn GenerateIconTemplateColumn(MenuItemAttribute attribute)
        {
            DataGridTemplateColumn template_col = new DataGridTemplateColumn();
            template_col.IsReadOnly = true;
            template_col.Header = attribute.Name;
            template_col.Header = attribute.ShowHeader ? attribute.Name : "";

            //template_col.SortMemberPath = attribute_name;

            if (attribute.HorizontalAlignment == System.Windows.HorizontalAlignment.Right)
            {
                template_col.HeaderStyle = GetRightAlignColumnHeaderStyle();
            }
            else if (attribute.HorizontalAlignment == System.Windows.HorizontalAlignment.Center)
            {
                template_col.HeaderStyle = GetCenterAlignColumnHeaderStyle();
            }
            else
            {
                template_col.HeaderStyle = GetLeftAlignColumnHeaderStyle();
            }

            MultiBinding tb_content_binding = new MultiBinding();
            tb_content_binding.Converter = new SubMenuPanelGridValueConverter();
            tb_content_binding.ConverterParameter = attribute;
            tb_content_binding.Bindings.Add(new Binding());
            tb_content_binding.Bindings.Add(new Binding("TriggerGridUpdate"));

            Binding command_binding = new Binding();

            FrameworkElementFactory elementFactory = new FrameworkElementFactory(typeof(Image));

            elementFactory.SetValue(Image.NameProperty, "standard_image");
            elementFactory.SetValue(Image.StyleProperty, null);
            elementFactory.SetValue(Image.MarginProperty, new Thickness(2));
            elementFactory.SetValue(Image.HorizontalAlignmentProperty, attribute.HorizontalAlignment);
            elementFactory.SetValue(Image.StretchProperty, Stretch.UniformToFill);
            elementFactory.SetValue(Image.WidthProperty, (double)16);
            elementFactory.SetValue(Image.HeightProperty, (double)16);
            //elementFactory.SetValue(TextElement.FontWeightProperty, FontWeights.Bold);

            elementFactory.SetValue(TagProperty, attribute);

            MultiBinding btn_enabled_binding = new MultiBinding();
            btn_enabled_binding.Converter = new SubMenuPanelGridActionEnabledConverter();
            btn_enabled_binding.ConverterParameter = attribute;
            btn_enabled_binding.Bindings.Add(new Binding());
            btn_enabled_binding.Bindings.Add(new Binding("TriggerGridUpdate"));

            MultiBinding btn_visibility_binding = new MultiBinding();
            btn_visibility_binding.Converter = new SubMenuPanelGridActionVisibilityConverter();
            btn_visibility_binding.ConverterParameter = attribute;
            btn_visibility_binding.Bindings.Add(new Binding());
            btn_visibility_binding.Bindings.Add(new Binding("TriggerGridUpdate"));

            elementFactory.SetBinding(IsEnabledProperty, btn_enabled_binding);
            elementFactory.SetBinding(VisibilityProperty, btn_visibility_binding);

            if (attribute.ValueBinding == null)
            {
                elementFactory.SetBinding(Image.SourceProperty, tb_content_binding);
            }
            else
            {
                elementFactory.SetBinding(Image.SourceProperty, attribute.ValueBinding);
            }

            DataTemplate data_template = new DataTemplate();
            data_template.VisualTree = elementFactory;

            template_col.CellTemplate = data_template;

            return template_col;
        }

        private DataGridTemplateColumn GenerateTextBoxTemplateColumn(MenuItemAttribute attribute)
        {
            DataGridTemplateColumn template_col = new DataGridTemplateColumn();
            template_col.IsReadOnly = true;
            template_col.Header = attribute.ShowHeader ? attribute.Name : "";
            if (!double.IsNaN(attribute.MaxWidth))
                template_col.MaxWidth = attribute.MaxWidth;
            if (!double.IsNaN(attribute.MinWidth))
                template_col.MinWidth = attribute.MinWidth;
            if (!double.IsNaN(attribute.Width))
                template_col.Width = attribute.Width;
            //template_col.SortMemberPath = attribute_name;

            if (attribute.HorizontalAlignment == System.Windows.HorizontalAlignment.Right)
            {
                template_col.HeaderStyle = GetRightAlignColumnHeaderStyle();
            }
            else if (attribute.HorizontalAlignment == System.Windows.HorizontalAlignment.Center)
            {
                template_col.HeaderStyle = GetCenterAlignColumnHeaderStyle();
            }
            else
            {
                template_col.HeaderStyle = GetLeftAlignColumnHeaderStyle();
            }


            MultiBinding tb_text_binding = new MultiBinding();
            tb_text_binding.Converter = new SubMenuPanelGridValueConverter();
            tb_text_binding.ConverterParameter = attribute;
            tb_text_binding.Bindings.Add(new Binding());
            tb_text_binding.Bindings.Add(new Binding("TriggerGridUpdate"));

            FrameworkElementFactory elementFactory = new FrameworkElementFactory(typeof(TextBox));

            elementFactory.SetValue(NameProperty, "textbox_action_button");
            elementFactory.SetValue(MarginProperty, new Thickness(2));
            elementFactory.SetValue(PaddingProperty, new Thickness(0));
            if (attribute.HorizontalAlignment == System.Windows.HorizontalAlignment.Right)
            {
                elementFactory.SetValue(TextBox.TextAlignmentProperty, TextAlignment.Right);
            }
            else if (attribute.HorizontalAlignment == System.Windows.HorizontalAlignment.Center)
            {
                elementFactory.SetValue(TextBox.TextAlignmentProperty, TextAlignment.Center);
            }
            else
            {
                elementFactory.SetValue(TextBox.TextAlignmentProperty, TextAlignment.Left);
            }
            elementFactory.SetValue(HorizontalAlignmentProperty, HorizontalAlignment.Stretch);

            elementFactory.SetValue(TagProperty, attribute);

            MultiBinding btn_enabled_binding = new MultiBinding();
            btn_enabled_binding.Converter = new SubMenuPanelGridActionEnabledConverter();
            btn_enabled_binding.ConverterParameter = attribute;
            btn_enabled_binding.Bindings.Add(new Binding());
            btn_enabled_binding.Bindings.Add(new Binding("TriggerGridUpdate"));

            MultiBinding btn_visibility_binding = new MultiBinding();
            btn_visibility_binding.Converter = new SubMenuPanelGridActionVisibilityConverter();
            btn_visibility_binding.ConverterParameter = attribute;
            btn_visibility_binding.Bindings.Add(new Binding());
            btn_visibility_binding.Bindings.Add(new Binding("TriggerGridUpdate"));

            elementFactory.SetBinding(IsEnabledProperty, btn_enabled_binding);
            elementFactory.SetBinding(VisibilityProperty, btn_visibility_binding);

            if (attribute.ValueBinding == null)
            {
                elementFactory.SetBinding(TextBox.TextProperty, tb_text_binding);
            }
            else
            {
                elementFactory.SetBinding(TextBox.TextProperty, attribute.ValueBinding);
            }

            //elementFactory.AddHandler(PanelGridButton.InternalKeyUpEvent, new KeyEventHandler(grid_button_KeyUp));
            //elementFactory.AddHandler(PanelGridButton.InternalKeyDownEvent, new KeyEventHandler(grid_button_KeyDown));

            DataTemplate data_template = new DataTemplate();
            data_template.VisualTree = elementFactory;

            template_col.CellTemplate = data_template;

            return template_col;
        }

        private void GenerateActionsColumns(SubMenuPanelMenuItem item)
        {
            List<MenuItemAction> actions = item.GetActions();

            if (actions != null && actions.Count > 0)
            {

                RelativeSource relParentSource = new RelativeSource(RelativeSourceMode.FindAncestor, typeof(DataGridRow), 1);
                int action_count = 0;
                foreach (MenuItemAction action in actions)
                {
                    DataGridTemplateColumn template_col = new DataGridTemplateColumn();
                    template_col.IsReadOnly = true;
                    template_col.Header = null;

                    Binding command_binding = new Binding();

                    
                    FrameworkElementFactory actionButton = new FrameworkElementFactory(typeof(PanelGridButton));

                    FrameworkElement actionIcon = action.IconGrid; // item.GetActionIcon(action);

                    MultiBinding btn_icon_binding = new MultiBinding();
                    btn_icon_binding.Converter = new SubMenuPanelGridActionIconConverter();
                    btn_icon_binding.ConverterParameter = action;
                    btn_icon_binding.Bindings.Add(new Binding());
                    btn_icon_binding.Bindings.Add(new Binding("TriggerGridUpdate"));

                    actionButton.SetBinding(ContentProperty, btn_icon_binding);

                    string button_name = "action_" + action_count.ToString();

                    
                    actionButton.SetValue(TagProperty, action);


                    actionButton.SetValue(NameProperty, button_name);
                    actionButton.SetBinding(Button.CommandParameterProperty, command_binding);
                    actionButton.SetValue(MarginProperty, new Thickness(0));
                    actionButton.SetValue(ToolTipProperty, action.Name);
                    actionButton.AddHandler(Button.ClickEvent, new RoutedEventHandler(datagrid_action_click));
                    actionButton.AddHandler(PanelGridButton.InternalKeyUpEvent, new KeyEventHandler(grid_button_KeyUp));
                    actionButton.AddHandler(PanelGridButton.InternalKeyDownEvent, new KeyEventHandler(grid_button_KeyDown));

                    
                    try
                    {
                        Style ab_style = (Style) FindResource("actions_hover_style");
                        if (ab_style != null)
                            actionButton.SetValue(StyleProperty, ab_style);
                    }
                    catch
                    {
                        
                    }

                    MultiBinding btn_enabled_binding = new MultiBinding();
                    btn_enabled_binding.Converter = new SubMenuPanelGridActionEnabledConverter();
                    btn_enabled_binding.ConverterParameter = action;
                    btn_enabled_binding.Bindings.Add(new Binding());
                    btn_enabled_binding.Bindings.Add(new Binding("TriggerGridUpdate"));

                    MultiBinding btn_opacity_binding = new MultiBinding();
                    btn_opacity_binding.Converter = new SubMenuPanelGridActionOpacityConverter();
                    btn_opacity_binding.ConverterParameter = action;
                    btn_opacity_binding.Bindings.Add(new Binding());
                    btn_opacity_binding.Bindings.Add(new Binding("TriggerGridUpdate"));

                    MultiBinding btn_visibility_binding = new MultiBinding();
                    btn_visibility_binding.Converter = new SubMenuPanelGridActionVisibilityConverter();
                    btn_visibility_binding.ConverterParameter = action;
                    btn_visibility_binding.Bindings.Add(new Binding());
                    btn_visibility_binding.Bindings.Add(new Binding("TriggerGridUpdate"));

                    actionButton.SetBinding(IsEnabledProperty, btn_enabled_binding);
                    actionButton.SetBinding(OpacityProperty, btn_opacity_binding);
                    actionButton.SetBinding(VisibilityProperty, btn_visibility_binding);

                    //elementFactory.AppendChild(actionButton);
                    action_count++;

                    DataTemplate data_template = new DataTemplate();
                    data_template.VisualTree = actionButton;

                    template_col.CellTemplate = data_template;

                    submenu_items_datagrid.Columns.Add(template_col);
                }
            }
        }

        #region panel grid view - event handler
        private void datagrid_action_click(object sender, RoutedEventArgs e)
        {
            Button btn_sender = sender as Button;

            if (btn_sender != null)
            {
                //string action = btn_sender.Tag as string;
                MenuItemAction btn_action = btn_sender.Tag as MenuItemAction;

                if (btn_action != null)
                    RaiseAction(btn_sender.CommandParameter, btn_action);
                else
                    RaiseAction(btn_sender.CommandParameter);
            }
        }
        private void datagrid_button_column_click(object sender, RoutedEventArgs e)
        {
            Button btn_sender = sender as Button;
            CheckBox chx_sender = sender as CheckBox;

            if(btn_sender != null)
            {
                MenuItemAttribute btn_attribute = btn_sender.Tag as MenuItemAttribute;

                if (btn_attribute != null)
                {
                    if (btn_attribute.RaiseClickAction != null)
                        RaiseAction(btn_sender.CommandParameter, btn_attribute.RaiseClickAction);
                    else
                        RaiseAction(btn_sender.CommandParameter, ((SubMenuPanelMenuItem)btn_sender.CommandParameter).DefaultAction);
                }
                else
                {
                    RaiseAction(btn_sender.CommandParameter);
                }
            }
            else if (chx_sender != null)
            {
                MenuItemAttribute btn_attribute = chx_sender.Tag as MenuItemAttribute;

                if (btn_attribute != null)
                {
                    if (btn_attribute.RaiseClickAction != null)
                        RaiseAction(chx_sender.CommandParameter, btn_attribute.RaiseClickAction);
                    else
                        RaiseAction(chx_sender.CommandParameter, ((SubMenuPanelMenuItem)chx_sender.CommandParameter).DefaultAction);
                }
                else
                {
                    RaiseAction(chx_sender.CommandParameter);
                }
            }
        }

        private void submenu_items_datagrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (_ignore_grid_selection_change)
                return;

            int sel_row_indowx = submenu_items_datagrid.SelectedIndex;

            if (sel_row_indowx >= 0)
            {
                // set the keyboard-focus to the first input control in the newly selected row
                DataGridRow new_selected_row = DataGridHelper.GetRow(submenu_items_datagrid, sel_row_indowx);

                if (new_selected_row != null)
                {
                    if (new_selected_row.IsKeyboardFocusWithin)
                        return;

                    bool b_focused = false;
                    List<TextBox> text_boxes = UIHelper.GetVisualChildrenByType<TextBox>(new_selected_row);

                    foreach (TextBox cur_tb in text_boxes)
                    {
                        if (cur_tb.IsEnabled && !cur_tb.IsReadOnly && cur_tb.Visibility == System.Windows.Visibility.Visible)
                        {
                            _comes_from_parent_row = true;
                            cur_tb.Focus();
                            b_focused = true;
                            break;
                        }
                    }
                    if (!b_focused)
                    {
                        List<PanelGridButton> buttons = UIHelper.GetVisualChildrenByType<PanelGridButton>(new_selected_row);
                        foreach (PanelGridButton cur_btn in buttons)
                        {
                            if (cur_btn.IsEnabled && cur_btn.Visibility == System.Windows.Visibility.Visible)
                            {
                                _comes_from_parent_row = true;
                                cur_btn.Focus();
                                b_focused = true;
                                break;
                            }
                        }
                    }
                }
            }
        }

        private void grid_button_KeyUp(object sender, KeyEventArgs e)
        {
            if (_comes_from_parent_row)
            {
                _comes_from_parent_row = false;
                return;
            }

            PanelGridButton sender_btn = sender as PanelGridButton;
            if (sender_btn == null)
                return;

            if (e.Key != Key.Down && e.Key != Key.Up)
                return;

            DataGridRow parent_row = UIHelper.TryFindParent<DataGridRow>(sender_btn);

            if (parent_row != null)
            {
                int row_index = DataGridHelper.FindRowIndex(parent_row);

                if (e.Key == Key.Up)
                    row_index--;
                else if (e.Key == Key.Down)
                    row_index++;
                else
                    return;

                if (row_index >= 0 && row_index < submenu_items_datagrid.Items.Count)
                {
                    DataGridRow new_selected_row = DataGridHelper.GetRow(submenu_items_datagrid, row_index);

                    if (new_selected_row != null)
                    {
                        _ignore_grid_selection_change = true;
                        new_selected_row.IsSelected = true;

                        string button_name = sender_btn.Name;
                        Button btn_new_row = UIHelper.FindVisualChildByName<PanelGridButton>(new_selected_row, button_name) as PanelGridButton;
                        if (btn_new_row != null && btn_new_row.IsEnabled && btn_new_row.Visibility == System.Windows.Visibility.Visible)
                            btn_new_row.Focus();
                        else
                        {
                            bool b_focused = false;
                            List<PanelGridButton> buttons = UIHelper.GetVisualChildrenByType<PanelGridButton>(new_selected_row);
                            foreach (PanelGridButton cur_btn in buttons)
                            {
                                if (cur_btn.IsEnabled && cur_btn.Visibility == System.Windows.Visibility.Visible)
                                {
                                    cur_btn.Focus();
                                    b_focused = true;
                                    break;
                                }
                            }

                            if (!b_focused)
                            {
                                List<TextBox> text_boxes = UIHelper.GetVisualChildrenByType<TextBox>(new_selected_row);

                                foreach (TextBox cur_tb in text_boxes)
                                {
                                    if (cur_tb.IsEnabled && !cur_tb.IsReadOnly && cur_tb.Visibility == System.Windows.Visibility.Visible)
                                    {
                                        cur_tb.Focus();
                                        b_focused = true;
                                        break;
                                    }
                                }
                            }
                        }
                        _ignore_grid_selection_change = false;
                    }
                }
            }
        }

        private void grid_button_KeyDown(object sender, KeyEventArgs e)
        {
            PanelGridButton sender_btn = sender as PanelGridButton;
            if (sender_btn == null)
                return;

            bool first_control_in_next_line = false;

            if (sender_btn.Name == _last_action_button_name)
            {
                if (e.Key == Key.Right || e.Key == Key.Tab)
                {
                    first_control_in_next_line = true;
                    e.Handled = true;
                }
            }
            else
            {
                return;
            }

            DataGridRow parent_row = UIHelper.TryFindParent<DataGridRow>(sender_btn);

            if (parent_row != null)
            {
                int row_index = DataGridHelper.FindRowIndex(parent_row);

                if (first_control_in_next_line)
                    row_index++;
                else
                    return;

                if (row_index >= 0 && row_index < submenu_items_datagrid.Items.Count)
                {
                    DataGridRow new_selected_row = DataGridHelper.GetRow(submenu_items_datagrid, row_index);

                    if (new_selected_row != null)
                    {
                        _ignore_grid_selection_change = true;
                        new_selected_row.IsSelected = true;

                        bool b_focused = false;
                        List<TextBox> text_boxes = UIHelper.GetVisualChildrenByType<TextBox>(new_selected_row);

                        foreach (TextBox cur_tb in text_boxes)
                        {
                            if (cur_tb.IsEnabled && !cur_tb.IsReadOnly && cur_tb.Visibility == System.Windows.Visibility.Visible)
                            {
                                _comes_from_parent_row = true;
                                cur_tb.Focus();
                                b_focused = true;
                                break;
                            }
                        }
                        if (!b_focused)
                        {
                            List<PanelGridButton> buttons = UIHelper.GetVisualChildrenByType<PanelGridButton>(new_selected_row);
                            foreach (PanelGridButton cur_btn in buttons)
                            {
                                if (cur_btn.IsEnabled && cur_btn.Visibility == System.Windows.Visibility.Visible)
                                {
                                    _comes_from_parent_row = true;
                                    cur_btn.Focus();
                                    b_focused = true;
                                    break;
                                }
                            }
                        }
                        _ignore_grid_selection_change = false;
                    }
                }
                else if (row_index == submenu_items_datagrid.Items.Count)
                {
                    _ignore_grid_selection_change = true;
                    parent_row.IsSelected = false;
                    _ignore_grid_selection_change = false;
                }
            }
        }

        private void submenu_items_datagrid_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            double star_min_width = StarColumnMinWidth;
            double space = 45; // column header, scroll bar
            double all_col_width = 0;

            int nStarColumnIndex = StarColumnAttributeIndex;

            double act_width = submenu_items_datagrid.ActualWidth;

            if (double.IsNaN(act_width) || double.IsInfinity(act_width))
                return;

            if(submenu_items_datagrid == null || submenu_items_datagrid.Columns.Count == 0)
                return;

            DataGridColumn start_column = submenu_items_datagrid.Columns[nStarColumnIndex];
            all_col_width = submenu_items_datagrid.Columns.Sum(c => c.ActualWidth);
            all_col_width += space;

            if (start_column != null)
            {
                double cur_bezeichnung_width = start_column.ActualWidth;



                if (double.IsNaN(cur_bezeichnung_width) || double.IsInfinity(cur_bezeichnung_width))
                    return;

                double diff_width = e.NewSize.Width - all_col_width;

                //Debug.WriteLine("e.NewSize.Width=" + e.NewSize.Width.ToString() + " - all_col_width=" + all_col_width.ToString());

                if ((cur_bezeichnung_width + diff_width) < star_min_width)
                    start_column.Width = star_min_width;
                else
                    start_column.Width = (cur_bezeichnung_width + diff_width);
            }
        }

        private void submenu_items_datagrid_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            if(e.Row.Item is SubMenuPanelMenuItem)
            {
                ((SubMenuPanelMenuItem) e.Row.Item).UpdateActionState();
                ((SubMenuPanelMenuItem) e.Row.Item).UpdateAttributeState();
            }
        }

        #endregion

        private void EnbaleLeftRightArrows()
        {
            arrowdown_button.Visibility = Visibility.Collapsed;
            arrowup_button.Visibility = Visibility.Collapsed;

            if (!_with_navigation_buttons)
                return;

            arrowleft_button.Visibility = Visibility.Visible;
            arrowright_button.Visibility = Visibility.Visible;
            
        }

        private void EnableUpDownArrows()
        {
            arrowleft_button.Visibility = Visibility.Collapsed;
            arrowright_button.Visibility = Visibility.Collapsed;

            if (!_with_navigation_buttons)
                return;

            arrowdown_button.Visibility = Visibility.Visible;
            arrowup_button.Visibility = Visibility.Visible;
        }

        private void SendNavigationDepthChanged()
        {
            if (NavigationDepthChanged != null)
                NavigationDepthChanged(this, EventArgs.Empty);
        }

        #region styles
        private Style GetLeftAlignColumnHeaderStyle()
        {
            Style ret = null;
            try
            {
                ret = (Style)TryFindResource("left_align_col_header");
                //ret = (Style) FindResource("left_align_col_header");
            }
            catch
            {
                
            }
            finally
            {
                if (ret == null)
                    ret = DefaultLeftAlignColumnHeaderStyle();
            }

            return ret;
        }

        private Style DefaultLeftAlignColumnHeaderStyle()
        {
            Style defaultStyle = new Style(typeof(DataGridColumnHeader), (Style)FindResource(typeof(DataGridColumnHeader)));
            defaultStyle.Setters.Add(new Setter(HorizontalAlignmentProperty, HorizontalAlignment.Left));
            defaultStyle.Setters.Add(new Setter(HorizontalContentAlignmentProperty, HorizontalAlignment.Left));
            return defaultStyle;
        }

        private Style GetRightAlignColumnHeaderStyle()
        {
            Style ret = null;
            try
            {
                ret = (Style)TryFindResource("right_align_col_header");
                //ret = (Style)FindResource("right_align_col_header");
            }
            catch
            {

            }
            finally
            {
                if (ret == null)
                    ret = DefaultRightAlignColumnHeaderStyle();
            }

            return ret;
        }

        private Style DefaultRightAlignColumnHeaderStyle()
        {
            Style defaultStyle = new Style(typeof(DataGridColumnHeader), (Style)FindResource(typeof(DataGridColumnHeader)));
            defaultStyle.Setters.Add(new Setter(HorizontalAlignmentProperty, HorizontalAlignment.Right));
            defaultStyle.Setters.Add(new Setter(HorizontalContentAlignmentProperty, HorizontalAlignment.Right));
            return defaultStyle;
        }

        private Style GetCenterAlignColumnHeaderStyle()
        {
            Style ret = null;
            try
            {
                ret = (Style)TryFindResource("center_align_col_header");
                //ret = (Style)FindResource("center_align_col_header");
            }
            catch
            {

            }
            finally
            {
                if (ret == null)
                    ret = DefaultCenterAlignColumnHeaderStyle();
            }

            return ret;
        }

        private Style DefaultCenterAlignColumnHeaderStyle()
        {
            Style defaultStyle = new Style(typeof(DataGridColumnHeader), (Style)FindResource(typeof(DataGridColumnHeader)));
            defaultStyle.Setters.Add(new Setter(HorizontalAlignmentProperty, HorizontalAlignment.Center));
            defaultStyle.Setters.Add(new Setter(HorizontalContentAlignmentProperty, HorizontalAlignment.Center));
            return defaultStyle;
        }
        #endregion

        #region Reset MAP ZP
        void UpdateLineMapZP()
        {
            _map_zp.BorderMaxZoom = 1;
            _map_zp.BorderMinZoom = 1;         
            _map_zp.WithBorderCheckWhileTranslationActive = false;
            _map_zp.WithBorderCheck = true;
            _map_zp.IgnoreYTranslation = true;            
        }

        void UpdateKachelMapZP()
        {
            _map_zp.BorderMaxZoom = 10;
            _map_zp.BorderMinZoom = 1;            
            _map_zp.WithBorderCheckWhileTranslationActive = false;
            _map_zp.IgnoreYTranslation = false;
            _map_zp.WithBorderCheck = true;
        }

        void ResetMapZp()
        {
            ResetMapZp(false);
        }
        void ResetMapZp(bool without_scale)
        {
            if (!without_scale)
            {
                _map_zp.ScaleTransform.BeginAnimation(ScaleTransform.ScaleXProperty, _scale_x_reset);
                _map_zp.ScaleTransform.BeginAnimation(ScaleTransform.ScaleYProperty, _scale_y_reset);
            }
            _map_zp.TranslateTransform.BeginAnimation(TranslateTransform.XProperty, _transl_x_reset);
            _map_zp.TranslateTransform.BeginAnimation(TranslateTransform.YProperty, _transl_y_reset);
        }
        void _transl_reset_x_Completed(object sender, EventArgs e)
        {
            _map_zp.TranslateTransform.BeginAnimation(TranslateTransform.XProperty, null);
            _map_zp.TranslateTransform.X = 0;
        }
        void _transl_reset_y_Completed(object sender, EventArgs e)
        {
            _map_zp.TranslateTransform.BeginAnimation(TranslateTransform.YProperty, null);
            _map_zp.TranslateTransform.Y = 0;
        }
        void _scale_reset_x_Completed(object sender, EventArgs e)
        {
            _map_zp.ScaleTransform.BeginAnimation(ScaleTransform.ScaleXProperty, null);
            _map_zp.ScaleTransform.ScaleX = 1;
        }
        void _scale_reset_y_Completed(object sender, EventArgs e)
        {
            _map_zp.ScaleTransform.BeginAnimation(ScaleTransform.ScaleYProperty, null);
            _map_zp.ScaleTransform.ScaleY = 1;
        }

        private void UpdateMapZPBorders()
        {
            if (_map_zp == null)
                return;

            if (ActiveLayoutMode == LayoutMode.Line || ActiveLayoutMode == LayoutMode.Kachel)
            {
                int items_per_row = 0;
                double item_width = 0;
                double item_height = 0;

                if (ActiveLayoutMode == LayoutMode.Line)
                {
                    items_per_row = _items_filtered.Count;
                    item_width = BigItemSize.Width;
                    item_height = BigItemSize.Height;
                }
                else
                {
                    items_per_row = (int)Math.Floor(submenu_base_canvas.ActualWidth / SmallItemSize.Width);
                    item_width = SmallItemSize.Width;
                    item_height = SmallItemSize.Height;
                }

                int rows = (int)Math.Ceiling((double)_items_filtered.Count / (double)items_per_row);
                double panel_canvas_height = 0;
                double panel_canvas_width = 0;

                panel_canvas_height = (item_height * rows);
                if (_items_filtered.Count >= items_per_row)
                    panel_canvas_width = (item_width * items_per_row);
                else
                    panel_canvas_width = _items_filtered.Count * item_width;

                //_map_zp.BorderCheckerWidth = BigItemSize.Width * _items_filtered.Count;
                //_map_zp.BorderCheckerHeight = BigItemSize.Height;                
                
                //_map_zp.BorderCheckerXMax = 50 / _map_zp.ScaleTransform.ScaleX;
                //_map_zp.BorderCheckerYMax = 50 / _map_zp.ScaleTransform.ScaleY;  

                _map_zp.BorderCheckerXMax = 0;
                _map_zp.BorderCheckerYMax = 0;  

                if (ActiveLayoutMode == LayoutMode.Line)
                {
                    //if (_animating_tile_panel.Width > panel_canvas_width)
                    //    _map_zp.BorderCheckerWidth = _animating_tile_panel.Width;
                    //else
                    //    _map_zp.BorderCheckerWidth = panel_canvas_width + 50 / _map_zp.ScaleTransform.ScaleX;
                    _map_zp.BorderCheckerWidth = panel_canvas_width;
                    _map_zp.BorderCheckerHeight = BigItemSize.Height;
                    _map_zp.BorderCheckerXMax = 0;
                    _map_zp.BorderCheckerYMax = 0;
                }
                else
                {
                    //_map_zp.BorderCheckerWidth = panel_canvas_width + 50 / _map_zp.ScaleTransform.ScaleX;                   
                    _map_zp.BorderCheckerWidth = panel_canvas_width;
                    //_map_zp.BorderCheckerWidth = (SmallItemSize.Width * _items_filtered.Count) + 50 / _map_zp.ScaleTransform.ScaleX;
                    if(panel_canvas_height < base_grid.ActualHeight/ _map_zp.ScaleTransform.ScaleY)
                        _map_zp.BorderCheckerHeight = (base_grid.ActualHeight / _map_zp.ScaleTransform.ScaleY) + 1 / _map_zp.ScaleTransform.ScaleY;
                    else
                        _map_zp.BorderCheckerHeight = panel_canvas_height;

                    submenu_base_canvas.Height = panel_canvas_height;
                }

              
            }
           
            else if(ActiveLayoutMode == LayoutMode.Grid)
            {
                
            }
        }
        #endregion
        /// <summary>
        /// diese methode holt sich ausgehend vom erforderlichplatz die gebrauchte höhe
        /// und animiert das panel auf die passende höhe
        /// der erforderlich platz wird berechnet abhängig von der anzahl der elemente die dargestellt werden
        /// </summary>
        private void UpdateAndAnimatePanelToHeight(double height)
        {
            _animation_active = true;
            DoubleAnimation canvasheight_da = new DoubleAnimation(height, new Duration(new TimeSpan(0, 0, 0, 0, 500)));
            canvasheight_da.DecelerationRatio = 1;
            canvasheight_da.Completed += new EventHandler(canvasheight_da_Completed);
            submenu_border.Tag = height;
            submenu_border.BeginAnimation(HeightProperty, canvasheight_da);
        }

        private void canvasheight_da_Completed(object sender, EventArgs e)
        {
            submenu_border.BeginAnimation(HeightProperty, null);
            submenu_border.Height = (double)submenu_border.Tag;
            _animation_active = false;

            if (_autohide_timer_after_action)
                StartAutoHideTimer();
        }
        #endregion

        #region EventHandler
        void Window_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            _resize_active_new_size = e.NewSize;
            bool panel_visible = (submenu_border.Height > 0);
            if(panel_visible)
            {
                UpdateAndAnimatePanelToHeight(PanelHeight);
            }
            _resize_active_new_size = null;
        }

        private void search_button_Click(object sender, RoutedEventArgs e)
        {
            if (SearchButtonClick != null)
                SearchButtonClick(this, new EventArgs());
        }

        private void newelem_button_Click(object sender, RoutedEventArgs e)
        {
            if (NewElemClick != null)
                NewElemClick(this, new EventArgs());
        }

        private void custom_button_Click(object sender, RoutedEventArgs e)
        {
            if(CustomButtonClick != null)
                CustomButtonClick(this, new EventArgs());
        }

        private void custom_button1_Click(object sender, RoutedEventArgs e)
        {
            if (CustomButton1Click != null)
                CustomButton1Click(this, new EventArgs());
        }

        void search_cbx_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if(search_cbx.SelectedItem != null)
            {
                ApplyFilterText(true);
            }
        }

        void search_cbx_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (search_cbx.SelectedItem != null)
                return;

            ApplyFilterText(false);
        }

        public void SetSearchFilter(string text)
        {
            search_cbx.SelectedItem = null;
            search_cbx.Text = text;
        }

        public string GetSearchFilter()
        {
            if (search_cbx.SelectedItem != null)
                return "";

            return search_cbx.Text;
        }

        private void ApplyFilterText(bool custom_filter)
        {
            _was_custom_filter = custom_filter;
//#if DEBUG
//            if(custom_filter)
//            {
//                Debug.WriteLine("*** Applying custom SubmenuPanelItem filter ...");
//                Debug.WriteLine("*** Filter: " + (search_cbx.SelectedItem as string) ?? "<null>");
//            }
//            else
//            {
//                Debug.WriteLine("*** Applying SubmenuPanelItem Textfilter ...");
//                Debug.WriteLine("*** Filter: " + (string.IsNullOrEmpty(search_cbx.Text) ? "<empty>" : search_cbx.Text));
//            }
//#endif

            var query_filtered_items = _items.Select(i => i);
            bool b_text_check = string.IsNullOrEmpty(search_cbx.Text);

            if (custom_filter)
                b_text_check = search_cbx.SelectedItem == null;

            if (!(!WithSearchField || b_text_check))
                query_filtered_items = _items.Where(i => IsFiltered(i));

            List<SubMenuPanelMenuItem> items_to_remove = _items_filtered.Where(i => !IsFiltered(i)).ToList();

            foreach (SubMenuPanelMenuItem filtered_item in query_filtered_items)
            {
                if (!_items_filtered.Contains(filtered_item))
                    _items_filtered.Add(filtered_item);
            }

            foreach (SubMenuPanelMenuItem viewbox in items_to_remove)
            {
                _items_filtered.Remove(viewbox);
            }

            _items_filtered.Sort((x, y) => x.CompareTo(y));

            ResetMapZp(true);

            UpdateMapZPBorders();

            _animating_tile_panel.InvalidateVisual();
            _animating_tile_panel.UpdateLayout();
        }

        public void ApplySort()
        {
            _items_filtered.Sort((x, y) => x.CompareTo(y));

            // update itemcount if filter is set
            if (WithShowItemCount && ItemCountFilterExpression != null)
            {
                ItemCount = _items.Where(i => ItemCountFilterExpression(i)).Count();
            }
        }

        void _submenu_fadein_Completed(object sender, EventArgs e)
        {
            BaseFrame.EnableMouseClicks();
            _autohide_timer_after_action = false;
            _submenu_fadein_da.Completed -= _submenu_fadein_Completed;
            //_submenu_fadein_da = null;
            submenu_border.BeginAnimation(HeightProperty, null);
            submenu_border.Height = PanelHeight;
            _animation_active = false;
                       

            SetMode();

            if (FadeInCompleted != null)
                FadeInCompleted(this, new EventArgs());

        }

        void SetMode()
        {
            if (ActiveLayoutMode == LayoutMode.Line)
                UpdateLineMapZP();
        }

        void _submenu_fadeout_da_Completed(object sender, EventArgs e)
        {
            BaseFrame.EnableMouseClicks();
            _autohide_timer_after_action = false;
            _submenu_fadeout_da.Completed -= _submenu_fadein_Completed;
            //_submenu_fadeout_da = null;
            submenu_border.BeginAnimation(HeightProperty, null);
            submenu_border.Height = 0;
            _animation_active = false;
            
            if (FadeOutCompleted != null)
                FadeOutCompleted(this, new EventArgs());
        }

        private void Rectangle_MouseDown(object sender, MouseButtonEventArgs e)
        {
            _resize_active = true;
            BaseFrame.MouseUp += BaseWindow_MouseUp;
            BaseFrame.MouseMove += BaseWindow_MouseMove;
            BaseFrame.DisableClicksMainArea();
            IsHitTestVisible = false;
            BaseFrame.OverlayManager.IsHitTestVisible = false;
            _submenu_resize_start_pos = e.GetPosition(BaseFrame);
            _start_height_resize = submenu_border.ActualHeight;
            BaseFrame.SetAppCursor(Cursors.SizeNS);
        }

        void BaseWindow_MouseMove(object sender, MouseEventArgs e)
        {
            Point cur_pos = e.GetPosition(BaseFrame);
            Vector v = cur_pos - _submenu_resize_start_pos;

            double new_height = _start_height_resize - v.Y;
            CheckPanelHeight(ref new_height);
            submenu_border.Height = new_height;
        }

        private void CheckPanelHeight(ref double new_height)
        {
            if (new_height < BaseFrameBase<SubMenuPanel>.MINHEIGHT_MAINCONTENT)
                new_height = BaseFrameBase<SubMenuPanel>.MINHEIGHT_MAINCONTENT;

            double max_height = BaseFrame.WindowBorder.ActualHeight - BaseFrame.TitleBar.ActualHeight - BaseFrameBase<SubMenuPanel>.MINHEIGHT_MAINCONTENT - BaseFrame.TaskBar.BaseBorder.ActualHeight;
            if (new_height > max_height)
                new_height = max_height;
        }

        void BaseWindow_MouseUp(object sender, MouseButtonEventArgs e)
        {
            _resize_active = false;
            BaseFrame.MouseUp -= BaseWindow_MouseUp;
            BaseFrame.MouseMove -= BaseWindow_MouseMove;
            BaseFrame.EnableClicksMainArea();
            IsHitTestVisible = true;
            BaseFrame.OverlayManager.IsHitTestVisible = true;
            BaseFrame.SetDefaultAppCursor();
        }

        void _animating_tile_panel_MouseLeave(object sender, MouseEventArgs e)
        {
            //if (ToggleButtonReference.IsChecked == true && WithAutoHideMenu == true && pin_button.IsChecked != true)
            //{
            //    if (e.GetPosition(submenu_base_canvas).Y < 0 && !_resize_active)
            //        ToggleButtonReference.IsChecked = false;
            //}

            if (ToggleButtonReference.IsChecked == true && WithAutoHideMenu == true && pin_button.IsChecked != true)
            {
                StartAutoHideTimer();
            }
        }

        void _animating_tile_panel_MouseEnter(object sender, MouseEventArgs e)
        {
            StopAutoHideTimer();
        }

        void _animating_tile_panel_MouseMove(object sender, MouseEventArgs e)
        {
            StopAutoHideTimer();
        }

        #region LayoutMode Setting
        void orderlayout1_button_Unchecked(object sender, RoutedEventArgs e)
        {
            ToggleButton button = (ToggleButton) sender;
            button.IsChecked = true;
        }

        void orderlayout1_button_Checked(object sender, RoutedEventArgs e)
        {
            ActiveLayoutMode = LayoutMode.Kachel;
        }

        void orderlayout2_button_Unchecked(object sender, RoutedEventArgs e)
        {
            ToggleButton button = (ToggleButton)sender;
            button.IsChecked = true;

        }

        void orderlayout2_button_Checked(object sender, RoutedEventArgs e)
        {
            ActiveLayoutMode = LayoutMode.Line;
        }

        void orderlayout3_button_Unchecked(object sender, RoutedEventArgs e)
        {
            ToggleButton button = (ToggleButton)sender;
            button.IsChecked = true;
        }

        void orderlayout3_button_Checked(object sender, RoutedEventArgs e)
        {
            ActiveLayoutMode = LayoutMode.Grid;
        }
        #endregion

        #region AutoHide

        private void StopAutoHideTimer()
        {
            if (_auto_hide_delay_timer != null)
            {
                _auto_hide_delay_timer.Stop();
                _auto_hide_delay_timer.Elapsed -= _auto_hide_delay_timer_Elapsed;
                _auto_hide_delay_timer = null;
            } 
        }

        private void StartAutoHideTimer()
        {
            StopAutoHideTimer();

            if (_resize_active || _animation_active || Mouse.GetPosition(submenu_base_canvas).Y >= 0)
            {
                if (!_ignore_autohide_after_action)
                    _autohide_timer_after_action = true;
                else
                    _ignore_autohide_after_action = false;

                return;
            }


            _auto_hide_delay_timer = new Timer(AutoHideTimerInterval);
            _auto_hide_delay_timer.Elapsed += new ElapsedEventHandler(_auto_hide_delay_timer_Elapsed);
            _auto_hide_delay_timer.Start();
        }

        void _auto_hide_delay_timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            StopAutoHideTimer();

            Dispatcher.Invoke(DispatcherPriority.Normal, (NoParas) delegate {

                if (ToggleButtonReference != null)
                    ToggleButtonReference.IsChecked = false;
            });
            
        }
        #endregion
        #endregion


        void _map_zp_TranslationAnimationCompleted(object sender, EventArgs e)
        {
            animating_tile_panel_items_control.IsHitTestVisible = true;
        }

        void _map_zp_TranslationAnimationStarted(object sender, EventArgs e)
        {
            animating_tile_panel_items_control.IsHitTestVisible = false;
        }

        #region Button movement
        private void CommandBinding_NavigateLeft(object sender, RoutedEventArgs e)
        {
            GoLeft();
        }

        private void CommandBinding_NavigateRight(object sender, RoutedEventArgs e)
        {
            GoRight();
        }

        public void GoLeft()
        {
           if (_items_filtered.Count == 0)
                        return;

                    if (_items_filtered.Count(i => i.IsSelected == true) == 0)
                    {
                        _items_filtered[0].IsSelected = true;
                        FocusHelper.Focus(_items_filtered[0]);
                        return;
                    }

                    SubMenuPanelMenuItem item = _items_filtered.Where(i => i.IsSelected).FirstOrDefault();
                    int curpos = _items_filtered.IndexOf(item);           

                    DeselectAllItems();
            
                    if (curpos == 0)
                    {
                        _items_filtered[_items_filtered.Count - 1].IsSelected = true;
                        FocusHelper.Focus(_items_filtered[_items_filtered.Count - 1]);
                    }
                    else
                    {
                        _items_filtered[curpos - 1].IsSelected = true;
                        FocusHelper.Focus(_items_filtered[curpos - 1]);
                    }

                    return;

                    #region OLD
                    //double cur_transform_x = _map_zp.TranslateTransform.X;
                    //double dMaxItems = MaxItemCountWidth();

                    //double new_x = cur_transform_x + (dMaxItems) * _big_item_size.Width;

                    //if (new_x > 0)
                    //    new_x = 0;

                    //DoubleAnimation transl_x = new DoubleAnimation(new_x, new Duration(new TimeSpan(0, 0, 0, 0, 500))) { DecelerationRatio = 1 };
                    //transl_x.Completed += new EventHandler(transl_x_Completed);

                    //_map_zp.TranslateTransform.BeginAnimation(TranslateTransform.XProperty, transl_x);
                    #endregion
        }

        public void GoRight()
        {
             if (_items_filtered.Count == 0)
                            return;

                        if (_items_filtered.Count(i => i.IsSelected == true) == 0)
                        {                
                            _items_filtered[0].IsSelected = true;
                            FocusHelper.Focus(_items_filtered[0]);            
                            return;
                        }

                        SubMenuPanelMenuItem item = _items_filtered.Where(i => i.IsSelected).FirstOrDefault();
                        int curpos = _items_filtered.IndexOf(item);            

                        DeselectAllItems();

                        if (curpos == _items_filtered.Count - 1)
                        {
                            _items_filtered[0].IsSelected = true;
                            FocusHelper.Focus(_items_filtered[0]);
                        }
                        else
                        {
                            _items_filtered[curpos + 1].IsSelected = true;
                            FocusHelper.Focus(_items_filtered[curpos + 1]);
                        }                

                        return;

                        #region OLD
                        //SubMenuPanelMenuItem m_item = FindSelectedMenuItem();
                        //m_item.IsSelected = true;

                        //    if (_map_zp == null)
                        //    return;

                        //if (_map_zp.TranslateTransform == null)
                        //    return;

                        //double cur_transform_x = _map_zp.TranslateTransform.X;
                        //double dMaxItems = MaxItemCountWidth();


                        //if (_items_filtered.Count < dMaxItems)
                        //    return;

                        //double new_x = cur_transform_x - (dMaxItems) * _big_item_size.Width;

                        //double complete_width = _map_zp.BorderCheckerWidth;
                        //if ((submenu_base_grid.ActualWidth + Math.Abs(new_x)) > complete_width)
                        //{
                        //    new_x = submenu_base_grid.ActualWidth - complete_width;
                        //}

                        //DoubleAnimation transl_x = new DoubleAnimation(new_x, new Duration(new TimeSpan(0, 0, 0, 0, 500))) { DecelerationRatio = 1 };
                        //transl_x.Completed += new EventHandler(transl_x_Completed);

                        //_map_zp.TranslateTransform.BeginAnimation(TranslateTransform.XProperty, transl_x);
                        ////_map_zp.TranslateTransform.BeginAnimation(TranslateTransform.YProperty, _transl_y_reset);
                        #endregion
        }

        private void arrowleft_button_Click(object sender, RoutedEventArgs e)
        {
            GoLeft();
        }

        private void arrowright_button_Click(object sender, RoutedEventArgs e)
        {
            GoRight();
        }

        private void arrowdown_button_Click(object sender, RoutedEventArgs e)
        {
            if (_map_zp == null)
                return;

            if (_map_zp.TranslateTransform == null)
                return;

            if (_active_layout_mode == LayoutMode.Kachel)
            {
                double cur_transform_y = _map_zp.TranslateTransform.Y;
                double dMaxItems = MaxItemCountHeight();


                if (_items_filtered.Count < dMaxItems)
                    return;

                double new_y = cur_transform_y - (dMaxItems)*_small_item_size.Height;

                double complete_height = _map_zp.BorderCheckerHeight;
                if ((submenu_base_grid.ActualHeight + Math.Abs(new_y)) > complete_height)
                {
                    new_y = submenu_base_grid.ActualHeight - complete_height;
                }

                DoubleAnimation transl_y = new DoubleAnimation(new_y, new Duration(new TimeSpan(0, 0, 0, 0, 500)))
                                               {DecelerationRatio = 1};
                transl_y.Completed += new EventHandler(transl_y_Completed);

                _map_zp.TranslateTransform.BeginAnimation(TranslateTransform.YProperty, transl_y);
            } 
            else if(_active_layout_mode == LayoutMode.Grid)
            {
                // sel. item im grid verändern
                if (submenu_items_datagrid.SelectedItem == null)
                {
                    submenu_items_datagrid.SelectedIndex = 0;
                    return;
                }
                int curpos = submenu_items_datagrid.SelectedIndex;

                if (curpos == submenu_items_datagrid.Items.Count - 1)
                {
                    submenu_items_datagrid.SelectedIndex = 0;
                }
                else
                {
                    submenu_items_datagrid.SelectedIndex += 1 ;
                }
            }
        }

        private void arrowup_button_Click(object sender, RoutedEventArgs e)
        {
            if (_map_zp == null)
                return;

            if (_map_zp.TranslateTransform == null)
                return;

            if (_active_layout_mode == LayoutMode.Kachel)
            {
                double cur_transform_y = _map_zp.TranslateTransform.Y;
                double dMaxItems = MaxItemCountHeight();

                double new_y = cur_transform_y + (dMaxItems)*_small_item_size.Height;

                if (new_y > 0)
                    new_y = 0;

                DoubleAnimation transl_y = new DoubleAnimation(new_y, new Duration(new TimeSpan(0, 0, 0, 0, 500)))
                                               {DecelerationRatio = 1};
                transl_y.Completed += new EventHandler(transl_y_Completed);

                _map_zp.TranslateTransform.BeginAnimation(TranslateTransform.YProperty, transl_y);
            }
            else if(_active_layout_mode == LayoutMode.Grid)
            {
                // sel. item im grid verändern
                // sel. item im grid verändern
                if (submenu_items_datagrid.SelectedItem == null)
                {
                    submenu_items_datagrid.SelectedIndex = submenu_items_datagrid.Items.Count - 1;
                    return;
                }
                int curpos = submenu_items_datagrid.SelectedIndex;

                if (curpos == 0)
                {
                    submenu_items_datagrid.SelectedIndex = submenu_items_datagrid.Items.Count - 1;
                }
                else
                {
                    submenu_items_datagrid.SelectedIndex -= 1;
                }
            }
        }

        void transl_x_Completed(object sender, EventArgs e)
        {
            if (_map_zp == null || _map_zp.TranslateTransform == null)
                return;

            double cur_x = _map_zp.TranslateTransform.X;
            _map_zp.TranslateTransform.BeginAnimation(TranslateTransform.XProperty, null);
            _map_zp.TranslateTransform.X = cur_x;
        }

        void transl_y_Completed(object sender, EventArgs e)
        {
            if (_map_zp == null || _map_zp.TranslateTransform == null)
                return;

            double cur_y = _map_zp.TranslateTransform.Y;
            _map_zp.TranslateTransform.BeginAnimation(TranslateTransform.YProperty, null);
            _map_zp.TranslateTransform.Y = cur_y;
        }

        private double MaxItemCountWidth()
        {
            Size szCheck = new Size(0,0);
            double max_item_width = szCheck.Width + ItemMargin.Left;

            if(_active_layout_mode == LayoutMode.Line)
            {
                szCheck = _big_item_size;
                double dMaxWidth = submenu_base_grid.ActualWidth;
                max_item_width = szCheck.Width + ItemMargin.Left;

                if (max_item_width == 0)
                    return 0;

                return dMaxWidth/max_item_width;
            }

            if (max_item_width == 0)
                return 0;

            return submenu_base_grid.ActualWidth / max_item_width;
        }

        private double MaxItemCountHeight()
        {
            Size szCheck = new Size(0, 0);
            double max_item_width = szCheck.Width + ItemMargin.Left;
            double max_item_height = szCheck.Height + ItemMargin.Top;

            if (_active_layout_mode == LayoutMode.Kachel)
            {
                szCheck = _big_item_size;
                double dMaxHeight = submenu_base_grid.ActualHeight;
                max_item_height = szCheck.Height + ItemMargin.Top;

                if (max_item_height == 0)
                    return 0;

                return dMaxHeight / max_item_height;
            }

            if (max_item_height == 0)
                return 0;

            return submenu_base_grid.ActualHeight / max_item_height;
        }
        #endregion

        #region Element EventHandler
        void item_LostKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            //SubMenuPanelMenuItem m_item = sender as SubMenuPanelMenuItem;

            //if (m_item != null)
            //    m_item.IsSelected = false;
        }

        void item_GotKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            SubMenuPanelMenuItem m_item = sender as SubMenuPanelMenuItem;

            if (m_item != null)
            {
                //DeselectAllItems();

                //m_item.IsSelected = true;
                //if (ShowActionsByLeftMouse)
                //    m_item.HideActionMenu = !m_item.HideActionMenu;

                this.MoveItemIntoView(m_item);
#if DEBUG
                //object f_element = FocusManager.GetFocusedElement(m_item);
                //Console.WriteLine("Got_Focus:" + f_element.ToString() + " Name:" + f_element.GetHashCode() + " KeyboardFocusWithin: " + IsKeyboardFocusWithin);
#endif
            }
        }

        private void submenu_base_canvas_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            UpdateMapZPBorders();
        }

        private void RaiseAction(object tag)
        {
            SubMenuPanelMenuItem m_i = tag as SubMenuPanelMenuItem;
            if(m_i != null)
                RaiseAction(tag, m_i.DefaultAction);
            else
                RaiseAction(tag, null);
        }
      
        private void RaiseAction(object tag, MenuItemAction action)
        {
            if (this.Action != null)
                this.Action(tag, new MenuItemActionEventArgs(action));
        }

        void item_ActionClicked(object sender, MenuItemActionEventArgs e)
        {
            CheckSubNavigation(sender, e.ClickedAction);
            RaiseAction(sender, e.ClickedAction);
        }

        void t_Unchecked(object sender, RoutedEventArgs e)
        {
            RaiseAction(((FrameworkElement)sender).Tag);
        }

        void t_Checked(object sender, RoutedEventArgs e)
        {
            RaiseAction(((FrameworkElement)sender).Tag);
        }

        void b_Click(object sender, RoutedEventArgs e)
        {
            SubMenuPanelMenuItem m_i = sender as SubMenuPanelMenuItem;
            if(m_i != null)
                CheckSubNavigation(sender, m_i.DefaultAction);
            else
                CheckSubNavigation(sender, null);
            
            RaiseAction(((FrameworkElement)sender).Tag);
        }

        private void CheckSubNavigation(object sender, MenuItemAction action)
        {
            SubMenuPanelMenuItem m_i = sender as SubMenuPanelMenuItem;
            if (m_i != null && action == m_i.DefaultAction && (m_i.HasChildren || m_i.ForceShowChildren))
            {
                // switch to subitem layer
                NavigateToSubItems(m_i);
            }
        }
        #endregion

        #region Dependency Properties



        public string CustomNewElementButtonText
        {
            get { return (string)GetValue(CustomNewElementButtonTextProperty); }
            set { SetValue(CustomNewElementButtonTextProperty, value); }
        }

        public static readonly DependencyProperty CustomNewElementButtonTextProperty =
            DependencyProperty.Register("CustomNewElementButtonText", typeof(string), typeof(SubMenuPanel), new UIPropertyMetadata("Neues Element Hinzufügen"));

        
        public string CustomButtonText
        {
            get { return (string)GetValue(CustomButtonTextProperty); }
            set { SetValue(CustomButtonTextProperty, value); }
        }
              
        public static readonly DependencyProperty CustomButtonTextProperty = DependencyProperty.Register("CustomButtonText", typeof(string), typeof(SubMenuPanel), new UIPropertyMetadata("Custom Button"));

        public string CustomButtonText1
        {
            get { return (string)GetValue(CustomButtonText1Property); }
            set { SetValue(CustomButtonText1Property, value); }
        }

        public static readonly DependencyProperty CustomButtonText1Property = DependencyProperty.Register("CustomButtonText1", typeof(string), typeof(SubMenuPanel), new UIPropertyMetadata("Custom Button1"));


        public string SearchButtonTooltip
        {
            get { return (string)GetValue(SearchButtonTooltipProperty); }
            set { SetValue(SearchButtonTooltipProperty, value); }
        }

        public static readonly DependencyProperty SearchButtonTooltipProperty = DependencyProperty.Register("SearchButtonTooltip", typeof(string), typeof(SubMenuPanel), new UIPropertyMetadata("Gehe zu Element"));

        public int StarColumnAttributeIndex
        {
            get { return (int)GetValue(StarColumnAttributeIndexProperty); }
            set { SetValue(StarColumnAttributeIndexProperty, value); }
        }
        public static readonly DependencyProperty StarColumnAttributeIndexProperty = DependencyProperty.Register("StarColumnAttributeIndex", typeof(int), typeof(SubMenuPanel), new UIPropertyMetadata(0));
        public double StarColumnMinWidth
        {
            get { return (double)GetValue(StarColumnMinWidthProperty); }
            set { SetValue(StarColumnMinWidthProperty, value); }
        }
        public static readonly DependencyProperty StarColumnMinWidthProperty = DependencyProperty.Register("StarColumnMinWidth", typeof(double), typeof(SubMenuPanel), new UIPropertyMetadata((double)125));


        public Brush SubMenuIconBgBrush
        {
            get { return (Brush)GetValue(SubMenuIconBgBrushProperty); }
            set { SetValue(SubMenuIconBgBrushProperty, value); }
        }
        public static readonly DependencyProperty SubMenuIconBgBrushProperty = DependencyProperty.Register("SubMenuIconBgBrush", typeof(Brush), typeof(SubMenuPanel), new UIPropertyMetadata(Brushes.Black));

        public Brush PanelBackground
        {
            get { return (Brush)GetValue(PanelBackgroundProperty); }
            set { SetValue(PanelBackgroundProperty, value); }
        }
        public static readonly DependencyProperty PanelBackgroundProperty = DependencyProperty.Register("PanelBackground", typeof(Brush), typeof(SubMenuPanel), new UIPropertyMetadata(Brushes.White));

        public Brush ButtonHighlightBackground
        {
            get { return (Brush)GetValue(ButtonHighlightBackgroundProperty); }
            set { SetValue(ButtonHighlightBackgroundProperty, value); }
        }
        public static readonly DependencyProperty ButtonHighlightBackgroundProperty = DependencyProperty.Register("ButtonHighlightBackground", typeof(Brush), typeof(SubMenuPanel), new UIPropertyMetadata(Brushes.Transparent));

        public int ItemCount
        {
            get { return (int)GetValue(ItemCountProperty); }
            set { SetValue(ItemCountProperty, value); }
        }
        public static readonly DependencyProperty ItemCountProperty = DependencyProperty.Register("ItemCount", typeof(int), typeof(SubMenuPanel), new UIPropertyMetadata(0));

        public Func<SubMenuPanelMenuItem, bool> ItemCountFilterExpression
        {
            get { return (Func<SubMenuPanelMenuItem, bool>)GetValue(ItemCountFilterExpressionProperty); }
            set { SetValue(ItemCountFilterExpressionProperty, value); }
        }
        public static readonly DependencyProperty ItemCountFilterExpressionProperty = DependencyProperty.Register("ItemCountFilterExpression", typeof(Func<SubMenuPanelMenuItem, bool>), typeof(SubMenuPanel), new UIPropertyMetadata(null));

        public bool WithShowItemCount
        {
            get { return (bool)GetValue(WithShowItemCountProperty); }
            set { SetValue(WithShowItemCountProperty, value); }
        }
        public static readonly DependencyProperty WithShowItemCountProperty = DependencyProperty.Register("WithShowItemCount", typeof(bool), typeof(SubMenuPanel), new UIPropertyMetadata(false));

        public bool ShowInfoIndicator
        {
            get { return (bool)GetValue(ShowInfoIndicatorProperty); }
            set { SetValue(ShowInfoIndicatorProperty, value); }
        }
        public static readonly DependencyProperty ShowInfoIndicatorProperty = DependencyProperty.Register("ShowInfoIndicator", typeof(bool), typeof(SubMenuPanel), new UIPropertyMetadata(false));

        public bool HighlightSearchButton
        {
            get { return (bool)GetValue(HighlightSearchButtonProperty); }
            set { SetValue(HighlightSearchButtonProperty, value); }
        }
        public static readonly DependencyProperty HighlightSearchButtonProperty = DependencyProperty.Register("HighlightSearchButton", typeof(bool), typeof(SubMenuPanel), new UIPropertyMetadata(false));


        public SubMenuPanelBreadCrumbItem CurrentBreadCrumbItem
        {
            get { return (SubMenuPanelBreadCrumbItem)GetValue(CurrentBreadCrumbItemProperty); }
            set { SetValue(CurrentBreadCrumbItemProperty, value); }
        }
        public static readonly DependencyProperty CurrentBreadCrumbItemProperty = DependencyProperty.Register("CurrentBreadCrumbItem", typeof(SubMenuPanelBreadCrumbItem), typeof(SubMenuPanel), new UIPropertyMetadata(null, new PropertyChangedCallback(OnCurrentBreadCrumbItem)));

        #endregion

        #region Properties
        public Window Window
        {
            get
            {
                return UIHelper.TryFindParent<Window>(this);
            }
        }

        public string TaskbarItemName
        {
            get { return _taskbar_item_name;}
            internal set
            {
                if(_taskbar_item_name != value)
                {
                    _taskbar_item_name = value;
                    _root_bread_crumb_item.Text = _taskbar_item_name;
                }
            }
        }

        public bool WithSearchField
        {
            get
            {
                return search_cbx.Visibility == Visibility.Visible;
            }
            set
            {
                if(value)
                {
                    search_rect.Visibility = Visibility.Visible;
                    search_button.Visibility = Visibility.Visible;
                    search_cbx.Visibility = Visibility.Visible;

                    search_cbx.TextChanged -= search_cbx_TextChanged;
                    search_cbx.TextChanged += search_cbx_TextChanged;
                    search_cbx.SelectionChanged -= search_cbx_SelectionChanged;
                    search_cbx.SelectionChanged += search_cbx_SelectionChanged;
                }
                else
                {
                    search_rect.Visibility = Visibility.Collapsed;
                    search_button.Visibility = Visibility.Collapsed;
                    search_cbx.Visibility = Visibility.Collapsed;

                    search_cbx.TextChanged -= search_cbx_TextChanged;
                    search_cbx.SelectionChanged -= search_cbx_SelectionChanged;
                }
            }
        }

        public bool WithSearchButton
        {
            get
            {
                return search_button.Visibility == Visibility.Visible;
            }
            set
            {
                if (value)
                {
                    search_rect.Visibility = Visibility.Visible;
                    search_button.Visibility = Visibility.Visible;
                }
                else
                {
                    if(!WithSearchField)
                        search_rect.Visibility = Visibility.Collapsed;
                    search_button.Visibility = Visibility.Collapsed;

                }
            }
        }

        public bool WithNewElementField
        {
            set
            {
                if (value)
                {
                    newelem_rect.Visibility = Visibility.Visible;
                    newelem_button.Visibility = Visibility.Visible;
                }
                else
                {
                    newelem_rect.Visibility = Visibility.Collapsed;
                    newelem_button.Visibility = Visibility.Collapsed;
                }
            }
        }

        public bool WithCustomButton
        {
            set
            {
                if (value)
                {
                    custom_rect.Visibility = Visibility.Visible;
                    custom_button.Visibility = Visibility.Visible;
                }
                else
                {
                    custom_rect.Visibility = Visibility.Collapsed;
                    custom_button.Visibility = Visibility.Collapsed;
                }
            }
        }

        public bool WithCustomButton1
        {
            set
            {
                if (value)
                {
                    custom_rect1.Visibility = Visibility.Visible;
                    custom_button1.Visibility = Visibility.Visible;
                }
                else
                {
                    custom_rect1.Visibility = Visibility.Collapsed;
                    custom_button1.Visibility = Visibility.Collapsed;
                }
            }
        }

        public bool WithNavigationButtons
        {
            set
            {
                _with_navigation_buttons = value;
                if (value)
                {
                    //nav_rect.Visibility = Visibility.Visible;
                    if (_active_layout_mode == LayoutMode.Line)
                        EnbaleLeftRightArrows();
                    else
                        EnableUpDownArrows();
                }
                else
                {
                    //nav_rect.Visibility = Visibility.Collapsed;
                    arrowleft_button.Visibility = Visibility.Collapsed;
                    arrowright_button.Visibility = Visibility.Collapsed;
                    arrowdown_button.Visibility = Visibility.Collapsed;
                    arrowup_button.Visibility = Visibility.Collapsed;
                }
            }
        }

        public bool WithGridPanelLayout
        {
            set
            {
                if (value)
                {
                    orderlayout3_button.Visibility = Visibility.Visible;
                }
                else
                {
                    orderlayout3_button.Visibility = Visibility.Collapsed;
                }
            }
        }

        public bool WithKachelPanelLayout
        {
            set
            {
                if(value)
                {
                    orderlayout1_button.Visibility = Visibility.Visible;
                }
                else
                {
                    orderlayout1_button.Visibility = Visibility.Collapsed;
                }
            }
        }

        public bool WithLinePanelLayout
        {
            set
            {
                if (value)
                {
                    orderlayout2_button.Visibility = Visibility.Visible;
                }
                else
                {
                    orderlayout2_button.Visibility = Visibility.Collapsed;
                }
            }
        }

        protected bool WithPinButton
        {
            set
            {
                if (value)
                {
                    pin_button.Visibility = Visibility.Visible;
                }
                else
                {
                    pin_button.Visibility = Visibility.Collapsed;
                }
            }
        }

        public bool WithAutoHideMenu
        {
            get
            {
                return _with_auto_hide;
            }
            set 
            { 
                _with_auto_hide = value;
                WithPinButton = value;
            }
        }

        public double AutoHideTimerInterval
        {
            get { return _auto_hide_timer_interval; }
            set { _auto_hide_timer_interval = value; }
        }

        public Grid BaseGrid
        {
            get
            {
                return base_grid;
            }
        }

        public AnimatingTilePanel AnimatingTilePanel
        {
            get
            {
                return _animating_tile_panel;
            }
        }

        public IBaseFrameBase BaseFrame
        {
            get;
            set;
        }

        /// <summary>
        /// das submenu panel ist verbunden mit einem togglebutton
        /// dieser sollte in der taskbar aufscheinen
        /// </summary>
        public ToggleButton ToggleButtonReference
        {
            get; set;
        }

        public LayoutMode ActiveLayoutMode
        {
            get { return _active_layout_mode; }
            set
            {
                if(value == _active_layout_mode)
                    return;

                _active_layout_mode = value;
                UnRegisterLayoutCheckedEvents();
                switch (_active_layout_mode)
                {
                    case LayoutMode.Kachel:
                        orderlayout1_button.IsChecked = true;
                        orderlayout2_button.IsChecked = false;
                        orderlayout3_button.IsChecked = false;

                        SwitchToKachel();
                        break;
                    case LayoutMode.Line:
                        orderlayout1_button.IsChecked = false;
                        orderlayout2_button.IsChecked = true;
                        orderlayout3_button.IsChecked = false;

                        SwitchToLine();
                        break;
                    case LayoutMode.Grid:
                        orderlayout1_button.IsChecked = false;
                        orderlayout2_button.IsChecked = false;
                        orderlayout3_button.IsChecked = true;

                        SwitchToGrid();
                        break;
                    default:
                        throw new Exception("unknown layout mode");
                }
                RegisterLayoutCheckedEvents();
            }
        }


        public Thickness ItemMargin
        {
            set; get;
        }

        public Size BigItemSize
        {
            get
            {
                return _big_item_size;
            }
            set
            {
                _big_item_size = value;
                if(_active_layout_mode == LayoutMode.Line)
                {
                    if (_animating_tile_panel != null)
                    {
                        _animating_tile_panel.ItemWidth = _big_item_size.Width;
                        _animating_tile_panel.ItemHeight = _big_item_size.Height;
                    }
                }
            }
        }

        public bool ShowActionsByLeftMouse
        {
            set;
            get;
        }

        public Size SmallItemSize
        {
            get
            {
                return _small_item_size;
            }
            set
            {
                _small_item_size = value;
                if (_active_layout_mode == LayoutMode.Kachel)
                {
                    if (_animating_tile_panel != null)
                    {
                        _animating_tile_panel.ItemWidth = _small_item_size.Width;
                        _animating_tile_panel.ItemHeight = _small_item_size.Height;
                    }
                }
            }
        }

        public double PanelHeight
        {
            get
            {
                //if (_custom_panel_size)
                //{
                //    return submenu_base_canvas.ActualHeight;
                //}

                if(_active_layout_mode == LayoutMode.Line)
                {
                    double height = BigItemSize.Height + sub_header_stack.Height;
                    CheckPanelHeight(ref height);
                    return height;
                }
                else if (_active_layout_mode == LayoutMode.Kachel)
                {
                    double panel_width = GetPanelWidth();

                    int items_per_row = (int)Math.Floor(panel_width / SmallItemSize.Width);
                    int rows = (int) Math.Ceiling((double)_items_filtered.Count/(double)items_per_row);
                    double height = (SmallItemSize.Height * rows) + sub_header_stack.Height;
                    CheckPanelHeight(ref height);
                    return height;
                }
                else if (_active_layout_mode == LayoutMode.Grid)
                {
                    double height = 400;
                    CheckPanelHeight(ref height);
                    return height;
                }
                else
                    throw new Exception("Layout mode unknown");
            }
        }

        protected double GetPanelWidth()
        {
            if (_resize_active_new_size.HasValue)
                return _resize_active_new_size.Value.Width;

            double panel_width = submenu_base_canvas.ActualWidth;
            if(panel_width == 0)
            {
                if (Window != null && Window.ActualWidth > 0)
                    panel_width = Window.ActualWidth;
                else
                    panel_width = 1000;
            }
            return panel_width;
        }


        /// <summary>
        /// dont use this list for writing
        /// instead use method additem
        /// removeitem
        /// </summary>
        public ObservableCollection<SubMenuPanelMenuItem> Items
        {
            get
            {
                return _items;
            }
        }

        public ReadOnlyObservableCollection<SubMenuPanelMenuItem> RootItems
        {
            get
            {
                return _root_items_readonly;
            }
        }

        public bool IsAnimationActive
        {
            get
            {
                return _animation_active;
            }
        }

        
        #endregion

        #region Items Helper Methods
        private void DeselectAllItems()
        {
            foreach (SubMenuPanelMenuItem item in _items)
            {
                item.IsSelected = false;
                item.ManualIsMouseOver = false;
                if (ShowActionsByLeftMouse)
                    item.HideActionMenu = true;
            }
        }
        #endregion

        #region Dependencyproeprty changed callbacks
        
        private static void OnCurrentBreadCrumbItem(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is SubMenuPanel)
            {
                SubMenuPanel panel = ((SubMenuPanel) d);

                if (e.OldValue is SubMenuPanelBreadCrumbItem)
                {
                    SubMenuPanelBreadCrumbItem old_bread_crumb_item = e.OldValue as SubMenuPanelBreadCrumbItem;

                    if (old_bread_crumb_item.MenuItem != null)
                    {
                        panel.DeRegisterSubCollectionChanged(old_bread_crumb_item.MenuItem);
                    }
                }

                if (e.NewValue is SubMenuPanelBreadCrumbItem)
                {
                    SubMenuPanelBreadCrumbItem new_bread_crumb_item = e.NewValue as SubMenuPanelBreadCrumbItem;

                    if (new_bread_crumb_item.MenuItem != null)
                    {
                        panel.RegisterSubCollectionChanged(new_bread_crumb_item.MenuItem);
                    }
                }
            }
        }
        #endregion

        #region MouseClick Simulation

        private void base_grid_StylusDown(object sender, StylusDownEventArgs e)
        {
            
            _stylus_down = true;

            if (_stylus_down_id == null)
            {
                _stylus_down_id = e.StylusDevice.Id;
                _stylusdown_pos = e.GetPosition(base_grid);
            }

            _mouse_down = true;
            _mousedown_pos = e.GetPosition(base_grid);

            e.Handled = true;
        }

        private void base_grid_StylusUp(object sender, StylusEventArgs e)
        {
            return;
            if (_reset_stylus)
            {
                e.Handled = true;
                _reset_stylus = false;
            }

            if(_stylus_down && _stylus_down_id.HasValue && _stylus_down_id.Value == e.StylusDevice.Id)
            {
                e.Handled = true;
                _stylus_down = false;
                _stylus_down_id = null;

                Point current_pos = e.GetPosition(base_grid);

                if ((current_pos - _stylusdown_pos).Length > 5)
                    return;

                _stylusdown_pos = new Point(0,0);

                SubMenuPanelMenuItem item;
                CollisionDetected(current_pos, out item);


                if (item != null)
                {
                    ToggleButton external_tg = null;
                    if (IsExternalToggleButton(item, out external_tg))
                        external_tg.IsChecked = external_tg.IsChecked == null ? false : !external_tg.IsChecked.Value;
                    else
                    {
                        CheckSubNavigation(item, null);
                        RaiseAction(item);
                    }

                    //if (ShowActionsByLeftMouse && e.ChangedButton == MouseButton.Right && e.RightButton == MouseButtonState.Released)
                    //{
                    //    item.HideActionMenu = !item.HideActionMenu;
                    //    item.ManualIsMouseOver = true;
                    //    item.IsHitTestVisible = true;
                    //}
                    //else
                    //{
                    //    // Sonderfall, wenn das Control direkt ein ToggleButton ist. Ist nicht schön. Sollte eigentlich ausgelagert werden
                    //    if ((e.ChangedButton == MouseButton.Left && e.LeftButton == MouseButtonState.Released) ||
                    //        (!ShowActionsByLeftMouse && e.ChangedButton == MouseButton.Right && e.RightButton == MouseButtonState.Released && !item.HasChildren))
                    //    {
                    //        ToggleButton external_tg = null;
                    //        if (IsExternalToggleButton(item, out external_tg))
                    //            external_tg.IsChecked = external_tg.IsChecked == null ? false : !external_tg.IsChecked.Value;
                    //        else
                    //        {
                    //            CheckSubNavigation(item, null);
                    //            RaiseAction(item);
                    //        }
                    //    }

                    //    if (!ShowActionsByLeftMouse && e.ChangedButton == MouseButton.Right && e.RightButton == MouseButtonState.Released)
                    //        CheckSubNavigation(item, null);
                    //}
                }
            }
        }

        private void base_grid_StylusMove(object sender, StylusEventArgs e)
        {
            
            if (_stylus_down && _stylus_down_id.HasValue && _stylus_down_id.Value == e.StylusDevice.Id)
            {
                e.Handled = true;
                Point current_pos = e.GetPosition(base_grid);

                Vector pos_diff = current_pos - _last_mousemove_pos;

                if (pos_diff.X == 0 && pos_diff.Y == 0)
                    return;

                if( (current_pos - _stylusdown_pos).Length > 5)
                {
                    _stylus_down = false;
                    _stylus_down_id = null;
                    _reset_stylus = true;
                }

                _last_mousemove_pos = current_pos;
            }
        }

        private void base_grid_MouseUp(object sender, MouseButtonEventArgs e)
        {
            Point pos = e.GetPosition(base_grid);

            // Bei einer großen Positionsveränderung, wird das Mouseup ignoriert und nicht an die Elemente weitergereicht
            if ((_mousedown_pos - pos).Length > 5 && _mouse_down)
            {
                _mouse_down = false;
                _stylus_down = false;
                _stylus_down_id = null;
                return;
            }
            _stylus_down = false;
            _stylus_down_id = null;
            _mouse_down = false;
            SubMenuPanelMenuItem item;
            CollisionDetected(pos, out item);            
            

            if (item != null)
            {
                if (ShowActionsByLeftMouse && e.ChangedButton == MouseButton.Right && e.RightButton == MouseButtonState.Released)
                {
                    item.HideActionMenu = !item.HideActionMenu;
                    item.ManualIsMouseOver = true;
                    item.IsHitTestVisible = true;
                }
                else
                {
                    // Sonderfall, wenn das Control direkt ein ToggleButton ist. Ist nicht schön. Sollte eigentlich ausgelagert werden
                    if ((e.ChangedButton == MouseButton.Left && e.LeftButton == MouseButtonState.Released) ||
                        (!ShowActionsByLeftMouse && e.ChangedButton == MouseButton.Right && e.RightButton == MouseButtonState.Released && !item.HasChildren))
                    {
                        ToggleButton external_tg = null;
                        if (IsExternalToggleButton(item, out external_tg))
                            external_tg.IsChecked = external_tg.IsChecked == null ? false : !external_tg.IsChecked.Value;
                        else
                        {
                            CheckSubNavigation(item, null);
                            RaiseAction(item);
                        }
                    }

                    if (!ShowActionsByLeftMouse && e.ChangedButton == MouseButton.Right && e.RightButton == MouseButtonState.Released)
                        CheckSubNavigation(item, null);
                }
            }
        }

        private bool IsExternalToggleButton(SubMenuPanelMenuItem item , out ToggleButton o_tg)
        {
            o_tg = null;

            if(item.Child is Grid)
            {
                Grid g = item.Child as Grid;
                if (g.Children.Count > 0)
                {
                    foreach (var child in g.Children)
                    {
                        if (child is ToggleButton)
                            o_tg = child as ToggleButton;
                    }
                }
            }

            return o_tg != null;
        }

        private void base_grid_MouseMove(object sender, MouseEventArgs e)
        {
            Point pos = e.GetPosition(base_grid);
            Vector pos_diff = pos - _last_mousemove_pos;
            
            if (pos_diff.X == 0 && pos_diff.Y == 0)
                return;

            SubMenuPanelMenuItem item;
            CollisionDetected(pos, out item);
            if (item != null)
                submenu_base_grid.Cursor = Cursors.Hand;
            else
                submenu_base_grid.Cursor = Cursors.ScrollAll;

            if (!_mouse_down) // IsMouseOver setter
            {
                if (item != _last_hover_element && _last_hover_element != null)
                    _last_hover_element.ManualIsMouseOver = false;

                if(item != null)
                {
                    _last_hover_element = item;
                    if (!ShowActionsByLeftMouse)
                    {
                        _last_hover_element.IsHitTestVisible = true;
                    }                   
                    
                    _last_hover_element.ManualIsMouseOver = true;
                }
            }

            _last_mousemove_pos = pos;
        }

        private void base_grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            _mouse_down = true;
            _mousedown_pos = e.GetPosition(base_grid);
        }

        private void base_grid_MouseLeave(object sender, MouseEventArgs e)
        {
            _mouse_down = false;
        }

        private void CollisionDetected(Point rel_mouse_pos, out SubMenuPanelMenuItem found_item)
        {
            Vector2d mouse_pos = new Vector2d(rel_mouse_pos.X, rel_mouse_pos.Y);
            found_item = null;

            foreach (SubMenuPanelMenuItem sub_menu_panel_menu_item in _items_filtered)
            {
                Point elem_pos = UIHelper.GetElementPosition(sub_menu_panel_menu_item, base_grid);


                double bottom = elem_pos.Y + sub_menu_panel_menu_item.ActualHeight * _map_zp.ScaleTransform.ScaleY;
                double right = elem_pos.X + sub_menu_panel_menu_item.ActualWidth * _map_zp.ScaleTransform.ScaleX;

                BoundingBox bbox = new BoundingBox(new Vector2d(elem_pos.X, elem_pos.Y), new Vector2d(right, bottom));

                if (found_item == null && bbox.PointInside(mouse_pos))
                {
                    found_item = sub_menu_panel_menu_item;
                }
                else if (ShowActionsByLeftMouse)
                    sub_menu_panel_menu_item.HideActionMenu = true;
                else
                    sub_menu_panel_menu_item.HideActionMenu = false;

                //sub_menu_panel_menu_item.IsSelected = false;
                
            }
        }

        void _items_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (_ignore_collection_change)
                return;

            if(CurrentBreadCrumbItem == _root_bread_crumb_item)
            {
                // if the collection of the root was modified
                // sync the collection changes with the root store
                if(e.NewItems != null)
                {
                    foreach(SubMenuPanelMenuItem new_item in e.NewItems)
                    {
                        if(!_root_items.Contains(new_item))
                            _root_items.Add(new_item);
                    }
                }

                if (e.OldItems != null)
                {
                    foreach (SubMenuPanelMenuItem del_item in e.OldItems)
                    {
                        if (_root_items.Contains(del_item))
                            _root_items.Remove(del_item);
                    }
                }
            }
            else
            {
                // if a sub item's collection has been modified
                // sync the changes to the submenu item
                if (CurrentBreadCrumbItem != null && CurrentBreadCrumbItem.MenuItem != null)
                {
                    if (e.NewItems != null)
                    {
                        foreach (SubMenuPanelMenuItem new_item in e.NewItems)
                        {
                            if (!CurrentBreadCrumbItem.MenuItem.Children.Contains(new_item))
                                CurrentBreadCrumbItem.MenuItem.Children.Add(new_item);
                        }
                    }

                    if (e.OldItems != null)
                    {
                        foreach (SubMenuPanelMenuItem del_item in e.OldItems)
                        {
                            if (CurrentBreadCrumbItem.MenuItem.Children.Contains(del_item))
                                CurrentBreadCrumbItem.MenuItem.Children.Remove(del_item);
                        }
                    }
                }
            }
        }

        private void DeRegisterSubCollectionChanged(SubMenuPanelMenuItem sub_item)
        {
            if(sub_item == null)
                return;

            sub_item.Children.CollectionChanged -= SubMenuItem_Children_CollectionChanged;
        }

        private void RegisterSubCollectionChanged(SubMenuPanelMenuItem sub_item)
        {
            if(sub_item == null)
                return;

            sub_item.Children.CollectionChanged += new System.Collections.Specialized.NotifyCollectionChangedEventHandler(SubMenuItem_Children_CollectionChanged);
        }

        void SubMenuItem_Children_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
 	        _ignore_collection_change = true;

            // synchronize submenu item children with current items
            if(e.NewItems != null)
                {
                    foreach(SubMenuPanelMenuItem new_item in e.NewItems)
                    {
                        if(!_items.Contains(new_item))
                        {
                            AddItem(new_item);
                            //_items.Add(new_item);

                            //if (ItemCountFilterExpression == null)
                            //{
                            //    ItemCount = ItemCount + 1;
                            //}
                            //else
                            //{
                            //    // if a IteCountFilterExpression has been set
                            //    // check if the current item 
                            //    if(ItemCountFilterExpression(new_item))
                            //    {
                            //        ItemCount = ItemCount + 1;
                            //    }
                            //}

                            //if (ShowActionsByLeftMouse)
                            //    new_item.HideActionMenu = true;
                            //else
                            //    new_item.HideActionMenu = false;

                            //GenerateDataGridColumns();
            
                            //AdjustCustomFilters(new_item);

                            //if (!WithSearchField || string.IsNullOrEmpty(search_cbx.Text) || IsFiltered(new_item))
                            //{
                            //    _items_filtered.Add(new_item);
                            //    _items_filtered.Sort((x, y) => x.CompareTo(y));
                            //}

                            //if (ItemAdded != null)
                            //    ItemAdded(this, new MenuItemManagementEventArgs(new_item));

                            //UpdateMapZPBorders();
                        }
                    }
                }

                if (e.OldItems != null)
                {
                    foreach (SubMenuPanelMenuItem del_item in e.OldItems)
                    {
                        if (_items.Contains(del_item))
                        {
                            RemoveItem(del_item);
                            //_items.Remove(del_item);

                            //if (ItemCountFilterExpression == null)
                            //{
                            //    ItemCount = ItemCount - 1;
                            //}
                            //else
                            //{
                            //    // if a IteCountFilterExpression has been set
                            //    // check if the current item 
                            //    if(ItemCountFilterExpression(del_item))
                            //    {
                            //        ItemCount = ItemCount - 1;
                            //    }
                            //}

                            //if (!WithSearchField || string.IsNullOrEmpty(search_cbx.Text) || IsFiltered(del_item))
                            //{
                            //    if(_items_filtered.Contains(del_item))
                            //    {
                            //        _items_filtered.Remove(del_item);
                            //        _items_filtered.Sort((x, y) => x.CompareTo(y));
                            //    }
                            //}

                            //UpdateMapZPBorders();
                        }
                    }
                }

            _ignore_collection_change = false;
        }

        public void NavigateToSubItems(SubMenuPanelMenuItem menu_item)
        {
            if (!_items.Contains(menu_item))
                return;

            SubMenuPanelBreadCrumbItem bread_crumb_item = _breadcrumb_path.SingleOrDefault(i => i.MenuItem == menu_item);

            Mouse.OverrideCursor = Cursors.Wait;

            if(bread_crumb_item != null)
            {
                // item existiert schon
                _ignore_collection_change = true;

                _ignore_collection_change = true;

                RemoveAllItemsOfCurrentBreadcrumbLayer();

                foreach (SubMenuPanelMenuItem root_item in bread_crumb_item.MenuItem.Children)
                    AddItem(root_item);


                CurrentBreadCrumbItem = bread_crumb_item;

                _ignore_collection_change = false;
            }
            else
            {
                if (menu_item != null)
                {
                    SubMenuPanelBreadCrumbItem new_bread_crumb_item = new SubMenuPanelBreadCrumbItem(this, menu_item.ToString(), menu_item);

                    _ignore_collection_change = true;

                    RemoveAllItemsOfCurrentBreadcrumbLayer();


                    foreach (SubMenuPanelMenuItem root_item in menu_item.Children)
                        AddItem(root_item);


                    //ApplyFilterText(_was_custom_filter);
                    //ApplySort();

                    _breadcrumb_path.Add(new_bread_crumb_item);

                    CurrentBreadCrumbItem = new_bread_crumb_item;
                    lb_breadcrumb.ItemsSource = _breadcrumb_path;

                    _ignore_collection_change = false;
                }
            }

            Mouse.OverrideCursor = null;

            UpdateBreadCrumbVisibility();
            SetSearchFilter(string.Empty); // clear search filter
            SendNavigationDepthChanged();
        }

        private void BreadCrumbButton_Click(object sender, RoutedEventArgs e)
        {
            Button sender_btn = sender as Button;

            if(sender_btn != null)
            {
                SubMenuPanelBreadCrumbItem bread_crumb_item = sender_btn.CommandParameter as SubMenuPanelBreadCrumbItem;

                Mouse.OverrideCursor = Cursors.Wait;

                if(bread_crumb_item != null)
                {
                    if(bread_crumb_item == _root_bread_crumb_item)
                    {
                        _ignore_collection_change = true;

                        RemoveAllItemsOfCurrentBreadcrumbLayer();


                        foreach (SubMenuPanelMenuItem root_item in _root_items)
                            AddItem(root_item);

                        SetSearchFilter(string.Empty); // clear search filter
                        ApplyFilterText(false);
                        ApplySort();

                        CurrentBreadCrumbItem = _root_bread_crumb_item;

                        _ignore_collection_change = false;
                    }
                    else
                    {
                        _ignore_collection_change = true;

                        _items.Clear();
                        _items_filtered.Clear();

                        if (bread_crumb_item.MenuItem != null)
                        {
                            RemoveAllItemsOfCurrentBreadcrumbLayer();

                            foreach (SubMenuPanelMenuItem root_item in bread_crumb_item.MenuItem.Children)
                                AddItem(root_item);


                            //ApplyFilterText(_was_custom_filter);
                            //ApplySort();
                        }

                        CurrentBreadCrumbItem = bread_crumb_item;

                        _ignore_collection_change = false;
                    }
                }

                Mouse.OverrideCursor = null;

                UpdateBreadCrumbVisibility();
                SetSearchFilter(string.Empty); // clear search filter
                SendNavigationDepthChanged();
            }
        }
        #endregion

        #region Attributes
        protected delegate void NoParas();
        private bool _resize_active;
        private bool _animation_active;
        private bool _autohide_timer_after_action;
        private bool _ignore_autohide_after_action;
        //private bool _resize_active;

        private Timer _auto_hide_delay_timer = null;
        private double _auto_hide_timer_interval = 450;

        private DoubleAnimation _submenu_fadein_da;
        private DoubleAnimation _submenu_fadeout_da;
        //private bool _custom_panel_size;
        private double _start_height_resize;
        private Point _submenu_resize_start_pos;
        private bool _with_auto_hide;
        private LayoutMode _active_layout_mode;

        private Size? _resize_active_new_size;

        public event EventHandler FadeOutCompleted;
        public event EventHandler FadeInCompleted;
        public event EventHandler<MenuItemActionEventArgs> Action;
        public event EventHandler NewElemClick;
        public event EventHandler NavigationDepthChanged;
        public event EventHandler CustomButtonClick;
        public event EventHandler CustomButton1Click;
        public event EventHandler SearchButtonClick;
        public event EventHandler<MenuItemManagementEventArgs> ItemRemoved;
        public event EventHandler<MenuItemManagementEventArgs> ItemAdded;

        protected Size _big_item_size;
        protected Size _small_item_size;

        protected ObservableCollection<SubMenuPanelMenuItem> _items = new ObservableCollection<SubMenuPanelMenuItem>();
        protected ObservableCollection<SubMenuPanelMenuItem> _items_filtered = new ObservableCollection<SubMenuPanelMenuItem>();
        protected ObservableCollection<SubMenuPanelMenuItem> _root_items = new ObservableCollection<SubMenuPanelMenuItem>();
        protected ReadOnlyObservableCollection<SubMenuPanelMenuItem> _root_items_readonly = null;

        protected AnimatingTilePanel _animating_tile_panel;

        private bool _with_navigation_buttons = true;

        protected MapZP _map_zp;
        protected DoubleAnimation _scale_x_reset;
        protected DoubleAnimation _scale_y_reset;
        protected DoubleAnimation _transl_x_reset;
        protected DoubleAnimation _transl_y_reset;

        protected Point _mousedown_pos;
        private bool _mouse_down = false;
        private SubMenuPanelMenuItem _last_hover_element = null;
        private Point _last_mousemove_pos = new Point();

        private bool _stylus_down = false;
        private int? _stylus_down_id = null;
        protected Point _stylusdown_pos;
        private bool _reset_stylus = false;

        private List<string> _custom_filter_values = new List<string>();
        private bool _grid_columns_generated = false;
        private bool _comes_from_parent_row = false;
        private bool _ignore_grid_selection_change = false;
        private string _last_action_button_name = "";

        private bool _was_custom_filter = false;
        private bool _ignore_collection_change = false;
        private string _taskbar_item_name = string.Empty;
        private SubMenuPanelBreadCrumbItem _root_bread_crumb_item = null;
        private ObservableCollection<SubMenuPanelBreadCrumbItem> _breadcrumb_path = new ObservableCollection<SubMenuPanelBreadCrumbItem>();
        #endregion   

        
      

    }
}
