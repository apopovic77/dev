﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using SlimDX.D3DCompiler;
using SlimDX.Direct3D10;
using Device = SlimDX.Direct3D10.Device;

namespace FxSyncManager
{
    public partial class EffectSyncManager : Form
    {
        public EffectSyncManager()
        {
            InitializeComponent();
        }

        private void FxSyncManager_Load(object sender, EventArgs e)
        {
            _log_box.Items.Clear();
            _fx_files.Items.Clear();

            if (!Directory.Exists(_path_to_fx_files))
            {
                MessageBox.Show("Path to FX files invalid: " + _path_to_fx_files);
                Application.Exit();
                return;
            }

            _fx_file_paths = Directory.GetFiles(_path_to_fx_files, _dx10_only.Checked ? "*10*.fx" : "*.fx");

            foreach (string fx_file in _fx_file_paths)
            {
                string filename = Path.GetFileName(fx_file);
                _fx_files.Items.Add(filename);
            }

            _select_all.Checked = true;
            _select_all_CheckedChanged(null, null);
        }

        VarType GetVarTypeFromEffectVar(EffectVariable variable)
        {
            if (variable.AsMatrix() != null)
                return VarType.Matrix;

            if (variable.AsResource() != null)
                return VarType.Resource;

            if (variable.AsScalar() != null)
                return VarType.Scalar;

            if (variable.AsVector() != null)
                return VarType.Vector;

            return VarType.None;
        }

        string GetCSharpTypeFromVarType(VarType type)
        {
            switch (type)
            {
                case VarType.Matrix:
                    return "LdxEffectMatrixVariable";

                case VarType.Resource:
                    return "LdxEffectTextureResourceVariable";

                case VarType.Scalar:
                    return "LdxEffectScalarVariable";

                case VarType.Vector:
                    return "LdxEffectVectorVariable";

                default:
                    throw new ArgumentOutOfRangeException("type");
            }
        }

        enum VarType
        {
            None,
            Matrix,
            Resource,
            Scalar,
            Vector,
        }

        struct VarInfo
        {
            public string Name;
            public VarType Type;
        }

        private void _update_button_Click(object sender, EventArgs e)
        {
            _log_box.Items.Clear();
            _log_box.Items.Add("Started ... " + DateTime.Now);

            Device device = new Device(DeviceCreationFlags.None);

            string compilation_errors = "";

            foreach (int selected in _fx_files.SelectedIndices)
            {
                Effect fx;
                string fx_path = _fx_file_paths[selected];
                string cs_path = Path.GetFileNameWithoutExtension(fx_path);

                if (cs_path.EndsWith("10"))
                    cs_path = cs_path.Remove(cs_path.Length - 2);

                string[] cs_candidates = Directory.GetFiles(_path_to_fx_files, cs_path + "*.cs");

                if (cs_candidates.Length == 0)
                {
                    _log_box.Items.Add("Error: Unable to find matching C# file for: " + fx_path);
                    continue;
                }

                // take the first C# files that matched
                cs_path = Path.Combine(_path_to_fx_files, cs_candidates[0]);

                // find HLSL macros / #define constants from shader file
                List<string> macro_names = new List<string>();
                List<ConstInfo> constants = new List<ConstInfo>();
                FindMacros(fx_path, ref macro_names, ref constants);

                List<ShaderMacro> d3d_macros = new List<ShaderMacro>();

                foreach (string macro_name in macro_names)
                    d3d_macros.Add(new ShaderMacro(macro_name, "1"));

                try
                {
                    fx = Effect.FromFile(device, fx_path, "fx_4_0", ShaderFlags.EnableBackwardsCompatibility, EffectFlags.None, null, null, d3d_macros.ToArray(), out compilation_errors);
                }
                catch (Exception ex)
                {
                    if (compilation_errors.Length > 0 && _show_warnings.Checked)
                        AddMultilineStringToLog(compilation_errors);

                    AddMultilineStringToLog(ex.ToString());
                    continue;
                }

                if (compilation_errors.Length > 0 && _show_warnings.Checked)
                    AddMultilineStringToLog(compilation_errors);

                if (!File.Exists(cs_path))
                {
                    _log_box.Items.Add("Error: Unable to open C# file: " + cs_path);
                    continue; 
                }

                TextReader rd = new StreamReader(cs_path);
                string cs_content = rd.ReadToEnd();
                rd.Close();
                rd.Dispose();

                int startIndex = cs_content.IndexOf(_decl_prefix);
                int endIndex = cs_content.IndexOf(_decl_suffix);

                if (startIndex < 0)
                {
                    _log_box.Items.Add(cs_path + " - Error: Start tag at C# code is missing (" + _decl_prefix +")");
                    continue;
                }

                if (endIndex < 0)
                {
                    _log_box.Items.Add(cs_path + " - Error: End tag at C# code is missing (" + _decl_suffix + ")");
                    continue;
                }

                startIndex += _decl_prefix.Length;

                string prefix = cs_content.Substring(0, startIndex);
                string suffix = cs_content.Substring(endIndex, cs_content.Length - endIndex);
                string decl = "\r\n";

                List<VarInfo> vars = new List<VarInfo>();

                for (int i = 0; i < fx.Description.GlobalVariableCount; ++i)
                {
                    EffectVariable var = fx.GetVariableByIndex(i);
                    vars.Add(new VarInfo { Type = GetVarTypeFromEffectVar(var), Name = var.Description.Name });
                }

                List<EffectTechnique> techniques = new List<EffectTechnique>();

                for (int i = 0; i < fx.Description.TechniqueCount; ++i)
                {
                    EffectTechnique tech = fx.GetTechniqueByIndex(i);
                    techniques.Add(tech);
                }

                // generate the C# code
                ListConstants(constants, ref decl);
                ListMacros(macro_names, ref decl);
                ListTechniques(techniques, ref decl);
                ListVariables(vars, ref decl);
                decl += "\t\t";

                // write the updated C# code to the file
                TextWriter wr = new StreamWriter(cs_path);
                wr.Write(prefix + decl + suffix);
                wr.Close();
            }

            device.Dispose();

            _log_box.Items.Add("Finished ... " + DateTime.Now);
        }

        private void AddMultilineStringToLog(string multiline_message)
        {
            string[] lines = multiline_message.Split(new[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string line in lines)
                _log_box.Items.Add(line);
        }

        #region Automatically determine HLSL Macros
        string[] preproc_directives = 
        {
            "#if",
            "#ifdef",
            "#ifndef",
            "#elif",
        };

        private struct ConstInfo
        {
            public string Name;
            public string Value;
        }

        private void FindMacros(string hlsl_path, ref List<string> macro_names, ref List<ConstInfo> constants)
        {
            if (!Path.IsPathRooted(hlsl_path))
                hlsl_path = _path_to_fx_files + hlsl_path;

            if (!File.Exists(hlsl_path))
            {
                _log_box.Items.Add("Error: Failed to include " + hlsl_path);
                return;
            }

            TextReader rd = new StreamReader(hlsl_path);
            string line;
            while ((line = rd.ReadLine()) != null)
            {
                HandleShaderLine(line, ref macro_names, ref constants);
            }

            rd.Close();
            rd.Dispose();
        }

        private void HandleShaderLine(string line, ref List<string> macro_names, ref List<ConstInfo> constants)
        {
            List<string> tokens = new List<string>(line.Split(new[] { ' ', '\t', '(', ')', }, StringSplitOptions.RemoveEmptyEntries));

            // handle #include directive
            if (tokens.Count > 0 && tokens[0] == "#include")
                FindMacros(tokens[1].Replace("\"", ""), ref macro_names, ref constants);

            int macro_index = -1;

            // determine if this line contains a constant definition
            if (tokens.Count > 2 && tokens[0] == "#define")
            {
                if (constants.Where(c => c.Name == tokens[1]).Count() == 0)
                    constants.Add(new ConstInfo { Name = tokens[1], Value = tokens[2] });
            }
            // determine if this line contains any conditional preprocessor directive
            else if (preproc_directives.Any(ppd => (macro_index = tokens.IndexOf(ppd)) >= 0))
            {
                if (tokens[macro_index + 1] == "defined" || tokens[macro_index + 1] == "!defined")
                {
                    for (int i = macro_index + 1; i < tokens.Count; ++i)
                    {
                        if (tokens[i] == "defined" || tokens[i] == "!defined")
                            AddUniqueToList(ref macro_names, tokens[++i]);
                    }
                }
                else
                    AddUniqueToList(ref macro_names, tokens[macro_index + 1]);
            }
        }

        private void AddUniqueToList(ref List<string> name_list, string name)
        {
            // ignore macros starting with underscores and already added ones
            if (!name.StartsWith("__") && !name_list.Contains(name))
                name_list.Add(name);
        }
        #endregion

        #region Write C# Code
        #region Macros & Constants
        private string FindConstantType(string constant_value)
        {
            bool bool_value;
            int int_value;
            float float_value;

            if (bool.TryParse(constant_value, out bool_value))
                return "bool";

            if (int.TryParse(constant_value, out int_value))
                return "int";

            if (float.TryParse(constant_value, out float_value))
                return "float";

            return "string";
        }

        private void ListConstants(List<ConstInfo> constants, ref string decl)
        {
            if (constants.Count > 0)
                decl += "\r\n\t\t#region Constants\r\n";

            foreach (ConstInfo c in constants)
            {
                decl += "\t\tpublic const " + FindConstantType(c.Value) + " " + c.Name + " = " + c.Value + ";\r\n";
            }

            if (constants.Count > 0)
                decl += "\t\t#endregion // Constants\r\n";
        }

        private void ListMacros(List<string> macros, ref string decl)
        {
            if (macros.Count > 0)
                decl += "\r\n\t\t#region Macros\r\n";

            foreach (string m in macros)
            {
                decl += "\t\tpublic LdxEffectMacro " + m + ";\r\n";
            }

            if (macros.Count > 0)
                decl += "\t\t#endregion // Macros\r\n";
        }
        #endregion

        #region Techniques
        private void ListTechniques(List<EffectTechnique> techniques, ref string decl)
        {
            if (techniques.Count > 0)
                decl += "\r\n\t\t#region Techniques\r\n";

            foreach (EffectTechnique t in techniques)
            {
                decl += "\t\tpublic LdxEffectTechnique " + t.Description.Name + ";\r\n";
            }

            if (techniques.Count > 0)
                decl += "\t\t#endregion // Techniques\r\n";
        }
        #endregion

        #region Variables
        private List<VarInfo> FilterVariables(List<VarInfo> variables, VarType type)
        {
            return (from v in variables
                    where v.Type == type
                    orderby v.Name
                    select v).ToList();
        }

        private void ListVariables(List<VarInfo> variables, ref string decl)
        {
            List<VarInfo> matrices = FilterVariables(variables, VarType.Matrix);
            List<VarInfo> resources = FilterVariables(variables, VarType.Resource);
            List<VarInfo> scalars = FilterVariables(variables, VarType.Scalar);
            List<VarInfo> vectors = FilterVariables(variables, VarType.Vector);

            if (matrices.Count > 0 || resources.Count > 0 || scalars.Count > 0 || vectors.Count > 0)
                decl += "\r\n\t\t#region Variables\r\n";

            ListVariablesEx(matrices, "Matrix Variables", ref decl);
            ListVariablesEx(resources, "Resource/Texture Variables", ref decl);
            ListVariablesEx(scalars, "Scalar Variables", ref decl);
            ListVariablesEx(vectors, "Vector Variables", ref decl);

            if (matrices.Count > 0 || resources.Count > 0 || scalars.Count > 0 || vectors.Count > 0)
                decl += "\r\n\t\t#endregion // Variables\r\n\r\n";
        }

        private void ListVariablesEx(List<VarInfo> variables, string region_name, ref string decl)
        {
            if (variables.Count > 0)
                decl += "\r\n\t\t#region " + region_name + "\r\n";

            foreach (VarInfo v in variables)
            {
                decl += "\t\tpublic " + GetCSharpTypeFromVarType(v.Type) + " " + v.Name + ";\r\n";
            }

            if (variables.Count > 0)
                decl += "\t\t#endregion // " + region_name + "\r\n";
        }
        #endregion
        #endregion // Write C# Code

        private void _tags_to_clipboard_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(_decl_prefix + "\r\n\t\t" + _decl_suffix);
        }

        private void _select_all_CheckedChanged(object sender, EventArgs e)
        {
            for (int i = 0; i < _fx_files.Items.Count; ++i)
                _fx_files.SetSelected(i, _select_all.Checked);
        }

        private void _dx10_only_CheckedChanged(object sender, EventArgs e)
        {
            FxSyncManager_Load(null, null);
        }

        private void EffectSyncManager_Resize(object sender, EventArgs e)
        {
            if (FormWindowState.Minimized == WindowState)
                Hide();
        }

        private void _tray_icon_DoubleClick(object sender, EventArgs e)
        {
            Show();
            WindowState = FormWindowState.Normal;
        }

        private void _log_box_DoubleClick(object sender, EventArgs e)
        {
            string item_string = _log_box.SelectedItem.ToString();

            int filepath_end = -1;

            if (filepath_end < 0)
            {
                filepath_end = item_string.IndexOf(".fx");

                if (filepath_end >= 0)
                    filepath_end += 3; // .fx
            }

            if (filepath_end < 0)
            {
                filepath_end = item_string.IndexOf(".cs");

                if (filepath_end >= 0)
                    filepath_end += 3; // .cs
            }

            if (filepath_end < 0)
            {
                filepath_end = item_string.IndexOf(".cpp");

                if (filepath_end >= 0)
                    filepath_end += 4; // .cpp
            }

            // no file path found, do nothing
            if (filepath_end < 0)
                return;

            string abs_path = null;

            // search backwards through the string to find a valid file path
            for (int i = filepath_end; i >= 0; --i)
            {
                string path_candidate = item_string.Substring(i, filepath_end - i);
                if (File.Exists(path_candidate))
                {
                    abs_path = path_candidate;
                    break;
                }

                if (File.Exists(_path_to_fx_files + path_candidate))
                {
                    abs_path = _path_to_fx_files + path_candidate;
                    break;
                }
            }

            // a path was found, open the file with visual studio
            if (abs_path != null)
            {
                Process.Start(@"C:\Program Files (x86)\Microsoft Visual Studio 10.0\Common7\IDE\devenv.exe","" + abs_path + " /command \"edit.goto 10\"");
            }
        }

        private const string _decl_prefix = "#region Effect Attributes // (machine-generated code)";
        private const string _decl_suffix = "#endregion // Effect Attributes (machine-generated code)";

        private string[] _fx_file_paths;
        private const string _path_to_fx_files = @"C:\logicxklu\Projekte\Intern\010-LxFramework\130-WpfDxEngine\5-Impl\c#\Libs\LdxCoreLib\Dx\Fx\";
    }
}
