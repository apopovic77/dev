﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Logicx.WpfUtility.ZoomingPanning;

namespace AtomApp
{

    public enum PanelType
    {
        Stack,
        Wrap

    }

    public class PhysicalObject
    {
        public enum DimType
        {
            Radius,
            Mass,
            Fläche
        }

        public void AddRadiusToCanvas(Panel panel, double scale)
        {
            AddRadialToCanvas(panel, Name, Radius, scale, Color);
        }

        public void AddMassToCanvas(Panel panel, double scale)
        {
            AddRadialToCanvas(panel, Name, Mass, scale, Color);
        }

        public void AddAreaToCanvas(Panel panel, double scale)
        {
            AddRadialToCanvas(panel, Name, Area, scale, Color);
        }

        public static void AddRadialToCanvas(Panel panel,string s_title, double radius, double scale, Color color)
        {
            double width = radius * scale;
            double height = width;

            Ellipse rad_ellipse = new Ellipse();
            rad_ellipse.Width = width;
            rad_ellipse.Height = height;
            rad_ellipse.Fill = new SolidColorBrush(color);
            rad_ellipse.HorizontalAlignment = HorizontalAlignment.Center;

            TextBlock title = new TextBlock();
            title.Text = s_title;
            title.HorizontalAlignment = HorizontalAlignment.Center;
            title.TextAlignment = TextAlignment.Center;
            Canvas.SetLeft(title, width / 2);
            Canvas.SetTop(title, height / 2);

            StackPanel sp2 = new StackPanel();
            sp2.Orientation = Orientation.Vertical;
            sp2.Margin = new Thickness(10,0,10,0);
            sp2.VerticalAlignment = VerticalAlignment.Center;
            sp2.Children.Add(rad_ellipse);
            sp2.Children.Add(title);

            panel.Children.Add(sp2);
        }


        public string Name;
        public double Radius;
        public double Mass;
        public Color Color = Colors.Red;
        public double Area;
        public double Einwohner;
    }

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            test();

            tab_control.SelectionChanged += tab_control_SelectionChanged;
            CompositionTarget.Rendering += CompositionTarget_Rendering;
        }


        void test()
        {

            List<PhysicalObject> planeten = new List<PhysicalObject>();
            planeten.Add(_sun);
            planeten.Add(_merkur);
            planeten.Add(_venus);
            planeten.Add(_earth);
            planeten.Add(_moon);
            planeten.Add(_mars);
            planeten.Add(_jupiter);
            planeten.Add(_saturn);
            planeten.Add(_uranus);
            planeten.Add(_neptun);
            AddTabbedITem("planeten", planeten, Math.Pow(10, -6), PhysicalObject.DimType.Radius);


            List<PhysicalObject> atom = new List<PhysicalObject>();
            atom.Add(_electron);
            atom.Add(_proton);
            AddTabbedITem("Atom", atom, Math.Pow(10, 31), PhysicalObject.DimType.Mass);

            List<PhysicalObject> precision = new List<PhysicalObject>();
            precision.Add(_singlefloat);
            precision.Add(_doublefloat);
            AddTabbedITem("FloatingPoint", precision, Math.Pow(10, -8), PhysicalObject.DimType.Fläche);

            List<PhysicalObject> galaxies = new List<PhysicalObject>();
            galaxies.Add(_milkyway);
            galaxies.Add(_andromedar);
            galaxies.Add(_sonnensystem);
            AddTabbedITem("Galaxies", galaxies, Math.Pow(10, -18), PhysicalObject.DimType.Radius);

            List<PhysicalObject> land_custom = new List<PhysicalObject>();
            land_custom.Add(_österreich);
            land_custom.Add(_usa);
            land_custom.Add(_europa);
            land_custom.Add(_asien);
            AddTabbedITem("Österreich", land_custom, Math.Pow(10, -6), PhysicalObject.DimType.Fläche);

            //CountryImporter importer = new CountryImporter();
            //AddTabbedITem("Land", importer.Import(), Math.Pow(10, -4), PhysicalObject.DimType.Fläche, PanelType.Wrap);

        }

        void AddTabbedITem(string title, List<PhysicalObject> objects, double scale, PhysicalObject.DimType dim_type, PanelType panel_type = PanelType.Stack)
        {
            TabItem ti = new TabItem();
            ti.Background = Brushes.White;
            ti.Header = title;

            tab_control.Items.Add(ti);
            tab_control.Background = Brushes.White;

            Canvas canvas = new Canvas();
            ti.Content = canvas;

            StackPanel sp = new StackPanel();
            sp.Orientation = Orientation.Horizontal;
            canvas.Children.Add(sp);

            WrapPanel wrap_panel = new WrapPanel();
            Binding b = new Binding("ActualWidth");
            b.Source = this;
            wrap_panel.SetBinding(WrapPanel.WidthProperty, b);
            canvas.Children.Add(wrap_panel);


            Panel panel;
            switch (panel_type)
            {
                case PanelType.Stack:
                    panel = sp;
                    break;
                case PanelType.Wrap:
                    panel = wrap_panel;
                    break;
                default:
                    throw new ArgumentOutOfRangeException("panel_type");
            }
            AddRadialObject(panel, objects, scale, dim_type);
        }


        static void AddRadialObject(Panel panel, List<PhysicalObject> rads, double scale, PhysicalObject.DimType dim_type )
        {
            foreach (PhysicalObject radial_object in rads)
            {
                switch (dim_type)
                {
                    case PhysicalObject.DimType.Radius:
                        radial_object.AddRadiusToCanvas(panel, scale);
                        break;
                    case PhysicalObject.DimType.Mass:
                        radial_object.AddMassToCanvas(panel, scale);
                        break;
                    case PhysicalObject.DimType.Fläche:
                        radial_object.AddAreaToCanvas(panel, scale);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException("dim_type");
                }
            }
        }


        void tab_control_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (_map_zp != null)
                _map_zp.DeRegisterEventHandler();
            _map_zp = new MapZP(this, (Canvas)((TabItem)tab_control.SelectedItem).Content);
            _map_zp.DeRegisterEventHandler();
            _map_zp.RegisterEventHandler();
        }



        void CompositionTarget_Rendering(object sender, EventArgs e)
        {
            if(_map_zp != null)
                _map_zp.Update();
        }

        private MapZP _map_zp;


        


        private static double _radius_elektron = 2.8179403267 * Math.Pow(10, -15);
        private static double _radius_proton = 0.8414 * Math.Pow(10, -15);

        private static double _mass_proton = 1.672621777 * Math.Pow(10, -27);
        private static double _mass_electron = 9.10938291 * Math.Pow(10, -31);

        private static double _radius_moon = 3476 * Math.Pow(10, 3);
        private static double _radius_earth = 6371 * Math.Pow(10, 3);
        private static double _radius_sun = 695800 * Math.Pow(10, 3);

        private PhysicalObject _electron = new PhysicalObject()
        {
            Name = "Elektron",
            Radius = _radius_elektron,
            Mass = _mass_electron,
        };

        private PhysicalObject _proton = new PhysicalObject()
        {
            Name = "Proton",
            Radius = _radius_proton,
            Mass = _mass_proton,
            Color = Colors.Blue,
        };

        private PhysicalObject _moon = new PhysicalObject()
        {
            Name = "Mond",
            Radius = _radius_moon,
            Mass = 7.349 * Math.Pow(10, 22),
            Color = Colors.DarkGray,
        };

        private PhysicalObject _earth = new PhysicalObject()
        {
            Name = "Erde",
            Radius = _radius_earth,
            Mass = 5.972 * Math.Pow(10, 24),
            Color = Colors.DodgerBlue,
        };

        private PhysicalObject _sun = new PhysicalObject()
        {
            Name = "Sonne",
            Radius = _radius_sun,
            Mass = 1.989 * Math.Pow(10, 30),
            Color = Colors.Yellow,
        };

        private PhysicalObject _merkur = new PhysicalObject()
        {
            Name = "Merkur",
            Radius = 2440 * Math.Pow(10, 3),
            Mass = 1.989 * Math.Pow(10, 30),
            Color = Colors.Red,
        };

        private PhysicalObject _venus = new PhysicalObject()
        {
            Name = "Venus",
            Radius = 6052 * Math.Pow(10, 3),
            Mass = 1.989 * Math.Pow(10, 30),
            Color = Colors.OrangeRed,
        };

        private PhysicalObject _mars = new PhysicalObject()
        {
            Name = "Mars",
            Radius = 2440 * Math.Pow(10, 3),
            Mass = 1.989 * Math.Pow(10, 30),
            Color = Colors.Orange,
        };

        private PhysicalObject _jupiter = new PhysicalObject()
        {
            Name = "Jupiter",
            Radius = 69911 * Math.Pow(10, 3),
            Mass = 1.989 * Math.Pow(10, 30),
            Color = Colors.RosyBrown,
        };

        private PhysicalObject _saturn = new PhysicalObject()
        {
            Name = "Saturn",
            Radius = 58232 * Math.Pow(10, 3),
            Mass = 1.989 * Math.Pow(10, 30),
            Color = Colors.SandyBrown,
        };

        private PhysicalObject _uranus = new PhysicalObject()
        {
            Name = "Uranus",
            Radius = 25362 * Math.Pow(10, 3),
            Mass = 1.989 * Math.Pow(10, 30),
            Color = Colors.DeepSkyBlue,
        };

        private PhysicalObject _neptun = new PhysicalObject()
        {
            Name = "Neptun",
            Radius = 24622 * Math.Pow(10, 3),
            Mass = 1.989 * Math.Pow(10, 30),
            Color = Colors.DarkBlue,
        };


        private PhysicalObject _österreich = new PhysicalObject()
        {
            Name = "Österreich",
            Area = 83855 * Math.Pow(10, 3),
            Color = Colors.Red,
        };

        private PhysicalObject _usa = new PhysicalObject()
        {
            Name = "USA",
            Area = 9857306 * Math.Pow(10, 3),
            Color = Colors.DarkBlue,
        };

        private PhysicalObject _europa = new PhysicalObject()
        {
            Name = "Europa",
            Area = 10180000 * Math.Pow(10, 3),
            Color = Colors.Azure,
        };


        private PhysicalObject _asien = new PhysicalObject()
        {
            Name = "ASIEN",
            Area = 44579000 * Math.Pow(10, 3),
            Color = Colors.DarkGreen,
        };


        private PhysicalObject _singlefloat = new PhysicalObject()
        {
            Name = "Single",
            Area = Math.Pow(2,32),
            Color = Colors.Red,
        };


        private PhysicalObject _doublefloat = new PhysicalObject()
        {
            Name = "Double",
            Area = Math.Pow(2, 46),
            Color = Colors.Blue,
        };

        
        private PhysicalObject _milkyway = new PhysicalObject()
        {
            Name = "Milkyway",
            Radius = (100 * 94607 * Math.Pow(10, 15))/2,
            Color = Colors.Yellow,
        };

        private PhysicalObject _andromedar = new PhysicalObject()
        {
            Name = "Andromedar",
            Radius = (140000 * Math.Pow(10, 15))/2,
            Color = Colors.Blue,
        };

        private PhysicalObject _sonnensystem = new PhysicalObject()
        {
            Name = "Sonnensystem",
            Radius = 4.5 * Math.Pow(10, 9),
            Color = Colors.Blue,
        };

        
        
    }
}
