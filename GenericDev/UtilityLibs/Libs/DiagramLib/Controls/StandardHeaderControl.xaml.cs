﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Logicx.DiagramLib.Resources.Converter;
using Logicx.WpfUtility.WpfHelpers;

namespace Logicx.DiagramLib.Controls
{
    /// <summary>
    /// Interaction logic for StandardHeaderControl.xaml
    /// </summary>
    public partial class StandardHeaderControl
    {
        #region Constants
        #endregion

        #region Construction and Initialization
        public StandardHeaderControl()
        {
            InitializeComponent();
        }
        #endregion

        

        #region Operations

        protected override void OnVisualParentChanged(DependencyObject oldParent)
        {
            base.OnVisualParentChanged(oldParent);
            UpdateBinding();
        }

        private void UpdateBinding()
        {
            BindingOperations.ClearBinding(main_border, BackgroundProperty);
            if(VisualParent != null)
            {
                // TryFindParentGeneric sucht einen VisualParent und achtet dabei auf den generic type
                // der mitgeliefert wird. Die Typenparameter des generic types werden NICHT beachtet !!!
                // wenn der genaue Typ gesucht werden soll, dann die Methode TryFindParent  verwenden!!
                DependencyObject diagram_host = UIHelper.TryFindParentGeneric(this, typeof(DiagramBase<int, int>));

                if (diagram_host != null)
                {
                    Binding mode_binding = new Binding("DiagramMode");
                    mode_binding.Source = diagram_host;
                    mode_binding.Converter = new DiagramModeToHeaderBackgroundConverter();
                    mode_binding.TargetNullValue = Brushes.White;

                    main_border.SetBinding(BackgroundProperty, mode_binding);
                }
            }
        }
        #endregion

        #region Event Handlers
        private void Border_MouseMove(object sender, MouseEventArgs e)
        {
            //e.Handled = true;
        }

        private void Border_MouseDown(object sender, MouseButtonEventArgs e)
        {
            //e.Handled = true;
        }

        private void Border_MouseUp(object sender, MouseButtonEventArgs e)
        {
            //e.Handled = true;
        }

        #endregion

        #region Properties
        #endregion

        #region Dependency Properties


        public string AnsichtenName
        {
            get { return (string)GetValue(AnsichtenNameProperty); }
            set { SetValue(AnsichtenNameProperty, value); }
        }

        // Using a DependencyProperty as the backing store for AnsichtenName.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty AnsichtenNameProperty =
            DependencyProperty.Register("AnsichtenName", typeof(string), typeof(StandardHeaderControl), new UIPropertyMetadata(string.Empty));


        public string PlanstandName
        {
            get { return (string)GetValue(PlanstandNameProperty); }
            set { SetValue(PlanstandNameProperty, value); }
        }

        // Using a DependencyProperty as the backing store for AnsichtenName.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PlanstandNameProperty =
            DependencyProperty.Register("PlanstandName", typeof(string), typeof(StandardHeaderControl), new UIPropertyMetadata(string.Empty));


        public string CenteredText
        {
            get { return (string)GetValue(CenteredTextProperty); }
            set { SetValue(CenteredTextProperty, value); }
        }

        // Using a DependencyProperty as the backing store for CenteredText.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CenteredTextProperty =
            DependencyProperty.Register("CenteredText", typeof(string), typeof(StandardHeaderControl), new UIPropertyMetadata(string.Empty));



        public string ArbeitsbereichName
        {
            get { return (string)GetValue(ArbeitsbereichNameProperty); }
            set { SetValue(ArbeitsbereichNameProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ArbeitsbereichName.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ArbeitsbereichNameProperty =
            DependencyProperty.Register("ArbeitsbereichName", typeof(string), typeof(StandardHeaderControl), new UIPropertyMetadata(string.Empty));



        public string ArbeitsbereichDB776UA
        {
            get { return (string)GetValue(ArbeitsbereichDB776UAProperty); }
            set { SetValue(ArbeitsbereichDB776UAProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ArbeitsbereichDB776UA.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ArbeitsbereichDB776UAProperty =
            DependencyProperty.Register("ArbeitsbereichDB776UA", typeof(string), typeof(StandardHeaderControl), new UIPropertyMetadata(string.Empty));




        public string ArbeitsbereichKmRange
        {
            get { return (string)GetValue(ArbeitsbereichKmRangeProperty); }
            set { SetValue(ArbeitsbereichKmRangeProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ArbeitsbereichKmRange.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ArbeitsbereichKmRangeProperty =
            DependencyProperty.Register("ArbeitsbereichKmRange", typeof(string), typeof(StandardHeaderControl), new UIPropertyMetadata(string.Empty));




        public Style InfoTextBlockStyle
        {
            get { return (Style)GetValue(InfoTextBlockStyleProperty); }
            set { SetValue(InfoTextBlockStyleProperty, value); }
        }

        // Using a DependencyProperty as the backing store for InfoTextBlockStyle.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty InfoTextBlockStyleProperty =
            DependencyProperty.Register("InfoTextBlockStyle", typeof(Style), typeof(StandardHeaderControl), new UIPropertyMetadata(null));




        public Style SubDataTextBlockStyle
        {
            get { return (Style)GetValue(SubDataTextBlockStyleProperty); }
            set { SetValue(SubDataTextBlockStyleProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SubDataTextBlockStyle.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SubDataTextBlockStyleProperty =
            DependencyProperty.Register("SubDataTextBlockStyle", typeof(Style), typeof(StandardHeaderControl), new UIPropertyMetadata(null));



        public Style DataTextBlockStyle
        {
            get { return (Style)GetValue(DataTextBlockStyleProperty); }
            set { SetValue(DataTextBlockStyleProperty, value); }
        }

        // Using a DependencyProperty as the backing store for DataTextBlockStyle.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DataTextBlockStyleProperty =
            DependencyProperty.Register("DataTextBlockStyle", typeof(Style), typeof(StandardHeaderControl), new UIPropertyMetadata(null));

        
        
        #endregion


        #region Events
        #endregion

        #region Routed Events
        #endregion

        #region Attributes
        #endregion

        #region Tests
        #endregion
        
    }
}
