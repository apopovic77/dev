﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessEngineLib
{
    interface PeStatus
    {
        long Status { get; }
        long Reason { get; }
    }


}
