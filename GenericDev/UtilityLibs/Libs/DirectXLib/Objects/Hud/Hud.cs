using System;
using System.Collections.Generic;
using System.Text;
using D3d = Microsoft.DirectX.Direct3D;
using System.Reflection;
using Microsoft.DirectX;
using System.Drawing;
using Logicx.DirectX.Objects;
using Logicx.DirectX.Engine;
using Microsoft.DirectX.Direct3D;


namespace Logicx.DirectX.Objects.Hud
{
    public class Hud : Object3d
    {
        protected D3d.Sprite m_sprite;
        protected D3d.Font m_font_2d, m_font_2d_small, m_font_2d_big;
        protected D3d.Sprite m_sprite_statusview;
        protected D3d.Texture m_texture_energy;
        protected string _text="";

        public Hud()
        {
        }
		

        public override void Init()
        {
            m_sprite = new D3d.Sprite(Engine3d.Device3d);
            m_sprite_statusview = new D3d.Sprite(Engine3d.Device3d);
            m_font_2d = new D3d.Font(Engine3d.Device3d, new System.Drawing.Font("Arial", 9, System.Drawing.FontStyle.Regular));
            m_font_2d_big = new D3d.Font(Engine3d.Device3d, new System.Drawing.Font("Arial",16, System.Drawing.FontStyle.Bold));
            m_font_2d_small = new D3d.Font(Engine3d.Device3d, new System.Drawing.Font("Arial", 8, System.Drawing.FontStyle.Regular));           
        }

        public override void Dispose()
        {
            if (m_sprite != null)
            {
                m_sprite.Dispose();
                m_sprite = null;// = new D3d.Sprite(Engine3d.Device3d);
            }
            if (m_sprite_statusview != null)
            {
                m_sprite_statusview.Dispose();
                m_sprite_statusview = null;// = new D3d.Sprite(Engine3d.Device3d);
            }
            if (m_font_2d != null)
            {
                m_font_2d.Dispose();
                m_font_2d = null;// = new D3d.Font(Engine3d.Device3d, new System.Drawing.Font("Arial", 9, System.Drawing.FontStyle.Regular));
            }
            if (m_font_2d_big != null)
            {
                m_font_2d_big.Dispose();
                m_font_2d_big = null;// = new D3d.Font(Engine3d.Device3d, new System.Drawing.Font("Arial",16, System.Drawing.FontStyle.Bold));
            }
            if (m_font_2d_small != null)
            {
                m_font_2d_small.Dispose();
                m_font_2d_small = null;// = new D3d.Font(Engine3d.Device3d, new System.Drawing.Font("Arial", 8, System.Drawing.FontStyle.Regular));
            }
            if (m_texture_energy != null)
            {
                m_texture_energy.Dispose();
                m_texture_energy = null;// = D3d.Te
            }
        }

        public override void Render(float elapsedTime)
        {

            m_sprite.Begin(D3d.SpriteFlags.SortTexture | D3d.SpriteFlags.AlphaBlend);
            m_sprite.Transform = Matrix.Transformation2D(
                  new Vector2(0, 0),
                  0f,
                  new Vector2(1f, 1f),
                  new Vector2(0, 0),
                  0,
                  new Vector2(0, 0));


            RenderText(elapsedTime);

            m_sprite.End();

        }

        private void RenderText(float elapsedTime)
        {
            m_font_2d_small.DrawText(m_sprite, "fps " + Math.Round(Engine3d.FPS, 2), 10, 3, Color.White);
            m_font_2d_small.DrawText(m_sprite, _text , 10, 20, Color.White);
        }


        public string Text
        {
            set
            {
                _text = value;
            }

            get
            {
                return _text;
            }
        }
    }
}
