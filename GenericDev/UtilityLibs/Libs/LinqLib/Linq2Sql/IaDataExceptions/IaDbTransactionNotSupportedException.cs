﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LinqLib.Linq2Sql.IaDataExceptions
{
    public class IaDbTransactionNotSupportedException : Exception
    {
        public IaDbTransactionNotSupportedException() : base("Transactions are not supported with this type of connection!") { }
        public IaDbTransactionNotSupportedException(string msg) : base(msg) { }
    }
}
