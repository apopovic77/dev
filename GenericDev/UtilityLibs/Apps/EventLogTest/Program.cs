﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace EventLogTest
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("EventLogTest .. reading local event logs");

            try
            {
                EventLog[] local_event_logs = EventLog.GetEventLogs();

                foreach(EventLog existing_log in local_event_logs)
                {
                    ReadEventLog(existing_log);
                }

            }
            catch(Exception ex)
            {
                Console.WriteLine("**** ERROR: " + ex.Message);
            }
        }

        static void ReadEventLog(EventLog log)
        {
            Console.WriteLine("START reading LOG '" + log.LogDisplayName + " (" + log.Log + ")' ");
            Console.WriteLine(string.Format("Number of entries: {0}", log.Entries.Count));

            foreach(EventLogEntry curentry in log.Entries)
            {
                Console.WriteLine("   Type: " + curentry.EntryType.ToString());
                //Console.WriteLine("   Id: " + curentry.EventID.ToString());
                Console.WriteLine("   Date: " + curentry.TimeWritten.ToString("dd.MM.yyyy HH:mm:ss"));
                Console.WriteLine("   Message: " + curentry.Message);
            }
            Console.WriteLine("====================================== ");
            Console.WriteLine("  ");
        }
    }
}
