﻿from thread import start_new_thread
from threading import RLock
from threading import Thread
import pymssql
import sys
import time
from WhatsAppNachricht import WhatsAppNachricht
from WhatsAppConfig import WhatsAppConfig

#this class is not thread safe and should be used as a singleton
class WhatsAppDb():
    def __init__(self):
        self.has_new_nachrichten = False
        self.abort_async = False
        self._lock = RLock()
        self._bg_thread = None
        self._connection = None
        self._cursor = None

    def OpenConnectionIntern(self):
        if self._connection != None:
            self.CloseConnectionIntern()
        self._connection, self._cursor = self.OpenConnection()
        return self._connection, self._cursor

    def CloseConnectionIntern(self):
        self.CloseConnection(self._connection,self._cursor)

    def OpenConnection(self):
        connection = pymssql.connect(WhatsAppConfig.DB_SERVER, WhatsAppConfig.DB_USER, WhatsAppConfig.DB_PASS, WhatsAppConfig.DB_NAME)
        cursor = connection.cursor()

        return connection, cursor

    def CloseConnection(self, connection, cursor):
        if(connection == None):
            return

        cursor.close()
        del cursor
        cursor = None

        connection.close()
        del connection
        connection = None


    def Dispose(self):
        if(self._bg_thread == None):
            return

        #self._bg_thread._stop()
        self.abort_async = True


        self.CloseConnectionIntern()
        self._connection = None
        self._cursor = None

        self._bg_thread.join()
        self._bg_thread = None


    #PUBLIC Method to give info about nachrichten arrival
    def WithNewNachricht(self):
        with self._lock:
            if self.has_new_nachrichten == True:
                self.has_new_nachrichten = False
                return True
            else:
                return False


    #ASYNC CHECK FOR DB NOTIFICATION
    def RunAsyncWaitForNewNachrichten(self):
        if(self._bg_thread != None):
            print('Aborting thread already running')
            return
        #self._connection, self._cursor = self.OpenConnectionIntern()
        self._bg_thread = Thread(target=self.BgThread)
        self._bg_thread.daemon = True
        self._bg_thread.name = "Checking for DB Notifcations"
        self._bg_thread.start()
        


    def BgThread(self):
        #try:
        #    self.UnInstallNotificationService()
        #    self.InstallNotificationService()
        #    #self.ClearConversationsQueue()


        #except pymssql.Error, err:
        #    print err
        #    print "cannot run service fatal error: check for stored procedures for install and uninstall listerens on db"
        #    return

        while(True):
            try:
                if self.abort_async == True:
                    return None
                time.sleep(2)

                if self.WaitForNewNachricht() == True:
                    with self._lock:
                        self.has_new_nachrichten = True

            except pymssql.Error, err:
                print err
                time.sleep(10)
            except RuntimeError, err:
                print err
                time.sleep(10)
            except NameError, err:
                print err
                time.sleep(10)
            except AttributeError, err:
                print err
                time.sleep(10)
            except:
                print "Unexpected error:", sys.exc_info()[0]
                time.sleep(10)



    def WaitForNewNachricht(self):
        connection, cursor = self.OpenConnection()

        new_nachricht = False
        try:
            sql =  """\
                    DECLARE @ConvHandle UNIQUEIDENTIFIER
                    DECLARE @message VARBINARY(MAX)
                   """
            sql += "USE ["+WhatsAppConfig.DB_NAME+"] "
            sql += """\
                    WAITFOR (RECEIVE top(1)  @ConvHandle=Conversation_Handle, @message=message_body FROM dbo.[ListenerQueue_WhatsAppServer]), TIMEOUT 30000;
	                BEGIN TRY END CONVERSATION @ConvHandle; END TRY BEGIN CATCH END CATCH
                    SELECT CAST(@message AS NVARCHAR(MAX)) 
                    """
        
            cursor.execute(sql)
            rows = cursor.fetchall()
            
            if(rows == None):
                return False

            timedout = str(rows).find('None') >= 0
            if timedout:
                return False

            new_nachricht = str(rows).find('root') >= 0
            while rows:
                if cursor.nextset():
                    rows = cursor.fetchall()
                else:
                    rows = None

            connection.commit()

        finally:
            self.CloseConnection(connection, cursor)

        return new_nachricht


    def InstallNotificationService(self):
        connection, cursor = self.OpenConnection()
        cursor.callproc('sp_InstallListenerNotification_WhatsAppServer')
        self.CloseConnection(connection, cursor)
        print "successfully installing notification listener"

    def UnInstallNotificationService(self):
        connection, cursor = self.OpenConnection()
        cursor.callproc('sp_UninstallListenerNotification_WhatsAppServer')
        self.CloseConnection(connection, cursor)
        print "successfully uninstalling notification listener"


    def GetNewNachrichten(self):
        connection, cursor = self.OpenConnection()
        cursor = connection.cursor(as_dict=True)
        cursor.execute("select n.Id,EmpfaengerAdresse, AbsenderAdresse, Body,PathToMedia from nachrichten n LEFT JOIN nachrichtmedium nm on n.Id = nm.NachrichtId left join Medien on Medien.Id = nm.MediumId where _RawGesendetAm is null or CHARINDEX('2,',_RawGesendetAm)<0")
        nachrichten = []
        for row in cursor:
            nachricht = WhatsAppNachricht(row['EmpfaengerAdresse'], row['AbsenderAdresse'], row['Body'], row['Id'], row['PathToMedia'])
            nachrichten.append(nachricht)
            
        self.CloseConnection(connection, cursor)
        print str(len(nachrichten)) + " Neue Nachrichten"
        return nachrichten

    def GetNachrichtWithId(self, id):
        connection, cursor = self.OpenConnection()
        cursor = connection.cursor(as_dict=True)
        cursor.execute("select n.Id,EmpfaengerAdresse, AbsenderAdresse, Body,PathToMedia from nachrichten n LEFT JOIN nachrichtmedium nm on n.Id = nm.NachrichtId left join Medien on Medien.Id = nm.MediumId where n.id = "+str(id))
        nachrichten = []
        for row in cursor:
            nachricht = WhatsAppNachricht(row['EmpfaengerAdresse'], row['AbsenderAdresse'], row['Body'], row['Id'], row['PathToMedia'])
            break
        self.CloseConnection(connection, cursor)
        return nachricht

    def SetMessageSent(self, id):
        nachricht = self.GetNachrichtWithId(id)
        
        if (nachricht == None):
            print "Nachricht mit der"+id+"ist nicht in der DB vorhanden, unerwarteter fehler"
            return

        nachricht.UpdateGesendetAm()
