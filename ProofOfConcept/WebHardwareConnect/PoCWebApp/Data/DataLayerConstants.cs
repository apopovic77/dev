﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PoCWebApp.Data
{
    public static class DataLayerConstants
    {
        public static string EnableAutoDbMigrationsConfigKey = "database:enableautomigration";
    }
}