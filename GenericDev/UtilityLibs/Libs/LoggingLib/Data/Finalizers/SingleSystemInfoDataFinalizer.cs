﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Text;
using LoggingLib.SystemInterop;

namespace LoggingLib.Data.Finalizers
{
    /// <summary>
    /// Implements a data finalizer which makes sure that only one system info log entry exists 
    /// </summary>
    public class SingleSystemInfoDataFinalizer : DataFinalizerBase
    {
        /// <summary>
        /// Will be called before the the item will be inserted in the data context
        /// </summary>
        /// <param name="logEntry">The log entry for this finalizer</param>
        /// <param name="dataContext">Data context which is about to submit its changes back to the database</param>
        /// <returns>Returns true if pre-update operations succeed. If this method returns false, the SubmitChanges() will not be executed.</returns>
        public override bool BeforeInsertItemInDataContext(LogEntry logEntry, LoggingDataContext dataContext)
        {
            bool bRet = true;
            long nChangeCnt = 0;

            lock (dataContext)
            {
                // LinQ query to retrieve all system info log entries in the current data context
                // filter by computer name
                var query1 = from sysinfolog in dataContext.LogEntries
                             join cat in dataContext.CategoryLogEntries on sysinfolog.LogId equals cat.LogId
                             where cat.CategoryName == LogManagerConstants.SYSCATEGORY_SYSTEMINFO &&
                                   sysinfolog.MachineName == ApplicationStatisticHelper.MachineName &&
                                   sysinfolog is SystemInfoLogEntry
                             select sysinfolog as SystemInfoLogEntry;

                // order results
                var query = from sysloginfo in query1
                            orderby sysloginfo.LogTimestamp descending
                            select sysloginfo;


                // delete all current system info log entries
                // since BeforeInsertItemInDataContext() will be called 
                // before the new item will be inserted into the data context
                try
                {
                    List<SystemInfoLogEntry> logEntries = query.ToList();

                    foreach(SystemInfoLogEntry curEntry in logEntries)
                    {
                        // delete all old entries
                        if (curEntry.InstalledSoftwares != null)
                        {
                            nChangeCnt += curEntry.InstalledSoftwares.Count;
                            dataContext.InstalledSoftwares.DeleteAllOnSubmit(curEntry.InstalledSoftwares);
                        }

                        if (curEntry.CategoryLogEntries != null)
                        {
                            nChangeCnt += curEntry.CategoryLogEntries.Count;
                            dataContext.CategoryLogEntries.DeleteAllOnSubmit(curEntry.CategoryLogEntries);
                        }

                        if (curEntry.SQLServers != null)
                        {
                            nChangeCnt += curEntry.SQLServers.Count;
                            dataContext.SQLServers.DeleteAllOnSubmit(curEntry.SQLServers);
                        }

                        dataContext.LogEntries.DeleteOnSubmit(curEntry);

                        nChangeCnt++;
                    }
                }
                catch
                {
                    
                }
                finally
                {
                    // check for old system info entries in the current changeset
                    ChangeSet csCheck = dataContext.GetChangeSet();
                    List<SystemInfoLogEntry> removeFromInserts = new List<SystemInfoLogEntry>();
                    SystemInfoLogEntry mostRecent = null;

                    foreach(object obj in csCheck.Inserts)
                    {
                        if(obj is SystemInfoLogEntry)
                        {
                            SystemInfoLogEntry sysLog = obj as SystemInfoLogEntry;

                            if (mostRecent == null)
                            {
                                mostRecent = sysLog;
                            }
                            else
                            {
                                if (mostRecent.LogTimestamp < sysLog.LogTimestamp)
                                {
                                    removeFromInserts.Add(mostRecent);
                                    mostRecent = sysLog;
                                }
                                else
                                {
                                    removeFromInserts.Add(sysLog);
                                }
                            }
                        }
                    }

                    foreach(SystemInfoLogEntry remEntry in removeFromInserts)
                    {
                        // delete all old entries
                        if (remEntry.InstalledSoftwares != null)
                        {
                            nChangeCnt += remEntry.InstalledSoftwares.Count;
                            dataContext.InstalledSoftwares.DeleteAllOnSubmit(remEntry.InstalledSoftwares);
                        }

                        if (remEntry.CategoryLogEntries != null)
                        {
                            nChangeCnt += remEntry.CategoryLogEntries.Count;
                            dataContext.CategoryLogEntries.DeleteAllOnSubmit(remEntry.CategoryLogEntries);
                        }

                        if (remEntry.SQLServers != null)
                        {
                            nChangeCnt += remEntry.SQLServers.Count;
                            dataContext.SQLServers.DeleteAllOnSubmit(remEntry.SQLServers);
                        }

                        dataContext.LogEntries.DeleteOnSubmit(remEntry);

                        nChangeCnt++;
                    }
                }
            }

            SetChangesMade(nChangeCnt);

            return bRet;
        }

        /// <summary>
        /// Will be called after the submitchanges() method invokation of the given data context
        /// </summary>
        /// <param name="dataContext">Data context which submitted its changes to the database</param>
        /// <returns>Returns true if post-update operations sueceed. If not false will be returned.</returns>
        /// <remarks>You may use the return value of false, for indicating a propblem on the data context and 
        /// rollback or reset a current transaction scope.</remarks>
        public override bool AfterSubmitChanges(LoggingDataContext dataContext)
        {
            bool bRet = true;
            long nChangeCnt = 0;

            lock (dataContext)
            {
                // LinQ query to retrieve all system info log entries in the current data context
                // filter by computer name
                var query1 = from sysinfolog in dataContext.LogEntries
                            join cat in dataContext.CategoryLogEntries on sysinfolog.LogId equals cat.LogId
                             where cat.CategoryName == LogManagerConstants.SYSCATEGORY_SYSTEMINFO &&
                                   sysinfolog.MachineName == ApplicationStatisticHelper.MachineName &&
                                   sysinfolog is SystemInfoLogEntry
                             select sysinfolog as SystemInfoLogEntry;

                // order results
                var query = from sysloginfo in query1
                            orderby sysloginfo.LogTimestamp descending
                            select sysloginfo;


                try
                {
                    List<SystemInfoLogEntry> logEntries = query.ToList();
                    bool bFirst = true;

                    foreach (SystemInfoLogEntry curEntry in logEntries)
                    {
                        if (bFirst)
                        {
                            // first item is the most recent system info entry
                            bFirst = false;
                        }
                        else
                        {
                            // delete all old entries
                            if (curEntry.InstalledSoftwares != null)
                            {
                                nChangeCnt += curEntry.InstalledSoftwares.Count;
                                dataContext.InstalledSoftwares.DeleteAllOnSubmit(curEntry.InstalledSoftwares);
                            }

                            if (curEntry.CategoryLogEntries != null)
                            {
                                nChangeCnt += curEntry.CategoryLogEntries.Count;
                                dataContext.CategoryLogEntries.DeleteAllOnSubmit(curEntry.CategoryLogEntries);
                            }

                            if (curEntry.SQLServers != null)
                            {
                                nChangeCnt += curEntry.SQLServers.Count;
                                dataContext.SQLServers.DeleteAllOnSubmit(curEntry.SQLServers);
                            }

                            dataContext.LogEntries.DeleteOnSubmit(curEntry);

                            nChangeCnt++;
                        }
                    }
                }
                catch
                {
                    bRet = false;
                }
            }

            SetChangesMade(nChangeCnt);

            return bRet;
        }
    }
}
