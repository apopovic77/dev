﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GeneticProgrammingLib
{
    public class RandomWithProbabilityGenerator
    {

        public static int Generate(int count_segments, double triangle_width, double triangle_height)
        {
            double x = _r.NextDouble() * triangle_width;
            double y = _r.NextDouble() * triangle_height;
            double alpha = Math.Atan(triangle_height / triangle_width);
            double len_per_segment = triangle_width / (double) count_segments;
            int segment = Convert.ToInt32(Math.Floor(x / len_per_segment));
            double triangle_gk = Math.Tan(alpha) * x;

            if (y > triangle_gk)
            {
                //uper triangle
                return 7;//count_segments - segment;
            }
            else
            {
                //lower triangle
                return segment + 1;
            }
        }

        private static Random _r = new Random();
    }
}
