﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows;
using CognitiveLib.Detector;
using NHibernate.Linq.Functions;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.PhantomJS;
using OpenQA.Selenium.Support.UI;

namespace CognitiveLib.WebSearch
{
    public class GeizhalsSearchResults : List<GeizhalsSearchResult>
    {
        public bool IsEmpty
        {
            get
            {
                return Count == 0;
            }
        }

    }

    public class GeizhalsSearchResult
    {
        public void AssertListsNotNull()
        {
            Produkte = new List<string>(1);
            Kategorien = new List<string>(1);
            Suchanfragen = new List<string>(1);
        }

        public bool IsEmpty
        {
            get
            {
                if (Produkte != null && Produkte.Count > 0 || Kategorien != null && Kategorien.Count > 0)
                    return false;
                return true;
            }
        }

        //public string ProduktBestMatchByCount
        //{
        //    get
        //    {

        //    }
        //}

        public string Brand
        {
            get
            {
                if (string.IsNullOrEmpty(ProdukeResultRaw))
                    return null;

                StringDetectionResult string_det_res = BrandDetector.DetectBrand(ProdukeResultRaw);
                return string_det_res.BestStringMatchByCount.String;
            }
        }


        public bool ContainsBrand
        {
            get
            {
                if (string.IsNullOrEmpty(ProdukeResultRaw))
                    return false;
                StringDetectionResult string_det_res = BrandDetector.DetectBrand(ProdukeResultRaw);
                string brand = string_det_res.BestStringMatchByCount.String;
                return true;
            }
        }

        public bool ResultContainsSearchString
        {
            get
            {
                if (ProdukeResultRaw != null)
                {
                    if (IsEqualNormalizedCompare(ProdukeResultRaw, SearchString))
                        return true;
                }

                foreach (string s in Produkte)
                {
                    if (IsEqualNormalizedCompare(ProdukeResultRaw, SearchString))
                        return true;
                }

                return false;
            }
        }

        private bool IsEqualNormalizedCompare(string s1, string s2)
        {
            string cmp_str1 = s1.Replace(" ", "").ToLower();
            string cmp_str2 = s2.Replace(" ", "").ToLower();
            if (cmp_str1.IndexOf(cmp_str2) >= 0)
                return true;
            if (cmp_str2.IndexOf(cmp_str1) >= 0)
                return true;
            return false;
        }

        public string Model
        {
            get
            {
                string produkt = ProdukeResultRaw;
                if (string.IsNullOrEmpty(produkt))
                    return null;

                string brand = Brand;
                if (!string.IsNullOrEmpty(brand))
                    produkt = produkt.ToLower().Replace(brand.ToLower(), "").Trim();

                return produkt.ToUpper();
            }
        }

        public override string ToString()
        {

            return $"Brand: {Brand} ";
        }


        public string SearchString;

        public string ProdukeResultRaw;
        public List<string> Produkte;
        public List<string> Kategorien;
        public List<string> Suchanfragen;
    }

    public class GeizhalsSelenium
    {
        private GeizhalsSelenium()
        {
            //string path = System.Environment.GetEnvironmentVariable("Path");
            //path += Environment.CurrentDirectory + "\\ApiService";
            //System.Environment.SetEnvironmentVariable("Path", path);
            //System.Environment.SetEnvironmentVariable("webdriver.chrome.driver", @"..\ApiService\chromedriver.exe");

            //_driver = new PhantomJSDriver();
            //Driver = new FirefoxDriver();
            _driver = new ChromeDriver();

            //Notice navigation is slightly different than the Java version
            //This is because 'get' is a keyword in C#
            _driver.Navigate().GoToUrl(UrlLogin);

            //just wait, makes everthing sure
            Thread.Sleep(10);
        }

        public GeizhalsSearchResult Query(string query_string)
        {
            lock (this)
            {
                try
                {
                    // Find the text input element by its name
                    IWebElement search_input = _driver.FindElement(By.Id("fs"));

                    search_input.Clear();
                    search_input.SendKeys(query_string);

                    try
                    {
                        WebDriverWait wait = new WebDriverWait(_driver, TimeSpan.FromSeconds(1));
                        wait.Until<IWebElement>((d) =>
                        {
                            IWebElement element = d.FindElement(By.Id("acbox"));
                            if (element.Displayed && element.Enabled
                                /*&& element.GetAttribute("aria-disabled") == null*/)
                            {
                                return element;
                            }
                            return null;
                        });
                    }
                    catch (Exception ex)
                    {
                        search_input.Clear();
                        _driver.FindElement(By.TagName("body")).Click();
                        return null;
                    }


                    IWebElement produkte_ul = _driver.FindElement(By.Id("acl1"));
                    IWebElement kategorien_ul = _driver.FindElement(By.Id("acl2"));
                    IWebElement suchanfrage_ul = _driver.FindElement(By.Id("acl3"));

                    GeizhalsSearchResult gsr = new GeizhalsSearchResult();
                    gsr.AssertListsNotNull();
                    gsr.SearchString = query_string;

                    foreach (IWebElement elem in produkte_ul.FindElements(By.TagName("li")))
                    {
                        
                        IWebElement a = elem.FindElement(By.TagName("a"));
                        if(gsr.ProdukeResultRaw == null)
                            gsr.ProdukeResultRaw = a.Text;
                        //wenn ein image gesetzt handelt es sich um ein produkt
                        if (a.Text.Split(" ".ToCharArray()).Length < 4)
                            gsr.Produkte.Add(a.Text);
                    }

                    foreach (IWebElement elem in kategorien_ul.FindElements(By.TagName("li")))
                    {
                        IWebElement a = elem.FindElement(By.TagName("a"));
                        gsr.Kategorien.Add(a.Text);
                    }

                    foreach (IWebElement elem in suchanfrage_ul.FindElements(By.TagName("li")))
                    {
                        IWebElement a = elem.FindElement(By.TagName("a"));
                        gsr.Suchanfragen.Add(a.Text);
                    }



                    try
                    {
                        search_input.Clear();
                        _driver.FindElement(By.TagName("body")).Click();

                        WebDriverWait wait = new WebDriverWait(_driver, TimeSpan.FromSeconds(1));
                        wait.Until<IWebElement>((d) =>
                        {
                            IWebElement element = d.FindElement(By.Id("acbox"));
                            if (!element.Displayed
                                /*&& element.GetAttribute("aria-disabled") == null*/)
                            {
                                return element;
                            }
                            return null;
                        });
                    }
                    catch (Exception ex)
                    {

                    }


                    return gsr;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                    return null;
                }
            }
        }

        public void Disconnect()
        {
            //Close the browser
            if (_driver != null)
            {
                _driver.Quit();
                _driver.Dispose();
            }
        }

        #region Singleton
        private static GeizhalsSelenium CreateSeleniumInstance()
        {
            lock (_lock)
            {
                _instance = new GeizhalsSelenium();
                return _instance;
            }
        }


        public static void Dispose(GeizhalsSelenium gz_sel)
        {
            lock (_lock)
            {
                gz_sel.Disconnect();
            }
        }

        public static void Dispose()
        {
            Dispose(_instance);
        }

        public static GeizhalsSelenium Instance()
        {
            lock (_lock)
            {
                if (_instance != null)
                    return _instance;

                CreateSeleniumInstance();
                return _instance;
            }
        }

        private static object _lock = new object();
        private static GeizhalsSelenium _instance;
        #endregion

        private IWebDriver _driver;
        private string UrlLogin = "https://geizhals.at";
    }
}

