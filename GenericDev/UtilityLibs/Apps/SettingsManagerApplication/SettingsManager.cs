using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace SettingsManagerApplication
{
    public partial class SettingsManager : Form
    {
        string _settings_file_content = "";
        string _settings_filename = "";
        //string _s1 = "//settingsmanager: construction begin";
        string _s2 = "//settingsmanager: construction end";
        //string _s3 = "//settingsmanager: savesettings begin";
        string _s4 = "//settingsmanager: savesettings end";
        string _s5 = "//settingsmanager: vardef begin";
        string _s6 = "//settingsmanager: vardef end";
        //string _s7 = "<!-- settingsmanager: vardef beg -->";
        //string _s8 = "<!-- settingsmanager: vardef end -->";
        bool _settings_changed = false;
        //const string Default_SettingsFileName = @"C:\Logicx\Projekte\Intern\010-LxFramework\90-GIS\5-Impl\c#\Libs\GisLib\GisConfig.cs";
        //const string Default_SettingsFileName = @"C:\logicxklu\Projekte\F&E\101-InfraAssist\20-Explorer\5-Impl\c#\Libs\IaExpControllerLib\Configuration\App\AppConfig.cs";
        //const string Default_SettingsFileName = @"C:\logicxklu\Projekte\Intern\010-LxFramework\90-GIS\5-Impl\c#\Libs\GisLib\GisConfig.cs";
        
        //const string Default_SettingsFileName = @"C:\Logicx\Projekte\F&E\100-InfraAssit\20-IaExp\5-Impl\c#\Libs\IaExpGisLib\IaExpGisConfig.cs";
        //const string Default_SettingsFileName = @"C:\Logicx\Projekte\Kunden\511-Ipoint\5-Impl\c#\libs\IPointControllerLib\IPointConfig.cs";
        //const string Default_SettingsFileName = @"C:\Logicx_Depot\3 - ViHast\Source\C#\ConfigurationLib\GeoSimul3dConfiguration.cs";
        // const string Default_SettingsFileName = @"C:\Logicx_Depot\3 - ViHast\Source\C#\ConfigurationLib\ProcessEnvironmentConfiguration.cs";
        //const string Default_SettingsFileName = @"C:\Logicx_Depot\3 - ViHast\Source\C#\ConfigurationLib\SimulationConfiguration.cs";
        //const string Default_SettingsFileName = @"C:\Logicx_Depot\3 - ViHast\Source\C#\sWFMELib\Processes\Interaction\InteractionConfiguration.cs";
        //const string Default_AppConfigFile = @"C:\Logicx_Depot\3 - ViHast\Source\C#\Applications\GeoSimul3dV2Application\bin\Debug\GeoSimul3dV2Application.exe.config";
        //const string Default_SettingsFileName = @"C:\Logicx_Depot\3 - ViHast\Source\C#\sWFMELib\Kernel\ProcessConfiguration.cs";
        //const string Default_AppConfigFile = @"C:\Logicx_Depot\3 - ViHast\Source\C#\Applications\GeoSimul3dV2Application\bin\Debug\GeoSimul3dV2Application.exe.config";
        //const string Default_AppConfigFile = @"C:\Logicx_Depot\3 - ViHast\Source\C#\Applications\GeoSimul3dV2Application\bin\Debug\GeoSimul3dV2Application.exe.config";

        const string Default_SettingsFileName = @"C:\logicxklu\Projekte\Kunden\001-KE Modul\5-Impl\c#\Libs\KeControllerLib\Configuration\KeAppConfig.cs";
        


        public SettingsManager()
        {
            InitializeComponent();
        }

        private void SettingsManager_Load(object sender, EventArgs e)
        {

            //set path to new settings file
            _settings_filename = Default_SettingsFileName;

            //open file
            FileStream fs = File.OpenRead(_settings_filename);

            ReadSettingsContent(fs);




        }

        private void ReadSettingsContent(Stream fs)
        {

            //read content to end from file
            StreamReader sr = new StreamReader(fs);
            _settings_file_content = sr.ReadToEnd();

            //close all reader stuff things
            sr.Close();
            sr.Dispose();
            fs.Close();
            fs.Dispose();

            //parse settings and fill 
            ParseSettingsContent();
        }

        private void loadSettingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Currently not implemented");
            return;

            //DialogResult dr = openFileDialog.ShowDialog();
            //if (dr == DialogResult.OK)
            //{
            //    //set path to new settings file
            //    _settings_filename = openFileDialog.FileName;

            //    //open file
            //    Stream fs = openFileDialog.OpenFile();

            //    ReadSettingsContent(fs);
            //}
        }

        private void ParseSettingsContent()
        {
            int index_beg = _settings_file_content.IndexOf(_s5);
            int index_end = _settings_file_content.IndexOf(_s6);



            
            string added_vars = _settings_file_content.Substring(index_beg + _s5.Length,index_end - (index_beg + _s5.Length));
            added_vars.TrimStart().TrimEnd();
            string[] arr_vars = added_vars.Split(";".ToCharArray());
            foreach (string var in arr_vars) {
                try
                {
                    if (var.Trim() == "")
                        continue;
                    string[] arr_tok = var.Trim().Split("=".ToCharArray(), 2);
                    string def = arr_tok[0];
                    string value = arr_tok[1];
                    string[] arr_def = def.Split(" ".ToCharArray());
                    string datatype = arr_def[1];
                    string name = arr_def[3];
                    if (datatype == "string")
                    {
                        value = value.Substring(2, value.Length - 3);
                    }

                    AddKnoten(datatype, name, value);
                }
                catch { }
            }
        }

        private void AddKnoten(string datatype, string name, string value)
        {
            TreeNode treeNode_datatype = new TreeNode("datatype: " + datatype);
            TreeNode treeNode_value = new TreeNode("value: " + value);
            TreeNode treeNode = new TreeNode(name, new TreeNode[] {treeNode_datatype,treeNode_value} );

            this.treeView1.Nodes.AddRange(new TreeNode[] { treeNode });
        }

        private void saveSettingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_settings_file_content == "")
                return;

            StreamWriter sw = File.CreateText(_settings_filename);

            sw.Write(_settings_file_content);

            sw.Flush();
            sw.Close();
            sw.Dispose();
            sw = null;
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void b_addsetting_Click(object sender, EventArgs e)
        {
            
            //check if values given
            if (!AssertDatatypeWithValue()) {
                MessageBox.Show("Cannot add setting please check value and datatype");
                return;
            }
            if (!AssertKommentar())
            {
                MessageBox.Show("Cannot add setting pleas check kommentar");
                return;
            }
            
            //check if setting is not existent
            if (!AssertSettingName())
            {
                MessageBox.Show("Cannot add setting already existent");
                return;
            }

            int index_s2 = _settings_file_content.IndexOf(_s2);
            string new_settings_content = _settings_file_content.Substring(0, index_s2 );

            //add construction info
            new_settings_content += "//SettingsManager-Kommentar: "+tb_kommentar.Text+"\r\n";
            new_settings_content += "\t\t\tif (System.Configuration.ConfigurationManager.AppSettings[\"" + tb_settingsname.Text + "\"] != null)\r\n" +
                                    "\t\t\t\t" + tb_settingsname.Text + " = " + GetConvertFunctionName() + "(System.Configuration.ConfigurationManager.AppSettings[\"" + tb_settingsname.Text + "\"]);\r\n";

            int index_s4 = _settings_file_content.IndexOf(_s4);
            new_settings_content += "\t\t\t" + _settings_file_content.Substring(index_s2, index_s4 - index_s2);

            //add save info
            new_settings_content += "//SettingsManager-Kommentar: " + tb_kommentar.Text + "\r\n";
            new_settings_content += "\t\t\tAddKey(_config, \"" + tb_settingsname.Text + "\", " + tb_settingsname.Text + ".ToString());\r\n";

            int index_s6 = _settings_file_content.IndexOf(_s6);
            new_settings_content += "\t\t\t" + _settings_file_content.Substring(index_s4, index_s6 - index_s4);

            //add var info
            new_settings_content += "public static " + GetDataType() + " " + tb_settingsname.Text + " = " + GetValueString() + ";\r\n";

            new_settings_content += "\t\t" + _settings_file_content.Substring(index_s6);

            _settings_file_content =  new_settings_content;
            _settings_changed = true;

            AddKnoten(GetDataType(),tb_settingsname.Text,tb_settingsdefvalue.Text);
        }

        private string GetValueString()
        {
            bool bstring = rb_string.Checked;
            bool bint = rb_int.Checked;
            bool bfloat = rb_float.Checked;
            bool bbool = rb_bool.Checked;

            if (bstring) 
                return "\"" + tb_settingsdefvalue.Text + "\"";
            else if (bfloat)
                return tb_settingsdefvalue.Text + "f";
            else 
                return tb_settingsdefvalue.Text;
        }

        private string GetDataType()
        {
            bool bstring = rb_string.Checked;
            bool bint = rb_int.Checked;
            bool bfloat = rb_float.Checked;
            bool bbool = rb_bool.Checked;

            if (bstring) return "string";
            if (bint) return "int";
            if (bfloat) return "float";
            if (bbool) return "bool";
            return "";
        }

        private string GetConvertFunctionName()
        {
            bool bstring = rb_string.Checked;
            bool bint = rb_int.Checked;
            bool bfloat = rb_float.Checked;
            bool bbool = rb_bool.Checked;

            if (bstring) return "Convert.ToString";
            if (bint) return "Convert.ToInt32";
            if (bfloat) return "Convert.ToSingle";
            if (bbool) return "Convert.ToBoolean";
            return "";
        }

        private bool AssertSettingName()
        {
            if (tb_settingsname.Text == "")
                return false;
            if (_settings_file_content.IndexOf(tb_settingsname.Text) >= 0)
                return false;

            return true;
        }

        private bool AssertKommentar()
        {
            if (tb_kommentar.Text == "")
                return false;

            return true;
        }

        /// <summary>
        /// checks if all values given, and if value suits datatype
        /// </summary>
        /// <returns></returns>
        private bool AssertDatatypeWithValue()
        {
            string value = tb_settingsdefvalue.Text;

            bool bstring = rb_string.Checked;
            bool bint = rb_int.Checked;
            bool bfloat = rb_float.Checked;
            bool bbool = rb_bool.Checked;

            if (!bbool && !bstring && !bint && !bfloat)
            {
                return false;
            }

            if (bstring) return true;

            if (bbool && value == "true" || value == "false") return true;


            float resf;
            if (bfloat && float.TryParse(value, out resf))
                return true;

            int resi;
            if ((bint && int.TryParse(value, out resi)))
                return true;

            try {
                if (bint)
                {
                    int.Parse(value, System.Globalization.NumberStyles.HexNumber);
                    return true;
                }
            }
            catch { }

            return false;
        }

        private void SettingsManager_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (_settings_changed) {
                if (MessageBox.Show("Do you want to save before quit?","Exiting",MessageBoxButtons.YesNo) == DialogResult.Yes)
                    saveSettingsToolStripMenuItem_Click(this,e);
            }
        }
    }
}