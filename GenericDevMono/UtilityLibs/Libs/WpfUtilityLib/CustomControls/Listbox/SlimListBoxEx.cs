﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace Logicx.WpfUtility.CustomControls.Listbox
{
    public class SlimListBoxEx : ItemsControl
    {
        #region Constants
        #endregion

        #region Construction and Initialization
        public SlimListBoxEx()
        {
            //ScrollViewer.SetCanContentScroll(this, true);
            //VirtualizingStackPanel.SetIsVirtualizing(this, true);
            //VirtualizingStackPanel.SetVirtualizationMode(this, VirtualizationMode.Recycling);
            this.Loaded += new System.Windows.RoutedEventHandler(SlimListBoxEx_Loaded);
            this.KeyUp += new System.Windows.Input.KeyEventHandler(SlimListBoxEx_KeyUp);
        }
        
        #endregion

        #region Operations

        #region Item Container Overrides
        protected override System.Windows.DependencyObject GetContainerForItemOverride()
        {
            ListBoxItem lbi = new ListBoxItem();
            lbi.HorizontalAlignment = HorizontalAlignment.Stretch;
            lbi.Loaded += lbi_Loaded;
            lbi.Unloaded += lbi_Unloaded;
            return lbi;
        }

        

        protected override bool IsItemItsOwnContainerOverride(object item)
        {
            return (item is ListBoxItem);
        }
        #endregion

        #region OnPropertyChanged
        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            if(e.Property == ItemsSourceProperty)
            {
                SelectedIndex = -1;
                OnItemsSourceChanged(e);
            }
            else if(e.Property == HasItemsProperty)
            {
                if((bool)e.NewValue)
                {
                    if (_buffer_selected_value != null)
                    {
                        if (SelectedValue != _buffer_selected_value)
                            SelectedValue = _buffer_selected_value;
                        else
                        {
                            if (_with_selected_value_path)
                            {
                                object sel_item = FindItemByValue(_buffer_selected_value);
                                SelectedItem = sel_item;
                            }
                            else
                            {
                                SelectedItem = SelectedValue;
                            }
                        }
                        _buffer_selected_value = null;
                    }
                }
                else if(SelectedIndex >= 0)
                {
                    SelectedIndex = -1;
                }
            }
            else if(e.Property == SelectedValuePathProperty)
            {
                RebuiltSelectedValuePath(Items);
                UpdateSelectedValue();
            }
            else if(e.Property == SelectedIndexProperty)
            {
                //INFO: Diese Listbox unterstützt NUR Einfachselektion
                // altes item deselektieren
                DeselectItemWithIndex((int) e.OldValue);
                // neues item selektieren
                SelectItemWithIndex((int) e.NewValue);

                UpdateSelectedValue();
                OnSelectionChanged();
            }
            else if(e.Property == SelectedItemProperty)
            {
                if(e.NewValue == null)
                {
                    SelectedIndex = -1;
                }
                else
                {
                    SelectedIndex = Items.IndexOf(SelectedItem);
                }
            }
            else if(e.Property == SelectedValueProperty)
            {
                if (!_internal_selection_change)
                {
                    if(_with_selected_value_path)
                    {
                        object sel_item = FindItemByValue(e.NewValue);
                        SelectedItem = sel_item;
                    }
                    else
                    {
                        SelectedItem = SelectedValue;
                    }

                    if(SelectedItem == null)
                        _buffer_selected_value = SelectedValue;
                }
            }

            base.OnPropertyChanged(e);
        }
        #endregion

        #region Event overrides
        protected override void OnMouseUp(System.Windows.Input.MouseButtonEventArgs e)
        {

            base.OnMouseUp(e);
        }
        #endregion

        #region Event raising
        private void OnSelectionChanged()
        {
            SelectionChangedEventArgs e = new SelectionChangedEventArgs(SelectionChangedEvent, _deselected_items, _selected_items);
            RaiseEvent(e);
        }
        #endregion

        #region Selection helpers
        private void DeselectItemWithIndex(int index)
        {
            _deselected_items.Clear();
            if (index >= 0 && Items != null && index < Items.Count)
            {
                try
                {
                    object data_item = Items[index];
                    if (data_item == null)
                        return;

                    ListBoxItem lbi = (ListBoxItem)ItemContainerGenerator.ContainerFromItem(data_item);
                    if (lbi != null)
                    {
                        if (lbi.IsSelected && !_deselected_items.Contains(data_item))
                            _deselected_items.Add(data_item);
                        lbi.IsSelected = false;
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex);
                }
            }
        }

        private void SelectItemWithIndex(int index)
        {
            _selected_items.Clear();

            if (index >= 0 && Items != null && index < Items.Count)
            {
                try
                {
                    object data_item = Items[index];
                    if (data_item == null)
                        return;

                    ListBoxItem lbi = (ListBoxItem)ItemContainerGenerator.ContainerFromItem(data_item);
                    if (lbi != null)
                    {
                        if (!lbi.IsSelected && !_selected_items.Contains(data_item))
                            _selected_items.Add(data_item);

                        lbi.IsSelected = true;
                    }

                    if (SelectedItem != data_item)
                        SelectedItem = data_item;
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex);
                }
            }

            if (index < 0)
                SelectedItem = null;
        }

        private void SelectItemWithContainer(ListBoxItem lbi)
        {
            if (lbi.IsEnabled)
            {
                object data_object = ItemContainerGenerator.ItemFromContainer(lbi);
                SelectedItem = data_object;
            }
        }

        private void UpdateSelectedValue()
        {
            _internal_selection_change = true;
             SelectedValue = GetItemValue(SelectedItem);
             _internal_selection_change = false;
        }
        #endregion

        #region Other Helpers
        private void OnItemsSourceChanged(DependencyPropertyChangedEventArgs e)
        {
            RebuiltSelectedValuePath((IEnumerable) e.NewValue);
        }

        private void RebuiltSelectedValuePath(IEnumerable items_source)
        {
            _with_selected_value_path = !string.IsNullOrEmpty(SelectedValuePath);

            if (_with_selected_value_path)
            {
                _value_paths = SelectedValuePath.Split(".".ToCharArray());
                if (_value_paths.Length <= 1)
                    _value_paths = null;
            }

            // force proeprty info rebuilt
            _value_path_property_info = null;

            if (items_source == null)
                return;

            foreach (object item in items_source)
            {
                if (item == null)
                    continue;


                if (_with_selected_value_path && _value_path_property_info == null)
                {

                    if (_value_paths == null)
                    {
                        _value_path_property_info = new PropertyInfo[] { item.GetType().GetProperty(SelectedValuePath) };
                    }
                    else
                    {
                        _value_path_property_info = new PropertyInfo[_value_paths.Length];
                        object curr_item = item;
                        for (int i = 0; i < _value_paths.Length; i++)
                        {
                            string value_path = _value_paths[i];
                            _value_path_property_info[i] = curr_item.GetType().GetProperty(value_path);
                            if (i < _value_paths.Length - 1)
                                curr_item = _value_path_property_info[i].GetValue(curr_item, null);
                        }

                    }
                }
                else
                    break;
            }
        }

        private void RebuiltSelectedValuePathFromObject(object item)
        {
            if (item == null)
                return;

            _with_selected_value_path = !string.IsNullOrEmpty(SelectedValuePath);

            if (_with_selected_value_path)
            {
                _value_paths = SelectedValuePath.Split(".".ToCharArray());
                if (_value_paths.Length <= 1)
                    _value_paths = null;
            }

            // force proeprty info rebuilt
            _value_path_property_info = null;

            if (_with_selected_value_path && _value_path_property_info == null)
            {

                if (_value_paths == null)
                {
                    _value_path_property_info = new PropertyInfo[] { item.GetType().GetProperty(SelectedValuePath) };
                }
                else
                {
                    _value_path_property_info = new PropertyInfo[_value_paths.Length];
                    object curr_item = item;
                    for (int i = 0; i < _value_paths.Length; i++)
                    {
                        string value_path = _value_paths[i];
                        _value_path_property_info[i] = curr_item.GetType().GetProperty(value_path);
                        if (i < _value_paths.Length - 1)
                            curr_item = _value_path_property_info[i].GetValue(curr_item, null);
                    }

                }
            }

        }

        private object GetItemValue(object item)
        {
            if (item == null)
                return null;

            if (!_with_selected_value_path)
                return item;

            if(_value_path_property_info==null)
                RebuiltSelectedValuePath(Items);

            if (_value_path_property_info == null)
                RebuiltSelectedValuePathFromObject(item);

            if (_value_path_property_info == null)
                return null;

            if (_value_paths== null)
                return _value_path_property_info[0].GetValue(item, null);
            else
            {
                object curr_item = item;
                for (int i = 0; i < _value_paths.Length; i++)
                {
                    string display_path = _value_paths[i];
                    if (i < _value_paths.Length - 1)
                        curr_item = _value_path_property_info[i].GetValue(curr_item, null);
                }
                return _value_path_property_info[_value_path_property_info.Length - 1].GetValue(curr_item, null);
            }
        }

        private object FindItemByValue(object value)
        {
            foreach (object item in Items)
            {
                if (item == null)
                    continue;
                
                object check_value = GetItemValue(item);

                if (IsEqual(check_value, value))
                    return item;

                if (check_value == value)
                    return item;
            }

            return null;
        }

        private bool IsEqual(object obj1, object obj2)
        {
            if (obj1 == null && obj2 == null)
                return true;

            try
            {
                if (obj1 is IComparable)
                    return ((IComparable)obj1).CompareTo(obj2) == 0;
            }
            catch
            {
            }

            if (obj1 is int && obj2 is int)
                return ((int) obj1) == ((int) obj2);

            if (obj1 is long && obj2 is long)
                return ((long)obj1) == ((long)obj2);

            if (obj1 is bool && obj2 is bool)
                return ((bool)obj1) == ((bool)obj2);

            if (obj1 is double && obj2 is double)
                return ((double)obj1) == ((double)obj2);

            if (obj1 is float && obj2 is float)
                return ((float)obj1) == ((float)obj2);

            if (obj1 is short && obj2 is short)
                return ((short)obj1) == ((short)obj2);

            if (obj1 is decimal && obj2 is decimal)
                return ((decimal)obj1) == ((decimal)obj2);

            if (obj1 is byte && obj2 is byte)
                return ((byte)obj1) == ((byte)obj2);

            if (obj1 is char && obj2 is char)
                return ((char)obj1) == ((char)obj2);

            if (obj1 is uint && obj2 is uint)
                return ((uint)obj1) == ((uint)obj2);

            if (obj1 is ulong && obj2 is ulong)
                return ((ulong)obj1) == ((ulong)obj2);

            if (obj1 is ushort && obj2 is ushort)
                return ((ushort)obj1) == ((ushort)obj2);

            if (obj1 is DateTime && obj2 is DateTime)
                return ((DateTime)obj1) == ((DateTime)obj2);

            if (obj1 is TimeSpan && obj2 is TimeSpan)
                return ((TimeSpan)obj1) == ((TimeSpan)obj2);


            if (obj1 is int? && obj2 is int?)
                return ((int?)obj1) == ((int?)obj2);

            if (obj1 is long? && obj2 is long?)
                return ((long?)obj1) == ((long?)obj2);

            if (obj1 is bool? && obj2 is bool?)
                return ((bool?)obj1) == ((bool?)obj2);

            if (obj1 is double? && obj2 is double?)
                return ((double?)obj1) == ((double?)obj2);

            if (obj1 is float? && obj2 is float?)
                return ((float?)obj1) == ((float?)obj2);

            if (obj1 is short? && obj2 is short?)
                return ((short?)obj1) == ((short?)obj2);

            if (obj1 is decimal? && obj2 is decimal?)
                return ((decimal?)obj1) == ((decimal?)obj2);

            if (obj1 is byte? && obj2 is byte?)
                return ((byte?)obj1) == ((byte?)obj2);

            if (obj1 is char? && obj2 is char?)
                return ((char?)obj1) == ((char?)obj2);

            if (obj1 is uint? && obj2 is uint?)
                return ((uint?)obj1) == ((uint?)obj2);

            if (obj1 is ulong? && obj2 is ulong?)
                return ((ulong?)obj1) == ((ulong?)obj2);

            if (obj1 is ushort? && obj2 is ushort?)
                return ((ushort?)obj1) == ((ushort?)obj2);

            if (obj1 is DateTime? && obj2 is DateTime?)
                return ((DateTime?)obj1) == ((DateTime?)obj2);

            if (obj1 is TimeSpan? && obj2 is TimeSpan?)
                return ((TimeSpan?)obj1) == ((TimeSpan?)obj2);

            if (obj1 is string && obj2 is string)
                return ((string)obj1) == ((string)obj2);

            return false;
        }

        #endregion
        #endregion

        #region Event Handlers

        void SlimListBoxEx_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
           
        }

        /// <summary>
        /// Listbox item was unloaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void lbi_Unloaded(object sender, RoutedEventArgs e)
        {
            ListBoxItem lbi = sender as ListBoxItem;
            lbi.MouseUp -= lbi_MouseUp;
            lbi.StylusUp -= lbi_StylusUp;
            lbi.GotFocus -= lbi_GotFocus;
        }
        /// <summary>
        /// ListBoxItem was loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void lbi_Loaded(object sender, RoutedEventArgs e)
        {
            ListBoxItem lbi = sender as ListBoxItem;
            lbi.MouseUp += lbi_MouseUp;
            lbi.StylusUp += lbi_StylusUp;
            lbi.GotFocus += lbi_GotFocus;
        }

        
        /// <summary>
        /// Stylus went up in ListBoxItem
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void lbi_StylusUp(object sender, System.Windows.Input.StylusEventArgs e)
        {
            ListBoxItem lbi = sender as ListBoxItem;
            if (lbi.IsEnabled)
                SelectItemWithContainer(lbi);
        }
        /// <summary>
        /// Mouse went up in ListBoxItem
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void lbi_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            ListBoxItem lbi = sender as ListBoxItem;
            if(lbi.IsEnabled)
                SelectItemWithContainer(lbi);
        }

        /// <summary>
        /// Called if a listbox item gains input focus
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void lbi_GotFocus(object sender, RoutedEventArgs e)
        {
            ListBoxItem lbi = sender as ListBoxItem;
            if (lbi.IsEnabled)
                SelectItemWithContainer(lbi);
        }

        void SlimListBoxEx_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            
        }
        #endregion

        #region Properties
        #endregion

        #region Dependency Properties

        /// <summary>
        /// Gets/Sets the selected index
        /// </summary>
        public int SelectedIndex
        {
            get { return (int)GetValue(SelectedIndexProperty); }
            set { SetValue(SelectedIndexProperty, value); }
        }
        public static readonly DependencyProperty SelectedIndexProperty =
            DependencyProperty.Register("SelectedIndex", typeof(int), typeof(SlimListBoxEx), new UIPropertyMetadata(-1));

        /// <summary>
        /// Gets/Sets the selected item
        /// </summary>
        public object SelectedItem
        {
            get { return (object)GetValue(SelectedItemProperty); }
            set { SetValue(SelectedItemProperty, value); }
        }
        public static readonly DependencyProperty SelectedItemProperty =
            DependencyProperty.Register("SelectedItem", typeof(object), typeof(SlimListBoxEx), new UIPropertyMetadata(null));

        /// <summary>
        /// Gets/Sets the selected value
        /// </summary>
        public object SelectedValue
        {
            get { return (object)GetValue(SelectedValueProperty); }
            set { SetValue(SelectedValueProperty, value); }
        }
        public static readonly DependencyProperty SelectedValueProperty =
            DependencyProperty.Register("SelectedValue", typeof(object), typeof(SlimListBoxEx), new UIPropertyMetadata(null));

        /// <summary>
        /// Gets/Sets the selected value path
        /// </summary>
        public string SelectedValuePath
        {
            get { return (string)GetValue(SelectedValuePathProperty); }
            set { SetValue(SelectedValuePathProperty, value); }
        }
        public static readonly DependencyProperty SelectedValuePathProperty =
            DependencyProperty.Register("SelectedValuePath", typeof(string), typeof(SlimListBoxEx), new UIPropertyMetadata(null));

        #endregion

        #region Events
        public static readonly RoutedEvent SelectionChangedEvent = EventManager.RegisterRoutedEvent(
            "SelectionChanged", RoutingStrategy.Bubble, typeof(SelectionChangedEventHandler), typeof(SlimListBoxEx));

        // Provide CLR accessors for the event
        public event SelectionChangedEventHandler SelectionChanged
        {
            add { AddHandler(SelectionChangedEvent, value); }
            remove { RemoveHandler(SelectionChangedEvent, value); }
        }


        #endregion

        #region Routed Events
        #endregion

        #region Attributes

        private List<object> _deselected_items = new List<object>();
        private List<object> _selected_items = new List<object>();

        private bool _with_selected_value_path = false;
        private PropertyInfo[] _value_path_property_info = null;
        private string[] _value_paths = null;

        private bool _internal_selection_change = false;

        private object _buffer_selected_value = null;
        #endregion

        #region Tests
        #endregion
    }
}
