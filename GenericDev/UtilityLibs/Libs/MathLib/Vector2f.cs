using System;
using System.Collections;
using System.Collections.Generic;

namespace MathLib
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public struct Vector2f
    {
        public static bool IsNull(Vector2f v)
        {
            if (v.X == 0 || v.Y == 0)
                return true;
            else
                return false;
        }

        public static Vector2f GetVector2fFromString(string geo_string)
        {
            string[] arr_lonlat = geo_string.Split(" ".ToCharArray());
            string lon = arr_lonlat[0];
            string lat = arr_lonlat[1];

            float flat = Convert.ToSingle(lat.Replace(".", ","));
            float flon = Convert.ToSingle(lon.Replace(".", ","));

            return new MathLib.Vector2f(flon, flat);
        }

        public static explicit operator Vector2d(Vector2f v)
        {
            return new Vector2d(Convert.ToDouble(v.X), Convert.ToDouble(v.Y));
        }

		public static Vector2f[] ArrayList2VectorArray(ArrayList al)
		{
			object[] oarr = al.ToArray();
			Vector2f[] varr = new Vector2f[oarr.Length];
			for(int i=0;i<varr.Length;i++)
			{
				varr[i] = (Vector2f)oarr[i];
			}
			return varr;
		}


		public float Determinante(Vector2f v2)
		{
			return (this.X * v2.Y) - (v2.X * this.Y);
		}

		/// <summary>
		/// der �bergebene vektor wird als richtungsvektor gesehen
		/// der winkel, verh�ltnis wird zur�ckgegeben
		/// </summary>
		/// <param name="v"></param>
		/// <returns></returns>
		public float GetAngle()
		{
			if(X == 0 && Y == 0)
				return 0;
			if(X == 0 && Y > 0)
				return (float)(Math.PI/2);
			if(X == 0 && Y < 0)
                return (float)(Math.PI + Math.PI / 2);
			if(Y == 0 && X>0)
				return 0;
			if(Y ==0 && X<0)
				return (float)(Math.PI);
			if(X>0 && Y>0)
				return (float)(Math.Atan(Y/X));
			if(X<0 && Y>0)
				return (float)(Math.PI + Math.Atan(Y/X));
			if(X<0 && Y<0)
				return (float)(Math.PI + Math.Atan(Y/X));
			if(X>0 && Y<0)
				return (float)(Math.PI + Math.PI + Math.Atan(Y/X));
			return 0;
//			return Math.Atan(Y/X);
		}

//		public float GetAngle(Vector tocompare){
//			float myangle = GetAngle();
//			float otherangle = tocompare.GetAngle();
//			return otherangle-myangle;
//		}

        public double GetAngle(Vector2f tocompare)
        {
            //double angle = GetLen()*tocompare.GetLen();
            double angle = Math.Acos(Math.Round(this.GetSkalarProduct(tocompare) / (this.GetLen() * tocompare.GetLen()),6));

            return angle;
        }

        /// <summary>
        /// Berechnet die Steigung des Vektors.
        /// </summary>
        /// <returns>
        /// * Wenn X und Y = 0, dann float.Nan
        /// * Wenn X = 0 und Y > 0, dann float.PositiveInfinity
        /// * Wenn X = 0 und Y < 0, dann float.NegativeInfinity 
        /// </returns>
        public float GetGradient()
        {
            if(X == 0)
            {
                if (Y == 0)
                {
                    return float.NaN;
                }
                else if (Y > 0)
                {
                    return float.PositiveInfinity;
                }
                else if (Y < 0)
                {
                    return float.NegativeInfinity;
                }
            }

            return  Y / X;
        }



		/// <summary>
		/// 
		/// </summary>
		/// <param name="angle">Radianten bitte!</param>
		public void Rotate(float angle){
//			X = Math.Cos(angle) * X + Math.Sin(angle)*y;
//			y = -Math.Sin(angle) * X + Math.Cos(angle)*y;


			float betrag = GetLen();
			float phase = GetAngle();
			
//			if(angle > Math.PI+Math.PI/2){
//				angle = angle % Math.PI;
//				phase -= angle;
//			}
//			else
//			{
				phase -= angle;
//			}
			
			X = (float)Math.Cos(phase)*betrag;
			Y = (float)Math.Sin(phase)*betrag;
		}

        //public Vector2f() : this(0f, 0f)
        //{}

		public Vector2f(float x,float y){
			X = x;
			Y = y;
		}

        public Vector2f(Vector2d v)
        {
            X = (float)v.X;
            Y = (float)v.Y;
        }

        public string FormattedX
        {
            get
            {
                return Math.Round(X,3).ToString();
            }
        }
        public string FormattedY
        {
            get
            {
                return Math.Round(Y, 3).ToString();
            }
        }

        //public double XD
        //{
        //    get
        //    {
        //        return X;
        //    }
        //    set
        //    {
        //        this.X = (float)value;
        //    }
        //}
        //public double YD
        //{
        //    get
        //    {
        //        return y;
        //    }
        //    set
        //    {
        //        this.y = (float)value;
        //    }
        //}

		public override string ToString() {
			return "X="+X+"; y="+Y+";";
		}

		public static bool operator ==(Vector2f v1, Vector2f v2) {
            // If both are null, or both are same instance, return true.
            if (System.Object.ReferenceEquals(v1, v2))
                return true;

            // If one is null, but not both, return false.
            if (((object)v1 == null) || ((object)v2 == null))
            {
                return false;
            }

            return (v1.X == v2.X && v1.Y == v2.Y);
		}

		public static bool operator !=(Vector2f v1, Vector2f v2) {
            return !(v1 == v2);
		}
		public static Vector2f operator +(Vector2f a,Vector2f b){
			Vector2f c = new Vector2f(0,0);
			c.X = a.X + b.X;
			c.Y = a.Y + b.Y;
			return c;
		}
        public static Vector2f operator +(Vector2f a, float b)
        {
            Vector2f c = new Vector2f(0, 0);
            c.X = a.X + b;
            c.Y = a.Y + b;
            return c;
        }
        public static Vector2f operator -(Vector2f a, float b)
        {
            Vector2f c = new Vector2f(0, 0);
            c.X = a.X - b;
            c.Y = a.Y - b;
            return c;
        }
		public static Vector2f operator *(Vector2f a,float skalar){
			return new Vector2f(a.X*skalar,a.Y*skalar);
		}
        public static Vector2f operator /(Vector2f a, float skalar)
        {
            return new Vector2f(a.X / skalar, a.Y / skalar);
        }
		public  static Vector2f operator -(Vector2f a, Vector2f b){
			Vector2f c = new Vector2f(a.X-b.X,a.Y-b.Y);
			return c;
		}

        public static double DegToRad(double deg)
        {
            return (deg * Math.PI) / 180;
        }
        public static double RadToDeg(double rad)
        {
            return (rad * 180) / Math.PI;
        }
        public static float DegToRad(float deg)
        {
            return (deg * (float)Math.PI) / 180f;
        }
        public static float RadToDeg(float rad)
        {
            return (rad * 180f) / (float)Math.PI;
        }

        public bool IsNull()
        {
            return X == 0 && Y == 0;
        }

        public float GetLen(){
			return (float)Math.Sqrt(X*X+Y*Y);
		}
		public Vector2f GetNorm(){
			Vector2f b = new Vector2f(0,0);
		    float len = GetLen();
            float len_kehrwert = (1f / len);
            if (len == 0)
                len_kehrwert = 0;
			b.X = X * len_kehrwert;
			b.Y = Y * len_kehrwert;
			return b;
		}

        public float GetNormDistance(Vector2f b)
        {
            float scal_pro = GetSkalarProduct(b);
            Vector2f proj_vect = GetNorm()*scal_pro;
            return (proj_vect - b).GetLen();
        }

		public float GetSkalarProduct(Vector2f b){
			return X*b.X+Y*b.Y;
		}
		public float GetSkalarProduct(){
			return X*X+Y*Y;
		}
		public Vector2f GetMirror(){
			Vector2f b = new Vector2f(0,0);
			b.X = (-X);
			b.Y = (-Y);
			return b;
		}
		public Vector2f GetOrthogonal1(){
			Vector2f b = new Vector2f(0,0);
			b.X = (-Y);
			b.Y = (X);
			return b;
		}
		public Vector2f GetOrthogonal2(){
			Vector2f b = new Vector2f(0,0);
			b.X = (Y);
			b.Y = (-X);
			return b;
		}

        public override bool Equals(Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            Vector2f v = (Vector2f)obj;
            if ((Object)v == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (X == v.X) && (Y == v.Y);
        }

		public override int GetHashCode()
		{
		    return X.GetHashCode() ^ Y.GetHashCode();
		}

        public float X;
        public float Y;




        public static List<Vector2f> GetVector2fListFromString(string geom)
        {
            List<Vector2f> v_list = new List<Vector2f>();
            string[] sarr_geom = geom.Split(",".ToCharArray());
            foreach (string s_v in sarr_geom) {
                Vector2f v = Vector2f.GetVector2fFromString(s_v);
                v_list.Add(v);
            }
            return v_list;
        }

        public static Vector2f[] GetVector2fArrayFromString(string geom)
        {
            List<Vector2f> v_list = GetVector2fListFromString(geom);
            return v_list.ToArray();
        }

    }
}
