﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using PoCShared;
using PoCWebApp.Data;

namespace PoCWebApp.Controllers.Api
{
    public class PrinterController : ApiController
    {
        // GET api/<controller>
        public IEnumerable<PrinterItem> Get(string printServer = null)
        {
            using (var context = new PoCPrinterContext())
            {
                if(!string.IsNullOrEmpty(printServer))
                    return context.Printers.Where(p => string.Equals(p.PrintServer, printServer, StringComparison.InvariantCultureIgnoreCase)).ToList();

                return context.Printers.ToList();
            }
        }

        // GET api/<controller>/5
        public PrinterItem Get(int id)
        {
            using (var context = new PoCPrinterContext())
            {
                return context.Printers.FirstOrDefault(p => p.Id == id);
            }
        }

        // POST api/<controller>
        public PrinterItem Post([FromBody]PrinterItem value)
        {
            using (var context = new PoCPrinterContext())
            {
                var existing = context.Printers.FirstOrDefault(p => p.Name == value.Name && p.PrintServer == value.PrintServer);
                if (existing != null)
                {
                    return existing;
                }

                context.Printers.Add(value);
                context.SaveChanges();

                return value;
            }
        }

        // PUT api/<controller>/5
        public PrinterItem Put(int id, [FromBody]PrinterItem value)
        {
            using (var context = new PoCPrinterContext())
            {
                var existing = context.Printers.FirstOrDefault(p => p.Name == value.Name && p.PrintServer == value.PrintServer);
                if (existing != null)
                {
                    existing.PrintServer = value.PrintServer;
                    existing.Network = value.Network;
                    existing.Name = value.Name;
                    existing.Status = value.Status;

                    context.SaveChanges();

                    return value;
                }
            }

            return null;
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
            using (var context = new PoCPrinterContext())
            {
                var existing =
                    context.Printers.FirstOrDefault(p => p.Id == id);
                if (existing != null)
                {
                    context.Printers.Remove(existing);
                    context.SaveChanges();
                }
            }
        }
    }
}