using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace MathLib
{
    public class SparseMatrix
    {
        public SparseMatrix(uint x, uint y)
        {
            _rows = x;
            _cols = y;
        }

        public void Populate()
        {
            uint size = _rows * _cols;

            for (uint i = 1; i <= size; i++)
            {
                Console.WriteLine("\n\n\nenter the index of entry {0}", i);

                Console.WriteLine("Enter the row index or enter 'f' if you are finished entering elements:");
                String str1 = Console.ReadLine();

                if (str1 == "f")
                    break;

                _row = UInt32.Parse(str1);
                Console.WriteLine("Enter the coloumn index:");
                String str2 = Console.ReadLine();
                _col = UInt32.Parse(str2);
                str1 = String.Concat(str1, ",", str2);

                if (_row >= _rows || _col >= _cols)
                {
                    Console.WriteLine("\n\n\ngiven index out of range. Enter again");
                    --i;
                }
                else if (_sparsematrix_nz_elems.ContainsKey(str1))
                {
                    Console.WriteLine("n\n\nElement with the given index already exists. Do you want to overwrite the existing value? Type 'y' for yes, 'n' for no");
                    String x = Console.ReadLine();
                    if (x == "y")
                    {
                        Console.WriteLine("enter the value of the element:");
                        String str3 = Console.ReadLine();
                        double val = double.Parse(str3);
                        if (val != 0)
                            _sparsematrix_nz_elems[str1] = val;
                        else
                            _sparsematrix_nz_elems.Remove(str1);
                    }
                    else
                        --i;
                }

                else
                {
                    Console.WriteLine("enter the value of the element:");
                    String str3 = Console.ReadLine();
                    double val = double.Parse(str3);

                    if (val != 0)
                        _sparsematrix_nz_elems.Add(str1, val);
                }
            }
            return;
        }

        public uint TotalElements()
        {
            return _row * _col;
        }

        public int TotalNonzero()
        {
            return _sparsematrix_nz_elems.Count;
        }

        public override string ToString()
        {
            string s = "";
            for (uint i = 1; i <= _rows; i++)
            {
                s += "\n\n\n";
                for (uint j = 1; j <= _cols; j++)
                {
                    String str = String.Concat(Convert.ToString(i), ",", Convert.ToString(j));
                    if (_sparsematrix_nz_elems.ContainsKey(str))
                        s += "\t\t"+ _sparsematrix_nz_elems[str];
                    else
                        s += "\t\t0";
                }
            }
            s += "\n\n\n";
            return s;
        }

        public double GetValue(uint row1, uint col1)
        {
            if (!_sparsematrix_nz_elems.ContainsKey(String.Concat(Convert.ToString(row1), ",", Convert.ToString(col1))))
                return 0;
            else
                return (double)(_sparsematrix_nz_elems[String.Concat(Convert.ToString(row1), ",", Convert.ToString(col1))]);
        }

        public void SetValue(uint row1, uint col1, double value)
        {
            String str = String.Concat(Convert.ToString(row1), ",", Convert.ToString(col1));

            if (_sparsematrix_nz_elems.ContainsKey(str))
            {
                Console.WriteLine("Element with the given index already exists. Do you want to overwrite the existing value? Type 'y' for yes, 'n' for no");
                String x = Console.ReadLine();
                if (x == "y")
                {
                    if (value != 0)
                        _sparsematrix_nz_elems[str] = value;
                    else
                        _sparsematrix_nz_elems.Remove(str);
                }
                else
                    return;
            }

            else
            {
                if (value != 0)
                    _sparsematrix_nz_elems.Add(str, value);
            }
            return;
        }

        private Hashtable _sparsematrix_nz_elems = new Hashtable();
        private uint _rows, _cols, _row, _col;

    }
}
