﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PoCShared;
using WebHardwareClient.ApiClient;
using WebHardwareClient.Core;
using WebHardwareClient.SignalR;

namespace WebHardwareClient
{
    class Program
    {
        public static List<PrinterItem> Printers { get; private set; }
        private static SignalClient _client;

        static void Main(string[] args)
        {
            RegisterPrinters();
            RunClient();

            Console.WriteLine("***");
            Console.WriteLine("*** Goto: http://http://awimob02/pocprint/Home/Print and try to print a server-sample doc on your PC");
            Console.WriteLine("***");
            Console.WriteLine("***");
            Console.WriteLine("*** ... waiting for print commands ...");
            Console.WriteLine("***");
            Console.WriteLine("*** Hit ENTER to quit");
            Console.WriteLine("***");

            Console.ReadLine();

            _client.StopClient();
        }

        static async void RunClient()
        {
            _client = new SignalClient();
            await _client.StartClient();
        }

        static void RegisterPrinters()
        {
            Printers = LocalPrinters.GetPrinters();
            Console.WriteLine("----------------------");
            Console.WriteLine("----------------------");
            Console.WriteLine(" ");
            var apiClient = new PrinterApiClient(ConfigurationManager.AppSettings["apihost"]);

            Console.WriteLine(("**** Registering printers "));

            foreach (var printer in Printers)
            {
                Console.WriteLine("----------------------");

                Console.WriteLine("\tAdding '" + printer.Name + "'...");
                try
                {
                    var addedPrinter = apiClient.Create(printer).Result;
                    if (addedPrinter != null)
                    {
                        Console.WriteLine("\tsuceed: ID='" + addedPrinter.Id + "'");
                    }
                    else
                    {
                        Console.WriteLine("\tFAILED");
                    }
                }
                catch (AggregateException aex)
                {
                    foreach (var ex in aex.InnerExceptions)
                    {
                        Console.WriteLine("\tFAILED: " + ex.Message);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("\tFAILED: " + ex.Message);
                }
                
            }

            Console.WriteLine("----------------------");
            Console.WriteLine("----------------------");
            Console.WriteLine(" ");
        }
    }
}
