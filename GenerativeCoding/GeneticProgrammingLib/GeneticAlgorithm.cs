﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using Logicx.Utilities;


namespace GeneticProgrammingLib
{
    public class GeneticAlgorithm<T, U> where T : Dna<U>, new()
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="target_dna">die genetic_target dna wird verwendet um die fitness zu überprüfen</param>
        /// <param name="population_iteration">es gibt keinen exakten population count da die anzahl der erzeugten kinder random</param>
        /// <param name="mutation_rate"></param>
        public GeneticAlgorithm()
        {
        }

        public void Run(bool in_background)
        {
            if (in_background)
            {
                if (_bg_worker != null)
                {
                    _bg_worker.Dispose();
                    _bg_worker = null;
                }
                _bg_worker = new QueuedBackgroundWorker("run genetic algorithm in bg", true);
                _bg_worker.DoWork += _bg_worker_DoWork;
                _bg_worker.RunWorkerAsync(IntervalMsec);
            }
            else
                _bg_worker_DoWork(null, null);
        }

        private void _bg_worker_DoWork(object sender, DoWorkEventArgs e)
        {
            int interval_ms = 0;
            if (e != null && e.Argument != null && e.Argument is int)
                interval_ms = (int)e.Argument;

            do
            {
                UpdateFrame();

                //Console.WriteLine("sim_end");
                if (interval_ms > 0)
                    System.Threading.Thread.Sleep(interval_ms);
            }
            while (true);
        }

        public void UpdateFrame()
        {
            //Console.WriteLine("sim_Start");
            if (_frame_count > 0 && _frame < _frame_count && !_all_dead)
            {
                _all_dead = true;
                foreach (var dna in Population)
                {
                    if (!dna.IsDead)
                        dna.SimulationUpdate(_frame);
                    if (!dna.IsDead)
                        _all_dead = false;
                }

                _frame++;
            }
            else
            {
                //simulation finished, reset frame to 0
                _frame = 0;
                _all_dead = false;

                Dna<U> local_best_dna = CalcFitness(out var local_sum_fitness);

                ////debug info + stop if finished
                //if (OverallBestDna.FitnessProbality >= 0.99)
                //{
                //    Console.WriteLine("Found it: " + OverallBestDna);
                //    return;
                //}
                //else if (_generations_created % 100 == 0)
                //    Console.WriteLine("Generation " + _generations_created + " " + local_best_dna.Fitness + " " + local_best_dna + " -overallbest-> " + OverallBestDna);

                CreateNewPopulation(local_sum_fitness);
            }
        }



        /// <summary>
        /// updates the fitness of each dna object in the current population
        /// sorts the list in the populiation by fitness (best first)
        /// </summary>
        /// <param name="local_sum_fitness"></param>
        /// <returns></returns>
        public Dna<U> CalcFitness(out double local_sum_fitness)
        {
            _r = new Random();
            local_sum_fitness = 0;


            Dna<U> local_best_dna = null;


            foreach (T dna in Population)
            {

                //update fitness
                dna.CalcFitness();
                local_sum_fitness += dna.Fitness;

                //save best dna
                if (local_best_dna == null || dna.Fitness > local_best_dna.Fitness)
                    local_best_dna = dna;
            }

            if (OverallBestDna == null || local_best_dna.Fitness > OverallBestDna.Fitness)
                OverallBestDna = local_best_dna;

            Population = Population.OrderByDescending(d => d.Fitness).ToList();

            return local_best_dna;
        }



        private Dna<U> PickOne(double sum_fitness)
        {
            var index = 0;
            var r = _r.NextDouble();

            while (r > 0)
            {
                r = r - (Population[index].Fitness / sum_fitness);
                index++;
            }

            index--;
            return Population[index];
        }


        private Dna<U> SelectRandomWithProbility(double local_sum_fitness)
        {
            double pick_value = _r.NextDouble() * (local_sum_fitness / 2);
            local_sum_fitness = 0;
            for (int i = 0; i < Population.Count; i++)
            {
                local_sum_fitness += Population[i].Fitness;
                if (local_sum_fitness >= pick_value)
                    return Population[i];
            }


            throw new Exception("unexpected");
        }

        public void CreateNewRandomPopulation()
        {
            Population = new List<T>(PopulationSize);


            for (int i = 1; i < PopulationSize; i++)
            {
                T dna = new T();
                dna.Id = i.ToString();
                dna.InitializeRandomGen();
                Population.Add(dna);
            }
        }

        public void CreateNewPopulation(double local_sum_fitness)
        {
            _generations_created++;
            List<T> new_population = new List<T>(PopulationSize);


            //mache fix mit den beider besten kandidaten mehrere kinder
            T first = Population[0];
            T second = Population[1];

            for (int j = 0; j < RandomChildrenCount.Generate()*10; j++)
            {
                Dna<U> child = first.CreateChildMidpoint(second, MutationRate);
                //child.Id = first.Id + "+" + second.Id + "." + j + "."; 
                new_population.Add((T)child);
            }

            for (int j = 10; j < Population[0].Genes.Count; j+=20)
            {
                for (int k = 0; k < 10; k++)
                {
                    Dna<U> child = first.CreateChildAfterMidpoint( j,MutationRate * k*5);
                    //child.Id = first.Id + "+" + second.Id + "." + j + "."; 
                    new_population.Add((T)child);
                }
            }

            for (int i = 0; i < PopulationSize; i++)
            {
                Dna<U> father = SelectRandomWithProbility(local_sum_fitness);
                Dna<U> mother = SelectRandomWithProbility(local_sum_fitness);
                int count_children = RandomChildrenCount.Generate();
                for (int j = 0; j < count_children; j++)
                {
                    Dna<U> child = father.CreateChildMidpoint(mother, MutationRate);
                    //child.Id = father.Id + "+" + mother.Id + "."+j+".";
                    new_population.Add((T)child);
                }
            }


            T first_clone = (T)first.CreateChildClone();
            first_clone.IsBest = true;
            new_population.Add(first_clone);

            lock (Population)
            {
                OldPopulation = Population;
                Population = new_population;
            }
        }

        /// <summary>
        /// gibt an wieviel simulations update gemacht werden müssen, bevor die fitness bestimmt werden kann
        /// es muss zuerst der phenologische objekt in form einer simulation ausgewertet werden, nach der simulation kann die fitness
        /// berechnet werden.
        /// In manchen Fällen muss keine simulation durchgeführt werden. zb das sentence app sample. sobald ein satz bestimmt wurde kann dieser
        /// fitness wert sofort ausgewertet werden.
        /// </summary>
        protected int _frame_count = 0;
        protected int _frame;

        public List<T> Population;
        public List<T> OldPopulation;
        /// <summary>
        /// die mutationsrate - > selbsterklärend
        /// </summary>
        public static double MutationRate = 0.05;
        /// <summary>
        /// mit diesem integer kannst du bestimmen von wiewielen elternpaaren
        /// ein kind generiert werden soll
        /// diese variable wird bei create population gebraucht
        /// </summary>
        public static int PopulationSize = 200;
        /// <summary>
        /// eine variable die mitspeichert die wievielte generation erstellt wurde
        /// </summary>
        protected int _generations_created = 0;

        public static int IntervalMsec = 0;
        protected bool _all_dead;

        protected static Random _r = new Random();

        public Dna<U> OverallBestDna = null;
        QueuedBackgroundWorker _bg_worker;
    }
}
