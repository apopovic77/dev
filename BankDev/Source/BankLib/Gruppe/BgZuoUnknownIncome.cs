﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BankLib.Data;

namespace BankLib.Gruppe
{
    public class BgZuoUnknownIncome : BgZuo
    {
        internal override void WriteToStream(BinaryWriter writer)
        {
            BinaryHelper.WriteString(writer, "BgZuoUnknownIncome");
            base.WriteToStream(writer);
        }

        /// <summary>
        /// Es kann immer eingefügt werden
        /// </summary>
        /// <param name="b"></param>
        /// <returns></returns>
        protected override bool CheckZuo(Buchung b)
        {
            if (b.Betrag > 0)
                return true;
            return false;
        }


        internal override int RulePriority
        {
            get { return int.MaxValue; }
        }


        public override string ToString()
        {
            return "[BgZuoUnknownIncome]";
        }

    }
}
