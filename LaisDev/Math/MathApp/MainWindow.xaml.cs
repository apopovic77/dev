﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MathApp
{

    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            

            List<QuadratischeGleichung> gleichungen = new List<QuadratischeGleichung>();
            for (int p = -3; p < 3; p++)
            {
                for (int q = -3; q < 3; q++)
                {
                    QuadratischeGleichung qg = new QuadratischeGleichung(p,q);
                    if ((int)qg.X1 == qg.X1 && (int)qg.X2 == qg.X2)
                        gleichungen.Add(qg);
                }
            }

            listview.ItemsSource = gleichungen;
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                double p = Convert.ToDouble(p_tbx.Text);
                double q = Convert.ToDouble(q_tbx.Text);

                double diskriminante = Math.Pow(p, 2) - 4*q;
                diskriminate_tb.Text = diskriminante.ToString();

                double x1 = -p/2 + Math.Sqrt(Math.Pow(p/2, 2) - q);
                double x2 = -p/2 - Math.Sqrt(Math.Pow(p/2, 2) - q);

                x1_tb.Text = x1.ToString();
                x2_tb.Text = x2.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), "ERROR");
            }

        }
    }
}
