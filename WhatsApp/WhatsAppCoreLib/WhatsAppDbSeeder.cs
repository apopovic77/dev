﻿using System;
using System.Threading.Tasks;
using Awanto.WhatsAppCore.Services;
using Awanto.WhatsAppCore.Utilities;

namespace Awanto.WhatsAppCore
{
    public class WhatsAppDbSeeder
    {
        public WhatsAppDbSeeder(WhatsAppDbContext ctx)
        {
            _ctx = ctx;
        }

        public async Task Seed()
        {
            _ctx.Database.EnsureCreated();

            //install notification listener für the python server app
            SqlDependencyEx sql_dependency = new SqlDependencyEx(
                WhatsAppService.GetConnectionString(),
                WhatsAppService.GetDatabaseName(),
                WhatsAppService.GetDatabaseUserName(),
                "Nachrichten",
                "dbo",
                SqlDependencyEx.NotificationTypes.Insert,
                false,
                "WhatsAppServer");

            sql_dependency.InstallNotification();
            Console.WriteLine("OK: Installed!");
        }

        private readonly WhatsAppDbContext _ctx;
    }
}
