﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace LoggingLib.Data.Serialization
{
    /// <summary>
    /// LogEntry encapsulation class used for serializing/deserializing 
    /// all kind of LogEntry and child classes
    /// </summary>
    public class LogEntryDataItem : IXmlSerializable
    {
        /// <summary>
        /// Default constructor for deserialization
        /// </summary>
        public LogEntryDataItem()
        {
        }

        /// <summary>
        /// Constructor initializing the attributes with log entry
        /// </summary>
        /// <param name="le"></param>
        public LogEntryDataItem(LogEntry le)
        {
            _data = le;
            _qualified_type_name = le.GetType().AssemblyQualifiedName;
        }

        /// <summary>
        /// the full qualified type name
        /// </summary>
        [XmlAttribute]
        public string QualifiedTypeName
        {
            get { return _qualified_type_name; }
            set { _qualified_type_name = value; }
        }

        /// <summary>
        /// Data of type QualifiedTypeName
        /// </summary>
        [XmlElement]
        public object Data
        {
            get { return _data; }
            set
            {
                _data = value;

                if (_data != null)
                {
                    if (_data.GetType().AssemblyQualifiedName != _qualified_type_name)
                        _qualified_type_name = _data.GetType().AssemblyQualifiedName;
                }
            }
        }

        /// <summary>
        /// Gets/Sets the data type
        /// </summary>
        [XmlIgnore]
        public System.Type DataType
        {
            get
            {
                return Type.GetType(_qualified_type_name);
            }
            set
            {
                if (value != null)
                    QualifiedTypeName = value.AssemblyQualifiedName;
            }
        }

        /// <summary>
        /// Gets the XmlSerialization-Schema of the current object
        /// </summary>
        /// <returns></returns>
        public XmlSchema GetSchema()
        {
            // not used
            return null;
        }

        /// <summary>
        /// Manually reads in the current object's data
        /// </summary>
        /// <param name="reader">current reader instance</param>
        public void ReadXml(XmlReader reader)
        {
            // Read attributes
            _qualified_type_name = reader.GetAttribute("QualifiedTypeName");

            // advance to first child note ( Data )
            reader.Read();

            // Deserialize Data depending on the object definition from above
            DataContractSerializer dcs = new DataContractSerializer(DataType);
            Data = dcs.ReadObject(reader);

            // Advance to next object's node !
            reader.Read();
        }

        /// <summary>
        /// Manually write current object's data
        /// </summary>
        /// <param name="writer">current writer instance</param>
        public void WriteXml(XmlWriter writer)
        {
            // create serializer instance for data object
            XmlSerializer ser = new XmlSerializer(this.DataType);

            // Write node attributes
            writer.WriteAttributeString("QualifiedTypeName", _qualified_type_name);

            // serialize dynmaic object
            //ser.Serialize(writer, Data);

            DataContractSerializer dcs = new DataContractSerializer(DataType);
            dcs.WriteObject(writer, Data);
        }

        #region Attribs

        private string _qualified_type_name = "";
        private object _data = null;
        #endregion
    }
}
