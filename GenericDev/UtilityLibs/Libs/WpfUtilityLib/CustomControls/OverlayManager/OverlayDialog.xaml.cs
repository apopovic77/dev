﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using Logicx.WpfUtility.CustomControls.ChangeInfo;
using Logicx.WpfUtility.CustomControls.Validation;
using Logicx.WpfUtility.WindowManagement;
using Logicx.WpfUtility.WpfHelpers;
using MathLib;

namespace Logicx.WpfUtility.CustomControls.OverlayManager
{
    public enum OverlayDialogState
    {
        Modal,
        Normal,
        Minimized,
        Maximized
    }

    public enum OverlayDialogMaximizeMode
    {
        Normal,
        ReserveErrorViewerSpace,
    }

    public enum OverlayDialogCenteringMode
    {
        Normal,
        EnsureErrorViewerSpace,
    }

    public enum OverlayDialogInitialShowMode
    {
        Optimized,
        Maximized,
        Custom
    }

    #region Window state and activity change event argument classes
    public class WindowStateChangeArgs : EventArgs
    {
        public WindowStateChangeArgs(OverlayDialogState oldState, OverlayDialogState newState)
        {
            _old_state = oldState;
            _new_state = newState;
        }

        public OverlayDialogState OldState
        {
            get { return _old_state; }
        }

        public OverlayDialogState NewState
        {
            get { return _new_state; }    
        }

        private OverlayDialogState _old_state = OverlayDialogState.Normal;
        private OverlayDialogState _new_state = OverlayDialogState.Normal;

    }

    public class WindowStateChangeingArgs : EventArgs
    {
        public WindowStateChangeingArgs(OverlayDialogState curState, OverlayDialogState newState)
        {
            _cur_state = curState;
            _new_state = newState;
        }

        public OverlayDialogState CurState
        {
            get { return _cur_state; }
        }

        public OverlayDialogState NewState
        {
            get { return _new_state; }
        }

        public bool CanChange
        {
            get { return _can_change; }
            set { _can_change = value; }
        }

        public bool DelayedChange
        {
            get { return _delayed_change; }
            set { _delayed_change = value; }
        }

        private OverlayDialogState _cur_state = OverlayDialogState.Normal;
        private OverlayDialogState _new_state = OverlayDialogState.Normal;
        private bool _can_change = true;
        private bool _delayed_change = false;

    }


    public class ActiveStateChangeArgs : EventArgs
    {
        public ActiveStateChangeArgs(bool oldState, bool newState)
        {
            _old_state = oldState;
            _new_state = newState;
        }

        public bool OldState
        {
            get { return _old_state; }
        }

        public bool NewState
        {
            get { return _new_state; }
        }

        private bool _old_state;
        private bool _new_state;

    }

    public class ActiveStateChangeingArgs : EventArgs
    {
        public ActiveStateChangeingArgs(bool curState, bool newState)
        {
            _cur_state = curState;
            _new_state = newState;
        }

        public bool CurState
        {
            get { return _cur_state; }
        }

        public bool NewState
        {
            get { return _new_state; }
        }

        public bool CanChange
        {
            get { return _can_change; }
            set { _can_change = value; }
        }

        public bool DelayedChange
        {
            get { return _delayed_change; }
            set { _delayed_change = value; }
        }

        private bool _cur_state;
        private bool _new_state;
        private bool _can_change = true;
        private bool _delayed_change = false;

    }
    #endregion

    /// <summary>
    /// Interaction logic for OverlayDialog.xaml
    /// </summary>
    public partial class OverlayDialog : UserControl, IDisposable
    {
        #region Construction and Initialization
        public const int IMAGE_DPI = 96;

        /// <summary>
        /// Static constructor
        /// </summary>
        static OverlayDialog()
        {
            FrameworkPropertyMetadata metadataCloseButtonStyle = new FrameworkPropertyMetadata();
            CloseButtonStyleProperty = DependencyProperty.Register("CloseButtonStyle",
                                                            typeof(Style), typeof(OverlayDialog), metadataCloseButtonStyle);

            FrameworkPropertyMetadata metadataHelpButttonStyle = new FrameworkPropertyMetadata();
            HelpButtonStyleProperty = DependencyProperty.Register("HelpButtonStyle",
                                                                  typeof (Style), typeof (OverlayDialog),metadataHelpButttonStyle);

            FrameworkPropertyMetadata metadataPrintButtonStyle = new FrameworkPropertyMetadata();
            PrintButtonStyleProperty = DependencyProperty.Register("PrintButtonStyle",
                                                            typeof(Style), typeof(OverlayDialog), metadataPrintButtonStyle);

            FrameworkPropertyMetadata metadataOverlayBackgroundBrush = new FrameworkPropertyMetadata(new SolidColorBrush(Color.FromArgb(100, 50, 50, 50)));
            OverlayBackgroundBrushProperty = DependencyProperty.Register("OverlayBackgroundBrush",
                                                            typeof(SolidColorBrush), typeof(OverlayDialog), metadataOverlayBackgroundBrush);


            FrameworkPropertyMetadata metadataMinimizeButtonStyle = new FrameworkPropertyMetadata();
            MinimizeButtonStyleProperty = DependencyProperty.Register("MinimizeButtonStyle",
                                                            typeof(Style), typeof(OverlayDialog), metadataMinimizeButtonStyle);

            FrameworkPropertyMetadata metadataMaximizeButtonStyle = new FrameworkPropertyMetadata();
            MaximizeButtonStyleProperty = DependencyProperty.Register("MaximizeButtonStyle",
                                                            typeof(Style), typeof(OverlayDialog), metadataMaximizeButtonStyle);

            FrameworkPropertyMetadata metadataNormalizeButtonStyle = new FrameworkPropertyMetadata();
            NormalizeButtonStyleProperty = DependencyProperty.Register("NormalizeButtonStyle",
                                                            typeof(Style), typeof(OverlayDialog), metadataNormalizeButtonStyle);

            // default dialog border styleu
            Style defaultBorderStyle = new Style(typeof(Border));
            defaultBorderStyle.Setters.Add(new Setter(Border.BorderBrushProperty, new SolidColorBrush(Color.FromArgb(255, 148, 148, 148))));
            defaultBorderStyle.Setters.Add(new Setter(Border.BackgroundProperty, new SolidColorBrush(Color.FromArgb(200, 150, 150, 150))));
            defaultBorderStyle.Setters.Add(new Setter(Border.BorderThicknessProperty, new Thickness(1)));
            defaultBorderStyle.Setters.Add(new Setter(Border.OpacityProperty, (double)1));
            defaultBorderStyle.Setters.Add(new Setter(Border.CornerRadiusProperty, new CornerRadius(8)));
            defaultBorderStyle.Setters.Add(new Setter(Border.PaddingProperty, new Thickness(8)));

            FrameworkPropertyMetadata metadataDialogBorderStyle = new FrameworkPropertyMetadata(defaultBorderStyle);
            DialogBorderStyleProperty = DependencyProperty.Register("DialogBorderStyle",
                                                            typeof(Style), typeof(OverlayDialog), metadataDialogBorderStyle);

            // default dialog border style
            Style defaultContentBorderStyle = new Style(typeof(Border));
            defaultContentBorderStyle.Setters.Add(new Setter(Border.BorderBrushProperty, Brushes.Transparent));
            defaultContentBorderStyle.Setters.Add(new Setter(Border.BackgroundProperty, Brushes.Transparent));
            defaultContentBorderStyle.Setters.Add(new Setter(Border.BorderThicknessProperty, new Thickness(1)));
            defaultContentBorderStyle.Setters.Add(new Setter(Border.OpacityProperty, (double)1));
            defaultContentBorderStyle.Setters.Add(new Setter(Border.CornerRadiusProperty, new CornerRadius(0)));
            defaultContentBorderStyle.Setters.Add(new Setter(Border.PaddingProperty, new Thickness(0)));

            FrameworkPropertyMetadata metadataDialogContentBorderStyle = new FrameworkPropertyMetadata(defaultContentBorderStyle);
            DialogContentBorderStyleProperty = DependencyProperty.Register("DialogContentBorderStyle",
                                                            typeof(Style), typeof(OverlayDialog), metadataDialogContentBorderStyle);

            FrameworkPropertyMetadata metadataDialogButtonStyle = new FrameworkPropertyMetadata(null);
            DialogButtonStyleProperty = DependencyProperty.Register("DialogButtonStyle",
                                                            typeof(Style), typeof(OverlayDialog), metadataDialogButtonStyle);


            FrameworkPropertyMetadata metadatMinimalSideLength = new FrameworkPropertyMetadata(double.NaN);
            MinimalSideLengthProperty = DependencyProperty.Register("MinimalSideLength",
                                                typeof(double), typeof(OverlayDialog), metadatMinimalSideLength);

            FrameworkPropertyMetadata metadataWindowState = new FrameworkPropertyMetadata(OverlayDialogState.Modal);
            WindowStateProperty = DependencyProperty.Register("WindowState",
                                                            typeof(OverlayDialogState), typeof(OverlayDialog), metadataWindowState);

            FrameworkPropertyMetadata metadataIsActive = new FrameworkPropertyMetadata(true);
            IsActiveProperty = DependencyProperty.Register("IsActive",
                                                            typeof(bool), typeof(OverlayDialog), metadataIsActive);

            FrameworkPropertyMetadata metadataOverlayManagerModalMode = new FrameworkPropertyMetadata(true);
            OverlayManagerModalModeProperty = DependencyProperty.Register("OverlayManagerModalMode",
                                                            typeof(bool), typeof(OverlayDialog), metadataOverlayManagerModalMode);

        }

        public OverlayDialog()
        {
            NeedsSizeOptimization = true;
            InitializeComponent();
            //UIHelper.WalkDictionary(this.Resources);
            InitBindings();
            Loaded += new RoutedEventHandler(OverlayDialog_Loaded);
            SizeChanged += new SizeChangedEventHandler(OverlayDialog_SizeChanged);

            _scale_transform = new ScaleTransform(1, 1);
            view_box.RenderTransform = _scale_transform;

            //DependencyPropertyDescriptor descr = DependencyPropertyDescriptor.FromProperty(OverlayManagerModalModeProperty, this.GetType());
            //if (descr != null)
            //    descr.AddValueChanged(this, OnOverlayManagerModalModeChanged);

            InitMultiTouch();
        }


        public OverlayDialog(OverlayDialogState state)
        {
            InitializeComponent();
            //UIHelper.WalkDictionary(this.Resources);
            WindowState = state;

            InitBindings();
            Loaded += new RoutedEventHandler(OverlayDialog_Loaded);
            SizeChanged += new SizeChangedEventHandler(OverlayDialog_SizeChanged);
            _scale_transform = new ScaleTransform(1, 1);
            view_box.RenderTransform = _scale_transform;

            //DependencyPropertyDescriptor descr = DependencyPropertyDescriptor.FromProperty(OverlayManagerModalModeProperty, this.GetType());
            //if (descr != null)
            //    descr.AddValueChanged(this, OnOverlayManagerModalModeChanged);

            InitMultiTouch();

        }

        

        ~OverlayDialog()
        {
            //DependencyPropertyDescriptor descr = DependencyPropertyDescriptor.FromProperty(OverlayManagerModalModeProperty, this.GetType());
            //if (descr != null)
            //    descr.RemoveValueChanged(this, OnOverlayManagerModalModeChanged);
        }

        private void InitMultiTouch()
        {
            if (Logicx.WpfUtility.CustomControls.OverlayManager.OverlayManager.IsMultiTouchEnabled)
            {
                // status buttons größer machen
                state_button_stack.LayoutTransform = new ScaleTransform(1.8, 1.8);
            }
        }

        private void InitMultiTouchAfterLoad()
        {
            // Title wird vergrößert wenn Multitouch aktiv ist
            if (Logicx.WpfUtility.CustomControls.OverlayManager.OverlayManager.IsMultiTouchEnabled && title_grid.MinHeight == 0)
                title_grid.MinHeight = title_grid.ActualHeight * 1.8;
        }

        protected void InitBindings()
        {
            // HilfeButton des OverlayDialogs wird mit Hilfebutton des OverlayHelp gebunden
            Binding HelpOpenClose = new Binding("IsChecked");
            HelpOpenClose.Mode = BindingMode.TwoWay;
            HelpOpenClose.Source = help_button;
            HelpOpenClose.Path = new PropertyPath("IsChecked");
            overlay_help_and_error_viewer.help_overlay_control.button_hide.SetBinding(ToggleButton.IsCheckedProperty, HelpOpenClose);

            //DependencyPropertyDescriptor descr1 = DependencyPropertyDescriptor.FromProperty(ToggleButton.IsCheckedProperty, typeof (ToggleButton));
            //descr1.AddValueChanged(help_button, HelpCheckedChanged);
            help_button.Checked += new RoutedEventHandler(help_button_Checked);
            help_button.Unchecked += new RoutedEventHandler(help_button_Unchecked);
        }

        void help_button_Unchecked(object sender, RoutedEventArgs e)
        {
            if (help_button.IsChecked != _old_help_checked)
            {
                _old_help_checked = help_button.IsChecked.Value;
                DialogContent.DialogImageDirty = true;
            }
        }

        void help_button_Checked(object sender, RoutedEventArgs e)
        {
            if (help_button.IsChecked != _old_help_checked)
            {
                _old_help_checked = help_button.IsChecked.Value;
                DialogContent.DialogImageDirty = true;
            }
        }

        /// <summary>
        /// Diese methode wird vom OverlayManager aufgerufen wenn ein neues 
        /// OverlayContent control angezeigt werden soll <see cref="OverlayManager.Show"/>
        /// </summary>
        /// <param name="content"></param>
        public void InitializeContent(OverlayContent content)
        {
            if (content == null)
                throw new ArgumentNullException("content");

            _OverlayContentControl = content;

            SetImageControl(content.DialogIcon);

            overlaydialog_content_control.Children.Clear();
            overlaydialog_content_control.Children.Add(content);

            // Titel und Subtitel binding funktionieren durch das setzen des
            // daten context
            this.DataContext = _OverlayContentControl;

            // print button ausblenden wenn das overlay content control das drucken nicht unterstützt
            print_button.Visibility = _OverlayContentControl.CanPrint ? Visibility.Visible : Visibility.Collapsed;

            // help button ausblenden wenn das overlay content control die Hilfefunktion nicht unterstützt
            help_button.Visibility = _OverlayContentControl.CanHelp ? Visibility.Visible : Visibility.Collapsed;

            content.RegisterFocusManagementEvents();

            if(!content.HasButtons)
            {
                button_grid.Height = 0;
            }

            Binding bindingMinSidelength = new Binding("MinimalSideLength");
            bindingMinSidelength.Mode = BindingMode.TwoWay;
            bindingMinSidelength.Source = _OverlayContentControl;
            SetBinding(MinimalSideLengthProperty, bindingMinSidelength);

            //if (_OverlayContentControl.ResizeMode == OverlayDialogResizeMode.NormalResize)
            //{
            //    view_box.Stretch = Stretch.None;
            //    view_box.StretchDirection = StretchDirection.Both;
            //}
        }

        public bool AddDialogButton(OverlayDialogButton curButton)
        {
            StackPanel buttonHost = GetButtonPanelForAlignment(curButton.ButtonAlignment);

            if (!buttonHost.Children.Contains(curButton))
            {
                if (!curButton.UsesCustomStyleing)
                {
                    if (DialogContent.DialogButtonStyle != null)
                    {
                        Binding styleBinding = new Binding("DialogButtonStyle");
                        styleBinding.Source = DialogContent;
                        curButton.SetBinding(StyleProperty, styleBinding);
                        //curButton.Style = DialogContent.DialogButtonStyle;
                    }
                    else if (DialogButtonStyle != null)
                    {
                        Binding styleBinding = new Binding("DialogButtonStyle");
                        styleBinding.Source = this;
                        curButton.SetBinding(StyleProperty, styleBinding);
                        //curButton.Style = DialogButtonStyle;
                    }
                }

                buttonHost.Children.Add(curButton);
                //buttonHost.Children.Insert(buttonHost.Children.Count > 0 ? (buttonHost.Children.Count - 1) : 0, curButton);
                return true;
            }

            return false;
        }

        public void SetImageControl(UIElement elem)
        {
            if(elem == null)
            {
                image_stackpanel.Children.Clear();
                image_stackpanel.Visibility = Visibility.Collapsed;
            }
            else
            {
                image_stackpanel.Children.Clear();
                image_stackpanel.Children.Add(elem);
                image_stackpanel.Visibility = Visibility.Visible;
                if (elem is FrameworkElement)
                {
                    //((FrameworkElement)elem).VerticalAlignment = VerticalAlignment.Center;
                    //((FrameworkElement)elem).VerticalAlignment = VerticalAlignment.Top;
                    //((FrameworkElement)elem).Margin = new Thickness(0,5,0,0);
                }
            }
        }
        #endregion

        #region Property Changed
        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            if(e.Property.Name == "Visibility")
            {
                if (DialogContent != null)
                    DialogContent.DialogVisibilityChanged((Visibility)e.NewValue);
            }
            else if(e.Property == OverlayManagerModalModeProperty)
            {
                if (IsActive)
                    overlay_canvas.Background = null;
                else
                    overlay_canvas.Background = Brushes.Transparent;
            }

            base.OnPropertyChanged(e);
        }
        #endregion

        #region Dependency Properties
        /// <summary>
        /// Mit diesem Property kann die standardmässige Minimierungsanimation überschrieben werden
        /// </summary>
        public Storyboard MinimizingAnimation
        {
            get { return (Storyboard)GetValue(MinimizingAnimationProperty); }
            set { SetValue(MinimizingAnimationProperty, value); }
        }
        
        public static readonly DependencyProperty MinimizingAnimationProperty =
            DependencyProperty.Register("MinimizingAnimation", typeof(Storyboard), typeof(OverlayDialog), new UIPropertyMetadata(null));



        public OverlayWindowMeta.OverlayAnimationProperties MinimizingAnimationFinalPos
        {
            get { return (OverlayWindowMeta.OverlayAnimationProperties)GetValue(MinimizingAnimationFinalPosProperty); }
            set { SetValue(MinimizingAnimationFinalPosProperty, value); }
        }

        // Using a DependencyProperty as the backing store for MinimizingAnimationFinalPos.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MinimizingAnimationFinalPosProperty =
            DependencyProperty.Register("MinimizingAnimationFinalPos", typeof(OverlayWindowMeta.OverlayAnimationProperties), typeof(OverlayDialog), new UIPropertyMetadata(null));



        /// <summary>
        /// Mit diesem Property kann die standardmässige Maximizingganimation überschrieben werden
        /// </summary>
        public Storyboard MaximizingAnimation
        {
            get { return (Storyboard)GetValue(MaximizingAnimationProperty); }
            set { SetValue(MaximizingAnimationProperty, value); }
        }

        // Using a DependencyProperty as the backing store for MinimizingAnimation.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MaximizingAnimationProperty =
            DependencyProperty.Register("MaximizingAnimation", typeof(Storyboard), typeof(OverlayDialog), new UIPropertyMetadata(null));


        public OverlayWindowMeta.OverlayAnimationProperties MaximizingAnimationFinalPos
        {
            get { return (OverlayWindowMeta.OverlayAnimationProperties)GetValue(MaximizingAnimationFinalPosProperty); }
            set { SetValue(MaximizingAnimationFinalPosProperty, value); }
        }

        // Using a DependencyProperty as the backing store for MaximizingAnimationFinalPos.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MaximizingAnimationFinalPosProperty =
            DependencyProperty.Register("MaximizingAnimationFinalPos", typeof(OverlayWindowMeta.OverlayAnimationProperties), typeof(OverlayDialog), new UIPropertyMetadata(null));



        /// <summary>
        /// Mit diesem Property kann die standardmässige Normalizinganimation überschrieben werden
        /// </summary>
        public Storyboard NormalizingAnimation
        {
            get { return (Storyboard)GetValue(NormalizingAnimationProperty); }
            set { SetValue(NormalizingAnimationProperty, value); }
        }
        
        public OverlayWindowMeta.OverlayAnimationProperties NormalizingAnimationFinalPos
        {
            get { return (OverlayWindowMeta.OverlayAnimationProperties)GetValue(NormalizingAnimationFinalPosProperty); }
            set { SetValue(NormalizingAnimationFinalPosProperty, value); }
        }

        // Using a DependencyProperty as the backing store for NormalizingAnimationFinalPos.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty NormalizingAnimationFinalPosProperty =
            DependencyProperty.Register("NormalizingAnimationFinalPos", typeof(OverlayWindowMeta.OverlayAnimationProperties), typeof(OverlayDialog), new UIPropertyMetadata(null));



        public bool IsAnimatingActiveState { get; set; }

        // Using a DependencyProperty as the backing store for MinimizingAnimation.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty NormalizingAnimationProperty =
            DependencyProperty.Register("NormalizingAnimation", typeof(Storyboard), typeof(OverlayDialog), new UIPropertyMetadata(null));
        #endregion

        #region Imaging methods
        public RenderTargetBitmap RenderToImage(ref double out_width, ref double out_height)
        {
            System.Windows.Visibility oldvis = Visibility;
            System.Windows.Visibility oldvis1 = overlay_help_error_grid.Visibility;
            
            Visibility = Visibility.Visible;
            view_box.RenderTransform = null;

            bool bHasShadow = false;

            bHasShadow = OverlayManager.WithDialogShadow || OverlayManager.WithBackgroundShadow;

            double width_offset = bHasShadow ? Logicx.WpfUtility.CustomControls.OverlayManager.OverlayManager.OVERLAY_SHADOW_IMAGE_OFFSET_X : 0;
            double height_offset = bHasShadow ? Logicx.WpfUtility.CustomControls.OverlayManager.OverlayManager.OVERLAY_SHADOW_IMAGE_OFFSET_Y : 0;

            if(!overlay_help_and_error_viewer.ShowHelp && !overlay_help_and_error_viewer.IsFullErrorInfoVisible)
            {
                overlay_help_error_grid.Visibility = Visibility.Collapsed;
                if (width_offset < DialogContent.ImageRenderOffsetWidth)
                    width_offset = DialogContent.ImageRenderOffsetWidth;

                if (height_offset < DialogContent.ImageRenderOffsetHeight)
                    height_offset = DialogContent.ImageRenderOffsetHeight;
            }
            else
            {
                width_offset = 0;

                if (width_offset < DialogContent.ImageRenderOffsetWidth && DialogContent.ImageRenderOffsetWidth > overlay_help_and_error_viewer.ActualWidth)
                    width_offset = DialogContent.ImageRenderOffsetWidth;

                if (height_offset < DialogContent.ImageRenderOffsetHeight)
                    height_offset = DialogContent.ImageRenderOffsetHeight;
            }

            UpdateLayout();



            RenderTargetBitmap rtb = UserControlConverterFunctions.GenerateImage(this,
                width_offset,
                height_offset);

            //if (bHasShadow)
            //{
            //    if (ActualWidth != out_width)
            //        out_width = ActualWidth;
            //    if (ActualHeight != out_height)
            //        out_height = ActualHeight;

            //    out_width += width_offset;
            //    out_height += height_offset;
            //}
            //else
            //{
            //    if (ActualWidth != out_width)
            //        out_width = ActualWidth;
            //    if (ActualHeight != out_height)
            //        out_height = ActualHeight;
            //}

            if (ActualWidth != out_width)
                out_width = ActualWidth;
            if (ActualHeight != out_height)
                out_height = ActualHeight;

            out_width += width_offset;
            out_height += height_offset;

            view_box.RenderTransform = _scale_transform;
            Visibility = oldvis;
            overlay_help_error_grid.Visibility = oldvis1;

            return rtb;
        }

        public BitmapImage RenderToBitmapImage(ref double out_width, ref double out_height)
        {
            System.Windows.Visibility oldvis = Visibility;
            System.Windows.Visibility oldvis1 = overlay_help_error_grid.Visibility;

            Visibility = Visibility.Visible;
            view_box.RenderTransform = null;

            bool bHasShadow = false;

            bHasShadow = OverlayManager.WithDialogShadow || OverlayManager.WithBackgroundShadow;

            double width_offset = bHasShadow ? Logicx.WpfUtility.CustomControls.OverlayManager.OverlayManager.OVERLAY_SHADOW_IMAGE_OFFSET_X : 0;
            double height_offset = bHasShadow ? Logicx.WpfUtility.CustomControls.OverlayManager.OverlayManager.OVERLAY_SHADOW_IMAGE_OFFSET_Y : 0;

            if (!overlay_help_and_error_viewer.ShowHelp && !overlay_help_and_error_viewer.IsFullErrorInfoVisible)
            {
                overlay_help_error_grid.Visibility = Visibility.Collapsed;
                if (width_offset < DialogContent.ImageRenderOffsetWidth)
                    width_offset = DialogContent.ImageRenderOffsetWidth;

                if (height_offset < DialogContent.ImageRenderOffsetHeight)
                    height_offset = DialogContent.ImageRenderOffsetHeight;
            }
            else
            {
                width_offset = 0;

                if (width_offset < DialogContent.ImageRenderOffsetWidth && DialogContent.ImageRenderOffsetWidth > overlay_help_and_error_viewer.ActualWidth)
                    width_offset = DialogContent.ImageRenderOffsetWidth;

                if (height_offset < DialogContent.ImageRenderOffsetHeight)
                    height_offset = DialogContent.ImageRenderOffsetHeight;
            }

            UpdateLayout();

            BitmapImage rtb = UserControlConverterFunctions.GenerateBitmapImage(this);

            if (bHasShadow)
            {
                if (ActualWidth != out_width)
                    out_width = ActualWidth;
                if (ActualHeight != out_height)
                    out_height = ActualHeight;

                out_width += width_offset;
                out_height += height_offset;
            }
            else
            {
                if (ActualWidth != out_width)
                    out_width = ActualWidth;
                if (ActualHeight != out_height)
                    out_height = ActualHeight;
            }

            view_box.RenderTransform = _scale_transform;
            Visibility = oldvis;
            overlay_help_error_grid.Visibility = oldvis1;

            return rtb;
        }
        #endregion
        
        #region Dialog methods
        public void RestoreFocusedElement()
        {
            if (DialogContent != null)
                DialogContent.RestoreKeyboardFocus();
        }
        /// <summary>
        /// Diese Methode veranlasst das schliessen/verstecken dieses Dialoges.
        /// Wird vom OverlayManager aufgerufen zum verwalten der sichtbaren Dialoge.
        /// </summary>
        /// <param name="forceClose">True wenn der Dialog auf jeden Fall geschlossen werden soll</param>
        /// <returns>True wenn der Dialog geschlossen wurde.</returns>
        public bool CloseDialog(bool forceClose)
        {
            if ((_OverlayContentControl).CanClose || forceClose)
            {
                if (_OverlayContentControl.InternalBeforeClose(forceClose))
                {
                    if ((_OverlayContentControl).Close(forceClose))
                    {
                        _OverlayContentControl.InternalClearButtons();

                        //alexp: damit man die selbe instanz wieder showen kann, andernfalls kommt eine exception, ist bereits unterelement ........
                        overlaydialog_content_control.Children.Remove(_OverlayContentControl);

                        //_OverlayContentControl.AfterClose();
                        return true;
                    }
                }
            }

            return false;
        }

        public bool HelpDialog()
        {
            if((_OverlayContentControl).CanHelp)
            {
                
            }

            return false;
        }

        /// <summary>
        /// Liefert das StackPanel welches die Buttons für eine bestimmte Ausrichtung hostet
        /// </summary>
        /// <param name="alignment"></param>
        /// <returns></returns>
        internal StackPanel GetButtonPanelForAlignment(OverlayButtonAlignment alignment)
        {
            switch(alignment)
            {
                case OverlayButtonAlignment.Left:
                    return button_panel_left;
                case OverlayButtonAlignment.Center:
                    return button_panel_center;
                case OverlayButtonAlignment.Right:
                    return button_panel_right;
            }

            return null;
        }
        /// <summary>
        /// Try to set input focus to an element of the dialog
        /// </summary>
        /// <returns></returns>
        /// <remarks>Needed to wire key events</remarks>
        internal bool SetInputFocus()
        {
            return RecursiveFocus(this);
        }


        /// <summary>
        /// Liefert die Instanz des OverlayManagers für dieses content control
        /// </summary>
        /// <remarks>Die Eigenschaft kann auch einen NULL Wert liefern, wenn das Control nicht im OverlayManager angezeigt wird.</remarks>
        public IOverlayManagerBase OverlayManager
        {
            get
            {
                if (_overlay_manager != null)
                    return _overlay_manager;

                DependencyObject o = UIHelper.TryFindParentGeneric(this, typeof(OverlayManagerBase<BaseFrame, SubMenuPanel, WindowTaskBar, WindowTitleBar, OverlayManager, SubMenuPanelMenuItem>));
                if (o != null)
                    _overlay_manager = o as IOverlayManagerBase;

                return _overlay_manager;

            }
        }

        /// <summary>
        /// Sets the dialog#s help scope
        /// </summary>
        /// <param name="helpScope"></param>
        public void SetHelpScopeForDialog(string helpScope)
        {
            string sCurScope = Logicx.WpfUtility.CustomControls.HelpProvider.HelpProvider.GetHelpScope(overlaydlg_grid);
            if (!string.Equals(sCurScope, helpScope))
            {
                Logicx.WpfUtility.CustomControls.HelpProvider.HelpProvider.SetHelpScope(overlaydlg_grid, helpScope);
            }    
        }

        internal bool RecursiveFocus(DependencyObject parent)
        {
            bool bRet = false;

            foreach (object child in LogicalTreeHelper.GetChildren(parent))
            {
                if (child is UIElement && (child != _OverlayContentControl))
                {
                    if (((UIElement)child).Focus())
                        return true;
                    else
                    {
                        bRet = RecursiveFocus(((UIElement)child));
                        if (bRet)
                            return true;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Gets the actual width of the dialog which should be used to center the dialog
        /// </summary>
        /// <returns></returns>
        public double GetCenteringWidth()
        {
            if(!HelpAndErrorViewAffectCentering)
            {
                return overlaydlg_grid.ActualWidth * _scale_transform.ScaleX;
            }
            else
            {
                if (overlay_help_and_error_viewer.IsHelpOrErrorVisible)
                    return this.ActualWidth * _scale_transform.ScaleX;
                else if(CenteringMode == OverlayDialogCenteringMode.EnsureErrorViewerSpace)
                    return (overlaydlg_grid.ActualWidth * _scale_transform.ScaleX) + HelpErrorProviderMaxWidth;
                else
                    return overlaydlg_grid.ActualWidth * _scale_transform.ScaleX;
            }
        }
        /// <summary>
        /// Gets the actual height of the dialog which should be used to center the dialog
        /// </summary>
        /// <returns></returns>
        public double GetCenteringHeight()
        {
            if (!HelpAndErrorViewAffectCentering)
            {
                return overlaydlg_grid.ActualHeight * _scale_transform.ScaleY;
            }
            else
            {
                if (overlay_help_and_error_viewer.IsHelpOrErrorVisible)
                    return this.ActualHeight * _scale_transform.ScaleY;
                else
                    return overlaydlg_grid.ActualHeight * _scale_transform.ScaleY;
            }
        }

        public void ShowSaveSuccessIndicatorTimed(double milliseconds)
        {
            if (_success_timer == null)
            {
                _success_timer = new System.Timers.Timer(milliseconds + 1000);
                _success_timer.Elapsed += new System.Timers.ElapsedEventHandler(success_show_Elapsed);
            }
            else
            {
                _success_timer.Stop();
                _success_timer.Interval = milliseconds + 1000;
            }

            ShowSaveSuccessIndicator();
            _success_timer.Start();
        }

        void success_show_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            if (sender == _success_timer)
            {
                _success_timer.Stop();
                _success_timer.Elapsed -= success_show_Elapsed;
                _success_timer.Dispose();
                _success_timer = null;
            }

            Dispatcher.Invoke(DispatcherPriority.Normal, (NoParas)delegate
            {
                HideSaveSuccessIndicator();
            });


        }

        public void ShowSaveSuccessIndicator()
        {
            if (overlay_help_and_error_viewer != null)
                overlay_help_and_error_viewer.ShowSaveSuccess = true;
        }

        public void HideSaveSuccessIndicator()
        {
            if (overlay_help_and_error_viewer != null)
                overlay_help_and_error_viewer.ShowSaveSuccess = false;
        }

        public bool GetSaveSuccessIndicatorVisibility()
        {
            if (overlay_help_and_error_viewer != null)
                return overlay_help_and_error_viewer.ShowSaveSuccess;

            return false;
        }

        internal bool EventOriginatesInHelpErrorControl(object sender, RoutedEventArgs e)
        {
            if (overlay_help_and_error_viewer != null)
                return overlay_help_and_error_viewer.EventOriginatesInHelpErrorControl(sender, e);

            return false;
        }

        internal bool EventOriginatesInHelpErrorArea(object sender, RoutedEventArgs e)
        {
            bool bret = CheckErrorHelpAreaSource(sender);

            if (!bret)
                bret = CheckErrorHelpAreaSource(e.Source);

            if (!bret)
                bret = CheckErrorHelpAreaSource(e.OriginalSource);

            return bret;
        }

        private bool CheckErrorHelpAreaSource(object sender)
        {
            if (sender == overlay_help_error_grid)
                return true;

            if(UIHelper.IsVisualChild(overlay_help_error_grid, sender as DependencyObject))
                return true;

            return false;
        }

        public bool ShowFullErrorInfoOnNextValidate
        {
            get { return overlay_help_and_error_viewer.ShowFullErrorInfoInsteadOfMini; }
            set { overlay_help_and_error_viewer.ShowFullErrorInfoInsteadOfMini = value; }
        }
        public void ShowFullErrorInfo()
        {
            ShowFullErrorInfo(false);
        }

        private void ShowFullErrorInfo(bool show_if_no_errors)
        {
            if(!overlay_error_provider.IsValid)
            {
                overlay_help_and_error_viewer.ShowFullErrorInfo();
            }
        }


        #region set window state
        internal void ExplizitSetState(OverlayDialogState state)
        {
            WindowStateChangeArgs changeArgs = new WindowStateChangeArgs(WindowState, state);
            this.WindowState = state;

            if (WindowStateChanged != null)
                WindowStateChanged(this, changeArgs);

            if (DialogContent != null)
                DialogContent.WindowStateChanged(changeArgs);
        }

        public void MinimizeStateDialog()
        {
            if (this.WindowState != OverlayDialogState.Minimized) // && this.WindowState != OverlayDialogState.Modal)
            {
                WindowStateChangeingArgs changingArgs = new WindowStateChangeingArgs(WindowState, OverlayDialogState.Minimized);

                if (WindowStateChanging != null)
                    WindowStateChanging(this, changingArgs);


                if (changingArgs.CanChange && !changingArgs.DelayedChange)
                {
                    WindowStateChangeArgs changeArgs = new WindowStateChangeArgs(WindowState, OverlayDialogState.Minimized);
                    this.WindowState = OverlayDialogState.Minimized;

                    if (WindowStateChanged != null)
                        WindowStateChanged(this, changeArgs);

                    if (DialogContent != null)
                        DialogContent.WindowStateChanged(changeArgs);
                }
            }
        }

        public void MaximizeStateDialog()
        {
            if (this.WindowState != OverlayDialogState.Maximized) // && this.WindowState != OverlayDialogState.Modal)
            {
                WindowStateChangeingArgs changingArgs = new WindowStateChangeingArgs(WindowState, OverlayDialogState.Maximized);

                if (WindowStateChanging != null)
                    WindowStateChanging(this, changingArgs);


                if (changingArgs.CanChange && !changingArgs.DelayedChange)
                {
                    WindowStateChangeArgs changeArgs = new WindowStateChangeArgs(WindowState, OverlayDialogState.Maximized);
                    this.WindowState = OverlayDialogState.Maximized;

                    if (WindowStateChanged != null)
                        WindowStateChanged(this, changeArgs);

                    if (DialogContent != null)
                        DialogContent.WindowStateChanged(changeArgs);
                }
            }
        }

        public void NormalStateDialog()
        {
            if (this.WindowState != OverlayDialogState.Normal) // && this.WindowState != OverlayDialogState.Modal)
            {
                WindowStateChangeingArgs changingArgs = new WindowStateChangeingArgs(WindowState, OverlayDialogState.Normal);

                if (WindowStateChanging != null)
                    WindowStateChanging(this, changingArgs);


                if (changingArgs.CanChange && !changingArgs.DelayedChange)
                {
                    WindowStateChangeArgs changeArgs = new WindowStateChangeArgs(WindowState, OverlayDialogState.Normal);
                    this.WindowState = OverlayDialogState.Normal;

                    if (WindowStateChanged != null)
                        WindowStateChanged(this, changeArgs);

                    if (DialogContent != null)
                        DialogContent.WindowStateChanged(changeArgs);
                }
            }
        }

        public void ModalStateDialog()
        {
            if (this.WindowState != OverlayDialogState.Modal) // && this.WindowState != OverlayDialogState.Modal)
            {
                WindowStateChangeingArgs changingArgs = new WindowStateChangeingArgs(WindowState, OverlayDialogState.Modal);

                if (WindowStateChanging != null)
                    WindowStateChanging(this, changingArgs);


                if (changingArgs.CanChange && !changingArgs.DelayedChange)
                {
                    WindowStateChangeArgs changeArgs = new WindowStateChangeArgs(WindowState, OverlayDialogState.Modal);
                    this.WindowState = OverlayDialogState.Modal;

                    if (WindowStateChanged != null)
                        WindowStateChanged(this, changeArgs);

                    if (DialogContent != null)
                        DialogContent.WindowStateChanged(changeArgs);
                }
            }
        }
        #endregion

        #region set active state
        public void SetActive()
        {
            if (!this.IsActive || IsAnimatingActiveState)
            {
                ActiveStateChangeingArgs changingArgs = new ActiveStateChangeingArgs(false, true);

                if (ActiveStateChanging != null)
                    ActiveStateChanging(this, changingArgs);

                if (changingArgs.CanChange && !changingArgs.DelayedChange)
                {
                    ActiveStateChangeArgs changeArgs = new ActiveStateChangeArgs(false, true);
                    this.IsActive = true;

                    if (ActiveStateChanged != null)
                        ActiveStateChanged(this, changeArgs);

                    IsAnimatingActiveState = false;

                    if (DialogContent != null)
                        DialogContent.ActiveStateChanged(changeArgs);
                }
            }
        }

        public void SetInActive()
        {
            if (this.IsActive || IsAnimatingActiveState)
            {
                ActiveStateChangeingArgs changingArgs = new ActiveStateChangeingArgs(true, false);

                if (ActiveStateChanging != null)
                    ActiveStateChanging(this, changingArgs);


                if (changingArgs.CanChange && !changingArgs.DelayedChange)
                {
                    ActiveStateChangeArgs changeArgs = new ActiveStateChangeArgs(true, false);
                    this.IsActive = false;

                    if (ActiveStateChanged != null)
                        ActiveStateChanged(this, changeArgs);

                    IsAnimatingActiveState = false;

                    if (DialogContent != null)
                        DialogContent.ActiveStateChanged(changeArgs);
                }
            }
        }

        internal void ExplizitSetActiveState(bool active)
        {
            ActiveStateChangeArgs changeArgs = new ActiveStateChangeArgs(this.IsActive, active);
            this.IsActive = active;

            if (ActiveStateChanged != null)
                ActiveStateChanged(this, changeArgs);

            IsAnimatingActiveState = false;

            if (DialogContent != null)
                DialogContent.ActiveStateChanged(changeArgs);
        }
        #endregion

        #region set drag mode
        internal void SetDragActive()
        {
            overlay_canvas.Background = Brushes.Transparent;
        }

        internal void SetDragInactive()
        {
            if(IsActive)
                overlay_canvas.Background = null;
        }
        #endregion

        #region Resizing
        public void ResizeToHeight(double new_height)
        {
            ResizeToHeight(new_height, ActualHeight);
        }

        public void ResizeToHeight(double new_height, double compare_width)
        {
            double cWidth = compare_width;
            double cHeight = ActualHeight;

            if(_OverlayContentControl != null)
            {
                if(_OverlayContentControl.ResizeMode == OverlayContent.OverlayDialogResizeModeEnum.ScaleResize)
                {
                    double scale_transform = new_height / cHeight;
                    _scale_transform.ScaleY = scale_transform;
                    _scale_transform.ScaleX = scale_transform;                    
                }
                else
                {
                    _OverlayContentControl.DialogImageDirty = true;
                    
                    double scale_transform = new_height / cHeight;
                    //overlaydialog_content_control.Height = overlaydialog_content_control.ActualHeight * scale_transform;
                    //overlaydialog_content_control.Width = overlaydialog_content_control.ActualWidth * scale_transform;

                    double calc_height = overlaydialog_content_control.ActualHeight * scale_transform;

                    if (overlaydialog_content_control.MinHeight.IsNaN() || calc_height > overlaydialog_content_control.MinHeight)
                    {
                        overlaydialog_content_control.Height = calc_height;
                        //dialog_content_grid.Height = dialog_content_grid.ActualHeight*scale_transform;
                        //overlay_help_error_grid.Height = overlay_help_error_grid.ActualHeight*scale_transform;
                        //overlaydlg_grid.Height = overlaydlg_grid.ActualHeight * scale_transform;
                        //overlay_shadow_grid.Height = overlay_shadow_grid.ActualHeight * scale_transform;
                        //dialog_border.Height = dialog_border.ActualHeight * scale_transform;    
                        //overlay_dialog_help_error_grid.Height = overlay_dialog_help_error_grid.ActualHeight * scale_transform;
                    }
                }
            }
        }

        public void ResizeToWidth(double new_width)
        {
            ResizeToWidth(new_width, ActualWidth);
        }

        public void ResizeToWidth(double new_width, double compare_width)
        {
            double cWidth = compare_width;
            double cHeight = ActualHeight;

            if (_OverlayContentControl != null)
            {
                if (_OverlayContentControl.ResizeMode == OverlayContent.OverlayDialogResizeModeEnum.ScaleResize)
                {
                    double scale_transform = new_width / cWidth;
                    _scale_transform.ScaleY = scale_transform;
                    _scale_transform.ScaleX = scale_transform;
                }
                else
                {
                    //cWidth = ActualWidth;

                    //if (!overlay_help_error_grid.ActualWidth.IsNaN())
                    //    cWidth -= overlay_help_error_grid.ActualWidth;

                    //cWidth = ActualWidthErrorHelpVisibility;

                    _OverlayContentControl.DialogImageDirty = true;

                    double scale_transform = new_width / cWidth;
                    //overlaydialog_content_control.Height = overlaydialog_content_control.ActualHeight * scale_transform;
                    double calc_width = overlaydialog_content_control.ActualWidth*scale_transform;

                    if(overlaydialog_content_control.MinWidth.IsNaN() || calc_width > overlaydialog_content_control.MinWidth)
                        overlaydialog_content_control.Width = new_width;
                }
            }
        }

        public void ResizeToCalcWidth(double new_width)
        {
            ResizeToCalcWidth(new_width, ActualWidth);
        }
        public void ResizeToCalcWidth(double new_width, double compare_width)
        {
            double cWidth = compare_width;
            double cHeight = ActualHeight;

            if (_OverlayContentControl != null)
            {
                if (_OverlayContentControl.ResizeMode == OverlayContent.OverlayDialogResizeModeEnum.ScaleResize)
                {
                    double scale_transform = new_width / cWidth;
                    _scale_transform.ScaleY = scale_transform;
                    _scale_transform.ScaleX = scale_transform;
                }
                else
                {
                    //cWidth = ActualWidth;

                    //if (!overlay_help_error_grid.ActualWidth.IsNaN())
                    //    cWidth -= overlay_help_error_grid.ActualWidth;

                    //cWidth = ActualWidthErrorHelpVisibility;

                    _OverlayContentControl.DialogImageDirty = true;

                    double scale_transform = new_width / cWidth;
                    //overlaydialog_content_control.Height = overlaydialog_content_control.ActualHeight * scale_transform;
                    double calc_width = overlaydialog_content_control.ActualWidth * scale_transform;

                    if (overlaydialog_content_control.MinWidth.IsNaN() || calc_width > overlaydialog_content_control.MinWidth)
                        overlaydialog_content_control.Width = calc_width;
                }
            }
        }

        private void Rectangle_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Released)
                return;

            _resizing_active = true;
            Point pos_mouse = e.GetPosition(this);
            _resize_drag_offset = new Vector2f((float) pos_mouse.X, (float) pos_mouse.Y);
            OverlayManager.BeginCanvasMouseCapture();
            StartResizing();
            e.Handled = true;
            DialogContent.DialogImageDirty = true;
        }
        public void Resizing_MouseMove(object sender, MouseEventArgs e)
        {
            if (!_resizing_active)
                return;

            
            DialogContent.DialogImageDirty = true;
            Point pos_mouse = e.GetPosition(this);
            //double desired_height = pos_mouse.Y;
            Vector2f new_pos = new Vector2f((float)pos_mouse.X, (float)pos_mouse.Y);

            Vector2f new_pos_dlg = new_pos - _resize_drag_offset;

#if DEBUG
            Debug.WriteLine("Resizing Pos: X=" + new_pos_dlg.X + " Y=" + new_pos_dlg.Y);
#endif
            _resize_drag_offset = new_pos; // _resize_drag_offset + new_pos_dlg;

            if (_OverlayContentControl.ResizeMode == OverlayContent.OverlayDialogResizeModeEnum.NormalResize && new_pos_dlg.X != 0 && new_pos_dlg.Y != 0)
            {
                double desired_height = ActualViewboxHeight + new_pos_dlg.Y;
                if (desired_height > MIN_HEIGHT && (MinimalSideLength.IsNaN() || desired_height > MinimalSideLength || ActualViewboxHeight < MinimalSideLength))
                    ResizeToHeight(desired_height);

                double desired_width = ActualViewBoxWidthForResizing + new_pos_dlg.X;

                if (_OverlayContentControl.ResizeMode == OverlayContent.OverlayDialogResizeModeEnum.NormalResize)
                    desired_width = overlaydialog_content_control.Width + new_pos_dlg.X;

                if (desired_width > MIN_WIDTH && (MinimalSideLength.IsNaN() || desired_width > MinimalSideLength || ActualViewboxWidth < MinimalSideLength))
                    ResizeToWidth(desired_width);
            }
            else if (Math.Abs(new_pos_dlg.Y) > Math.Abs(new_pos_dlg.X))
            {
                double desired_height = ActualViewboxHeight + new_pos_dlg.Y;
                if (desired_height > MIN_HEIGHT && (MinimalSideLength.IsNaN() || desired_height > MinimalSideLength || ActualViewboxHeight < MinimalSideLength))
                    ResizeToHeight(desired_height);
            }
            else
            {

                double desired_width = ActualViewBoxWidthForResizing + new_pos_dlg.X;

                if (_OverlayContentControl.ResizeMode == OverlayContent.OverlayDialogResizeModeEnum.NormalResize)
                    desired_width = overlaydialog_content_control.Width + new_pos_dlg.X;

                if (desired_width > MIN_WIDTH && (MinimalSideLength.IsNaN() || desired_width > MinimalSideLength || ActualViewboxWidth < MinimalSideLength))
                {
                    ResizeToWidth(desired_width);
                }

            }
        }

        public void Resizing_MouseUp(object sender, MouseButtonEventArgs e)
        {
            StopResizing();
        }

        private void OverlayButtonCanvas_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (IsResizing)
            {
                Resizing_MouseUp(sender, e);
                e.Handled = true;
            }
        }

        private void OverlayButtonCanvas_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if(IsResizing)
            {
                Rectangle_MouseDown(sender, e);
                e.Handled = true;
            }
        }

        private void OverlayButtonCanvas_MouseMove(object sender, MouseEventArgs e)
        {
            if(IsResizing)
            {
                Resizing_MouseMove(sender, e);
                e.Handled = true;
            }
        }

        internal void StopResizing()
        {
            if (IsResizing && _OverlayContentControl != null)
                _OverlayContentControl.OnResizeFinished();

            _resizing_active = false;
            overlay_button_canvas.Background = null;
            overlay_content_canvas.Background = null;
        }

        internal void StartResizing()
        {
            _resizing_active = true;
            overlay_button_canvas.Background = Brushes.Transparent;
            overlay_content_canvas.Background = Brushes.Transparent;

        }

        /// <summary>
        /// Wenn auf false, wird Fenster automatisch an die verfügbare Größe angepasst
        /// </summary>
        public bool AutoResized
        {
            set;
            get;
        }

        /// <summary>
        /// Muss auf true gesetzt werden, wenn für den Dialog nach einer freien Position gesucht werden soll
        /// </summary>
        public bool SearchFreePosition
        {
            set; get;
        }
        
        public Thickness SearchFreePositionMargin
        {
            set; get;
        }

        public OverlayContent.SearchPositionAlignmentEnum SearchPositionAlignment
        {
            get;
            set;
        }

        /// <summary>
        /// Wenn SearchFreePosition auf true ist, deutet diese Variable hin, dass die gefundene Position bereits gesetzt wurde
        /// </summary>
        public bool FoundFreePosition
        {
            set; get;
        }
        #endregion

        #endregion

        #region Properties

        public bool NeedsSizeOptimization
        {
            get;
            set;
        }

        public OverlayDialogMaximizeMode MaximizeMode
        {
            get
            {
                return _OverlayContentControl.MaximizeMode;
            }
        }

        public OverlayDialogCenteringMode CenteringMode
        {
            get
            {
                return _OverlayContentControl.CenteringMode;
            }
        }

        public OverlayDialogInitialShowMode InitialShowMode
        {
            get
            {
                return _OverlayContentControl.InitialShowMode;
            }
        }

        public bool IsResizing
        {
            get
            {
                return _resizing_active;
            }
        }

        /// <summary>
        /// Liefert das aktuelle OverlayContent control welches in dieser Dialoginstanz angezeigt wird
        /// </summary>
        /// <remarks>Ein cast auf <see cref="Logicx.IaExpController.Interfaces.IOverlayContent">IOverlayContent</see> ist möglich.</remarks>
        public OverlayContent DialogContent
        {
            get { return _OverlayContentControl; }
        }

        /// <summary>
        /// Liefert den error provider des dialoges
        /// </summary>
        public ErrorProvider ErrorProvider
        {
            get { return overlay_error_provider; }
        }

        /// <summary>
        /// Liefert den change info provider des dialoges
        /// </summary>
        public ChangeInformationProvider ChangeInfoProvider
        {
            get { return overlay_change_info_provider; }
        }

        /// <summary>
        /// Liefert den help provider der über den gesamten dialog gestellt ist
        /// </summary>
        public HelpProvider.HelpProvider HelpProvider
        {
            get { return overlay_dialog_help_provider; }
        }

        /// <summary>
        /// Eigenschaft welche angibt ob dieser Dialog gerade aktiv ist
        /// </summary>
        public bool IsActive
        {
            get
            {
                //return _isActive;
                return (bool)GetValue(IsActiveProperty);
            }
            set
            {
                if (IsActive != value)
                {
                    if (value)
                        overlay_canvas.Background = null;
                    else
                    {
                        if (DialogContent.GoInactiveBehaviuor != OverlayContent.GoInactiveBehaviuorEnum.DoNothing &&
                            DialogContent.GoInactiveBehaviuor != OverlayContent.GoInactiveBehaviuorEnum.OpacityAnimationDialog)
                        {
                            overlay_canvas.Background = Brushes.Transparent;
                        }
                    }

                    overlay_help_and_error_viewer.IsActive = value;
                    SetValue(IsActiveProperty, value);
                }
            }
        }

        public bool OverlayManagerModalMode
        {
            get { return (bool) GetValue(OverlayManagerModalModeProperty); }
        }

        /// <summary>
        /// Gets a flag if one of the buttons are visible or not
        /// </summary>
        public bool AreButtonsVisible
        {
            get
            {
                if(_OverlayContentControl != null)
                {
                    return _OverlayContentControl.HasButtons;
                }

                return false;
            }
        }

        /// <summary>
        /// Dependency Eigenschaft welche den Fensterstatus steuert
        /// </summary>
        public OverlayDialogState WindowState
        {
            get
            {
                return (OverlayDialogState)GetValue(WindowStateProperty);
            }
            set
            {
                SetValue(WindowStateProperty, value);

                minimizebutton.Visibility = value == OverlayDialogState.Modal ? Visibility.Collapsed : Visibility.Visible;
                if(value == OverlayDialogState.Modal)
                {
                    maximizebutton.Visibility = Visibility.Collapsed;
                    normalizebutton.Visibility = Visibility.Collapsed;
                }
                else if(value != OverlayDialogState.Maximized)
                {
                    maximizebutton.Visibility = Visibility.Visible;
                    normalizebutton.Visibility = Visibility.Collapsed;
                }
                else
                {
                    maximizebutton.Visibility = Visibility.Collapsed;
                    normalizebutton.Visibility = Visibility.Visible;
                }
            }
        }

        /// <summary>
        /// Dependency Eigenschaft welche den style des Close-Buttons setzt
        /// </summary>
        public Style CloseButtonStyle
        {
            get
            {
                return (Style)GetValue(CloseButtonStyleProperty);
            }
            set
            {
                SetValue(CloseButtonStyleProperty, value);
            }
        }



        public Brush ResizeGripBrush
        {
            get { return (Brush)GetValue(ResizeGripBrushProperty); }
            set { SetValue(ResizeGripBrushProperty, value); }
        }
        public static readonly DependencyProperty ResizeGripBrushProperty = DependencyProperty.Register("ResizeGripBrush", typeof(Brush), typeof(OverlayDialog), new UIPropertyMetadata(Brushes.White));



        /// <summary>
        /// Dependency Eigenschaft welche den style des Help-Buttons setzt
        /// </summary>
        public Style HelpButtonStyle
        {
            get
            {
                return (Style) GetValue(HelpButtonStyleProperty);
            }
            set
            {
                SetValue(HelpButtonStyleProperty, value);
            }
        }

        /// <summary>
        /// Dependency Eigenschaft welche den style des Print-Buttons setzt
        /// </summary>
        public Style PrintButtonStyle
        {
            get
            {
                return (Style)GetValue(PrintButtonStyleProperty);
            }
            set
            {
                SetValue(PrintButtonStyleProperty, value);
            }
        }

        /// <summary>
        /// Dependency Eigenschaft welche den style des Minimize-Buttons setzt
        /// </summary>
        public Style MinimizeButtonStyle
        {
            get
            {
                return (Style)GetValue(MinimizeButtonStyleProperty);
            }
            set
            {
                SetValue(MinimizeButtonStyleProperty, value);
            }
        }

        /// <summary>
        /// Dependency Eigenschaft welche den style des Maximize-Buttons setzt
        /// </summary>
        public Style MaximizeButtonStyle
        {
            get
            {
                return (Style)GetValue(MaximizeButtonStyleProperty);
            }
            set
            {
                SetValue(MaximizeButtonStyleProperty, value);
            }
        }

        /// <summary>
        /// Dependency Eigenschaft welche den style des Maximize-Buttons setzt
        /// </summary>
        public Style NormalizeButtonStyle
        {
            get
            {
                return (Style)GetValue(NormalizeButtonStyleProperty);
            }
            set
            {
                SetValue(NormalizeButtonStyleProperty, value);
            }
        }

        /// <summary>
        /// Dependency Eigenschaft welche den style des Dialog-Borders setzt
        /// </summary>
        public Style DialogBorderStyle
        {
            get
            {
                return (Style)GetValue(DialogBorderStyleProperty);
            }
            set
            {
                SetValue(DialogBorderStyleProperty, value);
            }
        }

        /// <summary>
        /// Dependency Eigenschaft welche den style des Dialog-Borders setzt
        /// </summary>
        public Style DialogContentBorderStyle
        {
            get
            {
                return (Style)GetValue(DialogContentBorderStyleProperty);
            }
            set
            {
                SetValue(DialogContentBorderStyleProperty, value);
            }
        }

        /// <summary>
        /// Dependency Eigenschaft welche den style der Dialog-Buttons setzt
        /// </summary>
        public Style DialogButtonStyle
        {
            get
            {
                return (Style)GetValue(DialogButtonStyleProperty);
            }
            set
            {
                SetValue(DialogButtonStyleProperty, value);
            }
        }

        /// <summary>
        /// Dependency Eigenschaft welche den style des Maximize-Buttons setzt
        /// </summary>
        public Style StatusSmallButtonStyle
        {
            get
            {
                return (Style)GetValue(StatusSmallButtonStyleProperty);
            }
            set
            {
                SetValue(StatusSmallButtonStyleProperty, value);
            }
        }

        /// <summary>
        /// Dependency Eigenschaft welche den style des Maximize-Buttons setzt
        /// </summary>
        public Style StatusNormalButtonStyle
        {
            get
            {
                return (Style)GetValue(StatusNormalButtonStyleProperty);
            }
            set
            {
                SetValue(StatusNormalButtonStyleProperty, value);
            }
        }

        /// <summary>
        /// Dependency Eigenschaft welche den Hintergrundbrush für den OverlayCanvas setzt
        /// </summary>
        public SolidColorBrush OverlayBackgroundBrush
        {
            get
            {
                return (SolidColorBrush)GetValue(OverlayBackgroundBrushProperty);
            }
            set
            {
                SetValue(OverlayBackgroundBrushProperty, value);
            }
        }

        public double MinimalSideLength
        {
            get
            {
                return (double)GetValue(MinimalSideLengthProperty);
            }
            set
            {
                SetValue(MinimalSideLengthProperty, value);
            }
        }

        /// <summary>
        /// wenn auf true dann wird der dialog automatisch zentral positioniert
        /// </summary>
        public bool AutoCenteredDialog
        {
            get
            {
                if (_OverlayContentControl != null && _OverlayContentControl.AutoCenteredDialog)
                    return true;
                return false;
            }
        }

        /// <summary>
        /// wenn auf true dann beeinträchtigt die hilfe und die fehlerdarstellung das center verhalten
        /// </summary>
        public bool HelpAndErrorViewAffectCentering
        {
            get
            {
                if (_OverlayContentControl != null && _OverlayContentControl.HelpAndErrorViewAffectCentering)
                    return true;
                return false;
            }
        }

        public double ActualVisibleDialogWidth
        {
            get
            {
                if (ActualWidth.IsNaN())
                    return 0;

                if (!overlay_help_and_error_viewer.ShowHelp && !overlay_help_and_error_viewer.IsFullErrorInfoVisible)
                {
                    return (ActualWidth - overlay_help_error_grid.ActualWidth);
                }

                return ActualWidth - overlay_help_error_grid.ActualWidth;
            }
        }

        public double ActualVisibleDialogHeight
        {
            get
            {
                if (ActualHeight.IsNaN())
                    return 0;
                
                return ActualHeight;
            }
        }

        public double ActualWidthErrorHelpVisibility
        {
            get
            {
                if (ActualWidth.IsNaN())
                    return 0;

                if (!overlay_help_and_error_viewer.ShowHelp && !overlay_help_and_error_viewer.IsFullErrorInfoVisible)
                {
                    return (ActualWidth - overlay_help_error_grid.ActualWidth) + DialogContent.ImageRenderOffsetWidth;
                }

                //return ActualWidth + DialogContent.ImageRenderOffsetWidth;
                return ActualWidth;
            }
        }

        public double ActualViewboxWidth
        {
            get
            {
                if (ActualWidth.IsNaN())
                    return 0;                
                
                if (!overlay_help_and_error_viewer.ShowHelp && !overlay_help_and_error_viewer.IsFullErrorInfoVisible)
                {
                    return (ActualWidth - overlay_help_error_grid.ActualWidth) * _scale_transform.ScaleX;
                }
                
                return ActualWidth * _scale_transform.ScaleX;
            }
        }

        public double ActualViewBoxWidthForResizing
        {
            get
            {
                if (ActualWidth.IsNaN())
                    return 0;

                return ActualWidth * _scale_transform.ScaleX;
            }
        }

        public double ActualViewboxHeight
        {
            get
            {
                if (ActualHeight.IsNaN())
                    return 0;

                //if (overlay_help_and_error_viewer.ShowHelp || overlay_help_and_error_viewer.IsFullErrorInfoVisible)
                //{
                //    return (ActualHeight - overlay_help_error_grid.ActualHeight) * _scale_transform.ScaleX;
                //}

                return ActualHeight * _scale_transform.ScaleY;
            }
        }


        public double ActualScaleX
        {
            get
            {
                if (_scale_transform == null)
                    return 1;

                return _scale_transform.ScaleX;
            }
        }

        public double ActualScaleY
        {
            get
            {
                if (_scale_transform == null)
                    return 1;

                return _scale_transform.ScaleY;
            }
        }

        public bool HelpOrErrorProviderVisible
        {
            get
            {
                return (overlay_help_and_error_viewer.ShowHelp || overlay_help_and_error_viewer.IsFullErrorInfoVisible);
            }
        }

        public double HelpErrorProviderWidth
        {
            get { return overlay_help_error_grid.ActualWidth; }
        }

        public double HelpErrorProviderMaxWidth
        {
            get { return overlay_help_and_error_viewer.ErrorViewerMaxWidth; }
        }

        public TextBlock DialogTitleTextBlock
        {
            get
            {
                return overlaydialog_bold_header_text;
            }
        }

        internal OverlayWindowMeta DialogMeta { get; set; }
        #endregion

        #region Event Handler

        private void OverlayCanvas_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (OverlayManagerModalMode)
            {
                if (!IsActive)
                    e.Handled = true; // "schlucken"
            }
            else
            {
                // von canvas auf dialog "umleiten"
                e.Handled = true;
                MouseButtonEventArgs newArgs = new MouseButtonEventArgs(e.MouseDevice, e.Timestamp, e.ChangedButton, e.StylusDevice);
                newArgs.RoutedEvent = UIElement.MouseDownEvent;
                RaiseEvent(newArgs);
            }
            //e.Handled = true;    
        }

        private void OnOverlayManagerModalModeChanged(object sender, EventArgs e)
        {
            if (IsActive)
                overlay_canvas.Background = null;
            else
                overlay_canvas.Background = Brushes.Transparent;
        }


        /// <summary>
        /// Wird aufgerufen sobald das Dialogcontrol geladen wurde
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OverlayDialog_Loaded(object sender, RoutedEventArgs e)
        {
            InitMultiTouchAfterLoad();

            if(_OverlayContentControl == null)
                return;

            if (ErrorProvider != null)
            {
                // listen to the error provider collection changed event
                // to update error conting
                ErrorProvider.ProviderErrorList.CollectionChanged += new System.Collections.Specialized.NotifyCollectionChangedEventHandler(ProviderErrorList_CollectionChanged);
                
            }

            overlay_help_and_error_viewer.ErrorVisibilityChanged += new EventHandler(overlay_help_and_error_viewer_ErrorVisibilityChanged);


            _OverlayContentControl.InternalInitButtons(this);

            if ((_OverlayContentControl).Show(true))
            {
                // try to set input focus in content control
                if (!_OverlayContentControl.SetInputFocus())
                {
                    SetInputFocus();
                }

                if (_OverlayContentControl != null)
                    _OverlayContentControl.InternalAfterShow();

                if (_OverlayContentControl != null)
                    _OverlayContentControl.UpdateImageRenderBehaviourRegistrations();
            }

        }

        void OverlayDialog_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if(DialogContent != null)
            {
                DialogContent.DialogImageDirty = true;
            }
        }

        /// <summary>
        /// Called if the parent error provider's error collection changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void ProviderErrorList_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (ErrorProvider != null)
            {
                if(ErrorProvider.ProviderErrorList.Count != _old_error_count)
                {
                    _old_error_count = ErrorProvider.ProviderErrorList.Count;
                    DialogContent.DialogImageDirty = true;
                }
            }
        }

        #region button event handler
        /// <summary>
        /// Der Print button wurde betätigt
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Print_Click(object sender, RoutedEventArgs e)
        {
            if ((_OverlayContentControl).CanPrint)
            {
                if(_OverlayContentControl.BeforePrint())
                {
                    if ((_OverlayContentControl).Print())
                        _OverlayContentControl.AfterPrint();
                }
            }
        }

        private void Minimize_Click(object sender, RoutedEventArgs e)
        {
            MinimizeStateDialog();
        }

        private void Maximize_Click(object sender, RoutedEventArgs e)
        {
            if (WindowState != OverlayDialogState.Maximized) 
                MaximizeStateDialog();
            else if(WindowState == OverlayDialogState.Maximized)
                NormalStateDialog();
        }

        private void Normalize_Click(object sender, RoutedEventArgs e)
        {
            NormalStateDialog();
        }

        /// <summary>
        /// Der Help button wurde betätigt
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Help_Click(object sender, RoutedEventArgs e)
        {
            if (HelpDialog())
            {
                if (HelpRequest != null)
                    HelpRequest(this, new EventArgs());
            }
        }

        private void HelpCheckedChanged(object sender, EventArgs e)
        {
            if(help_button.IsChecked != _old_help_checked)
            {
                _old_help_checked = help_button.IsChecked.Value;
                DialogContent.DialogImageDirty = true;
            }
        }

        void overlay_help_and_error_viewer_ErrorVisibilityChanged(object sender, EventArgs e)
        {
            DialogContent.DialogImageDirty = true;
        }

        /// <summary>
        /// Der Close button wurde betätigt
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Close_Click(object sender, RoutedEventArgs e)
        {
            if (CloseDialog(false))
            {
                if (Closed != null)
                    Closed(this, new EventArgs());
            }
        }
        #endregion

        private void overlay_change_info_provider_LoadChangeData(object sender, EventArgs e)
        {
            if(_OverlayContentControl != null)
                _OverlayContentControl.OnLoadChanges(sender as ChangeInformationProvider);
        }


        #endregion

        #region Events
        /// <summary>
        /// Event wenn der aktuelle dialog geschlossen wurde (wird im OverlayManager verwendet)
        /// </summary>
        public event EventHandler Closed;
        /// <summary>
        /// Event wenn eine Hilfe vom Benutzer angefordert wurde
        /// </summary>
        public event EventHandler HelpRequest;

        public event EventHandler<WindowStateChangeArgs> WindowStateChanged;
        public event EventHandler<WindowStateChangeingArgs> WindowStateChanging;

        public event EventHandler<ActiveStateChangeArgs> ActiveStateChanged;
        public event EventHandler<ActiveStateChangeingArgs> ActiveStateChanging;

        //public event EventHandler DialogActive;
        //public event EventHandler DialogInactive;
        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            BindingOperations.ClearAllBindings(overlay_help_and_error_viewer.help_overlay_control.button_hide);

            //DependencyPropertyDescriptor descr1 = DependencyPropertyDescriptor.FromProperty(ToggleButton.IsCheckedProperty, typeof(ToggleButton));
            //if (descr1 != null)
            //    descr1.RemoveValueChanged(help_button, HelpCheckedChanged);

            if(ErrorProvider != null)
                ErrorProvider.ProviderErrorList.CollectionChanged -= ProviderErrorList_CollectionChanged;
            overlay_help_and_error_viewer.ErrorVisibilityChanged -= overlay_help_and_error_viewer_ErrorVisibilityChanged;

            this.Loaded -= OverlayDialog_Loaded;
            this.SizeChanged -= OverlayDialog_SizeChanged;

            help_button.Checked -= help_button_Checked;
            help_button.Unchecked -= help_button_Unchecked;

            BindingOperations.ClearAllBindings(overlay_help_and_error_viewer);
            BindingOperations.ClearAllBindings(this);

            foreach (UIElement child in button_panel_left.Children)
            {
                BindingOperations.ClearAllBindings(child);
            }
            foreach (UIElement child in button_panel_center.Children)
            {
                BindingOperations.ClearAllBindings(child);
            }
            foreach (UIElement child in button_panel_right.Children)
            {
                BindingOperations.ClearAllBindings(child);
            }

            overlaydialog_content_control.Children.Clear();
            overlay_dialog_help_provider.Dispose();

            //overlay_error_provider = null;
            BindingOperations.ClearAllBindings(overlay_help_error_grid);
            BindingOperations.ClearAllBindings(help_button);

            overlay_help_and_error_viewer.Dispose();
            overlay_help_and_error_viewer = null;

            if(ErrorProvider != null)
                ErrorProvider.Dispose();
            if(ChangeInfoProvider != null)
                ChangeInfoProvider.Dispose();


            _OverlayContentControl.Dispose();
            _OverlayContentControl = null;
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if (!this._disposed)
            {
                //DependencyPropertyDescriptor descr = DependencyPropertyDescriptor.FromProperty(OverlayManagerModalModeProperty, this.GetType());
                //if (descr != null)
                //    descr.RemoveValueChanged(this, OnOverlayManagerModalModeChanged);

                BindingOperations.ClearAllBindings(this);
                UnregisterEvents(Closed);
                UnregisterEvents(HelpRequest);

                UnregisterEvents(WindowStateChanged);
                UnregisterEvents(WindowStateChanging);

                UnregisterEvents(ActiveStateChanged);
                UnregisterEvents(ActiveStateChanging);

                _disposed = true;

            }
        }

        #region Unregister Events
        private void UnregisterEvents(EventHandler event_handler)
        {
            if (event_handler == null)
                return;

            foreach (EventHandler eventDelegate in event_handler.GetInvocationList())
                event_handler -= eventDelegate;

        }

        private void UnregisterEvents(EventHandler<WindowStateChangeArgs> event_handler)
        {
            if (event_handler == null)
                return;

            foreach (EventHandler<WindowStateChangeArgs> eventDelegate in event_handler.GetInvocationList())
                event_handler -= eventDelegate;

        }

        private void UnregisterEvents(EventHandler<ActiveStateChangeArgs> event_handler)
        {
            if (event_handler == null)
                return;

            foreach (EventHandler<ActiveStateChangeArgs> eventDelegate in event_handler.GetInvocationList())
                event_handler -= eventDelegate;

        }

        private void UnregisterEvents(EventHandler<ActiveStateChangeingArgs> event_handler)
        {
            if (event_handler == null)
                return;

            foreach (EventHandler<ActiveStateChangeingArgs> eventDelegate in event_handler.GetInvocationList())
                event_handler -= eventDelegate;

        }

        private void UnregisterEvents(EventHandler<WindowStateChangeingArgs> event_handler)
        {
            if (event_handler == null)
                return;

            foreach (EventHandler<WindowStateChangeingArgs> eventDelegate in event_handler.GetInvocationList())
                event_handler -= eventDelegate;

        }
        #endregion

        private bool _disposed;
        #endregion

        #region Attribs

        private IOverlayManagerBase _overlay_manager;
        public Point StartupPosition;
        private const double MIN_HEIGHT = 100;
        private const double MIN_WIDTH = 100;
        private ScaleTransform _scale_transform;
        private bool _resizing_active;
        private Vector2f _resize_drag_offset = new Vector2f();

        protected delegate void NoParas();
        private OverlayContent _OverlayContentControl = null;
        //private bool _isActive = true;
        private int _old_error_count = 0;
        private bool _old_help_checked = false;

        private System.Timers.Timer _success_timer = null;

        #region Dependency properties
        public static readonly DependencyProperty CloseButtonStyleProperty;
        public static readonly DependencyProperty PrintButtonStyleProperty;
        public static readonly DependencyProperty HelpButtonStyleProperty;
        
        public static readonly DependencyProperty MinimizeButtonStyleProperty;
        public static readonly DependencyProperty MaximizeButtonStyleProperty;
        public static readonly DependencyProperty NormalizeButtonStyleProperty;
        public static readonly DependencyProperty DialogButtonStyleProperty;

        public static readonly DependencyProperty DialogBorderStyleProperty;
        public static readonly DependencyProperty DialogContentBorderStyleProperty;
        

        public static readonly DependencyProperty StatusSmallButtonStyleProperty;
        public static readonly DependencyProperty StatusNormalButtonStyleProperty;



        public static readonly DependencyProperty WindowStateProperty;
        public static readonly DependencyProperty IsActiveProperty;
        public static readonly DependencyProperty OverlayManagerModalModeProperty;

        public static readonly DependencyProperty MinimalSideLengthProperty;

        public static readonly DependencyProperty OverlayBackgroundBrushProperty;       
        #endregion


        #endregion

    }
}
