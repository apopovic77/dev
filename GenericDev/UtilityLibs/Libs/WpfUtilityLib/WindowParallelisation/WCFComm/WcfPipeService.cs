﻿using System;
using System.Diagnostics;
using System.ServiceModel;
using System.Threading;
using System.Xml;

namespace Logicx.WpfUtility.WindowParallelisation.WCFComm
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public abstract class WcfPipeService : IDisposable
    {
        public WcfPipeService()
        {
        }

        #region Service Management
        // If the service host can be started,
        // true will be returned. If false is returned
        // another process owns the pipe.
        public bool ServiceOn(string uri, string pipe_name)
        {
            _uri = uri;
            _pipe_name = pipe_name;

            if (_operational)
                return false;
            _host_creation_reset_event = new AutoResetEvent(false);
            StartWcfDispatcher();
            _host_creation_reset_event.WaitOne();
            _host_creation_reset_event.Close();
            _host_creation_reset_event = null;
            return _operational;
        }

        public void ServiceOff()
        {
            try
            {
                if (_host != null)
                {
                    StopDispatcher();
                    //if (_host.State != CommunicationState.Closed)
                    //    _host.Close();
                }
            }
            catch
            {
            }

            _operational = false;
        }

        protected abstract Type GetPipeServiceInterfaceType();

        // The thread which will listen for communications
        // from the other side of the pipe.
        private void HostThisService(object arg)
        {
            try
            {
                NetNamedPipeBinding pipe_binding = new NetNamedPipeBinding();
                pipe_binding.MaxReceivedMessageSize = 67108864;
                pipe_binding.MaxBufferPoolSize = 67108864;
                pipe_binding.MaxBufferSize = 67108864;
                pipe_binding.ReceiveTimeout = pipe_binding.SendTimeout = new TimeSpan(7, 0, 0, 0);
                pipe_binding.ReaderQuotas = new XmlDictionaryReaderQuotas() { MaxArrayLength = 67108864, MaxBytesPerRead = 67108864, MaxStringContentLength = 67108864 };

                _host = new ServiceHost(this, new Uri(_uri));
                _host.AddServiceEndpoint(GetPipeServiceInterfaceType(), pipe_binding, _pipe_name);
                _host.Open();
                _operational = true;
                _host_creation_reset_event.Set();
                System.Windows.Threading.Dispatcher.Run();
            }
            catch (Exception ex)
            {
                error = ex;
                _operational = false;
            }
        }


        public void StopDispatcher()
        {
            try
            {
                if (_wcf_service_thread == null)
                    return;

                _wcf_service_thread.Abort();
                _wcf_service_thread = null;
                //if (_wcf_service_thread != null)
                //    Dispatcher.FromThread(_wcf_service_thread).InvokeShutdown();
            }
            catch
            {
            }
        }

        public virtual void Dispose()
        {
            ServiceOff();
        }

        public void StartWcfDispatcher()
        {
            _wcf_service_thread = new Thread(new ParameterizedThreadStart(HostThisService));
            _wcf_service_thread.Name = "Wcf Service Thread";
            _wcf_service_thread.SetApartmentState(ApartmentState.STA);
            _wcf_service_thread.IsBackground = true;
            _wcf_service_thread.Start();
        }

        #endregion

        #region Service Networking Attribs
        private Thread _wcf_service_thread;
        private AutoResetEvent _host_creation_reset_event;
        private string _uri;
        private string _pipe_name;
        private bool _operational = false;
        private ServiceHost _host = null;
        private Exception error = null;
        #endregion
    }
}
