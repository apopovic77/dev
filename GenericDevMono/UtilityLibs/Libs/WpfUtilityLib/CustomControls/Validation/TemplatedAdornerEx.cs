﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;
using Logicx.WpfUtility.WpfHelpers;

namespace Logicx.WpfUtility.CustomControls.Validation
{
    /// <summary>
    /// Templated element adorner class
    /// </summary>
    public class TemplatedAdornerEx : Adorner
    {
        #region Construction and initialization
        /// <summary>
        /// Constructor of the class
        /// </summary>
        /// <param name="adornedElement">The adorned element (usually the element which causes validation)</param>
        /// <param name="adornerTemplate">The control template which should be used</param>
        public TemplatedAdornerEx(UIElement adornedElement, ControlTemplate adornerTemplate)
            : base(adornedElement)
        {
            Control control = new Control();
            control.DataContext = ErrorProvider.GetErrors(adornedElement);
            control.Template = adornerTemplate;
            this._child = control;
            base.AddVisualChild(this._child);
        }

        #endregion

        #region Operations
        /// <summary>
        /// Overrides the element arrangement
        /// </summary>
        /// <param name="size"></param>
        /// <returns></returns>
        protected override Size ArrangeOverride(Size size)
        {
            Size size2 = base.ArrangeOverride(size);
            if (this._child != null)
            {
                this._child.Arrange(new Rect(new Point(), size2));
            }
            return size2;
        }
        /// <summary>
        /// Clears all visual children
        /// </summary>
        public void ClearChild()
        {
            base.RemoveVisualChild(this._child);
            this._child = null;
        }
        /// <summary>
        /// Get the desired transform
        /// </summary>
        /// <param name="transform"></param>
        /// <returns></returns>
        public override GeneralTransform GetDesiredTransform(GeneralTransform transform)
        {
            if (this.ReferenceElement == null)
            {
                return transform;
            }
            GeneralTransformGroup group = new GeneralTransformGroup();
            group.Children.Add(transform);
            GeneralTransform transform2 = base.TransformToDescendant(this.ReferenceElement);
            if (transform2 != null)
            {
                group.Children.Add(transform2);
            }
            return group;
        }
        /// <summary>
        /// Gets the visual child at a given index
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        protected override Visual GetVisualChild(int index)
        {
            if ((this._child == null) || (index != 0))
            {
                throw new ArgumentOutOfRangeException("index", index, "Index out of range");
            }
            return this._child;
        }

        /// <summary>
        /// Measure override
        /// </summary>
        /// <param name="constraint"></param>
        /// <returns></returns>
        protected override Size MeasureOverride(Size constraint)
        {
            if (((this.ReferenceElement != null) && (base.AdornedElement != null)) && (base.AdornedElement.IsMeasureValid && !AreClose(this.ReferenceElement.DesiredSize, base.AdornedElement.DesiredSize)))
            {
                this.ReferenceElement.InvalidateMeasure();
            }
            this._child.Measure(constraint);
            return this._child.DesiredSize;
        }

        /// <summary>
        /// Check if two sizes are close to each other
        /// </summary>
        /// <param name="size1"></param>
        /// <param name="size2"></param>
        /// <returns></returns>
        private static bool AreClose(Size size1, Size size2)
        {
            return (AreClose(size1.Width, size2.Width) && AreClose(size1.Height, size2.Height));
        }
        /// <summary>
        /// Check if two values are close to each other
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <returns></returns>
        private static bool AreClose(double value1, double value2)
        {
            if (value1 == value2)
            {
                return true;
            }
            double num = ((Math.Abs(value1) + Math.Abs(value2)) + 10.0) * 2.2204460492503131E-16;
            double num2 = value1 - value2;
            return ((-num < num2) && (num > num2));
        }

        #endregion

        #region Properties
        /// <summary>
        /// Gets/sets the reference element (usually an AdornedElementPlaceholderEx element)
        /// </summary>
        public FrameworkElement ReferenceElement
        {
            get
            {
                return this._referenceElement;
            }
            set
            {
                this._referenceElement = value;
            }
        }
        //Gets/sets the visual children count
        protected override int VisualChildrenCount
        {
            get
            {
                if (this._child == null)
                {
                    return 0;
                }
                return 1;
            }
        }

        #endregion


        #region Atribs
        private Control _child;
        private FrameworkElement _referenceElement;

        #endregion
    }
}
