using System;

namespace Logicx.OracleSpatial
{
  /// <summary>
  /// Summary description for Point3D.
  /// </summary>
  [Serializable]
  public struct SdoPoint3D {

    public float X;
    public float Y;
    public float Z;

    public static SdoPoint3D Empty = new SdoPoint3D(0, 0, 0);

    public SdoPoint3D(float x, float y, float z)
    {

      this.X = x;
      this.Y = y;
      this.Z = z;
    }

    public override string ToString() {

      return "Point (X: " + X.ToString() + ", Y: " + Y.ToString() + ", Z: " + Z.ToString() + ")";
    }
  }
}
