﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;
using System.Windows.Media.Animation;

namespace Logicx.WpfUtility
{
    public class DesktopToggleButton : DesktopButton
    {
        public DesktopToggleButton(string button_name, Style button_style, Canvas button_canvas)
            : base(button_name, button_style, button_canvas)
        {
        }

        protected override void CreateButton(string button_name, Style button_style)
        {
            _button = new ToggleButton();
            _button.Name = button_name;
            _button.Style = button_style;
        }

        public ToggleButton ToggleButton
        {
            get { return (ToggleButton)_button; }
        }

    }
    public class DesktopNormalButton : DesktopButton
    {
        public DesktopNormalButton(string button_name, Style button_style, Canvas button_canvas)
            : base(button_name, button_style, button_canvas)
        {
        }

        protected override void CreateButton(string button_name, Style button_style)
        {
            _button = new Button();
            _button.Name = button_name;
            _button.Style = button_style;
        }
        
        public Button NormalButton
        {
            get { return (Button)_button; }
        }

    }

    public abstract class DesktopButton
    {
        public DesktopButton(string button_name, Style button_style, Canvas button_canvas)
        {
            CreateButton(button_name, button_style);
            if(button_canvas != null)
                button_canvas.Children.Add(_button);
        }

        protected abstract void CreateButton(string button_name, Style button_style);

        public void DefaultOpenAnim(Vector pos_start, Vector pos_end)
        {
            StartButtonAnim(
                pos_start, //pos_start
                pos_end, //pos_end
                1, //scale_start
                1, //scale_end
                0, //opacity_start
                1, //opacity_end
                100, //time_anim_start
                100, //time_anim_start_varianz
                1200, //time_anim_end
                100, //time_anim_end_varianz
                null); //completed delegate  
        }


        public void DefaultCloseAnim(Vector pos_start, Vector pos_end)
        {
            StartButtonAnim(
                pos_start, //pos_start
                pos_end, //pos_end
                1, //scale_start
                1, //scale_end
                1, //opacity_start
                0, //opacity_end
                100, //time_anim_start
                100, //time_anim_start_varianz
                1200, //time_anim_end
                100, //time_anim_end_varianz
                HideAnimationFinishedDelegate); //completed delegate  
        }

        public void DefaultCloseAnim(Vector pos_end)
        {
            StartButtonAnim(
                pos_end, //pos_end
                1, //scale_start
                1, //scale_end
                1, //opacity_start
                0, //opacity_end
                100, //time_anim_start
                100, //time_anim_start_varianz
                1200, //time_anim_end
                100, //time_anim_end_varianz
                HideAnimationFinishedDelegate); //completed delegate  
        }

        public void HideAnimationFinishedDelegate()
        {
            _button.Visibility = System.Windows.Visibility.Collapsed;
        }

        public void StartButtonAnim(Vector pos_end, float scale_start, float scale_end, float opacity_start, float opacity_end, int time_anim_start, int time_start_varianz, int time_anim_end, int time_end_varianz, AnimationFinishedDelegate animation_finished_delegate)
        {
            StartButtonAnim(null, pos_end, scale_start, scale_end, opacity_start, opacity_end, time_anim_start, time_start_varianz, time_anim_end, time_end_varianz, animation_finished_delegate);
        }

        public void StartButtonAnim(Vector? pos_start, Vector pos_end, float scale_start, float scale_end, float opacity_start, float opacity_end, int time_anim_start, int time_start_varianz, int time_anim_end, int time_end_varianz, AnimationFinishedDelegate animation_finished_delegate)
        {
            _button.Visibility = System.Windows.Visibility.Visible;
            _button.Opacity = opacity_start;
            if (pos_start.HasValue)
            {
                Canvas.SetLeft(_button, pos_start.Value.X);
                Canvas.SetTop(_button, pos_start.Value.Y);
            }
            ScaleTransform scale_transform = _button.RenderTransform as ScaleTransform;
            if (scale_transform == null)
            {
                scale_transform = new ScaleTransform();
                _button.RenderTransform = scale_transform;
            }
            scale_transform.ScaleX = scale_transform.ScaleY = scale_start;

            DoubleAnimation da;
            Storyboard sb = new Storyboard();
            sb.Duration = new Duration(new TimeSpan(0, 0, 0, 0, time_anim_end + time_end_varianz + time_start_varianz + time_anim_start));

            //start anim left pos
            float? pos_start_x = (pos_start.HasValue) ? (float)pos_start.Value.X : (float?)null;
            da = GetDoubleAnimation(pos_start_x, (float)pos_end.X, GetRandom(time_anim_start, time_start_varianz), GetRandom(time_anim_end, time_end_varianz));
            sb.Children.Add(da);
            Storyboard.SetTarget(da, _button);
            Storyboard.SetTargetProperty(da, new PropertyPath("(Canvas.Left)"));

            //start anim top pos
            float? pos_start_y = (pos_start.HasValue) ? (float)pos_start.Value.Y : (float?)null;
            da = GetDoubleAnimation(pos_start_y, (float)pos_end.Y, GetRandom(time_anim_start, time_start_varianz), GetRandom(time_anim_end, time_end_varianz));
            sb.Children.Add(da);
            Storyboard.SetTarget(da, _button);
            Storyboard.SetTargetProperty(da, new PropertyPath("(Canvas.Top)"));

            //start opacity 
            da = GetDoubleAnimation(opacity_start, opacity_end, time_anim_start, time_anim_end);
            sb.Children.Add(da);
            Storyboard.SetTarget(da, _button);
            Storyboard.SetTargetProperty(da, new PropertyPath("Opacity"));

            //scalex
            da = GetDoubleAnimation(scale_start, scale_end, time_anim_start, time_anim_end);
            sb.Children.Add(da);
            Storyboard.SetTarget(da, scale_transform);
            Storyboard.SetTargetProperty(da, new PropertyPath("ScaleX"));

            //scaley
            da = GetDoubleAnimation(scale_start, scale_end, time_anim_start, time_anim_end);
            sb.Children.Add(da);
            Storyboard.SetTarget(da, scale_transform);
            Storyboard.SetTargetProperty(da, new PropertyPath("ScaleY"));


            if (animation_finished_delegate != null)
            {
                sb.Completed += new EventHandler(animation_Completed);
                //merk dir die da wo du mit dem handler reg bist damit das completed kommt
                _animation_finished_delegate = animation_finished_delegate;
            }

            sb.Begin();
        }

        private int GetRandom(int time_anim_start, int varianz)
        {
            return time_anim_start + _random.Next(-varianz, varianz);
        }

        private DoubleAnimation GetDoubleAnimation(float to, int time_start, int time_duration)
        {
            return GetDoubleAnimation(to, time_start, time_duration);
        }

        private DoubleAnimation GetDoubleAnimation(float? from, float to, int time_start, int time_duration)
        {
            DoubleAnimation db = new DoubleAnimation();
            if(from.HasValue)
                db.From = from.Value;
            db.To = to;
            db.FillBehavior = FillBehavior.HoldEnd;
            db.DecelerationRatio = 0.8;
            db.AccelerationRatio = 0.2;
            db.BeginTime = new TimeSpan(0, 0, 0, 0, time_start);
            db.Duration = new Duration(new TimeSpan(0, 0, 0, 0, time_duration));
            return db;
        }

        void animation_Completed(object sender, EventArgs e)
        {

            _animation_finished_delegate();
        }

        public ButtonBase Button
        {
            get { return _button; }
        }


        protected ButtonBase _button;
        private static Random _random = new Random();
        private AnimationFinishedDelegate _animation_finished_delegate;
        public delegate void AnimationFinishedDelegate();
    }
}
