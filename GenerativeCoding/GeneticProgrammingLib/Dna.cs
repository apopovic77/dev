﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;

namespace GeneticProgrammingLib
{


    public abstract class Dna<T>
    {
        public Dna()
        {
        }

        public Dna(List<T> genes)
        {
            _genes = genes;
        }

        public abstract void SimulationUpdate(int frame);
        public abstract void CalcFitness();
        public abstract void InitializeRandomGen();


        public virtual void Dispose()
        {
            _disposed = true;
        }
        public bool IsDisposed
        {
            get { return _disposed; }
        }

        public List<T> Genes
        {
            get { return _genes; }
        }

        public bool IsBest
        {
            set
            {
                _isbest = value;
            }
            get { return _isbest; }
        }

        public Dna<T> CreateChildClone()
        {
            //this is father
            //para is mother

            //1. find midpoint
            //2. crossover
            //3. mutate


            Dna<T> child = (Dna<T>)Activator.CreateInstance(this.GetType());
            child._genes = _genes.ToList();
            return child;
        }

        public Dna<T> CreateChild(Dna<T> mother, double mutation_rate)
        {
            //this is father
            //para is mother

            //1. find midpoint
            //2. crossover
            //3. mutate

            Dna<T> child = this.Clone();

            for (int i = 0; i < _genes.Count; i++)
            { 
                if( ((float)_r.NextDouble()) < mutation_rate)
                    child.Mutate(i);
                else if((float)_r.NextDouble() >= 0.5f)
                    child._genes[i] = mother._genes[i];
            }

            return child;
        }

        public virtual Dna<T> Clone()
        {
            Dna<T> child = (Dna<T>)Activator.CreateInstance(this.GetType());
            child._genes = _genes.ToList();
            return child;
        }
        public Dna<T> CreateChildAfterMidpoint(int midpoint, double mutation_rate)
        {
            //this is father
            //para is mother

            //1. find midpoint
            //2. crossover
            //3. mutate



            Dna<T> child = (Dna<T>)Activator.CreateInstance(this.GetType());
            child._genes = _genes.ToList();

            for (int i = 0; i < _genes.Count; i++)
            {
                if (i >= midpoint)
                    if (((float)_r.NextDouble()) < mutation_rate)
                        child.Mutate(i);


            }

            return child;
        }


        public Dna<T> CreateChildMidpoint(Dna<T> mother, double mutation_rate)
        {
            //this is father
            //para is mother

            //1. find midpoint
            //2. crossover
            //3. mutate


            int midpoint = _r.Next(1, _genes.Count);

            Dna<T> child = (Dna<T>)Activator.CreateInstance(this.GetType());
            child._genes = _genes.ToList();

            for (int i = 0; i < _genes.Count; i++)
            {
                if (i >= midpoint)
                    child._genes[i] = mother._genes[i];

                if (((float)_r.NextDouble()) < mutation_rate)
                    child.Mutate(i);
            }

            return child;
        }

        public abstract void Mutate(int i);

        protected bool _disposed;

        public double Fitness;
        public double FitnessProbality;
        protected List<T> _genes;
        protected static Random _r = new Random();

        public static int DnaGeneSize;

        public string Id = "";


        /// <summary>
        /// wenn das teil gegen eine wand gefahren ist dann wird das flag auf true gesetzt
        /// </summary>
        public bool IsDead = false;
        protected int _died_at_frame = 0;
        protected int _active_frame = 0;
        private bool _isbest;

    }
}
