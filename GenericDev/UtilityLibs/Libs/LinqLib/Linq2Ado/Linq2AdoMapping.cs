﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Reflection;
using System.Text;
using LinqLib.Reflection;

namespace LinqLib.Linq2Ado
{
    public class Linq2AdoMapping<T>
    {
        #region Construction and Initialisation
        /// <summary>
        /// Internal constructor of the class
        /// </summary>
        /// <remarks>Use <see cref="Linq2AdoMapper">Linq2AdoMapper</see> to generate instances.</remarks>
        internal Linq2AdoMapping()
        {
            _linq_data_type = typeof(T);
            AnalyseType();
        }
        #endregion

        #region Operations
        /// <summary>
        /// Refreshes the internal mappings
        /// </summary>
        public void RefreshMapping()
        {
            AnalyseType();
        }

        /// <summary>
        /// Gets the data type of a column name within the mapping
        /// </summary>
        /// <param name="column_name">Name of the column</param>
        /// <returns>Data type of the column or NULL if the column does not exist in the current mapping</returns>
        public Type GetColumnDataType(string column_name)
        {
            if (!_column_property_mapping.ContainsKey(column_name))
                return null;

            return _column_property_mapping[column_name].PropertyType;
        }

        /// <summary>
        /// Generates a data table instance and fills it with the data from the list
        /// </summary>
        /// <param name="data_objects">List of data objects to generate a data table</param>
        /// <returns>A data table instance with rows for each data object</returns>
        public DataTable GenerateTableFromList(List<T> data_objects)
        {
            return GenerateTableFromList(data_objects, true);
        }

        /// <summary>
        /// Generates a data table instance and fills it with the data from the list
        /// </summary>
        /// <param name="data_objects">List of data objects to generate a data table</param>
        /// <param name="include_dbgenerated_columns">Flag if db generated columns should be included</param>
        /// <returns>A data table instance with rows for each data object</returns>
        public DataTable GenerateTableFromList(List<T> data_objects, bool include_dbgenerated_columns)
        {
            // get a data table with the schema
            DataTable dtRet = _ado_data_table.Clone();

            if(!include_dbgenerated_columns)
            {
                List<DataColumn> delete_columns = new List<DataColumn>();

                // get rid of db generated columns
                foreach(DataColumn checkCol in dtRet.Columns)
                {
                    string column_name = checkCol.ColumnName;
                    if(_column_mapping.ContainsKey(column_name))
                    {
                        if (_column_mapping[column_name].IsDbGenerated)
                            delete_columns.Add(checkCol);
                    }
                }

                foreach (DataColumn deleteCol in delete_columns)
                    dtRet.Columns.Remove(deleteCol);
            }

            foreach(T data_object in data_objects)
            {
                // create new row
                DataRow new_row = dtRet.NewRow();
                
                foreach(DataColumn column in dtRet.Columns)
                {
                    if (LinqReflectionHelper.IsGenericNullableType(_column_property_mapping[column.ColumnName]))
                    {
                        object value = _column_property_mapping[column.ColumnName].GetValue(data_object, null);

                        if (value == null)
                            new_row[column] = DBNull.Value;
                        else
                            new_row[column] = value;
                    }
                    else
                    {
                        // copy all column data
                        new_row[column] = _column_property_mapping[column.ColumnName].GetValue(data_object, null);
                    }
                }

                dtRet.Rows.Add(new_row);
                new_row.AcceptChanges();
            }

            dtRet.AcceptChanges();

            return dtRet;
        }

        /// <summary>
        /// Analysis a mapping type and generates a data table schema
        /// </summary>
        private void AnalyseType()
        {
            if (!LinqReflectionHelper.IsLinqTable(_linq_data_type))
                throw new ApplicationException("The data type '" + _linq_data_type.ToString() + "' is not a LINQ2SQL table.");

            _ado_data_table = new DataTable();
            _column_mapping.Clear();
            _column_property_mapping.Clear();

            foreach (PropertyInfo pI in _linq_data_type.GetProperties())
            {
                bool bIsColumn = false;
                ColumnAttribute columnData = null;
                string column_name = null;

                foreach (
                    object attribute in pI.GetCustomAttributes(typeof(ColumnAttribute), false))
                {
                    ColumnAttribute association = attribute as ColumnAttribute;
                    columnData = association;
                    if (association != null)
                    {
                        bIsColumn = true;

                        if (string.IsNullOrEmpty(association.Name))
                            column_name = pI.Name;
                        else
                            column_name = association.Name;
                    }

                }

                if (bIsColumn && columnData != null)
                {
                    _column_mapping[column_name] = columnData;
                    _column_property_mapping[column_name] = pI;

                    DataColumn newCol = new DataColumn(column_name);
                    newCol.AllowDBNull = columnData.CanBeNull;
                    newCol.AutoIncrement = columnData.IsDbGenerated;
                    newCol.ColumnMapping = MappingType.Element;

                    if (LinqReflectionHelper.IsGenericNullableType(pI))
                    {
                        newCol.DataType = LinqReflectionHelper.GetInnerTypeOfGenericType(pI.PropertyType);
                        newCol.AllowDBNull = true;
                    }
                    else
                    {
                        newCol.DataType = pI.PropertyType;
                    }

                    _ado_data_table.Columns.Add(newCol);
                }
            }

            if (_ado_data_table.Columns.Count <= 0)
                throw new ApplicationException("Mapping has no ColumnAttributes defined!");
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets the mapping data type
        /// </summary>
        public Type LinqDataType
        {
            get { return _linq_data_type; }
        }
        #endregion

        #region Attributes
        private Type _linq_data_type = null;
        private DataTable _ado_data_table = null;
        private Dictionary<string, ColumnAttribute> _column_mapping = new Dictionary<string, ColumnAttribute>();
        private Dictionary<string, PropertyInfo> _column_property_mapping = new Dictionary<string, PropertyInfo>();
        #endregion
    }
}
