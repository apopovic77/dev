﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using CognitiveLib.Scanner;
using CognitiveLib.WebSearch;
using FSBlog.GoogleSearch.GoogleClient;
using Logicx.Utility;
using Utilities;

namespace CognitiveLib.Detector
{
    public class ModelCodeDetectionResult
    {
        public WeightedString ModelCode
        {
            get
            {
                if (ModelCodeCandidates != null && ModelCodeCandidates.Count > 0)
                    return ModelCodeCandidates.MaxBy(o => o.Weight);
                return null;
            }
        }
        public List<WeightedString> ModelCodeCandidates;
    }
    public class DeviceModelCodeDetector : DeviceCodeDetector
    {
        public static ModelCodeDetectionResult Detect(string text)
        {
            if (string.IsNullOrEmpty(text))
                throw new Exception("cannot detect, no text given");

            // die ordnung der liste spielt eine rolle
            List<WeightedString> look_for = new List<WeightedString>(5);
            look_for.Add(new WeightedString("Model Code"));
            look_for.Add(new WeightedString("ModelCode"));

            ModelCodeDetectionResult sres = new ModelCodeDetectionResult();
            sres.ModelCodeCandidates = GetDeviceCodeCandidates(text, look_for, DeviceCodeDetector.GetBlackList(new List<string> { "Code", "No" }));
            return sres;
        }
    }
}
