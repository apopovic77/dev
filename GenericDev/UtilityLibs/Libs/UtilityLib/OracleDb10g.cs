//using System;
//using System.Collections.Generic;
//using System.Text;
//using Logicx.Utilities.Database;
//using System.Collections;
//using Oracle.DataAccess.Client;
//using System.Data;

//namespace Logicx.Utilities.Database
//{
//    public class OracleDb10g : DBConnection
//    {
        


//        protected OracleConnection m_connection;
//        protected OracleCommand     m_cmd;
//        protected OracleDataReader  m_rs;
//        protected OracleTransaction m_transaction;
//        protected OracleDataAdapter m_dataadapter;

//        protected override void SetUrl()
//        {
//            m_dburl = String.Format("User Id={0};Password={1};Data Source={3}:{4}/{2}",m_username, m_password, m_db, m_host, m_port);
//        }
//        public string GetUrl()
//        {
//            return m_dburl;
//        }

//        public OracleConnection Connection
//        {
//            get
//            {
//                return m_connection;
//            }
//        }
//        public override void BeginTransaction()
//        {
//            ClearResultSet(false);
//            SetAutoCommit(false);
//            Connect();

//            if (m_transaction != null)
//                return;

//            //clear cmd if available
//            if (m_cmd != null)
//            {
//                try { m_cmd.Dispose(); }
//                catch { }
//                m_cmd = null;
//            }
//            m_cmd = m_connection.CreateCommand();
//            m_transaction = m_connection.BeginTransaction();

//            m_cmd.Connection = m_connection;
//            //m_cmd.Transaction = m_transaction;
//        }

//        public override void Connect() {
//            try
//            {
//                Lock();
//                if (m_connection != null)
//                {
//                    try
//                    {
//                        if (m_connection.State == System.Data.ConnectionState.Open)
//                        {
//                            ClearResultSet(false);
//                            return;
//                        }
//                        Disconnect();
//                    }
//                    catch { }
//                }

//                SetUrl();
//                m_connection = new OracleConnection();
//                m_connection.ConnectionString = GetUrl();
//                m_connection.Open();

//                m_cmd = m_connection.CreateCommand();
//                if (!m_defaultAutoCommit)
//                {
//                    m_transaction = m_connection.BeginTransaction();

//                    m_cmd.Connection = m_connection;
//                    //m_cmd.Transaction = m_transaction;
//                }
//            }
//            finally {
//                Unlock();
//            }
//        }
//        public override void Execute(string dmlstmt)
//        {
//            try
//            {
//                Lock();
//                Connect();

//                ClearResultSet(false);
//                m_cmd.CommandText = dmlstmt;
//                m_cmd.ExecuteNonQuery();
//                ClearResultSet(false);
//            }
//            catch (Exception ex)
//            {
//                AddErrorMessage(ex.StackTrace);
//                throw ex;
//            }
//            finally
//            {
//                Unlock();
//            }
//        }
//        public override void Execute(string dmlstmt, bool withLock)
//        {
//            try
//            {
//                if (withLock)
//                    Lock();
//                Connect();

//                ClearResultSet(false);
//                m_cmd.CommandText = dmlstmt;
//                m_cmd.ExecuteNonQuery();
//                ClearResultSet(false);

//            }
//            catch (Exception ex)
//            {
//                AddErrorMessage(ex.StackTrace);
//                throw ex;
//            }
//            finally
//            {
//                if (withLock)
//                    Unlock();
//            }
//        }

//        public override void ExecuteCommit(string ddlstmt)
//        {

//            //Console.Out.WriteLine(ddlstmt);
//            try
//            {
//                Lock();
//                Connect();
//                ClearResultSet(false);
//                m_cmd.CommandText = ddlstmt;
//                m_cmd.ExecuteNonQuery();
//                ClearResultSet(false);
//                Commit();
//            }
//            catch (Exception ex)
//            {
//                AddErrorMessage(ex.StackTrace);
//                throw ex;
//            }
//            finally
//            {
//                Unlock();
//            }
//        }

//        /// <summary>
//        /// --> YOU HAVE TO LOCK THIS MANUALLY <--
//        /// </summary>
//        /// <param name="query">sql stmt</param>
//        public override void ExecuteQuery(string query)
//        {
//            Connect();
//            ClearResultSet(false);
//            m_cmd.CommandText = query;
//            m_rs = m_cmd.ExecuteReader();
//            m_columnCount = m_rs.FieldCount;
//            m_columnLabels = new string[m_columnCount];
//            for (int i = 0; i < m_columnCount; i++)
//                m_columnLabels[i] = m_rs.GetName(i).ToLower();
//        }

//        public override object ExecuteQueryScalar(string query, bool withLock)
//        {
//            try
//            {
//                if (withLock)
//                    Lock();
//                Connect();
//                ClearResultSet(false);
//                m_cmd.CommandText = query;
//                object scalar = m_cmd.ExecuteScalar();
//                ClearResultSet(false);
//                return scalar;
//            }
//            catch (Exception ex)
//            {
//                AddErrorMessage(ex.StackTrace);
//                throw ex;
//            }
//            finally
//            {
//                if (withLock)
//                    Unlock();
//            }
//        }

//        public override object ExecuteQueryScalar(string query)
//        {
//            try
//            {
//                Lock();
//                Connect();
//                ClearResultSet(false);
//                m_cmd.CommandText = query;
//                object scalar = m_cmd.ExecuteScalar();
//                ClearResultSet(false);
//                return scalar;
//            }
//            catch (Exception ex)
//            {
//                AddErrorMessage(ex.StackTrace);
//                throw ex;
//            }
//            finally
//            {
//                Unlock();
//            }
//        }

//        /// <summary>
//        /// use this for query returning only one line
//        /// locking is used here, so you do not have to lock this
//        /// if you expect more than one line, use GetLine
//        /// </summary>
//        /// <param name="query">sql query</param>
//        /// <returns>object array holding values of queryied line</returns>
//        public override object[] ExecuteQueryLine(string query)
//        {
//            //Console.Out.WriteLine(query);
//            try
//            {
//                Lock();
//                Connect();
//                ClearResultSet(false);
//                m_cmd.CommandText = query;
//                m_rs = m_cmd.ExecuteReader();
//                if (!m_rs.HasRows)
//                    throw new DBException("query did not return any results");
//                m_rs.Read();
//                object[] values = new object[m_rs.FieldCount];
//                for (int i = 0; i < m_rs.FieldCount; i++)
//                    values[i] = m_rs.GetValue(i);
//                ClearResultSet(false);
//                return values;
//            }
//            catch (Exception ex)
//            {
//                AddErrorMessage(ex.StackTrace);
//                throw ex;
//            }
//            finally
//            {
//                Unlock();
//            }
//        }

//        /// <summary>
//        /// use this for query returning only one line
//        /// locking is used here, so you do not have to lock this
//        /// if you expect more than one line, use GetLine
//        /// </summary>
//        /// <param name="query">sql query</param>
//        /// <returns>a hashtable holding values of queryied line, key is column name</returns>
//        public override Hashtable ExecuteQueryLineHash(string query)
//        {
//            //Console.Out.WriteLine(query);
//            try
//            {
//                Lock();
//                ClearResultSet(false);
//                m_cmd.CommandText = query;
//                m_rs = m_cmd.ExecuteReader();
//                if (!m_rs.HasRows)
//                    throw new DBException("query did not return any results");
//                m_rs.Read();
//                Hashtable values = new Hashtable();
//                for (int i = 0; i < m_rs.FieldCount; i++)
//                    values.Add(m_rs.GetName(i), m_rs.GetValue(i));
//                ClearResultSet(false);
//                return values;
//            }
//            catch (Exception ex)
//            {
//                AddErrorMessage(ex.StackTrace);
//                throw ex;
//            }
//            finally
//            {
//                Unlock();
//            }
//        }

//        public override bool HasMoreLines()
//        {
//            if (m_rs == null)
//                return false;
//            return m_rs.Read();
//        }

//        public override object[] GetLine()
//        {
//            if (m_columnCount == 0)
//                throw new DBException("ExecuteQuery muss vorher aufgerufen werden!");
//            object[] line = new object[m_columnCount];
//            for (int i = 0; i < m_columnCount; i++)
//                line[i] = m_rs.GetValue(i);
//            return line;
//        }

//        public override Hashtable GetLineHash()
//        {

//            if (m_columnCount == 0)
//                throw new DBException("ExecuteQuery muss vorher aufgerufen werden!");

//            Hashtable values = new Hashtable();
//            for (int i = 0; i < m_rs.FieldCount; i++)
//                values.Add(m_rs.GetName(i), m_rs.GetValue(i));
//            return values;
//        }
//        public Hashtable GetLineHash(OracleDataReader rs)
//        {
//            Hashtable values = new Hashtable();
//            for (int i = 0; i < rs.FieldCount; i++)
//                values.Add(rs.GetName(i), rs.GetValue(i));
//            return values;
//        }

//        /// <summary>
//        /// Method GetLineHash
//        /// </summary>
//        /// <param name="row">A  DataRow</param>
//        /// <param name="ds">A  DataSet</param>
//        /// <returns>A  Hashtable</retutns>
//        public override Hashtable GetLineHash(DataRow row, DataSet ds)
//        {
//            Hashtable values = new Hashtable();
//            for (int i = 0; i < ds.Tables[0].Columns.Count; i++)
//            {
//                values.Add(ds.Tables[0].Columns[i].ColumnName, row[i]);
//            }
//            return values;
//        }
//        /// <summary>
//        /// Method GetLineHash
//        /// </summary>
//        /// <param name="row">A  DataRow</param>
//        /// <param name="ds">A  DataSet</param>
//        /// <returns>A  Hashtable</retutns>
//        public override object[] GetLine(DataRow row, DataSet ds)
//        {
//            object[] values = new object[ds.Tables[0].Columns.Count];
//            for (int i = 0; i < ds.Tables[0].Columns.Count; i++)
//            {
//                values[i] = row[i];
//            }
//            return values;
//        }
//        protected override void Disconnect()
//        {
//            try
//            {
//                ClearResultSet(false);
//                if (m_transaction != null)
//                {
//                    m_transaction.Rollback();
//                    m_transaction = null;
//                }
//                if (m_rs != null)
//                {
//                    m_rs.Close();
//                    m_rs = null;
//                }
//                if (m_cmd != null)
//                {
//                    m_cmd.Dispose();
//                    m_cmd = null;
//                }
//                if (m_connection != null)
//                {
//                    m_connection.Dispose();
//                    m_connection = null;
//                }
//            }
//            catch (Exception e)
//            {
//                //XXX Debugging stuff removen
//                Console.Error.WriteLine(e);
//            }
//        }

//        public override void Commit()
//        {
//            if (m_transaction != null)
//            {
//                m_transaction.Commit();
//                SetAutoCommit(true);
//                m_transaction = null;
//            }

//        }
//        public override void Rollback()
//        {
//            if (m_transaction != null)
//            {
//                m_transaction.Rollback();
//                SetAutoCommit(true);
//                m_transaction = null;
//            }
//        }

//        protected override void SetAutoCommit(bool value)
//        {
//            m_defaultAutoCommit = value;

//            //if(!m_defaultAutoCommit){
//            //    if(m_transaction==null){
//            //        //try{ m_transaction = m_connection.BeginTransaction(); }catch(Exception e){}
//            //    }
//            //}else{
//            //    if(m_transaction!=null){
//            //        //try{ m_transaction.Commit(); }catch(Exception e){}
//            //    }
//            //}
//        }

//        public override Object GetDataReader()
//        {
//            return m_rs;
//        }

//        public override void ClearResultSet()
//        {
//            ClearResultSet(true);
//        }

//        public void ClearResultSet(bool explicitCall)
//        {
//            try
//            {
//                for (int i = 0; i < m_columnCount; i++)
//                    m_columnLabels[i] = null;
//                m_columnCount = 0;
//                m_columnLabels = null;
//                m_rs.Close();
//                m_rs = null;
//            }
//            catch { }
//            if (explicitCall)
//                Connect();
//        }
//    }
//}
