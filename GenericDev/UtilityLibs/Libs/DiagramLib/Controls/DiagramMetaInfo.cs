﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using Logicx.DiagramLib.Controls.MenuItemControls;

namespace Logicx.DiagramLib.Controls
{
    public class DiagramMetaInfo
    {

        #region Constants
        #endregion

        #region Construction and Initialization

        public DiagramMetaInfo(UserControl diagram, string title, bool is_hidden)
        {
            _diagram = diagram;
            _is_hidden = is_hidden;
            Title = title;
        }

        public DiagramMetaInfo(UserControl diagram, string title, SubMenuPanelMenuDiagramItem item, bool is_hidden)
            :this(diagram, title,is_hidden)
        {
            //_diagram = diagram;
            _sub_menu_panel_menu_diagram_item = item;
            //_is_hidden = is_hidden;
        }

        #endregion

        #region Operations
        #endregion

        #region Event Handlers
        #endregion

        #region Properties

        public UserControl Diagram
        {
            get { return _diagram; }
        }

        public SubMenuPanelMenuDiagramItem SubMenuPanelMenuDiagramItem
        {
            get { return _sub_menu_panel_menu_diagram_item; }
            set
            {
                _sub_menu_panel_menu_diagram_item = value;
            }
        }

        public bool IsHidden
        {
            get { return _is_hidden; }
            set { _is_hidden = value; }
        }

        public string Title
        {
            get;
            private set;
        }

        #endregion

        #region Dependency Properties
        #endregion

        #region Events
        #endregion

        #region Routed Events
        #endregion

        #region Attributes

        private UserControl _diagram;
        private SubMenuPanelMenuDiagramItem _sub_menu_panel_menu_diagram_item;
        private bool _is_hidden;

        #endregion

        #region Tests

        #endregion
    }
}
