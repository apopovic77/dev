﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Logicx.WpfUtility.WindowManagement
{
    public class PanelGridButton : Button
    {
        public static readonly RoutedEvent InternalKeyUpEvent = EventManager.RegisterRoutedEvent("InternalKeyUp",
                                                                                          RoutingStrategy.Bubble,
                                                                                          typeof(KeyEventHandler),
                                                                                          typeof(PanelGridButton));

        public static readonly RoutedEvent InternalKeyDownEvent = EventManager.RegisterRoutedEvent("InternalKeyDown",
                                                                              RoutingStrategy.Bubble,
                                                                              typeof(KeyEventHandler),
                                                                              typeof(PanelGridButton));

        public PanelGridButton()
        {
            KeyDown += new System.Windows.Input.KeyEventHandler(PanelGridButton_KeyDown);
            KeyUp += new System.Windows.Input.KeyEventHandler(PanelGridButton_KeyUp);
        }

        void PanelGridButton_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            System.Windows.Input.KeyEventArgs newArgs = new KeyEventArgs(e.KeyboardDevice, e.InputSource, e.Timestamp, e.Key);
            newArgs.RoutedEvent = InternalKeyUpEvent;
            newArgs.Source = this;
            RaiseEvent(newArgs);

            e.Handled = newArgs.Handled;
        }

        void PanelGridButton_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Down || e.Key == Key.Up)
            {
                e.Handled = true;
            }

            System.Windows.Input.KeyEventArgs newArgs = new KeyEventArgs(e.KeyboardDevice, e.InputSource, e.Timestamp, e.Key);
            newArgs.RoutedEvent = InternalKeyDownEvent;
            newArgs.Source = this;
            RaiseEvent(newArgs);

            if(!e.Handled)
                e.Handled = newArgs.Handled;
        }

        public event RoutedEventHandler InternalKeyUp
        {
            add
            {
                this.AddHandler(InternalKeyUpEvent, value);
            }
            remove
            {
                this.RemoveHandler(InternalKeyUpEvent, value);
            }
        }

        public event RoutedEventHandler InternalKeyDown
        {
            add
            {
                this.AddHandler(InternalKeyDownEvent, value);
            }
            remove
            {
                this.RemoveHandler(InternalKeyDownEvent, value);
            }
        }

    }
}
