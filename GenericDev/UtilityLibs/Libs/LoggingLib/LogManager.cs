﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.IO;
using System.Threading;
using System.Xml.Serialization;
using System.Transactions;
using LoggingLib.Trace;
using Logicx.Utilities;

using LinqLib.Serialization;

using LoggingLib.Configuration;
using LoggingLib.Configuration.Activators;
using LoggingLib.Data;
using LoggingLib.Data.Finalizers;
using LoggingLib.Data.Serialization;
using LoggingLib.Filter;
using LoggingLib.Interfaces;
using LoggingLib.SystemInterop;

namespace LoggingLib
{
    /// <summary>
    /// The log manager class
    /// </summary>
    public class LogManager
    {
        #region Construction and initialization
        /// <summary>
        /// Default constructor
        /// </summary>
        /// <remarks>Set the mapping database and call Initialize() manually before performing any logging actions.</remarks>
        public LogManager()
        {
            if (_current == null)
                _current = this;

        }
        /// <summary>
        /// constructor setting the database and connection string
        /// </summary>
        /// <param name="connectionString">Connection string</param>
        public LogManager(string connectionString) : this()
        {
            _connectionString = connectionString;
            Initialize();
        }

        public void Initialize(string preferred_connection_string)
        {
            if (_initialized)
                return;

            _connectionString = preferred_connection_string;
            Initialize();
        }

        /// <summary>
        /// Initialize the current log manager instance
        /// </summary>
        public void Initialize()
        {
            if (_initialized)
                return;

            // set the connection string if not initialized
            if (string.IsNullOrEmpty(_connectionString))
                _connectionString = Settings.GetConnectionString();

            
            // filename for serialization
            _serializationFile = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), Path.Combine(Settings.ProductName, Settings.SerializationFileName));
            
            if (!Directory.Exists(Path.GetDirectoryName(_serializationFile)))
                Directory.CreateDirectory(Path.GetDirectoryName(_serializationFile));

#if DEBUG
            //string file_name = Path.GetDirectoryName(_serializationFile) + "\\debug_output.txt";
            //TextWriterTraceListener tr2 = new TextWriterTraceListener(System.IO.File.CreateText(file_name));
            //Debug.Listeners.Add(tr2);
#endif

            // initialize the log filter based on the settings
            _log_filter = new LogFilter(Settings);
            // initialize the software scan filter
            _software_scan_filter = new SoftwareScanFilter(Settings);
            // initialize the data finalizer instances
            _finalizer_activator = new DataFinalizerActivator(Settings);
            // initialize the log formatters
            _formatter_activator = new LogFormatterActivator(Settings);

            // mark log as submitted for the timed submission
            _last_submission = DateTime.Now;
            _time_submission_trigger_minutes = Settings.TimedSubmissionTriggerMinutes;
            
            // event log initialization
            //if(!EventLog.SourceExists(Settings.ProductName))
            //    EventLog.CreateEventSource(Settings.ProductName, "Application");

            //try
            //{
            //    EventLog.CreateEventSource(Settings.ProductName, "Application");
            //}
            //catch
            //{
                
            //}

            _event_log = new EventLog();
            _event_log.Source = Settings.ProductName;

            // deserialize the log buffer is exists
            if (File.Exists(LogBufferSerializationFilePath))
            {
                FileInfo fi = new FileInfo(LogBufferSerializationFilePath);
                if (fi.Length > 0)
                    DeserializeLogBuffer();

                if (_log_buffer.Count() > 0)
                    _work_todo = true;
            }

            // initialize the logging data context
            _data_context = new LoggingDataContext(_connectionString);

            _data_context.LoadOptions = CreateDataLoadOptions();

            // connect to the data context
            ConnectDataContextAsync();

            // create the background worker thread for the logger
            _bw_logger = new QueuedBackgroundWorker();
            _bw_logger.Name = "LogManager: LogWorker Thread";
            _bw_logger.DoWork += new QueuedBackgroundWorker.DoWorkEventHandler(bw_logger_DoWork);
            _bw_logger.RunWorkerCompleted += new QueuedBackgroundWorker.RunWorkerCompletedEventHandler(bw_logger_RunWorkerCompleted);
            _bw_logger.RunWorkerAsync();

            // build category list on DC
            BuildCategoryList();

            _initialized = true;
        }

        /// <summary>
        /// Get data load options for the logging context
        /// </summary>
        /// <returns>Returns the data load options for the logging context</returns>
        private DataLoadOptions CreateDataLoadOptions()
        {
            //set data load options
            DataLoadOptions dlo = new DataLoadOptions();

            dlo.LoadWith<LogEntry>(le => le.CategoryLogEntries);
            dlo.LoadWith<Category>(c => c.CategoryLogEntries);

            return dlo;
        }

        /// <summary>
        /// Builds category objects
        /// </summary>
        private void BuildCategoryList()
        {
            string productName = ProductName;

            // create standard system categories
            Category catSysInfo = new Category();
            catSysInfo.CategoryName = "System Info";
            catSysInfo.ProductName = productName;
            _data_context.Categories.InsertOnSubmit(catSysInfo);

            Category catAppSession = new Category();
            catAppSession.CategoryName = "Application Session";
            catAppSession.ProductName = productName;
            _data_context.Categories.InsertOnSubmit(catAppSession);

            Category catAppMessage = new Category();
            catAppMessage.CategoryName = "Application Message";
            catAppMessage.ProductName = productName;
            _data_context.Categories.InsertOnSubmit(catAppMessage);

            Category catUserAction = new Category();
            catUserAction.CategoryName = "User Action";
            catUserAction.ProductName = productName;
            _data_context.Categories.InsertOnSubmit(catUserAction);


            bool bRetry = true;

            for (int i = 0; i < Settings.Categories.Count; i++)
            {
                string categoryName = Settings.Categories[i].Name;

                if (categoryName != "System Info" &&
                    categoryName != "Application Session" &&
                    categoryName != "Application Message" &&
                    categoryName != "User Action")
                {
                    if (bRetry)
                    {
                        try
                        {
                            Category search = _data_context.Categories.Single(c => c.CategoryName == categoryName && c.ProductName == productName);
                        }
                        catch (Exception ex)
                        {
                            // nach dem ersten timeout fehler nicht weiter versuchen die kategorien aus der datenbank zu laden
                            if (ex is SqlException)
                                bRetry = false;

                            // InvalidOperationException wenn keine Elemente gefunden
                            // SqlException wenn keine Verbindung zur Datenbank

                            Category newCat = new Category();
                            newCat.CategoryName = categoryName;
                            newCat.ProductName = productName;

                            _data_context.Categories.InsertOnSubmit(newCat);
                        }
                    }
                    else
                    {
                        // wenn nicht mehr aus der datenbank geladen werden soll
                        // dann einen neue categorie anlegen
                        // beim submitten auf die datenbank werden diese duplizierten kategoriefehler dann autom. aufgelöst
                        Category newCat = new Category();
                        newCat.CategoryName = categoryName;
                        newCat.ProductName = productName;

                        _data_context.Categories.InsertOnSubmit(newCat);
                    }
                }
            }

        }
        #endregion

        #region Methods

        #region Application Startup/Shutdown methods
        /// <summary>
        /// Call this method on application start up
        /// </summary>
        /// <returns>true if succeed</returns>
        public virtual bool ApplicationStartupActions()
        {
            if (!_initialized)
                Initialize();

            return true;
        }

        /// <summary>
        /// Call this method on application shutdown
        /// </summary>
        /// <returns>true if succeed</returns>
        public virtual bool ApplicationShutdownActions()
        {
            _cancel_signal_logger = true;

            // abort connection attempts
            if (_bw_data_connection != null && _bw_data_connection.IsBusy)
            {
                _bw_data_connection.Abort();
                _bw_data_connection.Join();
                _bw_data_connection = null;
            }

            // abort logger work
            if (_bw_logger!= null && _bw_logger.IsBusy)
            {
                // do not abort the thread
                // the worker thread will finish its work with the current log entry
                // and quits after that, no matter if pending log entries are in the queue or not
                //_bw_logger.Abort();
                _bw_logger.Join();
                _bw_logger = null;
            }

            if (_data_context == null)
                return true;

            // STEP 1: Check if pending updates are present in the current data context
            ChangeSet csData = _data_context.GetChangeSet();
            bool updatesPending = false;

            updatesPending = csData.Inserts.Count > 0;
            updatesPending |= csData.Updates.Count > 0;
            updatesPending |= csData.Deletes.Count > 0;

            if(updatesPending)
            {
                // if updates need to be done
                // synchronously try to conntect to the database
                // and submit the changes
                if(_data_context.DatabaseExists())
                {
                    // reset submit try counter
                    _max_submit_try_counter = 0;
                    if(_submit_duplicate_key_conflict_solved != null)
                    {
                        _submit_duplicate_key_conflict_solved.Clear();
                        _submit_duplicate_key_conflict_solved = null;
                    }
                    _submit_duplicate_key_conflict_solved = new List<object>();

                    bool bSubmitSucceed = false;

                    if(_data_context != null)
                        lock (_data_context)
                        {
                            try
                            {
                                // serialize change set to a file
                                ChangeSetSerializer.SerializetoFile(_data_context, SerializationFilePath, true);
                            }
                            catch
                            {
                                if (File.Exists(SerializationFilePath))
                                    File.Delete(SerializationFilePath);
                            }

                            LoggingDataContext tmpDataContext = new LoggingDataContext(_connectionString);
                            tmpDataContext.LoadOptions = CreateDataLoadOptions();

                            try
                            {
                                // deserialize changes from file
                                ChangeSetSerializer.DeserializeFromFile(SerializationFilePath, tmpDataContext);
                                // if sucessfully recreated the changeset from the file
                                // clear the file contents
                                StreamWriter writer = File.CreateText(SerializationFilePath);
                                writer.Write("");
                                writer.Close();

                                //using (TransactionScope trans = new TransactionScope(TransactionScopeOption.RequiresNew, new TimeSpan(12, 0, 0)))
                                {
                                    bSubmitSucceed = SubmitChangedLoggingDataContext(tmpDataContext);

                                    if (bSubmitSucceed)
                                    {
                                        //trans.Complete();
                                        // new data context instance after changes submitted
                                        // will reinit the data context (needed for finalizer manipulations below)
                                        _data_context = new LoggingDataContext(_connectionString);
                                        _data_context.LoadOptions = CreateDataLoadOptions();
                                    }
                                }
                            }
                            catch
                            {
                                if(File.Exists(SerializationFilePath))
                                    File.Delete(SerializationFilePath);
                            }
                        }

                    if (bSubmitSucceed)
                    {
                        // call all data finalizers
                        foreach (DataFinalizerBase curdataFinalizer in _finalizer_activator.FinalizerList)
                        {
                            curdataFinalizer.AfterSubmitChanges(_data_context);
                        }

                        ChangeSet csTmp = _data_context.GetChangeSet();
                        bool bUpdates = false;

                        bUpdates |= csTmp.Inserts.Count > 0;
                        bUpdates |= csTmp.Updates.Count > 0;
                        bUpdates |= csTmp.Deletes.Count > 0;

                        // if one of the data finalizers made an update after their execution
                        if (bUpdates)
                        {
                            try
                            {
                                // serialize change set to a file
                                ChangeSetSerializer.SerializetoFile(_data_context, SerializationFilePath, true);
                            }
                            catch
                            {
                                if (File.Exists(SerializationFilePath))
                                    File.Delete(SerializationFilePath);
                            }

                            LoggingDataContext tmpDataContext = new LoggingDataContext(_connectionString);
                            tmpDataContext.LoadOptions = CreateDataLoadOptions();

                            try
                            {
                                // deserialize changes from file
                                ChangeSetSerializer.DeserializeFromFile(SerializationFilePath, tmpDataContext);
                                // if sucessfully recreated the changeset from the file
                                // clear the file contents
                                StreamWriter writer = File.CreateText(SerializationFilePath);
                                writer.Write("");
                                writer.Close();

                                // resubmit changes
                                //using (TransactionScope trans = new TransactionScope())
                                {
                                    bSubmitSucceed = SubmitChangedLoggingDataContext(tmpDataContext);

                                    if (bSubmitSucceed)
                                    {
                                        //trans.Complete();
                                        _data_context = new LoggingDataContext(_connectionString);
                                        _data_context.LoadOptions = CreateDataLoadOptions();
                                    }
                                }
                            }
                            catch
                            {
                                if (File.Exists(SerializationFilePath))
                                    File.Delete(SerializationFilePath);
                            }
                        }
                    }
                } 
                else
                {
                    try
                    {
                        // serialize change set to a file
                        ChangeSetSerializer.SerializetoFile(_data_context, SerializationFilePath, true);
                    }
                    catch
                    {
                        if (File.Exists(SerializationFilePath))
                            File.Delete(SerializationFilePath);
                    }
                }
            }

            // serialize log buffer entries
            SerializeLogBuffer();

            // uninitialize log manager since all states have been changed and serialized
            _initialized = false;
            _data_context = null;
            _max_submit_tries = 0;
            _work_todo = false;
            _log_error_counter = 0;
            _log_error_sub_counter = 0;
            _data_context_connecting_first_time = true;
            _data_context_state = LoggingDataConnectionState.Offline;

            return true;
        }
        #endregion

        #region Attach LogEntry to category
        /// <summary>
        /// Attaches a log entry to all ceategories defined in the list
        /// </summary>
        /// <param name="categoryNameList"></param>
        private void AttachLogEntryToCategories(LogEntry le, string[] categoryNameList)
        {
            if (_data_context == null)
                return;

            string productName = ProductName;

            if(le != null)
            {                
                lock (_data_context)
                {
                    if (categoryNameList != null)
                    {
                        foreach (string catName in categoryNameList)
                        {
                            if (CategoryNameExists(catName))
                            {
                                bool alreadyAttached = false;

                                if (le.CategoryLogEntries != null)
                                {
                                    foreach (CategoryLogEntry cle in le.CategoryLogEntries)
                                    {
                                        if (cle.ProductName == productName &&
                                            cle.CategoryName == catName)
                                        {
                                            alreadyAttached = true;
                                            break;
                                        }
                                    }
                                }

                                if (!alreadyAttached)
                                {
                                    // attach log entry to category
                                    CategoryLogEntry cle = new CategoryLogEntry();
                                    cle.CategoryName = catName;
                                    cle.ProductName = productName;
                                    le.CategoryLogEntries.Add(cle);
                                }
                            }
                        }
                    }
                }
            } 
            else
            {
                throw new ArgumentNullException("le");
            }
        }

        /// <summary>
        /// Checks if the given category name exists at the current data context
        /// </summary>
        /// <param name="categoryName"></param>
        /// <returns></returns>
        private bool CategoryNameExists(string categoryName)
        {
            if(_data_context == null)
                return false;

            string productName = ProductName;

            lock (_data_context)
            {
                try
                {
                    Category search = _data_context.Categories.Single(c => c.CategoryName == categoryName && c.ProductName == productName);

                    // return true if we found the category
                    return true;
                }
                catch
                {
                    // InvalidOperationException wenn keine Elemente gefunden
                    // SqlException wenn keine Verbindung zur Datenbank

                    // look in the inserts of the current contexts changeset
                    ChangeSet cs = _data_context.GetChangeSet();

                    foreach (object obj in cs.Inserts)
                    {
                        if (obj is Category)
                        {
                            if (((Category) obj).CategoryName == categoryName &&
                                ((Category) obj).ProductName == productName)
                                return true;
                        }
                    }
                }

            }
            return false;
        }
        #endregion

        #region Log writing methods

        #region write Eventlog entries
        /// <summary>
        /// Writes an entry to the conputer's event log
        /// </summary>
        /// <param name="type"></param>
        /// <param name="message"></param>
        public void WriteEventLogMessage(EventLogEntryType type, string message)
        {
            WriteLogToTextFile(-1,type,message,null);
            //return;

            //if (_event_log != null)
            //    _event_log.WriteEntry(message, type);
        }
        /// <summary>
        /// Writes an entry to the conputer's event log
        /// </summary>
        /// <param name="type"></param>
        /// <param name="message"></param>
        public void WriteEventLogMessage(int eventId, EventLogEntryType type, string message)
        {
            WriteLogToTextFile(eventId, type, message, null);
            //return;

            //if (_event_log != null)
            //    _event_log.WriteEntry(message, type, eventId);
        }
        /// <summary>
        /// Writes an entry to the conputer's event log
        /// </summary>
        /// <param name="type"></param>
        /// <param name="message"></param>
        public void WriteEventLogMessage(int eventId, EventLogEntryType type, string message, byte[] rawData)
        {
            WriteLogToTextFile(eventId, type, message, rawData);
            //return;

            //if (_event_log != null)
            //    _event_log.WriteEntry(message, type, eventId, 0, rawData);
        }

        public static void WriteLogToTextFile(int eventId, EventLogEntryType type, string message, byte[] rawData, string productname)
        {
            StreamWriter writer = null;

            try
            {
                string fileName = "error.log";
                string backupName = "error_" + DateTime.Now.ToString("dMMMyyyy") + ".log";

                string log_path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), productname + "\\" + fileName);

                if (!Directory.Exists(Path.GetDirectoryName(log_path)))
                    Directory.CreateDirectory(Path.GetDirectoryName(log_path));

                string backup_log_path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), productname + "\\" + backupName);

                if (File.Exists(log_path))
                {
                    FileInfo fi = new FileInfo(log_path);

                    if (fi.Length > MAXERROR_FILE_SIZE)
                    {
                        File.Copy(log_path, backup_log_path);
                        StreamWriter tmp = File.CreateText(log_path);
                        tmp.Close();
                    }
                }
                writer = File.AppendText(log_path);
                writer.WriteLine("Date: " + DateTime.Now.ToString("dd.MM.yyyy hh:mm:ss"));
                writer.WriteLine("EventId: " + eventId);
                writer.WriteLine("EventLogEntryType: " + type.ToString());
                writer.WriteLine("Message: " + message.Replace("\r\n", "\n").Replace("\n", "\r\n"));
                if (rawData != null)
                    writer.WriteLine("rawData" + Convert.ToBase64String(rawData));
                writer.WriteLine("-----------------------------------------------------------------------");
                //writer.Close();
            }
            catch(Exception ex)
            {
                
            }
            finally
            {
                if (writer != null)
                    writer.Close();
            }
        }

        private void WriteLogToTextFile(int eventId, EventLogEntryType type, string message, byte[] rawData)
        {
            LogManager.WriteLogToTextFile(eventId, type, message, rawData, Settings.ProductName);
        }

        #endregion

        #region write System Information entry
        /// <summary>
        /// Writes a new system info message to the log
        /// </summary>
        public void WriteSystemInfo()
        {
            WriteSystemInfo(LogManagerConstants.EVENTID_SYSTEMINFO, Priority.Normal, Severity.Information, string.Empty, new string[0]);
        }
        /// <summary>
        /// Writes a new system info message to the log
        /// </summary>
        /// <param name="message">additional message</param>
        public void WriteSystemInfo(string message)
        {
            WriteSystemInfo(LogManagerConstants.EVENTID_SYSTEMINFO, Priority.Normal, Severity.Information, message, new string[0]);
        }
        /// <summary>
        /// Writes a new system info message to the log
        /// </summary>
        /// <param name="eventId">Event ID (Use LogManagerConstants.EVENTID_SYSTEMINFO)</param>
        /// <param name="message">additional message</param>
        public void WriteSystemInfo(long eventId, string message)
        {
            WriteSystemInfo(eventId, Priority.Normal, Severity.Information, message, new string[0]);
        }
        /// <summary>
        /// Writes a new system info message to the log
        /// </summary>
        /// <param name="eventId">Event ID (Use LogManagerConstants.EVENTID_SYSTEMINFO)</param>
        /// <param name="priority">Priority of the entry</param>
        /// <param name="severity">Severity of the entry</param>
        /// <param name="message">additional message</param>
        public void WriteSystemInfo(long eventId, Priority priority, Severity severity, string message)
        {
            WriteSystemInfo(eventId, priority, severity, message, new string[0]);
        }
        /// <summary>
        /// Writes a new system info message to the log
        /// </summary>
        /// <param name="eventId">Event ID (Use LogManagerConstants.EVENTID_SYSTEMINFO)</param>
        /// <param name="priority">Priority of the entry</param>
        /// <param name="severity">Severity of the entry</param>
        /// <param name="message">additional message</param>
        /// <param name="categories">string array with log category names</param>
        public void WriteSystemInfo(long eventId, Priority priority, Severity severity, string message, string[] categories)
        {
            // create new log entry data object
            SystemInfoLogEntry siLogentry = new SystemInfoLogEntry();

            //// attach to standard system category
            //AttachLogEntryToCategories(siLogentry, new string[] {LogManagerConstants.SYSCATEGORY_SYSTEMINFO});

            //// attach to user categories
            //AttachLogEntryToCategories(siLogentry, categories);

            // union the standard categories and the category name array
            siLogentry.UnattachedCategtories = StringArrayUnion(new string[] { LogManagerConstants.SYSCATEGORY_SYSTEMINFO }, categories);

            // manually tell the log worker thread to repopulate properties
            siLogentry.RequiresDataRefresh = true;

            // basic properties
            siLogentry.ProductName = ProductName;
            siLogentry.EventId = eventId;
            siLogentry.Priority = priority;
            siLogentry.Severity = severity;
            siLogentry.SessionId = _application_session_guid.ToString();
            siLogentry.Message = message;
            // activity properties
            object _old_scope = _activity_scope;

            if (_activity_scope == null)
                _activity_scope = Thread.CurrentThread;

            siLogentry.ActivityId = ApplicationStatisticHelper.GetActivityId(_activity_scope).ToString();
            siLogentry.ActivityDescription = ApplicationStatisticHelper.GetActivityDescription(_activity_scope);
            siLogentry.RelatedActivityId = ApplicationStatisticHelper.GetRelatedActivityId(_activity_scope).ToString();

            _activity_scope = _old_scope;

            // put the log entry into the buffer queue
            lock (_log_buffer)
            {
                _log_buffer.Add(new LogEntryDataItem(siLogentry));
                _work_todo = true;
            }
        }
        #endregion

        #region write Application Message entries

        /// <summary>
        /// Writes a new application message entry to the log
        /// </summary>
        /// <param name="eventId">Event Id</param>
        public void WriteAppMessage(long eventId)
        {
            WriteAppMessage(eventId, Priority.Normal, Severity.Information, string.Empty, new string[0]);
        }
        /// <summary>
        /// Writes a new application message entry to the log
        /// </summary>
        /// <param name="eventId">Event Id</param>
        /// <param name="categories">string array with log category names</param>
        public void WriteAppMessage(long eventId, string[] categories)
        {
            WriteAppMessage(eventId, Priority.Normal, Severity.Information, string.Empty, categories);
        }
        /// <summary>
        /// Writes a new application message entry to the log
        /// </summary>
        /// <param name="eventId">Event Id</param>
        /// <param name="message">additional message</param>
        public void WriteAppMessage(long eventId, string message)
        {
            WriteAppMessage(eventId, Priority.Normal, Severity.Information, message, new string[0]);
        }
        /// <summary>
        /// Writes a new application message entry to the log
        /// </summary>
        /// <param name="eventId">Event Id</param>
        /// <param name="message">additional message</param>
        /// <param name="categories">string array with log category names</param>
        public void WriteAppMessage(long eventId, string message, string[] categories)
        {
            WriteAppMessage(eventId, Priority.Normal, Severity.Information, message, categories);
        }
        /// <summary>
        /// Writes a new application message entry to the log
        /// </summary>
        /// <param name="eventId">Event Id</param>
        /// <param name="message">additional message</param>
        /// <param name="priority">Priority of the entry</param>
        public void WriteAppMessage(long eventId, Priority priority, string message)
        {
            WriteAppMessage(eventId, priority, Severity.Information, message, new string[0]);
        }
        /// <summary>
        /// Writes a new application message entry to the log
        /// </summary>
        /// <param name="eventId">Event Id</param>
        /// <param name="message">additional message</param>
        /// <param name="priority">Priority of the entry</param>
        /// <param name="categories">string array with log category names</param>
        public void WriteAppMessage(long eventId, Priority priority, string message, string[] categories)
        {
            WriteAppMessage(eventId, priority, Severity.Information, message, categories);
        }
        /// <summary>
        /// Writes a new application message entry to the log
        /// </summary>
        /// <param name="eventId">Event Id</param>
        /// <param name="message">additional message</param>
        /// <param name="priority">Priority of the entry</param>
        /// <param name="severity">Severity of the entry</param>
        public void WriteAppMessage(long eventId, Priority priority, Severity severity, string message)
        {
            WriteAppMessage(eventId, priority, severity, message, new string[0]);
        }
        /// <summary>
        /// Writes a new application message entry to the log
        /// </summary>
        /// <param name="eventId">Event Id</param>
        /// <param name="message">additional message</param>
        /// <param name="priority">Priority of the entry</param>
        /// <param name="severity">Severity of the entry</param>
        /// <param name="categories">string array with log category names</param>
        public void WriteAppMessage(long eventId, Priority priority, Severity severity, string message, string[] categories)
        {
            // create new log entry data object
            AppMessageLogEntry amLogEntry = new AppMessageLogEntry();

            // union the standard categories and the category name array
            amLogEntry.UnattachedCategtories = StringArrayUnion(new string[] { LogManagerConstants.SYSCATEGORY_APPMESSAGE }, categories);

            // manually tell the log worker thread to repopulate properties
            amLogEntry.RequiresDataRefresh = true;

            // basic properties
            amLogEntry.ProductName = ProductName;
            amLogEntry.EventId = eventId;
            amLogEntry.Priority = priority;
            amLogEntry.Severity = severity;
            amLogEntry.SessionId = _application_session_guid.ToString();
            amLogEntry.Message = message;
            // activity properties
            object _old_scope = _activity_scope;

            if (_activity_scope == null)
                _activity_scope = Thread.CurrentThread;

            amLogEntry.ActivityId = ApplicationStatisticHelper.GetActivityId(_activity_scope).ToString();
            amLogEntry.ActivityDescription = ApplicationStatisticHelper.GetActivityDescription(_activity_scope);
            amLogEntry.RelatedActivityId = ApplicationStatisticHelper.GetRelatedActivityId(_activity_scope).ToString();

            _activity_scope = _old_scope;

            // write thread information of the method calling thread
            // these information could not be auto populated in a worker thread
            amLogEntry.ThreadName = ApplicationStatisticHelper.ThreadName;
            amLogEntry.Win32ThreadId = ApplicationStatisticHelper.Win32ThreadId;

            // name of the calling method
            amLogEntry.MethodName = ApplicationStatisticHelper.CallingMethodSignature;
            // full qualified type name of the calling class
            amLogEntry.ClassName = ApplicationStatisticHelper.CallingClassName;

            // put the log entry into the buffer queue
            lock(_log_buffer)
            {
#if DEBUG
                Debug.WriteLine(string.Format("*** LOGGING: Adding AppMessage to buffer: EventId: {0}, Message: {1} ", eventId, message));
#endif
                _log_buffer.Add(new LogEntryDataItem(amLogEntry));
                _work_todo = true;
            }
        }
        #endregion

        #region write User Action entries
        /// <summary>
        /// Writes a new user action log entry
        /// </summary>
        /// <param name="eventId">Event ID of the log entry</param>
        /// <param name="actionType">User action type name/description</param>
        public void WriteUserAction(long eventId, string actionType)
        {
            WriteUserAction(eventId, Priority.Normal, Severity.Information, actionType, new string[0], new string[0], string.Empty, new string[0]);
        }
        /// <summary>
        /// Writes a new user action log entry
        /// </summary>
        /// <param name="eventId">Event ID of the log entry</param>
        /// <param name="actionType">User action type name/description</param>
        /// <param name="categories">string array with log category names</param>
        public void WriteUserAction(long eventId, string actionType, string[] categories)
        {
            WriteUserAction(eventId, Priority.Normal, Severity.Information, actionType, new string[0], new string[0], string.Empty, categories);
        }
        /// <summary>
        /// Writes a new user action log entry
        /// </summary>
        /// <param name="eventId">Event ID of the log entry</param>
        /// <param name="actionType">User action type name/description</param>
        /// <param name="message">additional message</param>
        public void WriteUserAction(long eventId, string actionType, string message)
        {
            WriteUserAction(eventId, Priority.Normal, Severity.Information, actionType, new string[0], new string[0], message, new string[0]);
        }
        /// <summary>
        /// Writes a new user action log entry
        /// </summary>
        /// <param name="eventId">Event ID of the log entry</param>
        /// <param name="actionType">User action type name/description</param>
        /// <param name="message">additional message</param>
        /// <param name="categories">string array with log category names</param>
        public void WriteUserAction(long eventId, string actionType, string message, string[] categories)
        {
            WriteUserAction(eventId, Priority.Normal, Severity.Information, actionType, new string[0], new string[0], message, categories);
        }
        /// <summary>
        /// Writes a new user action log entry
        /// </summary>
        /// <param name="eventId">Event ID of the log entry</param>
        /// <param name="severity">Severity of the entry</param>
        /// <param name="actionType">User action type name/description</param>
        public void WriteUserAction(long eventId, Severity severity, string actionType)
        {
            WriteUserAction(eventId, Priority.Normal, severity, actionType, new string[0], new string[0], string.Empty, new string[0]);
        }
        /// <summary>
        /// Writes a new user action log entry
        /// </summary>
        /// <param name="eventId">Event ID of the log entry</param>
        /// <param name="severity">Severity of the entry</param>
        /// <param name="actionType">User action type name/description</param>
        /// <param name="categories">string array with log category names</param>
        public void WriteUserAction(long eventId, Severity severity, string actionType, string[] categories)
        {
            WriteUserAction(eventId, Priority.Normal, severity, actionType, new string[0], new string[0], string.Empty, categories);
        }
        /// <summary>
        /// Writes a new user action log entry
        /// </summary>
        /// <param name="eventId">Event ID of the log entry</param>
        /// <param name="priority">Priority of the entry</param>
        /// <param name="actionType">User action type name/description</param>
        public void WriteUserAction(long eventId, Priority priority, string actionType)
        {
            WriteUserAction(eventId, priority, Severity.Information, actionType, new string[0], new string[0], string.Empty, new string[0]);
        }
        /// <summary>
        /// Writes a new user action log entry
        /// </summary>
        /// <param name="eventId">Event ID of the log entry</param>
        /// <param name="priority">Priority of the entry</param>
        /// <param name="actionType">User action type name/description</param>
        /// <param name="categories">string array with log category names</param>
        public void WriteUserAction(long eventId, Priority priority, string actionType, string[] categories)
        {
            WriteUserAction(eventId, priority, Severity.Information, actionType, new string[0], new string[0], string.Empty, categories);
        }
        /// <summary>
        /// Writes a new user action log entry
        /// </summary>
        /// <param name="eventId">Event ID of the log entry</param>
        /// <param name="severity">Severity of the entry</param>
        /// <param name="actionType">User action type name/description</param>
        /// <param name="message">additional message</param>
        public void WriteUserAction(long eventId, Severity severity, string actionType, string message)
        {
            WriteUserAction(eventId, Priority.Normal, severity, actionType, new string[0], new string[0], message, new string[0]);
        }
        /// <summary>
        /// Writes a new user action log entry
        /// </summary>
        /// <param name="eventId">Event ID of the log entry</param>
        /// <param name="severity">Severity of the entry</param>
        /// <param name="actionType">User action type name/description</param>
        /// <param name="message">additional message</param>
        /// <param name="categories">string array with log category names</param>
        public void WriteUserAction(long eventId, Severity severity, string actionType, string message, string[] categories)
        {
            WriteUserAction(eventId, Priority.Normal, severity, actionType, new string[0], new string[0], message, categories);
        }
        /// <summary>
        /// Writes a new user action log entry
        /// </summary>
        /// <param name="eventId">Event ID of the log entry</param>
        /// <param name="priority">Priority of the entry</param>
        /// <param name="actionType">User action type name/description</param>
        /// <param name="message">additional message</param>
        public void WriteUserAction(long eventId, Priority priority, string actionType, string message)
        {
            WriteUserAction(eventId, priority, Severity.Information, actionType, new string[0], new string[0], message, new string[0]);
        }
        /// <summary>
        /// Writes a new user action log entry
        /// </summary>
        /// <param name="eventId">Event ID of the log entry</param>
        /// <param name="priority">Priority of the entry</param>
        /// <param name="actionType">User action type name/description</param>
        /// <param name="message">additional message</param>
        /// <param name="categories">string array with log category names</param>
        public void WriteUserAction(long eventId, Priority priority, string actionType, string message, string[] categories)
        {
            WriteUserAction(eventId, priority, Severity.Information, actionType, new string[0], new string[0], message, categories);
        }
        /// <summary>
        /// Writes a new user action log entry
        /// </summary>
        /// <param name="eventId">Event ID of the log entry</param>
        /// <param name="priority">Priority of the entry</param>
        /// <param name="severity">Severity of the entry</param>
        /// <param name="actionType">User action type name/description</param>
        public void WriteUserAction(long eventId, Priority priority, Severity severity, string actionType)
        {
            WriteUserAction(eventId, priority, severity, actionType, new string[0], new string[0], string.Empty, new string[0]);
        }
        /// <summary>
        /// Writes a new user action log entry
        /// </summary>
        /// <param name="eventId">Event ID of the log entry</param>
        /// <param name="priority">Priority of the entry</param>
        /// <param name="severity">Severity of the entry</param>
        /// <param name="actionType">User action type name/description</param>
        /// <param name="categories">string array with log category names</param>
        public void WriteUserAction(long eventId, Priority priority, Severity severity, string actionType, string[] categories)
        {
            WriteUserAction(eventId, priority, severity, actionType, new string[0], new string[0], string.Empty, categories);
        }
        /// <summary>
        /// Writes a new user action log entry
        /// </summary>
        /// <param name="eventId">Event ID of the log entry</param>
        /// <param name="priority">Priority of the entry</param>
        /// <param name="severity">Severity of the entry</param>
        /// <param name="actionType">User action type name/description</param>
        /// <param name="actionInfos">String array containing action information (512 chars max per info, 5 infos will be stored in db)</param>
        /// <param name="actionDatas">String array containing action data (unlimited text per data, 3 data strings will be stored in db)</param>
        public void WriteUserAction(long eventId, Priority priority, Severity severity, string actionType, string[] actionInfos, string[] actionDatas)
        {
            WriteUserAction(eventId, priority, severity, actionType, actionInfos, actionDatas, string.Empty, new string[0]);
        }
        /// <summary>
        /// Writes a new user action log entry
        /// </summary>
        /// <param name="eventId">Event ID of the log entry</param>
        /// <param name="priority">Priority of the entry</param>
        /// <param name="severity">Severity of the entry</param>
        /// <param name="actionType">User action type name/description</param>
        /// <param name="actionInfos">String array containing action information (512 chars max per info, 5 infos will be stored in db)</param>
        /// <param name="actionDatas">String array containing action data (unlimited text per data, 3 data strings will be stored in db)</param>
        /// <param name="categories">string array with log category names</param>
        public void WriteUserAction(long eventId, Priority priority, Severity severity, string actionType, string[] actionInfos, string[] actionDatas, string[] categories)
        {
            WriteUserAction(eventId, priority, severity, actionType, actionInfos, actionDatas, string.Empty, new string[0]);
        }
        /// <summary>
        /// Writes a new user action log entry
        /// </summary>
        /// <param name="eventId">Event ID of the log entry</param>
        /// <param name="priority">Priority of the entry</param>
        /// <param name="severity">Severity of the entry</param>
        /// <param name="actionType">User action type name/description</param>
        /// <param name="actionInfos">String array containing action information (512 chars max per info, 5 infos will be stored in db)</param>
        /// <param name="actionDatas">String array containing action data (unlimited text per data, 3 data strings will be stored in db)</param>
        /// <param name="message">additional message</param>
        public void WriteUserAction(long eventId, Priority priority, Severity severity, string actionType, string[] actionInfos, string[] actionDatas, string message)
        {
            WriteUserAction(eventId, priority, severity, actionType, actionInfos, actionDatas, message, new string[0]);
        }
        /// <summary>
        /// Writes a new user action log entry
        /// </summary>
        /// <param name="eventId">Event ID of the log entry</param>
        /// <param name="priority">Priority of the entry</param>
        /// <param name="severity">Severity of the entry</param>
        /// <param name="actionType">User action type name/description</param>
        /// <param name="actionInfos">String array containing action information (512 chars max per info, 5 infos will be stored in db)</param>
        /// <param name="actionDatas">String array containing action data (unlimited text per data, 3 data strings will be stored in db)</param>
        /// <param name="message">additional message</param>
        /// <param name="categories">string array with log category names</param>
        public void WriteUserAction(long eventId, Priority priority, Severity severity, string actionType, string[] actionInfos, string[] actionDatas, string message, string[] categories)
        {
            // create new log entry data object
            UserActionLogEntry uaLogEntry = new UserActionLogEntry();

            // union the standard categories and the category name array
            uaLogEntry.UnattachedCategtories = StringArrayUnion(new string[] { LogManagerConstants.SYSCATEGORY_SYSTEMINFO }, categories);

            // manually tell the log worker thread to repopulate properties
            uaLogEntry.RequiresDataRefresh = true;

            // basic properties
            uaLogEntry.ProductName = ProductName;
            uaLogEntry.EventId = eventId;
            uaLogEntry.Priority = priority;
            uaLogEntry.Severity = severity;
            uaLogEntry.SessionId = _application_session_guid.ToString();
            uaLogEntry.Message = message;
            // activity properties
            object _old_scope = _activity_scope;

            if (_activity_scope == null)
                _activity_scope = Thread.CurrentThread;

            uaLogEntry.ActivityId = ApplicationStatisticHelper.GetActivityId(_activity_scope).ToString();
            uaLogEntry.ActivityDescription = ApplicationStatisticHelper.GetActivityDescription(_activity_scope);
            uaLogEntry.RelatedActivityId = ApplicationStatisticHelper.GetRelatedActivityId(_activity_scope).ToString();

            _activity_scope = _old_scope;

            // write thread information of the method calling thread
            // these information could not be auto populated in a worker thread
            uaLogEntry.ThreadName = ApplicationStatisticHelper.ThreadName;
            uaLogEntry.Win32ThreadId = ApplicationStatisticHelper.Win32ThreadId;

            #region Populate log specific information
            uaLogEntry.ActionType = actionType;
            int i = 1;

            do
            {
                if(actionInfos.Length >= i)
                {
                    switch(i)
                    {
                        case 1:
                            uaLogEntry.ActionInfo1 = actionInfos[i - 1]; break;
                        case 2:
                            uaLogEntry.ActionInfo2 = actionInfos[i - 1]; break;
                        case 3:
                            uaLogEntry.ActionInfo3 = actionInfos[i - 1]; break;
                        case 4:
                            uaLogEntry.ActionInfo4 = actionInfos[i - 1]; break;
                        case 5:
                            uaLogEntry.ActionInfo5 = actionInfos[i - 1]; break;
                    }
                }
                i++;
            } while (i <= 5);

            i = 1;
            do
            {
                if (actionInfos.Length >= i)
                {
                    switch (i)
                    {
                        case 1:
                            uaLogEntry.ActionData1 = actionDatas[i - 1]; break;
                        case 2:
                            uaLogEntry.ActionData3 = actionDatas[i - 1]; break;
                        case 3:
                            uaLogEntry.ActionData2 = actionDatas[i - 1]; break;
                    }
                }
                i++;
            } while (i <= 3);
            #endregion

            // put the log entry into the buffer queue
            lock (_log_buffer)
            {
#if DEBUG
                Debug.WriteLine(string.Format("*** LOGGING: Adding UserAction to buffer: EventId: {0}, ActionType: {1}, Message: {2} ", eventId, actionType, message));
#endif

                _log_buffer.Add(new LogEntryDataItem(uaLogEntry));
                _work_todo = true;
            }
        }
        #endregion

        #region write Application Session - START entry
        /// <summary>
        /// Writes a new application session start entry
        /// </summary>
        public void WriteAppSessionStart()
        {
            WriteAppSessionStart(LogManagerConstants.EVENTID_APPSTARTUP, Priority.Normal, Severity.Information, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, new string[0]);
        }
        /// <summary>
        /// Writes a new application session start entry
        /// </summary>
        /// <param name="categories">string array with log category names</param>
        public void WriteAppSessionStart(string[] categories)
        {
            WriteAppSessionStart(LogManagerConstants.EVENTID_APPSTARTUP, Priority.Normal, Severity.Information, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, categories);
        }
        /// <summary>
        /// Writes a new application session start entry
        /// </summary>
        /// <param name="message">additional message</param>
        public void WriteAppSessionStart(string message)
        {
            WriteAppSessionStart(LogManagerConstants.EVENTID_APPSTARTUP, Priority.Normal, Severity.Information, string.Empty, string.Empty, string.Empty, string.Empty, message, new string[0]);
        }
        /// <summary>
        /// Writes a new application session start entry
        /// </summary>
        /// <param name="message">additional message</param>
        /// <param name="categories">string array with log category names</param>
        public void WriteAppSessionStart(string message, string[] categories)
        {
            WriteAppSessionStart(LogManagerConstants.EVENTID_APPSTARTUP, Priority.Normal, Severity.Information, string.Empty, string.Empty, string.Empty, string.Empty, message, categories);
        }
        /// <summary>
        /// Writes a new application session start entry
        /// </summary>
        /// <param name="severity">Severity of the entry</param>
        /// <param name="message">additional message</param>
        public void WriteAppSessionStart(Severity severity, string message)
        {
            WriteAppSessionStart(LogManagerConstants.EVENTID_APPSTARTUP, Priority.Normal, severity, string.Empty, string.Empty, string.Empty, string.Empty, message, new string[0]);
        }
        /// <summary>
        /// Writes a new application session start entry
        /// </summary>
        /// <param name="severity">Severity of the entry</param>
        /// <param name="message">additional message</param>
        /// <param name="categories">string array with log category names</param>
        public void WriteAppSessionStart(Severity severity, string message, string[] categories)
        {
            WriteAppSessionStart(LogManagerConstants.EVENTID_APPSTARTUP, Priority.Normal, severity, string.Empty, string.Empty, string.Empty, string.Empty, message, categories);
        }
        /// <summary>
        /// Writes a new application session start entry
        /// </summary>
        /// <param name="severity">Severity of the entry</param>
        /// <param name="appName">application name</param>
        /// <param name="appType">application type</param>
        /// <param name="appVersion">application version</param>
        /// <param name="installMode">installation mode</param>
        public void WriteAppSessionStart(Severity severity, string appName, string appType, string appVersion, string installMode)
        {
            WriteAppSessionStart(LogManagerConstants.EVENTID_APPSTARTUP, Priority.Normal, severity, appName, appType, appVersion, installMode, string.Empty, new string[0]);
        }
        /// <summary>
        /// Writes a new application session start entry
        /// </summary>
        /// <param name="severity">Severity of the entry</param>
        /// <param name="appName">application name</param>
        /// <param name="appType">application type</param>
        /// <param name="appVersion">application version</param>
        /// <param name="installMode">installation mode</param>
        /// <param name="categories">string array with log category names</param>
        public void WriteAppSessionStart(Severity severity, string appName, string appType, string appVersion, string installMode, string[] categories)
        {
            WriteAppSessionStart(LogManagerConstants.EVENTID_APPSTARTUP, Priority.Normal, severity, appName, appType, appVersion, installMode, string.Empty, categories);
        }

        /// <summary>
        /// Writes a new application session start entry
        /// </summary>
        /// <param name="priority">Priority of the entry</param>
        /// <param name="message">additional message</param>
        public void WriteAppSessionStart(Priority priority, string message)
        {
            WriteAppSessionStart(LogManagerConstants.EVENTID_APPSTARTUP, priority, Severity.Information, string.Empty, string.Empty, string.Empty, string.Empty, message, new string[0]);
        }
        /// <summary>
        /// Writes a new application session start entry
        /// </summary>
        /// <param name="priority">Priority of the entry</param>
        /// <param name="message">additional message</param>
        /// <param name="categories">string array with log category names</param>
        public void WriteAppSessionStart(Priority priority, string message, string[] categories)
        {
            WriteAppSessionStart(LogManagerConstants.EVENTID_APPSTARTUP, priority, Severity.Information, string.Empty, string.Empty, string.Empty, string.Empty, message, categories);
        }
        /// <summary>
        /// Writes a new application session start entry
        /// </summary>
        /// <param name="priority">Priority of the entry</param>
        /// <param name="appName">application name</param>
        /// <param name="appType">application type</param>
        /// <param name="appVersion">application version</param>
        /// <param name="installMode">installation mode</param>
        public void WriteAppSessionStart(Priority priority, string appName, string appType, string appVersion, string installMode)
        {
            WriteAppSessionStart(LogManagerConstants.EVENTID_APPSTARTUP, priority, Severity.Information, appName, appType, appVersion, installMode, string.Empty, new string[0]);
        }
        /// <summary>
        /// Writes a new application session start entry
        /// </summary>
        /// <param name="priority">Priority of the entry</param>
        /// <param name="appName">application name</param>
        /// <param name="appType">application type</param>
        /// <param name="appVersion">application version</param>
        /// <param name="installMode">installation mode</param>
        /// <param name="categories">string array with log category names</param>
        public void WriteAppSessionStart(Priority priority, string appName, string appType, string appVersion, string installMode, string[] categories)
        {
            WriteAppSessionStart(LogManagerConstants.EVENTID_APPSTARTUP, priority, Severity.Information, appName, appType, appVersion, installMode, string.Empty, categories);
        }

        /// <summary>
        /// Writes a new application session start entry
        /// </summary>
        /// <param name="priority">Priority of the entry</param>
        /// <param name="severity">Severity of the entry</param>
        /// <param name="message">additional message</param>
        public void WriteAppSessionStart(Priority priority, Severity severity, string message)
        {
            WriteAppSessionStart(LogManagerConstants.EVENTID_APPSTARTUP, priority, severity, string.Empty, string.Empty, string.Empty, string.Empty, message, new string[0]);
        }
        /// <summary>
        /// Writes a new application session start entry
        /// </summary>
        /// <param name="priority">Priority of the entry</param>
        /// <param name="severity">Severity of the entry</param>
        /// <param name="message">additional message</param>
        /// <param name="categories">string array with log category names</param>
        public void WriteAppSessionStart(Priority priority, Severity severity, string message, string[] categories)
        {
            WriteAppSessionStart(LogManagerConstants.EVENTID_APPSTARTUP, priority, severity, string.Empty, string.Empty, string.Empty, string.Empty, message, categories);
        }
        /// <summary>
        /// Writes a new application session start entry
        /// </summary>
        /// <param name="priority">Priority of the entry</param>
        /// <param name="severity">Severity of the entry</param>
        /// <param name="appName">application name</param>
        /// <param name="appType">application type</param>
        /// <param name="appVersion">application version</param>
        /// <param name="installMode">installation mode</param>
        public void WriteAppSessionStart(Priority priority, Severity severity, string appName, string appType, string appVersion, string installMode)
        {
            WriteAppSessionStart(LogManagerConstants.EVENTID_APPSTARTUP, priority, severity, appName, appType, appVersion, installMode, string.Empty, new string[0]);
        }
        /// <summary>
        /// Writes a new application session start entry
        /// </summary>
        /// <param name="priority">Priority of the entry</param>
        /// <param name="severity">Severity of the entry</param>
        /// <param name="appName">application name</param>
        /// <param name="appType">application type</param>
        /// <param name="appVersion">application version</param>
        /// <param name="installMode">installation mode</param>
        /// <param name="categories">string array with log category names</param>
        public void WriteAppSessionStart(Priority priority, Severity severity, string appName, string appType, string appVersion, string installMode, string[] categories)
        {
            WriteAppSessionStart(LogManagerConstants.EVENTID_APPSTARTUP, priority, severity, appName, appType, appVersion, installMode, string.Empty, categories);
        }
        /// <summary>
        /// Writes a new application session start entry
        /// </summary>
        /// <param name="priority">Priority of the entry</param>
        /// <param name="severity">Severity of the entry</param>
        /// <param name="appName">application name</param>
        /// <param name="appType">application type</param>
        /// <param name="appVersion">application version</param>
        /// <param name="installMode">installation mode</param>
        /// <param name="message">additional message</param>
        public void WriteAppSessionStart(Priority priority, Severity severity, string appName, string appType, string appVersion, string installMode, string message)
        {
            WriteAppSessionStart(LogManagerConstants.EVENTID_APPSTARTUP, priority, severity, appName, appType, appVersion, installMode, message, new string[0]);
        }
        /// <summary>
        /// Writes a new application session start entry
        /// </summary>
        /// <param name="priority">Priority of the entry</param>
        /// <param name="severity">Severity of the entry</param>
        /// <param name="appName">application name</param>
        /// <param name="appType">application type</param>
        /// <param name="appVersion">application version</param>
        /// <param name="installMode">installation mode</param>
        /// <param name="message">additional message</param>
        /// <param name="categories">string array with log category names</param>
        public void WriteAppSessionStart(Priority priority, Severity severity, string appName, string appType, string appVersion, string installMode, string message, string[] categories)
        {
            WriteAppSessionStart(LogManagerConstants.EVENTID_APPSTARTUP, priority, severity, appName, appType, appVersion, installMode, message, categories);
        }

        /// <summary>
        /// Writes a new application session start entry
        /// </summary>
        /// <param name="eventId">Event Id (use LogManagerConstants.EVENTID_APPSTARTUP)</param>
        /// <param name="severity">Severity of the entry</param>
        /// <param name="message">additional message</param>
        public void WriteAppSessionStart(long eventId, Severity severity, string message)
        {
            WriteAppSessionStart(eventId, Priority.Normal, severity, string.Empty, string.Empty, string.Empty, string.Empty, message, new string[0]);
        }
        /// <summary>
        /// Writes a new application session start entry
        /// </summary>
        /// <param name="eventId">Event Id (use LogManagerConstants.EVENTID_APPSTARTUP)</param>
        /// <param name="severity">Severity of the entry</param>
        /// <param name="message">additional message</param>
        /// <param name="categories">string array with log category names</param>
        public void WriteAppSessionStart(long eventId, Severity severity, string message, string[] categories)
        {
            WriteAppSessionStart(eventId, Priority.Normal, severity, string.Empty, string.Empty, string.Empty, string.Empty, message, categories);
        }
        /// <summary>
        /// Writes a new application session start entry
        /// </summary>
        /// <param name="eventId">Event Id (use LogManagerConstants.EVENTID_APPSTARTUP)</param>
        /// <param name="severity">Severity of the entry</param>
        /// <param name="appName">application name</param>
        /// <param name="appType">appliction type</param>
        /// <param name="appVersion">application version</param>
        /// <param name="installMode">installation mode</param>
        public void WriteAppSessionStart(long eventId, Severity severity, string appName, string appType, string appVersion, string installMode)
        {
            WriteAppSessionStart(eventId, Priority.Normal, severity, appName, appType, appVersion, installMode, string.Empty, new string[0]);
        }
        /// <summary>
        /// Writes a new application session start entry
        /// </summary>
        /// <param name="eventId">Event Id (use LogManagerConstants.EVENTID_APPSTARTUP)</param>
        /// <param name="severity">Severity of the entry</param>
        /// <param name="appName">application name</param>
        /// <param name="appType">appliction type</param>
        /// <param name="appVersion">application version</param>
        /// <param name="installMode">installation mode</param>
        /// <param name="categories">string array with log category names</param>
        public void WriteAppSessionStart(long eventId, Severity severity, string appName, string appType, string appVersion, string installMode, string[] categories)
        {
            WriteAppSessionStart(eventId, Priority.Normal, severity, appName, appType, appVersion, installMode, string.Empty, categories);
        }

        /// <summary>
        /// Writes a new application session start entry
        /// </summary>
        /// <param name="eventId">Event Id (use LogManagerConstants.EVENTID_APPSTARTUP)</param>
        /// <param name="priority">Priority of the entry</param>
        /// <param name="message">additional message</param>
        public void WriteAppSessionStart(long eventId, Priority priority, string message)
        {
            WriteAppSessionStart(eventId, priority, Severity.Information, string.Empty, string.Empty, string.Empty, string.Empty, message, new string[0]);
        }
        /// <summary>
        /// Writes a new application session start entry
        /// </summary>
        /// <param name="eventId">Event Id (use LogManagerConstants.EVENTID_APPSTARTUP)</param>
        /// <param name="priority">Priority of the entry</param>
        /// <param name="message">additional message</param>
        /// <param name="categories">string array with log category names</param>
        public void WriteAppSessionStart(long eventId, Priority priority, string message, string[] categories)
        {
            WriteAppSessionStart(eventId, priority, Severity.Information, string.Empty, string.Empty, string.Empty, string.Empty, message, categories);
        }
        /// <summary>
        /// Writes a new application session start entry
        /// </summary>
        /// <param name="eventId">Event Id (use LogManagerConstants.EVENTID_APPSTARTUP)</param>
        /// <param name="priority">Priority of the entry</param>
        /// <param name="appName">application name</param>
        /// <param name="appType">application type</param>
        /// <param name="appVersion">application version</param>
        /// <param name="installMode">installation mode</param>
        public void WriteAppSessionStart(long eventId, Priority priority, string appName, string appType, string appVersion, string installMode)
        {
            WriteAppSessionStart(eventId, priority, Severity.Information, appName, appType, appVersion, installMode, string.Empty, new string[0]);
        }
        /// <summary>
        /// Writes a new application session start entry
        /// </summary>
        /// <param name="eventId">Event Id (use LogManagerConstants.EVENTID_APPSTARTUP)</param>
        /// <param name="priority">Priority of the entry</param>
        /// <param name="appName">application name</param>
        /// <param name="appType">application type</param>
        /// <param name="appVersion">application version</param>
        /// <param name="installMode">installation mode</param>
        /// <param name="categories">string array with log category names</param>
        public void WriteAppSessionStart(long eventId, Priority priority, string appName, string appType, string appVersion, string installMode, string[] categories)
        {
            WriteAppSessionStart(eventId, priority, Severity.Information, appName, appType, appVersion, installMode, string.Empty, categories);
        }

        /// <summary>
        /// Writes a new application session start entry
        /// </summary>
        /// <param name="eventId">Event Id (use LogManagerConstants.EVENTID_APPSTARTUP)</param>
        /// <param name="priority">Priority of the entry</param>
        /// <param name="severity">Severity of the entry</param>
        /// <param name="message">additional message</param>
        public void WriteAppSessionStart(long eventId, Priority priority, Severity severity, string message)
        {
            WriteAppSessionStart(eventId, priority, severity, string.Empty, string.Empty, string.Empty, string.Empty, message, new string[0]);
        }
        /// <summary>
        /// Writes a new application session start entry
        /// </summary>
        /// <param name="eventId">Event Id (use LogManagerConstants.EVENTID_APPSTARTUP)</param>
        /// <param name="priority">Priority of the entry</param>
        /// <param name="severity">Severity of the entry</param>
        /// <param name="message">additional message</param>
        /// <param name="categories">string array with log category names</param>
        public void WriteAppSessionStart(long eventId, Priority priority, Severity severity, string message, string[] categories)
        {
            WriteAppSessionStart(eventId, priority, severity, string.Empty, string.Empty, string.Empty, string.Empty, message, categories);
        }
        /// <summary>
        /// Writes a new application session start entry
        /// </summary>
        /// <param name="eventId">Event Id (use LogManagerConstants.EVENTID_APPSTARTUP)</param>
        /// <param name="priority">Priority of the entry</param>
        /// <param name="severity">Severity of the entry</param>
        /// <param name="appName">application name</param>
        /// <param name="appType">application type</param>
        /// <param name="appVersion">application version</param>
        /// <param name="installMode">installation mode</param>
        public void WriteAppSessionStart(long eventId, Priority priority, Severity severity, string appName, string appType, string appVersion, string installMode)
        {
            WriteAppSessionStart(eventId, priority, severity, appName, appType, appVersion, installMode, string.Empty, new string[0]);
        }
        /// <summary>
        /// Writes a new application session start entry
        /// </summary>
        /// <param name="eventId">Event Id (use LogManagerConstants.EVENTID_APPSTARTUP)</param>
        /// <param name="priority">Priority of the entry</param>
        /// <param name="severity">Severity of the entry</param>
        /// <param name="appName">application name</param>
        /// <param name="appType">application type</param>
        /// <param name="appVersion">application version</param>
        /// <param name="installMode">installation mode</param>
        /// <param name="categories">string array with log category names</param>
        public void WriteAppSessionStart(long eventId, Priority priority, Severity severity, string appName, string appType, string appVersion, string installMode, string[] categories)
        {
            WriteAppSessionStart(eventId, priority, severity, appName, appType, appVersion, installMode, string.Empty, categories);
        }
        /// <summary>
        /// Writes a new application session start entry
        /// </summary>
        /// <param name="eventId">Event Id (use LogManagerConstants.EVENTID_APPSTARTUP)</param>
        /// <param name="priority">Priority of the entry</param>
        /// <param name="severity">Severity of the entry</param>
        /// <param name="appName">application name</param>
        /// <param name="appType">application type</param>
        /// <param name="appVersion">application version</param>
        /// <param name="installMode">installation mode</param>
        /// <param name="message">additional message</param>
        public void WriteAppSessionStart(long eventId, Priority priority, Severity severity, string appName, string appType, string appVersion, string installMode, string message)
        {
            WriteAppSessionStart(eventId, priority, severity, appName, appType, appVersion, installMode, message, new string[0]);
        }
        /// <summary>
        /// Writes a new application session start entry
        /// </summary>
        /// <param name="eventId">Event Id (use LogManagerConstants.EVENTID_APPSTARTUP)</param>
        /// <param name="priority">Priority of the entry</param>
        /// <param name="severity">Severity of the entry</param>
        /// <param name="appName">application name</param>
        /// <param name="appType">application type</param>
        /// <param name="appVersion">application version</param>
        /// <param name="installMode">installation mode</param>
        /// <param name="message">additional message</param>
        /// <param name="categories">string array with log category names</param>
        public void WriteAppSessionStart(long eventId, Priority priority, Severity severity, string appName, string appType, string appVersion, string installMode, string message, string[] categories)
        {
            // store the current application session id
            _application_session_guid = Guid.NewGuid();

            // create new log entry data object
            AppSessionLogEntry asLogEntry = new AppSessionLogEntry();

            // union the standard categories and the category name array
            asLogEntry.UnattachedCategtories = StringArrayUnion(new string[] { LogManagerConstants.SYSCATEGORY_APPSESSION }, categories);

            // manually tell the log worker thread to repopulate properties
            asLogEntry.RequiresDataRefresh = true;

            // basic properties
            asLogEntry.ProductName = ProductName;
            asLogEntry.EventId = eventId;
            asLogEntry.Priority = priority;
            asLogEntry.Severity = severity;
            asLogEntry.SessionId = _application_session_guid.ToString();
            asLogEntry.Message = message;
            // activity properties
            object _old_scope = _activity_scope;

            if (_activity_scope == null)
                _activity_scope = Thread.CurrentThread;

            asLogEntry.ActivityId = ApplicationStatisticHelper.GetActivityId(_activity_scope).ToString();
            asLogEntry.ActivityDescription = ApplicationStatisticHelper.GetActivityDescription(_activity_scope);
            asLogEntry.RelatedActivityId = ApplicationStatisticHelper.GetRelatedActivityId(_activity_scope).ToString();

            _activity_scope = _old_scope;

            #region Populate log specifc information
            asLogEntry.IsStartEntry = true;
            asLogEntry.AppName = appName;
            asLogEntry.AppType = appType;
            asLogEntry.AppVersion = appVersion;
            asLogEntry.InstallMode = installMode;
            #endregion

            // put the log entry into the buffer queue
            lock (_log_buffer)
            {
                _log_buffer.Add(new LogEntryDataItem(asLogEntry));
                _work_todo = true;
            }
        }
        #endregion

        #region write Application Session - END entry

        /// <summary>
        /// Writes a new application session end entry
        /// </summary>
        public void WriteAppSessionEnd()
        {
            WriteAppSessionEnd(LogManagerConstants.EVENTID_APPEND, Priority.Normal, Severity.Information, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, new string[0]);
        }
        /// <summary>
        /// Writes a new application session end entry
        /// </summary>
        /// <param name="categories">string array with log category names</param>
        public void WriteAppSessionEnd(string[] categories)
        {
            WriteAppSessionEnd(LogManagerConstants.EVENTID_APPEND, Priority.Normal, Severity.Information, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, categories);
        }
        /// <summary>
        /// Writes a new application session end entry
        /// </summary>
        /// <param name="message">additional message</param>
        public void WriteAppSessionEnd(string message)
        {
            WriteAppSessionEnd(LogManagerConstants.EVENTID_APPEND, Priority.Normal, Severity.Information, string.Empty, string.Empty, string.Empty, string.Empty, message, new string[0]);
        }
        /// <summary>
        /// Writes a new application session end entry
        /// </summary>
        /// <param name="message">additional message</param>
        /// <param name="categories">string array with log category names</param>
        public void WriteAppSessionEnd(string message, string[] categories)
        {
            WriteAppSessionEnd(LogManagerConstants.EVENTID_APPEND, Priority.Normal, Severity.Information, string.Empty, string.Empty, string.Empty, string.Empty, message, categories);
        }
        /// <summary>
        /// Writes a new application session end entry
        /// </summary>
        /// <param name="severity">Severity of the entry</param>
        /// <param name="message">additional message</param>
        public void WriteAppSessionEnd(Severity severity, string message)
        {
            WriteAppSessionEnd(LogManagerConstants.EVENTID_APPEND, Priority.Normal, severity, string.Empty, string.Empty, string.Empty, string.Empty, message, new string[0]);
        }
        /// <summary>
        /// Writes a new application session end entry
        /// </summary>
        /// <param name="severity">Severity of the entry</param>
        /// <param name="message">additional message</param>
        /// <param name="categories">string array with log category names</param>
        public void WriteAppSessionEnd(Severity severity, string message, string[] categories)
        {
            WriteAppSessionEnd(LogManagerConstants.EVENTID_APPEND, Priority.Normal, severity, string.Empty, string.Empty, string.Empty, string.Empty, message, categories);
        }
        /// <summary>
        /// Writes a new application session end entry
        /// </summary>
        /// <param name="severity">Severity of the entry</param>
        /// <param name="appName">application name</param>
        /// <param name="appType">application type</param>
        /// <param name="appVersion">application version</param>
        /// <param name="installMode">installation mode</param>
        public void WriteAppSessionEnd(Severity severity, string appName, string appType, string appVersion, string installMode)
        {
            WriteAppSessionEnd(LogManagerConstants.EVENTID_APPEND, Priority.Normal, severity, appName, appType, appVersion, installMode, string.Empty, new string[0]);
        }
        /// <summary>
        /// Writes a new application session end entry
        /// </summary>
        /// <param name="severity">Severity of the entry</param>
        /// <param name="appName">application name</param>
        /// <param name="appType">application type</param>
        /// <param name="appVersion">application version</param>
        /// <param name="installMode">installation mode</param>
        /// <param name="categories">string array with log category names</param>
        public void WriteAppSessionEnd(Severity severity, string appName, string appType, string appVersion, string installMode, string[] categories)
        {
            WriteAppSessionEnd(LogManagerConstants.EVENTID_APPEND, Priority.Normal, severity, appName, appType, appVersion, installMode, string.Empty, categories);
        }

        /// <summary>
        /// Writes a new application session end entry
        /// </summary>
        /// <param name="priority">Priority of the entry</param>
        /// <param name="message">additional message</param>
        public void WriteAppSessionEnd(Priority priority, string message)
        {
            WriteAppSessionEnd(LogManagerConstants.EVENTID_APPEND, priority, Severity.Information, string.Empty, string.Empty, string.Empty, string.Empty, message, new string[0]);
        }
        /// <summary>
        /// Writes a new application session end entry
        /// </summary>
        /// <param name="priority">Priority of the entry</param>
        /// <param name="message">additional message</param>
        /// <param name="categories">string array with log category names</param>
        public void WriteAppSessionEnd(Priority priority, string message, string[] categories)
        {
            WriteAppSessionEnd(LogManagerConstants.EVENTID_APPEND, priority, Severity.Information, string.Empty, string.Empty, string.Empty, string.Empty, message, categories);
        }
        /// <summary>
        /// Writes a new application session end entry
        /// </summary>
        /// <param name="priority">Priority of the entry</param>
        /// <param name="appName">application name</param>
        /// <param name="appType">application type</param>
        /// <param name="appVersion">application version</param>
        /// <param name="installMode">installation mode</param>
        public void WriteAppSessionEnd(Priority priority, string appName, string appType, string appVersion, string installMode)
        {
            WriteAppSessionEnd(LogManagerConstants.EVENTID_APPEND, priority, Severity.Information, appName, appType, appVersion, installMode, string.Empty, new string[0]);
        }
        /// <summary>
        /// Writes a new application session end entry
        /// </summary>
        /// <param name="priority">Priority of the entry</param>
        /// <param name="appName">application name</param>
        /// <param name="appType">application type</param>
        /// <param name="appVersion">application version</param>
        /// <param name="installMode">installation mode</param>
        /// <param name="categories">string array with log category names</param>
        public void WriteAppSessionEnd(Priority priority, string appName, string appType, string appVersion, string installMode, string[] categories)
        {
            WriteAppSessionEnd(LogManagerConstants.EVENTID_APPEND, priority, Severity.Information, appName, appType, appVersion, installMode, string.Empty, categories);
        }

        /// <summary>
        /// Writes a new application session end entry
        /// </summary>
        /// <param name="priority">Priority of the entry</param>
        /// <param name="severity">Severity of the entry</param>
        /// <param name="message">additional message</param>
        public void WriteAppSessionEnd(Priority priority, Severity severity, string message)
        {
            WriteAppSessionEnd(LogManagerConstants.EVENTID_APPEND, priority, severity, string.Empty, string.Empty, string.Empty, string.Empty, message, new string[0]);
        }
        /// <summary>
        /// Writes a new application session end entry
        /// </summary>
        /// <param name="priority">Priority of the entry</param>
        /// <param name="severity">Severity of the entry</param>
        /// <param name="message">additional message</param>
        /// <param name="categories">string array with log category names</param>
        public void WriteAppSessionEnd(Priority priority, Severity severity, string message, string[] categories)
        {
            WriteAppSessionEnd(LogManagerConstants.EVENTID_APPEND, priority, severity, string.Empty, string.Empty, string.Empty, string.Empty, message, categories);
        }
        /// <summary>
        /// Writes a new application session end entry
        /// </summary>
        /// <param name="priority">Priority of the entry</param>
        /// <param name="severity">Severity of the entry</param>
        /// <param name="appName">application name</param>
        /// <param name="appType">application type</param>
        /// <param name="appVersion">application version</param>
        /// <param name="installMode">installation mode</param>
        public void WriteAppSessionEnd(Priority priority, Severity severity, string appName, string appType, string appVersion, string installMode)
        {
            WriteAppSessionEnd(LogManagerConstants.EVENTID_APPEND, priority, severity, appName, appType, appVersion, installMode, string.Empty, new string[0]);
        }
        /// <summary>
        /// Writes a new application session end entry
        /// </summary>
        /// <param name="priority">Priority of the entry</param>
        /// <param name="severity">Severity of the entry</param>
        /// <param name="appName">application name</param>
        /// <param name="appType">application type</param>
        /// <param name="appVersion">application version</param>
        /// <param name="installMode">installation mode</param>
        /// <param name="categories">string array with log category names</param>
        public void WriteAppSessionEnd(Priority priority, Severity severity, string appName, string appType, string appVersion, string installMode, string[] categories)
        {
            WriteAppSessionEnd(LogManagerConstants.EVENTID_APPEND, priority, severity, appName, appType, appVersion, installMode, string.Empty, categories);
        }
        /// <summary>
        /// Writes a new application session end entry
        /// </summary>
        /// <param name="priority">Priority of the entry</param>
        /// <param name="severity">Severity of the entry</param>
        /// <param name="appName">application name</param>
        /// <param name="appType">application type</param>
        /// <param name="appVersion">application version</param>
        /// <param name="installMode">installation mode</param>
        /// <param name="message">additional message</param>
        public void WriteAppSessionEnd(Priority priority, Severity severity, string appName, string appType, string appVersion, string installMode, string message)
        {
            WriteAppSessionEnd(LogManagerConstants.EVENTID_APPEND, priority, severity, appName, appType, appVersion, installMode, message, new string[0]);
        }
        /// <summary>
        /// Writes a new application session end entry
        /// </summary>
        /// <param name="priority">Priority of the entry</param>
        /// <param name="severity">Severity of the entry</param>
        /// <param name="appName">application name</param>
        /// <param name="appType">application type</param>
        /// <param name="appVersion">application version</param>
        /// <param name="installMode">installation mode</param>
        /// <param name="message">additional message</param>
        /// <param name="categories">string array with log category names</param>
        public void WriteAppSessionEnd(Priority priority, Severity severity, string appName, string appType, string appVersion, string installMode, string message, string[] categories)
        {
            WriteAppSessionEnd(LogManagerConstants.EVENTID_APPEND, priority, severity, appName, appType, appVersion, installMode, message, categories);
        }

        /// <summary>
        /// Writes a new application session end entry
        /// </summary>
        /// <param name="eventId">Event Id (use LogManagerConstants.EVENTID_APPEND)</param>
        /// <param name="severity">Severity of the entry</param>
        /// <param name="message">additional message</param>
        public void WriteAppSessionEnd(long eventId, Severity severity, string message)
        {
            WriteAppSessionEnd(eventId, Priority.Normal, severity, string.Empty, string.Empty, string.Empty, string.Empty, message, new string[0]);
        }
        /// <summary>
        /// Writes a new application session end entry
        /// </summary>
        /// <param name="eventId">Event Id (use LogManagerConstants.EVENTID_APPEND)</param>
        /// <param name="severity">Severity of the entry</param>
        /// <param name="message">additional message</param>
        /// <param name="categories">string array with log category names</param>
        public void WriteAppSessionEnd(long eventId, Severity severity, string message, string[] categories)
        {
            WriteAppSessionEnd(eventId, Priority.Normal, severity, string.Empty, string.Empty, string.Empty, string.Empty, message, categories);
        }
        /// <summary>
        /// Writes a new application session end entry
        /// </summary>
        /// <param name="eventId">Event Id (use LogManagerConstants.EVENTID_APPEND)</param>
        /// <param name="severity">Severity of the entry</param>
        /// <param name="appName">application name</param>
        /// <param name="appType">appliction type</param>
        /// <param name="appVersion">application version</param>
        /// <param name="installMode">installation mode</param>
        public void WriteAppSessionEnd(long eventId, Severity severity, string appName, string appType, string appVersion, string installMode)
        {
            WriteAppSessionEnd(eventId, Priority.Normal, severity, appName, appType, appVersion, installMode, string.Empty, new string[0]);
        }
        /// <summary>
        /// Writes a new application session end entry
        /// </summary>
        /// <param name="eventId">Event Id (use LogManagerConstants.EVENTID_APPEND)</param>
        /// <param name="severity">Severity of the entry</param>
        /// <param name="appName">application name</param>
        /// <param name="appType">appliction type</param>
        /// <param name="appVersion">application version</param>
        /// <param name="installMode">installation mode</param>
        /// <param name="categories">string array with log category names</param>
        public void WriteAppSessionEnd(long eventId, Severity severity, string appName, string appType, string appVersion, string installMode, string[] categories)
        {
            WriteAppSessionEnd(eventId, Priority.Normal, severity, appName, appType, appVersion, installMode, string.Empty, categories);
        }

        /// <summary>
        /// Writes a new application session end entry
        /// </summary>
        /// <param name="eventId">Event Id (use LogManagerConstants.EVENTID_APPEND)</param>
        /// <param name="priority">Priority of the entry</param>
        /// <param name="message">additional message</param>
        public void WriteAppSessionEnd(long eventId, Priority priority, string message)
        {
            WriteAppSessionEnd(eventId, priority, Severity.Information, string.Empty, string.Empty, string.Empty, string.Empty, message, new string[0]);
        }
        /// <summary>
        /// Writes a new application session end entry
        /// </summary>
        /// <param name="eventId">Event Id (use LogManagerConstants.EVENTID_APPEND)</param>
        /// <param name="priority">Priority of the entry</param>
        /// <param name="message">additional message</param>
        /// <param name="categories">string array with log category names</param>
        public void WriteAppSessionEnd(long eventId, Priority priority, string message, string[] categories)
        {
            WriteAppSessionEnd(eventId, priority, Severity.Information, string.Empty, string.Empty, string.Empty, string.Empty, message, categories);
        }
        /// <summary>
        /// Writes a new application session end entry
        /// </summary>
        /// <param name="eventId">Event Id (use LogManagerConstants.EVENTID_APPEND)</param>
        /// <param name="priority">Priority of the entry</param>
        /// <param name="appName">application name</param>
        /// <param name="appType">application type</param>
        /// <param name="appVersion">application version</param>
        /// <param name="installMode">installation mode</param>
        public void WriteAppSessionEnd(long eventId, Priority priority, string appName, string appType, string appVersion, string installMode)
        {
            WriteAppSessionEnd(eventId, priority, Severity.Information, appName, appType, appVersion, installMode, string.Empty, new string[0]);
        }
        /// <summary>
        /// Writes a new application session end entry
        /// </summary>
        /// <param name="eventId">Event Id (use LogManagerConstants.EVENTID_APPEND)</param>
        /// <param name="priority">Priority of the entry</param>
        /// <param name="appName">application name</param>
        /// <param name="appType">application type</param>
        /// <param name="appVersion">application version</param>
        /// <param name="installMode">installation mode</param>
        /// <param name="categories">string array with log category names</param>
        public void WriteAppSessionEnd(long eventId, Priority priority, string appName, string appType, string appVersion, string installMode, string[] categories)
        {
            WriteAppSessionEnd(eventId, priority, Severity.Information, appName, appType, appVersion, installMode, string.Empty, categories);
        }

        /// <summary>
        /// Writes a new application session end entry
        /// </summary>
        /// <param name="eventId">Event Id (use LogManagerConstants.EVENTID_APPEND)</param>
        /// <param name="priority">Priority of the entry</param>
        /// <param name="severity">Severity of the entry</param>
        /// <param name="message">additional message</param>
        public void WriteAppSessionEnd(long eventId, Priority priority, Severity severity, string message)
        {
            WriteAppSessionEnd(eventId, priority, severity, string.Empty, string.Empty, string.Empty, string.Empty, message, new string[0]);
        }
        /// <summary>
        /// Writes a new application session end entry
        /// </summary>
        /// <param name="eventId">Event Id (use LogManagerConstants.EVENTID_APPEND)</param>
        /// <param name="priority">Priority of the entry</param>
        /// <param name="severity">Severity of the entry</param>
        /// <param name="message">additional message</param>
        /// <param name="categories">string array with log category names</param>
        public void WriteAppSessionEnd(long eventId, Priority priority, Severity severity, string message, string[] categories)
        {
            WriteAppSessionEnd(eventId, priority, severity, string.Empty, string.Empty, string.Empty, string.Empty, message, categories);
        }
        /// <summary>
        /// Writes a new application session end entry
        /// </summary>
        /// <param name="eventId">Event Id (use LogManagerConstants.EVENTID_APPEND)</param>
        /// <param name="priority">Priority of the entry</param>
        /// <param name="severity">Severity of the entry</param>
        /// <param name="appName">application name</param>
        /// <param name="appType">application type</param>
        /// <param name="appVersion">application version</param>
        /// <param name="installMode">installation mode</param>
        public void WriteAppSessionEnd(long eventId, Priority priority, Severity severity, string appName, string appType, string appVersion, string installMode)
        {
            WriteAppSessionEnd(eventId, priority, severity, appName, appType, appVersion, installMode, string.Empty, new string[0]);
        }
        /// <summary>
        /// Writes a new application session end entry
        /// </summary>
        /// <param name="eventId">Event Id (use LogManagerConstants.EVENTID_APPEND)</param>
        /// <param name="priority">Priority of the entry</param>
        /// <param name="severity">Severity of the entry</param>
        /// <param name="appName">application name</param>
        /// <param name="appType">application type</param>
        /// <param name="appVersion">application version</param>
        /// <param name="installMode">installation mode</param>
        /// <param name="categories">string array with log category names</param>
        public void WriteAppSessionEnd(long eventId, Priority priority, Severity severity, string appName, string appType, string appVersion, string installMode, string[] categories)
        {
            WriteAppSessionEnd(eventId, priority, severity, appName, appType, appVersion, installMode, string.Empty, categories);
        }
        /// <summary>
        /// Writes a new application session end entry
        /// </summary>
        /// <param name="eventId">Event Id (use LogManagerConstants.EVENTID_APPEND)</param>
        /// <param name="priority">Priority of the entry</param>
        /// <param name="severity">Severity of the entry</param>
        /// <param name="appName">application name</param>
        /// <param name="appType">application type</param>
        /// <param name="appVersion">application version</param>
        /// <param name="installMode">installation mode</param>
        /// <param name="message">additional message</param>
        public void WriteAppSessionEnd(long eventId, Priority priority, Severity severity, string appName, string appType, string appVersion, string installMode, string message)
        {
            WriteAppSessionEnd(eventId, priority, severity, appName, appType, appVersion, installMode, message, new string[0]);
        }
        /// <summary>
        /// Writes a nes application session start entry
        /// </summary>
        /// <param name="eventId">Event Id (use LogManagerConstants.EVENTID_APPSTARTUP)</param>
        /// <param name="priority">Priority of the entry</param>
        /// <param name="severity">Severity of the entry</param>
        /// <param name="appName">application name</param>
        /// <param name="appType">application type</param>
        /// <param name="appVersion">application version</param>
        /// <param name="installMode">installation mode</param>
        /// <param name="message">additional message</param>
        /// <param name="categories">string array with log category names</param>
        public void WriteAppSessionEnd(long eventId, Priority priority, Severity severity, string appName, string appType, string appVersion, string installMode, string message, string[] categories)
        {
            // create new log entry data object
            AppSessionLogEntry asLogEntry = new AppSessionLogEntry();

            // union the standard categories and the category name array
            asLogEntry.UnattachedCategtories = StringArrayUnion(new string[] { LogManagerConstants.SYSCATEGORY_APPSESSION }, categories);

            // manually tell the log worker thread to repopulate properties
            asLogEntry.RequiresDataRefresh = true;

            // basic properties
            asLogEntry.ProductName = ProductName;
            asLogEntry.EventId = eventId;
            asLogEntry.Priority = priority;
            asLogEntry.Severity = severity;
            asLogEntry.SessionId = _application_session_guid.ToString();
            asLogEntry.Message = message;
            // activity properties
            object _old_scope = _activity_scope;

            if (_activity_scope == null)
                _activity_scope = Thread.CurrentThread;

            asLogEntry.ActivityId = ApplicationStatisticHelper.GetActivityId(_activity_scope).ToString();
            asLogEntry.ActivityDescription = ApplicationStatisticHelper.GetActivityDescription(_activity_scope);
            asLogEntry.RelatedActivityId = ApplicationStatisticHelper.GetRelatedActivityId(_activity_scope).ToString();

            _activity_scope = _old_scope;

            #region Populate log specifc information
            asLogEntry.IsStartEntry = false;
            #endregion

            // put the log entry into the buffer queue
            lock (_log_buffer)
            {
                _log_buffer.Add(new LogEntryDataItem(asLogEntry));
                _work_todo = true;
            }

            _application_session_guid = Guid.Empty;
        }
        #endregion

        #endregion

        #region Submit changes to database logic
        /// <summary>
        /// Submits the current changeset to the database and resovles conflicts if possible
        /// </summary>
        private bool SubmitChangedLoggingDataContext(LoggingDataContext submitDC)
        {
            // save the date and time of the current submission attempt
            _last_submission = DateTime.Now;

            bool bSucceed = false;

            bool conflictsSolved = false;

            //TransactionScope transActionScope = new TransactionScope();

            try
            {
                _max_submit_try_counter++;
#if DEBUG
                Debug.WriteLine("*** LogManager.SubmitChangedLoggingDataContext()");
                Debug.WriteLine(string.Format("*** Submit try {0}/{1}", _max_submit_try_counter, _max_submit_tries));
#endif

                submitDC.SubmitChanges(ConflictMode.ContinueOnConflict);

                bSucceed = true;

                Debug.WriteLine("*** Submit SUCESSFULL!");
            } 
            catch(ChangeConflictException ccEx)
            {
#if DEBUG
                Debug.Indent();
                Debug.WriteLine("Optimistic concurrency error. (" + ccEx.Message + ")");
                Debug.WriteLine(" ");
                Debug.Unindent();
#endif
                // check if the current change conflicts are related to the Categories
                // since we are simplyadding category entities on every application load
                foreach (ObjectChangeConflict occ in submitDC.ChangeConflicts)
                {
                    // get the meta table of the change conflict
                    MetaTable metaTable = submitDC.Mapping.GetTable(occ.GetType());
                    Category categoryInConflict = occ.Object as Category;


#if DEBUG
                    Debug.Indent();
                    Debug.WriteLine("Change conflict information");
                    Debug.Indent();
                    Debug.WriteLine("Meta table: " + metaTable.TableName);
                    Debug.WriteLine("Conflict object type: " + occ.Object.GetType().ToString());
                    Debug.Unindent();
                    Debug.Unindent();
#endif

                    foreach (MemberChangeConflict mcc in occ.MemberConflicts)
                    {
                        object currVal = mcc.CurrentValue;
                        object origVal = mcc.OriginalValue;
                        object databaseVal = mcc.DatabaseValue;
                        MemberInfo mi = mcc.Member;

#if DEBUG
                        Debug.Indent();
                        Debug.Indent();
                        Debug.WriteLine("Member change conflict information");
                        Debug.Indent();
                        Debug.WriteLine("Member: " + mi.Name);
                        Debug.WriteLine("Current value: " + currVal==null ? "null" : currVal.ToString());
                        Debug.WriteLine("Original value: " + origVal == null ? "null" : origVal.ToString());
                        Debug.WriteLine("Database value: " + databaseVal == null ? "null" : databaseVal.ToString());
                        Debug.Unindent();
                        Debug.Unindent();
                        Debug.Unindent();
#endif
                    }
                }

                // rethrow exception since it isn't an expected one
                //throw ccEx;
                return false;
            }
            catch(SqlException sqlEx)
            {
                if (sqlEx.Number == 2627 && sqlEx.Message.IndexOf("PRIMARY KEY")>=0 ) // primary key violation error
                {
                    // get the meta table object for the Category class
                    MetaTable metaTable = submitDC.Mapping.GetTable(typeof(Category));

                    // if the table name is part of the error message
                    if(sqlEx.Message.IndexOf(metaTable.TableName) >= 0)
                    {

#if DEBUG
                        Debug.Indent();
                        Debug.WriteLine("PRIMARY KEY violation error.");
                        Debug.Indent();
                        Debug.WriteLine("Error Number: 2627");
                        Debug.WriteLine("Table name: " + metaTable.TableName);
                        Debug.WriteLine("Trying to resolve ... ");
                        Debug.WriteLine(" ");
                        Debug.Unindent();
                        Debug.Unindent();
#endif
                        // we have a primary key violation on the category table

                        try
                        {
                            // expected change conflict
                            // cannot add a second category row with the same product name and category name
                            List<Category> cats = submitDC.Categories.ToList();
                            ChangeSet changeSet = submitDC.GetChangeSet();

                            List<PropertyInfo> primaryKeyFields = LinqLib.Reflection.LinqReflectionHelper.GetPrimaryKeys(typeof(Category));

                            foreach (object oinsertCat in changeSet.Inserts)
                            {
                                if (oinsertCat is Category)
                                {
                                    // try to cast the current insert object to a category
                                    Category insertCat = oinsertCat as Category;

                                    if (insertCat != null)
                                    {
                                        foreach (Category dcCat in cats) //submitDC.Categories)
                                        {
                                            // mark the conflicting entity as DeleteOnSubmit()
                                            if (LinqLib.Reflection.LinqReflectionHelper.ComparePrimaryKeyFields(insertCat, dcCat, primaryKeyFields))
                                            {
                                                submitDC.Categories.DeleteOnSubmit(dcCat);
                                                conflictsSolved = true;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        catch (SqlException sqlEx1)
                        {
#if DEBUG
                            Debug.Indent();
                            Debug.WriteLine("Unexpected SQL exception.");
                            Debug.WriteLine(sqlEx1.Message);
                            Debug.Unindent();
#endif
                            // error with the underlying sql connection - maybe connection loss
                            // returning false will giv the logging system space to recover later
                            return false;
                        }

#if DEBUG
                        if (conflictsSolved)
                        {
                            Debug.Indent();
                            Debug.Indent();
                            Debug.WriteLine(" ... conflicts resolved. Will resubmit changes.");
                            Debug.WriteLine(" ");
                            Debug.Unindent();
                            Debug.Unindent();
                        }
#endif
                    } 
                    else
                    {
#if DEBUG
                        Debug.Indent();
                        Debug.WriteLine("Unexpected PRIMARY KEY - SQL exception.");
                        Debug.WriteLine(sqlEx.Message);
                        Debug.Unindent();
#endif
                        //throw sqlEx;
                        return false;
                    }
                } 
                else
                {
#if DEBUG
                    Debug.Indent();
                    Debug.WriteLine("Unexpected SQL exception.");
                    Debug.WriteLine(sqlEx.Message);
                    Debug.Unindent();
#endif      
                    //throw sqlEx;
                    return false;
                }
            }
            catch(DuplicateKeyException dkEx)
            {
                // this Linq Exception occures if we have marked an entity as InsertOnSubmit()
                // with the same primary key of another one already attached to the data context
                // to resolve this error - remove this entity from the Inters list by calling
                // DeleteOnSubmit()

                if(dkEx.Object is Category)
                {
#if DEBUG
                    Debug.Indent();
                    Debug.WriteLine("LinQ duplicate key violation.");
                    Debug.Indent();
                    Debug.WriteLine("Type name " + dkEx.Object.GetType());
                    Debug.WriteLine("Trying to resolve ... ");
                    Debug.WriteLine(" ");
                    Debug.Unindent();
                    Debug.Unindent();
#endif
                    submitDC.Categories.DeleteOnSubmit(dkEx.Object as Category);
                    conflictsSolved = true;

                    // decrement the max try counter here
                    // the reason is, that we will get an extra exception vor every
                    // object in the context with a duplicate key
                    // it could be that the number of exceptions becomes very high
                    // so that's why we reset the counter back

                    // the only exception her is, if we are trying to resolve a conflict
                    // on an object more than once

                    if(!_submit_duplicate_key_conflict_solved.Contains(dkEx.Object))
                    {
                        _max_submit_try_counter--;
                        _submit_duplicate_key_conflict_solved.Add(dkEx.Object);
                    }

#if DEBUG
                    Debug.Indent();
                    Debug.Indent();
                    Debug.WriteLine(" ... conflicts resolved. Will resubmit changes.");
                    Debug.WriteLine(" ");
                    Debug.Unindent();
                    Debug.Unindent();
#endif
                }
                else
                {
#if DEBUG
                    Debug.Indent();
                    Debug.WriteLine("Unexpected Linq DuplicateKey exception.");
                    Debug.WriteLine(dkEx.Message);
                    Debug.Unindent();
#endif
                    //throw dkEx;
                    return false;
                }
            }
            catch(Exception ex)
            {
#if DEBUG
                Debug.Indent();
                Debug.WriteLine("Unexpected exception.");
                Debug.WriteLine(ex.Message);
                Debug.Unindent();
#endif                
            }
            finally
            {
                // if conflicts have been solved without any additional exception
                if(conflictsSolved)
                {
                    // try to submit the changes again if the max try counter was not reached
                    if (_max_submit_try_counter < _max_submit_tries)
                    {
                        //if (_data_context.Connection != null)
                        //    _data_context.Connection.Close();
                        bSucceed = SubmitChangedLoggingDataContext(submitDC);
                    }
                }
            }

            return bSucceed;
        }
        #endregion

        #region Data context connection
        /// <summary>
        /// Attempt to connect to the data context asnyc
        /// </summary>
        private void ConnectDataContextAsync()
        {
            // if a connection attempt is already running
            if (_bw_data_connection != null)
                return;

            _bw_data_connection = new QueuedBackgroundWorker();
            _bw_data_connection.Name = "LogManager: AyncConnection Thread";
            _bw_data_connection.DoWork += new QueuedBackgroundWorker.DoWorkEventHandler(bw_data_connection_DoWork);
            _bw_data_connection.RunWorkerCompleted += new QueuedBackgroundWorker.RunWorkerCompletedEventHandler(bw_data_connection_RunWorkerCompleted);
            _bw_data_connection.RunWorkerAsync();
        }
        /// <summary>
        /// Async connection attempt completed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bw_data_connection_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                if (_data_context_state == LoggingDataConnectionState.Online)
                {
                    if(_data_context_connecting_first_time)
                    {
                        if(File.Exists(SerializationFilePath))
                        {
                            FileInfo fi = new FileInfo(SerializationFilePath);

                            if(fi.Length > 0 )
                            {
                                if(_data_context != null)
                                    lock (_data_context)
                                    {
                                        try
                                        {
                                            // tell the background logger that we are currently deserializing the main data context
                                            // to recover from a previous disconnected application shutdown
                                            // We're not allowed to change the ChangeSet during this action
                                            // otherwise Exceptions will be thrown.
                                            _is_change_state_deserializing = true;

                                            ChangeSetSerializer.DeserializeFromFile(SerializationFilePath, _data_context);
                                            // if sucessfully recreated the changeset from the file
                                            // clear the file contents
                                            StreamWriter writer = File.CreateText(SerializationFilePath);
                                            writer.Write("");
                                            writer.Close();
                                        }
                                        catch
                                        {
                                            if (File.Exists(SerializationFilePath))
                                                File.Delete(SerializationFilePath);
                                        }
                                        finally
                                        {
                                            _is_change_state_deserializing = false;
                                        }
                                    }
                            }
                        }
                        _data_context_connecting_first_time = false;
                    }
                } 
            }
            catch
            {
               
            }
            finally
            {
                _bw_data_connection = null;
            }
        }
        /// <summary>
        /// Async connection attempt - do work
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bw_data_connection_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                _data_context_state = LoggingDataConnectionState.Connecting;

                if (!_data_context.DatabaseExists())
                    throw new Exception();

                _data_context_state = LoggingDataConnectionState.Online;
            } 
            catch(Exception ex)
            {
                _data_context_state = LoggingDataConnectionState.Offline;

                throw new Exception(string.Format(Properties.Resources.SQLConnectionError, _connectionString.Split(";".ToCharArray(), 2)[0]), ex);
            }
        }
        #endregion

        #region Logger background worker
        /// <summary>
        /// Async connection attempt completed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bw_logger_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
        }
        /// <summary>
        /// Do background logging. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bw_logger_DoWork(object sender, DoWorkEventArgs e)
        {
            int bufferCnt = 0;
            bool bTrySubmit = false;

            // do work until terminate signal from main thread
            // or if 2 or less pending log entries are in the queue (these will be finished)
            while (!_cancel_signal_logger || (bufferCnt <= 2 && bufferCnt > 0))
            {
                if ((_work_todo)&&(!_is_change_state_deserializing))
                {
                    bTrySubmit = false;
                    LogEntryDataItem leData = null;
                    LogEntry le = null;

                    lock (_log_buffer)
                    {
                        bufferCnt = _log_buffer.Count;
                        leData = _log_buffer.First();
                        le = (LogEntry)leData.Data;
                    }

                    if(le != null)
                    {
                        if(le.RequiresDataRefresh)
                        {
#if DEBUG
                            Debug.WriteLine(string.Format("*** LOGGING: Background data refresh of {0}, Event {1}. ", le.Diskriminator, le.EventId));
#endif

                            // fill standard properties to the log entry
                            // like timestamp, machine name, etc.
                            PupulateStandardProperties(le);

                            // if the log entry is an app message log entry
                            // fill additional process information
                            if (le is AppMessageLogEntry)
                            {
                                PupulateAppMessageProperties(le as AppMessageLogEntry);
                            }

                            if(le is SystemInfoLogEntry)
                            {
                                PopulateSystemInfoMessageProperties(le as SystemInfoLogEntry);
                            }

                            ApplyLogFormatter(le);

                            le.RequiresDataRefresh = false;
                        }

#if DEBUG
                        Debug.WriteLine(string.Format("*** LOGGING: Applying Filter for {0}, Event {1}. ", le.Diskriminator, le.EventId));
#endif
                        // apply the log filter against the log entry
                        if(_log_filter.ApplyFilter(le))
                        {
#if DEBUG
                            Debug.WriteLine(string.Format("***           Filter run passed "));
#endif
                            DataFinalizerBase dataFinalizer = _finalizer_activator.GetFinalizerForType(le.GetType());

                            if (dataFinalizer != null)
                            {
                                if(_data_context != null)
                                    lock (_data_context)
                                    {
                                        dataFinalizer.BeforeInsertItemInDataContext(le, _data_context);
                                        _log_error_counter += dataFinalizer.ChangesMade;
                                    }
                            }

                            // if this log entry should be saved
                            // insert it in the current data context
                            if(_data_context != null)
                                lock (_data_context)
                                {
                                    _data_context.LogEntries.InsertOnSubmit(le);
                                }
                            _log_error_counter++;

                            //ChangeSet csCheck = _data_context.GetChangeSet();
                            //int nPendingChanges = csCheck.Inserts.Count + csCheck.Updates.Count + csCheck.Deletes.Count;
                            
                            if (_log_error_counter > Settings.NumberOfChangesRequiredForDatabaseUpdate)
                            {
                                if ((_log_error_sub_counter > 0 && _log_error_sub_counter >= (Settings.NumberOfChangesRequiredAfterFailedUpdate > 0 ? Settings.NumberOfChangesRequiredAfterFailedUpdate : Settings.NumberOfChangesRequiredForDatabaseUpdate)) ||
                                _log_error_sub_counter == 0)
                                {
                                    bTrySubmit = true;
                                }
                                else
                                {
                                    _log_error_sub_counter++;
                                }
                            }
                        }
                        else
                        {
#if DEBUG
                            Debug.WriteLine(string.Format("***           Filter run FAILED "));
#endif
                        }

                        // remove the log entry from the todo list
                        lock (_log_buffer)
                        {
                            _log_buffer.Remove(leData);
                            _work_todo = _log_buffer.Count > 0;
                            bufferCnt = _log_buffer.Count;
                        }
                    }
                }
                else
                {
                    Thread.Sleep(200); // avoid 100% CU usage
                    lock (_log_buffer)
                    {
                        _work_todo = _log_buffer.Count > 0;
                        bufferCnt = _log_buffer.Count;
                    }

                    #region submit changes back to database if necessary

                    if(_data_context != null)
                        lock (_data_context)
                        {
                            ChangeSet csData = _data_context.GetChangeSet();
                            bool updatesPending = false;

                            updatesPending = csData.Inserts.Count > 0;
                            updatesPending |= csData.Updates.Count > 0;
                            updatesPending |= csData.Deletes.Count > 0;

                            if (updatesPending) // only try to submit if changes are present
                            {
                                try
                                {
                                    TimeSpan lastSubmissionTime = DateTime.Now - _last_submission;
                                    bool bSubmitDueTime = false;

                                    if (_time_submission_trigger_minutes > 0)
                                    {
                                        if (lastSubmissionTime.TotalMinutes >= _time_submission_trigger_minutes)
                                            bSubmitDueTime = true;
                                    }
                                    // check if a submitchanges is required
                                    if (bTrySubmit || bSubmitDueTime)
                                    {
                                        // avoid permanent submission tries due to offline work or non existent network
                                        _last_submission = DateTime.Now;

                                        if (_data_context.DatabaseExists())
                                        {
                                            // reset submit try counter
                                            _max_submit_try_counter = 0;
                                            if (_submit_duplicate_key_conflict_solved != null)
                                            {
                                                _submit_duplicate_key_conflict_solved.Clear();
                                                _submit_duplicate_key_conflict_solved = null;
                                            }
                                            _submit_duplicate_key_conflict_solved = new List<object>();

                                            bool bSubmitSucceed = false;
                                            if(_data_context != null)
                                                lock (_data_context)
                                                {
                                                    try
                                                    {
                                                        // serialize change set to a file
                                                        ChangeSetSerializer.SerializetoFile(_data_context, SerializationFilePath, true);
                                                    }
                                                    catch
                                                    {
                                                        if (File.Exists(SerializationFilePath))
                                                            File.Delete(SerializationFilePath);
                                                    }

                                                    LoggingDataContext tmpDataContext = new LoggingDataContext(_connectionString);
                                                    tmpDataContext.LoadOptions = CreateDataLoadOptions();

                                                    try
                                                    {
                                                        // deserialize changes from file
                                                        ChangeSetSerializer.DeserializeFromFile(SerializationFilePath, tmpDataContext);
                                                        // if sucessfully recreated the changeset from the file
                                                        // clear the file contents
                                                        StreamWriter writer = File.CreateText(SerializationFilePath);
                                                        writer.Write("");
                                                        writer.Close();

                                                        //using (TransactionScope trans = new TransactionScope())
                                                        {
                                                            bSubmitSucceed = SubmitChangedLoggingDataContext(tmpDataContext);

                                                            if (bSubmitSucceed)
                                                            {
                                                                //trans.Complete();
                                                                _data_context = new LoggingDataContext(_connectionString);
                                                                _data_context.LoadOptions = CreateDataLoadOptions();
                                                            }
                                                        }
                                                    }
                                                    catch
                                                    {
                                                        if (File.Exists(SerializationFilePath))
                                                            File.Delete(SerializationFilePath);
                                                    }

                                                    if (bSubmitSucceed)
                                                    {
                                                        // call all data finalizers
                                                        foreach (DataFinalizerBase curdataFinalizer in _finalizer_activator.FinalizerList)
                                                        {
                                                            curdataFinalizer.AfterSubmitChanges(_data_context);
                                                        }

                                                        ChangeSet csTmp = _data_context.GetChangeSet();
                                                        bool bUpdates = false;

                                                        bUpdates |= csTmp.Inserts.Count > 0;
                                                        bUpdates |= csTmp.Updates.Count > 0;
                                                        bUpdates |= csTmp.Deletes.Count > 0;

                                                        // if one of the data finalizers made an update after their execution
                                                        if (bUpdates)
                                                        {
                                                            try
                                                            {
                                                                // serialize change set to a file
                                                                ChangeSetSerializer.SerializetoFile(_data_context, SerializationFilePath, true);
                                                            }
                                                            catch
                                                            {
                                                                if (File.Exists(SerializationFilePath))
                                                                    File.Delete(SerializationFilePath);
                                                            }

                                                            tmpDataContext = new LoggingDataContext(_connectionString);
                                                            tmpDataContext.LoadOptions = CreateDataLoadOptions();

                                                            try
                                                            {
                                                                // deserialize changes from file
                                                                ChangeSetSerializer.DeserializeFromFile(SerializationFilePath, tmpDataContext);
                                                                // if sucessfully recreated the changeset from the file
                                                                // clear the file contents
                                                                StreamWriter writer = File.CreateText(SerializationFilePath);
                                                                writer.Write("");
                                                                writer.Close();

                                                                // resubmit changes
                                                                //using (TransactionScope trans = new TransactionScope())
                                                                {
                                                                    bSubmitSucceed = SubmitChangedLoggingDataContext(tmpDataContext);

                                                                    if (bSubmitSucceed)
                                                                    {
                                                                        //trans.Complete();
                                                                        _data_context = new LoggingDataContext(_connectionString);
                                                                        _data_context.LoadOptions = CreateDataLoadOptions();
                                                                    }
                                                                }
                                                            }
                                                            catch
                                                            {
                                                                if (File.Exists(SerializationFilePath))
                                                                    File.Delete(SerializationFilePath);
                                                            }
                                                        }
                                                    }
                                                }

                                            _log_error_sub_counter = 0;

                                            if (bSubmitSucceed)
                                            {
                                                _log_error_counter = 0;
                                            }
                                            else
                                            {
                                                _log_error_sub_counter++;
                                            }
                                        }
                                        else
                                        {
                                            if (_log_error_sub_counter == 0)
                                                _log_error_sub_counter++;
                                            else
                                                _log_error_sub_counter = 0; // reset sub counter if we tried to update the data in the database
                                        }
                                    }
                                }
                                finally
                                {
                                    bTrySubmit = false;
                                }
                            }
                        }

                    #endregion
                }
            }
        }

        /// <summary>
        /// Searches a log formatter and applies it to the current
        /// log entry
        /// </summary>
        /// <param name="entry"></param>
        private void ApplyLogFormatter(LogEntry entry)
        {
            ILogFormatter standardFormatter = _formatter_activator.GetStandardFormatter();
            ILogFormatter specificFormatter = _formatter_activator.GetFormatterForType(entry.GetType());

            if(specificFormatter != null)
            {
                entry.FormattedMessage = specificFormatter.FormatLogEntry(entry);
            }
            else if (standardFormatter != null)
            {
                entry.FormattedMessage = standardFormatter.FormatLogEntry(entry);
            }
        }

        /// <summary>
        /// Fills log entry with the standard properties for every log entry
        /// </summary>
        /// <param name="entry"></param>
        private void PupulateStandardProperties(LogEntry entry)
        {
            if( entry.UnattachedCategtories != null  && entry.UnattachedCategtories.Length > 0)
            {
                // attach entry to categories
                AttachLogEntryToCategories(entry, entry.UnattachedCategtories);
                entry.UnattachedCategtories = new string[0];
            }

            // product name
            entry.ProductName = ProductName;
            // log entry timestamp
            entry.LogTimestamp = ApplicationStatisticHelper.TimeStamp;
            // machine name
            entry.MachineName = ApplicationStatisticHelper.MachineName;
            // name of the currently logged on user
            entry.DomainLoginName = ApplicationStatisticHelper.CurrentUserName;
        }
        /// <summary>
        /// Fills the app message log entry with additional process information
        /// </summary>
        /// <param name="entry"></param>
        private void PupulateAppMessageProperties(AppMessageLogEntry entry)
        {
            // name of the current application domain
            entry.AppDomainName = ApplicationStatisticHelper.AppDomainName;
            // id of the current process
            entry.ProcedssId = ApplicationStatisticHelper.ProcessId;
            // name of the current process
            entry.ProcessName = ApplicationStatisticHelper.ProcessName;
            // name of the thread
            entry.ThreadName = ApplicationStatisticHelper.ThreadName;
            // id of the thread
            entry.Win32ThreadId = ApplicationStatisticHelper.Win32ThreadId;
        }

        /// <summary>
        /// Fills the system info log entry with data
        /// </summary>
        /// <param name="entry"></param>
        private void PopulateSystemInfoMessageProperties(SystemInfoLogEntry entry)
        {
            // collect hardware and system information
            SystemInfoCollector hwi = new SystemInfoCollector();

            entry.OperatingSystem = hwi.OperatingSystem;
            entry.SerivcePackInfo = hwi.ServicePackInfo;
            entry.FreeSysDiskSpace = hwi.FreeSysDiskSpace;
            entry.CPU_Speed = hwi.CpuSpeed.ToString();
            entry.CPU_Type = hwi.CpuType;
            entry.CPU_ID = hwi.CpuId;
            entry.Total_RAM = hwi.TotalRAM;
            entry.Available_RAM = hwi.AvailableRAM;
            entry.PC_Modell = hwi.PCModel;
            entry.PC_Manufacturer = hwi.PCManufacturer;
            entry.SoundCard = hwi.SoundCard;
            entry.NIC_1 = hwi.NIC1;
            entry.NIC_2 = hwi.NIC2;
            entry.NIC_3 = hwi.NIC3;
            entry.NIC_4 = hwi.NIC4;
            entry.VGACard = hwi.VideoController;
            entry.LogonServer = hwi.LogonServer;
            entry.Domain = hwi.UserDomain;
            entry.NumberPhysicalCPU = hwi.NumberOfPhysicalCPU;
            entry.NumberLogicalCPU = hwi.NumberOfLogicalCPU;
            entry.NumberCPUCore = hwi.NumerOfProcessorCores;
            entry.PagefileSize = hwi.PageFileSize;
            entry.SerialNumber = hwi.BiosSerialNumber;

            // installed software
            SoftwareCollector swI = new SoftwareCollector();

            foreach(SoftwareCollector.SoftwareItem software in swI.SoftwareItems)
            {
                // apply software scan filters
                if (_software_scan_filter.ApplyFilter(software))
                {
                    InstalledSoftware instSW = new InstalledSoftware();
                    instSW.Title = software.Title;
                    instSW.Version = software.Version;
                    instSW.Publisher = software.Publisher;

                    entry.InstalledSoftwares.Add(instSW);
                }
            }

            // sql server
            SQLServerCollector sqlSrv = new SQLServerCollector();

            foreach(SQLServerCollector.SQLServerItem sqlServer in sqlSrv.SQLInstances)
            {
                SQLServer sqlData = new SQLServer();
                sqlData.InstanceName = sqlServer.InstanceName;
                sqlData.InstanceStatus = sqlServer.InstanceStatus;
                sqlData.Licensemode = sqlServer.LicenseMode;
                sqlData.BinPath = sqlServer.BinPath;
                sqlData.Clustered = sqlServer.Clustered;
                sqlData.Edition = sqlServer.Edition;
                sqlData.FileVersion = sqlServer.FileVersion;
                sqlData.ServiceAccount = sqlServer.ServiceAccount;
                sqlData.StartMode = sqlServer.StartMode;
                sqlData.Version = sqlServer.Version;

                entry.SQLServers.Add(sqlData);
            }
        }

        #endregion

        #region Log buffer serialization/deserialization
        /// <summary>
        /// Serializes the log buffer of this instance to the log buffer file
        /// </summary>
        private void SerializeLogBuffer()
        {
            StreamWriter writer = File.CreateText(LogBufferSerializationFilePath);

            XmlSerializer serializer = new XmlSerializer(typeof(LogManager));
            serializer.Serialize(writer, (LogManager)this);

            writer.Close();
        }
        /// <summary>
        /// Deserializes the log buffer from the log buffer file and appends the 
        /// log entries from the file to the internal log buffer
        /// </summary>
        /// <returns>true if sucessfull</returns>
        private bool DeserializeLogBuffer()
        {
            // if the file doesn't exist return a null value
            if (!File.Exists(LogBufferSerializationFilePath))
                return true;

            try
            {
                // deserialize the file into a new log manager instance
                StreamReader reader = File.OpenText(LogBufferSerializationFilePath);

                XmlSerializer serializer = new XmlSerializer(typeof (LogManager));
                LogManager data = (LogManager) serializer.Deserialize(reader);

                reader.Close();

                // copy the log entries from the deserialized log buffer to the 
                // current buffer
                if(data.LogEntryBuffer != null && data.LogEntryBuffer.Count() > 0)
                {
                    foreach (LogEntryDataItem curEntry in data.LogEntryBuffer)
                        _log_buffer.Add(curEntry);
                }

                // clear the contents of the buffer log
                // to avoid multiple appends
                StreamWriter writer = File.CreateText(LogBufferSerializationFilePath);
                writer.Write("");
                writer.Close();
            }
            catch(Exception ex)
            {
#if DEBUG
                Debug.Indent();
                Debug.WriteLine("Error deserializing log buffer.");
                Debug.WriteLine(string.Format("Error: {0}", ex.Message));
                Debug.Unindent();
#endif
                return false;
            }

            return true;
        }
        #endregion

        #region Setter Methods
        public void SetConnectionString(string conn_string)
        {
            if (_connectionString == conn_string)
                return;


            if (_data_context != null)
            {

                try
                {
                    // serialize change set to a file
                    ChangeSetSerializer.SerializetoFile(_data_context, SerializationFilePath, true);
                }
                catch
                {
                    if (File.Exists(SerializationFilePath))
                        File.Delete(SerializationFilePath);
                }

                if (_data_context.Connection.State == System.Data.ConnectionState.Open)
                    _data_context.Connection.Close();

                _data_context.Dispose();
                _data_context = null;
                _connectionString = conn_string;

                LoggingDataContext tmpDataContext = new LoggingDataContext(_connectionString);
                tmpDataContext.LoadOptions = CreateDataLoadOptions();

                try
                {
                    // deserialize changes from file
                    ChangeSetSerializer.DeserializeFromFile(SerializationFilePath, tmpDataContext);
                    // if sucessfully recreated the changeset from the file
                    // clear the file contents
                    StreamWriter writer = File.CreateText(SerializationFilePath);
                    writer.Write("");
                    writer.Close();

                    // resubmit changes
                    //using (TransactionScope trans = new TransactionScope())
                    {
                        bool bSubmitSucceed = SubmitChangedLoggingDataContext(tmpDataContext);

                        if (bSubmitSucceed)
                        {
                            //trans.Complete();
                            _data_context = new LoggingDataContext(_connectionString);
                            //_data_context.Log = Console.Out;
                            _data_context.LoadOptions = CreateDataLoadOptions();
                        }
                    }
                }
                catch
                {
                    if (File.Exists(SerializationFilePath))
                        File.Delete(SerializationFilePath);
                }
            }
            else
            {
                _connectionString = conn_string;
                _data_context = new LoggingDataContext(_connectionString);
                _data_context.Log = Console.Out;
                _data_context.LoadOptions = CreateDataLoadOptions();
            }
        }
        #endregion

        #endregion

        #region Static Methods
        /// <summary>
        /// String array union
        /// </summary>
        /// <param name="arr1"></param>
        /// <param name="arr2"></param>
        /// <returns></returns>
        private static string[] StringArrayUnion(string[] arr1, string[] arr2)
        {
            List<string> l1 = new List<string>(arr1);
            List<string> l2 = new List<string>(arr2);

            return l1.Union(arr2).ToArray<string>();
        }

        /// <summary>
        /// Takes an exception object and returns a formated string with exception information
        /// </summary>
        /// <param name="ex">Exception to get information from</param>
        /// <returns>a string containing the exception description</returns>
        public static string FormatException(Exception ex)
        {
            return FormatException("", ex, false, false);
        }

        /// <summary>
        /// Takes an exception object and returns a formated string with exception information
        /// </summary>
        /// <param name="infoMessage">A info message prefix for the exception data</param>
        /// <param name="ex">Exception to get information from</param>
        /// <returns>a string containing the exception description</returns>
        public static string FormatException(string infoMessage, Exception ex)
        {
            return FormatException(infoMessage, ex, false, false);
        }

        /// <summary>
        /// Takes an exception object and returns a formated string with exception information
        /// </summary>
        /// <param name="infoMessage">A info message prefix for the exception data</param>
        /// <param name="ex">Exception to get information from</param>
        /// <param name="includeInnerExceptions">True if inner exceptions should be included</param>
        /// <returns>a string containing the exception description</returns>
        public static string FormatException(string infoMessage, Exception ex, bool includeInnerExceptions)
        {
            return FormatException(infoMessage, ex, includeInnerExceptions, false);
        }
        /// <summary>
        /// Takes an exception object and returns a formated string with exception information
        /// </summary>
        /// <param name="infoMessage">A info message prefix for the exception data</param>
        /// <param name="ex">Exception to get information from</param>
        /// <param name="includeInnerExceptions">True if inner exceptions should be included</param>
        /// <param name="includeStackTrace">true if the stack trace should be included</param>
        /// <returns>a string containing the exception description</returns>
        public static string FormatException(string infoMessage, Exception ex, bool includeInnerExceptions, bool includeStackTrace )
        {
            StringBuilder sbRet = new StringBuilder(7168);

            Exception tmpEx = ex;

            if(!string.IsNullOrEmpty(infoMessage))
                sbRet.Append(string.Format("{0}\n", infoMessage));

            if (includeInnerExceptions)
            {
                while(tmpEx != null)
                {
                    sbRet.Append(string.Format("Type: {0}\n", tmpEx.GetType().Name));
                    sbRet.Append(string.Format("Source: {0}\n", tmpEx.Source));
                    sbRet.Append(string.Format("Message: {0}\n", tmpEx.Message));
                    if (includeStackTrace)
                    {
                        sbRet.Append(string.Format("Stacktrace: {0}\n", tmpEx.StackTrace));
                    }

                    sbRet.Append("\n");

                    tmpEx = tmpEx.InnerException;
                }
            }
            else
            {
                if (tmpEx != null)
                {
                    sbRet.Append(string.Format("Type: {0}\n", tmpEx.GetType().Name));
                    sbRet.Append(string.Format("Source: {0}\n", tmpEx.Source));
                    sbRet.Append(string.Format("Message: {0}\n", tmpEx.Message));
                    if(includeStackTrace)
                    {
                        sbRet.Append(string.Format("Stacktrace: {0}\n", tmpEx.StackTrace));    
                    }

                    sbRet.Append("\n");
                }
            }

            return sbRet.ToString();
        }
        #endregion

        #region Properties
        public object ActivityScope
        {
            get { return _activity_scope; }
            set { _activity_scope = value; }
        }

        /// <summary>
        /// Gets a flag indicating if the log manager has been initialized
        /// </summary>
        public bool IsInitialized
        {
            get { return _initialized; }
        }

        /// <summary>
        /// Gets the current log manager's connection string
        /// </summary>
        [XmlIgnore]
        public string ConnectionString
        {
            get { return _connectionString; }
        }

        /// <summary>
        /// Gets the full path to the serialization file
        /// </summary>
        public string SerializationFilePath
        {
            get { return _serializationFile; }
        }

        /// <summary>
        /// Gets the full path and file name of the internal log entry buffer file
        /// </summary>
        public string LogBufferSerializationFilePath
        {
            get
            {
                if (string.IsNullOrEmpty(_log_buffer_serialization_filename))
                {
                    _log_buffer_serialization_filename = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), Settings.ProductName + "\\logbuffer.xml");

                    if (!Directory.Exists(Path.GetDirectoryName(_log_buffer_serialization_filename)))
                        Directory.CreateDirectory(Path.GetDirectoryName(_log_buffer_serialization_filename));
                }

                return _log_buffer_serialization_filename;
            }
        }

        /// <summary>
        /// Gets/Sets the internal list of the log buffer
        /// </summary>
        [XmlArray]
        public List<LogEntryDataItem> LogEntryBuffer
        {
            get { return _log_buffer; }
            set { _log_buffer = value; }
        }
        #endregion

        #region Static Properties
        /// <summary>
        /// Gets the current instance of the LogManager or create a new one if none exists
        /// </summary>
        public static LogManager Current
        {
            get
            {
                if(_current == null)
                    new LogManager();

                return _current;
            }
        }

        /// <summary>
        /// Gets a reference to the logging settings
        /// </summary>
        public static LoggingConfigSection Settings
        {
            get
            {
                if (_settings == null)
                {
                    _settings = ConfigurationManager.GetSection("LoggingSettings") as LoggingConfigSection;

                    if (_settings == null)
                        _settings = new LoggingConfigSection();
                }

                return _settings;
            }
        }

        /// <summary>
        /// Gets/Sets the product name which is using the log manager class.
        /// </summary>
        /// <remarks>The product name is a static-scoped name which cannot vary between different instances of the LogManager 
        /// within the same application domain.</remarks>
        public static string ProductName
        {
            get
            {
                return Settings.ProductName;
            }
            set
            {
                if(!string.IsNullOrEmpty(value))
                    Settings.ProductName = value;
            }
        }

        [XmlIgnore]
        public static List<string> ErrorLogFileNames
        {
            get
            {
                string error_log_path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), Settings.ProductName );

                List<string> fileNames = new List<string>();

                if(Directory.Exists(error_log_path))
                {
                    string[] allfiles = Directory.GetFiles(error_log_path);

                    foreach(string curfile in allfiles)
                    {
                        if (curfile.EndsWith("error.log"))
                            fileNames.Add(curfile);
                        else if (curfile.EndsWith(".log") && curfile.IndexOf("error_") >= 0)
                            fileNames.Add(curfile);
                    }
                }

                return fileNames;
            }
        }
        #endregion

        #region Attribs
        private static LogManager _current = null;
        private static LoggingConfigSection _settings = ConfigurationManager.GetSection("LoggingSettings") as LoggingConfigSection;

        private bool _initialized = false;
        private string _connectionString = "";
        private string _serializationFile = "";
        private string _log_buffer_serialization_filename = "";

        private LoggingDataContext _data_context = null;
        private bool _data_context_connecting_first_time = true;
        private LoggingDataConnectionState _data_context_state = LoggingDataConnectionState.Offline;
        private int _max_submit_try_counter = 0;
        private int _max_submit_tries = 10;   // maximal number of tries to submit changes if change conflicts are present
        private List<object> _submit_duplicate_key_conflict_solved = null;
        private long _log_error_counter = 0;
        private long _log_error_sub_counter = 0;

        private DateTime _last_submission = DateTime.MinValue;
        private int _time_submission_trigger_minutes = 10;

        private LogFilter _log_filter = null;
        private SoftwareScanFilter _software_scan_filter = null;
        private DataFinalizerActivator _finalizer_activator = null;
        private LogFormatterActivator _formatter_activator = null;
        private Guid _application_session_guid = Guid.Empty;

        private List<LogEntryDataItem> _log_buffer = new List<LogEntryDataItem>();
        private bool _work_todo = false; // set this to true if log entries are in the log buffer

        private bool _cancel_signal_logger = false;
        private QueuedBackgroundWorker _bw_data_connection = null;
        private QueuedBackgroundWorker _bw_logger = null;

        private bool _is_change_state_deserializing = false;

        private EventLog _event_log = null;

        private object _activity_scope = null;
        #endregion

        #region Constants

        public const long MAXERROR_FILE_SIZE = 2097152; // 2MB
        #endregion
    }
}
