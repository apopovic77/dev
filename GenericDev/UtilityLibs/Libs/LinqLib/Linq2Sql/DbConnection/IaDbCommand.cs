﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace LinqLib.Linq2Sql.DbConnection
{
    public class IaDbCommand : DbCommand
    {
        #region construction and initialization
        public IaDbCommand()
        {
            _internal_command = new SqlCommand();
        }

        public IaDbCommand(string cmdText)
        {
            _internal_command = new SqlCommand(cmdText);
        }

        public IaDbCommand(string cmdText, IaDbConnection connection)
        {
            _internal_connection = connection;
            _internal_command = new SqlCommand(cmdText, connection.SqlConnection);
        }

        public IaDbCommand(string cmdText, IaDbConnection connection, SqlTransaction transaction)
        {
            _internal_connection = connection;
            _internal_command = new SqlCommand(cmdText, connection.SqlConnection, transaction);
        }
        #endregion

        #region Operations

        #region pooling support
        internal void SwitchToPooledConnection()
        {
            if (_internal_connection != null && _internal_connection is PooledIaDbConnection)
            {
                // try switching to a free connection in the pool
                PooledIaDbConnection pooledCon = _internal_connection as PooledIaDbConnection;

                IaDbConnection private_con = pooledCon.RequestPooledConnection(true);

                if (private_con != pooledCon)
                {
                    private_con.LockContext("SwitchToPooledConnection");

                    try
                    {
                        // switch internal connection context to the new pooled connection
                        _pooled_connection = pooledCon;
                        _internal_connection = private_con;
                        _internal_command.Connection = private_con.SqlConnection;
                    }
                    finally
                    {
                        private_con.FreeContext("SwitchToPooledConnection");
                    }
                }
            }
        }

        internal void SwitchFromPooledConnection()
        {
            if(_pooled_connection != null)
            {
                IaDbConnection private_pooled = _internal_connection;
                private_pooled.LockContext("SwitchFromPooledConnection");
                try
                {
                    _pooled_connection.ReleasePooledConnection(_internal_connection);
                    _internal_connection = private_pooled;
                    _internal_command.Connection = private_pooled.SqlConnection;
                }
                finally
                {
                    private_pooled.FreeContext("SwitchFromPooledConnection");
                }

            }
        }
        #endregion

        public override void Cancel()
        {
            _internal_command.Cancel();
        }

        protected override DbParameter CreateDbParameter()
        {
            return _internal_command.CreateParameter();
        }

        public override System.Runtime.Remoting.ObjRef CreateObjRef(Type requestedType)
        {
            return _internal_command.CreateObjRef(requestedType);
        }

        protected override void Dispose(bool disposing)
        {
            _internal_command.Dispose();
            base.Dispose(disposing);
        }

        protected override DbDataReader ExecuteDbDataReader(System.Data.CommandBehavior behavior)
        {
            SwitchToPooledConnection();

            _internal_connection.LockContext("ExecuteDbDataReader");
            IaDbDataReader reader = null;

            try
            {
#if DEBUG
                if (WithDebugOutput)
                {
                    System.Diagnostics.Debug.WriteLine(_internal_connection.DebugIdentifier + DebugIdentifier + "##### ExecuteDbDataReader() - IaDbCommand");
                    System.Diagnostics.Debug.WriteLine(_internal_connection.DebugIdentifier + DebugIdentifier + "######## Hash: " + this.GetHashCode().ToString() + ", Thread: " + System.Threading.Thread.CurrentThread.Name);
                }
#endif

                _internal_connection.EnsureOpenConnection();

                SqlDataReader sqlreader = _internal_command.ExecuteReader(behavior);

                if (sqlreader == null || sqlreader.IsClosed)
                    _internal_connection.FreeContext("ExecuteDbDataReader[IsClosed]");
                else
                {
                    reader = new IaDbDataReader(sqlreader, this);
                    //reader = DynamicDataReaderTypeFactory.GetThreadDependentReaderInstance(sqlreader, this);
#if DEBUG
                    if (WithDebugOutput)
                    {
                        System.Diagnostics.Debug.WriteLine(_internal_connection.DebugIdentifier + DebugIdentifier + "######## Reader allocated: " + reader.GetHashCode().ToString() + ", Thread: " + System.Threading.Thread.CurrentThread.Name);
                        //System.Diagnostics.Debug.WriteLine(_internal_connection.DebugIdentifier + DebugIdentifier + "######## Reader type name: " + reader.GetType().ToString() + ", ThreadID: " + System.Threading.Thread.CurrentThread.GetHashCode().ToString());
                    }
#endif
                }

            }
            catch(Exception ex)
            {
#if DEBUG
                if(WithDebugOutput)
                {
                    System.Diagnostics.Debug.WriteLine(_internal_connection.DebugIdentifier + DebugIdentifier + "##### ERROR ExecuteReader EXCEPTION - Forcing to free lock, Thread: " + System.Threading.Thread.CurrentThread.Name);
                    System.Diagnostics.Debug.WriteLine(ex.ToString());
                }
#endif
                _internal_connection.FreeContext("ExecuteDbDataReader Exception");
                throw;
            }
            finally
            {
                //_internal_connection.FreeContext();
            }

            return reader;
        }

        public override int ExecuteNonQuery()
        {
            SwitchToPooledConnection();

            _internal_connection.LockContext("ExecuteNonQuery");
            int nRet = 0;

            try
            {
#if DEBUG
                if (WithDebugOutput)
                {
                    System.Diagnostics.Debug.WriteLine(_internal_connection.DebugIdentifier + DebugIdentifier + "##### ExecuteNonQuery() - IaDbCommand");
                    System.Diagnostics.Debug.WriteLine(_internal_connection.DebugIdentifier + DebugIdentifier + "######## Hash: " + this.GetHashCode().ToString() + ", Thread: " + System.Threading.Thread.CurrentThread.Name);
                }
#endif

                _internal_connection.EnsureOpenConnection();
                nRet = _internal_command.ExecuteNonQuery();
            }
            finally
            {
                _internal_connection.FreeContext("ExecuteNonQuery");
                SwitchFromPooledConnection();
            }

            return nRet;
        }

        public override object ExecuteScalar()
        {
            SwitchToPooledConnection();
            _internal_connection.LockContext("ExecuteScalar");
            object oRet = null;

            try
            {
#if DEBUG
                if (WithDebugOutput)
                {
                    System.Diagnostics.Debug.WriteLine(_internal_connection.DebugIdentifier + DebugIdentifier + "##### ExecuteScalar() - IaDbCommand");
                    System.Diagnostics.Debug.WriteLine(_internal_connection.DebugIdentifier + DebugIdentifier + "######## Hash: " + this.GetHashCode().ToString() + ", Thread: " + System.Threading.Thread.CurrentThread.Name);
                }
#endif

                _internal_connection.EnsureOpenConnection();
                oRet = _internal_command.ExecuteScalar();
            }
            finally
            {
                _internal_connection.FreeContext("ExecuteScalar");
                SwitchFromPooledConnection();
            }

            return oRet;
        }

        public override void Prepare()
        {
#if DEBUG
            if (WithDebugOutput)
            {
                System.Diagnostics.Debug.WriteLine(_internal_connection.DebugIdentifier + DebugIdentifier + "##### Prepare() - IaDbCommand");
                System.Diagnostics.Debug.WriteLine(_internal_connection.DebugIdentifier + DebugIdentifier + "######## Hash: " + this.GetHashCode().ToString() + ", Thread: " + System.Threading.Thread.CurrentThread.Name);
            }
#endif

            _internal_connection.EnsureOpenConnection();
            _internal_command.Prepare();
        }
        #endregion

        #region Properties

        public virtual string DebugIdentifier
        {
            get
            {
                return "###M" + this.GetHashCode().ToString();
            }
        }

        public IaDbConnection IaDbConnection
        {
            get { return _internal_connection; }
        }

        public PooledIaDbConnection PooledIaDbConnection
        {
            get { return _pooled_connection; }
        }

        public SqlCommand SqlCommand
        {
            get { return _internal_command; }
        }

        public override string CommandText
        {
            get
            {
                return _internal_command.CommandText;
            }
            set
            {
                _internal_command.CommandText = value;
            }
        }

        public override int CommandTimeout
        {
            get
            {
                return _internal_command.CommandTimeout;
            }
            set
            {
                _internal_command.CommandTimeout = value;
            }
        }

        public override System.Data.CommandType CommandType
        {
            get
            {
                return _internal_command.CommandType;
            }
            set
            {
                _internal_command.CommandType = value;
            }
        }

        protected override System.Data.Common.DbConnection DbConnection
        {
            get
            {
                return _internal_connection;
            }
            set
            {
                _internal_connection = value as IaDbConnection;
            }
        }

        protected override DbParameterCollection DbParameterCollection
        {
            get { return _internal_command.Parameters; }
        }

        protected override DbTransaction DbTransaction
        {
            get
            {
                return _internal_transaction; // _internal_command.Transaction;
            }
            set
            {
                _internal_transaction = value as IaDbTransaction;
                if(_internal_transaction != null)
                {
                    _internal_command.Transaction = _internal_transaction.SqlTransaction;
                }
                else
                {
                    _internal_command.Transaction = null;
                }
                //_internal_command.Transaction = (SqlTransaction)value;
            }
        }

        public override bool DesignTimeVisible
        {
            get
            {
                return _internal_command.DesignTimeVisible;
            }
            set
            {
                _internal_command.DesignTimeVisible = value;
            }
        }

        public override System.Data.UpdateRowSource UpdatedRowSource
        {
            get
            {
                return _internal_command.UpdatedRowSource;
            }
            set
            {
                _internal_command.UpdatedRowSource = value;
            }
        }

#if DEBUG
        public bool WithDebugOutput
        {
            get { return _internal_connection.WithDebugOutput; }
            set { _internal_connection.WithDebugOutput = value; }
        }
#endif

        #endregion

        #region Attribs
        private SqlCommand _internal_command = null;
        private IaDbConnection _internal_connection = null;
        private PooledIaDbConnection _pooled_connection = null;
        private IaDbTransaction _internal_transaction = null;
        #endregion
    }
}
