﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Xml.Linq;
using CognitiveLib.ApiService;
using FSBlog.GoogleSearch.GoogleClient;
using Google.Apis.Services;

namespace CognitiveLib.WebSearch
{
    public class GeizhalsSearch
    {
        #region Search Suggestion
        /// <summary>
        /// The Google Suggest search URL.
        /// </summary>
        /// <remarks>
        /// Add gl=dk for Google Denmark. Add lr=lang_da for danish results. Add hl=da to indicate the language of the UI making the request.
        /// </remarks>
        private const string _suggestSearchUrl = "https://geizhals.at/?";

        /// <summary>
        /// Gets the search suggestions from Google.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <returns>A list of <see cref="GoogleSuggestion"/>s.</returns>
        public static async Task<List<string>> GetSearchResult(string query)
        {
            if (String.IsNullOrWhiteSpace(query))
            {
                throw new ArgumentException("Argument cannot be null or empty!", "query");
            }

            string url = _suggestSearchUrl;
            url += HttpUtility.UrlEncode(query) + "&in=";

            string result = String.Empty;

            using (HttpClient client = new HttpClient())
            {
                result = await client.GetStringAsync(String.Format(_suggestSearchUrl, url));
            }

            XDocument doc = XDocument.Parse(result);
            List<string> suggestions = new List<string>(10);
            foreach (XElement completesugg in doc.Document.Descendants("CompleteSuggestion"))
            {
                string s = completesugg.Element("suggestion")?.Attribute("data")?.Value;
                if (string.IsNullOrEmpty(s))
                    suggestions.Add(s);
            }
            return suggestions;

            //var suggestions = from suggestion in doc.Document?.Descendants("CompleteSuggestion")
            //    select new GoogleSuggestion
            //    {
            //        Phrase = suggestion.Element("suggestion")?.Attribute("data")?.Value
            //    };

            //return suggestions.ToList();
        }
        #endregion

        public static IEnumerable<SearchResultHit> Search(string query)
        {
            // init client
            var client = new SearchClient(query);
            return client.Query();
        }
    }
}