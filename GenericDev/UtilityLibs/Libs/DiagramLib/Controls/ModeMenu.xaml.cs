﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Logicx.DiagramLib.Controls
{
    /// <summary>
    /// Interaction logic for ModeMenu.xaml
    /// </summary>
    public partial class ModeMenu : UserControl
    {
        public ModeMenu()
        {
            InitializeComponent();
        }

        #region Operations
        private void RaiseButtonClick()
        {
            if (ButtonClick != null)
                ButtonClick(this, new EventArgs());
        }
        #endregion

        #region Event Handlers
        private void mehrfachauswahl_btn_Click(object sender, RoutedEventArgs e)
        {
            e.Handled = true;
            Debug.WriteLine("Mehrfach Auswahl clicked");
            //mehrfach auswahl
            if (MehrfachAuswahlClick != null)
                MehrfachAuswahlClick(sender, e);

            RaiseButtonClick();
        }

        private void reset_zoom_btn_Click(object sender, RoutedEventArgs e)
        {
            e.Handled = true;
            Debug.WriteLine("Reset Zoom clicked");
            //reset zom
            if (ResetZoomClick != null)
                ResetZoomClick(sender, e);

            RaiseButtonClick();
        }
        #endregion

        #region Dependency Properties



        public Brush StrokeBrush
        {
            get { return (Brush)GetValue(StrokeBrushProperty); }
            set { SetValue(StrokeBrushProperty, value); }
        }

        // Using a DependencyProperty as the backing store for StrokeBrush.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty StrokeBrushProperty =
            DependencyProperty.Register("StrokeBrush", typeof(Brush), typeof(ModeMenu), new UIPropertyMetadata(Brushes.Blue));



        public Brush BackgroundBrush
        {
            get { return (Brush)GetValue(BackgroundBrushProperty); }
            set { SetValue(BackgroundBrushProperty, value); }
        }

        // Using a DependencyProperty as the backing store for BackgroundBrush.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty BackgroundBrushProperty =
            DependencyProperty.Register("BackgroundBrush", typeof(Brush), typeof(ModeMenu), new UIPropertyMetadata(Brushes.Black));



        public Brush LineBrush
        {
            get { return (Brush)GetValue(LineBrushProperty); }
            set { SetValue(LineBrushProperty, value); }
        }

        // Using a DependencyProperty as the backing store for LineBrush.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty LineBrushProperty =
            DependencyProperty.Register("LineBrush", typeof(Brush), typeof(ModeMenu), new UIPropertyMetadata(Brushes.White));


        public bool IsZoomActive
        {
            get { return (bool)GetValue(IsZoomActiveProperty); }
            set { SetValue(IsZoomActiveProperty, value); }
        }

        // Using a DependencyProperty as the backing store for LineBrush.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsZoomActiveProperty =
            DependencyProperty.Register("IsZoomActive", typeof(bool), typeof(ModeMenu), new UIPropertyMetadata(false));


        public bool IsMehrfachAuswahlActive
        {
            get { return (bool)GetValue(IsMehrfachAuswahlActiveProperty); }
            set { SetValue(IsMehrfachAuswahlActiveProperty, value); }
        }

        // Using a DependencyProperty as the backing store for LineBrush.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsMehrfachAuswahlActiveProperty =
            DependencyProperty.Register("IsMehrfachAuswahlActive", typeof(bool), typeof(ModeMenu), new UIPropertyMetadata(false));


        public BitmapEffect TextGlowBitmapEffect
        {
            get { return (BitmapEffect)GetValue(TextGlowBitmapEffectProperty); }
            set { SetValue(TextGlowBitmapEffectProperty, value); }
        }

        // Using a DependencyProperty as the backing store for LineBrush.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TextGlowBitmapEffectProperty =
            DependencyProperty.Register("TextGlowBitmapEffect", typeof(BitmapEffect), typeof(ModeMenu), new UIPropertyMetadata(null));
        #endregion

        #region Events
        public event EventHandler ButtonClick;

        public event EventHandler ResetZoomClick;
        public event EventHandler MehrfachAuswahlClick;
        #endregion
    }
}
