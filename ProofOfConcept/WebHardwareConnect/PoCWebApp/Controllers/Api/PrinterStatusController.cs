﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using PoCShared;
using PoCWebApp.App_Start;
using PoCWebApp.Config;
using PoCWebApp.Data;

namespace PoCWebApp.Controllers.Api
{
    public class PrinterStatusController : ApiController
    {
        // GET api/<controller>
        public IEnumerable<PrinterStatusItem> Get(string printServer = null)
        {
            using (var context = new PoCPrinterContext())
            {
                if (!string.IsNullOrEmpty(printServer))
                    return context.Printers.Where(p => string.Equals(p.PrintServer, printServer, StringComparison.InvariantCultureIgnoreCase))
                        .MapTo<PrinterStatusItem>(AutoMapperConfig.Mapper)
                        .ToList();

                return context.Printers
                    .MapTo<PrinterStatusItem>(AutoMapperConfig.Mapper)
                    .ToList();
            }
        }

        // GET api/<controller>/5
        public PrinterStatusItem Get(int id)
        {
            using (var context = new PoCPrinterContext())
            {
                return context.Printers.FirstOrDefault(p => p.Id == id)
                     .MapTo<PrinterStatusItem>(AutoMapperConfig.Mapper);
            }
        }

        // POST api/<controller>
        public void Post([FromBody]PrinterStatusItem value)
        {
            throw new InvalidOperationException();
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]PrinterStatusItem value)
        {
            using (var context = new PoCPrinterContext())
            {
                var existing = context.Printers.FirstOrDefault(p => p.Name == value.Name && p.PrintServer == value.PrintServer);
                if (existing != null)
                {
                    existing.Status = value.Status;
                    context.SaveChanges();
                }
            }
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
            throw new InvalidOperationException();
        }
    }
}