using System;
using System.Collections;
using System.Collections.Generic;

namespace MathLib
{
	/// <summary>
	/// y = k * x + d
	/// </summary>
	public class Regressionsgerade
	{
		protected Vector2d[] vertices;
		
		/// <summary>
		/// die steigung der geraden
		/// k = s_xy / s_x^2
		/// </summary>
		public double k;
		/// <summary>
		/// die konstante f(x) = y = d  <=> x = 0
		/// </summary>
		public double d;

		public double y_mittel;
		public double x_mittel;
		public double s_x2;
		public double s_xy;


		public Regressionsgerade(Vector2d[] vertices){
			Init(vertices);
		}	
        
		public Regressionsgerade(ArrayList vertices)
		{
			Init(Vector2d.ArrayList2VectorArray(vertices));
		}

        public Regressionsgerade(List<Vector2f> vertices)
        {
            Vector2d[] arr = new Vector2d[vertices.Count];
            for (int i = 0; i < vertices.Count; i++)
                arr[i] = (Vector2d)vertices[i];

            Init(arr);
        }

        public Vector2d GetOrthogonalVec()
        {
            return GetRichtungsVec().GetOrthogonal1();
        }

        public Vector2d GetRichtungsVec()
        {
            Vector2d p1 = new Vector2d(vertices[0].X, GetY(vertices[0].X));
            Vector2d p2 = new Vector2d(vertices[vertices.Length - 1].X, GetY(vertices[vertices.Length - 1].X));

            return (p2 - p1);
        }

		protected void Init(Vector2d[] vertices){
			this.vertices = MakeOneDimensional(vertices);
			y_mittel = GetYMittel();
			x_mittel = GetXMittel();
			s_x2 = GetSx2();
			s_xy = GetSxy();
			k = s_xy/s_x2;
			d = y_mittel - (s_xy/s_x2) * x_mittel;
		}

		public double GetY(double x){
			return k * x + d;
		}

		private double GetXMittel()
		{
			double x_mittel = 0;
			for(int k=0;k < vertices.Length;k++)
				x_mittel += vertices[k].X;
			x_mittel = x_mittel / vertices.Length;
			return x_mittel;
		}

		private double GetYMittel()
		{
			double y_mittel = 0;
			for(int k=0;k < vertices.Length;k++)
				y_mittel += vertices[k].Y;
			y_mittel = y_mittel / vertices.Length;
			return y_mittel;
		}

		private double GetSxy()
		{
			double res = 0;
			for(int k=0;k < vertices.Length;k++)
				res += (vertices[k].X-x_mittel)*(vertices[k].Y-y_mittel);
			res = res / vertices.Length;
			return res;
		}

		private double GetSx2()
		{
			double res = 0;
			for(int k=0;k < vertices.Length;k++)
				res += Math.Pow((vertices[k].X - x_mittel),2);
			res = res / vertices.Length;
			return res;
		}

		private Vector2d[] MakeOneDimensional(Vector2d[] vertices)
		{

			ArrayList result = new ArrayList();
			for(int i=0;i<vertices.Length;i++){
				Vector2d to_check = vertices[i];
				bool is_unique = true;
				ArrayList non_unique_verticesonX = new ArrayList();
				non_unique_verticesonX.Add(to_check);
				for(int j=0;j<vertices.Length;j++)
				{
					if(i!=j){
						Vector2d v1 = vertices[i];
						Vector2d v2 = vertices[j];
						if(v1.X == v2.X){
							non_unique_verticesonX.Add(v2);
							is_unique = false;
						}
					}					
				}
				if(is_unique)
				{
					result.Add(to_check);
				}
				else
				{
					double sum = 0;
					for(int k=0;k < non_unique_verticesonX.Count;k++){
						sum += ((Vector2d)non_unique_verticesonX[k]).Y;
					}
					result.Add(new Vector2d(((Vector2d)non_unique_verticesonX[0]).X,sum/non_unique_verticesonX.Count));
				}
			}
			if(result.Count <= 1)
                throw new Exception("Cannot create regessionsgerade as it is vertical");

			ArrayList remove_list = new ArrayList();
			for(int i=0;i<result.Count;i++)
			{
				for(int j=0;j<result.Count;j++)
				{
					if(i!=j)
					{
						Vector2d v1 = (Vector2d)result[i];
						Vector2d v2 = (Vector2d)result[j];
						if(v1.X == v2.X){
							if(IsNotIn(v1,remove_list))
								remove_list.Add(v1);						
						}
					}
				}
			}
			for(int i=0;i<remove_list.Count;i++)
				result.Remove(remove_list[i]);
			result.Sort(new VectorSortXAsc());

			return Vector2d.ArrayList2VectorArray(result);
		}

		private bool IsNotIn(Vector2d v1, ArrayList remove_list)
		{
			for(int i=0;i<remove_list.Count;i++){
				Vector2d v2 = (Vector2d)remove_list[i];
				if(v2 == v1)
					return false;
			}
			return true;
		}
	}
}
