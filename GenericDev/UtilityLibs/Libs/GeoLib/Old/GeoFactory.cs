using System;
using MathLib;


namespace GeoLib
{
	/// <summary>
	/// 
	/// </summary>
	public class GeoFactory
	{
		/// <summary>
		/// umweg kalkulationen
		/// </summary>
		public class DetourCalculation{

            private static int GetWegIndex(int i, bool wegline_in_reverse_order, int weglen) {
                if (!wegline_in_reverse_order)
                    return i;
                int i_reverse = weglen - 1 - i;
                if (i_reverse < 0) throw new IndexOutOfRangeException();
                return i_reverse;
            }


			/// <summary>
			/// berechnet den umweg �ber ein abstraktes mass
			/// dabei wird auch der fehler der durhc die strecken diskretisierung gemacht wird ber�cksichtigt
			/// auswirkung bzw. die gr�sse des fehler kann �ber den parameter error_proz definiert werden
			/// </summary>
			/// <param name="xs">vertex der den abfahrts punkt start punkt beschreibt</param>
			/// <param name="xe">vertex der den entpunkt des umweges spezifiziert</param>
			/// <param name="weg">route die normaler weise gefahren wird</param>
            /// <param name="wegline_in_reverse_order">entspricht der �bergebene weg der fahrt richtiung, oder der gegenfahrt richtung, wenn ja also wenn der weg der fahrtrichung entspricht dann soll dieser bool auf alse gesetzt werden</param>             
			/// <param name="error_proz">der error der dazugerechnet wird kann in gr�sse hier variert werden</param>
			/// <param name="umweg">umweg in abstraktem mass</param>
			/// <param name="gesamtweg">der gesamtweg + err der normalen route ohne umweg</param>
			/// <param name="umweg_prozent">umweg in prozent im vergleich zur gesamtstrecke ohne umweg</param>
            /// <param name="b_liegt_in_gegenrichtung">true -  es is in die b_liegt_in_gegenrichtung gefahren werden</param>
            /// <param name="entfernung_xs_von_wegstart">gibt die entferung des gegeben startknotens an, (der start konten xs definiert den punkt der als sicht des fahres angefahren werden muss)</param>
            /// <param name="entfernung_xs_von_wegstart_prozent">der obere parameter in % zur gesamt weg strecke</param>
            public static void GetUmweg(Vector2d xs, Vector2d xe, Line weg, bool wegline_in_reverse_order,double error_proz, out double umweg, out double gesamtweg, out double umweg_prozent, out bool b_liegt_in_gegenrichtung, out double entfernung_xs_von_wegstart, out double entfernung_xs_von_wegstart_prozent)
			{

				//BERECHNUNG DER ORTH.VECTOREN ZU XS UND XE
                int weglen = weg.Length;
                b_liegt_in_gegenrichtung = false; //initaliesier default m�ssig auf nein, und setz dann sp�ter unten auf ja wenn herausgefunden wird das doch in b_liegt_in_gegenrichtung gefahren werden muss
                entfernung_xs_von_wegstart = 0;
                double entfernung_xe_von_wegstart = 0;

				double c = 0;
				Vector2d t_vec;
				int xs_shortest_knoten = 0;
                Vector2d xs_shortest_umweg = xs - weg[GetWegIndex(0, wegline_in_reverse_order, weglen)];
				Vector2d xs_shortest_wegabzweigung = new Vector2d(0,0);
				double xs_len_shortest_umweg = xs_shortest_umweg.GetLen();

				int xe_shortest_knoten = 0;
                Vector2d xe_shortest_umweg = xe - weg[GetWegIndex(0, wegline_in_reverse_order, weglen)];
				Vector2d xe_shortest_wegabzweigung = new Vector2d(0,0);
				double xe_len_shortest_umweg = xe_shortest_umweg.GetLen();


				for(int i=0;i<weg.Length-1;i++)
				{
                    Vector2d wegteil = weg[GetWegIndex(i + 1, wegline_in_reverse_order, weglen)] - weg[GetWegIndex(i, wegline_in_reverse_order, weglen)];
					double wegteil_len = wegteil.GetLen();
                    t_vec = xs - weg[GetWegIndex(i, wegline_in_reverse_order, weglen)];
					c = t_vec.GetSkalarProduct(wegteil) / wegteil.GetSkalarProduct();
					double curr_wegteil_len = (wegteil * c).GetLen();
                    if (c > 0 && curr_wegteil_len <= wegteil_len)
                    {
                        Vector2d v_wegbisabzweigung = wegteil * c;
                        Vector2d v_umweg = t_vec - v_wegbisabzweigung;
                        double v_umweg_len = v_umweg.GetLen();
                        if (v_umweg_len < xs_len_shortest_umweg)
                        {
                            xs_shortest_umweg = v_umweg;
                            xs_len_shortest_umweg = v_umweg_len;
                            xs_shortest_knoten = i;
                            xs_shortest_wegabzweigung = v_wegbisabzweigung;
                        }
                    }


                    t_vec = xe - weg[GetWegIndex(i, wegline_in_reverse_order, weglen)];
					c = t_vec.GetSkalarProduct(wegteil) / wegteil.GetSkalarProduct();
					curr_wegteil_len = (wegteil * c).GetLen();
					if(c>0 && curr_wegteil_len <= wegteil_len )
					{
						Vector2d v_wegbisabzweigung = wegteil * c;
						Vector2d v_umweg = t_vec - v_wegbisabzweigung;
						double v_umweg_len = v_umweg.GetLen();
						if(v_umweg_len < xe_len_shortest_umweg)
						{
							xe_shortest_umweg = v_umweg;
							xe_len_shortest_umweg = v_umweg_len;
							xe_shortest_knoten = i;
							xe_shortest_wegabzweigung = v_wegbisabzweigung;
						}
					}
				}

				//check ob ein orthogonaler vektor gefunden wurde
				//wenn nicht nimm den knoten mit der k�rzesten verbindung zum xs oder xe punkt
				double len_t_vec = 0;
				for(int i=0;i<weg.Length;i++)
				{
                    t_vec = xs - weg[GetWegIndex(i, wegline_in_reverse_order, weglen)];
					len_t_vec = t_vec.GetLen();
					if(len_t_vec < xs_len_shortest_umweg)
					{
						xs_shortest_umweg = t_vec;
						xs_shortest_knoten = i;
						xs_len_shortest_umweg = len_t_vec;
						xs_shortest_wegabzweigung = new Vector2d(0,0);
					}
                    t_vec = xe - weg[GetWegIndex(i, wegline_in_reverse_order, weglen)];
					len_t_vec = t_vec.GetLen();
					if(len_t_vec < xe_len_shortest_umweg)
					{
						xe_shortest_umweg = t_vec;
						xe_shortest_knoten = i;
						xe_len_shortest_umweg = len_t_vec;
						xe_shortest_wegabzweigung = new Vector2d(0,0);
					}
				}

                

                /////////////////////////////////////////////////////////////
                //BERECHNUNG DES UMWEGES IN ABSTRAKTEM MASS, generell kommt 
                //jetz die berechnung der zur�ckgegebenen werte
                //da die BasisStrecke fertig analysiert wurde
                //bei allen berechnungen wird dabei ein fehler ber�cksichtigt
                //dieser ergibt sich aus der linearen betrachung der streckabschnitte 
                //sowie auch routenabweichender wegstrecken
				double m_error_proz = error_proz; //25%


                //berechnet den entfernungsweg ber�cksichtige dabei auch den fehler
                for (int i = 0; i < xs_shortest_knoten; i++)
                {
                    Vector2d wegteil = weg[GetWegIndex(i + 1, wegline_in_reverse_order, weglen)] - weg[GetWegIndex(i, wegline_in_reverse_order, weglen)];
                    double len_wegteil = wegteil.GetLen();
                    double error = GeoFactory.DistanceError.GetRouteDistanceError(len_wegteil, m_error_proz);
                    entfernung_xs_von_wegstart += wegteil.GetLen() + error;
                }
                entfernung_xs_von_wegstart += xs_shortest_wegabzweigung.GetLen();
                entfernung_xs_von_wegstart += xs_len_shortest_umweg;

                //berechnet den entfernungsweg ber�cksichtige dabei auch den fehler
                for (int i = 0; i < xe_shortest_knoten; i++)
                {
                    Vector2d wegteil = weg[GetWegIndex(i + 1, wegline_in_reverse_order, weglen)] - weg[GetWegIndex(i, wegline_in_reverse_order, weglen)];
                    double len_wegteil = wegteil.GetLen();
                    double error = GeoFactory.DistanceError.GetRouteDistanceError(len_wegteil, m_error_proz);
                    entfernung_xe_von_wegstart += wegteil.GetLen() + error;
                }
                entfernung_xe_von_wegstart += xe_shortest_wegabzweigung.GetLen();
                entfernung_xe_von_wegstart += xe_len_shortest_umweg;


                //muss in b_liegt_in_gegenrichtung gefahren werden
                double xs_len_wegabzweigung = xs_shortest_wegabzweigung.GetLen();
				double xe_len_wegabzweigung = xe_shortest_wegabzweigung.GetLen();
                if (entfernung_xs_von_wegstart > entfernung_xe_von_wegstart)
                    b_liegt_in_gegenrichtung = true;

				//handelt es sich bei start und ende um den selben punkt muss der umweg nicht 2 mal gerechnet werden
				if(xs == xe)
				{
					umweg = (xe_len_shortest_umweg * 2);
					umweg += 0.00001; // ein bisschen umweg in der stadt dazurechnen
				}
				else
					umweg = (xe_len_shortest_umweg * 2) + (xs_len_shortest_umweg * 2);

                //umwegaufrechnung f�r den weg der dreimal gefahren werden muss wenn mitzunehmender nicht auf BasisStrecke liegt, also in b_liegt_in_gegenrichtung liegt
                if (xs_shortest_knoten == xe_shortest_knoten && b_liegt_in_gegenrichtung && !(xs_shortest_wegabzweigung.X == 0 && xs_shortest_wegabzweigung.Y == 0))
				{
					//end liegt vor start und beide befinden sich am selben streckenst�ck
                    Vector2d wegteil = weg[GetWegIndex(xs_shortest_knoten + 1, wegline_in_reverse_order, weglen)] - weg[GetWegIndex(xs_shortest_knoten, wegline_in_reverse_order, weglen)];
					double wegteil_len = wegteil.GetLen();
					double error = GeoFactory.DistanceError.GetRouteDistanceError(xe_len_wegabzweigung,xs_len_wegabzweigung,wegteil_len,m_error_proz);
					umweg += (error + (xs_len_wegabzweigung-xe_len_wegabzweigung)) * 2; // XX eingebaut am 28.03.2007
                                                                                        // nimm jetz den weg den ich auf der strasse fahre wenn ende vor start liegt 2mal
                                                                                        // weil er eigentlich zur�ck fahren muss und dadurch die sache 
                                                                                        // genau 3 mal macht also 2 mal zu viel und deswegen mal 2
				}
                else if (b_liegt_in_gegenrichtung)
				{
					//end liegt vor start und sie liegen auf unterschiedlichen streckenst�cken
                    Vector2d next_xe = (xe_shortest_knoten < weg.Length - 1) ? weg[GetWegIndex(xe_shortest_knoten + 1, wegline_in_reverse_order, weglen)] : weg[GetWegIndex(xe_shortest_knoten, wegline_in_reverse_order, weglen)];
                    Vector2d wegteil = next_xe - weg[GetWegIndex(xe_shortest_knoten, wegline_in_reverse_order, weglen)];
					double wegteil_len = wegteil.GetLen();
					double error = GeoFactory.DistanceError.GetRouteDistanceError(xe_len_wegabzweigung,wegteil_len,wegteil_len,m_error_proz);
                    umweg += (error + (wegteil_len - xe_len_wegabzweigung)) * 2;          // siehe xx oben 28.03.2007
					int i=1;
					for(i=1;i<((xs_shortest_knoten-xe_shortest_knoten));i++)
					{
                        wegteil = weg[GetWegIndex(xe_shortest_knoten + 1 + i, wegline_in_reverse_order, weglen)] - weg[GetWegIndex(xe_shortest_knoten + i, wegline_in_reverse_order, weglen)];
						wegteil_len = wegteil.GetLen();
						error = GeoFactory.DistanceError.GetRouteDistanceError(wegteil_len,m_error_proz);
						umweg += (error + (wegteil_len)) * 2;                            // siehe xx oben 28.03.2007
					}
					i = xs_shortest_knoten-xe_shortest_knoten;
					if(xe_shortest_knoten+1+i<weg.Length)
					{
                        wegteil = weg[GetWegIndex(xe_shortest_knoten + 1 + i, wegline_in_reverse_order, weglen)] - weg[GetWegIndex(xe_shortest_knoten + i, wegline_in_reverse_order, weglen)];
						wegteil_len = wegteil.GetLen();
						error = GeoFactory.DistanceError.GetRouteDistanceError(0,xs_len_wegabzweigung,wegteil_len,m_error_proz);
                        umweg += (error + (xs_len_wegabzweigung)) * 2;                   // siehe xx oben 28.03.2007
					}
				}



				gesamtweg = GeoFactory.DistanceCalculation.GetAbstractDistanceWithError(weg,m_error_proz);
				umweg_prozent = (100*umweg)/gesamtweg;

                entfernung_xs_von_wegstart_prozent = (100 * entfernung_xs_von_wegstart) / gesamtweg;
			}
		}

		public class DistanceCalculation{
			/// <summary>
			/// North is positive
			/// East is positive
			/// </summary>
			/// <param name="from"></param>
			/// <param name="to"></param>
			/// <returns></returns>
			public static double GetRealDistance(GeoCoordinates from, GeoCoordinates to,bool withError)
			{
				double dist = GetRealDistance(from.Latitude,from.Longitude,to.Latitude,to.Longitude);
				if(withError)
					dist *= 1.1267;
				return dist;
			}	
			/// <summary>
			/// North is positive
			/// East is positive
			/// </summary>
			/// <param name="from"></param>
			/// <param name="to"></param>
			/// <returns></returns>
			public static double GetRealDistance(Vector2d from, Vector2d to,bool withError)
			{
				double dist = GetRealDistance(from.Y,from.X,to.Y,to.X);
				if(withError)
					dist *= 1.1267;
				return dist;
			}	
			/// <summary>
			/// North is positive
			/// East is positive
			/// </summary>
			/// <param name="from_latitude"></param>
			/// <param name="from_longitude"></param>
			/// <param name="to_latitude"></param>
			/// <param name="to_longitude"></param>
			/// <returns></returns>
			public static double GetRealDistance(double from_latitude,double from_longitude,double to_latitude,double to_longitude)
			{
				//ave. radius = 6371.315 (someone said more accurate is 6366.707)
				//equatorial radius = 6378.388
				//nautical mile = 1.15078
				double avg_earth_radius = 6366.707;
				double radlat1 = System.Math.PI * from_latitude / 180;
				double radlat2 = System.Math.PI * to_latitude /180;
				double radlong1 = Math.PI * from_longitude / 180;
				double radlong2 = Math.PI * to_longitude / 180;
			
				//spherical coordinates x=r*cos(ag)sin(at), y=r*sin(ag)*sin(at), z=r*cos(at)
				//zero ag is up so reverse lat
				//			if (tr[nq]=="N")
				radlat1=Math.PI/2-radlat1;
				//			if (tr[nq]=="S")radlat1=Math.PI/2+radlat1;
				//			if (gr[nq]=="W")
				//			radlong1=Math.PI*2-radlong1;
				//
				//			if (tr[nj]=="N")
				radlat2=Math.PI/2-radlat2;
				//			if (tr[nj]=="S")radlat2=Math.PI/2+radlat2;
				//			if (gr[nj]=="W")radlong2=Math.PI*2-radlong2;

				double x1 = avg_earth_radius * Math.Cos(radlong1)*Math.Sin(radlat1);
				double y1 = avg_earth_radius * Math.Sin(radlong1)*Math.Sin(radlat1);
				double z1 = avg_earth_radius * Math.Cos(radlat1);

				double x2 = avg_earth_radius * Math.Cos(radlong2)*Math.Sin(radlat2);
				double y2 = avg_earth_radius * Math.Sin(radlong2)*Math.Sin(radlat2);
				double z2 = avg_earth_radius * Math.Cos(radlat2);

				double d = Math.Sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2)+(z1-z2)*(z1-z2));

				//side, side, side, law of cosines and arccos
				double theta = Math.Acos((avg_earth_radius*avg_earth_radius+avg_earth_radius*avg_earth_radius-d*d)/(2*avg_earth_radius*avg_earth_radius));
				double distance = theta*avg_earth_radius;

				return distance; 
				//return distance/1.609344; 
				//return distance/1.852; 
			}	


			/// <summary>
			/// berechnet die l�nge des weges ignoriert dabei ob es sich um geo coordinaten handelt und f�gt eine
			/// einen error ein der proportional gross zur gr�sse des jeweiligen wegstrecken bereichs dazu addiert wird
			/// </summary>
			/// <param name="weg"></param>
			/// <param name="error_proz"></param>
			/// <returns></returns>
			public static double GetAbstractDistanceWithError(Line weg,double error_proz){

				double len = 0;
				for(int i=0;i<weg.Length-1;i++)
				{
					Vector2d v = weg[i] - weg[i+1];
					double temp_len = v.GetLen();
					len += temp_len + GeoFactory.DistanceError.GetRouteDistanceError(temp_len,error_proz);
				}
				return len;
			}
		
		}

		public class DistanceError
		{
			/// <summary>
			/// der error des gesamtstrecke in prozent
			/// </summary>
			protected static double m_errorproz = 0.25;


			/// <summary>
			/// gibt den error f�r eine gesamte teilstrecke zur�ck
			/// </summary>
			/// <param name="distance">l�nge der strecke</param>
			/// <returns>fehler der direkt BasisStrecke im mass der distanz</returns>
			public static double GetRouteDistanceError(double distance)
			{
				return GetRouteDistanceError(distance,m_errorproz);
			}
			public static double GetRouteDistanceError(double distance,double error_proz){
				return distance * (error_proz);
			}

			/// <summary>
			/// berechnet den fehler der durch die diskretisierung einer original route entsteht
			/// abh�ngig von der l�nge gesamt distanz handelt es sich dabei um eine gr�ssere
			/// abweichung von der eigentlich l�nge der route. man geht davon aus dass die luftlinien verbhindung grunds�tzlich 
			/// k�rzer als die org route ist.
			/// </summary>
			/// <param name="start">start der route f�r die der fehler berechnet werden soll</param>
			/// <param name="end">end der route f�r die der fehler berechnet werden soll</param>
			/// <param name="distance">gsamtl�nge der strecke</param>
			/// <returns>den fehler in dem mass der �bergebenen gesamt distanz</returns>
			public static double GetRouteDistanceError(double start,double end,double distance)
			{
				return GetRouteDistanceError(start,end,distance,m_errorproz);
			}

			public static double GetRouteDistanceError(double start,double end,double distance,double error_proz){
				//check ob start und end > 0 sind, da sich start bzw. ende auf der route befinden m�ssen
				if(start < 0 || end < 0 || start > distance || end > distance)
					throw new GeoLibException("Location point for start and route must be on route!");

				//vertausche start ende wenn start weiter hinten liegt
				if(start > end)
				{
					TransposeStartEndPoint(ref start,ref end);
				}

				//wenns wirklick gleich ist dann f�g einen kleinen unterschied ein
				if(start == end)
				{
					end += 0.0000000001; 
				}

				//wo liegt die h�lfte der strecke
				double disthalf = distance / 2;

				//berechne den strecken fehler
				double error = 0;
				double error_ges = distance * error_proz;
				double mue_error = error_ges/disthalf;
				double k = mue_error/disthalf;

				if(start <= disthalf && end <= disthalf)
				{
					error = GetIntegralOfErrorFunction(end,k) - GetIntegralOfErrorFunction(start,k);
				}
				else if(start > disthalf && end > disthalf)
				{
					TransposeStartEndPoint(ref start,ref end);
					error = GetIntegralOfErrorFunction(end,k) - GetIntegralOfErrorFunction(start,k);
				}
				else
				{
					error = GetIntegralOfErrorFunction(disthalf,k) - GetIntegralOfErrorFunction(start,k);
					error += GetIntegralOfErrorFunction(disthalf,k) - GetIntegralOfErrorFunction(distance-end,k);
				}
				return error;			
			}

			/// <summary>
			/// error function f(x) = k * x --> integral f(x) = k/2 * x^2 
			/// </summary>
			/// <param name="x">x</param>
			/// <returns>integral f(x)</returns>
			protected static double GetIntegralOfErrorFunction(double x,double k)
			{
				return (k/2) * ( x * x );
			}
		}
		
		/// <summary>
		/// vertauscht die beiden double werte
		/// start wird ende und ende wird start
		/// </summary>
		/// <param name="start"></param>
		/// <param name="end"></param>
		public static void TransposeStartEndPoint(ref double start,ref double end)
		{
			double temp = start;
			start = end;
			end = temp;		
		}
	}
}
