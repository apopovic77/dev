using ProcessEngine;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Linq;
using System.Diagnostics;
using System.Windows;
using LinqLib.Linq2Sql;
using LinqLib.Linq2Sql.Notification;

namespace ProcessEngineLib.Data
{
    //public partial class Projekt : OfflineDataObject
    //{
    //    public override long PrimaryKey
    //    {
    //        get { return Id; }
    //    }

    //    public override string ToString()
    //    {
    //        return this.Id + " " + this.ArbeitsTitel;
    //    }
    //}

    //--------------------------------------------------------------------------
    /// <summary>
    /// The DataContext for the PfStore.
    /// </summary>
    public partial class HangfireDataContext
    {

        #region Configuration
        public static string GetConnectionStringFromConfig()
        {

            string connection_string =
                "Data Source=" + PeConfig.Db_DataSource.Value +
                ";Initial Catalog=" + PeConfig.Db_InitialCatalog.Value +
                ";Persist Security Info=True;User ID=" + PeConfig.Db_UserId.Value +
                ";Password=" + PeConfig.Db_Password.Value;

            return connection_string;
        }
        #endregion // Configuration

        #region Pre Caching
        public void LoadDataCache()
        {
            try
            {
                if (!DatabaseExists())
                {
                    throw new Exception(@"Unable to connect to database: " + PeConfig.Db_InitialCatalog.Value + @"@" + PeConfig.Db_DataSource.Value + Environment.NewLine + @"(Wrong username/password/database)");
                }

                ClearCache();

                Stopwatch sw = Stopwatch.StartNew();

                _loading_steps = 13;
                _curr_loading_step = 0;


                //Log = Console.Out;


                //.ShowProgressStatusInfo(1,"Lade Info");
                //Load("OfflineProjekte", OfflineProjekte);
                ////.ShowProgressStatusInfo(5, "Lade ListValue");
                //Load("OfflineListValue", OfflineListValue);
                ////.ShowProgressStatusInfo(10, "Lade Hersteller");
                //Load("OfflineHersteller", OfflineHersteller);
                ////.ShowProgressStatusInfo(20, "Lade StatusInfo");
                //Load("OfflineHelpInfo", OfflineHelpInfo);
                ////.ShowProgressStatusInfo(25, "Lade HelpInfo");
                //Load("OfflineModelle", OfflineModelle);
                ////.ShowProgressStatusInfo(30, "Lade Modelle");
                //Load("OfflineGeraete", OfflineGeraete);
                ////.ShowProgressStatusInfo(35, "Lade Ger�te");
                //Load("OfflineKundes", OfflineKundes);
                ////.ShowProgressStatusInfo(40, "Lade StatusInfo");
                //Load("OfflineReparaturZeitstempel", OfflineReparaturZeitstempel);
                ////.ShowProgressStatusInfo(45, "Lade Reparatur Zeitstempel");
                //Load("OfflineReparaturAuftrags", OfflineReparaturAuftrags);
                ////.ShowProgressStatusInfo(50, "Lade Reparatur Auftr�ge");
                //Load("OfflineArchivReparaturAuftr�ge", OfflineArchivReparaturAuftr�ge);
                ////.ShowProgressStatusInfo(55, "Lade Benutzer");
                //Load("OfflineBenutzer", OfflineBenutzer);
                ////.ShowProgressStatusInfo(60, "Lade Rollen");
                //Load("OfflineRole", OfflineRole);
                ////.ShowProgressStatusInfo(65, "Lade User Interface");




                //.StartupProgressPercentage = 100;

                sw.Stop();
                //Debug.WriteLine("Data Cache Load Time " + sw.ElapsedMilliseconds + " msec");
            }
            catch (Exception e)
            {
                //todo logmanager integration
                //AppContext.LogManager.WriteAppMessage((long)IaLoggingEventId.InitializationError, Priority.Normal, Severity.Fatal, LogManager.FormatException("LoadCachedValues Error.", ex, true), new string[] { IaLoggingConstants.LOGCATGEORY_INITIALIZATION });
                //AppContext.LogManager.WriteEventLogMessage(-1, EventLogEntryType.Error, LogManager.FormatException("LoadCachedValues Error.", ex, true));
            }
        }

        /// <summary>
        ///     Discard all pending changes of current DataContext.
        ///     All un-submitted changes, including insert/delete/modify will lost.
        /// </summary>
        /// <param name="context"></param>
        public void DiscardPendingChanges()
        {
            RefreshPendingChanges(RefreshMode.OverwriteCurrentValues);
            ChangeSet changeSet = GetChangeSet();
            if (changeSet != null)
            {
                //Undo inserts
                foreach (object objToInsert in changeSet.Inserts)
                {
                    GetTable(objToInsert.GetType()).DeleteOnSubmit(objToInsert);
                }
                //Undo deletes
                foreach (object objToDelete in changeSet.Deletes)
                {
                    GetTable(objToDelete.GetType()).InsertOnSubmit(objToDelete);
                }
            }
        }

        /// <summary>
        ///     Refreshes all pending Delete/Update entity objects of current DataContext according to the specified mode.
        ///     Nothing will do on Pending Insert entity objects.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="refreshMode">A value that specifies how optimistic concurrency conflicts are handled.</param>
        public void RefreshPendingChanges(RefreshMode refreshMode)
        {
            ChangeSet changeSet = GetChangeSet();
            if (changeSet != null)
            {
                Refresh(refreshMode, changeSet.Deletes);
                Refresh(refreshMode, changeSet.Updates);
            }
        }

        public bool ChangesGiven
        {
            get
            {
                ChangeSet changeSet = GetChangeSet();
                if (changeSet != null)
                {
                    int count = changeSet.Inserts.Count + changeSet.Deletes.Count + changeSet.Updates.Count;
                    if (count > 0)
                        return true;
                }
                return false;
            }
        }



        public DbTransaction StartTransaction()
        {
            if (Transaction != null)
                return null;

            if (Connection.State == System.Data.ConnectionState.Closed ||
                Connection.State == System.Data.ConnectionState.Broken)
                Connection.Open();

            Transaction = Connection.BeginTransaction();

            return Transaction;
        }

        public bool FinishTransaction(bool rollback = false)
        {
            if (Transaction == null)
                return false;

            if (rollback)
                Transaction.Rollback();
            else
                Transaction.Commit();

            Transaction = null;
            return true;
        }

        //private void Load<T>(string table_property_name, ObservableEntityTableCollection<T> data_table_property) where T : OfflineDataObject, new()
        //{
        //    Console.WriteLine("Loaded " + table_property_name);
        //    //++_curr_loading_step;
        //    //NeoRepAppContext.StartupProgressPercentage = (int)(_curr_loading_step / _loading_steps * 100);
        //}

        public void ClearCache()
        {
            //_offline_projekte = null;
            //_offline_reparatur_zeitstempels = null;
            //_offline_list_value = null;
            //_offline_modelle  = null;
            //_offline_geraete = null;
            //_observeable_reparatur_auftraege = null;
            //_observeable_archiv_reparatur_auftraege = null;
            //_offline_kundes = null;
            //_offline_benutzer = null;
            //_offline_role = null;
        }


        public void InitPushNotification()
        {
            //InitializeChangeNotification("AenderungsNotifizierungen");
            //RegisterNotificationListener("Ger�t");
            ////RegisterNotificationListener("Modell");
            //RegisterNotificationListener("Hersteller");
            //RegisterNotificationListener("Info");
            //RegisterNotificationListener("ReparaturErsatzteil");
            //RegisterNotificationListener("ReparaturAuftrag");
            //RegisterNotificationListener("ReparaturZeitstempel");
            //RegisterNotificationListener("Kunde");
            //SqlChangeNotification += new EventHandler<LinqLib.Linq2Sql.Notification.SqlChangeNotificationEventArgs>(NeoRep_SqlChangeNotification);
        }

        private void NeoRep_SqlChangeNotification(object sender,
            LinqLib.Linq2Sql.Notification.SqlChangeNotificationEventArgs e)
        {
            //if (NeoRepAppContext.BaseFrame == null)
            //    return;

            //NeoRepAppContext.BaseFrame.Dispatcher.Invoke(DispatcherPriority.Normal, (StatusInfoInfo.NoParas) delegate
            //{
            //    UpdateChanges(e.NotificationMeta);
            //});
        }

        private void UpdateChanges(SqlChangeNotificationMeta notificationMeta)
        {
            //            if (NeoRepAppContext.LoggedInUser == null)
            //                return;

            //            NeoRepAppContext.DbUpdateActive = true;
            //#if DEBUG
            //            Debug.WriteLine("#####################################################");
            //            Debug.WriteLine("### SQL - Data change notification");
            //            Debug.WriteLine("Key: " + notificationMeta.NotificationKey);
            //            Debug.WriteLine("Old long value: " +
            //                            (notificationMeta.OldLongValue == null
            //                                ? "null"
            //                                : notificationMeta.OldLongValue.Value.ToString()));
            //            Debug.WriteLine("New long value: " +
            //                            (notificationMeta.CurrentLongValue == null
            //                                ? "null"
            //                                : notificationMeta.CurrentLongValue.Value.ToString()));
            //            Debug.WriteLine("Old string value: " + (notificationMeta.OldStringValue ?? "null"));
            //            Debug.WriteLine("New string value: " + (notificationMeta.CurrentStringValue ?? "null"));
            //            Debug.WriteLine("#####################################################");
            //#endif

            //            bool is_update = notificationMeta.CurrentStringValue == "update";
            //            bool is_insert = notificationMeta.CurrentStringValue == "insert";
            //            bool is_delete = notificationMeta.CurrentStringValue == "delete";

            //            if (notificationMeta.NotificationKey == "Ger�t")
            //            {
            //                // dieser key hat eine �nderung hervorgerufen
            //                Debug.WriteLine("Modell wurde ge�ndert");
            //                if (is_insert)
            //                {
            //                    if (OfflineModelle.Count() != Modell.Count())
            //                    {
            //                        OfflineModelle.UpdateDataCollection();
            //                        Debug.WriteLine("OfflineModelle wurde refreshed");
            //                    }
            //                }
            //                else if (is_update)
            //                {
            //                    Modell modell =
            //                        OfflineModelle.Where(
            //                                o => o.Id == Convert.ToInt64(notificationMeta.CurrentLongValue.Value))
            //                            .FirstOrDefault();

            //                    if (modell != null)
            //                    {
            //                        Refresh(RefreshMode.OverwriteCurrentValues, modell);

            //                        //NeoRepAppContext.RepTreeView.SetTviHeaderModell(modell.TreeViewItem, modell);
            //                    }


            //                    //RefreshValues(RefreshMode.OverwriteCurrentValues, Kunde);
            //                    //OfflineKundes.UpdateDataCollection();
            //                }

            //            }
            //            else if (notificationMeta.NotificationKey == "Hersteller")
            //            {
            //                // dieser key hat eine �nderung hervorgerufen
            //                Debug.WriteLine("Hersteller wurde ge�ndert");
            //                if (!is_update)
            //                    RefreshOffline();
            //                else
            //                {
            //                    Refresh(RefreshMode.OverwriteCurrentValues, Herstellers);
            //                    //OfflineHersteller.UpdateDataCollection();
            //                }
            //            }
            //            else if (notificationMeta.NotificationKey == "ReparaturZeitstempel")
            //            {
            //                // dieser key hat eine �nderung hervorgerufen
            //                //Debug.WriteLine("ReparaturZeitstempel wurde ge�ndert");
            //                ////if (!is_update_change)
            //                //    RefreshOffline();
            //                //else
            //                //{
            //                //    RefreshValues(RefreshMode.OverwriteCurrentValues, ReparaturZeitstempels);
            //                //    OfflineReparaturZeitstempel.UpdateDataCollection();
            //                //}
            //            }
            //            else if (notificationMeta.NotificationKey == "Info")
            //            {
            //                // dieser key hat eine �nderung hervorgerufen
            //                Debug.WriteLine("Info wurde ge�ndert");

            //                if (is_delete)
            //                {
            //                    //todo delete infos from nachrichten submenupanel if there
            //                }
            //                if (is_insert)
            //                {
            //                    Info info =
            //                        NeoRepDb.Instance()
            //                            .Info.Where(o => o.Id == notificationMeta.CurrentLongValue.Value &&
            //                                             (o.Empf�ngerId == NeoRepAppContext.LoggedInUser.Id || o.AbsenderId == NeoRepAppContext.LoggedInUser.Id || !o.Empf�ngerId.HasValue))
            //                            .SingleOrDefault();

            //                    if (info != null)
            //                    {
            //                        OfflineInfo.Add(info);
            //                        info.UpdateNachrichtenSubMenuPanel();
            //                    }
            //                }
            //                if (is_update)
            //                {
            //                    if (notificationMeta.CurrentLongValue == null)
            //                        return;

            //                    Info info =
            //                        OfflineInfo.Where(
            //                                o => o.Id == Convert.ToInt64(notificationMeta.CurrentLongValue.Value) &&
            //                                     (o.Empf�ngerId == NeoRepAppContext.LoggedInUser.Id || o.AbsenderId == NeoRepAppContext.LoggedInUser.Id || !o.Empf�ngerId.HasValue))
            //                            .FirstOrDefault();



            //                    if (info == null)
            //                    {
            //                        info =
            //                            NeoRepDb.Instance()
            //                                .Info.Where(o => o.Id == notificationMeta.CurrentLongValue.Value &&
            //                                                 (o.Empf�ngerId == NeoRepAppContext.LoggedInUser.Id || o.AbsenderId == NeoRepAppContext.LoggedInUser.Id || !o.Empf�ngerId.HasValue))
            //                                .SingleOrDefault();

            //                        if (info != null)
            //                            OfflineInfo.Add(info);
            //                    }

            //                    //bugfix code
            //                    if (info != null && info.ReparaturAuftragId.HasValue)
            //                    {
            //                        //ich bin drauf gekommen dass es sein kann das nicht alle objekte hergepushed werden ?!?!?!?
            //                        //deswegen hol ich sie jetzt so mit pull
            //                        var query_no_insert = NeoRepDb.Instance()
            //                            .Info.Where(
            //                                o =>
            //                                    o.ReparaturAuftragId == info.ReparaturAuftragId.Value &&
            //                                    o.GesendetAm > DateTime.Now.AddMinutes(-1));

            //                        foreach (Info info_pulled in query_no_insert)
            //                        {
            //                            if (!OfflineInfo.Contains(info_pulled))
            //                            {
            //                                OfflineInfo.Add(info_pulled);
            //                                info_pulled.UpdateNachrichtenSubMenuPanel();
            //                            }
            //                        }
            //                    }




            //                    if (info != null)
            //                    {
            //                        Refresh(RefreshMode.OverwriteCurrentValues, info);
            //                        info.UpdateNachrichtenSubMenuPanel();
            //                    }
            //                }
            //            }
            //            else if (notificationMeta.NotificationKey == "ReparaturAuftrag")
            //            {
            //                // dieser key hat eine �nderung hervorgerufen
            //                Debug.WriteLine("ReparaturAuftrag wurde ge�ndert");

            //                if (is_delete)
            //                {
            //                    if (notificationMeta.CurrentLongValue == null)
            //                        return;

            //                    ReparaturAuftrag rep_auftrag =
            //                        OfflineReparaturAuftrags.Where(
            //                                o => o.Id == Convert.ToInt64(notificationMeta.CurrentLongValue.Value))
            //                            .FirstOrDefault();

            //                    if (rep_auftrag == null)
            //                    {
            //                        return;
            //                    }

            //                    OfflineReparaturAuftrags.Remove(rep_auftrag);

            //                    rep_auftrag.RemoveFromSubMenuPanel();


            //                }
            //                if (is_insert)
            //                {
            //                    ReparaturAuftrag rep_auftrag =
            //                        NeoRepDb.Instance()
            //                            .ReparaturAuftrags.Where(o => o.Id == notificationMeta.CurrentLongValue.Value)
            //                            .SingleOrDefault();

            //                    if (rep_auftrag != null)
            //                    {
            //                        OfflineReparaturAuftrags.Add(rep_auftrag);

            //                        string curr_step = rep_auftrag.CurrProcessStep;
            //                        string new_curr_step = rep_auftrag.CurrProcessStep;

            //                        //rep_auftrag.UpdateProzessTreeView(curr_step, new_curr_step);
            //                        rep_auftrag.UpdateProzessSubMenuPanel(curr_step, new_curr_step);
            //                        rep_auftrag.UpdateTourItemInPanel();
            //                    }
            //                }
            //                if (is_update)
            //                {
            //                    if (notificationMeta.CurrentLongValue == null)
            //                        return;

            //                    ReparaturAuftrag rep_auftrag =
            //                        OfflineReparaturAuftrags.Where(
            //                                o => o.Id == Convert.ToInt64(notificationMeta.CurrentLongValue.Value))
            //                            .FirstOrDefault();

            //                    if (rep_auftrag != null)
            //                    {
            //                        NeoRepAppContext.RemoveFromSubMenuPanel(rep_auftrag);

            //                        string curr_step = rep_auftrag.CurrProcessStep;
            //                        string new_curr_step = null;
            //                        try
            //                        {
            //                            Refresh(RefreshMode.OverwriteCurrentValues, rep_auftrag);
            //                            new_curr_step = rep_auftrag.CurrProcessStep;
            //                        }
            //                        catch
            //                        {
            //                        }

            //                        //rep_auftrag.UpdateProzessTreeView(curr_step, new_curr_step);
            //                        if (new_curr_step != null)
            //                        {
            //                            rep_auftrag.UpdateProzessSubMenuPanel(curr_step, new_curr_step);
            //                            rep_auftrag.UpdateTourItemInPanel();
            //                        }
            //                        else
            //                        {
            //                            rep_auftrag.CurrProcessStep = curr_step;
            //                            rep_auftrag.RemoveFromSubMenuPanel();
            //                            OfflineReparaturAuftrags.Remove(rep_auftrag);

            //                            //check if there are infos to be removed
            //                            List<SubMenuPanelMenuItem> delete_info_items = NeoRepAppContext.InfoSubMenuPanel.Items.Where(
            //                                i => ((InfoSubMenuPanelItem) i).Info.ReparaturAuftragId == rep_auftrag.Id).ToList();

            //                            foreach (SubMenuPanelMenuItem sub_menuitem_info in delete_info_items)
            //                                NeoRepAppContext.InfoSubMenuPanel.RemoveItem(sub_menuitem_info);
            //                        }

            //                    }
            //                    else
            //                    {
            //                        rep_auftrag =
            //                            NeoRepDb.Instance()
            //                                .ReparaturAuftrags.Where(o => o.Id == notificationMeta.CurrentLongValue.Value)
            //                                .SingleOrDefault();

            //                        if (rep_auftrag != null)
            //                        {
            //                            OfflineReparaturAuftrags.Add(rep_auftrag);

            //                            string curr_step = rep_auftrag.CurrProcessStep;
            //                            string new_curr_step = rep_auftrag.CurrProcessStep;

            //                            //rep_auftrag.UpdateProzessTreeView(curr_step, new_curr_step);
            //                            rep_auftrag.UpdateProzessSubMenuPanel(curr_step, new_curr_step);
            //                            rep_auftrag.UpdateTourItemInPanel();
            //                        }
            //                    }
            //                }
            //            }
            //            else if (notificationMeta.NotificationKey == "ReparaturErsatzteil")
            //            {
            //                //// dieser key hat eine �nderung hervorgerufen
            //                //Debug.WriteLine("ReparaturErsatzteil wurde ge�ndert");
            //                //if (!is_update_change)
            //                //    RefreshOffline();
            //                //else
            //                //    OfflineReparaturAuftrags.UpdateDataCollection();
            //            }
            //            else if (notificationMeta.NotificationKey == "Kunde")
            //            {
            //                // dieser key hat eine �nderung hervorgerufen
            //                Debug.WriteLine("Kunde wurde ge�ndert");
            //                if (is_insert)
            //                {
            //                    if (OfflineKundes.Count() != Kunde.Count())
            //                    {
            //                        OfflineKundes.UpdateDataCollection();
            //                        Debug.WriteLine("OfflineKundes wurde refreshed");
            //                    }
            //                }
            //                else if (is_update)
            //                {
            //                    Kunde kunde =
            //                        OfflineKundes.Where(
            //                                o => o.Id == Convert.ToInt64(notificationMeta.CurrentLongValue.Value))
            //                            .FirstOrDefault();

            //                    if (kunde != null)
            //                    {
            //                        Refresh(RefreshMode.OverwriteCurrentValues, kunde);

            //                        //NeoRepAppContext.RepTreeView.SetTviHeaderKunde(kunde.TreeViewItem, kunde);
            //                        //TreeViewItem sub_tvi = kunde.TreeViewItem.Items[0] as TreeViewItem;
            //                        //NeoRepAppContext.RepTreeView.SetSubTviHeaderKunde(sub_tvi, kunde);
            //                    }

            //                    //RefreshValues(RefreshMode.OverwriteCurrentValues, Kunde);
            //                    //OfflineKundes.UpdateDataCollection();
            //                }
            //            }

            //            NeoRepAppContext.DbUpdateActive = false;
        }

        public void RefreshOffline()
        {
            throw new NotImplementedException();
            //NeoRepAppContext.DbUpdateActive = true;
            ////long max_online_g_id = Geraets.Max(o => o.Id);
            ////long max_offline_g_id = OfflineModelle.Max(o => o.Id);

            ////if (max_online_g_id > max_offline_g_id)
            ////{
            ////    //load new objects
            ////    foreach (var g in Geraets.Where(o => o.Id >= max_offline_g_id))
            ////        OfflineModelle.Add(g);
            ////}
            //if (OfflineListValue.Count() != ListValues.Count())
            //{
            //    OfflineListValue.UpdateDataCollection();
            //    Debug.WriteLine("OfflineListValue wurde refreshed");
            //}

            //if (OfflineInfo.Count() != Info.Count())
            //{
            //    OfflineInfo.UpdateDataCollection();
            //    Debug.WriteLine("OfflineInfo wurde refreshed");
            //}

            //if (OfflineHersteller.Count() != Herstellers.Count()) 
            //{
            //    OfflineHersteller.UpdateDataCollection();
            //    Debug.WriteLine("OfflineHersteller wurde refreshed");
            //}

            //if (OfflineModelle.Count() != Modell.Count())
            //{
            //    OfflineModelle.UpdateDataCollection();
            //    Debug.WriteLine("OfflineModelle wurde refreshed");
            //}

            //if (OfflineGeraete.Count() != Geraet.Count())
            //{
            //    OfflineGeraete.UpdateDataCollection();
            //    Debug.WriteLine("OfflineGeraete wurde refreshed");
            //}

            //if (OfflineKundes.Count() != Kunde.Count())
            //{
            //    OfflineKundes.UpdateDataCollection();
            //    Debug.WriteLine("OfflineKundes wurde refreshed");
            //}

            ////if (OfflineReparaturZeitstempel.Count() != ReparaturZeitstempels.Count()) 
            ////{
            ////    //OfflineReparaturZeitstempel.UpdateDataCollection();
            ////    //Debug.WriteLine("OfflineReparaturZeitstempel wurde refreshed");
            ////}


            //if (OfflineReparaturAuftrags.Count() != ReparaturAuftrags.Where(o => o.CurrProcessStep != ProzessStepsName.Erledigt).Count()) 
            //{
            //    OfflineReparaturAuftrags.UpdateDataCollection();
            //    Debug.WriteLine("OfflineReparaturAuftrags wurde refreshed");
            //}

            //if (OfflineBenutzer.Count() != Users.Count()) 
            //{
            //    OfflineBenutzer.UpdateDataCollection();
            //    Debug.WriteLine("OfflineBenutzer wurde refreshed");
            //}

            //if (OfflineRole.Count() != Roles.Count()) 
            //{
            //    OfflineRole.UpdateDataCollection();
            //    Debug.WriteLine("OfflineRole wurde refreshed");
            //}

            ////RefreshValues(RefreshMode.KeepCurrentValues, ReparaturZeitstempels);
            ////RefreshValues(RefreshMode.KeepCurrentValues, Herstellers);
            ////RefreshValues(RefreshMode.KeepCurrentValues, ListValues);

            ////RefreshValues(RefreshMode.KeepCurrentValues, Geraets);
            ////RefreshValues(RefreshMode.KeepCurrentValues, Kundes);
            ////RefreshValues(RefreshMode.KeepCurrentValues, Users);

            //NeoRepAppContext.DbUpdateActive = false;
        }


        //public long GetListValueId(string key, string value)
        //{
        //    ListValue lv = OfflineListValue.Where(o => o.LValue == "H�ndler" && o.LKey == "Kunde;Kundentyp").FirstOrDefault();

        //    if(lv == null)
        //        throw new Exception("ListValue with given Key and Value not existing");

        //    return lv.Id;
        //}

        //public ObservableEntityTableCollection<Projekt> OfflineProjekte
        //{
        //    get
        //    {
        //        if (_offline_projekte == null)
        //        {
        //            _offline_projekte = new ObservableEntityTableCollection<Projekt>(this, Projekt);

        //        }
        //        return _offline_projekte;
        //    }
        //}
        //public ObservableEntityTableCollection<Hersteller> OfflineHersteller
        //{
        //    get
        //    {
        //        if (_offline_hersteller == null)
        //        {
        //            _offline_hersteller = new ObservableEntityTableCollection<Hersteller>(this, Herstellers);


        //        }
        //        return _offline_hersteller;
        //    }
        //}

        //public ObservableEntityTableCollection<ListValue> OfflineListValue
        //{
        //    get
        //    {
        //        if (_offline_list_value == null)
        //        {
        //            _offline_list_value = new ObservableEntityTableCollection<ListValue>(this, ListValues);


        //        }
        //        return _offline_list_value;
        //    }
        //}

        //public ObserveableInfoCollection OfflineInfo
        //{
        //    get
        //    {
        //        if (NeoRepAppContext.LoggedInUser == null)
        //            return null;

        //        if (_offline_info == null)
        //        {
        //            _offline_info = new ObserveableInfoCollection(this, Info);


        //        }
        //        return _offline_info;
        //    }
        //}



        //public ObservableEntityTableCollection<HelpInfo> OfflineHelpInfo
        //{
        //    get
        //    {
        //        if (_offline_helpinfo_value == null)
        //        {
        //            _offline_helpinfo_value = new ObservableEntityTableCollection<HelpInfo>(this, HelpInfo);


        //        }
        //        return _offline_helpinfo_value;
        //    }
        //}

        //public ObserveableReparaturAuftragCollection OfflineReparaturAuftrags
        //{
        //    get
        //    {
        //        if (_observeable_reparatur_auftraege == null)
        //        {
        //            _observeable_reparatur_auftraege = new ObserveableReparaturAuftragCollection(this, ReparaturAuftrags);

        //        }
        //        return _observeable_reparatur_auftraege;
        //    }
        //}

        //public ObserveableArchivReparaturAuftragCollection OfflineArchivReparaturAuftr�ge
        //{
        //    get
        //    {
        //        if (_observeable_archiv_reparatur_auftraege == null)
        //        {
        //            _observeable_archiv_reparatur_auftraege = new ObserveableArchivReparaturAuftragCollection(this, ReparaturAuftrags);
        //        }
        //        return _observeable_archiv_reparatur_auftraege;
        //    }
        //}

        //public ObservableEntityTableCollection<Modell> OfflineModelle
        //{
        //    get
        //    {
        //        if (_offline_modelle == null)
        //        {
        //            _offline_modelle = new ObservableEntityTableCollection<Modell>(this, Modell);
        //            _offline_modelle.Sort(delegate(Modell x, Modell y)
        //            {
        //                //if (x.PartName == null && y.PartName == null) return 0;
        //                //else if (x.PartName == null) return -1;
        //                //else if (y.PartName == null) return 1;
        //                //else return x.PartName.CompareTo(y.PartName);

        //                //// A null value means that this object is greater.
        //                //if (other == null)
        //                //    return 1;
        //                //else
        //                //{
        //                int comp_kat = x.ModellKategorie.CompareTo(y.ModellKategorie);
        //                if (comp_kat != 0)
        //                    return comp_kat;
        //                else
        //                    return x.Hersteller.Name.CompareTo(y.Hersteller.Name);
        //                //}
        //            });

        //        }
        //        return _offline_modelle;
        //    }
        //}
        //public ObservableEntityTableCollection<Geraet> OfflineGeraete
        //{
        //    get
        //    {
        //        if (_offline_geraete == null)
        //        {
        //            _offline_geraete = new ObservableEntityTableCollection<Geraet>(this, Geraet);


        //        }
        //        return _offline_geraete;
        //    }
        //}

        ////public ObservableEntityTableCollection<Geraet> OfflineModelle
        ////{
        ////    get
        ////    {
        ////        if (_offline_modelle == null)
        ////        {
        ////            _offline_modelle = new ObservableEntityTableCollection<Geraet>(this, Geraets);


        ////        }
        ////        return _offline_modelle;
        ////    }
        ////}

        //public ObservableEntityTableCollection<Kunde> OfflineKundes
        //{
        //    get
        //    {
        //        if (_offline_kundes == null)
        //        {
        //            _offline_kundes = new ObservableEntityTableCollection<Kunde>(this, Kunde);
        //        }
        //        return _offline_kundes;
        //    }
        //}

        //public ObservableEntityTableCollection<User> OfflineBenutzer
        //{
        //    get
        //    {
        //        if (_offline_benutzer == null)
        //        {
        //            _offline_benutzer = new ObservableEntityTableCollection<User>(this, Users);


        //        }
        //        return _offline_benutzer;
        //    }
        //}

        //public ObservableEntityTableCollection<Role> OfflineRole
        //{
        //    get
        //    {
        //        if (_offline_role == null)
        //        {
        //            _offline_role = new ObservableEntityTableCollection<Role>(this, Roles);


        //        }
        //        return _offline_role;
        //    }
        //}


        //public ListCollectionView OfflineViewKundenH�ndler
        //{
        //    get
        //    {
        //        if (_offline_view_kunden_h�ndler == null)
        //        {
        //            _offline_view_kunden_h�ndler = new ListCollectionView(OfflineKundes);
        //            //_offline_view_kunden_h�ndler.Filter = o => ((Kunde)o).KundenTypLV == GetListValueId("H�ndler", "Kunde;Kundentyp");
        //            _offline_view_kunden_h�ndler.Filter = o => ((Kunde)o).KundenTypLV == "H�ndler";
        //        }

        //        return _offline_view_kunden_h�ndler;
        //    }
        //}


        private int _curr_loading_step = 0;
        private float _loading_steps = 0;

        //private ObservableEntityTableCollection<Projekt> _offline_projekte;
        //private ObservableEntityTableCollection<Hersteller> _offline_hersteller;
        //private ObservableEntityTableCollection<ListValue> _offline_list_value;
        //private ObserveableInfoCollection _offline_info;
        //private ObservableEntityTableCollection<HelpInfo> _offline_helpinfo_value;

        //private ObserveableReparaturAuftragCollection _observeable_reparatur_auftraege;
        //private ObserveableArchivReparaturAuftragCollection _observeable_archiv_reparatur_auftraege;
        //private ObservableEntityTableCollection<Modell> _offline_modelle;
        //private ObservableEntityTableCollection<Geraet> _offline_geraete;
        //private ObservableEntityTableCollection<Kunde> _offline_kundes;

        //private ObservableEntityTableCollection<User> _offline_benutzer;
        //private ObservableEntityTableCollection<Role> _offline_role;

        //private ListCollectionView _offline_view_kunden_h�ndler;

        #endregion
    }


    //--------------------------------------------------------------------------
    /// <summary>
    /// Configures loading options for the PfStoreDataContext and initializes internal data caches.
    /// </summary>
    public class LaisDb
    {
        //public static void LoadDataCache(string instance_key)
        //{
        //    lock (_lock)
        //    {
        //        NeoRepDbDataContext dc = Instance(instance_key);

        //        if (instance_key == "")
        //        {
        //            dc.LoadDataCache();
        //            dc.InitPushNotification();
        //        }
        //    }
        //}

        //public static void LoadDataCache()
        //{
        //    LoadDataCache(MAIN_INSTANCE_KEY);
        //}

        private static HangfireDataContext CreateDc()
        {
            return CreateDc(MAIN_INSTANCE_KEY);
        }

        private static HangfireDataContext CreateDc(string instance_key)
        {
            lock (_lock)
            {

                string con_string = HangfireDataContext.GetConnectionStringFromConfig();

                Console.WriteLine(@"connecting to: " + con_string);

                HangfireDataContext dc = new HangfireDataContext(con_string);
                dc.InstanceKey = instance_key;
                //dc.Log = Console.Out;
                dc.LoadOptions = CreateDataLoadOptions();
                if (instance_key.StartsWith("main_inst"))
                    dc.LoadDataCache();

                return dc;
            }
        }



        private static DataLoadOptions CreateDataLoadOptions()
        {
            DataLoadOptions dlo = new DataLoadOptions();
            ////dlo.LoadWith<ReparaturZeitstempel>(o => o.ReparaturAuftrag);
            //dlo.LoadWith<Modell>(o => o.Hersteller);
            //dlo.LoadWith<User>(o => o.Role);
            return dlo;
        }



        public static void DisposeDc(string instance_key)
        {
            lock (_lock)
            {
                if (!_instances.ContainsKey(instance_key))
                    return;

                _instances[instance_key].Dispose();
                _instances[instance_key].ClearCache();
                _instances.Remove(instance_key);
            }
        }

        public static void DisposeDc(DataContext dc)
        {
            lock (_lock)
            {
                if (!(dc is DataContextEx))
                    throw new Exception("Cannot Dispose Dc without Instance Key");

                string instance_key = ((DataContextEx)dc).InstanceKey;

                if (!_instances.ContainsKey(instance_key))
                    return;

                _instances[instance_key].Dispose();
                _instances[instance_key].ClearCache();
                _instances.Remove(instance_key);
            }
        }

        public static void DisposeDc()
        {
            DisposeDc(MAIN_INSTANCE_KEY);
        }

        public static bool IsDisposedDc(HangfireDataContext dc)
        {
            return !_instances.ContainsKey(dc.InstanceKey);
        }


        public static HangfireDataContext Instance(string instance_key)
        {
            lock (_lock)
            {
                if (_instances.ContainsKey(instance_key))
                    return _instances[instance_key];

                HangfireDataContext instance = CreateDc(instance_key);
                if (instance_key.StartsWith("main_inst") && WithPushNotification)
                    instance.InitPushNotification();
                _instances.Add(instance_key, instance);
                return instance;
            }
        }

        public static HangfireDataContext Instance()
        {
            return Instance(MAIN_INSTANCE_KEY);
        }

        public static bool WithPushNotification = true;
        private static object _lock = new object();
        private static Dictionary<string, HangfireDataContext> _instances = new Dictionary<string, HangfireDataContext>();
        public const string MAIN_INSTANCE_KEY = "main_inst_key_1244";


    }

}