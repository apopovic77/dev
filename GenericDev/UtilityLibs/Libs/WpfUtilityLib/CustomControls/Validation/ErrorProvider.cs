﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using Logicx.WpfUtility.WpfHelpers;
using Microsoft.Windows.Controls;
using Microsoft.Windows.Controls.Primitives;

namespace Logicx.WpfUtility.CustomControls.Validation
{
    /// <summary>
    /// Custom validation trigger
    /// </summary>
    public enum CustomValidationTrigger
    {
        Explicit,
        FocusLost,
        FocusGained
    }

    /// <summary>
    /// Error provider control
    /// </summary>
    public class ErrorProvider : ContentControl, IDisposable
    {
        #region Nested types
        /// <summary>
        /// Error control meta data
        /// </summary>
        private class ErrorControlMetaData
        {
            /// <summary>
            /// Flag indicating if the control has data bindings
            /// </summary>
            public bool HasDataBinding;
            /// <summary>
            /// Flag indicating if the control has a manual validation hook
            /// </summary>
            public bool IsManualValidationHook;
            /// <summary>
            /// Validation event handler
            /// </summary>
            public EventHandler<CustomValidationEventArgs> ValidationEventHandler;
            /// <summary>
            /// Event handler for data binding validation error collection changes
            /// </summary>
            public EventHandler ValidationErrorsChangedHandler;
            /// <summary>
            /// Data binding expression list for the control
            /// </summary>
            public List<BindingExpression> DataBindingExpressions = new List<BindingExpression>();
        }
        #endregion

        #region Construction and Initialization
        /// <summary>
        /// Static error provider constructor
        /// </summary>
        static ErrorProvider()
        {
            CustomValidationEvent = EventManager.RegisterRoutedEvent("CustomValidation", RoutingStrategy.Bubble, typeof(EventHandler<CustomValidationEventArgs>), typeof(ErrorProvider));

            ErrorsPropertyKey = DependencyProperty.RegisterAttachedReadOnly("Errors", typeof(ReadOnlyObservableCollection<ValidationErrorEx>), typeof(ErrorProvider), new FrameworkPropertyMetadata(ValidationErrorExCollection.Empty, FrameworkPropertyMetadataOptions.NotDataBindable));
            ErrorsProperty = ErrorsPropertyKey.DependencyProperty;


            ErrorCountProperty = DependencyProperty.Register("ErrorCount", typeof(int), typeof(ErrorProvider), new FrameworkPropertyMetadata(0));

            ValidationErrorsInternalProperty = DependencyProperty.RegisterAttached("ErrorsInternal", typeof(ValidationErrorExCollection), typeof(ErrorProvider), new FrameworkPropertyMetadata(null, new PropertyChangedCallback(ErrorProvider.OnErrorsInternalChanged)));
            HasErrorPropertyKey = DependencyProperty.RegisterAttachedReadOnly("HasError", typeof(bool), typeof(ErrorProvider), new FrameworkPropertyMetadata(false, FrameworkPropertyMetadataOptions.NotDataBindable));
            HasErrorProperty = HasErrorPropertyKey.DependencyProperty;

            IsValidPropertyKey = DependencyProperty.RegisterReadOnly("IsValid", typeof(bool), typeof(ErrorProvider), new FrameworkPropertyMetadata(true, FrameworkPropertyMetadataOptions.NotDataBindable));
            IsValidProperty = IsValidPropertyKey.DependencyProperty;

            CustomDataAffectsIsValidProperty = DependencyProperty.Register("CustomDataAffectsIsValid", typeof(bool), typeof(ErrorProvider), new FrameworkPropertyMetadata(true, FrameworkPropertyMetadataOptions.NotDataBindable));


            ParentVirtualizingItemsControlProperty = DependencyProperty.RegisterAttached("ParentVirtualizingItemsControl", typeof(ItemsControl), typeof(ErrorProvider), new FrameworkPropertyMetadata(null, new PropertyChangedCallback(ErrorProvider.OnParentVirtualizingItemsControlChanged)));
            VirtualItemsRegisteredKey = DependencyProperty.RegisterAttachedReadOnly("VirtualItemsRegistered", typeof(bool), typeof(ErrorProvider), new FrameworkPropertyMetadata(false, FrameworkPropertyMetadataOptions.NotDataBindable));
            VirtualItemsRegisteredProperty = VirtualItemsRegisteredKey.DependencyProperty;


            ErrorTemplateProperty = DependencyProperty.RegisterAttached("ErrorTemplate", typeof(ControlTemplate), typeof(ErrorProvider), new FrameworkPropertyMetadata(CreateDefaultErrorTemplate(), FrameworkPropertyMetadataOptions.NotDataBindable, new PropertyChangedCallback(ErrorProvider.OnErrorTemplateChanged)));
            ValidationAdornerSiteProperty = DependencyProperty.RegisterAttached("ValidationAdornerSite", typeof(DependencyObject), typeof(ErrorProvider), new FrameworkPropertyMetadata(null, new PropertyChangedCallback(ErrorProvider.OnValidationAdornerSiteChanged)));
            ValidationAdornerSiteForProperty = DependencyProperty.RegisterAttached("ValidationAdornerSiteFor", typeof(DependencyObject), typeof(ErrorProvider), new FrameworkPropertyMetadata(null, new PropertyChangedCallback(ErrorProvider.OnValidationAdornerSiteForChanged)));
            
            ValidationAdornerProperty = DependencyProperty.RegisterAttached("ValidationAdorner", typeof(TemplatedAdornerEx), typeof(ErrorProvider), new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.NotDataBindable));

            RegisterProperty = DependencyProperty.RegisterAttached("Register", typeof(bool), typeof(ErrorProvider), new FrameworkPropertyMetadata(false, FrameworkPropertyMetadataOptions.NotDataBindable, new PropertyChangedCallback(ErrorProvider.OnRegisterChanged)));

            CustomValidationTriggerProperty = DependencyProperty.RegisterAttached("CustomValidationTrigger", typeof(CustomValidationTrigger), typeof(ErrorProvider), new FrameworkPropertyMetadata(CustomValidationTrigger.Explicit, new PropertyChangedCallback(ErrorProvider.OnCustomValidationTriggerChanged)));

            ValidationGroupParentControlProperty = DependencyProperty.RegisterAttached("ValidationGroupControlParent", typeof(DependencyObject), typeof(ErrorProvider), new FrameworkPropertyMetadata(null, new PropertyChangedCallback(ErrorProvider.OnValidationGroupParentControlChanged)));
            ValidationGroupParentProperty = DependencyProperty.RegisterAttached("ValidationGroupParent", typeof(string), typeof(ErrorProvider), new FrameworkPropertyMetadata(string.Empty, new PropertyChangedCallback(ErrorProvider.OnValidationGroupParentChanged)));
            ValidationGroupParentTypeProperty = DependencyProperty.RegisterAttached("ValidationGroupParentType", typeof(Type), typeof(ErrorProvider), new FrameworkPropertyMetadata(null, new PropertyChangedCallback(ErrorProvider.OnValidationGroupParentTypeChanged)));

            ErrorSourceNameProperty = DependencyProperty.RegisterAttached("ErrorSourceName", typeof(string), typeof(ErrorProvider), new FrameworkPropertyMetadata(string.Empty, new PropertyChangedCallback(ErrorProvider.OnErrorSourceNameChanged)));

            ManualValidationHookProperty = DependencyProperty.RegisterAttached("ManualValidationHook", typeof(bool), typeof(ErrorProvider), new FrameworkPropertyMetadata(false, new PropertyChangedCallback(ErrorProvider.OnManualValidationHookChanged)));
            ReportsToErrorProviderProperty = DependencyProperty.RegisterAttached("ReportsToErrorProvider", typeof(bool), typeof(ErrorProvider), new FrameworkPropertyMetadata(true, new PropertyChangedCallback(ErrorProvider.OnReportsToErrorProviderChanged)));

        }
        /// <summary>
        /// Default constructor for design-time support
        /// </summary>
        public ErrorProvider()
        {
            //GenerateListSnapshots(); // to identify unlinked controls or manual hooks by this instance
            this.Loaded += new RoutedEventHandler(ErrorProvider_Loaded);
            this.Unloaded += new RoutedEventHandler(ErrorProvider_Unloaded);
        }

        
        #endregion

        #region Operations

        #region static attached property and template operations
        /// <summary>
        /// Adds an Validation Error to the error collection of an element
        /// </summary>
        /// <param name="validationError"></param>
        /// <param name="targetElement"></param>
        internal static void AddValidationError(ValidationErrorEx validationError, DependencyObject targetElement)
        {
            if (targetElement != null)
            {
                bool flag;
                ValidationErrorExCollection errorsInternal = GetErrorsInternal(targetElement);
                if (errorsInternal == null)
                {
                    flag = true;
                    errorsInternal = new ValidationErrorExCollection();
                    errorsInternal.Add(validationError);
                    targetElement.SetValue(ValidationErrorsInternalProperty, errorsInternal);
                }
                else
                {
                    flag = errorsInternal.Count == 0;
                    if (!errorsInternal.Contains(validationError))
                        errorsInternal.Add(validationError);
                }
                if (flag)
                {
                    targetElement.SetValue(HasErrorPropertyKey, true);
                }

                DependencyObject parentGroupObject = GetValidationGroupParentControl(targetElement);

                if (parentGroupObject != null)
                {
                    // add this error to the parent group element
                    AddValidationError(validationError, parentGroupObject);
                }

                if (!HasDataBindingError(targetElement) || !HasDataBindingWithValidationRules(targetElement))
                {
                    if(!(targetElement is UIElement)  || (((UIElement)targetElement).Visibility == Visibility.Visible))
                        ShowValidationAdorner(targetElement, true);
                }
                else
                {
                    ShowValidationAdorner(targetElement, false); //hide this validation template if the error list contains a databinding error 
                }
            }
        }
        /// <summary>
        /// Removes a Validation Error from the elements error collection
        /// </summary>
        /// <param name="validationError"></param>
        /// <param name="targetElement"></param>
        internal static void RemoveValidationError(ValidationErrorEx validationError, DependencyObject targetElement)
        {
            if (targetElement != null)
            {
                ValidationErrorExCollection errorsInternal = GetErrorsInternal(targetElement);
                if (((errorsInternal != null) && (errorsInternal.Count != 0)) && errorsInternal.Contains(validationError))
                {
                    bool flag = errorsInternal.Count == 1;
                    if (flag)
                    {
                        targetElement.ClearValue(HasErrorPropertyKey);
                    }
                    errorsInternal.Remove(validationError);

                    DependencyObject parentGroupObject = GetValidationGroupParentControl(targetElement);

                    if(parentGroupObject != null)
                    {
                        // remove this error from the parent group element
                        RemoveValidationError(validationError, parentGroupObject);
                    }

                    if (flag)
                    {
                        targetElement.ClearValue(ValidationErrorsInternalProperty);
                        ShowValidationAdorner(targetElement, false);
                    } 
                    else
                    {
                        if (!HasDataBindingError(targetElement) || !HasDataBindingWithValidationRules(targetElement))
                        {
                            if (!(targetElement is UIElement) || (((UIElement)targetElement).Visibility == Visibility.Visible))
                                ShowValidationAdorner(targetElement, true);
                        }
                        else
                        {
                            ShowValidationAdorner(targetElement, false);
                        }
                    }
                }
            }
        }



        /// <summary>
        /// Adds a validation event handler
        /// </summary>
        /// <param name="d"></param>
        /// <param name="handler"></param>
        public static void AddCustomValidationHandler(DependencyObject d, EventHandler<CustomValidationEventArgs> handler)
        {
            if (d == null)
            {
                throw new ArgumentNullException("d");
            }
            UIElement element2 = d as UIElement;
            if (element2 != null)
            {
                element2.AddHandler(CustomValidationEvent, handler);
            }
            else
            {
                ContentElement element = d as ContentElement;
                if (element != null)
                {
                    element.AddHandler(CustomValidationEvent, handler);
                }
                else
                {
                    UIElement3D elementd = d as UIElement3D;
                    if (elementd == null)
                    {
                        throw new ArgumentException("Invalid input element");
                    }
                    elementd.AddHandler(CustomValidationEvent, handler);
                }
            }

        }
        /// <summary>
        /// Removes an validation event handler
        /// </summary>
        /// <param name="d"></param>
        /// <param name="handler"></param>
        public static void RemoveCustomValidationHandler(DependencyObject d, EventHandler<CustomValidationEventArgs> handler)
        {
            if (d == null)
            {
                throw new ArgumentNullException("d");
            }
            UIElement element2 = d as UIElement;
            if (element2 != null)
            {
                element2.RemoveHandler(CustomValidationEvent, handler);
            }
            else
            {
                ContentElement element = d as ContentElement;
                if (element != null)
                {
                    element.RemoveHandler(CustomValidationEvent, handler);
                }
                else
                {
                    UIElement3D elementd = d as UIElement3D;
                    if (elementd == null)
                    {
                        throw new ArgumentException("Invalid input element");
                    }
                    elementd.RemoveHandler(CustomValidationEvent, handler);
                }
            }

        }

        /// <summary>
        /// Default error template
        /// </summary>
        /// <returns></returns>
        private static ControlTemplate CreateDefaultErrorTemplate()
        {
            ControlTemplate template = new ControlTemplate(typeof(Control));
            FrameworkElementFactory factory = new FrameworkElementFactory(typeof(Border), "Border");
            factory.SetValue(Border.BorderBrushProperty, Brushes.Red);
            factory.SetValue(Border.BorderThicknessProperty, new Thickness(1.0));
            FrameworkElementFactory child = new FrameworkElementFactory(typeof(AdornedElementPlaceholder), "Placeholder");
            factory.AppendChild(child);
            template.VisualTree = factory;
            template.Seal();
            return template;
        }

        /// <summary>
        /// Gets the read only errors collection for a framework element
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        public static ReadOnlyObservableCollection<ValidationErrorEx> GetErrors(DependencyObject element)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }
            return (ReadOnlyObservableCollection<ValidationErrorEx>)element.GetValue(ErrorsProperty);
        }
        /// <summary>
        /// Gets the internal errors collection with modification support
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        internal static ValidationErrorExCollection GetErrorsInternal(DependencyObject target)
        {
            return (ValidationErrorExCollection)target.GetValue(ValidationErrorsInternalProperty);
        }

        /// <summary>
        /// Get the error template
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        [AttachedPropertyBrowsableForType(typeof(DependencyObject))]
        public static ControlTemplate GetErrorTemplate(DependencyObject element)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }
            return (element.GetValue(ErrorTemplateProperty) as ControlTemplate);
        }

        /// <summary>
        /// Get the register flag
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        [AttachedPropertyBrowsableForType(typeof(DependencyObject))]
        public static bool GetRegister(DependencyObject element)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }
            return ((bool) element.GetValue(RegisterProperty));
        }

        [AttachedPropertyBrowsableForType(typeof(DependencyObject))]
        public static ItemsControl GetParentVirtualizingItemsControl(DependencyObject element)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }
            return ((ItemsControl)element.GetValue(ParentVirtualizingItemsControlProperty));
        }

        /// <summary>
        /// Get the manual validation hook flag
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        [AttachedPropertyBrowsableForType(typeof(DependencyObject))]
        public static bool GetManualValidationHook(DependencyObject element)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }
            return ((bool)element.GetValue(ManualValidationHookProperty));
        }

        /// <summary>
        /// Get the reports to error provider control flag
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        [AttachedPropertyBrowsableForType(typeof(DependencyObject))]
        public static bool GetReportsToErrorProvider(DependencyObject element)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }
            return ((bool)element.GetValue(ReportsToErrorProviderProperty));
        }

        /// <summary>
        /// Get the custom validation trigger property
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        [AttachedPropertyBrowsableForType(typeof(DependencyObject))]
        public static CustomValidationTrigger GetCustomValidationTrigger(DependencyObject element)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }
            return ((CustomValidationTrigger)element.GetValue(CustomValidationTriggerProperty));
        }

        /// <summary>
        /// Get the validation group control property
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        internal static DependencyObject GetValidationGroupParentControl(DependencyObject element)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }
            DependencyObject control = ((DependencyObject) element.GetValue(ValidationGroupParentControlProperty));

            if(control == null)
            {
                string sParentName = (string)element.GetValue(ValidationGroupParentProperty);

                if(!string.IsNullOrEmpty(sParentName))
                {
                    ErrorProvider errorProvider = GetTopMostErrorProviderForElement(element);

                    if (errorProvider != null)
                    {
                        control = UIHelper.FindVisualChildByName<DependencyObject>(errorProvider, sParentName);

                        SetValidationGroupParentControl(element, control);
                    }
                }
                else
                {
                    Type parentType = GetValidationGroupParentType(element);

                    if(parentType != null)
                    {
                        control = UIHelper.TryFindParent(element, parentType);

                        ErrorProvider errorProvider = GetTopMostErrorProviderForElement(control);

                        if (errorProvider == null)
                        {
                            control = null;
                        }
                        else
                        {
                            SetValidationGroupParentControl(element, control);
                        }
                    }
                }
            }
            return control;
        }

        /// <summary>
        /// Get the validation group type property
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        public static Type GetValidationGroupParentType(DependencyObject element)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }
            return (Type)element.GetValue(ValidationGroupParentTypeProperty);
        }

        /// <summary>
        /// Gets the VirtualItemsRegistered flag for an object
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        public static bool GetVirtualItemsRegistered(DependencyObject element)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }
            return (bool)element.GetValue(VirtualItemsRegisteredProperty);
        }

        /// <summary>
        /// Get the validation group control name property
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        [AttachedPropertyBrowsableForType(typeof(DependencyObject))]
        public static string GetValidationGroupParent(DependencyObject element)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }
            return ((string)element.GetValue(ValidationGroupParentProperty));
        }

        /// <summary>
        /// Get the error source name
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        [AttachedPropertyBrowsableForType(typeof(DependencyObject))]
        public static string GetErrorSourceName(DependencyObject element)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }
            return ((string)element.GetValue(ErrorSourceNameProperty));
        }

        /// <summary>
        /// Gets the HasError flag for an object
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        public static bool GetHasError(DependencyObject element)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }
            return (bool)element.GetValue(HasErrorProperty);
        }

        /// <summary>
        /// Gets the validation adorner site property for an element
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        [AttachedPropertyBrowsableForType(typeof(DependencyObject))]
        public static DependencyObject GetValidationAdornerSite(DependencyObject element)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }
            return (element.GetValue(ValidationAdornerSiteProperty) as DependencyObject);
        }
        /// <summary>
        /// gets the validation adorner site for for an element
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        [AttachedPropertyBrowsableForType(typeof(DependencyObject))]
        public static DependencyObject GetValidationAdornerSiteFor(DependencyObject element)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }
            return (element.GetValue(ValidationAdornerSiteForProperty) as DependencyObject);
        }
        /// <summary>
        /// Sets the error template
        /// </summary>
        /// <param name="element"></param>
        /// <param name="value"></param>
        public static void SetErrorTemplate(DependencyObject element, ControlTemplate value)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }
            if (!object.Equals(element.ReadLocalValue(ErrorTemplateProperty), value))
            {
                element.SetValue(ErrorTemplateProperty, value);
            }
        }

        /// <summary>
        /// Sets the register flag
        /// </summary>
        /// <param name="element"></param>
        /// <param name="value"></param>
        public static void SetRegister(DependencyObject element, bool value)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }
            if (!object.Equals(element.ReadLocalValue(RegisterProperty), value))
            {
                
                element.SetValue(RegisterProperty, value);
            }
        }


        public static void SetParentVirtualizingItemsControl(DependencyObject element, ItemsControl parent)
        {
            if(element == null)
            {
                throw new ArgumentNullException("element");
            }
            if(parent != null)
            {
                if(!(element is FrameworkElement) && string.IsNullOrEmpty(((FrameworkElement)element).Name))
                {
                    throw new ArgumentException("element must be a FrameworkElement and must have a Name set!");
                }
            }

            if (!object.Equals(element.ReadLocalValue(ParentVirtualizingItemsControlProperty), parent))
            {

                element.SetValue(ParentVirtualizingItemsControlProperty, parent);
            }
        }

        /// <summary>
        /// Sets the manual validation hook flag
        /// </summary>
        /// <param name="element"></param>
        /// <param name="value"></param>
        public static void SetManualValidationHook(DependencyObject element, bool value)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }
            if (!object.Equals(element.ReadLocalValue(ManualValidationHookProperty), value))
            {

                element.SetValue(ManualValidationHookProperty, value);
            }
        }

        /// <summary>
        /// Sets the reports to error provider control
        /// </summary>
        /// <param name="element"></param>
        /// <param name="value"></param>
        public static void SetReportsToErrorProvider(DependencyObject element, bool value)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }
            if (!object.Equals(element.ReadLocalValue(ReportsToErrorProviderProperty), value))
            {

                element.SetValue(ReportsToErrorProviderProperty, value);
            }
        }

        /// <summary>
        /// Sets the custom validation trigger type
        /// </summary>
        /// <param name="element"></param>
        /// <param name="value"></param>
        public static void SetCustomValidationTrigger(DependencyObject element, CustomValidationTrigger value)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }
            if (!object.Equals(element.ReadLocalValue(CustomValidationTriggerProperty), value))
            {
                element.SetValue(CustomValidationTriggerProperty, value);
            }
        }

        /// <summary>
        /// Sets the error source name
        /// </summary>
        /// <param name="element"></param>
        /// <param name="value"></param>
        public static void SetErrorSourceName(DependencyObject element, string value)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }
            if (!object.Equals(element.ReadLocalValue(ErrorSourceNameProperty), value))
            {
                element.SetValue(ErrorSourceNameProperty, value);
            }
        }

        /// <summary>
        /// Sets the validation group parent control
        /// </summary>
        /// <param name="element"></param>
        /// <param name="value"></param>
        internal static void SetValidationGroupParentControl(DependencyObject element, DependencyObject value)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }
            DependencyObject oldVal = element.ReadLocalValue(ValidationGroupParentControlProperty) as DependencyObject;

            if (!object.Equals(oldVal, value))
            {
                element.SetValue(ValidationGroupParentControlProperty, value);
            }
        }

        /// <summary>
        /// Sets the validation group parent type
        /// </summary>
        /// <param name="element"></param>
        /// <param name="value"></param>
        public static void SetValidationGroupParentType(DependencyObject element, Type value)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }
            DependencyObject oldVal = element.ReadLocalValue(ValidationGroupParentTypeProperty) as DependencyObject;

            if (!object.Equals(oldVal, value))
            {
                element.SetValue(ValidationGroupParentTypeProperty, value);
            }
        }

        /// <summary>
        /// Sets the validation group parent control name
        /// </summary>
        /// <param name="element"></param>
        /// <param name="value"></param>
        public static void SetValidationGroupParent(DependencyObject element, string value)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }
            string oldVal = element.ReadLocalValue(ValidationGroupParentProperty) as string;

            if (!object.Equals(oldVal, value))
            {
                element.SetValue(ValidationGroupParentProperty, value);
            }
        }

        /// <summary>
        /// Sets the validation adorner site
        /// </summary>
        /// <param name="element"></param>
        /// <param name="value"></param>
        public static void SetValidationAdornerSite(DependencyObject element, DependencyObject value)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }
            element.SetValue(ValidationAdornerSiteProperty, value);
        }
        /// <summary>
        /// Sets the validation adorner site for
        /// </summary>
        /// <param name="element"></param>
        /// <param name="value"></param>
        public static void SetValidationAdornerSiteFor(DependencyObject element, DependencyObject value)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }
            element.SetValue(ValidationAdornerSiteForProperty, value);
        }
        /// <summary>
        /// Show the validation adorner
        /// </summary>
        /// <param name="targetElement"></param>
        /// <param name="show"></param>
        internal static void ShowValidationAdorner(DependencyObject targetElement, bool show)
        {
            DependencyObject validationAdornerSite = GetValidationAdornerSite(targetElement);
            if (validationAdornerSite == null)
            {
                validationAdornerSite = targetElement;
            }
            ShowValidationAdornerHelper(targetElement, validationAdornerSite, show);
        }
        /// <summary>
        /// Validation adorner helper
        /// </summary>
        /// <param name="targetElement"></param>
        /// <param name="adornerSite"></param>
        /// <param name="show"></param>
        private static void ShowValidationAdornerHelper(DependencyObject targetElement, DependencyObject adornerSite, bool show)
        {
            UIElement visual = adornerSite as UIElement;
            if (visual != null)
            {
                AdornerLayer adornerLayer = AdornerLayer.GetAdornerLayer(visual);
                if (adornerLayer != null)
                {
                    TemplatedAdornerEx adorner = visual.ReadLocalValue(ValidationAdornerProperty) as TemplatedAdornerEx;
                    if (show && (adorner == null))
                    {
                        ControlTemplate errorTemplate = GetErrorTemplate(visual);
                        if (errorTemplate == null)
                        {
                            errorTemplate = GetErrorTemplate(targetElement);
                        }
                        if (errorTemplate != null)
                        {
                            adorner = new TemplatedAdornerEx(visual, errorTemplate);
                            adornerLayer.Add(adorner);
                            visual.SetValue(ValidationAdornerProperty, adorner);
                        }
                    }
                    else if (!show && (adorner != null))
                    {
                        adorner.ClearChild();
                        adornerLayer.Remove(adorner);
                        visual.ClearValue(ValidationAdornerProperty);
                    }
                }
            }
        }

        /// <summary>
        /// Returns true if the error collection for a control contains a databinding error
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        private static bool HasDataBindingError(DependencyObject element)
        {
            // check if the error collection contains a databinding error
            ValidationErrorExCollection internalCollection = GetErrorsInternal(element);

            if (internalCollection != null)
            {
                List<ValidationErrorEx> removeErrList = new List<ValidationErrorEx>();

                foreach (ValidationErrorEx err in internalCollection)
                {
                    if (err.ErrorFromDatabindingContext)
                        return true;
                }
            }

            return false;

        }
        #endregion

        #region static error provider helper methods
        /// <summary>
        /// Gets the instance of the parent ErrorProvider control if any
        /// </summary>
        /// <param name="element">Element which parent error provider control should be returned</param>
        /// <remarks>This may also return null if the element is not part of a visual tree</remarks>
        public static ErrorProvider GetErrorProviderForElement(DependencyObject element)
        {
            return UIHelper.TryFindParent<ErrorProvider>(element);
        }

        /// <summary>
        /// Gets the topmost error provider control instance in the visual tree
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        public static ErrorProvider GetTopMostErrorProviderForElement(DependencyObject element)
        {
            ErrorProvider provider = GetErrorProviderForElement(element);
            ErrorProvider parentProvider = null;

            do
            {
                parentProvider = GetErrorProviderForElement(provider);

                if (parentProvider != null)
                    provider = parentProvider;

            } while (parentProvider != null);

            return provider;
        }

        /// <summary>
        /// Find databindings with validation rules and subscribes to the error collection changes event
        /// </summary>
        /// <param name="element"></param>
        private static bool HasDataBindingWithValidationRules(DependencyObject element)
        {
            FrameworkElement felement = element as FrameworkElement;

            if (felement != null)
            {
                // get field info for type
                FieldInfo[] properties = felement.GetType().GetFields(BindingFlags.Public | BindingFlags.GetProperty | BindingFlags.Static | BindingFlags.FlattenHierarchy);

                // find all dependency properties of the type
                // and check for data bindings
                foreach (FieldInfo field in properties)
                {
                    if (field.FieldType == typeof(DependencyProperty))
                    {
                        DependencyProperty dp = (DependencyProperty)field.GetValue(null);
                        if (BindingOperations.IsDataBound(felement, dp))
                        {
                            BindingExpression bindingExpression = BindingOperations.GetBindingExpression(felement, dp);

                            if (bindingExpression != null)
                            {
                                if (bindingExpression.ParentBinding.ValidatesOnExceptions || bindingExpression.ParentBinding.ValidatesOnExceptions ||
                                    (bindingExpression.ParentBinding.ValidationRules != null && bindingExpression.ParentBinding.ValidationRules.Count > 0))
                                {
                                    return true;
                                }
                            }
                        }
                    }
                }
            }

            return false;
        }
        #endregion

        #region Error provider instance scope
        /// <summary>
        /// Call this method to force a validation on all controls in the error provider scope
        /// </summary>
        public void Validate()
        {
            _change_isvalid = false;
            ClearAllErrorsFromProviderElements();
            _change_isvalid = true;

            if (_associated_control_data.Count > 0)
            {
                foreach (DependencyObject element in _associated_control_data.Keys)
                {
                    CustomValidateElement(element);
                }
            }


            if (_associated_control_data.Count > 0)
            {
                // first validate all data bindings by calling UpdateSource()
                foreach (DependencyObject element in _associated_control_data.Keys)
                {
                    if (_associated_control_data[element].HasDataBinding)
                    {
                        foreach (BindingExpression expr in _associated_control_data[element].DataBindingExpressions)
                        {
                            // this will force data bound validation
                            // if errors occur, we will catch them with our error collection changed event handler
                            if (expr != null)
                            {
                                try
                                {
                                    expr.UpdateSource();
                                }catch(Exception ex)
                                {
                                    Debug.Write("#### ErrorProvider.UpdateSource() Exception");
                                    Debug.WriteLine(ex);
                                }
                            }
                        }
                    }
                }
            }


            OnValidationCompleted(new EventArgs());
        }

        /// <summary>
        /// Validates all child elements.
        /// </summary>
        /// <param name="parentElement">parent element to retrieve children from</param>
        public void ValidateScope(DependencyObject parentElement)
        {
            _change_isvalid = false;
            ClearAllErrorsFromProviderElements();
            _change_isvalid = true;

            if (_associated_control_data.Count > 0)
            {
                // first validate all data bindings by calling UpdateSource()
                foreach (DependencyObject element in _associated_control_data.Keys)
                {
                    if (_associated_control_data[element].HasDataBinding && UIHelper.IsVisualChild(parentElement, element))
                    {
                        foreach (BindingExpression expr in _associated_control_data[element].DataBindingExpressions)
                        {
                            // this will force data bound validation
                            // if errors occur, we will catch them with our error collection changed event handler
                            if (expr != null)
                                expr.UpdateSource();
                        }
                    }

                    if (UIHelper.IsVisualChild(parentElement, element))
                        CustomValidateElement(element);
                }
            }


            OnValidationCompleted(new EventArgs());
        }

        /// <summary>
        /// Custom validate element
        /// </summary>
        /// <param name="element"></param>
        private void CustomValidateElement(DependencyObject element)
        {
            _change_isvalid = false;
            RemoveElementNonDataBindingErrors(element);


            CustomValidationEventArgs e = new CustomValidationEventArgs();

            if (element is ContentElement)
            {
                ((ContentElement)element).RaiseEvent(e);
            }
            else if (element is UIElement)
            {
                ((UIElement)element).RaiseEvent(e);
            }
            else if (element is UIElement3D)
            {
                ((UIElement3D)element).RaiseEvent(e);
            }


            if (!e.Result.IsValid)
            {
                ValidationErrorEx errorEx = new ValidationErrorEx(element, e.Result.ErrorContent, null, this);
                AddValidationError(errorEx, element);

                if (GetReportsToErrorProvider(element))
                {
                    ErrorInfo errInfo = new ErrorInfo();
                    errInfo.Error = errorEx;
                    errInfo.SourceControl = element;
                    errInfo.SourceName = "";

                    errInfo.ItemIndex = CalcItemIndex();

                    if (element is FrameworkElement)
                        errInfo.SourceName = ((FrameworkElement) element).Name;

                    errInfo.ValidationGroupParent = null;

                    string sSourceName = GetErrorSourceName(element);
                    if (!string.IsNullOrEmpty(sSourceName))
                        errInfo.SourceName = sSourceName;

                    DependencyObject grpParent = GetValidationGroupParentControl(element);
                    errInfo.ValidationGroupParent = grpParent;

                    _error_provider_error_list.Add(errInfo);
                }
            }
            _change_isvalid = true;
            SetIsValid();
        }

        /// <summary>
        /// Gets a flag indicating if the current error provider instance has
        /// control meta information set
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        internal bool ContainsElementKey(DependencyObject element)
        {
            return _associated_control_data.ContainsKey(element);
        }
        /// <summary>
        /// Checks for data bindings in the element if the internal dictionary does not contain a key for the element
        /// </summary>
        /// <param name="element"></param>
        internal void InspectElement(DependencyObject element)
        {
            if (!ContainsElementKey(element))
            {
                // check if the elements' dependency properties contains a binding
                // with validation rules
                RegisterDataBoundErrorChanged(element);
            }
        }

        private void UnregisterAllControls()
        {
            if (_associated_control_data.Count > 0)
            {
                foreach (DependencyObject element in _associated_control_data.Keys)
                {
                    bool bManualHook = GetManualValidationHook(element);
                    DependencyPropertyDescriptor descr = DependencyPropertyDescriptor.FromProperty(System.Windows.Controls.Validation.ErrorsProperty, element.GetType());

                    if (bManualHook || _associated_control_data[element].HasDataBinding)
                    {
                        descr.RemoveValueChanged(element, OnValidationErrorChangedHandler);
                    }

                    ((FrameworkElement) element).LostFocus -= ErrorElement_FocusLost;
                    ((FrameworkElement) element).GotFocus -= ErrorElement_FocusGained;

                    //_unlinked_elements.Add(element);
                    ClearControlMaps(element);

                }
            }

            _associated_control_data.Clear();
        }

        private void ClearControlMaps(DependencyObject element)
        {

        }

        public void UnRegisterControl(DependencyObject element)
        {
            if (_associated_control_data.ContainsKey(element))
            {
                bool bManualHook = GetManualValidationHook(element);
                DependencyPropertyDescriptor descr = DependencyPropertyDescriptor.FromProperty(System.Windows.Controls.Validation.ErrorsProperty, element.GetType());

                if (bManualHook || _associated_control_data[element].HasDataBinding)
                {
                    descr.RemoveValueChanged(element, OnValidationErrorChangedHandler);
                }

                _associated_control_data.Remove(element);

                ClearControlMaps(element);
            }
        }

        public void UnRegisterControls(List<DependencyObject> elements)
        {
            foreach(DependencyObject element in elements)
            {
                UnRegisterControl(element);
            }
        }

        /// <summary>
        /// Find databindings with validation rules and subscribes to the error collection changes event
        /// </summary>
        /// <param name="element"></param>
        private void RegisterDataBoundErrorChanged(DependencyObject element)
        {
            FrameworkElement felement = element as FrameworkElement;

            if (felement != null)
            {
               
                bool bManualHook = GetManualValidationHook(felement);
                DependencyPropertyDescriptor descr = DependencyPropertyDescriptor.FromProperty(System.Windows.Controls.Validation.ErrorsProperty, felement.GetType());

                if(bManualHook)
                {
                    if (!ContainsElementKey(element))
                        _associated_control_data[element] = new ErrorControlMetaData();

                    _associated_control_data[element].IsManualValidationHook = true;

                    descr.AddValueChanged(felement, OnValidationErrorChangedHandler);
                    _associated_control_data[element].ValidationErrorsChangedHandler = OnValidationErrorChangedHandler;
                }

                // get field info for type
                FieldInfo[] properties = felement.GetType().GetFields(BindingFlags.Public | BindingFlags.GetProperty | BindingFlags.Static | BindingFlags.FlattenHierarchy);

                // find all dependency properties of the type
                // and check for data bindings
                foreach (FieldInfo field in properties)
                {
                    if (field.FieldType == typeof(DependencyProperty))
                    {
                        DependencyProperty dp = (DependencyProperty)field.GetValue(null);
                        if (BindingOperations.IsDataBound(felement, dp))
                        {
                            BindingExpression bindingExpression = BindingOperations.GetBindingExpression(felement, dp);

                            if (bindingExpression != null)
                            {
                                if (bindingExpression.ParentBinding.ValidatesOnExceptions || bindingExpression.ParentBinding.ValidatesOnExceptions ||
                                    (bindingExpression.ParentBinding.ValidationRules != null && bindingExpression.ParentBinding.ValidationRules.Count > 0))
                                {
                                    if (!ContainsElementKey(element))
                                        _associated_control_data[element] = new ErrorControlMetaData();

                                    _associated_control_data[element].HasDataBinding = true;
                                    _associated_control_data[element].IsManualValidationHook = false;

                                    if (_associated_control_data[element].ValidationErrorsChangedHandler == null)
                                    {
                                        if(_manual_validation_hook.Contains(felement))
                                        {
                                            // clear the manual validation hook value since this control 
                                            // has a data binding associated
                                            _manual_validation_hook.Remove(felement);
                                            SetManualValidationHook(felement, false);
                                        }

                                        descr.AddValueChanged(felement, OnValidationErrorChangedHandler);
                                        _associated_control_data[element].ValidationErrorsChangedHandler = OnValidationErrorChangedHandler;
                                    }

                                    if (_associated_control_data[element].DataBindingExpressions == null)
                                    {
                                        _associated_control_data[element].DataBindingExpressions = new List<BindingExpression>();
                                    }

                                    if (!_associated_control_data[element].DataBindingExpressions.Contains(bindingExpression))
                                        _associated_control_data[element].DataBindingExpressions.Add(bindingExpression);
                                        
                                    //return; // no need to inspect further more dependency properties
                                }
                            }
                        }
                    }
                }

                // no validation data binding found
                if (!ContainsElementKey(element))
                    _associated_control_data[element] = new ErrorControlMetaData();
            }
        }

        /// <summary>
        /// Unregister manual validation error eventing hook
        /// </summary>
        /// <param name="element"></param>
        private void UnManualRegisterDataBoundErrorChanged(DependencyObject element)
        {
            if(_manual_validation_hook.Contains(element))
            {
                if (ContainsElementKey(element) && 
                    _associated_control_data[element].IsManualValidationHook && 
                    (_associated_control_data[element].ValidationErrorsChangedHandler != null))
                {
                    DependencyPropertyDescriptor descr = DependencyPropertyDescriptor.FromProperty(System.Windows.Controls.Validation.ErrorsProperty, element.GetType());
                    if(descr != null)
                        descr.RemoveValueChanged(element, OnValidationErrorChangedHandler);

                    _associated_control_data[element].ValidationErrorsChangedHandler = null;
                    _associated_control_data[element].IsManualValidationHook = false;

                    _manual_validation_hook.Remove(element);
                }
            }
        }

        public void ClearAllErrors()
        {
            ClearAllErrorsFromProviderElements();
            _error_provider_error_list.Clear();

        }
        private void ClearAllErrorsFromProviderElements()
        {
            if (_associated_control_data.Count > 0)
            {
                foreach (DependencyObject element in _associated_control_data.Keys)
                {
                    RemoveElementAllErrors(element);
                }
            }

            if(_virtualized_error_store.ContainsKey(this))
            {
                _virtualized_error_store[this].Clear();
            }
        }

        private void RemoveElementDataBindingErrors(DependencyObject element)
        {
            List<ErrorInfo> removeList = new List<ErrorInfo>();

            foreach(ErrorInfo info in _error_provider_error_list)
            {
                if (info.SourceControl == element && info.Error.ErrorFromDatabindingContext)
                    removeList.Add(info);
            }

            foreach (ErrorInfo delError in removeList)
                _error_provider_error_list.Remove(delError);

            // remove databinding errors from the attached error collection property
            ValidationErrorExCollection internalCollection = GetErrorsInternal(element);

            if (internalCollection != null)
            {
                List<ValidationErrorEx> removeErrList = new List<ValidationErrorEx>();

                foreach (ValidationErrorEx err in internalCollection)
                {
                    if (err.ErrorFromDatabindingContext)
                        removeErrList.Add(err);
                }

                foreach (ValidationErrorEx delError in removeErrList)
                    RemoveValidationError(delError, element);

            }

            SetIsValid();
        }

        public void ClearElementNonDatabindingErrors(DependencyObject element)
        {
            _change_isvalid = false;
            RemoveElementNonDataBindingErrors(element);
            _change_isvalid = true;
            SetIsValid();
        }

        private void RemoveElementNonDataBindingErrors(DependencyObject element)
        {
            List<ErrorInfo> removeList = new List<ErrorInfo>();

            foreach (ErrorInfo info in _error_provider_error_list)
            {
                if (info.SourceControl == element && !info.Error.ErrorFromDatabindingContext && !_custom_error_list.Contains(info))
                    removeList.Add(info);
            }

            foreach (ErrorInfo delError in removeList)
                _error_provider_error_list.Remove(delError);


            // remove databinding errors from the attached error collection property
            ValidationErrorExCollection internalCollection = GetErrorsInternal(element);

            if (internalCollection != null)
            {
                List<ValidationErrorEx> removeErrList = new List<ValidationErrorEx>();

                foreach (ValidationErrorEx err in internalCollection)
                {
                    if (!err.ErrorFromDatabindingContext)
                        removeErrList.Add(err);
                }

                foreach (ValidationErrorEx delError in removeErrList)
                    RemoveValidationError(delError, element);

            }

            SetIsValid();
        }

        public void ClearElementAllErrors(DependencyObject element)
        {
            _change_isvalid = false;
            RemoveElementAllErrors(element);
            _change_isvalid = true;
            SetIsValid();
        }

        private void RemoveElementAllErrors(DependencyObject element)
        {
            List<ErrorInfo> removeList = new List<ErrorInfo>();

            foreach (ErrorInfo info in _error_provider_error_list)
            {
                if (info.SourceControl == element && !_custom_error_list.Contains(info))
                    removeList.Add(info);
            }

            foreach (ErrorInfo delError in removeList)
                _error_provider_error_list.Remove(delError);

            // remove databinding errors from the attached error collection property
            ValidationErrorExCollection internalCollection = GetErrorsInternal(element);

            if (internalCollection != null)
            {
                List<ValidationErrorEx> removeErrList = new List<ValidationErrorEx>();

                foreach (ValidationErrorEx err in internalCollection)
                {
                        removeErrList.Add(err);
                }

                foreach (ValidationErrorEx delError in removeErrList)
                    RemoveValidationError(delError, element);

            }

            SetIsValid();
        }

        /// <summary>
        /// Sets the internal key dependency proeprty for the IsValid falg
        /// </summary>
        private void SetIsValid()
        {
            if (!_change_isvalid)
                return;

            bool bOldVal = IsValid;
            bool newVal = false;

            if(CustomDataAffectsIsValid)
            {
                ErrorCount = _error_provider_error_list.Count;

                if(_virtualized_error_store.ContainsKey(this))
                {
                    ErrorCount += _virtualized_error_store[this].Sum(ic => ic.Value.Sum(di => di.Value.Sum(ei => ei.Value.Count())));
                }
                newVal = ErrorCount <= 0;
            }
            else
            {
                List<ErrorInfo> tmpList = new List<ErrorInfo>();
                foreach(ErrorInfo check in _error_provider_error_list)
                {
                    if (!_custom_error_list.Contains(check))
                        tmpList.Add(check);
                }

                ErrorCount = tmpList.Count;

                if (_virtualized_error_store.ContainsKey(this))
                {
                    ErrorCount += _virtualized_error_store[this].Sum(ic => ic.Value.Sum(di => di.Value.Sum(ei => ei.Value.Count( err_type => _custom_error_list.SingleOrDefault(ci => ci.Error == err_type.Key) == null))));
                }

                newVal = ErrorCount <= 0;
            }

            if (newVal != bOldVal)
            {
                SetValue(IsValidPropertyKey, newVal);
                OnIsValidChanged(new EventArgs());
            }
        }

        /// <summary>
        /// Checks for unlinked controls and registeres any if found
        /// </summary>
        public void CheckForUnlinkedControls()
        {
            // try to resolve visual tree and find unlinked controls for this error provider
            List<DependencyObject> resolvedElement = new List<DependencyObject>();
            foreach (DependencyObject elem in _unlinked_elements)
            {
                ErrorProvider parentProvider = GetErrorProviderForElement(elem);

                if (parentProvider != null && parentProvider == this)
                {
                    if (!_associated_control_data.ContainsKey(elem))
                    {
                        _associated_control_data[elem] = new ErrorControlMetaData();
                        _associated_control_data[elem].HasDataBinding = false;
                    }

                    resolvedElement.Add(elem);

                }
            }

            foreach (DependencyObject elem in resolvedElement)
            {
                
                _unlinked_elements.Remove(elem);
            }

            if (_associated_control_data.Count > 0)
            {
                // after loading event of error provider
                // loop through all registered elements and check for the 
                // data binding event
                foreach (DependencyObject elemet in _associated_control_data.Keys)
                {
                    RegisterDataBoundErrorChanged(elemet);
                }
            }
        }


        
        /// <summary>
        /// Rais ValidationCompleted event
        /// </summary>
        /// <param name="e"></param>
        protected void OnValidationCompleted(EventArgs e)
        {
            if (ValidationCompleted != null)
                ValidationCompleted(this, e);
        }

        /// <summary>
        /// Rais IsValidChanged event
        /// </summary>
        /// <param name="e"></param>
        protected void OnIsValidChanged(EventArgs e)
        {
            if (IsValidChanged != null)
                IsValidChanged(this, e);
        }

        #region Custom error/warning/information entry management of the provider

        public void ClearCustomErrors()
        {
            ClearCustoms(ErrorInfoType.Error);
        }

        public void ClearCustomWarnings()
        {
            ClearCustoms(ErrorInfoType.Warning);
        }

        public void ClearCustomInformations()
        {
            ClearCustoms(ErrorInfoType.Information);
        }

        public void ClearAllCustoms()
        {
            List<ErrorInfo> deleteList = new List<ErrorInfo>();

            foreach (ErrorInfo inspectInfo in _custom_error_list)
            {
                deleteList.Add(inspectInfo);
            }

            foreach (ErrorInfo delInfo in deleteList)
            {
                _custom_error_list.Remove(delInfo);

                if (_error_provider_error_list.Contains(delInfo))
                    _error_provider_error_list.Remove(delInfo);
            }

            SetIsValid();
        }

        public void ClearAllCustoms(DependencyObject source_control)
        {
            List<ErrorInfo> deleteList = new List<ErrorInfo>();

            foreach (ErrorInfo inspectInfo in _custom_error_list)
            {
                if(inspectInfo.SourceControl == source_control)
                    deleteList.Add(inspectInfo);
            }

            foreach (ErrorInfo delInfo in deleteList)
            {
                _custom_error_list.Remove(delInfo);

                if (_error_provider_error_list.Contains(delInfo))
                    _error_provider_error_list.Remove(delInfo);
            }

            SetIsValid();
        }

        public void AddCustomError(string source_name, DependencyObject source_control, DependencyObject validation_group_parent,
            string error_message)
        {
            AddCustom(ErrorInfoType.Error, source_name, source_control, validation_group_parent, error_message);
        }

        public void AddCustomWarning(string source_name, DependencyObject source_control, DependencyObject validation_group_parent,
            string error_message)
        {
            AddCustom(ErrorInfoType.Warning, source_name, source_control, validation_group_parent, error_message);
        }

        public void AddCustomInformation(string source_name, DependencyObject source_control, DependencyObject validation_group_parent,
            string error_message)
        {
            AddCustom(ErrorInfoType.Information, source_name, source_control, validation_group_parent, error_message);
        }

        public void RemoveCustom(ErrorInfo errInfo)
        {
            if(_custom_error_list.Contains(errInfo))
            {
                _custom_error_list.Remove(errInfo);

                if(_error_provider_error_list.Contains(errInfo))
                {
                    _error_provider_error_list.Remove(errInfo);
                }
            }

            SetIsValid();
        }

        private void AddCustom(ErrorInfoType type, string source_name, DependencyObject source_control, DependencyObject validation_group_parent,
            string error_message)
        {
            DependencyObject biningInError = source_control;
            if (biningInError == null)
                biningInError = new TextBlock();

            // create non-databound error
            ValidationErrorEx error = new ValidationErrorEx(biningInError, this);
            error.ErrorContent = error_message;

            ErrorInfo errInfo = new ErrorInfo();
            errInfo.Error = error;
            errInfo.SourceControl = source_control;
            errInfo.SourceName = source_name;
            errInfo.ValidationGroupParent = validation_group_parent;
            errInfo.Type = type;

            errInfo.ItemIndex = CalcItemIndex();

            _custom_error_list.Add(errInfo);
            _error_provider_error_list.Add(errInfo);

            SetIsValid();
        }

        private void ClearCustoms(ErrorInfoType type)
        {
            List<ErrorInfo> deleteList = new List<ErrorInfo>();

            foreach (ErrorInfo inspectInfo in _custom_error_list)
            {
                if (inspectInfo.Type == type)
                    deleteList.Add(inspectInfo);
            }

            foreach (ErrorInfo delInfo in deleteList)
            {
                if (_custom_error_list.Contains(delInfo))
                    _custom_error_list.Remove(delInfo);

                if (_error_provider_error_list.Contains(delInfo))
                    _error_provider_error_list.Remove(delInfo);
            }

            SetIsValid();
        }

        private int CalcItemIndex()
        {
            int nRet = 1;

            if(_error_provider_error_list.Count > 0)
            {
                nRet = _error_provider_error_list[_error_provider_error_list.Count - 1].ItemIndex + 1;
            }

            return nRet;
        }
        #endregion

        #endregion

        #region IDisposable Members

        private void GenerateListSnapshots()
        {
            _instance_snapshot_unlinked_elements = _unlinked_elements.Select(o => o).ToList();
            _instance_snapshot_manual_validation_hook = _manual_validation_hook.Select(o => o).ToList();
        }

        public void Dispose()
        {
            ClearAllErrors();
            UnregisterAllControls();
            //_unlinked_elements.Clear();
            _associated_control_data.Clear();
            //_manual_validation_hook.Clear();

            List<DependencyObject> rem_object = new List<DependencyObject>();

            foreach(DependencyObject obj in _unlinked_elements)
            {
                if (!_instance_snapshot_unlinked_elements.Contains(obj))
                    rem_object.Add(obj);
            }

            foreach (DependencyObject del in rem_object)
            {
                ((FrameworkElement)del).LostFocus -= ErrorElement_FocusLost;
                ((FrameworkElement)del).GotFocus -= ErrorElement_FocusGained;
                _unlinked_elements.Remove(del);
            }

            rem_object.Clear();

            foreach (DependencyObject obj in _manual_validation_hook)
            {
                if (!_instance_snapshot_manual_validation_hook.Contains(obj))
                    rem_object.Add(obj);
            }

            foreach (DependencyObject del in rem_object)
            {
                DependencyPropertyDescriptor descr = DependencyPropertyDescriptor.FromProperty(System.Windows.Controls.Validation.ErrorsProperty, del.GetType());
                if (descr != null)
                    descr.RemoveValueChanged(del, OnValidationErrorChangedHandler);

                _manual_validation_hook.Remove(del);
            }

            _container_name_dictionary.Clear();

            _instance_snapshot_unlinked_elements.Clear();
            _instance_snapshot_manual_validation_hook.Clear();

            EventHelper.RemoveAllCollectionChangedEventHandlers(_error_provider_error_list);
            EventHelper.RemoveAllCollectionChangedEventHandlers(_custom_error_list);

            _associated_control_data.Clear();

            _unlinked_elements.Clear();
            _manual_validation_hook.Clear();

            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if (!this._disposed)
            {
                BindingOperations.ClearAllBindings(this);
                _disposed = true;

            }
        }

        private bool _disposed = false;

        #endregion

        #endregion

        #region Event Handler

        #region static attached property event handler
        /// <summary>
        /// Called if the register property changes
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void OnRegisterChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue != null && e.NewValue is bool)
            {
                if( (bool)e.NewValue)
                {
                    // Note: If the XAML parser sets the register property
                    // the data bindings may not be set at this time
                    // So make sure to loop through all registered controls
                    // after error provider loading and check for data bindings
                    ErrorProvider parentProvider = GetErrorProviderForElement(d);
                    if (parentProvider != null)
                    {
                        parentProvider.RegisterDataBoundErrorChanged(d);

                        if (parentProvider.ContainsElementKey(d))
                            if (_unlinked_elements.Contains(d))
                                _unlinked_elements.Remove(d);

                    }
                    else
                    {
                        if(!_unlinked_elements.Contains(d))
                            _unlinked_elements.Add(d);
                    }

                }  
            }
        }

        /// <summary>
        /// Called if the manual validation hook property changes
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void OnManualValidationHookChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if ((e.NewValue != null && e.NewValue is bool) && (e.OldValue != null && e.OldValue is bool))
            {
                if ((bool)e.NewValue && !(bool)e.OldValue)
                {
                    // Note: If the XAML parser sets the register property
                    // the data bindings may not be set at this time
                    // So make sure to loop through all registered controls
                    // after error provider loading and check for data bindings
                    ErrorProvider parentProvider = GetErrorProviderForElement(d);
                    if (parentProvider != null)
                    {
                        if (!_manual_validation_hook.Contains(d))
                            _manual_validation_hook.Add(d);

                        parentProvider.RegisterDataBoundErrorChanged(d);
                    }
                    else
                    {
                        if (!_manual_validation_hook.Contains(d))
                            _manual_validation_hook.Add(d);
                    }
                }
                else if (!(bool)e.NewValue && (bool)e.OldValue)
                {
                    ErrorProvider parentProvider = GetErrorProviderForElement(d);
                    if (parentProvider != null)
                    {
                        parentProvider.UnManualRegisterDataBoundErrorChanged(d);

                        if (!parentProvider.ContainsElementKey(d))
                            _manual_validation_hook.Remove(d);
                    }
                }
            }
        }

        /// <summary>
        /// Called if the reports to error provider property has been changed
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void OnReportsToErrorProviderChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
        }


        /// <summary>
        /// Called if the register property changes
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void OnCustomValidationTriggerChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue != null && e.NewValue is CustomValidationTrigger)
            {
                ErrorProvider parentProvider = GetErrorProviderForElement(d);
                if (parentProvider != null)
                {
                    parentProvider.RegisterDataBoundErrorChanged(d);

                    if (((CustomValidationTrigger) e.NewValue) == CustomValidationTrigger.Explicit)
                    {
                        if(d is FrameworkElement)
                        {
                            ((FrameworkElement) d).LostFocus -= parentProvider.ErrorElement_FocusLost;
                            ((FrameworkElement) d).GotFocus -= parentProvider.ErrorElement_FocusGained;
                        }
                    }
                    else
                    {
                        if (d is FrameworkElement)
                        {
                            ((FrameworkElement)d).LostFocus += new RoutedEventHandler(parentProvider.ErrorElement_FocusLost);
                            ((FrameworkElement)d).GotFocus += new RoutedEventHandler(parentProvider.ErrorElement_FocusGained);
                        }
                    }
                    
                    if (parentProvider.ContainsElementKey(d))
                        if (_unlinked_elements.Contains(d))
                            _unlinked_elements.Remove(d);
                }
                else
                {
                    if (!_unlinked_elements.Contains(d))
                        _unlinked_elements.Add(d);
                }
            }
        }
        

        /// <summary>
        /// Called if the validation group parent property changes
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void OnValidationGroupParentControlChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (e.OldValue != null && e.OldValue is DependencyObject)
            {
                ValidationErrorExCollection internalCollection = GetErrorsInternal(d);

                if (internalCollection != null)
                {
                    // remove the errors from the old validationgroup parent
                    foreach (ValidationErrorEx error in internalCollection)
                    {
                        RemoveValidationError(error, e.OldValue as DependencyObject);
                    }
                }
            }

            if (e.NewValue != null && e.NewValue is DependencyObject)
            {
                ValidationErrorExCollection internalCollection = GetErrorsInternal(d);

                if(internalCollection != null)
                {
                    // copy the errors to the new validationgroup parent
                    foreach(ValidationErrorEx error in internalCollection)
                    {
                        AddValidationError(error, e.NewValue as DependencyObject);
                    }
                }
            }
        }

        /// <summary>
        /// Called if the validation group parent property changes
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void OnValidationGroupParentChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue != null && e.NewValue is string)
            {
                ErrorProvider errorProvider = GetTopMostErrorProviderForElement(d);

                if (errorProvider != null)
                {
                    DependencyObject element = UIHelper.FindVisualChildByName<DependencyObject>(errorProvider, e.NewValue as string);

                    SetValidationGroupParentControl(d, element);
                }
            } 
            else
            {
                SetValidationGroupParentControl(d, null);
            }
        }

        /// <summary>
        /// Called if the validation group parent type changes
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        public static void OnValidationGroupParentTypeChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue != null && e.NewValue is Type)
            {
                SetValidationGroupParentType(d, (Type)e.NewValue);
            }
            else
            {
                SetValidationGroupParentType(d, null);
            }
        }

        /// <summary>
        /// Called if the register property changes
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void OnErrorSourceNameChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (!object.Equals(e.OldValue, e.NewValue))
            {
                ErrorProvider parentProvider = GetErrorProviderForElement(d);

                if(parentProvider != null)
                {
                    foreach(ErrorInfo info in parentProvider.ProviderErrorList)
                    {
                        if (e.NewValue == null)
                            info.SourceName = string.Empty;
                        else
                            info.SourceName = Convert.ToString(e.NewValue);
                    }
                }
            }
        }

        /// <summary>
        /// Called if the internal errors collection changes
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void OnErrorsInternalChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ValidationErrorExCollection newValue = e.NewValue as ValidationErrorExCollection;
            if (newValue != null)
            {
                d.SetValue(ErrorsPropertyKey, new ReadOnlyObservableCollection<ValidationErrorEx>(newValue));
            }
            else
            {
                d.ClearValue(ErrorsPropertyKey);
            }
        }

        #region Experimenting - Virtualized Items enivonment
        private static void OnParentVirtualizingItemsControlChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ItemsControl newValue = e.NewValue as ItemsControl;
            ItemsControl oldValue = e.OldValue as ItemsControl;

            #region old code
            //// the _virtualizing_parent_control_reference_count dictionary makes sure,
            //// that the ItemsChanged event handler will only be called ONCE for the
            //// virtualizing parent itemscontrol
            //// the handler will be deregistered if all child controls deregister
            //if (oldValue != null)
            //{
            //    if (_virtualizing_parent_control_reference_count.ContainsKey(oldValue))
            //    {
            //        _virtualizing_parent_control_reference_count[oldValue]--;
            //        if (_virtualizing_parent_control_reference_count[oldValue] == 0)
            //        {
            //            oldValue.ItemContainerGenerator.ItemsChanged -= new ItemsChangedEventHandler(ItemContainerGenerator_ItemsChanged);
            //            _virtualizing_parent_control_reference_count.Remove(oldValue);
            //        }
            //    }
            //    else
            //    {
            //        oldValue.ItemContainerGenerator.ItemsChanged -= new ItemsChangedEventHandler(ItemContainerGenerator_ItemsChanged);
            //    }

            //}

            //if(newValue != null)
            //{
            //    _items_container_generator_link[newValue.ItemContainerGenerator] = newValue;

            //    if (!_virtualizing_parent_control_reference_count.ContainsKey(newValue))
            //    {
            //        _virtualizing_parent_control_reference_count[newValue] = 1;
            //        newValue.ItemContainerGenerator.ItemsChanged += new ItemsChangedEventHandler(ItemContainerGenerator_ItemsChanged);    

            //        if(!GetVirtualItemsRegistered(newValue))
            //        {
            //            RegisterItemContainerEvents(newValue);
            //            newValue.SetValue(VirtualItemsRegisteredKey, true);
            //        }
            //    }
            //    else
            //    {
            //        _virtualizing_parent_control_reference_count[newValue]++;
            //    }

            //}
            #endregion
            #region old code 2
            //if (newValue != null)
            //{
            //    if (d is FrameworkElement)
            //    {
            //        DependencyObject parent_container = GetParentItemContainer(d as FrameworkElement, newValue);

            //        if(parent_container != null)
            //        {
                       
            //            ((FrameworkElement)d).Loaded += new RoutedEventHandler(ErrorProviderVirtualizedControl_Loaded);

            //            if (parent_container is FrameworkElement)
            //            {
            //                if(!_container_name_dictionary.ContainsKey(newValue))
            //                {
            //                    _container_name_dictionary[newValue] = new List<string>();
            //                }

            //                if(!_container_name_dictionary[newValue].Contains(((FrameworkElement)d).Name))
            //                {
            //                    _container_name_dictionary[newValue].Add(((FrameworkElement) d).Name);
            //                }

            //                if (!GetVirtualItemsRegistered(newValue))
            //                {
            //                    if(newValue is DataGrid)
            //                    {
            //                        ((DataGrid)newValue).LoadingRow += new EventHandler<DataGridRowEventArgs>(DataGrid_LoadingRow);
            //                    }
            //                    newValue.SetValue(VirtualItemsRegisteredKey, true);
            //                }

            //                _data_context_link[parent_container as DependencyObject] = ((FrameworkElement)parent_container).DataContext;
            //                _items_control_link[parent_container] = newValue;
            //                _items_control_link[d] = newValue;

            //                if (!_initial_unload_registered.ContainsKey(parent_container))
            //                {
            //                    // wenn das attached proeprty gesetzt wird, dann ist der parent-container bereits geladen
            //                    ((FrameworkElement)parent_container).Unloaded += new RoutedEventHandler(VirtualItemContainer_UnLoaded);
            //                    _initial_unload_registered[parent_container] = true;
            //                }
            //            }
            //        }
            //    }
            //}
            #endregion

            if (e.NewValue != null)
            {
                if (d is FrameworkElement)
                {
                    if(!_container_name_dictionary.ContainsKey(newValue))
                    {
                        _container_name_dictionary[newValue] = new List<string>();
                    }

                    if (!_container_name_dictionary[newValue].Contains(((FrameworkElement)d).Name))
                    {
                        _container_name_dictionary[newValue].Add(((FrameworkElement)d).Name);
                    }

                    VirtualizingStackPanel stack_panel = GetItemsHost(e.NewValue as ItemsControl) as VirtualizingStackPanel;


                    if (stack_panel != null)
                    {
                        if (!GetVirtualItemsRegistered(newValue))
                        {
                            if (VirtualizingStackPanel.GetIsVirtualizing(e.NewValue as ItemsControl))
                            {
                                if(VirtualizingStackPanel.GetVirtualizationMode(e.NewValue as ItemsControl) == VirtualizationMode.Recycling)
                                {
                                    throw new Exception("VirtualizationMode.Recycling is not supported by the ErrorProvider. Change it to VirtualizationMode.Standard!");
                                }

                                VirtualizingStackPanel.AddCleanUpVirtualizedItemHandler(e.NewValue as ItemsControl, OnCleanUpVirtualizedItem);
                                newValue.SetValue(VirtualItemsRegisteredKey, true);
                            }
                        }
                    }
                }
            }
        }
        
        static void OnCleanUpVirtualizedItem(object sender, CleanUpVirtualizedItemEventArgs e)
        {
            foreach(string child_name in _container_name_dictionary[sender as FrameworkElement])
            {
                if(e.UIElement is FrameworkElement)
                {
                    FrameworkElement child = ((FrameworkElement) e.UIElement).FindName(child_name) as FrameworkElement;

                    if(child == null)
                    {
                        child = UIHelper.FindVisualChildByName<FrameworkElement>(e.UIElement, child_name);
                    }

                    if(child != null)
                    {
                        if(GetHasError(child))
                        {
                            e.Cancel = true;
                            return;
                        }
                    }
                    else
                    {
                        int x = 10;
                    }
                }
            }
        }


        //static void DataGrid_LoadingRow(object sender, DataGridRowEventArgs e)
        //{
        //    DataGridRow sender_row = sender as DataGridRow;
        //    if (sender_row != null)
        //        sender_row.Loaded += ErrorProviderVirtualizedControl_Loaded;
        //}

        //private static DependencyObject GetParentItemContainer(FrameworkElement element, ItemsControl ctrl)
        //{
        //    if (ctrl == null || element == null)
        //        return null;

        //    DependencyObject oRet = null;

        //    oRet = ctrl.ItemContainerGenerator.ContainerFromItem(element.DataContext);

        //    if(oRet == null)
        //    {
        //        Type container_type = GetItemContainerGeneratorType(ctrl.ItemContainerGenerator);
        //        oRet = UIHelper.TryFindParent(element, container_type);
        //    }

        //    return oRet;
        //}

        private static Type GetItemContainerGeneratorType(ItemContainerGenerator generator)
        {
            if(_item_container_generator_types.ContainsKey(generator))
            {
                return _item_container_generator_types[generator];
            }

            // use reflection to access private field
            FieldInfo fiType = generator.GetType().GetField("_containerType", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.GetField);
            if(fiType != null)
            {
                Type container_type = fiType.GetValue(generator) as Type;
                if(container_type != null)
                {
                    _item_container_generator_types[generator] = container_type;
                    return container_type;
                }
            }

            return null;
        }

        private static Panel GetItemsHost(ItemsControl ctrl)
        {
            if (ctrl == null)
                return null;

            PropertyInfo fiItemsHost = ctrl.GetType().GetProperty("ItemsHost", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.GetProperty);
            if(fiItemsHost != null)
            {
                return fiItemsHost.GetValue(ctrl, null) as Panel;
            }

            return null;
        }

        
        #region old code
        //static void ItemContainerGenerator_ItemsChanged(object sender, ItemsChangedEventArgs e)
        //{
        //    ItemContainerGenerator items_control_sender = sender as ItemContainerGenerator;

        //    if(items_control_sender != null)
        //    {
        //        Debug.WriteLine("###*** ItemContainerGenerator sent ItemsChanged-Event ...");
        //        if (_items_container_generator_link.ContainsKey(items_control_sender))
        //        {
        //            //RegisterItemContainerEvents(items_control_sender, _items_container_generator_link[items_control_sender].Items.Count);
        //            _items_container_generator_link[items_control_sender].SetValue(VirtualItemsRegisteredKey, false);
        //        }
                
        //    }
        //}

        //static void RegisterItemContainerEvents(ItemsControl items_control)
        //{
        //    if (items_control == null || items_control.Items.Count == 0)
        //        return;

        //    RegisterItemContainerEvents(items_control.ItemContainerGenerator, items_control.Items.Count);
        //}

        //static void RegisterItemContainerEvents(ItemContainerGenerator items_control_sender, int nCount)
        //{
        //    if (items_control_sender == null || nCount <= 0)
        //        return;

        //    for (int i = 0; i < nCount; i++)
        //    {
        //        DependencyObject container = items_control_sender.ContainerFromIndex(i);

        //        if (container is FrameworkElement)
        //        {
        //            ((FrameworkElement)container).Loaded += new RoutedEventHandler(VirtualItemContainer_Loaded);
        //            _data_context_link[container as DependencyObject] = ((FrameworkElement)container).DataContext;
        //            if (_items_container_generator_link.ContainsKey(items_control_sender))
        //                _items_control_link[container] = _items_container_generator_link[items_control_sender];
        //        }
        //    }
        //}
        #endregion

        #region old code 2
        //static void ErrorProviderVirtualizedControl_Loaded(object sender, RoutedEventArgs e)
        //{
        //    if (sender != null)
        //    {
        //        Debug.WriteLine("###*** LOADED '" + ((FrameworkElement)sender).Name + "' (" + sender.GetType().ToString().Substring(sender.GetType().ToString().LastIndexOf(".")+1) + "): ");
        //        if (sender is FrameworkElement)
        //        {
        //            ((FrameworkElement)sender).DataContextChanged += new DependencyPropertyChangedEventHandler(ErrorProviderVirtualizedControl_DataContextChanged);
        //            ((FrameworkElement)sender).Loaded -= new RoutedEventHandler(ErrorProviderVirtualizedControl_Loaded);
        //            ((FrameworkElement)sender).Unloaded += new RoutedEventHandler(ErrorProviderVirtualizedControl_UnLoaded);

        //            _data_context_link[sender as DependencyObject] = ((FrameworkElement)sender).DataContext;

        //            if (_items_control_link.ContainsKey(sender as DependencyObject))
        //            {
        //                ItemsControl parent_items_control = _items_control_link[sender as DependencyObject];
        //                RestoreVirtualizedErrors(parent_items_control, sender as FrameworkElement, _data_context_link[sender as DependencyObject]);
        //            }
        //        }
        //    }
        //}

        //static void ErrorProviderVirtualizedControl_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        //{
        //    if (sender != null)
        //    {
        //        Debug.WriteLine("###*** DATACONTEXT CHANGED '" + ((FrameworkElement) sender).Name + "' (" + sender.GetType().ToString().Substring(sender.GetType().ToString().LastIndexOf(".") + 1) + "): ");
        //        Debug.WriteLine("###*** From: " + e.OldValue == null ? "null" : e.OldValue.GetType().ToString().Substring(e.OldValue.GetType().ToString().LastIndexOf(".") + 1) + " to: " +
        //                                                                        e.NewValue == null ? "null" : e.NewValue.GetType().ToString().Substring(e.NewValue.GetType().ToString().LastIndexOf(".") + 1));
        //        Debug.WriteLine("###*** " + (GetHasError(sender as DependencyObject) ? " HAS ERRORS" : " NO errors "));
        //    }
        //}

        //static void ErrorProviderVirtualizedControl_UnLoaded(object sender, RoutedEventArgs e)
        //{
        //    if (sender != null)
        //    {
        //        Debug.WriteLine("###*** UNLOADED '" + ((FrameworkElement)sender).Name + "' (" + sender.GetType().ToString().Substring(sender.GetType().ToString().LastIndexOf(".") + 1) + "): ");
        //        if (sender is FrameworkElement)
        //        {
        //            ((FrameworkElement)sender).Loaded += new RoutedEventHandler(ErrorProviderVirtualizedControl_Loaded);
        //            ((FrameworkElement)sender).Unloaded -= new RoutedEventHandler(ErrorProviderVirtualizedControl_UnLoaded);
        //            ((FrameworkElement)sender).DataContextChanged -= new DependencyPropertyChangedEventHandler(ErrorProviderVirtualizedControl_DataContextChanged);

        //            if (_items_control_link.ContainsKey(sender as DependencyObject))
        //            {
        //                ItemsControl parent_items_control = _items_control_link[sender as DependencyObject];

        //                if (parent_items_control != null && _data_context_link.ContainsKey(sender as DependencyObject))
        //                {
        //                    bool still_exists = parent_items_control.Items.Contains(_data_context_link[sender as DependencyObject]);

        //                    if (still_exists)
        //                    {
        //                        Debug.WriteLine("###*** Unloading row ... saving errors");
        //                        SaveVirtualizedErrors(parent_items_control, sender as FrameworkElement, _data_context_link[sender as DependencyObject]);
        //                    }
        //                    else
        //                    {
        //                        Debug.WriteLine("###*** DELETE Item detected");
        //                        ClearVirtualizedErrors(parent_items_control, sender as FrameworkElement, _data_context_link[sender as DependencyObject]);
        //                    }
        //                }
        //            }
        //        }
        //    }
        //}

        //static void VirtualItemContainer_Loaded(object sender, RoutedEventArgs e)
        //{
        //    if(sender != null)
        //    {
        //        Debug.WriteLine("###*** LOADED " + sender.GetType().ToString().Substring(sender.GetType().ToString().LastIndexOf(".")+1) + ": ");
        //        if (sender is FrameworkElement)
        //        {
        //            ((FrameworkElement)sender).Loaded -= new RoutedEventHandler(VirtualItemContainer_Loaded);
        //            ((FrameworkElement)sender).Unloaded += new RoutedEventHandler(VirtualItemContainer_UnLoaded);

        //            _data_context_link[sender as DependencyObject] = ((FrameworkElement) sender).DataContext;

        //            //if(!_items_control_link.ContainsKey(sender as DependencyObject))
        //            //{
        //            //    ItemsControl parent_items_control = UIHelper.TryFindParent<ItemsControl>(sender as DependencyObject) as ItemsControl;
        //            //    if(parent_items_control != null)
        //            //    {
        //            //        _items_control_link[sender as DependencyObject] = parent_items_control;
        //            //    }
        //            //}

        //            //if(_items_control_link.ContainsKey(sender as DependencyObject))
        //            //{
        //            //    //ItemsControl parent_items_control = _items_control_link[sender as DependencyObject];
        //            //    //RestoreVirtualizedErrors(parent_items_control, sender as FrameworkElement, _data_context_link[sender as DependencyObject]);
        //            //}
        //        }
        //    }
        //}

        //static void VirtualItemContainer_UnLoaded(object sender, RoutedEventArgs e)
        //{
        //    if(sender != null)
        //    {
        //        Debug.WriteLine("###*** UNLOADED " + sender.GetType().ToString().Substring(sender.GetType().ToString().LastIndexOf(".")+1) + ": ");
        //        if (sender is FrameworkElement)
        //        {
        //            ((FrameworkElement)sender).Loaded += new RoutedEventHandler(VirtualItemContainer_Loaded);
        //            ((FrameworkElement)sender).Unloaded -= new RoutedEventHandler(VirtualItemContainer_UnLoaded);

        //            //if(_items_control_link.ContainsKey(sender as DependencyObject))
        //            //{
        //            //    ItemsControl parent_items_control = _items_control_link[sender as DependencyObject];

        //            //    if (parent_items_control != null && _data_context_link.ContainsKey(sender as DependencyObject))
        //            //    {
        //            //        bool still_exists = parent_items_control.Items.Contains(_data_context_link[sender as DependencyObject]);
                            
        //            //        if (still_exists)
        //            //        {
        //            //            Debug.WriteLine("###*** Unloading row ... saving errors");
        //            //            SaveVirtualizedErrors(parent_items_control, sender as FrameworkElement, _data_context_link[sender as DependencyObject]);
        //            //        }
        //            //        else
        //            //        {
        //            //            Debug.WriteLine("###*** DELETE Item detected");
        //            //            ClearVirtualizedErrors(parent_items_control, sender as FrameworkElement, _data_context_link[sender as DependencyObject]);
        //            //        }
        //            //    }
        //            //}
        //        }
        //    }
        //}

        //private static void ClearVirtualizedErrors(ItemsControl items_control, FrameworkElement control, object data_item)
        //{
        //    if (!_errorprovider_items_control_link.ContainsKey(items_control))
        //    {
        //        ErrorProvider parentProvider = GetErrorProviderForElement(items_control);
        //        if (parentProvider == null)
        //            return;

        //        _errorprovider_items_control_link[items_control] = parentProvider;
        //    }

        //    ErrorProvider error_provider = _errorprovider_items_control_link[items_control];

        //    if (_virtualized_error_store.ContainsKey(error_provider))
        //    {
        //        if (_virtualized_error_store[error_provider].ContainsKey(items_control))
        //        {
        //            if (_virtualized_error_store[error_provider][items_control].ContainsKey(data_item))
        //            {
        //                if (_virtualized_error_store[error_provider][items_control][data_item].ContainsKey(control.Name))
        //                    _virtualized_error_store[error_provider][items_control][data_item].Remove(control.Name);
                        
        //                if(_virtualized_error_store[error_provider][items_control][data_item].Count <= 0)
        //                    _virtualized_error_store[error_provider][items_control].Remove(data_item);
        //            }
        //        }
        //    }
        //}

        //private static void SaveVirtualizedErrors(ItemsControl items_control, FrameworkElement control, object data_item)
        //{
        //    if (!_errorprovider_items_control_link.ContainsKey(items_control))
        //    {
        //        ErrorProvider parentProvider = GetErrorProviderForElement(items_control);
        //        if (parentProvider == null)
        //            return;

        //        _errorprovider_items_control_link[items_control] = parentProvider;
        //    }

        //    ErrorProvider error_provider = _errorprovider_items_control_link[items_control];

        //    //private static Dictionary<ErrorProvider, Dictionary<ItemsControl, Dictionary<object, Dictionary<string, List<KeyValuePair<ValidationErrorEx, object>>>>>> _virtualized_error_store

        //    if (!_virtualized_error_store.ContainsKey(error_provider))
        //    {
        //        _virtualized_error_store[error_provider] = new Dictionary<ItemsControl, Dictionary<object, Dictionary<string, List<KeyValuePair<ValidationErrorEx, object>>>>>();
        //    }

        //    if (!_virtualized_error_store[error_provider].ContainsKey(items_control))
        //    {
        //        _virtualized_error_store[error_provider][items_control] = new Dictionary<object, Dictionary<string, List<KeyValuePair<ValidationErrorEx, object>>>>();
        //    }

        //    if (!_virtualized_error_store[error_provider][items_control].ContainsKey(data_item))
        //    {
        //        _virtualized_error_store[error_provider][items_control][data_item] = new Dictionary<string, List<KeyValuePair<ValidationErrorEx, object>>>();
        //    }

        //    _virtualized_error_store[error_provider][items_control][data_item].Clear();
        //    if (_container_name_dictionary.ContainsKey(items_control))
        //    {
        //        if (control != null)
        //        {
        //            if (_virtualized_error_store[error_provider][items_control][data_item].ContainsKey(control.Name))
        //                _virtualized_error_store[error_provider][items_control][data_item].Remove(control.Name);

        //            if (GetHasError(control))
        //            {
        //                if(!_virtualized_error_store[error_provider][items_control][data_item].ContainsKey(control.Name))
        //                    _virtualized_error_store[error_provider][items_control][data_item][control.Name] = new List<KeyValuePair<ValidationErrorEx, object>>();

        //                foreach (ValidationErrorEx error in GetErrorsInternal(control))
        //                {
        //                    if (error.Exception == null)
        //                    {
        //                        KeyValuePair<ValidationErrorEx, object> error_info = new KeyValuePair<ValidationErrorEx, object>(error, null);
        //                        _virtualized_error_store[error_provider][items_control][data_item][control.Name].Add(error_info);
        //                    }
        //                }
        //                error_provider.ClearElementAllErrors(control);
        //            }
        //        }
        //    }

        //}

        //private static void RestoreVirtualizedErrors(ItemsControl items_control, FrameworkElement control, object data_item)
        //{
        //    if (!_errorprovider_items_control_link.ContainsKey(items_control))
        //    {
        //        ErrorProvider parentProvider = GetErrorProviderForElement(items_control);
        //        if (parentProvider == null)
        //            return;

        //        _errorprovider_items_control_link[items_control] = parentProvider;
        //    }

        //    ErrorProvider error_provider = _errorprovider_items_control_link[items_control];

        //    if (_virtualized_error_store.ContainsKey(error_provider))
        //    {
        //        if (_virtualized_error_store[error_provider].ContainsKey(items_control))
        //        {
        //            if (_virtualized_error_store[error_provider][items_control].ContainsKey(data_item))
        //            {
        //                if (_virtualized_error_store[error_provider][items_control][data_item].ContainsKey(control.Name))
        //                {
        //                    if (control != null)
        //                    {
        //                        foreach (KeyValuePair<ValidationErrorEx, object> error_info in _virtualized_error_store[error_provider][items_control][data_item][control.Name])
        //                        {
        //                            ValidationErrorEx error = error_info.Key;
        //                            object value_causing_error = error_info.Value;
        //                            AddValidationError(error, control);
        //                        }
        //                    }

        //                    _virtualized_error_store[error_provider][items_control][data_item].Remove(control.Name);
        //                    if(_virtualized_error_store[error_provider][items_control][data_item].Count <= 0)
        //                        _virtualized_error_store[error_provider][items_control].Remove(data_item);
        //                }
        //            }
        //        }
        //    }
        //}
        #endregion


        #endregion

        /// <summary>
        /// Called if the error template changes
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void OnErrorTemplateChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ErrorProvider parentProvider = GetErrorProviderForElement(d);
            if(parentProvider != null)
            {
                parentProvider.InspectElement(d);

                if (parentProvider.ContainsElementKey(d))
                    if (_unlinked_elements.Contains(d))
                        _unlinked_elements.Remove(d);
            }
            else
            {
                if (!_unlinked_elements.Contains(d))
                    _unlinked_elements.Add(d);
            }

            // set this template as validation template for the data bound validation too
            System.Windows.Controls.Validation.SetErrorTemplate(d, e.NewValue as ControlTemplate);

            if (GetHasError(d) && !HasDataBindingError(d))
            {
                ShowValidationAdorner(d, false);
                ShowValidationAdorner(d, true);
            }
        }
        /// <summary>
        /// Called if the validation adorner site changes
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void OnValidationAdornerSiteChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ErrorProvider parentProvider = GetErrorProviderForElement(d);
            if (parentProvider != null)
            {
                parentProvider.InspectElement(d);

                if (parentProvider.ContainsElementKey(d))
                    if (_unlinked_elements.Contains(d))
                        _unlinked_elements.Remove(d);
            }
            else
            {
                if (!_unlinked_elements.Contains(d))
                    _unlinked_elements.Add(d);
            }

            DependencyObject oldValue = (DependencyObject)e.OldValue;
            DependencyObject newValue = (DependencyObject)e.NewValue;
            if (oldValue != null)
            {
                oldValue.ClearValue(ValidationAdornerSiteForProperty);
            }
            if ((newValue != null) && (d != GetValidationAdornerSiteFor(newValue)))
            {
                SetValidationAdornerSiteFor(newValue, d);
            }
            if (GetHasError(d))
            {
                if (oldValue == null)
                {
                    oldValue = d;
                }
                ShowValidationAdornerHelper(d, oldValue, false);
                ShowValidationAdorner(d, true);
            }
        }

        /// <summary>
        /// Called if the validation adorner site for changes
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void OnValidationAdornerSiteForChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ErrorProvider parentProvider = GetErrorProviderForElement(d);
            if (parentProvider != null)
            {
                parentProvider.InspectElement(d);

                if (parentProvider.ContainsElementKey(d))
                    if (_unlinked_elements.Contains(d))
                        _unlinked_elements.Remove(d);
            }
            else
            {
                if (!_unlinked_elements.Contains(d))
                    _unlinked_elements.Add(d);
            }

            DependencyObject oldValue = (DependencyObject)e.OldValue;
            DependencyObject newValue = (DependencyObject)e.NewValue;
            if (oldValue != null)
            {
                oldValue.ClearValue(ValidationAdornerSiteProperty);
            }
            if ((newValue != null) && (d != GetValidationAdornerSite(newValue)))
            {
                SetValidationAdornerSite(newValue, d);
            }
            
        }
        #endregion

        #region Validation error collection changed event handler
        /// <summary>
        /// Called if one of the assiciated controls databound validation produces or clears errors
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnValidationErrorChangedHandler(object sender, EventArgs e)
        {
            DependencyObject element = sender as DependencyObject;

            if(element != null)
            {
                _change_isvalid = false;
                RemoveElementDataBindingErrors(element);

                ReadOnlyObservableCollection<ValidationError> validationErrors = System.Windows.Controls.Validation.GetErrors(element);

                if(validationErrors != null && validationErrors.Count > 0)
                {
                    foreach(ValidationError dbError in validationErrors)
                    {
                        // create a new binding context error ex instance
                        ValidationErrorEx errorEx = new ValidationErrorEx(dbError, this);

                        AddValidationError(errorEx, element);

                        if (GetReportsToErrorProvider(element))
                        {
                            ErrorInfo errorInfo = new ErrorInfo();
                            errorInfo.Error = errorEx;
                            errorInfo.SourceControl = element;

                            errorInfo.ItemIndex = CalcItemIndex();

                            if (sender is FrameworkElement)
                                errorInfo.SourceName = ((FrameworkElement) element).Name;

                            string sSourceName = GetErrorSourceName(element);
                            if (!string.IsNullOrEmpty(sSourceName))
                                errorInfo.SourceName = sSourceName;

                            DependencyObject grpParent = GetValidationGroupParentControl(element);
                            errorInfo.ValidationGroupParent = grpParent;

                            _error_provider_error_list.Add(errorInfo);
                        }
                    }
                }
                _change_isvalid = true;

                SetIsValid();
            }
        }
        #endregion

        #region Error provider handler
        void ErrorProvider_Loaded(object sender, RoutedEventArgs e)
        {
            CheckForUnlinkedControls();
        }

        void ErrorProvider_Unloaded(object sender, RoutedEventArgs e)
        {
            UnregisterAllControls();
        }

        /// <summary>
        /// Called if the element lost its input focus
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void ErrorElement_FocusLost(object sender, RoutedEventArgs e)
        {
            DependencyObject element = sender as DependencyObject;

            if(element != null)
            {
                if( GetCustomValidationTrigger(element) == CustomValidationTrigger.FocusLost)
                {
                    CustomValidateElement(element);
                }
            }
        }
        /// <summary>
        /// Called if the element gained the input focus
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void ErrorElement_FocusGained(object sender, RoutedEventArgs e)
        {
            DependencyObject element = sender as DependencyObject;

            if (element != null)
            {
                if (GetCustomValidationTrigger(element) == CustomValidationTrigger.FocusGained)
                {
                    CustomValidateElement(element);
                }
            }
        }
        #endregion

        #endregion

        #region Properties
        /// <summary>
        /// Gets a flag if the current error provider is valid or not
        /// </summary>
        public bool IsValid
        {
            get
            {
                return (bool)GetValue(IsValidProperty);
            }
        }

        public int ErrorCount
        {
            get { return (int) GetValue(ErrorCountProperty); }
            set { SetValue(ErrorCountProperty, value); }
        }

        /// <summary>
        /// Gets a flag if custom data affects the is valid property
        /// </summary>
        public bool CustomDataAffectsIsValid
        {
            get
            {
                return (bool)GetValue(CustomDataAffectsIsValidProperty);
            }
            set
            {
                SetValue(CustomDataAffectsIsValidProperty, value);
            }
        }

        /// <summary>
        /// Gets the complete cumulated error list of all controls in the error provider
        /// </summary>
        public ObservableCollection<ErrorInfo> ProviderErrorList
        {
            get
            {
                return _error_provider_error_list;
            }
        }
        #endregion

        #region Dependency Properties
        /// <summary>
        /// Attached event for unbound controls to initiate or request validation
        /// </summary>
        public static readonly RoutedEvent CustomValidationEvent;
        /// <summary>
        /// Errors property returning an custom validation trigger property
        /// </summary>
        public static readonly DependencyProperty CustomValidationTriggerProperty;

        /// <summary>
        /// Validation group parent type
        /// </summary>
        private static readonly DependencyProperty ValidationGroupParentTypeProperty;
        /// <summary>
        /// Validation group parent control
        /// </summary>
        private static readonly DependencyProperty ValidationGroupParentControlProperty;
        /// <summary>
        /// Validation group parent control
        /// </summary>
        public static readonly DependencyProperty ValidationGroupParentProperty;

        /// <summary>
        /// Manual validation error hook
        /// </summary>
        public static readonly DependencyProperty ManualValidationHookProperty;
        /// <summary>
        /// Reports error to error provider control
        /// </summary>
        public static readonly DependencyProperty ReportsToErrorProviderProperty;

        /// <summary>
        /// Error source name
        /// </summary>
        public static readonly DependencyProperty ErrorSourceNameProperty;

        /// <summary>
        /// Errors property returning an ReadOnlyObservableCollection
        /// </summary>
        public static readonly DependencyProperty ErrorsProperty;
        /// <summary>
        /// Internal Errors collection for collection setting
        /// </summary>
        internal static readonly DependencyPropertyKey ErrorsPropertyKey;
        /// <summary>
        /// Errors property returning an error count
        /// </summary>
        public static readonly DependencyProperty ErrorCountProperty;

        /// <summary>
        /// Attached property holding the error template content control
        /// </summary>
        public static readonly DependencyProperty ErrorTemplateProperty;

        /// <summary>
        /// Attached property indicating if the control has an error or not
        /// </summary>
        public static readonly DependencyProperty HasErrorProperty;
        /// <summary>
        /// Internal attached property for setting
        /// </summary>
        internal static readonly DependencyPropertyKey HasErrorPropertyKey;

        /// <summary>
        /// Attached property registering the control to the error provider
        /// </summary>
        public static readonly DependencyProperty RegisterProperty;

        /// <summary>
        /// Attached adorner site for property
        /// </summary>
        public static readonly DependencyProperty ValidationAdornerSiteForProperty;
        /// <summary>
        /// Attached adorner site property
        /// </summary>
        public static readonly DependencyProperty ValidationAdornerSiteProperty;
        /// <summary>
        /// Attached internal errors collection
        /// </summary>
        internal static readonly DependencyProperty ValidationErrorsInternalProperty;

        /// <summary>
        /// Internal attached property for managing template changes
        /// </summary>
        private static readonly DependencyProperty ValidationAdornerProperty;

        /// <summary>
        /// Attached property specifying the parent virtualizing items control
        /// </summary>
        private static readonly DependencyProperty ParentVirtualizingItemsControlProperty;
        /// <summary>
        /// Dependency property 
        /// </summary>
        public static readonly DependencyProperty VirtualItemsRegisteredProperty;
        /// <summary>
        /// Private dependency property for setting the IsValid property
        /// </summary>
        private static readonly DependencyPropertyKey VirtualItemsRegisteredKey;

        /// <summary>
        /// Dependency property 
        /// </summary>
        public static readonly DependencyProperty IsValidProperty;
        /// <summary>
        /// Private dependency property for setting the IsValid property
        /// </summary>
        private static readonly DependencyPropertyKey IsValidPropertyKey;

        /// <summary>
        /// Dependency property 
        /// </summary>
        public static readonly DependencyProperty CustomDataAffectsIsValidProperty;

       
        #endregion

        #region events
        public event EventHandler ValidationCompleted;
        public event EventHandler IsValidChanged;
        #endregion

        #region Attribs
        private Dictionary<DependencyObject, ErrorControlMetaData> _associated_control_data = new Dictionary<DependencyObject, ErrorControlMetaData>();
        private List<DependencyObject> _instance_snapshot_unlinked_elements = new List<DependencyObject>();
        private List<DependencyObject> _instance_snapshot_manual_validation_hook = new List<DependencyObject>();

        private ObservableCollection<ErrorInfo> _error_provider_error_list = new ObservableCollection<ErrorInfo>();
        private ObservableCollection<ErrorInfo> _custom_error_list = new ObservableCollection<ErrorInfo>();

        private static List<DependencyObject> _unlinked_elements = new List<DependencyObject>();
        private static List<DependencyObject> _manual_validation_hook = new List<DependencyObject>();

        //                         error provider
        //                                                  virtualizing items control
        //                                                                           data object
        //                                                                                             control name
        //                                                                                                                       validation error 
        //                                                                                                                                           value instance causing the error
        private static Dictionary<ErrorProvider, Dictionary<ItemsControl, Dictionary<object, Dictionary<string, List<KeyValuePair<ValidationErrorEx, object>>>>>> _virtualized_error_store = new Dictionary<ErrorProvider, Dictionary<ItemsControl, Dictionary<object, Dictionary<string, List<KeyValuePair<ValidationErrorEx, object>>>>>>();

        private static Dictionary<FrameworkElement, List<string>> _container_name_dictionary = new Dictionary<FrameworkElement, List<string>>();
        private static Dictionary<ItemContainerGenerator, Type> _item_container_generator_types = new Dictionary<ItemContainerGenerator, Type>();

        //private static Dictionary<DependencyObject, object> _data_context_link = new Dictionary<DependencyObject, object>();
        //private static Dictionary<DependencyObject, ItemsControl> _items_control_link = new Dictionary<DependencyObject, ItemsControl>();
        //private static Dictionary<FrameworkElement, ErrorProvider> _errorprovider_items_control_link = new Dictionary<FrameworkElement, ErrorProvider>();
        //private static Dictionary<DependencyObject, bool> _initial_unload_registered = new Dictionary<DependencyObject, bool>();

        //private static Dictionary<ItemsControl, int> _virtualizing_parent_control_reference_count = new Dictionary<ItemsControl, int>();
        //private static Dictionary<ItemContainerGenerator, ItemsControl> _items_container_generator_link = new Dictionary<ItemContainerGenerator, ItemsControl>();
        private bool _change_isvalid = true;
        #endregion
    }
}
