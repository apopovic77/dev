using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.DirectX;
using Microsoft.DirectX.Direct3D;
using Logicx.DirectX.Engine;
using System.Windows.Forms;

namespace Logicx.DirectX.Objects.Meshes
{

    //-----------------------------------------------------------------------
    // AutoMesh - a class to manage meshes
    //-----------------------------------------------------------------------
    public class AutoMesh
    {
        Mesh mMesh;
        Device mD3d;

        /// <summary>
        /// Unused by Direct3d class.  You can use this field
        /// to store miscellaneous info with the mesh.
        /// </summary>
        public AutoMeshInfo Tag;
        public class AutoMeshInfo { }

        static Vector3[] sEmptyVector3Array = new Vector3[0];
        static Material[] sEmptyMaterialArray = new Material[0];
        static AutoTexture[] sEmptyAutoTextureArray = new AutoTexture[0];

        /// <summary>
        /// Set this variable to true if the mesh owns the textures it refers
        /// to.  When true, disposing this mesh also disposes the textures.
        /// Defaults to false.
        /// </summary>
        public bool OwnsTextures;

        // Save a copy of all Mesh data
        int mNumFaces;
        int mNumVertices;
        int mNumBytesPerVertex;
        MeshFlags mFlags;
        VertexFormats mVertexFormat;
        byte[] mIndexBufferCopy;
        byte[] mVertexBufferCopy;
        int[] mAttributeBufferCopy;

        // Textures and materials
        Material[] mMaterials = sEmptyMaterialArray;
        AutoTexture[] mTextures = sEmptyAutoTextureArray;

        // Cached info (vertices, bounding box, bounding sphere)
        Vector3[] mVertexCache;

        bool mBoundingBoxValid;
        Vector3 mBoundingBoxMin;
        Vector3 mBoundingBoxMax;

        bool mSphereValid;
        Vector3 mSphereCenter;
        float mSphereRadius;

        bool mSphereMinValid;
        Vector3 mSphereMinCenter;
        float mSphereMinRadius;

        /// <summary>
        /// Create an automesh.
        /// </summary>
        public AutoMesh(Device d3d, Mesh mesh)
        {
            mD3d = d3d;
            mMesh = mesh;
            mD3d.DeviceLost += new EventHandler(d3d_DxLost);
            mD3d.DeviceReset += new EventHandler(d3d_DxRestore);
        }

        /// <summary>
        /// Return the Direct3d object for this object
        /// </summary>
        public Device D3d { get { return mD3d; } }

        /// <summary>
        /// Return the mesh owned by this object (or null if the DirectX device is lost).
        /// NOTE: Setting this property DOES NOT DISPOSE the old mesh.
        /// Don't set this property when the device is lost (set from render is ok)
        /// Be sure to set Materials[] and Textures[] is required.
        /// </summary>
        public Mesh M
        {
            get
            {
                return mMesh;
            }
            set
            {
                // Set mesh, force a recalculation of bounding info (later)
                mMesh = value;
                mBoundingBoxValid = false;
                mSphereValid = false;
                mSphereMinValid = false;
            }
        }

        /// <summary>
        /// Set the mesh (use this function if you know the bounding stuff).
        /// NOTE: Setting this property DOES NOT DISPOSE the old mesh.
        /// Don't call this function when the device is lost (call from render is ok).
        /// Be sure to set Materials[] and Textures[] is required.
        /// </summary>
        public void SetMesh(Mesh mesh, Vector3 center, float radius, Vector3 min, Vector3 max)
        {
            mMesh = mesh;
            mSphereMinValid = true;
            mSphereMinCenter = center;
            mSphereMinRadius = radius;

            mSphereValid = true;
            mSphereCenter = center;
            mSphereRadius = radius;

            mBoundingBoxValid = true;
            mBoundingBoxMin = min;
            mBoundingBoxMax = max;
        }

        /// <summary>
        /// Gets/sets the array of materials (one for each subset to draw)
        /// </summary>
        public Material[] Materials
        {
            get { return mMaterials; }
            set
            {
                if (value == null)
                    mMaterials = sEmptyMaterialArray;
                else
                    mMaterials = value;
            }
        }

        /// <summary>
        /// Gets/sets the array of textures (one for each subset to draw).
        /// Returns an empty array if there are no textures, and any array
        /// element can be null if there is no texture for the given
        /// subset.
        /// </summary>
        public AutoTexture[] Textures
        {
            get { return mTextures; }
            set
            {
                if (value == null)
                    mTextures = sEmptyAutoTextureArray;
                else
                    mTextures = value;
            }
        }

        /// <summary>
        /// Draw all subsets of the mesh with the material and texture for
        /// the given subset.  If no texture/material is specified, the
        /// current one is used.
        /// </summary>
        public void Draw(bool useMeshMaterial)
        {
            if (mMesh == null)
                return;

            mD3d.VertexFormat = mMesh.VertexFormat;
            if (mTextures.Length == 0)
            {
                mMesh.DrawSubset(0);
            }
            else
                for (int i = 0; i < mTextures.Length; i++)
                {
                    if (mTextures[i] != null)
                        mD3d.SetTexture(0, mTextures[i].T);
                    if (useMeshMaterial)
                        mD3d.Material = mMaterials[i];
                    mMesh.DrawSubset(i);
                }

            Engine3d.Device3d.Material = new Material();
            Engine3d.Device3d.SetTexture(0, null);
        }

        /// <summary>
        /// Dispose this object and the mesh it holds.  The textures are
        /// also disposed if OwnsTextures is true.
        /// </summary>
        public void Dispose()
        {
            if (mMesh != null)
            {
                mMesh.Dispose();
                mMesh = null;
            }
            if (mD3d != null)
            {
                mD3d.DeviceLost -= new EventHandler(d3d_DxLost);
                mD3d.DeviceReset -= new EventHandler(d3d_DxRestore);
                mD3d = null;
            }
            mIndexBufferCopy = null;
            mVertexBufferCopy = null;
            mAttributeBufferCopy = null;
            mVertexCache = null;
            mBoundingBoxValid = false;
            mSphereValid = false;
            mSphereMinValid = false;
            if (OwnsTextures)
                for (int i = 0; i < mTextures.Length; i++)
                    if (mTextures[i] != null)
                        mTextures[i].Dispose();
            mTextures = sEmptyAutoTextureArray;
            mMaterials = sEmptyMaterialArray;
        }

        #region Utility Load Mesh from file
        /// <summary>
        /// Setup the form by loading the mesh
        /// </summary>
        public static AutoMesh FromFile(string filename)
        {
            try
            {
                // ToDo: Show a message form while loading			
                AutoMesh autoMesh = null;

                // Load the mesh (or fail and exit)
                try
                {
                    autoMesh = AutoMesh.LoadFromXFile(filename, MeshFlags.SystemMemory, Engine3d.Device3d);
                }
                catch (Exception ex)
                {
                    // Dispose whatever we have
                    try { autoMesh.Dispose(); }
                    catch { }
                    autoMesh = null;

                    MessageBox.Show("Error loading mesh");
                    return null;
                }

                // Detect unsupported texture formats
                VertexFormats format = autoMesh.M.VertexFormat;
                if ((format & VertexFormats.TextureCountMask) != VertexFormats.Texture0
                        && (format & VertexFormats.Texture1) != VertexFormats.Texture1)
                {
                    MessageBox.Show("Multiple textures not supported");
                    autoMesh.Dispose();
                    return null;
                }

                // Unsupported vertex formats
                VertexFormats unsupported = VertexFormats.Specular
                                            | VertexFormats.LastBetaD3DColor
                                            | VertexFormats.LastBetaUByte4
                                            | VertexFormats.PointSize
                                            | VertexFormats.Transformed;
                if ((int)(format & unsupported) != 0)
                {
                    MessageBox.Show("Unsupported vertex format");
                    autoMesh.Dispose();
                    return null;
                }

                // No position (sometimes this happens)
                if ((format & VertexFormats.Position) != VertexFormats.Position)
                {
                    MessageBox.Show("Unsupported vertex format (no position detected!)");
                    autoMesh.Dispose();
                    return null;
                }

                // Clone to new format, using 32 bit pixel format.
                try
                {
                    AutoMesh mMesh = autoMesh.Clone(Engine3d.Device3d, MeshFlags.Managed, autoMesh.M.VertexFormat, Format.A8R8G8B8, Usage.AutoGenerateMipMap, Pool.Managed);
                    return mMesh;
                }
                catch
                {
                    MessageBox.Show("Error cloning mesh");
                    try { autoMesh.Dispose(); }
                    catch { }
                    //try { mMesh.Dispose(); }
                    //catch { }
                    return null;
                }
                finally
                {
                    autoMesh.Dispose();
                    autoMesh = null;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Model " + filename + " cannot be loaded. Game won't work. Bye");
                throw ex;
            }
        }
        #endregion

        /// <summary>
        /// Cone the mesh (and textures that it contains), 
        /// optionally converting the vertex and texture format.
        /// OwnsTextures is copied to the new texture.
        /// The Tag is also copied.
        /// </summary>
        public AutoMesh Clone(Device d3d, MeshFlags flags, VertexFormats vertexFormat,
                        Format textureFormat, Usage usage, Pool pool)
        {
            // Clone the mesh vertex info
            Mesh mesh = mMesh.Clone(flags, vertexFormat, d3d);
            AutoMesh autoMesh = new AutoMesh(d3d, mesh);

            // Clone AutoMesh variables
            autoMesh.Tag = Tag;
            autoMesh.OwnsTextures = OwnsTextures;

            // Clone textures and materials
            if (mTextures.Length != 0)
            {
                // Clone materials
                autoMesh.mMaterials = new Material[mMaterials.Length];
                for (int i = 0; i < mMaterials.Length; i++)
                    autoMesh.mMaterials[i] = mMaterials[i];

                // Clone textures
                autoMesh.mTextures = new AutoTexture[mTextures.Length];
                for (int i = 0; i < mTextures.Length; i++)
                    if (mTextures[i] != null)
                    {
                        // Already cloned this texture?
                        bool alreadyConvertedTexture = false;
                        for (int j = 0; j < i; j++)
                            if (mTextures[i] == mTextures[j])
                            {
                                alreadyConvertedTexture = true;
                                autoMesh.mTextures[i] = autoMesh.mTextures[j];
                                break;
                            }
                        // Clone new texture
                        if (!alreadyConvertedTexture)
                            autoMesh.mTextures[i] = mTextures[i].Clone(d3d, textureFormat, usage, pool);
                    }
            }
            return autoMesh;
        }

        /// <summary>
        /// Save the mesh when the DirectX device is lost
        /// </summary>
        void d3d_DxLost(object sender, EventArgs ev)
        {
            if (mMesh == null)
                return;

            // Save all data needed to restore the mesh
            mNumFaces = mMesh.NumberFaces;
            mNumVertices = mMesh.NumberVertices;
            mNumBytesPerVertex = mMesh.NumberBytesPerVertex;
            mFlags = mMesh.Options.Value;
            mVertexFormat = mMesh.VertexFormat;

            // Copy pathIndex buffer
            mIndexBufferCopy = (byte[])mMesh.LockIndexBuffer(typeof(byte),
                                        LockFlags.ReadOnly, mMesh.IndexBuffer.Description.Size);
            mMesh.UnlockIndexBuffer();

            // Copy vertex buffer
            mVertexBufferCopy = (byte[])mMesh.LockVertexBuffer(typeof(byte),
                            LockFlags.ReadOnly, mMesh.NumberBytesPerVertex * mMesh.NumberVertices);
            mMesh.UnlockVertexBuffer();

            // Copy attribute buffer
            mAttributeBufferCopy = mMesh.LockAttributeBufferArray(LockFlags.ReadOnly);
            mMesh.UnlockAttributeBuffer(mAttributeBufferCopy);

            mMesh.Dispose();
            mMesh = null;
            mVertexCache = null;
        }

        /// <summary>
        /// Restore the mesh when the DirectX device is restored
        /// </summary>
        void d3d_DxRestore(object sender, EventArgs ev)
        {
            // If the direct3d device wasn't lost in the first place, don't restore it.
            // This happens the first timeMs around.
            if (mMesh != null)
                return;

            // Restore mesh			
            mMesh = new Mesh(mNumFaces, mNumVertices, mFlags, mVertexFormat, mD3d);
            //Debug.Assert(mMesh.NumberBytesPerVertex == mNumBytesPerVertex);

            // Copy pathIndex buffer
            GraphicsStream stream = mMesh.LockIndexBuffer(LockFlags.Discard);
            stream.Write(mIndexBufferCopy);
            mMesh.UnlockIndexBuffer();

            // Copy vertex buffer
            stream = mMesh.LockVertexBuffer(LockFlags.Discard);
            stream.Write(mVertexBufferCopy);
            mMesh.UnlockVertexBuffer();

            // Copy attribute buffer
            int[] attributeBuffer = mMesh.LockAttributeBufferArray(LockFlags.Discard);
            mAttributeBufferCopy.CopyTo(attributeBuffer, 0);
            mMesh.UnlockAttributeBuffer(attributeBuffer);

            mIndexBufferCopy = null;
            mVertexBufferCopy = null;
            mAttributeBufferCopy = null;
        }

        /// <summary>
        /// Returns the bounding box of the mesh (only when the DirectX device is not lost).
        /// Caches the result for subsequent calls.
        /// </summary>
        public void BoundingBox(out Vector3 min, out Vector3 max)
        {
            if (!mBoundingBoxValid)
            {
                GraphicsStream stream = mMesh.LockVertexBuffer(LockFlags.ReadOnly);
                Geometry.ComputeBoundingBox(stream, mMesh.NumberVertices, mMesh.VertexFormat,
                                            out mBoundingBoxMin, out mBoundingBoxMax);
                mMesh.UnlockVertexBuffer();
                mBoundingBoxValid = true;
            }
            min = mBoundingBoxMin;
            max = mBoundingBoxMax;
        }

        /// <summary>
        /// Returns the bounding sphere of the mesh (only when the DirectX device is not lost)
        /// Caches the result for subsequent calls.
        /// </summary>
        public float BoundingSphere(out Vector3 center)
        {
            if (!mSphereValid)
            {
                GraphicsStream stream = mMesh.LockVertexBuffer(LockFlags.ReadOnly);
                mSphereRadius = Geometry.ComputeBoundingSphere(stream, mMesh.NumberVertices,
                                                                mMesh.VertexFormat, out mSphereCenter);
                mMesh.UnlockVertexBuffer();
                mSphereValid = true;
            }
            center = mSphereCenter;
            return mSphereRadius;
        }

        /// <summary>
        /// Sometimes ComputeBoundingSphere doesn't return the smallest sphere,
        /// so this returns the smaller of ComputeBoundingSphere and ComputeBoundingBox
        /// </summary>
        public float BoundingSphereMin(out Vector3 center)
        {
            // Quick exit for previously calculated value
            if (mSphereMinValid)
            {
                center = mSphereMinCenter;
                return mSphereMinRadius;
            }

            // Get bounding sphere
            mSphereMinValid = true;
            mSphereMinRadius = BoundingSphere(out mSphereMinCenter);

            // Get bounding sphere around a bounding box
            float radius2;
            Vector3 min, max;
            BoundingBox(out min, out max);
            radius2 = ((max - min) * 0.5f).Length();

            // Bounding sphere smaller?			
            if (mSphereMinRadius <= radius2)
            {
                center = mSphereMinCenter;
                return mSphereMinRadius;
            }
            // Bounding box smaller
            mSphereMinCenter = center = (min + max) * 0.5f;
            mSphereMinRadius = radius2;
            return mSphereRadius;
        }

        /// <summary>
        /// This function returns all the vertices in the mesh.  The result is cached, and
        /// the same array is returned on repeated calls.  DO NOT MODIFY THE ARRAY.
        /// </summary>
        /// <returns></returns>
        public Vector3[] GetVertices()
        {
            // Return cached vertex list
            if (mVertexCache != null)
                return mVertexCache;
            if (mMesh == null)
                return sEmptyVector3Array;

            // Convert the mesh to vertex-only vertexFormat, and read the vertex array
            Mesh vertexMesh = mMesh.Clone(MeshFlags.SystemMemory | MeshFlags.Use32Bit, VertexFormats.Position, mD3d);
            mVertexCache = (Vector3[])vertexMesh.LockVertexBuffer(typeof(Vector3), LockFlags.ReadOnly, mMesh.NumberVertices);
            vertexMesh.Dispose();
            return mVertexCache;
        }

        /// <summary>
        /// Read a mesh from an X file, and load the textures which are
        /// assumed to be in the same directory.  
        /// Sets OwnsTextures to true (they will be disposed when the mesh is disposed)
        /// </summary>
        public static AutoMesh LoadFromXFile(string path, MeshFlags flags, Device d3d)
        {
            ExtendedMaterial[] extendedMaterials;
            String[] fileNames;
            AutoMesh mesh = new AutoMesh(d3d, Mesh.FromFile(path,
                                        MeshFlags.SystemMemory, d3d, out extendedMaterials));

            mesh.OwnsTextures = true;
            mesh.mTextures = new AutoTexture[extendedMaterials.Length];
            mesh.mMaterials = new Material[extendedMaterials.Length];
            fileNames = new string[extendedMaterials.Length];

            // Load all the textures for this mesh
            for (int i = 0; i < extendedMaterials.Length; i++)
            {
                if (extendedMaterials[i].TextureFilename != null)
                {
                    string textureFileName = System.IO.Path.Combine(
                                            System.IO.Path.GetDirectoryName(path),
                                            extendedMaterials[i].TextureFilename);
                    fileNames[i] = textureFileName;

                    // Scan to see if we already have this texture
                    bool alreadyHaveTexture = false;
                    for (int j = 0; j < i; j++)
                        if (textureFileName == fileNames[j])
                        {
                            mesh.mTextures[i] = mesh.mTextures[j];
                            alreadyHaveTexture = true;
                            break;
                        }
                    // Load texture (if we don't already have it)
                    if (!alreadyHaveTexture)
                        mesh.mTextures[i] = new AutoTexture(d3d,
                                            TextureLoader.FromFile(d3d, textureFileName));
                }
                mesh.mMaterials[i] = extendedMaterials[i].Material3D;
                mesh.mMaterials[i].Ambient = mesh.mMaterials[i].Diffuse;
            }
            return mesh;
        }
    }
}
