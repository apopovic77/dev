﻿using System;
using System.ComponentModel.DataAnnotations;
using GeneticProgrammingLib;
using MathLib;

namespace ShortestPathAgentsApp.GeneticAlgorithm
{
    public class FindPathGeneticAlgorithm : GeneticAlgorithm<SmartAgent,Vector2d>
    {
        public FindPathGeneticAlgorithm(Vector2d target_pos, Vector2d initial_pos)
        {
            SmartAgentConfig.AgentRotationVelocityFactor.Value = 0.3f;
            SmartAgentConfig.AgentTimeToLive.Value = 10;
            SmartAgentConfig.AgentVelocity.Value = 10;
            SmartAgentConfig.EvolutionMutationRate.Value = 0.2f;
            SmartAgentConfig.EvolutionPopulationSize.Value = 200;

            SmartAgent.TargetPos = target_pos;
            SmartAgent.InitialPos = initial_pos;
            
            SmartAgent.FovRotateAngleMax = 360;

            //IntervalMsec = (int) (1.0/ SmartAgent.Velocity * 100.0);
            //SmartAgent.DnaGeneSize = (int) ((SmartAgent.TimeToLiveSec * 1000) / IntervalMsec);

            IntervalMsec = 100;

            SmartAgent.DnaGeneSize = 300;
            //für jede Dna Gene Size muss ein frame simuliert werden
            _frame_count = SmartAgent.DnaGeneSize;



            SmartAgentConfig.EvolutionMutationRate.PropertyChanged += EvolutionMutationRate_PropertyChanged;
            SmartAgentConfig.EvolutionPopulationSize.PropertyChanged += EvolutionPopulationSize_PropertyChanged;
        }

        private void EvolutionPopulationSize_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            PopulationSize = SmartAgentConfig.EvolutionPopulationSize.Value;
        }

        private void EvolutionMutationRate_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            MutationRate = SmartAgentConfig.EvolutionMutationRate.Value;
        }
    }
}
