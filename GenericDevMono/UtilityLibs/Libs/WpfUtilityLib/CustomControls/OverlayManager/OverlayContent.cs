﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Threading;
using Logicx.Utilities;
using Logicx.WpfUtility.CustomControls.ChangeInfo;
using Logicx.WpfUtility.CustomControls.Validation;
using Logicx.WpfUtility.WindowManagement;
using Logicx.WpfUtility.WpfHelpers;
using MathLib;
using Logicx.WpfUtility.CustomControls.Combobox;
using Logicx.WpfUtility.CustomControls.AnimatedPanel;

namespace Logicx.WpfUtility.CustomControls.OverlayManager
{
    public class OverlayContent : ContentControl, IDisposable, INotifyPropertyChanged
    {
        public enum DataInitializationBehaviourEnum
        {
            Immediate,
            // immediatly within the Show method (before content is part of the visual tree) in the main thread
            Normal, // after the dialog has been loaded in the main thread
            Background // after the dialog has been loaded in a background thread
        }

        public enum ModalBehaviourEnum
        {
            TransparentBackground,                  // der modale hintergrund bereich des overlaymanagers bleibt transparent
            DarkenBackground,                       // modaler hintergrund wird verdunkelt
            TransparentBackgroundDeactivateAll,     // transparenter modaler hintergrund - deaktiviert automatisch alle offenen fenster
            DarkenBackgroundDeactivateAll,          // verdunkelter  modaler hintergrund - deaktiviert automatisch alle offenen fenster
            TransparentBackgroundMinimizeAll,       // transparenter modaler hintergrund - minimiert automatisch alle offenen fenster
            DarkenBackgroundMinimizeAll             // verdunkelter modaler hintergrund - minimiert automatisch alle offenen fenster
        }

        public enum LoadingBehaviourEnum
        {
            Normal,                                 // Keine Status information beim laden
            ActionIndicator,                        // Nur eine Anzeige der aktuellen Aktion
            ActionIndicatorWithPercent              // Aktuelle Aktion ink. Fortschrittsanzeige in Prozent
        }

        public enum GoInactiveBehaviuorEnum
        {
            DoNothing,                  // Keine Aktion ausführen wenn der Dialog inaktiv wird
            ImageOnly,                  // Das gerenderte Bild des Dialoges anzeigen sobald dieser inaktiv wirf (keine Animationen)
            OpacityAnimationDialog,     // Den Dialog angezeigt lassen, allerdings ausfaden
            OpacityAnimation,           // Das gerenderte Bild ausfaden
            CustomAnimation,            // Eine eigene Animation auf dem gerenderten Bild durchführen
            Minimize                    // Das gerenderte Bild minimieren
        }

        public enum DragVisualModeEnum
        {
            Dialog,     // Der Dialog wird gedraged
            Picture     // Das Bild wird gedraged
        }

        public enum ImageRenderBehaviourEnum
        {
            Always,         // Vor jeder Animation einen neuen Screenshot erstellen
            OnlyIfDirty,    // Nur einen neuen Screenshot erstellen wenn das IsDirty Flag gesetzt wird
            Intelligent     // Gleiches Verhalten wie bei OnlyIfDirty mit dem Unterschied, dass das DirtyFlag anhand von Steuerelementeingaben automatisch gesetzt wird.
        }

        public enum OverlayDialogResizeModeEnum
        {
            ScaleResize,    // scale transform wird beim resize verwendet
            NormalResize    // breite und höhe wird beim resize verändert
        }

        public enum SearchPositionAlignmentEnum
        {
            Left,
            Right
        }

        public class BackgroundSaveCompletionEventArgs : EventArgs
        {
            public BackgroundSaveCompletionEventArgs(bool success, object argument, object result, Exception error)
            {
                Success = success;
                Result = result;
                Error = error;
                Argument = argument;
            }

            public bool Success
            {
                get;
                private set;
            }

            public object Argument
            {
                get;
                private set;
            }

            public object Result
            {
                get;
                private set;
            }

            public Exception Error
            {
                get;
                private set;
            }
        }

        public class QueryDialogSubMenuPanelItemTypeEventArgs : EventArgs
        {
            public QueryDialogSubMenuPanelItemTypeEventArgs(Type current_type)
            {
                CurrentType = current_type;
            }

            public Type CurrentType
            {
                get;
                private set;
            }

            public Type NewType
            {
                get;
                set;
            }
        }
        
        #region Construction and Initialization
        /// <summary>
        /// Static constructor
        /// </summary>
        static OverlayContent()
        {
            #region Titles
            FrameworkPropertyMetadata metadataDialogTitle = new FrameworkPropertyMetadata("Title");
            DialogTitleProperty = DependencyProperty.Register("DialogTitle",
                                                            typeof(string), typeof(OverlayContent), metadataDialogTitle);

            FrameworkPropertyMetadata metadataDialogSubTitle = new FrameworkPropertyMetadata("");
            DialogSubTitleProperty = DependencyProperty.Register("DialogSubTitle",
                                                            typeof(string), typeof(OverlayContent), metadataDialogSubTitle);
            #endregion

            #region Styles

            // diese properties müssen VOR dem Show des coontents gesetzt werden
            // damit der overlay manager die custom styles verwendet wenn ein dialog angezeigt wird

            FrameworkPropertyMetadata metadataCloseButtonStyle = new FrameworkPropertyMetadata(null);
            CloseButtonStyleProperty = DependencyProperty.Register("CloseButtonStyle",
                                                            typeof(Style), typeof(OverlayContent), metadataCloseButtonStyle);

            FrameworkPropertyMetadata metadataHelpButttonStyle = new FrameworkPropertyMetadata(null);
            HelpButtonStyleProperty = DependencyProperty.Register("HelpButtonStyle",
                                                                  typeof(Style), typeof(OverlayContent), metadataHelpButttonStyle);

            FrameworkPropertyMetadata metadataPrintButtonStyle = new FrameworkPropertyMetadata(null);
            PrintButtonStyleProperty = DependencyProperty.Register("PrintButtonStyle",
                                                            typeof(Style), typeof(OverlayContent), metadataPrintButtonStyle);

            FrameworkPropertyMetadata metadataMinimizeButtonStyle = new FrameworkPropertyMetadata(null);
            MinimizeButtonStyleProperty = DependencyProperty.Register("MinimizeButtonStyle",
                                                            typeof(Style), typeof(OverlayContent), metadataMinimizeButtonStyle);

            FrameworkPropertyMetadata metadataMaximizeButtonStyle = new FrameworkPropertyMetadata(null);
            MaximizeButtonStyleProperty = DependencyProperty.Register("MaximizeButtonStyle",
                                                            typeof(Style), typeof(OverlayContent), metadataMaximizeButtonStyle);

            FrameworkPropertyMetadata metadataNormalizeButtonStyle = new FrameworkPropertyMetadata(null);
            NormalizeButtonStyleProperty = DependencyProperty.Register("NormalizeButtonStyle",
                                                            typeof(Style), typeof(OverlayContent), metadataNormalizeButtonStyle);

            FrameworkPropertyMetadata metadataDialogBorderStyle = new FrameworkPropertyMetadata(null);
            DialogBorderStyleProperty = DependencyProperty.Register("DialogBorderStyle",
                                                            typeof(Style), typeof(OverlayContent), metadataDialogBorderStyle);

            FrameworkPropertyMetadata metadataDialogContentBorderStyle = new FrameworkPropertyMetadata(null);
            DialogContentBorderStyleProperty = DependencyProperty.Register("DialogContentBorderStyle",
                                                            typeof(Style), typeof(OverlayContent), metadataDialogContentBorderStyle);


            FrameworkPropertyMetadata metadataDialogButtonStyle = new FrameworkPropertyMetadata(null);
            DialogButtonStyleProperty = DependencyProperty.Register("DialogButtonStyle",
                                                            typeof(Style), typeof(OverlayContent), metadataDialogButtonStyle);

            #endregion

            FrameworkPropertyMetadata metadataImageRenderOffsetWidth = new FrameworkPropertyMetadata((double)0);
            ImageRenderOffsetWidthProperty = DependencyProperty.Register("ImageRenderOffsetWidth",
                                                            typeof(double), typeof(OverlayContent), metadataImageRenderOffsetWidth);

            FrameworkPropertyMetadata metadataImageRenderOffsetHeight = new FrameworkPropertyMetadata((double)0);
            ImageRenderOffsetHeightProperty = DependencyProperty.Register("ImageRenderOffsetHeight",
                                                            typeof(double), typeof(OverlayContent), metadataImageRenderOffsetHeight);

            FrameworkPropertyMetadata metadataDefaultWindowState = new FrameworkPropertyMetadata(OverlayDialogState.Modal);
            DefaultWindowStateProperty = DependencyProperty.Register("DefaultWindowState",
                                                            typeof(OverlayDialogState), typeof(OverlayContent), metadataDefaultWindowState);

            FrameworkPropertyMetadata metadataDragVisualMode = new FrameworkPropertyMetadata(DragVisualModeEnum.Picture);
            DragVisualModeProperty = DependencyProperty.Register("DragVisualMode",
                                                            typeof(DragVisualModeEnum), typeof(OverlayContent), metadataDragVisualMode);

            FrameworkPropertyMetadata metadataInitializationBehaviour = new FrameworkPropertyMetadata(DataInitializationBehaviourEnum.Normal);
            InitializationBehaviourProperty = DependencyProperty.Register("InitializationBehaviour",
                                                            typeof(DataInitializationBehaviourEnum), typeof(OverlayContent), metadataInitializationBehaviour);

            FrameworkPropertyMetadata metadataModalBehaviour = new FrameworkPropertyMetadata(ModalBehaviourEnum.DarkenBackground);
            ModalBehaviourProperty = DependencyProperty.Register("ModalBehaviour",
                                                            typeof(ModalBehaviourEnum), typeof(OverlayContent), metadataModalBehaviour);

            FrameworkPropertyMetadata metadataGoInactiveBehaviour = new FrameworkPropertyMetadata(GoInactiveBehaviuorEnum.OpacityAnimation);
            GoInactiveBehaviourProperty = DependencyProperty.Register("GoInactiveBehaviour",
                                                typeof(GoInactiveBehaviuorEnum), typeof(OverlayContent), metadataGoInactiveBehaviour);

            FrameworkPropertyMetadata metadataResizeMode = new FrameworkPropertyMetadata(OverlayDialogResizeModeEnum.ScaleResize);
            ResizeModeProperty = DependencyProperty.Register("ResizeMode",
                                                typeof(OverlayDialogResizeModeEnum), typeof(OverlayContent), metadataResizeMode);

            FrameworkPropertyMetadata metadatMinimalSideLength = new FrameworkPropertyMetadata(double.NaN);
            MinimalSideLengthProperty = DependencyProperty.Register("MinimalSideLength",
                                                typeof(double), typeof(OverlayContent), metadatMinimalSideLength);


            FrameworkPropertyMetadata metadataLoadingBehaviour = new FrameworkPropertyMetadata(LoadingBehaviourEnum.Normal);
            LoadingBehaviourProperty = DependencyProperty.Register("LoadingBehaviour",
                                                            typeof(LoadingBehaviourEnum), typeof(OverlayContent), metadataLoadingBehaviour);

            FrameworkPropertyMetadata metadataImageRenderBehaviour = new FrameworkPropertyMetadata(ImageRenderBehaviourEnum.OnlyIfDirty);
            ImageRenderBehaviourProperty = DependencyProperty.Register("ImageRenderBehaviour",
                                                            typeof(ImageRenderBehaviourEnum), typeof(OverlayContent), metadataImageRenderBehaviour);

            FrameworkPropertyMetadata metadataDialogImageDirty = new FrameworkPropertyMetadata(true);
            DialogImageDirtyProperty = DependencyProperty.Register("DialogImageDirty",
                                                            typeof(bool), typeof(OverlayContent), metadataDialogImageDirty);



            FrameworkPropertyMetadata metadataMinimizeToStatusInfoDuringBackgroundSaving = new FrameworkPropertyMetadata(true);
            MinimizeToStatusInfoDuringBackgroundSavingProperty = DependencyProperty.Register("MinimizeToStatusInfoDuringBackgroundSaving",
                                                            typeof(bool), typeof(OverlayContent), metadataMinimizeToStatusInfoDuringBackgroundSaving);

            FrameworkPropertyMetadata metadataShowStatusInfoDuringBackgroundSaving = new FrameworkPropertyMetadata(true);
            ShowStatusInfoDuringBackgroundSavingProperty = DependencyProperty.Register("ShowStatusInfoDuringBackgroundSaving",
                                                            typeof(bool), typeof(OverlayContent), metadataShowStatusInfoDuringBackgroundSaving);

            ContentIdPropertyKey = DependencyProperty.RegisterReadOnly("ContentId", typeof(string), typeof(OverlayContent), new FrameworkPropertyMetadata("", FrameworkPropertyMetadataOptions.NotDataBindable));
            ContentIdProperty = ContentIdPropertyKey.DependencyProperty;

            #region Animations
            FrameworkPropertyMetadata metadataActivateStoryboard = new FrameworkPropertyMetadata(null);
            ActivateStoryboardProperty = DependencyProperty.Register("ActivateStoryboard",
                                                            typeof(Storyboard), typeof(OverlayContent), metadataActivateStoryboard);

            FrameworkPropertyMetadata metadataDeactivateStoryboard = new FrameworkPropertyMetadata(null);
            DeactivateStoryboardProperty = DependencyProperty.Register("DeactivateStoryboard",
                                                            typeof(Storyboard), typeof(OverlayContent), metadataDeactivateStoryboard);


            //FrameworkPropertyMetadata metadataLoadingStoryboard = new FrameworkPropertyMetadata(null);
            //LoadingStoryboardProperty = DependencyProperty.Register("LoadingStoryboard",
            //                                                typeof(Storyboard), typeof(OverlayContent), metadataLoadingStoryboard);

            FrameworkPropertyMetadata metadataLoadinStartgStoryboard = new FrameworkPropertyMetadata(null);
            LoadingStartStoryboardProperty = DependencyProperty.Register("LoadingStartStoryboard",
                                                            typeof(Storyboard), typeof(OverlayContent), metadataLoadinStartgStoryboard);

            FrameworkPropertyMetadata metadataLoadingStartStoryboardFinalPos = new FrameworkPropertyMetadata(null);
            LoadingStartStoryboardFinalPosProperty = DependencyProperty.Register("LoadingStartStoryboardFinalPos",
                                                                                 typeof (
                                                                                     OverlayWindowMeta.
                                                                                     OverlayAnimationProperties),
                                                                                 typeof (OverlayContent),
                                                                                 metadataLoadingStartStoryboardFinalPos);

            FrameworkPropertyMetadata metadataLoadingEndStoryboard = new FrameworkPropertyMetadata(null);
            LoadingEndStoryboardProperty = DependencyProperty.Register("LoadingEndStoryboard",
                                                            typeof(Storyboard), typeof(OverlayContent), metadataLoadingEndStoryboard);

            FrameworkPropertyMetadata metadataLoadingEndStoryboardFinalPos = new FrameworkPropertyMetadata(null);
            LoadingEndStoryboardFinalPosProperty = DependencyProperty.Register("LoadingEndStoryboardFinalPos",
                                                                                 typeof(OverlayWindowMeta.OverlayAnimationProperties),typeof(OverlayContent),
                                                                                 metadataLoadingEndStoryboardFinalPos);


            FrameworkPropertyMetadata metadataLoadingInformationControl = new FrameworkPropertyMetadata(null);
            LoadingInformationControlProperty = DependencyProperty.Register("LoadingInformationControl",
                                                            typeof(UIElement), typeof(OverlayContent), metadataLoadingInformationControl);


            #endregion
        }

        /// <summary>
        /// Default constructor for design support
        /// </summary>
        public OverlayContent()
        {
            FocusVisualStyle = null;
            Focusable = false;

            //UIHelper.WalkDictionary(this.Resources);
            BackgroundSavingStatusInfoTitle = "Daten werden gespeichert ...";

            //DependencyPropertyDescriptor descr = DependencyPropertyDescriptor.FromProperty(HelpProvider.HelpProvider.HelpScopeProperty, this.GetType());
            //if(descr != null)
            //{
            //    descr.AddValueChanged(this, OverlayContent_HelpScopeChanged);
            //}

            //DependencyPropertyDescriptor descr1 = DependencyPropertyDescriptor.FromProperty(ImageRenderBehaviourProperty, this.GetType());
            //if (descr1 != null)
            //{
            //    descr1.AddValueChanged(this, ImageRenderBehaviour_Changed);
            //}

            KeyboardNavigation.SetIsTabStop(this, false);

            if(string.IsNullOrEmpty(ContentId))
            {
                SetContentId(OverlayContent.GetDialogContentId(null));
            }

            this.Unloaded += new RoutedEventHandler(OverlayContent_Unloaded);
        }


        #endregion

        #region Dialog content ID generation
        public static string GetDialogContentId(object[] param)
        {
            string sRet = "OverlayContent_";

            if(param == null)
            {
                sRet += Guid.NewGuid().ToString();
            }
            else
            {
                if(param.Length > 0)
                {
                    for (int i = 0; i < param.Length; i++)
                        sRet += (i>0?"_":"") + param[i].GetHashCode().ToString();
                }
                else
                {
                    sRet += param.GetHashCode().ToString();
                }
            }

            return sRet;
        }

        protected void SetContentId(string content_id)
        {
            SetValue(ContentIdPropertyKey, content_id);
        }
        #endregion

        #region Properties

        public string ContentId
        {
            get { return (string) GetValue(ContentIdProperty); }
        }

        #region Animations
        /// <summary>
        /// Dependency Eigenschaft welche die aktivierungs animation angibt
        /// </summary>
        /// <remarks>Muss eine Animation mit definiertem Ende sein (keine Endlosanimationen erlaubt)</remarks>
        public Storyboard ActivateStoryboard
        {
            get
            {
                return (Storyboard)GetValue(ActivateStoryboardProperty);
            }
            set
            {
                SetValue(ActivateStoryboardProperty, value);
            }
        }

        /// <summary>
        /// Dependency Eigenschaft welche die deaktivierungs animation angibt
        /// </summary>
        /// <remarks>Muss eine Animation mit definiertem Ende sein (keine Endlosanimationen erlaubt)</remarks>
        public Storyboard DeactivateStoryboard
        {
            get
            {
                return (Storyboard)GetValue(DeactivateStoryboardProperty);
            }
            set
            {
                SetValue(DeactivateStoryboardProperty, value);
            }
        }

        /// <summary>
        /// Dependency Eigenschaft welche die lade animation angibt
        /// </summary>
        /// <remarks>Diese Animation muss ein definiertes ENDE haben und darf somit keine Infinite Animation sein.
        /// Erst wenn die Ladeanimation und die Dateninitialisierung fertig ist, wird der Dialog für Benutzereingaben 
        /// "freigeschalten".</remarks>
        //public Storyboard LoadingStoryboard
        //{
        //    get
        //    {
        //        return (Storyboard)GetValue(LoadingStoryboardProperty);
        //    }
        //    set
        //    {
        //        SetValue(LoadingStoryboardProperty, value);
        //    }
        //}

        /// <summary>
        /// Dependency Eigenschaft welche die lade animation angibt
        /// </summary>
        /// <remarks>Diese Animation muss ein definiertes ENDE haben und darf somit keine Infinite Animation sein.
        /// Erst wenn die Ladeanimation und die Dateninitialisierung fertig ist, wird der Dialog für Benutzereingaben 
        /// "freigeschalten".</remarks>
        public Storyboard LoadingStartStoryboard
        {
            get
            {
                return (Storyboard)GetValue(LoadingStartStoryboardProperty);
            }
            set
            {
                SetValue(LoadingStartStoryboardProperty, value);
            }
        }

        public OverlayWindowMeta.OverlayAnimationProperties LoadingStartStoryboardFinalPos
        {
            get
            {
                return (OverlayWindowMeta.OverlayAnimationProperties)GetValue(LoadingStartStoryboardFinalPosProperty);
            }
            set
            {
                SetValue(LoadingStartStoryboardFinalPosProperty, value);
            }
        }

        /// <summary>
        /// Dependency Eigenschaft welche die lade animation angibt
        /// </summary>
        /// <remarks>Diese Animation muss ein definiertes ENDE haben und darf somit keine Infinite Animation sein.
        /// Erst wenn die Ladeanimation und die Dateninitialisierung fertig ist, wird der Dialog für Benutzereingaben 
        /// "freigeschalten".</remarks>
        public Storyboard LoadingEndStoryboard
        {
            get
            {
                return (Storyboard)GetValue(LoadingEndStoryboardProperty);
            }
            set
            {
                SetValue(LoadingEndStoryboardProperty, value);
            }
        }

        public OverlayWindowMeta.OverlayAnimationProperties LoadingEndStoryboardFinalPos
        {
            get
            {
                return (OverlayWindowMeta.OverlayAnimationProperties)GetValue(LoadingEndStoryboardFinalPosProperty);
            }
            set
            {
                SetValue(LoadingEndStoryboardFinalPosProperty, value);
            }
        }

        public LoadingBehaviourEnum LoadingBehaviour
        {
            get
            {
                return (LoadingBehaviourEnum)GetValue(LoadingBehaviourProperty);
            }
            set
            {
                SetValue(LoadingBehaviourProperty, value);
            }
        }
        #endregion

        /// <summary>
        /// Dependency Eigenschaft welche die renderhäufigkeit des dialog bildes im overlay manager steuert
        /// </summary>
        public ImageRenderBehaviourEnum ImageRenderBehaviour
        {
            get
            {
                return (ImageRenderBehaviourEnum)GetValue(ImageRenderBehaviourProperty);
            }
            set
            {
                SetValue(ImageRenderBehaviourProperty, value);
            }
        }

        /// <summary>
        /// Dependency Eigenschaft welche das dialog content bild als "veraltet" markiert. 
        /// </summary>
        public bool DialogImageDirty
        {
            get
            {
                return (bool)GetValue(DialogImageDirtyProperty);
            }
            set
            {
                SetValue(DialogImageDirtyProperty, value);
            }
        }

        /// <summary>
        /// Dependency Eigenschaft welche das control festlegt welches zusätzlich bei der ladeanimation angezeigt wird
        /// </summary>
        public UIElement LoadingInformationControl
        {
            get
            {
                return (UIElement)GetValue(LoadingInformationControlProperty);
            }
            set
            {
                SetValue(LoadingInformationControlProperty, value);
            }
        }

        /// <summary>
        /// Dependency Eigenschaft welche die drag visual eigenschaft definiert
        /// </summary>
        public DragVisualModeEnum DragVisualMode
        {
            get
            {
                return (DragVisualModeEnum)GetValue(DragVisualModeProperty);
            }
            set
            {
                SetValue(DragVisualModeProperty, value);
            }
        }

        /// <summary>
        /// A fix width value which will be added to width calculations
        /// </summary>
        public double ImageRenderOffsetWidth
        {
            get
            {
                return (double)GetValue(ImageRenderOffsetWidthProperty);
            }
            set
            {
                SetValue(ImageRenderOffsetWidthProperty, value);
            }
        }
        /// <summary>
        /// A fix height value which will be added to height calculations
        /// </summary>
        public double ImageRenderOffsetHeight
        {
            get
            {
                return (double)GetValue(ImageRenderOffsetHeightProperty);
            }
            set
            {
                SetValue(ImageRenderOffsetHeightProperty, value);
            }
        }

        /// <summary>
        /// Dependency Eigenschaft welche den Text des Dialogtitels zurückgibt
        /// </summary>
        public OverlayDialogState DefaultWindowState
        {
            get
            {
                return (OverlayDialogState)GetValue(DefaultWindowStateProperty);
            }
            set
            {
                SetValue(DefaultWindowStateProperty, value);
            }
        }



        public Style OverlayDialogTitleStyle
        {
            get { return (Style)GetValue(OverlayDialogTitleStyleProperty); }
            set { SetValue(OverlayDialogTitleStyleProperty, value); }
        }
        public static readonly DependencyProperty OverlayDialogTitleStyleProperty = DependencyProperty.Register("OverlayDialogTitleStyle", typeof(Style), typeof(OverlayContent), new UIPropertyMetadata(null));


        public Style OverlayDialogSubTitleStyle
        {
            get { return (Style)GetValue(OverlayDialogSubTitleStyleProperty); }
            set { SetValue(OverlayDialogSubTitleStyleProperty, value); }
        }
        public static readonly DependencyProperty OverlayDialogSubTitleStyleProperty = DependencyProperty.Register("OverlayDialogSubTitleStyle", typeof(Style), typeof(OverlayContent), new UIPropertyMetadata(null));





        public string MinimizedWindowPanelName
        {
            get { return (string)GetValue(MinimizedWindowPanelNameProperty); }
            set { SetValue(MinimizedWindowPanelNameProperty, value); }
        }
        public static readonly DependencyProperty MinimizedWindowPanelNameProperty = DependencyProperty.Register("MinimizedWindowPanelName", typeof(string), typeof(OverlayContent), new UIPropertyMetadata("Dialoge"));

        public string MinimizedWindowInfoText
        {
            get { return (string)GetValue(MinimizedWindowInfoTextProperty); }
            set { SetValue(MinimizedWindowInfoTextProperty, value); }
        }
        public static readonly DependencyProperty MinimizedWindowInfoTextProperty = DependencyProperty.Register("MinimizedWindowInfoText", typeof(string), typeof(OverlayContent), new UIPropertyMetadata(string.Empty));

        public Style MinimizedWindowInfoTextStyle
        {
            get { return (Style)GetValue(MinimizedWindowInfoTextStyleProperty); }
            set { SetValue(MinimizedWindowInfoTextStyleProperty, value); }
        }
        public static readonly DependencyProperty MinimizedWindowInfoTextStyleProperty = DependencyProperty.Register("MinimizedWindowInfoTextStyle", typeof(Style), typeof(OverlayContent), new UIPropertyMetadata(null));

        /// <summary>
        /// Dependency Eigenschaft welche den Text des Dialogtitels zurückgibt
        /// </summary>
        public DataInitializationBehaviourEnum InitializationBehaviour
        {
            get
            {
                return (DataInitializationBehaviourEnum)GetValue(InitializationBehaviourProperty);
            }
            set
            {
                SetValue(InitializationBehaviourProperty, value);
            }
        }

        /// <summary>
        /// Dependency Eigenschaft welche das verhalten bei inactive angibt
        /// </summary>
        public GoInactiveBehaviuorEnum GoInactiveBehaviuor
        {
            get
            {
                return (GoInactiveBehaviuorEnum)GetValue(GoInactiveBehaviourProperty);
            }
            set
            {
                SetValue(GoInactiveBehaviourProperty, value);
            }
        }


        /// <summary>
        /// Dependency Eigenschaft welche das verhalten bei resize angibt
        /// </summary>
        public OverlayDialogResizeModeEnum ResizeMode
        {
            get
            {
                return (OverlayDialogResizeModeEnum)GetValue(ResizeModeProperty);
            }
            set
            {
                SetValue(ResizeModeProperty, value);
            }
        }

        public double MinimalSideLength
        {
            get
            {
                return (double) GetValue(MinimalSideLengthProperty);
            }
            set
            {
                SetValue(MinimalSideLengthProperty, value);
            }
        }

        /// <summary>
        /// Dependency Eigenschaft welche das verhalten im Modal-Modus beschreibt
        /// </summary>
        public ModalBehaviourEnum ModalBehaviour
        {
            get
            {
                return (ModalBehaviourEnum)GetValue(ModalBehaviourProperty);
            }
            set
            {
                SetValue(ModalBehaviourProperty, value);
            }
        }

        /// <summary>
        /// Dependency Eigenschaft welche den Text des Dialogtitels zurückgibt
        /// </summary>
        public string DialogTitle
        {
            get
            {
                return (string) GetValue(DialogTitleProperty);
            }
            set
            {
                SetValue(DialogTitleProperty, value);
            }
        }

        /// <summary>
        /// Dependency Eigenschaft welche angibt ob der dialog automatisch auf die statusanzeige minimiert werden soll
        /// sobald ein Hintergrundspeichern durchgeführt wurde
        /// </summary>
        public bool MinimizeToStatusInfoDuringBackgroundSaving
        {
            get
            {
                return (bool)GetValue(MinimizeToStatusInfoDuringBackgroundSavingProperty);
            }
            set
            {
                SetValue(MinimizeToStatusInfoDuringBackgroundSavingProperty, value);
            }
        }

        /// <summary>
        /// Dependency Eigenschaft welche angibt ob die status information eingeblendet wird sobald das
        /// Hintergrundspeichern beginnt (und diese nach dem Speichern wieder ausblendet)
        /// </summary>
        public bool ShowStatusInfoDuringBackgroundSaving
        {
            get
            {
                return (bool)GetValue(ShowStatusInfoDuringBackgroundSavingProperty);
            }
            set
            {
                SetValue(ShowStatusInfoDuringBackgroundSavingProperty, value);
            }
        }
        

        /// <summary>
        /// Dependency Eigenschaft welche den Text des Subtitels zurückgibt (nicht fett, rechts neben Titel)
        /// </summary>
        public string DialogSubTitle
        {
            get
            {
                return (string)GetValue(DialogSubTitleProperty);
            }
            set
            {
                SetValue(DialogSubTitleProperty, value);
            }
        }


        /// <summary>
        /// Dependency Eigenschaft welche den style des Close-Buttons setzt
        /// </summary>
        public Style CloseButtonStyle
        {
            get
            {
                return (Style)GetValue(CloseButtonStyleProperty);
            }
            set
            {
                SetValue(CloseButtonStyleProperty, value);
            }
        }

        /// <summary>
        /// Dependency Eigenschaft welche den style des Help-Buttons setzt
        /// </summary>
        public Style HelpButtonStyle
        {
            get
            {
                return (Style)GetValue(HelpButtonStyleProperty);
            }
            set
            {
                SetValue(HelpButtonStyleProperty, value);
            }
        }

        /// <summary>
        /// Dependency Eigenschaft welche den style des Print-Buttons setzt
        /// </summary>
        public Style PrintButtonStyle
        {
            get
            {
                return (Style)GetValue(PrintButtonStyleProperty);
            }
            set
            {
                SetValue(PrintButtonStyleProperty, value);
            }
        }

        /// <summary>
        /// Dependency Eigenschaft welche den style des Minimize-Buttons setzt
        /// </summary>
        public Style MinimizeButtonStyle
        {
            get
            {
                return (Style)GetValue(MinimizeButtonStyleProperty);
            }
            set
            {
                SetValue(MinimizeButtonStyleProperty, value);
            }
        }

        /// <summary>
        /// Dependency Eigenschaft welche den style des Maximize-Buttons setzt
        /// </summary>
        public Style MaximizeButtonStyle
        {
            get
            {
                return (Style)GetValue(MaximizeButtonStyleProperty);
            }
            set
            {
                SetValue(MaximizeButtonStyleProperty, value);
            }
        }

        /// <summary>
        /// Dependency Eigenschaft welche den style des Maximize-Buttons setzt
        /// </summary>
        public Style NormalizeButtonStyle
        {
            get
            {
                return (Style)GetValue(NormalizeButtonStyleProperty);
            }
            set
            {
                SetValue(NormalizeButtonStyleProperty, value);
            }
        }

        /// <summary>
        /// Dependency Eigenschaft welche den style des Dialog-Borders setzt
        /// </summary>
        public Style DialogBorderStyle
        {
            get
            {
                return (Style)GetValue(DialogBorderStyleProperty);
            }
            set
            {
                SetValue(DialogBorderStyleProperty, value);
            }
        }

        /// <summary>
        /// Dependency Eigenschaft welche den style des Dialog-Borders setzt
        /// </summary>
        public Style DialogContentBorderStyle
        {
            get
            {
                return (Style)GetValue(DialogContentBorderStyleProperty);
            }
            set
            {
                SetValue(DialogContentBorderStyleProperty, value);
            }
        }

        /// <summary>
        /// Dependency Eigenschaft welche den style der Dialog-Buttons setzt
        /// </summary>
        public Style DialogButtonStyle
        {
            get
            {
                return (Style)GetValue(DialogButtonStyleProperty);
            }
            set
            {
                SetValue(DialogButtonStyleProperty, value);
            }
        }

        /// <summary>
        /// Liefert die Instanz des Host-Dialoges.
        /// </summary>
        /// <remarks>Diese Eigenschaft kann auch einen null Wert liefern, wenn das OverlayControl momentan initialisiert,
        /// aber noch nicht im VisualTree vorhanden ist.</remarks>
        public OverlayDialog ParentDialog
        {
            get
            {
                return UIHelper.TryFindParent<OverlayDialog>(this);
            }
        }

        /// <summary>
        /// Gets the parent dialog's error provider instance
        /// </summary>
        public ErrorProvider ErrorProvider
        {
            get
            {
                OverlayDialog parentDialog = ParentDialog;

                if (parentDialog != null)
                    return parentDialog.ErrorProvider;
                
                return null;
            }
        }

        /// <summary>
        /// Gets the parent dialog's change information provider instance
        /// </summary>
        public ChangeInformationProvider ChangeInfoProvider
        {
            get
            {
                OverlayDialog parentDialog = ParentDialog;

                if (parentDialog != null)
                    return parentDialog.ChangeInfoProvider;

                return null;
            }
        }
        /// <summary>
        /// Liefert die Instanz des OverlayManagers für dieses content control
        /// </summary>
        /// <remarks>Die Eigenschaft kann auch einen NULL Wert liefern, wenn das Control nicht im OverlayManager angezeigt wird.</remarks>
        public IOverlayManagerBase OverlayManager
        {
            get
            {
                if (_overlay_manager != null)
                    return _overlay_manager as IOverlayManagerBase;

                _overlay_manager = UIHelper.TryFindParentGeneric(this, typeof(OverlayManagerBase<BaseFrame, SubMenuPanel, WindowTaskBar, WindowTitleBar, OverlayManager, SubMenuPanelMenuItem>));
                return _overlay_manager as IOverlayManagerBase;
            }
        }

        /// <summary>
        /// gets a flag indicating if the overlay content has dialog buttons
        /// </summary>
        public bool HasButtons
        {
            get
            {
                return _buttons.Count > 0;
            }
        }

        /// <summary>
        /// wenn true dann wird der dialog mit einem schatten im hintergrund angezeigt
        /// </summary>
        public bool? EnableDialogShadow
        {
            get;
            set;
        }

        /// <summary>
        /// wenn true dann wird der dialog mit einem schatten im hintergrund angezeigt
        /// </summary>
        public bool? EnableBackgroundShadow
        {
            get;
            set;
        }


        /// <summary>
        /// wenn auf true dann wird der dialog automatisch zentral positioniert
        /// </summary>
        public bool AutoCenteredDialog
        {
            get;
            set;
        }

        public UIElement DialogIcon
        {
            get; 
            set;
        }

        /// <summary>
        /// wenn auf true dann beeinträchtigt die hilfe und die fehlerdarstellung das center verhalten
        /// </summary>
        public bool HelpAndErrorViewAffectCentering
        {
            get;
            set;
        }

        /// <summary>
        /// Eigenschaft welche definiert ob sich der content geändert hat
        /// </summary>
        public bool HasChanged
        {
            get { return _has_changed; }
        }

        public bool IsInitialized
        {
            get { return _initialized; }
        }

        /// <summary>
        /// Eigenschaft die den Titel der Statusinformation während des Hintergrundspeicherns angibt.<br/>
        /// NOTIZ: Diese Eigenschaft ändert nicht den Titel bei angezeigter Statusinformation. Nur beim nächstmaligen speichern wird der Titel dann verwendet.
        /// </summary>
        public string BackgroundSavingStatusInfoTitle
        {
            get;
            set;
        }

        internal bool MarkedClosed { get; set; }

        public bool CanReportStatus
        {
            get
            {
                return (OverlayManager != null && OverlayManager is IOverlayManagerBase && ((IOverlayManagerBase)OverlayManager).IsBlockingStatusInfoVisible());
            }
        }

        public bool ResizesWithWindowInMaximizedMode
        {
            get { return (bool)GetValue(ResizesWithWindowInMaximizedModeProperty); }
            set { SetValue(ResizesWithWindowInMaximizedModeProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ResizesWithWindow.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ResizesWithWindowInMaximizedModeProperty =
            DependencyProperty.Register("ResizesWithWindowInMaximizedMode", typeof(bool), typeof(OverlayContent), new UIPropertyMetadata(false));

        
        #endregion

        #region Virtuelle Properties die überschrieben werden können/müssen
        public virtual bool CanPrint
        {
            get { return false; }
        }

        public virtual bool CanClose
        {
            get { return true; }
        }

        public virtual bool AutoResized
        {
            get { return false; }
        }

        public Vector2f InitialPosition
        {
            get { return _initial_position; }
            set { _initial_position = value; }
        }

        public virtual bool SearchFreePosition
        {
            get { return false; }
        }

        public virtual Thickness SearchFreePositionMargin
        {
            get { return new Thickness();}
        }

        public virtual SearchPositionAlignmentEnum SearchPositionAlignment
        {
            get { return SearchPositionAlignmentEnum.Left; }
        }

        public virtual bool CanHelp
        {
            get { return false; }
        }

        

        public virtual Type DialogSubmenuPanelItemType
        {
            get
            {
                return _queried_dialog_submenupanel_item_type;
            }
        }

        internal Type GetDialogSubmenuPanelItemType()
        {
            if (QueryCustomDialogSubmenuPanelItemType != null && !_has_queried_dialog_submenupanel_item_type)
            {
                QueryDialogSubMenuPanelItemTypeEventArgs args = new QueryDialogSubMenuPanelItemTypeEventArgs(typeof(DialogMenuItem));
                QueryCustomDialogSubmenuPanelItemType(this, args);

                if (args.NewType != null)
                {
                    _queried_dialog_submenupanel_item_type = args.NewType;
                    _has_queried_dialog_submenupanel_item_type = true;
                }
            }

            return DialogSubmenuPanelItemType;
        }


        public virtual OverlayDialogMaximizeMode MaximizeMode
        {
            get
            {
                return OverlayDialogMaximizeMode.Normal;
            }
        }

        public virtual OverlayDialogInitialShowMode InitialShowMode
        {
            get
            {
                return OverlayDialogInitialShowMode.Optimized;
            }
        }

        public virtual OverlayDialogCenteringMode CenteringMode
        {
            get
            {
                return OverlayDialogCenteringMode.Normal;
            }
        }

        /// <summary>
        /// Wenn dieses Property auf true gesetzt ist, wird der Dialog automatisch an den vorhandenen Bildschirmplatz angepasst
        /// Anderenfalls bleibt die Dialoggröße unverändert. Dieses Property überschreiben, wenn ein anderes Verhalten als das Standardverhalten erwünscht ist.
        /// Default: true
        /// </summary>
        public virtual bool AdjustDialogToScreenshot
        {
            get { return true; }
        }
        #endregion

        #region Opertations
        /// <summary>
        /// Adds a new overlay dialog button
        /// </summary>
        /// <param name="button"></param>
        public void AddButton(OverlayDialogButton button)
        {
            _buttons.Add(button);
            
        }
        /// <summary>
        /// Removes an overlay dialog button
        /// </summary>
        /// <param name="button"></param>
        public void RemoveButton(OverlayDialogButton button)
        {
            _buttons.Remove(button);
        }

        /// <summary>
        /// Returns the button object with the given text
        /// </summary>
        /// <param name="text"></param>
        public OverlayDialogButton GetButtonByText(string text)
        {
            foreach (OverlayDialogButton curButton in _buttons)
                if (curButton.Content != null && curButton.Content is string && curButton.Content.ToString() == text)
                    return curButton;

            return null;
        }
        /// <summary>
        /// Gets the overlay button at a given index position of an alignment group
        /// </summary>
        /// <param name="idx"></param>
        /// <param name="alignment"></param>
        /// <returns></returns>
        public OverlayDialogButton GetButtonByIndex(int idx, OverlayButtonAlignment alignment)
        {
            int alignment_idx = 0;

            foreach (OverlayDialogButton curButton in _buttons)
            {
                if(curButton.ButtonAlignment == alignment)
                {
                    if (alignment_idx == idx)
                        return curButton;

                    alignment_idx++;
                }
            }

            return null;
        }

        /// <summary>
        /// Initialize buttons and add event handlers
        /// </summary>
        public void InternalInitButtons(OverlayDialog parent)
        {
            if (parent == null)
                parent = ParentDialog;

            // overlay dialog instance is available at this point
            if (parent != null)
            {
                foreach(OverlayDialogButton curButton in _buttons)
                {
                    if(parent.AddDialogButton(curButton))
                    {
                        curButton.Click += new RoutedEventHandler(OverlayDialogButton_Click);
                    }
                }
            }
        }
        /// <summary>
        /// Clear button assignments
        /// </summary>
        public void InternalClearButtons()
        {
            // overlay dialog instance is available at this point
            if (ParentDialog != null)
            {
                foreach (OverlayDialogButton curButton in _buttons)
                {
                    StackPanel buttonHost = ParentDialog.GetButtonPanelForAlignment(curButton.ButtonAlignment);

                    if (buttonHost.Children.Contains(curButton))
                    {
                        BindingOperations.ClearBinding(curButton, StyleProperty);
                        buttonHost.Children.Remove(curButton);
                        curButton.Click -= OverlayDialogButton_Click;
                    }
                }
            }
        }

        internal bool SetInputFocus()
        {
            foreach (OverlayDialogButton curButton in _buttons)
            {
                if (curButton.ButtonKeyAssignment == Key.Enter)
                    if (curButton.Focus())
                        return true;
            }

            return RecursiveFocus(this);
        }

        protected bool RecursiveFocus(DependencyObject parent)
        {
            bool bRet = false;

            foreach (object child in LogicalTreeHelper.GetChildren(parent))
            {
                if (child != null && child is UIElement)
                {
                    if (((UIElement)child).Focus())
                    {
                        return true;
                    }
                    else
                    {
                        bRet = RecursiveFocus(((UIElement)child));
                        if (bRet)
                            return true;
                    }
                }
            }

            return false;
        }

        private void UnregisterUiUpdateEvents()
        {
            foreach(FrameworkElement elem in _registered_controls)
            {
                if(elem is TextBox)
                {
                    TextBox tb = elem as TextBox;
                    try
                    {
                        tb.TextChanged -= tb_TextChanged;
                       
                    }
                    catch
                    {}
                }
                else if(elem is ComboBox)
                {
                    ComboBox cb = elem as ComboBox;
                    try
                    {
                        cb.SelectionChanged -= cb_SelectionChanged;

                    }
                    catch
                    { }
                }
                else if(elem is TabControl)
                {
                    TabControl tc = elem as TabControl;
                    try
                    {
                        tc.SelectionChanged -= tc_SelectionChanged;
                    }
                    catch
                    { }
                }
            }

            _registered_controls.Clear();
        }

        private void RegisterUiUpdateEvents(DependencyObject parent)
        {
            List<TextBox> text_boxes = UIHelper.GetVisualChildrenByType<TextBox>(parent);
            List<ComboBox> combo_boxes = UIHelper.GetVisualChildrenByType<ComboBox>(parent);
            List<TabControl> tab_controls = UIHelper.GetVisualChildrenByType<TabControl>(parent);

            foreach(TextBox tb in text_boxes)
            {
                if(!_registered_controls.Contains(tb))
                {
                    tb.TextChanged += new TextChangedEventHandler(tb_TextChanged);
                    _registered_controls.Add(tb);
                }
            }

            foreach (ComboBox cb in combo_boxes)
            {
                if (!_registered_controls.Contains(cb))
                {
                    cb.SelectionChanged += new SelectionChangedEventHandler(cb_SelectionChanged);
                    _registered_controls.Add(cb);
                }
            }

            foreach (TabControl tc in tab_controls)
            {
                if (!_registered_controls.Contains(tc))
                {
                    tc.SelectionChanged += new SelectionChangedEventHandler(tc_SelectionChanged);
                    _registered_controls.Add(tc);
                    
                }

                foreach (TabItem tabItem in tc.Items)
                {
                    if (tabItem.Content != null)
                    {
                        RegisterUiUpdateEvents(tabItem.Content as DependencyObject);
                    }
                }
            }
        }

        public void UpdateImageRenderBehaviourRegistrations()
        {
            //if(ImageRenderBehaviour == ImageRenderBehaviour.Intelligent)
            {
                RegisterUiUpdateEvents(this);
            }
        }

        internal void RegisterFocusManagementEvents()
        {
            RegisterFocusManagementEvents(this);
        }

        public void RegisterFocusManagementEvents(DependencyObject parent)
        {
            List<TextBox> text_boxes = UIHelper.GetVisualChildrenByType<TextBox>(parent);
            List<Button> buttons = UIHelper.GetVisualChildrenByType<Button>(parent);
            List<CheckBox> checkboxes = UIHelper.GetVisualChildrenByType<CheckBox>(parent);
            List<RadioButton> radiobuttons = UIHelper.GetVisualChildrenByType<RadioButton>(parent);
            List<ComboBox> combo_boxes = UIHelper.GetVisualChildrenByType<ComboBox>(parent);
            List<TabControl> tab_controls = UIHelper.GetVisualChildrenByType<TabControl>(parent);

            foreach (TextBox tb in text_boxes)
            {
                if (!_focus_registered_controls.Contains(tb))
                {
                    Keyboard.AddGotKeyboardFocusHandler(tb, KeyboardFocus_Changed);
                    _focus_registered_controls.Add(tb);
                }
            }

            foreach (Button btn in buttons)
            {
                if (!_focus_registered_controls.Contains(btn))
                {
                    Keyboard.AddGotKeyboardFocusHandler(btn, KeyboardFocus_Changed);
                    _focus_registered_controls.Add(btn);
                }
            }

            foreach (CheckBox btn in checkboxes)
            {
                if (!_focus_registered_controls.Contains(btn))
                {
                    Keyboard.AddGotKeyboardFocusHandler(btn, KeyboardFocus_Changed);
                    _focus_registered_controls.Add(btn);
                }
            }

            foreach (RadioButton btn in radiobuttons)
            {
                if (!_focus_registered_controls.Contains(btn))
                {
                    Keyboard.AddGotKeyboardFocusHandler(btn, KeyboardFocus_Changed);
                    _focus_registered_controls.Add(btn);
                }
            }

            foreach (ComboBox cb in combo_boxes)
            {
                if (!_focus_registered_controls.Contains(cb))
                {
                    Keyboard.AddGotKeyboardFocusHandler(cb, KeyboardFocus_Changed);
                    _focus_registered_controls.Add(cb);
                }
            }

            foreach (TabControl tc in tab_controls)
            {
                if (!_focus_registered_controls.Contains(tc))
                {
                    //Keyboard.AddGotKeyboardFocusHandler(tc, KeyboardFocus_Changed);
                    tc.SelectionChanged += new SelectionChangedEventHandler(tc_focus_SelectionChanged);
                    _focus_registered_controls.Add(tc);
                }

                foreach (TabItem tabItem in tc.Items)
                {
                    if (tabItem.Content != null)
                    {
                        RegisterFocusManagementEvents(tabItem.Content as DependencyObject);
                    }
                }
            }
        }

        private void UnregisterFocusManagementEvents()
        {
            foreach (DependencyObject elem in _focus_registered_controls)
            {
                if (elem is TabControl)
                {
                    TabControl tc = elem as TabControl;
                    try
                    {
                        tc.SelectionChanged -= tc_focus_SelectionChanged;
                    }
                    catch
                    { }
                }
                else
                {
                    Keyboard.RemoveGotKeyboardFocusHandler(elem, KeyboardFocus_Changed);
                }
            }

            _focus_registered_controls.Clear();
        }

        public void RestoreKeyboardFocus()
        {
            if (_lastElementWithKeyboardFocus is IInputElement)
                Keyboard.Focus(_lastElementWithKeyboardFocus as IInputElement);
                //((FrameworkElement) _lastElementWithKeyboardFocus).Focus();
        }

        public List<DependencyObject> GetAllErrorProviderRegisteredControls()
        {
            List<DependencyObject> all_visual_children = null;
            List<DependencyObject> error_provider_registered_children = new List<DependencyObject>();
            all_visual_children = UIHelper.GetAllVisualChildren(this);

            foreach (DependencyObject obj in all_visual_children)
            {
                if (ErrorProvider.GetRegister(obj))
                    error_provider_registered_children.Add(obj);
            }

            return error_provider_registered_children;
        }
        #endregion

        #region Background Saving
        public void DoBackgroundSaving(object argument)
        {
            AbortBackgroundSaving();

            _background_save_data = new QueuedBackgroundWorker();
            _background_save_data.Name = this.GetType().Name + " Background Saving";
            _background_save_data.DoWork += new QueuedBackgroundWorker.DoWorkEventHandler(background_saving_DoWork);
            _background_save_data.RunWorkerCompleted += new QueuedBackgroundWorker.RunWorkerCompletedEventHandler(background_saving_RunWorkerCompleted);
            _background_save_data.RunWorkerAsync(argument);
        }

        public void AbortBackgroundSaving()
        {
            if (_background_save_data != null && _background_save_data.IsBusy)
            {
                _background_save_data.Abort();
                //_background_save_data.Join();
                _background_save_data = null;
            }
        }

        /// <summary>
        /// Override this method to implement background saving
        /// </summary>
        /// <param name="argument"></param>
        /// <returns></returns>
        protected  virtual object BackgroundSave_Worker(object argument)
        {
  
            return null;
        }

        /// <summary>
        /// Override this method to do a task after background saving
        /// </summary>
        /// <param name="argument"></param>
        /// <param name="result"></param>
        /// <param name="error"></param>
        protected virtual void BackgroundSave_Worker_Completed(object argument, object result, Exception error)
        {
            
        }

        private void background_saving_DoWork(object sender, Logicx.Utilities.DoWorkEventArgs e)
        {
            bool bMinimize = false;
            bool bShowStatus = false;

            // Dependency Properties brauchen Dispatcher Access !!
            if (Dispatcher.CheckAccess())
            {
                bMinimize = MinimizeToStatusInfoDuringBackgroundSaving;
                bShowStatus = ShowStatusInfoDuringBackgroundSaving;
            }
            else
            {
                Dispatcher.Invoke(DispatcherPriority.Normal, (NoParas)delegate
                {
                    bMinimize = MinimizeToStatusInfoDuringBackgroundSaving;
                    bShowStatus = ShowStatusInfoDuringBackgroundSaving;
                });
            }


            if (bMinimize)
            {
                if (Dispatcher.CheckAccess())
                {
                    SaveMinimizeToPosition();
                }
                else
                {
                    Dispatcher.Invoke(DispatcherPriority.Normal, (NoParas) delegate
                       {
                           SaveMinimizeToPosition();
                       });
                }

            }

            if (bShowStatus)
            {
                if (OverlayManager != null && OverlayManager is IOverlayManagerBase)
                {
                    ((IOverlayManagerBase)OverlayManager).ShowBlockingStatusInfo("Daten speichern ...", BackgroundSavingStatusInfoTitle);
                    ((IOverlayManagerBase)OverlayManager).UpdateBlockingStatusInfo(0, "");
                }
            }

            _background_save_argument = e.Argument;
            e.Result = BackgroundSave_Worker(e.Argument);
        }

        private void background_saving_RunWorkerCompleted(object sender, Logicx.Utilities.RunWorkerCompletedEventArgs e)
        {
            _background_save_data.DoWork -= background_saving_DoWork;
            _background_save_data.RunWorkerCompleted -= background_saving_RunWorkerCompleted;

            bool bMinimize = false;
            bool bShowStatus = false;

            // Dependency Properties brauchen Dispatcher Access !!
            if (Dispatcher.CheckAccess())
            {
                bMinimize = MinimizeToStatusInfoDuringBackgroundSaving;
                bShowStatus = ShowStatusInfoDuringBackgroundSaving;
            }
            else
            {
                Dispatcher.Invoke(DispatcherPriority.Normal, (NoParas)delegate
                {
                    bMinimize = MinimizeToStatusInfoDuringBackgroundSaving;
                    bShowStatus = ShowStatusInfoDuringBackgroundSaving;
                });
            }

            // call completed method
            BackgroundSave_Worker_Completed(_background_save_argument, e.Result, e.Error);

            if (bMinimize && !MarkedClosed && _must_recover_position)
            {
                if (Dispatcher.CheckAccess())
                {
                    SaveRestoreNormalPosition();
                }
                else
                {
                    Dispatcher.Invoke(DispatcherPriority.Normal, (NoParas)delegate
                    {
                        SaveRestoreNormalPosition();
                    });
                }

            }

            if (bShowStatus)
            {
                Dispatcher.Invoke(DispatcherPriority.Normal, (NoParas)delegate
                {
                    if (OverlayManager != null && OverlayManager is IOverlayManagerBase)
                        ((IOverlayManagerBase)OverlayManager).HideBlockingStatusInfo();
                });
            }

            if (Dispatcher.CheckAccess())
            {
                if(BackgroundSavingCompleted != null)
                    BackgroundSavingCompleted(this, new BackgroundSaveCompletionEventArgs(e.Error == null, _background_save_argument, e.Result, e.Error));
            }
            else
            {
                Dispatcher.Invoke(DispatcherPriority.Normal, (NoParas)delegate
                {
                    if(BackgroundSavingCompleted != null)
                    BackgroundSavingCompleted(this, new BackgroundSaveCompletionEventArgs(e.Error == null, _background_save_argument, e.Result, e.Error));
                });
            }
        }

        private void SaveMinimizeToPosition()
        {
            _must_recover_position = false;

            if (this.ParentDialog != null && this.OverlayManager != null)
            {
                OverlayWindowMeta metaInfo = this.ParentDialog.DialogMeta;
                if (metaInfo != null)
                {
                    _last_window_position = new Point(Canvas.GetLeft(metaInfo.Dialog), Canvas.GetTop(metaInfo.Dialog));
                    ((IOverlayManagerBase)OverlayManager).OverlayWindowManager.MinimizeDialogToPosition(metaInfo, _last_window_position, new Point(650, Application.Current.MainWindow.ActualHeight / 2), true);
                    _must_recover_position = true;
                }
                
            }
        }

        private void SaveRestoreNormalPosition()
        {
            _must_recover_position = false;

            if (this.ParentDialog != null && this.OverlayManager != null)
            {
                OverlayWindowMeta metaInfo = this.ParentDialog.DialogMeta;
                if (metaInfo != null)
                {
                    ((IOverlayManagerBase)OverlayManager).OverlayWindowManager.NormalizeDialogToPosition(metaInfo, new Point(650, Application.Current.MainWindow.ActualHeight / 2), _last_window_position, true);
                }

            }
        }
        #endregion

        #region Eventhandler
        #region Unload
        void OverlayContent_Unloaded(object sender, RoutedEventArgs e)
        {

            this.Unloaded -= OverlayContent_Unloaded;
            //DependencyPropertyDescriptor descr = DependencyPropertyDescriptor.FromProperty(HelpProvider.HelpProvider.HelpScopeProperty, this.GetType());
            //if (descr != null)
            //{
            //    descr.RemoveValueChanged(this, OverlayContent_HelpScopeChanged);
            //}

            //DependencyPropertyDescriptor descr1 = DependencyPropertyDescriptor.FromProperty(ImageRenderBehaviourProperty, this.GetType());
            //if (descr1 != null)
            //{
            //    descr1.RemoveValueChanged(this, ImageRenderBehaviour_Changed);
            //}            
        }

        private void UnregisterEvents(EventHandler<OverlayWindowMeta.OverlayAnimationCompletedArgs> event_handler)
        {
            if (event_handler == null)
                return;

            foreach (EventHandler<OverlayWindowMeta.OverlayAnimationCompletedArgs> eventDelegate in event_handler.GetInvocationList())
                event_handler -= eventDelegate;

        }

        private void UnregisterEvents(EventHandler event_handler)
        {
            if (event_handler == null)
                return;

            foreach (EventHandler eventDelegate in event_handler.GetInvocationList())
                event_handler -= eventDelegate;

        }

        private void UnregisterEvents(PropertyChangedEventHandler event_handler)
        {
            if (event_handler == null)
                return;

            foreach (PropertyChangedEventHandler eventDelegate in event_handler.GetInvocationList())
                event_handler -= eventDelegate;
        }

        private void UnregisterEvents(EventHandler<QueryDialogSubMenuPanelItemTypeEventArgs> event_handler)
        {
            if (event_handler == null)
                return;

            foreach (EventHandler<QueryDialogSubMenuPanelItemTypeEventArgs> eventDelegate in event_handler.GetInvocationList())
                event_handler -= eventDelegate;
        }
        #endregion

        #region keyboard focus management
        private void KeyboardFocus_Changed(object sender, KeyboardFocusChangedEventArgs e)
        {
            if (ParentDialog != null && OverlayManager != null && OverlayManager is IOverlayManagerBase)
            {
                if(!ParentDialog.IsActive)
                {
                    OverlayWindowMeta thisMeta = ((IOverlayManagerBase)OverlayManager).GetMetaForContent(this);
                    if(!thisMeta.IsAnimationRunning)
                        ((IOverlayManagerBase)OverlayManager).SetActiveDialog(thisMeta);
                }
            }

            if (ImageRenderBehaviour == ImageRenderBehaviourEnum.Intelligent)
            {
                if (_lastElementWithKeyboardFocus is TextBox && !_textboxes_focused.Contains(_lastElementWithKeyboardFocus as TextBox))
                {
                    // store textboxes which received the input focus once in a temporary list
                    // for deregistration of TextContainerChange(d) delegates
                    _textboxes_focused.Add(_lastElementWithKeyboardFocus as TextBox);
                }

                _lastElementWithKeyboardFocus = sender as DependencyObject;

                
#if DEBUG
                string sInfo = "{0} with Name '{1}' recevied Keyboard-Focus.";
                string sname = "";

                if (sender is FrameworkElement)
                    sname = ((FrameworkElement) sender).Name;

                Debug.WriteLine(string.Format(sInfo, sender.GetType().Name, sname));
#endif
            }
        }
        void tc_focus_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            TabControl tabSender = sender as TabControl;

            if (tabSender != null && tabSender.SelectedIndex >= 0 && tabSender.Items[tabSender.SelectedIndex] is TabItem)
            {
                RegisterFocusManagementEvents((tabSender.Items[tabSender.SelectedIndex] as TabItem).Content as DependencyObject);
                //RegisterFocusManagementEvents(tabSender.Items[tabSender.SelectedIndex] as DependencyObject);
            }
        }
        #endregion

        #region UI update event handler
        void tb_TextChanged(object sender, TextChangedEventArgs e)
        {
            DialogImageDirty = true;
        }
        void cb_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DialogImageDirty = true;
        }

        void tc_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DialogImageDirty = true;
            TabControl tabSender = sender as TabControl;

            if (tabSender != null && tabSender.SelectedIndex >= 0)
            {
                RegisterUiUpdateEvents(tabSender.Items[tabSender.SelectedIndex] as DependencyObject);
            }
        }
        #endregion

        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            if(e.Property == HelpProvider.HelpProvider.HelpScopeProperty)
            {
                string sScope = HelpProvider.HelpProvider.GetHelpScope(this);
                if (!string.IsNullOrEmpty(sScope))
                {
                    if (ParentDialog != null)
                        ParentDialog.SetHelpScopeForDialog(sScope);
                }
            }
            else if(e.Property == ImageRenderBehaviourProperty)
            {
                if (ImageRenderBehaviour != ImageRenderBehaviourEnum.Intelligent)
                {
                    UnregisterUiUpdateEvents();
                }
                else
                {
                    RegisterUiUpdateEvents(this);
                }
            }

            base.OnPropertyChanged(e);
        }
        /// <summary>
        /// Called as soon as the help scope attached property changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void OverlayContent_HelpScopeChanged(object sender, EventArgs e)
        {
            string sScope = HelpProvider.HelpProvider.GetHelpScope(this);
            if(!string.IsNullOrEmpty(sScope))
            {
                if (ParentDialog != null)
                    ParentDialog.SetHelpScopeForDialog(sScope);
            }

            // remove value changed handler
            DependencyPropertyDescriptor descr = DependencyPropertyDescriptor.FromProperty(HelpProvider.HelpProvider.HelpScopeProperty, this.GetType());
            if (descr != null)
            {
                descr.RemoveValueChanged(this, OverlayContent_HelpScopeChanged);
            }
        }

        void ImageRenderBehaviour_Changed(object sender, EventArgs e)
        {
            if (ImageRenderBehaviour != ImageRenderBehaviourEnum.Intelligent)
            {
                UnregisterUiUpdateEvents();
            }
            else
            {
                RegisterUiUpdateEvents(this);
            }
        }

        /// <summary>
        /// Overlay dialog button clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void OverlayDialogButton_Click(object sender, RoutedEventArgs e)
        {
            e.Handled = true;
            if (ParentDialog.IsResizing)
            {
                ParentDialog.StopResizing();
                return;
            }

            if (sender is OverlayDialogButton)
                ((OverlayDialogButton)sender).ShowActionIndicator = true;

            bool reset_action_indicator = OverlayButtonClicked(sender as OverlayDialogButton);

            if (sender is OverlayDialogButton && reset_action_indicator)
                ((OverlayDialogButton)sender).ShowActionIndicator = false;
        }
        
        #endregion

        #region Virtuelle methoden welche überschrieben werden können
        /// <summary>
        /// wird aufgerufen wenn eine Taste gedrückt wird
        /// </summary>
        /// <param name="e"></param>
        public virtual void OverlayKeyUp(System.Windows.Input.KeyEventArgs e)
        {
            if (e.IsUp)
            {
                if (e.Key == Key.Escape)
                {
                    e.Handled = true;

                    // force dialog closeure
                    if (OverlayManager != null && OverlayManager is IOverlayManagerBase)
                           ((IOverlayManagerBase)OverlayManager).HideContent(this, false);
                }
                else
                {
                    // loog through standard key assignments

                    foreach (OverlayDialogButton curButton in _buttons)
                    {
                        if (curButton.ButtonKeyAssignment == e.Key)
                        {
                           curButton.ShowActionIndicator = true;

                           bool reset_action_indicator = OverlayButtonClicked(curButton);

                            if ( reset_action_indicator)
                                curButton.ShowActionIndicator = false;

                            return;
                        }
                    }
                }
            }
        }

        protected void ClearButtonActionIndicators()
        {
            foreach (OverlayDialogButton curButton in _buttons)
            {
                curButton.ShowActionIndicator = false;
            }
        }

        /// <summary>
        /// Wird aufgerufen, wenn der Benutzer einen der OverlayButtons klicked
        /// </summary>
        /// <param name="button"></param>
        public virtual bool OverlayButtonClicked(OverlayDialogButton button)
        {
            return true;
        }

        /// <summary>
        /// Wurd aufgerufen bevor das Control angezeigt wird
        /// </summary>
        /// <param name="firstTime">true wenn dieser Content in der Dialoginstanz das erste Mal angezeigt wird</param>
        /// <returns>false wenn das control nicht angezeigt werden soll.</returns>
        public virtual bool BeforeShow(bool firstTime)
        {
            return true;
        }
        /// <summary>
        /// Wird aufgerufen nachdem das Control angezeigt wurde
        /// </summary>
        public virtual void AfterShow()
        {

        }

        internal void InternalAfterShow()
        {
            MarkedClosed = false;
            AfterShow();
        }

        /// <summary>
        /// Wird aufgerufen wenn der aktuelle content angezeigt wird.
        /// </summary>
        /// <param name="firstTime"></param>
        /// <returns></returns>
        public virtual bool Show(bool firstTime)
        {
            return true;
        }

        /// <summary>
        /// Wird aufgerufen bevor das control geschlossen wird
        /// </summary>
        /// <param name="forceClose">True wenn das Control auf jeden Fall geschlossen wird.</param>
        /// <returns>false wenn das Control nicht geschlossen werden kann.</returns>
        /// <remarks>Wenn <see cref="forceClose"/> True ist, dann wird der 
        /// Rückgabewert ignoriert und das Control auf jeden Fall geschlossen.</remarks>
        public virtual bool BeforeClose(bool forceClose)
        {
            return true;
        }

        internal bool InternalBeforeClose(bool forceClose)
        {
            bool bRet = BeforeClose(forceClose);

            if(bRet)
            {
                // wenn der content geschlossen wird
                // eventuelle status informationen ausblenden
                if (OverlayManager != null && OverlayManager is IOverlayManagerBase && ((IOverlayManagerBase)OverlayManager).IsBlockingStatusInfoVisible())
                    ((IOverlayManagerBase)OverlayManager).HideBlockingStatusInfo();
            }

            return bRet;

        }
        /// <summary>
        /// Wird aufgerufen nachdem das Control geschlossen wurde
        /// </summary>
        public virtual void AfterClose()
        {
            
        }

        internal void InternalAfterClose()
        {
            MarkedClosed = true;
            AfterClose();

            OnOverlayContentClosed();

            _overlay_manager = null;
        }

        /// <summary>
        /// Wird aufgerufen wenn der aktuelle content geschlossen wird.
        /// </summary>
        /// <param name="forceClose"></param>
        /// <returns></returns>
        public virtual bool Close(bool forceClose)
        {
            return true;
        }

        /// <summary>
        /// Wurd aufgerufen bevor das Control gedruckt wird wird
        /// </summary>
        /// <returns>false wenn das control nicht gedruckt werden soll.</returns>
        public virtual bool BeforePrint()
        {
            return true;
        }
        /// <summary>
        /// wird aufgerufen nachdem das Control gedruckt wurde
        /// </summary>
        public virtual void AfterPrint()
        {

        }

        /// <summary>
        /// Wird aufgerufen wenn der aktuelle content gedruckt wird
        /// </summary>
        /// <returns></returns>
        public virtual bool Print()
        {
            return true;
        }

        /// <summary>
        /// Wird aufgerufen wenn die Hilfe für den aktuellen Dialog angezeigt werden soll. 
        /// </summary>
        /// <returns></returns>
        public virtual bool Help()
        {
            return true;
        }

        /// <summary>
        /// Flag welches bestimmt ob der content beim zentrieren auf die aktuelle client-size in zusammenhang
        /// mit dem submenupanel rücksicht genommen wird. Wenn false dann wird die gesamte overlaymanager größe
        /// für die berechnung herangezogen
        /// </summary>
        public virtual bool ClientAreaSizeSensitive
        {
            get { return false; }
        }

        /// <summary>
        /// Called if the overlay content gets closed
        /// </summary>
        protected virtual void OnOverlayContentClosed()
        {
            
            if (OverlayContentClosed != null)
                OverlayContentClosed(this, new EventArgs());
        }

        protected virtual void OnDataInitialized()
        {
            if (DataInitialized != null)
                DataInitialized(this, new EventArgs());
        }

        protected virtual void SetHasChanged()
        {
            _has_changed = true;
        }
        protected virtual void ClearHasChanged()
        {
            _has_changed = false;
        }

        protected virtual void Initialize(bool calledFromBackground)
        {

        }

        internal void InternalInitialize(bool calledFromBackground)
        {
            Initialize(calledFromBackground);

            _initialized = true;
            OnDataInitialized();

            if (Dispatcher.CheckAccess())
            {
                
                DialogImageDirty = true;
                RegisterFocusManagementEvents();
            }
            else
            {
                Dispatcher.Invoke(DispatcherPriority.Normal, (NoParas)delegate
                {
                    DialogImageDirty = true;
                    RegisterFocusManagementEvents();
                });
            }
        }

        internal void OnOverlayAnimationFinished(object sender, OverlayWindowMeta.OverlayAnimationCompletedArgs e)
        {
            if (OverlayAnimationFinished != null)
                OverlayAnimationFinished(sender, e);
        }

        /// <summary>
        /// Called if the local change information provider queries the content control for change information
        /// </summary>
        /// <param name="provider"></param>
        public virtual void OnLoadChanges(ChangeInformationProvider provider)
        {
            // set the provider.ProviderChangeList property to push change information
            // to all controls in the visual tree
        }

        internal void ActiveStateChanged(ActiveStateChangeArgs e)
        {
            OnActiveStateChanged(e);
        }

        /// <summary>
        /// This method will be called if the dialog changed its active state
        /// </summary>
        /// <param name="e"></param>
        protected virtual void OnActiveStateChanged(ActiveStateChangeArgs e)
        {

        }

        internal void WindowStateChanged(WindowStateChangeArgs e)
        {
            OnWindowStateChanged(e);
        }

        /// <summary>
        /// This method will be called if the dialog changed its window state
        /// </summary>
        /// <param name="e"></param>
        protected virtual void OnWindowStateChanged(WindowStateChangeArgs e)
        {
            
        }

        internal void DialogVisibilityChanged(Visibility newValue)
        {
            if (newValue == Visibility.Visible && _first_visible)
            {
                _first_visible = false;
                System.Timers.Timer focusTimer = new System.Timers.Timer(50);
                focusTimer.Elapsed += new System.Timers.ElapsedEventHandler(focusTimer_Elapsed);
                focusTimer.Start();
            }

            OnDialogVisibilityChanged(newValue);
        }

        /// <summary>
        /// This method will be called if the dialog changes its visiblity
        /// </summary>
        /// <param name="newValue"></param>
        protected virtual void OnDialogVisibilityChanged(Visibility newValue)
        {
        }

        /// <summary>
        /// This method will be called once after the dialog first changes its visiblity from hidden to vlisible
        /// </summary>
        protected virtual void OnSetInitialFocus()
        {
            RecursiveFocus(this);
        }

        internal void OnResizeFinished()
        {
            ResizeFinished();
        }

        protected virtual void ResizeFinished()
        {
            
        }

        protected void SendPropertyChanged(string property_name)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property_name));
        }

        void focusTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            System.Timers.Timer focusTimer = sender as System.Timers.Timer;
            focusTimer.Stop();
            focusTimer.Elapsed -= focusTimer_Elapsed;
            focusTimer.Dispose();
            Dispatcher.Invoke(DispatcherPriority.Normal, (NoParas)delegate { OnSetInitialFocus(); });
        }
        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            if (_disposed || _disposing)
                return;

            DisposeInternal(true);

            if(Dispatcher.CheckAccess())
            {
                // text boxes may contain TextContainerChange(d) event reference leaks
                List<AcCbx> accbx_boxes = UIHelper.GetVisualChildrenByType<AcCbx>(this);
                foreach (AcCbx child_accbx in accbx_boxes)
                {
                    child_accbx.Dispose();
                }
                accbx_boxes.Clear();


                List<TextBox> text_boxes = UIHelper.GetVisualChildrenByType<TextBox>(this);
                foreach (TextBox child_tb in text_boxes)
                {
                    if (Dispatcher.CheckAccess())
                    {
                        
                        //BindingOperations.ClearAllBindings(child_tb);
                    }
                    EventHelper.RemoveAllTextContainerChangedEventHanders(child_tb);
                    EventHelper.RemoveAllTextContainerChangeEventHanders(child_tb);
                }
                text_boxes.Clear();
            }
            UnregisterFocusManagementEvents();

            if (Dispatcher.CurrentDispatcher.CheckAccess())
                BindingOperations.ClearAllBindings(this);

            UnregisterEvents(OverlayContentClosed);
            UnregisterEvents(OverlayAnimationFinished);
            UnregisterEvents(DataInitialized);
            UnregisterEvents(PropertyChanged);
            UnregisterEvents(QueryCustomDialogSubmenuPanelItemType);

            foreach (OverlayDialogButton curButton in _buttons)
            {
                    curButton.Click -= OverlayDialogButton_Click;
            }

            if(_lastElementWithKeyboardFocus is TextBox)
            {

                EventHelper.RemoveAllTextContainerChangedEventHanders(_lastElementWithKeyboardFocus as TextBox);
                EventHelper.RemoveAllTextContainerChangeEventHanders(_lastElementWithKeyboardFocus as TextBox);
            }

            foreach(FrameworkElement elem in _registered_controls)
            {
                if(elem is TextBox)
                {
                    if (Dispatcher.CheckAccess())
                    {
                        //BindingOperations.ClearAllBindings(elem);
                    }
                    EventHelper.RemoveAllTextContainerChangedEventHanders(elem as TextBox);
                    EventHelper.RemoveAllTextContainerChangeEventHanders(elem as TextBox);
                }
            }

            foreach (FrameworkElement elem in _focus_registered_controls)
            {
                if (elem is TextBox)
                {
                    if (Dispatcher.CheckAccess())
                    {
                        //BindingOperations.ClearAllBindings(elem);
                    }
                    EventHelper.RemoveAllTextContainerChangedEventHanders(elem as TextBox);
                    EventHelper.RemoveAllTextContainerChangeEventHanders(elem as TextBox);
                }
            }

            foreach (TextBox child_tb in _textboxes_focused)
            {
                if (Dispatcher.CheckAccess())
                {
                    //BindingOperations.ClearAllBindings(child_tb);
                }
                EventHelper.RemoveAllTextContainerChangedEventHanders(child_tb);
                EventHelper.RemoveAllTextContainerChangeEventHanders(child_tb);
            }
            _textboxes_focused.Clear();
            

            _buttons.Clear();
            _registered_controls.Clear();
            _focus_registered_controls.Clear();
            _lastElementWithKeyboardFocus = null;
            _overlay_manager = null;

            _disposed = true;
        }

        private void DisposeInternal(bool disposing)
        {
            _disposing = true;
            Dispose(disposing);
        }

        protected virtual void Dispose(bool disposing)
        {
        }

        public bool Disposed { get { return _disposed; } }

        #endregion

        #region Attribs

        protected delegate void NoParas();


        #region Events

        //public event EventHandler LoadingAnimationCompleted;
        public event EventHandler<OverlayWindowMeta.OverlayAnimationCompletedArgs> OverlayAnimationFinished;
        public event EventHandler DataInitialized;
        public event EventHandler OverlayContentClosed;
        public event EventHandler BackgroundSavingCompleted;

        public event EventHandler<QueryDialogSubMenuPanelItemTypeEventArgs> QueryCustomDialogSubmenuPanelItemType;

        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        private bool _disposed = false;
        private bool _disposing = false;

        private bool _first_visible = true;

        private QueuedBackgroundWorker _background_save_data = null;
        private object _background_save_argument = null;

        private List<OverlayDialogButton> _buttons = new List<OverlayDialogButton>();
        private bool _has_changed = false;
        private bool _initialized = false;
        private DependencyObject _lastElementWithKeyboardFocus = null;
        private Point _last_window_position;
        private bool _must_recover_position = false;

        private List<FrameworkElement> _registered_controls = new List<FrameworkElement>();
        private List<FrameworkElement> _focus_registered_controls = new List<FrameworkElement>();
        private List<TextBox> _textboxes_focused = new List<TextBox>();

        #region Dependency properties
        public static readonly DependencyProperty DialogTitleProperty;
        public static readonly DependencyProperty DialogSubTitleProperty;

        public static readonly DependencyProperty DragVisualModeProperty;

        public static readonly DependencyProperty ContentIdProperty;
        private static readonly DependencyPropertyKey ContentIdPropertyKey;

        public static readonly DependencyProperty ImageRenderBehaviourProperty;
        public static readonly DependencyProperty DialogImageDirtyProperty;

        public static readonly DependencyProperty DefaultWindowStateProperty;
        public static readonly DependencyProperty InitializationBehaviourProperty;
        public static readonly DependencyProperty ModalBehaviourProperty;
        public static readonly DependencyProperty GoInactiveBehaviourProperty;

        public static readonly DependencyProperty ActivateStoryboardProperty;
        public static readonly DependencyProperty DeactivateStoryboardProperty;
        
        //public static readonly DependencyProperty LoadingStoryboardProperty;
        public static readonly DependencyProperty LoadingStartStoryboardProperty;
        public static readonly DependencyProperty LoadingStartStoryboardFinalPosProperty;
        public static readonly DependencyProperty LoadingEndStoryboardProperty;
        public static readonly DependencyProperty LoadingEndStoryboardFinalPosProperty;
        public static readonly DependencyProperty LoadingBehaviourProperty;

        public static readonly DependencyProperty MinimalSideLengthProperty;

        public static readonly DependencyProperty LoadingInformationControlProperty;

        public static readonly DependencyProperty ResizeModeProperty;

        public static readonly DependencyProperty ImageRenderOffsetWidthProperty;
        public static readonly DependencyProperty ImageRenderOffsetHeightProperty;

        public static readonly DependencyProperty ShowStatusInfoDuringBackgroundSavingProperty;
        public static readonly DependencyProperty MinimizeToStatusInfoDuringBackgroundSavingProperty;

        public static readonly DependencyProperty CloseButtonStyleProperty;
        public static readonly DependencyProperty PrintButtonStyleProperty;
        public static readonly DependencyProperty HelpButtonStyleProperty;

        public static readonly DependencyProperty MinimizeButtonStyleProperty;
        public static readonly DependencyProperty MaximizeButtonStyleProperty;
        public static readonly DependencyProperty NormalizeButtonStyleProperty;
        public static readonly DependencyProperty DialogButtonStyleProperty;

        public static readonly DependencyProperty DialogBorderStyleProperty;
        public static readonly DependencyProperty DialogContentBorderStyleProperty;
        #endregion

        private DependencyObject _overlay_manager = null;
        private Vector2f _initial_position = new Vector2f();

        private Type _queried_dialog_submenupanel_item_type = typeof (DialogMenuItem);
        private bool _has_queried_dialog_submenupanel_item_type = false;
        #endregion

    }
}
