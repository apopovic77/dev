﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Logicx.WpfUtility.UiManipulation
{
    public class TouchDrag : UiManipulator
    {
        public TouchDrag(UIElement ui_element, Canvas canvas, UIElement event_base, EnabledAxes enabled_axes) : this(ui_element, canvas, event_base, enabled_axes, true) { }
        public TouchDrag(UIElement ui_element, Canvas canvas, UIElement event_base, EnabledAxes enabled_axes, bool initialize_canvas_position) : base(ui_element, canvas, event_base, enabled_axes, initialize_canvas_position) { }
        
        #region Event Registration
        public override void Register()
        {
            _ui_element.StylusDown += StylusDown;
        }

        public override void DeRegister()
        {
            _ui_element.StylusDown -= StylusDown;
        }


        private void RegisterManipulationEvents()
        {
            if (_events_registered)
                return;

            SetCanvasBackground();
            _event_base.StylusUp += StylusUp;
            _event_base.StylusMove += StylusMove;
            _event_base.StylusLeave += StylusLeave;

            _events_registered = true;
        }

        private void DeRegisterManipulationEvents()
        {
            if (!_events_registered)
                return;

            UnsetCanvasBackground();
            _event_base.StylusUp -= StylusUp;
            _event_base.StylusMove -= StylusMove;
            _event_base.StylusLeave -= StylusLeave;

            _events_registered = false;
        }
        #endregion

        public void StylusDown(object sender, StylusDownEventArgs e)
        {
            //i am already dragging forget the second attemp to drag
            if (_events_registered)
                return;

            e.Handled = true;

            RegisterManipulationEvents();
            _my_stylus_id = e.StylusDevice.Id;
            _drag_offset = e.GetPosition(_ui_element);
            RaisUiManipulationStarted(_ui_element);
        }

        public void StylusUp(object sender, StylusEventArgs e)
        {
            if (e.StylusDevice.Id != _my_stylus_id)
                return;

            //e.Handled = true;

            DeRegisterManipulationEvents();
            RaiseUiManipulationCompleted(_ui_element);
        }

        public void StylusMove(object sender, StylusEventArgs e)
        {
            if (e.StylusDevice.Id != _my_stylus_id)
                return;

            e.Handled = true;

            Point pos = e.GetPosition(_canvas);
            Point ui_pos_next = new Point(
                                    pos.X - _drag_offset.X,
                                    pos.Y - _drag_offset.Y
                                       );

            if (_enabled_axes == EnabledAxes.YAxis || _enabled_axes == EnabledAxes.All)
                Canvas.SetTop(_ui_element, ui_pos_next.Y);
            if (_enabled_axes == EnabledAxes.XAxis || _enabled_axes == EnabledAxes.All)
                Canvas.SetLeft(_ui_element, ui_pos_next.X);

            RailsUiElementPositionChanged(_ui_element, ui_pos_next);
        }

        public void StylusLeave(object sender, StylusEventArgs e)
        {
            e.Handled = true;
            
            StylusUp(sender, e);
        }

        protected Point DragOffset
        {
            get { return _drag_offset; }
        }
        private int _my_stylus_id;
        private bool _events_registered;
        private Point _drag_offset;
    }
}
