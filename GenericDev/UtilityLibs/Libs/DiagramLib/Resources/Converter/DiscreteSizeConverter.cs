﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Data;
using Logicx.DiagramLib.Scales;

namespace Logicx.DiagramLib.Resources.Converter
{
    public class DiscreteSizeConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if(values.Length == 2 && values[1] is ScaleControlBase)
            {
                ScaleControlBase scale_control = values[1] as ScaleControlBase;

                    if (scale_control is DiscreteScaleControl<Object>)
                    {
                        double unit_size = scale_control.GetUnitSize();
                        //if(double.IsInfinity(DiscreteScale.UNIT_SIZE_FACTOR * unit_size) || double.IsNaN(DiscreteScale.UNIT_SIZE_FACTOR * unit_size))
                        //    Debug.WriteLine("INFINITY OR NAN IN DISCRETESIZECONVERTER");
                        return DiscreteScale.UNIT_SIZE_FACTOR * unit_size;
                    }
            }

            return 0;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
