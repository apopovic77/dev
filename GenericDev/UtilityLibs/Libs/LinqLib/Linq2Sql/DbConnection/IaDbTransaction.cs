﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace LinqLib.Linq2Sql.DbConnection
{
    public class IaDbTransaction : DbTransaction
    {
        #region construction and initialisation
        public IaDbTransaction(IaDbConnection connection)
        {
            _connection = connection;
            _internal_transaction = _connection.SqlConnection.BeginTransaction();
        }

        public IaDbTransaction(IaDbConnection connection, string transactionName)
        {
            _connection = connection;
            _internal_transaction = _connection.SqlConnection.BeginTransaction(transactionName);
        }

        public IaDbTransaction(IaDbConnection connection, IsolationLevel isolationLevel)
        {
            _connection = connection;
            _internal_transaction = _connection.SqlConnection.BeginTransaction(isolationLevel);
        }

        public IaDbTransaction(IaDbConnection connection, IsolationLevel isolationLevel, string transactionName)
        {
            _connection = connection;
            _internal_transaction = _connection.SqlConnection.BeginTransaction(isolationLevel, transactionName);
        }
        #endregion

        #region operations
        public override void Commit()
        {
            _internal_transaction.Commit();
        }

        public override System.Runtime.Remoting.ObjRef CreateObjRef(Type requestedType)
        {
            return _internal_transaction.CreateObjRef(requestedType);
        }

        protected override void Dispose(bool disposing)
        {
            _internal_transaction.Dispose();
            base.Dispose(disposing);
        }

        public override object InitializeLifetimeService()
        {
            return _internal_transaction.InitializeLifetimeService();
        }

        public override void Rollback()
        {
            _internal_transaction.Rollback();
        }
        #endregion

        #region properties
        public virtual string DebugIdentifier
        {
            get
            {
                return "###T" + this.GetHashCode().ToString();
            }
        }

        public IaDbConnection IaDbConnection
        {
            get { return _connection; }
        }

        public SqlTransaction SqlTransaction
        {
            get { return _internal_transaction; }
        }

        protected override System.Data.Common.DbConnection DbConnection
        {
            get { return _connection; }
        }

        public override IsolationLevel IsolationLevel
        {
            get { return _internal_transaction.IsolationLevel; }
        }

#if DEBUG
        public bool WithDebugOutput
        {
            get { return _connection.WithDebugOutput; }
            set { _connection.WithDebugOutput = value; }
        }
#endif

        #endregion

        #region attributes
        private SqlTransaction _internal_transaction = null;
        private IaDbConnection _connection = null;
        #endregion
    }
}
