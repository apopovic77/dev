﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace LinqLib.Linq2Sql.Notification
{
    public class SqlChangeNotificationMeta
    {
        #region Constants
        #endregion

        #region Construction and Initialization
        /// <summary>
        /// Constructor of the class
        /// </summary>
        /// <param name="table_name"></param>
        /// <param name="notification_key"></param>
        /// <param name="notification_command"></param>
        internal SqlChangeNotificationMeta(string connection_string, string table_name, string notification_key)
        {
            ConnectionString = connection_string;
            TableName = table_name;
            NotificationKey = notification_key;
            NotificationCommand = "SELECT [NotizifierzungsSchluessel], [AktuellerWert], [AlterWert], [AktuellerWert_Char], [AlterWert_Char], [HostName], [UserName] FROM [dbo].[" + table_name + "] WHERE [NotizifierzungsSchluessel] = '" + EscapeSqlString(notification_key) + "'";
        }
        #endregion

        #region Operations
        /// <summary>
        /// Start listening for data changes
        /// </summary>
        public void StartListening()
        {
            if (_sql_connection != null)
                return;

            _sql_connection = new SqlConnection(ConnectionString);

            _sql_command = new SqlCommand(NotificationCommand, _sql_connection);
            _sql_command.Notification = null;

            _sql_dependency = new SqlDependency(_sql_command); //, "service=AenderungsNotifizierungenService", 0);
            _sql_dependency.OnChange += new OnChangeEventHandler(_sql_dependency_OnChange);

            SqlDataAdapter adapter = new SqlDataAdapter(_sql_command);
            // Fill the DataSet.
            DataSet ds = new DataSet();
            adapter.Fill(ds);

            ExctractDataFromDataSet(ds);

        }

        
        /// <summary>
        /// Stop listening for data changes
        /// </summary>
        public void StopListening()
        {
            if (_sql_connection == null)
                return;

            if(_sql_dependency != null)
            {
                _sql_dependency.OnChange -= _sql_dependency_OnChange;
                _sql_dependency = null;
            }

            if (_sql_command != null)
                _sql_command = null;

            if(_sql_connection != null)
            {
                _sql_connection.Close();
                _sql_connection.Dispose();
                _sql_connection = null;
            }
        }

        /// <summary>
        /// Exctract data from data set
        /// </summary>
        /// <param name="change_notification_data"></param>
        private void ExctractDataFromDataSet(DataSet change_notification_data)
        {
            if(change_notification_data.Tables.Count > 0 && change_notification_data.Tables[0].Rows.Count > 0)
            {
                CurrentStringValue = OldStringValue;
                CurrentLongValue = OldLongValue;

                if(change_notification_data.Tables[0].Rows[0]["AktuellerWert"] == null || change_notification_data.Tables[0].Rows[0]["AktuellerWert"] == System.DBNull.Value)
                {
                    CurrentLongValue = null;    
                }
                else
                {
                    CurrentLongValue = (long?) change_notification_data.Tables[0].Rows[0]["AktuellerWert"];
                }

                if (change_notification_data.Tables[0].Rows[0]["AktuellerWert_Char"] == null || change_notification_data.Tables[0].Rows[0]["AktuellerWert_Char"] == System.DBNull.Value)
                {
                    CurrentStringValue = null;
                }
                else
                {
                    CurrentStringValue = Convert.ToString(change_notification_data.Tables[0].Rows[0]["AktuellerWert_Char"]);
                }



                if (change_notification_data.Tables[0].Rows[0]["AlterWert"] == null || change_notification_data.Tables[0].Rows[0]["AlterWert"] == System.DBNull.Value)
                {
                    OldLongValue = null;
                }
                else
                {
                    OldLongValue = (long?)change_notification_data.Tables[0].Rows[0]["AlterWert"];
                }

                if (change_notification_data.Tables[0].Rows[0]["AlterWert_Char"] == null || change_notification_data.Tables[0].Rows[0]["AlterWert_Char"] == System.DBNull.Value)
                {
                    OldStringValue = null;
                }
                else
                {
                    OldStringValue = Convert.ToString(change_notification_data.Tables[0].Rows[0]["AlterWert_Char"]);
                }


                if (change_notification_data.Tables[0].Rows[0]["HostName"] == null || change_notification_data.Tables[0].Rows[0]["HostName"] == System.DBNull.Value)
                {
                    HostName = null;
                }
                else
                {
                    HostName = Convert.ToString(change_notification_data.Tables[0].Rows[0]["HostName"]);
                }

                if (change_notification_data.Tables[0].Rows[0]["UserName"] == null || change_notification_data.Tables[0].Rows[0]["UserName"] == System.DBNull.Value)
                {
                    UserName = null;
                }
                else
                {
                    UserName = Convert.ToString(change_notification_data.Tables[0].Rows[0]["UserName"]);
                }
            }
        }

        /// <summary>
        /// Sql injection prevention
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        private string EscapeSqlString(string input)
        {
            return input.Replace("'", "''");
        }
        #endregion

        #region Event Handlers
        /// <summary>
        /// Underlying command caused changes to the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void _sql_dependency_OnChange(object sender, SqlNotificationEventArgs e)
        {
            SqlDependency dependency =(SqlDependency)sender;

            _sql_dependency.OnChange -= _sql_dependency_OnChange;
            _sql_dependency = null;

            try
            {
                _sql_command.Notification = null;
                _sql_dependency = new SqlDependency(_sql_command); //, "service=AenderungsNotifizierungenService", 0);
                _sql_dependency.OnChange += new OnChangeEventHandler(_sql_dependency_OnChange);

                SqlDataAdapter adapter = new SqlDataAdapter(_sql_command);
                // Fill the DataSet.
                DataSet ds = new DataSet();
                adapter.Fill(ds);

                ExctractDataFromDataSet(ds);
            }
            catch (SqlException sqlEx)
            {

            }

            if (OnChange != null)
                OnChange(this, new EventArgs());
        }
        #endregion

        #region Properties
        public string ConnectionString
        {
            get;
            private set;
        }

        public string TableName
        {
            get;
            private set;
        }

        public string NotificationKey
        {
            get;
            private set;
        }

        public string NotificationCommand
        {
            get;
            private set;
        }

        public string CurrentStringValue
        {
            get;
            private set;
        }

        public string OldStringValue
        {
            get;
            private set;
        }

        public long? CurrentLongValue
        {
            get;
            private set;
        }

        public long? OldLongValue
        {
            get;
            private set;
        }



        public string HostName
        {
            get;
            private set;
        }

        public string UserName
        {
            get;
            private set;
        }
        #endregion

        #region Dependency Properties
        #endregion

        #region Events

        public event EventHandler OnChange;
        #endregion

        #region Routed Events
        #endregion

        #region Attributes
        private SqlConnection _sql_connection = null;
        private SqlCommand _sql_command = null;
        private SqlDependency _sql_dependency = null;
        #endregion

        #region Tests
        #endregion
    }
}
