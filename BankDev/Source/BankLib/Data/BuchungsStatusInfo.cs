﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankLib.Data
{
    public class BuchungsStatusInfo
    {
        public BuchungsStatusInfo()
        {
            LastKtoStand = new Dictionary<Konto, float>();
            foreach (Konto kto in Konten.AlleKonten)
                LastKtoStand.Add(kto, kto.AnfangsKtoStand);
        }

        public void SaveToFile()
        {
            if (File.Exists(PathToStatusFile))
                File.Delete(PathToStatusFile);

            using (BinaryWriter writer = new BinaryWriter(File.Open(PathToStatusFile, FileMode.CreateNew)))
            {
                WriteToStream(writer);
            }
        }

        public bool LoadFromFile()
        {
            if (!File.Exists(PathToStatusFile))
            {
                return false;
            }


            using (BinaryReader reader = new BinaryReader(File.Open(PathToStatusFile, FileMode.Open)))
            {
                LoadFromStream(reader);
            }

            return true;
        }

        internal void WriteToStream(BinaryWriter writer)
        {
            LastBuchungIndex = Buchungen.AlleBuchungen.Count;
            LastBuchung = Buchungen.AlleBuchungen[Buchungen.AlleBuchungen.Count - 1];
            for (int i = 0; i < Konten.AlleKonten.Count; i++)
                LastKtoStand[Konten.AlleKonten[i]] = Konten.AlleKonten[i].CurrentKtoStand;

            LastBuchung.WriteToStream(writer);

            writer.Write(LastWebUpdate.ToBinary());
            writer.Write(LastBuchungIndex);
            writer.Write(CountBuchungen);

            writer.Write(Konten.AlleKonten.Count);
            for (int i = 0; i < Konten.AlleKonten.Count; i++)
            {
                BinaryHelper.WriteString(writer,Konten.AlleKonten[i].KtoNummer);
                writer.Write(Konten.AlleKonten[i].CurrentKtoStand);    
            }
        }

        internal void LoadFromStream(BinaryReader reader)
        {
            LastBuchung = new Buchung();
            LastBuchung.LoadFromStream(reader);

            LastWebUpdate = DateTime.FromBinary(reader.ReadInt64());
            LastBuchungIndex = reader.ReadInt32();
            CountBuchungen = reader.ReadInt32();

            int count_kto = reader.ReadInt32();
            LastKtoStand = new Dictionary<Konto, float>(count_kto);
            for (int i = 0; i < count_kto; i++)
            {
                string kto_nummer = BinaryHelper.ReadString(reader);
                float last_kto_stand = reader.ReadSingle();
                Konto kto = Konten.GetKontoByKtoNr(kto_nummer);
                LastKtoStand.Add(kto, last_kto_stand);
            }
        }

        public DateTime LastWebUpdate = new DateTime();
        public Buchung LastBuchung = new Buchung();
        public int LastBuchungIndex = 0;
        public Dictionary<Konto, float> LastKtoStand;
        public int CountBuchungen;

        public static string PathToStatusFile = AppHelper.PathToBankData+"LastBuchungsStatus.dat";
    }
}
