﻿namespace FxSyncManager
{
    partial class EffectSyncManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EffectSyncManager));
            this._fx_files = new System.Windows.Forms.ListBox();
            this._update_button = new System.Windows.Forms.Button();
            this._select_all = new System.Windows.Forms.CheckBox();
            this._log_box = new System.Windows.Forms.ListBox();
            this._tags_to_clipboard = new System.Windows.Forms.Button();
            this._dx10_only = new System.Windows.Forms.CheckBox();
            this._tray_icon = new System.Windows.Forms.NotifyIcon(this.components);
            this._show_warnings = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // _fx_files
            // 
            this._fx_files.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._fx_files.FormattingEnabled = true;
            this._fx_files.IntegralHeight = false;
            this._fx_files.Location = new System.Drawing.Point(12, 12);
            this._fx_files.Name = "_fx_files";
            this._fx_files.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this._fx_files.Size = new System.Drawing.Size(679, 428);
            this._fx_files.TabIndex = 0;
            // 
            // _update_button
            // 
            this._update_button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._update_button.Location = new System.Drawing.Point(697, 12);
            this._update_button.Name = "_update_button";
            this._update_button.Size = new System.Drawing.Size(98, 38);
            this._update_button.TabIndex = 1;
            this._update_button.Text = "Update C# from FX code";
            this._update_button.UseVisualStyleBackColor = true;
            this._update_button.Click += new System.EventHandler(this._update_button_Click);
            // 
            // _select_all
            // 
            this._select_all.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._select_all.AutoSize = true;
            this._select_all.Location = new System.Drawing.Point(697, 56);
            this._select_all.Name = "_select_all";
            this._select_all.Size = new System.Drawing.Size(70, 17);
            this._select_all.TabIndex = 2;
            this._select_all.Text = "Select All";
            this._select_all.UseVisualStyleBackColor = true;
            this._select_all.CheckedChanged += new System.EventHandler(this._select_all_CheckedChanged);
            // 
            // _log_box
            // 
            this._log_box.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._log_box.BackColor = System.Drawing.Color.Black;
            this._log_box.ForeColor = System.Drawing.Color.White;
            this._log_box.FormattingEnabled = true;
            this._log_box.Location = new System.Drawing.Point(12, 446);
            this._log_box.Name = "_log_box";
            this._log_box.Size = new System.Drawing.Size(783, 108);
            this._log_box.TabIndex = 3;
            this._log_box.DoubleClick += new System.EventHandler(this._log_box_DoubleClick);
            // 
            // _tags_to_clipboard
            // 
            this._tags_to_clipboard.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._tags_to_clipboard.Location = new System.Drawing.Point(720, 369);
            this._tags_to_clipboard.Name = "_tags_to_clipboard";
            this._tags_to_clipboard.Size = new System.Drawing.Size(75, 66);
            this._tags_to_clipboard.TabIndex = 4;
            this._tags_to_clipboard.Text = "Copy C# Start/End Tags to Clipboard";
            this._tags_to_clipboard.UseVisualStyleBackColor = true;
            this._tags_to_clipboard.Click += new System.EventHandler(this._tags_to_clipboard_Click);
            // 
            // _dx10_only
            // 
            this._dx10_only.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._dx10_only.AutoSize = true;
            this._dx10_only.Location = new System.Drawing.Point(697, 79);
            this._dx10_only.Name = "_dx10_only";
            this._dx10_only.Size = new System.Drawing.Size(82, 17);
            this._dx10_only.TabIndex = 5;
            this._dx10_only.Text = "Filter *10*.fx";
            this._dx10_only.UseVisualStyleBackColor = true;
            this._dx10_only.CheckedChanged += new System.EventHandler(this._dx10_only_CheckedChanged);
            // 
            // _tray_icon
            // 
            this._tray_icon.Icon = ((System.Drawing.Icon)(resources.GetObject("_tray_icon.Icon")));
            this._tray_icon.Text = "LogicX EffectSyncManager";
            this._tray_icon.Visible = true;
            this._tray_icon.DoubleClick += new System.EventHandler(this._tray_icon_DoubleClick);
            // 
            // _show_warnings
            // 
            this._show_warnings.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._show_warnings.AutoSize = true;
            this._show_warnings.Location = new System.Drawing.Point(697, 102);
            this._show_warnings.Name = "_show_warnings";
            this._show_warnings.Size = new System.Drawing.Size(98, 17);
            this._show_warnings.TabIndex = 6;
            this._show_warnings.Text = "Show warnings";
            this._show_warnings.UseVisualStyleBackColor = true;
            // 
            // EffectSyncManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(807, 562);
            this.Controls.Add(this._show_warnings);
            this.Controls.Add(this._dx10_only);
            this.Controls.Add(this._tags_to_clipboard);
            this.Controls.Add(this._log_box);
            this.Controls.Add(this._select_all);
            this.Controls.Add(this._update_button);
            this.Controls.Add(this._fx_files);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "EffectSyncManager";
            this.Text = "FxSyncManager";
            this.Load += new System.EventHandler(this.FxSyncManager_Load);
            this.Resize += new System.EventHandler(this.EffectSyncManager_Resize);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox _fx_files;
        private System.Windows.Forms.Button _update_button;
        private System.Windows.Forms.CheckBox _select_all;
        private System.Windows.Forms.ListBox _log_box;
        private System.Windows.Forms.Button _tags_to_clipboard;
        private System.Windows.Forms.CheckBox _dx10_only;
        private System.Windows.Forms.NotifyIcon _tray_icon;
        private System.Windows.Forms.CheckBox _show_warnings;
    }
}

