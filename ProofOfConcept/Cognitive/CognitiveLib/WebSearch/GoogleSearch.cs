﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;
using CognitiveLib.ApiService;
using FSBlog.GoogleSearch.GoogleClient;
using Google.Apis.Services;
using HtmlAgilityPack;
using Logicx.Utilities;

namespace CognitiveLib.WebSearch
{
    public class GoogleSearch
    {
        #region Search Suggestion
        /// <summary>
        /// The Google Suggest search URL.
        /// </summary>
        /// <remarks>
        /// Add gl=dk for Google Denmark. Add lr=lang_da for danish results. Add hl=da to indicate the language of the UI making the request.
        /// </remarks>
        //private string _suggestSearchUrl = "http://www.google.com/complete/search?output=toolbar&q={0}&hl=en";
        private static string _suggestSearchUrl = "http://www.google.at/search?q={0}&output=toolbar";



        public static List<string> GetSearchSuggestions(string query)
        {
            Task<List<string>> task_google_suggestions = GoogleSearch.GetSearchSuggestionsAsync(query);
            task_google_suggestions.Wait();
            List<string> suggestions = task_google_suggestions.Result;
            return suggestions;

        }


        //private async Task<string> WrapSomeMethod(string query)
        //{
        //    //adding .ConfigureAwait(false) may NOT be what you want but google it.
        //    return await Task.Run(() => GoogleSearch.GetSearchSuggestionsAsync(query)).ConfigureAwait(false);
        //}

        /// <summary>
        /// Gets the search suggestions from Google.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <returns>A list of <see cref="GoogleSuggestion"/>s.</returns>
        public static async Task<List<string>> GetSearchSuggestionsAsync(string query)
        {
            if (String.IsNullOrWhiteSpace(query))
            {
                throw new ArgumentException("Argument cannot be null or empty!", "query");
            }

            string result = String.Empty;

            using (HttpClient client = new HttpClient())
            {
                result = await client.GetStringAsync(String.Format(_suggestSearchUrl, query)).ConfigureAwait(false);
            }

            int meintest_du_i = result.IndexOf("Meintest du:");
            if (meintest_du_i > 0)
            {
                string res_sub = result.Substring(meintest_du_i + 13);
                if (res_sub.Substring(0, 300).IndexOf("Auf gut Glück") > 0)
                    return null;



                int index = res_sub.IndexOf("/search?q=");
                if (index > 0)
                {
                    string meintest_du = res_sub.Substring(index + 10).Split("&".ToCharArray(), 2)[0].Replace("+", " ");
                    return new List<string>() { meintest_du };
                }

                //// From String
                //HtmlDocument doc = new HtmlDocument();
                //doc.LoadHtml(result);

                ////// From Web
                ////var url = "http://html-agility-pack.net/";
                ////var web = new HtmlWeb();
                ////var doc = web.Load(url);

                //StringBuilder sb = new StringBuilder();
                //foreach (HtmlTextNode node in doc.DocumentNode.SelectNodes("//text()"))
                //{
                //    sb.AppendLine(node.Text);
                //}
                //string final = sb.ToString();


                
            }
            return null;


            //XDocument doc = XDocument.Parse(result);
            //List<string> suggestions = new List<string>(10);
            //foreach (XElement completesugg in doc.Document.Descendants("CompleteSuggestion"))
            //{
            //    string s = completesugg.Element("suggestion")?.Attribute("data")?.Value;
            //    if (string.IsNullOrEmpty(s))
            //        suggestions.Add(s);
            //}
            //return suggestions;

            //var suggestions = from suggestion in doc.Document?.Descendants("CompleteSuggestion")
            //    select new GoogleSuggestion
            //    {
            //        Phrase = suggestion.Element("suggestion")?.Attribute("data")?.Value
            //    };

            //return suggestions.ToList();
        }
        #endregion

        public static IEnumerable<SearchResultHit> Search(string query)
        {
            // init client
            var client = new SearchClient(query);
            return client.Query();
        }

        /// <summary>
        /// this method simulates a user query from the browser
        /// </summary>
        /// <param name="query_string"></param>
        /// <param name="count_analyse_hits"></param>
        /// <param name="with_urlinfo"></param>
        /// <returns></returns>
        public static string GetSearchResultAsText(string query_string, int count_analyse_hits = 3, bool with_urlinfo=true)
        {
            try
            {
                int curr_analysed = 0;
                IEnumerable<SearchResultHit> srh = GoogleSearch.Search(query_string);
                string search_text = "";
                foreach (SearchResultHit hit in srh)
                {
                    search_text += hit.Text.Replace("<b>", "").Replace("</b>", "");
                    if (with_urlinfo)
                        search_text += hit.CleanUri;
                    search_text += "\n";

                    //stop condition
                    curr_analysed++;
                    if (curr_analysed >= count_analyse_hits)
                        break;
                }
                return search_text;
            }
            catch (Exception ex)
            {
                return "";
            }
        }


        /// <summary>
        /// this uses the google custom search api
        /// </summary>
        /// <param name="query_string"></param>
        /// <param name="count_analyse_hits"></param>
        /// <param name="with_urlinfo"></param>
        /// <returns></returns>
        public static dynamic GetCustomSearchResultAsText(string query_string, int count_analyse_hits = 3, bool with_urlinfo = true)
        {
            return GoogleCustomSearch.GetResponseString(query_string);
        }





    }
}