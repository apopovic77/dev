﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LoggingLib.Data.Finalizers
{
    /// <summary>
    /// Base class for data finalizer classes
    /// </summary>
    public class DataFinalizerBase
    {
        #region Operations
        /// <summary>
        /// Will be called before the the item will be inserted in the data context
        /// </summary>
        /// <param name="logEntry">The log entry for this finalizer</param>
        /// <param name="dataContext">Data context which is about to submit its changes back to the database</param>
        /// <returns>Returns true if pre-update operations succeed. If this method returns false, the SubmitChanges() will not be executed.</returns>
        public virtual bool BeforeInsertItemInDataContext(LogEntry logEntry, LoggingDataContext dataContext)
        {
            return true;
        }

        /// <summary>
        /// Will be called after the submitchanges() method invokation of the given data context
        /// </summary>
        /// <param name="dataContext">Data context which submitted its changes to the database</param>
        /// <returns>Returns true if post-update operations sueceed. If not false will be returned.</returns>
        /// <remarks>You may use the return value of false, for indicating a propblem on the data context and 
        /// rollback or reset a current transaction scope.</remarks>
        public virtual bool AfterSubmitChanges(LoggingDataContext dataContext)
        {
            return true;
        }

        /// <summary>
        /// Sets the number of changes made
        /// </summary>
        /// <param name="nr"></param>
        protected void SetChangesMade(long nr)
        {
            _changes_made = nr;
        }
        #endregion

        #region Properties
        /// <summary>
        /// gets/Sets the linked log data type
        /// </summary>
        public Type LinkedType
        {
            get { return _linked_type; }
            set { _linked_type = value; }
        }

        /// <summary>
        /// Returns the number of changes made during the last execution
        /// </summary>
        public long ChangesMade
        {
            get { return _changes_made; }
        }
        #endregion

        #region Attribs
        private Type _linked_type = null;
        private long _changes_made = 0;
        #endregion
    }
}