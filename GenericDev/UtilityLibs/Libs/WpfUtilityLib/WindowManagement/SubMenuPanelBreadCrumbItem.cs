﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Media;
using Logicx.WpfUtility.Converters;
using Logicx.WpfUtility.CustomControls.Navigation;
using Logicx.WpfUtility.CustomControls.TouchButton;

namespace Logicx.WpfUtility.WindowManagement
{
    #region SubmenuPanelBreadCrumbButtonVisibilityConverter
    public class SubmenuPanelBreadCrumbButtonVisibilityConverter : IMultiValueConverter
    {
        public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value != null && value.Length == 2)
            {
                if (value[0] is ISubMenuPanelBreadCrumbItem)
                {
                    ISubMenuPanelBreadCrumbItem bread_crumb_item = value[0] as ISubMenuPanelBreadCrumbItem;
                    bool check_button = false;

                    if (parameter != null && parameter is string)
                        check_button = ((string)parameter) != "0";
                    else if (parameter != null && parameter is int)
                        check_button = ((int)parameter) != 0;

                    if (bread_crumb_item == bread_crumb_item.ParentPanel.CurrentBreadCrumbItem)
                    {
                        if (check_button)
                            return Visibility.Collapsed;
                        else
                            return Visibility.Visible;
                    }
                    else
                    {
                        if (check_button)
                            return Visibility.Visible;
                        else
                            return Visibility.Collapsed;
                    }
                }
            }

            return Visibility.Collapsed;
        }

        public object[] ConvertBack(object value, Type[] targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }
    #endregion

    public interface ISubMenuPanelBreadCrumbItem
    {
        /// <summary>
        /// Gets/Sets the text of the breadcrumb item
        /// </summary>
        string Text { get; set; }

        ISubMenuPanelMenuItem MenuItem { get; set; }

        /// <summary>
        /// Gets the parent SubMenuPanel instance
        /// </summary>
        ISubMenuPanelBase ParentPanel { get; }

        bool IsLastItem { get; set; }
        object AdditionalObject { get; set; }

        event PropertyChangedEventHandler PropertyChanged;
        event RoutedEventHandler Click;
    }

    public class SubMenuPanelBreadCrumbItem : StackPanel, INotifyPropertyChanged, ISubMenuPanelBreadCrumbItem
    {
        #region Constants
        #endregion

        #region Construction and Initialization
        /// <summary>
        /// Constructor of the class
        /// </summary>
        /// <param name="text"></param>
        public SubMenuPanelBreadCrumbItem(ISubMenuPanelBase parent_panel, string text)
            : this(parent_panel, text, null)
        {
        }

        /// <summary>
        /// Constructor of the class
        /// </summary>
        /// <param name="text"></param>
        /// <param name="menu_item"></param>
        public SubMenuPanelBreadCrumbItem(ISubMenuPanelBase parent_panel, string text, ISubMenuPanelMenuItem menu_item)
        {
            if (parent_panel == null)
                throw new ArgumentNullException("parent_panel");

            _parent_panel = parent_panel;
            _text = text;
            _menu_item = menu_item;

            InitialzeControl();
        }

        protected virtual void InitialzeControl()
        {

            Orientation = Orientation.Horizontal;

            TextBlock tb = new TextBlock();
            tb.Margin = new Thickness(2, 0, 2, 0);
            tb.FontWeight = FontWeights.Bold;
            tb.Width = double.NaN;
            Binding binding_text = new Binding("Text");
            binding_text.Source = this;
            tb.SetBinding(TextBlock.TextProperty, binding_text);

            MultiBinding visibility_binding = new MultiBinding();

            Binding visibility_binding_param1 = new Binding();
            visibility_binding_param1.Source = this;

            Binding visibility_binding_param2 = new Binding("IsLastItem");
            visibility_binding_param2.Source = this;

            visibility_binding.Converter = new SubmenuPanelBreadCrumbButtonVisibilityConverter();
            visibility_binding.ConverterParameter = 0;

            visibility_binding.Bindings.Add(visibility_binding_param1);
            visibility_binding.Bindings.Add(visibility_binding_param2);

            tb.SetBinding(TextBlock.VisibilityProperty, visibility_binding);
            Children.Add(tb);

            TouchButton touchb = new TouchButton();
            touchb.Click += new RoutedEventHandler(touchb_Click);
            touchb.Margin = new Thickness(2, 0, 2, 0);
            touchb.CommandParameter = this;

            //Binding commandparam_binding = new Binding("CommandParameter");
            //commandparam_binding.Source = this;
            //touchb.SetBinding(TouchButton.CommandParameterProperty, commandparam_binding);

            Binding content_binding = new Binding("Text");
            content_binding.Source = this;
            touchb.SetBinding(TouchButton.ContentProperty, content_binding);

            MultiBinding visibility_touch_binding = new MultiBinding();

            Binding visibility_touch_param1 = new Binding();
            visibility_touch_param1.Source = this;

            Binding visibility_touch_param2 = new Binding("IsLastItem");
            visibility_touch_param2.Source = this;

            visibility_touch_binding.Bindings.Add(visibility_touch_param1);
            visibility_touch_binding.Bindings.Add(visibility_touch_param2);

            visibility_touch_binding.Converter = new SubmenuPanelBreadCrumbButtonVisibilityConverter();
            visibility_touch_binding.ConverterParameter = 1;
            touchb.SetBinding(TextBlock.VisibilityProperty, visibility_touch_binding);

            Children.Add(touchb);

            TextBlock sep_tb = new TextBlock();
            sep_tb.FontFamily = new FontFamily("Webdings");
            sep_tb.Text = "8";
            sep_tb.Margin = new Thickness(2, 2, 2, 0);
            sep_tb.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#AE323232"));
            sep_tb.Width = double.NaN;

            Binding sep_binding = new Binding("IsLastItem");
            sep_binding.Source = this;
            sep_binding.Converter = new InverseBooleanToVisibilityCollapsedConverter();
            sep_tb.SetBinding(TextBlock.VisibilityProperty, sep_binding);

            Children.Add(sep_tb);
        }
        #endregion

        #region Operations
        protected void SendPropertyChanged(string property_name)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property_name));
        }

        protected void RaiseClickEvent(object sender, RoutedEventArgs e)
        {
            if (Click != null)
                Click(sender, e);
        }
        #endregion

        #region Event Handlers
        void touchb_Click(object sender, RoutedEventArgs e)
        {
            RaiseClickEvent(sender, e);
        }
        #endregion

        

        #region Properties
        /// <summary>
        /// Gets/Sets the text of the breadcrumb item
        /// </summary>
        public string Text
        {
            get { return _text; }
            set
            {
                if(_text != value)
                {
                    _text = value;
                    SendPropertyChanged("Text");
                }
            }
        }
        /// <summary>
        /// Gets/Sets the associated menu item of the breadcrumb item
        /// </summary>
        public ISubMenuPanelMenuItem MenuItem
        {
            get { return _menu_item; }
            set
            {
                if (_menu_item != value)
                {
                    _menu_item = value;
                    SendPropertyChanged("MenuItem");
                }
            }
        }
        /// <summary>
        /// Gets the parent SubMenuPanel instance
        /// </summary>
        public ISubMenuPanelBase ParentPanel
        {
            get { return _parent_panel; }
        }

        public bool IsLastItem
        {
            get { return _is_last_item; }
            set
            {
                if(_is_last_item != value)
                {
                    _is_last_item = value;
                    SendPropertyChanged("IsLastItem");
                }
            }
        }

        public object AdditionalObject
        {
            get { return _additional_object; }
            set
            {
                if(_additional_object != value)
                {
                    _additional_object = value;
                    SendPropertyChanged("AdditionalObject");
                }
            }

        }
        #endregion

        #region Dependency Properties
        #endregion

        #region Events
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        #region Routed Events
        /// <summary>
        /// Wird geworfen, wenn auf den TouchButton gedrückt wird
        /// </summary>
        public event RoutedEventHandler Click;
        #endregion

        #region Attributes
        private bool _is_last_item = false;
        private ISubMenuPanelBase _parent_panel = null;
        private string _text = string.Empty;
        private ISubMenuPanelMenuItem _menu_item = null;
        private object _additional_object = null;
        #endregion

        #region Tests
        #endregion
    }
}
