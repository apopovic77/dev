﻿using System;

namespace LoggingLib
{
    /// <summary>
    /// Struct holding constant values of the application
    /// </summary>
    public struct LogManagerConstants
    {
        // system category names
        public const string SYSCATEGORY_SYSTEMINFO = "System Info";
        public const string SYSCATEGORY_APPSESSION = "Application Session";
        public const string SYSCATEGORY_APPMESSAGE = "Application Message";
        public const string SYSCATEGORY_USERACTION = "User Action";

        // standard event Ids
        public const long EVENTID_SYSTEMINFO = 99;

        public const long EVENTID_APPSTARTUP = 1;
        public const long EVENTID_APPEND = 2;
    }

    /// <summary>
    /// Enumeration describing the priority values
    /// </summary>
    public enum Priority : int
    {
        /// <summary>
        /// Low priority
        /// </summary>
        Low = 0, 
        /// <summary>
        /// Normal priority - default value for log entries
        /// </summary>
        Normal = 1,
        /// <summary>
        /// High priority - default value for severity Critical or Fatal
        /// </summary>
        High = 2
    }

    /// <summary>
    /// Enumeration describing the severity values
    /// </summary>
    public enum Severity : int
    {
        /// <summary>
        /// Debug message
        /// </summary>
        Debug = 0,
        /// <summary>
        /// Not important verbose message
        /// </summary>
        Verbose = 1,
        /// <summary>
        /// Information message
        /// </summary>
        Information = 2,
        /// <summary>
        /// Warning message
        /// </summary>
        Warning = 3,
        /// <summary>
        /// Error message
        /// </summary>
        Error = 4,
        /// <summary>
        /// Critical error message
        /// </summary>
        Critical = 5,
        /// <summary>
        /// Fatal error message - typically causing a not recoverable application state
        /// </summary>
        Fatal = 6
    }
    /// <summary>
    /// Enumeration controlling the logging database connection state
    /// </summary>
    public enum LoggingDataConnectionState
    {
        /// <summary>
        /// The last data context connection attempt succeed.
        /// Data context should be online
        /// </summary>
        Online,
        /// <summary>
        /// The last connection attempt failed. Data context is offline.
        /// </summary>
        Offline,
        /// <summary>
        /// A connection attempt is currently in progress
        /// </summary>
        Connecting
    }

    /// <summary>
    /// software filter type enumeration
    /// </summary>
    public enum SoftwareFilterType
    {
        Exact,
        RegEx
    }
}