﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace Utility.Configuration
{
    public class UserConfigurationService : IDisposable
    {
        #region Constants
        #endregion

        #region Construction and Initialization
        /// <summary>
        /// Constructor of the class
        /// </summary>
        /// <param name="application_name">Name of the application (no special characters! the name of the configuration folder)</param>
        public UserConfigurationService(string application_name) : this(application_name, "user.settings", false)
        {
            
        }
        /// <summary>
        /// Constructor of the class
        /// </summary>
        /// <param name="application_name">Name of the application (no special characters! the name of the configuration folder)</param>
        /// <param name="file_name">settings file name</param>
        /// <param name="roaming_profile">true if the file will be stored in the roaming folder</param>
        public UserConfigurationService(string application_name, string file_name, bool roaming_profile)
        {
            if (string.IsNullOrEmpty(application_name))
                throw new ArgumentException("Applicationname must be set.", "application_name");
            if (string.IsNullOrEmpty(file_name))
                throw new ArgumentException("Filename must be set.", "file_name");

            string local_temp_path = "";

            string app_folder = Environment.GetFolderPath(roaming_profile ? Environment.SpecialFolder.ApplicationData : Environment.SpecialFolder.LocalApplicationData);
            local_temp_path = Path.Combine(app_folder, application_name);
            if (!Directory.Exists(local_temp_path))
                Directory.CreateDirectory(local_temp_path);

            _cfg_path = local_temp_path;
            _cfg_filename = file_name;
        }
        #endregion

        #region Operations
        /// <summary>
        /// Loads the configuration store
        /// </summary>
        /// <param name="reset">true if store should be deleted</param>
        /// <returns>true if succeeded</returns>
        public bool Load(bool reset)
        {
            Stream stream;
            _last_error = string.Empty;

            bool bRet = false;
            if (!reset)
            {
                stream = null;
                string str = Path.Combine(_cfg_path, _cfg_filename);
                try
                {
                    if (File.Exists(str))
                    {
                        stream = File.Open(str, FileMode.Open, FileAccess.Read, FileShare.Read);
                        BinaryFormatter binaryFormatter = new BinaryFormatter();
                        _stores = (Hashtable)binaryFormatter.Deserialize(stream);
                        bRet = true;
                    }
                }
                catch (Exception e)
                {
                    Debug.WriteLine("UserConfigurationService.Load() - Failed with error: " + e.Message);
                    _last_error = e.Message;
                }
                finally
                {
                    if (stream != null)
                    {
                        stream.Close();
                        stream = null;
                    }
                }
            }
            if (_stores == null)
            {
                _stores = new Hashtable();
            }
            return bRet;
        }

        /// <summary>
        /// Saves the configuration
        /// </summary>
        /// <returns>true if succeeded</returns>
        public bool Save()
        {
            Stream stream = null;
            string str = Path.Combine(_cfg_path, _cfg_filename);
            _last_error = string.Empty;

            bool bRet = false;
            try
            {
                if (!Directory.Exists(_cfg_path))
                {
                    Directory.CreateDirectory(_cfg_path);
                }
                stream = File.Open(str, FileMode.Create, FileAccess.ReadWrite, FileShare.None);
                new BinaryFormatter().Serialize(stream, _stores);
                bRet = true;
            }
            catch (Exception e)
            {
                Debug.WriteLine("UserConfigurationService.Save() - Failed with error: " + e.Message);
                _last_error = e.Message;
            }
            finally
            {
                if (stream != null)
                {
                    stream.Close();
                    stream = null;
                }
            }
            return bRet;
        }

        /// <summary>
        /// Gets a config store for a special type
        /// </summary>
        /// <param name="storeOwnerType">store type</param>
        /// <returns>A reference to the config store</returns>
        public ConfigurationStore GetConfigStore(Type storeOwnerType)
        {
            if (storeOwnerType == null)
            {
                throw new ArgumentNullException();
            }
            string str = storeOwnerType.FullName;
            return GetConfigStore(str);
        }

        /// <summary>
        /// Gets a config store of a specified name
        /// </summary>
        /// <param name="store_name">name of the store</param>
        /// <returns>A reference to the config store</returns>
        public ConfigurationStore GetConfigStore(string store_name)
        {
            ConfigurationStore configStore = (ConfigurationStore)_stores[store_name];
            if (configStore == null)
            {
                configStore = new ConfigurationStore();
                _stores[store_name] = configStore;
            }
            return configStore;
        }

        /// <summary>
        /// Resets a store to its defaults
        /// </summary>
        /// <param name="storeOwnerType">store type</param>
        public void ResetConfigStore(Type storeOwnerType)
        {
            if (storeOwnerType == null)
            {
                throw new ArgumentNullException();
            }
            string str = storeOwnerType.FullName;
            ResetConfigStore(str);
        }

        /// <summary>
        /// Resets a store to its defaults
        /// </summary>
        /// <param name="store_name">name of the store</param>
        public void ResetConfigStore(string store_name)
        {
            if (_stores.ContainsKey(store_name))
                _stores.Remove(store_name);
        }

        /// <summary>
        /// Dispose config store
        /// </summary>
        public void Dispose()
        {
            _stores = null;
        }
        #endregion

        #region Event Handlers
        #endregion

        #region Properties
        /// <summary>
        /// Gets the last error occured
        /// </summary>
        public string LastError
        {
            get { return _last_error; }
        }
        #endregion

        #region Dependency Properties
        #endregion

        #region Events
        #endregion

        #region Routed Events
        #endregion

        #region Attributes
        /// <summary>
        /// Internal member storing the config stores
        /// </summary>
        private Hashtable _stores;
        /// <summary>
        /// Internal member storing the path of the config file
        /// </summary>
        private string _cfg_path;
        /// <summary>
        /// Internal member storing the name of the config file
        /// </summary>
        private string _cfg_filename;
        /// <summary>
        /// Internal member storing the application name
        /// </summary>
        private string _application_name;
        /// <summary>
        /// The last error occured
        /// </summary>
        private string _last_error = string.Empty;
        #endregion

        #region Tests
        #endregion

    }
}
