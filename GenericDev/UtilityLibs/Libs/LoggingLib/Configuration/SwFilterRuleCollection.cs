﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace LoggingLib.Configuration
{
    /// <summary>
    /// The collection class that will store the list of each element/item that
    ///        is returned back from the configuration manager.
    /// </summary>
    [ConfigurationCollection(typeof(SwFilterRuleElement))]
    public class SwFilterRuleCollection : ConfigurationElementCollection
    {
        /// <summary>
        /// Create a new element of the collection's type
        /// </summary>
        /// <returns>An instance of the new object</returns>
        protected override ConfigurationElement CreateNewElement()
        {
            return new SwFilterRuleElement();
        }

        /// <summary>
        /// Gets an element's key
        /// </summary>
        /// <param name="element">Element to get the key from</param>
        /// <returns>An object instance which represents the key of the element</returns>
        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((SwFilterRuleElement)(element)).CompareFieldName + ((SwFilterRuleElement)(element)).CompareValue;
        }

        /// <summary>
        /// Indexer overload returning the element at a specified index
        /// </summary>
        /// <param name="idx">Index of the item to retrieve</param>
        /// <returns>The instance of the item at the specified index</returns>
        public SwFilterRuleElement this[int idx]
        {
            get
            {
                return (SwFilterRuleElement)BaseGet(idx);
            }
        }
    }
}
