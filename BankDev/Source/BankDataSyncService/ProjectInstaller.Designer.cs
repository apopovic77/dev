﻿namespace BankDataSyncService
{
    partial class ProjectInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BankDataSyncServiceProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            this.BankDataSyncServiceInstaller = new System.ServiceProcess.ServiceInstaller();
            // 
            // BankDataSyncServiceProcessInstaller
            // 
            this.BankDataSyncServiceProcessInstaller.Account = System.ServiceProcess.ServiceAccount.NetworkService;
            this.BankDataSyncServiceProcessInstaller.Password = null;
            this.BankDataSyncServiceProcessInstaller.Username = null;
            // 
            // BankDataSyncServiceInstaller
            // 
            this.BankDataSyncServiceInstaller.DelayedAutoStart = true;
            this.BankDataSyncServiceInstaller.Description = "Ladet über PhantomJS Daten von der Hypo Hompage alle 2 Stunden";
            this.BankDataSyncServiceInstaller.DisplayName = "BankDataSyncService";
            this.BankDataSyncServiceInstaller.ServiceName = "BankDataSyncService";
            this.BankDataSyncServiceInstaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.BankDataSyncServiceProcessInstaller,
            this.BankDataSyncServiceInstaller});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller BankDataSyncServiceProcessInstaller;
        private System.ServiceProcess.ServiceInstaller BankDataSyncServiceInstaller;
    }
}