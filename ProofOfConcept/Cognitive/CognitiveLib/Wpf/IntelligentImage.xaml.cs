﻿using System;
using System.IO;
using System.Windows.Controls;
using System.Windows.Shapes;
using CognitiveLib.ApiService;
using CognitiveLib.ApiService.EmguCV;
using CognitiveLib.Scanner;
using CognitiveLib.Typenschild;
using Emgu.CV;
using Google.Cloud.Vision.V1;
using MessagingToolkit.QRCode.Codec;
using MessagingToolkit.QRCode.Codec.Data;
using Brush = System.Windows.Media.Brush;
using Brushes = System.Windows.Media.Brushes;
using Rectangle = System.Windows.Shapes.Rectangle;

namespace CognitiveLib.CognitiveService
{
    /// <summary>
    /// Interaktionslogik für IntelligentImage.xaml
    /// </summary>
    public partial class IntelligentImage : UserControl
    {
        public struct RectangleBoundingBox
        {
            public int X1;
            public int Y1;
            public int Width;
            public int Height;
        }

        public enum CvServiceType
        {
            AzureVision,
            GoogleVision,
            EmguCvOcr,
            QrCode,
            /// <summary>
            /// a combination of emgu = prep as doc and then use azure
            /// </summary>
            EmguAzure,
            DeviceTypeDetector
        }

        public IntelligentImage(string path_to_img, int rotation=0)
        {
            InitializeComponent();

            _path_to_img = path_to_img;
            _curr_rotation = rotation;

            UpdateImage();
        }

        private void UpdateImage()
        {
            //remove everything but the image itself
            for (int i = canvas.Children.Count - 1; i >=1 ; i--)
                canvas.Children.RemoveAt(i);

            if (_curr_rotation != 0)
            {
                _curr_img_bytes = ImageUtils.LoadRotateGetJpgBytes(_path_to_img, _curr_rotation);
                _curr_img_byte_format = "jpeg";
            }
            else
                _curr_img_bytes = ImageUtils.LoadImageBytes(_path_to_img, out _curr_img_byte_format);

            System.Windows.Media.ImageSource img_src = ImageUtils.GetBitmapImgSource(_curr_img_bytes, _curr_img_byte_format);
            img.Source = img_src;
        }

        public void Rotate()
        {
            if (string.IsNullOrEmpty(_path_to_img))
                return;
            if (!File.Exists(_path_to_img))
                return;

            _curr_rotation += 90;
            if (_curr_rotation >= 360)
                _curr_rotation = 0;

            UpdateImage();
        }

        public void AnalyzeImage()
        {
            switch (CvService)
            {
                case CvServiceType.AzureVision:
                    AzureCvAnalyzeImage();
                    break;
                case CvServiceType.GoogleVision:
                    GoogleCvAnalyzeImage();
                    break;
                case CvServiceType.EmguCvOcr:
                    EmguCvOCRImage();
                    break;
                case CvServiceType.QrCode:
                    QrCodeReader();
                    break;
                case CvServiceType.EmguAzure:
                    EmguAzureAnalyzeImage();
                    break;
                case CvServiceType.DeviceTypeDetector:
                    DeviceTypeDetectorAnalyzeImage();
                    break;
            }
        }

        protected void AddTextToImage(string text, string bb_s, Brush font_color)
        {
            string[] bb_sarr = bb_s.Split(",".ToCharArray(), 4);

            int x1 = Convert.ToInt32(bb_sarr[0]);
            int y1 = Convert.ToInt32(bb_sarr[1]);
            int width = Convert.ToInt32(bb_sarr[2]);
            int height = Convert.ToInt32(bb_sarr[3]);

            TextBlock tb = new TextBlock();
            tb.Text = text;
            tb.FontSize = 20;
            tb.Foreground = font_color;

            canvas.Children.Add(tb);

            Canvas.SetLeft(tb, x1);
            Canvas.SetTop(tb, y1);
        }

        protected void AddBoundingBoxToImage(RectangleBoundingBox bb, Brush stroke_brush)
        {
            Rectangle rect = new Rectangle();
            rect.Stroke = stroke_brush;
            rect.StrokeThickness = 1;
            rect.Width = bb.Width;
            rect.Height = bb.Height;
            canvas.Children.Add(rect);

            Canvas.SetLeft(rect, bb.X1);
            Canvas.SetTop(rect, bb.Y1);
        }

        protected void AddBoundingBoxToImage(Vertex[] vertices, Brush stroke_brush)
        {
            for (int i = 0; i < 3; i++)
            {
                Line l = new Line();
                l.X1 = vertices[i].X;
                l.Y1 = vertices[i].Y;
                l.X2 = vertices[i+1].X;
                l.Y2 = vertices[i+1].Y;
                l.Fill = stroke_brush;
                l.StrokeThickness = 2;
                l.Stroke = stroke_brush;

                canvas.Children.Add(l);
            }
        }

        public void EmguAzureAnalyzeImage()
        {
            if (_curr_img_bytes == null)
                throw new Exception("no bytes given");

            IImage image = OpenCvHelper.LoadImageFromEncodedBytes(_curr_img_bytes);
            using (IImage warped_image = DocumentScanner.ScanToGrayDoc(image))
            {
                CvInvoke.Imwrite("last_converstion.jpg", warped_image);
                _curr_img_bytes = OpenCvHelper.EncodedBytesFromImage(warped_image);
            }

            AzureCvAnalyzeImage();
        }

        public void DeviceTypeDetectorAnalyzeImage()
        {
            TypeDetectionResult res = DeviceInfoDetector.Detect(_path_to_img);
            AnalysationSummary = res.OcrText+"\n\n";
            AnalysationSummary += "Brand: " + res.Brand.BestStringMatchByCount + "\n";
            AnalysationSummary += "BrandByModel: " + res.ModelNoDR.Brand + "\n";
            AnalysationSummary += "ModelNo: " + res.ModelNoDR.ModelNo + "\n";
            AnalysationSummary += "ModelCode: " + res.ModelCodeDR.ModelCode + "\n";
            AnalysationSummary += "TypeNo: " + res.TypeNoDR.TypeNo + "\n";
            if(res.SerialNoDR != null)
                AnalysationSummary += "SerialNo: " + res.SerialNoDR.SerialNo + "\n";
            AnalysationSummary += "VersionNo: " + ((res.VersionNoDR!= null)?res.VersionNoDR.VersionNo.String:"") + "\n";
            AnalysationSummary += "Kategorie: " + res.ModelNoDR.GeizhalsBestMatchKategorie + "\n";
            AnalysationSummary += "WithBarcode: " + res.IsImageWithBarcode + "\n";
            if(res.BarcodeScanResult != null)
                AnalysationSummary += "BarcodeScans: " + res.BarcodeScanResult.ToString();
        }

        #region Azure CV
        public void AzureCvAnalyzeImage()
        {
            if (_curr_img_bytes == null)
                throw new Exception("no bytes given");

            AnalysationSummary = "";
            dynamic obj = null;
            obj = AzureService.MakeAnalysisRequest(_curr_img_bytes, out _json_response);

            foreach (dynamic region in obj.regions)
            {
                AddBoundingBoxToImage(ConvertAzureBBString((string)region.boundingBox), Brushes.Blue);

                foreach (dynamic line in region.lines)
                {
                    if (!string.IsNullOrEmpty(AnalysationSummary))
                        AnalysationSummary += "\n";
                    AddBoundingBoxToImage(ConvertAzureBBString((string)region.boundingBox), Brushes.Yellow);
                    string text = "";
                    foreach (dynamic word in line.words)
                    {
                        AddBoundingBoxToImage(ConvertAzureBBString((string)region.boundingBox), Brushes.Red);
                        AddTextToImage((string)word.text, (string)word.boundingBox, Brushes.Red);
                        text += word.text + " ";
                    }
                    AnalysationSummary += text;
                    //AddTextToImage(text, (string)word.boundingBox, Brushes.Red);
                }
            }
        }

        private RectangleBoundingBox ConvertAzureBBString(string bb_s)
        {
            string[] bb_sarr = bb_s.Split(",".ToCharArray(), 4);

            int x = Convert.ToInt32(bb_sarr[0]);
            int y = Convert.ToInt32(bb_sarr[1]);
            int width = Convert.ToInt32(bb_sarr[2]);
            int height = Convert.ToInt32(bb_sarr[3]);

            return new RectangleBoundingBox() { X1 = x, Y1 = y, Width = width, Height = height };
        }

        #endregion

        #region Google CV
        public void GoogleCvAnalyzeImage()
        {
            if (string.IsNullOrEmpty(_path_to_img) || !File.Exists(_path_to_img))
                throw new Exception("no img given");

            AnalysationSummary = "";
            object auth_res = GoogleService.AuthExplicit("level-dragon-150409", ".\\google.cloudservice.auth.json");
            GoogleService.GoogleDetectLogoResponses logo_responses = GoogleService.GetFileAnotationStrongTyped(_path_to_img);

            foreach (GoogleService.GoogleDetectLogoResponse logo_response in logo_responses.GoogleDetectLogoResponse)
            {
                AddBoundingBoxToImage(logo_response.Vertices, Brushes.Red);
                AnalysationSummary += logo_response.Description + "\n";
            }
        }
        #endregion

        #region EmguCV
        public void EmguCvOCRImage()
        {
            //if (string.IsNullOrEmpty(_path_to_img) || !File.Exists(_path_to_img))
            //    throw new Exception("no img given");

            //AnalysationSummary = "";
            //AnalysationSummary = Ocr.GetText(_path_to_img);
            //if (string.IsNullOrEmpty(AnalysationSummary))
            //    AnalysationSummary = "NOTHING DETECTED!";
        }

        #endregion

        #region MessageingToolkit QR Code
        public void QrCodeReader()
        {
            if (string.IsNullOrEmpty(_path_to_img) || !File.Exists(_path_to_img))
                throw new Exception("no img given");

            AnalysationSummary = "";

            try
            {
                QRCodeDecoder dec = new QRCodeDecoder();
                AnalysationSummary = dec.Decode(new QRCodeBitmapImage(ImageUtils.GetBitmap(_path_to_img)));
            }
            catch (Exception ex)
            {
            }

            if (string.IsNullOrEmpty(AnalysationSummary))
                AnalysationSummary = "NOTHING DETECTED!";
        }

        #endregion

        protected string _json_response;
        protected string _path_to_img;
        protected int    _curr_rotation = 0;
        protected byte[] _curr_img_bytes;
        protected string _curr_img_byte_format = "jpeg";

        public string AnalysationSummary { get; set; }
        public CvServiceType CvService { get; set; }
    }
}
