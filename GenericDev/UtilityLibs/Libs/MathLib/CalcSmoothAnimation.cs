﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MathLib
{
    public class CalcSmoothAnimation
    {
        /// <summary>
        /// A class to calculate scale steps in reliance on a smooth-factor, the scale steps have to be multiplied to get the target_scale
        /// </summary>
        public CalcSmoothAnimation(float target_scale, float smooth)
        {
            _target_scale = target_scale;
            _smooth = smooth;
        }

        /// <summary>
        /// This Method calculates the scale step No. t
        /// </summary>
        public float GetValue(int t)
        {
            float x = 1, y = 1, prev_x;
            for (int i = 1; i <= t; i++)
            {
                prev_x = x;
                x = prev_x + (_target_scale - prev_x) * _smooth;
                y = x / prev_x;
            }
            return y;
        }

        /// <summary>
        /// This Method calculates the next scale step
        /// </summary>
        public float GetNextValue()
        {
            _prev_x = _x;
            _x = _prev_x + (_target_scale - _prev_x) * _smooth;
            _y = _x / _prev_x;
            return _y;
        }

        #region Attributes
        private float _target_scale, _smooth;
        private float _x = 1;
        private float _prev_x;
        private float _y;
        #endregion
    }
}
