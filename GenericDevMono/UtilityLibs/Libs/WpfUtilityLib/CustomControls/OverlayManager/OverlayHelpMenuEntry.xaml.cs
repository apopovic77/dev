﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Logicx.WpfUtility.CustomControls.HelpProvider;

namespace Logicx.WpfUtility.CustomControls.OverlayManager
{
    /// <summary>
    /// Interaktionslogik für OverlayHelpMenuEntry.xaml
    /// </summary>
    public partial class OverlayHelpMenuEntry : UserControl
    {
        public OverlayHelpMenuEntry()
        {
            InitializeComponent();
        }

        public OverlayHelpMenuEntry(string title, HelpNode ref_help_node)
        {
            InitializeComponent();
            menu_title.Text = title;
            Ref_Help_Node = ref_help_node;
        }        
        
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (Clicked != null)
                Clicked(this, new EventArgs());
        }

        public event EventHandler Clicked;
        public readonly HelpNode Ref_Help_Node;


    }
}
