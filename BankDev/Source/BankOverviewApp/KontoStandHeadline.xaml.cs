﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BankOverviewApp
{
    /// <summary>
    /// Interaktionslogik für KontoStandHeadline.xaml
    /// </summary>
    public partial class KontoStandHeadline : UserControl
    {
        public KontoStandHeadline()
        {
            InitializeComponent();
        }

 
        private void BetragTb_OnLoaded(object sender, RoutedEventArgs e)
        {
            BetragTb.Background = Brushes.White;
            float betrag = (float)BetragTb.Tag;
            if (betrag < 0)
                BetragTb.Foreground = Brushes.Red;
            else
                BetragTb.Foreground = Brushes.Black;
        }
    }
}
