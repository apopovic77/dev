﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MathLib;

namespace Logicx.WpfUtility.PieMenu
{
    /// <summary>
    /// Interaction logic for PieMenu.xaml
    /// </summary>
    public class PieMenuBase<PE, BUT, TOG> : UserControl
        where PE : PieMenuEntryBase<PE>
        where BUT : PE, IButtonPieMenuEntry
        where TOG : PE, ITogglePieMenuEntry
    {
        protected enum ShowMenuAnimationStyle
        {
            NoAnimation,
            ForwardAnimation,
            BackwardAnimation
        }

        public PieMenuBase()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            _base_grid = new Grid();

            this.Content = _base_grid;

        }

        public void ResetMenu()
        {
            if (_parent_menu_slices != null)
                _parent_menu_slices.Clear();

            if (_root_menu_slice == null)
                return;

            ShowMenuSlice(_root_menu_slice, ShowMenuAnimationStyle.NoAnimation);
        }

        public void SetMenu(List<PE> menu_entries, double outer_radius, double inner_radius, Color inner_ellipse_color, Color stroke_inner_ellipse, Color outer_ellipse_color, double font_size, SolidColorBrush font_brush, System.Windows.Media.Effects.BitmapEffect font_effect, bool close_on_outerellipse_click)
        {
            _close_on_outer_ellipse_click = close_on_outerellipse_click;

            SetMenu(menu_entries, outer_radius, inner_radius, font_size, font_brush, PieMenuSlice.DefaultFontStrokeBrush, PieMenuSlice.DefaultFontStrokeThickness, font_effect);
            _root_menu_slice.InnerEllipse.Stroke = new SolidColorBrush(stroke_inner_ellipse);
            _root_menu_slice.InnerEllipse.Fill = new SolidColorBrush(inner_ellipse_color);
            _root_menu_slice.OuterEllipse.Fill = new SolidColorBrush(outer_ellipse_color);

            _root_menu_slice.PieFontBitmapEffect = font_effect;
            _root_menu_slice.PieFontBrush = font_brush;
            _root_menu_slice.PieFontSize = font_size;
        }

        public void SetMenu(List<PE> menu_entries, double outer_radius, double inner_radius)
        {
            SetMenu(menu_entries, outer_radius, inner_radius, PieMenuSlice.DefaultFontSize, PieMenuSlice.DefaultFontBrush, PieMenuSlice.DefaultFontStrokeBrush, PieMenuSlice.DefaultFontStrokeThickness, null);
        }

        public void SetMenu(List<PE> menu_entries, double outer_radius, double inner_radius, double font_size, Brush font_brush, Brush font_stroke_brush, float font_stroke_thickness, System.Windows.Media.Effects.BitmapEffect font_effect)
        {
            _root_menu_slice = null;
            _menu_slices = new Dictionary<PE, PieMenuSliceBase<PE, BUT, TOG>>();
            _parent_menu_slices = new Stack<PieMenuSliceBase<PE, BUT, TOG>>();
            _menu_entries = menu_entries;

            _inner_radius = inner_radius;
            _outer_radius = outer_radius;

            PieFontBitmapEffect = font_effect;
            PieFontBrush = font_brush;
            PieFontSize = font_size;

            PieMenuSliceBase<PE, BUT, TOG> menu_slice = CreateMenuSlice(menu_entries, font_size, font_brush, font_stroke_brush, font_stroke_thickness, font_effect);
            _root_menu_slice = menu_slice;

            ShowMenuSlice(_root_menu_slice, ShowMenuAnimationStyle.NoAnimation);
        }


        protected PieMenuSliceBase<PE, BUT, TOG> CreateMenuSlice(List<PE> menu_entries, double font_size, Brush font_brush, System.Windows.Media.Effects.BitmapEffect font_effect)
        {
            return CreateMenuSlice(menu_entries, font_size, font_brush, null, 0, font_effect);
        }

        //protected PieMenuSlice CreateMenuSlice(List<E> menu_entries, double font_size, Brush font_brush, Brush font_stroke_brush, float font_stroke_thickness, System.Windows.Media.Effects.BitmapEffect font_effect)
        //{
        //    return CreateMenuSlice(menu_entries.Cast<E>().ToList(), font_size, font_brush, font_stroke_brush, font_stroke_thickness, font_effect);
        //}

        protected PieMenuSliceBase<PE, BUT, TOG> CreateMenuSlice(List<PE> menu_entries, double font_size, Brush font_brush, Brush font_stroke_brush, float font_stroke_thickness, System.Windows.Media.Effects.BitmapEffect font_effect)
        {
            PieMenuSliceBase<PE, BUT, TOG> menu_slice = new PieMenuSliceBase<PE, BUT, TOG>(_close_on_outer_ellipse_click);
            menu_slice.SetMenuDimensions(_outer_radius, _inner_radius);
            menu_slice.PieFontBitmapEffect = font_effect;
            menu_slice.PieFontBrush = font_brush;
            menu_slice.PieFontStrokeBrush = font_stroke_brush;
            menu_slice.PieFontStrokeThickness = font_stroke_thickness;
            menu_slice.PieFontSize = font_size;

            //add menuentries
            if (menu_entries != null)
                menu_slice.MenuEntries = menu_entries;
            else
                menu_slice.MenuEntries = null;

            //register for interaction handlers
            menu_slice.BackClick += new EventHandler(menu_slice_BackClick);
            menu_slice.ButtonClick += new EventHandler<PieMenuSliceBase<PE, BUT, TOG>.ButtonClickEventArgs<PE>>(menu_slice_ButtonClick);
            menu_slice.EnterSubMenuClick += new EventHandler<PieMenuSliceBase<PE, BUT, TOG>.EnterSubMenuClickEventArgs<PE>>(menu_slice_EnterSubMenuClick);

            return menu_slice;
        }


        private void ShowMenuSlice(PieMenuSliceBase<PE, BUT, TOG> menu_slice, ShowMenuAnimationStyle anim_style)
        {
            _parent_menu_slices.Push(menu_slice);
            if (anim_style == ShowMenuAnimationStyle.NoAnimation)
            {
                _base_grid.Children.Clear();
                _base_grid.Children.Add(menu_slice);
                menu_slice.ResetAnimations();
            }
            else if (anim_style == ShowMenuAnimationStyle.BackwardAnimation)
            {
                _base_grid.Children.Clear();
                _base_grid.Children.Add(menu_slice);
                menu_slice.StartBackwardAnimation();
            }
            else if (anim_style == ShowMenuAnimationStyle.ForwardAnimation)
            {
                _base_grid.Children.Clear();
                _base_grid.Children.Add(menu_slice);
                menu_slice.StartForwardAnimation();
            }
            else
            {
                throw new Exception("Animation Type unkown");
            }
        }

        public void HideMenu()
        {
            Visibility = Visibility.Hidden;
            ResetMenu();

            if (MenuHidden != null)
                MenuHidden(this, new EventArgs());
        }

        #region Find Methods
        public PE FindPieMenuEntryByNameAndKey(string name, string key)
        {
            if (_menu_entries == null)
                return null;

            PE entry = FindPieMenuEntryByKeyAndName(_menu_entries, key, name);

            if (entry == null)
                return null;

            return entry;
        }

        public PE FindPieMenuEntryByKey(string entry_key)
        {
            if (_menu_entries == null)
                return null;

            PE entry = FindPieMenuEntryByKey(_menu_entries, entry_key);

            if (entry == null)
                return null;

            return entry;
        }

        public PE FindPieMenuEntry(string entry_name)
        {
            if (_menu_entries == null)
                return null;

            PE entry = FindPieMenuEntry(_menu_entries, entry_name);

            if (entry == null || !(entry is PE))
                return null;

            return entry as PE;
        }

        //private E FindPieMenuEntry(List<E> entries, string entry_name)
        //{
        //    if (entries == null)
        //        return null;

        //    return FindPieMenuEntry(entries, entry_name);
        //}

        private PE FindPieMenuEntry(List<PE> entries, string entry_name)
        {
            for (int i = 0; i < entries.Count; i++)
            {
                if (entries[i].Name == entry_name)
                    return entries[i];
                if (entries[i].SubEntries != null)
                {
                    PE entry = FindPieMenuEntry(entries[i].SubEntries, entry_name);
                    if (entry != null)
                        return entry;
                }
            }
            return null;
        }

        //private E FindPieMenuEntryByKey(List<E> entries, string entry_key)
        //{
        //    if (entries == null)
        //        return null;

        //    E entry = FindPieMenuEntryByKey(entries, entry_key);

        //    if (entry != null && entry is E)
        //        return entry as E;

        //    return null;
        //}

        private PE FindPieMenuEntryByKey(List<PE> entries, string entry_key)
        {
            for (int i = 0; i < entries.Count; i++)
            {
                if (entries[i].Key == entry_key)
                    return entries[i];
                if (entries[i].SubEntries != null)
                {
                    PE entry = FindPieMenuEntryByKey(entries[i].SubEntries, entry_key);
                    if (entry != null)
                        return entry;
                }
            }
            return null;
        }

        //private E FindPieMenuEntryByKeyAndName(List<E> entries, string entry_key, string name)
        //{
        //    if (entries == null)
        //        return null;

        //    return FindPieMenuEntryByKeyAndName(entries, entry_key, name);
        //}

        private PE FindPieMenuEntryByKeyAndName(List<PE> entries, string entry_key, string name)
        {
            for (int i = 0; i < entries.Count; i++)
            {
                if (string.Equals(entries[i].Key, entry_key) && string.Equals(entries[i].Name, name))
                    return entries[i] as PE;
                if (entries[i].SubEntries != null)
                {
                    PE entry = FindPieMenuEntryByKeyAndName(entries[i].SubEntries, entry_key, name);
                    if (entry != null)
                        return entry;
                }
            }
            return null;
        }
        #endregion

        #region Eventhandlers
        void menu_slice_ButtonClick(object sender, PieMenuSliceBase<PE, BUT, TOG>.ButtonClickEventArgs<PE> e)
        {
            if (e.PieMenuEntry.HidesMenuOnClick)
                HideMenu();
        }

        private void menu_slice_EnterSubMenuClick(object sender, PieMenuSliceBase<PE, BUT, TOG>.EnterSubMenuClickEventArgs<PE> e)
        {
            if (e.PieMenuEntry != null && e.PieMenuEntry is PE)
            {
                if (!_menu_slices.ContainsKey(e.PieMenuEntry as PE))
                    _menu_slices.Add(e.PieMenuEntry as PE, CreateMenuSlice(e.PieMenuEntry.SubEntries, PieFontSize, PieFontBrush, PieFontStrokeBrush, PieFontStrokeThickness, PieFontBitmapEffect));

                ShowMenuSlice(_menu_slices[e.PieMenuEntry as PE], ShowMenuAnimationStyle.ForwardAnimation);
            }
        }

        private void menu_slice_BackClick(object sender, EventArgs e)
        {
            if (_parent_menu_slices.Count < 2)
            {
                HideMenu();
                return;
            }

            _parent_menu_slices.Pop();
            PieMenuSliceBase<PE, BUT, TOG> menu_slice = _parent_menu_slices.Pop();

            if (menu_slice == null)
                return;

            ShowMenuSlice(menu_slice, ShowMenuAnimationStyle.BackwardAnimation);
        }
        #endregion

        #region Properties
        public Vector2d CenterPostion
        {
            get
            {
                return new Vector2d(_outer_radius, _outer_radius);
            }
        }
        public double PieFontSize
        {
            set;
            get;
        }
        public Brush PieFontBrush
        {
            set;
            get;
        }
        public Brush PieFontStrokeBrush
        {
            set;
            get;
        }
        public float PieFontStrokeThickness
        {
            get;
            set;
        }
        public BitmapEffect PieFontBitmapEffect
        {
            set;
            get;
        }
        public List<PE> PieMenuEntries
        {
            get
            {
                return _menu_entries;
            }
            set
            {
                _menu_entries = value;
            }
        }
        #endregion

        #region Attributes

        protected Grid _base_grid;
        protected bool _close_on_outer_ellipse_click;
        protected List<PE> _menu_entries;

        protected PieMenuSliceBase<PE, BUT, TOG> _root_menu_slice;
        protected Dictionary<PE, PieMenuSliceBase<PE, BUT, TOG>> _menu_slices = new Dictionary<PE, PieMenuSliceBase<PE, BUT, TOG>>();
        protected Stack<PieMenuSliceBase<PE, BUT, TOG>> _parent_menu_slices = new Stack<PieMenuSliceBase<PE, BUT, TOG>>();

        protected double _inner_radius = 30;
        protected double _outer_radius = 150;

        public event EventHandler MenuHidden;

        private int? _move_stylus_id = null;
        #endregion


    }
}
