﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Shapes;
using Logicx.DiagramLib.Util;
using Logicx.DiagramLib.ViewConfig;

namespace Logicx.DiagramLib.Scales
{

    public class DiscreteScaleControl<T> : TypedScaleControl<T>
    {
        #region Constants
        #endregion

        #region Construction and Initialization
        public DiscreteScaleControl()
        {
        }

        public DiscreteScaleControl(AxisScaleConfig<T> config)
            : this()
        {
            Config = config;

            Init(true);
        }

        #endregion

        #region Operations

        private void Init(bool generate_always)
        {
            _current_scale = ScaleFactory.GetInstance().GetScale(_config);

            if (_current_scale == null)
                throw new ArgumentException("No ScaleType available for the given time range " +
                                            Config.Start + " " + Config.End);

            _current_scale.Control = this;

            _scale_object_list = new List<KeyValuePair<T, double>>();

            double size = Config != null && Config.Orientation == Orientation.Horizontal ? _render_width : _render_height;

            double current_pos = 0;
            if (size != 0)
            {
                // initialize key value list
                current_pos = Config.Orientation == Orientation.Horizontal ? Config.Padding.Left : size - Config.Padding.Bottom;
                //current_pos = Config.Orientation == Orientation.Horizontal ? 0 : size;
            }

            double unit_size;
            List<T> values_unique;
            if (Config.ValuesUnique == null || Config.ValuesUnique.Count == 0)
            {
                unit_size = size / Config.Values.Count;
                values_unique = Config.Values;
            }
            else
            {
                unit_size = size / Config.ValuesUnique.Count;
                values_unique = Config.ValuesUnique;
            }

            current_pos += unit_size / 2;
            foreach (T value in values_unique)
            {
                _scale_object_list.Add(new KeyValuePair<T, double>(value, current_pos));

                if (Config.Orientation == Orientation.Horizontal)
                    current_pos += unit_size;
                else
                    current_pos -= unit_size;
            }

            _current_scale.Init(size, generate_always);
        }

        public double GetRenderWidth()
        {
            return _render_width;
        }

        public double GetRenderHeight()
        {
            return _render_height;
        }

        public void ClearChildren()
        {
            foreach (var child in ScaleCanvas.Children)
            {
                if (child is Rectangle)
                    BindingOperations.ClearBinding((Rectangle)child, Rectangle.FillProperty);
                if (child is TextBlock)
                    BindingOperations.ClearBinding((TextBlock)child, TextBlock.StyleProperty);
            }

            ScaleCanvas.Children.Clear();
        }

        public void AddToChildren(List<UIElement> elements)
        {
            foreach (var ui_element in elements)
            {
                ScaleCanvas.Children.Add(ui_element);
            }
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);

            ClearChildren();
            if (_config != null)
            {
                _config.PropertyChanged -= _config_PropertyChanged;
                _config.Dispose();
            }

            if (_current_scale != null)
                _current_scale.Dispose();

            _current_scale = null;
        }

        private void AlignData()
        {
            double size = Config != null && Config.Orientation == Orientation.Horizontal ? _render_width : _render_height;
            if (size != 0)
                _current_scale.AlignData(Config.Orientation);
        }

        public override double TransformAxisValueToPixel(T value)
        {
            return TransformAxisValueToPixel(value, false);
        }

        public override double TransformAxisValueToPixel(T value, bool margin_sensitive)
        {
            T value1 = value;

            IEnumerable<KeyValuePair<T, double>> key_value_pairs = _scale_object_list.Where(o => o.Key.Equals(value1));
            if (key_value_pairs.Count() > 0)
            {
                KeyValuePair<T, double> key_value_pair = key_value_pairs.First();

                if (margin_sensitive)
                    return key_value_pair.Value;

                if (Config.Orientation == Orientation.Horizontal)
                    return key_value_pair.Value - Config.Padding.Left;

                return key_value_pair.Value - Config.Padding.Top;
                //return key_value_pair.Value;
            }

            return 0;
        }

        public override T TransformAxisPixelToValue(double pixel)
        {
            return TransformAxisPixelToValue(pixel, false);
        }

        public override T TransformAxisPixelToValue(double pixel, bool margin_sensitive)
        {
            if (_scale_object_list == null)
                return default(T);

            double value;
            double unit_size;
            if (margin_sensitive)
                value = pixel;
            else
            {
                if (Config.Orientation == Orientation.Horizontal)
                    value = pixel + Config.Padding.Left;
                else
                    value = pixel + Config.Padding.Top;
            }
            //value = pixel;

            List<T> values_unique;
            if (Config.ValuesUnique == null || Config.ValuesUnique.Count == 0)
                values_unique = Config.Values;
            else
                values_unique = Config.ValuesUnique;

            if (Config.Orientation == Orientation.Horizontal)
                unit_size = _render_width / values_unique.Count;
            else
                unit_size = _render_height / values_unique.Count;

            KeyValuePair<T, double> key_value_pair =
                _scale_object_list.Where(o => IsInRange(o.Value, unit_size, value)).FirstOrDefault();

            if ((key_value_pair.Key == null))
                return _scale_object_list.ToArray()[0].Key;

            return key_value_pair.Key;
        }

        private static bool IsInRange(double reference_value, double unit_size, double value_to_check)
        {
            double ref_left = reference_value - unit_size / 2;
            double ref_right = reference_value + unit_size / 2;

            if (value_to_check <= reference_value)
            {
                if (ref_left <= value_to_check)
                    return true;
            }
            else
            {
                if (ref_right >= value_to_check)
                    return true;
            }


            return false;
        }

        public override double GetUnitSize()
        {
            return _current_scale.GetUnitSize(Config.Orientation);
        }

        public override List<double> GetGridLinePositions()
        {
            if (_current_scale == null)
                Init(true);

            return _current_scale.GetGridLinePositions();
        }

        public override double GetScaleStartPixel()
        {
            if (Config.Orientation == Orientation.Horizontal)
                return Config.Padding.Left + _current_scale.GetUnitOffset(Config.Orientation);

            return Config.Padding.Top + _current_scale.GetUnitOffset(Config.Orientation);
        }

        public override double GetScaleEndPixel()
        {
            if (Config.Orientation == Orientation.Horizontal)
                return this.ActualWidth - Config.Padding.Right;

            return this.ActualHeight - Config.Padding.Bottom;
        }

        #endregion

        #region Event Handlers

        void DiscreteScaleControlChanged()
        {
            _render_width = ActualWidth;
            _render_height = ActualHeight;

            if (_render_width >= Config.Padding.Left + Config.Padding.Right)
                _render_width -= Config.Padding.Left + Config.Padding.Right;

            if (_render_height >= Config.Padding.Top + Config.Padding.Bottom)
                _render_height -= Config.Padding.Top + Config.Padding.Bottom;

            InitAndAlign(true);
        }

        protected override void OnScaleSizeChanged(SizeChangedEventArgs e)
        {
            if (Config == null)
                return;

            if (e.WidthChanged)
            {
                _render_width = e.NewSize.Width;
                //if (_render_width >= Config.Padding.Left + Config.Padding.Right)
                //    _render_width -= Config.Padding.Left + Config.Padding.Right;
            }

            if (e.HeightChanged)
            {
                _render_height = e.NewSize.Height;
                //if (_render_height >= Config.Padding.Top + Config.Padding.Bottom)
                //    _render_height -= Config.Padding.Top + Config.Padding.Bottom;
            }

            InitAndAlign(e.WidthChanged || e.HeightChanged);
        }

        protected override void InitAndAlign(bool generate_always)
        {
            if (Config != null)
                Padding = Config.Padding;

            Init(generate_always);

            AlignData();

            OnScalingChanged();
        }

        void _config_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Start")
                DiscreteScaleControlChanged();
            else if (e.PropertyName == "End")
                DiscreteScaleControlChanged();
            else if (e.PropertyName == "Values")
                DiscreteScaleControlChanged();
            else if (e.PropertyName == "ValuesUnique")
                DiscreteScaleControlChanged();
            else if (e.PropertyName == "Description")
                DiscreteScaleControlChanged();
            else if (e.PropertyName == "Orientation")
                DiscreteScaleControlChanged();
            else if (e.PropertyName == "Padding")
                DiscreteScaleControlChanged();
            else if (e.PropertyName == "Alignment")
                ;

        }
        #endregion

        #region Properties

        public override AxisScaleConfig<T> Config
        {
            get
            {
                return _config;
            }
            set
            {
                if (_config != null)
                    _config.PropertyChanged -= _config_PropertyChanged;

                _config = value;

                _config.PropertyChanged += _config_PropertyChanged;

                _current_scale = ScaleFactory.GetInstance().GetScale(_config);

                if (_current_scale == null)
                    throw new ArgumentException("No ScaleType available for the given time range " +
                                                Config.Start + " " + Config.End);

                _current_scale.Control = this;
            }
        }

        public List<KeyValuePair<T, double>> DiscreteObjects
        {
            get
            {
                return _scale_object_list;
            }
            set
            {
                _scale_object_list = value;
            }
        }

        public override bool CanKeyMove
        {
            get { return true; }
        }

        public override double ScaleUnitSmallInPixel
        {
            get
            {
                return 0;
            }
        }
        #endregion

        #region Dependency Properties
        #endregion

        #region Events
        #endregion

        #region Routed Events
        #endregion

        #region Attributes

        private ScaleType _current_scale;

        private double _render_width;
        private double _render_height;
        private AxisScaleConfig<T> _config;

        private List<KeyValuePair<T, double>> _scale_object_list;

        #endregion

        #region Tests
        #endregion

    }
}
