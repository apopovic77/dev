﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PoCShared;

namespace WebHardwareClient.ApiClient
{
    public class PrinterStatusApiClient : PocApiClient
    {
        public const string UriApiPrinterStatus = "printerstatus";
        public const string UriApiPrinterStatusId = "printerstatus/{0}";

        public PrinterStatusApiClient(string endpointUrl, string apiAccessToken = "") : base (endpointUrl, apiAccessToken)
        { }

        public async Task<List<PrinterStatusItem>> Get(int? id, bool scopeToLocalPrinters = true)
        {
            string uri = UriApiPrinterStatus;
            string param = "";

            if (id != null)
                uri = string.Format(UriApiPrinterStatusId, id.Value);

            if (scopeToLocalPrinters)
                param = "printServer=" + Environment.MachineName;

            return await Util.CallGetApiAsync<List<PrinterStatusItem>>(uri, param);
        }

        public async void Update(PrinterStatusItem printer)
        {
            string uri = UriApiPrinterStatus;

            if (printer.Id > 0)
                uri = string.Format(UriApiPrinterStatusId, printer.Id);
            else
            {
                throw new InvalidOperationException("Id must be set (existing printer)");
            }

            await Util.CallPutApiAsync<PrinterStatusItem>(uri, printer);
        }
    }
}
