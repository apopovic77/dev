using System;
using System.Drawing;
using System.Net.Mime;
using DirectXLib.Objects.Fonts;
using Logicx.DirectX.Management;
using Microsoft.DirectX.Direct3D;
using D3d = Microsoft.DirectX.Direct3D;
using Logicx.DirectX.Engine;
using Microsoft.DirectX;



namespace Logicx.DirectX.Objects.Fonts
{
    public class FontScreen2d : Font2d
    {
        public enum CoordinateType
        {
            ModelSpaceCoords = 1,
            ScreenPixelCoords = 2
        }

        public enum AlighnmentType
        {
            Left = 1,
            Center = 2
        }


        #region Contruction

        public FontScreen2d(string text, string font_fam, int font_size, FontStyle fontweight) : base(text,font_fam,font_size,fontweight)
        {
        }

        #endregion

        public override void Render(float elapsedTime)
        {
            //dont do anything if there is no text do render
            if (string.IsNullOrEmpty(Text))
                return;

            //set world coord system back to identity
            Engine3d.Device3d.Transform.World = Matrix.Identity;


            _sprite.Begin(D3d.SpriteFlags.SortTexture | D3d.SpriteFlags.AlphaBlend);
            _sprite.Transform = Matrix.Transformation2D(
                  new Vector2(0, 0),
                  0f,
                  new Vector2(1f, 1f),
                  new Vector2(0, 0),
                  0,
                  new Vector2(0, 0));

            Vector3 plot2d = Position;
            switch(_coordtype)
            {
                case CoordinateType.ModelSpaceCoords:
                    plot2d = Vector3.Project(Position, Engine3d.Camera.Viewport, Engine3d.Camera.Projection, Engine3d.Camera.View, Matrix.Identity);
                    break;
                case CoordinateType.ScreenPixelCoords:
                    break;
                default:
                    throw new Exception3d("CoordType unknown");
            }

            _drawing_area = new Rectangle((int) plot2d.X-50, (int) plot2d.Y,100,100);

            //if(_drawing_area == null)
            //    _active_font.Font3d.DrawText(_sprite, Text, (int) plot2d.X, (int) plot2d.Y, Color);
            //else
            //{
                //Graphics g = Engine3d.Device3d.PresentationParameters.DeviceWindow.CreateGraphics();

                _active_font.Font3d.DrawText(_sprite, Text, _drawing_area, DrawTextFormat.Center | DrawTextFormat.NoClip | DrawTextFormat.WordBreak, Color);
            //}
                
            _sprite.End();
        }

        #region Properties
        public Rectangle DrawingArea
        {
            set
            {
                   
            }
        }
        #endregion

        #region Attribs
        protected CoordinateType _coordtype = CoordinateType.ModelSpaceCoords;
        protected AlighnmentType _alignmenttype;
        protected Rectangle _drawing_area;
        #endregion
    }
}