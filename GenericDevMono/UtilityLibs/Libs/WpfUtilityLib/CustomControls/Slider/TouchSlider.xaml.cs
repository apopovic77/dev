﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using Logicx.WpfUtility.UiManipulation;
using Logicx.WpfUtility.WpfHelpers;
using Windows7.Multitouch;

namespace Logicx.WpfUtility.CustomControls.Slider
{
    /// <summary>
    /// Interaction logic for TouchSlider.xaml
    /// </summary>
    public partial class TouchSlider : UserControl, IDisposable
    {
        #region nested types
        public enum SlideDirection
        {
            Up,
            Down,
            Left,
            Right
        }
        #endregion

        #region Constants
        private const int ANIMATION_TIME_IN_MS = 200;
        #endregion

        #region Construction and Initialization

        private static bool? _is_multitouch_enabled = null;

        internal static bool IsMultiTouchEnabled
        {
            get
            {
                if (!_is_multitouch_enabled.HasValue)
                    _is_multitouch_enabled = TouchHandler.DigitizerCapabilities.IsMultiTouchReady;

                return _is_multitouch_enabled.Value;
            }
        }

        public TouchSlider()
        {
            InitializeComponent();

            base_grid.SizeChanged += new SizeChangedEventHandler(base_grid_SizeChanged);
            elements_control.SizeChanged += new SizeChangedEventHandler(elements_control_SizeChanged);

            _mouse_drag_manipulator = new UiManipulatorRotationalMouseDrag(ui_stack, canvas, canvas, UiManipulator.EnabledAxes.YAxis, new Rect());
            _mouse_drag_manipulator.UiManipulationStarted += new EventHandler<UiManipulationEventArgs>(_ui_manipulator_UiManipulationStarted);
            _mouse_drag_manipulator.UiManipulationCompleted += new EventHandler<UiManipulationEventArgs>(_ui_manipulator_UiManipulationCompleted);
            _mouse_drag_manipulator.UiElementPositionChanged += new EventHandler<UiManipulationEventArgs>(_ui_manipulator_UiElementPositionChanged);

            if (IsMultiTouchEnabled)
            {
                _touch_drag_manipulator = new UiManipulatorTouchRotationalInertiaProcessManipulation(ui_stack, canvas, canvas, UiManipulator.EnabledAxes.YAxis, new Rect());

                _touch_drag_manipulator.UiManipulationStarted += new EventHandler<UiManipulationEventArgs>(_ui_manipulator_UiManipulationStarted);
                //_touch_drag_manipulator.UiManipulationCompleted += new EventHandler<UiManipulationEventArgs>(_ui_manipulator_UiManipulationCompleted);
                _touch_drag_manipulator.UiManipulationInertiaCompleted += new EventHandler<UiManipulationEventArgs>(_ui_manipulator_UiManipulationCompleted);
                _touch_drag_manipulator.UiElementPositionChanged += new EventHandler<UiManipulationEventArgs>(_ui_manipulator_UiElementPositionChanged);
            }

            if (SelectionIndicatorBorderStyle == null)
            {
                SelectionIndicatorBorderStyle = DefaultSelectionIndicatorBorderStyle;
            }
            if (ControlBorderStyle == null)
            {
                ControlBorderStyle = DefaultControlBorderStyle;
            }
        }

        #endregion

        #region Operations

        #region Dependency property changed
        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(e);
            if (e.Property == OrientationProperty)
            {
                if (e.NewValue is Orientation && ((Orientation)e.NewValue) == System.Windows.Controls.Orientation.Horizontal)
                {
                    _mouse_drag_manipulator.EnableAxes = UiManipulator.EnabledAxes.XAxis;
                    if (_touch_drag_manipulator != null)
                        _touch_drag_manipulator.EnableAxes = UiManipulator.EnabledAxes.XAxis;
                    AdjustSelectionIndicator();
                    AdjustInitialCanvasProperties();
                }
                else if (e.NewValue is Orientation && ((Orientation)e.NewValue) == System.Windows.Controls.Orientation.Vertical)
                {
                    _mouse_drag_manipulator.EnableAxes = UiManipulator.EnabledAxes.YAxis;
                    if (_touch_drag_manipulator != null)
                        _touch_drag_manipulator.EnableAxes = UiManipulator.EnabledAxes.YAxis;
                    AdjustSelectionIndicator();
                    AdjustInitialCanvasProperties();
                }
            }
            else if (e.Property == ItemWidthProperty)
            {
                AdjustSelectionIndicator();
            }
            else if (e.Property == ItemHeightProperty)
            {
                AdjustSelectionIndicator();
            }
            else if (e.Property == SelectedIndexProperty)
            {
                if (!_content_loaded)
                {
                    _require_scroll_after_loaded = true;
                    return;
                }

                if (!_ignore_selection_changes)
                {
                    SelectItemByIndex((int)e.NewValue);
                }
            }
            else if (e.Property == SelectedItemProperty)
            {
                if (!_content_loaded)
                {
                    _require_scroll_after_loaded = true;
                    return;
                }

                if (!_ignore_selection_changes)
                {
                    _ignore_selection_changes = true;

                    int old_selected_index = SelectedIndex;

                    if (e.NewValue != null)
                        SelectedIndex = elements_control.Items.IndexOf(e.NewValue);
                    else
                        SelectedIndex = -1;

                    _ignore_selection_changes = false;
                    ScrollToSelectedItem();

                    if (!_require_scroll_after_loaded)
                        if (old_selected_index != SelectedIndex)
                            SendSelectionChanged();
                }
            }
            else if (e.Property == ItemsSourceProperty)
            {
                elements_control.ItemContainerGenerator.ItemsChanged += new System.Windows.Controls.Primitives.ItemsChangedEventHandler(ItemContainerGenerator_Init_ItemsChanged);
            }
        }
        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            Dispose(true);
        }

        private void Dispose(bool disposing)
        {
            if (_disposing)
                return;

            if (!_disposed)
            {
                _disposing = disposing;

                base_grid.SizeChanged -= base_grid_SizeChanged;
                elements_control.SizeChanged -= elements_control_SizeChanged;

                _mouse_drag_manipulator.UiManipulationStarted -= _ui_manipulator_UiManipulationStarted;
                _mouse_drag_manipulator.UiManipulationCompleted -= _ui_manipulator_UiManipulationCompleted;
                _mouse_drag_manipulator.UiElementPositionChanged -= _ui_manipulator_UiElementPositionChanged;

                _mouse_drag_manipulator.DeRegister();

                if (_touch_drag_manipulator != null)
                {
                    _touch_drag_manipulator.UiManipulationStarted -= _ui_manipulator_UiManipulationStarted;
                    //_touch_drag_manipulator.UiManipulationCompleted -= _ui_manipulator_UiManipulationCompleted;
                    _touch_drag_manipulator.UiManipulationInertiaCompleted -= _ui_manipulator_UiManipulationCompleted;
                    _touch_drag_manipulator.UiElementPositionChanged -= _ui_manipulator_UiElementPositionChanged;

                    _touch_drag_manipulator.DeRegister();
                }

                _mouse_drag_manipulator = null;
                _touch_drag_manipulator = null;

                //EventHelper.RemoveAllTextContainerChangedEventHanders(tb);
                //EventHelper.RemoveAllTextContainerChangeEventHanders(tb);
                _disposed = true;
            }
        }

        #endregion

        #region Event raising
        private void SendSelectionChanged()
        {
            if (SelectionChanged != null)
                SelectionChanged(this, new SelectionChangedEventArgs(ListBox.SelectionChangedEvent, new List<object>(), new List<object>()));
        }
        #endregion

        #region Selection methods

        private void SelectItemByIndex(int index)
        {
            if (index >= 0 && index < elements_control.Items.Count)
            {
                object item = elements_control.Items[index];
                SelectedItem = item;
            }
            else
            {
                SelectedItem = null;
            }

            SendSelectionChanged();
        }

        private void ScrollToSelectedItem()
        {

            if (SelectedItem != null)
            {
                DependencyObject container = elements_control.ItemContainerGenerator.ContainerFromItem(SelectedItem);

                if (container != null && container is FrameworkElement)
                {
                    Point position = UIHelper.GetElementPosition((FrameworkElement)container, elements_control);
                    Point position_selection_indicator = UIHelper.GetElementPosition(selection_indicator_border, base_grid);

                    if (Orientation == System.Windows.Controls.Orientation.Vertical)
                    {
                        double center_new_item = position.Y + ItemHeight / 2;
                        double current_top = Canvas.GetTop(ui_stack);
                        if (double.IsNaN(current_top))
                            current_top = 0;

                        double center_selector_offset = position_selection_indicator.Y + selection_indicator_border.ActualHeight / 2;
                        double lower_viewport_center = center_new_item + current_top;
                        double diff_lower = center_selector_offset - lower_viewport_center;

                        double new_top = (current_top + diff_lower) - elements_control.ActualHeight;

                        Canvas.SetTop(ui_stack, new_top);
                        Canvas.SetLeft(ui_stack, 0);

                        //Storyboard sb_scroll_animation = GetTopStoryBoard(new_top);
                        //sb_scroll_animation.Completed += new EventHandler(sb_scroll_left_animation_Completed);
                        //ActivateAnimationProtection();
                        //ui_stack.BeginStoryboard(sb_scroll_animation);
                    }
                    else
                    {
                        double center_new_item = position.X + ItemWidth / 2;
                        double current_left = Canvas.GetLeft(ui_stack);
                        if (double.IsNaN(current_left))
                            current_left = 0;

                        double center_selector_offset = position_selection_indicator.X + selection_indicator_border.ActualWidth / 2;
                        double lower_viewport_center = center_new_item + current_left;
                        double diff_lower = center_selector_offset - lower_viewport_center;

                        double new_left = (current_left + diff_lower) - elements_control.ActualWidth;

                        Canvas.SetLeft(ui_stack, new_left);
                        Canvas.SetTop(ui_stack, 0);


                        //Storyboard sb_scroll_animation = GetLeftStoryBoard(new_top);
                        //sb_scroll_animation.Completed += new EventHandler(sb_scroll_left_animation_Completed);
                        //ActivateAnimationProtection();
                        //ui_stack.BeginStoryboard(sb_scroll_animation);
                    }
                }
                else
                {
                    _require_scroll_after_loaded = true;
                }
            }
        }

        /// <summary>
        /// Called after an UI manipulation has been completed. Will check for the nearest selectable item
        /// and animate the top/left positions to bring this item into the selection area
        /// </summary>
        private void AdjustScrollingPositionAfterUiManipulation()
        {
            if (elements_control.Items.Count == 0)
                return;

            Point current_position = _ui_end_position;
            Point position_selection_indicator = UIHelper.GetElementPosition(selection_indicator_border, base_grid);

            if (Orientation == System.Windows.Controls.Orientation.Vertical)
            {
                double center_selector_offset = position_selection_indicator.Y + selection_indicator_border.ActualHeight / 2;

                double element_height = ItemHeight; // stack_height / elements_control.Items.Count;
                int index_lower = (int)((center_selector_offset - current_position.Y) / element_height);//% elements_control.Items.Count;
                _selected_index_preview = index_lower % elements_control.Items.Count;

                int index_higher = index_lower + 1;

                double lower_center = element_height * (index_lower + 1) - element_height / 2;
                double higher_center = element_height * (index_higher + 1) - element_height / 2;

                double lower_viewport_center = lower_center + current_position.Y;
                double high_viewport_center = higher_center + current_position.Y;

                double diff_lower = center_selector_offset - lower_viewport_center;
                double diff_higher = center_selector_offset - high_viewport_center;

                double new_top = 0;

                Debug.WriteLine(string.Format("Cur POS: {0}, Center Off: {1}, Height: {2}", current_position.Y, center_selector_offset, element_height));
                Debug.WriteLine(string.Format("LOWER {0}, HIGHER {1}, DIR {2}", _selected_index_preview, _selected_index_preview + 1, _slide_direction));
                Debug.WriteLine(string.Format("LOWER Anz: {0},  HIGHER Anz: {1}", index_lower, index_higher));
                Debug.WriteLine(string.Format("Diff LOWER: {0},  Diff HIGHER: {1}", diff_lower, diff_higher));

                bool use_directional_mode = true;
                bool use_higher = true;
                double max_dif = ItemHeight;
                double percentage_diff_lower = Math.Abs(diff_lower) * 100.0 / max_dif;
                double percentage_diff_higher = Math.Abs(diff_higher) * 100.0 / max_dif;
                double correction_value = 0;

                if (percentage_diff_higher <= 20.0)
                    use_directional_mode = true;
                else
                    use_directional_mode = false;

                if (use_directional_mode || _force_directional_mode)
                {
                    use_higher = _slide_direction == SlideDirection.Up;

                    if (_slide_direction == SlideDirection.Down)
                    {
                        correction_value = ItemHeight;
                        _selected_index_preview--;
                    }
                }
                else
                {
                    use_higher = Math.Abs(diff_lower) > Math.Abs(diff_higher) && (Math.Abs(lower_center) != Math.Abs(higher_center));
                }

                //if( Math.Abs(diff_lower) > Math.Abs(diff_higher) && ( Math.Abs(lower_center) != Math.Abs(higher_center)))
                //if(_slide_direction == SlideDirection.Up)
                if (use_higher)
                {
                    _selected_index_preview = index_higher % elements_control.Items.Count;
                    // scroll to higher
                    //Debug.WriteLine("HIGHER is nearer");
                    new_top = current_position.Y + diff_higher;
                    new_top += correction_value;

                    if (new_top < -_mouse_drag_manipulator.ElementSize.Height)
                    {
                        Debug.WriteLine("VH new_top -----");
                        Canvas.SetTop(ui_stack, new_top + _mouse_drag_manipulator.ElementSize.Height - diff_higher - correction_value);
                        new_top += _mouse_drag_manipulator.ElementSize.Height;

                    }
                    else if (new_top > 0)
                    {
                        Debug.WriteLine("VH new_top +++++");
                        Canvas.SetTop(ui_stack, new_top - _mouse_drag_manipulator.ElementSize.Width - diff_higher - correction_value);
                        new_top -= _mouse_drag_manipulator.ElementSize.Height;
                    }

                }
                else
                {
                    // scrol to lower
                    //Debug.WriteLine("LOWER is nearer");
                    new_top = current_position.Y + diff_lower;
                    new_top += correction_value;

                    if (new_top < -_mouse_drag_manipulator.ElementSize.Height)
                    {
                        Debug.WriteLine("VL new_top -----");
                        Canvas.SetTop(ui_stack, new_top + _mouse_drag_manipulator.ElementSize.Height - diff_lower - correction_value);
                        new_top += _mouse_drag_manipulator.ElementSize.Height;

                    }
                    else if (new_top > 0)
                    {
                        Debug.WriteLine("VL new_top +++++");
                        Canvas.SetTop(ui_stack, new_top - _mouse_drag_manipulator.ElementSize.Height - diff_lower - correction_value);
                        new_top -= _mouse_drag_manipulator.ElementSize.Height;
                    }
                }


                Storyboard sb_scroll_animation = GetTopStoryBoard(new_top);
                _anim_target_top = new_top;
                sb_scroll_animation.Completed += new EventHandler(sb_scroll_top_animation_Completed);
                ActivateAnimationProtection();
                ui_stack.BeginStoryboard(sb_scroll_animation);
            }
            else
            {
                double center_selector_offset = position_selection_indicator.X + selection_indicator_border.ActualWidth / 2;

                double element_width = ItemWidth;
                int index_lower = (int)((center_selector_offset - current_position.X) / element_width);
                _selected_index_preview = index_lower % elements_control.Items.Count;

                int index_higher = index_lower + 1;

                double lower_center = element_width * (index_lower + 1) - element_width / 2;
                double higher_center = element_width * (index_higher + 1) - element_width / 2;

                double lower_viewport_center = lower_center + current_position.X;
                double high_viewport_center = higher_center + current_position.X;

                double diff_lower = center_selector_offset - lower_viewport_center;
                double diff_higher = center_selector_offset - high_viewport_center;


                Debug.WriteLine(string.Format("Cur POS: {0}, Center Off: {1}, Width: {2}", current_position.Y, center_selector_offset, element_width));
                Debug.WriteLine(string.Format("LOWER {0}, HIGHER {1}, DIR {2}", _selected_index_preview, _selected_index_preview + 1, _slide_direction));
                Debug.WriteLine(string.Format("LOWER Anz: {0},  HIGHER Anz: {1}", index_lower, index_higher));
                Debug.WriteLine(string.Format("Diff LOWER: {0},  Diff HIGHER: {1}", diff_lower, diff_higher));

                double new_left = 0;

                bool use_directional_mode = true;
                bool use_higher = true;
                double max_dif = ItemWidth;
                double percentage_diff_lower = Math.Abs(diff_lower) * 100.0 / max_dif;
                double percentage_diff_higher = Math.Abs(diff_higher) * 100.0 / max_dif;
                double correction_value = 0;

                if (percentage_diff_higher <= 20.0)
                    use_directional_mode = true;
                else
                    use_directional_mode = false;

                if (use_directional_mode || _force_directional_mode)
                {
                    use_higher = _slide_direction == SlideDirection.Left;

                    if (_slide_direction == SlideDirection.Right)
                    {
                        correction_value = ItemWidth;
                        _selected_index_preview--;
                    }
                }
                else
                {
                    use_higher = Math.Abs(diff_lower) > Math.Abs(diff_higher) && (Math.Abs(lower_center) != Math.Abs(higher_center));
                }

                //if (Math.Abs(diff_lower) > Math.Abs(diff_higher) && (Math.Abs(lower_center) != Math.Abs(higher_center)))
                //if(_slide_direction == SlideDirection.Right)
                if (use_higher)
                {
                    _selected_index_preview = index_higher % elements_control.Items.Count;
                    // scroll to higher
                    //Debug.WriteLine("HIGHER is nearer");
                    new_left = current_position.X + diff_higher;
                    new_left += correction_value;

                    if (new_left < -_mouse_drag_manipulator.ElementSize.Width)
                    {
                        Debug.WriteLine("HH new_top -----");
                        Canvas.SetLeft(ui_stack, new_left + _mouse_drag_manipulator.ElementSize.Width - diff_higher - correction_value);
                        new_left += _mouse_drag_manipulator.ElementSize.Width;

                    }
                    else if (new_left > 0)
                    {
                        Debug.WriteLine("HH new_top +++++");
                        Canvas.SetLeft(ui_stack, new_left - _mouse_drag_manipulator.ElementSize.Width - diff_higher - correction_value);
                        new_left -= _mouse_drag_manipulator.ElementSize.Width;
                    }
                }
                else
                {
                    // scrol to lower
                    //Debug.WriteLine("LOWER is nearer");
                    new_left = current_position.X + diff_lower;
                    new_left += correction_value;

                    if (new_left < -_mouse_drag_manipulator.ElementSize.Width)
                    {
                        Debug.WriteLine("HL new_top -----");
                        Canvas.SetLeft(ui_stack, new_left + _mouse_drag_manipulator.ElementSize.Width - diff_lower - correction_value);
                        new_left += _mouse_drag_manipulator.ElementSize.Width;

                    }
                    else if (new_left > 0)
                    {
                        Debug.WriteLine("HL new_top +++++");
                        Canvas.SetLeft(ui_stack, new_left - _mouse_drag_manipulator.ElementSize.Width - diff_lower - correction_value);
                        new_left -= _mouse_drag_manipulator.ElementSize.Width;
                    }
                }

                Storyboard sb_scroll_animation = GetLeftStoryBoard(new_left);
                _anim_target_left = new_left;
                sb_scroll_animation.Completed += new EventHandler(sb_scroll_left_animation_Completed);
                ActivateAnimationProtection();
                ui_stack.BeginStoryboard(sb_scroll_animation);
            }
        }

        /// <summary>
        /// Called after the new selected item has been set to the scrolling position.
        /// The SelectedItem and SelectedIndex should be set here
        /// </summary>
        private void AdjustSelectedItemAfterUIManipulation()
        {
            _ignore_selection_changes = true;
            int old_selected_index = SelectedIndex;

            if (_selected_index_preview > elements_control.Items.Count)
                SelectedIndex = -1;

            SelectedIndex = _selected_index_preview;
            if (SelectedIndex < 0 || SelectedIndex > elements_control.Items.Count)
                SelectedItem = null;
            else
                SelectedItem = elements_control.Items[SelectedIndex];


            _ignore_selection_changes = false;

            if (old_selected_index != SelectedIndex)
                SendSelectionChanged();

            _selected_index_preview = -1;
        }


        #region selection animation
        private Storyboard GetTopStoryBoard(double target)
        {
            DoubleAnimation in_animation = new DoubleAnimation(target, new Duration(TimeSpan.FromMilliseconds(ANIMATION_TIME_IN_MS)), FillBehavior.Stop);
            Storyboard.SetTargetProperty(in_animation, new PropertyPath(Canvas.TopProperty));

            Storyboard sb_return = new Storyboard();
            sb_return.AccelerationRatio = 0.1;
            sb_return.DecelerationRatio = 0.4;
            sb_return.Children.Add(in_animation);

            return sb_return;
        }

        private Storyboard GetLeftStoryBoard(double target)
        {
            DoubleAnimation in_animation = new DoubleAnimation(target, new Duration(TimeSpan.FromMilliseconds(ANIMATION_TIME_IN_MS)), FillBehavior.Stop);
            Storyboard.SetTargetProperty(in_animation, new PropertyPath(Canvas.LeftProperty));

            Storyboard sb_return = new Storyboard();
            sb_return.AccelerationRatio = 0.1;
            sb_return.DecelerationRatio = 0.4;
            sb_return.Children.Add(in_animation);

            return sb_return;
        }

        private void ActivateAnimationProtection()
        {
            base_grid.IsHitTestVisible = false;
        }

        private void DeactivateAnimationProtection()
        {
            base_grid.IsHitTestVisible = true;
        }
        #endregion
        #endregion

        #region Helper operations
        private void AdjustSelectionIndicator()
        {
            BindingOperations.ClearBinding(selection_indicator_border, HeightProperty);
            BindingOperations.ClearBinding(selection_indicator_border, WidthProperty);

            if (Orientation == System.Windows.Controls.Orientation.Horizontal)
            {
                Binding height_binding = new Binding("ActualHeight");
                height_binding.Source = this;
                selection_indicator_border.SetBinding(HeightProperty, height_binding);

                Binding width_binding = new Binding("ItemWidth");
                width_binding.Source = this;
                selection_indicator_border.SetBinding(WidthProperty, width_binding);
            }
            else
            {
                Binding width_binding = new Binding("ActualWidth");
                width_binding.Source = this;
                selection_indicator_border.SetBinding(WidthProperty, width_binding);

                Binding height_binding = new Binding("ItemHeight");
                height_binding.Source = this;
                selection_indicator_border.SetBinding(HeightProperty, height_binding);
            }
        }

        private void AdjustInitialCanvasProperties()
        {
            if (Orientation == System.Windows.Controls.Orientation.Vertical)
            {
                Canvas.SetTop(ui_stack, 0);//-elements_control.ActualHeight);
                Canvas.SetLeft(ui_stack, 0);
            }
            else
            {
                Canvas.SetLeft(ui_stack, 0);//-elements_control.ActualWidth);
                Canvas.SetTop(ui_stack, 0);
            }
        }

        private void AdjustItemWidthAndHeight()
        {
            if (!AutoSize || elements_control.Items.Count == 0)
                return;

            double max_width = 0;
            double max_height = 0;

            for (int i = 0; i < elements_control.Items.Count; i++)
            {
                DependencyObject container = elements_control.ItemContainerGenerator.ContainerFromIndex(i);

                if (container is FrameworkElement)
                {
                    FrameworkElement container_elem = container as FrameworkElement;

                    if (container_elem.ActualWidth > max_width)
                        max_width = container_elem.ActualWidth;

                    if (container_elem.ActualHeight > max_height)
                        max_height = container_elem.ActualHeight;
                }
            }

            ItemHeight = max_height;
            ItemWidth = max_width;
        }
        #endregion


        #endregion

        #region Event Handlers

        #region scroll animation
        void sb_scroll_top_animation_Completed(object sender, EventArgs e)
        {
            if (sender is Storyboard)
                ((Storyboard)sender).Completed -= sb_scroll_top_animation_Completed;
            else
                ((ClockGroup)sender).Completed -= sb_scroll_top_animation_Completed;

            double cur_top = Canvas.GetTop(ui_stack);
            ui_stack.BeginAnimation(Canvas.TopProperty, null);
            //Canvas.SetTop(ui_stack, cur_top);
            Canvas.SetTop(ui_stack, _anim_target_top);

            AdjustSelectedItemAfterUIManipulation();
            DeactivateAnimationProtection();
        }

        void sb_scroll_left_animation_Completed(object sender, EventArgs e)
        {
            if (sender is Storyboard)
                ((Storyboard)sender).Completed -= sb_scroll_left_animation_Completed;
            else
                ((ClockGroup)sender).Completed -= sb_scroll_left_animation_Completed;

            double cur_left = Canvas.GetLeft(ui_stack);
            ui_stack.BeginAnimation(Canvas.LeftProperty, null);
            //Canvas.SetLeft(ui_stack, cur_left);
            Canvas.SetTop(ui_stack, _anim_target_left);

            AdjustSelectedItemAfterUIManipulation();
            DeactivateAnimationProtection();
        }
        #endregion

        #region Mouse and stylus interception
        private void canvas_border_MouseDown(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
        }

        private void canvas_border_MouseUp(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
        }

        private void canvas_border_MouseMove(object sender, MouseEventArgs e)
        {
            e.Handled = true;
        }

        private void canvas_border_StylusDown(object sender, StylusDownEventArgs e)
        {
            e.Handled = true;
        }

        private void canvas_border_StylusUp(object sender, StylusEventArgs e)
        {
            e.Handled = true;
        }

        private void canvas_border_StylusMove(object sender, StylusEventArgs e)
        {
            e.Handled = true;
        }
        #endregion

        void base_grid_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            ScrollToSelectedItem();
        }

        private void canvas_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            e.Handled = true;

            if (!IsAnimationProtectionActive)
                return;

            double nDelta = 0;

            Point pt_cur_position = new Point(Canvas.GetLeft(ui_stack), Canvas.GetTop(ui_stack));

            if (Orientation == System.Windows.Controls.Orientation.Vertical)
            {
                if (InverseMouseWheel)
                    nDelta = ItemHeight * (e.Delta > 0 ? 1 : -1);
                else
                    nDelta = ItemHeight * (e.Delta < 0 ? 1 : -1);

                double next_left = pt_cur_position.X;
                double next_top = pt_cur_position.Y + nDelta;

                if (next_top < -_mouse_drag_manipulator.ElementSize.Height)
                {
                    // adjust current top position
                    Canvas.SetTop(ui_stack, next_top + _mouse_drag_manipulator.ElementSize.Height - nDelta);
                    next_top += _mouse_drag_manipulator.ElementSize.Height;

                }
                else if (next_top > 0)
                {
                    Canvas.SetTop(ui_stack, next_top - _mouse_drag_manipulator.ElementSize.Height - nDelta);
                    next_top -= _mouse_drag_manipulator.ElementSize.Height;

                }

                if (nDelta > 0)
                {
                    if (InverseMouseWheel)
                        _slide_direction = SlideDirection.Up;
                    else
                        _slide_direction = SlideDirection.Down;
                }
                else
                {
                    if (InverseMouseWheel)
                        _slide_direction = SlideDirection.Down;
                    else
                        _slide_direction = SlideDirection.Up;
                }
                _ui_end_position = new Point(next_left, next_top);
            }
            else
            {
                if (InverseMouseWheel)
                    nDelta = ItemWidth * (e.Delta > 0 ? 1 : -1);
                else
                    nDelta = ItemWidth * (e.Delta < 0 ? 1 : -1);

                double next_left = pt_cur_position.X + nDelta;
                double next_top = pt_cur_position.Y;

                if (next_left < -_mouse_drag_manipulator.ElementSize.Width)
                {
                    Canvas.SetLeft(ui_stack, next_left + _mouse_drag_manipulator.ElementSize.Width - nDelta);
                    next_left += _mouse_drag_manipulator.ElementSize.Width;
                }
                else if (next_left > 0)
                {
                    Canvas.SetLeft(ui_stack, next_left - _mouse_drag_manipulator.ElementSize.Width - nDelta);
                    next_left -= _mouse_drag_manipulator.ElementSize.Width;
                }

                if (nDelta > 0)
                {
                    if (InverseMouseWheel)
                        _slide_direction = SlideDirection.Right;
                    else
                        _slide_direction = SlideDirection.Left;
                }
                else
                {
                    if (InverseMouseWheel)
                        _slide_direction = SlideDirection.Left;
                    else
                        _slide_direction = SlideDirection.Right;
                }

                _ui_end_position = new Point(next_left, next_top);
            }

            AdjustScrollingPositionAfterUiManipulation();
        }

        void elements_control_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            _mouse_drag_manipulator.ElementSize = new Rect(0, 0, e.NewSize.Width, e.NewSize.Height);
            if (_touch_drag_manipulator != null)
                _touch_drag_manipulator.ElementSize = _mouse_drag_manipulator.ElementSize;

            AdjustSelectionIndicator();
            AdjustInitialCanvasProperties();
        }

        private void elements_control_Loaded(object sender, RoutedEventArgs e)
        {
            AdjustItemWidthAndHeight();
            elements_control.ItemContainerGenerator.ItemsChanged += new System.Windows.Controls.Primitives.ItemsChangedEventHandler(ItemContainerGenerator_ItemsChanged);

            _content_loaded = true;

            if (_require_scroll_after_loaded)
            {
                _ignore_selection_changes = true;

                SelectedIndex = elements_control.Items.IndexOf(SelectedItem);
                ScrollToSelectedItem();
                _require_scroll_after_loaded = false;
                _ignore_selection_changes = false;
                SendSelectionChanged();
            }
        }

        private void elements_control_Unloaded(object sender, RoutedEventArgs e)
        {
            elements_control.ItemContainerGenerator.ItemsChanged -= ItemContainerGenerator_ItemsChanged;
        }

        void ItemContainerGenerator_ItemsChanged(object sender, System.Windows.Controls.Primitives.ItemsChangedEventArgs e)
        {
            AdjustItemWidthAndHeight();
        }

        void ItemContainerGenerator_Init_ItemsChanged(object sender, System.Windows.Controls.Primitives.ItemsChangedEventArgs e)
        {
            // wird aufgerufen wenn sich die items im container ändern (z.b. nach einem ItemsSource change)
            elements_control.ItemContainerGenerator.ItemsChanged -= ItemContainerGenerator_Init_ItemsChanged;
            SelectedIndex = 0;
        }

        #region UIManipulation event handlers
        void _ui_manipulator_UiElementPositionChanged(object sender, UiManipulationEventArgs e)
        {
            _last_mouse_pos = new Point(_last_mouse_pos.X + Math.Abs(e.ActualPosition.X), _last_mouse_pos.Y + Math.Abs(e.ActualPosition.Y));

            //Debug.WriteLine("Sender: " + sender.GetHashCode());
            if (Orientation == System.Windows.Controls.Orientation.Vertical)
            {

                if (e.ActualPosition.Y == 0)
                {
                    _ui_last_position = e.ActualPosition;
                    return;
                }

                Vector vDir = e.ActualPosition - _ui_last_position;

                //Debug.WriteLine(string.Format("ActPOS: {0}, LastPOS: {1}, VECTOR: {2}, VLength: {3}", e.ActualPosition.Y.ToString("0.00"), _ui_last_position.Y.ToString("0.00"), vDir.Y.ToString("0.00"), vDir.Length.ToString("0.00")));

                if (vDir.Y < 0)
                    _slide_direction = SlideDirection.Up;
                else if (vDir.Y > 0)
                    _slide_direction = SlideDirection.Down;
            }
            else
            {
                if (e.ActualPosition.X == 0)
                {
                    _ui_last_position = e.ActualPosition;
                    return;
                }

                Vector vDir = e.ActualPosition - _ui_last_position;

                if (vDir.X < 0)
                    _slide_direction = SlideDirection.Left;
                else if (vDir.X > 0)
                    _slide_direction = SlideDirection.Right;
            }

            _ui_last_position = e.ActualPosition;
        }

        void _ui_manipulator_UiManipulationCompleted(object sender, UiManipulationEventArgs e)
        {
            _ui_manipulation_active = false;
            if (e == null)
                ;//_ui_end_position = _ui_last_position;
            else
                _ui_end_position = e.ActualPosition;

            CloseTimer();

            Vector vDir = _ui_end_position - _ui_start_position;
            //Debug.WriteLine("_ui_manipulator_UiManipulationCompleted");
            if (Orientation == System.Windows.Controls.Orientation.Vertical)
            {
                double dCheck = vDir.Length;
                if (dCheck > this.ActualHeight)
                    dCheck = _mouse_drag_manipulator.ElementSize.Height - dCheck;

                if (dCheck < ItemHeight / 2 && _last_mouse_pos.Y < ItemHeight / 2)
                    _force_directional_mode = true;
            }
            else
            {
                double dCheck = vDir.Length;
                if (dCheck > this.ActualWidth)
                    dCheck = _mouse_drag_manipulator.ElementSize.Width - dCheck;

                if (vDir.Length < ItemWidth / 2 && _last_mouse_pos.X < ItemWidth / 2)
                    _force_directional_mode = true;
            }
            AdjustScrollingPositionAfterUiManipulation();
            _force_directional_mode = false;
            _last_mouse_pos = new Point();
        }

        void _ui_manipulator_UiManipulationStarted(object sender, UiManipulationEventArgs e)
        {
            _ui_manipulation_active = true;
            _force_directional_mode = false;
            _ui_start_position = e.ActualPosition;
            _ui_last_position = _ui_start_position;

            CloseTimer();
            _manipultion_timer = new Timer();
            _manipultion_timer.Interval = 2000;
            _manipultion_timer.Start();
            _manipultion_timer.Elapsed += new ElapsedEventHandler(_manipultion_timer_Elapsed);

            //Debug.WriteLine("   _ui_manipulator_UiManipulationStarted startpos= " + _ui_start_position);
        }

        void _manipultion_timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            Dispatcher.Invoke(DispatcherPriority.Normal, (NoParas) delegate
            {
                _ui_manipulator_UiManipulationCompleted(null, null);
            });
        }

        private void CloseTimer()
        {
            if (_manipultion_timer != null)
            {
                _manipultion_timer.Stop();
                _manipultion_timer.Elapsed -= _manipultion_timer_Elapsed;
                _manipultion_timer.Close();
                _manipultion_timer.Dispose();
                //_manipultion_timer = null;
            }
        }

        #endregion

        #endregion

        #region Properties

        private bool IsAnimationProtectionActive
        {
            get { return base_grid.IsHitTestVisible; }
        }

        public bool InverseMouseWheel
        {
            get;
            set;
        }

        #region static properties
        public static Style DefaultSelectionIndicatorBorderStyle
        {
            get
            {
                if (_default_selection_border_style != null)
                    return _default_selection_border_style;

                _default_selection_border_style = new Style(typeof(Border));
                _default_selection_border_style.Setters.Add(new Setter(Border.BackgroundProperty, new SolidColorBrush(Color.FromArgb(0x1F, 0x00, 0x12, 0xef))));

                return _default_selection_border_style;
            }
        }

        public static Style DefaultControlBorderStyle
        {
            get
            {
                if (_default_control_border_style != null)
                    return _default_control_border_style;

                _default_control_border_style = new Style(typeof(Border));
                _default_control_border_style.Setters.Add(new Setter(Border.BackgroundProperty, Brushes.Transparent));

                return _default_control_border_style;
            }
        }
        #endregion
        #endregion

        #region Dependency Properties
        public object ItemsSource
        {
            get { return (object)GetValue(ItemsSourceProperty); }
            set { SetValue(ItemsSourceProperty, value); }
        }
        public static readonly DependencyProperty ItemsSourceProperty = DependencyProperty.Register("ItemsSource", typeof(object), typeof(TouchSlider), new UIPropertyMetadata(null));

        public Orientation Orientation
        {
            get { return (Orientation)GetValue(OrientationProperty); }
            set { SetValue(OrientationProperty, value); }
        }
        public static readonly DependencyProperty OrientationProperty = DependencyProperty.Register("Orientation", typeof(Orientation), typeof(TouchSlider), new UIPropertyMetadata(Orientation.Vertical));


        public double ItemWidth
        {
            get { return (double)GetValue(ItemWidthProperty); }
            set { SetValue(ItemWidthProperty, value); }
        }
        public static readonly DependencyProperty ItemWidthProperty = DependencyProperty.Register("ItemWidth", typeof(double), typeof(TouchSlider), new UIPropertyMetadata((double)40));

        public double ItemHeight
        {
            get { return (double)GetValue(ItemHeightProperty); }
            set { SetValue(ItemHeightProperty, value); }
        }
        public static readonly DependencyProperty ItemHeightProperty = DependencyProperty.Register("ItemHeight", typeof(double), typeof(TouchSlider), new UIPropertyMetadata((double)20));


        public object SelectedItem
        {
            get { return (object)GetValue(SelectedItemProperty); }
            set { SetValue(SelectedItemProperty, value); }
        }
        public static readonly DependencyProperty SelectedItemProperty = DependencyProperty.Register("SelectedItem", typeof(object), typeof(TouchSlider), new UIPropertyMetadata(null));

        public int SelectedIndex
        {
            get { return (int)GetValue(SelectedIndexProperty); }
            set { SetValue(SelectedIndexProperty, value); }
        }
        public static readonly DependencyProperty SelectedIndexProperty = DependencyProperty.Register("SelectedIndex", typeof(int), typeof(TouchSlider), new UIPropertyMetadata((int)-1));

        public bool AutoSize
        {
            get { return (bool)GetValue(AutoSizeProperty); }
            set { SetValue(AutoSizeProperty, value); }
        }
        public static readonly DependencyProperty AutoSizeProperty = DependencyProperty.Register("AutoSize", typeof(bool), typeof(TouchSlider), new UIPropertyMetadata(true));


        /// <summary>
        /// das item template für ein item in dem control
        /// </summary>
        public DataTemplate ItemTemplate
        {
            get { return (DataTemplate)GetValue(ItemTemplateProperty); }
            set { SetValue(ItemTemplateProperty, value); }
        }
        public static readonly DependencyProperty ItemTemplateProperty = DependencyProperty.Register("ItemTemplate", typeof(DataTemplate), typeof(TouchSlider), new UIPropertyMetadata(null));

        /// <summary>
        /// der style für den selection indicator
        /// </summary>
        public Style SelectionIndicatorBorderStyle
        {
            get { return (Style)GetValue(SelectionIndicatorBorderStyleProperty); }
            set { SetValue(SelectionIndicatorBorderStyleProperty, value); }
        }
        public static readonly DependencyProperty SelectionIndicatorBorderStyleProperty = DependencyProperty.Register("SelectionIndicatorBorderStyle", typeof(Style), typeof(TouchSlider), new UIPropertyMetadata(null));


        /// <summary>
        /// der style für das control
        /// </summary>
        public Style ControlBorderStyle
        {
            get { return (Style)GetValue(ControlBorderStyleProperty); }
            set { SetValue(ControlBorderStyleProperty, value); }
        }
        public static readonly DependencyProperty ControlBorderStyleProperty = DependencyProperty.Register("ControlBorderStyle", typeof(Style), typeof(TouchSlider), new UIPropertyMetadata(null));

        #endregion

        #region Events
        #endregion

        #region Routed Events
        public event SelectionChangedEventHandler SelectionChanged;
        #endregion

        #region Attributes
        private bool _disposed = false;
        private bool _disposing = false;
        private bool _ignore_selection_changes = false;
        private bool _content_loaded = false;
        private int _selected_index_preview = -1;
        private bool _require_scroll_after_loaded = false;
        private bool _force_directional_mode = false;
        private Point _last_mouse_pos;

        private double _anim_target_top = 0;
        private double _anim_target_left = 0;

        private UiManipulatorRotationalMouseDrag _mouse_drag_manipulator = null;
        private UiManipulatorTouchRotationalInertiaProcessManipulation _touch_drag_manipulator = null;

        private bool _ui_manipulation_active = false;
        private Point _ui_start_position;
        private Point _ui_last_position;
        private Point _ui_end_position;
        private SlideDirection _slide_direction = SlideDirection.Down;

        private Timer _manipultion_timer;
        private delegate void NoParas();

        private static Style _default_selection_border_style = null;
        private static Style _default_control_border_style = null;
        #endregion

        #region Tests
        #endregion
    }
}
