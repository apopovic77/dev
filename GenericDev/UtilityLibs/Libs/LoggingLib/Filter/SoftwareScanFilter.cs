﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using LoggingLib.Configuration;
using LoggingLib.Data;
using LoggingLib.SystemInterop;

namespace LoggingLib.Filter
{
    /// <summary>
    /// Class implementing a software scan filter
    /// </summary>
    internal class SoftwareScanFilter
    {
        #region nested classes and types
        /// <summary>
        /// Internal filter rule class
        /// </summary>
        internal class FilterRule
        {
            #region Construction and initialization
            /// <summary>
            /// Make sure the default constructor may only be called within 
            /// this assembly. Object of these type are only for <see cref="SoftwareScanFilter"/> internal
            /// data processing.
            /// </summary>
            internal FilterRule()
            { }

            /// <summary>
            /// Internal constructor generating a new filter rule instance
            /// </summary>
            /// <param name="type">Type of the filter rule</param>
            /// <param name="compareFieldName">Field name to compare</param>
            /// <param name="compareValue">value for comparision</param>
            internal FilterRule(SoftwareFilterType type, string compareFieldName, string compareValue)
            {
                _type = type;
                _compare_field_name = compareFieldName;
                _compare_value = compareValue;
            }
            #endregion

            #region Operations
            /// <summary>
            /// Checks if the given log entry matches the current filter rule
            /// </summary>
            /// <param name="swTitle">Software title</param>
            /// <param name="swPublisher">Software publisher</param>
            /// <param name="swVersion">Software version</param>
            /// <returns>Returns false if the rule doesn't apply at the log entry</returns>
            internal bool CheckFilterRule(string swTitle, string swPublisher, string swVersion)
            {
                bool bRet = false;

                string checkValue = swTitle;

                if (_compare_field_name.ToLower() == "publisher")
                    checkValue = swPublisher;
                else if (_compare_field_name.ToLower() == "version")
                    checkValue = swVersion;

                switch (_type)
                {
                    case SoftwareFilterType.Exact:
                        {
                            bRet = checkValue.Equals(_compare_value);
                        }; break;

                    case SoftwareFilterType.RegEx:
                        {
                            string sregexFilter = _compare_value;
                            //sregexFilter = sregexFilter.Replace(".","\\.");
                            //sregexFilter = sregexFilter.Replace("^","\\^");
                            //sregexFilter = sregexFilter.Replace("$","\\$");
                            //sregexFilter = sregexFilter.Replace("[","\\[");
                            //sregexFilter = sregexFilter.Replace("]","\\]");
                            //sregexFilter = sregexFilter.Replace("+","\\+");
                            //sregexFilter = sregexFilter.Replace("-","\\-");
                            //sregexFilter = sregexFilter.Replace("?","\\?");

                            Regex checkFilter = new Regex(sregexFilter, RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.Singleline);

                            bRet = checkFilter.IsMatch(checkValue);
                        }; break;
                }

                return bRet;
            }
            #endregion

            #region Properties
            /// <summary>
            /// Gets the filter rule type
            /// </summary>
            internal SoftwareFilterType RuleType
            {
                get { return _type; }
            }
            #endregion

            #region Attribs
            private SoftwareFilterType _type = SoftwareFilterType.Exact;
            private string _compare_field_name = "";
            private string _compare_value = "";
            #endregion
        }
        #endregion

        #region Construction and initialization
        /// <summary>
        /// Constructor of the software scan filter
        /// </summary>
        /// <param name="settings">Logging configuration settings</param>
        public SoftwareScanFilter(LoggingConfigSection settings)
        {
            if (settings == null)
                throw new ArgumentException("Settings must not be null.", "settings");

            BuildFilterRules(settings);
        }
        #endregion

        #region Operations and Methods
        /// <summary>
        /// Applies the log filter to a log entry
        /// </summary>
        /// <param name="swItem">A software item collected by the software collector</param>
        /// <returns>Returns true is the entry should be logged, otherwise false</returns>
        public bool ApplyFilter(SoftwareCollector.SoftwareItem swItem)
        {
            if (swItem == null)
                return false;

            return ApplyFilter(swItem.Title, swItem.Publisher, swItem.Version);
        }
        /// <summary>
        /// Applies the log filter to a log entry
        /// </summary>
        /// <param name="swTitle">Software title</param>
        /// <param name="swPublisher">Software publisher</param>
        /// <param name="swVersion">Software version</param>
        /// <returns>Returns true is the entry should be logged, otherwise false</returns>
        public bool ApplyFilter(string swTitle, string swPublisher, string swVersion)
        {
            bool bRet = true;
            bool bFirst = true;

            foreach(FilterRule curRule in _filter_rule_list)
            {
                if(bFirst)
                {
                    bRet = curRule.CheckFilterRule(swTitle, swPublisher, swVersion);
                    bFirst = false;
                } 
                else
                {
                    bRet |= curRule.CheckFilterRule(swTitle, swPublisher, swVersion);
                    if (bRet)
                        break;
                }
            }

            return bRet;
        }
        /// <summary>
        /// Builds the internal filter rules collection
        /// </summary>
        /// <param name="settings">Logging configuration settings</param>
        private void BuildFilterRules(LoggingConfigSection settings)
        {
            if (_filter_rule_list == null)
                _filter_rule_list = new List<FilterRule>();

            foreach(SwFilterRuleElement scanFilter in settings.SoftwareScanFilter)
            {
                FilterRule scanRule = new FilterRule(scanFilter.Type, scanFilter.CompareFieldName, scanFilter.CompareValue);
                _filter_rule_list.Add(scanRule);
            }
        }
        #endregion

        #region Attribs
        private List<FilterRule> _filter_rule_list = new List<FilterRule>();
        #endregion
    }
}