﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace Logicx.WpfUtility.WpfHelpers
{
    public class EventHelper
    {
        #region Unregister Standard Event delegates
        public static void UnregisterEvents(EventHandler event_handler)
        {
            if (event_handler == null)
                return;

            foreach (EventHandler eventDelegate in event_handler.GetInvocationList())
                event_handler -= eventDelegate;

        }

        public static void UnregisterEvents(PropertyChangedEventHandler event_handler)
        {
            if (event_handler == null)
                return;

            foreach (PropertyChangedEventHandler eventDelegate in event_handler.GetInvocationList())
                event_handler -= eventDelegate;

        }

        public static void UnregisterEvents(PropertyChangingEventHandler event_handler)
        {
            if (event_handler == null)
                return;

            foreach (PropertyChangingEventHandler eventDelegate in event_handler.GetInvocationList())
                event_handler -= eventDelegate;

        }
        #endregion

        #region Standard Reflection Methods
        /// <summary>
        /// Gets the invocation list of a given event name
        /// </summary>
        /// <param name="event_provider"></param>
        /// <param name="object_type"></param>
        /// <param name="event_name"></param>
        /// <returns></returns>
        public static Delegate[] GetInvocationList(object event_provider, Type object_type, string event_name)
        {
            return GetInvocationList(event_provider, object_type, event_name, false);
        }

        /// <summary>
        /// Gets the invocation list of a given event name
        /// </summary>
        /// <param name="event_provider"></param>
        /// <param name="object_type"></param>
        /// <param name="event_name"></param>
        /// <param name="static_event"></param>
        /// <returns></returns>
        public static Delegate[] GetInvocationList(object event_provider, Type object_type, string event_name, bool static_event)
        {
            FieldInfo f1 = object_type.GetField(event_name,
                (static_event ? BindingFlags.Static : BindingFlags.Instance) | BindingFlags.NonPublic);

            if (f1 == null)
                return null;

            object obj = f1.GetValue(event_provider);

            if (obj is Delegate)
            {
                Delegate event_delegate = obj as Delegate;

                Delegate[] invocation_list = event_delegate.GetInvocationList();
                return invocation_list;
            }

            return null;
        }
        /// <summary>
        /// Gets the event delegate of a given event name
        /// </summary>
        /// <param name="event_provider"></param>
        /// <param name="object_type"></param>
        /// <param name="event_name"></param>
        /// <returns></returns>
        public static Delegate GetEventDelegate(object event_provider, Type object_type, string event_name)
        {
            return GetEventDelegate(event_provider, object_type, event_name, false);
        }
        /// <summary>
        /// Gets the event delegate of a given event name
        /// </summary>
        /// <param name="event_provider"></param>
        /// <param name="object_type"></param>
        /// <param name="event_name"></param>
        /// <param name="static_event"></param>
        /// <returns></returns>
        public static Delegate GetEventDelegate(object event_provider, Type object_type, string event_name, bool static_event)
        {
             FieldInfo f1 = object_type.GetField(event_name,
                (static_event ? BindingFlags.Static : BindingFlags.Instance) | BindingFlags.NonPublic);

            if (f1 == null)
                return null;

            object obj = f1.GetValue(event_provider);

            if (obj is Delegate)
            {
                return obj as Delegate;
            }

            return null;
        }


        public static void UpdateEventDelegate(Delegate value, object event_provider, Type object_type, string event_name)
        {
            UpdateEventDelegate(value, event_provider, object_type, event_name, false);
        }

        public static void UpdateEventDelegate(Delegate value, object event_provider, Type object_type, string event_name, bool static_event)
        {
            FieldInfo f1 = object_type.GetField(event_name,
               (static_event ? BindingFlags.Static : BindingFlags.Instance) | BindingFlags.NonPublic);

            if (f1 == null)
                return;

            f1.SetValue(event_provider, value);

        }

        public static void ClearInvocationList(Delegate target_delegate)
        {
            
        }
        #endregion

        #region INotifyCollectionChanged deregistration
        /// <summary>
        /// Removes all CollectionChanged event subscriptions from the source object
        /// </summary>
        /// <param name="event_source_object"></param>
        public static void RemoveAllCollectionChangedEventHandlers(INotifyCollectionChanged event_source_object)
        {
            if(event_source_object == null)
                return;

            // get the current event delegate
            NotifyCollectionChangedEventHandler event_delegate = GetEventDelegate(event_source_object, event_source_object.GetType(), "CollectionChanged") as NotifyCollectionChangedEventHandler;
            // get the invocation list
            Delegate[] invocation_list = EventHelper.GetInvocationList(event_source_object, event_source_object.GetType(), "CollectionChanged");

            // clear the invocation list
            if(event_delegate != null && invocation_list != null)
            {
                foreach (NotifyCollectionChangedEventHandler eventDelegate in invocation_list)
                    event_delegate -= eventDelegate;
            }

            // write the new delegate back to the source object to clear it
            UpdateEventDelegate(event_delegate, event_source_object, event_source_object.GetType(), "CollectionChanged");
        }
        #endregion

        #region TextContainerChanged
        public static void RemoveAllTextContainerChangedEventHanders(TextBox event_source_object)
        {
            if (Application.Current != null && !Application.Current.Dispatcher.CheckAccess())
                return;

            if (event_source_object == null)
                return;

            if(BindingOperations.IsDataBound(event_source_object, TextBox.TextProperty))
                BindingOperations.ClearBinding(event_source_object, TextBox.TextProperty);

            PropertyInfo p1 = event_source_object.GetType().GetProperty("TextContainer",
                BindingFlags.Instance | BindingFlags.NonPublic);

            if (p1 == null)
                return;

            object obj = p1.GetValue(event_source_object, null);

            if (obj != null)
            {
                // get the current event delegate
                Delegate event_delegate = GetEventDelegate(obj, obj.GetType(), "ChangedHandler");
                // get the invocation list
                Delegate[] invocation_list = EventHelper.GetInvocationList(obj, obj.GetType(), "ChangedHandler");

                //// clear the invocation list
                //if (event_delegate != null && invocation_list != null)
                //{
                //    foreach (NotifyCollectionChangedEventHandler eventDelegate in invocation_list)
                //        event_delegate -= eventDelegate;
                //}

                // write the new delegate back to the source object to clear it
                UpdateEventDelegate(null, obj, obj.GetType(), "ChangedHandler");
            }
        }

        public static void RemoveAllTextContainerChangeEventHanders(TextBox event_source_object)
        {
            if (Application.Current != null && !Application.Current.Dispatcher.CheckAccess())
                return;

            if (event_source_object == null)
                return;

            if (BindingOperations.IsDataBound(event_source_object, TextBox.TextProperty))
                BindingOperations.ClearBinding(event_source_object, TextBox.TextProperty);

            PropertyInfo p1 = event_source_object.GetType().GetProperty("TextContainer",
                BindingFlags.Instance | BindingFlags.NonPublic);

            if (p1 == null)
                return;

            object obj = p1.GetValue(event_source_object, null);

            if (obj != null)
            {
                // get the current event delegate
                Delegate event_delegate = GetEventDelegate(obj, obj.GetType(), "ChangeHandler");
                // get the invocation list
                Delegate[] invocation_list = EventHelper.GetInvocationList(obj, obj.GetType(), "ChangeHandler");

                //// clear the invocation list
                //if (event_delegate != null && invocation_list != null)
                //{
                //    foreach (NotifyCollectionChangedEventHandler eventDelegate in invocation_list)
                //        event_delegate -= eventDelegate;
                //}

                // write the new delegate back to the source object to clear it
                UpdateEventDelegate(null, obj, obj.GetType(), "ChangeHandler");
            }
        }

        
        #endregion


    }
}
