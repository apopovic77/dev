﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;

namespace Logicx.DiagramLib.Scales
{
    public abstract class ScaleType : IDisposable
    {
        #region Constants
        #endregion

        #region Construction and Initialization
        #endregion

        #region Operations

        public abstract void Init(double size, bool generate_always);

        public abstract void AlignData(Orientation orientation);

        public abstract double GetUnitSize(Orientation orientation);

        public abstract double GetUnitOffset(Orientation orientation);

        /// <summary>
        /// Delivers the offset in pixel which can result if greater units will be shown.
        /// For example: If a time scale generally shows hours, but a month scale only displays days, 
        /// the scale should start at 0:00. Therefore the Unit offset delivers the number of pixel of the resulting hour.
        /// </summary>
        /// <returns></returns>
        public abstract double GetUnitOffset();

        public abstract List<double> GetGridLinePositions();

        #region IDisposable Members

        public void Dispose()
        {
            if (_disposed || _disposing)
                return;

            _disposing = true;

            Dispose(true);
            _disposed = true;
        }

        protected virtual void Dispose(bool disposing)
        {

        }


        #endregion
        #endregion

        #region Event Handlers
        #endregion

        #region Properties

        internal virtual Object Control
        {
            get;
            set;
        }

        #endregion

        #region Dependency Properties
        #endregion

        #region Events
        #endregion

        #region Routed Events
        #endregion

        #region Attributes
        private bool _disposed = false;
        private bool _disposing = false;
        #endregion

        #region Tests
        #endregion

    }
}
