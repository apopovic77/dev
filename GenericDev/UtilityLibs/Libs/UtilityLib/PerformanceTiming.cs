using System;
using System.Runtime.InteropServices;

namespace Logicx.Utilities
{
    /// <summary>
    /// source siehe http://www.aspheute.com/artikel/20011206.htm
    /// </summary>
    [Serializable]
    public class PerformanceTiming
    {
        [DllImport("KERNEL32")]
        public static extern bool QueryPerformanceCounter(ref Int64 nPfCt);

        [DllImport("KERNEL32")]
        public static extern bool QueryPerformanceFrequency(ref Int64 nPfFreq);

        protected Int64 _i64Frequency;
        protected Int64 _i64Start;

        public PerformanceTiming()
        {
            QueryPerformanceFrequency(ref _i64Frequency);
            _i64Start = 0;
        }

        public void Start()
        {
            QueryPerformanceCounter(ref _i64Start);
        }

        /// <summary>
        /// returns the meassurement in milliseconds
        /// </summary>
        /// <returns></returns>
        public double End()
        {
            Int64 i64End = 0;
            QueryPerformanceCounter(ref i64End);
            return (((i64End - _i64Start) / (double)_i64Frequency) ) * 1000;
        }
    }
}