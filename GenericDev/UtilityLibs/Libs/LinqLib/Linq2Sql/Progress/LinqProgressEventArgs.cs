﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LinqLib.Linq2Sql.Progress
{
    public class LinqProgressEventArgs : EventArgs
    {
        public LinqProgressEventArgs(double percent_complete, int total_inserts, int inserts_done,
            int total_updates, int updates_done, int total_deletes, int deletes_done, string last_catch)
        {
            _percent_complete = percent_complete;
            _total_inserts = total_inserts;
            _total_updates = total_updates;
            _total_deletes = total_deletes;
            _inserts_done = inserts_done;
            _updates_done = updates_done;
            _deletes_done = deletes_done;
            _last_catch = last_catch;
        }

        public double PercentComplete
        {
            get { return _percent_complete; }
        }

        public string LastCatch
        {
            get { return _last_catch; }
        }

        public int TotalInserts
        {
            get { return _total_inserts; }
        }

        public int InsertsDone
        {
            get { return _inserts_done; }
        }

        public int TotalUpdates
        {
            get { return _total_updates; }
        }

        public int UpdatesDone
        {
            get { return _updates_done; }
        }

        public int TotalDeletes
        {
            get { return _total_deletes; }
        }

        public int DeletesDone
        {
            get { return _deletes_done; }
        }

        private double _percent_complete = 0;
        private int _total_inserts = 0;
        private int _inserts_done = 0;
        private int _total_deletes = 0;
        private int _deletes_done = 0;
        private int _total_updates = 0;
        private int _updates_done = 0;
        private string _last_catch = "";
    }
}
