﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using PoCShared;

namespace PoCWebApp.App_Start
{
    public class AutoMapperConfig
    {
        private static IMapper mapper;

        public static IMapper Mapper
        {
            get { return mapper; }
            set { mapper = value; }
        }

        public static void Configure()
        {
            var config = new MapperConfiguration(cfg =>
            {

                cfg.CreateMap<PrinterItem, PrinterStatusItem>();
                //cfg.CreateMap<InfoCenterSectionViewModel, Models.InfoCenterSection>();
            });

            mapper = config.CreateMapper();

        }
    }
}