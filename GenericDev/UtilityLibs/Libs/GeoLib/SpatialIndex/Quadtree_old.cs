﻿using System;
using System.Collections.Generic;
using Logicx.Geo.Geometries;
using MathLib;

namespace Logicx.Geo.SpatialIndex
{
    public class Quadtree
    {
        protected class QuadTreeNode
        {
            #region Initialization
            public QuadTreeNode(BoundingBox indexed_area, int depth)
            {
                IndexedArea = indexed_area;
                Depth = depth;
            }

            /// <summary>
            /// -----
            /// |0|1|
            /// -----
            /// |3|2|
            /// -----
            /// </summary>
            private void CreateSubNodes()
            {
                SubNodes = new QuadTreeNode[4];

                int subnode_depth = Depth + 1;

                BoundingBox b0, b1, b2, b3;
                GetSubNodeBoundingBoxes(out b0, out b1, out b2, out b3);
                SubNodes[0] = new QuadTreeNode(b0, subnode_depth);
                SubNodes[1] = new QuadTreeNode(b1, subnode_depth);
                SubNodes[2] = new QuadTreeNode(b2, subnode_depth);
                SubNodes[3] = new QuadTreeNode(b3, subnode_depth);
            }

            private void GetSubNodeBoundingBoxes(out BoundingBox b0, out BoundingBox b1, out BoundingBox b2, out BoundingBox b3)
            {
                double subnode_side_len = IndexedArea.Width / 2;
                Vector2d center_this = IndexedArea.Center;

                b0 = new BoundingBox(new Vector2d(center_this.X - subnode_side_len, center_this.Y - subnode_side_len), center_this);
                b1 = new BoundingBox(new Vector2d(center_this.X, center_this.Y - subnode_side_len), new Vector2d(center_this.X + subnode_side_len, center_this.Y));
                b2 = new BoundingBox(center_this, new Vector2d(center_this.X + subnode_side_len, center_this.Y + subnode_side_len));
                b3 = new BoundingBox(new Vector2d(center_this.X - subnode_side_len, center_this.Y), new Vector2d(center_this.X, center_this.Y + subnode_side_len));
            }

            #endregion

            #region INSERTION
            public void Insert(object geowpfobj, BoundingBox bounding_box, int max_depth)
            {
                if (Depth == max_depth)
                {
                    //rect must be stored in this node
                    if (Objects == null)
                    {
                        Objects = new List<object>();
                        BoundingBoxes = new List<BoundingBox>();
                    }

                    Objects.Add(geowpfobj);
                    BoundingBoxes.Add(bounding_box);
                    return;
                }

                if (SubNodes == null)
                    CreateSubNodes();

                double subnode_side_len = IndexedArea.Width / 2;
                Vector2d this_center = IndexedArea.Center;

                if ( /*RECT SIZE FIT  */ bounding_box.Width > subnode_side_len || bounding_box.Height > subnode_side_len ||
                    /*XLINE INTERSECT*/ (bounding_box.TopLeft.Y < this_center.Y && bounding_box.BottomRight.Y > this_center.Y) ||
                    /*YLINE INTERSECT*/ (bounding_box.TopLeft.X < this_center.X && bounding_box.BottomRight.X > this_center.X)
                  )
                {
                    //rect must be stored in this node
                    if (Objects == null)
                    {
                        Objects = new List<object>();
                        BoundingBoxes = new List<BoundingBox>();
                    }

                    Objects.Add(geowpfobj);
                    BoundingBoxes.Add(bounding_box);
                    return;
                }

                if (bounding_box.TopLeft.X <= this_center.X && bounding_box.BottomRight.X <= this_center.X &&
                    bounding_box.TopLeft.Y <= this_center.Y && bounding_box.BottomRight.Y <= this_center.Y)
                {
                    SubNodes[0].Insert(geowpfobj, bounding_box, max_depth);
                    return;
                }
                if (bounding_box.TopLeft.X >= this_center.X && bounding_box.BottomRight.X >= this_center.X &&
                    bounding_box.TopLeft.Y <= this_center.Y && bounding_box.BottomRight.Y <= this_center.Y)
                {
                    SubNodes[1].Insert(geowpfobj, bounding_box, max_depth);
                    return;
                }
                if (bounding_box.TopLeft.X >= this_center.X && bounding_box.BottomRight.X >= this_center.X &&
                    bounding_box.TopLeft.Y >= this_center.Y && bounding_box.BottomRight.Y >= this_center.Y)
                {
                    SubNodes[2].Insert(geowpfobj, bounding_box, max_depth);
                    return;
                }
                if (bounding_box.TopLeft.X <= this_center.X && bounding_box.BottomRight.X <= this_center.X &&
                    bounding_box.TopLeft.Y >= this_center.Y && bounding_box.BottomRight.Y >= this_center.Y)
                {
                    SubNodes[3].Insert(geowpfobj, bounding_box, max_depth);
                    return;
                }

                throw new Exception("this cannot be check alogrithm");
            }
            #endregion

            #region Query
            public void Query(BoundingBox query_area, List<object> found_objs)
            {
                /////////////
                //check for fully overlapping query area
                //if so, we can return all subnodes immediatelly
                if (query_area.TopLeft.X <= IndexedArea.TopLeft.X && query_area.BottomRight.X >= IndexedArea.BottomRight.X &&
                    query_area.TopLeft.Y <= IndexedArea.TopLeft.Y && query_area.BottomRight.Y >= IndexedArea.BottomRight.Y)
                {
                    GetAllSubObjects(found_objs);
                    return;
                }


                //////////////
                //check this objects for overlapping
                if (Objects != null)
                {
                    for (int i = 0; i < Objects.Count; i++)
                    {
                        object geoobj_i = Objects[i];
                        BoundingBox bounding_box_i = BoundingBoxes[i];
                        if (!(query_area.BottomRight.X <= bounding_box_i.TopLeft.X || query_area.TopLeft.X >= bounding_box_i.BottomRight.X ||
                             query_area.BottomRight.Y <= bounding_box_i.TopLeft.Y || query_area.TopLeft.Y >= bounding_box_i.BottomRight.Y))
                        {
                            found_objs.Add(geoobj_i);
                        }
                    }
                }



                //////////////
                //check subnodes for overlapping objects
                if (SubNodes == null) return;
                BoundingBox b0, b1, b2, b3;
                GetSubNodeBoundingBoxes(out b0, out b1, out b2, out b3);
                if (!(query_area.BottomRight.X <= b0.TopLeft.X || query_area.TopLeft.X >= b0.BottomRight.X ||
                      query_area.BottomRight.Y <= b0.TopLeft.Y || query_area.TopLeft.Y >= b0.BottomRight.Y))
                    SubNodes[0].Query(query_area, found_objs);
                if (!(query_area.BottomRight.X <= b1.TopLeft.X || query_area.TopLeft.X >= b1.BottomRight.X ||
                      query_area.BottomRight.Y <= b1.TopLeft.Y || query_area.TopLeft.Y >= b1.BottomRight.Y))
                    SubNodes[1].Query(query_area, found_objs);
                if (!(query_area.BottomRight.X <= b2.TopLeft.X || query_area.TopLeft.X >= b2.BottomRight.X ||
                      query_area.BottomRight.Y <= b2.TopLeft.Y || query_area.TopLeft.Y >= b2.BottomRight.Y))
                    SubNodes[2].Query(query_area, found_objs);
                if (!(query_area.BottomRight.X <= b3.TopLeft.X || query_area.TopLeft.X >= b3.BottomRight.X ||
                      query_area.BottomRight.Y <= b3.TopLeft.Y || query_area.TopLeft.Y >= b3.BottomRight.Y))
                    SubNodes[3].Query(query_area, found_objs);
            }

            private void GetAllSubObjects(List<object> found_objs)
            {
                if (Objects != null)
                    for (int i = 0; i < Objects.Count; i++)
                    {
                        found_objs.Add(Objects[i]);
                    }

                if (SubNodes == null)
                    return;

                SubNodes[0].GetAllSubObjects(found_objs);
                SubNodes[1].GetAllSubObjects(found_objs);
                SubNodes[2].GetAllSubObjects(found_objs);
                SubNodes[3].GetAllSubObjects(found_objs);
            }

            #endregion

            #region Attribs
            protected int Depth;
            public BoundingBox IndexedArea;
            public List<object> Objects;
            public List<BoundingBox> BoundingBoxes;
            public QuadTreeNode[] SubNodes;
            #endregion
        }

        #region Initialization
        public Quadtree(BoundingBox indexed_area, int max_depth)
        {
            EnsureIndexedAreaIsQuad(ref indexed_area);
            _root = new QuadTreeNode(indexed_area, 1);
            _max_depth = max_depth;
        }

        /// <summary>
        /// ensure indexed area is a quad
        /// </summary>
        private void EnsureIndexedAreaIsQuad(ref BoundingBox indexed_area)
        {
            if (indexed_area.Width == indexed_area.Height)
                return;

            Vector2d center = indexed_area.Center;
            double long_side = indexed_area.Width;
            if (indexed_area.Height > indexed_area.Width)
                long_side = indexed_area.Height;

            double half = long_side / 2;
            indexed_area = new BoundingBox(new Vector2d(center.X - half, center.Y - half), new Vector2d(center.X + half, center.Y + half));
        }
        #endregion

        public void Insert(object obj, BoundingBox bounding_box)
        {
            _root.Insert(obj, bounding_box, _max_depth);
        }

        public List<object> Query(BoundingBox query_area)
        {
            List<object> found_objs = new List<object>();
            _root.Query(query_area, found_objs);
            return found_objs;
        }

        #region Attribs
        protected int _max_depth = 20;
        protected QuadTreeNode _root;
        #endregion

    }
}
