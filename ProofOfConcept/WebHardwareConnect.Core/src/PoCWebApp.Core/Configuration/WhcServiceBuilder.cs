﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;

namespace PoCWebApp.Core.Configuration
{
    public class WhcServiceBuilder : IWhcServiceBuilder
    {
        public IServiceCollection Services { get; }

        public WhcServiceBuilder(IServiceCollection services)
        {
            //services.CheckArgumentNull(nameof(services));

            Services = services;
        }
    }
}
