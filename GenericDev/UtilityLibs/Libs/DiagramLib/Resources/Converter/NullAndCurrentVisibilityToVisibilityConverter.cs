﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Data;

namespace Logicx.DiagramLib.Resources.Converter
{
    public class NullAndCurrentVisibilityToVisibilityConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            bool bVisible = false;

            if(values != null && values.Length == 2)
            {
                if(values[0] is Visibility && ((Visibility)values[0]) == Visibility.Visible)
                {
                    if (values[1] != null)
                        bVisible = true;
                }
            }

            if (bVisible)
                return Visibility.Visible;

            return Visibility.Collapsed;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }
}
