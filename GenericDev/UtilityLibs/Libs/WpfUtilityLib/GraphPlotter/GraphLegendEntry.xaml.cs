﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Logicx.WpfUtility.GraphPlotter;

namespace Logicx.WpfUtility.GraphPlotter
{
    /// <summary>
    /// Interaktionslogik für GraphLegendEntry.xaml
    /// </summary>
    public partial class GraphLegendEntry : UserControl
    {
        public GraphLegendEntry(GraphPlot parent, GraphLegend graphparent)
        {
            InitializeComponent();
            _parent = parent;
            _graphlegend_parent = graphparent;
            _isFrozen = false;

            _legend_bg_brush = Brushes.Transparent;
            _legend_active_bg_brush = new SolidColorBrush(Color.FromArgb(50,255,255,255));

        }        
 
        private void myMainGrid_MouseEnter(object sender, MouseEventArgs e)
        {
            if (!_graphlegend_parent.IsFrozen)
            {
                _parent.PutLineToFront(myEntryName.Text);
                _graphlegend_parent.ActivateEntry(myEntryName.Text);
            }
        }

        public void ActivateEntry()
        {
            if(!_graphlegend_parent.IsFrozen)
                myMainGrid.Background = _legend_active_bg_brush;
        }

        public void DeactivateEntry()
        {
            if(!_graphlegend_parent.IsFrozen)
                myMainGrid.Background = _legend_bg_brush;
        }        
        
        private void myMainGrid_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if(_isFrozen)
            {
                _graphlegend_parent.IsFrozen = false;
                _isFrozen = false;
                _parent.IsGraphFrozen = false;
                myMainGrid.Background = _legend_active_bg_brush;
                lockimage.Opacity = 0;
                myBorder.BorderBrush = Brushes.Transparent;
            }
            else
            {
                if(!_graphlegend_parent.IsFrozen)
                {
                    _graphlegend_parent.IsFrozen = true;
                    _isFrozen = true;
                    _parent.IsGraphFrozen = true;
                    //myMainGrid.Background = Brushes.DarkOrange;
                    lockimage.Opacity = 1;
                    myBorder.BorderBrush = Brushes.Transparent;
                }
            }
        }

        public Brush GraphFillBrush
        {
            set
            {
                myFillColor.Fill = value;
                if(value == null)
                {
                    legend_stack.Children.Remove(myFillColor);
                    myEntryColor.Height = 5;
                }
            }
        }

        #region Attributes
        private bool _isFrozen;
        private GraphPlot _parent;
        private GraphLegend _graphlegend_parent;
        private Brush _legend_bg_brush;
        private Brush _legend_active_bg_brush;
        #endregion
    }
}