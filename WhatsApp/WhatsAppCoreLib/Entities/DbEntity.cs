﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Awanto.WhatsAppCore.Entities
{
    public class DbEntity
    {
        public int Id { get; set; }

        public string ErstelltVon { get; set; }
        public DateTime ErstelltAm { get; set; }

        public string GeaendertVon { get; set; }
        public DateTime GeaendertAm { get; set; }

        public virtual void OnBeforeInsert()
        {
            if (string.IsNullOrEmpty(ErstelltVon))
                ErstelltVon = System.Diagnostics.Process.GetCurrentProcess().ProcessName;
            ErstelltAm = DateTime.Now;
        }

        public virtual void OnBeforeUpdate()
        {
            if (string.IsNullOrEmpty(ErstelltVon))
                ErstelltVon = System.Diagnostics.Process.GetCurrentProcess().ProcessName;
            GeaendertAm = DateTime.Now;
        }
    }
}
