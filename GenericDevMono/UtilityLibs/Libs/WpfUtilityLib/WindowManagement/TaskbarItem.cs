﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;
using Logicx.WpfUtility.CustomControls.OverlayManager;

namespace Logicx.WpfUtility.WindowManagement
{
    public class TaskbarItem : TaskbarItemBase<BaseFrame, SubMenuPanel, WindowTaskBar, WindowTitleBar, OverlayManager, SubMenuPanelMenuItem>
    {
        public TaskbarItem(FrameworkElement framework_element, string title) : base(framework_element, title)
        {
        }
    }
}
