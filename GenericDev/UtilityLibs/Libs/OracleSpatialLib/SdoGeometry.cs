using System;
using CoreLab.Oracle;
using MathLib;

namespace Logicx.OracleSpatial
{
    [Serializable]
    public struct SdoElemInfo
    {
        public int SdoStartingOffset;
        public int SdoEtype;
        public int SdoInterpretation;
    }

  /// <summary>
  /// Summary description for SdoGeometry.
  /// </summary>
  [Serializable]
  public class SdoGeometry {

    public int SdoGType;
    public float SdoSrid;
    public SdoPoint3D SdoPoint;
    public SdoElemInfo[] SdoElemInfos;
    public Vector2f[] SdoOrdinates;

    private static Vector2f[] emptyfloatArray = new Vector2f[0];

    public SdoGeometry() {

      this.SdoGType = 0;
      this.SdoSrid = 0;
      this.SdoOrdinates = emptyfloatArray;
      this.SdoPoint = SdoPoint3D.Empty;
    }

    public SdoGeometry(SdoGeometry obj) : this() {

      if (this != obj && obj != null) {
        this.SdoGType = obj.SdoGType;
        this.SdoSrid = obj.SdoSrid;
        this.SdoElemInfos = new SdoElemInfo[obj.SdoElemInfos.Length];
        obj.SdoElemInfos.CopyTo(this.SdoElemInfos, 0);
        this.SdoOrdinates = new Vector2f[obj.SdoOrdinates.Length];
        obj.SdoOrdinates.CopyTo(this.SdoOrdinates, 0);
        this.SdoPoint = new SdoPoint3D(obj.SdoPoint.X, obj.SdoPoint.Y, obj.SdoPoint.Z);
      }
    }

    public SdoGeometry(int gType, float srid, SdoPoint3D point, SdoElemInfo[] elemInfos, Vector2f[] ordinate)
    {

      this.SdoGType = gType;
      this.SdoSrid = srid;
      this.SdoPoint = point;
      this.SdoElemInfos = elemInfos;
      this.SdoOrdinates = ordinate;
    }

    public int GetGType() {

      return (int)(this.SdoGType % 100);
    }

    public int GetDims() {

      return (int)(this.SdoGType / 1000);
    }

    public int GetLrsDim() {

      int lrsDim;
      lrsDim =(int)(this.SdoGType / 100);
      lrsDim = (int)(lrsDim % 10);
      return lrsDim;
    }
  }
}
