﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MathLib
{
    public class LinearInterpolation
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="stuetzstelle_1">x value must be smaller than x value of stuetzstelle 2</param>
        /// <param name="stuetzstelle_2">x value must be greater than x value of stuetzstelle 1</param>
        public LinearInterpolation(Vector2f stuetzstelle_1, Vector2f stuetzstelle_2)
        {
            _stuetzstelle_1 = stuetzstelle_1;
            _stuetzstelle_2 = stuetzstelle_2;

            float k = (_stuetzstelle_2.Y - _stuetzstelle_1.Y)/(_stuetzstelle_2.X - _stuetzstelle_1.X);
            float d = _stuetzstelle_1.Y - k * _stuetzstelle_1.X;
            _lin_eq = new LinearEquation(k, d);
        }

        public float GetFx(float x)
        {
            return _lin_eq.GetFx(x);
        }

        protected Vector2f _stuetzstelle_1;
        protected Vector2f _stuetzstelle_2;
        protected LinearEquation _lin_eq;
    }
}
