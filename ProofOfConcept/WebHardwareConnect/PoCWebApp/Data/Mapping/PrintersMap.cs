﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;
using PoCShared;

namespace PoCWebApp.Data.Mapping
{
    public class PrintersMap : EntityTypeConfiguration<PrinterItem>
    {
        public PrintersMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            this.Property(t => t.Name)
                .HasMaxLength(200);

            this.Property(t => t.PrintServer)
                .HasMaxLength(400);

            this.Property(t => t.Type)
                 .HasMaxLength(200);


            // Properties
            // Table & Column Mappings
            this.ToTable("Printers");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.PrintServer).HasColumnName("PrintServer");
            this.Property(t => t.Type).HasColumnName("Type");
            

            // Relationships
        }
    }
}