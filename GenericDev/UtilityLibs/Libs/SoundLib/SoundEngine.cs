﻿using System;
using System.Collections;
using Microsoft.DirectX.DirectSound;

namespace Logicx.Sound
{
    public class SoundEngine
    {
        public SoundEngine(IntPtr parent_window)
        {
            try
            {
                _sounds = new Hashtable();

                _device = new Device();
                _description = new BufferDescription();
                _description.ControlEffects = false;

                _device.SetCooperativeLevel(parent_window, CooperativeLevel.Normal);
            }
            catch
            {
                _with_sounds = false;
            }
        }


        public void AddSound(string sound_key, string path_to_soundfile)
        {
            //create array of buffers for sound explosion
            SecondaryBuffer[] sound_explosion = new SecondaryBuffer[3];
            sound_explosion[0] = new SecondaryBuffer(path_to_soundfile, _description, _device);
            for (int i = 1; i < sound_explosion.Length; i++)
                sound_explosion[i] = sound_explosion[0].Clone(_device);
            _sounds.Add(sound_key, sound_explosion);
        }


        public void PlaySound(string sound_name)
        {
            if (!_with_sounds) return;

            SecondaryBuffer[] sound = (SecondaryBuffer[])_sounds[sound_name];
            if (sound == null) return;

            for (int i = 0; i < sound.Length; i++)
            {
                SecondaryBuffer e = sound[i];
                if (!e.Status.Playing)
                {
                    e.Play(0, BufferPlayFlags.Default);
                    return;
                }
            }
        }

        // private field to hold the Device for later use
        private Device _device;
        private Hashtable _sounds;
        private bool _with_sounds = true;
        private BufferDescription _description;
    }
}
