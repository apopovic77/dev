﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BankLib.Data;

namespace BankOverviewApp
{
    /// <summary>
    /// Interaktionslogik für KontenOverview.xaml
    /// </summary>
    public partial class KontenOverview : UserControl
    {
        public KontenOverview()
        {
            InitializeComponent();

            KontoStandHeadline kto;
            float sum = 0;
            foreach (Konto k in Konten.AlleKonten)
            {
                kto = new KontoStandHeadline();
                kto.OwnerTb.Text = k.Owner;
                float stand = Konten.GetGiroKontoStand(k.Owner);
                sum += stand;
                kto.BetragTb.Tag = stand;
                kto.BetragTb.Text = stand.ToString("C2", CultureInfo.CreateSpecificCulture("de-DE"));
                Stack.Children.Add(kto);
            }

            kto = new KontoStandHeadline();
            kto.OwnerTb.Text = "Gesamt";
            kto.BetragTb.Tag = sum;
            kto.BetragTb.Text = sum.ToString("C2", CultureInfo.CreateSpecificCulture("de-DE"));
            //kto.Background = Brushes.Gray;
            Stack.Children.Add(kto);
        }

        public KontenOverview(DateTime monyear)
        {
            InitializeComponent();

            KontoStandHeadline kto;
            float sum = 0;
            foreach (Konto k in Konten.AlleKonten)
            {
                kto = new KontoStandHeadline();
                kto.OwnerTb.Text = k.Owner;
                float stand = Konten.GetGiroKontoStandMonat(k.Owner, monyear.Month, monyear.Year);
                sum += stand;
                kto.BetragTb.Tag = stand;
                kto.BetragTb.Text = stand.ToString("C2", CultureInfo.CreateSpecificCulture("de-DE"));
                Stack.Children.Add(kto);
            }

            kto = new KontoStandHeadline();
            kto.OwnerTb.Text = "Sum";
            kto.BetragTb.Tag = sum;
            kto.BetragTb.Text = sum.ToString("C2", CultureInfo.CreateSpecificCulture("de-DE"));
            //kto.Background = Brushes.Gray;
            Stack.Children.Add(kto);
        }
    }
}
