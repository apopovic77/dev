﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Data;

namespace Logicx.DiagramLib.Resources.Converter
{
    public class YAxisBottomPaddingConverter : IMultiValueConverter
    {
        #region IMultiValueConverter Members

        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            Thickness current_padding = new Thickness(0);

            if (parameter != null)
            {
                current_padding = (Thickness)parameter;
            }

            if (values != null && values.Length == 2)
            {
                Visibility y_scale_visibility = (Visibility)values[0];


                if (y_scale_visibility == Visibility.Visible)
                {

                    if (values[1] != DependencyProperty.UnsetValue)
                    {
                        double current_height = (double)values[1];

                        if (!double.IsNaN(current_height) && !double.IsInfinity(current_height))
                        {
                            return new Thickness(current_padding.Left, current_padding.Top, current_padding.Right, current_height);
                        }
                    }
                }
            }

            return current_padding;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }

        #endregion
    }
}
