﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PoCShared
{
    [Serializable]
    public class PrinterStatusItem
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string PrintServer { get; set; }
        public PrinterStatus Status { get; set; }
        public string StatusText { get; set; }
    }
}
