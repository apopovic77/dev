﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace OpenCvDocScannerTestApp
{
    /// <summary>
    /// Interaktionslogik für ConfigSlider.xaml
    /// </summary>
    public partial class ConfigSlider : UserControl
    {
        public ConfigSlider()
        {
            InitializeComponent();
        }

        public ConfigSlider(string title)
        {
            InitializeComponent();
            Tag = title;
            Expander.Header = "Konfig "+title;
        }

        private void Slider_OnValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            ValueChanged?.Invoke(this, new EventArgs());
        }

        public float Value
        {
            get { return Convert.ToSingle(slider.Value); }
            set { slider.Value = value; }
        }


        public event EventHandler ValueChanged;
    }
}
