using System;
using System.ServiceModel;
using NamedPipe.Communication;

namespace Logicx.WpfUtility.WindowParallelisation.WCFComm
{
    public class WindowCommReceiver :  IDisposable
    {
        // Use this constructor to create
        // a pipe with a unique name.
        public WindowCommReceiver(IPipeService pipe_service, string pipe_name)
        {
            _pipe_name = pipe_name;
            _pipe_service = pipe_service;
        }

        // If the service host can be started,
        // true will be returned. If false is returned
        // another process owns the pipe.
        public bool ServiceOn()
        {
            return ( _operational = HostThisService());
        }

        public void ServiceOff()
        {
            if (_host != null)
                if (_host.State != CommunicationState.Closed)
                    _host.Close();


            _operational = false;
        }

        // The thread which will listen for communications
        // from the other side of the pipe.
        private bool HostThisService()
        {
            try
            {
                _host = new ServiceHost(_pipe_service, new Uri(URI));

                NetNamedPipeBinding pipe_binding = new NetNamedPipeBinding();
                pipe_binding.ReceiveTimeout = pipe_binding.SendTimeout = new TimeSpan(7, 0, 0, 0);

                // Usage BasicHttpBinding can be used if this is 
                // not going to be on the local machine.
                _host.AddServiceEndpoint( typeof( IPipeService ),
                                          pipe_binding,
                                          _pipe_name );
                _host.Open();

                _operational = true;
            }
            catch (Exception ex)
            {
                error = ex;
                _operational = false;
            }

            return _operational;
        }

        
        public void Dispose()
        {
            this.ServiceOff();
        }



        public static string URI = "net.pipe://localhost/Pipe";

        private IPipeService _pipe_service;
        private bool _operational = false;
        private ServiceHost _host = null;
        private string _pipe_name  = string.Empty;

        public Exception error = null;

    }
}
