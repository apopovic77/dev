﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Logicx.WpfUtility.WindowManagement;
using Logicx.WpfUtility.WpfHelpers;

namespace Logicx.WpfUtility.CustomControls.OverlayManager
{
    /// <summary>
    /// Interaction logic for OverlayHelpAndErrorViewer.xaml
    /// </summary>
    public partial class OverlayHelpAndErrorViewer : UserControl, IDisposable
    {
        #region nested types
        private enum ErrorVisibilityMode
        {
            None,
            MiniErrorVisible,
            FullErrorVisible
        }
        #endregion

        #region construction and initialization
        /// <summary>
        /// Static constructor
        /// </summary>
        static OverlayHelpAndErrorViewer()
        {
            FrameworkPropertyMetadata metadataShowHelp = new FrameworkPropertyMetadata(false);
            ShowHelpProperty = DependencyProperty.Register("ShowHelp",
                                                            typeof(bool), typeof(OverlayHelpAndErrorViewer), metadataShowHelp);

            FrameworkPropertyMetadata metadataShowError = new FrameworkPropertyMetadata(false);
            ShowErrorProperty = DependencyProperty.Register("ShowError",
                                                            typeof(bool), typeof(OverlayHelpAndErrorViewer), metadataShowError);

            FrameworkPropertyMetadata metadataErrorCount = new FrameworkPropertyMetadata(0);
            ErrorCountPropertyKey = DependencyProperty.RegisterReadOnly("ErrorCount", 
                                                            typeof(int), typeof(OverlayHelpAndErrorViewer), metadataErrorCount);
            ErrorCountProperty = ErrorCountPropertyKey.DependencyProperty;

            FrameworkPropertyMetadata metadataOverlayBackgroundBrush = new FrameworkPropertyMetadata(new SolidColorBrush(Color.FromArgb(100, 50, 50, 50)));
            OverlayBackgroundBrushProperty = DependencyProperty.Register("OverlayBackgroundBrush",
                                                            typeof(SolidColorBrush), typeof(OverlayHelpAndErrorViewer), metadataOverlayBackgroundBrush);

            FrameworkPropertyMetadata metadataShowChangeInfo = new FrameworkPropertyMetadata(false);
            ShowChangeInfoProperty = DependencyProperty.Register("ShowChangeInfo",
                                                            typeof(bool), typeof(OverlayHelpAndErrorViewer), metadataShowChangeInfo);

            FrameworkPropertyMetadata metadataShowSaveSuccess = new FrameworkPropertyMetadata(false);
            ShowSaveSuccessProperty = DependencyProperty.Register("ShowSaveSuccess",
                                                            typeof(bool), typeof(OverlayHelpAndErrorViewer), metadataShowSaveSuccess);

            FrameworkPropertyMetadata metadataChangeInfoAdornerVisible = new FrameworkPropertyMetadata(true);
            ChangeInfoAdornerVisibleProperty = DependencyProperty.Register("ChangeInfoAdornerVisible",
                                                            typeof(bool), typeof(OverlayHelpAndErrorViewer), metadataChangeInfoAdornerVisible);
            
        }

        public OverlayHelpAndErrorViewer()
        {
            InitializeComponent();
            InitAnimations();

            change_info_button.Tag = true;

            //DependencyPropertyDescriptor descr = DependencyPropertyDescriptor.FromProperty(ShowHelpProperty, this.GetType());
            //if (descr != null)
            //    descr.AddValueChanged(this, ShowHelp_Changed);

            //descr = DependencyPropertyDescriptor.FromProperty(ShowErrorProperty, this.GetType());
            //if (descr != null)
            //    descr.AddValueChanged(this, ShowError_Changed);

            //descr = DependencyPropertyDescriptor.FromProperty(ShowChangeInfoProperty, this.GetType());
            //if (descr != null)
            //    descr.AddValueChanged(this, ShowChangeInfo_Changed);

            //descr = DependencyPropertyDescriptor.FromProperty(ShowSaveSuccessProperty, this.GetType());
            //if (descr != null)
            //    descr.AddValueChanged(this, ShowSaveSuccess_Changed);

            //descr = DependencyPropertyDescriptor.FromProperty(ChangeInfoAdornerVisibleProperty, this.GetType());
            //if (descr != null)
            //    descr.AddValueChanged(this, ChangeInfoAdornerVisible_Changed);

            control_canvas.SizeChanged += new SizeChangedEventHandler(control_canvas_SizeChanged);
            //error_overlay_control.SizeChanged += new SizeChangedEventHandler(error_overlay_control_SizeChanged);

            help_overlay_control.Visibility = Visibility.Hidden;
            error_overlay_control.Visibility = Visibility.Hidden;
            mini_error_counter.Visibility = Visibility.Hidden;
            change_info_button.Visibility = Visibility.Hidden;
            save_success_info.Visibility = Visibility.Hidden;

            change_info_button.Width = CHANGE_INFO_WIDTH;
            save_success_info.Width = 0;

            error_overlay_control.SetHelpAndErrorViewer(this);
        }

        #endregion

        #region properties
        public OverlayErrorViewer ErrorViewer
        {
            get { return error_overlay_control; }
        }
        /// <summary>
        /// Liefert die Instanz des OverlayManagers für dieses content control
        /// </summary>
        /// <remarks>Die Eigenschaft kann auch einen NULL Wert liefern, wenn das Control nicht im OverlayManager angezeigt wird.</remarks>
        public IOverlayManagerBase OverlayManager
        {
            get
            {
                if (_overlay_manager != null)
                    return _overlay_manager;

                DependencyObject o = UIHelper.TryFindParentGeneric(this, typeof(OverlayManagerBase<BaseFrame, SubMenuPanel, WindowTaskBar, WindowTitleBar, OverlayManager, SubMenuPanelMenuItem>));
                if (o != null)
                    _overlay_manager = o as IOverlayManagerBase;

                return _overlay_manager;

            }
        }

        /// <summary>
        /// Liefert die Instanz des Host-Dialoges.
        /// </summary>
        /// <remarks>Diese Eigenschaft kann auch einen null Wert liefern, wenn das OverlayControl momentan initialisiert,
        /// aber noch nicht im VisualTree vorhanden ist.</remarks>
        public OverlayDialog ParentDialog
        {
            get
            {
                return UIHelper.TryFindParent<OverlayDialog>(this);
            }
        }

        /// <summary>
        /// Gets/sets the flag if the help is visible or not
        /// </summary>
        public bool ShowHelp
        {
            get
            {
                return (bool)GetValue(ShowHelpProperty);
            }
            set
            {
                if ((bool)GetValue(ShowHelpProperty) != value)
                {
                    SetValue(ShowHelpProperty, value);
                }
            }
        }
        /// <summary>
        /// Gets/sets the flag if the error info should be shown
        /// </summary>
        public bool ShowError
        {
            get
            {
                return (bool)GetValue(ShowErrorProperty);
            }
            set
            {
                if ((bool)GetValue(ShowErrorProperty) != value)
                {
                    SetValue(ShowErrorProperty, value);
                }
            }
        }

        public bool IsFullErrorInfoVisible
        {
            get
            {
                return _error_visibility == ErrorVisibilityMode.FullErrorVisible;
            }
        }

        /// <summary>
        /// Gets/sets the flag if the help is visible or not
        /// </summary>
        public bool ShowChangeInfo
        {
            get
            {
                return (bool)GetValue(ShowChangeInfoProperty);
            }
            set
            {
                if ((bool)GetValue(ShowChangeInfoProperty) != value)
                {
                    SetValue(ShowChangeInfoProperty, value);
                }
            }
        }

        /// <summary>
        /// Gets/sets the flag if the help is visible or not
        /// </summary>
        public bool ShowSaveSuccess
        {
            get
            {
                return (bool)GetValue(ShowSaveSuccessProperty);
            }
            set
            {
                if ((bool)GetValue(ShowSaveSuccessProperty) != value)
                {
                    SetValue(ShowSaveSuccessProperty, value);
                }
            }
        }

        

        /// <summary>
        /// Gets/sets the flag if the help is visible or not
        /// </summary>
        public bool ChangeInfoAdornerVisible
        {
            get
            {
                return (bool)GetValue(ChangeInfoAdornerVisibleProperty);
            }
            set
            {
                if ((bool)GetValue(ChangeInfoAdornerVisibleProperty) != value)
                {
                    SetValue(ChangeInfoAdornerVisibleProperty, value);
                }
            }
        }

        /// <summary>
        /// gets the error count
        /// </summary>
        public int ErrorCount
        {
            get { return (int) GetValue(ErrorCountProperty); }
        }
        /// <summary>
        /// Returns true if the error or help is visible
        /// </summary>
        public bool IsHelpOrErrorVisible
        {
            get
            {
                return ShowHelp || ShowError || ShowChangeInfo || ShowSaveSuccess;
            }
        }

        /// <summary>
        /// Dependency Eigenschaft welche den Hintergrundbrush für den OverlayCanvas setzt
        /// </summary>
        public SolidColorBrush OverlayBackgroundBrush
        {
            get
            {
                return (SolidColorBrush)GetValue(OverlayBackgroundBrushProperty);
            }
            set
            {
                SetValue(OverlayBackgroundBrushProperty, value);
            }
        }

        /// <summary>
        /// Eigenschaft welche angibt ob dieser Dialog gerade aktiv ist
        /// </summary>
        public bool IsActive
        {
            get
            {
                return _isActive;
            }
            set
            {
                if (_isActive != value)
                {
                    if (value)
                    {
                        overlay_mini_error_canvas.Background = null;
                        overlay_help_canvas.Background = null;
                        overlay_error_canvas.Background = null;
                        overlay_change_info_button_canvas.Background = null;
                        overlay_save_success_canvas.Background = null;
                    }
                    else
                    {
                        overlay_mini_error_canvas.Background = Brushes.Transparent;
                        overlay_help_canvas.Background = Brushes.Transparent;
                        overlay_error_canvas.Background = Brushes.Transparent;
                        overlay_change_info_button_canvas.Background = Brushes.Transparent;
                        overlay_save_success_canvas.Background = Brushes.Transparent;
                    }
                }

                _isActive = value;
            }
        }

        public bool ShowFullErrorInfoInsteadOfMini
        {
            get;
            set;
        }

        private double ChangeInfoHeight
        {
            get
            {
                double changeinfo_height = 0;

                if (ShowSaveSuccess || ShowChangeInfo)
                {
                    changeinfo_height = change_info_button.ActualHeight + CONTROL_MARGIN;
                }

                return changeinfo_height;
            }
        }

        public double ErrorViewerMaxWidth
        {
            get
            {
                return error_overlay_control.MaxWidth;
            }
        }
        #endregion

        #region operations

        private void ShowHelpErrorViewer()
        {
            Visibility = Visibility.Visible;
        }

        private void HideHelpErrorViewer()
        {
            Visibility = Visibility.Collapsed;
        }

        private void InitAnimations()
        {
            //create fade in animator
            // help viewer
            _fade_in_help_viewer = new DoubleAnimation();
            _fade_in_help_viewer.From = 0;
            _fade_in_help_viewer.To = 0;
            _fade_in_help_viewer.DecelerationRatio = 1;
            _fade_in_help_viewer.Duration = new Duration(TimeSpan.FromMilliseconds(_fadein_anim_duration_full_in_msec));
            _fade_in_help_viewer.FillBehavior = FillBehavior.HoldEnd;
            _fade_in_help_viewer.Completed += Fade_in_help_Completed;
            // mini error viewer
            _fade_in_mini_error_info = new DoubleAnimation();
            _fade_in_mini_error_info.From = 0;
            _fade_in_mini_error_info.To = 0;
            _fade_in_mini_error_info.DecelerationRatio = 1;
            _fade_in_mini_error_info.Duration = new Duration(TimeSpan.FromMilliseconds(_fadein_anim_duration_full_in_msec));
            _fade_in_mini_error_info.FillBehavior = FillBehavior.HoldEnd;
            _fade_in_mini_error_info.Completed += Fade_in_mini_error_info_Completed;
            _fade_in_mini_error_info1 = new DoubleAnimation();
            _fade_in_mini_error_info1.From = 0;
            _fade_in_mini_error_info1.To = 0;
            _fade_in_mini_error_info1.DecelerationRatio = 1;
            _fade_in_mini_error_info1.Duration = new Duration(TimeSpan.FromMilliseconds(_fadein_anim_duration_full_in_msec));
            _fade_in_mini_error_info1.FillBehavior = FillBehavior.HoldEnd;
            _fade_in_mini_error_info1.Completed += Fade_in_mini_error_info1_Completed;
            // change info viewer
            _fade_in_change_info = new DoubleAnimation();
            _fade_in_change_info.From = 0;
            _fade_in_change_info.To = 0;
            _fade_in_change_info.DecelerationRatio = 1;
            _fade_in_change_info.Duration = new Duration(TimeSpan.FromMilliseconds(_fadein_anim_duration_full_in_msec));
            _fade_in_change_info.FillBehavior = FillBehavior.HoldEnd;
            _fade_in_change_info.Completed += Fade_in_change_info_Completed;
            _fade_in_change_info1 = new DoubleAnimation();
            _fade_in_change_info1.From = 0;
            _fade_in_change_info1.To = 0;
            _fade_in_change_info1.DecelerationRatio = 1;
            _fade_in_change_info1.Duration = new Duration(TimeSpan.FromMilliseconds(_fadein_anim_duration_full_in_msec));
            _fade_in_change_info1.FillBehavior = FillBehavior.HoldEnd;
            _fade_in_change_info1.Completed += Fade_in_change_info1_Completed;
            // save success viewer
            _fade_in_save_success_info = new DoubleAnimation();
            _fade_in_save_success_info.From = 0;
            _fade_in_save_success_info.To = 0;
            _fade_in_save_success_info.DecelerationRatio = 1;
            _fade_in_save_success_info.Duration = new Duration(TimeSpan.FromMilliseconds(_fadein_anim_duration_full_in_msec));
            _fade_in_save_success_info.FillBehavior = FillBehavior.HoldEnd;
            _fade_in_save_success_info.Completed += Fade_in_save_success_info_Completed;
            // full error viewer
            _fade_in_error_viewer = new DoubleAnimation();
            _fade_in_error_viewer.From = 0;
            _fade_in_error_viewer.To = 0;
            _fade_in_error_viewer.DecelerationRatio = 1;
            _fade_in_error_viewer.Duration = new Duration(TimeSpan.FromMilliseconds(_fadein_anim_duration_full_in_msec));
            _fade_in_error_viewer.FillBehavior = FillBehavior.HoldEnd;
            _fade_in_error_viewer.Completed += Fade_in_full_error_info_Completed;
            // full error viewer property 2
            _fade_in_error_viewer2 = new DoubleAnimation();
            _fade_in_error_viewer2.From = 0;
            _fade_in_error_viewer2.To = 0;
            _fade_in_error_viewer2.DecelerationRatio = 1;
            _fade_in_error_viewer2.Duration = new Duration(TimeSpan.FromMilliseconds(_fadein_anim_duration_full_in_msec));
            _fade_in_error_viewer2.FillBehavior = FillBehavior.HoldEnd;
            _fade_in_error_viewer2.Completed += Fade_in_full_error_info2_Completed;
            // full error viewer property 3
            _fade_in_error_viewer3 = new DoubleAnimation();
            _fade_in_error_viewer3.From = 0;
            _fade_in_error_viewer3.To = 0;
            _fade_in_error_viewer3.DecelerationRatio = 1;
            _fade_in_error_viewer3.Duration = new Duration(TimeSpan.FromMilliseconds(_fadein_anim_duration_full_in_msec));
            _fade_in_error_viewer3.FillBehavior = FillBehavior.HoldEnd;
            _fade_in_error_viewer3.Completed += Fade_in_full_error_info3_Completed;

            //create fade out animator
            // help viewer
            _fade_out_help_viewer = new DoubleAnimation();
            _fade_out_help_viewer.From = 0;
            _fade_out_help_viewer.To = 0;
            _fade_out_help_viewer.Duration = new Duration(TimeSpan.FromMilliseconds(_fadout_anim_duration_full_in_msec));
            _fade_out_help_viewer.FillBehavior = FillBehavior.HoldEnd;
            _fade_out_help_viewer.Completed += Fade_out_help_Completed;
            // mini error viewer
            _fade_out_mini_error_info = new DoubleAnimation();
            _fade_out_mini_error_info.From = 0;
            _fade_out_mini_error_info.To = 0;
            _fade_out_mini_error_info.Duration = new Duration(TimeSpan.FromMilliseconds(_fadout_anim_duration_full_in_msec));
            _fade_out_mini_error_info.FillBehavior = FillBehavior.HoldEnd;
            _fade_out_mini_error_info.Completed += Fade_out_mini_error_info_Completed;
            _fade_out_mini_error_info1 = new DoubleAnimation();
            _fade_out_mini_error_info1.From = 0;
            _fade_out_mini_error_info1.To = 0;
            _fade_out_mini_error_info1.Duration = new Duration(TimeSpan.FromMilliseconds(_fadout_anim_duration_full_in_msec));
            _fade_out_mini_error_info1.FillBehavior = FillBehavior.HoldEnd;
            _fade_out_mini_error_info1.Completed += Fade_out_mini_error_info1_Completed;
            // change info viewer
            _fade_out_change_info = new DoubleAnimation();
            _fade_out_change_info.From = 0;
            _fade_out_change_info.To = 0;
            _fade_out_change_info.Duration = new Duration(TimeSpan.FromMilliseconds(_fadout_anim_duration_full_in_msec));
            _fade_out_change_info.FillBehavior = FillBehavior.HoldEnd;
            _fade_out_change_info.Completed += Fade_out_change_info_Completed;
            _fade_out_change_info1 = new DoubleAnimation();
            _fade_out_change_info1.From = 0;
            _fade_out_change_info1.To = 0;
            _fade_out_change_info1.Duration = new Duration(TimeSpan.FromMilliseconds(_fadout_anim_duration_full_in_msec));
            _fade_out_change_info1.FillBehavior = FillBehavior.HoldEnd;
            _fade_out_change_info1.Completed += Fade_out_change_info1_Completed;
            // save success viewer
            _fade_out_save_success_info = new DoubleAnimation();
            _fade_out_save_success_info.From = 0;
            _fade_out_save_success_info.To = 0;
            _fade_out_save_success_info.Duration = new Duration(TimeSpan.FromMilliseconds(_fadout_anim_duration_full_in_msec));
            _fade_out_save_success_info.FillBehavior = FillBehavior.HoldEnd;
            _fade_out_save_success_info.Completed += Fade_out_save_success_info_Completed;
            // full error viewer
            _fade_out_error_viewer = new DoubleAnimation();
            _fade_out_error_viewer.From = 0;
            _fade_out_error_viewer.To = 0;
            _fade_out_error_viewer.Duration = new Duration(TimeSpan.FromMilliseconds(_fadout_anim_duration_full_in_msec));
            _fade_out_error_viewer.FillBehavior = FillBehavior.HoldEnd;
            _fade_out_error_viewer.Completed += Fade_out_full_error_info_Completed;
            // full error viewer property 2
            _fade_out_error_viewer2 = new DoubleAnimation();
            _fade_out_error_viewer2.From = 0;
            _fade_out_error_viewer2.To = 0;
            _fade_out_error_viewer2.Duration = new Duration(TimeSpan.FromMilliseconds(_fadout_anim_duration_full_in_msec));
            _fade_out_error_viewer2.FillBehavior = FillBehavior.HoldEnd;
            _fade_out_error_viewer2.Completed += Fade_out_full_error_info2_Completed;
            // full error viewer property 3
            _fade_out_error_viewer3 = new DoubleAnimation();
            _fade_out_error_viewer3.From = 0;
            _fade_out_error_viewer3.To = 0;
            _fade_out_error_viewer3.Duration = new Duration(TimeSpan.FromMilliseconds(_fadout_anim_duration_full_in_msec));
            _fade_out_error_viewer3.FillBehavior = FillBehavior.HoldEnd;
            _fade_out_error_viewer3.Completed += Fade_out_full_error_info3_Completed;



            //create shrink animator
            // help viewer shrink height
            _shrink_grow_height_help_viewer = new DoubleAnimation();
            _shrink_grow_height_help_viewer.From = 0;
            _shrink_grow_height_help_viewer.To = 0;
            _shrink_grow_height_help_viewer.DecelerationRatio = 1;
            _shrink_grow_height_help_viewer.Duration = new Duration(TimeSpan.FromMilliseconds(_fadein_anim_duration_full_in_msec));
            _shrink_grow_height_help_viewer.FillBehavior = FillBehavior.HoldEnd;
            _shrink_grow_height_help_viewer.Completed += ShrinkGrow_help_Completed;
            // error viewer shrink height
            _shrink_grow_height_error_viewer = new DoubleAnimation();
            _shrink_grow_height_error_viewer.From = 0;
            _shrink_grow_height_error_viewer.To = 0;
            _shrink_grow_height_error_viewer.DecelerationRatio = 1;
            _shrink_grow_height_error_viewer.Duration = new Duration(TimeSpan.FromMilliseconds(_fadein_anim_duration_full_in_msec));
            _shrink_grow_height_error_viewer.FillBehavior = FillBehavior.HoldEnd;
            _shrink_grow_height_error_viewer.Completed += ShrinkGrow_error_Completed;
        }

        #region Help viewer animation control
        private void ResetHelpAnimations()
        {
            double curr_left_canvas_pos = Canvas.GetLeft(help_overlay_control);
            help_overlay_control.BeginAnimation(Canvas.LeftProperty, null);
            Canvas.SetLeft(help_overlay_control, curr_left_canvas_pos);

            double curr_height = help_overlay_control.Height;
            help_overlay_control.BeginAnimation(FrameworkElement.HeightProperty, null);
            help_overlay_control.Height = curr_height;

            _fade_in_help_viewer.AccelerationRatio = 0;
            _fade_in_help_viewer.DecelerationRatio = 0;
            _fade_out_help_viewer.AccelerationRatio = 0;
            _fade_out_help_viewer.DecelerationRatio = 0;
            _shrink_grow_height_help_viewer.AccelerationRatio = 0;
            _shrink_grow_height_help_viewer.DecelerationRatio = 0;
        }

        private void FadeOutHelp()
        {            
            ResetHelpAnimations();

            // options_control_grid
            double curr_left_canvas_pos = Canvas.GetLeft(help_overlay_control);
            help_overlay_control.BeginAnimation(Canvas.LeftProperty, null);
            Canvas.SetLeft(help_overlay_control, curr_left_canvas_pos);

            _fade_out_help_viewer.From = curr_left_canvas_pos;
            _fade_out_help_viewer.To = -help_overlay_control.ActualWidth;
            _fade_out_help_viewer.DecelerationRatio = 1;

            int duration_curr = Convert.ToInt32(((help_overlay_control.ActualWidth + curr_left_canvas_pos) / help_overlay_control.ActualWidth) * _fadout_anim_duration_full_in_msec);
            if (duration_curr < 0) duration_curr = 1;
            _fade_out_help_viewer.Duration = new Duration(TimeSpan.FromMilliseconds(duration_curr));

            help_overlay_control.BeginAnimation(Canvas.LeftProperty, _fade_out_help_viewer, HandoffBehavior.Compose);

            if(_error_visibility == ErrorVisibilityMode.FullErrorVisible)
            {
                ShrinkGrowFullErrorViewer();
            }
        }

        private void FadeInHelp()
        {
            ShowHelpErrorViewer();

            ResetHelpAnimations();

            // set corrent height depending on the error info currently visible
            help_overlay_control.Height = CalculateHelpViewerHeight(control_canvas.ActualHeight);
            
            if(_error_visibility == ErrorVisibilityMode.FullErrorVisible)
            {
                ShrinkGrowFullErrorViewer();
            }

            //starte fade in animation
            _fade_in_help_viewer.DecelerationRatio = 1;
            _fade_in_help_viewer.From = -help_overlay_control.ActualWidth;
            _fade_in_help_viewer.To = 0;
            help_overlay_control.Visibility = Visibility.Visible;
            help_overlay_control.BeginAnimation(Canvas.LeftProperty, _fade_in_help_viewer, HandoffBehavior.Compose);
        }

        private void ShrinkGrowHelpViewer()
        {
            ResetHelpAnimations();

            double curr_height = help_overlay_control.Height;
            if (double.IsNaN(curr_height))
                return;

            help_overlay_control.BeginAnimation(FrameworkElement.HeightProperty, null);
            help_overlay_control.Height = curr_height;

            _shrink_grow_height_help_viewer.From = curr_height;
            _shrink_grow_height_help_viewer.To = CalculateHelpViewerHeight(control_canvas.ActualHeight);

            int duration_curr = Convert.ToInt32(_fadein_anim_duration_full_in_msec);
            if (duration_curr < 0) duration_curr = 1;
            _shrink_grow_height_help_viewer.Duration = new Duration(TimeSpan.FromMilliseconds(duration_curr));

            help_overlay_control.BeginAnimation(FrameworkElement.HeightProperty, _shrink_grow_height_help_viewer);
        }

        #endregion

        #region Change-Info animation control
        private void ResetChangeInfoAnimations()
        {
            double curr_left_canvas_pos = Canvas.GetLeft(change_info_button);
            change_info_button.BeginAnimation(Canvas.LeftProperty, null);
            Canvas.SetLeft(change_info_button, curr_left_canvas_pos);

            curr_left_canvas_pos = Canvas.GetLeft(save_success_info);
            save_success_info.BeginAnimation(Canvas.LeftProperty, null);
            Canvas.SetLeft(save_success_info, curr_left_canvas_pos);

            _fade_in_change_info.AccelerationRatio = 0;
            _fade_in_change_info.DecelerationRatio = 0;
            _fade_out_change_info.AccelerationRatio = 0;
            _fade_out_change_info.DecelerationRatio = 0;

            _fade_in_change_info1.AccelerationRatio = 0;
            _fade_in_change_info1.DecelerationRatio = 0;
            _fade_out_change_info1.AccelerationRatio = 0;
            _fade_out_change_info1.DecelerationRatio = 0;
        }

        private void FadeInChangeInfo()
        {
            ResetChangeInfoAnimations();

            //starte fade in animation
            _fade_in_change_info.From = -CHANGE_INFO_WIDTH; // change_info_button.ActualWidth;
            change_info_button.BeginAnimation(Canvas.LeftProperty, _fade_in_change_info);
            change_info_button.Visibility = Visibility.Visible;

            _fade_in_change_info1.From = -SUCCESS_INFO_SPACE;
            _fade_in_change_info1.To = CHANGE_INFO_WIDTH - SUCCESS_INFO_SPACE;
            save_success_info.BeginAnimation(Canvas.LeftProperty, _fade_in_change_info1);
        }

        private void FadeOutChangeInfo()
        {
            ResetChangeInfoAnimations();

            // options_control_grid
            double curr_left_canvas_pos = Canvas.GetLeft(change_info_button);
            if (double.IsNaN(curr_left_canvas_pos))
                return;

            change_info_button.BeginAnimation(Canvas.LeftProperty, null);
            Canvas.SetLeft(change_info_button, curr_left_canvas_pos);

            _fade_out_change_info.From = curr_left_canvas_pos;
            _fade_out_change_info.To = -CHANGE_INFO_WIDTH; // change_info_button.ActualWidth;

            int duration_curr = 1;
            if(change_info_button.ActualWidth > 0)
                duration_curr = Convert.ToInt32(((change_info_button.ActualWidth + curr_left_canvas_pos) / change_info_button.ActualWidth) * _fadout_anim_duration_full_in_msec);

            if (duration_curr < 0) duration_curr = 1;
            _fade_out_change_info.Duration = new Duration(TimeSpan.FromMilliseconds(duration_curr));

            change_info_button.BeginAnimation(Canvas.LeftProperty, _fade_out_change_info);



            curr_left_canvas_pos = Canvas.GetLeft(save_success_info);
            if (double.IsNaN(curr_left_canvas_pos))
                return;

            save_success_info.BeginAnimation(Canvas.LeftProperty, null);
            Canvas.SetLeft(save_success_info, curr_left_canvas_pos);

            _fade_out_change_info1.From = curr_left_canvas_pos;
            _fade_out_change_info1.To = 0 - SUCCESS_INFO_SPACE; // change_info_button.ActualWidth;
            _fade_out_change_info1.Duration = new Duration(TimeSpan.FromMilliseconds(duration_curr));

            save_success_info.BeginAnimation(Canvas.LeftProperty, _fade_out_change_info1);
        }

        #endregion

        #region Save success animation control
        private void ResetSaveSuccessAnimations()
        {
            //double curr_left_canvas_pos = Canvas.GetLeft(save_success_info);
            //save_success_info.BeginAnimation(Canvas.LeftProperty, null);
            //Canvas.SetLeft(save_success_info, curr_left_canvas_pos);
            double curWidth = save_success_info.Width;
            save_success_info.BeginAnimation(FrameworkElement.WidthProperty, null);
            save_success_info.Width = curWidth;

            _fade_in_save_success_info.AccelerationRatio = 0;
            _fade_in_save_success_info.DecelerationRatio = 0;
            _fade_out_save_success_info.AccelerationRatio = 0;
            _fade_out_save_success_info.DecelerationRatio = 0;
        }

        private void FadeInSaveSuccessInfo()
        {
            ResetSaveSuccessAnimations();

            //starte fade in animation
            //_fade_in_save_success_info.From = -save_success_info.ActualWidth;
            //save_success_info.BeginAnimation(Canvas.LeftProperty, _fade_in_save_success_info);
            //save_success_info.Visibility = Visibility.Visible;

            _fade_in_save_success_info.From = 0;
            _fade_in_save_success_info.To = SUCCESS_INFO_WIDTH;
            save_success_info.BeginAnimation(FrameworkElement.WidthProperty, _fade_in_save_success_info);
            save_success_info.Visibility = Visibility.Visible;
        }

        private void FadeOutSaveSuccessInfo()
        {
            ResetSaveSuccessAnimations();

            double curWidth = save_success_info.Width;
            if (double.IsNaN(curWidth))
                return;

            save_success_info.BeginAnimation(FrameworkElement.WidthProperty, null);
            save_success_info.Width =  curWidth;

            _fade_out_save_success_info.From = curWidth;
            _fade_out_save_success_info.To = 0;

            int duration_curr = _fadout_anim_duration_full_in_msec;
            _fade_out_save_success_info.Duration = new Duration(TimeSpan.FromMilliseconds(duration_curr));
            save_success_info.BeginAnimation(FrameworkElement.WidthProperty, _fade_out_save_success_info);

            // options_control_grid
            //double curr_left_canvas_pos = Canvas.GetLeft(save_success_info);
            //if (double.IsNaN(curr_left_canvas_pos))
            //    return;

            //save_success_info.BeginAnimation(Canvas.LeftProperty, null);
            //Canvas.SetLeft(save_success_info, curr_left_canvas_pos);

            //_fade_out_save_success_info.From = curr_left_canvas_pos;
            //_fade_out_save_success_info.To = -save_success_info.ActualWidth;

            //int duration_curr = Convert.ToInt32(((save_success_info.ActualWidth + curr_left_canvas_pos) / save_success_info.ActualWidth) * _fadout_anim_duration_full_in_msec);
            //if (duration_curr < 0) duration_curr = 1;
            //_fade_out_save_success_info.Duration = new Duration(TimeSpan.FromMilliseconds(duration_curr));

            //save_success_info.BeginAnimation(Canvas.LeftProperty, _fade_out_save_success_info);
        }

        #endregion

        #region Mini-Error info animation control
        private void ResetMiniErrorAnimations()
        {
            double curr_left_canvas_pos = Canvas.GetLeft(mini_error_counter);
            mini_error_counter.BeginAnimation(Canvas.LeftProperty, null);
            Canvas.SetLeft(mini_error_counter, curr_left_canvas_pos);

            _fade_in_mini_error_info.AccelerationRatio = 0;
            _fade_in_mini_error_info.DecelerationRatio = 0;
            _fade_out_mini_error_info.AccelerationRatio = 0;
            _fade_out_mini_error_info.DecelerationRatio = 0;
        }

        private void ResetMiniErrorUpDownAnimations()
        {
            double curr_top_canvas_pos = Canvas.GetTop(mini_error_counter);
            mini_error_counter.BeginAnimation(Canvas.TopProperty, null);
            Canvas.SetTop(mini_error_counter, curr_top_canvas_pos);

            _fade_in_mini_error_info1.AccelerationRatio = 0;
            _fade_in_mini_error_info1.DecelerationRatio = 0;
            _fade_out_mini_error_info1.AccelerationRatio = 0;
            _fade_out_mini_error_info1.DecelerationRatio = 0;
        }

        private void FadeInMiniErrorInfo()
        {
            _mini_error_fadin_running = true;
            ResetMiniErrorAnimations();

            //starte fade in animation
            _fade_in_mini_error_info.From = -mini_error_counter.ActualWidth;
            _fade_in_mini_error_info.To = 0;
            mini_error_counter.BeginAnimation(Canvas.LeftProperty, _fade_in_mini_error_info);
            mini_error_counter.Visibility = Visibility.Visible;
        }

        private void FadeOutMiniErrorInfo()
        {
            _mini_error_fadout_running = true;
            ResetMiniErrorAnimations();

            // options_control_grid
            double curr_left_canvas_pos = Canvas.GetLeft(mini_error_counter);
            if (double.IsNaN(curr_left_canvas_pos))
                return;

            mini_error_counter.BeginAnimation(Canvas.LeftProperty, null);
            Canvas.SetLeft(mini_error_counter, curr_left_canvas_pos);

            _fade_out_mini_error_info.From = curr_left_canvas_pos;
            _fade_out_mini_error_info.To = -mini_error_counter.ActualWidth;

            int duration_curr = Convert.ToInt32(((mini_error_counter.ActualWidth + curr_left_canvas_pos) / mini_error_counter.ActualWidth) * _fadout_anim_duration_full_in_msec);
            if (duration_curr < 0) duration_curr = 1;
            _fade_out_mini_error_info.Duration = new Duration(TimeSpan.FromMilliseconds(duration_curr));

            mini_error_counter.BeginAnimation(Canvas.LeftProperty, _fade_out_mini_error_info);
        }

        private void MoveUpMiniErrorInfo()
        {
            ResetMiniErrorUpDownAnimations();

            double curr_top_canvas_pos = Canvas.GetTop(mini_error_counter);
            if (double.IsNaN(curr_top_canvas_pos))
                return;

            //starte fade in animation
            _fade_in_mini_error_info1.From = curr_top_canvas_pos;
            _fade_in_mini_error_info1.To = curr_top_canvas_pos - ChangeInfoHeight;
            mini_error_counter.BeginAnimation(Canvas.TopProperty, _fade_in_mini_error_info1);
        }

        private void MoveDownMiniErrorInfo()
        {
            ResetMiniErrorUpDownAnimations();

            double curr_top_canvas_pos = Canvas.GetTop(mini_error_counter);
            if (double.IsNaN(curr_top_canvas_pos))
                return;

            //starte fade in animation
            _fade_out_mini_error_info1.From = curr_top_canvas_pos;
            _fade_out_mini_error_info1.To = control_canvas.ActualHeight - mini_error_counter.ActualHeight;
            mini_error_counter.BeginAnimation(Canvas.TopProperty, _fade_out_mini_error_info1);
        }
        #endregion

        #region Full error viewer animation control
        private void ResetFullErrorAnimations()
        {
            double curr_left_canvas_pos = Canvas.GetLeft(error_overlay_control);
            error_overlay_control.BeginAnimation(Canvas.LeftProperty, null);
            Canvas.SetLeft(error_overlay_control, curr_left_canvas_pos);

            double curr_height = error_overlay_control.Height;
            error_overlay_control.BeginAnimation(FrameworkElement.HeightProperty, null);
            error_overlay_control.Height = curr_height;

            double curr_top_canvas_pos = Canvas.GetTop(error_overlay_control);
            error_overlay_control.BeginAnimation(Canvas.TopProperty, null);
            Canvas.SetTop(error_overlay_control, curr_top_canvas_pos);

            _fade_in_error_viewer.AccelerationRatio = 0;
            _fade_in_error_viewer2.AccelerationRatio = 0;
            _fade_in_error_viewer3.AccelerationRatio = 0;
            _fade_out_error_viewer.AccelerationRatio = 0;
            _fade_out_error_viewer2.AccelerationRatio = 0;
            _fade_out_error_viewer3.AccelerationRatio = 0;
            _fade_in_error_viewer.DecelerationRatio = 0;
            _fade_in_error_viewer2.DecelerationRatio = 0;
            _fade_in_error_viewer3.DecelerationRatio = 0;
            _fade_out_error_viewer.DecelerationRatio = 0;
            _fade_out_error_viewer2.DecelerationRatio = 0;
            _fade_out_error_viewer3.DecelerationRatio = 0;

            _shrink_grow_height_error_viewer.AccelerationRatio = 0;
            _shrink_grow_height_error_viewer.DecelerationRatio = 0;
        }

        private void FadeInFullErrorInfo()
        {
            ResetFullErrorAnimations();

            if (_buffer_visibility == ErrorVisibilityMode.MiniErrorVisible && ShowHelp)
            {
                //starte fade in animation
                _fade_in_error_viewer.From = -error_overlay_control.ActualWidth;
                error_overlay_control.BeginAnimation(Canvas.LeftProperty, _fade_in_error_viewer);

                _fade_in_error_viewer2.From = 0;
                _fade_in_error_viewer2.To = CalculateErrorInfoheight(control_canvas.ActualHeight);
                error_overlay_control.BeginAnimation(FrameworkElement.HeightProperty, _fade_in_error_viewer2);

                _fade_in_error_viewer3.From = control_canvas.ActualHeight - ChangeInfoHeight;
                _fade_in_error_viewer3.To = control_canvas.ActualHeight - CalculateErrorInfoheight(control_canvas.ActualHeight) - ChangeInfoHeight;
                error_overlay_control.BeginAnimation(Canvas.TopProperty, _fade_in_error_viewer3);

                error_overlay_control.Visibility = Visibility.Visible;
            }
            else if (_buffer_visibility == ErrorVisibilityMode.None || (_buffer_visibility == ErrorVisibilityMode.MiniErrorVisible && !ShowHelp))
            {
                error_overlay_control.Height = CalculateErrorInfoheight(control_canvas.ActualHeight);
                Canvas.SetTop(error_overlay_control, control_canvas.ActualHeight - error_overlay_control.Height - ChangeInfoHeight);

                //starte fade in animation
                _fade_in_error_viewer.From = -error_overlay_control.ActualWidth;
                _fade_in_error_viewer.To = 0;
                error_overlay_control.BeginAnimation(Canvas.LeftProperty, _fade_in_error_viewer);
                error_overlay_control.Visibility = Visibility.Visible;
            }
        }

        private void FadeOutFullErrorInfo()
        {
            ResetFullErrorAnimations();

            if (ShowHelp)
            {
                // fade out
                double curr_left_canvas_pos = Canvas.GetLeft(error_overlay_control);

                if (double.IsNaN(curr_left_canvas_pos))
                    return;

                error_overlay_control.BeginAnimation(Canvas.LeftProperty, null);
                Canvas.SetLeft(error_overlay_control, curr_left_canvas_pos);

                _fade_out_error_viewer.From = curr_left_canvas_pos;
                _fade_out_error_viewer.To = -error_overlay_control.ActualWidth;

                int duration_curr = Convert.ToInt32(((error_overlay_control.ActualWidth + curr_left_canvas_pos) / error_overlay_control.ActualWidth) * _fadout_anim_duration_full_in_msec);
                if (duration_curr < 0) duration_curr = 1;
                _fade_out_error_viewer.Duration = new Duration(TimeSpan.FromMilliseconds(duration_curr));

                error_overlay_control.BeginAnimation(Canvas.LeftProperty, _fade_out_error_viewer);


                double curr_height = error_overlay_control.Height;
                error_overlay_control.BeginAnimation(FrameworkElement.HeightProperty, null);
                error_overlay_control.Height = curr_height;

                _fade_out_error_viewer2.From = curr_height;
                _fade_out_error_viewer2.To = 0;

                //duration_curr = Convert.ToInt32(((error_overlay_control.ActualHeight + curr_height) / error_overlay_control.ActualHeight) * _fadout_anim_duration_full_in_msec);
                duration_curr = _fadout_anim_duration_full_in_msec;

                if (duration_curr < 0) duration_curr = 1;
                _fade_out_error_viewer2.Duration = new Duration(TimeSpan.FromMilliseconds(duration_curr));

                error_overlay_control.BeginAnimation(FrameworkElement.HeightProperty, _fade_out_error_viewer2);



                double curr_top_canvas_pos = Canvas.GetTop(error_overlay_control);
                error_overlay_control.BeginAnimation(Canvas.TopProperty, null);
                Canvas.SetTop(error_overlay_control, curr_top_canvas_pos);

                _fade_out_error_viewer3.From = curr_top_canvas_pos;
                _fade_out_error_viewer3.To = control_canvas.ActualHeight - ChangeInfoHeight;

                duration_curr = Convert.ToInt32( _fadout_anim_duration_full_in_msec);
                if (duration_curr < 0) duration_curr = 1;
                _fade_out_error_viewer3.Duration = new Duration(TimeSpan.FromMilliseconds(duration_curr));

                error_overlay_control.BeginAnimation(Canvas.TopProperty, _fade_out_error_viewer3);
            }
            else
            {
                // fade out
                double curr_left_canvas_pos = Canvas.GetLeft(error_overlay_control);
                if (double.IsNaN(curr_left_canvas_pos))
                    return;

                error_overlay_control.BeginAnimation(Canvas.LeftProperty, null);
                Canvas.SetLeft(error_overlay_control, curr_left_canvas_pos);

                _fade_out_error_viewer.From = curr_left_canvas_pos;
                _fade_out_error_viewer.To = -error_overlay_control.ActualWidth;

                int duration_curr = Convert.ToInt32(((error_overlay_control.ActualWidth + curr_left_canvas_pos)/error_overlay_control.ActualWidth)*_fadout_anim_duration_full_in_msec);
                if (duration_curr < 0) duration_curr = 1;
                _fade_out_error_viewer.Duration = new Duration(TimeSpan.FromMilliseconds(duration_curr));

                error_overlay_control.BeginAnimation(Canvas.LeftProperty, _fade_out_error_viewer);
            }
        }

        private void ShrinkGrowFullErrorViewer()
        {
            ResetFullErrorAnimations();

            double curr_height = error_overlay_control.Height;
            if (double.IsNaN(curr_height))
                return;

            error_overlay_control.BeginAnimation(FrameworkElement.HeightProperty, null);
            error_overlay_control.Height = curr_height;

            double curr_top_canvas_pos = Canvas.GetTop(error_overlay_control);
            error_overlay_control.BeginAnimation(Canvas.TopProperty, null);
            Canvas.SetTop(error_overlay_control, curr_top_canvas_pos);

            _shrink_grow_height_error_viewer.From = curr_height;
            _shrink_grow_height_error_viewer.To = CalculateErrorInfoheight(control_canvas.ActualHeight);

            double diff1 = _shrink_grow_height_error_viewer.To.Value - _shrink_grow_height_error_viewer.From.Value;
            diff1 = Math.Abs(diff1);

            double duration_curr = _fadein_anim_duration_full_in_msec;
            if (duration_curr < 0) duration_curr = 1;
            _shrink_grow_height_error_viewer.Duration = new Duration(TimeSpan.FromMilliseconds(duration_curr));

            error_overlay_control.BeginAnimation(FrameworkElement.HeightProperty, _shrink_grow_height_error_viewer);


            _fade_out_error_viewer3.From = curr_top_canvas_pos;
            _fade_out_error_viewer3.To = control_canvas.ActualHeight - CalculateErrorInfoheight(control_canvas.ActualHeight) - ChangeInfoHeight;


            double diff2 = _fade_out_error_viewer3.To.Value - _fade_out_error_viewer3.From.Value;
            diff2 = Math.Abs(diff2);

            duration_curr = _fadein_anim_duration_full_in_msec * diff2 / diff1;
            if (duration_curr < 0) duration_curr = 1;

            _fade_out_error_viewer3.Duration = new Duration(TimeSpan.FromMilliseconds(duration_curr));
            error_overlay_control.BeginAnimation(Canvas.TopProperty, _fade_out_error_viewer3);


        }

        #endregion

        private void OnErrorVisibilityChanged()
        {
            if (ErrorVisibilityChanged != null)
                ErrorVisibilityChanged(this, new EventArgs());
        }

        private double CalculateHelpViewerHeight(double canvasHeight)
        {
            double dRet = 0;

            if (ShowError && _error_visibility != ErrorVisibilityMode.None)
            {
                if(_error_visibility == ErrorVisibilityMode.MiniErrorVisible)
                {
                    // mini error info visible
                    double minierror_height = mini_error_counter.DesiredSize.Height;
                    dRet = canvasHeight - minierror_height - CONTROL_MARGIN - ChangeInfoHeight;
                }
                else
                {
                    // full error info visible
                    double errorinfo_height = canvasHeight*ERRORINFO_HEIGHT_PERCENTAGE/100.0;
                    dRet = canvasHeight - errorinfo_height - CONTROL_MARGIN;// -ChangeInfoHeight;
                }
            }
            else
            {
                dRet = canvasHeight - ChangeInfoHeight;
            }

            return dRet;
        }

        private double CalculateErrorInfoheight(double canvasHeight)
        {
            double dRet = 0;

            if (ShowError && _error_visibility != ErrorVisibilityMode.None)
            {
                if (_error_visibility == ErrorVisibilityMode.MiniErrorVisible)
                {
                    // mini error info visible
                    dRet = mini_error_counter.DesiredSize.Height;
                }
                else
                {
                    if (ShowHelp)
                    {
                        // full error info visible
                        dRet = (canvasHeight * ERRORINFO_HEIGHT_PERCENTAGE / 100.0) - ChangeInfoHeight;
                    }
                    else
                    {
                        dRet = (canvasHeight * ERRORINFO_HEIGHT_PERCENTAGE_NO_HELP / 100.0) - ChangeInfoHeight;
                    }
                }
            }

            return dRet;
        }

        /// <summary>
        /// Shows the full error info pane
        /// </summary>
        public void ShowFullErrorInfo()
        {
            _buffer_visibility = ErrorVisibilityMode.None;

            if (_error_visibility == ErrorVisibilityMode.MiniErrorVisible)
            {
                // FadeIn-/FadeOutFullErrorInfo will use this variable to 
                // to decide the animation methode
                _buffer_visibility = ErrorVisibilityMode.MiniErrorVisible;
                FadeOutMiniErrorInfo(); // fade out mini error counter
            }

            _error_visibility = ErrorVisibilityMode.FullErrorVisible;
            FadeInFullErrorInfo();  // fade in full error info

            if (ShowHelp)
                ShrinkGrowHelpViewer(); // shrink help viewer

            OnErrorVisibilityChanged();

        }

        internal bool EventOriginatesInHelpErrorControl(object sender, RoutedEventArgs e)
        {
            bool bret = CheckErrorHelpSource(sender);

            if (!bret)
                bret = CheckErrorHelpSource(e.Source);

            if (!bret)
                bret = CheckErrorHelpSource(e.OriginalSource);

            return bret;
        }

        private bool CheckErrorHelpSource(object sender)
        {
            if (sender == help_overlay_control)
                return true;

            if (UIHelper.IsVisualChild(help_overlay_control, sender as DependencyObject))
                return true;

            if (sender == mini_error_counter)
                return true;

            if (UIHelper.IsVisualChild(mini_error_counter, sender as DependencyObject))
                return true;

            if (sender == error_overlay_control)
                return true;

            if (UIHelper.IsVisualChild(error_overlay_control, sender as DependencyObject))
                return true;

            return false;
        }
        /// <summary>
        /// Hides the full error info
        /// </summary>
        public void HideFullErrorInfo()
        {
            _buffer_visibility = _error_visibility;

            FadeOutFullErrorInfo();
            if(ShowError)
            {
                _error_visibility = ErrorVisibilityMode.MiniErrorVisible;
                FadeInMiniErrorInfo();
            }
            else
            {
                _error_visibility = ErrorVisibilityMode.None;
            }

            if(ShowHelp)
                ShrinkGrowHelpViewer();

            _buffer_visibility = ErrorVisibilityMode.None;
            OnErrorVisibilityChanged();
        }
        #endregion

        #region event handler

        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            if(e.Property == ShowHelpProperty)
            {
                ShowHelp_Changed(null, null);
            }
            else if(e.Property == ShowErrorProperty)
            {
                ShowError_Changed(null, null);
            }
            else if(e.Property == ShowChangeInfoProperty)
            {
                ShowChangeInfo_Changed(null, null);
            }
            else if(e.Property == ShowSaveSuccessProperty)
            {
                ShowSaveSuccess_Changed(null, null);
            }
            else if(e.Property == ChangeInfoAdornerVisibleProperty)
            {
                ChangeInfoAdornerVisible_Changed(null, null);
            }

            base.OnPropertyChanged(e);
        }
        /// <summary>
        /// The size of the control hosting canvas changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void control_canvas_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            ResetFullErrorAnimations();
            ResetHelpAnimations();
            ResetMiniErrorAnimations();
            //ResetChangeInfoAnimations();
            //ResetSaveSuccessAnimations();



            Canvas.SetTop(change_info_button, e.NewSize.Height - change_info_button.ActualHeight);
            Canvas.SetTop(save_success_info, e.NewSize.Height - save_success_info.ActualHeight);

            help_overlay_control.Height = CalculateHelpViewerHeight(e.NewSize.Height); // control_canvas.ActualHeight;
            Canvas.SetTop(mini_error_counter, e.NewSize.Height - mini_error_counter.ActualHeight - ChangeInfoHeight);

            if (ShowHelp)
            {
                error_overlay_control.Height = e.NewSize.Height*ERRORINFO_HEIGHT_PERCENTAGE/100.0;
                Canvas.SetTop(error_overlay_control, e.NewSize.Height - (e.NewSize.Height * ERRORINFO_HEIGHT_PERCENTAGE / 100.0) - ChangeInfoHeight); //error_overlay_control.ActualHeight);
            }
            else
            {
                error_overlay_control.Height = e.NewSize.Height*ERRORINFO_HEIGHT_PERCENTAGE_NO_HELP/100.0;
                Canvas.SetTop(error_overlay_control, e.NewSize.Height - (e.NewSize.Height * ERRORINFO_HEIGHT_PERCENTAGE_NO_HELP / 100.0) - ChangeInfoHeight); //error_overlay_control.ActualHeight);
            }

            e.Handled = true;
        }

        /// <summary>
        /// Cllaed if the ShowHelp dependency property changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ShowHelp_Changed(object sender, EventArgs e)
        {
            //if (!ShowHelp && !ShowError && !ShowChangeInfo && !ShowSaveSuccess)
            //    HideHelpErrorViewer();
            //else
            //    ShowHelpErrorViewer();

            if (ShowHelp)
                FadeInHelp();
            else
                FadeOutHelp();
        }

        /// <summary>
        /// Cllaed if the ShowError dependency property changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ShowError_Changed(object sender, EventArgs e)
        {
            if (!ShowHelp && !ShowError && !ShowChangeInfo && !ShowSaveSuccess)
                HideHelpErrorViewer();
            else
                ShowHelpErrorViewer();

            if (ShowError)
            {
                if (_error_visibility == ErrorVisibilityMode.None)
                {
                    if (ShowFullErrorInfoInsteadOfMini)
                    {
                        ShowFullErrorInfoInsteadOfMini = false;
                        ShowFullErrorInfo();
                    }
                    else
                    {
                        _error_visibility = ErrorVisibilityMode.MiniErrorVisible;

                        // wenn noch keine error info angezeigt wird
                        if (ShowHelp)
                        {
                            ShrinkGrowHelpViewer();
                            FadeInMiniErrorInfo();
                        }
                        else
                        {
                            FadeInMiniErrorInfo();
                            
                        }
                        OnErrorVisibilityChanged();
                    }
                    
                }
            } 
            else
            {
                if(_error_visibility == ErrorVisibilityMode.MiniErrorVisible)
                {
                    _error_visibility = ErrorVisibilityMode.None;
                    FadeOutMiniErrorInfo();
                    ShrinkGrowHelpViewer();
                    OnErrorVisibilityChanged();
                }
                else if(_error_visibility == ErrorVisibilityMode.FullErrorVisible)
                {
                    _error_visibility = ErrorVisibilityMode.None;
                    HideFullErrorInfo();
                }
            }
        }

        /// <summary>
        /// Cllaed if the ShowChangeInfo dependency property changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ShowChangeInfo_Changed(object sender, EventArgs e)
        {
            if (!ShowHelp && !ShowError && !ShowChangeInfo && !ShowSaveSuccess)
                HideHelpErrorViewer();
            else
                ShowHelpErrorViewer();

            if (ShowChangeInfo)
            {
                if(!ShowSaveSuccess)
                {
                    if (ShowHelp)
                        ShrinkGrowHelpViewer();

                    if (ShowError && _error_visibility == ErrorVisibilityMode.FullErrorVisible)
                        ShrinkGrowFullErrorViewer();

                    MoveUpMiniErrorInfo();
                }

                FadeInChangeInfo();
            }
            else
            {
                if (!ShowSaveSuccess)
                {
                    if (ShowHelp)
                        ShrinkGrowHelpViewer();

                    if (ShowError && _error_visibility == ErrorVisibilityMode.FullErrorVisible)
                        ShrinkGrowFullErrorViewer();

                    MoveDownMiniErrorInfo();
                }

                FadeOutChangeInfo();
            }
        }

        /// <summary>
        /// Cllaed if the ShowSaveSuccess dependency property changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ShowSaveSuccess_Changed(object sender, EventArgs e)
        {
            if (!ShowHelp && !ShowError && !ShowChangeInfo && !ShowSaveSuccess)
                HideHelpErrorViewer();
            else
                ShowHelpErrorViewer();

            if (ShowSaveSuccess)
            {
                if (!ShowChangeInfo)
                {
                    if (ShowHelp)
                        ShrinkGrowHelpViewer();

                    if (ShowError && _error_visibility == ErrorVisibilityMode.FullErrorVisible)
                        ShrinkGrowFullErrorViewer();

                    MoveUpMiniErrorInfo();
                }

                FadeInSaveSuccessInfo();
            }
            else
            {
                if (!ShowChangeInfo)
                {
                    if (ShowHelp)
                        ShrinkGrowHelpViewer();

                    if (ShowError && _error_visibility == ErrorVisibilityMode.FullErrorVisible)
                        ShrinkGrowFullErrorViewer();

                    MoveDownMiniErrorInfo();
                }

                FadeOutSaveSuccessInfo();
            }
        }

        private void ChangeInfoAdornerVisible_Changed(object sender, EventArgs e)
        {
            change_info_button.Tag = ChangeInfoAdornerVisible;
        }

         /// <summary>
        /// Called after the control has been loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HelpAndErrorViewer_Loaded(object sender, RoutedEventArgs e)
        {
            if (ParentDialog.HelpProvider != null)
            {
                // set the default help viewer
                ParentDialog.HelpProvider.SetHelpOverlay(help_overlay_control);
            }

            if(ParentDialog.ErrorProvider != null)
            {
                // listen to the error provider collection changed event
                // to update error conting
                ParentDialog.ErrorProvider.ProviderErrorList.CollectionChanged += new System.Collections.Specialized.NotifyCollectionChangedEventHandler(ProviderErrorList_CollectionChanged);
            }

            error_overlay_control.SetErrorProvider(ParentDialog.ErrorProvider);

            double curr_left_canvas_pos = Canvas.GetLeft(change_info_button);
            if (double.IsNaN(curr_left_canvas_pos))
            {
                if (ShowChangeInfo)
                    Canvas.SetLeft(change_info_button, 0);
                else
                    Canvas.SetLeft(change_info_button, -CHANGE_INFO_WIDTH);
            }

            curr_left_canvas_pos = Canvas.GetLeft(save_success_info);
            if (double.IsNaN(curr_left_canvas_pos))
            {
                if (ShowChangeInfo)
                    Canvas.SetLeft(save_success_info, CHANGE_INFO_WIDTH - SUCCESS_INFO_SPACE);
                else
                    Canvas.SetLeft(save_success_info, -SUCCESS_INFO_SPACE);
            }
        }


        /// <summary>
        /// Called if the parent error provider's error collection changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void ProviderErrorList_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (ParentDialog.ErrorProvider != null)
            {
                SetValue(ErrorCountPropertyKey, ParentDialog.ErrorProvider.ProviderErrorList.Count);
            }
        }
        /// <summary>
        /// Called if the user clicks on the mini error info button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MiniErrorInfo_Clicked(object sender, RoutedEventArgs e)
        {
            ShowFullErrorInfo();
        }

        /// <summary>
        /// toggle change info adorner visibility
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeInfoButton_Clicked(object sender, RoutedEventArgs e)
        {
            if(ParentDialog != null)
            {
                if(ParentDialog.ChangeInfoProvider != null)
                {
                    bool oldValue = ParentDialog.ChangeInfoProvider.ChangeInfoAdornerVisible;
                    ParentDialog.ChangeInfoProvider.ChangeInfoAdornerVisible = !oldValue;
                }
            }
        }

        #region help animation event handler
        /// <summary>
        /// fade out  help completed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Fade_out_help_Completed(object sender, EventArgs e)
        {
            if (!ShowHelp && !ShowError && !ShowChangeInfo && !ShowSaveSuccess)
                HideHelpErrorViewer();
            else
                ShowHelpErrorViewer();

            help_overlay_control.Visibility = Visibility.Hidden;
            //ResetHelpAnimations();
        }
        /// <summary>
        /// fade in help completed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Fade_in_help_Completed(object sender, EventArgs e)
        {
        }
        /// <summary>
        /// fade out  mini error info completed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Fade_out_mini_error_info_Completed(object sender, EventArgs e)
        {
            _mini_error_fadout_running = false;

            if (_error_visibility != ErrorVisibilityMode.MiniErrorVisible && !_mini_error_fadin_running && !_mini_error_fadout_running)
                mini_error_counter.Visibility = Visibility.Hidden;
            //ResetMiniErrorAnimations();
        }

        /// <summary>
        /// fade out  mini error info completed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Fade_out_mini_error_info1_Completed(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// fade out  mini error info completed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Fade_out_change_info_Completed(object sender, EventArgs e)
        {
            change_info_button.Visibility = Visibility.Hidden;
        }

        /// <summary>
        /// fade out  mini error info completed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Fade_out_change_info1_Completed(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// fade out  mini error info completed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Fade_out_save_success_info_Completed(object sender, EventArgs e)
        {
            save_success_info.Visibility = Visibility.Hidden;
        }

        /// <summary>
        /// fade in mini error info completed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Fade_in_mini_error_info_Completed(object sender, EventArgs e)
        {
            _mini_error_fadin_running = false;
            mini_error_counter.BeginAnimation(Canvas.LeftProperty, null);
            Canvas.SetLeft(mini_error_counter, 0);
        }

        /// <summary>
        /// fade in mini error info completed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Fade_in_mini_error_info1_Completed(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// fade in mini error info completed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Fade_in_change_info_Completed(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// fade in mini error info completed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Fade_in_change_info1_Completed(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// fade in mini error info completed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Fade_in_save_success_info_Completed(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// fade out  full error info completed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Fade_out_full_error_info_Completed(object sender, EventArgs e)
        {
            if(_error_visibility != ErrorVisibilityMode.FullErrorVisible)
                error_overlay_control.Visibility = Visibility.Hidden;

            //ResetFullErrorAnimations();
        }
        /// <summary>
        /// fade in full error info completed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Fade_in_full_error_info_Completed(object sender, EventArgs e)
        {
            _buffer_visibility = ErrorVisibilityMode.FullErrorVisible;
        }
        /// <summary>
        /// fade out  full error info 2 completed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Fade_out_full_error_info2_Completed(object sender, EventArgs e)
        {
        }
        /// <summary>
        /// fade in full error info 2 completed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Fade_in_full_error_info2_Completed(object sender, EventArgs e)
        {
        }
        /// <summary>
        /// fade out  full error info 3 completed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Fade_out_full_error_info3_Completed(object sender, EventArgs e)
        {
        }
        /// <summary>
        /// fade in full error info 3 completed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Fade_in_full_error_info3_Completed(object sender, EventArgs e)
        {
        }

        private void ShrinkGrow_help_Completed(object sender, EventArgs e)
        {
            //ResetHelpAnimations();
        }

        private void ShrinkGrow_error_Completed(object sender, EventArgs e)
        {
            //ResetFullErrorAnimations();
        }

        #endregion

        #endregion

        public event EventHandler ErrorVisibilityChanged;

        #region Dependency properties
        public static readonly DependencyProperty ShowHelpProperty;
        public static readonly DependencyProperty ShowErrorProperty;
        
        public static readonly DependencyProperty ErrorCountProperty;
        private static readonly DependencyPropertyKey ErrorCountPropertyKey;

        public static readonly DependencyProperty ShowChangeInfoProperty;
        public static readonly DependencyProperty ShowSaveSuccessProperty;
        public static readonly DependencyProperty ChangeInfoAdornerVisibleProperty;
        #endregion

        #region IDisposable Implementation
        public void Dispose()
        {
            //DependencyPropertyDescriptor descr = DependencyPropertyDescriptor.FromProperty(ShowHelpProperty, this.GetType());
            //if (descr != null)
            //    descr.RemoveValueChanged(this, ShowHelp_Changed);

            //descr = DependencyPropertyDescriptor.FromProperty(ShowErrorProperty, this.GetType());
            //if (descr != null)
            //    descr.RemoveValueChanged(this, ShowError_Changed);

            //descr = DependencyPropertyDescriptor.FromProperty(ShowChangeInfoProperty, this.GetType());
            //if (descr != null)
            //    descr.RemoveValueChanged(this, ShowChangeInfo_Changed);

            //descr = DependencyPropertyDescriptor.FromProperty(ShowSaveSuccessProperty, this.GetType());
            //if (descr != null)
            //    descr.RemoveValueChanged(this, ShowSaveSuccess_Changed);

            //descr = DependencyPropertyDescriptor.FromProperty(ChangeInfoAdornerVisibleProperty, this.GetType());
            //if (descr != null)
            //    descr.RemoveValueChanged(this, ChangeInfoAdornerVisible_Changed);

            Debug.WriteLine("### Disposing OverlayHelpAndErrorView...");
            ParentDialog.HelpProvider.SetHelpOverlay(null);
            error_overlay_control.Dispose();
            error_overlay_control.SetErrorProvider(null);
            ParentDialog.ErrorProvider.ProviderErrorList.CollectionChanged -= ProviderErrorList_CollectionChanged;

        }
        #endregion

        #region attribs

        private IOverlayManagerBase _overlay_manager;
        private DoubleAnimation _fade_in_help_viewer;
        private DoubleAnimation _fade_in_mini_error_info;
        private DoubleAnimation _fade_in_mini_error_info1;
        private DoubleAnimation _fade_in_change_info;
        private DoubleAnimation _fade_in_change_info1;
        private DoubleAnimation _fade_in_save_success_info;
        private DoubleAnimation _fade_in_error_viewer;
        private DoubleAnimation _fade_in_error_viewer2;
        private DoubleAnimation _fade_in_error_viewer3;
        private int _fadein_anim_duration_full_in_msec = 700;

        private DoubleAnimation _fade_out_help_viewer;
        private DoubleAnimation _fade_out_mini_error_info;
        private DoubleAnimation _fade_out_mini_error_info1;
        private DoubleAnimation _fade_out_change_info;
        private DoubleAnimation _fade_out_change_info1;
        private DoubleAnimation _fade_out_save_success_info;
        private DoubleAnimation _fade_out_error_viewer;
        private DoubleAnimation _fade_out_error_viewer2;
        private DoubleAnimation _fade_out_error_viewer3;
        private int _fadout_anim_duration_full_in_msec = 800;

        private DoubleAnimation _shrink_grow_height_help_viewer;
        private DoubleAnimation _shrink_grow_height_error_viewer;

        private ErrorVisibilityMode _buffer_visibility = ErrorVisibilityMode.None;
        private ErrorVisibilityMode _error_visibility = ErrorVisibilityMode.None;

        private const double CONTROL_MARGIN = 10;
        private const double ERRORINFO_HEIGHT_PERCENTAGE = 30;
        private const double ERRORINFO_HEIGHT_PERCENTAGE_NO_HELP = 50;
        private const double SUCCESS_INFO_WIDTH = 20;
        private const double CHANGE_INFO_WIDTH = 20;
        private const double SUCCESS_INFO_SPACE = 6;
        private bool _isActive = true;
        private bool _mini_error_fadin_running = false;
        private bool _mini_error_fadout_running = false;

        private bool _is_initializing = true;

        #region Dependency properties
        public static readonly DependencyProperty OverlayBackgroundBrushProperty;
        #endregion
        #endregion
    }
}
