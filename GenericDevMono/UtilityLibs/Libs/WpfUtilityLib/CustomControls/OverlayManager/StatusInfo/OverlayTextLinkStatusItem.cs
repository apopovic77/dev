﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Logicx.WpfUtility.CustomControls.OverlayManager.StatusInfo
{
    public class OverlayTextLinkStatusItem : OverlayConfirmStatusItem
    {
        public OverlayTextLinkStatusItem() : base()
        {
            StackPanel sp = new StackPanel();
            sp.Orientation = Orientation.Horizontal;

            TextBlock tb = new TextBlock();
            tb.FontSize = 18;
            tb.Foreground = Brushes.White;
            tb.FontFamily = new FontFamily("Calibri");
            TextBlock tb_link = new TextBlock();
            tb_link.FontSize = 18;
            tb_link.Foreground = Brushes.White;
            tb_link.FontFamily = new FontFamily("Calibri");

            tb.Text = "Informationen zur neuen Release finden Sie unter ";


            Button b = new Button();
            b.Click += new RoutedEventHandler(b_Click);
            tb_link.Text = "Schulungsvideos";
            tb_link.TextDecorations = TextDecorations.Underline;

            b.Content = tb_link;
            
            sp.Children.Add(tb);
            sp.Children.Add(b);

            ConfirmInfo.ContenStackPanel.Children.Clear();
            ConfirmInfo.ContenStackPanel.Children.Add(sp);

            Closeable = false;
        }

        void b_Click(object sender, RoutedEventArgs e)
        {
            if(SchulungsvideosClicked != null)
                SchulungsvideosClicked(this, new EventArgs());
        }

        public event EventHandler SchulungsvideosClicked;

    }
}
