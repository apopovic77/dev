﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Threading;
using Logicx.Utilities;
using Logicx.WpfUtility.WindowParallelisation.WCFComm;
using NamedPipe;
using NamedPipe.Communication;

namespace Logicx.WpfUtility.WindowParallelisation
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class BoundWindowOverlay : Window, IPipeService
    {
        public BoundWindowOverlay(string bound_process_name, string bound_window_title, string pipe_comm_name)
        {
            _bound_window_title = bound_window_title;
            _bound_process_name = bound_process_name;

            _window_comm_pipe_receive = new WindowCommReceiver(this, pipe_comm_name);
            if(!_window_comm_pipe_receive.ServiceOn())
            {
#if DEBUG
                Console.WriteLine(_window_comm_pipe_receive.error.Message);
#endif
            }

            Title = "BoundedOverlayWindow";
            //ShowInTaskbar = false;
            AllowsTransparency = true;
            Background = Brushes.Transparent;
            //Opacity = 0.2;
            WindowStyle = WindowStyle.None;
            ResizeMode = ResizeMode.NoResize;
            IsHitTestVisible = false;
            Focusable = false;
            Topmost = true;

            Loaded += new RoutedEventHandler(BoundWindowOverlay_Loaded);
        }

        #region Impl

        public virtual void PipeMessage(List<string> data)
        {
        }

        public void SetWindowDimension(double left, double top, double width, double height)
        {
            this.Left = left;
            this.Top = top;
            this.Width = width;
            this.Height = height;
        }
        #endregion


        void BoundWindowOverlay_Loaded(object sender, RoutedEventArgs e)
        {
            FindBoundWindow(_bound_process_name);
        }

        private void FindBoundWindow(string bound_process_name)
        {
            _bound_window_process = Process.GetProcesses().Where(p => p.ProcessName.ToLower().IndexOf(bound_process_name.ToLower()) >= 0).OrderByDescending(p => p.StartTime) .FirstOrDefault();
            
            if(_bound_window_process == null)
                return;

            if (FoundBoundedWindow != null)
                FoundBoundedWindow(this, new EventArgs());

            _check_for_termination_bg_worker = new QueuedBackgroundWorker("check_for_termination_bg_worker", true);
            _check_for_termination_bg_worker.DoWork += new QueuedBackgroundWorker.DoWorkEventHandler(check_for_termination_bg_worker_DoWork);
            _check_for_termination_bg_worker.RunWorkerAsync();
        }

        #region Event Handler
        void check_for_termination_bg_worker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                while (true)
                {
                    Thread.Sleep(2000);
                    Process process = Process.GetProcesses().Where(p => p.Id == _bound_window_process.Id).SingleOrDefault();
                    if (process == null)
                    {
                        Dispatcher.Invoke(DispatcherPriority.Normal, (NoParas)Close);
                        return;
                    }
                }
            }
            catch
            {
            }
        }

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            base.OnClosing(e);

            try
            {
                _window_comm_pipe_receive.Dispose();

                if (_check_for_termination_bg_worker != null)
                {
                    _check_for_termination_bg_worker.Abort();
                    _check_for_termination_bg_worker = null;
                }
            }
            catch
            {
            }
        }


        protected override void OnSourceInitialized(EventArgs e)
        {
            HwndSource hwndSource = PresentationSource.FromVisual(this) as HwndSource;

            if (hwndSource != null)
            {
                _window_overlay_handle = hwndSource.Handle;
            }
        }
        #endregion

        #region Attributes
        private string _bound_window_title;
        private string _bound_process_name;
        protected IntPtr _window_overlay_handle = IntPtr.Zero;
        private IntPtr _bound_window_handle = IntPtr.Zero;
        protected Process _bound_window_process;

        private QueuedBackgroundWorker _check_for_termination_bg_worker;

        public event EventHandler FoundBoundedWindow;

        protected delegate void NoParas();

        protected WindowCommReceiver _window_comm_pipe_receive;// = new WindowCommReceiver();
        #endregion

    }
}
