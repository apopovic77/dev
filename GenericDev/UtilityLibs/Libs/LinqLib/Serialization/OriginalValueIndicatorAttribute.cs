﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LinqLib.Serialization
{
    /// <summary>
    /// Attribute class implementation for marking a property that it holds
    /// the original value of a primary key property within the same type
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class OriginalValueIndicatorAttribute : Attribute
    {
        private string _mapped_primary_key_property_name = "";

        /// <summary>
        /// default constructor of the attribute
        /// </summary>
        /// <param name="mappedPrimaryKeyPropertyName">Name of the mapped primary key property name</param>
        public OriginalValueIndicatorAttribute(string mappedPrimaryKeyPropertyName)
        {
            _mapped_primary_key_property_name = mappedPrimaryKeyPropertyName;
        }

        /// <summary>
        /// Gets/Sets the name of the mapped primary key proeprty name
        /// </summary>
        public string MappedPrimaryKeyPropertyName
        {
            get { return _mapped_primary_key_property_name; }
            set { _mapped_primary_key_property_name = value; }
        }
    }
}
