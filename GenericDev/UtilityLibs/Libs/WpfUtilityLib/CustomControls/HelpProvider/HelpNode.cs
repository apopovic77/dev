﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using Logicx.WpfUtility.Data;

namespace Logicx.WpfUtility.CustomControls.HelpProvider
{
    public class HelpNode : IOneLevelTreeViewItem, IDisposable
    {
        public HelpNode(HelpNode parent_node, Visual ref_uielement, string db_scope, string db_key)
        {
            _parent_node = parent_node;
            ChildrenList = new List<IOneLevelTreeViewItem>();
            _ref_visual = ref_uielement;
            Scope = db_scope;
            DB_Key = db_key;
        }

        #region Public Methods
        /// <summary>
        /// dem aktuellen Knoten wird ein Kind hinzugefügt
        /// </summary>
        /// <param name="child"></param>
        public void AddChildNode(HelpNode child)
        {
            ChildrenList.Add(child);
        }
        #endregion

        #region Properties
        /// <summary>
        /// Holt aus der Datenbank den Titel
        /// </summary>
        public string HelpTitle
        {
            get
            {
                if (DBEntry == null)
                    return "nicht vorhanden - HT";

                return DBEntry.Title;
            }
        }

        /// <summary>
        /// holt hilfe beschreibung aus der datenbank
        /// </summary>
        public string HelpDescription
        {
            get
            {
                if (DBEntry == null)
                    return "nicht vorhanden - HD";
                return DBEntry.ShortDescription + "\n\n" + DBEntry.LongDescription;
            }
        }

        protected AppHelp  DBEntry
        {
            get
            {
                if (_app_help != null)
                    return _app_help;
                else
                {
                    _app_help = HelpProvider.GetHelpEntry(Scope, DB_Key);
                    return _app_help;
                }
            }
        }

        public Visual RefVisual
        {
            get
            {
                return _ref_visual;
            }
        }
        public Visual RefDisplayedVisual;

        public HelpNode ParentNode
        {
            get
            {
                return _parent_node;
            }
        }
        public List<IOneLevelTreeViewItem> ChildrenList { set; get; }
        public string ItemHeader
        {
            get
            {
                return HelpTitle;
            }
        }

        public string Scope;
        public readonly string DB_Key;
        #endregion

        #region IDisposeable Implementation
        public void Dispose()
        {
            _ref_visual = null;
            _parent_node = null;
            ChildrenList.Clear();
        }
        #endregion

        #region Attributes
        protected Visual _ref_visual;
        protected HelpNode _parent_node;

        protected AppHelp _app_help = null;
        #endregion

    }
}
