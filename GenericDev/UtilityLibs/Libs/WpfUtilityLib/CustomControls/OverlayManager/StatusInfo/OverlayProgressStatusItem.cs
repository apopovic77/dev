﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace Logicx.WpfUtility.CustomControls.OverlayManager.StatusInfo
{
    public class OverlayProgressStatusItem : OverlayStatusItem
    {
        public OverlayProgressStatusItem()
        {
            ProgressInfo = new ProgressInfo();
            ProgressInfo.Margin = new Thickness(10,0,0,0);
            border.Child = ProgressInfo;

            Closeable = false;
        }

        public ProgressInfo ProgressInfo
        {
            get; set;
        }
    }
}
