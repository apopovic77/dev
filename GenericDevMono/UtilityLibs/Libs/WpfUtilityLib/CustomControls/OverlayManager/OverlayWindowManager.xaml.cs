﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Logicx.WpfUtility.CustomControls.AnimatedPanel;
using Logicx.WpfUtility.CustomControls.Taskbar;
using Logicx.WpfUtility.WindowManagement;
using Logicx.WpfUtility.WpfHelpers;
using MathLib;

namespace Logicx.WpfUtility.CustomControls.OverlayManager
{
    /// <summary>
    /// Interaction logic for OverlayWindowManager.xaml
    /// </summary>
    public partial class OverlayWindowManager : UserControl
    {
        #region nested classes
        private class StateSnapshotInfo
        {
            public OverlayWindowMeta DialogMeta;
            public OverlayDialogState SnapshotState = OverlayDialogState.Normal;
            public bool SnapshotActiveState = false;
        }
        #endregion

        public OverlayWindowManager()
        {
            InitializeComponent();
        }

        #region internal operations
        internal void RepositionWindowManager()
        {
            RepositionWindowManager(_parent_canvas_size);
        }

        internal void RepositionWindowManager(Size newParentCanvasSize)
        {
            _parent_canvas_size = newParentCanvasSize;

            // move window manager to bottom right corner
            double dWMWidth = ActualWidth;
            double dWMHeight = ActualHeight;

            Canvas.SetLeft(this, newParentCanvasSize.Width - dWMWidth);
            Canvas.SetTop(this, newParentCanvasSize.Height - dWMHeight);
        }
        #endregion

        #region Operations
        public Storyboard GetFadeinOpacityAnimation()
        {
            return GetOpacityAnimation(double.NaN, 1, 0.1, double.NaN, 1);
        }

        public Storyboard GetFadeoutOpacityAnmiation()
        {
            return GetOpacityAnimation(1, 0.3, 0.3, double.NaN, 1);
        }

        private Storyboard GetOpacityAnimation(double from, double to, double duration, double deceleration, double acceleration)
        {
            if (double.IsNaN(from) && double.IsNaN(to))
                return null;

            if (deceleration < 0 || deceleration > 1)
                throw new ArgumentException("deceleration muss zwischen 0,0 und 1,0 liegen.");

            if (acceleration < 0 || acceleration > 1)
                throw new ArgumentException("acceleration muss zwischen 0,0 und 1,0 liegen.");

            Storyboard out_story_board = new Storyboard();
            double anim_dur = duration;

            DoubleAnimation opacitydoubleanim = new DoubleAnimation();
            if (!double.IsNaN(from))
                opacitydoubleanim.From = from;

            if (!double.IsNaN(to))
                opacitydoubleanim.To = to;

            opacitydoubleanim.Duration = new Duration(TimeSpan.FromSeconds(duration));
            Storyboard.SetTargetProperty(opacitydoubleanim, new PropertyPath(FrameworkElement.OpacityProperty));

            if (!double.IsNaN(acceleration))
                opacitydoubleanim.AccelerationRatio = acceleration;

            if (!double.IsNaN(deceleration))
                opacitydoubleanim.DecelerationRatio = deceleration;

            out_story_board.Children.Add(opacitydoubleanim);

            return out_story_board;
        }

        public Storyboard GetDefaultMinimizeAnimation(OverlayWindowMeta meta, Point from, Point to, ref double scale_to)
        {
            return GetDefaultMinimizeAnimation(meta, from, to, false, ref scale_to, true);
        }

        public Storyboard GetDefaultMinimizeAnimation(OverlayWindowMeta meta, Point from, Point to, bool linear_path, ref double oscale, bool for_taskbar)
        {
            return GetDefaultMinimizeAnimation(meta, from, to, linear_path, ref oscale, for_taskbar, false);
        }

        public Storyboard GetDefaultMinimizeAnimation(OverlayWindowMeta meta, Point from, Point to, bool linear_path, ref double oscale, bool for_taskbar, bool fade_out)
        {

            //if(meta == null || meta.Dialog == null || meta.Dialog.ActualWidth == 0 || meta.Dialog.ActualHeight == 0)
            if (meta == null || meta.Dialog == null || meta.Dialog.ActualViewboxWidth == 0 || meta.Dialog.ActualViewboxHeight == 0)
                return null;

            Storyboard out_story_board = new Storyboard();
            double anim_dur = 1;

            //double s_offset = content_control.ItemScale;
            double s_offset = 1;

            double scale;
            //Für Version 1
            if (!for_taskbar || OverlayManager.TaskbarPanel == null)
            {
                //SubMenuPanel p = OverlayManager.WindowTaskBar.GetSubMenuPanelByTitle("Dialoge");
                ISubMenuPanelBase p = GetSubMenuPanelByTitle(meta.Dialog.DialogContent.MinimizedWindowPanelName);
                
                oscale = Double.NaN;
                //if (meta.Dialog.ActualWidth >= meta.Dialog.ActualHeight)
                if (meta.Dialog.ActualViewboxWidth >= meta.Dialog.ActualViewboxHeight)
                {
                    //scale = (content_control.ItemWidth*s_offset) / meta.Dialog.ActualWidth;
                    scale = (p.BigItemSize.Width - p.ItemMargin.Left - p.ItemMargin.Right) / meta.Dialog.ActualViewboxWidth;
                    //double new_height = meta.Dialog.ActualHeight * scale;
                    //double new_width = meta.Dialog.ActualWidth*scale;
                    double new_height = meta.Dialog.ActualViewboxHeight * scale;
                    double new_width = meta.Dialog.ActualViewboxWidth * scale;
                    to.Y += (p.BigItemSize.Height - new_height) / 2;
                    to.X += (p.BigItemSize.Width - new_width) / 2;
                }
                else
                {
                    //scale = (content_control.ItemHeight*s_offset) / meta.Dialog.ActualHeight;
                    scale = (p.BigItemSize.Height - p.ItemMargin.Bottom - p.ItemMargin.Top) / meta.Dialog.ActualViewboxHeight;
                    //double new_width = meta.Dialog.ActualWidth * scale;
                    //double new_height = meta.Dialog.ActualHeight*scale;
                    double new_width = meta.Dialog.ActualViewboxWidth * scale;
                    double new_height = meta.Dialog.ActualViewboxHeight * scale;
                    to.X += (p.BigItemSize.Width - new_width) / 2;
                    to.Y += (p.BigItemSize.Height - new_height) / 2;
                }
            }
            // Für Version 2
            else
            {
                if (meta.Dialog.ActualViewboxWidth >= meta.Dialog.ActualViewboxHeight)
                {
                    scale = oscale;
                    double new_height = meta.Dialog.ActualViewboxHeight * scale;
                    double new_width = meta.Dialog.ActualViewboxWidth * scale;
                    //to.Y += (content_control.ItemHeight - new_height) / 2;
                    //to.X += (content_control.ItemWidth - new_width) / 2;
                }
                else
                {
                    scale = oscale;
                    double new_width = meta.Dialog.ActualViewboxWidth * scale;
                    double new_height = meta.Dialog.ActualViewboxHeight * scale;
                    //to.X += (content_control.ItemWidth - new_width) / 2;
                    //to.Y += (content_control.ItemHeight - new_height) / 2;
                }
            }

            CheckPoint(ref to);
            CheckPoint(ref from);

            if (linear_path)
            {
                DoubleAnimation myDoubleLeftAnim = new DoubleAnimation();
                myDoubleLeftAnim.DecelerationRatio = 1;
                myDoubleLeftAnim.From = from.X;
                myDoubleLeftAnim.To = to.X;
                myDoubleLeftAnim.Duration = new Duration(TimeSpan.FromSeconds(anim_dur));
                Storyboard.SetTargetProperty(myDoubleLeftAnim, new PropertyPath(Canvas.LeftProperty));

                DoubleAnimation myDoubleTopAnim = new DoubleAnimation();
                myDoubleTopAnim.DecelerationRatio = 1;
                myDoubleTopAnim.From = from.Y;
                myDoubleTopAnim.To = to.Y;
                myDoubleTopAnim.Duration = new Duration(TimeSpan.FromSeconds(anim_dur));
                Storyboard.SetTargetProperty(myDoubleTopAnim, new PropertyPath(Canvas.TopProperty));

                out_story_board.Children.Add(myDoubleLeftAnim);
                out_story_board.Children.Add(myDoubleTopAnim);
            }
            else
            {
                // Create the animation path.
                PathGeometry animationPath = new PathGeometry();
                PathFigure pFigure = new PathFigure();
                pFigure.StartPoint = from;
                //PolyBezierSegment pBezierSegment = new PolyBezierSegment();

                QuadraticBezierSegment pBezierSegment = new QuadraticBezierSegment();

                double prop = 0.5d;

                Point p1 = new Point(from.X + (to.X - from.X) * prop, from.Y);
                Point p2 = new Point(to.X, to.Y + (from.Y - to.Y) * prop);
                Vector v1 = (p2 - p1);
                double len = v1.Length * 0.5d;
                if (v1.Length > 0)
                    v1.Normalize();
                v1 = v1 * len;
                p1 = p1 + v1;

                pBezierSegment.Point1 = p1;
                pBezierSegment.Point2 = to;
                pFigure.Segments.Add(pBezierSegment);
                animationPath.Figures.Add(pFigure);

                // Freeze the PathGeometry for performance benefits.
                animationPath.Freeze();

                DoubleAnimationUsingPath myDoubleLeftAnim = new DoubleAnimationUsingPath();
                myDoubleLeftAnim.DecelerationRatio = 1;
                myDoubleLeftAnim.PathGeometry = animationPath;
                myDoubleLeftAnim.Source = PathAnimationSource.X;
                myDoubleLeftAnim.Duration = new Duration(TimeSpan.FromSeconds(anim_dur));
                Storyboard.SetTargetProperty(myDoubleLeftAnim, new PropertyPath(Canvas.LeftProperty));

                DoubleAnimationUsingPath myDoubleTopAnim = new DoubleAnimationUsingPath();
                myDoubleTopAnim.DecelerationRatio = 1;
                myDoubleTopAnim.PathGeometry = animationPath;
                myDoubleTopAnim.Source = PathAnimationSource.Y;
                myDoubleTopAnim.Duration = new Duration(TimeSpan.FromSeconds(anim_dur));
                Storyboard.SetTargetProperty(myDoubleTopAnim, new PropertyPath(Canvas.TopProperty));

                out_story_board.Children.Add(myDoubleLeftAnim);
                out_story_board.Children.Add(myDoubleTopAnim);
            }

            ScaleTransform s_trans = new ScaleTransform();

            meta.DialogWindowScreenshot.RenderTransform = s_trans;

            DoubleAnimation myDoubleScaleXAnimation = new DoubleAnimation();
            myDoubleScaleXAnimation.DecelerationRatio = 1;
            myDoubleScaleXAnimation.To = scale;
            myDoubleScaleXAnimation.Duration = new Duration(TimeSpan.FromSeconds(anim_dur));
            Storyboard.SetTargetProperty(myDoubleScaleXAnimation, new PropertyPath("(UIElement.RenderTransform).(ScaleTransform.ScaleX)"));

            DoubleAnimation myDoubleScaleYAnimation = new DoubleAnimation();
            myDoubleScaleYAnimation.DecelerationRatio = 1;
            myDoubleScaleYAnimation.To = scale;
            myDoubleScaleYAnimation.Duration = new Duration(TimeSpan.FromSeconds(anim_dur));
            Storyboard.SetTargetProperty(myDoubleScaleYAnimation, new PropertyPath("(UIElement.RenderTransform).(ScaleTransform.ScaleY)"));

            out_story_board.Children.Add(myDoubleScaleXAnimation);
            out_story_board.Children.Add(myDoubleScaleYAnimation);

            if (fade_out)
            {
                DoubleAnimation fadeOutOpacityAnimation = new DoubleAnimation();
                fadeOutOpacityAnimation.DecelerationRatio = 1;
                fadeOutOpacityAnimation.To = 0;
                fadeOutOpacityAnimation.Duration = new Duration(TimeSpan.FromSeconds(anim_dur));
                Storyboard.SetTargetProperty(fadeOutOpacityAnimation, new PropertyPath(OpacityProperty));

                out_story_board.Children.Add(fadeOutOpacityAnimation);
            }

            oscale = scale;

            return out_story_board;
        }

        public static Storyboard GetDefaultTranslateScaleAnimation(Point from, Point to, bool linear_path, double scale_from_x, double scale_from_y, double scale_to_x, double scale_to_y, bool fade_out, double duration)
        {
            Storyboard out_story_board = new Storyboard();
            double anim_dur = duration;

            CheckPoint(ref to);
            CheckPoint(ref from);

            if (linear_path)
            {
                DoubleAnimation myDoubleLeftAnim = new DoubleAnimation();
                myDoubleLeftAnim.DecelerationRatio = 1;
                myDoubleLeftAnim.From = from.X;
                myDoubleLeftAnim.To = to.X;
                myDoubleLeftAnim.Duration = new Duration(TimeSpan.FromSeconds(anim_dur));
                Storyboard.SetTargetProperty(myDoubleLeftAnim, new PropertyPath(Canvas.LeftProperty));

                DoubleAnimation myDoubleTopAnim = new DoubleAnimation();
                myDoubleTopAnim.DecelerationRatio = 1;
                myDoubleTopAnim.From = from.Y;
                myDoubleTopAnim.To = to.Y;
                myDoubleTopAnim.Duration = new Duration(TimeSpan.FromSeconds(anim_dur));
                Storyboard.SetTargetProperty(myDoubleTopAnim, new PropertyPath(Canvas.TopProperty));

                out_story_board.Children.Add(myDoubleLeftAnim);
                out_story_board.Children.Add(myDoubleTopAnim);
            }
            else
            {
                // Create the animation path.
                PathGeometry animationPath = new PathGeometry();
                PathFigure pFigure = new PathFigure();
                pFigure.StartPoint = from;
                //PolyBezierSegment pBezierSegment = new PolyBezierSegment();

                QuadraticBezierSegment pBezierSegment = new QuadraticBezierSegment();

                double prop = 0.5d;

                Point p1 = new Point(from.X + (to.X - from.X) * prop, from.Y);
                Point p2 = new Point(to.X, to.Y + (from.Y - to.Y) * prop);
                Vector v1 = (p2 - p1);
                double len = v1.Length * 0.5d;
                if (v1.Length > 0)
                    v1.Normalize();
                v1 = v1 * len;
                p1 = p1 + v1;

                pBezierSegment.Point1 = p1;
                pBezierSegment.Point2 = to;
                pFigure.Segments.Add(pBezierSegment);
                animationPath.Figures.Add(pFigure);

                // Freeze the PathGeometry for performance benefits.
                animationPath.Freeze();

                DoubleAnimationUsingPath myDoubleLeftAnim = new DoubleAnimationUsingPath();
                myDoubleLeftAnim.DecelerationRatio = 1;
                myDoubleLeftAnim.PathGeometry = animationPath;
                myDoubleLeftAnim.Source = PathAnimationSource.X;
                myDoubleLeftAnim.Duration = new Duration(TimeSpan.FromSeconds(anim_dur));
                Storyboard.SetTargetProperty(myDoubleLeftAnim, new PropertyPath(Canvas.LeftProperty));

                DoubleAnimationUsingPath myDoubleTopAnim = new DoubleAnimationUsingPath();
                myDoubleTopAnim.DecelerationRatio = 1;
                myDoubleTopAnim.PathGeometry = animationPath;
                myDoubleTopAnim.Source = PathAnimationSource.Y;
                myDoubleTopAnim.Duration = new Duration(TimeSpan.FromSeconds(anim_dur));
                Storyboard.SetTargetProperty(myDoubleTopAnim, new PropertyPath(Canvas.TopProperty));

                out_story_board.Children.Add(myDoubleLeftAnim);
                out_story_board.Children.Add(myDoubleTopAnim);
            }

            DoubleAnimation myDoubleScaleXAnimation = new DoubleAnimation();
            myDoubleScaleXAnimation.DecelerationRatio = 1;
            myDoubleScaleXAnimation.From = scale_from_x;
            myDoubleScaleXAnimation.To = scale_to_x;
            myDoubleScaleXAnimation.Duration = new Duration(TimeSpan.FromSeconds(anim_dur));
            Storyboard.SetTargetProperty(myDoubleScaleXAnimation, new PropertyPath("(UIElement.RenderTransform).(ScaleTransform.ScaleX)"));

            DoubleAnimation myDoubleScaleYAnimation = new DoubleAnimation();
            myDoubleScaleYAnimation.DecelerationRatio = 1;
            myDoubleScaleYAnimation.From = scale_from_y;
            myDoubleScaleYAnimation.To = scale_to_y;
            myDoubleScaleYAnimation.Duration = new Duration(TimeSpan.FromSeconds(anim_dur));
            Storyboard.SetTargetProperty(myDoubleScaleYAnimation, new PropertyPath("(UIElement.RenderTransform).(ScaleTransform.ScaleY)"));

            out_story_board.Children.Add(myDoubleScaleXAnimation);
            out_story_board.Children.Add(myDoubleScaleYAnimation);

            if (fade_out)
            {
                DoubleAnimation fadeOutOpacityAnimation = new DoubleAnimation();
                fadeOutOpacityAnimation.DecelerationRatio = 1;
                fadeOutOpacityAnimation.To = 0;
                fadeOutOpacityAnimation.Duration = new Duration(TimeSpan.FromSeconds(anim_dur));
                Storyboard.SetTargetProperty(fadeOutOpacityAnimation, new PropertyPath(OpacityProperty));

                out_story_board.Children.Add(fadeOutOpacityAnimation);
            }

            return out_story_board;
        }

        public Storyboard GetDefaultNormalizeAnimation(Point from, Point to, Point to_scale)
        {
            return GetDefaultNormalizeAnimation(from, to, to_scale, false);
        }

        public Storyboard GetDefaultNormalizeAnimation(Point from, Point to, Point to_scale, bool linear_path)
        {
            double anim_dur = 1;

            Storyboard myStoryboard = new Storyboard();

            CheckPoint(ref to);
            CheckPoint(ref to_scale);

            if (linear_path)
            {
                DoubleAnimation myDoubleLeftAnim = new DoubleAnimation();
                myDoubleLeftAnim.DecelerationRatio = 1;
                myDoubleLeftAnim.From = from.X;
                myDoubleLeftAnim.To = to.X;
                myDoubleLeftAnim.Duration = new Duration(TimeSpan.FromSeconds(anim_dur));
                Storyboard.SetTargetProperty(myDoubleLeftAnim, new PropertyPath(Canvas.LeftProperty));

                DoubleAnimation myDoubleTopAnim = new DoubleAnimation();
                myDoubleTopAnim.DecelerationRatio = 1;
                myDoubleTopAnim.From = from.Y;
                myDoubleTopAnim.To = to.Y;
                myDoubleTopAnim.Duration = new Duration(TimeSpan.FromSeconds(anim_dur));
                Storyboard.SetTargetProperty(myDoubleTopAnim, new PropertyPath(Canvas.TopProperty));

                myStoryboard.Children.Add(myDoubleLeftAnim);
                myStoryboard.Children.Add(myDoubleTopAnim);
            }
            else
            {
                // Create the animation path.
                PathGeometry animationPath = new PathGeometry();
                PathFigure pFigure = new PathFigure();
                pFigure.StartPoint = from;
                
                //PolyBezierSegment pBezierSegment = new PolyBezierSegment();

                QuadraticBezierSegment pBezierSegment = new QuadraticBezierSegment();

                double prop = 0.5d;

                Point p1 = new Point(from.X - (from.X - to.X) * prop, from.Y);
                Point p2 = new Point(to.X, to.Y + (from.Y - to.Y) * prop);
                Vector v1 = (p2 - p1);
                double len = v1.Length * 0.5d;
                if (v1.Length > 0)
                    v1.Normalize();
                v1 = v1 * len;
                p1 = p1 + v1;

                pBezierSegment.Point1 = p1;
                pBezierSegment.Point2 = to;
                pFigure.Segments.Add(pBezierSegment);
                animationPath.Figures.Add(pFigure);

                // Freeze the PathGeometry for performance benefits.
                animationPath.Freeze();

                DoubleAnimationUsingPath myDoubleLeftAnim = new DoubleAnimationUsingPath();
                myDoubleLeftAnim.DecelerationRatio = 1;
                myDoubleLeftAnim.PathGeometry = animationPath;
                myDoubleLeftAnim.Source = PathAnimationSource.X;
                myDoubleLeftAnim.Duration = new Duration(TimeSpan.FromSeconds(anim_dur));
                Storyboard.SetTargetProperty(myDoubleLeftAnim, new PropertyPath(Canvas.LeftProperty));

                DoubleAnimationUsingPath myDoubleTopAnim = new DoubleAnimationUsingPath();
                myDoubleTopAnim.DecelerationRatio = 1;
                myDoubleTopAnim.PathGeometry = animationPath;
                myDoubleTopAnim.Source = PathAnimationSource.Y;
                myDoubleTopAnim.Duration = new Duration(TimeSpan.FromSeconds(anim_dur));
                Storyboard.SetTargetProperty(myDoubleTopAnim, new PropertyPath(Canvas.TopProperty));

                myStoryboard.Children.Add(myDoubleLeftAnim);
                myStoryboard.Children.Add(myDoubleTopAnim);
            }



            DoubleAnimation myDoubleScaleXAnimation = new DoubleAnimation();
            //myDoubleScaleXAnimation.From = dlgMeta.Dialog.ActualWidth;
            myDoubleScaleXAnimation.DecelerationRatio = 1;
            myDoubleScaleXAnimation.To = to_scale.X;
            myDoubleScaleXAnimation.Duration = new Duration(TimeSpan.FromSeconds(anim_dur));
            Storyboard.SetTargetProperty(myDoubleScaleXAnimation, new PropertyPath("(UIElement.RenderTransform).(ScaleTransform.ScaleX)"));

            DoubleAnimation myDoubleScaleYAnimation = new DoubleAnimation();
            //myDoubleScaleYAnimation.From = dlgMeta.Dialog.ActualHeight;
            myDoubleScaleYAnimation.DecelerationRatio = 1;
            myDoubleScaleYAnimation.To = to_scale.Y;
            myDoubleScaleYAnimation.Duration = new Duration(TimeSpan.FromSeconds(anim_dur));
            Storyboard.SetTargetProperty(myDoubleScaleYAnimation, new PropertyPath("(UIElement.RenderTransform).(ScaleTransform.ScaleY)"));

            myStoryboard.Children.Add(myDoubleScaleXAnimation);
            myStoryboard.Children.Add(myDoubleScaleYAnimation);


            return myStoryboard;
        }

        public Storyboard GetDefaultMaximizeAnimation(OverlayWindowMeta meta, Point to_topleft, Point to_scale)
        {
            double anim_dur = 1;

            var trans = meta.DialogWindowScreenshot.RenderTransform as ScaleTransform;
            if (trans == null)
            {
                ScaleTransform s_trans = new ScaleTransform();
                meta.DialogWindowScreenshot.RenderTransform = s_trans;
                trans = s_trans;
            }

            to_scale.X *= trans.ScaleX;
            to_scale.Y *= trans.ScaleY;

            CheckPoint(ref to_topleft);
            CheckPoint(ref to_scale);

            DoubleAnimation myDoubleCanvasLeftAnimation = new DoubleAnimation();
            myDoubleCanvasLeftAnimation.DecelerationRatio = 1;
            myDoubleCanvasLeftAnimation.To = to_topleft.X;
            myDoubleCanvasLeftAnimation.Duration = new Duration(TimeSpan.FromSeconds(anim_dur));
            Storyboard.SetTargetProperty(myDoubleCanvasLeftAnimation, new PropertyPath(Canvas.LeftProperty));

            DoubleAnimation myDoubleCanvasTopAnimation = new DoubleAnimation();
            myDoubleCanvasTopAnimation.DecelerationRatio = 1;
            myDoubleCanvasTopAnimation.To = to_topleft.Y;
            myDoubleCanvasTopAnimation.Duration = new Duration(TimeSpan.FromSeconds(anim_dur));
            Storyboard.SetTargetProperty(myDoubleCanvasTopAnimation, new PropertyPath(Canvas.TopProperty));

            DoubleAnimation myDoubleScaleXAnimation = new DoubleAnimation();
            //myDoubleScaleXAnimation.From = dlgMeta.Dialog.ActualWidth;
            myDoubleScaleXAnimation.DecelerationRatio = 1;
            myDoubleScaleXAnimation.To = to_scale.X;
            myDoubleScaleXAnimation.Duration = new Duration(TimeSpan.FromSeconds(anim_dur));
            Storyboard.SetTargetProperty(myDoubleScaleXAnimation, new PropertyPath("(UIElement.RenderTransform).(ScaleTransform.ScaleX)"));

            DoubleAnimation myDoubleScaleYAnimation = new DoubleAnimation();
            //myDoubleScaleYAnimation.From = dlgMeta.Dialog.ActualHeight;
            myDoubleScaleYAnimation.DecelerationRatio = 1;
            myDoubleScaleYAnimation.To = to_scale.Y;
            myDoubleScaleYAnimation.Duration = new Duration(TimeSpan.FromSeconds(anim_dur));
            Storyboard.SetTargetProperty(myDoubleScaleYAnimation, new PropertyPath("(UIElement.RenderTransform).(ScaleTransform.ScaleY)"));

            Storyboard myStoryboard = new Storyboard();
            myStoryboard.Children.Add(myDoubleCanvasLeftAnimation);
            myStoryboard.Children.Add(myDoubleCanvasTopAnimation);
            myStoryboard.Children.Add(myDoubleScaleXAnimation);
            myStoryboard.Children.Add(myDoubleScaleYAnimation);

            return myStoryboard;
        }

        public Storyboard GetDefaultStartupAnimation(OverlayWindowMeta meta, Point from_topleft, Point to_topleft, Point from_scale, Point to_scale)
        {
            double anim_dur = 1;
            Storyboard sb = new Storyboard();

            ScaleTransform trans = meta.DialogWindowScreenshot.RenderTransform as ScaleTransform;
            if (trans == null)
            {
                meta.DialogWindowScreenshot.RenderTransform = new ScaleTransform();
            }
            else if (trans.ScaleX.IsNaN() || trans.ScaleY.IsNaN())
            {
                trans.ScaleX = 1;
                trans.ScaleY = 1;
            }

            CheckPoint(ref from_topleft);
            CheckPoint(ref to_topleft);
            CheckPoint(ref from_scale);
            CheckPoint(ref to_scale);

            DoubleAnimation myDoubleCanvasLeftAnimation = new DoubleAnimation();
            myDoubleCanvasLeftAnimation.DecelerationRatio = 1;
            if (from_topleft.X >= 0)
                myDoubleCanvasLeftAnimation.From = from_topleft.X;
            myDoubleCanvasLeftAnimation.To = to_topleft.X;
            myDoubleCanvasLeftAnimation.Duration = new Duration(TimeSpan.FromSeconds(anim_dur));
            Storyboard.SetTargetProperty(myDoubleCanvasLeftAnimation, new PropertyPath(Canvas.LeftProperty));

            DoubleAnimation myDoubleCanvasTopAnimation = new DoubleAnimation();
            myDoubleCanvasTopAnimation.DecelerationRatio = 1;
            if (from_topleft.Y >= 0)
                myDoubleCanvasTopAnimation.From = from_topleft.Y;
            myDoubleCanvasTopAnimation.To = to_topleft.Y;
            myDoubleCanvasTopAnimation.Duration = new Duration(TimeSpan.FromSeconds(anim_dur));
            Storyboard.SetTargetProperty(myDoubleCanvasTopAnimation, new PropertyPath(Canvas.TopProperty));

            DoubleAnimation myDoubleScaleXAnimation = new DoubleAnimation();
            //myDoubleScaleXAnimation.From = dlgMeta.Dialog.ActualWidth;
            myDoubleScaleXAnimation.DecelerationRatio = 1;
            if (from_scale.X >= 0)
                myDoubleScaleXAnimation.From = from_scale.X;
            myDoubleScaleXAnimation.To = to_scale.X;
            myDoubleScaleXAnimation.Duration = new Duration(TimeSpan.FromSeconds(anim_dur));
            Storyboard.SetTargetProperty(myDoubleScaleXAnimation, new PropertyPath("(UIElement.RenderTransform).(ScaleTransform.ScaleX)"));

            DoubleAnimation myDoubleScaleYAnimation = new DoubleAnimation();
            //myDoubleScaleYAnimation.From = dlgMeta.Dialog.ActualHeight;
            myDoubleScaleYAnimation.DecelerationRatio = 1;
            if (from_scale.Y >= 0)
                myDoubleScaleYAnimation.From = from_scale.Y;
            myDoubleScaleYAnimation.To = to_scale.Y;
            myDoubleScaleYAnimation.Duration = new Duration(TimeSpan.FromSeconds(anim_dur));
            Storyboard.SetTargetProperty(myDoubleScaleYAnimation, new PropertyPath("(UIElement.RenderTransform).(ScaleTransform.ScaleY)"));

            sb.Children.Add(myDoubleCanvasLeftAnimation);
            sb.Children.Add(myDoubleCanvasTopAnimation);
            sb.Children.Add(myDoubleScaleXAnimation);
            sb.Children.Add(myDoubleScaleYAnimation);

            //OverlayWindowMeta.OverlayAnimationProperties prop = new OverlayWindowMeta.OverlayAnimationProperties();
            //prop.FinalLeft = to_topleft.X;
            //prop.FinalTop = to_topleft.Y;
            //prop.FinalScalex = to_scale.X;
            //prop.FinalScaley = to_scale.Y;
            //meta.FinalAnimationProperties = prop;

            return sb;
        }

        public Storyboard GetDefaultStartupAnimation(OverlayWindowMeta meta, Point from_topleft, Point to_topleft, Point to_scale)
        {
            return GetDefaultStartupAnimation(meta, from_topleft, to_topleft, new Point(-1, -1), to_scale);
        }

        private static void CheckPoint(ref Point point_tocheck)
        {
            if (point_tocheck == null)
            {
                point_tocheck = new Point(1, 1);
                return;
            }

            if (Double.IsNegativeInfinity(point_tocheck.X) || Double.IsPositiveInfinity(point_tocheck.X) || Double.IsNaN(point_tocheck.X) || Double.IsInfinity(point_tocheck.X))
                point_tocheck.X = 1;
            if (Double.IsNegativeInfinity(point_tocheck.Y) || Double.IsPositiveInfinity(point_tocheck.Y) || Double.IsNaN(point_tocheck.Y) || Double.IsInfinity(point_tocheck.Y))
                point_tocheck.Y = 1;

        }

        public void MinimizeDialogToPosition(OverlayWindowMeta meta, Point from, Point to, bool linear_path)
        {
            MinimizeDialogToPosition(meta, from, to, linear_path, false);
        }
        public void MinimizeDialogToPosition(OverlayWindowMeta meta, Point from, Point to, bool linear_path, bool fade_out)
        {
            if (meta.Dialog.WindowState != OverlayDialogState.Minimized && meta.Dialog.WindowState != OverlayDialogState.Modal)
            {
                meta.WaitForAnimationFinished();
                double scale = 1;
                meta.Dialog.MinimizingAnimation = GetDefaultMinimizeAnimation(meta, from, to, linear_path, ref scale, false, fade_out);
                OverlayWindowMeta.OverlayAnimationProperties proper = new OverlayWindowMeta.OverlayAnimationProperties();
                proper.FinalLeft = to.X;
                proper.FinalTop = to.Y;
                proper.FinalScalex = scale;
                proper.FinalScaley = scale;
                meta.Dialog.MinimizingAnimationFinalPos = proper;
                meta.Dialog.MinimizeStateDialog();

            }
        }

        public void NormalizeDialogToPosition(OverlayWindowMeta meta, Point from, Point to, bool linear_path)
        {
            meta.WaitForAnimationFinished();
            meta.Dialog.NormalizingAnimation = GetDefaultNormalizeAnimation(from, to, new Point(1, 1), linear_path);
            OverlayWindowMeta.OverlayAnimationProperties prop = new OverlayWindowMeta.OverlayAnimationProperties();
            prop.FinalLeft = to.X;
            prop.FinalTop = to.Y;
            prop.FinalScalex = 1;
            prop.FinalScaley = 1;
            meta.Dialog.NormalizingAnimationFinalPos = prop;
            meta.Dialog.NormalStateDialog();
        }

        public void MinimizeAll()
        {
            MinimizeAll(new List<OverlayWindowMeta>());
        }

        public void MinimizeAll(OverlayWindowMeta except)
        {
            MinimizeAll(new List<OverlayWindowMeta> { except });
        }

        public void MinimizeAll(List<OverlayWindowMeta> exception_list)
        {
            if (OverlayManager != null)
            {
                foreach (OverlayWindowMeta dlgcheck in OverlayManager.DialogWindowList)
                {
                    if (dlgcheck.Dialog.WindowState != OverlayDialogState.Minimized && (exception_list == null || !exception_list.Contains(dlgcheck)))
                    {
                        dlgcheck.Dialog.MinimizeStateDialog();
                    }
                }
            }
        }

        public void AddMinizedWindow(OverlayWindowMeta meta)
        {
            if (meta == null)
                return;

            if (meta.Dialog.MinimizingAnimation != null)
                return;

            OverlayWindowMeta checkMeta = GetMetaForMinimizedDialog(meta.Dialog);

            if (checkMeta != null)
                return; // already in minimized state

            _minimized_windows.Add(meta);

            if (WindowTaskBar != null)
            {
                //SubMenuPanel sub_menu_panel = OverlayManager.WindowTaskBar.GetSubMenuPanelByTitle("Dialoge");
                ISubMenuPanelBase sub_menu_panel = GetSubMenuPanelByTitle(meta.Dialog.DialogContent.MinimizedWindowPanelName);
                
                sub_menu_panel.Action -= minimized_icon_clicked;
                sub_menu_panel.Action += new EventHandler<SubMenuPanelMenuItem.MenuItemActionEventArgs>(minimized_icon_clicked);

                Type panel_item_type = meta.Dialog.DialogContent.GetDialogSubmenuPanelItemType();
                if (!typeof(IDialogMenuItem).IsAssignableFrom(panel_item_type))
                    throw new ArgumentException("DialogSubmenuPanelItemType must inherit from DialogMenuItem");

                //DialogMenuItem animButton = new DialogMenuItem();
                IDialogMenuItem animButton = (IDialogMenuItem)Activator.CreateInstance(panel_item_type);

                //animButton.Margin = sub_menu_panel.ItemMargin;
                animButton.Width = sub_menu_panel.BigItemSize.Width - sub_menu_panel.ItemMargin.Left - sub_menu_panel.ItemMargin.Right;
                animButton.Height = sub_menu_panel.BigItemSize.Height - sub_menu_panel.ItemMargin.Top - sub_menu_panel.ItemMargin.Bottom;
                animButton.Tag = meta;
                meta.ClickedWindowTaskBarElement = null;

                meta.DialogThumbScreenshot.Visibility = Visibility.Visible;
                meta.ResetAnimatedProperties();
                meta.DialogWindowScreenshot.Opacity = 1;

                if(!string.IsNullOrEmpty(meta.Dialog.DialogContent.MinimizedWindowInfoText))
                {
                    animButton.SetContent(meta.DialogThumbScreenshot, meta.DialogWindowScreenshot.ActualWidth, meta.Dialog.DialogContent.MinimizedWindowInfoText);
                    if(meta.Dialog.DialogContent.MinimizedWindowInfoTextStyle != null)
                        animButton.SetInfoTextStyle(meta.Dialog.DialogContent.MinimizedWindowInfoTextStyle);
                }
                else
                {
                    animButton.SetContent(meta.DialogThumbScreenshot, meta.DialogWindowScreenshot.ActualWidth);    
                }

                AddSubMenuPanelItem(ref sub_menu_panel, animButton);
                //sub_menu_panel.AddItem(animButton);

                _metadata_button_matrix[meta] = animButton;
            }
            else if (OverlayManager.TaskbarPanel != null)
            {
                if (meta.TaskbarPlaceholder.Tag != null && meta.TaskbarPlaceholder.Tag is TaskbarPanel.TaskbarItem)
                {
                    meta.DialogThumbScreenshot.Visibility = Visibility.Visible;
                    TaskbarPanel.TaskbarItem item = (TaskbarPanel.TaskbarItem)meta.TaskbarPlaceholder.Tag;
                    meta.TaskbarPlaceholder.Opacity = 1;
                    OverlayManager.TaskbarPanel.ChangeItemContent(item.Container, meta.DialogThumbScreenshot);
                    meta.TaskbarPlaceholder.MouseUp += new MouseButtonEventHandler(TaskbarPlaceholder_MouseUp);
                    item.ReferenceObject = meta;

                    Point Final_Pos = OverlayManager.TaskbarPanel.GetElementPosition(meta.TaskbarPlaceholder);
                    Debug.WriteLine("Final-Taskbar-Pos: " + Final_Pos);
                }
            }
            else
            {
                Type panel_item_type = meta.Dialog.DialogContent.GetDialogSubmenuPanelItemType();
                if (!typeof(IDialogMenuItem).IsAssignableFrom(panel_item_type))
                    throw new ArgumentException("DialogSubmenuPanelItemType must inherit from DialogMenuItem");

                //DialogMenuItem animButton = new DialogMenuItem();
                IDialogMenuItem animButton = (IDialogMenuItem)Activator.CreateInstance(panel_item_type);

                animButton.Width = content_control.ItemWidth;
                animButton.Height = content_control.ItemHeight;
                animButton.Tag = meta;

                //animButton.Click += new EventHandler(animButton_Click);
                // Über die Viewbox wird die Margin zwischen den einzelnen Elementen gesteuert. Bis keine bessere Möglichkeit implementiert wird, das Template so lassen

                if (!string.IsNullOrEmpty(meta.Dialog.DialogContent.MinimizedWindowInfoText))
                {
                    animButton.SetContent(meta.DialogThumbScreenshot, content_control.ItemWidth * content_control.ItemScale, meta.Dialog.DialogContent.MinimizedWindowInfoText);
                    if (meta.Dialog.DialogContent.MinimizedWindowInfoTextStyle != null)
                        animButton.SetInfoTextStyle(meta.Dialog.DialogContent.MinimizedWindowInfoTextStyle);
                }
                else
                {
                    animButton.SetContent(meta.DialogThumbScreenshot, content_control.ItemWidth * content_control.ItemScale);
                }

                //buttonThumb.Content = meta.DialogThumbScreenshot;
                meta.DialogThumbScreenshot.Visibility = Visibility.Visible;

                _metadata_button_matrix[meta] = animButton;

                content_control.Children.Add(animButton as FrameworkElement);
                content_control.UpdateLayout();
            }
        }

        void sub_menu_panel_Action(object sender, SubMenuPanelMenuItem.MenuItemActionEventArgs e)
        {
            throw new NotImplementedException();
        }


        public void RemoveMinimizedWindow(OverlayWindowMeta meta)
        {
            if (!_minimized_windows.Contains(meta))
                return;

            if (WindowTaskBar != null)
            {
                meta.DialogThumbScreenshot.Visibility = Visibility.Hidden;
                //SubMenuPanel sub_menu_panel_jahre = OverlayManager.WindowTaskBar.GetSubMenuPanelByTitle("Dialoge");
                ISubMenuPanelBase sub_menu_panel_jahre = GetSubMenuPanelByTitle(meta.Dialog.DialogContent.MinimizedWindowPanelName);
                
                //_metadata_button_matrix[meta].Click -= animButton_Click;
                sub_menu_panel_jahre.RemoveItem(_metadata_button_matrix[meta] as FrameworkElement);
                _metadata_button_matrix[meta].ResetContent();
            }
            else if (OverlayManager.TaskbarPanel == null)
            {
                meta.DialogThumbScreenshot.Visibility = Visibility.Hidden;
                //_metadata_button_matrix[meta].Click -= animButton_Click;

                content_control.Children.Remove(_metadata_button_matrix[meta] as FrameworkElement);
                _metadata_button_matrix[meta].ResetContent();

                content_control.UpdateLayout();
            }
            else
            {
                OverlayManager.TaskbarPanel.RemoveItem(meta.TaskbarPlaceholder, true);
            }

            _minimized_windows.Remove(meta);

        }

        private OverlayWindowMeta GetMetaForMinimizedDialog(OverlayDialog d)
        {
            return _minimized_windows.Where(x => x.Dialog == d).SingleOrDefault();
        }
        #endregion

        #region state snapshot management
        public bool ContainsSnapshot(string snapshot_name)
        {
            return _state_snapshots.ContainsKey(snapshot_name);
        }

        public bool CreateSnapshot(string snapshot_name)
        {
            // wenn der snapshot schon existiert -> false
            if (ContainsSnapshot(snapshot_name))
                return false;

            _state_snapshots[snapshot_name] = new List<StateSnapshotInfo>();

            foreach (OverlayWindowMeta curMeta in OverlayManager.DialogWindowList)
            {
                StateSnapshotInfo info = new StateSnapshotInfo();
                info.DialogMeta = curMeta;
                info.SnapshotState = curMeta.Dialog.WindowState;
                info.SnapshotActiveState = curMeta.IsActive;

                _state_snapshots[snapshot_name].Add(info);
            }

            return true;
        }

        public bool RemoveSnapshot(string snapshot_name)
        {
            if (!ContainsSnapshot(snapshot_name))
                return false;

            _state_snapshots[snapshot_name].Clear();
            _state_snapshots[snapshot_name] = null;
            _state_snapshots.Remove(snapshot_name);
            return true;
        }

        public bool RemoveFromSnapshot(string snapshot_name, OverlayWindowMeta removeMeta)
        {
            if (!ContainsSnapshot(snapshot_name))
                return false;

            StateSnapshotInfo info = _state_snapshots[snapshot_name].Where(s => s.DialogMeta == removeMeta).SingleOrDefault();

            if (info != null)
            {
                _state_snapshots[snapshot_name].Remove(info);
                if (_state_snapshots[snapshot_name].Count <= 0)
                    _state_snapshots.Remove(snapshot_name);

                return true;
            }

            return false;
        }

        public void ClearAllSnapshots()
        {
            List<string> snapshot_names = _state_snapshots.Keys.ToList<string>();
            foreach (string snapshot in snapshot_names)
                RemoveSnapshot(snapshot);
        }

        public bool RecoverSnapshotStates(string snapshot_name, bool clear_snapshot_after_recovery)
        {
            if (!ContainsSnapshot(snapshot_name))
                return false;

            foreach (OverlayWindowMeta curMeta in OverlayManager.DialogWindowList)
            {
                StateSnapshotInfo oldSates = _state_snapshots[snapshot_name].Where(s => s.DialogMeta == curMeta).SingleOrDefault();

                if (oldSates != null)
                {
                    if (oldSates.SnapshotState != curMeta.Dialog.WindowState)
                    {
                        switch (oldSates.SnapshotState)
                        {
                            case OverlayDialogState.Maximized:
                                {
                                    curMeta.Dialog.MaximizeStateDialog();
                                    continue;
                                }; break;
                            case OverlayDialogState.Normal:
                                {
                                    curMeta.Dialog.NormalStateDialog();
                                    continue;
                                }; break;
                            case OverlayDialogState.Minimized:
                                {
                                    curMeta.Dialog.MinimizeStateDialog();
                                    continue;
                                }; break;
                            case OverlayDialogState.Modal:
                                {
                                    curMeta.Dialog.ModalStateDialog();
                                    continue;
                                }; break;
                        }
                    }
                }
            }

            if (clear_snapshot_after_recovery)
                RemoveSnapshot(snapshot_name);

            return true;
        }
        #endregion

        #region Events

        void TaskbarPlaceholder_MouseUp(object sender, MouseButtonEventArgs e)
        {
            //MessageBox.Show("glick!");
            StackPanel btnThumb = sender as StackPanel;

            if (btnThumb != null)
            {
                TaskbarPanel.TaskbarItem item = btnThumb.Tag as TaskbarPanel.TaskbarItem;
                OverlayWindowMeta meta = item.ReferenceObject as OverlayWindowMeta;


                if (meta != null)
                {
                    ScaleTransform s_tran = meta.DialogWindowScreenshot.RenderTransform as ScaleTransform;

                    // normalize window

                    if (s_tran != null)
                    {
                        //s_tran.ScaleX *= btnThumb.ScaleX;
                        //s_tran.ScaleY *= btnThumb.ScaleY;
                    }

                    meta.Dialog.NormalStateDialog();
                    meta.SetActiveAfterStateChange = true;
                }
            }
        }

        private void minimized_icon_clicked(object sender, SubMenuPanelMenuItem.MenuItemActionEventArgs e)
        {
            //MessageBox.Show("glick!");
            SubMenuPanelMenuItem btnThumb = sender as SubMenuPanelMenuItem;

            if (btnThumb != null)
            {
                OverlayWindowMeta meta = btnThumb.Tag as OverlayWindowMeta;


                if (meta != null)
                {
                    //ScaleTransform s_tran = meta.DialogWindowScreenshot.RenderTransform as ScaleTransform;

                    //// normalize window

                    //if (s_tran != null)
                    //{
                    //    s_tran.ScaleX *= btnThumb.ScaleX;
                    //    s_tran.ScaleY *= btnThumb.ScaleY;
                    //}

                    meta.ClickedWindowTaskBarElement = btnThumb;
                    meta.Dialog.NormalStateDialog();
                    meta.SetActiveAfterStateChange = true;
                }
            }
        }

        private void content_control_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            RepositionWindowManager();
        }
        #endregion

        #region Properties
        public List<OverlayWindowMeta> MinimizedWindows
        {
            get { return _minimized_windows; }
        }

        /// <summary>
        /// Liefert die Instanz des OverlayManagers für dieses content control
        /// </summary>
        /// <remarks>Die Eigenschaft kann auch einen NULL Wert liefern, wenn das Control nicht im OverlayManager angezeigt wird.</remarks>
        private IOverlayManagerBase OverlayManager
        {
            get
            {
                if (_overlay_manager != null)
                    return _overlay_manager as IOverlayManagerBase;

                DependencyObject o = UIHelper.TryFindParentGeneric(this, typeof(OverlayManagerBase<BaseFrame, SubMenuPanel, WindowTaskBar, WindowTitleBar, OverlayManager, SubMenuPanelMenuItem>));
                if (o != null)
                    _overlay_manager = o;

                return _overlay_manager as IOverlayManagerBase;
                //return UIHelper.TryFindParent<OverlayManager>(this);
            }
        }

        private IWindowTaskBarBase WindowTaskBar
        {
            get
            {
                if (_task_bar != null)
                    return _task_bar;

                if (OverlayManager == null)
                    return null;

                Type t = _overlay_manager.GetType();

                PropertyInfo p_info = t.GetProperty("WindowTaskBar");
                if (p_info != null)
                {
                    object taskbar = p_info.GetValue(_overlay_manager, null);

                    if (taskbar != null && taskbar is IWindowTaskBarBase)
                        _task_bar = taskbar as IWindowTaskBarBase;
                }

                return _task_bar;
            }
        }

        private ISubMenuPanelBase GetSubMenuPanelByTitle(string s)
        {
            if (WindowTaskBar == null)
                return null;

            if (_add_sub_menu_panel_item_mi == null)
            {
                Type t = WindowTaskBar.GetType();
                _add_sub_menu_panel_item_mi = t.GetMethod("GetSubMenuPanelByTitle", new Type[] { typeof(string) });
                
            }

            object o = null;
            if (_add_sub_menu_panel_item_mi != null)
                o = _add_sub_menu_panel_item_mi.Invoke(WindowTaskBar, new object[] { s });

            if (o == null)
                return null;

            return o as ISubMenuPanelBase;
        }

        private void AddSubMenuPanelItem(ref ISubMenuPanelBase panel, ISubMenuPanelMenuItem menu_item)
        {            
            if (_get_sub_menu_panel_by_title_mi == null)
            {
                Type t = panel.GetType();
                _get_sub_menu_panel_by_title_mi = t.GetMethod("AddItem");
            }

            if (_get_sub_menu_panel_by_title_mi != null)
                _get_sub_menu_panel_by_title_mi.Invoke(panel, new object[] { menu_item });
            
        }
        #endregion

        #region attribs
        private MethodInfo _add_sub_menu_panel_item_mi = null;
        private MethodInfo _get_sub_menu_panel_by_title_mi = null;

        private DependencyObject _overlay_manager = null;
        private IWindowTaskBarBase _task_bar = null;
        private Size _parent_canvas_size;
        private List<OverlayWindowMeta> _minimized_windows = new List<OverlayWindowMeta>();
        private Dictionary<OverlayWindowMeta, IDialogMenuItem> _metadata_button_matrix = new Dictionary<OverlayWindowMeta, IDialogMenuItem>();

        private Dictionary<string, List<StateSnapshotInfo>> _state_snapshots = new Dictionary<string, List<StateSnapshotInfo>>();
        #endregion


    }
}
