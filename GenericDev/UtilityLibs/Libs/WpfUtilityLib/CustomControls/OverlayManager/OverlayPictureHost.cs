﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Logicx.WpfUtility.CustomControls.OverlayManager
{
    /// <summary>
    /// Die Picture Host Klasse hält den Screenshot der Dialoge und eventuelle zusätzliche Informationscontrols
    /// </summary>
    public class OverlayPictureHost : Grid
    {
        #region Constructor
        static OverlayPictureHost()
        {
            IsAnimatingPropertyKey = DependencyProperty.RegisterReadOnly("IsAnimating", typeof(bool), typeof(OverlayPictureHost), new FrameworkPropertyMetadata(false, FrameworkPropertyMetadataOptions.NotDataBindable));
            IsAnimatingProperty = IsAnimatingPropertyKey.DependencyProperty;

            IsDragingPropertyKey = DependencyProperty.RegisterReadOnly("IsDraging", typeof(bool), typeof(OverlayPictureHost), new FrameworkPropertyMetadata(false, FrameworkPropertyMetadataOptions.NotDataBindable));
            IsDragingProperty = IsDragingPropertyKey.DependencyProperty;
        }

        public OverlayPictureHost()
        {
            ShadowScale = 1;
            //ColumnDefinition coldef = new ColumnDefinition();
            //coldef.Width = GridLength.Auto;
            //RowDefinition rowdef = new RowDefinition();
            //rowdef.Height = GridLength.Auto;

            //ColumnDefinitions.Add(coldef);
            //RowDefinitions.Add(rowdef);

            //this.Background = Brushes.Magenta;

            Binding bindingWidth = new Binding("ActualWidth");
            bindingWidth.Source = this;
            _screenshot_image.SetBinding(WidthProperty, bindingWidth);

            Binding bindingHeight = new Binding("ActualHeight");
            bindingHeight.Source = this;
            _screenshot_image.SetBinding(HeightProperty, bindingHeight);

            Children.Add(_screenshot_image);
        }
        #endregion

        #region Operations
        public void SetImage(RenderTargetBitmap img) //(BitmapImage img) //
        {
            if (img == null)
                return;

            ScaleTransform trans = this.RenderTransform as ScaleTransform;
            double old_sx = 1, old_sy = 1;
            if(trans != null)
            {
                old_sx = trans.ScaleX;
                old_sy = trans.ScaleY;
                trans.ScaleX = 1;
                trans.ScaleY = 1;
            }

            Canvas.SetLeft(_screenshot_image, 0);
            Canvas.SetTop(_screenshot_image, 0);
            System.Windows.Visibility oldVisibility = ScreenshotImage.Visibility;
            _screenshot_image.Visibility = System.Windows.Visibility.Hidden;
            FreeImageResource();
            img.Freeze();
            _screenshot_image.Source = img;
            _screenshot_image.Visibility = oldVisibility;
            AddElement(_screenshot_image, 0);

            if(trans != null)
            {
                trans.ScaleX = old_sx;
                trans.ScaleY = old_sy;
            }
            //UpdateLayout();
        }

        public void SetImage(Image img)
        {
            if (img == null)
                return;

            Canvas.SetLeft(ScreenshotImage, 0);
            Canvas.SetTop(ScreenshotImage, 0);
            System.Windows.Visibility oldVisibility = ScreenshotImage.Visibility;
            FreeImageResource();
            _screenshot_image = img;
            AddElement(_screenshot_image, 0);
            ScreenshotImage.Visibility = oldVisibility;
        }

        public void SetImage(BitmapImage img)
        {
            if (img == null)
                return;

            Canvas.SetLeft(ScreenshotImage, 0);
            Canvas.SetTop(ScreenshotImage, 0);
            System.Windows.Visibility oldVisibility = ScreenshotImage.Visibility;
            FreeImageResource();
            _screenshot_image.Source = img;
            AddElement(_screenshot_image, 0);
            ScreenshotImage.Visibility = oldVisibility;
        }

        public void AddElement(FrameworkElement elem)
        {
            AddElement(elem, -1);
        }

        public void AddElement(FrameworkElement elem, int index)
        {
            if (index < 0)
                Children.Add(elem);
            else
                Children.Insert(index, elem);
        }

        public void SetImagePosition(double left, double top)
        {
            Canvas.SetLeft(ScreenshotImage, left);
            Canvas.SetTop(ScreenshotImage, top);
        }

        public void FreeImageResource()
        {
            FreeImageResource(false);
        }

        public void FreeImageResource(bool free_forever)
        {
            Children.Remove(_screenshot_image);
            if (ScreenshotImage.Source != null)
            {
                if (ScreenshotImage.Source is BitmapImage)
                {
                    ((BitmapImage) ScreenshotImage.Source).StreamSource.Dispose();
                    //((BitmapImage)ScreenshotImage.Source).StreamSource = null;
                }

                BindingOperations.ClearAllBindings(_screenshot_image);

                try
                {
                    ScreenshotImage.Source = null;
                }catch{}

                if (free_forever)
                    return;
                    
                _screenshot_image = new Image();

                Binding bindingWidth = new Binding("ActualWidth");
                bindingWidth.Source = this;
                _screenshot_image.SetBinding(WidthProperty, bindingWidth);

                Binding bindingHeight = new Binding("ActualHeight");
                bindingHeight.Source = this;
                _screenshot_image.SetBinding(HeightProperty, bindingHeight);

                UpdateLayout();
            }  

            //if (ScreenshotImage.Source != null && ScreenshotImage.Source is BitmapImage)
            //{
            //    if (((BitmapImage)ScreenshotImage.Source).StreamSource != null)
            //    {
            //        ((BitmapImage)ScreenshotImage.Source).StreamSource.Dispose();
            //        ((BitmapImage)ScreenshotImage.Source).StreamSource = null;

            //    }

            //    ScreenshotImage.Source = null;
            //    UpdateLayout();
            //}
        }

        public void SetInformationControl(UIElement ctrl)
        {
            RemoveInformationControl(false);
            
            if (ctrl != null)
                if (!Children.Contains(ctrl))
                {
                    Children.Add(ctrl);
                    _information_control = ctrl;
                }
            
            UpdateLayout();
        }

        public void RemoveInformationControl(bool with_update_layout)
        {
            if (_information_control != null)
                if (Children.Contains(_information_control))
                {
                    Children.Remove(_information_control);
                    BindingOperations.ClearAllBindings(_information_control);
                    if (with_update_layout)
                        UpdateLayout();
                }

            _information_control = null;
        }
        #endregion

        #region property key setters
        internal void SetIsAnimating(bool value)
        {
            SetValue(IsAnimatingPropertyKey, value);
        }

        internal void SetIsDraging(bool value)
        {
            SetValue(IsDragingPropertyKey, value);
        }
        #endregion

        #region Properties
        public Image ScreenshotImage
        {
            get { return _screenshot_image; }
        }

        public bool IsAnimating
        {
            get { return (bool) GetValue(IsAnimatingProperty); }
        }

        public bool IsDraging
        {
            get { return (bool)GetValue(IsDragingProperty); }
        }

        public double CalculatedWidth
        {
            get
            {
                if(RendersShadow)
                {
                    if (ImageRenderOffsetWidth > OverlayManager.OVERLAY_SHADOW_IMAGE_OFFSET_X)
                        return ActualWidth - ImageRenderOffsetWidth;
                    else
                        return ActualWidth - OverlayManager.OVERLAY_SHADOW_IMAGE_OFFSET_X;
                }
                else
                {
                    return ActualWidth - ImageRenderOffsetWidth;
                }
            }
        }

        public double CalculatedHeight
        {
            get
            {
                if (RendersShadow)
                {
                    if (ImageRenderOffsetHeight > OverlayManager.OVERLAY_SHADOW_IMAGE_OFFSET_Y)
                        return ActualHeight - ImageRenderOffsetHeight;
                    else
                        return ActualHeight - OverlayManager.OVERLAY_SHADOW_IMAGE_OFFSET_Y;
                }
                else
                {
                    return ActualHeight - ImageRenderOffsetHeight; // (ImageRenderOffsetHeight * ShadowScale);
                }
            }
        }

        public bool RendersShadow { get; set; }
        public double ShadowScale { get; set; }

        public double ImageRenderOffsetWidth { get; set; }
        public double ImageRenderOffsetHeight { get; set; }
        #endregion

        #region Dependency Properties
        public static readonly DependencyProperty IsAnimatingProperty;
        private static readonly DependencyPropertyKey IsAnimatingPropertyKey;

        public static readonly DependencyProperty IsDragingProperty;
        private static readonly DependencyPropertyKey IsDragingPropertyKey;
        #endregion

        #region Attribs
        private Image _screenshot_image = new Image { Visibility = System.Windows.Visibility.Visible };
        private UIElement _information_control = null;
        #endregion
    }
}
