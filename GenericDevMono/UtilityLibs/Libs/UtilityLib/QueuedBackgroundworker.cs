﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Threading;



namespace Logicx.Utilities
{
    // Zusammenfassung:
    //     Stellt Daten für den System.ComponentModel.BackgroundWorker.DoWork-Ereignishandler
    //     bereit.
    [Serializable]
    public class DoWorkEventArgs : EventArgs
    {
        // Zusammenfassung:
        //     Initialisiert eine neue Instanz der System.ComponentModel.DoWorkEventArgs-Klasse.
        //
        // Parameter:
        //   argument:
        //     Gibt ein Argument für einen asynchronen Vorgang an.
        public DoWorkEventArgs(object argument)
        {
            _argument = argument;
        }

        ~DoWorkEventArgs()
        {
            ClearData();
        }

        internal void ClearData()
        {
            _argument = null;
            _result = null;
        }
        // Zusammenfassung:
        //     Ruft einen Wert ab, der das Argument eines asynchronen Vorgangs darstellt.
        //
        // Rückgabewerte:
        //     Ein System.Object, das das Argument eines asynchronen Vorgangs darstellt.
        public object Argument
        {
            get
            {
                return _argument;
            }
        }
        //
        // Zusammenfassung:
        //     Ruft einen Wert ab, der das Ergebnis eines asynchronen Vorgangs darstellt,
        //     oder legt diesen fest.
        //
        // Rückgabewerte:
        //     Ein System.Object, das das Ergebnis eines asynchronen Vorgangs darstellt.
        public object Result
        {
            get
            {
                return _result;
            }
            set
            {
                _result = value;
            }
        }

        private object _result;
        private object _argument;
    }

    // Zusammenfassung:
    //     Stellt Daten für das MethodNameCompleted-Ereignis zur Verfügung.
    [Serializable]
    public class RunWorkerCompletedEventArgs : EventArgs
    {
        // Zusammenfassung:
        //     Initialisiert eine neue Instanz der System.ComponentModel.RunWorkerCompletedEventArgs-Klasse.
        //
        // Parameter:
        //   error:
        //     Ein während des asynchronen Vorgangs aufgetretener Fehler.
        //
        //   cancelled:
        //     Ein Wert, der angibt, ob der asynchrone Vorgang abgebrochen wurde.
        //
        //   result:
        //     Das Ergebnis eines asynchronen Vorgangs.
        public RunWorkerCompletedEventArgs(object result, Exception error)
        {
            _result = result;
            _error = error;
        }

        ~RunWorkerCompletedEventArgs()
        {
            ClearData();
        }

        internal void ClearData()
        {
            _result = null;
            _error = null;
        }

        // Zusammenfassung:
        //     Ruft einen Wert ab, der das Ergebnis eines asynchronen Vorgangs darstellt.
        //
        // Rückgabewerte:
        //     Ein System.Object, das das Ergebnis eines asynchronen Vorgangs darstellt.
        //
        // Ausnahmen:
        //   System.InvalidOperationException:
        //     System.ComponentModel.AsyncCompletedEventArgs.Cancelled ist true.
        //
        //   System.Reflection.TargetInvocationException:
        //     System.ComponentModel.AsyncCompletedEventArgs.Error ist nicht null. Die System.Exception.InnerException-Eigenschaft
        //     enthält einen Verweis auf System.ComponentModel.AsyncCompletedEventArgs.Error.
        public object Result
        {
            get
            {
                return _result;
            }
        }
        public Exception Error
        {
            get
            {
                return _error;
            }
        }

        private object _result;
        private Exception _error;
    }

    [Serializable]
    public class QueuedBackgroundworker : IDisposable
    {
        #region BwThread
        [Serializable]
        private class BwThread
        {
            public BwThread()
            {
            }

            #region Properties
            public object Parameter
            {
                set
                {
                    _parameter = value;
                }

                get
                {
                    return _parameter;
                }
            }

            public Thread Thread
            {
                set
                {
                    _thread = value;
                }

                get
                {
                    return _thread;
                }
            }

            public QueuedBackgroundworker Parent
            {
                set
                {
                    _parent = value;
                }

                get
                {
                    return _parent;
                }
            }

            public string Name
            {
                get { return _name; }
                set { _name = value; }
            }
            #endregion

            [NonSerialized]
            private Thread _thread;
            private object _parameter;
            private QueuedBackgroundworker _parent;
            private string _name;

            public bool IsBackgroundThread { get; set; }
        }
        #endregion

        /// <summary>
        /// Initialisiert eine neue Instanz der QueueBackgroundWorker-Klasse.
        /// </summary>
        public QueuedBackgroundworker()
        {
        }

        /// <summary>
        /// Initialisiert eine neue Instanz der QueueBackgroundWorker-Klasse.
        /// </summary>
        /// <param name="name">Name des Threads</param>
        public QueuedBackgroundworker(string name)
        {
            _name = name;
        }

        public QueuedBackgroundworker(string name, bool is_background_thread)
        {
            _is_background_thread = is_background_thread;
            _name = name;
        }

        /// <summary>
        ///     Ruft einen Wert ab, der angibt, ob System.ComponentModel.BackgroundWorker
        ///     einen asynchronen Vorgang ausführt.
        /// Rückgabewerte:
        ///     true, wenn der System.ComponentModel.BackgroundWorker einen asynchronen Vorgang
        ///     ausführt, andernfalls false.
        /// </summary>
        public bool IsBusy
        {
            get
            {
                return _isbusy;
            }
        }

        /// <summary>
        /// Method Join
        /// </summary>
        public void Join()
        {
            // im join darf sich immer nur ein thread aufhalten
            lock (this)
            {
                lock (_queue)
                {
                    if (_queue.Count == 0 && _bwthread == null)
                        return;
                }

                // wenn der thread  == null ist, befindet sich das workitem noch immer in der queue
                // das heisst der thread hat noch nicht gestartet
                // deswegen muss man hier auf die creation des threads warten
                if (_bwthread.Thread == null)
                {
                    //create queue
                    lock (_queue)
                    {
                        _manualresetevent_threadstarted = new ManualResetEvent(false);
                    }

                    _manualresetevent_threadstarted.WaitOne();

                    //destroy queue
                    lock (_queue)
                    {
                        _manualresetevent_threadstarted.Close();
                        _manualresetevent_threadstarted = null;
                    }
                }
                _bwthread.Thread.Join();
            }
        }

        /// <summary>
        /// hoffe dieses abort funkt so, habe diese methode viel später eingebaut
        /// weiss nicht genau ob ich den zusammenhange noch checke?!?
        /// </summary>
        public void Abort()
        {
            if (!_isbusy)
                return;

            _bwthread.Thread.Abort();
        }

        public Thread Thread
        {
            get { return _bwthread.Thread; }
        }


        #region DoWork
        /// <summary>
        ///     Tritt ein, wenn System.ComponentModel.BackgroundWorker.RunWorkerAsync() aufgerufen
        ///     wird.
        /// </summary>
        public event DoWorkEventHandler DoWork;
        public delegate void DoWorkEventHandler(object sender, DoWorkEventArgs e);
        /// <summary>
        /// Löst das System.ComponentModel.BackgroundWorker.DoWork-Ereignis aus.
        /// Parameter: 
        /// Ein System.EventArgs, das die Ereignisdaten enthält.
        /// </summary>
        /// <param name="e"></param>
        protected virtual void OnDoWork(DoWorkEventArgs e)
        {
            if (DoWork != null)
                DoWork(this, e);
        }
        #endregion

        #region RunWorkerCompleted
        /// <summary>
        ///    Tritt ein, wenn der Hintergrundvorgang entweder abgeschlossen ist, abgebrochen
        ///    wurde oder eine Ausnahme ausgelöst hat.
        /// </summary>
        public event RunWorkerCompletedEventHandler RunWorkerCompleted;
        public delegate void RunWorkerCompletedEventHandler(object sender, RunWorkerCompletedEventArgs e);
        /// <summary>
        /// Löst das System.ComponentModel.BackgroundWorker.RunWorkerCompleted-Ereignis.
        /// aus.
        /// Parameter: Ein System.EventArgs, das die Ereignisdaten enthält.
        /// </summary>
        /// <param name="e"></param>
        protected virtual void OnRunWorkerCompleted(RunWorkerCompletedEventArgs e)
        {
            if (RunWorkerCompleted != null)
                RunWorkerCompleted(this, e);
        }
        #endregion


        /// <summary>
        /// Zusammenfassung:
        ///     Startet die Ausführung eines Hintergrundvorgangs.
        ///
        /// Ausnahmen:
        ///   System.InvalidOperationException:
        ///     System.ComponentModel.BackgroundWorker.IsBusy ist true.        
        /// </summary>
        public void RunWorkerAsync()
        {
            RunWorkerAsync(null);
        }
        /// <summary>
        /// Zusammenfassung:
        ///     Startet die Ausführung eines Hintergrundvorgangs.
        ///
        /// Parameter:
        ///   argument:
        ///     Ein Parameter, der von dem Hintergrundvorgang verwendet wird, der im System.ComponentModel.BackgroundWorker.DoWork-Ereignishandler
        ///     ausgeführt wird.
        ///
        /// Ausnahmen:
        ///   System.InvalidOperationException:
        ///     System.ComponentModel.BackgroundWorker.IsBusy ist true.
        /// </summary>
        /// <param name="argument"></param>
        public void RunWorkerAsync(object argument)
        {
            _bwthread = new BwThread();
            _bwthread.IsBackgroundThread = _is_background_thread;
            _bwthread.Parent = this;
            _bwthread.Parameter = argument;
            _bwthread.Name = _name;
            _queue.Enqueue(_bwthread);
            RunBwManager();
        }

        /// <summary>
        /// Method RunBwManager
        /// </summary>
        private void RunBwManager()
        {
            while (true)
            {
                lock (_queue)
                {
                    if (_queue.Count > 0 && _count_running_threads < _max_count_running_threads)
                    {
                        //start threads from queue
                        BwThread bwthread = (BwThread)_queue.Dequeue();

                        if (bwthread == null)
                            break;

                        Thread t = new Thread(new ParameterizedThreadStart(RunWorkerAsyncExecution));
                        t.Name = bwthread.Name;
                        t.IsBackground = bwthread.IsBackgroundThread;
                        bwthread.Thread = t;
                        _count_running_threads++;
                        t.Start(bwthread);

                        //hier muss gecheckt werden ob ein join aufgerufen wurde für diesen thread
                        //dies kann ich überprüfen in dem ich schau ob jemand ein manual reset event angelegt hat
                        // das thread started signal ist nur dann interessant wenn jemand im join wartet
                        if (bwthread.Parent._manualresetevent_threadstarted != null)
                        {
                            bwthread.Parent._manualresetevent_threadstarted.Set();
                        }

                    }
                    else
                        break;
                }
            }
        }


        private void RunWorkerAsyncExecution(object parameter)
        {
            BwThread bwthread = (BwThread)parameter;
            DoWorkEventArgs e = new DoWorkEventArgs(bwthread.Parameter);
            Exception error = null;
            try
            {
                //set busy flag
                bwthread.Parent._isbusy = true;
                //execute thread logic  
                bwthread.Parent.OnDoWork(e);
            }
            catch (Exception ex)
            {
                error = ex;

#if DEBUG
                //MessageBox.Show(ex.ToString());
                System.Diagnostics.Debug.WriteLine(ex.ToString());
#endif
            }

            RunWorkerCompletedEventArgs re = null;
            try
            {
                //unset busy flag
                bwthread.Parent._isbusy = false;
                //start finished handler
                re = new RunWorkerCompletedEventArgs(e.Result, error);
                bwthread.Parent.Result = e.Result;
                bwthread.Parent.Error = error;

                bwthread.Parent.OnRunWorkerCompleted(re);

            }
            catch (Exception ex)
            {
#if DEBUG
                //MessageBox.Show(ex.ToString());
                System.Diagnostics.Debug.WriteLine(ex.ToString());
#endif
            }
            finally
            {
                if (re != null)
                {
                    re.ClearData();
                    re = null;
                }
                e.ClearData();
                e = null;

                bwthread.Parameter = null;
            }

            //exit from this thread 
            lock (_queue)
            {
                _count_running_threads--;
            }
            RunBwManager();
        }

        #region IDisposable Members

        public void Dispose()
        {
            _result = null;
            _error = null;
        }

        #endregion

        /// <summary>
        /// after thread execution the result attribs is made access via this property.
        /// this result is also return with the runworkerfinished event
        /// </summary>
        public object Result
        {
            set
            {
                _result = value;
            }

            get
            {
                return _result;
            }
        }

        public Exception Error
        {
            set
            {
                _error = value;
            }

            get
            {
                return _error;
            }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        #region Attributes
        private static Queue _queue = new Queue();
        private static int _max_count_running_threads = 1000;
        private static int _count_running_threads = 0;
        private bool _isbusy;
        private BwThread _bwthread;
        private ManualResetEvent _manualresetevent_threadstarted;
        private object _result;
        private Exception _error;
        private string _name = "";
        private bool _is_background_thread;
        #endregion


    }
}
