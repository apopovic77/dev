﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace Logicx.WpfUtility.CustomControls.ChangeInfo
{
    public enum ChangeType
    {
        Insert,
        Update,
        Delete
    }

    public class ChangeInfo
    {
        #region Properties
        /// <summary>
        /// Type of the change
        /// </summary>
        public ChangeType ChangeType
        {
            get { return _change_type; }
            set { _change_type = value; }
        }
        /// <summary>
        /// Gets/sets the control which caused the error
        /// </summary>
        public DependencyObject SourceControl { get; set; }
        /// <summary>
        /// Gets/sets the validation group parent control
        /// </summary>
        public DependencyObject ValidationGroupParent { get; set; }
        /// <summary>
        /// Gets/sets the error source name if set (if not set, the source control's name will be used)
        /// </summary>
        public string SourceName { get; set; }
        /// <summary>
        /// Item index
        /// </summary>
        public int ItemIndex { get; set; }

        /// <summary>
        /// Date and time of the change
        /// </summary>
        public DateTime? ChangeDate { get; set; }
        /// <summary>
        /// Username who did the change
        /// </summary>
        public string ChangeUsername { get; set;}
        /// <summary>
        /// Name of the field which changed
        /// </summary>
        public string FieldName { get; set;}
        /// <summary>
        /// Name of the property which represents the field in the current data context
        /// </summary>
        public string PropertyName { get; set; }
        /// <summary>
        /// Old value of the field
        /// </summary>
        public object OldValue { get; set; }
        /// <summary>
        /// New value of the field
        /// </summary>
        public object NewValue { get; set; }
        /// <summary>
        /// Datatype of the value
        /// </summary>
        public Type ValueType { get; set; }

        /// <summary>
        /// Flag indicating if the OldValue field holds the original value of the change chain
        /// </summary>
        public bool HasOriginalValue { get; set; }
        /// <summary>
        /// Flag indicating if the NewValue field holds the current value within the change chain
        /// </summary>
        public bool HasCurrentValue { get; set; }

        public long ChangeKey { get; set;}
        /// <summary>
        /// Data object from store
        /// </summary>
        public object SourceObject { get; set; }
        /// <summary>
        /// Data object key
        /// </summary>
        public object SourceObjectKey { get; set; }
        /// <summary>
        /// Type of the source data object
        /// </summary>
        public Type SourceType { get; set; }
        /// <summary>
        /// Row guid from store
        /// </summary>
        public Guid RowGuid { get; set; }

        /// <summary>
        /// Flag indicating if the ChangeInformationProvider mapped this change to a field within the visual tree.
        /// </summary>
        public bool MappedChangedToField { get; set; }
        #endregion

        #region Operations
        #endregion

        #region Attribs

        private ChangeType _change_type = ChangeType.Update;
        #endregion
    }
}
