﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Timers;
using System.Windows.Threading;

namespace Logicx.WpfUtility.CustomControls.OverlayManager.StatusInfo
{
    /// <summary>
    /// Interaction logic for OverlayBlockingProgressStatusInfo.xaml
    /// </summary>
    public partial class OverlayBlockingProgressStatusInfo : UserControl, IDisposable
    {
        static OverlayBlockingProgressStatusInfo()
        {
            FrameworkPropertyMetadata metadataFilledBackgroundBrush = new FrameworkPropertyMetadata(new SolidColorBrush(Color.FromArgb(0xFF, 0xFF, 0xFF, 0xFF)));
            FilledBackgroundBrushProperty = DependencyProperty.Register("FilledBackgroundBrush",
                                                            typeof(Brush), typeof(OverlayBlockingProgressStatusInfo), metadataFilledBackgroundBrush);

            FrameworkPropertyMetadata metadataColorBgBrushBrush = new FrameworkPropertyMetadata(new SolidColorBrush(Color.FromArgb(0x33, 0x6B, 0x6B, 0x6B)));
            ColorBgBrushBrushProperty = DependencyProperty.Register("ColorBgBrushBrush",
                                                            typeof(Brush), typeof(OverlayBlockingProgressStatusInfo), metadataColorBgBrushBrush);

            FrameworkPropertyMetadata metadataTextBrush = new FrameworkPropertyMetadata(new SolidColorBrush(Color.FromArgb(0xFF, 0x77, 0x77, 0x77)));
            TextBrushProperty = DependencyProperty.Register("TextBrush",
                                                            typeof(Brush), typeof(OverlayBlockingProgressStatusInfo), metadataTextBrush);

            FrameworkPropertyMetadata metadataLongOperationIndicatorTextBrush = new FrameworkPropertyMetadata(new SolidColorBrush(Color.FromArgb(0xFF, 0xB5, 0xE0, 0xF0)));
            LongOperationIndicatorTextBrushProperty = DependencyProperty.Register("LongOperationIndicatorTextBrush",
                                                            typeof(Brush), typeof(OverlayBlockingProgressStatusInfo), metadataLongOperationIndicatorTextBrush);
        }
        

        public OverlayBlockingProgressStatusInfo()
        {
            InitializeComponent();

            // bindings für ProgressInfo control
            Binding bindingColorBgBrush = new Binding("ColorBgBrushBrush");
            bindingColorBgBrush.Source = this;
            bindingColorBgBrush.Mode = BindingMode.TwoWay;
            progress_info_control.SetBinding(ProgressInfo.ColorBgBrushBrushProperty, bindingColorBgBrush);

            Binding bindingTextBrush = new Binding("TextBrush");
            bindingTextBrush.Source = this;
            bindingTextBrush.Mode = BindingMode.TwoWay;
            progress_info_control.SetBinding(ProgressInfo.TextBrushProperty, bindingTextBrush);

            Binding bindingLongOperationIndicatorTextBrush = new Binding("LongOperationIndicatorTextBrush");
            bindingLongOperationIndicatorTextBrush.Source = this;
            bindingLongOperationIndicatorTextBrush.Mode = BindingMode.TwoWay;
            progress_info_control.SetBinding(ProgressInfo.LongOperationIndicatorTextBrushProperty, bindingLongOperationIndicatorTextBrush);

            progress_info_control.action_progress.Style = (Style) FindResource("overlay_blockingprogress_style");

            this.Loaded += new RoutedEventHandler(OverlayBlockingProgressStatusInfo_Loaded);
            progress_info_control.TimerTicked += new EventHandler(progress_info_control_TimerTicked);
            Reset();
        }



        /// <summary>
        /// Die Animation wurde aus dem XAML herausgenommen und hardcodet in diese Loaded-Event Funktion genommen,
        /// da es vorkommen konnte, das die FindAncester Methode keinen gültigen Wert für den Endwert der Animation gefunden hat
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void OverlayBlockingProgressStatusInfo_Loaded(object sender, RoutedEventArgs e)
        {
            double target_width = Double.NaN;
            try
            {
                target_width = Application.Current.MainWindow.ActualWidth;
                
            }
            catch (Exception)
            {}

            // Wenn das MainWindow nicht gefunden werden kann oder einen ungültigen Wert liefert, wird dieser DefaultWert genommen
            if(Double.IsInfinity(target_width) || Double.IsNaN(target_width))
                target_width = 1500;

            DoubleAnimation d = new DoubleAnimation(0, target_width, new Duration(new TimeSpan(0, 0, 0, 1)));
            d.DecelerationRatio = 1;
            Storyboard.SetTargetProperty(d, new PropertyPath("Width"));

            Storyboard sb = new Storyboard();
            sb.Children.Add(d);

            bg_border.BeginStoryboard(sb);
        }

        #region Operations
        public void Reset()
        {
            progress_info_control.Reset();
        }

        void progress_info_control_TimerTicked(object sender, EventArgs e)
        {
            OnTimerTicked();
        }

        protected void OnTimerTicked()
        {
            if(TimerTicked != null)
            {
                TimerTicked(this, new EventArgs());
            }
        }

        /// <summary>
        /// Implement IDisposable.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            // This object will be cleaned up by the Dispose method.
            // Therefore, you should call GC.SupressFinalize to
            // take this object off the finalization queue 
            // and prevent finalization code for this object
            // from executing a second time.
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Dispose(bool disposing) executes in two distinct scenarios. 
        /// If disposing equals true, the method has been called directly 
        /// or indirectly by a user's code. Managed and unmanaged resources 
        /// can be disposed. 
        /// If disposing equals false, the method has been called by the 
        /// runtime from inside the finalizer and you should not reference  
        /// other objects. Only unmanaged resources can be disposed.
        /// </summary>
        /// <param name="disposing">disposing flag</param>
        private void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if (!_disposed)
            {
                // If disposing equals true, dispose all managed 
                // and unmanaged resources.
                if (disposing)
                {
                }
            }
            _disposed = true;
        }
        #endregion

        #region properties

        public string InformationTitle
        {
            get { return progress_info_control.InformationTitle; }
            set { progress_info_control.InformationTitle = value; }
        }
        public string SubInformationTitle
        {
            get { return progress_info_control.SubInformationTitle; }
            set { progress_info_control.SubInformationTitle = value; }
        }

        public string CurrentAction
        {
            get { return progress_info_control.CurrentAction; }
            set { progress_info_control.CurrentAction = value; }
        }

        public double CurrentActionProgress
        {
            get { return progress_info_control.CurrentActionProgress; }
            set { progress_info_control.CurrentActionProgress = value; }
        }

        public bool EnableLongOperationIndicator
        {
            get { return progress_info_control.EnableLongOperationIndicator; }
            set { progress_info_control.EnableLongOperationIndicator = value; }
        }

        public double LongOperationTrigger
        {
            get { return progress_info_control.LongOperationTrigger; }
            set { progress_info_control.LongOperationTrigger = value; }
        }

        public string LongOperationText
        {
            get { return progress_info_control.LongOperationText; }
            set { progress_info_control.LongOperationText = value; }
        }

        public Visibility TimeIndicatorVisibility
        {
            get { return progress_info_control.TimeIndicatorVisibility; }
            set { progress_info_control.TimeIndicatorVisibility = value; }
        }

        public double TimeElapsed
        {
            get { return progress_info_control.TimeElapsed; }
        }

        public bool IsLongOperationIndicatorActive
        {
            get { return progress_info_control.IsLongOperationIndicatorActive; }
        }

        public bool TransparentBackground
        {
            get
            {
                return _transparent_background;
            }
            set
            {
                if (_transparent_background != value)
                {
                    _transparent_background = value;

                    if (_transparent_background)
                    {
                        bg_border.Background = new SolidColorBrush(Colors.Transparent);
                        bg_border.BorderBrush = new SolidColorBrush(Colors.Transparent);
                    }
                    else
                    {
                        bg_border.Background = new SolidColorBrush(Color.FromArgb(0x33, 0x6b, 0x6b, 0x6b));
                        bg_border.BorderBrush = new SolidColorBrush(Color.FromArgb(0x33, 0x6b, 0x6b, 0x6b));
                    }
                }
            }
        }
        #endregion

        public Brush FilledBackgroundBrush
        {
            get { return (Brush)GetValue(FilledBackgroundBrushProperty); }
            set { SetValue(FilledBackgroundBrushProperty, value); }
        }

        public Brush ColorBgBrushBrush
        {
            get { return (Brush)GetValue(ColorBgBrushBrushProperty); }
            set { SetValue(ColorBgBrushBrushProperty, value); }
        }

        public Brush TextBrush
        {
            get { return (Brush)GetValue(TextBrushProperty); }
            set { SetValue(TextBrushProperty, value); }
        }

        public Brush LongOperationIndicatorTextBrush
        {
            get { return (Brush)GetValue(LongOperationIndicatorTextBrushProperty); }
            set { SetValue(LongOperationIndicatorTextBrushProperty, value); }
        }

        public bool ShowPercentage
        {
            get { return progress_info_control.ShowPercentage; }
            set { progress_info_control.ShowPercentage = value; }
        }

        public bool ShowCurrentAction
        {
            get { return progress_info_control.ShowCurrentAction; }
            set { progress_info_control.ShowCurrentAction = value; }
        }

        public void StartHidingAnimation()
        {
            try
            {
                if(_hiding_sb != null)
                    return;
                
               _hiding_sb = (Storyboard) FindResource("anim_startcontent_fade_out");

               _hiding_sb.Completed += new EventHandler(_hiding_sb_Completed);
               _hiding_sb.Begin();
               
            }
            catch (Exception)
            {
                Visibility = Visibility.Hidden;
            }

        }

        public void StopHidingAnimation()
        {
            if(_hiding_sb != null)
            {
                _hiding_sb.Stop();
                _hiding_sb = null;

                Opacity = 1;
            }
        }

        #region EventHandler
        void _hiding_sb_Completed(object sender, EventArgs e)
        {
            if(_hiding_sb != null)
                _hiding_sb.Stop();

            _hiding_sb = null;

            this.Visibility = System.Windows.Visibility.Hidden;
            this.Opacity = 1;

            if (StatusControlHidden != null)
                StatusControlHidden(this, new EventArgs());
        }
        #endregion

        #region Events
        public event EventHandler TimerTicked;
        public event EventHandler StatusControlHidden;
        #endregion

        #region dependency properties
        public static readonly DependencyProperty FilledBackgroundBrushProperty;
        public static readonly DependencyProperty ColorBgBrushBrushProperty;
        public static readonly DependencyProperty TextBrushProperty;
        public static readonly DependencyProperty LongOperationIndicatorTextBrushProperty;
        #endregion

        #region attribs

        private Storyboard _hiding_sb = null;

        private bool _transparent_background = true;
        
        private bool _show_percentage = true;
        private bool _show_current_action = true;

        private DateTime _dtstart = DateTime.Now;
        private DateTime _last_action = DateTime.Now;
        private Timer _elapsed_timer = null;
        private bool _disposed = false;
        private string _current_action = "";
        // long operation info
        // wenn sich eine gewisse zeitlang kein status ändert, wird durch flashen des
        // info textes angegeben, dass im hintergrund gearbeitet wird
        private Timer _long_operation_flash_timer = null;
        private bool _enable_logoperation_indicator = false;
        private double _longoperation_indicator_trigger = 8000; // ms
        private string _longoperation_indicator_text = "Die aktuelle Aktion wird im Hintergrund abgeschlossen.\nBitte das Fenster nicht schliessen auch wenn keine Statusänderungen sichtbar sind!";
        private bool _longoperation_indicator_visible = false;
        private double _time_elapsed = 0;

        private const double FLASHTIMEOUT_PERIOD = 1600;
        protected delegate void NoParas();
        #endregion

    }
}
