﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Data;

namespace Logicx.WpfUtility.Converters
{
    public class DoubleMultiplyConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            double output = 0;
            try
            {
                double d_val = (double)value;
                double d_mult = (double) parameter;
                return d_val*d_mult;
            }
            catch (Exception)
            {
                
                throw;
            }

            return output;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }
}

