﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using MathLib;

namespace Logicx.WpfUtility.GraphPlotter
{
    public class NewtonInterpolationPlot : GraphPlot
    {
        public NewtonInterpolationPlot() : base()
        {
            InitializeComponent();
        }

        public void AddNewtonPoly(NewtonInterpolation poly, float min, float max)
        {
            float minf, maxf;
            minf = min;
            maxf = max;

            List<Vector2f> v_list = new List<Vector2f>();
            for(;minf <= maxf; minf = minf + (max - min)/10 )
            {
                v_list.Add(new Vector2f(minf, -1 *(float)poly.GetNewtonPolynomValue(minf)));
            }

            string_id++;
            AddLine("Newton"+string_id, v_list, Brushes.Beige,100, Brushes.Red);
        }

        #region Attributes

        protected int string_id = 0;
        #endregion
    }
}
