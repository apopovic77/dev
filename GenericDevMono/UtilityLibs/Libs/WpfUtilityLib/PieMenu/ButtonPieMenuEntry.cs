﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;

namespace Logicx.WpfUtility.PieMenu
{
    /// <summary>
    /// Interface zur Unterscheidung eines ButtonPieMenuEntry von einem normalen PieMenuEntry
    /// </summary>
    public interface IButtonPieMenuEntry
    {
        void RaiseClick();
    }

    [Serializable]
    //TODO Änderungen in dieser Klasse müssen auch in der entsprechenden Klasse im IaApi Lib durchgeführt werden, da diese Klasse hier eine Kopie derjenigen im IaApi ist 
    public class ButtonPieMenuEntry : PieMenuEntry, IButtonPieMenuEntry
    {
        //TODO Änderungen in dieser Klasse müssen auch in der entsprechenden Klasse im IaApi Lib durchgeführt werden, da diese Klasse hier eine Kopie derjenigen im IaApi ist 
        public ButtonPieMenuEntry(Color bg_active_c, string name)
            : base(bg_active_c, name)
        {
        }
        //TODO Änderungen in dieser Klasse müssen auch in der entsprechenden Klasse im IaApi Lib durchgeführt werden, da diese Klasse hier eine Kopie derjenigen im IaApi ist 
        public ButtonPieMenuEntry(string name, string key)
            : base(name, key)
        {
            Name = name;
        }
        //TODO Änderungen in dieser Klasse müssen auch in der entsprechenden Klasse im IaApi Lib durchgeführt werden, da diese Klasse hier eine Kopie derjenigen im IaApi ist 
        public ButtonPieMenuEntry(string name)
            : base(name, name)
        {
            Name = name;
        }
        //TODO Änderungen in dieser Klasse müssen auch in der entsprechenden Klasse im IaApi Lib durchgeführt werden, da diese Klasse hier eine Kopie derjenigen im IaApi ist 
        public void RaiseClick()
        {
            if (IsEnabled == false)
                return;

            if (Click != null)
                Click(this, new EventArgs());
        }
        //TODO Änderungen in dieser Klasse müssen auch in der entsprechenden Klasse im IaApi Lib durchgeführt werden, da diese Klasse hier eine Kopie derjenigen im IaApi ist 
        public event EventHandler Click;
    }
}
