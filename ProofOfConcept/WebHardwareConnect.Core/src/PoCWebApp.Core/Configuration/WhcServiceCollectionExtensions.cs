﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;

namespace PoCWebApp.Core.Configuration
{
    public static class WhcServiceCollectionExtensions
    {
        public static IWhcServiceBuilder AddWhc(this IServiceCollection services, IMvcBuilder mvcBuilder)
        {
            return new WhcServiceBuilder(services);
        }
    }
}
