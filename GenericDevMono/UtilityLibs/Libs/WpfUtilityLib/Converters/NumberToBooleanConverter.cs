﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;

namespace Logicx.WpfUtility.Converters
{
    [ValueConversion(typeof(int), typeof(bool))]
    public class NumberToBooleanConverter : IValueConverter
    {
        #region IValueConverter Members
        public object Convert(object value, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            int val_i=0;
            if (!int.TryParse(value.ToString(), out val_i))
                throw new NotSupportedException();

            if (val_i == 0)
                return false;
            else
                return true;
        }

        public object ConvertBack(object value, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            bool val_b = (bool)value;
            if (val_b)
                return 1;
            else
                return 0;
        }
        #endregion
    }
}
