﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Utility.BinarySerialization
{
    public class BinarySerializationFactory
    {
        #region BINARY SERIALIZATION
         #region Writing
        public static void SaveObject<T>(string path, T to_be_saved_object) where T : IBinarySerializable
        {
            FileStream fs = File.Open(path, FileMode.Create);
            using (BinaryWriter binwriter = new BinaryWriter(fs))
            {
                to_be_saved_object.Serialize(binwriter);
            }
            fs.Close();
            fs.Dispose();
        }

        //public static void SaveDictionary<T,K>(string path, Dictionary<T,K> tobe_saved_objects, bool with_compression) 
        //    where T : struct
        //    where K : IBinarySerializable, new()
        //{
        //    if (with_compression)
        //    {
        //        throw new NotImplementedException();

        //        //MemoryStream mem_stream = new MemoryStream();
        //        //WriteDataToStream(mem_stream, tobe_saved_objects);
        //        //byte[] data = mem_stream.ToArray();
        //        //mem_stream.Close();

        //        //LZOCompressor lzo = new LZOCompressor();
        //        //byte[] compressed = lzo.Compress(data);

        //        //Stream stream = new FileStream(path, FileMode.Create);
        //        //stream.Write(compressed, 0, compressed.Length);
        //        //stream.Close();
        //    }
        //    else
        //    {
        //        FileStream fs = File.Open(path, FileMode.Create);
        //        WriteDataToStream(fs, tobe_saved_objects);
        //        fs.Close();
        //    }
        //}


        public static void Save<T>(string path, List<T> tobe_saved_objects, bool with_compression) where T : IBinarySerializable, new()
         {
             if (with_compression)
             {
                 throw new NotImplementedException();

                 //MemoryStream mem_stream = new MemoryStream();
                 //WriteDataToStream(mem_stream, tobe_saved_objects);
                 //byte[] data = mem_stream.ToArray();
                 //mem_stream.Close();

                 //LZOCompressor lzo = new LZOCompressor();
                 //byte[] compressed = lzo.Compress(data);

                 //Stream stream = new FileStream(path, FileMode.Create);
                 //stream.Write(compressed, 0, compressed.Length);
                 //stream.Close();
             }
             else
             {
                 FileStream fs = File.Open(path, FileMode.Create);
                 WriteDataToStream(fs, tobe_saved_objects);
                 fs.Close();
             }
         }

        public static void Save<T>(BinaryWriter binwriter, List<T> tobe_saved_objects) where T : IBinarySerializable, new()
        {
            WriteDataToStream(binwriter, tobe_saved_objects);
        }

        private static void WriteDataToStream<T>(BinaryWriter binwriter, List<T> tobe_saved_objects) where T : IBinarySerializable, new()
        {
            binwriter.Write(tobe_saved_objects.Count);
            for (int i = 0; i < tobe_saved_objects.Count; i++)
            {
                tobe_saved_objects[i].Serialize(binwriter);
            }
        }

            //private static void WriteDataToStream<T, K>(BinaryWriter binwriter, Dictionary<T, K> tobe_saved_objects)
            //    where T : struct
            //    where K : IBinarySerializable, new()
            //{
            //    binwriter.Write(tobe_saved_objects.Count);

            //    foreach (KeyValuePair<T, K> key_value_pair in tobe_saved_objects)
            //    {
            //        binwriter.Write(key_value_pair.Key);
            //    }


            //    foreach (var tobe_saved_object in tobe_saved_objects)
            //    {
            //        binwriter.Write(tobe_saved_object.Key);
            //    }


            //    for (int i = 0; i < tobe_saved_objects.Count; i++)
            //    {
            //        tobe_saved_objects[i].Serialize(binwriter);
            //    }
            //}

        private static void WriteDataToStream<T>(Stream stream, List<T> tobe_saved_objects) where T : IBinarySerializable, new()
         {
             using (BinaryWriter binwriter = new BinaryWriter(stream))
             {
                 WriteDataToStream(binwriter, tobe_saved_objects);
             }
         }

         #endregion

         //#region Reading
        public static T LoadObject<T>(string path) where T : IBinarySerializable, new()
        {
            T t_desrialized = new T();
            FileStream fs = File.Open(path, FileMode.Open);
            using (BinaryReader binreader = new BinaryReader(fs))
            {
                t_desrialized.Deserialize(binreader);
            }
            fs.Close();
            fs.Dispose();
            return t_desrialized;

        }

        public static void LoadObject<T>(string path, ref T object_to_be_updated) where T : IBinarySerializable
        {
            if (!File.Exists(path))
                return;
                //throw new Exception("object kann nicht deserialisiert werden, object ist nicht vorhanden.");

            FileStream fs = File.Open(path, FileMode.Open);
            using (BinaryReader binreader = new BinaryReader(fs))
            {
                object_to_be_updated.Deserialize(binreader);
            }
            fs.Close();
            fs.Dispose();
        }

        public static List<T> LoadList<T>(BinaryReader binreader) where T : IBinarySerializable, new()
        {
            return ReadFromStream<T>(binreader);
        }

        public static List<T> LoadList<T>(string path, bool with_compression) where T : IBinarySerializable, new()
         {
             if (with_compression)
             {
                 throw new NotImplementedException();
                 /*

                 FileStream fs = File.Open(path, FileMode.Open);
                 byte[] compressed = new byte[fs.Length];
                 fs.Read(compressed, 0, compressed.Length);
                 fs.Close();

                 LZOCompressor lzo = new LZOCompressor();
                 byte[] data = lzo.Compress(compressed);

                 MemoryStream mem_stream = new MemoryStream(data);
                 List<T> desrialized_objects = ReadFromStream<T>(mem_stream);
                 mem_stream.Close();
                 return desrialized_objects;
                 
                 */
             }
             else
             {
                 if (!File.Exists(path))
                     return null;

                 FileStream fs = File.Open(path, FileMode.Open);
                 List<T> desrialized_objects = ReadFromStream<T>(fs);
                 fs.Close();
                 fs.Dispose();
                 return desrialized_objects;
             }
         }

        private static List<T> ReadFromStream<T>(BinaryReader binreader) where T : IBinarySerializable, new()
        {
            int count_objects = binreader.ReadInt32();
            List<T>  objects = new List<T>(count_objects);
            for (int i = 0; i < count_objects; i++)
            {
                T t = new T();
                t.Deserialize(binreader);
                objects.Add(t);
            }
            return objects;
        }

         private static List<T> ReadFromStream<T>(Stream stream) where T : IBinarySerializable, new ()
         {
             List<T> objects = null;
             using (BinaryReader binreader = new BinaryReader(stream))
             {
                 objects = ReadFromStream<T>(binreader);
             }
             return objects;
         }
         //#endregion
        #endregion


    }
}
