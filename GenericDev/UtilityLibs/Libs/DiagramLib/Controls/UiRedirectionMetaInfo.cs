﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace Logicx.DiagramLib.Controls
{
    public class UiRedirectionMetaInfo
    {
        public UiRedirectionMetaInfo(MouseButtonEventHandler mouse_down_redirection, MouseButtonEventHandler mouse_up_redirection, MouseEventHandler mouse_move_redirection,
            StylusDownEventHandler stylus_down_redirection, StylusEventHandler stylus_up_redirection, StylusEventHandler stylus_move_redirection)
        {
            MouseDownRedirection = mouse_down_redirection;
            MouseUpRedirection = mouse_up_redirection;
            MouseMoveRedirection = mouse_move_redirection;

            StylusDownRedirection = stylus_down_redirection;
            StylusUpRedirection = stylus_up_redirection;
            StylusMoveRedirection = stylus_move_redirection;
        }

        public MouseButtonEventHandler MouseDownRedirection
        {
            get;
            private set;
        }

        public MouseButtonEventHandler MouseUpRedirection
        {
            get;
            private set;
        }

        public MouseEventHandler MouseMoveRedirection
        {
            get;
            private set;
        }

        public StylusDownEventHandler StylusDownRedirection
        {
            get;
            private set;
        }

        public StylusEventHandler StylusUpRedirection
        {
            get;
            private set;
        }

        public StylusEventHandler StylusMoveRedirection
        {
            get;
            private set;
        }
    }
}
