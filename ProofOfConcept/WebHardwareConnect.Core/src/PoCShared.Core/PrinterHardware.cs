﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PoCShared.Core
{
    public class PrinterHardware : HardwareItem
    {
        public override string Type { get; set; }

        public PrinterHardware()
        {
            Type = "PRINTER";
        }
    }
}
