﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Logicx.Geo.Geometries;
using Logicx.WpfUtility.WpfHelpers;
using MathLib;

namespace Logicx.WpfUtility.CustomControls.Taskbar
{
    /// <summary>
    /// Interaktionslogik für TaskbarPanel.xaml
    /// </summary>
    public partial class TaskbarPanel : UserControl
    {
        public class TaskbarItem
        {
            public FrameworkElement FrameworkElement;
            public double MinSpacingLeft;
            public double MinSpacingRight;
            public StackPanel Container;
            public Grid ContentGrid;
            public Border MirrorBorder;
            public double ItemWidth;
            public object ReferenceObject;
        }

        public TaskbarPanel()
        {
            InitializeComponent();
            //ScaleTransform = new ScaleTransform(1,1);
            RenderBar(_min_width_panel, _kante_len);
            item_panel_stack.SizeChanged += new SizeChangedEventHandler(item_panel_stack_SizeChanged);
        }

        void item_panel_stack_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            RenderBar((float)CalculateRenderBarWidth(), _kante_len);
        }

        void item_panel_stack_LayoutUpdated(object sender, EventArgs e)
        {
            RenderBar((float)CalculateRenderBarWidth(), _kante_len);
        }

        public void AddItem(FrameworkElement item)
        {
            AddItem(item, 5, 5);
        }

        public StackPanel AddPlaceholderItem(FrameworkElement framework_element, double min_spacing_left, double min_spacing_right)
        {
            // Neue Elemente die über eine Animation der Taskbar hinzugefügt werden, werden standardmässig an die vorletzte Stelle des StackPanels angefügt
            return AddItem(framework_element, min_spacing_left, min_spacing_right, true, (_items.Count > 0) ? _items.Count - 1 : 0);
        }

        public void AddItem(FrameworkElement framework_element, double min_spacing_left, double min_spacing_right)
        {
            AddItem(framework_element, min_spacing_left, min_spacing_right, false, item_panel_stack.Children.Count);
        }

        public StackPanel AddItem(FrameworkElement framework_element, double min_spacing_left, double min_spacing_right, bool is_anim_placeholder, int insert_position)
        {
            if (_items == null)
                _items = new List<TaskbarItem>();

            TaskbarItem item = new TaskbarItem();
            item.FrameworkElement = framework_element;
            item.MinSpacingLeft = min_spacing_left;
            item.MinSpacingRight = min_spacing_right;
            item.ItemWidth = framework_element.ActualWidth;
            _items.Add(item);

            item.FrameworkElement.SizeChanged += new SizeChangedEventHandler(item_SizeChanged);
            item.FrameworkElement.Tag = item;

            double item_height = ITEM_HEIGHT;

            Grid grid_item = new Grid();
            grid_item.ClipToBounds = false;
            grid_item.Cursor = Cursors.Hand;
            grid_item.Height = item_height;
            grid_item.ColumnDefinitions.Add(new ColumnDefinition() { Width = GridLength.Auto });
            item.FrameworkElement.VerticalAlignment = VerticalAlignment.Center;
            grid_item.Children.Add(item.FrameworkElement);

            VisualBrush mirror_brush = new VisualBrush(grid_item);
            mirror_brush.Transform = new ScaleTransform(1, -1, 0, item_height / 2);

            Border mirror_border = new Border();
            mirror_border.Height = item_height;
            mirror_border.Width = item.FrameworkElement.ActualWidth;
            mirror_border.Background = mirror_brush;
            mirror_border.OpacityMask = (Brush)FindResource("opacity_brush");

            StackPanel sp = new StackPanel();
            sp.Children.Add(grid_item);
            sp.Children.Add(mirror_border);
            sp.Tag = item;
            item.Container = sp;
            item.ContentGrid = grid_item;
            item.MirrorBorder = mirror_border;

            if (is_anim_placeholder)
                sp.Opacity = 0;
      
            item_panel_stack.Children.Insert(insert_position, sp);

            if (ItemAdded != null)
                ItemAdded(this, new EventArgs());

            if (WithSpreadMenuManagement)
            {
                if (framework_element is SingleSpreadMenu)
                {
                    SingleSpreadMenu menu = (SingleSpreadMenu)framework_element;
                    menu.ToggleButton.Checked += new RoutedEventHandler(ToggleButton_Checked);
                    menu.ToggleButton.Unchecked += new RoutedEventHandler(ToggleButton_Unchecked);
                }
                else if (framework_element is PointerWrapPanelButton)
                {
                    PointerWrapPanelButton menu = (PointerWrapPanelButton)framework_element;
                    menu.ToggleButton.Checked += new RoutedEventHandler(ToggleButton_Checked);
                    menu.ToggleButton.Unchecked += new RoutedEventHandler(ToggleButton_Unchecked);
                }
            }
            return sp;
        }

        public void RemoveItem(FrameworkElement framework_element, bool animated_width)
        {
            if(!item_panel_stack.Children.Contains(framework_element))
                return;                
            
            TaskbarItem item = framework_element.Tag as TaskbarItem;
            if(item == null || !animated_width)
                item_panel_stack.Children.Remove(framework_element);
            else
            {
                HideItemContent(framework_element);

                DoubleAnimation d2 = new DoubleAnimation();
                d2.DecelerationRatio = 1;
                d2.From = item.ContentGrid.ActualWidth + item.MinSpacingLeft + item.MinSpacingRight;
                d2.To = 0;
                d2.Duration = new Duration(TimeSpan.FromSeconds(1));
                Storyboard.SetTargetProperty(d2, new PropertyPath(FrameworkElement.WidthProperty));

                Storyboard sb = new Storyboard();
                sb.Name = "StackPanelName_" + framework_element.GetHashCode();
                framework_element.Name = sb.Name;
                sb.Children.Add(d2);
                sb.Completed += new EventHandler(sb_Completed);


                item.ContentGrid.Margin = new Thickness(0);
                item.MinSpacingRight = 0;
                item.MinSpacingLeft = 0;
                item.MirrorBorder.Child = null;
                item.MirrorBorder.Width = 0;
                item.ContentGrid.BeginStoryboard(sb);

                _running_animations.Add(sb.Name);

                if(_running_animations.Count == 1)
                    item_panel_stack.LayoutUpdated += new EventHandler(item_panel_stack_LayoutUpdated);
            }
        }

        void sb_Completed(object sender, EventArgs e)
        {
            ClockGroup c_group = sender as ClockGroup;
            if (c_group != null)
            {
                c_group.Completed -= sb_Completed;
                FrameworkElement found_elem = null;
                foreach(FrameworkElement f_elem in item_panel_stack.Children)
                {
                    if (f_elem.Name == c_group.Timeline.Name)
                        found_elem = f_elem;
                }
                if (found_elem != null)
                {
                    ClearTaskbarItem(found_elem);
                    item_panel_stack.Children.Remove(found_elem);
                    item_panel_stack.UpdateLayout();
                }

                _running_animations.Remove(c_group.Timeline.Name);
                if(_running_animations.Count == 0)
                    item_panel_stack.LayoutUpdated -= item_panel_stack_LayoutUpdated;
            }
        }



        /// <summary>
        /// Bestimmt die Position des Framework element in Bezug auf das Hauptfenster
        /// (ist notwendig um bei der Minimierungsanimation an die korrekte Stelle zu fahren)
        /// Bei der Operation wird davon ausgegangen, dass die Taskbar sichtbar ist. Falls es also durch eine Margin-Animation zu einem Offset der Taskbar
        /// gekommen ist, wird dieser abgezogen
        /// </summary>
        /// <param name="elem"></param>
        /// <returns></returns>
        public Point GetElementPosition(FrameworkElement elem)
        {
            return GetElementPosition(elem, Application.Current.MainWindow);
        }

        public Point GetElementPosition(FrameworkElement elem, Visual ref_visual)
        {
            this.UpdateLayout();
            GeneralTransform transform = elem.TransformToAncestor(ref_visual);

            Point rootPoint = transform.Transform(new Point(0, 0));

            Thickness margin = this.Margin;

            rootPoint.X -= margin.Left/2;
            rootPoint.X += margin.Right/2;

            rootPoint.Y += margin.Bottom/2;
            rootPoint.Y -= margin.Top/2;
            
            return rootPoint;
        }

        public double GetNewScale(double additional_width)
        {
            double window_width = BaseGridWidth;
            double old_tb_width = TablePath.ActualWidth;
            double tb_width = TablePath.ActualWidth + additional_width;
            double spacing = 20;           
            double new_scale = 1;
            
            if (window_width < tb_width + spacing * 2)
            {
                //adjust scale
                double desired_width = window_width - spacing * 2;
                double old_scale = desired_width / old_tb_width;
                new_scale = desired_width / tb_width;
#if DEBUG
                Debug.WriteLine("Width-Diff:" + Math.Abs((additional_width * old_scale - additional_width * new_scale)));
                Debug.WriteLine("window_width " + window_width + " desired_width " + desired_width + " new_scale " + new_scale + "taskbar_panel.ActualWidth " + ActualWidth);
#endif
            }

            return new_scale;
        }

        public double GetNewScale()
        {
            return GetNewScale(0);
        }

        /// <summary>
        /// Mit dieser Methode soll ein Placeholder Element mit einem sichbaren Objekt ausgetauscht werden.
        /// (Nachdem z.B. die Minimierungsanimation in die Taskbar vollzogen wurde)
        /// </summary>
        /// <param name="container"></param>
        /// <param name="newElement"></param>
        public void ChangeItemContent(StackPanel container, FrameworkElement newElement)
        {            
            TaskbarItem changed_item = container.Tag as TaskbarItem;
            changed_item.ItemWidth = newElement.ActualWidth;
            changed_item.FrameworkElement = newElement;
            
            changed_item.FrameworkElement.VerticalAlignment = VerticalAlignment.Center;
            changed_item.ItemWidth = newElement.ActualWidth;

            Grid grid_item = container.Children[0] as Grid;
            Border mirror_border = container.Children[1] as Border;

            if(grid_item == null)
            {
                grid_item = container.Children[1] as Grid;
                mirror_border = container.Children[0] as Border;
            }


            Image img = grid_item.Children[0] as Image;
            
            grid_item.ClipToBounds = false;
            grid_item.Height = img.Height;
            grid_item.Width = img.Width;
            changed_item.FrameworkElement.Width = img.Width;
            grid_item.ColumnDefinitions.Clear();
            changed_item.FrameworkElement.VerticalAlignment = VerticalAlignment.Center;
            grid_item.Children.Clear();
            grid_item.Children.Add(changed_item.FrameworkElement);

            VisualBrush mirror_brush = new VisualBrush(grid_item);
            mirror_brush.Transform = new ScaleTransform(1, -1, 0, ITEM_HEIGHT / 2);
            mirror_border.Width = img.Width;
            mirror_border.Height = ITEM_HEIGHT;
            mirror_border.Background = mirror_brush;
            mirror_border.OpacityMask = (Brush)FindResource("opacity_brush");
        }


        void item_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            FrameworkElement framework_element = (FrameworkElement)sender;
            TaskbarItem changed_item = (TaskbarItem)framework_element.Tag;
            if (changed_item.Container.Children.Count > 1)
            {
                Border mirror_border = (Border)changed_item.Container.Children[1];
                mirror_border.Width = framework_element.ActualWidth;
            }
            changed_item.ItemWidth = framework_element.ActualWidth;

            double width_panel = CalculateRenderBarWidth();

            RenderBar((float)width_panel, _kante_len);
        }

        public void RenderBar(float width, float kante_len)
        {
            border.Width = width;
            item_panel_stack.Width = width;

            List<Vector2f> poly_verts = new List<Vector2f>();
            poly_verts.Add(new Vector2f(0, 100));

            Vector2f v_rot_topleft = new Vector2f(kante_len, 0);
            v_rot_topleft.Rotate((((float)Math.PI) / 2f) * 0.6f);
            Vector2f v_topleft = poly_verts[0];
            v_topleft += v_rot_topleft;
            poly_verts.Add(v_topleft);

            Vector2f v_bottom_right = new Vector2f(width, 100);

            Vector2f v_rot_topright = new Vector2f(kante_len, 0);
            v_rot_topright.Rotate((((float)Math.PI) / 2f) * 1.4f);
            Vector2f v_topright = v_bottom_right;
            v_topright += v_rot_topright;
            poly_verts.Add(v_topright);

            poly_verts.Add(v_bottom_right);

            BoundingBox bounding_box;
            path.Data = DataTypeConverterFunctions.SimplePolygon2StreamGeometry(poly_verts, out bounding_box);

            //Window win = UIHelper.TryFindParent<Window>(this);
            //if(win != null)
            //{
            //    if(width > win.ActualWidth)
            //    {
            //        ScaleTransform s = this.RenderTransform as ScaleTransform;
            //        if(s == null)
            //        {
            //            s = new ScaleTransform();
                        
            //            this.RenderTransform = s;
            //        }
            //        double scale = (win.ActualWidth+20)/width;
                   
            //        s.ScaleX = scale;
            //        s.ScaleY = scale;
            //    }
            //}
        }


        /// <summary>
        /// Berechnet die neue Größe der angezeigten Taskbar Tisches
        /// </summary>
        /// <returns></returns>
        protected double CalculateRenderBarWidth()
        {
            double width_panel = 0;
            for (int i = 0; i < item_panel_stack.Children.Count; i++)
            {
                FrameworkElement container_stack = (FrameworkElement)item_panel_stack.Children[i];
                TaskbarItem item = (TaskbarItem)container_stack.Tag;

                double margin_left = item.MinSpacingLeft;
                double margin_right = item.MinSpacingRight;

                if (i == 0)
                    margin_left = MIN_SPACING_BORDER;

                if (i == item_panel_stack.Children.Count - 1)
                    margin_right = MIN_SPACING_BORDER;

                container_stack.Margin = new Thickness(margin_left, 0, margin_right, 0);

                if (container_stack is StackPanel)
                {
                    foreach (FrameworkElement elem in ((StackPanel)container_stack).Children)
                    {
                        if (elem is Grid)
                        {
                            width_panel += elem.ActualWidth;
                        }
                    }
                }
                else
                {
                    width_panel += item.ItemWidth;
                }

                width_panel += margin_left + margin_right;
            }

            return width_panel;
        }

        protected void HideItemContent(FrameworkElement elem)
        {
            StackPanel sp = elem as StackPanel;
            if (sp != null)
            {
                if (sp.Children.Count > 0)
                {
                    Grid g = sp.Children[0] as Grid;
                    if (g != null)
                        g.Visibility = System.Windows.Visibility.Hidden;
                }

                if (sp.Children.Count > 1)
                {
                    Grid g = sp.Children[1] as Grid;
                    if (g != null)
                        g.Visibility = System.Windows.Visibility.Hidden;
                }
            }
        }

        protected void ClearTaskbarItem(FrameworkElement elem)
        {
            StackPanel sp = elem as StackPanel;
            if (sp != null)
            {
                TaskbarItem item = sp.Tag as TaskbarItem;
                if (item != null)
                {
                    item.FrameworkElement.SizeChanged -= item_SizeChanged;
                    item.FrameworkElement = null;
                    item.Container = null;
                    item.ReferenceObject = null;
                    Grid g = item.ContentGrid;
                    if (g != null)
                        g.Children.Clear();

                    _items.Remove(item);
                }
            }
        }

        #region EventHandler
        void ToggleButton_Unchecked(object sender, RoutedEventArgs e)
        {
        }

        void ToggleButton_Checked(object sender, RoutedEventArgs e)
        {
            FrameworkElement framework_element = (FrameworkElement)sender;

            framework_element = UIHelper.TryFindParent<SingleSpreadMenu>(framework_element);
            if (framework_element == null)
            {
                framework_element = (FrameworkElement)sender;
                framework_element = UIHelper.TryFindParent<PointerWrapPanelButton>(framework_element);
            }

            TaskbarItem changed_item = (TaskbarItem)framework_element.Tag;

            //close all other spread menu items
            foreach (TaskbarItem taskbar_item in _items)
            {
                if (taskbar_item == changed_item)
                    continue;

                if(taskbar_item.FrameworkElement is SingleSpreadMenu)
                {
                    SingleSpreadMenu menu = (SingleSpreadMenu)taskbar_item.FrameworkElement;
                    menu.ToggleButton.IsChecked = false;
                }
                else if (taskbar_item.FrameworkElement is PointerWrapPanelButton)
                {
                    PointerWrapPanelButton menu = (PointerWrapPanelButton)taskbar_item.FrameworkElement;
                    menu.ToggleButton.IsChecked = false;
                }
            }
        }
        #endregion

        #region Properties
        public Path TablePath
        {
            get
            {
                return path;
            }
        }

        public bool IsSpreadMenuOpen
        {
            get
            {
                //close all other spread menu items
                foreach (TaskbarItem taskbar_item in _items)
                {
                    if (taskbar_item.FrameworkElement is SingleSpreadMenu)
                    {
                        SingleSpreadMenu menu = (SingleSpreadMenu)taskbar_item.FrameworkElement;
                        if(menu.ToggleButton.IsChecked.HasValue && menu.ToggleButton.IsChecked.Value)
                            return true;
                    }

                    if (taskbar_item.FrameworkElement is PointerWrapPanelButton)
                    {
                        PointerWrapPanelButton menu = (PointerWrapPanelButton)taskbar_item.FrameworkElement;
                        if (menu.ToggleButton.IsChecked.HasValue && menu.ToggleButton.IsChecked.Value)
                            return true;
                    }
                }
                return false;
            }
        }

        public bool WithSpreadMenuManagement
        {
            get { return (bool) GetValue(WithSpreadMenuManagementProperty); }
            set { SetValue(WithSpreadMenuManagementProperty, value); }
        }
        public static readonly DependencyProperty WithSpreadMenuManagementProperty = DependencyProperty.Register("WithSpreadMenuManagement", typeof (bool), typeof (TaskbarPanel), new PropertyMetadata(true));

        //public ScaleTransform ScaleTransform
        //{
        //    get { return (ScaleTransform)GetValue(ScaleTransformProperty); }
        //    set { SetValue(ScaleTransformProperty, value); }
        //}
        //public static readonly DependencyProperty ScaleTransformProperty = DependencyProperty.Register("ScaleTransform", typeof(ScaleTransform), typeof(TaskbarPanel), new UIPropertyMetadata(null));



        public double BaseGridWidth
        {
            get { return (double)GetValue(BaseGridWidthProperty); }
            set { SetValue(BaseGridWidthProperty, value); }
        }

        // Using a DependencyProperty as the backing store for BaseGridWidth.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty BaseGridWidthProperty =
            DependencyProperty.Register("BaseGridWidth", typeof(double), typeof(TaskbarPanel), new UIPropertyMetadata(0.0d));


        #endregion

        #region Attributes
        List<String> _running_animations = new List<string>();
        protected List<TaskbarItem> _items;
        
        protected const double MIN_SPACING_BORDER = 60;
        protected float _min_width_panel = 100;
        protected float _kante_len = 60;

        public readonly float ITEM_HEIGHT = 40;

        public event EventHandler ItemAdded;
        #endregion

    }
}
