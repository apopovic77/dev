﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading.Tasks;
using PoCShared;

namespace WebHardwareClient.Core
{
    internal static class LocalPrinters
    {

        public static List<PrinterItem> GetPrinters()
        {
            var ret = new List<PrinterItem>();

            ManagementScope objScope = new ManagementScope(ManagementPath.DefaultPath); //For the local Access
            objScope.Connect();

            SelectQuery selectQuery = new SelectQuery();
            selectQuery.QueryString = "Select * from win32_Printer";

            var printerQuery = new ManagementObjectSearcher(objScope, selectQuery);
            foreach (var printer in printerQuery.Get())
            {
                string name = (string)printer.GetPropertyValue("Name");
                var status = printer.GetPropertyValue("Status");
                var isDefault = printer.GetPropertyValue("Default");
                bool isNetworkPrinter = (bool)printer.GetPropertyValue("Network");
                bool isShared = (bool)printer.GetPropertyValue("Shared");
                string shareName = (string)printer.GetPropertyValue("ShareName");


                Console.WriteLine("{0} (Status: {1}, Default: {2}, Network: {3}",
                            name, status, isDefault, isNetworkPrinter);

                ret.Add(new PrinterItem()
                {
                    Name = name,
                    Network = isNetworkPrinter,
                    Status = PrinterStatus.Unknown,
                    PrintServer = Environment.MachineName,
                    Shared = isShared,
                    ShareName = shareName
                });
            }

            return ret;
        }
    }
}
