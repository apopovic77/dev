using System;
using System.Collections.Generic;
using System.Text;

namespace MathLib
{
    public class CubicBezierCurve
    {
        Vector2d _p0, _p1, _p2, _p3;
       

        public CubicBezierCurve(Vector2d p0, Vector2d p1, Vector2d p2, Vector2d p3)
        {
            _p0 = p0;
            _p1 = p1;
            _p2 = p2;
            _p3 = p3;
        }


        public Vector2d Calc(double t)
        {
            Vector2d point_res = (_p0 * -1 + _p1 * 3 - _p2 * 3 + _p3) * (t * t * t) +
                                 (_p0 * 3 - _p1 * 6 + _p2 * 3) * (t * t) +
                                 (_p0 * -3 + _p1 * 3) * t + _p0;

            return point_res;
        }

        public Vector2d CalcPeak()
        {
            if (double.MinValue != _peak.Y && double.MinValue != _peak.X)
                return _peak;

            for(double i = 0; i <= 1; i += 0.01)
            {
                Vector2d cur_v = Calc(i);
                if (_peak.Y < cur_v.Y)
                    _peak = cur_v;
            }

            return _peak;
        }

        public Vector2d CalcMin()
        {
            if (double.MaxValue != _min.Y && double.MaxValue != _min.X)
                return _min;

            for (double i = 0; i <= 1; i += 0.00001)
            {
                Vector2d cur_v = Calc(i);
                if (_min.Y > cur_v.Y)
                    _min = cur_v;
            }

            return _min;
        }

        private Vector2d _peak = new Vector2d(double.MinValue, double.MinValue);
        private Vector2d _min = new Vector2d(double.MaxValue, double.MaxValue);
    }
}
