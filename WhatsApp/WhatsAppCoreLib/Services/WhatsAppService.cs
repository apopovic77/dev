﻿using System;
using System.Collections.Generic;
using System.IO;
using Awanto.WhatsAppCore.Entities;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Reflection;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Console;

namespace Awanto.WhatsAppCore.Services
{
    public class WhatsAppService
    {
        public static void SendNachricht(string empfänger, string absender, string body)
        {
            WhatsAppDbContext db = new WhatsAppDbContext(GetDbContextOptions());

            WhatsAppNachricht nachricht = new WhatsAppNachricht();
            nachricht.EmpfaengerAdresse = empfänger;
            nachricht.AbsenderAdresse = absender;
            nachricht.Body = body;
            nachricht.ErstelltVon = "WhatsAppService";

            db.WhatsAppNachrichten.Add(nachricht);
            db.SaveChanges();

            db.Dispose();
        }

        public static void SendNachricht(WhatsAppNachricht nachricht)
        {
            WhatsAppDbContext db = new WhatsAppDbContext(GetDbContextOptions());
            nachricht.ErstelltVon = "WhatsAppService";
            db.WhatsAppNachrichten.Add(nachricht);
            db.SaveChanges();
            db.Dispose();
        }


        public static void SendImageNachricht(string empfänger, string absender, string caption, int bild_id)
        {
            WhatsAppDbContext db = new WhatsAppDbContext(GetDbContextOptions());

            WhatsAppImageNachricht nachricht = new WhatsAppImageNachricht();
            nachricht.EmpfaengerAdresse = empfänger;
            nachricht.AbsenderAdresse = absender;
            nachricht.Body = caption;
            nachricht.Medien = new List<NachrichtMedium>() { new NachrichtMedium() { Nachricht = nachricht, Medium = db.Bilder.Where(be => be.Id == bild_id).First() } };
            nachricht.ErstelltVon = "WhatsAppService";

            db.WhatsAppImageNachrichten.Add(nachricht);
            db.SaveChanges();

            db.Dispose();
        }

        public static DbContextOptions<WhatsAppDbContext> GetDbContextOptions(bool with_console_logging = false)
        {
            var options_builder = new DbContextOptionsBuilder<WhatsAppDbContext>();

            if (with_console_logging)
            {
                var lf = new LoggerFactory();
                lf.AddProvider(new Utilities.ConsoleLoggerProvider());
                options_builder.UseLoggerFactory(lf);
            }

            options_builder.UseSqlServer(GetConnectionString());
            return options_builder.Options;
        }

        public static string GetConnectionString()
        {
            //return Configuration.GetConnectionString("WhatsAppDbConnectionString");
            return Configuration["Database:WhatsAppDbConnectionString"];
        }

        public static string GetDatabaseName()
        {
            return Configuration["Database:WhatsAppDatabaseName"];
        }

        public static string GetDatabaseUserName()
        {
            return Configuration["Database:WhatsAppDbUsername"];
        }

        public static string GetPhoneNr()
        {
            return Configuration["WhatsApp:PhoneNr"];
        }

        private static string GetPathToConfig()
        {
            Type t = typeof(WhatsAppDbContext);
            // Instantiate an Assembly class to the assembly housing the Integer type.  
            Assembly assembly = Assembly.GetAssembly(t);

            int l_index = assembly.Location.LastIndexOf("\\");
            string path_to_config = assembly.Location.Substring(0, l_index) + "\\WhatsAppConfig.json";

            if (!File.Exists(path_to_config))
                path_to_config = assembly.Location.Substring(0, l_index) + "\\netcoreapp2.0\\WhatsAppConfig.json";

            if (!File.Exists(path_to_config))
                throw new Exception("cannot find WhatsAppConfig.Json");

            return path_to_config;
        }


        public static WhatsAppDbContext CreateDbContext()
        {
            WhatsAppDbContext dbx = new WhatsAppDbContext(GetDbContextOptions());
            return dbx;
        }


        private static IConfigurationRoot _config = null;
        public static IConfigurationRoot Configuration
        {
            get{
                if(_config == null) {
                    _config = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile(GetPathToConfig())
                    .Build();
                }
                return _config;
            }
        }

        public static bool IsSqlExpressDb
        {
            get
            {
                try
                {
                    return Convert.ToBoolean(Configuration["Database:SqlExpressDb"]);
                }
                catch
                {
                }
                return false;
            }
        }
    }
}
