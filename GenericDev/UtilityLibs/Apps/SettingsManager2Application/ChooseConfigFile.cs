﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SettingsManagerApplication
{
    public partial class ChooseConfigFile : Form
    {
        public ChooseConfigFile(SettingsManager.ConfigFileInfo[] config_files)
        {
            InitializeComponent();
            _config_files = config_files;

            lb_configfiles.DataSource = config_files.Select(cf=>cf.PathToClassFile).ToList();
            lb_configfiles.SelectionMode = SelectionMode.One;
            lb_configfiles.MouseDoubleClick += new MouseEventHandler(lb_configfiles_MouseDoubleClick);
        }

        public SettingsManager.ConfigFileInfo GetSelectedConfigFile()
        {
            string path_to_class_file =  (string)lb_configfiles.SelectedItem;
            return _config_files.Where(cf => cf.PathToClassFile == path_to_class_file).Single();
        }

        void lb_configfiles_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            Close();
        }

        private SettingsManager.ConfigFileInfo[] _config_files;

        private void b_ok_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
