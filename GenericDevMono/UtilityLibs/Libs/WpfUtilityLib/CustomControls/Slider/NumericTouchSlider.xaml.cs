﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Logicx.WpfUtility.Converters;

namespace Logicx.WpfUtility.CustomControls.Slider
{
    /// <summary>
    /// Interaction logic for NumericTouchSlider.xaml
    /// </summary>
    public partial class NumericTouchSlider : UserControl, IDisposable
    {
        #region Nested types 
        public class SliderItemSizeConverter : IMultiValueConverter
        {

            #region IMultiValueConverter Members

            public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
            {
                if (values != null && values.Length == 2 && values[0] != DependencyProperty.UnsetValue && values[1] != DependencyProperty.UnsetValue)
                {
                    double size = (double) values[0];
                    int digits = (int) values[1];

                    if (digits == 0)
                        return (double)0;

                    return size/digits;
                }

                return (double)0;
            }

            public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
            {
                return null;
            }

            #endregion
        }
        #endregion

        #region Constants
        #endregion

        #region Construction and Initialization
        public NumericTouchSlider()
        {
            InitializeComponent();
            _digits_list.AddRange(new int[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9});
            _max_digits_list.AddRange(new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 });
            this.Loaded += new RoutedEventHandler(NumericTouchSlider_Loaded);
        }

        
        #endregion

        #region Operations

        #region Dependency property changed
        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(e);
            if(e.Property == OrientationProperty)
            {
                AdjustAllSliderSizeBindings();
            }
        }
        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            Dispose(true);
        }

        private void Dispose(bool disposing)
        {
            if (_disposing)
                return;

            if (!_disposed)
            {
                _disposing = disposing;


                //EventHelper.RemoveAllTextContainerChangedEventHanders(tb);
                //EventHelper.RemoveAllTextContainerChangeEventHanders(tb);
                _disposed = true;
            }
        }

        #endregion

        #region Control methods

        private void AdjustAllSliderSizeBindings()
        {
            for(int i=0; i < slider_stack.Children.Count; i++)
            {
                AdjustSliderWidthHeightBindings(slider_stack.Children[i] as TouchSlider);
            }
        }

        private void AdjustSliderWidthHeightBindings(TouchSlider slider_control)
        {
            if (slider_control == null)
                return;

            BindingOperations.ClearBinding(slider_control, HeightProperty);
            BindingOperations.ClearBinding(slider_control, WidthProperty);
            BindingOperations.ClearBinding(slider_control, OrientationProperty);

            if (Orientation == System.Windows.Controls.Orientation.Horizontal)
            {
                Binding height_binding = new Binding("ActualHeight");
                height_binding.Source = this;
                slider_control.SetBinding(HeightProperty, height_binding);

                MultiBinding width_binding = new MultiBinding();

                Binding single_width_binding = new Binding("ActualWidth");
                single_width_binding.Source = this;

                Binding number_digits_binding = new Binding("NumberOfDigits");
                number_digits_binding.Source = this;

                width_binding.Bindings.Add(single_width_binding);
                width_binding.Bindings.Add(number_digits_binding);

                width_binding.Converter = new SliderItemSizeConverter();
                slider_control.SetBinding(WidthProperty, width_binding);
            }
            else
            {
                Binding width_binding = new Binding("ActualWidth");
                width_binding.Source = this;
                slider_control.SetBinding(WidthProperty, width_binding);

                MultiBinding height_binding = new MultiBinding();

                Binding single_height_binding = new Binding("ActualHeight");
                single_height_binding.Source = this;

                Binding number_digits_binding = new Binding("NumberOfDigits");
                number_digits_binding.Source = this;

                height_binding.Bindings.Add(single_height_binding);
                height_binding.Bindings.Add(number_digits_binding);

                height_binding.Converter = new SliderItemSizeConverter();
                slider_control.SetBinding(HeightProperty, height_binding);
            }

        }

        private void AdjustMaxDigitList()
        {
            _max_digits_list.Clear();
            int nMaxDigit = GetDigitOfPosition(MaxValue, NumberOfDigits - 1);
            for (int i = 0; i < (nMaxDigit + 1); i++)
                _max_digits_list.Add(i);
        }

        private void GenerateSliderControls()
        {
            int current_digits = slider_stack.Children.Count;

            if(current_digits < NumberOfDigits)
            {
                int missing_digits = NumberOfDigits - current_digits;

                for (int i = 0; i < missing_digits; i++)
                {
                    // create controls
                    TouchSlider single_digit_slider = new TouchSlider();
                    single_digit_slider.ItemsSource = _digits_list;
                    single_digit_slider.InverseMouseWheel = _inverse_mouse_wheel;

                    Binding orientation_binding = new Binding("Orientation");
                    orientation_binding.Source = this;
                    orientation_binding.Mode = BindingMode.OneWay;
                    orientation_binding.Converter = new InverseOrientationConverter();
                    single_digit_slider.SetBinding(TouchSlider.OrientationProperty, orientation_binding);

                    Binding datatemplate_binding = new Binding("ItemTemplate");
                    datatemplate_binding.Source = this;
                    single_digit_slider.SetBinding(TouchSlider.ItemTemplateProperty, datatemplate_binding);

                    Binding selection_border_style_binding = new Binding("SelectionIndicatorBorderStyle");
                    selection_border_style_binding.Source = this;
                    single_digit_slider.SetBinding(TouchSlider.SelectionIndicatorBorderStyleProperty, selection_border_style_binding);

                    Binding control_border_style_binding = new Binding("ControlBorderStyle");
                    control_border_style_binding.Source = this;
                    single_digit_slider.SetBinding(TouchSlider.ControlBorderStyleProperty, control_border_style_binding);

                    AdjustSliderWidthHeightBindings(single_digit_slider);
                    single_digit_slider.SelectedItem = 0;

                    slider_stack.Children.Add(single_digit_slider);
                    //single_digit_slider.SelectionChanged += new SelectionChangedEventHandler(single_digit_slider_SelectionChanged);
                }
            }

            //if (Orientation == System.Windows.Controls.Orientation.Horizontal)
            //{
                int nDigitIndex = 0;
                for (int i = slider_stack.Children.Count-1; i >=0; i--)
                {
                    nDigitIndex = (slider_stack.Children.Count - 1) - i;
                    ((TouchSlider)slider_stack.Children[i]).SelectionChanged -= single_digit_slider_SelectionChanged;

                    if (nDigitIndex < NumberOfDigits)
                    {
                        slider_stack.Children[i].Visibility = Visibility.Visible;
                        if(nDigitIndex == (NumberOfDigits-1))
                        {
                            ((TouchSlider)slider_stack.Children[i]).ItemsSource = _max_digits_list;
                        }
                        else
                        {
                            ((TouchSlider)slider_stack.Children[i]).ItemsSource = _digits_list;    
                        }
                        
                        ((TouchSlider)slider_stack.Children[i]).SelectionChanged += new SelectionChangedEventHandler(single_digit_slider_SelectionChanged);
                    }
                    else
                    {
                        slider_stack.Children[i].Visibility = Visibility.Collapsed;
                        ((TouchSlider)slider_stack.Children[i]).ItemsSource = _digits_list;
                    }
                }
            //}
            //else
            //{
            //    for (int i = 0; i < slider_stack.Children.Count; i++)
            //    {
            //        ((TouchSlider)slider_stack.Children[i]).SelectionChanged -= single_digit_slider_SelectionChanged;
            //        if (i  < NumberOfDigits)
            //        {
            //            slider_stack.Children[i].Visibility = Visibility.Visible;
            //            ((TouchSlider) slider_stack.Children[i]).SelectionChanged += new SelectionChangedEventHandler(single_digit_slider_SelectionChanged);
            //        }
            //        else
            //        {
            //            slider_stack.Children[i].Visibility = Visibility.Collapsed;
            //        }
            //    }
            //}

                if (Value != 0)
                    AdjustSlidersToValue();

            _initial_controls_generated = true;
        }

        #endregion


        #region Event raising
        private void SendSelectionChanged()
        {
            if (SelectionChanged != null)
                SelectionChanged(this, new SelectionChangedEventArgs(ListBox.SelectionChangedEvent, new List<object>(), new List<object>()));
        }
        #endregion

        #region helper methods
        private int GetNumberOfDigits(int number)
        {
            int checkNumber = number;
            if (checkNumber < 0)
                checkNumber += -1;

            int nNumberOfDigits = 1;
            double dCheck = checkNumber/10.0;

            while(dCheck >= 1)
            {
                nNumberOfDigits++;
                dCheck /= 10.0;
            }

            return nNumberOfDigits;
        }

        private int GetMaxNumberForDigits(int number_of_digits)
        {
            int nRet = 9;

            if(number_of_digits > 1)
            {
                for(int i=1; i<number_of_digits;i++)
                {
                    nRet *= 10;
                    nRet += 9;
                }
            }
            return nRet;
        }

        private void AdjustSlidersToValue()
        {
            if (_ignore_value_changed)
                return;

            if (slider_stack.Children.Count == 0)
                return;

            _ignore_single_slider_selection_changes = true;
            // alle auf 0 stellen
            for(int i=0; i < slider_stack.Children.Count; i++)
            {
                if(slider_stack.Children[i] is TouchSlider)
                {
                    ((TouchSlider) slider_stack.Children[i]).SelectedItem = 0;
                }
            }

            int nDigitIndex = 0;
            int nControlIndex = nDigitIndex;
            int nCheckValue = Value;

            while(nCheckValue != 0)
            {
                //if (Orientation == System.Windows.Controls.Orientation.Horizontal)
                //{
                    nControlIndex = (slider_stack.Children.Count - 1) - nDigitIndex;
                //}
                //else
                //{
                //    nControlIndex = nDigitIndex;
                //}

                decimal divByTen = ((decimal) nCheckValue/(decimal) 10); //Math.Pow(10, (nDigitIndex + 1)));
                int idivByTen = ((int) nCheckValue/(int) 10); //Math.Pow(10, (nDigitIndex + 1)));

                decimal diff = divByTen - idivByTen;
                int nDigitNumber = (int)(diff * (decimal)10.0);

                if (slider_stack.Children[nControlIndex] is TouchSlider)
                {
                    ((TouchSlider)slider_stack.Children[nControlIndex]).SelectedItem = nDigitNumber;
                }
                nDigitIndex++;
                nCheckValue = idivByTen;
            }

            _ignore_single_slider_selection_changes = false;
        }

        private int GetDigitOfPosition(int value, int position)
        {
            decimal divByTen = ((decimal)value / (decimal)Math.Pow(10, (position + 1)));
            int idivByTen = ((int)value / (int)Math.Pow(10, (position + 1)));

            decimal diff = divByTen - idivByTen;
            int nDigitNumber = (int) (diff*(decimal)10.0);

            return nDigitNumber;
        }

        private int SetDigitOfPosition(int complete_value, int position, int new_value)
        {
            int current_number = GetDigitOfPosition(complete_value, position);
            int new_number = new_value;

            int ndigit_diff = (new_number - current_number);

            if (current_number + ndigit_diff >= 10)
                ndigit_diff = ndigit_diff - 10;

            int ndif = ndigit_diff * (int)(Math.Pow(10, position));



            return complete_value + ndif;
        }
        #endregion

        #endregion

        #region Event Handlers

        private void StackPanel_MouseMove(object sender, MouseEventArgs e)
        {
            e.Handled = true;
        }

        private void StackPanel_StylusMove(object sender, StylusEventArgs e)
        {
            e.Handled = true;
        }

        void NumericTouchSlider_Loaded(object sender, RoutedEventArgs e)
        {
            if (!_initial_controls_generated)
                GenerateSliderControls();
        }

        #region dependency property callback and coersion
        private static void OnMinValueChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is NumericTouchSlider)
            {
                NumericTouchSlider slider_control = (NumericTouchSlider)d;

                int number_of_digits = slider_control.GetNumberOfDigits((int)e.NewValue);
                if (number_of_digits > slider_control.NumberOfDigits)
                    slider_control.NumberOfDigits = number_of_digits;
            }
        }

        private static object OnMinValueCoerce(DependencyObject d, object basevalue)
        {
            if (d is NumericTouchSlider)
            {
                NumericTouchSlider slider_control = (NumericTouchSlider)d;

                if (!coerce_current)
                {
                    if (basevalue is int)
                    {
                        int new_value = (int)basevalue;

                        int curMaxValue = slider_control.MaxValue;
                        if (new_value > curMaxValue)
                        {
                            int nNewMax = slider_control.GetMaxNumberForDigits(slider_control.GetMaxNumberForDigits(new_value));
                            if (new_value < nNewMax)
                                slider_control.MaxValue = nNewMax;
                            else
                                slider_control.MaxValue = new_value + 1;
                        }
                    }
                }

                coerce_current = false;
            }

            return basevalue;
        }

        private static void OnMaxValueChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is NumericTouchSlider)
            {
                NumericTouchSlider slider_control = (NumericTouchSlider)d;

                int number_of_digits = slider_control.GetNumberOfDigits((int)e.NewValue);
                if (number_of_digits > slider_control.NumberOfDigits)
                {
                    slider_control.NumberOfDigits = number_of_digits;
                }
                else
                {
                    slider_control.AdjustMaxDigitList();
                    slider_control.GenerateSliderControls();
                }
            }
        }

        private static object OnMaxValueCoerce(DependencyObject d, object basevalue)
        {
            if (d is NumericTouchSlider)
            {
                NumericTouchSlider slider_control = (NumericTouchSlider)d;

                if (!coerce_current)
                {
                    if (basevalue is int)
                    {
                        int new_value = (int)basevalue;

                        int curMinValue = slider_control.MinValue;
                        if (new_value < curMinValue)
                        {
                            slider_control.MaxValue = new_value - 1;
                        }
                    }
                }

                coerce_current = false;
            }

            return basevalue;
        }

        private static void OnNumberOfDigitsChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is NumericTouchSlider)
            {
                NumericTouchSlider slider_control = (NumericTouchSlider)d;

                slider_control.AdjustMaxDigitList();
                slider_control.GenerateSliderControls();
            }
        }

        private static object OnNumberOfDigitsCoerce(DependencyObject d, object basevalue)
        {
            if (d is NumericTouchSlider)
            {
                NumericTouchSlider slider_control = (NumericTouchSlider)d;

                if (!coerce_current)
                {
                    if (basevalue is int)
                    {
                        int new_number_of_digits = (int) basevalue;

                        int curMinDigits = slider_control.GetNumberOfDigits(slider_control.MinValue);
                        int curMaxDigits = slider_control.GetNumberOfDigits(slider_control.MaxValue);

                        if(new_number_of_digits  < curMaxDigits )
                        {
                            slider_control.MaxValue = slider_control.GetMaxNumberForDigits(new_number_of_digits);
                        }

                        if(new_number_of_digits < curMinDigits)
                        {
                            slider_control.MinValue = 0;
                        }
                    }
                }

                coerce_current = false;
            }

            return basevalue;
        }

        private bool _ignore_value_callback_change = false;
        private static void OnValueChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is NumericTouchSlider)
            {
                NumericTouchSlider slider_control = (NumericTouchSlider)d;

                if (slider_control._ignore_value_callback_change)
                    return;

                var coerced_value = OnValueCoerce(d, slider_control.Value);

                if (coerced_value == DependencyProperty.UnsetValue)
                    return;

                if (((int)coerced_value) != slider_control.Value)
                {
                    slider_control._ignore_value_callback_change = true;
                    slider_control.Value = ((int) coerced_value);
                    slider_control._ignore_value_callback_change = false;

                    bool old_ignore_value_change = slider_control._ignore_value_changed;
                    slider_control._ignore_value_changed = false;
                    slider_control.AdjustSlidersToValue();
                    slider_control._ignore_value_changed = old_ignore_value_change;
                }
                else
                {
                    slider_control.AdjustSlidersToValue();
                }

                slider_control.SendSelectionChanged();
            }
        }

        private static object OnValueCoerce(DependencyObject d, object basevalue)
        {
            if (d is NumericTouchSlider)
            {
                NumericTouchSlider slider_control = (NumericTouchSlider)d;

                if (!coerce_current)
                {
                    if (basevalue is int)
                    {
                        int new_value = (int)basevalue;

                        if (new_value < slider_control.MinValue)
                        {
                            basevalue = slider_control.MinValue;
                            bool old_ignore_value_change = slider_control._ignore_value_changed;
                            slider_control._ignore_value_changed = false;
                            slider_control.AdjustSlidersToValue();
                            slider_control._ignore_value_changed = old_ignore_value_change;
                        }
                        else if (new_value > slider_control.MaxValue)
                        {
                            basevalue = slider_control.MaxValue;
                            bool old_ignore_value_change = slider_control._ignore_value_changed;
                            slider_control._ignore_value_changed = false;
                            slider_control.AdjustSlidersToValue();
                            slider_control._ignore_value_changed = old_ignore_value_change;
                        }
                    }
                }

                coerce_current = false;
            }

            return basevalue;
        }
        #endregion

        #region single slider change
        void single_digit_slider_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (_ignore_single_slider_selection_changes)
                return;

            if(! (sender is TouchSlider))
                return;

            TouchSlider slider_sender = sender as TouchSlider;

            int digit_index = slider_stack.Children.IndexOf(slider_sender);

            //if(Orientation == System.Windows.Controls.Orientation.Horizontal)
            //{
                digit_index = slider_stack.Children.Count - digit_index - 1;
            //}

            if(digit_index >= 0)
            {
                int new_number = (int)(slider_sender.SelectedItem ?? 0);
                _ignore_value_changed = true;
                Value = SetDigitOfPosition(Value, digit_index, new_number);
                _ignore_value_changed = false;
            }
        }
        #endregion

        #endregion

        #region Properties

        public bool InverseMouseWheel
        {
            get { return _inverse_mouse_wheel; }   
            set
            {
                if(_inverse_mouse_wheel != value)
                {
                    _inverse_mouse_wheel = value;
                    foreach(UIElement child in slider_stack.Children)
                    {
                        if(child is TouchSlider)
                        {
                            ((TouchSlider) child).InverseMouseWheel = _inverse_mouse_wheel;
                        }
                    }
                }
            }
        }
        #endregion

        #region Dependency Properties
        public int MinValue
        {
            get { return (int)GetValue(MinValueProperty); }
            set { SetValue(MinValueProperty, value); }
        }
        public static readonly DependencyProperty MinValueProperty = DependencyProperty.Register("MinValue", typeof(object), typeof(NumericTouchSlider), new UIPropertyMetadata(0, new PropertyChangedCallback(OnMinValueChanged), new CoerceValueCallback(OnMinValueCoerce)));

        public int MaxValue
        {
            get { return (int)GetValue(MaxValueProperty); }
            set { SetValue(MaxValueProperty, value); }
        }
        public static readonly DependencyProperty MaxValueProperty = DependencyProperty.Register("MaxValue", typeof(object), typeof(NumericTouchSlider), new UIPropertyMetadata(99, new PropertyChangedCallback(OnMaxValueChanged), new CoerceValueCallback(OnMaxValueCoerce)));

        public int NumberOfDigits
        {
            get { return (int)GetValue(NumberOfDigitsProperty); }
            set { SetValue(NumberOfDigitsProperty, value); }
        }
        public static readonly DependencyProperty NumberOfDigitsProperty = DependencyProperty.Register("NumberOfDigits", typeof(object), typeof(NumericTouchSlider), new UIPropertyMetadata(2, new PropertyChangedCallback(OnNumberOfDigitsChanged), new CoerceValueCallback(OnNumberOfDigitsCoerce)));

        public Orientation Orientation
        {
            get { return (Orientation)GetValue(OrientationProperty); }
            set { SetValue(OrientationProperty, value); }
        }
        public static readonly DependencyProperty OrientationProperty = DependencyProperty.Register("Orientation", typeof(Orientation), typeof(NumericTouchSlider), new UIPropertyMetadata(Orientation.Horizontal));

        public int Value
        {
            get { return (int)GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }
        public static readonly DependencyProperty ValueProperty = DependencyProperty.Register("Value", typeof(int), typeof(NumericTouchSlider), new UIPropertyMetadata(0, new PropertyChangedCallback(OnValueChanged)));





        /// <summary>
        /// das item template für ein item in dem control
        /// </summary>
        public DataTemplate ItemTemplate
        {
            get { return (DataTemplate)GetValue(ItemTemplateProperty); }
            set { SetValue(ItemTemplateProperty, value); }
        }
        public static readonly DependencyProperty ItemTemplateProperty = DependencyProperty.Register("ItemTemplate", typeof(DataTemplate), typeof(NumericTouchSlider), new UIPropertyMetadata(null));

        /// <summary>
        /// der style für den selection indicator
        /// </summary>
        public Style SelectionIndicatorBorderStyle
        {
            get { return (Style)GetValue(SelectionIndicatorBorderStyleProperty); }
            set { SetValue(SelectionIndicatorBorderStyleProperty, value); }
        }
        public static readonly DependencyProperty SelectionIndicatorBorderStyleProperty = DependencyProperty.Register("SelectionIndicatorBorderStyle", typeof(Style), typeof(NumericTouchSlider), new UIPropertyMetadata(null));


        /// <summary>
        /// der style für das control
        /// </summary>
        public Style ControlBorderStyle
        {
            get { return (Style)GetValue(ControlBorderStyleProperty); }
            set { SetValue(ControlBorderStyleProperty, value); }
        }
        public static readonly DependencyProperty ControlBorderStyleProperty = DependencyProperty.Register("ControlBorderStyle", typeof(Style), typeof(NumericTouchSlider), new UIPropertyMetadata(null));


        #endregion

        #region Events
        public event SelectionChangedEventHandler SelectionChanged;
        #endregion

        #region Routed Events
        #endregion

        #region Attributes
        private bool _disposed = false;
        private bool _disposing = false;

        private bool _inverse_mouse_wheel = false;

        private bool _initial_controls_generated = false;
        private int _number_of_digits = 0;
        private bool _ignore_single_slider_selection_changes = false;
        private bool _ignore_value_changed = false;

        private static bool coerce_current;
        private List<int> _digits_list = new List<int>();
        private List<int> _max_digits_list = new List<int>();
        #endregion

        #region Tests
        #endregion
    }
}
