﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using System.Windows;
using System.Windows.Shapes;

namespace Logicx.WpfUtility.BrushManipulation
{
    public static class BrushExtensions
    {
        public static Brush Tile(Brush org_brush)
        {
            //VisualBrush vb = new VisualBrush ();

            //vb.TileMode = TileMode.Tile;

            //vb.Viewport = new Rect(0, 0, 10, 10);
            //vb.ViewportUnits = BrushMappingMode.Absolute; 

            //vb.Viewbox = new Rect(0, 0, 12, 12); 
            //vb.ViewboxUnits = BrushMappingMode.Absolute; 

            //Ellipse ellipse = new Ellipse();
            //ellipse.Fill = org_brush; 
            //ellipse.Width = 10; 
            //ellipse.Height = 10;

            //return vb;


            #region Variante1 (Schachbrett)
            //DrawingBrush myBrush = new DrawingBrush();

            //GeometryDrawing backgroundSquare =
            //    new GeometryDrawing(
            //        Brushes.White,
            //        null,
            //        new RectangleGeometry(new Rect(0, 0, 100, 100)));

            //GeometryGroup aGeometryGroup = new GeometryGroup();
            //aGeometryGroup.Children.Add(new RectangleGeometry(new Rect(0, 0, 50, 50)));
            //aGeometryGroup.Children.Add(new RectangleGeometry(new Rect(50, 50, 50, 50)));

           

            //GeometryDrawing checkers = new GeometryDrawing(org_brush, null, aGeometryGroup);

            //DrawingGroup checkersDrawingGroup = new DrawingGroup();
            //checkersDrawingGroup.Children.Add(backgroundSquare);
            //checkersDrawingGroup.Children.Add(checkers);

            //myBrush.Drawing = checkersDrawingGroup;
            //myBrush.Viewport = new Rect(0, 0, 0.5, 0.5);
            //myBrush.TileMode = TileMode.Tile;

            //return myBrush;
            #endregion

            #region Variante2 (Radial-Gradient Brush)
            //RadialGradientBrush myBrush = new RadialGradientBrush();
            //myBrush.GradientOrigin = new Point(0.5, 0.5);
            
            //myBrush.GradientStops.Add(new GradientStop(Colors.White, 0));            
            //myBrush.GradientStops.Add(new GradientStop((Color)ColorConverter.ConvertFromString(org_brush.ToString()), 1.0));

            //return myBrush;
            #endregion

            #region Variante3 (Linear-Gradient Brush)
            //LinearGradientBrush myBrush = new LinearGradientBrush();
            //myBrush.StartPoint = new Point(0, 0.5);
            //myBrush.EndPoint = new Point(1, 0.5);
            //myBrush.GradientStops.Add(new GradientStop((Color)ColorConverter.ConvertFromString(org_brush.ToString()), 0.0));            
            //myBrush.GradientStops.Add(new GradientStop(Colors.White, 1.0));

            //return myBrush;
            
            #endregion

            #region
            DrawingBrush myBrush = new DrawingBrush();

            GeometryDrawing backgroundSquare =
                new GeometryDrawing(
                    Brushes.White,
                    null,
                    new RectangleGeometry(new Rect(0, 0, 100, 100)));

            GeometryGroup aGeometryGroup = new GeometryGroup();
            aGeometryGroup.Children.Add(new RectangleGeometry(new Rect(0, 0, 10, 100)));
            aGeometryGroup.Children.Add(new RectangleGeometry(new Rect(90, 0, 10, 100)));



            GeometryDrawing checkers = new GeometryDrawing(org_brush, null, aGeometryGroup);

            DrawingGroup checkersDrawingGroup = new DrawingGroup();
            checkersDrawingGroup.Children.Add(backgroundSquare);
            checkersDrawingGroup.Children.Add(checkers);

            myBrush.Drawing = checkersDrawingGroup;
            myBrush.Viewport = new Rect(0, 0, 1, 1);
            //myBrush.TileMode = TileMode.Tile;

            return myBrush;
            #endregion

        }
    }
}
