﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Logicx.WpfUtility.WindowManagement;
using Logicx.WpfUtility.WpfHelpers;

namespace Logicx.WpfUtility.CustomControls.OverlayManager
{
    /// <summary>
    /// Interaction logic for OverlayDialogText.xaml
    /// </summary>
    public partial class OverlayDialogText
    {
        public OverlayDialogText()
        {
            InitializeComponent();
        }

        public OverlayDialogText(string text1, SolidColorBrush backBrush)
        {
            _text1 = text1;
            InitializeComponent();
            tb_color.Background = backBrush;

            DialogTitle = "Overlay dialog test - dialog";
            DialogSubTitle = _text1;
            AutoCenteredDialog = true;

            AddButton(new OverlayDialogButton("Message", OverlayButtonAlignment.Left));
            AddButton(new OverlayDialogButton("Sub", OverlayButtonAlignment.Left));
            AddButton(new OverlayDialogButton("Ok", OverlayButtonAlignment.Right));
            AddButton(new OverlayDialogButton("Abbrechen", OverlayButtonAlignment.Right));

            ModalBehaviour = OverlayContent.ModalBehaviourEnum.TransparentBackground;
        }

        #region IOverlayContent interface implementation

        public bool Show(bool firstTime)
        {
            return true;
        }

        public bool Close(bool forceClose)
        {
            return true;
        }

        public bool Help()
        {
            return true;
        }

        public bool Print()
        {
            return true;
        }

        /// <summary>
        /// Eigenschaft welche definiert ob der content das Drucken unterstützt oder nicht
        /// </summary>
        public bool CanPrint
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Eigenschaft welche dem Hostdialog mitteilt ob der Content geschlossen werden kann.
        /// </summary>
        public bool CanClose
        {
            get
            {
                return true;
            }
        }

        public bool CanHelp
        {
            get
            {
                return true;
            }
        }

        public bool AutoResized
        {
            get { return true; }
        }
        #endregion

        #region Virtuelle methoden welche überschrieben werden können
        /// <summary>
        /// Wird aufgerufen, wenn der Benutzer einen der OverlayButtons klicked
        /// </summary>
        /// <param name="button"></param>
        public override bool OverlayButtonClicked(OverlayDialogButton button)
        {
            if(button.Content == "Ok")
            {
                // OK - aktuellen dialog schliessen (nicht erzwingen)
                if (OverlayManager != null)
                    OverlayManager.HideContent(this, true);
            }
            else if(button.Content == "Abbrechen")
            {
                // abbrechen - aktuellen dialog schliessen (erzwingen)
                if (OverlayManager != null)
                    //if (((OverlayDialog)OverlayManager.ActiveDialog) == this.ParentDialog)
                    OverlayManager.HideContent(this, true);
            }
            else if (button.Content == "Message")
            {
                // abbrechen - aktuellen dialog schliessen (erzwingen)
                if (OverlayManager != null)
                    OverlayManager.ShowMessageBox("Message 1", "Dies ist eine Testmessage in modaler Form!", MessageBoxButton.OKCancel, MessageBoxImage.Information);
            }
            else if (button.Content == "Sub")
            {
                // abbrechen - aktuellen dialog schliessen (erzwingen)
                if (OverlayManager != null)
                {
                    OverlayDialogText sub1 = new OverlayDialogText("Sub Window", new SolidColorBrush(Colors.Violet));
                    sub1.ModalBehaviour = OverlayContent.ModalBehaviourEnum.TransparentBackgroundMinimizeAll;
                    OverlayManager.Show(sub1, OverlayDialogState.Modal);
                }
            }

            return true;
        }

        /// <summary>
        /// Wurd aufgerufen bevor das Control angezeigt wird
        /// </summary>
        /// <param name="firstTime">true wenn dieser Content in der Dialoginstanz das erste Mal angezeigt wird</param>
        /// <returns>false wenn das control nicht angezeigt werden soll.</returns>
        public override bool BeforeShow(bool firstTime)
        {
            return true;
        }
        /// <summary>
        /// Wird aufgerufen nachdem das Control angezeigt wurde
        /// </summary>
        public override void AfterShow()
        {

        }

        /// <summary>
        /// Wird aufgerufen bevor das control geschlossen wird
        /// </summary>
        /// <param name="forceClose">True wenn das Control auf jeden Fall geschlossen wird.</param>
        /// <returns>false wenn das Control nicht geschlossen werden kann.</returns>
        /// <remarks>Wenn <see cref="forceClose"/> True ist, dann wird der 
        /// Rückgabewert ignoriert und das Control auf jeden Fall geschlossen.</remarks>
        public override bool BeforeClose(bool forceClose)
        {
            return true;
        }
        /// <summary>
        /// Wird aufgerufen nachdem das Control geschlossen wurde
        /// </summary>
        public override void AfterClose()
        {

        }

        /// <summary>
        /// Wurd aufgerufen bevor das Control gedruckt wird wird
        /// </summary>
        /// <returns>false wenn das control nicht gedruckt werden soll.</returns>
        public override bool BeforePrint()
        {
            return true;
        }
        /// <summary>
        /// wird aufgerufen nachdem das Control gedruckt wurde
        /// </summary>
        public override void AfterPrint()
        {

        }
        #endregion

        private string _text1 = "";
    }
}
