﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MathLib;

namespace Logicx.WpfUtility.ZoomingPanning
{
    /// <summary>
    /// Interaction logic for DrawBezierWindow.xaml
    /// </summary>
    public partial class DrawBezierWindow : Window
    {
        public DrawBezierWindow()
        {
            InitializeComponent();
        }

        internal void DrawBezier(MathLib.CubicBezierCurve jump_scale_animation_bezier)
        {
            double width = 0;
            double height = 0;
            for (float i = 0; i <= 1; i+=0.01f)
            {
                Vector2d res = jump_scale_animation_bezier.Calc(i);
                Ellipse e = new Ellipse();
                e.Width = 200;
                e.Height = 0.01;
                e.Fill = Brushes.Red;
                canvas.Children.Add(e);
                Canvas.SetLeft(e, res.X);
                Canvas.SetTop(e, res.Y);
                width = res.X;
                height = res.Y;
            }

            scale.ScaleX = 100 / width;
            scale.ScaleY = 100 / height;
        }
    }
}
