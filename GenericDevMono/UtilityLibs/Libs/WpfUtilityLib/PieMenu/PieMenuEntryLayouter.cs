﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Logicx.WpfUtility.PieMenu;

namespace Logicx.WpfUtility.PieMenu
{
    public class PieMenuEntryLayouter
    {
        public static void DoLayoutHarmony<P>(List<P> pie_menu_entries, double padding, double start_angle) where P : PieMenuEntryBase<P>
        {
            if (pie_menu_entries == null || pie_menu_entries.Count == 0)
                return;

            int count_entries = pie_menu_entries.Count;
            double angle_per_entry = (Math.PI * 2 / count_entries) - padding;

            double curr_angle = start_angle;

            foreach (P entry in pie_menu_entries)
            {
                entry.AngleStart = curr_angle;
                entry.AngleEnd = curr_angle + angle_per_entry;
                curr_angle += angle_per_entry + padding;
            }
        }

        public static void DoLayoutHarmony(List<PieMenuEntry> pie_menu_entries, double padding, double start_angle)
        {
            DoLayoutHarmony<PieMenuEntry>(pie_menu_entries, padding, start_angle);
        }

        public static void DoLayoutSpecificCounterClockwise<P>(List<P> pie_menu_entries, double padding, double start_angle, double pie_angle) where P : PieMenuEntryBase<P>
        {
            if (pie_menu_entries == null || pie_menu_entries.Count == 0)
                return;

            double curr_angle = start_angle;
            for (int i = 0; i < pie_menu_entries.Count; i++)
            {
                pie_menu_entries[i].AngleStart = curr_angle;
                pie_menu_entries[i].AngleEnd = curr_angle + pie_angle;
                curr_angle = pie_menu_entries[i].AngleEnd + padding;
            }
        }

        public static void DoLayoutSpecificCounterClockwise(List<PieMenuEntry> pie_menu_entries, double padding, double start_angle, double pie_angle)
        {
            DoLayoutSpecificCounterClockwise<PieMenuEntry>(pie_menu_entries, padding, start_angle, pie_angle);
        }

        public static void DoLayoutSpecificClockwise(List<PieMenuEntry> pie_menu_entries, double padding, double start_angle, double pie_angle)
        {
            DoLayoutSpecificClockwise<PieMenuEntry>(pie_menu_entries, padding, start_angle, pie_angle);
        }

        public static void DoLayoutSpecificClockwise<P>(List<P> pie_menu_entries, double padding, double start_angle, double pie_angle) where P : PieMenuEntryBase<P>
        {
            if (pie_menu_entries == null || pie_menu_entries.Count == 0)
                return;

            double curr_angle = start_angle;
            for (int i = 0; i < pie_menu_entries.Count; i++)
            {
                pie_menu_entries[i].AngleStart = curr_angle - pie_angle;
                pie_menu_entries[i].AngleEnd = curr_angle;
                curr_angle = pie_menu_entries[i].AngleStart - padding;
            }
        }

    }
}