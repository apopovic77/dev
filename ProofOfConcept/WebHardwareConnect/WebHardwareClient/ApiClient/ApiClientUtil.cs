﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace WebHardwareClient.ApiClient
{
    internal class ApiClientUtil
    {
        internal PocApiClient ConnectedClient { get; private set; }
        protected virtual HttpClient HttpClient
        {
            get { return new HttpClient(); }
        }

        internal ApiClientUtil(PocApiClient connectedClient)
        {
            ConnectedClient = connectedClient;
        }
        
        internal virtual async Task<T> CallPostApiAsync<T>(string apiAction, object data)
        {
            using (var client = HttpClient)
            {
                client.BaseAddress = new Uri(ConnectedClient.ApiUrl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("Authorization-Token", ConnectedClient.AccessToken);
                client.DefaultRequestHeaders.Add("api-version", ConnectedClient.ApiVersion.ToString());

                var response = await client.PostAsJsonAsync(apiAction, data);

                if (response.IsSuccessStatusCode)
                {
                    var apiResponse = await response.Content.ReadAsAsync<T>();
                    ConnectedClient.LastResponse = apiResponse;

                    return apiResponse;
                }
                else
                {
                    throw new HttpRequestException(response.StatusCode.ToString());
                }
            }
        }

        internal virtual async Task<T> CallPutApiAsync<T>(string apiAction, object data)
        {
            using (var client = HttpClient)
            {
                client.BaseAddress = new Uri(ConnectedClient.ApiUrl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("Authorization-Token", ConnectedClient.AccessToken);
                client.DefaultRequestHeaders.Add("api-version", ConnectedClient.ApiVersion.ToString());

                var response = await client.PutAsJsonAsync(apiAction, data);

                if (response.IsSuccessStatusCode)
                {
                    var apiResponse = await response.Content.ReadAsAsync<T>();
                    ConnectedClient.LastResponse = apiResponse;

                    return apiResponse;
                }
                else
                {
                    throw new HttpRequestException(response.StatusCode.ToString());
                }
            }
        }

        internal virtual async Task<T> CallGetApiAsync<T>(string apiAction, string uriParameters = "") 
        {
            using (var client = HttpClient)
            {
                client.BaseAddress = new Uri(ConnectedClient.ApiUrl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("Authorization-Token", ConnectedClient.AccessToken);
                client.DefaultRequestHeaders.Add("api-version", ConnectedClient.ApiVersion.ToString());

                var response = await client.GetAsync(apiAction + (string.IsNullOrEmpty(uriParameters) ? "" : "?" + uriParameters));

                if (response.IsSuccessStatusCode)
                {
                    var apiResponse = await response.Content.ReadAsAsync<T>();
                    ConnectedClient.LastResponse = apiResponse;

                    return apiResponse;
                }
                else
                {
                    throw new HttpRequestException(response.StatusCode.ToString());
                }
            }
        }

        internal virtual async Task<T> CallDeleteApiAsync<T>(string apiAction)
        {
            using (var client = HttpClient)
            {
                client.BaseAddress = new Uri(ConnectedClient.ApiUrl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("Authorization-Token", ConnectedClient.AccessToken);
                client.DefaultRequestHeaders.Add("api-version", ConnectedClient.ApiVersion.ToString());

                var response = await client.DeleteAsync(apiAction);

                if (response.IsSuccessStatusCode)
                {
                    var apiResponse = await response.Content.ReadAsAsync<T>();
                    ConnectedClient.LastResponse = apiResponse;

                    return apiResponse;
                }
                else
                {
                    throw new HttpRequestException(response.StatusCode.ToString());
                }
            }
        }

        //internal virtual string GetPagingParameters(PagingData data)
        //{
        //    string ret = "";

        //    ret += "SearchTerm=" + (data != null ? HttpUtility.UrlEncode(data.SearchTerm) : "");
        //    ret += "&PageLimit=" + (data != null ? HttpUtility.UrlEncode(data.PageLimit.ToString()) : "");
        //    ret += "&Page=" + (data != null ? HttpUtility.UrlEncode(data.Page.ToString()) : "");
        //    return ret;
        //}
    }
}
