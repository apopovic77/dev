﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MathLib;

namespace ArtApp
{
    /// <summary>
    /// Interaktionslogik für DragLine.xaml
    /// </summary>
    public partial class DragLine : UserControl
    {
        public DragLine()
        {
            InitializeComponent();
        }


        public void Update()
        {
            Update(line, ellipse1, ellipse2, Edge);
            Update(line2, ellipse12, ellipse22, Edge2);
        }

        public void Update(Line line, Ellipse ellipse1, Ellipse ellipse2, Edge edge)
        {
            line.X1 = edge.V1.X;
            line.Y1 = edge.V1.Y;
            line.X2 = edge.V2.X;
            line.Y2 = edge.V2.Y;

            if (ellipse1 != null)
            {
                Canvas.SetLeft(ellipse1, edge.V1.X - ellipse1.Width / 2);
                Canvas.SetTop(ellipse1, edge.V1.Y - ellipse1.Width / 2);
            }

            if (ellipse2 != null)
            {
                Canvas.SetLeft(ellipse2, edge.V2.X - ellipse2.Width / 2);
                Canvas.SetTop(ellipse2, edge.V2.Y - ellipse2.Width / 2);
            }
        }


        //public Vector2f P = new Vector2f(7, 10);
        //public Vector2f Q = new Vector2f(100, 50);

        public Edge Edge = new Edge();
        public Edge Edge2 = new Edge();
    }
}
