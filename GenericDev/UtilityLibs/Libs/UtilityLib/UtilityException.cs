using System;

using System.Collections.Generic;
using System.Text;

namespace Logicx.Utilities
{
    public class UtilityException : System.ApplicationException
    {
        public static String[] eMessages = {   
                                            /*0*/				 "no id available, query not successful, specify an id an requery!"
								            };

        private int m_errorCode = -1;
        public UtilityException(int errorCode)
            : base(eMessages[errorCode])
        {
            m_errorCode = errorCode;
        }
        public UtilityException(string errormsg)
            : base(errormsg)
        {
        }
        public UtilityException() { }

        public int ErrorCode
        {
            get
            {
                return m_errorCode;
            }
        }


        /// <summary>
        /// Return information about the exception
        /// </summary>
        public override string Message
        {
            get
            {
                string strMsg = string.Empty;

                strMsg = "Generic application error. Enable\n";
                strMsg += "debug output for detailed information.";

                return strMsg;
            }
        }
    }
}
