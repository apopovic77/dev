using System;
using System.Collections.Generic;
using System.Text;

namespace MathLib.Structures
{
    public class Node
    {
        public Node() { }
        public Node(ulong id) {
            _nodeid = id;
        }

        protected ulong _nodeid = 0;


		
		
		public ulong NodeId
		{
			set {
				_nodeid = value;
			}
			
			get {
				return _nodeid;
			}
		}
    }
}
