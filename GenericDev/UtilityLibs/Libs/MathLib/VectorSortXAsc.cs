using System;
using System.Collections;

namespace MathLib
{
	/// <summary>
	/// 
	/// </summary>
	public class VectorSortXAsc : IComparer  
	{
		int IComparer.Compare( Object x1, Object x2 )  
		{
			double X1 = ((Vector2d)x1).X;
			double X2 = ((Vector2d)x2).X;
			if(X1 < X2)
				return -1;
			else if(X1 == X2)
				return 0;
			else
				return 1;
		}
	}
}


