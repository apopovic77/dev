﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Logicx.DiagramLib.Util
{
    public class PropertyChangedMeta
    {
        public PropertyChangedMeta(string property_name, object oldValue, object newValue)
        {
            PropertyName = property_name;
            NewValue = newValue;
            OldValue = oldValue;
        }

        public string PropertyName
        {
            get;
            private set;
        }

        public object NewValue
        {
            get;
            private set;
        }

        public object OldValue
        {
            get;
            private set;
        }
    }
}
