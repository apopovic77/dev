﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Interop;
using Logicx.WpfUtility.CustomControls.OverlayManager;

namespace Logicx.WpfUtility.WindowManagement
{
    public abstract class BaseFrame : BaseFrameBase<BaseFrame,SubMenuPanel, WindowTaskBar, WindowTitleBar, OverlayManager, SubMenuPanelMenuItem>
    {
        public BaseFrame(Window base_window, ReportInitStatusDelegate status_delegate, string[] startup_args)
            : base(base_window, status_delegate, startup_args)
        {}

        public BaseFrame(Window base_window, ReportInitStatusDelegate status_delegate)
            : base(base_window, status_delegate, null)
        { }

        public BaseFrame(Window base_window)
            : base(base_window, null, null)
        { }

        public BaseFrame() : base()
        {}

        protected override void RemoveWindowHook()
        {
            if (!_added_window_hook)
                return;

            Monitor.Enter(_change_hook_monitor);

            IntPtr handle = (new WindowInteropHelper(_base_window)).Handle;
            HwndSource.FromHwnd(handle).RemoveHook(new HwndSourceHook(WindowProc));

            _added_window_hook = false;

            Monitor.Exit(_change_hook_monitor);
        }
        protected override void AddWindowHook()
        {
            if (_added_window_hook)
                return;

            Monitor.Enter(_change_hook_monitor);

            IntPtr handle = (new WindowInteropHelper(_base_window)).Handle;
            HwndSource.FromHwnd(handle).AddHook(new HwndSourceHook(WindowProc));

            _added_window_hook = true;

            Monitor.Exit(_change_hook_monitor);
        }
        protected override bool User32GetCursorPos(out POINT lpPoint)
        {
            return GetCursorPos(out lpPoint);
        }
        protected override short User32GetKeyState(int keyCode)
        {
            return GetKeyState(keyCode);
        }

        private static System.IntPtr WindowProc(
            System.IntPtr hwnd,
            int msg,
            System.IntPtr wParam,
            System.IntPtr lParam,
            ref bool handled)
        {
            switch (msg)
            {
                case 0x0024:
                    WmGetMinMaxInfo(hwnd, lParam);
                    handled = true;
                    break;
            }

            return (System.IntPtr)0;
        }

        private static void WmGetMinMaxInfo(System.IntPtr hwnd, System.IntPtr lParam)
        {

            MINMAXINFO mmi = (MINMAXINFO)Marshal.PtrToStructure(lParam, typeof(MINMAXINFO));

            // Adjust the maximized size and position to fit the work area of the correct monitor
            int MONITOR_DEFAULTTONEAREST = 0x00000002;
            System.IntPtr monitor = MonitorFromWindow(hwnd, MONITOR_DEFAULTTONEAREST);

            if (monitor != System.IntPtr.Zero)
            {

                MONITORINFO monitorInfo = new MONITORINFO();
                GetMonitorInfo(monitor, monitorInfo);
                RECT rcWorkArea = monitorInfo.rcWork;
                RECT rcMonitorArea = monitorInfo.rcMonitor;
                mmi.ptMaxPosition.x = Math.Abs(rcWorkArea.left - rcMonitorArea.left);
                mmi.ptMaxPosition.y = Math.Abs(rcWorkArea.top - rcMonitorArea.top);
                mmi.ptMaxSize.x = Math.Abs(rcWorkArea.right - rcWorkArea.left);
                mmi.ptMaxSize.y = Math.Abs(rcWorkArea.bottom - rcWorkArea.top);
            }

            Marshal.StructureToPtr(mmi, lParam, true);
        }

        #region external call
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool GetCursorPos(out POINT lpPoint);

        [DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
        protected static extern short GetKeyState(int keyCode);

        [DllImport("user32")]
        internal static extern bool GetMonitorInfo(IntPtr hMonitor, MONITORINFO lpmi);

        /// <summary>
        /// 
        /// </summary>
        [DllImport("User32")]
        internal static extern IntPtr MonitorFromWindow(IntPtr handle, int flags);
        #endregion
    }
}
