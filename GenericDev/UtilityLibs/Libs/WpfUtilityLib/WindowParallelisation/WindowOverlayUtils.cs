﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows;

namespace Logicx.WpfUtility.WindowParallelisation
{
    public class WindowOverlayUtils
    {

    }


    class WindowFinder
    {
        [DllImport("user32.dll", SetLastError = true)]
        static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

        [DllImport("user32.dll")]
        static extern bool GetWindowRect(IntPtr hWnd, out RECT lpRect);

        [DllImport("user32.dll")]
        static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int X, int Y, int cx, int cy, uint uFlags);

        [StructLayout(LayoutKind.Sequential)]
        public struct RECT
        {
            public int Left;
            public int Top;
            public int Right;
            public int Bottom;
        }

        static readonly IntPtr HWND_TOPMOST = new IntPtr(-1);
        static readonly IntPtr HWND_NOTOPMOST = new IntPtr(-2);
        static readonly IntPtr HWND_TOP = new IntPtr(0);
        static readonly IntPtr HWND_BOTTOM = new IntPtr(1);

        const UInt32 SWP_NOSIZE = 0x0001;
        const UInt32 SWP_NOMOVE = 0x0002;
        const UInt32 SWP_NOZORDER = 0x0004;
        const UInt32 SWP_NOREDRAW = 0x0008;
        const UInt32 SWP_NOACTIVATE = 0x0010;
        const UInt32 SWP_FRAMECHANGED = 0x0020;
        const UInt32 SWP_SHOWWINDOW = 0x0040;
        const UInt32 SWP_HIDEWINDOW = 0x0080;
        const UInt32 SWP_NOCOPYBITS = 0x0100;
        const UInt32 SWP_NOOWNERZORDER = 0x0200;
        const UInt32 SWP_NOSENDCHANGING = 0x0400;



        IntPtr m_ptr = IntPtr.Zero;
        static IntPtr my_handle = IntPtr.Zero;
        Rect m_rect = new Rect();
        BackgroundWorker w;
        private WindowFinder()
        {
            m_ptr = IntPtr.Zero;

            w = new BackgroundWorker();
            w.DoWork += new DoWorkEventHandler(w_DoWork);
            w.RunWorkerCompleted += new RunWorkerCompletedEventHandler(w_RunWorkerCompleted);
        }


        static WindowFinder wf;
        public static void Start(string windowName, IntPtr handle)
        {
            my_handle = handle;

            wf = new WindowFinder();
            wf.w.RunWorkerAsync(windowName);
        }

        public static void Stop()
        {
            if (wf == null)
                wf = new WindowFinder();
            wf.w.CancelAsync();
        }

        void w_DoWork(object sender, DoWorkEventArgs e)
        {
            IntPtr hWnd = FindWindow(null, (string)e.Argument);
            while (!w.CancellationPending && hWnd == m_ptr)
            {
                System.Threading.Thread.Sleep(100);
                hWnd = FindWindow(null, (string)e.Argument);
            }
            m_ptr = hWnd;
            RECT r = new RECT();
            while (!w.CancellationPending && ((r.Left == r.Right) & (r.Bottom == r.Top)))
            {
                System.Threading.Thread.Sleep(100);
                GetWindowRect(m_ptr, out r);
            }


            m_rect = new Rect(r.Left, r.Top, r.Right - r.Left, r.Bottom - r.Top);


            e.Cancel = true;
        }

        void w_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (OnWindowFound != null)
                OnWindowFound(m_ptr, m_rect);
        }

        public delegate void OnWindowFoundHandler(IntPtr handle, Rect position);
        public static event OnWindowFoundHandler OnWindowFound;
    }
}
