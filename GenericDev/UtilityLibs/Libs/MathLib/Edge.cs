using System;

namespace MathLib
{
	/// <summary>
	/// 
	/// </summary>
	public struct Edge
	{
		public Edge(Vector2d v1,Vector2d v2){
			this.V1 = v1;
			this.V2 = v2;
		}
		public Vector2d V1;
		public Vector2d V2;

		public Vector2d GetLeftVertex(){
			if(V1.X <= V2.X)
				return V1;
			else 
				return V2;
		}


	    public Vector2d Vector
	    {
	        get { return V2 - V1; }
	    }

       

	    /// <summary>
		/// gibt ture zur�ck wenn die Edge vertical ist 
		/// --> X Koordinaten beider Vektoren sind gleich
		/// </summary>
		/// <returns></returns>
		public bool IsEdgeVertical(){
			if(V1.X == V2.X)
				return true;
			else
				return false;
		}

		public double GetLen(){
			return (V1-V2).GetLen();
		}
	}
}
