﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Logicx.WpfUtility.CustomControls.Taskbar.ContainerStyles
{
    /// <summary>
    /// Interaktionslogik für Monocle.xaml
    /// </summary>
    public partial class Monocle : UserControl
    {
        public Monocle()
        {
            InitializeComponent();
        }

        public Grid Grid
        {
            get { return grid; }
        }

        public void SetBackground(Brush bg_brush)
        {
            bg_ellipse.Fill = bg_brush;
        }
    }
}
