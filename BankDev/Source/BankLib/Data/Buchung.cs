﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;

namespace BankLib.Data
{
    public class Buchung
    {
        public Buchung()
        {
            //KtoNummer="";
            //ZahlungsReferenz="";
            //Waehrung="";
            //BuchungsText="";
            //UmsatzTextRaw="";
            //UmsatzTextRaw2="";
            //Owner="";
            //UmsatzType="";
            //BuchungsType="";
            //UmsatzText="";
            //UmsatzText2="";
            //MetaInfoRaw = "";
            MetaInfoPeriodicity = -1;
            //MetaInfoGroupAdd = "";
        }

        public override string ToString()
        {
            return "KtoNr " + KtoNummer + " Buchungsdatum " + BuchungsDatum.ToShortDateString() + " Betrag "+Betrag + " UmsatzType "+UmsatzType;
        }

        internal void WriteToStream(BinaryWriter writer)
        {
            BinaryHelper.WriteString(writer,KtoNummer);
            writer.Write(BuchungsDatum.ToBinary());
            writer.Write(ValutaDatum.ToBinary());
            writer.Write(Datum.ToBinary());
            writer.Write(UmsatzZeit.ToBinary());
            BinaryHelper.WriteString(writer, ZahlungsReferenz);
            BinaryHelper.WriteString(writer, Waehrung);
            writer.Write(Betrag);
            BinaryHelper.WriteString(writer, BuchungsText);
            BinaryHelper.WriteString(writer, UmsatzTextRaw);
            BinaryHelper.WriteString(writer, Owner);
            BinaryHelper.WriteString(writer, UmsatzType);
            BinaryHelper.WriteString(writer, BuchungsType);
            writer.Write(Kreditkarte);
            BinaryHelper.WriteString(writer, UmsatzTextRaw2);
            BinaryHelper.WriteString(writer, UmsatzText);
            BinaryHelper.WriteString(writer, UmsatzText2);
            BinaryHelper.WriteString(writer, MetaInfoRaw);
            writer.Write(MetaInfoPeriodicity);
            BinaryHelper.WriteString(writer, MetaInfoGroupAdd);
            BinaryHelper.WriteNullableDateTime(writer, MetaInfoCorrDateTime);
        }

        internal void LoadFromStream(BinaryReader reader)
        {
            KtoNummer = BinaryHelper.ReadString(reader);
            BuchungsDatum = DateTime.FromBinary(reader.ReadInt64());
            ValutaDatum = DateTime.FromBinary(reader.ReadInt64());
            Datum = DateTime.FromBinary(reader.ReadInt64());
            UmsatzZeit = DateTime.FromBinary(reader.ReadInt64());
            ZahlungsReferenz = BinaryHelper.ReadString(reader);
            Waehrung = BinaryHelper.ReadString(reader);
            Betrag = reader.ReadSingle();
            BuchungsText = BinaryHelper.ReadString(reader);
            UmsatzTextRaw = BinaryHelper.ReadString(reader);
            Owner = BinaryHelper.ReadString(reader);
            UmsatzType = BinaryHelper.ReadString(reader);
            BuchungsType = BinaryHelper.ReadString(reader);
            Kreditkarte = reader.ReadBoolean();
            UmsatzTextRaw2 = BinaryHelper.ReadString(reader);
            UmsatzText = BinaryHelper.ReadString(reader);
            UmsatzText2 = BinaryHelper.ReadString(reader);
            MetaInfoRaw = BinaryHelper.ReadString(reader);
            MetaInfoPeriodicity = reader.ReadInt32();
            MetaInfoGroupAdd = BinaryHelper.ReadString(reader);
            MetaInfoCorrDateTime = BinaryHelper.ReadNullableDateTime(reader);
        }
        
        public void LoadFromCsvLine(string line)
        {
            string[] line_arr = line.Split(";".ToCharArray());

            KtoNummer = line_arr[0];
            BuchungsDatum = GetDateFromCsvString(line_arr[2]);
            ValutaDatum = GetDateFromCsvString(line_arr[3]);
            Datum = ValutaDatum;
            UmsatzZeit = GetDateTimeFromCsvString(line_arr[4]);
            ZahlungsReferenz = line_arr[5];
            Waehrung = line_arr[6];
            Betrag = GetBetragFromCsv(line_arr[7]);

            BuchungsText = line_arr[8].Replace("\"","");
            BuchungsType = GetBuchungsTypeReadable(BuchungsText);

            if (line_arr.Length <= 9)
                return;

            UmsatzTextRaw = line_arr[9].Replace("\"", "");
            if (line_arr.Length > 10)
                UmsatzTextRaw2 = line_arr[10].Replace("\"", "");

            
            UmsatzText = GetTextWithoutNumbers(UmsatzTextRaw);
            UmsatzText2 = GetTextWithoutNumbers(UmsatzTextRaw2);

            CreateReadableUmsatzType();
        }

        private string GetTextWithoutNumbers(string text_raw)
        {
            if (text_raw == null)
                return "";

            string text_readable = "";
            string[] text_arr = text_raw.Split(" ".ToCharArray());
            for (int i = 0; i < text_arr.Length; i++)
            {
                if (string.IsNullOrEmpty(text_arr[i]))
                    continue;

                char first_c = text_arr[i][0];
                int iout;
                if (Int32.TryParse(first_c.ToString(), out iout))
                    return text_readable;

                if (text_arr[i].Length > 3)
                {
                    first_c = text_arr[i][2];
                    if (Int32.TryParse(first_c.ToString(), out iout))
                        return text_readable;
                }

                if (i > 0)
                    text_readable += " ";
                text_readable += text_arr[i];
            }
            return text_readable;
        }

        private float GetBetragFromCsv(string s_betrag)
        {
            try
            {
                //s_betrag = s_betrag.Replace(".", "");
                //s_betrag = s_betrag.Replace(",", ".");
                return Convert.ToSingle(s_betrag,CultureInfo.GetCultureInfo("de-DE").NumberFormat);
            }
            catch
            {
                return 0;
            }
        }

        private DateTime GetDateFromCsvString(string s_date)
        {
            try
            {
                if(String.IsNullOrEmpty(s_date)) return new DateTime();
                if (s_date.Length < 10) return new DateTime();
                if (s_date.Length == 10) return Convert.ToDateTime(s_date);
                return Convert.ToDateTime(s_date.Substring(0, 10));
            }
            catch { }
            return new DateTime();
        }



        private DateTime GetDateTimeFromCsvString(string s_date)
        {
            try
            {
                if (String.IsNullOrEmpty(s_date)) return new DateTime();
                if (s_date.Length < 10) return new DateTime();

                string s_date_only = s_date.Substring(0, 10);
                string[] time_arr = s_date.Substring(11, 5).Split(".".ToCharArray(),2);
                int hour = Convert.ToInt32(time_arr[0]);
                int min =  Convert.ToInt32(time_arr[1]);

                DateTime date = Convert.ToDateTime(s_date_only);
                date = date.AddHours(hour);
                date = date.AddMinutes(min);
                return date;

            }
            catch { }
            return new DateTime();
        }

        private string GetBuchungsTypeReadable(string org_name)
        {
            if (!BuchungsTypes.Values.ContainsKey(org_name))
                return org_name;
            return BuchungsTypes.Values[org_name];
        }

        public void CreateReadableUmsatzType()
        {
            UmsatzType = ExecTransformUmsatzTextRaw();

            Datum = ValutaDatum;

            if (BuchungsType == "POS")
                BuchungsType = "Bankomat Kassa";

            if(BuchungsType.StartsWith("SB"))
                UmsatzType = UmsatzType.Replace("UMBUCHUNG INTERNET AM", "UMBUCHUNG");
            UmsatzType = UmsatzType.Replace("DANKT", "").Trim();
            UmsatzType = UmsatzType.Replace("und Creditor ID:", "").Trim();
            UmsatzType = UmsatzType.Replace("Creditor ID:", "").Trim();
            UmsatzType = UmsatzType.Replace("REF:", "").Trim();

            if (UmsatzType.IndexOf("Paysafecard", System.StringComparison.Ordinal)>=0)
                UmsatzType = "Paysafecard";

            ExecTransformMetaInfo();
        }

        private void ExecTransformMetaInfo()
        {
            if (!string.IsNullOrEmpty(MetaInfoRaw))
            {
                string[] meta_arr = MetaInfoRaw.Split("|".ToCharArray());

                int pos_arr = 0;
                int periodicity;
                if (!Int32.TryParse(meta_arr[pos_arr], out periodicity))
                {
                    //dann gehen wir davon aus dass es sich um einen beschreibungstext handelt
                    if (!string.IsNullOrEmpty(UmsatzTextRaw2))
                        UmsatzType += " - " + meta_arr[pos_arr];
                    else
                        UmsatzType += " für " + meta_arr[pos_arr];
                    pos_arr++;
                }

                if (meta_arr.Length <= pos_arr)
                    return;

                //jetzt muss die perio
                bool is_periodicity = Int32.TryParse(meta_arr[pos_arr], out periodicity);
                if (!is_periodicity)
                {
                    //dann gehen wir davon aus dass es sich um eine Gruppen Zuordnungs Info handel
                    //also um den Namen der Gruppen zu der dieses Ding zugeordnet werden soll
                    MetaInfoGroupAdd = meta_arr[pos_arr];
                    return;
                }

                MetaInfoPeriodicity = periodicity;
                pos_arr++;

                if (meta_arr.Length <= pos_arr)
                    return;

                //check if meta attribute given
                int index_of_attrsep = meta_arr[pos_arr].IndexOf(":", System.StringComparison.Ordinal);
                if (index_of_attrsep >= 0)
                {
                    string[] meta_attrib_arr = meta_arr[pos_arr].Split(":".ToCharArray(), 2);
                    if (meta_attrib_arr.Length == 1)
                    {
                        //hier hat es einen syntax fehler im text gegeben
                        //geh davon aus das es sich um ein group add handelt
                        MetaInfoGroupAdd = meta_attrib_arr[0];
                        return;
                    }

                    //anweisung sollte geben sein
                    string command = meta_attrib_arr[0];
                    string command_value = meta_attrib_arr[1];

                    switch (command)
                    {
                        case "corrdate":
                            DateTime corr_date;
                            bool datecorr_success = DateTime.TryParse(command_value, out corr_date);
                            if (!datecorr_success)
                            {
                                AppHelper.WriteFileLog("\r\nKonnte MetaTag Anweisung nicht verarbeiten. Korrektur Datum konnte nicht geparsed werden. " + meta_arr[pos_arr] + "\r\n");
                                return;
                            }
                            MetaInfoCorrDateTime = corr_date;
                            Datum = MetaInfoCorrDateTime.Value;
                            return;
                        default:
                            AppHelper.WriteFileLog("\r\nKonnte MetaTag Anweisung " + meta_arr[pos_arr] + " nicht verarbeiten\r\n");
                            return;
                    }
                }

                MetaInfoGroupAdd = meta_arr[pos_arr];
            }
        }

        private string ExecTransformUmsatzTextRaw()
        {
            try
            {
                //check for MetaInfo
                int indexof_beg = UmsatzTextRaw2.IndexOf("[", System.StringComparison.Ordinal);
                if (indexof_beg >= 0)
                {
                    //metatag given
                    int indexof_end = UmsatzTextRaw2.IndexOf("]", System.StringComparison.Ordinal);
                    MetaInfoRaw = UmsatzTextRaw2.Substring(indexof_beg + 1, indexof_end - indexof_beg - 1);
                    UmsatzTextRaw2 = UmsatzTextRaw2.Substring(0, indexof_beg - 1);
                }

                if (string.IsNullOrEmpty(UmsatzTextRaw2))
                {
                    //check for MetaInfo
                    indexof_beg = UmsatzTextRaw.IndexOf("[", System.StringComparison.Ordinal);
                    if (indexof_beg >= 0)
                    {
                        //metatag given
                        int indexof_end = UmsatzTextRaw.IndexOf("]", System.StringComparison.Ordinal);
                        MetaInfoRaw = UmsatzTextRaw.Substring(indexof_beg + 1, indexof_end - indexof_beg - 1);
                        UmsatzTextRaw = UmsatzTextRaw.Substring(0, indexof_beg - 1);
                    }
                }




                //special case for pos int
                if (BuchungsText.StartsWith("POS") && UmsatzTextRaw.StartsWith("AT"))
                {
                    string[] sarr = UmsatzTextRaw.Split(" ".ToCharArray());
                    string umsatz_text = "";
                    if (sarr.Length > 6)
                        umsatz_text += sarr[6];
                    if (sarr.Length > 7)
                        umsatz_text += " " + sarr[7];
                    return GetTextWithoutNumbers(umsatz_text);
                }

                //special case for pos int
                if (BuchungsText == "SEPA-Zahlung INET" || BuchungsText == "Internetauftrag")
                {
                    if (UmsatzTextRaw.StartsWith("ÜBERWEISUNG"))
                    {
                        string[] sarr = UmsatzTextRaw2.Split("\n".ToCharArray(), 2);
                        if (sarr.Length == 2)
                            return "Überweisung an "+GetTextWithoutNumbers(sarr[1]);
                        else if(!string.IsNullOrEmpty(UmsatzTextRaw2))
                            return "Überweisung an "+GetTextWithoutNumbers(UmsatzTextRaw2);
                        else
                            return "Überweisung";
                    }
                }

                if (BuchungsText == "Barauszahlung")
                    return "Barauszahlung";

                if (BuchungsText == "Bareinzahlung")
                    return "Bareinzahlung";

                if (BuchungsType == "Bankomat")
                    return "BANKOMAT";

                //Return default name convention
                string[] org_name_split = UmsatzTextRaw.Split(" ".ToCharArray(), 2);
                if (!UmsatzTypes.Values.ContainsKey(org_name_split[0]))
                {
                    String res = GetTextWithoutNumbers(UmsatzTextRaw);
                    if (!string.IsNullOrEmpty(res))
                        return res;

                    if (string.IsNullOrEmpty(UmsatzTextRaw))
                        return "";

                    return UmsatzTextRaw.Split(" ".ToCharArray(), 2)[0];
                }
                else
                {
                    return UmsatzTypes.Values[org_name_split[0]];
                }
            }
            catch
            {
                return GetTextWithoutNumbers(UmsatzTextRaw);
            }
        }


        public bool IsSameBuchung(Buchung ob)
        {
            if (KtoNummer == ob.KtoNummer && BuchungsDatum == ob.BuchungsDatum
                && BuchungsText == ob.BuchungsText && UmsatzTextRaw == ob.UmsatzTextRaw &&
                UmsatzTextRaw2 == ob.UmsatzTextRaw2)
            {
                if (Betrag == ob.Betrag)
                {
                    return true;
                }
                else
                {
                    //Buchungszeile komplett identisch nur der Betrag stimmt nicht
                    AppHelper.WriteFileLog("\nBuchung nicht identisch\n" + ob.ToString()+"\n"+ToString()+"\n");
                    return false;
                }
            }
            return false;
        }

        public string BetragFormatted
        {
            get
            {
                return Betrag.ToString("C2", CultureInfo.CreateSpecificCulture("de-DE"));
            }
        }

        public string TooltipText
        {
            get
            {
                return String.Format("UmsatzTextRaw\n{0}\nUmsatzTextRaw2\n{1}\nMetaInfoRaw\n{3}\nBuchungstext\n{2}", UmsatzTextRaw, UmsatzTextRaw2, BuchungsText, MetaInfoRaw);
            }
        }

        public Konto Konto
        {
            get
            {
                if (string.IsNullOrEmpty(KtoNummer))
                    return null;
                return Konten.AlleKonten.FirstOrDefault(k => k.KtoNummer == KtoNummer);
            }
        }

        public string KtoNummer { get; set; }
        public DateTime Datum { get; set; }
        public DateTime BuchungsDatum { get; set; }
        public DateTime ValutaDatum { get; set; }
        public DateTime UmsatzZeit { get; set; }
        public string ZahlungsReferenz { get; set; }
        public string Waehrung { get; set; }
        public string BuchungsText { get; set; }
        public string UmsatzTextRaw { get; set; }
        public string UmsatzTextRaw2 { get; set; }
        public string Owner { get; set; }
        public string UmsatzType { get; set; }
        public string BuchungsType { get; set; }
        public bool Kreditkarte { get; set; }
        public string UmsatzText { get; set; }
        public string UmsatzText2 { get; set; }
        public string MetaInfoRaw { get; set; }
        public int MetaInfoPeriodicity { get; set; }
        public string MetaInfoGroupAdd { get; set; }
        public DateTime? MetaInfoCorrDateTime { get; set; }

        public float Betrag
        {
            get { return _betrag; }
            set { _betrag = (float)Math.Round(value, 2); }}

        private float _betrag = 0;


    }
}
