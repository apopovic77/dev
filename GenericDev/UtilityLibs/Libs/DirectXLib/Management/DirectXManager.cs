using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using Microsoft.DirectX.Direct3D;
using Logicx.Utilities;
using Logicx.DirectX.Utilities;

namespace Logicx.DirectX.Management
{
    public class DirectXManager : IDisposable
    {
        // The rendering device
        protected Device m_device3d;
        public Device Device3d
        {
            get {
                return m_device3d;
            }
        }
        public Control RenderTarget {
            get {
                return m_rendertarget;
            }
        }

        public bool IsDeviceInitialized = false;

        #region DirectX Attribs
        //private UserControl m_rendertarget_u;
        //private Form m_rentertarget_f;
        private Control m_rendertarget;
        protected PresentParameters m_presentParams;
        protected Settings3d m_graphicsSettings = new Settings3d();
        public Settings3d GraphicsSettings { get { return m_graphicsSettings; } }
        protected D3DEnumeration m_enumerationSettings = new D3DEnumeration();
        private string m_deviceStats; // String to hold D3D device stats
        private Caps m_graphicsCaps; // Caps for the device
        private CreateFlags m_behavior;// Indicate sw or hw vertex processing
        protected Caps Caps { get { return m_graphicsCaps; } }
        protected BehaviorFlags BehaviorFlags { get { return new BehaviorFlags(m_behavior); } }
        protected bool m_windowed = true; //is windowed rendering on
        private bool m_deviceLost = false;
        public int DxWindowWidth = 640;
        public int DxWindowHeight = 480;
        private LabelDebugWriter m_debuginfo;
        #endregion

        #region DirectX Events
        public event EventHandler DxDeviceLost;
        public event EventHandler DxDeviceReset;
        public event EventHandler DxDisposing;
        public event System.ComponentModel.CancelEventHandler DxResizing;
        #endregion

        #region Render Loop Processing Attribs
        private bool m_updateingScenes;
        private bool m_singleStep;
        private float m_framePerSecond = 0f;
        protected float m_appTime;             // Current time in seconds
        protected float m_elapsedTime;      // Time elapsed since last frame
        protected string m_frameStats; // String to hold frame stats
        private float m_lastTime = 0.0f; // The last time
        private int m_frames = 0; // Number of rames since our last update
        private int m_appPausedCount = 0; // How many times has the app been paused (and when can it resume)?
        #endregion

        public DirectXManager(Control rendertarget)
        {
            m_rendertarget = rendertarget;
        }

        public DirectXManager(Control rendertarget, LabelDebugWriter debuginfo)
        {
            m_rendertarget = rendertarget;
            m_debuginfo = debuginfo;
        }

        #region dx init
        protected virtual bool ConfirmDevice(Caps caps, VertexProcessingType vertexProcessingType, Format format) { return true; }

        public void InitGraphicsDevice()
        { 
        
            m_enumerationSettings.ConfirmDeviceCallback = new D3DEnumeration.ConfirmDeviceCallbackType(this.ConfirmDevice);
            m_enumerationSettings.Enumerate();

            try
            {
                if (!FindBestWindowedMode(true, false))
                    throw new NoCompatibleDevicesException();

                DXUtilities3d.Timer(TIMER.START);

                GraphicsAdapterInfo adapterInfo = m_graphicsSettings.AdapterInfo;
                GraphicsDeviceInfo deviceInfo = m_graphicsSettings.DeviceInfo;
                m_windowed = m_graphicsSettings.IsWindowed;
                // Set up the presentation parameters
                BuildPresentParamsFromSettings();

                if (deviceInfo.Caps.PrimitiveMiscCaps.IsNullReference)
                    throw new Exception3d("No Device available. Init Stop!");

                CreateFlags createFlags = new CreateFlags();
                //createFlags = CreateFlags.SoftwareVertexProcessing;

                if (m_graphicsSettings.VertexProcessingType == VertexProcessingType.Software)
                {
                    createFlags = CreateFlags.SoftwareVertexProcessing;
                    //throw new Exception3d("Software Vertex Processing currently not supported");
                }
                else if (m_graphicsSettings.VertexProcessingType == VertexProcessingType.Mixed)
                {
                    createFlags = CreateFlags.MixedVertexProcessing;
                    //throw new Exception3d("Mixed Vertex Processing currently not supported");
                }
                else if (m_graphicsSettings.VertexProcessingType == VertexProcessingType.Hardware)
                {
                    createFlags = CreateFlags.HardwareVertexProcessing;
                }
                else if (m_graphicsSettings.VertexProcessingType == VertexProcessingType.PureHardware)
                {
                    createFlags = CreateFlags.HardwareVertexProcessing | CreateFlags.PureDevice;
                }
                else
                    throw new ApplicationException();

                createFlags |= CreateFlags.MultiThreaded;

               // CREATE THE DX DEVICE, MOST IMPORTANT
                m_device3d = new Device(m_graphicsSettings.AdapterOrdinal,
                                        m_graphicsSettings.DevType,
                                        m_rendertarget, 
                                        createFlags, 
                                        m_presentParams);

                // Store device Caps
                m_graphicsCaps = m_device3d.DeviceCaps;
                m_behavior = createFlags;

                // Store device description
                if (deviceInfo.DevType == DeviceType.Reference)
                    m_deviceStats = "REF";
                else if (deviceInfo.DevType == DeviceType.Hardware)
                    m_deviceStats = "HAL";
                else if (deviceInfo.DevType == DeviceType.Software)
                    m_deviceStats = "SW";

                BehaviorFlags behaviorFlags = new BehaviorFlags(createFlags);
                if ((behaviorFlags.HardwareVertexProcessing) &&
                    (behaviorFlags.PureDevice))
                {
                    if (deviceInfo.DevType == DeviceType.Hardware)
                        m_deviceStats += " (pure hw vp)";
                    else
                        m_deviceStats += " (simulated pure hw vp)";
                }
                else if ((behaviorFlags.HardwareVertexProcessing))
                {
                    if (deviceInfo.DevType == DeviceType.Hardware)
                        m_deviceStats += " (hw vp)";
                    else
                        m_deviceStats += " (simulated hw vp)";
                }
                else if (behaviorFlags.MixedVertexProcessing)
                {
                    if (deviceInfo.DevType == DeviceType.Hardware)
                        m_deviceStats += " (mixed vp)";
                    else
                        m_deviceStats += " (simulated mixed vp)";
                }
                else if (behaviorFlags.SoftwareVertexProcessing)
                {
                    m_deviceStats += " (sw vp)";
                }

                if (deviceInfo.DevType == DeviceType.Hardware)
                {
                    m_deviceStats += ": ";
                    m_deviceStats += adapterInfo.AdapterDetails.Description;
                }

                // Setup the event handlers for our device
                m_device3d.DeviceLost += new System.EventHandler(Device3dLostHandler);
                m_device3d.DeviceReset += new System.EventHandler(Device3dResetHandler);
                m_device3d.Disposing += new System.EventHandler(Device3dDisposingHandler);
                m_device3d.DeviceResizing += new System.ComponentModel.CancelEventHandler(Device3dRisizingHandler);

                IsDeviceInitialized = true;

                try
                {
                    // Initialize the app's device-dependent objects
                    Device3dLostHandler(null, null);
                    Device3dResetHandler(null, null);
                    return;
                }
                catch
                {
                    // Cleanup before we try again
                    Device3dLostHandler(null, null);
                    Device3dDisposingHandler(null, null);
                    m_device3d.Dispose();
                    m_device3d = null;
                    if (m_rendertarget.Disposing)
                        return;
                }

                
            }
            catch 
            {
                throw new Exception3d("DirectX 3d Initialization failed! No Rendering Device available. Maybe Device is in use.");
            }
        }

        /// <summary>
        /// Sets up graphicsSettings with best available windowed mode, subject to 
        /// the doesRequireHardware and doesRequireReference constraints.  
        /// </summary>
        /// <param name="doesRequireHardware">Does the device require hardware support</param>
        /// <param name="doesRequireReference">Does the device require the ref device</param>
        /// <returns>true if a mode is found, false otherwise</returns>
        public bool FindBestWindowedMode(bool doesRequireHardware, bool doesRequireReference)
        {
            // Get display mode of primary adapter (which is assumed to be where the window will appear)
            DisplayMode primaryDesktopDisplayMode = Manager.Adapters[0].CurrentDisplayMode;

            GraphicsAdapterInfo bestAdapterInfo = null;
            GraphicsDeviceInfo bestDeviceInfo = null;
            DeviceCombo bestDeviceCombo = null;

            foreach (GraphicsAdapterInfo adapterInfo in m_enumerationSettings.AdapterInfoList)
            {
                foreach (GraphicsDeviceInfo deviceInfo in adapterInfo.DeviceInfoList)
                {
                    if (doesRequireHardware && deviceInfo.DevType != DeviceType.Hardware)
                        continue;
                    if (doesRequireReference && deviceInfo.DevType != DeviceType.Reference)
                        continue;
                    foreach (DeviceCombo deviceCombo in deviceInfo.DeviceComboList)
                    {
                        bool adapterMatchesBackBuffer = (deviceCombo.BackBufferFormat == deviceCombo.AdapterFormat);
                        if (!deviceCombo.IsWindowed)
                            continue;
                        if (deviceCombo.AdapterFormat != primaryDesktopDisplayMode.Format)
                            continue;
                        // If we haven't found a compatible DeviceCombo yet, or if this set
                        // is better (because it's a HAL, and/or because formats match better),
                        // save it
                        if (bestDeviceCombo == null ||
                            bestDeviceCombo.DevType != DeviceType.Hardware && deviceInfo.DevType == DeviceType.Hardware ||
                            deviceCombo.DevType == DeviceType.Hardware && adapterMatchesBackBuffer)
                        {
                            bestAdapterInfo = adapterInfo;
                            bestDeviceInfo = deviceInfo;
                            bestDeviceCombo = deviceCombo;
                            if (deviceInfo.DevType == DeviceType.Hardware && adapterMatchesBackBuffer)
                            {
                                // This windowed device combo looks great -- take it
                                goto EndWindowedDeviceComboSearch;
                            }
                            // Otherwise keep looking for a better windowed device combo
                        }
                    }
                }
            }
        EndWindowedDeviceComboSearch:
            if (bestDeviceCombo == null)
            {
                m_debuginfo.WriteInfo("DxManager: No suitable Device found");
                return false;
            }

            m_graphicsSettings.WindowedAdapterInfo = bestAdapterInfo;
            m_graphicsSettings.WindowedDeviceInfo = bestDeviceInfo;
            m_graphicsSettings.WindowedDeviceCombo = bestDeviceCombo;
            m_graphicsSettings.IsWindowed = true;
            m_graphicsSettings.WindowedDisplayMode = primaryDesktopDisplayMode;
            m_graphicsSettings.WindowedWidth = m_rendertarget.ClientRectangle.Right - m_rendertarget.ClientRectangle.Left;
            m_graphicsSettings.WindowedHeight = m_rendertarget.ClientRectangle.Bottom - m_rendertarget.ClientRectangle.Top;
            if (m_enumerationSettings.AppUsesDepthBuffer)
                m_graphicsSettings.WindowedDepthStencilBufferFormat = (DepthFormat)bestDeviceCombo.DepthStencilFormatList[0];
            m_graphicsSettings.WindowedMultisampleType = (MultiSampleType)bestDeviceCombo.MultiSampleTypeList[0];
            m_graphicsSettings.WindowedMultisampleQuality = 0;
            m_graphicsSettings.WindowedVertexProcessingType = (VertexProcessingType)bestDeviceCombo.VertexProcessingTypeList[0];
            //m_graphicsSettings.WindowedVertexProcessingType = VertexProcessingType.Software;
            m_graphicsSettings.WindowedPresentInterval = (PresentInterval)bestDeviceCombo.PresentIntervalList[0];

            if (m_debuginfo != null)
            {
                m_debuginfo.WriteInfo("WindowedAdapterInfo");
                m_debuginfo.WriteInfo(m_graphicsSettings.WindowedAdapterInfo.ToString().Split("\n".ToCharArray())[0]);
                m_debuginfo.WriteInfo("WindowedDeviceInfo");
                m_debuginfo.WriteInfo(m_graphicsSettings.WindowedDeviceInfo.ToString().Split("\n".ToCharArray())[0]);
                m_debuginfo.WriteInfo("WindowedVertexProcessingType");
                m_debuginfo.WriteInfo(m_graphicsSettings.WindowedVertexProcessingType.ToString().Split("\n".ToCharArray())[0]);
            }
            return true;
        }


        /// <summary>
        /// Build presentation parameters from the current settings
        /// </summary>
        public void BuildPresentParamsFromSettings()
        {
            if (m_presentParams == null) m_presentParams = new PresentParameters();
            m_presentParams.Windowed = m_graphicsSettings.IsWindowed;
            m_presentParams.BackBufferCount = 1;
            m_presentParams.MultiSample = m_graphicsSettings.MultisampleType;
            m_presentParams.MultiSampleQuality = m_graphicsSettings.MultisampleQuality;
            m_presentParams.SwapEffect = SwapEffect.Discard;
            m_presentParams.EnableAutoDepthStencil = m_enumerationSettings.AppUsesDepthBuffer;
            m_presentParams.AutoDepthStencilFormat = m_graphicsSettings.DepthStencilBufferFormat;
            m_presentParams.PresentFlag = PresentFlag.None;
            m_presentParams.BackBufferWidth = m_rendertarget.ClientRectangle.Right - m_rendertarget.ClientRectangle.Left;
            m_presentParams.BackBufferHeight = m_rendertarget.ClientRectangle.Bottom - m_rendertarget.ClientRectangle.Top;
            m_presentParams.BackBufferFormat = m_graphicsSettings.DeviceCombo.BackBufferFormat;
            m_presentParams.FullScreenRefreshRateInHz = 0;
            m_presentParams.PresentationInterval = PresentInterval.Immediate;
            m_presentParams.DeviceWindow = m_rendertarget;
        }
        #endregion

        #region D3d EventHandlers
        protected virtual void Device3dDisposingHandler(System.Object sender, System.EventArgs e)
        {
            if (DxDisposing != null)
                DxDisposing(sender, e);
        }
        protected virtual void Device3dLostHandler(System.Object sender, System.EventArgs e)
        {
            if (DxDeviceLost != null)
                DxDeviceLost(sender, e);
        }
        protected void Device3dResetHandler(System.Object sender, System.EventArgs e)
        {
            if (DxDeviceReset != null)
                DxDeviceReset(sender, e);
        }
        /// <summary>
        /// Fired when our environment was resized
        /// </summary>
        /// <param name="sender">the device that's resizing our environment</param>
        /// <param name="e">Set the cancel member to true to turn off automatic device reset</param>
        protected void Device3dRisizingHandler(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (DxResizing != null)
                DxResizing(sender, e);
        }
        #endregion

        #region Destruktor
		private bool m_disposed = false;
		~DirectXManager() {
			Dispose(false);
		}
		public void Dispose() {
			Dispose(true);
			System.GC.SuppressFinalize(this);
		}
		public void Dispose(bool explicitCall) {
			if(!this.m_disposed) {
				if(explicitCall){
					//explicit call object are available perform custom cleanup
				}

                try
                {
                }
                catch { }

			}
			m_disposed = true;
        }
        #endregion

    }
}
