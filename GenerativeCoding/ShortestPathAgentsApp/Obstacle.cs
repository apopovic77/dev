﻿

using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace ShortestPathAgentsApp
{
    public class Obstacle
    {
        //public Obstacle(Canvas canvas)
        //{
        //    Canvas = canvas;
        //}
        public double Width
        {
            get { return Shape.Width; }
        }

        public double Height
        {
            get { return Shape.Height; }
        }

        public double X
        {
            get { return Canvas.GetLeft(Shape); }
            set { Canvas.SetLeft(Shape, value); }
        }

        public double Y
        {
            get { return Canvas.GetTop(Shape); }
            set { Canvas.SetTop(Shape, value); }
        }

        public double Rotation
        {
            set { Shape.LayoutTransform = new RotateTransform(value); }
            get
            {
                if (Shape.LayoutTransform == null)
                    return 0;
                if (!(Shape.LayoutTransform is RotateTransform))
                    return 0;

                return ((RotateTransform) Shape.LayoutTransform).Angle;
            }
        }
        public Shape Shape;
    }
}