﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Shapes;
using Logicx.DiagramLib.ViewConfig;

namespace Logicx.DiagramLib.Scales
{
    class IntScaleControl : TypedScaleControl<int>
    {
        #region Constants
        #endregion

        #region Construction and Initialization
        public IntScaleControl()
        {
            _scale_unit_small = 1;
        }

        public IntScaleControl(AxisScaleConfig<int> config, string int_values_ascending_string)
            : this()
        {
            Config = config;
            IntValuesAscendingString = int_values_ascending_string;
        }

        #endregion

        #region Operations

        private void Init(bool generate_always)
        {
            _current_scale = ScaleFactory.GetInstance().GetScale(Config);

            if (_current_scale == null)
                throw new ArgumentException("No ScaleType available for the given time range " +
                                            Config.Start + " " + Config.End);

            _current_scale.Control = this;

            double size = Config != null && Config.Orientation == Orientation.Horizontal ? ActualWidth : ActualHeight;

            _current_scale.Init(size, generate_always);
        }

        public void AlignLeftTop(UIElement ui_element, double left, double top)
        {
            Canvas.SetLeft(ui_element, left);
            Canvas.SetTop(ui_element, top);
        }

        public void AlignLeftBottom(UIElement ui_element, double left, double bottom)
        {
            Canvas.SetLeft(ui_element, left);
            Canvas.SetBottom(ui_element, bottom);
        }

        public double GetRenderWidth()
        {
            return _render_width;
        }

        public double GetRenderHeight()
        {
            return _render_height;
        }

        public void ClearChildren()
        {
            foreach (var child in ScaleCanvas.Children)
            {
                if (child is Rectangle)
                    BindingOperations.ClearBinding((Rectangle)child, Rectangle.FillProperty);
                if (child is TextBlock)
                    BindingOperations.ClearBinding((TextBlock)child, TextBlock.StyleProperty);
            }

            ScaleCanvas.Children.Clear();
        }

        public void AddToChildren(List<UIElement> elements)
        {
            foreach (var ui_element in elements)
            {
                ScaleCanvas.Children.Add(ui_element);
            }
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);

            ClearChildren();
            if (_config != null)
            {
                _config.PropertyChanged -= _config_PropertyChanged;
                _config.Dispose();
            }

            if (_current_scale != null)
                _current_scale.Dispose();

            _current_scale = null;
        }

        private void AlignData()
        {
            _current_scale.AlignData(Config.Orientation);
        }

        public override double TransformAxisValueToPixel(int value)
        {
            return TransformAxisValueToPixel(value, false);
        }
        public override double TransformAxisValueToPixel(int value, bool margin_sensitive)
        {
            int wholespan = Config.End - Config.Start + 1;
            int diff = wholespan - (value - Config.Start);

            //Debug.WriteLine("value " + value + " has pixel " + ((margin_sensitive ? Config.Padding.Left : 0) + _render_width * ((double)diff / wholespan)));

            if (Config.Orientation == Orientation.Horizontal)
                return (margin_sensitive ? Config.Padding.Left : 0) + _render_width * ((double)diff / wholespan);

            return (margin_sensitive ? Config.Padding.Bottom : 0) + _render_height * ((double)diff / wholespan);
        }

        public override int TransformAxisPixelToValue(double pixel)
        {
            return TransformAxisPixelToValue(pixel, false);
        }

        public override int TransformAxisPixelToValue(double pixel, bool margin_sensitive)
        {
            int wholespan = Config.End - Config.Start + 1;
            if (Config.Orientation == Orientation.Horizontal)
            {
                return (int)Math.Round(_render_width == 0 ? 0 : (pixel - (margin_sensitive ? Config.Padding.Left : 0)) / _render_width * wholespan);
            }

            return (int)Math.Round(_render_height == 0 ? 0 : Config.Start + ((_render_height - pixel) / _render_height) * wholespan);
        }

        public override double GetUnitSize()
        {
            return _current_scale.GetUnitSize(Config.Orientation);
        }

        public override List<double> GetGridLinePositions()
        {
            //return _current_scale.GetGridLinePositions();
            if (_current_scale == null)
                Init(true);
            //return new List<double>();

            return _current_scale.GetGridLinePositions();
        }

        public override double GetScaleStartPixel()
        {
            if (Config.Orientation == Orientation.Horizontal)
                return Config.Padding.Left;

            return Config.Padding.Top;
        }

        public override double GetScaleEndPixel()
        {
            if (Config.Orientation == Orientation.Horizontal)
                return this.ActualWidth - Config.Padding.Right;

            return this.ActualHeight - Config.Padding.Bottom;
        }

        #endregion

        #region Event Handlers

        private void IntScaleCanvasChanged()
        {
            _render_width = ActualWidth;
            _render_height = ActualHeight;

            if (_render_width >= Config.Padding.Left + Config.Padding.Right)
                _render_width -= Config.Padding.Left + Config.Padding.Right;

            if (_render_height >= Config.Padding.Top + Config.Padding.Bottom)
                _render_height -= Config.Padding.Top + Config.Padding.Bottom;

            InitAndAlign(true);
        }


        protected override void OnScaleSizeChanged(SizeChangedEventArgs e)
        {
            if (Config == null)
                return;

            if (e.WidthChanged)
            {
                _render_width = e.NewSize.Width;
                //if (_render_width >= Config.Padding.Left + Config.Padding.Right)
                //    _render_width -= Config.Padding.Left + Config.Padding.Right;
            }

            if (e.HeightChanged)
            {
                _render_height = e.NewSize.Height;
                //if (_render_height >= Config.Padding.Top + Config.Padding.Bottom)
                //    _render_height -= Config.Padding.Top + Config.Padding.Bottom;
            }

            InitAndAlign(e.WidthChanged || e.HeightChanged);
        }

        protected override void InitAndAlign(bool generate_always)
        {
            if (Config != null)
                Padding = Config.Padding;

            Init(generate_always);

            AlignData();

            OnScalingChanged();
        }



        void _config_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Start")
                IntScaleCanvasChanged();
            else if (e.PropertyName == "End")
                IntScaleCanvasChanged();
            else if (e.PropertyName == "Description")
                IntScaleCanvasChanged();
            else if (e.PropertyName == "Orientation")
            {
                IntScaleCanvasChanged();

            }
            else if (e.PropertyName == "Padding")
                IntScaleCanvasChanged();
            else if (e.PropertyName == "Alignment")
                ;
        }
        #endregion

        #region Properties

        public override AxisScaleConfig<int> Config
        {
            get
            {
                return _config;
            }
            set
            {
                if (_config != null)
                    _config.PropertyChanged -= _config_PropertyChanged;

                _config = value;

                _config.PropertyChanged += _config_PropertyChanged;

                _current_scale = ScaleFactory.GetInstance().GetScale(_config);

                if (_current_scale == null)
                    throw new ArgumentException("No ScaleType available for the given time range " +
                                                Config.Start + " " + Config.End);

                _current_scale.Control = this;
            }
        }

        public string IntValuesAscendingString
        {
            get;
            private set;
        }

        public override bool CanKeyMove
        {
            get { return true; }
        }

        public override double ScaleUnitSmallInPixel
        {
            get
            {
                if (_config != null)
                {
                    double first = TransformAxisValueToPixel(_config.Start);
                    double second = TransformAxisValueToPixel(_config.Start + _scale_unit_small);

                    return Math.Abs(second - first);
                }

                return 0;
            }
        }
        #endregion

        #region Dependency Properties
        #endregion

        #region Events
        #endregion

        #region Routed Events
        #endregion

        #region Attributes

        private ScaleType _current_scale;

        private double _render_width;
        private double _render_height;
        private AxisScaleConfig<int> _config;

        private readonly int _scale_unit_small;

        #endregion

        #region Tests

        #endregion

    }
}
