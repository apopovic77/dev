﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Reflection;
using System.Text;

namespace LoggingLib.Data
{
    public class LoggingDataContextBase : DataContext
    {
        #region Construction and initialization
        public LoggingDataContextBase(string fileOrServerOrConnection)
            : base(fileOrServerOrConnection)
        {
            EnsureMetaData();
        }

        public LoggingDataContextBase(IDbConnection connection)
            : base(connection)
        { 
            EnsureMetaData(); 
        }

        public LoggingDataContextBase(IDbConnection connection, MappingSource mapping)
            : base(connection, mapping)
        {
            EnsureMetaData();
        }

        public LoggingDataContextBase(string fileOrServerOrConnection, MappingSource mapping)
            : base(fileOrServerOrConnection, mapping)
        {
            EnsureMetaData();
        }

        private void EnsureMetaData()
        {
            // will call the GETer of all Table properties
            // this will force an internal datacontext update of the meta information

            foreach(PropertyInfo pI in GetType().GetProperties())
            {
                if(pI.PropertyType.IsGenericType)
                {
                    if(pI.PropertyType.GetGenericTypeDefinition() == typeof(System.Data.Linq.Table<>))
                    {
                        object val = pI.GetValue(this, null);
                    }
                }
            }
        }
        #endregion
    }
}
