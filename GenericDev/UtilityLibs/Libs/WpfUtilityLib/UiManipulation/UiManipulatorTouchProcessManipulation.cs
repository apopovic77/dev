﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using Windows7.Multitouch.WPF;
using Windows7.Multitouch.Manipulation;
using ManipulationDeltaEventArgs = Windows7.Multitouch.Manipulation.ManipulationDeltaEventArgs;

namespace Logicx.WpfUtility.UiManipulation
{
    public class TouchProcessManipulation : UiManipulator
    {
        public TouchProcessManipulation(UIElement ui_element, Canvas canvas, UIElement event_base, EnabledAxes enabled_axes) : this(ui_element, canvas, event_base, enabled_axes, true) { }
        public TouchProcessManipulation(UIElement ui_element, Canvas canvas, UIElement event_base, EnabledAxes enabled_axes, bool initialize_canvas_position) : base(ui_element, canvas, event_base, enabled_axes, initialize_canvas_position) { }
        
        public override void Register()
        {
            TransformGroup transform_group = new TransformGroup();
            _translate_transform = new TranslateTransform();
            _translate_transform.X = 0;
            _translate_transform.Y = 0;
            _scale_transform = new ScaleTransform();
            _scale_transform.ScaleX = 1;
            _scale_transform.ScaleY = 1;
            _rotate_transform = new RotateTransform();
            _rotate_transform.Angle = 0;
            transform_group.Children.Add(_scale_transform);
            transform_group.Children.Add(_rotate_transform);
            transform_group.Children.Add(_translate_transform);
            _ui_element.RenderTransform = transform_group;
            _ui_element.RenderTransformOrigin = new Point(0.5, 0.5);

            _processor = new SmoothManipulationProcessor();
            _processor.ManipulationDelta += ProcessManipulationDelta;
            _processor.SmoothTranslation = 0.2f;
            _processor.SmoothRotation = 0.08f;
            _processor.SmoothScale = 0.2f;

            _ui_element.StylusDown += StylusDown;
        }

        public override void DeRegister()
        {
            _ui_element.StylusDown -= StylusDown;

            _processor.CompleteManipulation();
            _processor.ManipulationDelta -= ProcessManipulationDelta;
            _processor = null;

            _translate_transform = null;
            _scale_transform = null;
            _rotate_transform = null;
        }

        private void RegisterManipulationEvents()
        {
            if (_events_registered)
                return;

            SetCanvasBackground();
            _event_base.StylusUp += StylusUp;
            _event_base.StylusMove += StylusMove;
            _event_base.StylusLeave += StylusLeave;

            _events_registered = true;
        }

        private void DeRegisterManipulationEvents()
        {
            if (!_events_registered)
                return;

            UnsetCanvasBackground();
            _event_base.StylusUp -= StylusUp;
            _event_base.StylusMove -= StylusMove;
            _event_base.StylusLeave -= StylusLeave;

            _events_registered = false;
        }

        public void StylusDown(object sender, StylusDownEventArgs e)
        {
            e.Handled = true;

            if (!_events_registered)
            {
                RegisterManipulationEvents();
                RaisUiManipulationStarted(_ui_element);
                _my_stylus_ids = new List<int>();
                _my_stylus_ids.Add(e.StylusDevice.Id);
            }
            else
            {
                if (_my_stylus_ids.Count == 2)
                    return;
                _my_stylus_ids.Add(e.StylusDevice.Id);
            }

            Point pos = e.GetPosition(_canvas);
            _processor.ProcessDown((uint)e.StylusDevice.Id, pos.ToDrawingPointF());
        }

        public void StylusUp(object sender, StylusEventArgs e)
        {
            if (!_events_registered)
                return;

            if (!_my_stylus_ids.Contains(e.StylusDevice.Id))
                return;

            //e.Handled = true;

            _my_stylus_ids.Remove(e.StylusDevice.Id);

            Point pos = e.GetPosition(_canvas);
            _processor.ProcessUp((uint)e.StylusDevice.Id, pos.ToDrawingPointF());

            if (_my_stylus_ids.Count > 0)
                return;

            DeRegisterManipulationEvents();
            RaiseUiManipulationCompleted(_ui_element);
        }

        public void StylusMove(object sender, StylusEventArgs e)
        {
            if (!_my_stylus_ids.Contains(e.StylusDevice.Id))
                return;

            e.Handled = true;

            Point pos = e.GetPosition(_canvas);
            _processor.ProcessMove((uint)e.StylusDevice.Id, pos.ToDrawingPointF());
        }

        public void StylusLeave(object sender, StylusEventArgs e)
        {
            e.Handled = true;

            StylusUp(sender, e);
        }

        public void ProcessManipulationDelta(object sender, ManipulationDeltaEventArgs e)
        {
            double left = Canvas.GetLeft(_ui_element);
            double top = Canvas.GetTop(_ui_element);

            if (double.IsNaN(left))
                left = 0;
            if (double.IsNaN(top))
                top = 0;

            if (_enabled_axes == EnabledAxes.XAxis || _enabled_axes == EnabledAxes.All)
                Canvas.SetLeft(_ui_element, left + e.TranslationDelta.Width);
            if (_enabled_axes == EnabledAxes.YAxis || _enabled_axes == EnabledAxes.All)
                Canvas.SetTop(_ui_element, top + e.TranslationDelta.Height);

            _rotate_transform.Angle += e.RotationDelta * 180 / Math.PI;

            _scale_transform.ScaleX *= e.ScaleDelta;
            _scale_transform.ScaleY *= e.ScaleDelta;
        }

        #region Attributes
        private SmoothManipulationProcessor _processor;
        private TranslateTransform _translate_transform;
        private ScaleTransform _scale_transform;
        private RotateTransform _rotate_transform;

        private List<int> _my_stylus_ids;
        private bool _events_registered;
        #endregion
    }
}
