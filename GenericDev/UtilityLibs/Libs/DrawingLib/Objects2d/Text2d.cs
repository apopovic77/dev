using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Collections;
using System.Windows.Forms;
using Logicx.Utilities;
using Utility;
using Logicx.Drawing.Objects3d;

namespace Logicx.Drawing.Objects2d
{
    #region Font Glyph Structs
    public struct FontStyleDescription
    {
        public string FontName;
        public int FontSize;
        public FontStyle FontWeight;
        public override string ToString()
        {
            return FontName + " " + FontSize + " " + FontWeight;
        }

    }
    public struct GlyphStyleDescription
    {
        public char Char;
        public FontStyleDescription FontStyleDescription;
    }
    public struct GlyphDescription
    {
        public char Char;
        public int Width;
        public int Height;
    }
    #endregion


    /// <summary>
    /// THIS CLASS IS NOT THREAD SAFE
    /// </summary>
    public class Text2d : Object2d
    {


        #region construction
        /// <summary>
        /// parent not needed in this class
        /// set the <fsize=8>text</fsize> like <fname=Courier>love me, <b>tell me <#FF0000>baby</b> what went</fname> wrong, try</#> to understand
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="text"></param>
        public Text2d(Control parent, string text) : base(parent)
        {
            _text = text;

            _default_fontstyle = new FontStyleDescription();
            _default_fontstyle.FontName = "Arial";
            _default_fontstyle.FontSize = 9;
            _default_fontstyle.FontWeight = FontStyle.Regular;
            _default_fontcolor = Color.Black;

            _active_fontstyle = _default_fontstyle;
            _active_fontcolor = _default_fontcolor;

            if (_fonts == null)
            {
                _fonts = new Hashtable();
                _brushes = new Hashtable();
                _glyphdescriptions = new Hashtable();
            }

            SetFont();
            SetBrush();
        }
        #endregion

        #region Font Rendering MAIN
        protected override void DoRender(System.Drawing.Graphics g)
		{
            _graphics = g;

            if (_text == null)
                return;

            Point curr_pos = new Point();
            for (int i = 0; i < _text.Length; i++)
            {   
                char curr_char = _text[i];
                char next_char = (i == _text.Length-1)?'0':_text[i+1];

                if (curr_char == '<')
                { 
                    //meta info detected
                    string metainfo = GetMetaInfo(ref i);
                    ProcessMetaInfo(metainfo);
                    continue;
                }

                GlyphDescription glyph_desc = GetGlyphDescription(curr_char);
                RenderGlyph(g, curr_char, curr_pos);

                curr_pos.X += glyph_desc.Width;
                
            }   
		}

        private void RenderGlyph(System.Drawing.Graphics g, char curr_char, Point curr_pos)
        {
            curr_pos.X += Position.X;
            curr_pos.Y += Position.Y;
            g.DrawString(curr_char.ToString(),_active_font,_active_brush, curr_pos);

        }
        #endregion

        #region Font rendering HELPER METHODS
        private void ProcessMetaInfo(string metainfo)
        {
            if (metainfo[0] == '#')
            { 
                //color setting
                _active_fontcolor = DataTypeFactory.Hex2Color(metainfo);
                SetBrush();
                return;
            }

            if (metainfo.StartsWith("fname"))
            {
                _active_fontstyle.FontName = metainfo.Substring(metainfo.IndexOf("=")+1);
                SetFont();
                return;
            }

            if (metainfo.StartsWith("fsize"))
            {
                _active_fontstyle.FontSize = Convert.ToInt32(metainfo.Substring(metainfo.IndexOf("=")+1));
                SetFont();
                return;
            }

            switch(metainfo)
            {
                case "b":
                    //bold font active
                    _active_fontstyle.FontWeight = FontStyle.Bold;
                    SetFont();
                    break;
                case "/b":
                    _active_fontstyle.FontWeight = FontStyle.Regular;
                    SetFont();
                    break;
                case "/#":
                    _active_fontcolor = _default_fontcolor;
                    SetBrush();
                    break;
                case "/fname":
                    _active_fontstyle.FontName = _default_fontstyle.FontName;
                    SetFont();
                    break;
                case "/fsize":
                    _active_fontstyle.FontSize = _default_fontstyle.FontSize;
                    SetFont();
                    break;
                default:
                    throw new Exception("cannot process metainfo; metainfo unknown");
            }
        }

        private void SetFont()
        {
            //set out of cache if there
            if (_fonts.Contains(_active_fontstyle))
            {
                _active_font = (Font)_fonts[_active_fontstyle];
                return;
            }

            //create the Font new as not given
            _active_font = new Font(_active_fontstyle.FontName, _active_fontstyle.FontSize, _active_fontstyle.FontWeight);
            //save to cache
            _fonts.Add(_active_fontstyle, _active_font);
        }

        private void SetBrush()
        {
            //set out of cache if there
            if (_brushes.Contains(_active_fontcolor))
            {
                _active_brush = (Brush)_brushes[_active_fontcolor];
                return;
            }

            //create the Font new as not given
            _active_brush = new SolidBrush(_active_fontcolor);
            //save to cache
            _brushes.Add(_active_fontcolor, _active_brush);            
        }

        private string GetMetaInfo(ref int index_meta_desc_start)
        {
            int index_meta_desc_end = _text.Substring(index_meta_desc_start).IndexOf(">");
            string metainfo = _text.Substring(index_meta_desc_start).Substring(1, index_meta_desc_end-1);

            //advance cursor
            index_meta_desc_start += index_meta_desc_end;
            return metainfo;
        }

        private GlyphDescription GetGlyphDescription(char curr_char)
        {
            //load from cache if there
            GlyphStyleDescription gstyle = new GlyphStyleDescription();
            gstyle.Char = curr_char;
            gstyle.FontStyleDescription = _active_fontstyle;
            if (_glyphdescriptions.Contains(gstyle))
                return (GlyphDescription)_glyphdescriptions[gstyle];

            //meassure char
            SizeF char_size = _graphics.MeasureString(curr_char.ToString(), _active_font, 300, DrawUtilities.Instance.TextAlighnmentLeft );

            //create new GlyphDescription
            GlyphDescription gdesc = new GlyphDescription();
            gdesc.Char = curr_char;
            gdesc.Height = Convert.ToInt32(Math.Round(char_size.Height));
            gdesc.Width = Convert.ToInt32(Math.Round(char_size.Width)) - Convert.ToInt32(Math.Floor((float)gstyle.FontStyleDescription.FontSize / 2f)); // 4 = std offset on right side

            if (curr_char == ' ')
                gdesc.Width = _space_width;

            //save to cache if not there
            _glyphdescriptions.Add(gstyle,gdesc);
            return gdesc;
        }
        #endregion

        #region Attributes
        protected System.Drawing.Graphics _graphics;
        protected string    _text;

        protected int _space_width = 5;

        protected FontStyleDescription _active_fontstyle;
        protected Color     _active_fontcolor;
        protected Font      _active_font;
        protected Brush     _active_brush;

        protected FontStyleDescription _default_fontstyle;
        protected Color     _default_fontcolor;
        
        protected static Hashtable _fonts;
        protected static Hashtable _brushes;
        protected static Hashtable _glyphdescriptions;
        #endregion

    }
}
