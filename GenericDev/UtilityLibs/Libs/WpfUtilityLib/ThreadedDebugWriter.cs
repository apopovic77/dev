﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using Logicx.WpfUtility.VisualHosting;

namespace Logicx.WpfUtility
{
    public class ThreadedDebugWriter : ThreadedFrameworkElement
    {
        public ThreadedDebugWriter(bool start_dispatcher_immediatelly, bool auto_unload, string thread_name) : base(start_dispatcher_immediatelly, auto_unload, thread_name)
        {
        }

        protected override FrameworkElement CreateThreadedChildContent()
        {
            ThreadedTextBlock = new TextBlock();
            DebugWriter = new TextBlockDebugWriter(ThreadedTextBlock);
            DebugWriter.AutoFlush = true;
            DebugWriter.WithDateInfo = true;
            DebugWriter.MaxMessagesToShow = 20;
            DebugWriter.WriteInfo("Creation Wpf Visual Gen Service");
            ThreadedTextBlock.Loaded += new RoutedEventHandler(ThreadedTextBlock_Loaded);

            return ThreadedTextBlock;
        }

        void ThreadedTextBlock_Loaded(object sender, RoutedEventArgs e)
        {
            if (WriterInitialized != null)
                WriterInitialized(this, new EventArgs());
        }

        public event EventHandler WriterInitialized;
        public TextBlock ThreadedTextBlock;
        public TextBlockDebugWriter DebugWriter;
    }
}
