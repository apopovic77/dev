﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;

namespace LinqLib.Linq2Sql.DbConnection
{
   public class IaDbDataReader : DbDataReader
   {
       #region construction and initialization
       public IaDbDataReader(SqlDataReader reader, IaDbCommand command) : base()
       {
           EnterLocalLock();
           _internal_reader = reader;
           _internal_command = command;
           _internal_connection = command.IaDbConnection;
           _hash_code = this.GetHashCode();
           _internal_reader_hash_code = _internal_reader.GetHashCode();
           
       }
       #endregion

       #region Operations
       public override void Close()
       {
           _internal_reader.Close();
           CheckCloseLock("IaDbDataReader.Close");
           ExitLocalLock();
       }

       public override System.Runtime.Remoting.ObjRef CreateObjRef(Type requestedType)
       {
           return _internal_reader.CreateObjRef(requestedType);
       }


       protected override void Dispose(bool disposing)
       {
           EnterLocalLock();
           _disposed = true;
           _internal_reader.Dispose();
           base.Dispose(disposing);
           ExitLocalLock();
       }

       public override bool GetBoolean(int ordinal)
       {
           EnterLocalLock();
           bool bRet = false;
           try
           {
               if (!_disposed && !_internal_reader.IsClosed)
                   bRet = _internal_reader.GetBoolean(ordinal);

           }
           catch(Exception)
           {
               CheckCloseLock("IaDbDataReader.GetBoolean");
               throw;
           }
           finally
           {
               ExitLocalLock();
           }
           return bRet;
       }

       public override byte GetByte(int ordinal)
       {
           EnterLocalLock();
           byte bRet = 0;
           try
           {
               if (!_disposed && !_internal_reader.IsClosed)
                   bRet = _internal_reader.GetByte(ordinal);
           }
           catch (Exception)
           {
               CheckCloseLock("IaDbDataReader.GetByte");
               throw;
           }
           finally
           {
               ExitLocalLock();
           }
           return bRet;
       }

       public override long GetBytes(int ordinal, long dataOffset, byte[] buffer, int bufferOffset, int length)
       {
           EnterLocalLock();
           long lRet = 0;
           try
           {
               if (!_disposed && !_internal_reader.IsClosed)
                   lRet = _internal_reader.GetBytes(ordinal, dataOffset, buffer, bufferOffset, length);
           }
           catch (Exception)
           {
               CheckCloseLock("IaDbDataReader.GetBytes");
               throw;
           }
           finally
           {
               ExitLocalLock();
           }

           return lRet;
       }

       public override char GetChar(int ordinal)
       {
           EnterLocalLock();
           char cRet = (char)0;
           try
           {
               if (!_disposed && !_internal_reader.IsClosed)
                   cRet = (char)_internal_reader.GetByte(ordinal);
           }
           catch (Exception)
           {
               CheckCloseLock("IaDbDataReader.GetChar");
               throw;
           }
           finally
           {
               ExitLocalLock();
           }
           return cRet;
       }

       public override long GetChars(int ordinal, long dataOffset, char[] buffer, int bufferOffset, int length)
       {
           EnterLocalLock();
           long lRet = 0;
           try
           {
               if (!_disposed && !_internal_reader.IsClosed)
                   lRet = _internal_reader.GetChars(ordinal, dataOffset, buffer, bufferOffset, length);
           }
           catch (Exception)
           {
               CheckCloseLock("IaDbDataReader.GetChars");
               throw;
           }
           finally
           {
               ExitLocalLock();
           }
           return lRet;
       }

       public override string GetDataTypeName(int ordinal)
       {
           EnterLocalLock();
           string sRet = "";
           try
           {
               if (!_disposed && !_internal_reader.IsClosed)
                   sRet = _internal_reader.GetDataTypeName(ordinal);
           }
           catch (Exception)
           {
               CheckCloseLock("IaDbDataReader.GetDataTypeName");
               throw;
           }
           finally
           {
               ExitLocalLock();
           }
           return sRet;
       }

       public override DateTime GetDateTime(int ordinal)
       {
           EnterLocalLock();
           DateTime dtRet = DateTime.MinValue;
           try
           {
               if (!_disposed && !_internal_reader.IsClosed)
                   dtRet = _internal_reader.GetDateTime(ordinal);
           }
           catch (Exception)
           {
               CheckCloseLock("IaDbDataReader.GetDateTime");
               throw;
           }
           finally
           {
               ExitLocalLock();
           }
           return dtRet;
       }

       public override decimal GetDecimal(int ordinal)
       {
           EnterLocalLock();
           decimal dRet = 0;
           try
           {
               if (!_disposed && !_internal_reader.IsClosed)
                   dRet = _internal_reader.GetDecimal(ordinal);
           }
           catch (Exception)
           {
               CheckCloseLock("IaDbDataReader.GetDecimal");
               throw;
           }
           finally
           {
               ExitLocalLock();
           }
           return dRet;
       }

       public override double GetDouble(int ordinal)
       {
           EnterLocalLock();
           double dRet = 0;
           try
           {
               if (!_disposed && !_internal_reader.IsClosed)
                   dRet = _internal_reader.GetDouble(ordinal);
           }
           catch (Exception)
           {
               CheckCloseLock("IaDbDataReader.GetDouble");
               throw;
           }
           finally
           {
               ExitLocalLock();
           }
           return dRet;

       }

       public override System.Collections.IEnumerator GetEnumerator()
       {
           EnterLocalLock();
           System.Collections.IEnumerator iRet = null;
           try
           {
               if (!_disposed && !_internal_reader.IsClosed)
                   iRet = _internal_reader.GetEnumerator();
           }
           catch (Exception)
           {
               CheckCloseLock("IaDbDataReader.GetEnumerator");
               throw;
           }
           finally
           {
               ExitLocalLock();
           }

           return iRet;
       }

       public override Type GetFieldType(int ordinal)
       {
           EnterLocalLock();
           Type tRet = null;
           try
           {
               if (!_disposed && !_internal_reader.IsClosed)
                   tRet = _internal_reader.GetFieldType(ordinal);
           }
           catch (Exception)
           {
               CheckCloseLock("IaDbDataReader.GetFieldType");
               throw;
           }
           finally
           {
               ExitLocalLock();
           }
           return tRet;
       }

       public override float GetFloat(int ordinal)
       {
           EnterLocalLock();
           float fRet = 0;
           try
           {
               if (!_disposed && !_internal_reader.IsClosed)
                   fRet = _internal_reader.GetFloat(ordinal);
           }
           catch (Exception)
           {
               CheckCloseLock("IaDbDataReader.GetFloat");
               throw;
           }
           finally
           {
               ExitLocalLock();
           }
           return fRet;
       }

       public override Guid GetGuid(int ordinal)
       {
           EnterLocalLock();
           Guid gRet = Guid.Empty;
           try
           {
               if (!_disposed && !_internal_reader.IsClosed)
                   gRet = _internal_reader.GetGuid(ordinal);
           }
           catch (Exception)
           {
               CheckCloseLock("IaDbDataReader.GetGuid");
               throw;
           }
           finally
           {
               ExitLocalLock();
           }
           return gRet;
       }

       //public override int GetHashCode()
       //{
       //    try
       //    {
       //         return _internal_reader.GetHashCode();
       //    }
       //    catch (Exception)
       //    {
       //        CheckCloseLock("IaDbDataReader.GetHashCode");
       //        throw;
       //    }
       //}

       public override short GetInt16(int ordinal)
       {
           EnterLocalLock();
           short nRet = 0;
           try
           {
               if (!_disposed && !_internal_reader.IsClosed)
                   nRet = _internal_reader.GetInt16(ordinal);
           }
           catch (Exception)
           {
               CheckCloseLock("IaDbDataReader.GetInt16");
               throw;
           }
           finally
           {
               ExitLocalLock();
           }
           return nRet;
       }

       public override int GetInt32(int ordinal)
       {
           EnterLocalLock();
           int nRet = 0;
           try
           {
               if (!_disposed && !_internal_reader.IsClosed)
                   nRet = _internal_reader.GetInt32(ordinal);
           }
           catch (Exception)
           {
               CheckCloseLock("IaDbDataReader.GetInt32");
               throw;
           }
           finally
           {
               ExitLocalLock();
           }
           return nRet;
       }

       public override long GetInt64(int ordinal)
       {
           EnterLocalLock();
           long lRet = 0;
           try
           {
               if (!_disposed && !_internal_reader.IsClosed)
                   lRet = _internal_reader.GetInt64(ordinal);
           }
           catch (Exception)
           {
               CheckCloseLock("IaDbDataReader.GetInt64");
               throw;
           }
           finally
           {
               ExitLocalLock();
           }
           return lRet;
       }

       public override string GetName(int ordinal)
       {
           EnterLocalLock();
           string sRet = "";
           try
           {
               if (!_disposed && !_internal_reader.IsClosed)
                   sRet = _internal_reader.GetName(ordinal);

                return _internal_reader.GetName(ordinal);
           }
           catch (Exception)
           {
               CheckCloseLock("IaDbDataReader.GetName");
               throw;
           }
           finally
           {
               ExitLocalLock();
           }

           return sRet;
       }

       public override int GetOrdinal(string name)
       {
           EnterLocalLock();
           int nRet = -1;
           try
           {
               if (!_disposed && !_internal_reader.IsClosed)
                   nRet = _internal_reader.GetOrdinal(name);
           }
           catch (Exception)
           {
               CheckCloseLock("IaDbDataReader.GetOrdinal");
               throw;
           }
           finally
           {
               ExitLocalLock();
           }
           return nRet;
       }

       public override Type GetProviderSpecificFieldType(int ordinal)
       {
           EnterLocalLock();
           Type tRet = null;
           try
           {
               if (!_disposed && !_internal_reader.IsClosed)
                   tRet = _internal_reader.GetProviderSpecificFieldType(ordinal);
           }
           catch (Exception)
           {
               CheckCloseLock("IaDbDataReader.GetProviderSpecificFieldType");
               throw;
           }
           finally
           {
               ExitLocalLock();
           }
           return tRet;
       }

       public override object GetProviderSpecificValue(int ordinal)
       {
           EnterLocalLock();
           object oRet = null;
           try
           {
               if (!_disposed && !_internal_reader.IsClosed)
                   oRet = _internal_reader.GetProviderSpecificValue(ordinal);
           }
           catch (Exception)
           {
               CheckCloseLock("IaDbDataReader.GetProviderSpecificValue");
               throw;
           }
           finally
           {
               ExitLocalLock();
           }
           return oRet;
       }

       public override int GetProviderSpecificValues(object[] values)
       {
           EnterLocalLock();
           int nRet = 0;
           try
           {
               if (!_disposed && !_internal_reader.IsClosed)
                   nRet = _internal_reader.GetProviderSpecificValues(values);
           }
           catch (Exception)
           {
               CheckCloseLock("IaDbDataReader.GetProviderSpecificValues");
               throw;
           }
           finally
           {
               ExitLocalLock();
           }
           return nRet;
       }

       public override System.Data.DataTable GetSchemaTable()
       {
           EnterLocalLock();
           System.Data.DataTable dtRet = null; 
           try
           {
               if (!_disposed && !_internal_reader.IsClosed)
                   dtRet = _internal_reader.GetSchemaTable();
           }
           catch (Exception)
           {
               CheckCloseLock("IaDbDataReader.GetSchemaTable");
               throw;
           }
           finally
           {
               ExitLocalLock();
           }
           return dtRet;
       }

       public override string GetString(int ordinal)
       {
           EnterLocalLock();
           string sRet = string.Empty;
           try
           {
               if (!_disposed && !_internal_reader.IsClosed)
                   sRet = _internal_reader.GetString(ordinal);
           }
           catch (Exception)
           {
               CheckCloseLock("IaDbDataReader.GetString");
               throw;
           }
           finally
           {
               ExitLocalLock();
           }
           return sRet;
       }

       public override object GetValue(int ordinal)
       {
           EnterLocalLock();
           object oRet = null;

           try
           {
               if (!_disposed && !_internal_reader.IsClosed)
                   oRet = _internal_reader.GetValue(ordinal);
           }
           catch (Exception)
           {
               CheckCloseLock("IaDbDataReader.GetValue");
               throw;
           }
           finally
           {
               ExitLocalLock();
           }
           return oRet;
       }

       public override int GetValues(object[] values)
       {
           EnterLocalLock();
           int nRet = 0;
           try
           {
               if (!_disposed && !_internal_reader.IsClosed)
                   nRet = _internal_reader.GetValues(values);

           }
           catch (Exception)
           {
               CheckCloseLock("IaDbDataReader.GetValues");
               throw;
           }
           finally
           {
               ExitLocalLock();
           }
           return nRet;
       }

       public override object InitializeLifetimeService()
       {
           EnterLocalLock();
           object oRet = null;

           try
           {
               if (!_disposed && !_internal_reader.IsClosed)
                   oRet = _internal_reader.InitializeLifetimeService();
                return _internal_reader.InitializeLifetimeService();
           }
           catch (Exception)
           {
               CheckCloseLock("IaDbDataReader.InitializeLifetimeService");
               throw;
           }
           finally
           {
               ExitLocalLock();
           }
           return oRet;
       }

       public override bool IsDBNull(int ordinal)
       {
           EnterLocalLock();
           bool bRet = false;
           try
           {
               if (!_disposed && !_internal_reader.IsClosed)
                   bRet = _internal_reader.IsDBNull(ordinal);
           }
           catch (Exception)
           {
               CheckCloseLock("IaDbDataReader.IsDBNull");
               throw;
           }
           finally
           {
               ExitLocalLock();
           }

           return bRet;
       }

       public override bool NextResult()
       {
           EnterLocalLock();
           bool bRet = false;
           if (!_disposed && !_internal_reader.IsClosed)
               bRet = _internal_reader.NextResult();
           CheckCloseLock("IaDbDataReader.NextResult");
           ExitLocalLock();
           return bRet;
       }

       public override bool Read()
       {
           EnterLocalLock();

           bool bRet = false;
           
           if(!_disposed && !_internal_reader.IsClosed)
               bRet =_internal_reader.Read();

           CheckCloseLock("IaDbDataReader.Read");
           ExitLocalLock();
           return bRet;
       }

       public override string ToString()
       {
           return _internal_reader.ToString();
       }

       private void CheckCloseLock(string debug_info)
       {
           if(IsClosed)
           {
               try
               {
                   _internal_connection.FreeContext(DebugIdentifier + " " + debug_info + "[IsClosed]");
               }
               catch
               {
                   // could throw a sync exception if this is called
                   // due to an eobject disposal
               }

               if(_internal_command.PooledIaDbConnection != null)
               {
                   _internal_command.SwitchFromPooledConnection();
               }
           }
       }

       private void EnterLocalLock()
       {
           Monitor.Enter(LockParalellsystem);
       }

       private void ExitLocalLock()
       {
           Monitor.Exit(LockParalellsystem);
       }
       #endregion

       #region Properties
       public virtual string DebugIdentifier
       {
           get
           {
               return "###R" + _hash_code.ToString();
           }
       }

       public SqlDataReader SqlDataReader
       {
           get { return _internal_reader; }
       }

       public IaDbCommand IaDbCommand
       {
           get { return _internal_command; }
       }

       public IaDbConnection IaDbConnection
       {
           get { return _internal_connection; }
       }

       public override int Depth
       {
           get
           {
               EnterLocalLock();
               int nRet = 0;
               if (!_disposed && !_internal_reader.IsClosed)
                    nRet =  _internal_reader.Depth;
               ExitLocalLock();
               return nRet;
           }
       }

       public override int FieldCount
       {
           get
           {
               EnterLocalLock();
               int nRet = 0;
               if(!_disposed && !_internal_reader.IsClosed)
                    nRet = _internal_reader.FieldCount;
               ExitLocalLock();
               return nRet;
           }
       }

       public override bool HasRows
       {
           get
           {
               EnterLocalLock();
               bool bRet = false;
               if (!_disposed && !_internal_reader.IsClosed)
                    bRet = _internal_reader.HasRows;
               ExitLocalLock();
               return bRet;
           }
       }

       public override bool IsClosed
       {
           get
           {
               EnterLocalLock();
               bool bRet = true;
               if (!_disposed && !_internal_reader.IsClosed)
                    bRet = _internal_reader.IsClosed;
               ExitLocalLock();
               return bRet;
           }
       }

       public override int RecordsAffected
       {
           get
           {
               EnterLocalLock();
               int nRet = 0;
               if (!_disposed && !_internal_reader.IsClosed)
                    nRet = _internal_reader.RecordsAffected;
               ExitLocalLock();
               return nRet;
           }
       }

       public override object this[int ordinal]
       {
           get
           {
               EnterLocalLock();
               object oRet = null;
               if (!_disposed && !_internal_reader.IsClosed)
                    oRet = _internal_reader[ordinal];
               ExitLocalLock();
               return oRet;
           }
       }

       public override object this[string name]
       {
           get
           {
               EnterLocalLock();
               object oRet = null;
               if (!_disposed && !_internal_reader.IsClosed)
                    oRet = _internal_reader[name];
               ExitLocalLock();
               return oRet;
           }
       }

       public override int VisibleFieldCount
       {
           get
           {
               EnterLocalLock();
               int nRet = 0;
               if (!_disposed && !_internal_reader.IsClosed)
                    nRet = _internal_reader.VisibleFieldCount;
               ExitLocalLock();
               return nRet;
           }
       }

#if DEBUG
       public bool WithDebugOutput
       {
           get { return _internal_connection.WithDebugOutput; }
           set { _internal_connection.WithDebugOutput = value; }
       }
#endif
       #endregion

       #region Attribs
       private SqlDataReader _internal_reader = null;
       private IaDbCommand _internal_command = null;
       private IaDbConnection _internal_connection = null;
       private int _hash_code = 0;
       private int _internal_reader_hash_code = 0;
       private bool _disposed = false;

       private object LockParalellsystem = new object();
       #endregion
   }
}
