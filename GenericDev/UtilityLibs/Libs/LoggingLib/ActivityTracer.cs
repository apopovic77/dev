﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using LoggingLib.Trace;

namespace LoggingLib
{
    /// <summary>
    /// Class implementing an activity tracer for logging.
    /// Uses the System.Diagnostic.Trace.CorrelationManager to generate 
    /// thread dependend activity Ids
    /// </summary>
    /// <example>
    /// The following example shows how to use this class
    /// <code>
    /// using(ActivityTracer outerTracer = new ActivityTracer())
    /// {
    ///     // all log entries within the outerTracer scope
    ///     // will get the same activity id
    ///     
    ///     using(ActivityTracer innerTracer = new ActivityTracer())
    ///     {
    ///         // all log entries within the innerTracer scope
    ///         // will get the activity id of the innerTracer instance
    /// 
    ///         // But nesting tracers is supportet:
    ///         // That means - all log entries within the innerTracer
    ///         // scope will get the activity id of the outerTracer scope
    ///         // set in the RelatedActivityId property
    /// 
    ///         // innerTracer.RelatedActivityId == outerTracer.ActivityId
    ///     }
    /// }
    /// </code>
    /// </example>
    public class ActivityTracer : IDisposable
    {
        #region Construction and initialization
        /// <summary>
        /// Constructor of the class (starting an activity)
        /// </summary>
        public ActivityTracer()
        {
            // start a logical operation
            _activity_info = Trace.LoggingCorrelationManager.StartLogicalOperation();
        }
        /// <summary>
        /// Constructor of the class (starting an activity)
        /// </summary>
        /// <param name="description">An activity description text</param>
        public ActivityTracer(string description)
        {
            // start a logical operation
            _activity_info = Trace.LoggingCorrelationManager.StartLogicalOperation(description);
        }

        /// <summary>
        /// Constructor of the class (starting an activity)
        /// </summary>
        /// <param name="correlation_scope">correlation scope for this activity</param>
        public ActivityTracer(object correlation_scope)
        {
            // start a logical operation
            _activity_info = Trace.LoggingCorrelationManager.StartLogicalOperation(correlation_scope, string.Empty);
        }

        /// <summary>
        /// Constructor of the class (starting an activity)
        /// </summary>
        /// <param name="correlation_scope">correlation scope of this activity</param>
        /// <param name="description">An activity description text</param>
        public ActivityTracer(object correlation_scope, string description)
        {
            // start a logical operation
            _activity_info = Trace.LoggingCorrelationManager.StartLogicalOperation(correlation_scope, description);
        }
        #endregion

        #region Dispose implementation
        /// <summary>
        /// Desctructor of the class (ending an activity)
        /// </summary>
        public void Dispose()
        {
            if (_activity_info == null)
                return;

            // stops the logical operation
            Trace.LoggingCorrelationManager.StopLogicalOperation(_activity_info.ScopeIdentifier);
        }
        #endregion



        #region Operations


        #endregion

        #region Properties
        /// <summary>
        /// Gets the activity info class of the current instance
        /// </summary>
        public ActivityInfo ActivityInfo
        {
            get { return _activity_info; }
        }
        /// <summary>
        /// Gets the related activity info class of the current instance
        /// </summary>
        /// <remarks>Returns null if this activity trace has no related activity</remarks>
        public ActivityInfo RelatedActivityInfo
        {
            get
            {
                Stack<ActivityInfo> activity_stack = Trace.LoggingCorrelationManager.GetActivityStackForScope(_activity_info.ScopeIdentifier);
                if(activity_stack != null)
                {
                    ActivityInfo[] activities = activity_stack.ToArray();
                    if (activities.Length > 1)
                        return activities[1];
                }

                return null;
            }
        }
        #endregion

        #region Attribs
        private ActivityInfo _activity_info = null;
        #endregion
    }
}
