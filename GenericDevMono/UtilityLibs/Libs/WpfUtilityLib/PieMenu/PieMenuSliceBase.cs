﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Logicx.WpfUtility;
using Logicx.WpfUtility.CustomControls.OverlayManager.StatusInfo;
using MathLib;
using Windows7.Multitouch;

namespace Logicx.WpfUtility.PieMenu
{
    #region Converters
    public class MyCloneConverter : IValueConverter
    {
        public static MyCloneConverter Instance = new MyCloneConverter();
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is Freezable)
            {
                value = (value as Freezable).Clone();
            }
            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
    #endregion

    /// <summary>
    /// Interaction logic for PieMenu.xaml
    /// </summary>
    public partial class PieMenuSliceBase<PE, BUT, TOG> : UserControl
        where PE : PieMenuEntryBase<PE>
        where BUT : PE, IButtonPieMenuEntry
        where TOG : PE, ITogglePieMenuEntry
    {
        private static bool? _is_multitouch_enabled = null;

        internal static bool IsMultiTouchEnabled
        {
            get
            {
                if (!_is_multitouch_enabled.HasValue)
                    _is_multitouch_enabled = TouchHandler.DigitizerCapabilities.IsMultiTouchReady;

                return _is_multitouch_enabled.Value;
            }
        }

        public PieMenuSliceBase()
        {
            InitializeComponent();
            SetMenuDimensions(_outer_radius, _inner_radius);
        }

        public PieMenuSliceBase(bool close_on_outer_ellipse_click)
        {
            InitializeComponent();
            SetMenuDimensions(_outer_radius, _inner_radius);
            _close_on_outer_ellipse_clicked = close_on_outer_ellipse_click;
        }

        //public void SetMenuEntries<PE>(List<PE> entries) where PE : PieMenuEntryBase<PE>
        //{
        //    _path_text_store = new Dictionary<Path, Path>();
        //    _menu_entries = entries;
        //    menuitem_canvas.Children.Clear();
        //    if (_menu_entries == null) return;
        //    foreach (PE entry in entries)
        //        AddMenuItem(entry);
        //}

        //public List<PE> GetMenuEntries<PE>() where PE : PieMenuEntryBase<PE>
        //{
        //    if (_menu_entries != null && _menu_entries is List<PE>)
        //        return _menu_entries as List<PE>;

        //    return null;
        //}

        private void InitializeComponent()
        {
            String xaml_string = @"
            <UserControl
    xmlns=""http://schemas.microsoft.com/winfx/2006/xaml/presentation""
    xmlns:x=""http://schemas.microsoft.com/winfx/2006/xaml""
    xmlns:PieMenu=""clr-namespace:Logicx.WpfUtility.PieMenu;assembly=WpfUtilityLib"">

            <Canvas Name=""base_canvas"" >    
            <Canvas.Resources>
            <Style x:Key=""innerborder_style_inactive"" TargetType=""{x:Type Border}"">
                <Setter Property=""Border.Background"" Value=""#11FFFFFF""></Setter>
            </Style>

            <Style x:Key=""innerborder_style_active"" TargetType=""{x:Type Border}"">
                <Setter Property=""Border.Background"" Value=""#55FFFFFF""></Setter>
            </Style>


            <Storyboard x:Key=""reset_anim_story"" >
                <DoubleAnimation AccelerationRatio=""1"" Storyboard.TargetName=""scaletrans"" Storyboard.TargetProperty=""ScaleX"" To=""1"" Duration=""00:00:00.0000000"" FillBehavior=""HoldEnd""/>
                <DoubleAnimation AccelerationRatio=""1"" Storyboard.TargetName=""scaletrans"" Storyboard.TargetProperty=""ScaleY"" To=""1"" Duration=""00:00:00.0000000"" FillBehavior=""HoldEnd""/>
                <DoubleAnimation DecelerationRatio=""1"" Storyboard.TargetName=""base_canvas"" Storyboard.TargetProperty=""Opacity"" To=""1"" Duration=""00:00:00.0000000"" FillBehavior=""HoldEnd""/>
            </Storyboard>
            
            <Storyboard x:Key=""hide_anim_story"">
                <DoubleAnimation AccelerationRatio=""1"" Storyboard.TargetName=""scaletrans"" Storyboard.TargetProperty=""ScaleX"" From=""1"" To=""0"" Duration=""00:00:00.0000000"" FillBehavior=""HoldEnd""/>
                <DoubleAnimation AccelerationRatio=""1"" Storyboard.TargetName=""scaletrans"" Storyboard.TargetProperty=""ScaleY"" From=""1"" To=""0"" Duration=""00:00:00.0000000"" FillBehavior=""HoldEnd""/>
                <DoubleAnimation DecelerationRatio=""1"" Storyboard.TargetName=""base_canvas"" Storyboard.TargetProperty=""Opacity"" From=""1"" To=""0"" Duration=""00:00:00.0000000"" FillBehavior=""HoldEnd""/>
            </Storyboard>

            <Storyboard x:Key=""fw_anim_story"">
                <DoubleAnimation AccelerationRatio=""1"" Storyboard.TargetName=""scaletrans"" Storyboard.TargetProperty=""ScaleX"" From=""0"" To=""1"" Duration=""00:00:00.0000000"" FillBehavior=""HoldEnd""/>
                <DoubleAnimation AccelerationRatio=""1"" Storyboard.TargetName=""scaletrans"" Storyboard.TargetProperty=""ScaleY"" From=""0"" To=""1"" Duration=""00:00:00.0000000"" FillBehavior=""HoldEnd""/>
                <DoubleAnimation AccelerationRatio=""1"" Storyboard.TargetName=""base_canvas"" Storyboard.TargetProperty=""Opacity"" From=""0"" To=""1"" Duration=""00:00:00.0000000"" FillBehavior=""HoldEnd""/>
            </Storyboard>
            
            <Storyboard x:Key=""bw_anim_story"">
                <DoubleAnimation AccelerationRatio=""1"" Storyboard.TargetName=""scaletrans"" Storyboard.TargetProperty=""ScaleX"" From=""2"" To=""1"" Duration=""00:00:00.0000000"" FillBehavior=""HoldEnd""/>
                <DoubleAnimation AccelerationRatio=""1"" Storyboard.TargetName=""scaletrans"" Storyboard.TargetProperty=""ScaleY"" From=""2"" To=""1"" Duration=""00:00:00.0000000"" FillBehavior=""HoldEnd""/>
                <DoubleAnimation AccelerationRatio=""1"" Storyboard.TargetName=""base_canvas"" Storyboard.TargetProperty=""Opacity"" From=""0"" To=""1"" Duration=""00:00:00.0000000"" FillBehavior=""HoldEnd""/>
            </Storyboard>

            <Storyboard x:Key=""selected_anim_story"">
                <DoubleAnimation AccelerationRatio=""1"" Storyboard.TargetName=""scaletrans"" Storyboard.TargetProperty=""ScaleX"" From=""1"" To=""2"" Duration=""00:00:00.0000000"" FillBehavior=""HoldEnd""/>
                <DoubleAnimation AccelerationRatio=""1"" Storyboard.TargetName=""scaletrans"" Storyboard.TargetProperty=""ScaleY"" From=""1"" To=""2"" Duration=""00:00:00.0000000"" FillBehavior=""HoldEnd""/>
                <DoubleAnimation AccelerationRatio=""1"" Storyboard.TargetName=""base_canvas"" Storyboard.TargetProperty=""Opacity"" From=""1"" To=""0"" Duration=""00:00:00.0000000"" FillBehavior=""HoldEnd""/>
            </Storyboard>
        </Canvas.Resources>
        <Canvas.RenderTransform>
            <ScaleTransform x:Name=""scaletrans"" />
        </Canvas.RenderTransform>
        <Border Visibility=""Collapsed"" Background=""#22FFFFFF"" Name=""outer_ellipse_inactivecolor""></Border>        
        <Border Visibility=""Collapsed"" Name=""inner_ellipse_inactivecolor"" Style=""{StaticResource innerborder_style_inactive}"" />
        <Border Visibility=""Collapsed"" Name=""inner_ellipse_activecolor"" Style=""{StaticResource innerborder_style_active}""/>

        <Ellipse Name=""outer_ellipse"" Fill=""{Binding ElementName=outer_ellipse_inactivecolor, Path=Background, Converter={x:Static PieMenu:MyCloneConverter.Instance}}""></Ellipse>
        <Ellipse Name=""inner_ellipse"" Fill=""{Binding ElementName=inner_ellipse_inactivecolor, Path=Background, Converter={x:Static PieMenu:MyCloneConverter.Instance}}"" Stroke=""White"">
            

            <Ellipse.Triggers>
                <EventTrigger RoutedEvent=""UIElement.MouseEnter"">
                    <BeginStoryboard>
                        <Storyboard>                          
                            <ColorAnimation DecelerationRatio=""1"" Storyboard.TargetProperty=""Fill.Color"" From=""{Binding ElementName=inner_ellipse_inactivecolor, Path=Background.Color, Converter={x:Static PieMenu:MyCloneConverter.Instance}}"" To=""{Binding ElementName=inner_ellipse_activecolor, Path=Background.Color, Converter={x:Static PieMenu:MyCloneConverter.Instance}}"" Duration=""00:00:00.25"" FillBehavior=""HoldEnd"" ></ColorAnimation>
                        </Storyboard>
                    </BeginStoryboard>
                </EventTrigger>
                <EventTrigger RoutedEvent=""UIElement.MouseLeave"">
                    <BeginStoryboard>
                        <Storyboard>
                            <ColorAnimation DecelerationRatio=""1"" Storyboard.TargetProperty=""Fill.Color"" To=""{Binding ElementName=inner_ellipse_inactivecolor, Path=Background.Color, Converter={x:Static PieMenu:MyCloneConverter.Instance}}"" From=""{Binding ElementName=inner_ellipse_activecolor, Path=Background.Color, Converter={x:Static PieMenu:MyCloneConverter.Instance}}"" Duration=""00:00:00.8"" FillBehavior=""HoldEnd"" ></ColorAnimation>
                        </Storyboard>
                    </BeginStoryboard>
                </EventTrigger>
            </Ellipse.Triggers>

            <!--
            <Ellipse.Triggers>
                <EventTrigger RoutedEvent=""UIElement.MouseDown"">
                    <BeginStoryboard>
                        <Storyboard>
                            <DoubleAnimation AccelerationRatio=""1"" Storyboard.TargetName=""scaletrans"" Storyboard.TargetProperty=""ScaleX"" From=""1"" To=""0"" Duration=""00:00:00.2000000"" FillBehavior=""HoldEnd""/>
                            <DoubleAnimation AccelerationRatio=""1"" Storyboard.TargetName=""scaletrans"" Storyboard.TargetProperty=""ScaleY"" From=""1"" To=""0"" Duration=""00:00:00.2000000"" FillBehavior=""HoldEnd""/>
                            <DoubleAnimation DecelerationRatio=""1"" Storyboard.TargetName=""base_canvas"" Storyboard.TargetProperty=""Opacity"" From=""1"" To=""0"" Duration=""00:00:00.2000000"" FillBehavior=""HoldEnd""/>
                        </Storyboard>
                    </BeginStoryboard>
                </EventTrigger>

                <EventTrigger RoutedEvent=""FrameworkElement.Loaded"">
                    <BeginStoryboard>
                        <Storyboard>
                            <DoubleAnimation AccelerationRatio=""1"" Storyboard.TargetName=""scaletrans"" Storyboard.TargetProperty=""ScaleX"" From=""2"" To=""1"" Duration=""00:00:00.2000000"" FillBehavior=""HoldEnd""/>
                            <DoubleAnimation AccelerationRatio=""1"" Storyboard.TargetName=""scaletrans"" Storyboard.TargetProperty=""ScaleY"" From=""2"" To=""1"" Duration=""00:00:00.2000000"" FillBehavior=""HoldEnd""/>
                            <DoubleAnimation AccelerationRatio=""1"" Storyboard.TargetName=""base_canvas"" Storyboard.TargetProperty=""Opacity"" From=""0"" To=""1"" Duration=""00:00:00.2000000"" FillBehavior=""HoldEnd""/>
                        </Storyboard>
                    </BeginStoryboard>
                </EventTrigger>
            </Ellipse.Triggers>-->
        </Ellipse>
        <Canvas Name=""menuitem_canvas"">
            
        </Canvas>
    </Canvas>
    </UserControl>";

            try
            {
                object o = XamlReader.Parse(xaml_string);
                if(o is UserControl)
                {
                    //Canvas c = ((UserControl) o).FindName("base_canvas") as Canvas;
                    Content = o as UserControl;
                }
            }
            catch (Exception ex)
            {

            }

            menuitem_canvas = ((UserControl)Content).FindName("menuitem_canvas") as Canvas;
            scaletrans = ((UserControl)Content).FindName("scaletrans") as ScaleTransform;
            base_canvas = ((UserControl)Content).FindName("base_canvas") as Canvas;
            outer_ellipse = ((UserControl)Content).FindName("outer_ellipse") as Ellipse;
            inner_ellipse = ((UserControl)Content).FindName("inner_ellipse") as Ellipse;

            outer_ellipse.MouseDown += new MouseButtonEventHandler(outer_ellipse_MouseDown);
            outer_ellipse.MouseUp += new MouseButtonEventHandler(outer_ellipse_MouseUp);
            outer_ellipse.StylusDown += new StylusDownEventHandler(outer_ellipse_StylusDown);
            outer_ellipse.StylusUp += new StylusEventHandler(outer_ellipse_StylusUp);

            inner_ellipse.MouseDown += new MouseButtonEventHandler(inner_ellipse_MouseDown);
            inner_ellipse.MouseUp += new MouseButtonEventHandler(inner_ellipse_MouseUp);
            inner_ellipse.MouseMove += new MouseEventHandler(inner_ellipse_MouseMove);    
            inner_ellipse.StylusDown += new StylusDownEventHandler(inner_ellipse_StylusDown);
            inner_ellipse.StylusUp += new StylusEventHandler(inner_ellipse_StylusUp);
            inner_ellipse.StylusMove += new StylusEventHandler(inner_ellipse_StylusMove);
           
        }

        public Ellipse InnerEllipse
        {
            get { return inner_ellipse; }
        }
        public Ellipse OuterEllipse
        {
            get { return outer_ellipse; }
        }

        private Canvas menuitem_canvas;
        private ScaleTransform scaletrans;
        private Canvas base_canvas;
        private Ellipse outer_ellipse;
        private Ellipse inner_ellipse;

        public List<PE> MenuEntries
        {
            set
            {
                _path_text_store = new Dictionary<Path, Path>();
                _menu_entries = value;
                menuitem_canvas.Children.Clear();
                if (_menu_entries == null) return;
                foreach (PE entry in _menu_entries)
                    AddMenuItem(entry);
            }
            get { return _menu_entries; }
        }

        public void SetMenuDimensions(double outer_radius, double inner_radius)
        {
            _outer_radius = outer_radius;
            _inner_radius = inner_radius;

            Width = _outer_radius * 2;
            Height = _outer_radius * 2;

            scaletrans.CenterX = _outer_radius;
            scaletrans.CenterY = _outer_radius;

            base_canvas.Width = _outer_radius * 2;
            base_canvas.Height = _outer_radius * 2;

            outer_ellipse.Width = _outer_radius * 2;
            outer_ellipse.Height = _outer_radius * 2;

            inner_ellipse.Width = _inner_radius * 2;
            inner_ellipse.Height = _inner_radius * 2;
            Canvas.SetLeft(inner_ellipse, _outer_radius - _inner_radius);
            Canvas.SetTop(inner_ellipse, _outer_radius - _inner_radius);
        }

        protected void AddMenuItem<PE>(PE menu_entry) where PE : PieMenuEntryBase<PE>
        {
            double start_angle_rad = menu_entry.AngleStart;
            double pie_angle_rad = menu_entry.AnglePie;

            Path p = CreatePieShape(_outer_radius, _inner_radius, start_angle_rad, pie_angle_rad);
            p.Tag = menu_entry;
            p.MouseDown += new MouseButtonEventHandler(pie_item_MouseDown);
            p.MouseUp += new MouseButtonEventHandler(pie_item_MouseUp);

            if (IsMultiTouchEnabled)
            {
                p.StylusDown += new StylusDownEventHandler(p_StylusDown);
                p.StylusUp += new StylusEventHandler(p_StylusUp);
            }
            menuitem_canvas.Children.Add(p);

            // Create the initial formatted text string.
            FormattedText formattedText = new FormattedText(
                menu_entry.Name,
                CultureInfo.GetCultureInfo("en-us"),
                FlowDirection.LeftToRight,
                new Typeface("Calibri"),
                _font_size,
                _font_brush);

            Geometry g = formattedText.BuildGeometry(new Point());
            TransformGroup tg = new TransformGroup();

            double rot_text_angle_deg = 180 / Math.PI * (start_angle_rad - (pie_angle_rad / 2));
            if (rot_text_angle_deg > 0)
                rot_text_angle_deg = (rot_text_angle_deg % 360) - 360;

            if (rot_text_angle_deg <= 90 && rot_text_angle_deg >= -90 || rot_text_angle_deg <= -270 && rot_text_angle_deg >= -450)
            {
                rot_text_angle_deg *= -1;
                //create textblock
                tg.Children.Add(new TranslateTransform(_outer_radius + _text_pie_padding, -g.Bounds.Height));
                tg.Children.Add(new RotateTransform(rot_text_angle_deg));
            }
            else
            {
                rot_text_angle_deg *= -1;
                //create textblock
                tg.Children.Add(new TranslateTransform(-_outer_radius - _text_pie_padding - g.Bounds.Width, -g.Bounds.Height));
                tg.Children.Add(new RotateTransform(180 + rot_text_angle_deg));
            }

            Path p_text = new Path();
            p_text.Data = g;
            p_text.Fill = _font_brush;
            p_text.Stroke = _font_stroke_brush;
            p_text.StrokeThickness = _font_stroke_thickness;
            p_text.RenderTransform = tg;
            p_text.IsHitTestVisible = false;
            p_text.Focusable = false;
            p_text.BitmapEffect = _font_bitmap_effect;

            Canvas c = new Canvas();
            c.IsHitTestVisible = true;
            c.Width = formattedText.Width;
            c.Height = formattedText.Height;
            c.Background = Brushes.Transparent;
            c.RenderTransform = tg;
            c.Tag = p;
            c.MouseDown += new MouseButtonEventHandler(pie_item_MouseDown);
            c.MouseUp += new MouseButtonEventHandler(pie_item_MouseUp);
            if (IsMultiTouchEnabled)
            {
                c.StylusDown += new StylusDownEventHandler(p_StylusDown);
                c.StylusUp += new StylusEventHandler(p_StylusUp);
            }

            _path_text_store.Add(p, p_text);

            menu_entry.Shape = p;
            menu_entry.ShapeText = p_text;
            menu_entry.ShapeTextBoundingBox = c;
            menu_entry.UpdatePieStyle();

            menuitem_canvas.Children.Add(p_text);
            Canvas.SetLeft(p_text, _outer_radius);
            Canvas.SetTop(p_text, _outer_radius);

            menuitem_canvas.Children.Add(c);
            Canvas.SetLeft(c, _outer_radius);
            Canvas.SetTop(c, _outer_radius);
        }




        public Path CreatePieShape(double outer_radius, double inner_radius, double start_angle_rad, double pie_angle_rad)
        {
            Vector2d center = new Vector2d(outer_radius, outer_radius);

            double start_angle_deg = 180 / Math.PI * start_angle_rad;
            double pie_angle_deg = 180 / Math.PI * pie_angle_rad;

            Vector2d start_point = new Vector2d(1, 0);
            start_point.Rotate(start_angle_rad);
            start_point = center + start_point * outer_radius;

            Vector2d arc1_point = new Vector2d(1, 0);
            arc1_point.Rotate(start_angle_rad - pie_angle_rad);
            arc1_point = center + arc1_point * outer_radius;

            Vector2d line_seg_point1 = new Vector2d(1, 0);
            line_seg_point1.Rotate(start_angle_rad - pie_angle_rad);
            line_seg_point1 = center + line_seg_point1 * inner_radius;

            Vector2d arc2_point = new Vector2d(1, 0);
            arc2_point.Rotate(start_angle_rad);
            arc2_point = center + arc2_point * inner_radius;

            PathGeometry pathGeometry = new PathGeometry();
            PathFigure pathFigure = new PathFigure();
            pathFigure.StartPoint = new Point(start_point.X, start_point.Y);
            pathFigure.IsClosed = true;

            ArcSegment arcSegment1 = new ArcSegment();
            arcSegment1.IsLargeArc = pie_angle_rad >= Math.PI;
            arcSegment1.Point = new Point(arc1_point.X, arc1_point.Y);
            arcSegment1.Size = new Size(outer_radius, outer_radius);
            arcSegment1.SweepDirection = SweepDirection.Clockwise;

            LineSegment lineSegment = new LineSegment(new Point(line_seg_point1.X, line_seg_point1.Y), true);

            ArcSegment arcSegment2 = new ArcSegment();
            arcSegment2.IsLargeArc = pie_angle_rad >= Math.PI;
            arcSegment2.Point = new Point(arc2_point.X, arc2_point.Y);
            arcSegment2.Size = new Size(inner_radius, inner_radius);
            arcSegment2.SweepDirection = SweepDirection.Counterclockwise;

            LineSegment lineSegment2 = new LineSegment(new Point(start_point.X, start_point.Y), true);

            pathFigure.Segments.Add(arcSegment1);
            pathFigure.Segments.Add(lineSegment);
            pathFigure.Segments.Add(arcSegment2);
            pathFigure.Segments.Add(lineSegment2);

            pathGeometry.Figures.Add(pathFigure);

            Path path = new Path();
            path.Data = pathGeometry;
            return path;
        }

        #region Menu Animations
        public void ResetAnimations()
        {
            Storyboard reset_anim_story = (Storyboard)base_canvas.FindResource("reset_anim_story");
            base_canvas.BeginStoryboard(reset_anim_story);
        }
        protected void StartHideAnimation()
        {
            Storyboard hide_anim_story = (Storyboard)base_canvas.FindResource("hide_anim_story");
            hide_anim_story.Completed += new EventHandler(hide_anim_story_Completed);
            base_canvas.BeginStoryboard(hide_anim_story);
        }
        private void hide_anim_story_Completed(object sender, EventArgs e)
        {
            if (BackClick != null)
                BackClick(this, new EventArgs());
        }


        private void StartSelectedAnimation(Path path_sender)
        {
            _cached_shape_for_selected_anim_story = path_sender;
            Storyboard selected_anim_story = (Storyboard)base_canvas.FindResource("selected_anim_story");
            selected_anim_story.Completed +=new EventHandler(selected_anim_story_Completed);
            base_canvas.BeginStoryboard(selected_anim_story);
        }
        private Path _cached_shape_for_selected_anim_story;
        private void selected_anim_story_Completed(object sender, EventArgs e)
        {
            Path p_shape = _cached_shape_for_selected_anim_story;
            PE pieMenuEntry = (PE)p_shape.Tag;

            if (EnterSubMenuClick != null)
                EnterSubMenuClick(this, new EnterSubMenuClickEventArgs<PE>(pieMenuEntry));
        }

        public void StartForwardAnimation()
        {
            Storyboard fw_anim_story = (Storyboard)base_canvas.FindResource("fw_anim_story");
            base_canvas.BeginStoryboard(fw_anim_story);
        }
        public void StartBackwardAnimation()
        {
            Storyboard bw_anim_story = (Storyboard)base_canvas.FindResource("bw_anim_story");
            base_canvas.BeginStoryboard(bw_anim_story);
        }
        #endregion

        #region EventHandlers
        private void outer_ellipse_MouseDown(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
        }

        private void outer_ellipse_MouseUp(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
            if (_close_on_outer_ellipse_clicked)
                StartHideAnimation();
        }

        void outer_ellipse_StylusUp(object sender, StylusEventArgs e)
        {
            e.Handled = true;
            if (_close_on_outer_ellipse_clicked)
                StartHideAnimation();
        }

        void outer_ellipse_StylusDown(object sender, StylusDownEventArgs e)
        {
            e.Handled = true;
        }

        private void inner_ellipse_MouseDown(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
        }
        private void inner_ellipse_MouseUp(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;

            StartHideAnimation();
        }


        private void inner_ellipse_MouseMove(object sender, MouseEventArgs e)
        {
            if (!_is_moving)
            {
                _is_moving = true;
                if (BeginMove != null)
                    BeginMove(this, new BeginMoveEventArgs(int.MinValue));
            }
        }

        private void inner_ellipse_StylusMove(object sender, StylusEventArgs e)
        {
            if (!_is_moving)
            {
                _is_moving = true;
                if (BeginMove != null)
                    BeginMove(this, new BeginMoveEventArgs(e.StylusDevice.Id));
            }
        }

        void inner_ellipse_StylusUp(object sender, StylusEventArgs e)
        {
            e.Handled = true;

            if (_is_moving)
            {
                _is_moving = false;
            }
            else
            {
                StartHideAnimation();
            }
        }

        public void StopMoving()
        {
            if (_is_moving)
                _is_moving = false;
        }

        void inner_ellipse_StylusDown(object sender, StylusDownEventArgs e)
        {
            e.Handled = true;
        }

        private void pie_item_MouseDown(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
        }
        private void pie_item_MouseUp(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
            if (e.RightButton == MouseButtonState.Pressed)
                return;

            Path p_shape;
            if (sender is Path)
                p_shape = (Path)sender;
            else if (sender is Canvas && ((Canvas)sender).Tag is Path)
                p_shape = (Path)((Canvas)sender).Tag;
            else
                return;

            HandleUp(p_shape);
        }

        void p_StylusUp(object sender, StylusEventArgs e)
        {
            e.Handled = true;

            Path p_shape;
            if (sender is Path)
                p_shape = (Path)sender;
            else if (sender is Canvas && ((Canvas)sender).Tag is Path)
                p_shape = (Path)((Canvas)sender).Tag;
            else
                return;

            HandleUp(p_shape);
        }

        void p_StylusDown(object sender, StylusDownEventArgs e)
        {
            e.Handled = true;
        }

        void HandleUp(Path p_shape)
        {
            PE pieMenuEntry = (PE)p_shape.Tag;

            if (pieMenuEntry is TOG)
            {
                TOG t_pie_entry = (TOG)pieMenuEntry;
                t_pie_entry.IsChecked = !t_pie_entry.IsChecked;
                pieMenuEntry.UpdatePieStyle();
            }
            else if (pieMenuEntry is BUT)
            {
                BUT b_pie_entry = (BUT)pieMenuEntry;
                b_pie_entry.RaiseClick();
            }

            if (pieMenuEntry.SubEntries == null)
            {
                if (ButtonClick != null)
                    ButtonClick(this, new ButtonClickEventArgs<PE>(pieMenuEntry));
                return;
            }

            if (pieMenuEntry.SubEntries != null && pieMenuEntry.SubEntries.Count > 0)
                StartSelectedAnimation(p_shape);
        }
        #endregion

        #region Properties
        public double OuterRadius
        {
            get
            {
                return _outer_radius;
            }
        }
        public double PieFontSize
        {
            set { _font_size = value; }
            get { return _font_size; }
        }
        public Brush PieFontBrush
        {
            set { _font_brush = value; }
            get { return _font_brush; }
        }
        public Brush PieFontStrokeBrush
        {
            set { _font_stroke_brush = value; }
            get { return _font_stroke_brush; }
        }
        public float PieFontStrokeThickness
        {
            set { _font_stroke_thickness = value; }
            get { return _font_stroke_thickness; }
        }
        public BitmapEffect PieFontBitmapEffect
        {
            set { _font_bitmap_effect = value; }
            get { return _font_bitmap_effect; }
        }
        #endregion

        #region Attributes

        protected bool _close_on_outer_ellipse_clicked;

        protected List<PE> _menu_entries;

        /// <summary>
        /// in pixel kann man hier angeben wieviel platz zwischen pie und text liegen soll
        /// </summary>
        protected double _text_pie_padding = 10;

        public static readonly double DefaultFontSize = 20;
        protected double _font_size = DefaultFontSize;
        public static readonly Brush DefaultFontBrush = Brushes.White;
        public static readonly Brush DefaultFontStrokeBrush = Brushes.Transparent;
        public static readonly float DefaultFontStrokeThickness = 0;
        protected Brush _font_brush = DefaultFontBrush;
        protected BitmapEffect _font_bitmap_effect = null;

        protected Brush _font_stroke_brush = Brushes.White;
        protected float _font_stroke_thickness = 0;

        protected Dictionary<Path, Path> _path_text_store;

        protected double _inner_radius = 30;
        protected double _outer_radius = 150;


        private bool _is_moving = false;

        public event EventHandler BackClick;
        public event EventHandler<ButtonClickEventArgs<PE>> ButtonClick;
        public class ButtonClickEventArgs<PE> : EventArgs where PE : PieMenuEntryBase<PE>
        {
            public ButtonClickEventArgs(PE menu_entry)
            {
                PieMenuEntry = menu_entry;
            }

            public PE PieMenuEntry;
        }

        public event EventHandler<EnterSubMenuClickEventArgs<PE>> EnterSubMenuClick;
        public class EnterSubMenuClickEventArgs<PE> : EventArgs where PE : PieMenuEntryBase<PE>
        {
            public EnterSubMenuClickEventArgs(PE menu_entry)
            {
                PieMenuEntry = menu_entry;
            }

            public PE PieMenuEntry;
        }

        public event EventHandler<BeginMoveEventArgs> BeginMove;
        public class BeginMoveEventArgs : EventArgs
        {
            public BeginMoveEventArgs(int stylus_id)
            {
                StylusId = stylus_id;
            }

            public int StylusId;
        }
        #endregion




    }
}