﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ArtLib
{
    /// <summary>
    /// Interaction logic for Pattern.xaml
    /// </summary>
    public partial class Pattern : UserControl
    {
        public Pattern()
        {
            InitializeComponent();

            CompositionTarget.Rendering += CompositionTarget_Rendering;


            v1.X = 0;
            v1.Y = 0;

            v2.X = 0;
            v2.Y = 0;

            v3.X = 0;
            v3.Y = 0;
        }

        void CompositionTarget_Rendering(object sender, EventArgs e)
        {
            path.Data = Geometry.Parse("F1 M 0,0L 100,100L 200,200L 10,60 Z");
        }

        private Vector v1,v2,v3;
    }
}
