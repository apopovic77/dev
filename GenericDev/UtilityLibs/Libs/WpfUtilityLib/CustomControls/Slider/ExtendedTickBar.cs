﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Controls.Primitives;
using System.Globalization;

namespace Logicx.WpfUtility.CustomControls.Slider
{
    public class ExtendedTickBar : TickBar
    {
        static ExtendedTickBar()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(ExtendedTickBar), new FrameworkPropertyMetadata(typeof(ExtendedTickBar)));
        }

        public ExtendedTickBar()
        {

            Loaded += new RoutedEventHandler(ExtendedTickBar_Loaded);            
        }

        void ExtendedTickBar_Loaded(object sender, RoutedEventArgs e)
        {
            if (StartYear > 0)
                _startDate = new DateTime(Convert.ToInt32(StartYear), 1, 1);

            _month = (EndYear - StartYear) * 12;

            _initialized = true;

        }

        protected override void OnRender(DrawingContext dc)
        {
            if (_initialized)
            {
                Size size = new Size(base.ActualWidth, base.ActualHeight);

                FormattedText formattedText = null;

                DateTime curDate = _startDate;

                int middle = Convert.ToInt32(Math.Round((_month / 12) / 2));
                middle = middle * 12;

                for (int i = 0; i <= _month; i++)
                {
                    if ((i % 12) == 0)
                    {
                        curDate = i == 0 ? curDate : curDate.AddMonths(12);
                        formattedText = new FormattedText(curDate.Year.ToString(), CultureInfo.GetCultureInfo("de"),
                        FlowDirection.LeftToRight, new Typeface("Verdana"), 8, TickBrush);

                        dc.DrawLine(new Pen(TickBrush, 1), new Point((i * (size.Width / _month)), size.Height), new Point((i * (size.Width / _month)), 4));
                        if (_month <= 180 || (_month > 180 && (i == 0 || i == _month)))
                            dc.DrawText(formattedText, new Point((i * (size.Width / _month) - 9), size.Height));
                        else if (i == middle)
                            dc.DrawText(formattedText, new Point((i * (size.Width / _month) - 9), size.Height));
                    }
                    else
                    {
                        //dc.DrawLine(new Pen(Brushes.Black, 0.1), new Point((i * (size.Width / _month)), size.Height-5), new Point((i * (size.Width / _month)), 9));
                        //dc.DrawText(new FormattedText(".", CultureInfo.GetCultureInfo("de"),
                        //FlowDirection.LeftToRight, new Typeface("Verdana"), 8, Brushes.Black), new Point((i * (size.Width / _month)), size.Height));
                    }
                    if (_month <= 12)
                    {
                        dc.DrawLine(new Pen(TickBrush, 0.5), new Point((i * (size.Width / _month)), size.Height - 5), new Point((i * (size.Width / _month)), 9));
                        //dc.DrawText(new FormattedText(".", CultureInfo.GetCultureInfo("de"),
                        //FlowDirection.LeftToRight, new Typeface("Verdana"), 8, Brushes.Black), new Point((i * (size.Width / _month)), size.Height));
                    }
                }
            }
        }



        public Brush TickBrush
        {
            get { return (Brush)GetValue(TickBrushProperty); }
            set { SetValue(TickBrushProperty, value); }
        }

        // Using a DependencyProperty as the backing store for TickBrush.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TickBrushProperty =
            DependencyProperty.Register("TickBrush", typeof(Brush), typeof(ExtendedTickBar), new FrameworkPropertyMetadata(Brushes.Black, FrameworkPropertyMetadataOptions.AffectsRender));

        

        public double StartYear
        {
            get { return (double)GetValue(StartYearProperty); }
            set { SetValue(StartYearProperty, value); }
        }

        public static readonly DependencyProperty StartYearProperty =
            DependencyProperty.Register("StartYear", typeof(double), typeof(ExtendedTickBar), new FrameworkPropertyMetadata((double)0, FrameworkPropertyMetadataOptions.AffectsRender));




        public double EndYear
        {
            get { return (double)GetValue(EndYearProperty); }
            set { SetValue(EndYearProperty, value); }
        }

        public static readonly DependencyProperty EndYearProperty =
            DependencyProperty.Register("EndYear", typeof(double), typeof(ExtendedTickBar), new FrameworkPropertyMetadata((double)0, FrameworkPropertyMetadataOptions.AffectsRender));


        #region Attributes
        DateTime _startDate = new DateTime(1900, 1, 1);
        DateTime _endDate = new DateTime(1900, 12, 31);

        bool _initialized = false;

        double _month;
        #endregion
    }
}
