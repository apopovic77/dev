using System;
using System.Data;
using System.Data.Odbc;
using System.Collections;
using System.IO;

namespace Logicx.Utilities.Database
{
	/// <summary>
	/// 
	/// </summary>
	abstract public class DBConnection : System.IDisposable {

        public bool WithDebugInfo = false;
        public string PathToDebugInfoFile = "c:\\db_debuggin.txt";

		protected string m_username;
		protected string m_password;
		protected string m_db="";
		protected string m_host="localhost";
		protected string m_port="";
		protected string m_dburl="";

        protected int recursive_counter = 0;

		protected int      m_columnCount=0;
		protected string[] m_columnLabels=null;

		protected  DataTable	   m_datatable;

		protected  bool m_defaultAutoCommit = true;

		public DBConnection()
		{}

        private StreamWriter m_DebugFile;
        private StreamWriter GetDebugFile()
        {
            try
            {
                if (m_DebugFile == null)
                {

                    m_DebugFile = File.AppendText(PathToDebugInfoFile);
                    return m_DebugFile;
                }
                else
                    return m_DebugFile;
            }
            catch  { }
            return null;
        }
        public void AddErrorMessage(string msg) {
            if (WithDebugInfo) {
                try
                {
                    StreamWriter sw = GetDebugFile();
                    sw.WriteLine("\n\n" + System.DateTime.Now + "\n===============================\n" + msg + "\n");
                    sw.Flush();
                }
                catch { }
            }
        }


		protected abstract void SetUrl();
		public abstract void Connect();
		public abstract void Execute(string dmlstmt);
		public abstract void Execute(string dmlstmt,bool withLock);
		public abstract void ExecuteCommit(string ddlstmt);
		public abstract void ExecuteQuery(string query);
		public abstract object ExecuteQueryScalar(string query);
		public abstract object ExecuteQueryScalar(string query,bool withLock);
		public abstract object[] ExecuteQueryLine(string query);
		public abstract Hashtable ExecuteQueryLineHash(string query);
		protected abstract void Disconnect();
		public abstract void Commit();
		public abstract void BeginTransaction();
		public abstract void Rollback();
		public abstract void ClearResultSet();
		protected abstract void SetAutoCommit(bool value);
		public abstract Object GetDataReader();
		public abstract bool HasMoreLines();
		public abstract object[] GetLine();
		public abstract object[] GetLine(DataRow row, DataSet ds);
		public abstract Hashtable GetLineHash();
		public abstract Hashtable GetLineHash(DataRow row, DataSet ds);

		private System.Threading.Mutex m_mutex = new System.Threading.Mutex(false,"dbmutexx");

		public void Lock(){
			m_mutex.WaitOne();
		}
		public void Unlock(){
			m_mutex.ReleaseMutex();
		}




		public string Username{
			set{ m_username = value; }
			get{ return m_username; }
		}
		public string Password{
			set{ m_password = value; }
			get{ return m_password; }
		}
		public string Db{
			set{ m_db = value; }
			get{ return m_db; }
		}
		public string Host{
			set{ m_host = value; }
			get{ return m_host; }
		}
		public string Port{
			set{ m_port = value; }
			get{ return m_port; }
		}


		private bool m_disposed = false;
		~DBConnection() {
			Dispose(false);
		}
		public void Dispose() {
			Dispose(true);
			System.GC.SuppressFinalize(this);
		}
		public void Dispose(bool explicitCall) {
			if(!this.m_disposed) {
				if(explicitCall){
					//explicit call object are available perform custom cleanup
				}

				//close and dispose db stuff				
				Disconnect();

				m_username=null;
				m_password=null;
				m_db=null;
				m_host=null;
				m_port=null;
				m_dburl=null;

                try
                {
                    if (m_DebugFile != null)
                    {
                        m_DebugFile.Flush();
                        m_DebugFile.Close();
                    }
                }
                catch { }

				for(int i=0;i<m_columnCount;i++)
					m_columnLabels[i]=null;
			}
			m_disposed = true;   
		}



		public int ColumnCount{
			get{ return m_columnCount; }
		}
	}
}
