﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Windows.Markup;

namespace Logicx.WpfUtility.Converters
{
    public class DoubleToFormattedNumberConverterExtension : MarkupExtension
    {
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return new DoubleToFormattedNumberConverter();
        }
    }


    [ValueConversion(typeof(double), typeof(string))]
    public class DoubleToFormattedNumberConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return null;

            int round_stellen;
            if (parameter == null || !int.TryParse(parameter.ToString(), out round_stellen))
                round_stellen = 3;

            double d_value = System.Convert.ToDouble(value);
            return String.Format("{0:0.000}", Math.Round(d_value, round_stellen)).Replace(",",".");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return null;
            if (value is string && string.IsNullOrEmpty(value.ToString()))
                return null;
            return System.Convert.ToDouble(value);
        }
    }


    public class DoubleToFormattedStringConverterExtension : MarkupExtension
    {
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return new DoubleToFormattedStringConverter();
        }
    }


    [ValueConversion(typeof(double), typeof(string))]
    public class DoubleToFormattedStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return null;

            int round_stellen;
            if (parameter == null || !int.TryParse(parameter.ToString(), out round_stellen))
                round_stellen = 3;

            double d_value = System.Convert.ToDouble(value);
            return String.Format("{0:0.00}", Math.Round(d_value, round_stellen)).Replace(",", ".");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return null;
            if (value is string && string.IsNullOrEmpty(value.ToString()))
                return null;
            return System.Convert.ToDouble(value);
        }
    }
}
