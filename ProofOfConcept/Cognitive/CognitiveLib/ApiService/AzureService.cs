﻿using System;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CognitiveLib.ApiService
{
    public class AzureService
    {

        #region Azure Image CV Service

        /// <summary>
        /// Gets the analysis of the specified image file by using the Computer Vision REST API.
        /// </summary>
        /// <param name="imageFilePath">The image file.</param>
        public static dynamic MakeAnalysisRequest(byte[] image_bytes, out string json_response)
        {
            HttpClient client = new HttpClient();

            // Request headers.
            client.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", AzureService.SUBSCRIPTIONKEY);

            HttpResponseMessage response;

            using (ByteArrayContent content = new ByteArrayContent(image_bytes))
            {
                // This example uses content type "application/octet-stream".
                // The other content types you can use are "application/json" and "multipart/form-data".
                content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");

                // Execute the REST API call.
                response = client.PostAsync(AzureService.UriOcr, content).Result;

                // Get the JSON response.
                json_response = response.Content.ReadAsStringAsync().Result;

                //// Display the JSON response.
                //Console.WriteLine("\nResponse:\n");
                //Console.WriteLine(JsonPrettyPrint(contentString));

                return JsonConvert.DeserializeObject(json_response);
            }
        }

        /// <summary>
        /// Gets the analysis of the specified image file by using the Computer Vision REST API.
        /// </summary>
        /// <param name="imageFilePath">The image file.</param>
        public static JObject MakeAnalysisRequest(string imageFilePath, out string json_response)
        {
            // Request body. Posts a locally stored JPEG image.
            byte[] byteData = GetImageAsByteArray(imageFilePath);

            return MakeAnalysisRequest(byteData, out json_response);
        }


        /// <summary>
        /// Returns the contents of the specified file as a byte array.
        /// </summary>
        /// <param name="imageFilePath">The image file to read.</param>
        /// <returns>The byte array of the image data.</returns>
        static byte[] GetImageAsByteArray(string imageFilePath)
        {
            FileStream fileStream = new FileStream(imageFilePath, FileMode.Open, FileAccess.Read);
            BinaryReader binaryReader = new BinaryReader(fileStream);
            return binaryReader.ReadBytes((int)fileStream.Length);
        }


        /// <summary>
        /// Formats the given JSON string by adding line breaks and indents.
        /// </summary>
        /// <param name="json">The raw JSON string to format.</param>
        /// <returns>The formatted JSON string.</returns>
        static string JsonPrettyPrint(string json)
        {
            if (string.IsNullOrEmpty(json))
                return string.Empty;

            json = json.Replace(Environment.NewLine, "").Replace("\t", "");

            StringBuilder sb = new StringBuilder();
            bool quote = false;
            bool ignore = false;
            int offset = 0;
            int indentLength = 3;

            foreach (char ch in json)
            {
                switch (ch)
                {
                    case '"':
                        if (!ignore) quote = !quote;
                        break;
                    case '\'':
                        if (quote) ignore = !ignore;
                        break;
                }

                if (quote)
                    sb.Append(ch);
                else
                {
                    switch (ch)
                    {
                        case '{':
                        case '[':
                            sb.Append(ch);
                            sb.Append(Environment.NewLine);
                            sb.Append(new string(' ', ++offset * indentLength));
                            break;
                        case '}':
                        case ']':
                            sb.Append(Environment.NewLine);
                            sb.Append(new string(' ', --offset * indentLength));
                            sb.Append(ch);
                            break;
                        case ',':
                            sb.Append(ch);
                            sb.Append(Environment.NewLine);
                            sb.Append(new string(' ', offset * indentLength));
                            break;
                        case ':':
                            sb.Append(ch);
                            sb.Append(' ');
                            break;
                        default:
                            if (ch != ' ') sb.Append(ch);
                            break;
                    }
                }
            }

            return sb.ToString().Trim();
        }
        #endregion

        // **********************************************
        // *** Update or verify the following values. ***
        // **********************************************

        // Replace the subscriptionKey string value with your valid subscription key.
        public const string SUBSCRIPTIONKEY = "a7c08c71779c40fbb1e57861e4ca5f19";

        // Replace or verify the region.
        //
        // You must use the same region in your REST API call as you used to obtain your subscription keys.
        // For example, if you obtained your subscription keys from the westus region, replace 
        // "westcentralus" in the URI below with "westus".
        //
        // NOTE: Free trial subscription keys are generated in the westcentralus region, so if you are using
        // a free trial subscription key, you should not need to change this region.
        public const string URIBASE_ANALYSE = "https://westeurope.api.cognitive.microsoft.com/vision/v1.0/analyze";

        public const string URIBASE_OCR = "https://westeurope.api.cognitive.microsoft.com/vision/v1.0/ocr";

        public const string URIBASE_HANDWRITING = "https://westeurope.api.cognitive.microsoft.com/vision/v1.0/recognizeText";

        public const string REQ_PARA_ANALYSE = "language=en&detectOrientation=true";

        public const string REQ_PARA_OCR = "visualFeatures=Categories,Description,Color&language=en&detectOrientation=false";

        public const string REQ_PARA_HANDWRITING = "handwriting=true";

        public static string UriAnalyse
        {
            get
            {
                return URIBASE_ANALYSE + "?" + REQ_PARA_ANALYSE;
            }
        }


        public static string UriOcr
        {
            get
            {
                return URIBASE_OCR + "?" + REQ_PARA_OCR;
            }
        }

        public static string UriHandwriting
        {
            get
            {
                return URIBASE_HANDWRITING + "?" + REQ_PARA_HANDWRITING;
            }
        }
    }
}
