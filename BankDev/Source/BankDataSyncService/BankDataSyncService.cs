﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using BankLib.Data;

namespace BankDataSyncService
{
    public partial class BankDataSyncService : ServiceBase
    {
        public BankDataSyncService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {

            try
            {
                if (File.Exists(AppHelper.PathToLogFile))
                    File.Delete(AppHelper.PathToLogFile);
                if (File.Exists(BuchungsStatusInfo.PathToStatusFile))
                    File.Delete(BuchungsStatusInfo.PathToStatusFile);

            }
            catch
            {
            }
            this.EventLog.WriteEntry("Starte BankDataSyncer");
            _bg_thread = new Thread(BgWorker);
            _bg_thread.Start();
        }

        private void BgWorker()
        {
            try
            {
                AppHelper.WriteFileLog("\r\nCurrent Working Dir Old " + Directory.GetCurrentDirectory());
                Directory.SetCurrentDirectory(@"E:\Dev\BankDev\Release\BankDataSyncService");
                AppHelper.WriteFileLog("\r\nCurrent Working Dir " + Directory.GetCurrentDirectory());
                while (true)
                {
                    bool success = false;
                    lock (this)
                    {
                        if (_abort)
                            return;

                        try
                        {
                            string error_msg="";
                            Buchungen.UpdateAllFromWeb(ref error_msg);
                            success = true;
                        }
                        catch(Exception ex)
                        {
                            //unerwartet exception of den error log schreiben
                            this.EventLog.WriteEntry("UNERWARTET FEHLER FETCH DATA FROM HYPO WEB" + ex.ToString());
                            AppHelper.WriteFileLog("\r\n\r\nUNERWARTET FEHLER FETCH DATA FROM HYPO WEB" + ex.ToString());
                        }
                    }

                    if (success)
                    {
                        int wait_min = 0;
                        while (wait_min < 120)
                        {
                            Thread.Sleep(new TimeSpan(0, 0, 1, 0));
                            wait_min++;
                        }
                    }
                    else
                        Thread.Sleep(new TimeSpan(0, 0, 20, 0));
                }
            }
            catch
            {
            }
        }





        protected override void OnStop()
        {
            if (_bg_thread != null)
            {
                lock (this)
                {
                    _abort = true;
                    _bg_thread.Abort();
                    _bg_thread.Join();
                    _bg_thread = null;
                    this.EventLog.WriteEntry("BankDataSyncService erfolgreich gestoppt");
                    AppHelper.WriteFileLog("BankDataSyncService erfolgreich gestoppt");
                }
            }
        }

        //private EventLog _event_log;
        private Thread _bg_thread;
        private bool _abort;
    }
}
