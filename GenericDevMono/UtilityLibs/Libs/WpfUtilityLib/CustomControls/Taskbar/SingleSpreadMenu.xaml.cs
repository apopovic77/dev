﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using Logicx.WpfUtility.CustomControls.Taskbar.ContainerStyles;
using MathLib;

namespace Logicx.WpfUtility.CustomControls.Taskbar
{
    /// <summary>
    /// Interaktionslogik für SingleSpreadMenu.xaml
    /// </summary>
    public partial class SingleSpreadMenu : UserControl
    {
        public class MenuItem
        {
            public void RaiseClickEvent()
            {
                if (Click != null)
                    Click(this, new EventArgs());
            }

            public void SetText(string text)
            {
                SpreadMenuItemContainer.text_block.Text = text;
            }

            public void SetBackgroundBrush(Brush new_brush)
            {
                if (ContainerUserControl != null && ContainerUserControl is Monocle)
                {
                    ((Monocle)ContainerUserControl).bg_ellipse.Fill = new_brush;
                }
            }

            public void ResetBackgroundBrush()
            {
                if (ContainerUserControl != null && ContainerUserControl is Monocle)
                {
                    ((Monocle)ContainerUserControl).bg_ellipse.Fill = OriginalBackgroundBrush;
                }
            }

            #region Attributes
            public UserControl ContainerUserControl;
            public FrameworkElement FrameworkElement;
            public string Text;
            public string Key;
            public SpreadMenuContainer SpreadMenuItemContainer;
            public event EventHandler Click;
            public bool HideOnClick;
            public int Index;
            public object Tag;
            internal Brush OriginalBackgroundBrush = null;
            #endregion
        }

        public enum MenuItemContainerStyle
        {
            Nothing = 1,
            Monocle = 2,
            UnderMonocle = 3
        }

        public enum SpreadDirs
        {
            ToRight = 1,
            ToLeft = 2
        }

        public SingleSpreadMenu() : this(SpreadDirs.ToRight)
        {
        }

        public SingleSpreadMenu(SpreadDirs spread_dirs)
        {
            InitializeComponent();

            SpreadDir = spread_dirs;

            _scale_out_anim = new DoubleAnimation();
            _scale_out_anim.From = 1;
            _scale_out_anim.To = 0;
            _scale_out_anim.Duration = new Duration(new TimeSpan(0, 0, 0, 0, 500));
            _scale_out_anim.AccelerationRatio = 1;
            _scale_out_anim.Completed += new EventHandler(scale_out_anim_Completed);

            _scale_in_anim = new DoubleAnimation();
            _scale_in_anim.From = 0;
            _scale_in_anim.To = 1;
            _scale_in_anim.Duration = new Duration(new TimeSpan(0, 0, 0, 0, 500));
            _scale_in_anim.AccelerationRatio = 0.5;
            _scale_in_anim.DecelerationRatio = 0.5;

            _opacity_out_anim = new DoubleAnimation();
            _opacity_out_anim.To = 0;
            _opacity_out_anim.From = 1;
            _opacity_out_anim.Duration = new Duration(new TimeSpan(0, 0, 0, 0, 300));
            _opacity_out_anim.AccelerationRatio = 1;

            _opacity_in_anim = new DoubleAnimation();
            _opacity_in_anim.From = 0;
            _opacity_in_anim.To = 1;
            _opacity_in_anim.Duration = new Duration(new TimeSpan(0, 0, 0, 0, 300));
            _opacity_in_anim.AccelerationRatio = 1;

            Canvas = canvas;
        }

        public MenuItem AddMenuItem(FrameworkElement framework_element, string text, int index, bool hide_on_click, MenuItemContainerStyle container_style, Brush background_brush, Brush border_brush, double border_thickness)
        {
            
            framework_element.Margin = new Thickness(0);
            framework_element.VerticalAlignment = VerticalAlignment.Center;
            framework_element.HorizontalAlignment = HorizontalAlignment.Center;

            SpreadMenuContainer container = new SpreadMenuContainer(SpreadDir);
            container.Cursor = Cursors.Hand;
            container.text_block.Text = text;
            container.MouseUp += new MouseButtonEventHandler(container_MouseUp);
            container.MouseLeave += new MouseEventHandler(container_MouseLeave);
            container.MouseEnter += new MouseEventHandler(container_MouseEnter);

            MenuItem menu_item = new MenuItem();
            menu_item.FrameworkElement = framework_element;
            menu_item.HideOnClick = hide_on_click;
            menu_item.SpreadMenuItemContainer = container;
            menu_item.Text = text;
            menu_item.Index = index;

            menu_item.OriginalBackgroundBrush = background_brush;

            container.Tag = menu_item;
            framework_element.Tag = menu_item;
            if (_menu_items == null) _menu_items = new List<MenuItem>();
            _menu_items.Add(menu_item);
            _menu_items = _menu_items.OrderBy(mi => mi.Index).ToList();

            if (container_style == MenuItemContainerStyle.Monocle || container_style == MenuItemContainerStyle.UnderMonocle)
            {
                Monocle monocle = new Monocle();
                monocle.VerticalAlignment = VerticalAlignment.Center;
                monocle.HorizontalAlignment = HorizontalAlignment.Center;
                monocle.bg_ellipse.Fill = background_brush;
                monocle.bg_ellipse.Stroke = border_brush;
                monocle.bg_ellipse.StrokeThickness = border_thickness;
                if (container_style == MenuItemContainerStyle.UnderMonocle)
                    monocle.grid.Children.Insert(0,framework_element);
                else
                    monocle.grid.Children.Insert(0, framework_element);
                menu_item.ContainerUserControl = monocle;
            }

            if (menu_item.ContainerUserControl != null)
            {
                container.container_grid.Children.Add(menu_item.ContainerUserControl);
                menu_item.ContainerUserControl.Tag = menu_item;
            }
            else
                container.container_grid.Children.Add(framework_element);

            Canvas.Children.Add(container);
            return menu_item;
        }

        public MenuItem AddMenuItem(FrameworkElement framework_element, string text, int index, bool hide_on_click, MenuItemContainerStyle container_style)
        {
            return AddMenuItem(framework_element, text, index, hide_on_click, container_style, Brushes.White, Brushes.Gray, 1.5);
        }


        public MenuItem AddMenuItem(FrameworkElement framework_element, string text, int index, bool hide_on_click)
        {
            return AddMenuItem(framework_element, text, index, hide_on_click, MenuItemContainerStyle.Nothing, null,null,0);
        }


        public MenuItem AddMenuItem(FrameworkElement framework_element, string text)
        {
            return AddMenuItem(framework_element, text, 0, true, MenuItemContainerStyle.Nothing, null,null,0);
        }

        public MenuItem FindMenuItem(string text)
        {
            return _menu_items.FirstOrDefault(m => string.Equals(m.Text, text));
        }

        public MenuItem FindMenuItemByKey(string key)
        {
            return _menu_items.FirstOrDefault(m => string.Equals(m.Key, key));           
        }

        public void ShowMenu()
        {
            if (!toggle_button.IsChecked.Value)
            {
                toggle_button.IsChecked = true;
                Canvas.Visibility = System.Windows.Visibility.Visible;
            }
        }

        public void HideMenu()
        {
            if (toggle_button.IsChecked.Value)
            {
                toggle_button.IsChecked = false;
                Canvas.Visibility = System.Windows.Visibility.Collapsed;
            }
        }

        public void ClearMenu()
        {
            foreach (MenuItem menuItem in _menu_items)
            {
                menuItem.SpreadMenuItemContainer.MouseUp -= container_MouseUp;
                menuItem.SpreadMenuItemContainer.MouseLeave -= container_MouseLeave;
                menuItem.SpreadMenuItemContainer.MouseEnter -= container_MouseEnter;
            }

            Canvas.Children.Clear();
            _menu_items.Clear();
        }

        public void RemoveMenuItem(MenuItem menuItem)
        {
            if(_menu_items.Contains(menuItem))
            {
                if (Canvas.Children.Contains(menuItem.SpreadMenuItemContainer))
                {
                    menuItem.SpreadMenuItemContainer.MouseUp -= container_MouseUp;
                    menuItem.SpreadMenuItemContainer.MouseLeave -= container_MouseLeave;
                    menuItem.SpreadMenuItemContainer.MouseEnter -= container_MouseEnter;
                    Canvas.Children.Remove(menuItem.SpreadMenuItemContainer);
                }
                _menu_items.Remove(menuItem);
            }
        }

        public void UnSetActiveStyles()
        {
            foreach (SingleSpreadMenu.MenuItem menuitem in MenuItems)
            {
                menuitem.ResetBackgroundBrush();
                menuitem.SpreadMenuItemContainer.UnSetActiveStyle();
            }
        }

        #region Animations
        private void StartFadeIn()
        {
            Canvas.Visibility = System.Windows.Visibility.Visible;

            Vector2d p0, p1, p2, res;
            QuadBezierCurve quad_bezier_curve;

            double min_height = 500;
            double height = _menu_items.Count * SpreadMenuContainer.MAX_HEIGHT + _menu_items.Count * 15;
            double all_max_curve_width = 90;
            if (SpreadDir == SpreadDirs.ToLeft)
                all_max_curve_width*=-1;
            double max_curve_width = all_max_curve_width;

            Canvas.Height = height;

            if(height < min_height)
            {
                p0 = new Vector2d(0, 0);
                p1 = new Vector2d(0, min_height / -2);
                p2 = new Vector2d(all_max_curve_width, -min_height);
                quad_bezier_curve = new QuadBezierCurve(p0, p1, p2);
                res = quad_bezier_curve.Calc(height / min_height);
                max_curve_width = res.X;
            }

            p0 = new Vector2d(0, 0);
            p1 = new Vector2d(0, height / -2);
            p2 = new Vector2d(max_curve_width, -height);
            quad_bezier_curve = new QuadBezierCurve(p0, p1, p2);

            for (int i = 0; i < _menu_items.Count; i++)
            {
                MenuItem menu_item = _menu_items[i];
                double pos_bezier_t = (double)(i + 1) / (double)_menu_items.Count;
                res = quad_bezier_curve.Calc(pos_bezier_t);

                double curve_height = res.Y;
                double curve_width = res.X;

                menu_item.SpreadMenuItemContainer.StartOpenAnim(curve_height, curve_width);
            }

            _scale_transform.BeginAnimation(ScaleTransform.ScaleXProperty, _scale_in_anim);
            _scale_transform.BeginAnimation(ScaleTransform.ScaleYProperty, _scale_in_anim);
            canvas.BeginAnimation(OpacityProperty, _opacity_in_anim);
        }

        private void StartFadeOut()
        {
            Vector2d p0, p1, p2, res;
            QuadBezierCurve quad_bezier_curve;

            double min_height = 500;
            double height = _menu_items.Count * SpreadMenuContainer.MAX_HEIGHT + _menu_items.Count * 15;
            double all_max_curve_width = 90;
            if (SpreadDir == SpreadDirs.ToLeft)
                all_max_curve_width *= -1;
            double max_curve_width = all_max_curve_width;


            if (height < min_height)
            {
                p0 = new Vector2d(0, 0);
                p1 = new Vector2d(0, min_height / -2);
                p2 = new Vector2d(all_max_curve_width, -min_height);
                quad_bezier_curve = new QuadBezierCurve(p0, p1, p2);
                res = quad_bezier_curve.Calc(height / min_height);
                max_curve_width = res.X;
            }

            p0 = new Vector2d(0, 0);
            p1 = new Vector2d(0, height / -2);
            p2 = new Vector2d(max_curve_width, -height);
            quad_bezier_curve = new QuadBezierCurve(p0, p1, p2);

            for (int i = 0; i < _menu_items.Count; i++)
            {
                MenuItem menu_item = _menu_items[i];
                double pos_bezier_t = (double)(i + 1) / (double)_menu_items.Count;
                res = quad_bezier_curve.Calc(pos_bezier_t);

                double curve_height = res.Y;
                double curve_width = res.X;

                menu_item.SpreadMenuItemContainer.StartCloseAnim(curve_height, curve_width);
            }

            _scale_transform.BeginAnimation(ScaleTransform.ScaleXProperty, _scale_out_anim);
            _scale_transform.BeginAnimation(ScaleTransform.ScaleYProperty, _scale_out_anim);
            Canvas.BeginAnimation(OpacityProperty, _opacity_out_anim);
        }

        void scale_out_anim_Completed(object sender, EventArgs e)
        {
            Canvas.Visibility = System.Windows.Visibility.Collapsed;
            _scale_transform.BeginAnimation(ScaleTransform.ScaleXProperty, null);
            _scale_transform.BeginAnimation(ScaleTransform.ScaleYProperty, null);
            Canvas.BeginAnimation(OpacityProperty, null);
        }
        #endregion

        #region EventHandler
        private void toggle_button_Checked(object sender, RoutedEventArgs e)
        {
            StartFadeIn();
            border.Visibility = Visibility.Visible;
            path.Visibility = Visibility.Visible;
            toggle_button.Opacity = 0.2;
        }

        private void toggle_button_Unchecked(object sender, RoutedEventArgs e)
        {
            StartFadeOut();
            border.Visibility = Visibility.Collapsed;
            path.Visibility = Visibility.Collapsed;
            toggle_button.Opacity = 1;
        }


        void container_MouseUp(object sender, MouseButtonEventArgs e)
        {
            FrameworkElement framework_element = (FrameworkElement)sender;
            MenuItem menu_item = (MenuItem)framework_element.Tag;
            menu_item.RaiseClickEvent();
            if(menu_item.HideOnClick)
                toggle_button.IsChecked = false;
        }


        void container_MouseEnter(object sender, MouseEventArgs e)
        {
            if (MouseEnterSpreadItem != null)
                MouseEnterSpreadItem(sender, e);
        }

        void container_MouseLeave(object sender, MouseEventArgs e)
        {
            if (MouseLeaveSpreadItem != null)
                MouseLeaveSpreadItem(sender, e);
        }

        #endregion

        #region Properties
        public bool ContainsItems
        {
            get { return _menu_items != null && _menu_items.Count > 0; }
        }

        public List<MenuItem> MenuItems
        {
            get { return _menu_items; }
        }

        public Canvas Canvas
        {
            set
            {
                _canvas = value;
                _scale_transform = new ScaleTransform();
                _scale_transform.CenterX = SpreadMenuContainer.MAX_WIDTH / 2;
                _scale_transform.CenterY = SpreadMenuContainer.MAX_HEIGHT / 2;
                _canvas.RenderTransform = _scale_transform;
                _canvas.Visibility = Visibility.Collapsed;
            }
            get
            {
                return _canvas;
            }
        }

        public ToggleButton ToggleButton
        {
            get
            {
                return toggle_button;
            }
        }

        public Style SpreadButtonStyle
        {
            get { return (Style) GetValue(SpreadButtonStyleProperty); }
            set { SetValue(SpreadButtonStyleProperty, value); }
        }
        public static readonly DependencyProperty SpreadButtonStyleProperty = DependencyProperty.Register("SpreadButtonStyle", typeof (Style), typeof (SingleSpreadMenu), new PropertyMetadata(null));
        #endregion

        protected ScaleTransform _scale_transform;
        protected Canvas _canvas;
        protected List<MenuItem> _menu_items = new List<MenuItem>();
        protected DoubleAnimation _scale_out_anim;
        protected DoubleAnimation _scale_in_anim;
        protected DoubleAnimation _opacity_out_anim;
        protected DoubleAnimation _opacity_in_anim;
        public SpreadDirs SpreadDir;

        public event EventHandler<MouseEventArgs> MouseEnterSpreadItem;
        public event EventHandler<MouseEventArgs> MouseLeaveSpreadItem;
    }
}


