﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Utility
{
    public class Noisex
    {
        private double Noise(int x)
          {
              x = (x << 13) ^ x;
              return (1.0 - ((x*(x*x*15731 + 789221) + 1376312589) & 0x7fffffff) / 1073741824.0);
          }


        private double SmoothedNoise(double x)
        {
            return Noise((int)x) / 2.0 + Noise((int)x - 1) / 4.0 + Noise((int)x + 1) / 4.0;
        }


        private double InterpolatedNoise(double x)
        {

            int integer_X = (int)x;
            double fractional_X = x - integer_X;

            double v1 = SmoothedNoise(integer_X);
            double v2 = SmoothedNoise(integer_X + 1);

            return Cosine_Interpolate(v1, v2, fractional_X);
        }


        public double PerlinNoise1d(double x)
        {

            double total = 0;
            double freq = _frequency;
            double ampl = _amplitude;
            for (int i = 0; i < _number_of_octaves; i++)
            {
                total = total + InterpolatedNoise(x * freq) * ampl;
                freq *= 2;
                ampl *= _persistence;
            }
            return total;
        }


      private double Cubic_Interpolate(double v0, double v1, double v2, double v3, double x)
      {
          double P = (v3 - v2) - (v0 - v1);
          double Q = (v0 - v1) - P;
          double R = v2 - v0;
          double S = v1;

          return P* Math.Pow(x,3) + Q * Math.Pow(x,2) + R*x + S;
      }

        private double Cosine_Interpolate(double a, double b, double x)
        {
            double ft = x*Math.PI;
            double f = (1 - Math.Cos(ft))*0.5;

            return a*(1 - f) + b*f;
        }



        private double Noise2(int x, int y)
        {
            int n = x + y*57;
            n = (n << 13) ^ n;
            return (1.0 - ((n*(n*n*15731 + 789221) + 1376312589) & 0x7fffffff)/1073741824.0);
        }

        private double SmoothedNoise2(double x, double y)
        {
            double corners = (Noise2((int)x - 1, (int)y - 1) + Noise2((int)x + 1, (int)y - 1) + Noise2((int) x - 1, (int) y + 1) + Noise2((int) x + 1, (int) y + 1))/16.0;
            double sides = (Noise2((int) x - 1, (int) y) + Noise2((int) x + 1, (int) y) + Noise2((int) x, (int) y - 1) + Noise2((int) x, (int) y + 1))/8.0;
            double center = Noise2((int)x, (int)y)/4.0;
            return corners + sides + center;
        }

        private double InterpolatedNoise2(double x, double y)
        {
            int integer_X    = (int)x;
            double fractional_X = x - integer_X;

            int integer_Y    = (int)y;
            double fractional_Y = y - integer_Y;

            double v1 = SmoothedNoise2(integer_X, integer_Y);
            double v2 = SmoothedNoise2(integer_X + 1, integer_Y);
            double v3 = SmoothedNoise2(integer_X,     integer_Y + 1);
            double v4 = SmoothedNoise2(integer_X + 1, integer_Y + 1);

            double i1 = Cosine_Interpolate(v1 , v2 , fractional_X);
            double i2 = Cosine_Interpolate(v3 , v4 , fractional_X);

            return Cosine_Interpolate(i1 , i2 , fractional_Y);
        }


        public double PerlinNoise2d(double x, double y)
        {
            double total = 0;
            double freq = _frequency;
            double ampl = _amplitude;
            for (int i = 0; i < _number_of_octaves; i++)
            {
                total = total + InterpolatedNoise2(x * freq, y * freq) * ampl;
                freq *= 2;
                ampl *= _persistence;
            }
            return total;
        }

        private double _amplitude = 1;
        private double _frequency = 0.0625;
        private double _persistence = 1.0 / 4.0;
        private int _number_of_octaves = 16;
    }
}
