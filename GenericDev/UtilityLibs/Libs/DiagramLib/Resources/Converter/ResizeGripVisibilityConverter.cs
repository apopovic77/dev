﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Data;

namespace Logicx.DiagramLib.Resources.Converter
{
    public class ResizeGripVisibilityConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if(values.Length > 1)
            {
                if (values[0] is bool)
                {
                    bool isSelected = (bool) values[0];
                    bool orFlags = false;

                    for(int i=1; i < values.Length; i++)
                    {
                        if(values[i] is bool)
                        {
                            orFlags |= (bool) values[i];
                        }
                    }

                    if (isSelected && orFlags)
                        return Visibility.Visible;
                }

            }
            else
            {
                if(values[0] is bool)
                {
                    if((bool)values[0])
                        return Visibility.Visible;
                }
            }

            return Visibility.Collapsed;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }
}
