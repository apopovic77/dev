﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using Logicx.Utility;
using MathLib;

namespace CognitiveLib.Scanner
{
    public class DocumentScanner
    {

        /// <summary>
        /// this medthod detects an alignment rect and performs a perspective warp
        /// </summary>
        /// <param name="org_image"></param>
        /// <param name="resize_to_height"></param>
        /// <param name="thresh1"></param>
        /// <param name="thresh2"></param>
        /// <param name="smooth"></param>
        /// <param name="approx_poly_db_factor"></param>
        /// <param name="min_arc_len"></param>
        /// <param name="min_contour_area"></param>
        /// <returns></returns>
        public static IImage WarpToFrontal(IImage org_image, int resize_to_height = 1000, int thresh1 = 96, int thresh2 = 255, int smooth = 9, float approx_poly_db_factor = 0.02f, int min_arc_len = 50, int min_contour_area = 20)
        {
            UMat warped_img = null;
            Image<Gray, byte> org_image_gray = ((Image<Bgr, byte>)org_image).Convert<Gray, byte>();

            Image<Bgr, byte> image = null;
            //if (org_image.Size.Height > resize_to_height)
            //{
            float img_as_ratio = (float)org_image.Size.Width / (float)org_image.Size.Height;
            int width = (int)((float)img_as_ratio * (float)resize_to_height);
            Size size = new Size(width, resize_to_height);
            image = new Image<Bgr, byte>(size.Width, size.Height); //this is image with resize
            CvInvoke.Resize(org_image, image, size, 0, 0, Inter.Area);
            //}
            //else
            //{
            //    image = (Image<Bgr, byte>)org_image.Clone();
            //}





            Image<Gray, byte> gray_scale_image = ((Image<Bgr, byte>)image).Convert<Gray, byte>();


            if (smooth > 0)
            {
                Image<Gray, byte> blurred_image = gray_scale_image.SmoothGaussian(smooth, smooth, 0, 0);
                gray_scale_image.Dispose();
                gray_scale_image = blurred_image;
            }

            CvInvoke.Threshold(gray_scale_image,gray_scale_image, thresh1,thresh2,ThresholdType.Binary);

            CvInvoke.Imshow("thresed", gray_scale_image);

            //UMat canny_image = new UMat();
            //            CvInvoke.Canny(gray_scale_image, canny_image, thresh1, thresh2);
            //gray_scale_image.Dispose();


            float area_larges_rect = 0f;
            Point[] largest_rect = null;

            float largest_match = float.MaxValue;
            Point[] largest_match_rect = null;

            VectorOfPoint points_cv = new VectorOfPoint();
            using (VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint())
            {
                CvInvoke.FindContours(gray_scale_image, contours, null, RetrType.List, ChainApproxMethod.ChainApproxSimple);
                gray_scale_image.Dispose();



                for (int i = 0; i < contours.Size; i++)
                {

                    if (contours[i].Size < 2)
                        continue;


                    double arc_length = CvInvoke.ArcLength(contours[i], true);

                    //if (arc_length < min_arc_len)
                    //    continue;


                    VectorOfPoint contour = contours[i];
                    CvInvoke.ApproxPolyDP(contours[i], contour, approx_poly_db_factor * arc_length, true);

                    if (contours[i].Size < 2)
                        continue;


                    //////continue if path is not closed
                    //float diff_len = OpenCvHelper.GetLen(contour[0], contour[contour.Size - 1]);
                    //if (diff_len > 5)
                    //    continue;

                    //float solidity = OpenCvHelper.GetSolidity(contour);
                    //if (solidity < 0.5)
                    //    continue;

                    double contour_area = CvInvoke.ContourArea(contour);
                    if (contour_area < min_contour_area)
                        continue;

                    if (contour.Size > 4 || contour.Size < 4)
                        continue;

                    //float is_contour_perc = OpenCvHelper.IsConvexPolygonPerc(contour);
                    //if (is_contour_perc < 0.99f)
                    //    continue;

                    //OpenCvHelper.GetSolidity(contour);
                    ////List<Point> hull = Geometry.MakeConvexHull(contour.ToArray().ToList());
                    ////float len_hull = OpenCvHelper.GetLen(hull);
                    ////float area_hull = Geometry.AreaOfConvexPolygon(hull);

                    ////Point tl = new Point(), tr = new Point(), bl = new Point(), br = new Point();
                    ////Geometry.GetMinMaxCorners(hull, ref tl, ref tr, ref bl, ref br);
                    ////Point[] bbox_hull = new Point[4] { tl, tr, br, bl };
                    ////List<Point> bbox_hull_list = bbox_hull.ToList();
                    ////bbox_hull_list.Add(tl);
                    ////float len_rect = OpenCvHelper.GetLen(bbox_hull_list);
                    ////float area_rect = (tl.ToVector2F() - tr.ToVector2F()).GetLen() *
                    ////                  (tl.ToVector2F() - bl.ToVector2F()).GetLen();
                    //float solidity = OpenCvHelper.GetSolidity(contour); ;

                    //if (solidity < 0.8)
                    //    continue;

                    //float area_diff = Math.Abs(len_rect - len_hull);
                    //if (area_diff < 1f)
                    //{
                    //    if (len_rect > area_larges_rect)
                    //    {
                    //        area_larges_rect = len_rect;
                    //        largest_rect = bbox_hull;
                    //    }
                    //}


                    //TODO check if rect really is a rect, sometimes this thing is a triangle

                    //1. get max len seite
                    //2. get diff lenght from other seiten
                    //3. sum diff length
                    //4. berechne den durchschnitt aller seiten unterschiede
                    //5. berechne den prozentuellen unterschied zur maxlen
                    //6. wenn wert unterschritten dann kein rect




                    VectorOfPoint perfect_square_points = new VectorOfPoint(
                        new Point[]
                        {
                            new Point(0, 0),
                            new Point(1, 0),
                            new Point(1, 1),
                            new Point(0, 1)
                        });

                    double ratio = CvInvoke.MatchShapes(perfect_square_points, contour, ContoursMatchType.I3);
                    if (ratio > 0.1)
                        continue;

                    List<Point> hull = Geometry.MakeConvexHull(contour.ToArray().ToList());
                    float area_hull = Geometry.AreaOfConvexPolygon(hull);
                    Point tl = new Point(), tr = new Point(), bl = new Point(), br = new Point();
                    Geometry.GetMinMaxCorners(hull, ref tl, ref tr, ref bl, ref br);
                    Point[] bbox_hull = new Point[4] { tl, tr, br, bl };
                    List<Point> bbox_hull_list = bbox_hull.ToList();
                    bbox_hull_list.Add(tl);
                    float area_rect = (tl.ToVector2F() - tr.ToVector2F()).GetLen() *
                                      (tl.ToVector2F() - bl.ToVector2F()).GetLen();

                    float solidity = area_rect / area_hull;

                    if (ratio < largest_match)
                    {
                        PointF[] points = OpenCvHelper.OrderRectPoints(contour.ToArray());
                        largest_match = (float)ratio;
                        largest_match_rect = points.ToPoint();
                    }

                    if (area_rect > area_larges_rect)
                    {
                        area_larges_rect = area_rect;
                        largest_rect = bbox_hull;
                    }


                    ////if (!CvInvoke.IsContourConvex(contour))
                    ////    continue;

                    //if (!OpenCvHelper.IsContourConvex(contour))
                    //    continue;



                    //# approximate the contour
                    //                    peri = cv2.arcLength(c, True)

                    //                    approx = cv2.approxPolyDP(c, 0.02 * peri, True)

                    //#if our approximated contour has four points, then we
                    //# can assume that we have found our screen
                    //	if len(approx) == 4:
                    //		screenCnt = approx
                    //		break






                    points_cv.Push(contour);

                    using (VectorOfVectorOfPoint contour_to_draw = new VectorOfVectorOfPoint(contour))
                        CvInvoke.DrawContours(image, contour_to_draw, -1, new MCvScalar(0, 0, 255), 1);

                    contour.Dispose();
                }
            }

            if (points_cv.Size > 0)
            {
                //CvInvoke.ConvexHull(points_cv, points_cv);

                List<Point> hull = Geometry.MakeConvexHull(points_cv.ToArray().ToList());
                Point[] hull_arr = hull.ToArray();
                VectorOfPoint hull_vop = new VectorOfPoint(hull_arr);

                VectorOfVectorOfPoint convex_hull_to_draw = new VectorOfVectorOfPoint(hull_vop);
                double area_hull = CvInvoke.ContourArea(hull_vop);
                CvInvoke.DrawContours(image, convex_hull_to_draw, 0, new MCvScalar(0, 255, 255), 1);
                convex_hull_to_draw.Dispose();

                RotatedRect rect = CvInvoke.MinAreaRect(hull_vop);
                PointF[] boxf = CvInvoke.BoxPoints(rect);
                Point[] boxi = boxf.ToPoint();

                using (VectorOfVectorOfPoint min_area_rect_pointstodraw = new VectorOfVectorOfPoint(new VectorOfPoint(boxi)))
                    CvInvoke.DrawContours(image, min_area_rect_pointstodraw, 0, new MCvScalar(255, 255, 0), 1);

                warped_img = new UMat();

                Point tl = new Point(), tr = new Point(), bl = new Point(), br = new Point();
                Geometry.GetMinMaxCorners(hull, ref tl, ref tr, ref bl, ref br);
                Point[] minmax_corner_hull = new Point[4] { tl, tr, br, bl };
                VectorOfPoint minmax_corner_hull_vvop = new VectorOfPoint(minmax_corner_hull);
                double area_min_max_corner = CvInvoke.ContourArea(minmax_corner_hull_vvop);

                using (VectorOfVectorOfPoint minmaxarea_points_todraw = new VectorOfVectorOfPoint(minmax_corner_hull_vvop))
                    CvInvoke.DrawContours(image, minmaxarea_points_todraw, 0, new MCvScalar(255, 100, 255), 1);




                //float scale_bbox = (float)org_image_gray.Size.Height / (float)resize_to_height;
                //if (Math.Abs(area_hull - area_min_max_corner) > 15000)
                //{

                //    for (int i_scale = 0; i_scale < boxi.Length; i_scale++)
                //        boxi[i_scale] = new Point((int)(boxi[i_scale].X * scale_bbox), (int)(boxi[i_scale].Y * scale_bbox));

                //    OpenCvHelper.FourPointTransform(org_image_gray, warped_img, boxi);
                //}
                //else
                //{
                //    for (int i_scale = 0; i_scale < minmax_corner_hull.Length; i_scale++)
                //        minmax_corner_hull[i_scale] = new Point((int)(minmax_corner_hull[i_scale].X * scale_bbox), (int)(minmax_corner_hull[i_scale].Y * scale_bbox));

                //    OpenCvHelper.FourPointTransform(org_image_gray, warped_img, minmax_corner_hull);
                //}

                //DocumentScanner.FourPointTransform(image, warped_img, minmax_corner_hull);

                hull_vop.Dispose();


                //int width = (int)((float)img_as_ratio * (float)resize_to_height);
                //Size size = new Size(width, resize_to_height);
                //CvInvoke.Resize(warped_img, image, size, 0, 0, Inter.Area);



                // convert the warped image to grayscale, then threshold it
                // to give it that 'black and white' paper effect
                //warped_img = threshold_adaptive(warped, 251, offset = 10)
                //CvInvoke.AdaptiveThreshold(warped_img, warped_img, 251, AdaptiveThresholdType.GaussianC,ThresholdType.Binary,11,10);

                //CvInvoke.ConvertScaleAbs(warped_img, warped_img, 255, 0);



                //warped_img = warped.astype("uint8") * 255



                //warped_img.Dispose();



            }

            if (largest_match_rect != null)
                using (VectorOfVectorOfPoint match_rect_vop = new VectorOfVectorOfPoint(new VectorOfPoint(largest_match_rect)))
                    CvInvoke.DrawContours(image, match_rect_vop, 0, new MCvScalar(255, 100, 20), 2);


            if (largest_rect != null)
            {
                using (VectorOfVectorOfPoint minmaxarea_points_todraw = new VectorOfVectorOfPoint(new VectorOfPoint(largest_rect)))
                    CvInvoke.DrawContours(image, minmaxarea_points_todraw, 0, new MCvScalar(20, 100, 255), 3);

                Vector2d tl = largest_rect[0].ToVector2d();
                Vector2d tr = largest_rect[1].ToVector2d();
                Vector2d br = largest_rect[2].ToVector2d();
                Vector2d bl = largest_rect[3].ToVector2d();

                Vector2d org_tl = new Vector2d(0,0);
                Vector2d org_tr = new Vector2d(image.Size.Width,0);
                Vector2d org_br = new Vector2d(image.Size.Width, org_image_gray.Size.Height);
                Vector2d org_bl = new Vector2d(0, image.Size.Height);

                Edge top = new Edge(org_tl, org_tr);
                Edge right = new Edge(org_tr, org_br);
                Edge bottom = new Edge(org_br, org_bl);
                Edge left = new Edge(org_bl, org_tl);

                Edge tl_dir = new Edge(tl, tl + (tl - br).GetNorm() * 10000);
                Edge tr_dir = new Edge(tr, tr + (tr - bl).GetNorm() * 10000);
                Edge br_dir = new Edge(br, br + (br - tl).GetNorm() * 10000);
                Edge bl_dir = new Edge(bl, bl + (bl - tr).GetNorm() * 10000);


                //Vector2d cutpoint;
                //if (!LinearAlgebra.GetCutPointInSegment(left, tl_dir, out cutpoint))
                //    LinearAlgebra.GetCutPointInSegment(top, tl_dir, out cutpoint);
                //tl = cutpoint;

                //if (!LinearAlgebra.GetCutPointInSegment(top, tr_dir, out cutpoint))
                //    LinearAlgebra.GetCutPointInSegment(right, tr_dir, out cutpoint);
                //tr = cutpoint;

                //if (!LinearAlgebra.GetCutPointInSegment(right, br_dir, out cutpoint))
                //    LinearAlgebra.GetCutPointInSegment(bottom, br_dir, out cutpoint);
                //br = cutpoint;

                //if (!LinearAlgebra.GetCutPointInSegment(bottom, bl_dir, out cutpoint))
                //    LinearAlgebra.GetCutPointInSegment(left, bl_dir, out cutpoint);
                //bl = cutpoint;

                //largest_rect[0] = tl.ToPoint();
                //largest_rect[1] = tr.ToPoint();
                //largest_rect[2] = br.ToPoint();
                //largest_rect[3] = bl.ToPoint();

                largest_rect[0] = (tl + ((tl_dir.v2 - tl_dir.v1).GetNorm() * 400f)).ToPoint();
                largest_rect[1] = (tr + ((tr_dir.v2 - tr_dir.v1).GetNorm() * 400f)).ToPoint();
                largest_rect[2] = (br + ((br_dir.v2 - br_dir.v1).GetNorm() * 400f)).ToPoint();
                largest_rect[3] = (bl + ((bl_dir.v2 - bl_dir.v1).GetNorm() * 400f)).ToPoint();

                //using (VectorOfVectorOfPoint scaled_bounding_rect = new VectorOfVectorOfPoint(new VectorOfPoint(largest_rect)))
                //    CvInvoke.DrawContours(image, scaled_bounding_rect, 0, new MCvScalar(20, 100, 255), 1);

                float scale_bbox = (float)org_image_gray.Size.Height / (float)resize_to_height;
                for (int i_scale = 0; i_scale < largest_rect.Length; i_scale++)
                    largest_rect[i_scale] = new Point((int)(largest_rect[i_scale].X * scale_bbox), (int)(largest_rect[i_scale].Y * scale_bbox));

                if(warped_img != null)
                    OpenCvHelper.FourPointTransform(org_image_gray, warped_img, largest_rect);

            }


            //CvInvoke.Imshow("Canny", canny_image);
            //canny_image.Dispose();

            CvInvoke.Imshow("Image", image);


            org_image_gray.Dispose();

            image.Dispose();
            points_cv.Dispose();


            return warped_img;

        }




        public static IImage ScanToGrayDoc(IImage org_image, int resize_to_height = 1000, int thresh1 = 93, int thresh2 = 199, int smooth = -1, float approx_poly_db_factor = 0.001f, int min_arc_len = 1, int min_contour_area = 5)
        {
            UMat warped_img = null;
            Image<Gray, byte> org_image_gray = ((Image<Bgr, byte>)org_image).Convert<Gray, byte>();

            Image<Bgr, byte> image = null;
            //if (org_image.Size.Height > resize_to_height)
            //{
                float img_as_ratio = (float)org_image.Size.Width / (float)org_image.Size.Height;
                int width = (int)((float)img_as_ratio * (float)resize_to_height);
                Size size = new Size(width, resize_to_height);
                image = new Image<Bgr, byte>(size.Width, size.Height); //this is image with resize
                CvInvoke.Resize(org_image, image, size, 0, 0, Inter.Area);
            //}
            //else
            //{
            //    image = (Image<Bgr, byte>)org_image.Clone();
            //}





            Image<Gray, byte> gray_scale_image = ((Image<Bgr, byte>)image).Convert<Gray, byte>();


            if (smooth > 0)
            {
                Image<Gray, byte> blurred_image = gray_scale_image.SmoothGaussian(smooth, smooth, 0, 0);
                gray_scale_image.Dispose();
                gray_scale_image = blurred_image;
            }


            UMat canny_image = new UMat();

            CvInvoke.Canny(gray_scale_image, canny_image, thresh1, thresh2);

            gray_scale_image.Dispose();



            VectorOfPoint points_cv = new VectorOfPoint();
            using (VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint())
            {
                CvInvoke.FindContours(canny_image, contours, null, RetrType.External, ChainApproxMethod.ChainApproxSimple);

                for (int i = 0; i < contours.Size; i++)
                {




                    double arc_length = CvInvoke.ArcLength(contours[i], true);
                    
                    if (arc_length < min_arc_len)
                        continue;


                    VectorOfPoint contour = new VectorOfPoint();
                    CvInvoke.ApproxPolyDP(contours[i], contour, approx_poly_db_factor * arc_length, true);

                    //////continue if path is not closed
                    //float diff_len = OpenCvHelper.GetLen(contour[0], contour[contour.Size - 1]);
                    //if (diff_len > 5)
                    //    continue;



                    //if(OpenCvHelper.IsRect(contour))
                    //    using (VectorOfVectorOfPoint contour_to_draw = new VectorOfVectorOfPoint(contour))
                    //        CvInvoke.DrawContours(image, contour_to_draw, -1, new MCvScalar(10, 255, 255), 2);


                    double contour_area = CvInvoke.ContourArea(contour);
                    if (contour_area < min_contour_area)
                        continue;

                    //if (contour.Size > 8)
                    //    continue;

                    //float is_contour_perc = OpenCvHelper.IsConvexPolygonPerc(contour);
                    //if (is_contour_perc < 0.99f)
                    //    continue;

                    ////if (!CvInvoke.IsContourConvex(contour))
                    ////    continue;

                    //if (!OpenCvHelper.IsContourConvex(contour))
                    //    continue;



                    //# approximate the contour
                    //                    peri = cv2.arcLength(c, True)

                    //                    approx = cv2.approxPolyDP(c, 0.02 * peri, True)

                    //#if our approximated contour has four points, then we
                    //# can assume that we have found our screen
                    //	if len(approx) == 4:
                    //		screenCnt = approx
                    //		break






                    points_cv.Push(contour);

                    using (VectorOfVectorOfPoint contour_to_draw = new VectorOfVectorOfPoint(contour))
                        CvInvoke.DrawContours(image, contour_to_draw, -1, new MCvScalar(0, 0, 255), 1);

                    contour.Dispose();
                }
            }

            if (points_cv.Size > 0)
            {
                //CvInvoke.ConvexHull(points_cv, points_cv);

                List<Point> hull = Geometry.MakeConvexHull(points_cv.ToArray().ToList());
                Point[] hull_arr = hull.ToArray();
                VectorOfPoint hull_vop = new VectorOfPoint(hull_arr);

                VectorOfVectorOfPoint convex_hull_to_draw = new VectorOfVectorOfPoint(hull_vop);
                double area_hull = CvInvoke.ContourArea(hull_vop);
                CvInvoke.DrawContours(image, convex_hull_to_draw, 0, new MCvScalar(0, 255, 255), 1);
                convex_hull_to_draw.Dispose();

                RotatedRect rect = CvInvoke.MinAreaRect(hull_vop);
                PointF[] boxf = CvInvoke.BoxPoints(rect);
                Point[] boxi = boxf.ToPoint();

                using (VectorOfVectorOfPoint min_area_rect_pointstodraw = new VectorOfVectorOfPoint(new VectorOfPoint(boxi)))
                    CvInvoke.DrawContours(image, min_area_rect_pointstodraw, 0, new MCvScalar(255, 255, 0), 1);

                warped_img = new UMat();

                Point tl = new Point(), tr = new Point(), bl = new Point(), br = new Point();
                Geometry.GetMinMaxCorners(hull, ref tl, ref tr, ref bl, ref br);
                Point[] minmax_corner_hull = new Point[4] { tl, tr, br, bl };
                VectorOfPoint minmax_corner_hull_vvop = new VectorOfPoint(minmax_corner_hull);
                double area_min_max_corner = CvInvoke.ContourArea(minmax_corner_hull_vvop);

                using (VectorOfVectorOfPoint minmaxarea_points_todraw = new VectorOfVectorOfPoint(minmax_corner_hull_vvop))
                    CvInvoke.DrawContours(image, minmaxarea_points_todraw, 0, new MCvScalar(255, 100, 255), 2);




                float scale_bbox = (float)org_image_gray.Size.Height / (float)resize_to_height;
                if (Math.Abs(area_hull - area_min_max_corner) > 15000)
                {

                    for (int i_scale = 0; i_scale < boxi.Length; i_scale++)
                        boxi[i_scale] = new Point((int)(boxi[i_scale].X * scale_bbox), (int)(boxi[i_scale].Y * scale_bbox));

                    OpenCvHelper.FourPointTransform(org_image_gray, warped_img, boxi);
                }
                else
                {
                    for (int i_scale = 0; i_scale < minmax_corner_hull.Length; i_scale++)
                        minmax_corner_hull[i_scale] = new Point((int)(minmax_corner_hull[i_scale].X * scale_bbox), (int)(minmax_corner_hull[i_scale].Y * scale_bbox));

                    OpenCvHelper.FourPointTransform(org_image_gray, warped_img, minmax_corner_hull);
                }

                //DocumentScanner.FourPointTransform(image, warped_img, minmax_corner_hull);

                hull_vop.Dispose();


                //int width = (int)((float)img_as_ratio * (float)resize_to_height);
                //Size size = new Size(width, resize_to_height);
                //CvInvoke.Resize(warped_img, image, size, 0, 0, Inter.Area);



                // convert the warped image to grayscale, then threshold it
                // to give it that 'black and white' paper effect
                //warped_img = threshold_adaptive(warped, 251, offset = 10)
                //CvInvoke.AdaptiveThreshold(warped_img, warped_img, 251, AdaptiveThresholdType.GaussianC,ThresholdType.Binary,11,10);

                //CvInvoke.ConvertScaleAbs(warped_img, warped_img, 255, 0);


                
                //warped_img = warped.astype("uint8") * 255

                

                //warped_img.Dispose();

            }






            CvInvoke.Imshow("Canny", canny_image);
            canny_image.Dispose();

            CvInvoke.Imshow("Image", image);


            org_image_gray.Dispose();

            image.Dispose();
            points_cv.Dispose();


            return warped_img;

        }


        public static VectorOfPoint FindLargestContour(IInputOutputArray canny_edges, IInputOutputArray result)
        {
            int largest_contour_index = 0;
            double largest_area = 0;
            VectorOfPoint largest_contour;

            using (Mat hierachy = new Mat())
            using (VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint())
            {

                CvInvoke.FindContours(canny_edges, contours, hierachy, RetrType.Tree, ChainApproxMethod.ChainApproxNone);

                for (int i = 0; i < contours.Size; i++)
                {
                    MCvScalar color = new MCvScalar(0, 0, 255);

                    double a = CvInvoke.ContourArea(contours[i], false);  //  Find the area of contour
                    if (a > largest_area)
                    {
                        largest_area = a;
                        largest_contour_index = i;                //Store the index of largest contour
                    }

                    CvInvoke.DrawContours(result, contours, largest_contour_index, new MCvScalar(255, 0, 0));
                }

                CvInvoke.DrawContours(result, contours, largest_contour_index, new MCvScalar(0, 0, 255), 3, LineType.EightConnected, hierachy);
                largest_contour = new VectorOfPoint(contours[largest_contour_index].ToArray());
            }

            return largest_contour;
        }
    }
}
