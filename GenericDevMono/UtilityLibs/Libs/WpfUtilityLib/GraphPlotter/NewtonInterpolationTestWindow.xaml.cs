﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Logicx.Geo.Geometries;
using Logicx.WpfUtility.WpfHelpers;
using MathLib;

namespace Logicx.WpfUtility.GraphPlotter
{
    /// <summary>
    /// Interaction logic for NewtonInterpolationTestWindow.xaml
    /// </summary>
    public partial class NewtonInterpolationTestWindow : Window
    {
        public NewtonInterpolationTestWindow()
        {
            InitializeComponent();    
        }

        public NewtonInterpolationTestWindow(NewtonInterpolation plot, float x_start, float x_end)
        {
            InitializeComponent();
            PaintLine(plot, x_start, x_end);
        }

        public NewtonInterpolationTestWindow(CubicBezierCurve plot, float x_start, float x_end)
        {
            InitializeComponent();
            PaintLine(plot, x_start, x_end);
        }

        public void AddPoint(double x, double y)
        {
            Ellipse ell = new Ellipse();
            ell.Width = 20;
            ell.Height = 20;
            ell.Fill = Brushes.Black;
            Canvas.SetTop(ell, y);
            Canvas.SetLeft(ell, x);
            myCanvas.Children.Add(ell);
        }

        private void PaintLine(CubicBezierCurve poly, float min, float max)
        {
            if(poly== null)
                return;

            float minf, maxf;
            minf = min;
            maxf = max;

            _myGeometryGroup  = new GeometryGroup();

            List<double> d_l = new List<double>();
            for (; minf <= maxf; minf = minf + (max - min) / 50)
            {
                LineGeometry myLineGeometry = new LineGeometry();
                Vector2d p1 = poly.Calc(minf);
                Vector2d p2 = poly.Calc(minf + (max - min)/50);
                myLineGeometry.StartPoint = new Point(p1.X, p1.Y);
                myLineGeometry.EndPoint = new Point(p2.X, p2.Y);
                d_l.Add(p1.Y);

                _myGeometryGroup.Children.Add(myLineGeometry);
            }

            mypath.Data = _myGeometryGroup;
        }

        private void PaintLine(NewtonInterpolation poly, float min, float max)
        {
            if(poly == null)
                return;

            float minf, maxf;
            minf = min;
            maxf = max;

            _myGeometryGroup = new GeometryGroup();
           
            for (; minf <= maxf; minf = minf + (max - min) / 50)
            {
                LineGeometry myLineGeometry = new LineGeometry();
                myLineGeometry.StartPoint = new Point(minf, (float)poly.GetNewtonPolynomValue(minf));
                myLineGeometry.EndPoint = new Point(minf + (max - min) / 50, (float)poly.GetNewtonPolynomValue(minf + (max - min) / 50));

                _myGeometryGroup.Children.Add(myLineGeometry);
            }

            mypath.Data = _myGeometryGroup;
        }

        #region Attributes

        private GeometryGroup _myGeometryGroup;
        private int string_id = 0;
        #endregion

    }
}
