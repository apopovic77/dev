﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using Logicx.Utilities;

namespace Logicx.WpfUtility.CustomControls.OverlayManager.StatusInfo
{
    /// <summary>
    /// Interaction logic for OverlayStatusItem.xaml
    /// </summary>
    public partial class OverlayStatusItem : UserControl
    {
        public OverlayStatusItem()
        {
            InitializeComponent();
            
        }

        #region Animations
        public void StartFadeOut()
        {
            DoubleAnimation anim = new DoubleAnimation();
            anim.From = 0;
            anim.To = -border.ActualWidth;
            anim.DecelerationRatio = 0.5;
            anim.AccelerationRatio = 0.5;
            anim.Duration = new Duration(new TimeSpan(0, 0, 0, 0, 500));
            anim.Completed += new EventHandler(fadeout_anim_Completed);

            base_grid.BeginAnimation(Canvas.LeftProperty, anim);
        }

        public void StartFadeIn()
        { 
            DoubleAnimation anim = new DoubleAnimation();
            anim.From = -border.ActualWidth;
            anim.To = 0;
            anim.DecelerationRatio = 0.5;
            anim.AccelerationRatio = 0.5;
            anim.Duration = new Duration(new TimeSpan(0,0,0,0,500));

            base_grid.BeginAnimation(Canvas.LeftProperty, anim);

            if(TimeOutMsec>0)
                StartTimeout();
        }
        #endregion

        #region Timeout Mgmt
        public void ResetTimeout()
        {
            if(TimeOutMsec <= 0)
                return;
            DateTime start_timeout = DateTime.Now;
            _end_timeout = start_timeout + new TimeSpan(0, 0, 0, 0, TimeOutMsec);
        }

        public void StartTimeout()
        {
            if(_bg_worker != null && _bg_worker.IsBusy)
            {
                ResetTimeout();
                return;
            }

            _bg_worker = new QueuedBackgroundWorker("timeout_status_item");
            _bg_worker.DoWork += new QueuedBackgroundWorker.DoWorkEventHandler(_bg_worker_DoWork);
            _bg_worker.RunWorkerCompleted += new QueuedBackgroundWorker.RunWorkerCompletedEventHandler(_bg_worker_RunWorkerCompleted);
            _bg_worker.RunWorkerAsync();
        }
        
        public void StopTimeout()
        {
            if(_bg_worker == null)
                return;

            _bg_worker.Abort();
            _bg_worker = null;
        }

        void _bg_worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if(e.Error != null)
                return;

            Dispatcher.Invoke(DispatcherPriority.Normal, (NoParas)StartFadeOut);
        }

        void _bg_worker_DoWork(object sender, DoWorkEventArgs e)
        {
            DateTime start_timeout = DateTime.Now;
            _end_timeout = start_timeout + new TimeSpan(0, 0, 0, 0, TimeOutMsec);

            while (true)
            {
                Thread.Sleep(TimeOutMsec);
                if (DateTime.Now > _end_timeout)
                    break;
            }
        }
        #endregion

        public void SetAnimation()
        {
            DoubleAnimation danim = new DoubleAnimation()
            {
                From = 0,
                To = 1,
                DecelerationRatio = 0.5,
                AccelerationRatio = 0.5,
                Duration = new Duration(new TimeSpan(0, 0, 0, 0, 500)),
                RepeatBehavior = RepeatBehavior.Forever,
                AutoReverse = true
            };
            bg_border.BeginAnimation(FrameworkElement.OpacityProperty, danim);
        }

        public void UnSetAnimation()
        {
            bg_border.BeginAnimation(FrameworkElement.OpacityProperty, null);
        }

        public void SetInfoStyle()
        {
            bg_border.Background = (Brush)FindResource("info_background_brush");
        }

        public void SetErrorStyle()
        {
            bg_border.Background = (Brush)FindResource("error_background_brush");
        }

        public void SetNormalStyle()
        {
            bg_border.Background = (Brush)FindResource("background_brush");
        }

        #region Eventhandlers
        void fadeout_anim_Completed(object sender, EventArgs e)
        {
            if (FadeOutCompleted != null)
                FadeOutCompleted(this, new EventArgs());
        }
        void OverlayStatusItem_MouseDown(object sender, MouseButtonEventArgs e)
        {
            StartFadeOut();
        }
        private void help_button_Click(object sender, RoutedEventArgs e)
        {

        }

        private void abort_button_Click(object sender, RoutedEventArgs e)
        {
            if (AbortClicked != null)
                AbortClicked(this, new EventArgs());
        }
        #endregion

        #region Dependency Property
        public Style ConfirmButtonStyle
        {
            get { return (Style)GetValue(ConfirmButtonStyleProperty); }
            set { SetValue(ConfirmButtonStyleProperty, value); }
        }
        public static readonly DependencyProperty ConfirmButtonStyleProperty = DependencyProperty.Register("ConfirmButtonStyle", typeof(Style), typeof(OverlayStatusItem), new UIPropertyMetadata(null));

        public Style SmallButtonStyle
        {
            get { return (Style)GetValue(SmallButtonStyleProperty); }
            set { SetValue(SmallButtonStyleProperty, value); }
        }
        public static readonly DependencyProperty SmallButtonStyleProperty = DependencyProperty.Register("SmallButtonStyle", typeof(Style), typeof(OverlayStatusItem), new UIPropertyMetadata(null));
        #endregion

        #region Properties

        public bool IsVisible
        {
            get
            {
                double left = Canvas.GetLeft(base_grid);
                if(left == 0)
                    return true;
                else
                    return false;
            }
        }
        public bool Closeable
        {
            set
            {
                canvas.Cursor = null;
                MouseDown -= OverlayStatusItem_MouseDown;

                if(value)
                {
                    canvas.Cursor = Cursors.Hand;
                    MouseDown += OverlayStatusItem_MouseDown;
                }
            }
        }
        public bool WithHelpButton
        {
            set
            {
                if(value)
                {
                    help_button.Visibility = Visibility.Visible;
                }
                else
                {
                    help_button.Visibility = Visibility.Hidden;
                }
            }
        }
        public bool WithAbortButton
        {
            set
            {
                if (value)
                {
                    abort_button.Visibility = Visibility.Visible;
                }
                else
                {
                    abort_button.Visibility = Visibility.Hidden;
                }
            }
        }

        public Border Border
        {
            get
            {
                return border;
            }
        }
        #endregion

        #region Attributes
        protected delegate void NoParas();
        public string Id;
        public int TimeOutMsec;
        protected DateTime _end_timeout;
        protected QueuedBackgroundWorker _bg_worker;
        public event EventHandler FadeOutCompleted;
        public event EventHandler AbortClicked;
        #endregion
    }
}
