﻿using System.Windows;
using System.Windows.Controls;

namespace Logicx.WpfUtility.WindowManagement
{
    /// <summary>
    /// Interaction logic for TaskbarItem.xaml
    /// </summary>
    public partial class TaskbarItem : UserControl
    {
        public TaskbarItem(FrameworkElement framework_element, string title)
        {
            InitializeComponent();

            item_text_tb.Text = title;
            item_border.Child = framework_element;
        }

        public override string ToString()
        {
            return "Taskbar Item " + Title;
        }

        public Style ContainerBorderStyle
        {
            get { return (Style)GetValue(ContainerBorderStyleProperty); }
            set { SetValue(ContainerBorderStyleProperty, value); }
        }
        public static readonly DependencyProperty ContainerBorderStyleProperty = DependencyProperty.Register("ContainerBorderStyle", typeof(Style), typeof(TaskbarItem), new UIPropertyMetadata(null));

        public Style TextStyle
        {
            get { return (Style)GetValue(TextStyleProperty); }
            set { SetValue(TextStyleProperty, value); }
        }
        public static readonly DependencyProperty TextStyleProperty = DependencyProperty.Register("TextStyle", typeof(Style), typeof(TaskbarItem), new UIPropertyMetadata(null));


        public FrameworkElement FrameworkElement
        {
            get
            {
                return (FrameworkElement)item_border.Child;
            }
        }


        public string Title
        {
            get
            {
                return item_text_tb.Text;
            }
        }

        public FrameworkElement Container
        {
            get; set;
        }

        public WindowTaskBarBase<SubMenuPanel>.ItemType ItemType
        { 
            get; set;
        }
    }
}
