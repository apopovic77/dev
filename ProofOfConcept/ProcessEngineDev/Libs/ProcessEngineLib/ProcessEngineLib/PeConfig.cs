using Logicx.Utility;
using System;

namespace ProcessEngine
{
    namespace ProcessEngineLib
    {
        public class PeConfig
        {
            
        }
    }


    /// <summary>
    /// <para>If you want to create a new Configuration-File
    /// this class can be copied and renamed. </para>
    /// <para>
    /// It has
    /// all neccessary methods and tags that are used
    /// within the SettingsManagerApplication
    /// </para>
    /// </summary>
    public class PeConfig : SystemConfig
    {
        private PeConfig(string path_to_config_file)
            : base(path_to_config_file)
        {
        }

        protected override void RegisterConfigProperties()
        {
            //settingsmanager: register begin


            
            Db_DataSource.PropertyChanged += Config_PropertyChanged;
			ConfigProperties.Add("Db_DataSource", Db_DataSource);
			Db_InitialCatalog.PropertyChanged += Config_PropertyChanged;
			ConfigProperties.Add("Db_InitialCatalog", Db_InitialCatalog);
			Db_UserId.PropertyChanged += Config_PropertyChanged;
			ConfigProperties.Add("Db_UserId", Db_UserId);
			Db_Password.PropertyChanged += Config_PropertyChanged;
			ConfigProperties.Add("Db_Password", Db_Password);
			//settingsmanager: register end
        }

        protected override void ExecLoadValues()
        {
            //settingsmanager: construction begin

            
            //SettingsManager-Kommentar: no comment
			if (_config.AppSettings.Settings["Db_DataSource_value"] != null)
				Db_DataSource.Value = Convert.ToString(_config.AppSettings.Settings["Db_DataSource_value"].Value);
			if (_config.AppSettings.Settings["Db_DataSource_bez"] != null)
				Db_DataSource.Bezeichnung = _config.AppSettings.Settings["Db_DataSource_bez"].Value;
			//SettingsManager-Kommentar: no comment
			if (_config.AppSettings.Settings["Db_InitialCatalog_value"] != null)
				Db_InitialCatalog.Value = Convert.ToString(_config.AppSettings.Settings["Db_InitialCatalog_value"].Value);
			if (_config.AppSettings.Settings["Db_InitialCatalog_bez"] != null)
				Db_InitialCatalog.Bezeichnung = _config.AppSettings.Settings["Db_InitialCatalog_bez"].Value;
			//SettingsManager-Kommentar: no comment
			if (_config.AppSettings.Settings["Db_UserId_value"] != null)
				Db_UserId.Value = Convert.ToString(_config.AppSettings.Settings["Db_UserId_value"].Value);
			if (_config.AppSettings.Settings["Db_UserId_bez"] != null)
				Db_UserId.Bezeichnung = _config.AppSettings.Settings["Db_UserId_bez"].Value;
			//SettingsManager-Kommentar: no comment
			if (_config.AppSettings.Settings["Db_Password_value"] != null)
				Db_Password.Value = Convert.ToString(_config.AppSettings.Settings["Db_Password_value"].Value);
			if (_config.AppSettings.Settings["Db_Password_bez"] != null)
				Db_Password.Bezeichnung = _config.AppSettings.Settings["Db_Password_bez"].Value;
			//settingsmanager: construction end 
        }

        protected override void ExecSaveSettings()
        {
            //settingsmanager: savesettings begin

            
            //SettingsManager-Kommentar: no comment
			AddKey(_config, "Db_DataSource_value", Db_DataSource.Value.ToString());
			AddKey(_config, "Db_DataSource_bez", Db_DataSource.Bezeichnung);
			//SettingsManager-Kommentar: no comment
			AddKey(_config, "Db_InitialCatalog_value", Db_InitialCatalog.Value.ToString());
			AddKey(_config, "Db_InitialCatalog_bez", Db_InitialCatalog.Bezeichnung);
			//SettingsManager-Kommentar: no comment
			AddKey(_config, "Db_UserId_value", Db_UserId.Value.ToString());
			AddKey(_config, "Db_UserId_bez", Db_UserId.Bezeichnung);
			//SettingsManager-Kommentar: no comment
			AddKey(_config, "Db_Password_value", Db_Password.Value.ToString());
			AddKey(_config, "Db_Password_bez", Db_Password.Bezeichnung);
			//settingsmanager: savesettings end
        }


        //settingsmanager: vardef begin 

        
        public static StringConfigValue Db_DataSource = new StringConfigValue("Db_DataSource","no comment","");
		public static StringConfigValue Db_InitialCatalog = new StringConfigValue("Db_InitialCatalog","no comment","");
		public static StringConfigValue Db_UserId = new StringConfigValue("Db_UserId","no comment","");
		public static StringConfigValue Db_Password = new StringConfigValue("Db_Password","no comment","");
		//settingsmanager: vardef end 



        #region Singleton Instance
        public static void CreateInstance(string path_to_config_file)
        {
            lock (_instance_creation_lock)
            {
                _instance = new PeConfig(path_to_config_file);
            }
        }
        public static void CreateInstance()
        {
            CreateInstance(GetPathToConfigFile());
        }


        public static PeConfig Instance
        {
            get
            {
                lock (_instance_creation_lock)
                {
                    if (_instance == null)
                        throw new Exception("Instance not existing, you need to create the instance via CreateInstance()");
                    return _instance;
                }
            }
        }

        private static object _instance_creation_lock = new object();
        private static PeConfig _instance;


        public static string HangfireConnectionString = @"Server=awisrv01\awialpop;Database=Hangfire;Trusted_Connection=True;";
        #endregion

    }
}


