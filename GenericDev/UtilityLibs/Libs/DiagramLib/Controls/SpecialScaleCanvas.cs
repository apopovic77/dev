﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Input;
using System.Diagnostics;
using Windows7.Multitouch;
using System.Windows;

namespace Logicx.DiagramLib.Controls
{
    public class SpecialScaleCanvas : Canvas
    {
        private static bool? _is_multitouch_enabled = null;

        internal static bool IsMultiTouchEnabled
        {
            get
            {
                if (!_is_multitouch_enabled.HasValue)
                    _is_multitouch_enabled = TouchHandler.DigitizerCapabilities.IsMultiTouchReady;

                return _is_multitouch_enabled.Value;
            }
        }
        protected override System.Windows.Media.HitTestResult HitTestCore(System.Windows.Media.PointHitTestParameters hitTestParameters)
        {
            if (hitTestParameters.HitPoint.X <= LeftPadding)
                return null;

            // bei multitouch ist dieser canvas immer aktiv
            if(IsMultiTouchEnabled)
                return base.HitTestCore(hitTestParameters);

            if (Mouse.LeftButton == MouseButtonState.Pressed)
            {
                _was_pressed = true;
                return base.HitTestCore(hitTestParameters);
            }
            else if (Mouse.LeftButton == MouseButtonState.Released)
            {
                if (_was_pressed)
                    return base.HitTestCore(hitTestParameters);
            }

            return null;
        }

        protected override System.Windows.Media.GeometryHitTestResult HitTestCore(System.Windows.Media.GeometryHitTestParameters hitTestParameters)
        {
            if (hitTestParameters.HitGeometry.Bounds.Right <= LeftPadding)
                return null;

            // bei multitouch ist dieser canvas immer aktiv
            if (IsMultiTouchEnabled)
                return base.HitTestCore(hitTestParameters);

            if (Mouse.LeftButton == MouseButtonState.Pressed)
            {
                _was_pressed = true;
                return base.HitTestCore(hitTestParameters);
            }
            else if (Mouse.LeftButton == MouseButtonState.Released)
            {
                if (_was_pressed)
                    return base.HitTestCore(hitTestParameters);
            }

            return null;
        }

        public void ResetCaptureState()
        {
            _was_pressed = false;
        }



        public double LeftPadding
        {
            get { return (double)GetValue(LeftPaddingProperty); }
            set { SetValue(LeftPaddingProperty, value); }
        }

        // Using a DependencyProperty as the backing store for LeftPadding.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty LeftPaddingProperty =
            DependencyProperty.Register("LeftPadding", typeof(double), typeof(SpecialScaleCanvas), new UIPropertyMetadata(50d));

        

        private bool _was_pressed;
        private bool _zoom_mode;
    }
}
