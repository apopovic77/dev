﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PoCShared.Core
{
    public class Registration
    {
        /// <summary>
        /// Client pc name
        /// </summary>
        public string ClientName { get; set; }
    }
}
