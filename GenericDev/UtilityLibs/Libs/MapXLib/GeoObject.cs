using System;
using Logicx.Utilities.Database;

namespace MapX
{
    public enum LANGUAGE
    {
        deutsch = 1,
        english = 2,
        french = 3,
        greek = 4,
        croation = 5,
        yugoslavian = 6,
        czeck = 7,
        belgian = 8,
        turkish = 9,
        macedonian = 10,
        hungarian = 11,
        italian = 12,
        slovenian = 13,
        deutschOesterreich = 14
    }

	/// <summary>
	/// Summary description for GeoObject.
	/// </summary>
	[Serializable]
	public class GeoObject
	{
		/// <summary>
		/// sollte der city namen nicht �ber eine City loc abegfragt worden sein sondern �ber die region 3
		/// dann wird dieses falg auf true gesetzt.
		/// </summary>
		public bool CityNameIsRegion3 = false;

		/// <summary>
		/// phi .. x .. latitude   0� at the equator, 90� at the pole
		/// </summary>
		public double longitude = 0;
		
		/// <summary>
		/// phi .. x .. latitude   0� at the equator, 90� at the pole
		/// </summary>
		public double latitude = 0;

		/// <summary>
		/// position X given in pixel, this value always corresponds to a certain altitude
		/// </summary>
		public double x = 0;

		/// <summary>
		/// wie l�nge f�hrt man ungef�hr auf der strecke
		/// </summary>
		public double drivetime=0;

		/// <summary>
		/// position Y given in pixel, this value always corresponds to a certain altitude
		/// </summary>
		public double y = 0;

		/// <summary>
		/// altitude from where obj is being observed
		/// </summary>
		public double altitude = 0;

		/// <summary>
		/// size in miles or km dependent on unit of a pixel
		/// </summary>
		public double pixelsize = 0;

		/// <summary>
		/// if this obj was instantiated by a route calc made by mappoint, then the exact distance of the route is set here
		/// </summary>
		public double distance = 0;

		/// <summary>
		/// name of the location that this geoobj is pointing to
		/// </summary>
		public string name = "";
		public string state = "";
		public string region2 = "";
		public string city = "";
		public string country = "";
		public string country_code = "";
		public string state_code = "";
		public uint zip = 0;

		/// <summary>
		/// data is stored with the given language
		/// </summary>
		public LANGUAGE language = LANGUAGE.english;

		/// <summary>
		/// geoUnits of obj
		/// </summary>
		public MapPoint.GeoUnits units = MapPoint.GeoUnits.geoKm;

		public GeoObject(){}



		public GeoObject(double longitude,double latitude){
			this.longitude = longitude;
			this.latitude = latitude;
		}

		public GeoObject(long x, long y)
		{
			this.x = x;
			this.y = y;
		}

		public void SetLanguage(DBConnection db, LANGUAGE language){
			this.language = language;
			//Try to change the country name into the given languange
			try{
				if(country_code == ""){
					if(country != "")
						country_code = db.ExecuteQueryScalar("select country_code from country where country_name = '"+country+"'").ToString();
					else
						throw new Exception("dummy");
				}
				country = db.ExecuteQueryScalar("select country_name from country where country_code = '"+ country_code+"' and country_language_id = "+language).ToString();
			}catch{}

			//Try to change the state name into the given languange
			try{
				if(state_code == ""){
					if(state != "")
						state_code = db.ExecuteQueryScalar("select state_code from state where state_name = '"+state+"'").ToString();
					else
						throw new Exception("dummy");
				}
				string query = "select state_name from state where state_code = '"+state_code+"' and state_country_code = '"+country_code+"' and state_language_id = "+(uint)language;
				state = db.ExecuteQueryScalar(query).ToString();
			}catch{}
		}

		public void Insert(DBConnection db){
			//check for country_code, is needed when inserting
			if(country_code == "" && country != "")
				country_code = db.ExecuteQueryScalar("select country_code from country where country_name = '"+country+"'").ToString();
			else if(country_code == "")
				return;
			//check for state_code, is needed when inserting
			if(state_code == "" && state != "")
				state_code = db.ExecuteQueryScalar("select state_code from state where state_name = '"+state+"'").ToString();
			else if(state_code == "")
				return;
			//get spoken language id of the country
			string spoken_language = db.ExecuteQueryScalar("select country_spokenlanguage_id from country where country_code = '"+country_code+"'").ToString();
			//zip code is not necessarily needen when insertinger, therefor null if not specified
			string zipCode = "null"; if(zip!=0) zipCode = zip.ToString();
			//create dml stmt and execute
			string dml = "";
			dml += "insert into "+country_code+" values (";
			dml += "null,'"+city.ToUpper().Trim().Replace(" ","")+"','"+city+"','"+city+"',"+zipCode+",'"+state_code+"',"+latitude.ToString().Replace(",",".")+","+longitude.ToString().Replace(",",".")+",'auin','auto_in','"+country_code+"',"+spoken_language; 
			dml += ");";
			db.Execute(dml);
		}

		public override string ToString(){
			string ret = "GeoObject Data of >"+name+"< in language >"+language+"<\n";
			ret += "state: "+state+"\n";
			ret += "state_code: "+state_code+"\n";
			ret += "region2: "+region2+"\n";
			ret += "city: "+city+"\n";
			ret += "country: "+country+"\n";
			ret += "country code: "+country_code+"\n";
			ret += "x: "+x+"\n";
			ret += "y: "+y+"\n";
			ret += "longitude: "+longitude+"\n";
			ret += "latitude:  "+latitude+"\n";
			ret += "altitude:  "+altitude+"\n";
			ret += "pixelsize: "+pixelsize+"\n";
			ret += "distance: "+distance+"\n";
			return ret;
		}
	}
}
