﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BankLib.Data;
using BankLib.Gruppe;

namespace BankOverviewApp
{
    public enum SortCol
    {
        Datum,
        Betrag,
        BuchungsType,
        UmsatzText
    }

    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            int b_loaded = Buchungen.LoadBuchungenFromFile();

            //List<Buchung> bs = Buchungen.AlleBuchungen.Where(b => b.UmsatzTextRaw.IndexOf("Johanna") > 0).ToList();


            BuchungsGruppen.LoadGruppenFromFile();

            BaseGrid.DataContext = Buchungen.AlleBuchungen;

            KontenOverview kto_overview_all = new KontenOverview();
            top_stack.Children.Add(kto_overview_all);



            AddCopyCommandToGridView();

            //add log info
            log_tb.Text = AppHelper.ReadLogFile();

            Gruppenzuordnung gz = new Gruppenzuordnung();
            gz.HorizontalAlignment = HorizontalAlignment.Stretch;
            gz.VerticalAlignment = VerticalAlignment.Stretch;
            groupadd_grid.Children.Add(gz);

            GruppenCreateParser gcp = new GruppenCreateParser(gz);
            groupoverview_grid.Children.Add(gcp);

            Gruppen gp = new Gruppen(gcp, gz);
            gruppen_erstellung_grid.Children.Add(gp);

            GroupedOverview overv = new GroupedOverview();
            overview_grid.Children.Add(overv);

            
            filter_ctrl = new FilterControl();
            filter_ctrl.FilterSortChanged += FilterControl_OnFilterSortChanged;
            buchung_grid.Children.Add(filter_ctrl);

        }


        void AddCopyCommandToGridView()
        {
            ExecutedRoutedEventHandler handler = (sender_, arg_) => { CopySelectedCells(); };
            var command = new RoutedCommand("Copy", typeof(GridView));
            command.InputGestures.Add(new KeyGesture(Key.C, ModifierKeys.Control, "Copy"));
            buchungen_listview.CommandBindings.Add(new CommandBinding(command, handler));
            try
            { System.Windows.Clipboard.SetData(DataFormats.Text, ""); }
            catch (COMException)
            { }
        }

        private void CopySelectedCells()
        {
            if (buchungen_listview.SelectedItems.Count != 0)
            {
                var sb = new StringBuilder();

                foreach (Buchung b in buchungen_listview.SelectedItems)
                    sb.AppendLine(b.BuchungsDatum.ToShortDateString()+"\t"+b.BuchungsDatum.ToShortDateString()+"\t"+b.ValutaDatum.ToShortDateString()+"\t"+b.BuchungsType+"\t"+b.UmsatzType+"\t"+b.Betrag);

                try
                { 
                    Clipboard.SetData(DataFormats.Text, sb.ToString());
                }
                catch (COMException)
                { 
                    MessageBox.Show("Sorry, unable to copy to the clipboard. Try again.");
                }
            }
        }



        private void FilterControl_OnFilterSortChanged(object sender, EventArgs e)
        {
            FilterSortGrid();
        }

        private void FilterSortGrid()
        {
            switch (_sort_col)
            {
                case SortCol.Datum:
                    _sort_datum_asc = !_sort_datum_asc;
                    Datum_Click(null, null);
                    break;
                case SortCol.BuchungsType:
                    _sort_buchungstype_asc = !_sort_buchungstype_asc;
                    BuchungsType_Click(null, null);
                    break;
                case SortCol.Betrag:
                    _sort_betrag_asc = !_sort_betrag_asc;
                    Betrag_Click(null, null);
                    break;
                case SortCol.UmsatzText:
                    _sort_umsatztext_asc = !_sort_umsatztext_asc;
                    UmsatzText_Click(null, null);
                    break;
                //case SortCol.ValutaDatum:
                //    _sort_valutadatum_asc = !_sort_valutadatum_asc;
                //    ValutaDatum_Click(null, null);
                //    break;
                default:
                    throw new Exception("not implemented sort col value");
            }
        }

        private void Datum_Click(object sender, RoutedEventArgs e)
        {
            _sort_col = SortCol.Datum;
            _sort_datum_asc = !_sort_datum_asc;
            if(_sort_datum_asc)
                buchungen_listview.ItemsSource = filter_ctrl.GetFilteredBuchungen().OrderBy(b => b.BuchungsDatum).ThenBy(b => b.Datum).ToList();
            else
                buchungen_listview.ItemsSource = filter_ctrl.GetFilteredBuchungen().OrderByDescending(b => b.BuchungsDatum).ThenByDescending(b => b.Datum).ToList();
        }
        //private void ValutaDatum_Click(object sender, RoutedEventArgs e)
        //{
        //    _sort_col = SortCol.ValutaDatum;
        //    _sort_valutadatum_asc = !_sort_valutadatum_asc;
        //    if (_sort_valutadatum_asc)
        //        buchungen_listview.ItemsSource = GetFilteredBuchungen().OrderBy(b => b.ValutaDatum).ThenBy(b=>b.BuchungsDatum).ToList();
        //    else
        //        buchungen_listview.ItemsSource = GetFilteredBuchungen().OrderByDescending(b => b.ValutaDatum).ThenByDescending(b=>b.BuchungsDatum).ToList();
        //}
        private void BuchungsType_Click(object sender, RoutedEventArgs e)
        {
            _sort_col = SortCol.BuchungsType;
            _sort_buchungstype_asc = !_sort_buchungstype_asc;
            if (_sort_buchungstype_asc)
                buchungen_listview.ItemsSource = filter_ctrl.GetFilteredBuchungen().OrderBy(b => b.BuchungsType).ThenByDescending(b => b.ValutaDatum).ToList();
            else
                buchungen_listview.ItemsSource = filter_ctrl.GetFilteredBuchungen().OrderByDescending(b => b.BuchungsType).ThenByDescending(b => b.ValutaDatum).ToList();
        }
        private void Betrag_Click(object sender, RoutedEventArgs e)
        {
            _sort_col = SortCol.Betrag;
            _sort_betrag_asc = !_sort_betrag_asc;
            if (_sort_betrag_asc)
                buchungen_listview.ItemsSource = filter_ctrl.GetFilteredBuchungen().OrderBy(b => b.Betrag).ThenByDescending(b => b.ValutaDatum).ToList();
            else
                buchungen_listview.ItemsSource = filter_ctrl.GetFilteredBuchungen().OrderByDescending(b => b.Betrag).ThenByDescending(b => b.ValutaDatum).ToList();
        }
        private void UmsatzText_Click(object sender, RoutedEventArgs e)
        {
            _sort_col = SortCol.UmsatzText;
            _sort_umsatztext_asc = !_sort_umsatztext_asc;
            if (_sort_umsatztext_asc)
                buchungen_listview.ItemsSource = filter_ctrl.GetFilteredBuchungen().OrderBy(b => b.UmsatzType).ThenByDescending(b => b.ValutaDatum).ToList();
            else
                buchungen_listview.ItemsSource = filter_ctrl.GetFilteredBuchungen().OrderByDescending(b => b.UmsatzType).ThenByDescending(b => b.ValutaDatum).ToList();
        }

        private bool _sort_datum_asc;
        private bool _sort_valutadatum_asc;
        private bool _sort_betrag_asc;
        private bool _sort_buchungstype_asc;
        private bool _sort_umsatztext_asc;

        private SortCol _sort_col = SortCol.Datum;
        private FilterControl filter_ctrl;

    }
}
