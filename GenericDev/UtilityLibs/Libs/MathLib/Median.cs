using System;
using System.Collections.Generic;
using System.Text;

namespace MathLib
{
    public class Median
    {
        public Median() {}

		public List<float> Values
		{
			set {
				_values = value;
			}
			
			get {
				return _values;
			}
		}

        public float[] ValuesAlreadySorted
        {
            set
            {
                _values_arr = value;
            }

            get
            {
                return _values_arr;
            }
        }

        public float GetMedian(){
            if (_values.Count == 0 && _values_arr == null)
                return 0;

            if (_values_arr != null)
                return GetMedian(_values_arr);
            
			if(_values.Count == 1)
				return _values[0];

            //Asc Sorting
            _values.Sort();

            if (_values.Count % 2 == 0)
            {
                if (_values.Count == 2)
                    return (_values[0] + _values[1]) / 2;
                else
                    return (0.5f * (_values[_values.Count / 2] + _values[_values.Count / 2 + 1]));
            }
            else
                return _values[(_values.Count + 1) / 2];
        }

        private float GetMedian(float[] values_arr)
        {
            if (values_arr.Length == 1)
                return values_arr[0];


            if (values_arr.Length % 2 == 0)
            {
                if (values_arr.Length == 2)
                    return (values_arr[0] + values_arr[1]) / 2;
                else
                    return (0.5f * (values_arr[values_arr.Length / 2] + values_arr[values_arr.Length / 2 + 1]));
            }
            else
                return values_arr[(values_arr.Length + 1) / 2];            
        }
		

        protected List<float> _values = new List<float>();
        protected float[] _values_arr;
		

    }
}
