﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Shapes;
using Logicx.DiagramLib.Controls;
using Logicx.DiagramLib.Util;
using Logicx.DiagramLib.ViewConfig;

namespace Logicx.DiagramLib.Scales
{

    public class TimeScaleControl : TypedScaleControl<DateTime>
    {
        #region Constants
        #endregion
        #region Construction and Initialization
        public TimeScaleControl()
        {
            _scale_unit_small = new TimeSpan(0, 30, 0);
        }

        public TimeScaleControl(AxisScaleConfig<DateTime> config)
            : this()
        {
            Config = config;
        }

        #endregion

        #region Operations

        private void Init(bool generate_always)
        {
            ScaleType current_scale = ScaleFactory.GetInstance().GetScale(Config);

            if (current_scale == null)
                throw new ArgumentException("No ScaleType available for the given time range " +
                                            Config.Start + " " + Config.End);

            _current_scale = current_scale;
            _current_scale.Control = this;

            double size = Config != null && Config.Orientation == Orientation.Horizontal ? _render_width : _render_height;
            if (size != 0)
                _current_scale.Init(size, generate_always);
        }

        public double GetRenderWidth()
        {
            return _render_width;
        }

        public double GetRenderHeight()
        {
            return _render_height;
        }

        public void ClearChildren()
        {
            foreach (var child in ScaleCanvas.Children)
            {
                if (child is Rectangle)
                    BindingOperations.ClearBinding((Rectangle)child, Rectangle.FillProperty);
                else if (child is TextBlock)
                    BindingOperations.ClearBinding((TextBlock)child, TextBlock.StyleProperty);
            }

            ScaleCanvas.Children.Clear();
        }

        public void AddToChildren(List<UIElement> elements)
        {
            foreach (var ui_element in elements)
            {
                ScaleCanvas.Children.Add(ui_element);
            }
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            Children.Clear();

            if (_config != null)
            {
                _config.PropertyChanged -= _config_PropertyChanged;
                _config.Dispose();
            }

            if (_current_scale != null)
                _current_scale.Dispose();

            _current_scale = null;
        }

        private void AlignData()
        {
            double size = Config != null && Config.Orientation == Orientation.Horizontal ? _render_width : _render_height;
            if (size != 0)
                _current_scale.AlignData(Config.Orientation);
        }

        public override double TransformAxisValueToPixel(DateTime value)
        {
            return TransformAxisValueToPixel(value, false);
        }

        public override double TransformAxisValueToPixel(DateTime value, bool margin_sensitive)
        {
            DateTime start = Config.Start;
            DateTime end = Config.End;
            TimeSpan wholespan = end - start;

            if (Config.Orientation == Orientation.Horizontal)
            {
                TimeSpan diff = value - start;
                return (margin_sensitive ? Config.Padding.Left : 0) + _render_width * ((double)diff.Ticks / wholespan.Ticks);
            }
            else
            {
                TimeSpan diff = end - value;
                return (margin_sensitive ? Config.Padding.Top : 0) + _render_height * ((double)diff.Ticks / wholespan.Ticks);
            }
        }

        public override DateTime TransformAxisPixelToValue(double pixel)
        {
            return TransformAxisPixelToValue(pixel, false);
        }

        public override DateTime TransformAxisPixelToValue(double pixel, bool margin_sensitive)
        {
            DateTime start = Config.Start;
            DateTime end = Config.End;
            TimeSpan wholespan = end - start;

            double scale_offset = _current_scale.GetUnitOffset(); // required because month scale, for example, only shows days and should start at 0:00
            //Debug.WriteLine("Unit OFFSET in TransformAxisPixelToValue: " + scale_offset);
            if (Config.Orientation == Orientation.Horizontal)
            {
                return start.AddTicks((long)(wholespan.Ticks * (_render_width == 0 ? 0 : ((pixel - (margin_sensitive ? Config.Padding.Left : 0) - scale_offset) / _render_width))));
            }

            return start.AddTicks((long)(wholespan.Ticks * (_render_height == 0 ? 0 : ((_render_height + Config.Padding.Top - (pixel - (margin_sensitive ? Config.Padding.Top : 0)) - scale_offset) / _render_height))));
        }

        public override double GetUnitSize()
        {
            return _current_scale.GetUnitSize(Config.Orientation);
        }

        public override List<double> GetGridLinePositions()
        {
            if (_current_scale == null)
                Init(true);

            return _current_scale.GetGridLinePositions();
        }

        public override double GetScaleStartPixel()
        {
            if (Config.Orientation == Orientation.Horizontal)
                return Config.Padding.Left + _current_scale.GetUnitOffset(Config.Orientation);

            return Config.Padding.Top + _current_scale.GetUnitOffset(Config.Orientation);
        }

        public override double GetScaleEndPixel()
        {
            if (Config.Orientation == Orientation.Horizontal)
                return this.ActualWidth - Config.Padding.Right;

            return this.ActualHeight - Config.Padding.Bottom;
        }

        #endregion

        #region Event Handlers

        void TimeScaleCanvasChanged()
        {
            _render_width = ActualWidth;
            _render_height = ActualHeight;

            //Debug.WriteLine(" Timescalecontrol Actualwidth = " + ActualWidth + ", renderwidth = " + _render_width);
            if (_render_width >= Config.Padding.Left + Config.Padding.Right)
                _render_width -= Config.Padding.Left + Config.Padding.Right;

            if (_render_height >= Config.Padding.Top + Config.Padding.Bottom)
                _render_height -= Config.Padding.Top + Config.Padding.Bottom;

            InitAndAlign(true);
        }

        protected override void OnScaleSizeChanged(SizeChangedEventArgs e)
        {
            if (Config == null)
                return;

            if (e.WidthChanged)
            {
                _render_width = e.NewSize.Width;
                //if (_render_width >= Config.Padding.Left + Config.Padding.Right)
                //    _render_width -= Config.Padding.Left + Config.Padding.Right;
            }

            if (e.HeightChanged)
            {
                _render_height = e.NewSize.Height;
                //if (_render_height >= Config.Padding.Top + Config.Padding.Bottom)
                //    _render_height -= Config.Padding.Top + Config.Padding.Bottom;
            }
            //Debug.WriteLine(" Timescalecontrol ScaleSizeChanged Actualwidth = " + ActualWidth + ", renderwidth = " + _render_width);

            InitAndAlign(e.WidthChanged || e.HeightChanged);
        }

        protected override void InitAndAlign(bool generate_always)
        {
            if (Config != null)
                Padding = Config.Padding;

            Init(generate_always);

            AlignData();

            OnScalingChanged();
        }

        void _config_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            //Debug.WriteLine("Timescalecontrol PROPERTY CHANGED '" + e.PropertyName + "' : start=" + Config.Start + ", end=" + Config.End);
            if (e.PropertyName == "Start")
                TimeScaleCanvasChanged();
            else if (e.PropertyName == "End")
                TimeScaleCanvasChanged();
            else if (e.PropertyName == "Description")
                TimeScaleCanvasChanged();
            else if (e.PropertyName == "Orientation")
                TimeScaleCanvasChanged();
            else if (e.PropertyName == "Padding")
                TimeScaleCanvasChanged();
            else if (e.PropertyName == "Alignment")
                ;

        }
        #endregion

        #region Properties

        public override AxisScaleConfig<DateTime> Config
        {
            get
            {
                return _config;
            }
            set
            {
                if (_config != null)
                    _config.PropertyChanged -= _config_PropertyChanged;

                _config = value;

                _config.PropertyChanged += _config_PropertyChanged;

                _current_scale = ScaleFactory.GetInstance().GetScale(_config);

                if (_current_scale == null)
                    throw new ArgumentException("No ScaleType available for the given time range " +
                                                Config.Start + " " + Config.End);

                _current_scale.Control = this;
            }
        }

        public override bool CanKeyMove
        {
            get { return true; }
        }

        public override double ScaleUnitSmallInPixel
        {
            get
            {
                if (_config != null)
                {
                    double first = TransformAxisValueToPixel(_config.Start);
                    double second = TransformAxisValueToPixel(_config.Start.AddMilliseconds(_scale_unit_small.TotalMilliseconds));

                    return Math.Abs(second - first);
                }

                return 0;
            }
        }

        #endregion

        #region Dependency Properties
        #endregion

        #region Events
        #endregion

        #region Routed Events
        #endregion

        #region Attributes

        private ScaleType _current_scale;

        private double _render_width;
        private double _render_height;
        private AxisScaleConfig<DateTime> _config;

        private readonly TimeSpan _scale_unit_small;

        #endregion

        #region Tests
        #endregion

    }
}
