﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Awanto.WhatsAppCore.Migrations
{
    public partial class InitialDb : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "EventLogEntries",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedTime = table.Column<DateTime>(nullable: true),
                    ErstelltAm = table.Column<DateTime>(nullable: false),
                    ErstelltVon = table.Column<string>(nullable: true),
                    EventId = table.Column<int>(nullable: true),
                    GeaendertAm = table.Column<DateTime>(nullable: false),
                    GeaendertVon = table.Column<string>(nullable: true),
                    LogLevel = table.Column<string>(nullable: true),
                    Message = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EventLogEntries", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Medien",
                columns: table => new
                {
                    Annotation = table.Column<string>(nullable: true),
                    IsText = table.Column<bool>(nullable: true),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Discriminator = table.Column<string>(nullable: false),
                    ErstelltAm = table.Column<DateTime>(nullable: false),
                    ErstelltVon = table.Column<string>(nullable: true),
                    GeaendertAm = table.Column<DateTime>(nullable: false),
                    GeaendertVon = table.Column<string>(nullable: true),
                    PathToMedia = table.Column<string>(maxLength: 255, nullable: true),
                    Referenz = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Medien", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Nachrichten",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AbsenderAdresse = table.Column<string>(nullable: false),
                    AntwortAufId = table.Column<int>(nullable: true),
                    Body = table.Column<string>(nullable: false),
                    Discriminator = table.Column<string>(nullable: false),
                    EmpfaengerAdresse = table.Column<string>(nullable: false),
                    ErstelltAm = table.Column<DateTime>(nullable: false),
                    ErstelltVon = table.Column<string>(nullable: true),
                    GeaendertAm = table.Column<DateTime>(nullable: false),
                    GeaendertVon = table.Column<string>(nullable: true),
                    Subject = table.Column<string>(nullable: true),
                    _RawGesendetAm = table.Column<string>(nullable: true),
                    _RawKommunikationsKanaele = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Nachrichten", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Nachrichten_Nachrichten_AntwortAufId",
                        column: x => x.AntwortAufId,
                        principalTable: "Nachrichten",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "NachrichtMedium",
                columns: table => new
                {
                    NachrichtId = table.Column<int>(nullable: false),
                    MediumId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NachrichtMedium", x => new { x.NachrichtId, x.MediumId });
                    table.ForeignKey(
                        name: "FK_NachrichtMedium_Medien_MediumId",
                        column: x => x.MediumId,
                        principalTable: "Medien",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_NachrichtMedium_Nachrichten_NachrichtId",
                        column: x => x.NachrichtId,
                        principalTable: "Nachrichten",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Nachrichten_AntwortAufId",
                table: "Nachrichten",
                column: "AntwortAufId");

            migrationBuilder.CreateIndex(
                name: "IX_NachrichtMedium_MediumId",
                table: "NachrichtMedium",
                column: "MediumId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "EventLogEntries");

            migrationBuilder.DropTable(
                name: "NachrichtMedium");

            migrationBuilder.DropTable(
                name: "Medien");

            migrationBuilder.DropTable(
                name: "Nachrichten");
        }
    }
}
