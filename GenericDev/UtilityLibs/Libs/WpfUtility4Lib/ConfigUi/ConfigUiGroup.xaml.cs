﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Logicx.Utility;

namespace Logicx.WpfUtility4.ConfigUi
{
    /// <summary>
    /// Interaction logic for ConfigUiGroup.xaml
    /// </summary>
    public partial class ConfigUiGroup : UserControl
    {
        public ConfigUiGroup(string system_config_name, string property_group_name)
        {
            InitializeComponent();
            
            SystemConfigName = system_config_name;
            ProperyGroupName = property_group_name;

            title.Text = " > " + ProperyGroupName.ToUpper();
        }

        public void AddConfigProperty(BoolConfigValue config_value)
        {
            CheckBox cbx = new CheckBox();
            cbx.Style = (Style)FindResource("config_chkbx_style");
            cbx.Content = config_value.Bezeichnung.ToUpper();

            SetUiElem(config_value, cbx, CheckBox.IsCheckedProperty);
        }

        public void AddConfigProperty(FloatConfigValue config_value)
        {
            if (!config_value.HasMinMaxValues)
            {
                Stringx text_box = new Stringx();
                text_box.Bezeichnung = config_value.Bezeichnung;
                SetUiElem(config_value, text_box, TextBox.TextProperty);
            }
            else
            {
                Sliderx sliderx = new Sliderx();
                sliderx.Bezeichnung = config_value.Bezeichnung;
                sliderx.Minimum = config_value.Minimum;
                sliderx.Maximum = config_value.Maximum;
                SetUiElem(config_value, sliderx, Sliderx.ValueProperty);
            }
        }

        public void AddConfigProperty(IntConfigValue config_value)
        {
            if (!config_value.HasMinMaxValues)
            {
                Stringx text_box = new Stringx();
                text_box.Bezeichnung = config_value.Bezeichnung;
                SetUiElem(config_value, text_box.tbx, TextBox.TextProperty);
            }
            else
            {
                Sliderx sliderx = new Sliderx();
                sliderx.Bezeichnung = config_value.Bezeichnung;
                sliderx.Minimum = config_value.Minimum;
                sliderx.Maximum = config_value.Maximum; 
                SetUiElem(config_value, sliderx, Sliderx.ValueProperty);
            }
        }

        public void AddConfigProperty(Vector2FloatConfigValue config_value)
        {
            DoubleSliderx dsliderx = new DoubleSliderx();
            dsliderx.Minimum = config_value.Minimum;
            dsliderx.Maximum = config_value.Maximum;
            dsliderx.Bezeichnung = config_value.Bezeichnung;
            dsliderx.Margin = new Thickness(0, 2, 0, 2);
            stack.Children.Add(dsliderx);

            Binding binding_v1 = new Binding("ValueStart");
            binding_v1.Source = config_value; binding_v1.Mode = BindingMode.TwoWay;
            Binding binding_v2 = new Binding("ValueEnd");
            binding_v2.Source = config_value; binding_v2.Mode = BindingMode.TwoWay;
            dsliderx.SetBinding(DoubleSliderx.ValueStartProperty, binding_v1);
            dsliderx.SetBinding(DoubleSliderx.ValueEndProperty, binding_v2);
            
        }

        public void AddConfigProperty(StringConfigValue config_value)
        {
            Stringx text_box = new Stringx();
            text_box.Bezeichnung = config_value.Bezeichnung;
            SetUiElem(config_value, text_box, TextBox.TextProperty);
        }

        private void SetUiElem(ConfigValue config_value, FrameworkElement ui_elem, DependencyProperty dp)
        {
            Binding binding = new Binding("Value");
            binding.Source = config_value;
            binding.Mode = BindingMode.TwoWay;
            ui_elem.SetBinding(dp, binding);
            ui_elem.Margin = new Thickness(0,2,0,2);
            stack.Children.Add(ui_elem);
        }

        private void SetUiElem(ConfigValue config_value, Stringx ui_elem, DependencyProperty dp)
        {
            Binding binding = new Binding("Value");
            binding.Source = config_value;
            binding.Mode = BindingMode.TwoWay;
            ui_elem.tbx.SetBinding(dp, binding);
            ui_elem.Margin = new Thickness(0, 2, 0, 2);
            stack.Children.Add(ui_elem);
        }


        public string SystemConfigName;
        public string ProperyGroupName;

    }
}
