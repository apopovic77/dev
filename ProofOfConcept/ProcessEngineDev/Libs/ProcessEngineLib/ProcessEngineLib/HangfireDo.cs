﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Hangfire;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ProcessEngine;

namespace ProcessEngineLib
{
    public abstract class HangfireDo : Do
    {
        public HangfireDo()
        {
        }

        public virtual HangfireDo DoInternal(params object[] parameters_objarr)
        {
            JObject jobject = (JObject)parameters_objarr[parameters_objarr.Length - 1];
            HangfireDo deserialized_instance = (HangfireDo)jobject.ToObject(this.GetType());

            deserialized_instance.Do(parameters_objarr);
            return deserialized_instance;
        }

        public virtual object Do(params object[] parameters_objarr)
        {
            _do_execution_time = DateTime.Now;
            Console.WriteLine("Debug[" + Name + "] Start At " + _do_execution_time.ToString());
            object res = DoImpl();
            return res;
        }


        public abstract object DoImpl();

        public virtual void RunBg(params object[] parameters_objarr)
        {
            _run_creation_time = DateTime.Now;

            GlobalConfiguration.Configuration
            .UseColouredConsoleLogProvider()
            .UseSqlServerStorage(PeConfig.HangfireConnectionString);

            //BackgroundJob.Enqueue(() => Console.WriteLine("Fire-and-forget " + DateTime.Now.ToString()));
            //BackgroundJob.Enqueue<HangfireDo>(t => t.Do());

            MethodInfo[] methods = typeof(BackgroundJob).GetMethods().Where(m => m.Name == "Enqueue" && m.IsGenericMethod).ToArray();
            MethodInfo method = methods[0];
            MethodInfo generic_enqueue = method.MakeGenericMethod(this.GetType());
            MethodInfo methodGetExpression = typeof(HangfireDo).GetMethod("GetExpression");
            MethodInfo generic_methodGetExpression = methodGetExpression.MakeGenericMethod(this.GetType());

            var param = generic_methodGetExpression.Invoke(this, new object[] { parameters_objarr });
            generic_enqueue.Invoke(this, new[] { param });
         }

        public Expression<Action<T>> GetExpression<T>(params object[] parameters_objarr)
            where T : HangfireDo
        {
            List<object> list = parameters_objarr.ToList();
            list.Add(this);
            return x => DoInternal(list.ToArray());
        }

        public DateTime RunCreationTime
        {
            get { return _run_creation_time; }
        }

        public DateTime DoExecutionTime
        {
            get { return _do_execution_time; }
        }

        #region Attributes
        public string Name;
        [JsonProperty]
        private DateTime _run_creation_time;
        [JsonProperty]
        private DateTime _do_execution_time;
        #endregion

    }

    public abstract class ProcDo : HangfireDo
    {
        public override object Do(params object[] parameters_objarr)
        {
            for (int i = 0; i < parameters_objarr.Length - 1; i++)
            {
                In.Add("para_" + i, parameters_objarr[0]);
            }

            object res= base.Do(parameters_objarr);

            Out.Add("res", res);

            Console.WriteLine("Debug[" + Name + "] Out=" + Out["res"]);


            if (_thendo != null && _thendo.Count > 0)
            {
                for (int i = 0; i < _thendo.Count; i++)
                {
                    JObject jobject = (JObject)_thendo[i];
                    string thendo_type_s = _thendo_types[i];
                    Type thendo_type = Type.GetType(thendo_type_s);

                    ProcDo then_do = (ProcDo)jobject.ToObject(thendo_type);


                    if (then_do.In.ContainsKey("In"))
                        then_do.In.Add("In", null);
                    then_do.In["In"] = this.Out["res"];

                    then_do.Do();

                    this.Out["res"] = then_do.Out["res"];
                }
            }

            return res;
        }

        public void ThenDo(ProcDo dohf)
        {
            if (_thendo == null)
            {
                _thendo = new List<object>(2);
                _thendo_types = new List<string>(2);
            }

            _thendo.Add(dohf);
            _thendo_types.Add(dohf.GetType().ToString());
        }

        public object DefaultIn
        {
            get
            {
                if (In.ContainsKey("In"))
                    return In["In"];
                else
                    return null;
            }
        }

        [JsonProperty]
        private List<object> _thendo;
        [JsonProperty]
        private List<string> _thendo_types;

        public Dictionary<string, object> In = new Dictionary<string, object>(2);
        public Dictionary<string, object> Out = new Dictionary<string, object>(2);
    }

    public class AddDo : ProcDo
    {
        public AddDo()
        {
            Name = "AddDo";
        }

        public AddDo(params int[] to_add) : this()
        {
            ToAddValues = to_add;
        }

        public override object DoImpl()
        {
            return ToAddValues.Sum();
        }

        public int[] ToAddValues;

    }

    public class SubtractDo : ProcDo
    {
        public SubtractDo()
        {
            Name = "SubtractorDo";
        }

        public SubtractDo(params int[] to_subtract) : this()
        {
            ToSubstractValues = to_subtract;
        }

        public override object DoImpl()
        {
            return GetInValue() - ToSubstractValues.Sum();
        }

        protected int GetInValue()
        {
            int value = 0;
            if (DefaultIn != null)
            {
                try
                {
                    value = Convert.ToInt32(DefaultIn);
                }
                catch { }
            }
            return value;
        }

        public int[] ToSubstractValues;

    }

}
