﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GeneticProgrammingLib
{
    public class RandomChildrenCount
    {
        
        public static int Generate()
        {
            float pick_value = (float)_r.NextDouble();
            float local_sum = 0;
            for (int i = 0; i < _propability.Length; i++)
            {
                local_sum += _propability[i];
                if (local_sum >= pick_value)
                    return i+1;
            }

            return _propability.Length;
        }

        //definiere die wahrscheinlichkeitsverteilung für die anzahl der kinder von einem kind bis 5 kinder
        private static float[] _propability = new float[] {0.6f, 0.3f, 0.07f, 0.25f, 0.005f};
        private static Random _r = new Random();
    }
}
