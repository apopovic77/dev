using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Collections;

namespace Logicx.Utilities
{
    public class ConsoleDebugWriter : IDebugWriter
    {

        protected ArrayList m_msgs = new ArrayList();
        protected bool _withdateinfo = false;


        public int MaxMessagesToShow = 19;

        public ConsoleDebugWriter()
        {
            AutoFlush = true;
        }

        public bool AutoFlush { get; set; }

        public bool WithDateInfo
        {
            set { _withdateinfo = value; }
        }

        public virtual void WriteInfo(string msg)
        {
            if (_withdateinfo)
                msg = DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() + ": " + msg;
            Console.WriteLine(msg);
        }
    }
}
