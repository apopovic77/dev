﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace Logicx.DiagramLib.Util
{
    public class DateTimeUtil
    {

        private const int ONE_OUR_MSEC = 3600 * 1000; // * 10000000;
        private const int ONE_DAY_MSEC = 24 * ONE_OUR_MSEC;


        /// <summary>
        /// Determines whether the given date_time is a value on 0:00:00.
        /// </summary>
        /// <param name="date_time"></param>
        /// <returns></returns>
        public static bool IsDayChange(DateTime date_time)
        {
            TimeSpan time_of_day = date_time.TimeOfDay;
            int hours = time_of_day.Hours;

            return hours == 0;
        }

        public static double GetHoursToDayChange(DateTime date_time)
        {
            TimeSpan time_of_day = date_time.TimeOfDay;

            DateTime add_days = date_time.Subtract(time_of_day).AddDays(1);
            double hours_to_day_change = (add_days - GetFullHour(date_time)).TotalMilliseconds / 3600000;

            return hours_to_day_change;
        }

        public static DateTime GetFullHour(DateTime date_time)
        {
            DateTime newtime = date_time.Subtract(date_time.TimeOfDay).AddHours(date_time.Hour);
            return newtime;
        }

        public static DateTime GetFullMonth(DateTime date_time)
        {
            DateTime newtime = GetFullHour(date_time);
            newtime.AddMonths(-1 * date_time.Month);

            return newtime;
        }

        public static double GetHoursToEnd(DateTime date_time, DateTime end)
        {
            return (end - date_time).TotalHours;
        }

        public static double GetMSecOfHour(DateTime date_time)
        {
            DateTime day = new DateTime(date_time.Year, date_time.Month, date_time.Day, date_time.Hour, 0, 0);
            TimeSpan span = date_time.Subtract(day);

            return span.TotalMilliseconds;
        }

        public static bool IsMonthChange(DateTime date_time)
        {
            DateTime last_day = date_time.AddDays(-1);


            return last_day.Month != date_time.Month;
        }
        public static double GetDaysToMonthChange(DateTime date_time)
        {
            int month;
            int year;
            if (date_time.Month == 12)
            {
                year = date_time.Year + 1;
                month = 1;
            }
            else
            {
                year = date_time.Year;
                month = date_time.Month + 1;
            }
            DateTime next_month_start = new DateTime(year, month, 1);
            TimeSpan time_of_month = next_month_start - date_time;

            return time_of_month.Days;
        }

        public static double GetDaysToEndChange(DateTime date_time, DateTime end_date)
        {
            return (end_date - date_time).TotalDays;
        }

        /// <summary>
        /// Determines whether the given date_time is a value on 0:00:00.
        /// </summary>
        /// <param name="date_time"></param>
        /// <returns></returns>
        public static bool IsYearChange(DateTime date_time)
        {
            int month = date_time.Month;
            return month == 1;
        }


        public static int GetMonthToYearChange(DateTime date_time)
        {
            return 12 - date_time.Month;
        }

        public static int GetNumberOfMonth(DateTime start, DateTime end)
        {
            if (start.Year != end.Year)
            {
                int month_start = GetMonthToYearChange(start);
                int months_between = 0;
                for (int year = start.Year + 1; year < end.Year; year++)
                {
                    months_between += 12;
                }
                int month_end = end.Month;

                return month_start + months_between + month_end + 1;
            }

            return end.Month - start.Month + 1;
        }

        public static DateTime GetNextDateModulo(DateTime date_time, int modulo)
        {
            DateTime day = date_time;

            while (day.Day % modulo != 0)
                day = day.AddDays(1);

            return day;
        }
    }
}
