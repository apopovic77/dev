﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MathLib;

namespace ArtApp
{
    /// <summary>
    /// Interaktionslogik für Ball.xaml
    /// </summary>
    public partial class Ball : UserControl
    {
        public Ball()
        {
            InitializeComponent();

            v_richtung = v_richtung.GetNorm();

            UpdatePosBall();
        }


        public void Update(TimeSpan elapsed_time)
        {
            Vector2d v_bewegung = v_richtung * _geschwindigkeit * elapsed_time.Milliseconds / 1000f;



            Edge curr_edge = new Edge(v_pos, v_pos+v_bewegung + v_richtung * ellipse.Width/2);


            foreach (DragLine dline in Engine.DragLines)
            {
                Vector2d cutpoint;
                bool is_cut = LinearAlgebra.GetCutPointInSegment(curr_edge, dline.Edge, out cutpoint);

                if (!is_cut)
                    continue;



                v_richtung = v_richtung.GetMirror();
                Vector2d vorth = dline.Edge.Vector.GetOrthogonal1().GetNorm();
                double len_ortho = v_richtung.GetSkalarProduct(vorth);

                if (len_ortho < 0)
                {
                    vorth = dline.Edge.Vector.GetOrthogonal2().GetNorm();
                    len_ortho = v_richtung.GetSkalarProduct(vorth);
                }

                Vector2d xr = vorth * len_ortho;
                v_richtung = vorth + (xr - v_richtung);

                //v_pos = cutpoint + v_richtung*ellipse.Width;
                //v_pos = cutpoint;

                //Ellipse cutpoint_ellipse = new Ellipse() {Fill = Brushes.Red, Width = 10, Height = 10};
                //UpdatePos(cutpoint_ellipse, v_pos);
                //Engine.Canvas.Children.Add(cutpoint_ellipse);


                 //v_richtung = v_richtung.GetMirror() + dline.Edge.Vector.GetOrthogonal1().GetNorm() * 2 + v_richtung.GetSkalarProduct(dline.Edge.Vector.GetOrthogonal1().GetNorm());

                 //v_richtung = v_richtung.GetNorm();


                //Vector2d spiegelungs_vector = dline.Edge.Vector.GetOrthogonal1();
                //spiegelungs_vector = spiegelungs_vector.GetNorm();
                //double angle_rot = v_richtung.GetAngleBetween(spiegelungs_vector);


                //v_richtung.Rotate(angle_rot);

                //v_bewegung = v_richtung * _geschwindigkeit * elapsed_time.Milliseconds / 1000f;
                //v_pos = v_pos + v_bewegung;

                break;







                v_bewegung = v_richtung * _geschwindigkeit * elapsed_time.Milliseconds / 1000f;
            }

            
            
            
            
            if (v_pos.X + ellipse.Width > Engine.Canvas.ActualWidth)
            {
                v_bewegung.X = -v_bewegung.X;
                v_richtung.X = -v_richtung.X;
            }

            if (v_pos.Y + ellipse.Height > Engine.Canvas.ActualHeight)
            {
                v_bewegung.Y = -v_bewegung.Y;
                v_richtung.Y = -v_richtung.Y;
            }

            if (v_pos.X < 0)
            {
                v_bewegung.X = -v_bewegung.X;
                v_richtung.X = -v_richtung.X;
            }

            if (v_pos.Y < 0)
            {
                v_bewegung.Y = -v_bewegung.Y;
                v_richtung.Y = -v_richtung.Y;
            }

            v_pos = v_pos + v_bewegung;





            UpdatePosBall();
        }


        public void UpdatePosBall()
        {
            UpdatePos(this,v_pos);
        }

        public void UpdatePos(FrameworkElement ui_elem, Vector2d pos)
        {
            Canvas.SetLeft(ui_elem, pos.X - ui_elem.Width / 2d);
            Canvas.SetTop(ui_elem, pos.Y - ui_elem.Width / 2d);
        }

        private double winkel = 0;
        private float slowdown = 1f;
        Vector2d v_pos = new Vector2d(600, 600);
        Vector2d v_richtung = new Vector2d(6, 2);
        private float _geschwindigkeit = 150;
    }
}
