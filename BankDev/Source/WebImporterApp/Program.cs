﻿
using System;
using System.IO;
using BankLib.Data;


namespace WebImporterApp
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            int c_loaded = Buchungen.LoadBuchungenFromFile();

            //foreach (Buchung buchung in Buchungen.AlleBuchungen)
            //{
            //    if (buchung.BuchungsType == "POS")
            //        buchung.BuchungsType = "Bankomat Kassa";
            //    if (buchung.UmsatzType == "AT")
            //        Console.WriteLine(buchung.UmsatzTextRaw);
            //    buchung.CreateReadableUmsatzType();
            //}


            //Buchungen.SaveBuchungenToFile();
            //return;
            
            //string error_msg = "";
            //DateTime from_date = DateTime.Now;
            //from_date = from_date.AddMonths(-1);
            //Buchungen.LoadFromWebNewBuchungenAndSave(Konten.AlleKonten[0], from_date, ref error_msg);
            //return; 

            //UpdateCorrectUmsatzTexte();
            //return;

            //RemoveLastEntries(10);
            //return;

            UpdateAllFromWeb();
            return;
        }


        private static void UpdateAllFromWeb()
        {
            if (File.Exists(BuchungsStatusInfo.PathToStatusFile))
                File.Delete(BuchungsStatusInfo.PathToStatusFile);

            if (File.Exists(AppHelper.PathToLogFile))
                File.Delete(AppHelper.PathToLogFile);

            string error_msg = "";
            Buchungen.UpdateAllFromWeb(ref error_msg);
        }

        private static void RemoveLastEntries(int count_to_remove)
        {
            int count = Buchungen.LoadBuchungenFromFile();
            Buchungen.AlleBuchungen.RemoveRange(Buchungen.AlleBuchungen.Count - count_to_remove, count_to_remove);
            Buchungen.RemoveBuchungenFromFile();
            Buchungen.SaveBuchungenToFile();
            if (File.Exists(BuchungsStatusInfo.PathToStatusFile))
                File.Delete(BuchungsStatusInfo.PathToStatusFile);
        }

        private static void UpdateCorrectUmsatzTexte()
        {
            int count = Buchungen.LoadBuchungenFromFile();
            foreach (Buchung b in Buchungen.AlleBuchungen)
            {
                b.CreateReadableUmsatzType();
            }
            Buchungen.SaveBuchungenToFile();
        }


        //private static bool IsElementPresent(IWebDriver _driver, By by, out IWebElement query)
        //{
        //    try
        //    {
        //        query = _driver.FindElement(by);
        //        return true;
        //    }
        //    catch (NoSuchElementException e)
        //    {
        //        query = null;
        //        return false;
        //    }
        //}

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="_driver"></param>
        ///// <param name="by"></param>
        ///// <param name="element"></param>
        ///// <returns>return false, if element cannot be found</returns>
        //private static bool WaitUntilElementIsPresent(IWebDriver _driver, By by,IWebElement element)
        //{
        //    for (int second = 0;;second++)
        //    {
        //        if (second >= 60)
        //            return false;
        //        try
        //        {
        //            if (IsElementPresent(_driver, by, out element))
        //                break;
        //        }
        //        catch (Exception e)
        //        {
        //        }
        //        Thread.Sleep(1000);
        //    }
        //    return true;
        //}
    }
}

