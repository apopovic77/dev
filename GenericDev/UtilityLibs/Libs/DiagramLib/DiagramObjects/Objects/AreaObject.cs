﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Logicx.DiagramLib.Resources.Converter;
using Logicx.DiagramLib.Util;

namespace Logicx.DiagramLib.DiagramObjects.Objects
{
    public abstract class AreaObject<Tx, Ty> : PointObject<Tx, Ty>
    {
        #region Constants
        #endregion

        #region Construction and Initialization
        /// <summary>
        /// Constructor of the class
        /// </summary>
        /// <param name="from_x_value"></param>
        /// <param name="from_y_value"></param>
        /// <param name="to_x_value"></param>
        /// <param name="to_y_value"></param>
        public AreaObject(Tx from_x_value, Ty from_y_value, Tx to_x_value, Ty to_y_value)
            : base(from_x_value, from_y_value)
        {
            _to_x_value = _last_to_x_value = to_x_value;
            _to_y_value = _last_to_y_value = to_y_value;
        }

        #endregion

        #region Operations

        /// <summary>
        /// Overrides the reset last value method
        /// </summary>
        protected override void OnResetLastValues()
        {
            base.OnResetLastValues();
            ToXValue = _last_to_x_value;
            ToYValue = _last_to_y_value;
        }

        /// <summary>
        /// Overrides the clear binding method
        /// </summary>
        protected override void OnClearVisualBindings()
        {
            base.OnClearVisualBindings();

            if (ObjectVisual == null)
                return;

            BindingOperations.ClearBinding(ObjectVisual, FrameworkElement.WidthProperty);
            BindingOperations.ClearBinding(ObjectVisual, FrameworkElement.HeightProperty);

            _last_to_x_value = ToXValue;
            _last_to_y_value = ToYValue;
        }

        /// <summary>
        /// Overrides the binding update method
        /// </summary>
        protected override void OnUpdateVisualBindings()
        {
            base.OnUpdateVisualBindings();
            if (ObjectVisual == null)
                return;

            BindingOperations.ClearBinding(ObjectVisual, Canvas.TopProperty);

            MultiBinding top_position_binding = new MultiBinding();

            Binding y_value_binding = new Binding("ToYValue");
            y_value_binding.Source = this;
            top_position_binding.Bindings.Add(y_value_binding);

            Binding top_y_scale_binding = new Binding("YAxisControl.ScaleUnitSize");
            top_y_scale_binding.RelativeSource = DiagramBindingSource;
            top_position_binding.Bindings.Add(top_y_scale_binding);

            Binding top_y_axis_binding = new Binding("YAxisControl");
            top_y_axis_binding.RelativeSource = DiagramBindingSource;
            top_position_binding.Bindings.Add(top_y_axis_binding);

            Binding y_is_background_binding1 = new Binding("IsBackgroundObject");
            y_is_background_binding1.Source = this;
            top_position_binding.Bindings.Add(y_is_background_binding1);

            top_position_binding.Converter = new ValueToPositionConverter();
            ObjectVisual.SetBinding(Canvas.TopProperty, top_position_binding);


            BindingOperations.ClearBinding(ObjectVisual, FrameworkElement.WidthProperty);
            BindingOperations.ClearBinding(ObjectVisual, FrameworkElement.HeightProperty);

            _last_to_x_value = ToXValue;
            _last_to_y_value = ToYValue;

            MultiBinding width_binding = new MultiBinding();

            Binding x_from_value_binding = new Binding("XValue");
            x_from_value_binding.Source = this;
            width_binding.Bindings.Add(x_from_value_binding);

            Binding x_to_value_binding = new Binding("ToXValue");
            x_to_value_binding.Source = this;
            width_binding.Bindings.Add(x_to_value_binding);

            Binding x_scale_binding = new Binding("XAxisControl.ScaleUnitSize");
            x_scale_binding.RelativeSource = DiagramBindingSource;
            width_binding.Bindings.Add(x_scale_binding);

            Binding x_axis_binding = new Binding("XAxisControl");
            x_axis_binding.RelativeSource = DiagramBindingSource;
            width_binding.Bindings.Add(x_axis_binding);

            Binding x_is_background_binding = new Binding("IsBackgroundObject");
            x_is_background_binding.Source = this;
            width_binding.Bindings.Add(x_is_background_binding);

            width_binding.Converter = new FromToValueToPositionConverter();
            ObjectVisual.SetBinding(FrameworkElement.WidthProperty, width_binding);





            MultiBinding height_binding = new MultiBinding();

            Binding y_from_value_binding = new Binding("YValue");
            y_from_value_binding.Source = this;
            height_binding.Bindings.Add(y_from_value_binding);

            Binding y_to_value_binding = new Binding("ToYValue");
            y_to_value_binding.Source = this;
            height_binding.Bindings.Add(y_to_value_binding);

            Binding y_scale_binding = new Binding("YAxisControl.ScaleUnitSize");
            y_scale_binding.RelativeSource = DiagramBindingSource;
            height_binding.Bindings.Add(y_scale_binding);

            Binding y_axis_binding = new Binding("YAxisControl");
            y_axis_binding.RelativeSource = DiagramBindingSource;
            height_binding.Bindings.Add(y_axis_binding);

            Binding y_is_background_binding = new Binding("IsBackgroundObject");
            y_is_background_binding.Source = this;
            height_binding.Bindings.Add(y_is_background_binding);

            height_binding.Converter = new FromToValueToPositionConverter();
            ObjectVisual.SetBinding(FrameworkElement.HeightProperty, height_binding);
        }


        internal override void SetValues(Tx target_x_value, Ty target_y_value)
        {
            //Debug.WriteLine("  Area Object SetToXValue=" + target_x_value + ", SetToYValue=" + target_y_value + ", old values " + ToXValue + " " + ToYValue);
            SetToXValue(target_x_value);
            SetToYValue(target_y_value);
            //Debug.WriteLine("  Area Object CHANGED " + ToXValue + " " + ToYValue);

            base.SetValues(target_x_value, target_y_value);
        }

        /// <summary>
        /// Called after a move to operation has been completed
        /// </summary>
        /// <param name="target_x_value"></param>
        /// <param name="target_y_value"></param>
        /// <param name="x_offset_in_pixel"></param>
        /// <param name="y_offest_in_pixel"></param>
        protected override void OnObjectMoved(Tx target_x_value, Ty target_y_value, double x_offset_in_pixel, double y_offest_in_pixel)
        {
            //Debug.WriteLine(" AreaObject OnObjectMoved target values " + target_x_value + " " + target_y_value + ", offsets " + x_offset_in_pixel + " " + y_offest_in_pixel);

            base.OnObjectMoved(target_x_value, target_y_value, x_offset_in_pixel, y_offest_in_pixel);

            _last_to_x_value = ToXValue;
            _last_to_y_value = ToYValue;
        }

        private void SetToXValue(Tx target_x)
        {
            if (target_x is double)
            {
                double target_x_typed = (double)Convert.ChangeType(target_x, typeof(double));
                double x_val = (double)Convert.ChangeType(XValue, typeof(double));
                double to_x = (double)Convert.ChangeType(ToXValue, typeof(double));

                double new_to_x = to_x + target_x_typed - x_val;
                ToXValue = (Tx)Convert.ChangeType(new_to_x, typeof(Tx));
            }
            else if (target_x is int)
            {
                int target_x_typed = (int)Convert.ChangeType(target_x, typeof(int));
                int x_val = (int)Convert.ChangeType(XValue, typeof(int));
                int to_x = (int)Convert.ChangeType(ToXValue, typeof(int));

                int new_to_x = to_x + target_x_typed - x_val;
                ToXValue = (Tx)Convert.ChangeType(new_to_x, typeof(Tx));
            }
            else if (target_x is short)
            {
                short target_x_typed = (short)Convert.ChangeType(target_x, typeof(short));
                short x_val = (short)Convert.ChangeType(XValue, typeof(short));
                short to_x = (short)Convert.ChangeType(ToXValue, typeof(short));

                short new_to_x = (short)(to_x + target_x_typed - x_val);
                ToXValue = (Tx)Convert.ChangeType(new_to_x, typeof(Tx));
            }
            else if (target_x is float)
            {
                float target_x_typed = (float)Convert.ChangeType(target_x, typeof(float));
                float x_val = (float)Convert.ChangeType(XValue, typeof(float));
                float to_x = (float)Convert.ChangeType(ToXValue, typeof(float));

                float new_to_x = to_x + target_x_typed - x_val;
                ToXValue = (Tx)Convert.ChangeType(new_to_x, typeof(Tx));
            }
            else if (target_x is DateTime)
            {
                DateTime target_x_typed = (DateTime)Convert.ChangeType(target_x, typeof(DateTime));
                DateTime x_val = (DateTime)Convert.ChangeType(XValue, typeof(DateTime));
                DateTime to_x = (DateTime)Convert.ChangeType(ToXValue, typeof(DateTime));

                DateTime new_to_x = to_x + (target_x_typed - x_val);
                ToXValue = (Tx)Convert.ChangeType(new_to_x, typeof(Tx));
            }
            //else
            //{
            //    throw new ArgumentException("Not supported type for ToYValue calculation!");
            //}
        }

        private void SetToYValue(Ty target_y)
        {
            if (target_y is double)
            {
                double target_y_typed = (double)Convert.ChangeType(target_y, typeof(double));
                double y_val = (double)Convert.ChangeType(YValue, typeof(double));
                double to_y = (double)Convert.ChangeType(ToYValue, typeof(double));

                double new_to_y = to_y + target_y_typed - y_val;
                ToYValue = (Ty)Convert.ChangeType(new_to_y, typeof(Ty));
            }
            else if (target_y is int)
            {
                int target_y_typed = (int)Convert.ChangeType(target_y, typeof(int));
                int y_val = (int)Convert.ChangeType(YValue, typeof(int));
                int to_y = (int)Convert.ChangeType(ToYValue, typeof(int));

                int new_to_y = to_y + target_y_typed - y_val;
                ToYValue = (Ty)Convert.ChangeType(new_to_y, typeof(Ty));
            }
            else if (target_y is short)
            {
                short target_y_typed = (short)Convert.ChangeType(target_y, typeof(short));
                short y_val = (short)Convert.ChangeType(YValue, typeof(short));
                short to_y = (short)Convert.ChangeType(ToYValue, typeof(short));

                short new_to_y = (short)(to_y + target_y_typed - y_val);
                ToYValue = (Ty)Convert.ChangeType(new_to_y, typeof(Ty));
            }
            else if (target_y is float)
            {
                float target_y_typed = (float)Convert.ChangeType(target_y, typeof(float));
                float y_val = (float)Convert.ChangeType(YValue, typeof(float));
                float to_y = (float)Convert.ChangeType(ToYValue, typeof(float));

                float new_to_y = to_y + target_y_typed - y_val;
                ToYValue = (Ty)Convert.ChangeType(new_to_y, typeof(Ty));
            }
            else if (target_y is DateTime)
            {
                DateTime target_y_typed = (DateTime)Convert.ChangeType(target_y, typeof(DateTime));
                DateTime y_val = (DateTime)Convert.ChangeType(YValue, typeof(DateTime));
                DateTime to_y = (DateTime)Convert.ChangeType(ToXValue, typeof(DateTime));

                DateTime new_to_y = to_y + (target_y_typed - y_val);
                ToYValue = (Ty)Convert.ChangeType(new_to_y, typeof(Ty));
            }
            //else
            //{
            //    throw new ArgumentException("Not supported type for ToYValue calculation!");
            //}
        }

        protected override void OnPreviewObjectResize(Tx from_x_value, Ty from_y_value, Tx to_x_value, Ty to_y_value, out bool can_size)
        {
            base.OnPreviewObjectResize(from_x_value, from_y_value, to_x_value, to_y_value, out can_size);
            ToXValue = to_x_value;
            ToYValue = to_y_value;
        }

        protected override void OnObjectResized(Tx from_x_value, Ty from_y_value, Tx to_x_value, Ty to_y_value)
        {
            base.OnObjectResized(from_x_value, from_y_value, to_x_value, to_y_value);
            ToXValue = to_x_value;
            ToYValue = to_y_value;
            _last_to_x_value = ToXValue;
            _last_to_y_value = ToYValue;
        }
        #endregion

        #region Event Handlers
        #endregion

        #region Properties
        /// <summary>
        /// Gets/sets the x value
        /// </summary>
        public Tx ToXValue
        {
            get { return _to_x_value; }
            set
            {
                Tx oldValue = value;
                SendPropertyChanging("ToXValue");
                _to_x_value = value;
                SendPropertyChanged("ToXValue");

                OnPropertyChanged(new PropertyChangedMeta("ToXValue", oldValue, _to_x_value));
            }
        }
        /// <summary>
        /// Gets/Sets the y value
        /// </summary>
        public Ty ToYValue
        {
            get { return _to_y_value; }
            set
            {
                Ty oldValue = value;
                SendPropertyChanging("ToYValue");
                _to_y_value = value;
                SendPropertyChanged("ToYValue");

                OnPropertyChanged(new PropertyChangedMeta("ToYValue", oldValue, _to_y_value));
            }
        }
        #endregion

        #region Dependency Properties
        #endregion

        #region Events
        #endregion

        #region Routed Events
        #endregion

        #region Attributes
        private Tx _to_x_value;
        private Ty _to_y_value;

        private Tx _last_to_x_value;
        private Ty _last_to_y_value;
        #endregion

        #region Tests
        #endregion
    }
}
