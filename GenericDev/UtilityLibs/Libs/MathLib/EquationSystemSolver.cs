﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MathLib
{
    public class EquationSystemSolver
    {
        /// <summary>
        /// Gauss'sches Eliminationsverfahren mit Zeilenpivotisierung
        /// </summary>
        /// <param name="coefficient_matrix"></param>
        /// <param name="dimension"></param>
        /// <param name="result"></param>
        /// <returns>True if solution found, else False</returns>
        public static bool GaussElimination(float[][] coefficient_matrix, int dimension, out float[] result)
        {
            int i, j;                    // Zeile, Spalte
            bool irregular_pivot = false;
            const float epsilon = 0.01f;   // Genauigkeit
            result = new float[dimension];

            int current_line = 0;
            do
            {
                float pivot_value = Math.Abs(coefficient_matrix[current_line][current_line]);                // Zeilenpivotisierung
                int pivotzeile = current_line;                  // Pivotzeile
                for (i = current_line + 1; i < dimension; i++)
                {
                    if (Math.Abs(coefficient_matrix[i][current_line]) > pivot_value)
                    {
                        pivot_value = Math.Abs(coefficient_matrix[i][current_line]);
                        pivotzeile = i;
                    }
                }

                irregular_pivot = (pivot_value < epsilon);
                if (irregular_pivot) break;           // nicht loesbar 

                if (pivotzeile != current_line)  // falls erforderlich, Zeilen tauschen
                {
                    for (j = current_line; j <= dimension; j++)
                    {
                        float h = coefficient_matrix[current_line][j];
                        coefficient_matrix[current_line][j] = coefficient_matrix[pivotzeile][j];
                        coefficient_matrix[pivotzeile][j] = h;
                    }
                }

                // Elimination --> Nullen in Spalte s ab Zeile s+1
                for (i = current_line + 1; i < dimension; i++)
                {
                    float multiplicator = -(coefficient_matrix[i][current_line] / coefficient_matrix[current_line][current_line]);                      // Multiplikationsfaktor
                    coefficient_matrix[i][current_line] = 0.0f;
                    for (j = current_line + 1; j <= dimension; j++)   // die einzelnen Spalten
                        coefficient_matrix[i][j] += multiplicator * coefficient_matrix[current_line][j];       // Addition der Zeilen i, s
                }

                current_line++;
            } while (current_line < dimension - 1);

            if (irregular_pivot)
            {
                return false;
            }
            else
            {
                // Berechnen der Loesungen aus der entstandenen Dreiecksmatrix
                // letzte Zeile
                result[dimension - 1] = coefficient_matrix[dimension - 1][dimension] / coefficient_matrix[dimension - 1][dimension - 1];

                // restliche Zeilen
                for (i = dimension - 2; i >= 0; i--)
                {
                    for (j = dimension - 1; j > i; j--)
                    {
                        coefficient_matrix[i][dimension] -= result[j] * coefficient_matrix[i][j];    // rechte Seite berechnen
                    }
                    result[i] = coefficient_matrix[i][dimension] / coefficient_matrix[i][i];       // Loesung
                }

                return true;
            }
        }


    }
}
