﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Runtime.Serialization;
using System.Text;
using Utility.Cryptography.CSP;

namespace Utility.Configuration
{
    /// <summary>
    /// Enumeration for specifying configuration store encryption types
    /// </summary>
    public enum ConfigEncryption
    {
        /// <summary>
        /// CurrentUser - CSP for current user will be used
        /// </summary>
        CurrentUser = 0,
        /// <summary>
        /// LocalSystem - CSP of local system will be used
        /// </summary>
        LocalSystem = 1
    }

    /// <summary>
    /// The class <c>ConfigurationStore</c> implements a configuration store object.
    /// </summary>
    [SerializableAttribute()]
    public class ConfigurationStore : ISerializable
    {
        #region Constants
        #endregion

        #region Construction and Initialization
        		/// <summary>
		/// Creates a new instance of the class
		/// </summary>
		internal ConfigurationStore()
		{
		}

		/// <summary>
		/// Creates a new instance of the class
		/// </summary>
		/// <param name="info">serialization informations</param>
		/// <param name="context">streaming context</param>
        private ConfigurationStore(SerializationInfo info, StreamingContext context)
		{
			int i = info.GetInt32("Count");
			if (i != 0)
			{
				IDictionary iDictionary = Store;
				for (int j = 0; j < i; j++)
				{
					string str = info.GetString(String.Concat("n", j));
					object local = info.GetValue(String.Concat("v", j), typeof(object));
					iDictionary.Add(str, local);
				}
			}
		}
        #endregion

        #region Operations
        /// <summary>
        /// Clears a store
        /// </summary>
        public void Clear()
        {
            if (!IsEmpty)
            {
                _store.Clear();
            }
        }

        /// <summary>
        /// Clears the value of the store
        /// </summary>
        /// <param name="name"></param>
        public void ClearValue(string name)
        {
            if (name == null || name.Length == 0)
            {
                throw new ArgumentException();
            }
            if (_store != null)
            {
                _store.Remove(name);
            }
        }

        /// <summary>
        /// Checks if the store contains a specific value
        /// </summary>
        /// <param name="name">name of setting</param>
        /// <returns>true if the store contains a value for the setting <c>name</c></returns>
        public bool ContainsValue(string name)
        {
            if (name == null || name.Length == 0)
            {
                throw new ArgumentException();
            }
            if (_store != null && _store.Contains(name))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Gets the object value of a named setting
        /// </summary>
        /// <param name="name">name of setting</param>
        /// <returns>an instance of the value object</returns>
        public object GetValue(string name)
        {
            if (name == null || name.Length == 0)
            {
                throw new ArgumentException();
            }
            object local = null;
            if (_store != null)
            {
                local = _store[name];
            }
            return local;
        }

        /// <summary>
        /// Gets the boolean value of a named setting
        /// </summary>
        /// <param name="name">name of setting</param>
        /// <param name="defaultValue">default value if setting not found in store</param>
        /// <returns>the value for the setting <c>name</c></returns>
        public bool GetValue(string name, bool defaultValue)
        {
            object local = GetValue(name);
            if (local != null)
            {
                return (bool)local;
            }
            else
            {
                return defaultValue;
            }
        }

        /// <summary>
        /// Gets the integer value of a named setting
        /// </summary>
        /// <param name="name">name of setting</param>
        /// <param name="defaultValue">default value if setting not found in store</param>
        /// <returns>the value for the setting <c>name</c></returns>
        public int GetValue(string name, int defaultValue)
        {
            object local = GetValue(name);
            if (local != null)
            {
                return (int)local;
            }
            else
            {
                return defaultValue;
            }
        }

        /// <summary>
        /// Gets the string value of a named setting
        /// </summary>
        /// <param name="name">name of setting</param>
        /// <param name="defaultValue">default value if setting not found in store</param>
        /// <returns>the value for the setting <c>name</c></returns>
        public string GetValue(string name, string defaultValue)
        {
            object local = GetValue(name);
            if (local != null)
            {
                return (String)local;
            }
            else
            {
                return defaultValue;
            }
        }

        /// <summary>
        /// Gets the encrypted string value of a named setting
        /// </summary>
        /// <param name="name">name of setting</param>
        /// <param name="defaultValue">default value if setting not found in store</param>
        /// <param name="encryption">encryption type</param>
        /// <returns>the value for the setting <c>name</c></returns>
        public string GetValue(string name, string defaultValue, ConfigEncryption encryption)
        {
            string str = GetValue(name, null);
            if (str == null)
            {
                return defaultValue;
            }
            if (str.Length == 0)
            {
                return str;
            }
            try
            {
                if (encryption == ConfigEncryption.CurrentUser)
                {
                    str = DataProtection.DecryptUserData(str);
                }
                else
                {
                    str = DataProtection.DecryptData(str);
                }
            }
            catch (Exception)
            {
                str = defaultValue;
            }
            return str;
        }

        /// <summary>
        /// Sets the boolean value of a named setting
        /// </summary>
        /// <param name="name">name of setting</param>
        /// <param name="value">actual value to set</param>
        /// <param name="defaultValue">default value for the setting</param>
        public void SetValue(string name, bool value, bool defaultValue)
        {
            if (name == null || name.Length == 0)
            {
                throw new ArgumentException();
            }
            if (value != defaultValue)
            {
                Store[name] = value;
            }
            else if (!IsEmpty)
            {
                _store.Remove(name);
                return;
            }
        }

        /// <summary>
        /// Sets the integer value of a named setting
        /// </summary>
        /// <param name="name">name of setting</param>
        /// <param name="value">actual value to set</param>
        /// <param name="defaultValue">default value for the setting</param>
        public void SetValue(string name, int value, int defaultValue)
        {
            if (name == null || name.Length == 0)
            {
                throw new ArgumentException();
            }
            if (value != defaultValue)
            {
                Store[name] = value;
            }
            else if (!IsEmpty)
            {
                _store.Remove(name);
                return;
            }
        }

        /// <summary>
        /// Sets the object value of a named setting
        /// </summary>
        /// <param name="name">name of setting</param>
        /// <param name="value">actual value to set</param>
        public void SetValue(string name, object value)
        {
            if (name == null || name.Length == 0)
            {
                throw new ArgumentException();
            }
            if (!(value is ISerializable))
            {
                throw new ArgumentException("Object must implement ISerializable", "value");
            }
            Store[name] = value;
        }

        /// <summary>
        /// Sets the string value of a named setting
        /// </summary>
        /// <param name="name">name of setting</param>
        /// <param name="value">actual value to set</param>
        /// <param name="defaultValue">default value for the setting</param>
        public void SetValue(string name, string value, string defaultValue)
        {
            if (name == null || name.Length == 0)
            {
                throw new ArgumentException();
            }
            if (!(value == defaultValue))
            {
                if (value == null)
                {
                    throw new ArgumentNullException();
                }
                Store[name] = value;
            }
            else if (!IsEmpty)
            {
                _store.Remove(name);
                return;
            }
        }

        /// <summary>
        /// Sets the encrypted string value of a named setting
        /// </summary>
        /// <param name="name">name of setting</param>
        /// <param name="svalue">actual value to set</param>
        /// <param name="defaultValue">default value for the setting</param>
        /// <param name="encryption">encryption type</param>
        public void SetValue(string name, string svalue, string defaultValue, ConfigEncryption encryption)
        {
            if (name == null || name.Length == 0)
            {
                throw new ArgumentException();
            }
            if (!(svalue == defaultValue))
            {
                if (svalue == null)
                {
                    throw new ArgumentNullException();
                }
                if (encryption == ConfigEncryption.CurrentUser)
                {
                    svalue = DataProtection.EncryptUserData(svalue);
                }
                else
                {
                    svalue = DataProtection.EncryptData(svalue);
                }
                Store[name] = svalue;
            }
            else if (!IsEmpty)
            {
                _store.Remove(name);
                return;
            }
        }

        /// <summary>
        /// Implements ISerializable.GetObjectData() and returns the object data
        /// </summary>
        /// <param name="info">serialization info</param>
        /// <param name="context">streaming context</param>
        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            int i = 0;
            if (_store != null)
            {
                i = _store.Count;
            }
            info.AddValue("Count", i);
            if (i != 0)
            {
                int j = 0;
                IDictionaryEnumerator iDictionaryEnumerator = _store.GetEnumerator();
                while (iDictionaryEnumerator.MoveNext())
                {
                    DictionaryEntry dictionaryEntry = (DictionaryEntry)iDictionaryEnumerator.Current;
                    info.AddValue(String.Concat("n", j), dictionaryEntry.Key);
                    info.AddValue(String.Concat("v", j), dictionaryEntry.Value);
                    j++;
                }
            }
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets a flag specifying whether the store is empty or not
        /// </summary>
        public bool IsEmpty
        {
            get
            {
                if (_store != null)
                {
                    return _store.Count == 0;
                }
                else
                {
                    return true;
                }
            }
        }

        /// <summary>
        /// Gets the store as dictionary object
        /// </summary>
        private IDictionary Store
        {
            get
            {
                if (_store == null)
                {
                    _store = new HybridDictionary(false);
                }
                return _store;
            }
        }
        #endregion

        #region Attributes
        /// <summary>
        /// Internal store member
        /// </summary>
        private HybridDictionary _store;
        #endregion

        #region Tests
        #endregion
    }
}
