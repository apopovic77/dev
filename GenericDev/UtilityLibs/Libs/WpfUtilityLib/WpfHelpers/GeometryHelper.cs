﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;
using Logicx.Geo.Geometries;
using MathLib;

namespace Logicx.WpfUtility.WpfHelpers
{
    public class GeometryHelper
    {
        public static StreamGeometry SimplePolygon2StreamGeometry(SimplePolygon mypoly)
        {
            if (mypoly == null)
                return null;

            StreamGeometry streamgeometry = new StreamGeometry();
            streamgeometry.FillRule = FillRule.Nonzero;
            using (StreamGeometryContext ctx = streamgeometry.Open())
            {
                int pos = 0;



                foreach (Vector2f vect in mypoly.Vertices)
                {
                    if (pos == 0)
                    {
                        Point start_point = new Point(vect.X, vect.Y);
                        ctx.BeginFigure(start_point, true /* is filled */, false /* is not closed? */);
                        pos++;
                    }
                    else
                        ctx.LineTo(new Point(vect.X, vect.Y), true /* is stroked */, false /* is smooth join */);
                }
            }
            return streamgeometry;
        }
    }
}
