﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MathLib
{
    [Serializable]
    public struct Vector3f
    {
        public float X;
        public float Y;
        public float Z;
    }
}
