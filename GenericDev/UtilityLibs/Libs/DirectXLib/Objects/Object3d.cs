using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.DirectX;
using Microsoft.DirectX.Direct3D;
using Logicx.DirectX.Engine;

namespace Logicx.DirectX.Objects
{

    public abstract class Object3d
    {
        /// <summary> Basic structures used in the game</summary>
        public struct Attitude
        {
            public float Pitch;
            public float Heading;
            public float Roll;

            //Heading � Rotation about the Z axis
            //Pitch � Rotation about the X axis
            //Roll � Rotation about the Y axis

            public Attitude(float pitch, float heading, float roll)
            {
                Pitch = pitch;
                Heading = heading;
                Roll = roll;
            }
            public static float Aepc(float angle)
            {
                if (angle > (2.0 * Math.PI))
                {
                    angle -= (float)(2.0 * Math.PI);
                }
                if (angle < 0.0f)
                {
                    angle += (float)(2.0 * Math.PI);
                }
                return angle;
            }
            public static float Aepc2(float angle)
            {
                if (angle > Math.PI)
                {
                    angle -= (float)(2.0 * Math.PI);
                }
                if (angle < -Math.PI)
                {
                    angle += (float)(2.0 * Math.PI);
                }
                return angle;
            }
        }

        public Object3d() { }


        public void UpdateWorldMatrix()
        {

            Matrix matrix = Matrix.Identity * Matrix.RotationYawPitchRoll(m_vOrientation.Heading, m_vOrientation.Pitch, m_vOrientation.Roll) * Matrix.Translation(m_vPosition);

            if (m_vScaleing != null)
            {
                matrix.Multiply(Matrix.Scaling(m_vScaleing));
            }

            Engine3d.Device3d.Transform.World = matrix;

        }

        public Vector3 Position
        {
            get { return m_vPosition * m_vScaleing.X; }
            set
            {
                Vector3 pos = value;
                pos.X /= m_vScaleing.X;
                pos.Y /= m_vScaleing.Y;
                pos.Z /= m_vScaleing.Z;
                m_vPosition = pos;
                m_vPosition = value;
            }
        }
        protected Vector3 m_vPosition = new Vector3(0f, 0f, 0f);
        protected Vector3 m_vVelocity;
        public Vector3 Scaleing { get { return m_vScaleing; } set { m_vScaleing = value; } }
        protected Vector3 m_vScaleing = new Vector3(1f, 1f, 1f);
        protected Attitude m_vOrientation;

        public virtual float North { get { return m_vPosition.Y; } set { m_vPosition.Y = value; } }
        public virtual float East { get { return m_vPosition.X; } set { m_vPosition.X = value; } }
        public virtual float Height { get { return m_vPosition.Z; } set { m_vPosition.Z = value; } }
        public virtual float Yaw { get { return m_vOrientation.Roll; } set { m_vOrientation.Roll = value; } }
        public virtual float Pitch { get { return m_vOrientation.Pitch; } set { m_vOrientation.Pitch = value; } }
        public virtual float Roll { get { return m_vOrientation.Heading; } set { m_vOrientation.Heading = value; } }

        protected readonly float PI = Convert.ToSingle(Math.PI);
        protected readonly float PI_half = Convert.ToSingle(Math.PI / 2);
        protected readonly float PI_quad = Convert.ToSingle(Math.PI / 4);

        
        public abstract void Render(float elapsedTime);
        public abstract void Init();
        public virtual void Dispose() { }
        public virtual bool IsAlive() { return true; }
    }
}
