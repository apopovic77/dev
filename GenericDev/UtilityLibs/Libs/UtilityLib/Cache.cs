﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Logicx.Utility
{
    /// <summary>
    /// Generic cache that operates like a usual LRU (last recently used) cache.
    /// </summary>
    /// <typeparam name="K"></typeparam>
    /// <typeparam name="T"></typeparam>
    public class Cache<K, T>
    {
        private struct CacheData
        {
            public DateTime LastUsed;
            public K Key;
            public T Item;
        }

        public Cache()
        {
            _items = new Dictionary<K, CacheData>();
            _last_used= new List<CacheData>();
        }

        public void Add(K key, T item)
        {
            if (_items.Count == _max_cached_items)
                RemoveLeastUsed();

            CacheData cd = new CacheData();
            cd.Key = key;
            cd.Item = item;
            cd.LastUsed = DateTime.Now;

            _items.Add(key, cd);
            _last_used.Add(cd);
        }

        protected virtual void RemoveLeastUsed()
        {
            _last_used.Sort((x, y) => y.LastUsed.CompareTo(x.LastUsed));

            CacheData cd = _last_used[_last_used.Count - 1];
            _items.Remove(cd.Key);
        }

        public T Get(K key)
        {
            CacheData cd = _items[key];
            cd.LastUsed = DateTime.Now;
            return cd.Item;
        }

        public bool Contains(K key)
        {
            return _items.ContainsKey(key);
        }


        private int _max_cached_items = 100;
        private Dictionary<K, CacheData> _items;
        private List<CacheData> _last_used;

    }
}
