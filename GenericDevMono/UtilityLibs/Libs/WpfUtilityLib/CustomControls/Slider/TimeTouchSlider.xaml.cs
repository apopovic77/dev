﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Logicx.WpfUtility.CustomControls.Slider
{
    /// <summary>
    /// Interaction logic for TimeTouchSlider.xaml
    /// </summary>
    public partial class TimeTouchSlider : UserControl, IDisposable
    {
        #region Constants
        #endregion

        #region Construction and Initialization
        public TimeTouchSlider()
        {
            InitializeComponent();
        }
        #endregion

        #region Operations
        #region Value management
        private void AdjustValue()
        {
            _ignore_value_changes = true;

            bool require_hour_slider_adjustment = false;
            bool require_min_slider_adjustment = false;
            bool require_sec_slider_adjustment = false;

            int hour = hours_slider.Value;
            int min = minutes_slider.Value;
            int sec = seconds_slider.Value;

            #region value checks
            if (hour < 0)
            {
                hour = 0;
                require_hour_slider_adjustment = true;
            }
            else if (hour > 23)
            {
                hour = 23;
                require_hour_slider_adjustment = true;
            }

            if (min < 0)
            {
                min = 0;
                require_min_slider_adjustment = true;
            }
            else if (min > 59)
            {
                min = 59;
                require_min_slider_adjustment = true;
            }

            if (sec < 0)
            {
                sec = 0;
                require_sec_slider_adjustment = true;
            }
            else if (sec > 59)
            {
                min = 59;
                require_sec_slider_adjustment = true;
            }

            #endregion

            if(Value.Hour != hour || Value.Minute != min || Value.Second != sec)
            {
                DateTime dtNewValue = Value;
                Value = new DateTime(dtNewValue.Year, dtNewValue.Month, dtNewValue.Day, hour, min, sec);
            }

            _ignore_selection_changes = true;
            if (require_hour_slider_adjustment)
                hours_slider.Value = hour;

            if (require_min_slider_adjustment)
                minutes_slider.Value = min;

            if (require_sec_slider_adjustment)
                seconds_slider.Value = sec;

            _ignore_selection_changes = false;

            _ignore_value_changes = false;
        }

        private void SyncSlidersWithCurrentValue()
        {
            _ignore_selection_changes = true;

            hours_slider.Value = Value.Hour;
            minutes_slider.Value = Value.Minute;
            seconds_slider.Value = Value.Second;

            _ignore_selection_changes = false;
        }
        #endregion

        #region Property changing and coersion

        private bool _ignore_value_callback_change = false;
        private static void OnValueChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is TimeTouchSlider)
            {
                TimeTouchSlider slider_control = (TimeTouchSlider)d;

                if (slider_control._ignore_value_callback_change)
                    return;

                var coerced_value = OnValueCoerce(d, slider_control.Value);

                if (coerced_value == DependencyProperty.UnsetValue)
                    return;

                if (((DateTime)coerced_value) != slider_control.Value)
                {
                    slider_control._ignore_value_callback_change = true;
                    slider_control.Value = ((DateTime)coerced_value);
                    slider_control._ignore_value_callback_change = false;
                }

                if (!slider_control._ignore_value_changes)
                    slider_control.SyncSlidersWithCurrentValue();
            }
        }

        private static object OnValueCoerce(DependencyObject d, object basevalue)
        {
            if (d is NumericTouchSlider)
            {
                TimeTouchSlider slider_control = (TimeTouchSlider)d;

                if (!coerce_current)
                {
                }

                coerce_current = false;
            }

            return basevalue;
        }
        #endregion
        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            Dispose(true);
        }

        private void Dispose(bool disposing)
        {
            if (_disposing)
                return;

            if (!_disposed)
            {
                _disposing = disposing;


                //EventHelper.RemoveAllTextContainerChangedEventHanders(tb);
                //EventHelper.RemoveAllTextContainerChangeEventHanders(tb);
                _disposed = true;
            }
        }

        #endregion

        #region Event Handlers
        private void StackPanel_MouseMove(object sender, MouseEventArgs e)
        {
            e.Handled = true;
        }

        private void StackPanel_StylusMove(object sender, StylusEventArgs e)
        {
            e.Handled = true;
        }

        private void hours_slider_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (_ignore_selection_changes)
                return;

            AdjustValue();
        }

        private void minutes_slider_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (_ignore_selection_changes)
                return;

            AdjustValue();
        }

        private void seconds_slider_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (_ignore_selection_changes)
                return;

            AdjustValue();
        }
        #endregion

        #region Properties
        public bool InverseMouseWheel
        {
            get { return _inverse_mouse_wheel; }
            set
            {
                if (_inverse_mouse_wheel != value)
                {
                    _inverse_mouse_wheel = value;

                    hours_slider.InverseMouseWheel = _inverse_mouse_wheel;
                    minutes_slider.InverseMouseWheel = _inverse_mouse_wheel;
                    seconds_slider.InverseMouseWheel = _inverse_mouse_wheel;
                }
            }
        }
        #endregion

        #region Dependency Properties
        public DateTime Value
        {
            get { return (DateTime)GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }
        public static readonly DependencyProperty ValueProperty = DependencyProperty.Register("Value", typeof(DateTime), typeof(TimeTouchSlider), new UIPropertyMetadata(DateTime.MinValue, new PropertyChangedCallback(OnValueChanged)));

        public double SliderWidth
        {
            get { return (double)GetValue(SliderWidthProperty); }
            set { SetValue(SliderWidthProperty, value); }
        }
        public static readonly DependencyProperty SliderWidthProperty = DependencyProperty.Register("SliderWidth", typeof(double), typeof(TimeTouchSlider), new UIPropertyMetadata((double)60));

        public double SliderHeight
        {
            get { return (double)GetValue(SliderHeightProperty); }
            set { SetValue(SliderHeightProperty, value); }
        }
        public static readonly DependencyProperty SliderHeightProperty = DependencyProperty.Register("SliderHeight", typeof(double), typeof(TimeTouchSlider), new UIPropertyMetadata((double)55));

        public bool AllowAdjustSeconds
        {
            get { return (bool)GetValue(AllowAdjustSecondsProperty); }
            set { SetValue(AllowAdjustSecondsProperty, value); }
        }
        public static readonly DependencyProperty AllowAdjustSecondsProperty = DependencyProperty.Register("AllowAdjustSeconds", typeof(bool), typeof(TimeTouchSlider), new UIPropertyMetadata(false));


        /// <summary>
        /// das item template für ein item in dem control
        /// </summary>
        public DataTemplate ItemTemplate
        {
            get { return (DataTemplate)GetValue(ItemTemplateProperty); }
            set { SetValue(ItemTemplateProperty, value); }
        }
        public static readonly DependencyProperty ItemTemplateProperty = DependencyProperty.Register("ItemTemplate", typeof(DataTemplate), typeof(TimeTouchSlider), new UIPropertyMetadata(null));

        /// <summary>
        /// der style für den selection indicator
        /// </summary>
        public Style SelectionIndicatorBorderStyle
        {
            get { return (Style)GetValue(SelectionIndicatorBorderStyleProperty); }
            set { SetValue(SelectionIndicatorBorderStyleProperty, value); }
        }
        public static readonly DependencyProperty SelectionIndicatorBorderStyleProperty = DependencyProperty.Register("SelectionIndicatorBorderStyle", typeof(Style), typeof(TimeTouchSlider), new UIPropertyMetadata(null));


        /// <summary>
        /// der style für das control
        /// </summary>
        public Style ControlBorderStyle
        {
            get { return (Style)GetValue(ControlBorderStyleProperty); }
            set { SetValue(ControlBorderStyleProperty, value); }
        }
        public static readonly DependencyProperty ControlBorderStyleProperty = DependencyProperty.Register("ControlBorderStyle", typeof(Style), typeof(TimeTouchSlider), new UIPropertyMetadata(null));

        public Style SeparatorTextBlockStyle
        {
            get { return (Style)GetValue(SeparatorTextBlockStyleProperty); }
            set { SetValue(SeparatorTextBlockStyleProperty, value); }
        }
        public static readonly DependencyProperty SeparatorTextBlockStyleProperty = DependencyProperty.Register("SeparatorTextBlockStyle", typeof(Style), typeof(TimeTouchSlider), new UIPropertyMetadata(null));

        #endregion

        #region Events
        #endregion

        #region Routed Events
        #endregion

        #region Attributes
        private bool _disposed = false;
        private bool _disposing = false;

        private bool _ignore_selection_changes = false;
        private bool _ignore_value_changes = false;

        private static bool coerce_current;
        private bool _inverse_mouse_wheel = false;
        #endregion

        #region Tests
        #endregion
        
    }
}
