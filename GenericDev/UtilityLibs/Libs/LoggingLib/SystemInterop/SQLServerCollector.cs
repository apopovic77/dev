﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Management;
using System.Text;
using Microsoft.Win32;

namespace LoggingLib.SystemInterop
{
    public class SQLServerCollector
    {
        #region Nested SQLServerItem class
        public class SQLServerItem
        {
            #region Construction and initialization
            /// <summary>
            /// Default constructor
            /// </summary>
            public SQLServerItem()
            {
                
            }
            #endregion

            #region Properties
            public string InstanceName
            {
                get { return _instance_name; }
                set { _instance_name = value; }
            }

            public string Version
            {
                get { return _version; }
                set { _version = value; }
            }

            public string Edition
            {
                get { return _edition; }
                set { _edition = value; }
            }

            public string LicenseMode
            {
                get { return _license_mode; }
                set { _license_mode = value; }
            }

            public string BinPath
            {
                get { return _binpath; }
                set { _binpath = value; }
            }

            public string StartMode
            {
                get { return _start_mode; }
                set { _start_mode = value; }
            }

            public string ServiceAccount
            {
                get { return _service_account; }
                set { _service_account = value; }
            }

            public string InstanceStatus
            {
                get { return _instance_status; }
                set { _instance_status = value; }
            }

            public string FileVersion
            {
                get { return _file_version; }
                set { _file_version = value; }
            }

            public string Clustered
            {
                get { return _clustered; }
                set { _clustered = value; }
            }
            #endregion

            #region Operations and Methods
            #endregion

            #region Attribs

            private string _instance_name = "";
            private string _version = "";
            private string _edition = "";
            private string _license_mode = "";
            private string _binpath = "";
            private string _start_mode = "";
            private string _service_account = "";
            private string _instance_status = "";
            private string _file_version = "";
            private string _clustered = "";
            #endregion
        }
        #endregion

        #region Construction and initialization
        public SQLServerCollector()
        {
            ScanSQLServer();
            SetClusterInfo();
            ReadVersion();
            ReadEdition();
            ReadLicenseModes();
        }
        #endregion

        #region Properties
        public List<SQLServerItem> SQLInstances
        {
            get { return _scanned_sql_server; }
        }
        #endregion

        #region Operations and Methods
        private void ScanSQLServer()
        {
            string wmiQueryObject = "Win32_Service";

            ManagementObjectSearcher searcher;
            int i = 0;
            try
            {
                searcher = new ManagementObjectSearcher("SELECT * FROM " + wmiQueryObject);

                foreach (ManagementObject wmi_HD in searcher.Get())
                {
                    i++;

                    string sName = "", sPath = "";

                    sName = Convert.ToString(wmi_HD.Properties["Name"].Value);
                    sPath = Convert.ToString(wmi_HD.Properties["PathName"].Value);
    
                    if( sPath.ToLower().IndexOf("sqlservr.exe") >= 0)
                    {
                        SQLServerItem sqlServer = new SQLServerItem();

                        // this service is a sql instance
                        if(sName.ToUpper().Equals("MSSQLSERVER"))
                        {
                            sqlServer.InstanceName = "MSSQLSERVER";
                        } 
                        else
                        {
                            // MSSQL$INSTANCENAME
                            sqlServer.InstanceName = sName.ToUpper().Substring(6);
                        }

                        string sTmp = sPath;

                        if(sTmp.StartsWith("\""))
                        {
                            int idx = sTmp.IndexOf('"', 1);
                            sTmp = sTmp.Substring(1, idx - 1);
                        }
                        else
                        {
                            string[] data = sTmp.Split(new char[] {' '});
                            sTmp = data[0];
                        }

                        sqlServer.BinPath = sTmp;
                        sqlServer.StartMode = Convert.ToString(wmi_HD.Properties["StartMode"].Value);
                        sqlServer.ServiceAccount = Convert.ToString(wmi_HD.Properties["StartName"].Value);
                        sqlServer.InstanceStatus = Convert.ToString(wmi_HD.Properties["State"].Value);


                        FileVersionInfo fvI = FileVersionInfo.GetVersionInfo(sTmp);
                        sqlServer.FileVersion = fvI.FileMajorPart.ToString() + "." + fvI.FileMinorPart.ToString() + "." + fvI.FileBuildPart.ToString();

                        _scanned_sql_server.Add(sqlServer);
                    }
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                Debug.WriteLine("ScanSQLServer(): " + ex.ToString());
#endif
            }
        }
        private bool IsClustered()
        {
            string wmiQueryObject = "Win32_Service";

            ManagementObjectSearcher searcher;
            int i = 0;
            try
            {
                searcher = new ManagementObjectSearcher("SELECT * FROM " + wmiQueryObject);

                foreach (ManagementObject wmi_HD in searcher.Get())
                {
                    i++;

                    string sName = "", sCaption="", sPath = "";

                    sName = Convert.ToString(wmi_HD.Properties["Name"].Value);
                    sCaption = Convert.ToString(wmi_HD.Properties["Caption"].Value);
                    sPath = Convert.ToString(wmi_HD.Properties["PathName"].Value);

                    if (sPath.ToLower().IndexOf("clussvc.exe") >= 0)
                    {
                        if (sCaption == "Culster Service" && sName == "ClusSvc")
                            return true;
                    }
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                Debug.WriteLine("IsClustered(): " + ex.ToString());
#endif
            }

            return false;
        }

        private void SetClusterInfo()
        {
            string sClusterInfo = IsClustered() ? "Cluster" : "NoCluster";

            foreach (SQLServerItem sqlServer in _scanned_sql_server)
                sqlServer.Clustered = sClusterInfo;
        }

        private void ReadVersion()
        {
            RegistryKey hklm = Registry.LocalMachine;

            foreach(SQLServerItem sqlServer in _scanned_sql_server)
            {
                if( sqlServer.InstanceName.ToUpper().Equals("MSSQLSERVER"))
                {
                    // standard instance
                    RegistryKey keySQL = hklm.OpenSubKey("SOFTWARE\\Microsoft\\MSSQLServer\\MSSQLServer\\CurrentVersion");
                    if(keySQL != null)
                    {
                        string sVer = Convert.ToString(keySQL.GetValue("CurrentVersion"));

                        if(sVer.StartsWith("8."))
                        {
                            // sql 2000
                            sVer = Convert.ToString(keySQL.GetValue("CSDVersion"));
                        }

                        sqlServer.Version = sVer;
                    }
                }
                else
                {
                    RegistryKey keySQL = hklm.OpenSubKey("SOFTWARE\\Microsoft\\Microsoft SQL Server\\" + sqlServer.InstanceName + "\\MSSQLSERVER\\CurrentVersion");
                    if(keySQL != null)
                    {
                        string sVer = Convert.ToString(keySQL.GetValue("CurrentVersion"));

                        if (sVer.StartsWith("8."))
                        {
                            // sql 2000
                            sVer = Convert.ToString(keySQL.GetValue("CSDVersion"));
                        }

                        sqlServer.Version = sVer;
                    }
                }
            }
        }

        private void ReadEdition()
        {
            RegistryKey hklm = Registry.LocalMachine;

            foreach (SQLServerItem sqlServer in _scanned_sql_server)
            {
                RegistryKey sqlKey = hklm.OpenSubKey("SOFTWARE\\Microsoft\\Microsoft SQL Server");

                if(sqlKey != null)
                {
                    RegistryKey keyInstanceNames = sqlKey.OpenSubKey("Instance Names");

                    if(keyInstanceNames != null)
                    {
                        if (keyInstanceNames.OpenSubKey("SQL") != null)
                        {
                            string sVal = Convert.ToString(keyInstanceNames.OpenSubKey("SQL").GetValue(sqlServer.InstanceName));

                            if(!string.IsNullOrEmpty(sVal))
                            {
                                RegistryKey instEdition = sqlKey.OpenSubKey(sVal + "\\Setup");
                                if (instEdition != null)
                                {
                                    string sEdition = Convert.ToString(instEdition.GetValue("Edition"));
                                    sqlServer.Edition = sEdition;
                                }
                                else
                                {
                                    sqlServer.Edition = Properties.Resources.SQLNoEdition;    
                                }
                            }
                            else
                            {
                                sqlServer.Edition = Properties.Resources.SQLNoEdition;
                            }
                        } 
                        else
                        {
                            RegistryKey keySetup = sqlKey.OpenSubKey("Setup");
                            if (keySetup != null)
                            {
                                string sEdition = Convert.ToString(keySetup.GetValue("Edition"));
                                sqlServer.Edition = sEdition;
                            }
                        }

                    }
                    else
                    {
                        RegistryKey keySetup = sqlKey.OpenSubKey("Setup");
                        if (keySetup != null)
                        {
                            string sEdition = Convert.ToString(keySetup.GetValue("Edition"));
                            sqlServer.Edition = sEdition;
                        }
                    }
                }
            }
        }

        private void ReadLicenseModes()
        {
            RegistryKey hklm = Registry.LocalMachine;

            foreach (SQLServerItem sqlServer in _scanned_sql_server)
            {
                RegistryKey sqlKey = hklm.OpenSubKey("SOFTWARE\\Microsoft\\Microsoft SQL Server");

                if (sqlKey != null)
                {
                    foreach(string subKeyName in sqlKey.GetSubKeyNames())
                    {
                        try
                        {
                            //Int64 versInfo = Int64.Parse(subKeyName);

                            RegistryKey licenseInfokey = sqlKey.OpenSubKey(subKeyName + "\\MSSQLLicenseInfo");

                            if(licenseInfokey != null)
                            {
                                // subkeyname=90 => MSSQL9.00
                                RegistryKey licenseDetailsKey = licenseInfokey.OpenSubKey("MSSQL" + subKeyName.Substring(0, subKeyName.Length - 1) + ".00");
                                if(licenseDetailsKey != null)
                                {
                                    string sMode = Convert.ToString(licenseDetailsKey.GetValue("Mode"));
                                    string sConc = Convert.ToString(licenseDetailsKey.GetValue("ConcurrentLimit"));

                                    if(!string.IsNullOrEmpty(sMode))
                                    {
                                        if (sMode == "0")
                                            sqlServer.LicenseMode = "Per-Seat:No-of-Licenses:" + sConc;
                                        else if (sMode == "2")
                                            sqlServer.LicenseMode = "Per-Processor:No-of-Licenses:" + sConc;
                                        else if (sMode == "1")
                                            sqlServer.LicenseMode = "Use Control pannel to Change the License Mode";
                                    }
                                    else
                                    {
                                        sqlServer.LicenseMode = Properties.Resources.SQLNoLicense;    
                                    }
                                } 
                                else
                                {
                                    sqlServer.LicenseMode = Properties.Resources.SQLNoLicense;    
                                }
                            } 
                            else
                            {
                                sqlServer.LicenseMode = Properties.Resources.SQLNoLicense;
                            }
                        }
                        catch
                        {
                            // not a version key
                        }
                    }
                }
            }
        }
        #endregion

        #region Attribs

        private List<SQLServerItem> _scanned_sql_server = new List<SQLServerItem>();
        #endregion
    }
}
