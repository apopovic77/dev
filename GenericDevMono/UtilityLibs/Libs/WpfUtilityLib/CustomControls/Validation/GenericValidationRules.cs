﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;

namespace Logicx.WpfUtility.CustomControls.Validation
{
    public class IntegerValidationRule : ValidationRule
    {
        public override ValidationResult Validate(object value, System.Globalization.CultureInfo cultureInfo)
        {
            if (value is string)
            {
                if (!string.IsNullOrEmpty((string)value))
                {
                    try
                    {
                        int test = int.Parse(Convert.ToString(value), cultureInfo.NumberFormat);

                        if (!(MinValue == 0 && MaxValue == 0))
                        {
                            if (test < MinValue || test > MaxValue)
                            {
                                return new ValidationResult(false, ErrorMessage);
                            }
                        }
                    }
                    catch
                    {
                        return new ValidationResult(false, ErrorMessage);
                    }
                }
            }
            else if (value is int)
            {
                int test = (int)value;

                if (!(MinValue == 0 && MaxValue == 0))
                {
                    if (test < MinValue || test > MaxValue)
                    {
                        return new ValidationResult(false, ErrorMessage);
                    }
                }
            }

            return new ValidationResult(true, "");
        }

        public string ErrorMessage
        {
            get { return _error_message; }
            set { _error_message = value; }
        }

        public int MinValue
        {
            get { return _min; }
            set { _min = value; }
        }

        public int MaxValue
        {
            get { return _max; }
            set { _max = value; }
        }

        private int _min = 0;
        private int _max = 0;
        private string _error_message = "Die Eingabe muss eine Zahl sein.";
    }
}
