<!doctype html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <title>Introduction to Responsive Web Design</title>

    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
    <link rel="icon" type="image/png" href="favicon.png" />

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="./dist/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="./dist/css/bootstrap-theme.min.css">

    <!-- Latest compiled and minified JavaScript -->
    <script src="./dist/js/bootstrap.min.js"></script>



    <style>
        @font-face {
            font-family: din1;
            src: url("DIN Alternate Light.ttf");
        }

        @font-face {
            font-family: din2;
            src: url("DIN Alternate.ttf");
        }

        @font-face {
            font-family: din3;
            src: url("DIN Alternate Medium.ttf");
        }

        html, body {
            width:100%;
            height:100%;
        }
        body {

            padding: 0;
            margin: 0;
            font-family: 'din1';
            color: gray;
            font-size: 4vw;
        }

        .image {
            text-align: center;
        }

        img {
            width: 500px;
        }

        .div1 {
            background-color: gray; padding-top: 100px;padding-bottom: 100px;width:100%
        }

        .div2 {
            background-color: white; padding-top: 100px;padding-bottom: 100px;width:100%
        }

    </style>

</head>
<body style="background-color: white">



    
<?php

$files1 = scandir('./');

for ($i = 0; $i < count($files1); $i++)
{
	$path_parts = pathinfo('./' & $files1[$i]);
    
    if($path_parts['extension'] == "php")
        echo $path_parts['extension'];
    
    
}

?>



    <div style="background-color: gray; padding: 100px;">
        <div class="image">
            <img src="./13.png" />
                        <div style="margin-top: 10px; color: #83BB26;font-family:din2">Webseite in Bauphase 2</div>
            <div style="margin-top: 10px; color: white;font-family:din1">tel: 0664 2030480<br>email: office@pdrei.at</div>
        </div>
    </div>

    <div style="background-color: gray; padding: 100px;">
        <div class="image">
            <img src="./3.png" />
                        <div style="margin-top: 10px; color: #83BB26;font-family:din2">Webseite in Bauphase 2</div>
            <div style="margin-top: 10px; color: white;font-family:din1">tel: 0664 2030480<br>email: office@pdrei.at</div>
        </div>
    </div>

    <div style="background-color: white; padding: 100px;">
        <div class="image">
            <img src="./3.png" />
                        <div style="margin-top: 10px; color: #83BB26;font-family:din2">Webseite in Bauphase 2</div>
            <div style="margin-top: 10px; color: gray;font-family:din1">tel: 0664 2030480<br>email: office@pdrei.at</div>
        </div>
    </div>

    <div style="background-color: gray; padding: 100px;">
        <div class="image">
            <img src="./42.png" />
            <div style="margin-top: 10px; color: white;font-family:din2">Webseite in Bauphase 2<br />Status >20%</div>
        </div>
    </div>

    <div style="background-color: white; padding: 100px;">
        <div class="image">
            <img src="./42.png" />
            <div style="margin-top: 10px; color: #E62172;font-family:din2">Webseite in Bauphase 2<br />Status >20%</div>
        </div>
    </div>


    <div style="background-color: gray; padding: 100px;">
        <div class="image">
            <img src="./1.png" />
            <div style="margin-top: 10px; color: white;font-family:din2">Webseite in Bauphase 2</div>
            <div style="margin-top: 10px; color: #83BB26;font-family:din1">tel: 0664 2030480<br>email: office@pdrei.at</div>
        </div>
    </div>
    
    <div style="background-color: white; padding: 100px;">
        <div class="image">
            <img src="./1.png" />
            <div style="margin-top: 10px; color: gray;font-family:din2">Webseite in Bauphase 2</div>
            <div style="margin-top: 10px; color: #83BB26;font-family:din1">tel: 0664 2030480<br>email: office@pdrei.at</div>
        </div>
    </div>

    <div style="background-color: gray; padding: 100px;">
        <div class="image">
            <img src="./7.png" />
            <div style="margin-top: 10px; color: white;font-family:din2">Webseite in Bauphase 2</div>
            <div style="margin-top: 10px; color: black;font-family:din1">tel: 0664 2030480<br>emails: office@pdrei.at</div>
        </div>
    </div>
    
    <div style="background-color: gray; padding: 100px;">
        <div class="image">
            <img src="./5.png" />
            <div style="margin-top: 10px; color: white;font-family:din2">Webseite in Bauphase 2</div>
            <div style="margin-top: 10px; color: black;font-family:din1">tel: 0664 2030480<br>email: office@pdrei.at</div>
        </div>
    </div>

    <div style="background-color: white; padding: 100px;">
        <div class="image">
            <img src="./5.png" />
            <div style="margin-top: 10px; color: gray;font-family:din2">Webseite in Bauphase 2</div>
            <div style="margin-top: 10px; color: #83BB26;font-family:din1">tel: 0664 2030480<br>email: office@pdrei.at</div>
        </div>
    </div>

    <div style="background-color: gray; padding: 100px;">
        <div class="image">
            <img src="./10.png" />
            <div style="margin-top: 10px; color: white;font-family:din2">Webseite in Bauphase 2</div>
            <div style="margin-top: 10px; color: #83BB26;font-family:din1">tel: 0664 2030480<br>email: office@pdrei.at</div>
        </div>
    </div>

    <div style="background-color: white; padding: 100px;">
        <div class="image">
            <img src="./10.png" />
            <div style="margin-top: 10px; color: gray;font-family:din2">Webseite in Bauphase 2</div>
            <div style="margin-top: 10px; color: #83BB26;font-family:din2">tel: 0664 2030480<br>email: office@pdrei.at</div>
        </div>
    </div>

    <div style="background-color: gray; padding: 100px;">
        <div class="image">
            <img src="./2.png" />
            <div style="margin-top: 10px; color: white;">Webseite in Bauphase 2</div>
            <div style="margin-top: 10px; color: #83BB26;">Webseite in Bauphase 2</div>
            <div style="margin-top: 10px; color: black;">Webseite in Bauphase 2</div>
            <div style="margin-top: 10px; color: #E62172;">Webseite Bauphase >2 erreicht</div>
            <div style="margin-top: 10px; color: #E62172; font-family: 'DIN MediumAlternate'">Webseite Bauphase >2 erreicht</div>
        </div>
    </div>

    <div style="background-color: white; padding: 100px;">
        <div class="image">
            <img src="./2.png" />
            <div style="margin-top: 10px; color: white;">Webseite in Bauphase 2</div>
            <div style="margin-top: 10px; color: gray;">Webseite in Bauphase 2</div>
            <div style="margin-top: 10px; color: black;">Webseite in Bauphase 2</div>
            <div style="margin-top: 10px; color: darkgreen;">Webseite in Bauphase 2</div>
        </div>
    </div>

    <div style="background-color: gray; padding: 100px;">
        <div class="image">
            <img src="./3.png" />
            <div style="margin-top: 10px; color: white;">Webseite in Bauphase 2</div>
            <div style="margin-top: 10px; color: #83BB26;">Webseite in Bauphase 2</div>
            <div style="margin-top: 10px; color: black;">Webseite in Bauphase 2</div>
            <div style="margin-top: 10px; color: #E62172;">Webseite Bauphase >2 erreicht</div>
            <div style="margin-top: 10px; color: #E62172; font-family: 'DIN MediumAlternate'">Webseite Bauphase >2 erreicht</div>
        </div>
    </div>

    <div style="background-color: white; padding: 100px;">
        <div class="image">
            <img src="./3.png" />
            <div style="margin-top: 10px; color: white;">Webseite in Bauphase 2</div>
            <div style="margin-top: 10px; color: gray;">Webseite in Bauphase 2</div>
            <div style="margin-top: 10px; color: black;">Webseite in Bauphase 2</div>
            <div style="margin-top: 10px; color: darkgreen;">Webseite in Bauphase 2</div>
        </div>
    </div>

    <div style="background-color: gray; padding: 100px;">
        <div class="image">
            <img src="./42.png" />
            <div style="margin-top: 10px; color: white;">Webseite in Bauphase 2</div>
            <div style="margin-top: 10px; color: #83BB26;">Webseite in Bauphase 2</div>
            <div style="margin-top: 10px; color: black;">Webseite in Bauphase 2</div>
            <div style="margin-top: 10px; color: #E62172;">Webseite Bauphase >2 erreicht</div>
            <div style="margin-top: 10px; color: #E62172; font-family: 'DIN MediumAlternate'">Webseite Bauphase >2 erreicht</div>
        </div>
    </div>

    <div style="background-color: white; padding: 100px;">
        <div class="image">
            <img src="./42.png" />
            <div style="margin-top: 10px; color: white;">Webseite in Bauphase 2</div>
            <div style="margin-top: 10px; color: gray;">Webseite in Bauphase 2</div>
            <div style="margin-top: 10px; color: black;">Webseite in Bauphase 2</div>
            <div style="margin-top: 10px; color: darkgreen;">Webseite in Bauphase 2</div>
        </div>
    </div>

    <div style="background-color: gray; padding: 100px;">
        <div class="image">
            <img src="./7.png" />
            <div style="margin-top: 10px; color: white;">Webseite in Bauphase 2</div>
            <div style="margin-top: 10px; color: #83BB26;">Webseite in Bauphase 2</div>
            <div style="margin-top: 10px; color: black;">Webseite in Bauphase 2</div>
            <div style="margin-top: 10px; color: #E62172;">Webseite Bauphase >2 erreicht</div>
            <div style="margin-top: 10px; color: #E62172; font-family: 'DIN MediumAlternate'">Webseite Bauphase >2 erreicht</div>
        </div>
    </div>

    <div style="background-color: white; padding: 100px;">
        <div class="image">
            <img src="./7.png" />
            <div style="margin-top: 10px; color: white;">Webseite in Bauphase 2</div>
            <div style="margin-top: 10px; color: gray;">Webseite in Bauphase 2</div>
            <div style="margin-top: 10px; color: black;">Webseite in Bauphase 2</div>
            <div style="margin-top: 10px; color: darkgreen;">Webseite in Bauphase 2</div>
        </div>
    </div>

    <div style="background-color: gray; padding: 100px;">
        <div class="image">
            <img src="./5.png" />
            <div style="margin-top: 10px; color: white;">Webseite in Bauphase 2</div>
            <div style="margin-top: 10px; color: #83BB26;">Webseite in Bauphase 2</div>
            <div style="margin-top: 10px; color: black;">Webseite in Bauphase 2</div>
            <div style="margin-top: 10px; color: #E62172;">Webseite Bauphase >2 erreicht</div>
            <div style="margin-top: 10px; color: #E62172; font-family: 'DIN MediumAlternate'">Webseite Bauphase >2 erreicht</div>
        </div>
    </div>

    <div style="background-color: white; padding: 100px;">
        <div class="image">
            <img src="./5.png" />
            <div style="margin-top: 10px; color: white;">Webseite in Bauphase 2</div>
            <div style="margin-top: 10px; color: gray;">Webseite in Bauphase 2</div>
            <div style="margin-top: 10px; color: black;">Webseite in Bauphase 2</div>
            <div style="margin-top: 10px; color: darkgreen;">Webseite in Bauphase 2</div>
        </div>
    </div>

    <div style="background-color: gray; padding: 100px;">
        <div class="image">
            <img src="./6.png" />
            <div style="margin-top: 10px; color: white;">Webseite in Bauphase 2</div>
            <div style="margin-top: 10px; color: #83BB26;">Webseite in Bauphase 2</div>
            <div style="margin-top: 10px; color: black;">Webseite in Bauphase 2</div>
            <div style="margin-top: 10px; color: #E62172;">Webseite Bauphase >2 erreicht</div>
            <div style="margin-top: 10px; color: #E62172; font-family: 'DIN MediumAlternate'">Webseite Bauphase >2 erreicht</div>
        </div>
    </div>

    <div style="background-color: white; padding: 100px;">
        <div class="image">
            <img src="./6.png" />
            <div style="margin-top: 10px; color: white;">Webseite in Bauphase 2</div>
            <div style="margin-top: 10px; color: gray;">Webseite in Bauphase 2</div>
            <div style="margin-top: 10px; color: black;">Webseite in Bauphase 2</div>
            <div style="margin-top: 10px; color: darkgreen;">Webseite in Bauphase 2</div>
        </div>
    </div>

    <div style="background-color: gray; padding: 100px;">
        <div class="image">
            <img src="./10.png" />
            <div style="margin-top: 10px; color: white;">Webseite in Bauphase 2</div>
            <div style="margin-top: 10px; color: #83BB26;">Webseite in Bauphase 2</div>
            <div style="margin-top: 10px; color: black;">Webseite in Bauphase 2</div>
            <div style="margin-top: 10px; color: #E62172;">Webseite Bauphase >2 erreicht</div>
            <div style="margin-top: 10px; color: #E62172; font-family: 'DIN MediumAlternate'">Webseite Bauphase >2 erreicht</div>
        </div>
    </div>

    <div style="background-color: white; padding: 100px;">
        <div class="image">
            <img src="./10.png" />
            <div style="margin-top: 10px; color: white;">Webseite in Bauphase 2</div>
            <div style="margin-top: 10px; color: gray;">Webseite in Bauphase 2</div>
            <div style="margin-top: 10px; color: black;">Webseite in Bauphase 2</div>
            <div style="margin-top: 10px; color: darkgreen;">Webseite in Bauphase 2</div>
        </div>
    </div>

    <div style="background-color: gray; padding: 100px;">
        <div class="image">
            <img src="./1.png" />
            <div style="margin-top: 10px; color: white;">Webseite in Bauphase 2</div>
            <div style="margin-top: 10px; color: gray;">Webseite in Bauphase 2</div>
            <div style="margin-top: 10px; color: black;">Webseite in Bauphase 2</div>
            <div style="margin-top: 10px; color: darkgreen;">Webseite in Bauphase 2</div>
        </div>
    </div>

    <div style="background-color: white; padding: 100px;">
        <div class="image">
            <img src="./1.png" />
            <div style="margin-top: 10px; color: white;">Webseite in Bauphase 2</div>
            <div style="margin-top: 10px; color: gray;">Webseite in Bauphase 2</div>
            <div style="margin-top: 10px; color: black;">Webseite in Bauphase 2</div>
            <div style="margin-top: 10px; color: darkgreen;">Webseite in Bauphase 2</div>
        </div>
    </div>
    <div style="background-color: gray; padding: 100px;">
        <div class="image">
            <img src="./1.png" />
            <div style="margin-top: 10px; color: white;">Webseite in Bauphase 2</div>
            <div style="margin-top: 10px; color: gray;">Webseite in Bauphase 2</div>
            <div style="margin-top: 10px; color: black;">Webseite in Bauphase 2</div>
            <div style="margin-top: 10px; color: darkgreen;">Webseite in Bauphase 2</div>
        </div>
    </div>

    <div style="background-color: white; padding: 100px;">
        <div class="image">
            <img src="./1.png" />
            <div style="margin-top: 10px; color: white;">Webseite in Bauphase 2</div>
            <div style="margin-top: 10px; color: gray;">Webseite in Bauphase 2</div>
            <div style="margin-top: 10px; color: black;">Webseite in Bauphase 2</div>
            <div style="margin-top: 10px; color: darkgreen;">Webseite in Bauphase 2</div>
        </div>
    </div>

    <div style="background-color: gray; padding: 100px;">
        <div class="image">
            <img src="./1.png" />
            <div style="margin-top: 10px; color: white;">Webseite in Bauphase 2</div>
            <div style="margin-top: 10px; color: gray;">Webseite in Bauphase 2</div>
            <div style="margin-top: 10px; color: black;">Webseite in Bauphase 2</div>
            <div style="margin-top: 10px; color: darkgreen;">Webseite in Bauphase 2</div>
        </div>
    </div>

    <div style="background-color: white; padding: 100px;">
        <div class="image">
            <img src="./1.png" />
            <div style="margin-top: 10px; color: white;">Webseite in Bauphase 2</div>
            <div style="margin-top: 10px; color: gray;">Webseite in Bauphase 2</div>
            <div style="margin-top: 10px; color: black;">Webseite in Bauphase 2</div>
            <div style="margin-top: 10px; color: darkgreen;">Webseite in Bauphase 2</div>
        </div>
    </div>

    <div style="background-color: gray; padding: 100px;">
        <div class="image">
            <img src="./1.png" />
            <div style="margin-top: 10px; color: white;">Webseite in Bauphase 2</div>
            <div style="margin-top: 10px; color: gray;">Webseite in Bauphase 2</div>
            <div style="margin-top: 10px; color: black;">Webseite in Bauphase 2</div>
            <div style="margin-top: 10px; color: darkgreen;">Webseite in Bauphase 2</div>
        </div>
    </div>

    <div style="background-color: white; padding: 100px;">
        <div class="image">
            <img src="./1.png" />
            <div style="margin-top: 10px; color: white;">Webseite in Bauphase 2</div>
            <div style="margin-top: 10px; color: gray;">Webseite in Bauphase 2</div>
            <div style="margin-top: 10px; color: black;">Webseite in Bauphase 2</div>
            <div style="margin-top: 10px; color: darkgreen;">Webseite in Bauphase 2</div>
        </div>
    </div>

    <div style="background-color: gray; padding: 100px;">
        <div class="image">
            <img src="./1.png" />
            <div style="margin-top: 10px; color: white;">Webseite in Bauphase 2</div>
            <div style="margin-top: 10px; color: gray;">Webseite in Bauphase 2</div>
            <div style="margin-top: 10px; color: black;">Webseite in Bauphase 2</div>
            <div style="margin-top: 10px; color: darkgreen;">Webseite in Bauphase 2</div>
        </div>
    </div>

    <div style="background-color: white; padding: 100px;">
        <div class="image">
            <img src="./1.png" />
            <div style="margin-top: 10px; color: white;">Webseite in Bauphase 2</div>
            <div style="margin-top: 10px; color: gray;">Webseite in Bauphase 2</div>
            <div style="margin-top: 10px; color: black;">Webseite in Bauphase 2</div>
            <div style="margin-top: 10px; color: darkgreen;">Webseite in Bauphase 2</div>
        </div>
    </div>

    <div style="background-color: gray; padding: 100px;">
        <div class="image">
            <img src="./2.png" />
            <div style="margin-top: 10px; color: gray;">Webseite in Bauphase 2</div>
        </div>
    </div>

    <div style="background-color: gray; padding: 100px;">
        <div class="image">
            <img src="./2.png" />
            <div style="margin-top: 10px; color: white;">Webseite in Bauphase 2</div>
        </div>
    </div>

    <div style="background-color: gray; padding: 100px;">
        <div class="image">
            <img src="./3.png" />
            <div style="margin-top: 10px;">Webseite in Bauphase 2</div>
        </div>
    </div>

    <div style="background-color: gray; padding: 100px;">
        <div class="image">
            <img src="./42.png" />
            <div style="margin-top: 10px;">Webseite in Bauphase 2</div>
        </div>
    </div>

    <div style="background-color: gray; padding: 100px;">
        <div class="image">
            <img src="./5.png" />
            <div style="margin-top: 10px;">Webseite in Bauphase 2</div>
        </div>
    </div>

    <div style="background-color: gray; padding: 100px;">
        <div class="image">
            <img src="./6.png" />
            <div style="margin-top: 10px;">Webseite in Bauphase 2</div>
        </div>
    </div>

    <div style="background-color: gray; padding: 100px;">
        <div class="image">
            <img src="./7.png" />
            <div style="margin-top: 10px;">Webseite in Bauphase 2</div>
        </div>
    </div>

    <div style="background-color: gray; padding: 100px;">
        <div class="image">
            <img src="./8.png" />
            <div style="margin-top: 10px;">Webseite in Bauphase 2</div>
        </div>
    </div>

    <div style="background-color: gray; padding: 100px;">
        <div class="image">
            <img src="./9.png" />
            <div style="margin-top: 10px;">Webseite in Bauphase 2</div>
        </div>
    </div>

    <div style="background-color: gray; padding: 100px;">
        <div class="image">
            <img src="./10.png" />
            <div style="margin-top: 10px;">Webseite in Bauphase 2</div>
        </div>
    </div>

</body>
</html>

