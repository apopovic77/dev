using System;
using System.Collections.Generic;
using System.Text;
using Logicx.Geo.Geometries;
using Microsoft.DirectX;
using Microsoft.DirectX.Direct3D;
using D3d = Microsoft.DirectX.Direct3D;
using System.Drawing;
using Logicx.DirectX.Objects;
using GeoLib;
using MathLib;
using Logicx.DirectX.Engine;

namespace Logicx.DirectX.Objects.GeometricPrimitives
{
    public class Line3d : Object3d
    {

        #region Attributes
        protected bool _simple_line_drawing = false;
        protected SimpleLine _line;
        protected D3d.CustomVertex.PositionColored[] _vertices;
        protected int _edge_count = 0;
        protected Color _linecolor = Color.Blue;
        protected bool _dynamic_line_width = false;
        protected float _current_line_width = 1f;
        protected LinearEquation _le;
        protected float _le_k, _le_d;
        float _le_initial_scale = 20f;
        float _le_smallest_scale = 2f;
        protected Material _line_mat;
        #endregion


        public Line3d()
        {
        }

        public SimpleLine Line
        {
            set
            {
                _line = value;
                _line.DeleteIdenticalLineVertices();
                SetLineVertices();
            }
            get
            {
                return _line;
            }
        }


        public Color Linecolor
        {
            set
            {
                _linecolor = value;
                _line_mat = new Material();
                _line_mat.Ambient = _line_mat.Diffuse = value;

            }
            get
            {
                return _linecolor;
            }
        }
		
		public bool DynamicLineWidth
		{
			set 
            {
				_dynamic_line_width = value;
			}
			get 
            {
				return _dynamic_line_width;
			}
		}
		
		public float LineWidth
		{
			set {
				_current_line_width = value;
			}
			
			get {
				return _current_line_width;
			}
		}

        public bool SimpleLineDrawing
		{
			set {
				_simple_line_drawing = value;
			}
			
			get {
				return _simple_line_drawing;
			}
		}
		
		public float LineScaleInitial
		{
			set {
				_le_initial_scale = value;
			}
			
			get {
				return _le_initial_scale;
			}
		}
		
		public float LineScaleSmallest
		{
			set {
				_le_smallest_scale = value;
			}
			
			get {
				return _le_smallest_scale;
			}
		}

        public override void Init()
        {
        }

        private float GetDynamicLineWidth()
        {
            if (_le == null) { 
                //create linear equation
                _le = new LinearEquation((_le_initial_scale - _le_smallest_scale) / -12000, _le_smallest_scale);
            }
            if (Engine3d.Camera!= null)
                return _le.GetFx(Engine3d.Camera.Position.Z);
            else
                return _current_line_width;
        }

        private void SetLineVertices()
        {
            if (_line.Vertices == null)
                return;

            //get the new line width if needed
            if (_dynamic_line_width)
            {
                float line_width = GetDynamicLineWidth();

                //check up ein update notwendig
                if (_current_line_width == line_width)
                    return;

                _current_line_width = line_width;
            }

            //create vertices with respect to linewidth given
            if (_line.Vertices.Count >= 2)
            {
                if (!_simple_line_drawing)
                {
                    Vector2f[] verts = _line.GetTriangledShapeArroundLine(_current_line_width);
                    _vertices = new Microsoft.DirectX.Direct3D.CustomVertex.PositionColored[verts.Length];
                    for (int i = 0; i < verts.Length; i++)
                    {
                        _vertices[i].Color = _linecolor.ToArgb();
                        _vertices[i].X = verts[i].X;
                        _vertices[i].Y = verts[i].Y;
                        _vertices[i].Z = 0;
                    }
                }
                else 
                {
                    _vertices = new Microsoft.DirectX.Direct3D.CustomVertex.PositionColored[_line.Count];
                    for (int i = 0; i < _line.Count; i++)
                    {
                        _vertices[i].Color = _linecolor.ToArgb();
                        _vertices[i].X = _line[i].X;
                        _vertices[i].Y = _line[i].Y;
                        _vertices[i].Z = 0;
                    }
                }
                _edge_count = _line.Vertices.Count - 1;
            }
        }

        public void Update(float elapsedTime)
        {
            if (!_dynamic_line_width)
                return;

            ////mach das update nur alle 25 frames
            //if (Engine.RenderFrame % Engine.FontCullCheckPeriodicity != 0)
            //    return;

            //create new vertices with respect to new line width
            SetLineVertices();
        }

        public override void Render(float elapsedTime)
        {
            Update(elapsedTime);

            Engine3d.Device3d.Transform.World = Microsoft.DirectX.Matrix.Identity;
            Engine3d.Device3d.VertexFormat = D3d.CustomVertex.PositionColored.Format;
            Engine3d.Device3d.RenderState.FillMode = Microsoft.DirectX.Direct3D.FillMode.Solid;
            Engine3d.Device3d.Material = _line_mat;

            if (_edge_count > 0 && !_simple_line_drawing)
            {
                Engine3d.Device3d.DrawUserPrimitives(Microsoft.DirectX.Direct3D.PrimitiveType.TriangleStrip, _vertices.Length - 2, _vertices);
            }
            else if(_edge_count > 0) 
            {
                Engine3d.Device3d.DrawUserPrimitives(Microsoft.DirectX.Direct3D.PrimitiveType.LineStrip, _vertices.Length - 1, _vertices);
            }
        }
    }
}
