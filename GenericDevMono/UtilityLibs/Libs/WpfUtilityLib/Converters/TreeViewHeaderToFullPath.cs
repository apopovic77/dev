﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Markup;

namespace Logicx.WpfUtility.Converters
{
    public class TreeViewHeaderToFullPath : MarkupExtension
    {
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return new GetParentAndReturnFullPath();
        }
    }

    public class GetParentAndReturnFullPath : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            StringBuilder myFullPath = new StringBuilder();

            try
            {
                TreeViewItem currentItem = value as TreeViewItem;
                if (currentItem != null)
                {
                    TreeViewItem parentItem = currentItem.Parent as TreeViewItem;
                    while (parentItem != null)
                    {
                        myFullPath.Insert(0, parentItem.Header + " -> ");
                        parentItem = parentItem.Parent as TreeViewItem;
                    }
                    myFullPath.Append(currentItem.Header.ToString());
                }
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
            return myFullPath.ToString();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }
    }  
}
