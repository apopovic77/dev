﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using PoCWebApp.Config;

namespace PoCWebApp.Data.Infrastructure
{
    public class DbInitializer : IDatabaseInitializer<PoCPrinterContext>
    {
        public void InitializeDatabase(PoCPrinterContext context)
        {

            bool compatiblemodel = true;
            bool dbexists = false;

            using (var dbcontext = new PoCPrinterContext())
            {
                if (dbcontext.Database.Exists())
                {
                    dbexists = true;
                    compatiblemodel = dbcontext.Database.CompatibleWithModel(true);
                }
            }

            if (!compatiblemodel || !dbexists)
            {
                if (!AppConfig.AutomaticDbMigrationsEnabled)
                    throw new MigrationsDisabledException("Automatic migrations are disabled in the configuration file. Cannot create or update database!");
            }

            if (!compatiblemodel && dbexists)
            {
                Database.SetInitializer<PoCPrinterContext>(new MigrateDatabaseToLatestVersion<PoCPrinterContext, PoCWebApp.Data.Migrations.Configuration>());

                var dbmigrator = new DbMigrator(new PoCWebApp.Data.Migrations.Configuration());
                dbmigrator.Update();
            }


            using (var dbcontext = new PoCPrinterContext())
            {
                if (!dbcontext.Database.Exists())
                {
                    dbcontext.Database.Create();

                    InsertSeedData(dbcontext);

                }
            }
        }


        /// <summary>
        /// This method inserts the default data in the database when its created for the first time
        /// Then Context will help us to make changes
        /// </summary>
        /// <param name="dbcontext"></param>
        public void InsertSeedData(PoCPrinterContext dbcontext, bool isCreatingDatbase = true)
        {
            var seedTime = DateTime.Now.ToUniversalTime();
            var assemblyVersion = this.GetType().Assembly.GetName().Version.ToString();

            //dbcontext.InstanceTypes.AddOrUpdate(
            //    p => p.Name,
            //    new Models.InstanceType { Name = "Production", LastUpdate = seedTime },
            //    new Models.InstanceType { Name = "Test", LastUpdate = seedTime },
            //    new Models.InstanceType { Name = "Development", LastUpdate = seedTime }
            //    );

            //if (!dbcontext.GlobeVersion.Any(p => p.Version == assemblyVersion))
            //    dbcontext.GlobeVersion.Add(new Models.GlobeVersion { Version = assemblyVersion, Description = "" });

            //// do NOT use AddOrUpdate because it is overwriting 
            //// generated ApiKeys

            ////dbcontext.Applications.AddOrUpdate(
            ////    p => p.Name,
            ////    new Models.Application { Name = "PackMan", StandardTheme = "PackMan" },
            ////    new Models.Application { Name = "Production Monitor", StandardTheme = "ProductionMonitor" },
            ////    new Models.Application { Name = "FA Reports", StandardTheme = "FAReports" }
            ////    );
            //if (!dbcontext.Applications.Any(p => p.Name == "Xtend"))
            //    dbcontext.Applications.Add(new Models.Application { Name = "Xtend", StandardTheme = "Xtend" });
            //if (!dbcontext.Applications.Any(p => p.Name == "PackMan"))
            //    dbcontext.Applications.Add(new Models.Application { Name = "PackMan", StandardTheme = "PackMan" });
            //if (!dbcontext.Applications.Any(p => p.Name == "Production Monitor"))
            //    dbcontext.Applications.Add(new Models.Application { Name = "Production Monitor", StandardTheme = "ProductionMonitor" });
            //if (!dbcontext.Applications.Any(p => p.Name == "FA Reports"))
            //    dbcontext.Applications.Add(new Models.Application { Name = "FA Reports", StandardTheme = "FAReports" });
            //if (!dbcontext.Applications.Any(p => p.Name == "eFreight"))
            //    dbcontext.Applications.Add(new Models.Application { Name = "eFreight", StandardTheme = "eFreight" });

            //dbcontext.SaveChanges();
        }
    }
}