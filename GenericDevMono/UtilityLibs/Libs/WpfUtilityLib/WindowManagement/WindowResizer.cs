﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows;
using System.Windows.Input;

namespace Logicx.WpfUtility.WindowManagement
{
    public class WindowResizer
    {
        private Window target = null;

        private bool resizeRight = false;
        private bool resizeLeft = false;
        private bool resizeUp = false;
        private bool resizeDown = false;

        private Dictionary<UIElement, short> leftElements = new Dictionary<UIElement, short>();
        private Dictionary<UIElement, short> rightElements = new Dictionary<UIElement, short>();
        private Dictionary<UIElement, short> upElements = new Dictionary<UIElement, short>();
        private Dictionary<UIElement, short> downElements = new Dictionary<UIElement, short>();

        private PointAPI resizePoint = new PointAPI();
        private Size resizeSize = new Size();
        private Point resizeWindowPoint = new Point();

        private delegate void RefreshDelegate();

        public WindowResizer(Window target)
        {
            this.target = target;

            if (target == null)
            {
                throw new Exception("Invalid Window handle");
            }
        }

        #region add resize components
        private void connectMouseHandlers(UIElement element)
        {
            element.MouseLeftButtonDown += new MouseButtonEventHandler(element_MouseLeftButtonDown);
            element.MouseEnter += new MouseEventHandler(element_MouseEnter);
            element.MouseLeave += new MouseEventHandler(FinishResize);
        }

        public void addResizerRight(UIElement element)
        {
            connectMouseHandlers(element);
            rightElements.Add(element, 0);
        }

        public void addResizerLeft(UIElement element)
        {
            connectMouseHandlers(element);
            leftElements.Add(element, 0);
        }

        public void addResizerUp(UIElement element)
        {
            connectMouseHandlers(element);
            upElements.Add(element, 0);
        }

        public void addResizerDown(UIElement element)
        {
            connectMouseHandlers(element);
            downElements.Add(element, 0);
        }

        public void addResizerRightDown(UIElement element)
        {
            connectMouseHandlers(element);
            rightElements.Add(element, 0);
            downElements.Add(element, 0);
        }

        public void addResizerLeftDown(UIElement element)
        {
            connectMouseHandlers(element);
            leftElements.Add(element, 0);
            downElements.Add(element, 0);
        }

        public void addResizerRightUp(UIElement element)
        {
            connectMouseHandlers(element);
            rightElements.Add(element, 0);
            upElements.Add(element, 0);
        }

        public void addResizerLeftUp(UIElement element)
        {
            connectMouseHandlers(element);
            leftElements.Add(element, 0);
            upElements.Add(element, 0);
        }
        #endregion

        #region resize handlers
        private void element_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (!AllowResize)
                return;

            if (WindowResizeStarted != null)
                WindowResizeStarted(this, new EventArgs());

            GetCursorPos(out resizePoint);
            resizeSize = new Size(target.ActualWidth, target.ActualHeight);
            resizeWindowPoint = new Point(target.Left, target.Top);

            #region updateResizeDirection
            UIElement sourceSender = (UIElement)sender;
            if (leftElements.ContainsKey(sourceSender))
            {
                resizeLeft = true;
            }
            if (rightElements.ContainsKey(sourceSender))
            {
                resizeRight = true;
            }
            if (upElements.ContainsKey(sourceSender))
            {
                resizeUp = true;
            }
            if (downElements.ContainsKey(sourceSender))
            {
                resizeDown = true;
            }
            #endregion

            Thread t = new Thread(new ThreadStart(updateSizeLoop));
            t.Name = "Mouse Position Poll Thread";
            t.Start();
        }

        private void updateSizeLoop()
        {
            try
            {
                while (resizeDown || resizeLeft || resizeRight || resizeUp)
                {
                    target.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Render, new RefreshDelegate(updateSize));
                    target.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Render, new RefreshDelegate(updateMouseDown));
                    Thread.Sleep(20);
                }

                target.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Render, new RefreshDelegate(FinishResize));
            }
            catch (Exception)
            {
            }
        }

        #region updates
        private void updateSize()
        {
            try
            {
                PointAPI p = new PointAPI();
                GetCursorPos(out p);

                //if (double.IsNaN(resizeSize.Width) || double.IsNaN(resizeSize.Height))
                //    return;

                if (resizeRight)
                {
                    if (this.resizeSize.Width - (resizePoint.X - p.X) < MinResizeWidth)
                        target.Width = MinResizeWidth;
                    else
                        target.Width = this.resizeSize.Width - (resizePoint.X - p.X);
                }

                if (resizeDown)
                {
                    if (resizeSize.Height - (resizePoint.Y - p.Y) < MinResizeHeight)
                        target.Height = MinResizeHeight;
                    else
                        target.Height = resizeSize.Height - (resizePoint.Y - p.Y);
                }

                if (resizeLeft)
                {
                    if (resizeSize.Width + (resizePoint.X - p.X) < MinResizeWidth)
                        target.Width = MinResizeWidth;
                    else
                    {
                        target.Width = resizeSize.Width + (resizePoint.X - p.X);
                        target.Left = resizeWindowPoint.X - (resizePoint.X - p.X);
                    }
                }

                if (resizeUp)
                {
                    if (resizeSize.Height + (resizePoint.Y - p.Y) < MinResizeHeight)
                        target.Height = MinResizeHeight;
                    else
                    {
                        target.Height = resizeSize.Height + (resizePoint.Y - p.Y);
                        target.Top = resizeWindowPoint.Y - (resizePoint.Y - p.Y);
                    }
                }

                if (WindowSizeUpdated != null)
                    WindowSizeUpdated(this, null);
            }
            catch (Exception ex)
            {
            }
        }

        private void updateMouseDown()
        {
            if (Mouse.LeftButton == MouseButtonState.Released)
            {
                resizeRight = false;
                resizeLeft = false;
                resizeUp = false;
                resizeDown = false;
            }
        }
        #endregion
        #endregion

        #region cursor updates
        private void element_MouseEnter(object sender, MouseEventArgs e)
        {
            if (!AllowResize)
            {
                target.Cursor = Cursors.Arrow;
                return;
            }

            bool resizeRight = false;
            bool resizeLeft = false;
            bool resizeUp = false;
            bool resizeDown = false;

            UIElement sourceSender = (UIElement)sender;
            if (leftElements.ContainsKey(sourceSender))
            {
                resizeLeft = true;
            }
            if (rightElements.ContainsKey(sourceSender))
            {
                resizeRight = true;
            }
            if (upElements.ContainsKey(sourceSender))
            {
                resizeUp = true;
            }
            if (downElements.ContainsKey(sourceSender))
            {
                resizeDown = true;
            }

            if ((resizeLeft && resizeDown) || (resizeRight && resizeUp))
            {
                setNESWCursor(sender, e);
            }
            else if ((resizeRight && resizeDown) || (resizeLeft && resizeUp))
            {
                setNWSECursor(sender, e);
            }
            else if (resizeLeft || resizeRight)
            {
                setWECursor(sender, e);
            }
            else if (resizeUp || resizeDown)
            {
                setNSCursor(sender, e);
            }
        }

        private void setWECursor(object sender, MouseEventArgs e)
        {
            target.Cursor = Cursors.SizeWE;
        }

        private void setNSCursor(object sender, MouseEventArgs e)
        {
            target.Cursor = Cursors.SizeNS;
        }

        private void setNESWCursor(object sender, MouseEventArgs e)
        {
            target.Cursor = Cursors.SizeNESW;
        }

        private void setNWSECursor(object sender, MouseEventArgs e)
        {
            target.Cursor = Cursors.SizeNWSE;
        }

        private void FinishResize(object sender, MouseEventArgs e)
        {
            if (!AllowResize)
                return;

            if (!resizeDown && !resizeLeft && !resizeRight && !resizeUp)
            {
                target.Cursor = Cursors.Arrow;
            }
        }

        private void FinishResize()
        {
            target.Cursor = Cursors.Arrow;
            if (WindowResizeComplete != null)
                WindowResizeComplete(this, new EventArgs());
        }
        #endregion

        #region Properties

        public bool AllowResize
        {
            get;
            set;
        }

        #endregion

        #region external call
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool GetCursorPos(out PointAPI lpPoint);

        private struct PointAPI
        {
            public int X;
            public int Y;
        }
        #endregion

        public event EventHandler WindowResizeStarted;
        public event EventHandler WindowResizeComplete;
        public event EventHandler WindowSizeUpdated;

        public float MinResizeWidth = 400;
        public float MinResizeHeight = 250;
    }

}
