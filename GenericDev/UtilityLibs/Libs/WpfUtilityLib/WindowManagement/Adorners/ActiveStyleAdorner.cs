﻿using System.Windows;
using System.Windows.Documents;
using System.Windows.Media;

namespace Logicx.WpfUtility.WindowManagement.Adorners
{
    public class ActiveStyleAdorner : Adorner
    {
        public ActiveStyleAdorner(FrameworkElement adorned_element, Brush render_brush, Pen render_pen)
            : base(adorned_element)
        {
            _render_brush = render_brush;
            _render_pen = render_pen;
            IsHitTestVisible = false;
            Focusable = false;
        }

        protected override void OnRender(DrawingContext drawingContext)
        {
            double width = this.AdornedElement.DesiredSize.Width;
            double height = this.AdornedElement.DesiredSize.Height;

            if(AdornedElement is FrameworkElement)
            {
                FrameworkElement frame_element = (FrameworkElement) AdornedElement;
                width = frame_element.ActualWidth;
                height = frame_element.ActualHeight;
            }

            Rect adorned_element_rect = new Rect(new Size(width, height));
            drawingContext.DrawRectangle(_render_brush, _render_pen, adorned_element_rect);
        }

        private Brush _render_brush;
        private Pen _render_pen;
    }

}
