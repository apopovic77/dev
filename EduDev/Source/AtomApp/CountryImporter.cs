﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using Google.GData.Client;
using Google.GData.Spreadsheets;

namespace AtomApp
{
    public class CountryImporter
    {
        public List<PhysicalObject> Import()
        {
            List<PhysicalObject> objects = new List<PhysicalObject>();
            
            
            Service = new SpreadsheetsService("exampleCo-exampleApp-1");
            Service.setUserCredentials("apopovic.aut@gmail.com", "#1inetpass");

            AtomLink link = InitSheet("Länder Statistik");
            WorksheetQuery ws_query = new WorksheetQuery(link.HRef.ToString());
            WorksheetFeed ws_feed = Service.Query(ws_query);


            WorksheetEntry sheet = ws_feed.Entries.Select(ws => (WorksheetEntry)ws).Where(ws => ws.Title.Text == "Countries").FirstOrDefault();
            List<CellEntry> cell_entries = GetCellEntriesCol(sheet, 1);
            List<CellEntry> cell_entries_einwohner = GetCellEntriesCol(sheet, 4);
            List<CellEntry> cell_entries_area = GetCellEntriesCol(sheet, 5);
            for (int i = 1; i < cell_entries.Count; i++)
            {
                try
                {
                    PhysicalObject obj = new PhysicalObject();
                    obj.Name = cell_entries[i].Value;
                    obj.Einwohner = Convert.ToDouble(cell_entries_einwohner[i].Value);
                    obj.Area = Convert.ToDouble(cell_entries_area[i].Value);
                    obj.Color = Color.FromRgb((byte)(255.0 * _rand.NextDouble()), (byte)(255.0 * _rand.NextDouble()), (byte)(255.0 * _rand.NextDouble()));
                    objects.Add(obj);
                }
                catch
                {
                }
            }

            return objects.OrderByDescending(o => o.Area).ToList();
        }

        public List<CellEntry> GetCellEntriesCol(WorksheetEntry worksheet, int col)
        {
            AtomLink cellFeedLink = worksheet.Links.FindService(GDataSpreadsheetsNameTable.CellRel, null);
            CellQuery cell_query = new CellQuery(cellFeedLink.HRef.ToString());
            CellFeed cell_feed = Service.Query(cell_query);
            return cell_feed.Entries.Select(c => (CellEntry)c).Where(c => c.Column == col).ToList();
        }


        public string GetCellEntryValue(WorksheetEntry worksheet, uint row, uint col)
        {
            AtomLink cellFeedLink = worksheet.Links.FindService(GDataSpreadsheetsNameTable.CellRel, null);
            CellQuery cell_query = new CellQuery(cellFeedLink.HRef.ToString());
            CellFeed cell_feed = Service.Query(cell_query);
            CellEntry cell_entry = cell_feed.Entries.Select(c => (CellEntry)c).FirstOrDefault(c => c.Row == row && c.Column == col);
            if (cell_entry != null)
                return cell_entry.Value;
            else
                return null;
        }

        public AtomLink InitSheet(string sheetname)
        {
            SpreadsheetQuery query = new SpreadsheetQuery();
            SpreadsheetFeed feed = Service.Query(query);
            SpreadsheetEntry entry1 = (SpreadsheetEntry)feed.Entries.FirstOrDefault(e => e.Title.Text == sheetname);
            AtomLink link = entry1.Links.FindService(GDataSpreadsheetsNameTable.WorksheetRel, null);
            return link;
        }

        private SpreadsheetsService Service;
        private Random _rand = new Random();
    }
}
