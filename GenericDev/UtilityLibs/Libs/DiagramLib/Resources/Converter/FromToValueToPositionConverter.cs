﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Data;
using Logicx.DiagramLib.Scales;

namespace Logicx.DiagramLib.Resources.Converter
{
    public class FromToValueToPositionConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (values.Length == 5)
            {
                object from_value = values[0]; // 
                object to_value = values[1];

                if (values[2] == DependencyProperty.UnsetValue || values[3] == DependencyProperty.UnsetValue || !(values[4] is bool))
                    return 0;

                double scalue_unit_size = (double)values[2];
                ScaleControlBase scale_control = values[3] as ScaleControlBase;
                bool isBackgroundObject = (bool)values[4];

                if (from_value != null && to_value != null && scale_control != null)
                {
                    if (scale_control is DiscreteScaleControl<Object>)
                    {
                        double unit_size = scale_control.GetUnitSize();
                        return 0.9*unit_size;
                    }
                    else
                    {
                        double to_position = scale_control.GenericTransformAxisValueToPixel(to_value, isBackgroundObject);
                        double from_position = scale_control.GenericTransformAxisValueToPixel(from_value,
                                                                                              isBackgroundObject);

                        //if(double.IsNaN(Math.Abs(to_position - from_position)) || double.IsInfinity(Math.Abs(to_position - from_position)))
                        //    Debug.WriteLine("INFINITY OR NAN IN FROMTOVALUETOPOSITION CONVERTER");
                        return Math.Abs(to_position - from_position);
                    }
                }
            }

            return 0;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }
}
