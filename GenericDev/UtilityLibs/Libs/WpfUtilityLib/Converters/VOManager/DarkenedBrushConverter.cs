﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Windows.Media;

namespace Logicx.WpfUtility.Converters.VOManager
{
    [ValueConversion(typeof(SolidColorBrush), typeof(SolidColorBrush))]
    public class DarkenedBrushConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return Brushes.Transparent;
            SolidColorBrush brush = (SolidColorBrush)value;
            Color c = brush.Color;
            return BrushGenerator.GenerateDarkendBrush(c, (double) parameter);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            SolidColorBrush brush = (SolidColorBrush)value;
            Color c = brush.Color;
            return BrushGenerator.GenerateDarkendBrush(c, 1 / (double) parameter);
        }
    }
}
