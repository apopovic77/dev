﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MathLib;

namespace ArtApp
{

    public class Engine
    {
        public static Canvas Canvas;

        public static List<DragLine> DragLines = new List<DragLine>();
    }

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            canvas.Children.Add(_ball);
            Engine.Canvas = canvas;

            CompositionTarget.Rendering += Render;
        }

        void Render(object sender, EventArgs e)
        {
            DateTime current_time = DateTime.Now;
            long elapsedTicks = current_time.Ticks - _last_render.Ticks;
            TimeSpan elapsed_time = new TimeSpan(elapsedTicks);

            //if (elapsed_time.Milliseconds / 1000f < 1f / 60f)
            //    return;

            _last_render = current_time;






            _ball.Update(elapsed_time);

            if(_active_dline != null)
                _active_dline.Update();

            geschwindigkeit += geschwindigkeitsverändung;

            nico_y = nico_y + geschwindigkeit;

            SetPos(100,nico_y);

        }

        private double geschwindigkeitsverändung = 0.05;
        private double geschwindigkeit = 0;

        private double nico_x = 0;
        private double nico_y = 0;

        private DateTime _last_render;

        public void SetPos(double x, double y)
        {
            Canvas.SetTop(nico, y);
            Canvas.SetLeft(nico,x);
        }


        private void Canvas_OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            canvas.MouseUp += canvas_MouseUp;
            canvas.MouseMove += canvas_MouseMove;

            DragLine dline = new DragLine();

            dline.Edge.V1.X = (float)e.GetPosition(canvas).X;
            dline.Edge.V1.Y = (float)e.GetPosition(canvas).Y;
            dline.Edge.V2 = dline.Edge.V1;
            dline.Update();




            canvas.Children.Add(dline);
            Engine.DragLines.Add(dline);
            _active_dline = dline;
        }

        void canvas_MouseMove(object sender, MouseEventArgs e)
        {
            tb1.Text = e.GetPosition(canvas).ToString();

            _active_dline.Edge.V2.X = (float)e.GetPosition(canvas).X;
            _active_dline.Edge.V2.Y = (float)e.GetPosition(canvas).Y;

            Vector2d v = _active_dline.Edge.Vector;
            double len = v.GetLen();
            v = v.GetNorm();
            v = v*len/2;

            Vector2d v2 = v.GetOrthogonal1().GetNorm();
            v2 = v2*100;

            _active_dline.Edge2.V1 = _active_dline.Edge.V1 + v;
            _active_dline.Edge2.V2 = _active_dline.Edge.V1 + v+ v2;
        }

        void canvas_MouseUp(object sender, MouseButtonEventArgs e)
        {
            canvas.MouseUp -= canvas_MouseUp;
            canvas.MouseMove -= canvas_MouseMove;
            tb1.Text = "";
            _active_dline = null;
        }


        private DragLine _active_dline;
        private Ball _ball = new Ball();




    }
}
