﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Animation;
using System.Windows.Threading;
using Logicx.DiagramLib.Scales;
using Logicx.WpfUtility.WpfHelpers;

namespace Logicx.DiagramLib.ViewConfig
{


    public class AxisScaleConfig<T> : UIElement, INotifyPropertyChanged, IDisposable
    {

        #region Constants
        #endregion

        #region Construction and Initialization

        public AxisScaleConfig(T start, T end)
        {
            Start = start;
            End = end;

            if (start is DateTime)
                StandardUnitSizeDateTime = TimeSpan.FromDays(1);
            else if (start is int)
                StandardUnitSizeNumber = (T)(object)5;
            else if (start is double)
                StandardUnitSizeNumber = (T)(object)5.0;
        }

        public AxisScaleConfig(List<T> values)
        {
            _values = values;

            IsDiscrete = true;
        }

        public AxisScaleConfig(List<T> values, List<T> values_unique)
            : this(values)
        {
            _values_unique = values_unique;
        }

        #endregion

        #region Operations

        public void SetStartAndEnd(T start, T end)
        {
            _dont_fire_start_end_events = true;

            Start = start;
            End = end;

            _dont_fire_start_end_events = false;

            SendPropertyChanged("Start");
            SendPropertyChanged("End");
        }

        protected void SendPropertyChanged(string property_name)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property_name));
        }

        public void InitializeFrom(AxisScaleConfig<T> source_config)
        {
            if (source_config.Disposed)
                throw new ObjectDisposedException("source_config");

            //UpdateStartAndEnd(source_config.Start, source_config.End);
            SetStartAndEnd(source_config.Start, source_config.End);

            if (Values != null)
                Values.Clear();

            if (ValuesUnique != null)
                ValuesUnique.Clear();

            if (source_config.Values != null && source_config.Values.Count > 0)
            {
                _values.AddRange(source_config.Values);

                if (source_config.ValuesUnique != null)
                {
                    if (_values_unique == null)
                        _values_unique = new List<T>();
                    _values_unique.AddRange(source_config.ValuesUnique);
                }

                IsDiscrete = true;
            }
            else
            {
                IsDiscrete = false;
            }

            Description = source_config.Description;
            Tag = source_config.Tag;
        }

        private void UpdateStartAndEnd(T start, T end)
        {
            if (start is IComparable)
            {
                if (((IComparable)start).CompareTo(End) >= 0)
                {
                    End = end;
                    Start = start;
                    return;
                }
            }
            Start = start;
            End = end;
        }

        #region IDisposable Members

        public void Dispose()
        {
            if (_disposed || _disposing)
                return;

            _disposing = true;

            EventHelper.UnregisterEvents(PropertyChanged);

            Dispose(true);
            _disposed = true;
        }

        protected virtual void Dispose(bool disposing)
        {

        }


        #endregion

        #endregion

        #region Event Handlers
        private static void OnPaddingChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is AxisScaleConfig<T>)
            {
                ((AxisScaleConfig<T>)d).SendPropertyChanged("Padding");
            }
        }

        private static void OnOrientationChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is AxisScaleConfig<T>)
            {
                ((AxisScaleConfig<T>)d).SendPropertyChanged("Orientation");
            }
        }

        private static void OnAlignmentChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is AxisScaleConfig<T>)
            {
                ((AxisScaleConfig<T>)d).SendPropertyChanged("Alignment");
            }
        }

        private static void OnEndChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is AxisScaleConfig<T>)
            {
                AxisScaleConfig<T> axis_scale_config = (AxisScaleConfig<T>)d;
                if (!axis_scale_config._dont_fire_start_end_events)
                    axis_scale_config.SendPropertyChanged("End");
            }
        }

        private static void OnStartChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is AxisScaleConfig<T>)
            {
                AxisScaleConfig<T> axis_scale_config = (AxisScaleConfig<T>)d;
                if (!axis_scale_config._dont_fire_start_end_events)
                    axis_scale_config.SendPropertyChanged("Start");
            }
        }

        private static object OnEndCoerce(DependencyObject d, object basevalue)
        {
            AxisScaleConfig<T> dep_obj = (AxisScaleConfig<T>)d;

            if (!coerce_current)
            {
                if (basevalue is int)
                {

                }
                else if (basevalue is double)
                {

                }
                else if (basevalue is DateTime)
                {
                    DateTime start = (DateTime)(object)dep_obj.Start;
                    DateTime old_end = (DateTime)(object)dep_obj.End;
                    DateTime end = (DateTime)basevalue;

                    // if on next line an exception appears, the reason for the error probably happens in BtMassnahmenDiagram.GetInitialTimeRanges, Line 2321
                    // Wartungsfenster definition is not set correctly in that case!!!
                    DateTime min_start = end.AddMilliseconds(-1 * ScaleFactory.MinDatetimeMsec());
                    //if (end <= DateTime.MinValue.AddMilliseconds(ScaleFactory.MinDatetimeMsec()))
                    //    min_start = DateTime.MinValue;
                    //else
                    //    min_start = end.AddMilliseconds(-1 * ScaleFactory.MinDatetimeMsec());

                    if ((start != default(DateTime) && old_end != default(DateTime)) && start > min_start)
                    {
                        AxisScaleConfig<DateTime> config = (AxisScaleConfig<DateTime>)(object)dep_obj;
                        coerce_current = true;
                        config.Start = min_start;

                        return end;
                    }
                }
            }

            coerce_current = false;

            return basevalue;
        }

        private static object OnStartCoerce(DependencyObject d, object basevalue)
        {
            AxisScaleConfig<T> dep_obj = (AxisScaleConfig<T>)d;

            if (!coerce_current)
            {
                if (basevalue is int)
                {

                }
                else if (basevalue is double)
                {

                }
                else if (basevalue is DateTime)
                {
                    DateTime start = (DateTime)basevalue;
                    DateTime old_start = (DateTime)(object)dep_obj.Start;
                    DateTime end = (DateTime)(object)dep_obj.End;

                    // if on next line an exception appears, the reason for the error probably happens in BtMassnahmenDiagram.GetInitialTimeRanges, Line 2361 (28.03.2011)
                    // Wartungsfenster definition is not set correctly in that case!!!
                    DateTime min_end = start.AddMilliseconds(ScaleFactory.MinDatetimeMsec());
                    //if (start >= DateTime.MaxValue.AddMilliseconds(-1 * ScaleFactory.MinDatetimeMsec()))
                    //    min_end = DateTime.MaxValue;
                    //else
                    //    min_end = start.AddMilliseconds(ScaleFactory.MinDatetimeMsec());

                    if (old_start != default(DateTime) && end != default(DateTime) && end < min_end)
                    {
                        AxisScaleConfig<DateTime> config = (AxisScaleConfig<DateTime>)(object)dep_obj;
                        coerce_current = true;
                        config.End = min_end;

                        return start;
                    }
                }
            }

            coerce_current = false;

            return basevalue;
        }



        #endregion

        #region Properties

        public List<T> Values
        {
            get
            {
                return _values;
            }
            set
            {
                _values = value;
                SendPropertyChanged("Values");
            }
        }

        public List<T> ValuesUnique
        {
            get
            {
                return _values_unique;
            }
            set
            {
                _values_unique = value;
                SendPropertyChanged("ValuesUnique");
            }
        }

        public string Description
        {
            get
            {
                return _description;
            }
            set
            {
                _description = value;
                SendPropertyChanged("Description");
            }
        }

        public bool IsDiscrete
        {
            get { return _is_discrete; }
            set
            {
                _is_discrete = value;
                SendPropertyChanged("IsDiscrete");
            }
        }

        public object Tag
        {
            get { return _tag; }
            set
            {
                if (_tag != value)
                {
                    _tag = value;
                    SendPropertyChanged("Tag");
                }
            }
        }

        /// <summary>
        /// Gets a flag if the object has been disposed
        /// </summary>
        public bool Disposed
        {
            get { return _disposed; }
        }
        /// <summary>
        /// Gets a flag if the object is disposing
        /// </summary>
        protected bool Disposing
        {
            get { return _disposing; }
        }

        #endregion

        #region Dependency Properties


        public Thickness Padding
        {
            get { return (Thickness)GetValue(PaddingProperty); }
            set { SetValue(PaddingProperty, value); }
        }

        public static readonly DependencyProperty PaddingProperty =
            DependencyProperty.Register("Padding", typeof(Thickness), typeof(AxisScaleConfig<T>), new UIPropertyMetadata(new Thickness(0), new PropertyChangedCallback(AxisScaleConfig<T>.OnPaddingChanged)));




        public Orientation Orientation
        {
            get { return (Orientation)GetValue(OrientationProperty); }
            set { SetValue(OrientationProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Orientation.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty OrientationProperty =
            DependencyProperty.Register("Orientation", typeof(Orientation), typeof(AxisScaleConfig<T>), new UIPropertyMetadata(Orientation.Horizontal, new PropertyChangedCallback(AxisScaleConfig<T>.OnOrientationChanged)));




        public ScaleAlignment Alignment
        {
            get { return (ScaleAlignment)GetValue(AlignmentProperty); }
            set { SetValue(AlignmentProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Alignment.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty AlignmentProperty =
            DependencyProperty.Register("Alignment", typeof(ScaleAlignment), typeof(AxisScaleConfig<T>), new UIPropertyMetadata(ScaleAlignment.Bottom, new PropertyChangedCallback(AxisScaleConfig<T>.OnAlignmentChanged)));




        public T Start
        {
            get { return (T)GetValue(StartProperty); }
            set
            {
                SetValue(StartProperty, value);
            }
        }

        // Using a DependencyProperty as the backing store for End.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty StartProperty =
            DependencyProperty.Register("Start", typeof(T), typeof(AxisScaleConfig<T>), new UIPropertyMetadata(default(T), new PropertyChangedCallback(AxisScaleConfig<T>.OnStartChanged), new CoerceValueCallback(OnStartCoerce)));

        public T End
        {
            get { return (T)GetValue(EndProperty); }
            set
            {
                SetValue(EndProperty, value);
            }
        }

        // Using a DependencyProperty as the backing store for End.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty EndProperty =
            DependencyProperty.Register("End", typeof(T), typeof(AxisScaleConfig<T>), new UIPropertyMetadata(default(T), new PropertyChangedCallback(AxisScaleConfig<T>.OnEndChanged), new CoerceValueCallback(AxisScaleConfig<T>.OnEndCoerce)));


        #endregion

        #region Events
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        #region Routed Events
        #endregion

        #region Attributes

        //private T _start;
        //private T _end;
        private string _description;
        private bool _is_discrete;

        private object _tag = null;

        private List<T> _values;
        private List<T> _values_unique;

        private bool _disposed = false;
        private bool _disposing = false;

        private bool _dont_fire_start_end_events;

        public readonly T StandardUnitSizeNumber;
        public readonly TimeSpan StandardUnitSizeDateTime;

        private static bool coerce_current;
        #endregion

        protected delegate void NoParas();


    }
}
