﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Logicx.WpfUtility.CustomControls.OverlayManager
{
    public delegate void MessageBoxResultDelegate(object sender, MessageBoxResultArgs result);

    public class MessageBoxResultArgs : EventArgs
    {
        public MessageBoxResultArgs()
        {

        }

        public MessageBoxResultArgs(MessageBoxResult result)
        {
            _message_box_result = result;
        }

        public MessageBoxResult Result
        {
            get { return _message_box_result; }
        }

        public object Tag
        {
            get; 
            set;
        }

        private MessageBoxResult _message_box_result = MessageBoxResult.None;
    }

    /// <summary>
    /// Interaction logic for OverlayMessageBox.xaml
    /// </summary>
    public partial class OverlayMessageBox // : IOverlayContent
    {
        public OverlayMessageBox()
        {
            InitializeComponent();
            DialogTitle = " ";
            DialogSubTitle = " ";

            _button1 = new OverlayDialogButton("OK", OverlayButtonAlignment.Right, Key.Enter);
            _button2 = new OverlayDialogButton("Nein", OverlayButtonAlignment.Right);
            _button3 = new OverlayDialogButton("Abbrechen", OverlayButtonAlignment.Right, Key.Escape);

            AutoCenteredDialog = true;
            DefaultWindowState = OverlayDialogState.Modal;
            ModalBehaviour = OverlayContent.ModalBehaviourEnum.DarkenBackground;
            InitializationBehaviour = OverlayContent.DataInitializationBehaviourEnum.Immediate;

            AddButton(_button1);
            MessageBoxButtons = MessageBoxButton.OK;
            MessageBoxImage = MessageBoxImage.None;


        }

        #region IOverlayContent interface implementation

        public bool Show(bool firstTime)
        {
            return true;
        }

        public bool Close(bool forceClose)
        {
            return true;
        }

        public bool Print()
        {
            return true;
        }

        public bool Help()
        {
            return true;
        }

        /// <summary>
        /// Eigenschaft welche definiert ob der content das Drucken unterstützt oder nicht
        /// </summary>
        public bool CanPrint
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Eigenschaft welche dem Hostdialog mitteilt ob der Content geschlossen werden kann.
        /// </summary>
        public bool CanClose
        {
            get
            {
                return true;
            }
        }

        public bool CanHelp
        {
            get
            {
                return false;
            }
        }

        public bool AutoResized
        {
            get { return true; }
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets/Sets the text
        /// </summary>
        public string MessageBoxText
        {
            get { return textblock_text.Text; }
            set { textblock_text.Text = value; }
        }
        /// <summary>
        /// Gets/sets the buttons which should be visible in the message box
        /// </summary>
        public MessageBoxButton MessageBoxButtons
        {
            get
            {
                return _message_box_buttons;
            }

            set
            {
                if (_message_box_buttons != value)
                {
                    _message_box_buttons = value;

                    RemoveButton(_button1);
                    RemoveButton(_button2);
                    RemoveButton(_button3);

                    switch(_message_box_buttons)
                    {
                        case MessageBoxButton.OK:
                            {
                                _button1.Content = "Ok";
                                AddButton(_button1);
                            }
                            ; break;
                        case MessageBoxButton.OKCancel:
                            {
                                _button1.Content = "Ok";
                                _button3.Content = "Abbrechen";
                                AddButton(_button1);
                                AddButton(_button3);
                            }
                            ; break;
                        case MessageBoxButton.YesNo:
                            {
                                _button1.Content = "Ja";
                                _button3.Content = "Nein";
                                AddButton(_button1);
                                AddButton(_button3);
                            }
                            ; break;
                        case MessageBoxButton.YesNoCancel:
                            {
                                _button1.Content = "Ja";
                                _button2.Content = "Nein";
                                _button3.Content = "Abbrechen";
                                AddButton(_button1);
                                AddButton(_button2);
                                AddButton(_button3);
                            }
                            ; break;
                    }
                }
            }
        }
        /// <summary>
        /// Messagebox image
        /// </summary>
        public MessageBoxImage MessageBoxImage
        {
            get
            {
                return _message_box_image;
            }

            set
            {
                if (_message_box_image != value)
                {
                    _message_box_image = value;

                    if (_message_box_image != MessageBoxImage.None)
                    {
                        System.Drawing.Icon icon = System.Drawing.SystemIcons.Information;

                        if(_message_box_image == MessageBoxImage.Asterisk)
                        {
                            icon = System.Drawing.SystemIcons.Asterisk;
                        }
                        else if (_message_box_image == MessageBoxImage.Error || _message_box_image == MessageBoxImage.Stop)
                        {
                            icon = System.Drawing.SystemIcons.Error;
                        }
                        else if(  _message_box_image == MessageBoxImage.Exclamation)
                        {
                            icon = System.Drawing.SystemIcons.Exclamation;
                        }
                        else if( _message_box_image == MessageBoxImage.Hand)
                        {
                            icon = System.Drawing.SystemIcons.Hand;
                        }
                        else if (_message_box_image == MessageBoxImage.Information)
                        {
                            icon = System.Drawing.SystemIcons.Information;
                        }
                        else if(_message_box_image == MessageBoxImage.Question)
                        {
                            icon = System.Drawing.SystemIcons.Question;
                        }
                        else if( _message_box_image == MessageBoxImage.Warning)
                        {
                            icon = System.Drawing.SystemIcons.Warning;
                        }
                        
                        BitmapSource bs = Imaging.CreateBitmapSourceFromHIcon(icon.Handle, new Int32Rect(0, 0, 32, 32), BitmapSizeOptions.FromWidthAndHeight(32, 32));
                        image_icon.Source = bs;
                        image_icon.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        image_icon.Visibility = Visibility.Collapsed;
                    }
                }
            }
        }
        /// <summary>
        /// Messagebox result
        /// </summary>
        public MessageBoxResult Result
        {
            get { return _message_box_result; }
        }
        #endregion

        #region Virtuelle methoden welche überschrieben werden können

        /// <summary>
        /// Wird aufgerufen, wenn der Benutzer einen der OverlayButtons klicked
        /// </summary>
        /// <param name="button"></param>
        public override bool OverlayButtonClicked(OverlayDialogButton button)
        {
            if (button == _button1)
            {
                switch(_message_box_buttons)
                {
                    case MessageBoxButton.OK:
                    case MessageBoxButton.OKCancel:
                        _message_box_result = MessageBoxResult.OK; break;
                    case MessageBoxButton.YesNo:
                    case MessageBoxButton.YesNoCancel:
                        _message_box_result = MessageBoxResult.Yes; break;
                }

                if (OverlayManager != null)
                    OverlayManager.HideContent(this, true);
            }

            if (button == _button2)
            {
                _message_box_result = MessageBoxResult.None;

                switch (_message_box_buttons)
                {
                    case MessageBoxButton.YesNoCancel:
                        _message_box_result = MessageBoxResult.No; break;
                }

                if (OverlayManager != null)
                    OverlayManager.HideContent(this, true);
            }

            if (button == _button3)
            {
                _message_box_result = MessageBoxResult.None;

                switch (_message_box_buttons)
                {
                    case MessageBoxButton.OKCancel:
                    case MessageBoxButton.YesNoCancel:
                        _message_box_result = MessageBoxResult.Cancel; break;
                    case MessageBoxButton.YesNo:
                        _message_box_result = MessageBoxResult.No; break;
                }

                if (OverlayManager != null)
                    OverlayManager.HideContent(this, true);

            }

            return true;
        }

        /// <summary>
        /// Wurd aufgerufen bevor das Control angezeigt wird
        /// </summary>
        /// <param name="firstTime">true wenn dieser Content in der Dialoginstanz das erste Mal angezeigt wird</param>
        /// <returns>false wenn das control nicht angezeigt werden soll.</returns>
        public override bool BeforeShow(bool firstTime)
        {
            if(firstTime)
                _message_box_result = MessageBoxResult.Cancel;

            return true;
        }
        /// <summary>
        /// Wird aufgerufen nachdem das Control angezeigt wurde
        /// </summary>
        public override void AfterShow()
        {
            _button1.Focus();
        }

        /// <summary>
        /// Wird aufgerufen bevor das control geschlossen wird
        /// </summary>
        /// <param name="forceClose">True wenn das Control auf jeden Fall geschlossen wird.</param>
        /// <returns>false wenn das Control nicht geschlossen werden kann.</returns>
        /// <remarks>Wenn <see cref="forceClose"/> True ist, dann wird der 
        /// Rückgabewert ignoriert und das Control auf jeden Fall geschlossen.</remarks>
        public override bool BeforeClose(bool forceClose)
        {
            return true;
        }
        /// <summary>
        /// Wird aufgerufen nachdem das Control geschlossen wurde
        /// </summary>
        public override void AfterClose()
        {
            base.AfterClose();
        }

        /// <summary>
        /// Wurd aufgerufen bevor das Control gedruckt wird wird
        /// </summary>
        /// <returns>false wenn das control nicht gedruckt werden soll.</returns>
        public override bool BeforePrint()
        {
            return true;
        }
        /// <summary>
        /// wird aufgerufen nachdem das Control gedruckt wurde
        /// </summary>
        public override void AfterPrint()
        {

        }
        #endregion

        #region Attribs

        private MessageBoxButton _message_box_buttons = MessageBoxButton.OK;
        private MessageBoxImage _message_box_image = MessageBoxImage.None;
        private MessageBoxResult _message_box_result = MessageBoxResult.None;

        private OverlayDialogButton _button1 = null;
        private OverlayDialogButton _button2 = null;
        private OverlayDialogButton _button3 = null;
        #endregion
    }

}
