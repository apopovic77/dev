﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Logicx.DiagramLib.Controls
{
    /// <summary>
    /// Interaction logic for MehrfachAuswahlButtonContent.xaml
    /// </summary>
    public partial class MehrfachAuswahlButtonContent : UserControl
    {
        public MehrfachAuswahlButtonContent()
        {
            InitializeComponent();
        }

        public Brush StrokeBrush
        {
            get { return (Brush)GetValue(StrokeBrushProperty); }
            set { SetValue(StrokeBrushProperty, value); }
        }

        // Using a DependencyProperty as the backing store for StrokeBrush.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty StrokeBrushProperty =
            DependencyProperty.Register("StrokeBrush", typeof(Brush), typeof(MehrfachAuswahlButtonContent), new UIPropertyMetadata(Brushes.Blue));



        public Brush BackgroundBrush
        {
            get { return (Brush)GetValue(BackgroundBrushProperty); }
            set { SetValue(BackgroundBrushProperty, value); }
        }

        // Using a DependencyProperty as the backing store for BackgroundBrush.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty BackgroundBrushProperty =
            DependencyProperty.Register("BackgroundBrush", typeof(Brush), typeof(MehrfachAuswahlButtonContent), new UIPropertyMetadata(Brushes.Black));



        public Brush LineBrush
        {
            get { return (Brush)GetValue(LineBrushProperty); }
            set { SetValue(LineBrushProperty, value); }
        }

        // Using a DependencyProperty as the backing store for LineBrush.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty LineBrushProperty =
            DependencyProperty.Register("LineBrush", typeof(Brush), typeof(MehrfachAuswahlButtonContent), new UIPropertyMetadata(Brushes.White));


        public bool IsMehrfachAuswahlActive
        {
            get { return (bool)GetValue(IsMehrfachAuswahlActiveProperty); }
            set { SetValue(IsMehrfachAuswahlActiveProperty, value); }
        }

        // Using a DependencyProperty as the backing store for LineBrush.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsMehrfachAuswahlActiveProperty =
            DependencyProperty.Register("IsMehrfachAuswahlActive", typeof(bool), typeof(MehrfachAuswahlButtonContent), new UIPropertyMetadata(false));
    }
}
