﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Logicx.WpfUtility.WindowManagement;

namespace Logicx.WpfUtility.CustomControls.OverlayManager
{
    public class OverlayManager : OverlayManagerBase<BaseFrame, SubMenuPanel, WindowTaskBar, WindowTitleBar, OverlayManager, SubMenuPanelMenuItem>
    {
        public OverlayManager() : base()
        {}
    }
}
