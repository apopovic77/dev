﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace Logicx.Utility
{
    public class FpsCounter
    {
        public FpsCounter()
        {
            _sw = new Stopwatch();
            _fps_counter_start = DateTime.Now;
        }

        /// <summary>
        /// call this in your render loop
        /// </summary>
        /// <returns>elapsed time since last tick call</returns>
        public long Tick()
        {
            _sw.Stop();
            _elapsedTimeSinceLastTick = _sw.ElapsedMilliseconds - _last_tick;
            _last_tick = _sw.ElapsedMilliseconds;
            ++_frames;
            //++_frames_200ms;

            // Update the stats once per second
            if (_sw.ElapsedMilliseconds - _lasttime > 1000)
            {
                float elapsed_secs = (_sw.ElapsedMilliseconds - _lasttime)/1000f;
                Fps = _frames / elapsed_secs;
                _lasttime = _sw.ElapsedMilliseconds;
                _frames = 0;
            }

            //// Update the stats once per second
            //if (_sw.ElapsedMilliseconds - _lasttime > 200)
            //{
            //    float elapsed_secs = (_sw.ElapsedMilliseconds - _lasttime_200ms) / 1000f;
            //    Fps200ms = _frames_200ms / elapsed_secs;
            //    _lasttime_200ms = _sw.ElapsedMilliseconds;
            //    _frames_200ms = 0;
            //}

            _sw.Start();
            return _elapsedTimeSinceLastTick;
        }

        public long GetLastElapsedTime()
        {
            return GetElapsedTime(_last_tick);
        }


        public void Freeze()
        {
            _sw.Stop();
        }

        public long UnFreeze()
        {
            //reset fps
            Fps = 60;
            _lasttime = _sw.ElapsedMilliseconds;
            _frames = 0;

            //Fps200ms = 60;
            //_lasttime_200ms = _sw.ElapsedMilliseconds;
            //_frames_200ms = 0;

            long curr_elapsed_time = _sw.ElapsedMilliseconds;
            _sw.Start();
            return curr_elapsed_time;
        }


        public long GetElapsedTime(long last_tick)
        {
            //_sw.Stop();
            long elapsedTime = _sw.ElapsedMilliseconds - last_tick;
            //_sw.Start();
            return elapsedTime;
        }

        public long CurrElapsedAppTime
        {
            get
            {
                //_sw.Stop();
                long elapsedAppTime = _sw.ElapsedMilliseconds;
                //_sw.Start();
                return elapsedAppTime;
            }
        }



        public void WriteDebug()
        {
            Debug.WriteLine("FPS "+Fps);
        }

        protected Stopwatch _sw;
        protected DateTime _fps_counter_start;
        protected long _last_tick;

        protected int _frames = 0;
        protected long _lasttime = 0;
        //protected int _frames_200ms = 0;
        //protected long _lasttime_200ms = 0;
        /// <summary>
        /// messung wird jede sekunde hergestellt
        /// </summary>
        public float Fps = 0;
        /// <summary>
        /// messung wird alle 200 ms hergestellt
        /// </summary>
        //public float Fps200ms = 0;
        protected long _elapsedTimeSinceLastTick = 0;

    }
}
