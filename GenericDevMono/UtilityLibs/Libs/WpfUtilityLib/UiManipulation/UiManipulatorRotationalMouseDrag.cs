﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace Logicx.WpfUtility.UiManipulation
{
    public class UiManipulatorRotationalMouseDrag : MouseDrag
    {
        public UiManipulatorRotationalMouseDrag(UIElement ui_element, Canvas canvas, UIElement event_base, EnabledAxes enabled_axes, Rect element_size) : this(ui_element, canvas, event_base, enabled_axes, element_size, true)
        {
        }

        public UiManipulatorRotationalMouseDrag(UIElement ui_element, Canvas canvas, UIElement event_base, EnabledAxes enabled_axes, Rect element_size, bool initialize_canvas_position)
            : base(ui_element, canvas, event_base, enabled_axes, initialize_canvas_position)
        {
            _element_size = element_size;
        }
        

        public Rect ElementSize
        {
            get { return _element_size; }
            set { _element_size = value; }
        }

        public override void  MouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        {
            base.MouseMove(sender, e);

            double next_left = Canvas.GetLeft(_ui_element);
            double next_top = Canvas.GetTop(_ui_element);

            GetRotationalPosition(_enabled_axes, _element_size, ref next_left, ref next_top);

            Canvas.SetLeft(_ui_element, next_left);
            Canvas.SetTop(_ui_element, next_top);
        }

        public static void GetRotationalPosition(EnabledAxes enabled_axes, Rect element_size, ref double next_left, ref double next_top)
        {
            //assert out of bounds
            if (enabled_axes == EnabledAxes.XAxis || enabled_axes == EnabledAxes.All)
            {
                if (next_left < -element_size.Width)
                {
                    next_left += element_size.Width;
                }
                else if (next_left > 0)
                {
                    next_left -= element_size.Width;
                }
            }
            if (enabled_axes == EnabledAxes.YAxis || enabled_axes == EnabledAxes.All)
            {
                if (next_top < -element_size.Height)
                {
                    next_top += element_size.Height;
                }
                else if (next_top > 0)
                {
                    next_top -= element_size.Height;
                }
            }
        }

        private Rect _element_size;
    }
}
