﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using Logicx.DiagramLib.Util;

namespace Logicx.DiagramLib.Scales
{
    class MonthScale : ScaleType
    {

        #region Constants

        private const long ONE_OUR_MSEC = 3600l * 1000; // * 10000000;
        private const long ONE_DAY_MSEC = 24 * ONE_OUR_MSEC;

        private const short LOD_1 = 1; // 1 unit is one day
        private const short LOD_5 = 5; // 1 unit is three day
        private const short LOD_10 = 10;
        private const short LOD_15 = 15;
        private const short LOD_30 = 30;

        private const double LONG_LINE_HEIGHT = 9;
        private const double MIDDLE_LINE_HEIGHT = 5;
        private const double SHORT_LINE_HEIGHT = 3;

        private const int MIN_LINE_DISTANCE = 10;
        private const double MIN_SCALE_DISTANCE = 30;

        private short[] _days = { 1, 5, 10, 15 };

        #endregion

        #region Construction and Initialization

        public MonthScale()
        {
            _monthListLong = new string[12];
            _monthListShort = new string[12];

            for (int i = 1; i <= _monthListLong.Count(); i++)
            {
                DateTime date = new DateTime(2010, i, 1);
                string month_name_long = Thread.CurrentThread.CurrentCulture.DateTimeFormat.MonthNames[date.Month - 1];

                _monthListLong[i - 1] = month_name_long;
                _monthListShort[i - 1] = month_name_long.Substring(0, 3);
            }
        }
        #endregion

        #region Operations

        public override void Init(double size, bool generate_always)
        {
            short old_level = _line_lod;
            short old_text_level = _text_lod;

            DateTime start = _time_scale_control.Config.Start;
            DateTime end = _time_scale_control.Config.End;

            double months = DateTimeUtil.GetNumberOfMonth(start, end);
            double number_days = ((end - start).TotalMilliseconds / ONE_DAY_MSEC);

            if (months > 30) // if more than 30 months visible show only months (approximately 30 days)
                _lod = LOD_30;
            if (months > 20)
                _lod = LOD_15;
            else if (months > 5)
                _lod = LOD_10;
            else if (months > 3)
                _lod = LOD_5;
            else
                _lod = LOD_1;

            double day_distance = _time_scale_control.GetRenderWidth() / number_days;
            if (_time_scale_control.Config.Orientation == Orientation.Vertical)
                day_distance = _time_scale_control.GetRenderHeight() / number_days;

            int line_index = 0;
            _line_lod = _days[line_index++];
            while (line_index < _days.Count() - 1 && day_distance * _line_lod < MIN_LINE_DISTANCE)
            {
                _line_lod = _days[line_index++];
            }

            double max_description_size = GetMaxDescriptionSizeForDays(30);
            line_index = 0;
            _text_lod = _days[line_index++];
            while (line_index < _days.Count() && day_distance * _text_lod < max_description_size * 1.5)
            {
                _text_lod = _days[line_index++];
            }


            double pixel_per_unit = size / (months * ((double)30 / _lod));
            while (pixel_per_unit < MIN_SCALE_DISTANCE)
            {
                if (_lod == LOD_1)
                    _lod = LOD_5;
                else if (_lod == LOD_5)
                    _lod = LOD_10;
                else if (_lod == LOD_10)
                    _lod = LOD_15;
                else if (_lod == LOD_15)
                    _lod = LOD_30;
                else
                    break;

                pixel_per_unit = size / (months * ((double)30 / _lod));
            }
            double days_to_month_change = DateTimeUtil.GetDaysToMonthChange(start);
            if (!DateTimeUtil.IsMonthChange(start))
                _unit_offset = pixel_per_unit * days_to_month_change / _lod;


            if (generate_always || old_level != _line_lod || old_text_level != _text_lod)
            {
                _time_scale_control.ClearChildren();
                GenerateData();
                AddChildren();
            }
        }

        private void GenerateData()
        {
            _outer_lines.Clear();
            _long_lines.Clear();
            _middle_lines.Clear();
            _short_lines.Clear();
            _long_texts.Clear();
            _long_dates.Clear();

            GenerateDayAndSubData();
        }

        private void GenerateDayAndSubData()
        {
            DateTime start = DateTimeUtil.GetFullHour(_time_scale_control.Config.Start);
            DateTime end = DateTimeUtil.GetFullHour(_time_scale_control.Config.End);

            double height_months = _time_scale_control.Config.Orientation == Orientation.Horizontal
                                 ? _time_scale_control.GetRenderHeight()
                                 : 1;
            double width_months = _time_scale_control.Config.Orientation == Orientation.Horizontal
                                ? 1
                                : _time_scale_control.GetRenderWidth();
            double long_height = _time_scale_control.Config.Orientation == Orientation.Horizontal
                                ? LONG_LINE_HEIGHT
                                : 1;
            double long_width = _time_scale_control.Config.Orientation == Orientation.Horizontal
                               ? 1
                               : LONG_LINE_HEIGHT;

            RotateTransform trans = new RotateTransform();
            int number_days = (int)((end - start).TotalMilliseconds / ONE_DAY_MSEC);
            double day_distance = _time_scale_control.GetRenderWidth() / number_days;
            if (_time_scale_control.Config.Orientation == Orientation.Vertical)
            {
                trans.Angle = 270;
                day_distance = _time_scale_control.GetRenderHeight() / number_days;
            }
            for (DateTime month = start; month <= end; month = month.AddDays(1))
            {
                int int_days = month.Day;

                if ((DateTimeUtil.IsMonthChange(month)) || month == start) // Generate Month texts
                {
                    string str = GetMonth(start, end, month, day_distance);
                    if (str.Length != 0)
                    {
                        TextBlock b = GeometryUtils.GenerateTextBlock(str, TextStyleType.InfoText, typeof(TimeScaleControl), _time_scale_control);
                        b.LayoutTransform = trans;
                        _long_dates.Add(b);
                    }
                }

                if (DateTimeUtil.IsMonthChange(month)) // Generate Year Change lines
                {
                    Rectangle outer_line = GeometryUtils.GenerateRectange(height_months, width_months, typeof(TimeScaleControl), _time_scale_control);
                    _outer_lines.Add(outer_line);
                }
                else
                {
                    int day_modulo = int_days % _line_lod;
                    if (day_modulo == 0)  // lines for days
                    {
                        Rectangle r = GeometryUtils.GenerateRectange(long_height, long_width, typeof(TimeScaleControl), _time_scale_control);
                        _long_lines.Add(r);
                    }

                    int text_modulo = int_days % _text_lod;
                    if (text_modulo == 0 && _text_lod < 15 && (_text_lod == 1 || (_text_lod != LOD_1 && int_days != 30))) // texts for days
                    {
                        TextBlock b = GeometryUtils.GenerateTextBlock(month.Day.ToString(), TextStyleType.ScaleText, typeof(TimeScaleControl), _time_scale_control);
                        _long_texts.Add(b);
                    }
                }

                if (month == end)
                    break;
            }
        }

        private void AddChildren()
        {
            List<UIElement> elements = new List<UIElement>();

            elements.AddRange(_short_lines.Cast<UIElement>());
            elements.AddRange(_middle_lines.Cast<UIElement>());
            elements.AddRange(_long_lines.Cast<UIElement>());
            elements.AddRange(_outer_lines.Cast<UIElement>());
            elements.AddRange(_long_texts.Cast<UIElement>());
            elements.AddRange(_long_dates.Cast<UIElement>());

            _time_scale_control.AddToChildren(elements);
        }

        public override void AlignData(Orientation orientation)
        {
            DateTime start = DateTimeUtil.GetFullHour(_time_scale_control.Config.Start);
            DateTime end = DateTimeUtil.GetFullHour(_time_scale_control.Config.End);

            int month_index = 0;
            int day_index = 0;
            int hour_index = 0;
            int text_index = 0;

            double current_day_position = 0;
            double day_distance = 0;
            if (orientation == Orientation.Horizontal)
            {
                //current_day_position = _time_scale_control.Config.Padding.Left;
                current_day_position = 0;
                double first = _time_scale_control.TransformAxisValueToPixel(start);
                double second = _time_scale_control.TransformAxisValueToPixel(start.AddDays(1));

                day_distance = second - first;
            }
            else if (orientation == Orientation.Vertical)
            {
                //current_day_position = _time_scale_control.Config.Padding.Bottom;
                current_day_position = 0;

                double first = _time_scale_control.TransformAxisValueToPixel(end);
                double second = _time_scale_control.TransformAxisValueToPixel(end.AddDays(-1));

                day_distance = second - first;
            }
            for (DateTime month = start; month <= end; month = month.AddDays(1))
            {
                int int_day = month.Day;

                if ((DateTimeUtil.IsMonthChange(month)) || month == start) // Align month texts
                {
                    string str = GetMonth(start, end, month, day_distance);
                    if (str.Length != 0 && _long_dates.Count > month_index)
                    {
                        TextBlock text_block = _long_dates.ToArray()[month_index++];
                        if (orientation == Orientation.Horizontal)
                        {
                            Canvas.SetLeft(text_block, current_day_position + 5);
                            //Canvas.SetBottom(text_block, _time_scale_control.Config.Padding.Bottom);
                            Canvas.SetBottom(text_block, 0);
                        }
                        else
                        {
                            //Canvas.SetRight(text_block, _time_scale_control.Config.Padding.Right);
                            Canvas.SetRight(text_block, 0);
                            Canvas.SetBottom(text_block, current_day_position + 5);
                        }
                    }
                }

                if (DateTimeUtil.IsMonthChange(month) && _outer_lines.Count > day_index) // align month lines
                {
                    Rectangle rectangle = _outer_lines.ToArray()[day_index++];
                    if (orientation == Orientation.Horizontal)
                    {
                        Canvas.SetLeft(rectangle, current_day_position);
                        //Canvas.SetTop(rectangle, _time_scale_control.Config.Padding.Top);
                        Canvas.SetTop(rectangle, 0);
                    }
                    else
                    {
                        //Canvas.SetLeft(rectangle, _time_scale_control.Config.Padding.Left);
                        Canvas.SetLeft(rectangle, 0);
                        Canvas.SetBottom(rectangle, current_day_position);
                    }
                }
                else
                {
                    int day_modulo = int_day % _line_lod;
                    if (day_modulo == 0 && _long_lines.Count > hour_index) // draw day lines
                    {
                        Rectangle rectangle = _long_lines.ToArray()[hour_index++];
                        if (orientation == Orientation.Horizontal)
                        {
                            Canvas.SetLeft(rectangle, current_day_position);
                            //Canvas.SetTop(rectangle, _time_scale_control.Config.Padding.Top);
                            Canvas.SetTop(rectangle, 0);
                        }
                        else
                        {
                            //Canvas.SetLeft(rectangle, _time_scale_control.Config.Padding.Left);
                            Canvas.SetLeft(rectangle, 0);
                            Canvas.SetBottom(rectangle, current_day_position);
                        }
                    }

                    int text_modulo = int_day % _text_lod;
                    if (text_modulo == 0 && _text_lod < 15 && (_text_lod == 1 || (_text_lod != LOD_1 && int_day != 30)) && _long_texts.Count > text_index) // draw day texts
                    {
                        TextBlock text_block = _long_texts.ToArray()[text_index++];
                        FormattedText ft = new FormattedText(text_block.Text, Thread.CurrentThread.CurrentCulture, FlowDirection.LeftToRight, new Typeface(TextBlock.FontStyleProperty.Name), 10, Brushes.Black);

                        if (orientation == Orientation.Horizontal)
                        {
                            Canvas.SetLeft(text_block, current_day_position - ft.Width / 2);
                            //Canvas.SetTop(text_block, _time_scale_control.Config.Padding.Top + LONG_LINE_HEIGHT + 3);
                            Canvas.SetTop(text_block, LONG_LINE_HEIGHT + 3);
                        }
                        else
                        {
                            //Canvas.SetLeft(text_block, _time_scale_control.Config.Padding.Left + LONG_LINE_HEIGHT + 3);
                            Canvas.SetLeft(text_block, LONG_LINE_HEIGHT + 3);
                            Canvas.SetBottom(text_block, current_day_position - ft.Width / 2);
                        }
                    }
                }

                if (month == end)
                    break;

                current_day_position += day_distance;
            }
        }

        public override double GetUnitSize(Orientation orientation)
        {
            DateTime start = DateTimeUtil.GetFullHour(_time_scale_control.Config.Start);
            DateTime end = DateTimeUtil.GetFullHour(_time_scale_control.Config.End);
            int number_days = (int)((end - start).TotalMilliseconds / ONE_DAY_MSEC);

            double num_units = number_days;
            if (_lod != 0)
            {
                num_units /= _lod;
            }
            if (orientation == Orientation.Horizontal)
                return _time_scale_control.GetRenderWidth() / num_units;

            return _time_scale_control.GetRenderHeight() / num_units;
        }

        public override double GetUnitOffset(Orientation orientation)
        {
            return _unit_offset;
        }

        public override double GetUnitOffset()
        {
            double first = _time_scale_control.TransformAxisValueToPixel(_time_scale_control.Config.Start);
            double second = _time_scale_control.TransformAxisValueToPixel(_time_scale_control.Config.Start.AddHours(1));
            double hour_distance = (second - first);

            return _time_scale_control.Config.Start.Hour * hour_distance;
        }

        private static double GetMaxDescriptionSizeForMonth(int number)
        {
            double max = 0;
            int increment = 12 / number;
            for (int i = 0; i < number; i += increment)
            {
                FormattedText ft = new FormattedText(_monthListLong[i], Thread.CurrentThread.CurrentCulture, FlowDirection.LeftToRight, new Typeface(TextBlock.FontStyleProperty.Name), 10, Brushes.Black);
                if (ft.Width > max)
                    max = ft.Width;
            }

            return max;
        }

        private static double GetMaxDescriptionSizeForDays(int number)
        {
            double max = 0;
            for (int i = 1; i <= number; i++)
            {
                FormattedText ft = new FormattedText(i.ToString(), Thread.CurrentThread.CurrentCulture, FlowDirection.LeftToRight, new Typeface(TextBlock.FontStyleProperty.Name), 10, Brushes.Black);
                if (ft.Width > max)
                    max = ft.Width;
            }

            return max;
        }

        private static string GetMonth(DateTime start, DateTime end, DateTime month, double month_distance)
        {
            double dist = month_distance * 30 / 1.5;
            if (month == start)
            {
                dist = DateTimeUtil.GetDaysToMonthChange(month) * month_distance / 1.5;
            }
            if (month.Month == end.Month)
            {
                dist = DateTimeUtil.GetDaysToEndChange(month, end) * month_distance / 1.5;
            }

            string str = _monthListLong[month.Month - 1];
            FormattedText ft = new FormattedText(str, Thread.CurrentThread.CurrentCulture, FlowDirection.LeftToRight, new Typeface(TextBlock.FontStyleProperty.Name), 10, Brushes.Black);

            if (ft.Width <= dist)
                return str;

            str = _monthListShort[month.Month - 1];
            ft = new FormattedText(str, Thread.CurrentThread.CurrentCulture, FlowDirection.LeftToRight, new Typeface(TextBlock.FontStyleProperty.Name), 10, Brushes.Black);
            if (ft.Width <= dist)
                return str;

            return "";
        }

        public override List<double> GetGridLinePositions()
        {
            List<double> positions = new List<double>();

            DateTime start = DateTimeUtil.GetFullHour(_time_scale_control.Config.Start);
            DateTime end = DateTimeUtil.GetFullHour(_time_scale_control.Config.End);

            DateTime current_date = start;

            if (_lod == 0)
                return positions;

            current_date = DateTimeUtil.GetNextDateModulo(current_date, _lod);
            DateTime next_date;
            do
            {
                double pixel = _time_scale_control.TransformAxisValueToPixel(current_date, true);// +pixel_offset;
                positions.Add(pixel);

                current_date = DateTimeUtil.GetNextDateModulo(current_date.AddDays(1), _lod);
                next_date = DateTimeUtil.GetNextDateModulo(current_date.AddDays(1), _lod);
                if (next_date.Month != current_date.Month)
                {
                    if (current_date.Month == 2)
                    {
                        pixel = _time_scale_control.TransformAxisValueToPixel(current_date, true);// +pixel_offset;
                        positions.Add(pixel);
                    }

                    current_date = new DateTime(next_date.Year, next_date.Month, 1).AddHours(next_date.Hour);
                }
            } while (current_date.Ticks < end.Ticks);

            return positions;
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            _time_scale_control = null;
            _outer_lines.Clear();
            _long_lines.Clear();
            _middle_lines.Clear();
            _short_lines.Clear();
            _long_texts.Clear();
            _long_dates.Clear();
        }
        #endregion

        #region Event Handlers
        #endregion

        #region Properties

        internal override Object Control
        {
            get
            {
                return _time_scale_control;
            }
            set
            {
                if (value == null)
                    throw new ArgumentException("Control must not be null!");

                if (value is TimeScaleControl)
                {
                    _time_scale_control = (TimeScaleControl)value;
                }
                else
                    throw new ArgumentException("Type " + value.GetType());
            }
        }

        #endregion

        #region Dependency Properties
        #endregion

        #region Events
        #endregion

        #region Routed Events
        #endregion

        #region Attributes

        private TimeScaleControl _time_scale_control;

        private short _line_lod;
        private short _text_lod;
        private short _lod;

        private double _unit_offset;

        private static string[] _monthListLong;
        private static string[] _monthListShort;

        private readonly List<Rectangle> _outer_lines = new List<Rectangle>();
        private readonly List<Rectangle> _long_lines = new List<Rectangle>();
        private readonly List<Rectangle> _middle_lines = new List<Rectangle>();
        private readonly List<Rectangle> _short_lines = new List<Rectangle>();

        private readonly List<TextBlock> _long_texts = new List<TextBlock>();
        private readonly List<TextBlock> _long_dates = new List<TextBlock>();

        #endregion

        #region Tests
        #endregion

    }
}
