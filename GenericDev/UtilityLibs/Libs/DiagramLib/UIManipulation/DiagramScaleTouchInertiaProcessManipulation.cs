﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Logicx.DiagramLib.Scales;
using Logicx.WpfUtility.UiManipulation;

namespace Logicx.DiagramLib.UIManipulation
{
    public class DiagramScaleTouchInertiaProcessManipulation : TouchInertiaProcessManipulation
    {
        public class DiagrammScaleMoveEventArgs : EventArgs
        {
            public DiagrammScaleMoveEventArgs(double delta_width, double delta_height)
            {
                DeltaWidth = delta_width;
                DeltaHeight = delta_height;
            }

            public double DeltaWidth
            {
                get;
                private set;
            }

            public double DeltaHeight
            {
                get;
                private set;
            }
        }

        public class DiagramScaleScaleingEventArgs : EventArgs
        {
            public DiagramScaleScaleingEventArgs(double delta_scaling, Point down_position)
            {
                DeltaScaling = delta_scaling;
                DownPosition = down_position;
            }

            public double DeltaScaling
            {
                get;
                private set;
            }

            public Point DownPosition
            {
                get;
                private set;
            }
        }

        public class ShowRightClickInfoEventArgs : EventArgs
        {
            public ShowRightClickInfoEventArgs(Point down_position)
            {
                DownPosition = down_position;
            }

            public Point DownPosition
            {
                get;
                private set;
            }
        }

        public DiagramScaleTouchInertiaProcessManipulation(UIElement ui_element, Canvas canvas, UIElement event_base, EnabledAxes enabled_axes)
            : base(ui_element, canvas, event_base, enabled_axes, false)
        {
            //_with_scaling = false;
            _with_rotation = false;
            EnableInertiaAnimation = false;
        }



        public override void ProcessManipulationDelta(object sender, Windows7.Multitouch.Manipulation.ManipulationDeltaEventArgs e)
        {
            //if (e.RotationDelta > 0)
            //    _simulate_right_mouse_click = false;

            //if (e.ScaleDelta != 1)
            //    _simulate_right_mouse_click = false;

            //if (Math.Abs(e.CumulativeTranslation.Width) > 5 || Math.Abs(e.CumulativeTranslation.Height) > 5)
            //    _simulate_right_mouse_click = false;

            if (_with_scaling && _my_stylus_ids.Count > 1)
            {
                double rounded_scale_delta = Math.Round(e.ScaleDelta, 2);
                double new_delta_factor = Math.Round(Math.Abs(_last_scale_delta - rounded_scale_delta), 2);

                if (new_delta_factor != 0)
                {
                    _simulate_right_mouse_click = false;

                    
                    if(rounded_scale_delta < 1.0)
                    {
                        new_delta_factor *= -1;
                    }

                    Debug.WriteLine("### SCALE DELTA: " + new_delta_factor);
                    if (ScalingChanged != null)
                        ScalingChanged(this, new DiagramScaleScaleingEventArgs(new_delta_factor, DragOffset));

                    _last_scale_delta = rounded_scale_delta;
                }
            }
            else
            {
                _last_scale_delta = 1.0;
            }

            if (_with_translation && _my_stylus_ids.Count == 1)
            {
                if (Math.Abs(e.TranslationDelta.Width) > 0 || Math.Abs(e.TranslationDelta.Height) > 0)
                {
                    double event_delta_width = 0;
                    double event_delta_height = 0;

                    _internal_comulative_translate_x += e.TranslationDelta.Width;
                    _internal_comulative_translate_y += e.TranslationDelta.Height;

                    if (Math.Abs(_internal_comulative_translate_x) > _translate_trigger_x_value && Math.Abs(_internal_comulative_translate_y) > _translate_trigger_y_value)
                    {
                        event_delta_width = _internal_comulative_translate_x;
                        event_delta_height = _internal_comulative_translate_y;

                        _internal_comulative_translate_x = 0;
                        _internal_comulative_translate_y = 0;
                    }
                    else if (Math.Abs(_internal_comulative_translate_x) > _translate_trigger_x_value)
                    {
                        event_delta_width = _internal_comulative_translate_x;

                        _internal_comulative_translate_x = 0;
                    }
                    else if (Math.Abs(_internal_comulative_translate_y) > _translate_trigger_y_value)
                    {
                        event_delta_height = _internal_comulative_translate_y;

                        _internal_comulative_translate_y = 0;
                    }

                    if (TranslationChanged != null && (event_delta_height != 0 || event_delta_width != 0))
                        TranslationChanged(this, new DiagrammScaleMoveEventArgs(event_delta_width, event_delta_height));
                }
            }
        }

        public override void StylusDown(object sender, System.Windows.Input.StylusDownEventArgs e)
        {
            ResetRightClickUiTimer();

            if (!_device_list.Contains(e.StylusDevice))
                _device_list.Add(e.StylusDevice);

            if (StylusDownWiredThrough != null)
                StylusDownWiredThrough(sender, e);

            if (_my_stylus_ids == null || _my_stylus_ids.Count==0)
            {
                // erster stylus
                _simulate_right_mouse_click = true;
                _move_stylus_id = e.StylusDevice.Id;
                _stylus_translate_down_position = e.GetPosition(UIElement);
                _stylus_move_last_position = _stylus_translate_down_position;

                _first_down_time = DateTime.Now;
                _stylus_down_point = e.GetPosition(UIElement);

                // nach 200ms ein UI Feedback geben, dass der Rechts-Click simuliert wird
                // die UI Animation kann dann 500ms laufen - ab diesem zeitpunkt wird das dann als rechts-click gewertet
                _show_right_click_ui_delay = new Timer();
                _show_right_click_ui_delay.Interval = 200;
                _show_right_click_ui_delay.Elapsed += new ElapsedEventHandler(_show_right_click_ui_delay_Elapsed);
                _show_right_click_ui_delay.Start();

            }
            else
            {
                if (HideRightClickSimulationFeedback != null)
                    HideRightClickSimulationFeedback(this, EventArgs.Empty);

                _move_stylus_id = int.MinValue;
                _stylus_translate_down_position = default(Point);
                _stylus_move_last_position = default(Point);
                
                _simulate_right_mouse_click = false;
                _first_down_time = DateTime.MinValue;
                _stylus_down_point = default(Point);
            }

            _internal_comulative_translate_x = 0;
            _internal_comulative_translate_y = 0;

            base.StylusDown(sender, e);
        }

        public override void StylusMove(object sender, System.Windows.Input.StylusEventArgs e)
        {
            if (!_device_list.Contains(e.StylusDevice))
                _device_list.Add(e.StylusDevice);

            if (StylusMoveWiredThrough != null)
                StylusMoveWiredThrough(sender, e);

            base.StylusMove(sender, e);

            //if(_move_stylus_id == e.StylusDevice.Id)
            //{
            //    Point cur_position = e.GetPosition(UIElement);
            //    Vector delta_position = _stylus_move_last_position - cur_position;
            //}

            Point up_point = e.GetPosition(UIElement);
            Vector v_move = up_point - _stylus_down_point;
            if (v_move.Length > 7 && _simulate_right_mouse_click)
            {
                _simulate_right_mouse_click = false;
                _first_down_time = DateTime.MinValue;
                _stylus_down_point = default(Point);

                ResetRightClickUiTimer();
                if (HideRightClickSimulationFeedback != null)
                    HideRightClickSimulationFeedback(this, EventArgs.Empty);
            }
        }

        public override void StylusUp(object sender, System.Windows.Input.StylusEventArgs e)
        {
            if (!_device_list.Contains(e.StylusDevice))
                _device_list.Add(e.StylusDevice);

            ResetRightClickUiTimer();
            if (HideRightClickSimulationFeedback != null)
                HideRightClickSimulationFeedback(this, EventArgs.Empty);

            if (StylusUpWiredThrough != null)
                StylusUpWiredThrough(sender, e);

            if (_my_stylus_ids != null && _my_stylus_ids.Contains(e.StylusDevice.Id))
            {
                if(_my_stylus_ids.Count == 1)
                    _device_list.Clear();

                if (_my_stylus_ids.Count == 1 && _simulate_right_mouse_click)
                {

                    TimeSpan ts_action = DateTime.Now - _first_down_time;
                    //_first_down_time = DateTime.Now;
                    _first_down_time = DateTime.MinValue;

                    
                    if(ts_action.TotalSeconds < 0.7)
                    {
                        _simulate_right_mouse_click = false;
                        _first_down_time = DateTime.MinValue;
                        _stylus_down_point = default(Point);
                    }
                    else
                    {

                        Point up_point = e.GetPosition(UIElement);
                        Vector v_move = up_point - _stylus_down_point;
                        if(v_move.Length > 7)
                        {
                            _simulate_right_mouse_click = false;
                            _first_down_time = DateTime.MinValue;
                            _stylus_down_point = default(Point);
                        }
                    }
                }
                else
                {
                    _simulate_right_mouse_click = false;
                    _first_down_time = DateTime.MinValue;
                    _stylus_down_point = default(Point);
                }
            }
            else
            {
                _device_list.Clear();
                _simulate_right_mouse_click = false;
                _first_down_time = DateTime.MinValue;
                _stylus_down_point = default(Point);
            }

            _internal_comulative_translate_x = 0;
            _internal_comulative_translate_y = 0;

            base.StylusUp(sender, e);
        }

        private void ResetRightClickUiTimer()
        {
            if(_show_right_click_ui_delay != null)
            {
                _show_right_click_ui_delay.Elapsed -= _show_right_click_ui_delay_Elapsed;
                _show_right_click_ui_delay.Stop();
                _show_right_click_ui_delay.Dispose();
                _show_right_click_ui_delay = null;
            }
        }

        void _show_right_click_ui_delay_Elapsed(object sender, ElapsedEventArgs e)
        {
            ResetRightClickUiTimer();

            if (ShowRightClickSimulationFeedback != null)
                ShowRightClickSimulationFeedback(this, new ShowRightClickInfoEventArgs(_stylus_down_point));
        }


        public bool SimulateRightMouseClick
        {
            get { return _simulate_right_mouse_click; }
        }

        public bool WithTranslation
        {
            get { return _with_translation; }
            set { _with_translation = value; }
        }

        public double TranslateTriggerXValue
        {
            get { return _translate_trigger_x_value; }
            set { _translate_trigger_x_value = value; }
        }

        public double TranslateTriggerYValue
        {
            get { return _translate_trigger_y_value; }
            set { _translate_trigger_y_value = value; }
        }

        public List<StylusDevice> DeviceList
        {
            get { return _device_list; }
        }

        public event EventHandler<DiagramScaleScaleingEventArgs> ScalingChanged;
        public event EventHandler<DiagrammScaleMoveEventArgs> TranslationChanged;
        public event EventHandler<ShowRightClickInfoEventArgs> ShowRightClickSimulationFeedback;
        public event EventHandler HideRightClickSimulationFeedback;

        public event StylusDownEventHandler StylusDownWiredThrough;
        public event StylusEventHandler StylusUpWiredThrough;
        public event StylusEventHandler StylusMoveWiredThrough;

        private Timer _show_right_click_ui_delay = null;
        private bool _with_translation = true;
        private double _translate_trigger_x_value = 1.5;
        private double _translate_trigger_y_value = 1.5;

        private double _last_scale_delta = 1.0;

        private Point _stylus_translate_down_position = default(Point);
        private Point _stylus_move_last_position = default(Point);
        private int _move_stylus_id = int.MinValue;

        private double _internal_comulative_translate_x = 0;
        private double _internal_comulative_translate_y = 0;

        private bool _simulate_right_mouse_click = false;
        private DateTime _first_down_time = DateTime.MinValue;
        private Point _stylus_down_point;

        private List<StylusDevice> _device_list = new List<StylusDevice>();
    }
}
