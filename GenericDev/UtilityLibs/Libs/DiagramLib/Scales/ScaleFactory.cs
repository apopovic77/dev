﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Logicx.DiagramLib.ViewConfig;

namespace Logicx.DiagramLib.Scales
{


    internal struct LongRange<T>
    {
        private readonly double _min;
        private readonly double _max;
        private readonly AxisScaleConfig<T> _config;


        public LongRange(double min, double max, AxisScaleConfig<T> config)
        {
            if (max < min)
                throw new ArgumentException();

            _min = min;
            _max = max;
            _config = config;
        }

        public double Min
        {
            get
            {
                return _min;
            }
        }

        public double Max
        {
            get
            {
                return _max;
            }
        }

        public AxisScaleConfig<T> Config
        {
            get
            {
                return _config;
            }
        }

        public override string ToString()
        {
            return "LongRange [ Min=" + Min + ", Max=" + Max + ", Config=[Description=" + _config.Description + ", Start=" + _config.Start + ", End=" + _config.End + "] ]";
        }
    }

    internal class LongRangeOverlapComparer<T> : IEqualityComparer<LongRange<T>>
    {
        public bool Equals(LongRange<T> x, LongRange<T> y)
        {
            if (!x.Config.Equals(y.Config))
                return false;

            if (x.Min > y.Max)
                return false;

            if (x.Max < y.Min)
                return false;

            return true;
        }

        public int GetHashCode(LongRange<T> obj)
        {
            //return obj.GetHashCode();
            return obj.Config.GetHashCode();
        }

    }


    internal class ScaleFactory
    {

        #region Constants

        private const long ONE_OUR_MSEC = 3600l * 1000; // * 10000000;
        private const long MAX_DAY_MSEC = 31 * 24 * ONE_OUR_MSEC;
        private const long MAX_MONTH_MSEC = 366 * 24 * ONE_OUR_MSEC;

        private const long ONE_INT = 1;
        private const long MAX_INTS = 1000;

        private const long MIN_DBL = 0;
        private const long MAX_DOUBLE = 5000;

        #endregion

        #region Construction and Initialization

        private ScaleFactory()
        {
            _scale_instances_datetime = new Dictionary<LongRange<DateTime>, ScaleType>(new LongRangeOverlapComparer<DateTime>());

            _scale_instances_int = new Dictionary<LongRange<int>, ScaleType>(new LongRangeOverlapComparer<int>());

            _scale_instances_double = new Dictionary<LongRange<double>, ScaleType>(new LongRangeOverlapComparer<double>());

            _scale_instances_discrete = new Dictionary<LongRange<object>, ScaleType>(new LongRangeOverlapComparer<object>());
        }

        #endregion

        #region Operations

        public static ScaleFactory GetInstance()
        {
            return _instance;
        }

        public static long MinDatetimeMsec()
        {
            return ONE_OUR_MSEC * 3;
        }

        public static int MinInt()
        {
            return (int)ONE_INT;
        }

        public static double MinDouble()
        {
            return MIN_DBL;
        }

        public void AddScale<T>(AxisScaleConfig<T> config)
        {
            if (config.Start is DateTime)
            {
                //Debug.WriteLine("Add Scale " + config.GetHashCode() + ": " + config.Start + ", " + config.End);
                LongRange<T> day_range = new LongRange<T>(ONE_OUR_MSEC * 3, MAX_DAY_MSEC, config);
                LongRange<DateTime> dt = (LongRange<DateTime>)(object)day_range;

                if (!_scale_instances_datetime.ContainsKey(dt))
                    _scale_instances_datetime.Add(dt, new DayScale());

                LongRange<T> month_range = new LongRange<T>(MAX_DAY_MSEC + 1, MAX_MONTH_MSEC, config);
                dt = (LongRange<DateTime>)(object)month_range;

                if (!_scale_instances_datetime.ContainsKey(dt))
                    _scale_instances_datetime.Add(dt, new MonthScale());
            }
            else if (config.Start is int)
            {
                LongRange<T> long_range = new LongRange<T>(ONE_INT, MAX_INTS, config);
                LongRange<int> range = (LongRange<int>)(object)long_range;

                if (!_scale_instances_int.ContainsKey(range))
                    _scale_instances_int.Add(range, new IntScale());
            }
            else if (config.Start is double)
            {
                LongRange<T> long_range = new LongRange<T>(MIN_DBL, MAX_DOUBLE, config);
                LongRange<double> range = (LongRange<double>)(object)long_range;

                if (!_scale_instances_double.ContainsKey(range))
                    _scale_instances_double.Add(range, new DoubleScale());
            }
            else if (config.Values.Count() > 0)
            {
                LongRange<T> long_range = new LongRange<T>(0, 0, config);
                LongRange<object> range = (LongRange<object>)(object)long_range;

                if (!_scale_instances_discrete.ContainsKey(range))
                    _scale_instances_discrete.Add(range, new DiscreteScale());
            }
        }

        public ScaleType GetScale<T>(AxisScaleConfig<T> config)
        {
            if (config.Start is DateTime && config.End is DateTime)
            {
                DateTime start = (DateTime)(Object)config.Start;
                DateTime end = (DateTime)(Object)config.End;
                double msec = Math.Round((end - start).TotalSeconds, 0) * 1000;

                IEnumerable<KeyValuePair<LongRange<DateTime>, ScaleType>> key_value_pairs =
                    _scale_instances_datetime.Where(o => o.Key.Config.Equals(config) && o.Key.Min <= msec && o.Key.Max >= msec);
                if (key_value_pairs.Count() > 0)
                    return key_value_pairs.First().Value;
            }
            else if (config.Start is int && config.End is int)
            {
                int start = (int)(Object)config.Start;
                int end = (int)(Object)config.End;
                int units = end - start;

                IEnumerable<KeyValuePair<LongRange<int>, ScaleType>> key_value_pairs =
                    _scale_instances_int.Where(o => o.Key.Config.Equals(config) && o.Key.Min <= units && o.Key.Max >= units);
                if (key_value_pairs.Count() > 0)
                    return key_value_pairs.First().Value;
            }
            else if (config.Start is double && config.End is double)
            {
                double start = (double)(Object)config.Start;
                double end = (double)(Object)config.End;
                double units = end - start;

                IEnumerable<KeyValuePair<LongRange<double>, ScaleType>> key_value_pairs =
                    _scale_instances_double.Where(o => o.Key.Config.Equals(config) && o.Key.Min <= units && o.Key.Max >= units);
                if (key_value_pairs.Count() > 0)
                    return key_value_pairs.First().Value;

            }
            else if (config.Values.Count() > 0)
            {
                IEnumerable<KeyValuePair<LongRange<object>, ScaleType>> key_value_pairs = _scale_instances_discrete.Where(o => o.Key.Config.Equals(config));
                if (key_value_pairs.Count() > 0)
                {
                    return key_value_pairs.First().Value;
                }
            }

            return null;
        }

        #endregion

        #region Event Handlers
        #endregion

        #region Properties
        #endregion

        #region Dependency Properties
        #endregion

        #region Events
        #endregion

        #region Routed Events
        #endregion

        #region Attributes

        private static readonly ScaleFactory _instance = new ScaleFactory();

        private readonly Dictionary<LongRange<DateTime>, ScaleType> _scale_instances_datetime;

        private readonly Dictionary<LongRange<int>, ScaleType> _scale_instances_int;

        private readonly Dictionary<LongRange<double>, ScaleType> _scale_instances_double;

        private readonly Dictionary<LongRange<object>, ScaleType> _scale_instances_discrete;

        #endregion

        #region Tests
        #endregion

    }
}
