﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Logicx.Geo.Geometries;
using Logicx.WpfUtility.WpfHelpers;
using MathLib;

namespace Logicx.WpfUtility.CustomControls.Taskbar
{
    public enum RenderDirection
    {
        Left, Right, Top, Bottom
    }

    public enum ItemType
    {
        Normal, Button, ToggleButton
    }
    /// <summary>
    /// Interaction logic for AppleTaskbarExplorer.xaml
    /// </summary>
    public partial class PointerWrapPanel : UserControl
    {
        public PointerWrapPanel()
        {
            InitializeComponent();
        }

        #region Add/Remove Items
        public void AddItem(FrameworkElement elem, ItemType type, int insert_pos)
        {
            if(type == ItemType.Normal)
            {
                elem.MouseDown += new MouseButtonEventHandler(elem_MouseDown);
                elem.MouseUp += new MouseButtonEventHandler(elem_MouseUp);
                if (insert_pos < 0 || insert_pos > items_panel.Children.Count)
                    items_panel.Children.Add(elem);
                else
                    items_panel.Children.Insert(insert_pos, elem);
            }
            else if(type == ItemType.Button)
            {
                elem.MouseUp += new MouseButtonEventHandler(elem_MouseUp);
                items_panel.Children.Add(elem);
            }
            else if (type == ItemType.Button)
            {
                Button b = new Button();
                b.Content = elem;
                b.Click += new RoutedEventHandler(items_button_Click);
                if (insert_pos < 0 || insert_pos > items_panel.Children.Count)
                    items_panel.Children.Add(b);
                else
                    items_panel.Children.Insert(insert_pos, b);
            }
            else if(type == ItemType.ToggleButton)
            {
                ToggleButton togglebutton = new ToggleButton();
                togglebutton.Content = elem;
                togglebutton.Click += new RoutedEventHandler(items_togglebutton_Click);
                if (insert_pos < 0 || insert_pos > items_panel.Children.Count)
                    items_panel.Children.Add(togglebutton);
                else
                    items_panel.Children.Insert(insert_pos, togglebutton);
            }
        }

        public void AddItem(FrameworkElement elem, ItemType type)
        {
            AddItem(elem, type, -1);
        }

        public void RemoveItem(FrameworkElement elem)
        {

            for (int i = 0; i < items_panel.Children.Count; i++ )
            {
                ContentControl cont = items_panel.Children[i] as ContentControl;
                if (cont != null && cont.Content == elem)
                    items_panel.Children.Remove(cont);
            }
        }
        #endregion

        #region Show/Hide
        public void Show(Vector2d arrow_position, double width, double height, RenderDirection render_dir)
        {
            if(!_showen)
            {
                this.Visibility = Visibility.Hidden;
                UpdatePath(arrow_position, width, height, render_dir);
                StartFadeInAnimation(arrow_position);
                _showen = true;
            }
            else
            {
                if (this.Visibility == Visibility.Hidden)
                    this.Visibility = Visibility.Visible;
                UpdatePath(arrow_position, width, height, render_dir);
            }

            _last_arrow_pos = arrow_position;
        }

        public void Hide()
        {
            Hide(true);
        }

        public void Hide(bool with_event)
        {
            if(!_showen)
                return;

            _throw_fadeout_finished_event = with_event;
            StartFadeOutAnimation();
        }
        #endregion

        #region Create Path
        protected void UpdatePath(Vector2d arrow_position, double width, double height, RenderDirection render_dir)
        {
            AdaptWidthHeight(ref width, ref height, render_dir);
            Vector2d topleft = GeneratePath(arrow_position, width, height, render_dir);
            Canvas.SetLeft(items_panel, topleft.X + _panel_margin);
            Canvas.SetTop(items_panel, topleft.Y + _panel_margin);
            items_panel.MaxWidth = width - 2 * _panel_margin;
            items_panel.MaxHeight = height - 2*_panel_margin;
        }

        protected Vector2d GeneratePath(Vector2d arrow_position, double width, double height, RenderDirection render_dir)
        {
            Vector2d topleft = CheckArrowPosition(arrow_position, width, height, render_dir);

            List<Vector2d> poly_verts = new List<Vector2d>();
            double corner_rad = 10;

            poly_verts.Add(new Vector2d(topleft.X, topleft.Y + corner_rad));
            poly_verts.Add(new Vector2d(topleft.X + corner_rad, topleft.Y));
            EllipseGeometry ell_geom = new EllipseGeometry(new Point(topleft.X + corner_rad, topleft.Y + corner_rad), corner_rad, corner_rad);

            poly_verts.Add(new Vector2d(topleft.X + width - corner_rad , topleft.Y));
            poly_verts.Add(new Vector2d(topleft.X + width, topleft.Y + corner_rad));
            EllipseGeometry ell_geom2 = new EllipseGeometry(new Point(topleft.X + width - corner_rad, topleft.Y + corner_rad), corner_rad, corner_rad);

            poly_verts.Add(new Vector2d(topleft.X + width, topleft.Y + height - corner_rad));
            poly_verts.Add(new Vector2d(topleft.X + width - corner_rad, topleft.Y + height));
            EllipseGeometry ell_geom3 = new EllipseGeometry(new Point(topleft.X + width - corner_rad, topleft.Y + height - corner_rad), corner_rad, corner_rad);

            poly_verts.Add(new Vector2d(topleft.X + corner_rad, topleft.Y + height));
            poly_verts.Add(new Vector2d(topleft.X, topleft.Y + height - corner_rad));
            EllipseGeometry ell_geom4 = new EllipseGeometry(new Point(topleft.X + corner_rad, topleft.Y + height - corner_rad), corner_rad, corner_rad);
            poly_verts.Add(poly_verts[0]);

            GeometryGroup ell_group = new GeometryGroup();
            ell_group.Children.Add(ell_geom);
            ell_group.Children.Add(ell_geom2);
            ell_group.Children.Add(ell_geom3);
            ell_group.Children.Add(ell_geom4);

            List<Vector2d> symbol_verts = new List<Vector2d>();

            Vector2d arrow_center_pos;
            switch (render_dir)
            {
                case RenderDirection.Bottom:
                    arrow_center_pos = arrow_position + new Vector2d(0, _symbol_spacing);
                    symbol_verts.Add(new Vector2d(arrow_center_pos.X - _symbol_spacing, arrow_center_pos.Y));
                    symbol_verts.Add(new Vector2d(arrow_center_pos.X + _symbol_spacing, arrow_center_pos.Y));
                    symbol_verts.Add(new Vector2d(arrow_center_pos.X, arrow_center_pos.Y - _symbol_spacing));
                    break;
                case RenderDirection.Top:
                    arrow_center_pos = arrow_position - new Vector2d(0, _symbol_spacing);
                    symbol_verts.Add(new Vector2d(arrow_center_pos.X - _symbol_spacing, arrow_center_pos.Y));
                    symbol_verts.Add(new Vector2d(arrow_center_pos.X + _symbol_spacing, arrow_center_pos.Y));
                    symbol_verts.Add(new Vector2d(arrow_center_pos.X, arrow_center_pos.Y + _symbol_spacing));
                    break;
                case RenderDirection.Left:
                    arrow_center_pos = arrow_position - new Vector2d( _symbol_spacing, 0);
                    symbol_verts.Add(new Vector2d(arrow_center_pos.X, arrow_center_pos.Y - _symbol_spacing));
                    symbol_verts.Add(new Vector2d(arrow_center_pos.X, arrow_center_pos.Y + _symbol_spacing));
                    symbol_verts.Add(new Vector2d(arrow_center_pos.X + _symbol_spacing, arrow_center_pos.Y));
                    break;
                case RenderDirection.Right:
                    arrow_center_pos = arrow_position + new Vector2d(_symbol_spacing, 0);
                    symbol_verts.Add(new Vector2d(arrow_center_pos.X, arrow_center_pos.Y - _symbol_spacing));
                    symbol_verts.Add(new Vector2d(arrow_center_pos.X, arrow_center_pos.Y + _symbol_spacing));
                    symbol_verts.Add(new Vector2d(arrow_center_pos.X - _symbol_spacing, arrow_center_pos.Y));
                    break;

                
            }

            symbol_verts.Add(symbol_verts.First());

            BoundingBox bounding_box;
            GeometryGroup geom_group = new GeometryGroup();
            geom_group.Children.Add(DataTypeConverterFunctions.SimplePolygon2StreamGeometry(poly_verts, out bounding_box));
            geom_group.Children.Add(DataTypeConverterFunctions.SimplePolygon2StreamGeometry(symbol_verts, out bounding_box));
            geom_group.Children.Add(ell_geom);
            
            CombinedGeometry comb_geom = new CombinedGeometry(GeometryCombineMode.Union, geom_group, ell_group);

            border_path.Data = comb_geom;

            return topleft;
            
        }

        /// <summary>
        /// Ausgehend von der Pfeilspitze, wird die TopLeft Position des Controls berechnet und zurückgegeben
        /// </summary>
        /// <param name="arrow_pos"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="render_dir"></param>
        /// <returns></returns>
        protected Vector2d CheckArrowPosition(Vector2d arrow_pos, double width, double height, RenderDirection render_dir)
        {
            Vector2d topleft = new Vector2d();
            Vector2d arrow_center_pos;
            switch (render_dir)
            {
                case RenderDirection.Left:
                    arrow_center_pos = arrow_pos - new Vector2d(_symbol_spacing, 0);
                    topleft = new Vector2d(arrow_center_pos.X - width, arrow_center_pos.Y - height / 2);

                    if (topleft.Y < 0)
                        topleft.Y = 0;
                    if (topleft.Y + height > main_canvas.ActualHeight)
                        topleft.Y -= topleft.Y + height - main_canvas.ActualHeight;

                    break;
                case RenderDirection.Right:
                    arrow_center_pos = arrow_pos + new Vector2d(_symbol_spacing, 0);
                    topleft = new Vector2d(arrow_center_pos.X, arrow_center_pos.Y + height / 2);

                    if (topleft.Y < 0)
                        topleft.Y = 0;
                    if (topleft.Y + height > main_canvas.ActualHeight)
                        topleft.Y -= topleft.Y + height - main_canvas.ActualHeight;

                    break;
                case RenderDirection.Top:
                    arrow_center_pos = arrow_pos - new Vector2d(0, _symbol_spacing);
                    topleft = new Vector2d(arrow_center_pos.X - width / 2, arrow_center_pos.Y - height);

                    if (topleft.X < 0)
                        topleft.X = 0;
                    if (topleft.X + width > main_canvas.ActualWidth)
                        topleft.X -= topleft.X + width - main_canvas.ActualWidth;

                    break;
                case RenderDirection.Bottom:
                    arrow_center_pos = arrow_pos + new Vector2d(0, _symbol_spacing);
                    topleft = new Vector2d(arrow_center_pos.X - width / 2, arrow_center_pos.Y);

                    if (topleft.X < 0)
                        topleft.X = 0;
                    if (topleft.X + width > main_canvas.ActualWidth)
                        topleft.X -= topleft.X + width - main_canvas.ActualWidth;
                    break;

            }

            return topleft;
        }

        /// <summary>
        /// Diese Methode passt die Dimensionen des resultierenden Controls an.
        /// Durch den dargestellten Pfeil, verliert das Control etwas an der möglichen darzustellenden Größe
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="render_dir"></param>
        protected void AdaptWidthHeight(ref double width, ref double height, RenderDirection render_dir)
        {
            switch (render_dir)
            {
                case RenderDirection.Bottom:
                case RenderDirection.Top:
                    height -= _symbol_spacing;
                    break;
                case RenderDirection.Left:
                case RenderDirection.Right:
                    width -= _symbol_spacing;
                    break;
            }
        }
        #endregion

        #region ScaleAnimations
        protected void StartFadeInAnimation(Vector2d arrow_position)
        {
            if (_fadein_anims != 0)
                return;

            Monitor.Enter(_fadein_monitor);
            _fadein_anims = 2;
            Monitor.Exit(_fadein_monitor);

            Storyboard sb = GetScaleAnimation(0, 1, arrow_position);
            sb.Completed += new EventHandler(fadein_Completed);            

            this.Visibility = System.Windows.Visibility.Visible;
            border_path.BeginStoryboard(sb);
            items_panel.BeginStoryboard(sb);

        }

        protected void StartFadeOutAnimation()
        {
            if (_fadeout_anims != 0)
                return;

            Storyboard sb = GetScaleAnimation(1, 0, _last_arrow_pos);
            sb.Completed += new EventHandler(fadeout_Completed);
            
            border_path.BeginStoryboard(sb);
            items_panel.BeginStoryboard(sb);

            Monitor.Enter(_fadeout_monitor);
            _fadeout_anims = 2;
            Monitor.Exit(_fadeout_monitor);
            
        }

        protected Storyboard GetScaleAnimation(double from, double to, Vector2d arrow_pos)
        {
            DoubleAnimation scale_anim_x = new DoubleAnimation();
            if (to == 0)
            {
                scale_anim_x.DecelerationRatio = 0.2;
                scale_anim_x.AccelerationRatio = 0.8;
            }
            else
            {
                scale_anim_x.DecelerationRatio = 0.8;
                scale_anim_x.AccelerationRatio = 0.2;
            }
            scale_anim_x.From = from;
            scale_anim_x.To = to;
            scale_anim_x.Duration = new Duration(TimeSpan.FromSeconds(0.5));
            Storyboard.SetTargetProperty(scale_anim_x, new PropertyPath("(UIElement.RenderTransform).(ScaleTransform.ScaleX)"));

            DoubleAnimation scale_anim_y = new DoubleAnimation();
            if (to == 0)
            {
                scale_anim_y.DecelerationRatio = 0.2;
                scale_anim_y.AccelerationRatio = 0.8;
            }
            else
            {
                scale_anim_y.DecelerationRatio = 0.8;
                scale_anim_y.AccelerationRatio = 0.2;
            }
            scale_anim_y.From = from;
            scale_anim_y.To = to;
            scale_anim_y.Duration = new Duration(TimeSpan.FromSeconds(0.5));
            Storyboard.SetTargetProperty(scale_anim_y, new PropertyPath("(UIElement.RenderTransform).(ScaleTransform.ScaleY)"));

            Storyboard sb = new Storyboard();

            sb.Children.Add(scale_anim_x);
            sb.Children.Add(scale_anim_y);
            
            border_path.RenderTransform = new ScaleTransform(1, 1, arrow_pos.X, arrow_pos.Y);
            double left = Canvas.GetLeft(items_panel);
            double top = Canvas.GetTop(items_panel);
            items_panel.RenderTransform = new ScaleTransform(1, 1, arrow_pos.X - left, arrow_pos.Y - top);

            return sb;

        }
        #endregion

        #region Properties
        /// <summary>
        /// Dient zur Speicherung eines bestimmten Elementes, auf das der Pfeil zeigen soll
        /// </summary>
        public FrameworkElement PointToElement
        {
            set; get;
        }

        public bool IsOpen
        {
            get
            {
                return _showen;
            }
        }
        #endregion

        #region Drag Operations
        protected void RevertDragOperations()
        {
            if(_drag_mode_enabled)
            {
                _elem_mouse_down = false;
                _drag_mode_enabled = false;
                if(main_canvas.Children.Contains(_dragable_mouse_down_element))
                    main_canvas.Children.Remove(_dragable_mouse_down_element);

                items_panel.Children.Insert(_dragable_element_pos, _dragable_mouse_down_element);

                main_canvas.Background = null;
            }
        }

        protected void FinishDragging()
        {
            if (_drag_mode_enabled)
            {
                main_canvas.Background = null;
                _drag_mode_enabled = false;
                _dragable_mouse_down_element.MouseUp -= elem_MouseUp;
                _dragable_mouse_down_element.MouseDown -= elem_MouseDown;
            }

            _elem_mouse_down = false;
        }

        protected bool IsPointOverPath(Point new_pos)
        {
            BoundingBox box = new BoundingBox(new Vector2d(border_path.Data.Bounds.TopLeft.X, border_path.Data.Bounds.TopLeft.Y), new Vector2d(border_path.Data.Bounds.BottomRight.X, border_path.Data.Bounds.BottomRight.Y));
            return box.PointInside(new Vector2f((float) new_pos.X, (float) new_pos.Y));
        }
        #endregion

        #region EventHandler
        void elem_MouseUp(object sender, MouseButtonEventArgs e)
        {
            Point new_pos = e.MouseDevice.GetPosition(this);
            if (!IsPointOverPath(new_pos))
                FinishDragging();
            else
                RevertDragOperations();
            
        }

        void elem_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if(_elem_mouse_down == false)
            {
                _elem_mouse_down = true;
                _dragable_mouse_down_element = sender as FrameworkElement;
                _dragable_element_pos = items_panel.Children.IndexOf(_dragable_mouse_down_element);
                _elem_mouse_down_pos = e.MouseDevice.GetPosition(this);
                _elem_mouse_down_rel_pos = e.MouseDevice.GetPosition(_dragable_mouse_down_element);
            }
        }

        private void main_canvas_MouseMove(object sender, MouseEventArgs e)
        {
            if(_elem_mouse_down)
            {
                Point new_pos = e.MouseDevice.GetPosition(this);
                if(_drag_mode_enabled)
                {
                    Canvas.SetLeft(_dragable_mouse_down_element, new_pos.X - _elem_mouse_down_rel_pos.X);
                    Canvas.SetTop(_dragable_mouse_down_element, new_pos.Y - _elem_mouse_down_rel_pos.Y);
                    if(IsOpen)
                    { 
                        if(!IsPointOverPath(new_pos))
                            Hide(false);
                    }
                }
                else
                {
                    if (Math.Abs((_elem_mouse_down_pos - new_pos).X) > _drag_offset || Math.Abs((_elem_mouse_down_pos - new_pos).Y) > _drag_offset)
                    {
                        _drag_mode_enabled = true;
                        items_panel.Children.Remove(_dragable_mouse_down_element);
                        main_canvas.Children.Add(_dragable_mouse_down_element);
                        main_canvas.Background = Brushes.Transparent;
                        Canvas.SetLeft(_dragable_mouse_down_element, new_pos.X - _elem_mouse_down_rel_pos.X);
                        Canvas.SetTop(_dragable_mouse_down_element, new_pos.Y - _elem_mouse_down_rel_pos.Y);
                    }
                    else
                        Debug.WriteLine(_elem_mouse_down_pos + " zu " + new_pos);
                }
            }
        }

        private void main_canvas_MouseLeave(object sender, MouseEventArgs e)
        {
            RevertDragOperations();
        }

        private void main_canvas_MouseUp(object sender, MouseButtonEventArgs e)
        {
            Point new_pos = e.MouseDevice.GetPosition(this);
            if (!IsPointOverPath(new_pos))
                FinishDragging();
            else
                RevertDragOperations();
        }

        void fadein_Completed(object sender, EventArgs e)
        {
            Monitor.Enter(_fadein_monitor);
            _fadein_anims--;
            if (_fadein_anims == 0)
            {
                _showen = true;
                if (FadeInCompleted != null)
                    FadeInCompleted(this, new EventArgs());
            }
            Monitor.Exit(_fadein_monitor);

            ClockGroup c = sender as ClockGroup;
            if(c != null && c.Timeline != null && !c.Timeline.IsFrozen)
                c.Timeline.Completed -= fadein_Completed;

        }

        void fadeout_Completed(object sender, EventArgs e)
        {
            Monitor.Enter(_fadeout_monitor);
            _fadeout_anims--;
            if (_fadeout_anims == 0)
            {
                _showen = false;
                if (_throw_fadeout_finished_event && FadeOutCompleted != null)
                    FadeOutCompleted(this, new EventArgs());
            }
            Monitor.Exit(_fadeout_monitor);

            ClockGroup c = sender as ClockGroup;
            if (c != null && c.Timeline != null && !c.Timeline.IsFrozen)
                c.Timeline.Completed -= fadeout_Completed;
        }

        void items_togglebutton_Click(object sender, RoutedEventArgs e)
        {
            ToggleButton button = sender as ToggleButton;

            if(button != null)
            {
                if(ToggleButtonClicked != null)
                    ToggleButtonClicked(button.Content, new EventArgs());
            }

        }

        void items_button_Click(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;

            if (button != null)
            {
                if (ButtonClicked != null)
                    ButtonClicked(button.Content, new EventArgs());
            }
        }

        #endregion

        #region Events
        public event EventHandler<EventArgs> FadeOutCompleted;
        public event EventHandler<EventArgs> FadeInCompleted;
        public event EventHandler<EventArgs> ButtonClicked;
        public event EventHandler<EventArgs> ToggleButtonClicked;
        #endregion

        #region Attributes
        private Vector2d _last_arrow_pos;

        private readonly int _panel_margin = 5;
        private readonly double _symbol_spacing = 10;

        private int _fadeout_anims;
        private int _fadein_anims;
        private bool _throw_fadeout_finished_event;
        private object _fadeout_monitor = new object();
        private object _fadein_monitor = new object();

        #region Dragging
        private bool _elem_mouse_down;
        private bool _drag_mode_enabled;
        private FrameworkElement _dragable_mouse_down_element;
        private Point _elem_mouse_down_pos;
        private Point _elem_mouse_down_rel_pos;
        private readonly double _drag_offset = 10;
        private int _dragable_element_pos;
        #endregion

        private bool _showen;
        #endregion
    }
}
