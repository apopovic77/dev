﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Data;

namespace Logicx.WpfUtility.Converters
{

    public enum DateTimePropertyName
    {
        Date,
        Hour,
        Minute,
        Second
    }

    public class DateTimeToIntConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is DateTime && parameter is DateTimePropertyName)
            {
                int result = 0;
                DateTime date_time = (DateTime)value;

                DateTimePropertyName name = (DateTimePropertyName)parameter;
                switch (name)
                {
                    case DateTimePropertyName.Date:
                        return date_time.Date;
                    case DateTimePropertyName.Hour:
                        result = date_time.Hour;
                        break;
                    case DateTimePropertyName.Minute:
                        result = date_time.Minute;
                        break;
                    case DateTimePropertyName.Second:
                        result = date_time.Second;
                        break;
                }

                return result;
            }

            return -1;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }

    }
}
