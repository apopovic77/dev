﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Logicx.WpfUtility.WindowManagement;
using Logicx.WpfUtility.WpfHelpers;

namespace Logicx.WpfUtility.CustomControls.OverlayManager
{
    public class OverlayCanvas : Canvas
    {
        protected override System.Windows.Media.HitTestResult HitTestCore(System.Windows.Media.PointHitTestParameters hitTestParameters)
        {
            if (!_canvas_mouse_capute_mode)
            {
                if(Mouse.LeftButton == MouseButtonState.Pressed && OverlayManager.HasActiveWindows() && !OverlayManager.StylusOrigin)
                {
                    OverlayManager.SetActiveDialog(null);
                }
                return null;
            }

            return base.HitTestCore(hitTestParameters);
        }

        protected override System.Windows.Media.GeometryHitTestResult HitTestCore(System.Windows.Media.GeometryHitTestParameters hitTestParameters)
        {
            if (!_canvas_mouse_capute_mode)
            {
                if (Mouse.LeftButton == MouseButtonState.Pressed && OverlayManager.HasActiveWindows() && !OverlayManager.StylusOrigin)
                {
                    OverlayManager.SetActiveDialog(null);
                }

                return null;
            }

            return base.HitTestCore(hitTestParameters);
        }

        /// <summary>
        /// Liefert die Instanz des OverlayManagers für dieses content control
        /// </summary>
        /// <remarks>Die Eigenschaft kann auch einen NULL Wert liefern, wenn das Control nicht im OverlayManager angezeigt wird.</remarks>
        public IOverlayManagerBase OverlayManager
        {
            get
            {
                if (_overlay_manager != null)
                    return _overlay_manager;

                DependencyObject o = UIHelper.TryFindParentGeneric(this, typeof(OverlayManagerBase<BaseFrame, SubMenuPanel, WindowTaskBar, WindowTitleBar, OverlayManager, SubMenuPanelMenuItem>));
                if (o != null)
                    _overlay_manager = o as IOverlayManagerBase;

                return _overlay_manager;

            }
        }

        public bool CanvasMouseCapute
        {
            get { return _canvas_mouse_capute_mode; }
            set { _canvas_mouse_capute_mode = value; }
        }

        private IOverlayManagerBase _overlay_manager;
        private bool _canvas_mouse_capute_mode = false;
        private OverlayManager _parent_overlay_manager = null;

    }
}
