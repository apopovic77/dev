﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using Logicx.WpfUtility.WpfHelpers;

namespace Logicx.WpfUtility.CustomControls.OverlayManager
{
    public class OverlayWizardPage : AdornerDecorator
    {
        #region Construction
        /// <summary>
        /// Static constructor
        /// </summary>
        static OverlayWizardPage()
        {
            FrameworkPropertyMetadata metadataPageTitle = new FrameworkPropertyMetadata("Title");
            PageTitleProperty = DependencyProperty.Register("PageTitle",
                                                            typeof(string), typeof(OverlayContent), metadataPageTitle);

            FrameworkPropertyMetadata metadataPageSubTitle = new FrameworkPropertyMetadata("");
            PageSubTitleProperty = DependencyProperty.Register("PageSubTitle",
                                                            typeof(string), typeof(OverlayContent), metadataPageSubTitle);

        }
        #endregion

        #region Operation

        #region Wizard button handlers
        /// <summary>
        /// Called if the user clicks the NEXT button
        /// </summary>
        /// <param name="navigatedFrom">Previous wizard page in the classtack</param>
        /// <param name="fromDirection">Navigation direction of the previous page</param>
        /// <returns>The wizard page which should be shown next or null if the wizard should stay at the current page</returns>
        /// <remarks>Implement page determination logic or branching here</remarks>
        public virtual OverlayWizardPage Next(OverlayWizardPage navigatedFrom, NavigationSource fromDirection)
        {
            if(ParentWizard != null)
                return ParentWizard.GetNextPageInChain(this);

            return null;
        }

        /// <summary>
        /// Called if the user clicks the BACK button
        /// </summary>
        /// <param name="navigatedFrom">Previous wizard page in the callstack</param>
        /// <param name="fromDirection">Navigation direction of the previous page</param>
        /// <returns>The wizard page which should be shown next or null if the wizard should stay at the current page</returns>
        /// <remarks>Implement page determination logic or branching here</remarks>
        public virtual OverlayWizardPage Prev(OverlayWizardPage navigatedFrom, NavigationSource fromDirection)
        {
            if (ParentWizard != null)
                return ParentWizard.GetPrevPageInChain(this);

            return null;            
        }

        /// <summary>
        /// Called if the user clicks the FINISH button
        /// </summary>
        /// <param name="navigatedFrom">Previous wizard page in the callstack</param>
        /// <param name="fromDirection">Navigation direction of the previous page</param>
        /// <returns>True if the wizard can be finished</returns>
        public virtual bool Finish(OverlayWizardPage navigatedFrom, NavigationSource fromDirection)
        {
            // finalize data here
            // the wizard dialog will be closed after this method call

            return true;
        }

        /// <summary>
        /// Called from the wizard if the user closes the wizard dialog with the X or Cancel command
        /// </summary>
        public virtual void Cancel()
        {
            
        }

        /// <summary>
        /// Called if the user clicks the HELP button
        /// </summary>
        public virtual void Help()
        {
            
        }
        #endregion

        #region Page change handlers
        /// <summary>
        /// Called from the wizard after the current page was hidden from the wizard. 
        /// This usually happens if the user clicks a NEXT, BACK or CLOSE button.
        /// </summary>
        public virtual void AfterHidePage()
        {
        }
        /// <summary>
        /// Called from the wizard after the current page was added to the parent container's
        /// child element collection and will be shown in the wizard.
        /// </summary>
        public virtual void AfterShowPage()
        {
            
        }
        #endregion



        /// <summary>
        /// Sets the parent wizard who hosts this page
        /// </summary>
        /// <param name="parent"></param>
        internal void SetParentWizard(OverlayWizard parent)
        {
            _parent_wizard = parent;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets the parent wizard control (may be null)
        /// </summary>
        public OverlayWizard ParentWizard
        {
            get { return _parent_wizard; }
        }

        /// <summary>
        /// Dependency Eigenschaft welche den Text des Dialogtitels zurückgibt
        /// </summary>
        public string PageTitle
        {
            get
            {
                return (string)GetValue(PageTitleProperty);
            }
            set
            {
                SetValue(PageTitleProperty, value);
            }
        }

        /// <summary>
        /// Dependency Eigenschaft welche den Text des Subtitels zurückgibt (nicht fett, rechts neben Titel)
        /// </summary>
        public string PageSubTitle
        {
            get
            {
                return (string)GetValue(PageSubTitleProperty);
            }
            set
            {
                SetValue(PageSubTitleProperty, value);
            }
        }
        #endregion

        #region Attribs
        private OverlayWizard _parent_wizard = null;
        private string _wizard_page_title = "";

        #region Dependency properties
        public static readonly DependencyProperty PageTitleProperty;
        public static readonly DependencyProperty PageSubTitleProperty;
        #endregion

        #endregion
    }
}
