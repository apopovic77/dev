﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Logicx.WpfUtility.WpfHelpers;

namespace Logicx.DiagramLib.Scales
{
    public abstract class ScaleControlBase : Canvas, INotifyPropertyChanged, IDisposable
    {
        #region Constants
        #endregion

        #region Construction and Initialization
        public ScaleControlBase()
        {
            _visible_area = new Canvas();
            //_visible_area.Background = Brushes.Yellow;
            _visible_area.ClipToBounds = true;

            ScaleCanvas = new Canvas();
            //ScaleCanvas.Background = Brushes.White;
            ScaleCanvas.SizeChanged += new SizeChangedEventHandler(ScaleCanvas_SizeChanged);
            _visible_area.Children.Add(ScaleCanvas);
            Children.Add(_visible_area);
        }

        void ScaleCanvas_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            OnScaleSizeChanged(e);
        }

        #endregion

        #region Operations
        protected void BaseControlSizeChanged(object sender, SizeChangedEventArgs e)
        {
            //Debug.WriteLine("ActualWidth: " + ActualWidth + " " + _visible_area.ActualWidth + " " + ScaleCanvas.ActualWidth);
            if (e.WidthChanged)
            {
                _visible_area.Width = e.NewSize.Width - Padding.Left - Padding.Right;
                _visible_area.Height = ActualHeight - Padding.Top - Padding.Bottom;

                Canvas.SetLeft(_visible_area, Padding.Left);

                if (Orientation == Orientation.Horizontal)
                {
                    ScaleCanvas.Width = _visible_area.Width * Zoomfactor;
                    ScaleCanvas.Height = ActualHeight - Padding.Top - Padding.Bottom;
                }
            }

            if (e.HeightChanged)
            {
                _visible_area.Height = e.NewSize.Height - Padding.Top - Padding.Bottom;
                _visible_area.Width = ActualWidth - Padding.Left - Padding.Right;

                Canvas.SetTop(_visible_area, Padding.Top);

                if (Orientation == Orientation.Vertical)
                {
                    ScaleCanvas.Height = _visible_area.Height * Zoomfactor;
                    ScaleCanvas.Width = _visible_area.Width - Padding.Left - Padding.Right;
                }
            }
        }

        private void ApplyNewPadding()
        {
            double new_width = ActualWidth - Padding.Left - Padding.Right;
            double new_height = ActualHeight - Padding.Top - Padding.Bottom;
            _visible_area.Width = new_width > 0 ? new_width : 0;
            _visible_area.Height = new_height > 0 ? new_height : 0;

            Canvas.SetLeft(_visible_area, Padding.Left);
            Canvas.SetTop(_visible_area, Padding.Top);
        }

        internal double GetVisibleLeftmostPosition()
        {
            return ScrollOffset;
        }

        internal double GetVisibleCenteredPosition()
        {
            if (Orientation == Orientation.Horizontal)
                return ScrollOffset + _visible_area.ActualWidth / 2;
            else
            {
                return ScrollOffset + _visible_area.ActualHeight / 2;
            }
        }

        public abstract double GetScaleStartPixel();
        public abstract double GetScaleEndPixel();

        /// <summary>
        /// Delivers the pixel size for one unit
        /// </summary>
        /// <returns></returns>
        public abstract double GetUnitSize();

        public abstract List<double> GetGridLinePositions();

        public abstract double GenericTransformAxisValueToPixel(object value, bool margin_sensitive);
        public abstract object GenericTransformAxisPixelToValue(double pixel, bool margin_sensitive);



        protected void OnScalingChanged()
        {
            ScaleUnitSize = GetUnitSize();

            if (ScaleingChanged != null)
                ScaleingChanged(this, EventArgs.Empty);

            //SendPropertyChanged("ScaleUnitSize");
        }

        protected void SendPropertyChanged(string property_name)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property_name));
        }

        internal void SendScaleChanged()
        {
            SendPropertyChanged("ScaleUnitSize");
        }

        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(e);
            if (e.Property == VisibilityProperty)
            {
                if (((Visibility)e.NewValue) == Visibility.Hidden)
                {
                    if (Orientation == System.Windows.Controls.Orientation.Horizontal)
                    {
                        MinHeight = 0;
                        Height = 0;
                    }
                    else
                    {
                        MinWidth = 0;
                        Width = 0;
                    }
                }
            }
        }

        protected virtual void InitAndAlign(bool generate_always)
        {

        }

        private void AfterZoomfactorChanged()
        {
            double scale = Zoomfactor / _last_zoom_factor;
            _last_zoom_factor = Zoomfactor;

            if (Orientation == Orientation.Horizontal)
                ScaleCanvas.Width = _visible_area.ActualWidth * Zoomfactor;
            if (Orientation == Orientation.Vertical)
                ScaleCanvas.Height = _visible_area.ActualHeight * Zoomfactor;

            InitAndAlign(true);
        }

        private void AfterScrollOffsetChanged()
        {
            _last_scroll_offset = ScrollOffset;

            if (Orientation == Orientation.Horizontal)
                Canvas.SetLeft(ScaleCanvas, ScrollOffset * (-1));
            else
                Canvas.SetTop(ScaleCanvas, ScrollOffset * (-1));

            InitAndAlign(true);
        }

        protected virtual void OnScaleSizeChanged(SizeChangedEventArgs e)
        {

        }

        #region IDisposable Members

        public void Dispose()
        {
            if (_disposed || _disposing)
                return;

            _disposing = true;

            EventHelper.UnregisterEvents(ScaleingChanged);
            EventHelper.UnregisterEvents(PropertyChanged);

            Dispose(true);
            _disposed = true;
        }

        protected virtual void Dispose(bool disposing)
        {

        }


        #endregion
        #endregion

        #region Events

        public event EventHandler ScaleingChanged;
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        #region Properties
        /// <summary>
        /// Gets a flag if the object has been disposed
        /// </summary>
        public bool Disposed
        {
            get { return _disposed; }
        }
        /// <summary>
        /// Gets a flag if the object is disposing
        /// </summary>
        protected bool Disposing
        {
            get { return _disposing; }
        }

        public Orientation Orientation
        {
            get { return _orientation; }
            set { _orientation = value; }
        }

        protected Thickness Padding
        {
            get { return _Padding; }
            set
            {
                if (value != _Padding)
                {
                    _Padding = value;
                    ApplyNewPadding();
                }
            }
        }

        #endregion

        #region Dependency Properties
        public Style ScaleTextStyle
        {
            get { return (Style)GetValue(ScaleTextStyleProperty); }
            set { SetValue(ScaleTextStyleProperty, value); }
        }

        //Using a DependencyProperty as the backing store for ScaleTextStyle.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ScaleTextStyleProperty =
            DependencyProperty.Register("ScaleTextStyle", typeof(Style), typeof(ScaleControlBase), new UIPropertyMetadata(null));

        public Style ScaleInfoTextStyle
        {
            get { return (Style)GetValue(ScaleInfoTextStyleProperty); }
            set { SetValue(ScaleInfoTextStyleProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ScaleInfoTextStyle.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ScaleInfoTextStyleProperty =
            DependencyProperty.Register("ScaleInfoTextStyle", typeof(Style), typeof(ScaleControlBase), new UIPropertyMetadata(null));


        public Brush LineBrush
        {
            get { return (Brush)GetValue(LineBrushProperty); }
            set { SetValue(LineBrushProperty, value); }
        }

        // Using a DependencyProperty as the backing store for LineBrush.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty LineBrushProperty =
            DependencyProperty.Register("LineBrush", typeof(Brush), typeof(ScaleControlBase), new UIPropertyMetadata(Brushes.Black));


        public double ScaleFactor
        {
            get { return (double)GetValue(ScaleFactorProperty); }
            set { SetValue(ScaleFactorProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ScaleFactor.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ScaleFactorProperty =
            DependencyProperty.Register("ScaleFactor", typeof(double), typeof(ScaleControlBase), new UIPropertyMetadata(1d));


        public double ScaleUnitSize
        {
            get { return (double)GetValue(ScaleUnitSizeProperty); }
            set { SetValue(ScaleUnitSizeProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ScaleFactor.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ScaleUnitSizeProperty =
            DependencyProperty.Register("ScaleUnitSize", typeof(double), typeof(ScaleControlBase), new UIPropertyMetadata(1d));

        // change to ScaleType enum
        public int ScaleType
        {
            get { return (int)GetValue(ScaleTypeProperty); }
            set { SetValue(ScaleTypeProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ScaleType.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ScaleTypeProperty =
            DependencyProperty.Register("ScaleType", typeof(int), typeof(ScaleControlBase), new UIPropertyMetadata(0));




        public double Zoomfactor
        {
            get { return (double)GetValue(ZoomfactorProperty); }
            set { SetValue(ZoomfactorProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Zoomfactor.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ZoomfactorProperty =
            DependencyProperty.Register("Zoomfactor", typeof(double), typeof(ScaleControlBase), new UIPropertyMetadata(1d, new PropertyChangedCallback(OnZoomfactorChangedChanged)));

        public double ScrollOffset
        {
            get { return (double)GetValue(ScrollOffsetProperty); }
            set { SetValue(ScrollOffsetProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ScrollOffset.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ScrollOffsetProperty =
            DependencyProperty.Register("ScrollOffset", typeof(double), typeof(ScaleControlBase), new UIPropertyMetadata(0d, new PropertyChangedCallback(OnScrollOffsetChanged)));
        #endregion

        #region Event handler
        static void OnZoomfactorChangedChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is ScaleControlBase)
            {
                ScaleControlBase scale_control = d as ScaleControlBase;
                scale_control.AfterZoomfactorChanged();
            }
        }

        static void OnScrollOffsetChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is ScaleControlBase)
            {
                ScaleControlBase scale_control = d as ScaleControlBase;
                //diagram.SendPropertyChanged("ScrollOffsetY");
                scale_control.AfterScrollOffsetChanged();
            }
        }
        #endregion

        #region Attributes
        private bool _disposed = false;
        private bool _disposing = false;

        private Orientation _orientation = Orientation.Horizontal;

        private Canvas _visible_area;
        protected Canvas ScaleCanvas;

        private double _last_zoom_factor = 1.0;
        private double _last_scroll_offset = 0.0;

        private Thickness _Padding;

        #endregion
    }
}
