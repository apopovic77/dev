﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Logicx.DiagramLib.ViewConfig
{
    public enum ScaleAlignment
    {
        Left,
        Right,
        Top,
        Bottom
    }

    public enum GridLineVisibility
    {
        None,
        All,
        Horizontal,
        Vertical
    }

    public enum ScaleType
    {
        Day,
        Month,
        Quartal,
        Year,
        Fract,
        Tenner,
        Hundred,
        Custom
    }

    public enum ObjectViewMode
    {
        Invisible,
        Culled,
        Detail1,
        Detail2,
        Detail3,
        Detail4,
        Detail5
    }

    [Flags]
    public enum CursorMarkerVisibility
    {
        None = 0x00,
        VerticalLine = 0x01,
        HorizontalLine = 0x02,
        ShowCoordinatesOnCursor = 0x70
    }
}
