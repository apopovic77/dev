﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;

namespace Logicx.WpfUtility.CustomControls.Validation
{
    /// <summary>
    /// Validation error extended collection
    /// </summary>
    public class ValidationErrorExCollection : ObservableCollection<ValidationErrorEx>
    {
        #region Construction and initialisation
        /// <summary>
        /// Constructor of the class
        /// </summary>
        static ValidationErrorExCollection()
        {
            Empty = new ReadOnlyObservableCollection<ValidationErrorEx>(new ValidationErrorExCollection());
        }
        #endregion

        #region Operations
        /// <summary>
        /// Finds an error for a binding object
        /// </summary>
        /// <param name="binding"></param>
        /// <returns></returns>
        private int FindErrorForBinding(object binding)
        {
            for (int i = 0; i < base.Count; i++)
            {
                if (base[i].BindingInError == binding && base[i].ErrorFromDatabindingContext)
                {
                    return i;
                }
            }
            return -1;
        }

        /// <summary>
        /// Finds an error for a control
        /// </summary>
        /// <param name="control"></param>
        /// <returns></returns>
        private int FindErrorForControl(DependencyObject control)
        {
            for (int i = 0; i < base.Count; i++)
            {
                if (base[i].ControlCausingError == control && !base[i].ErrorFromDatabindingContext)
                {
                    return i;
                }
            }
            return -1;
        }

        /// <summary>
        /// Inserts an item
        /// </summary>
        /// <param name="index"></param>
        /// <param name="item"></param>
        protected override void InsertItem(int index, ValidationErrorEx item)
        {
            if (item.ErrorFromDatabindingContext && this.FindErrorForBinding(item.BindingInError) > -1)
            {
                // no duplicate databinding items allowed
                return;
            }

            base.InsertItem(index, item);
        }



        /// <summary>
        /// Gets the readonly error collection for a dependency object
        /// </summary>
        /// <param name="d"></param>
        /// <returns></returns>
        internal static ReadOnlyObservableCollection<ValidationErrorEx> GetReadOnlyErrors(DependencyObject d)
        {
            ValidationErrorExCollection errorsInternal = ErrorProvider.GetErrorsInternal(d);
            if (errorsInternal == null)
            {
                return Empty;
            }
            if (errorsInternal._readonly_wrapper == null)
            {
                errorsInternal._readonly_wrapper = new ReadOnlyObservableCollection<ValidationErrorEx>(errorsInternal);
            }
            return errorsInternal._readonly_wrapper;
        }

        protected override void ClearItems()
        {
            foreach (ValidationErrorEx err in Items)
                err.ErrorProvider = null;

            base.ClearItems();
        }

        protected override void RemoveItem(int index)
        {
            if (index >= 0 && index < Items.Count)
                Items[index].ErrorProvider = null;

            base.RemoveItem(index);
        }
        #endregion

        #region Properties
        /// <summary>
        /// Empty collection
        /// </summary>
        public static readonly ReadOnlyObservableCollection<ValidationErrorEx> Empty;
        #endregion

        #region Attribs
        private ReadOnlyObservableCollection<ValidationErrorEx> _readonly_wrapper;
        #endregion
    }
}
