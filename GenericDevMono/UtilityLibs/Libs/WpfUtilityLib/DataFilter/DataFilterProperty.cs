﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Logicx.WpfUtility.DataFilter
{
    [Serializable]
    public class DataFilterProperty<T> : INotifyPropertyChanged, IComparable
    {
        public DataFilterProperty()
        {
        }
        public DataFilterProperty(T value, bool selected )
        {
            _value = value;
            _selected = selected;
        }

        protected void Notify(string property_name)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property_name));
        }


        public T Value
        {
            get
            {
                return _value;
                
            }
            set
            {
                _value = value;
                //Notify("Value");
            }
        }

        public bool Selected
        {
            set
            {

                if (_selected != value)
                {
                    _selected = value;
                    Notify("Selected");
                }
                else
                {
                    _selected = value;
                }
            }
            get
            {
                return _selected;
            }
        }

        #region IComparable Members

        public int CompareTo(object obj)
        {
            if (Value is IComparable && obj is DataFilterProperty<T>)
            {
                return ((IComparable) Value).CompareTo(((DataFilterProperty<T>)obj).Value);
            }

            return 0;
        }

        #endregion

        protected T _value;
        protected bool _selected;
        public event PropertyChangedEventHandler PropertyChanged;

        
    }
}
