﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;
using Logicx.DiagramLib.Controls;

namespace Logicx.DiagramLib.Resources.Converter
{
    [ValueConversion(typeof(DiagramMode), typeof(Brush))]
    public class DiagramModeToHeaderBackgroundConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is DiagramMode)
            {
                if (((DiagramMode)value) == DiagramMode.MultiSelect)
                    return Brushes.LightGoldenrodYellow;
            }

            return Brushes.White;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is Brush && ((Brush)value) == Brushes.LightGoldenrodYellow)
                return DiagramMode.MultiSelect;

            return DiagramMode.Normal;
        }
    }
}
