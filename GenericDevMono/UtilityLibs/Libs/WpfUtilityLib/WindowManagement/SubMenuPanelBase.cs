﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Media3D;
using System.Windows.Shapes;
using System.Windows.Threading;
using Logicx.Geo.Geometries;
using Logicx.WpfUtility.Converters;
using Logicx.WpfUtility.CustomControls.Combobox;
using Logicx.WpfUtility.CustomControls.OverlayManager;
using Logicx.WpfUtility.CustomControls.TouchButton;
using Logicx.WpfUtility.Extensions;
using Logicx.WpfUtility.WindowManagement.Adorners;
using Logicx.WpfUtility.WpfHelpers;
using Logicx.WpfUtility.ZoomingPanning;
using MathLib;
using Microsoft.Windows.Controls;
using Microsoft.Windows.Controls.Primitives;
using PixelLab.Wpf;

namespace Logicx.WpfUtility.WindowManagement
{
    public class CreateSubMenuBreadCrumbItemEventArgs : EventArgs
    {
        public CreateSubMenuBreadCrumbItemEventArgs(ISubMenuPanelBase parent_panel, string text, ISubMenuPanelMenuItem menu_item)
        {
            ParentPanel = parent_panel;
            Text = text;
            MenuItem = menu_item;
        }

        public ISubMenuPanelBase ParentPanel
        {
            get;
            private set;
        }

        public string Text
        {
            get;
            private set;
        }

        public ISubMenuPanelMenuItem MenuItem
        {
            get;
            private set;
        }

        public ISubMenuPanelBreadCrumbItem CreatedItem
        {
            get;
            set;
        }
    }

    public interface ISubMenuPanelBase
    {
        UserControl ContentControl { get; }
        Grid base_grid { get; }
        TouchToggleButton OrderLayout1Button { get; }
        TouchToggleButton OrderLayout2Button { get; }
        TouchToggleButton OrderLayout3Button { get; }
        ListBox LbBreadCrumb { get; }
        TouchButton ArrowleftButton { get; }
        TouchButton ArrowrightButton { get; }
        TouchButton ArrowupButton { get; }
        TouchButton ArrowdownButton { get; }
        TouchToggleButton PinButton { get; }
        TouchButton NewelemButton { get; }
        TouchButton CustomButton1 { get; }
        TouchButton CustomButton { get; }
        TouchButton SearchButton { get; }
        ItemsControl AnimatingTilePanelItemsControl { get; }
        Grid SubmenuBaseGrid { get; }
        Canvas SubmenuBaseCanvas { get; }
        StackPanel SubHeaderStack { get; }
        DataGrid SubmenuItemsDatagrid { get; }
        Border SubmenuBorder { get; }
        AcCbx SearchCbx { get; }
        Rectangle NewelemRect { get; }
        Rectangle SearchRect { get; }
        Rectangle CustomRect1 { get; }
        Rectangle CustomRect { get; }
        string CustomNewElementButtonText { get; set; }
        string CustomButtonText { get; set; }
        string CustomButtonText1 { get; set; }
        string SearchButtonTooltip { get; set; }
        int StarColumnAttributeIndex { get; set; }
        double StarColumnMinWidth { get; set; }
        Brush SubMenuIconBgBrush { get; set; }
        Brush PanelBackground { get; set; }
        Brush ButtonHighlightBackground { get; set; }
        int ItemCount { get; set; }
        bool WithShowItemCount { get; set; }
        bool ShowInfoIndicator { get; set; }
        bool HighlightSearchButton { get; set; }
        ISubMenuPanelBreadCrumbItem CurrentBreadCrumbItem { get; set; }
        Window Window { get; }
        string TaskbarItemName { get; }
        bool WithSearchField { get; set; }
        bool WithSearchButton { get; set; }
        bool WithNewElementField { set; }
        bool WithCustomButton { set; }
        bool WithCustomButton1 { set; }
        bool WithNavigationButtons { set; }
        bool WithGridPanelLayout { set; }
        bool WithKachelPanelLayout { set; }
        bool WithLinePanelLayout { set; }
        bool WithAutoHideMenu { get; set; }
        bool AllowResize { get; set; }
        double AutoHideTimerInterval { get; set; }
        Grid BaseGrid { get; }
        AnimatingTilePanel AnimatingTilePanel { get; }

        /// <summary>
        /// das submenu panel ist verbunden mit einem togglebutton
        /// dieser sollte in der taskbar aufscheinen
        /// </summary>
        ToggleButton ToggleButtonReference { get; set; }

        Thickness ItemMargin { set; get; }
        Size BigItemSize { get; set; }
        bool ShowActionsByLeftMouse { set; get; }
        Size SmallItemSize { get; set; }
        double PanelHeight { get; }

        bool IsAnimationActive { get; }
        object Content { get; set; }
        bool HasContent { get; }
        DataTemplate ContentTemplate { get; set; }
        DataTemplateSelector ContentTemplateSelector { get; set; }
        string ContentStringFormat { get; set; }
        Brush BorderBrush { get; set; }
        Thickness BorderThickness { get; set; }
        Brush Background { get; set; }
        Brush Foreground { get; set; }
        FontFamily FontFamily { get; set; }
        double FontSize { get; set; }
        FontStretch FontStretch { get; set; }
        FontStyle FontStyle { get; set; }
        FontWeight FontWeight { get; set; }
        HorizontalAlignment HorizontalContentAlignment { get; set; }
        VerticalAlignment VerticalContentAlignment { get; set; }
        int TabIndex { get; set; }
        bool IsTabStop { get; set; }
        Thickness Padding { get; set; }
        ControlTemplate Template { get; set; }
        Style Style { get; set; }
        bool OverridesDefaultStyle { get; set; }
        TriggerCollection Triggers { get; }
        DependencyObject TemplatedParent { get; }
        ResourceDictionary Resources { get; set; }
        object DataContext { get; set; }
        BindingGroup BindingGroup { get; set; }
        XmlLanguage Language { get; set; }
        string Name { get; set; }
        object Tag { get; set; }
        InputScope InputScope { get; set; }
        double ActualWidth { get; }
        double ActualHeight { get; }
        Transform LayoutTransform { get; set; }
        double Width { get; set; }
        double MinWidth { get; set; }
        double MaxWidth { get; set; }
        double Height { get; set; }
        double MinHeight { get; set; }
        double MaxHeight { get; set; }
        FlowDirection FlowDirection { get; set; }
        Thickness Margin { get; set; }
        HorizontalAlignment HorizontalAlignment { get; set; }
        VerticalAlignment VerticalAlignment { get; set; }
        Style FocusVisualStyle { get; set; }
        Cursor Cursor { get; set; }
        bool ForceCursor { get; set; }
        bool IsInitialized { get; }
        bool IsLoaded { get; }
        object ToolTip { get; set; }
        ContextMenu ContextMenu { get; set; }
        DependencyObject Parent { get; }
        bool HasAnimatedProperties { get; }
        InputBindingCollection InputBindings { get; }
        CommandBindingCollection CommandBindings { get; }
        bool AllowDrop { get; set; }
        Size DesiredSize { get; }
        bool IsMeasureValid { get; }
        bool IsArrangeValid { get; }
        Size RenderSize { get; set; }
        Transform RenderTransform { get; set; }
        Point RenderTransformOrigin { get; set; }
        bool IsMouseDirectlyOver { get; }
        bool IsMouseOver { get; }
        bool IsStylusOver { get; }
        bool IsKeyboardFocusWithin { get; }
        bool IsMouseCaptured { get; }
        bool IsMouseCaptureWithin { get; }
        bool IsStylusDirectlyOver { get; }
        bool IsStylusCaptured { get; }
        bool IsStylusCaptureWithin { get; }
        bool IsKeyboardFocused { get; }
        bool IsInputMethodEnabled { get; }
        double Opacity { get; set; }
        Brush OpacityMask { get; set; }
        BitmapEffect BitmapEffect { get; set; }
        Effect Effect { get; set; }
        BitmapEffectInput BitmapEffectInput { get; set; }
        string Uid { get; set; }
        Visibility Visibility { get; set; }
        bool ClipToBounds { get; set; }
        Geometry Clip { get; set; }
        bool SnapsToDevicePixels { get; set; }
        bool IsFocused { get; }
        bool IsEnabled { get; set; }
        bool IsHitTestVisible { get; set; }
        bool IsVisible { get; }
        bool Focusable { get; set; }
        int PersistId { get; }
        DependencyObjectType DependencyObjectType { get; }
        bool IsSealed { get; }
        Dispatcher Dispatcher { get; }

        bool WithCustomBreadCrumbItemGeneration { get; set; }
        event EventHandler<CreateSubMenuBreadCrumbItemEventArgs> RequestBreadCrumbItemCreation;

        void StartFadeIn();
        void StartFadeOut();
        void RemoveItem(FrameworkElement item);
        void RemoveAllItemsOfCurrentBreadcrumbLayer();
        void RemoveAllItems();
        void ResetHoverElementState();
        FrameworkElement GetItemByElement(FrameworkElement elem);
        ///SubMenuPanelMenuItem FindMenuItemByKey(string key);
        void SetSearchFilter(string text);
        string GetSearchFilter();
        void ApplySort();
        void GoLeft();
        void GoRight();
        void NavigateToSubItems(ISubMenuPanelMenuItem menu_item);
        ISubMenuPanelBreadCrumbItem CreateBreadCrumbItem(ISubMenuPanelBase parent_panel, string text, ISubMenuPanelMenuItem menu_item);

        event EventHandler FadeOutCompleted;
        event EventHandler FadeInCompleted;
        event EventHandler<SubMenuPanelMenuItem.MenuItemActionEventArgs> Action;
        event EventHandler NewElemClick;
        event EventHandler NavigationDepthChanged;
        event EventHandler CustomButtonClick;
        event EventHandler CustomButton1Click;
        event EventHandler SearchButtonClick;
        event EventHandler<SubMenuPanelMenuItem.MenuItemManagementEventArgs> ItemRemoved;
        event EventHandler<SubMenuPanelMenuItem.MenuItemManagementEventArgs> ItemAdded;
        bool ShouldSerializeContent();
        string ToString();
        event MouseButtonEventHandler PreviewMouseDoubleClick;
        event MouseButtonEventHandler MouseDoubleClick;
        bool ShouldSerializeStyle();
        bool ApplyTemplate();
        void OnApplyTemplate();
        void BeginStoryboard(Storyboard storyboard);
        void BeginStoryboard(Storyboard storyboard, HandoffBehavior handoffBehavior);
        void BeginStoryboard(Storyboard storyboard, HandoffBehavior handoffBehavior, bool isControllable);
        bool ShouldSerializeTriggers();
        bool ShouldSerializeResources();
        object FindResource(object resourceKey);
        object TryFindResource(object resourceKey);
        void SetResourceReference(DependencyProperty dp, object name);
        BindingExpression GetBindingExpression(DependencyProperty dp);
        BindingExpressionBase SetBinding(DependencyProperty dp, BindingBase binding);
        BindingExpression SetBinding(DependencyProperty dp, string path);
        void BringIntoView();
        void BringIntoView(Rect targetRectangle);
        bool MoveFocus(TraversalRequest request);
        DependencyObject PredictFocus(FocusNavigationDirection direction);
        void BeginInit();
        void EndInit();
        void RegisterName(string name, object scopedElement);
        void UnregisterName(string name);
        object FindName(string name);
        event EventHandler<DataTransferEventArgs> TargetUpdated;
        event EventHandler<DataTransferEventArgs> SourceUpdated;
        event DependencyPropertyChangedEventHandler DataContextChanged;
        event RequestBringIntoViewEventHandler RequestBringIntoView;
        event SizeChangedEventHandler SizeChanged;
        event EventHandler Initialized;
        event RoutedEventHandler Loaded;
        event RoutedEventHandler Unloaded;
        event ToolTipEventHandler ToolTipOpening;
        event ToolTipEventHandler ToolTipClosing;
        event ContextMenuEventHandler ContextMenuOpening;
        event ContextMenuEventHandler ContextMenuClosing;
        void ApplyAnimationClock(DependencyProperty dp, AnimationClock clock);
        void ApplyAnimationClock(DependencyProperty dp, AnimationClock clock, HandoffBehavior handoffBehavior);
        void BeginAnimation(DependencyProperty dp, AnimationTimeline animation);
        void BeginAnimation(DependencyProperty dp, AnimationTimeline animation, HandoffBehavior handoffBehavior);
        object GetAnimationBaseValue(DependencyProperty dp);
        bool ShouldSerializeInputBindings();
        bool ShouldSerializeCommandBindings();
        void RaiseEvent(RoutedEventArgs e);
        void AddHandler(RoutedEvent routedEvent, Delegate handler);
        void AddHandler(RoutedEvent routedEvent, Delegate handler, bool handledEventsToo);
        void RemoveHandler(RoutedEvent routedEvent, Delegate handler);
        void AddToEventRoute(EventRoute route, RoutedEventArgs e);
        void InvalidateMeasure();
        void InvalidateArrange();
        void InvalidateVisual();
        void Measure(Size availableSize);
        void Arrange(Rect finalRect);
        void UpdateLayout();
        Point TranslatePoint(Point point, UIElement relativeTo);
        IInputElement InputHitTest(Point point);
        bool CaptureMouse();
        void ReleaseMouseCapture();
        bool CaptureStylus();
        void ReleaseStylusCapture();
        bool Focus();
        event MouseButtonEventHandler PreviewMouseDown;
        event MouseButtonEventHandler MouseDown;
        event MouseButtonEventHandler PreviewMouseUp;
        event MouseButtonEventHandler MouseUp;
        event MouseButtonEventHandler PreviewMouseLeftButtonDown;
        event MouseButtonEventHandler MouseLeftButtonDown;
        event MouseButtonEventHandler PreviewMouseLeftButtonUp;
        event MouseButtonEventHandler MouseLeftButtonUp;
        event MouseButtonEventHandler PreviewMouseRightButtonDown;
        event MouseButtonEventHandler MouseRightButtonDown;
        event MouseButtonEventHandler PreviewMouseRightButtonUp;
        event MouseButtonEventHandler MouseRightButtonUp;
        event MouseEventHandler PreviewMouseMove;
        event MouseEventHandler MouseMove;
        event MouseWheelEventHandler PreviewMouseWheel;
        event MouseWheelEventHandler MouseWheel;
        event MouseEventHandler MouseEnter;
        event MouseEventHandler MouseLeave;
        event MouseEventHandler GotMouseCapture;
        event MouseEventHandler LostMouseCapture;
        event QueryCursorEventHandler QueryCursor;
        event StylusDownEventHandler PreviewStylusDown;
        event StylusDownEventHandler StylusDown;
        event StylusEventHandler PreviewStylusUp;
        event StylusEventHandler StylusUp;
        event StylusEventHandler PreviewStylusMove;
        event StylusEventHandler StylusMove;
        event StylusEventHandler PreviewStylusInAirMove;
        event StylusEventHandler StylusInAirMove;
        event StylusEventHandler StylusEnter;
        event StylusEventHandler StylusLeave;
        event StylusEventHandler PreviewStylusInRange;
        event StylusEventHandler StylusInRange;
        event StylusEventHandler PreviewStylusOutOfRange;
        event StylusEventHandler StylusOutOfRange;
        event StylusSystemGestureEventHandler PreviewStylusSystemGesture;
        event StylusSystemGestureEventHandler StylusSystemGesture;
        event StylusEventHandler GotStylusCapture;
        event StylusEventHandler LostStylusCapture;
        event StylusButtonEventHandler StylusButtonDown;
        event StylusButtonEventHandler StylusButtonUp;
        event StylusButtonEventHandler PreviewStylusButtonDown;
        event StylusButtonEventHandler PreviewStylusButtonUp;
        event KeyEventHandler PreviewKeyDown;
        event KeyEventHandler KeyDown;
        event KeyEventHandler PreviewKeyUp;
        event KeyEventHandler KeyUp;
        event KeyboardFocusChangedEventHandler PreviewGotKeyboardFocus;
        event KeyboardFocusChangedEventHandler GotKeyboardFocus;
        event KeyboardFocusChangedEventHandler PreviewLostKeyboardFocus;
        event KeyboardFocusChangedEventHandler LostKeyboardFocus;
        event TextCompositionEventHandler PreviewTextInput;
        event TextCompositionEventHandler TextInput;
        event QueryContinueDragEventHandler PreviewQueryContinueDrag;
        event QueryContinueDragEventHandler QueryContinueDrag;
        event GiveFeedbackEventHandler PreviewGiveFeedback;
        event GiveFeedbackEventHandler GiveFeedback;
        event DragEventHandler PreviewDragEnter;
        event DragEventHandler DragEnter;
        event DragEventHandler PreviewDragOver;
        event DragEventHandler DragOver;
        event DragEventHandler PreviewDragLeave;
        event DragEventHandler DragLeave;
        event DragEventHandler PreviewDrop;
        event DragEventHandler Drop;
        event DependencyPropertyChangedEventHandler IsMouseDirectlyOverChanged;
        event DependencyPropertyChangedEventHandler IsKeyboardFocusWithinChanged;
        event DependencyPropertyChangedEventHandler IsMouseCapturedChanged;
        event DependencyPropertyChangedEventHandler IsMouseCaptureWithinChanged;
        event DependencyPropertyChangedEventHandler IsStylusDirectlyOverChanged;
        event DependencyPropertyChangedEventHandler IsStylusCapturedChanged;
        event DependencyPropertyChangedEventHandler IsStylusCaptureWithinChanged;
        event DependencyPropertyChangedEventHandler IsKeyboardFocusedChanged;
        event EventHandler LayoutUpdated;
        event RoutedEventHandler GotFocus;
        event RoutedEventHandler LostFocus;
        event DependencyPropertyChangedEventHandler IsEnabledChanged;
        event DependencyPropertyChangedEventHandler IsHitTestVisibleChanged;
        event DependencyPropertyChangedEventHandler IsVisibleChanged;
        event DependencyPropertyChangedEventHandler FocusableChanged;
        bool IsAncestorOf(DependencyObject descendant);
        bool IsDescendantOf(DependencyObject ancestor);
        DependencyObject FindCommonVisualAncestor(DependencyObject otherVisual);
        GeneralTransform TransformToAncestor(Visual ancestor);
        GeneralTransform2DTo3D TransformToAncestor(Visual3D ancestor);
        GeneralTransform TransformToDescendant(Visual descendant);
        GeneralTransform TransformToVisual(Visual visual);
        Point PointToScreen(Point point);
        Point PointFromScreen(Point point);
        bool Equals(object obj);
        int GetHashCode();
        object GetValue(DependencyProperty dp);
        void SetValue(DependencyProperty dp, object value);
        void SetValue(DependencyPropertyKey key, object value);
        void ClearValue(DependencyProperty dp);
        void ClearValue(DependencyPropertyKey key);
        void CoerceValue(DependencyProperty dp);
        void InvalidateProperty(DependencyProperty dp);
        object ReadLocalValue(DependencyProperty dp);
        LocalValueEnumerator GetLocalValueEnumerator();
        bool CheckAccess();
        void VerifyAccess();
    }

    public class SubMenuPanelBase<B, S, T, U, O, SU> : UserControl, ISubMenuPanelBase
        where S : SubMenuPanelBase<B, S, T, U, O, SU>, new()
        where B : BaseFrameBase<B, S, T, U, O, SU>
        where T : WindowTaskBarBase<B, S, T, U, O, SU>, new()
        where U : WindowTitleBarBase<B, S, T, U, O, SU>, new()
        where O : OverlayManagerBase<B, S, T, U, O, SU>, new()
        where SU : SubMenuPanelMenuItem
    {
        #region nested types
        public enum LayoutMode
        {
            Kachel,
            Line,
            Grid
        }

        #endregion

        #region Construction & Init

        public SubMenuPanelBase()
        {
            //_root_bread_crumb_item = new SubMenuPanelBreadCrumbItem(this, string.Empty);
            //_root_bread_crumb_item.Click += new RoutedEventHandler(BreadCrumbButton_Click);

            InitializeComponent();
            WithCustomButton1 = false;
            WithCustomButton = false;
            
            _root_items_readonly = new ReadOnlyObservableCollection<SU>(_root_items);
            _items.CollectionChanged += new System.Collections.Specialized.NotifyCollectionChangedEventHandler(_items_CollectionChanged);
            //zum verzögerten registrieren der adorner und dem animated panel :-(
            Loaded += new RoutedEventHandler(Menu_Loaded);
            this.Unloaded += new RoutedEventHandler(Menu_Unloaded);

            KeyDown += new KeyEventHandler(SubMenuPanel_KeyDown);
            KeyUp += new KeyEventHandler(SubMenuPanel_KeyUp);


            BigItemSize = new Size(200, 100);
            SmallItemSize = new Size(100, 50);

            _active_layout_mode = LayoutMode.Line;
            OrderLayout1Button.IsChecked = false;
            OrderLayout2Button.IsChecked = true;
            OrderLayout3Button.IsChecked = false;

            InitDefaultValues();
            
        }

        /// <summary>
        /// Hier wird der XAML Code geladen. Das wurde so gemacht, das man in einer anderes Lib von dieser Klasse ableiten kann. Bei "partial" Klassen werden die Resourcen über mehrere Libs nicht gefunden.
        /// 
        /// <remarks> 
        /// * Der zu ladende Xaml String darf keine Events haben. 
        /// * Bei XAML Namespaces muss assembly angegeben werden.
        /// * class Attribut darf nicht vorkommen.
        /// * Erzeugte Elemente muss man von außerhalb mit FindName suchen
        /// 
        /// </remarks>
        /// </summary>
        private void InitializeComponent()
        {
            string name = Assembly.GetAssembly(typeof(DoubleDevideByTwo)).GetName().Name;

            string xaml_string =
                @"<UserControl
    xmlns=""http://schemas.microsoft.com/winfx/2006/xaml/presentation""
    xmlns:x=""http://schemas.microsoft.com/winfx/2006/xaml"" 
    xmlns:Combobox=""clr-namespace:Logicx.WpfUtility.CustomControls.Combobox;assembly=WpfUtilityLib"" 
    xmlns:Wpf=""clr-namespace:PixelLab.Wpf;assembly=PixelLab.Wpf"" 
    xmlns:WindowManagement=""clr-namespace:Logicx.WpfUtility.WindowManagement;assembly=WpfUtilityLib"" 
    xmlns:Controls=""clr-namespace:Microsoft.Windows.Controls;assembly=WPFToolkit"" 
    xmlns:Converters=""clr-namespace:Logicx.WpfUtility.Converters;assembly=WpfUtilityLib"" 
    xmlns:TouchButton=""clr-namespace:Logicx.WpfUtility.CustomControls.TouchButton;assembly=WpfUtilityLib"">
    
    <UserControl.Resources>

        <DropShadowEffect x:Key=""drop_shadow_effect"" ShadowDepth=""3.77953"" Opacity=""0.4"" Color=""#FF000000"" Direction=""315"" />
        <SolidColorBrush x:Key=""menu_background_brush"" Color=""#b5ffffff""></SolidColorBrush>
        <SolidColorBrush x:Key=""menu_border_brush"" Color=""DarkGray""></SolidColorBrush>
        <SolidColorBrush x:Key=""default_hoover_bg_brush"" Color=""#4E00D7FF""></SolidColorBrush>

        <WindowManagement:SubmenuPanelBreadCrumbButtonVisibilityConverter x:Key=""SubmenuPanelBreadCrumbButtonVisibilityConverter""/>
        <Converters:InverseBooleanToVisibilityCollapsedConverter x:Key=""InverseBooleanToVisibilityCollapsedConverter""/>

        <Style x:Key=""action_menu_style"" TargetType=""{x:Type Menu}"">
            <Setter Property=""OverridesDefaultStyle"" Value=""True""/>
            <Setter Property=""SnapsToDevicePixels"" Value=""True""/>   
            <Setter Property=""HorizontalAlignment"" Value=""Right""/>
            <Setter Property=""Template"">
                <Setter.Value>
                    <ControlTemplate TargetType=""{x:Type Menu}"">
                        <Grid MaxWidth=""{Binding RelativeSource={RelativeSource FindAncestor, AncestorType={x:Type Menu}},Path=MaxWidth}"" MaxHeight=""{Binding RelativeSource={RelativeSource FindAncestor, AncestorType={x:Type Menu}},Path=MaxHeight}"">
                            <Border VerticalAlignment=""Top"" HorizontalAlignment=""Right"" Width=""Auto"" Background=""{StaticResource menu_background_brush}""  CornerRadius=""4"" Padding=""1"" BorderBrush=""{StaticResource menu_border_brush}"" BorderThickness=""1"">
                                <WrapPanel KeyboardNavigation.DirectionalNavigation=""Contained"" HorizontalAlignment=""Right"" VerticalAlignment=""Top"" ClipToBounds=""True"" Orientation=""Vertical"" IsItemsHost=""True""/>
                            </Border>
                        </Grid>
                    </ControlTemplate>
                </Setter.Value>
            </Setter>
        </Style>

        <ControlTemplate x:Key=""action_menuitem_style"" TargetType=""{x:Type MenuItem}"">
            <Border Name=""Border"" BorderThickness=""0"" Padding=""2"" Margin=""0"">
                <Grid>
                    <Grid.ColumnDefinitions>
                        <ColumnDefinition Width=""Auto""/>
                        <ColumnDefinition Width=""*""/>
                    </Grid.ColumnDefinitions>
                    <ContentPresenter Grid.Column=""0"" Name=""content_presenter_icon"" VerticalAlignment=""Center"" HorizontalAlignment=""Center"" ContentSource=""Icon"" RecognizesAccessKey=""True"" />
                    <ContentPresenter Grid.Column=""1"" Name=""content_presenter"" VerticalAlignment=""Center"" HorizontalAlignment=""Center"" ContentSource=""Header"" RecognizesAccessKey=""True"" />                    
                </Grid>
            </Border>    
            <ControlTemplate.Triggers>
                <Trigger Property=""UIElement.IsFocused"" Value=""True"">
                    <Setter Property=""Background"" Value=""{Binding RelativeSource={RelativeSource AncestorType={x:Type UserControl}, AncestorLevel=2},Path=BaseFrame.AdornerActiveRenderBrush}"" TargetName=""Border""></Setter>
                </Trigger>
                <Trigger Property=""UIElement.IsFocused"" Value=""False"">
                    <Setter Property=""Background"" Value=""Transparent""  TargetName=""Border""></Setter>
                </Trigger>
                <Trigger Property=""IsEnabled"" Value=""False"">
                    <Setter Property=""Opacity"" TargetName=""content_presenter""  Value=""0.5""></Setter>
                    <Setter Property=""Opacity"" TargetName=""content_presenter_icon""  Value=""0.5""></Setter>
                </Trigger>
                <Trigger Property=""IsEnabled"" Value=""True"">
                    <Setter Property=""Opacity"" TargetName=""content_presenter"" Value=""1""></Setter>
                    <Setter Property=""Opacity"" TargetName=""content_presenter_icon"" Value=""1""></Setter>
                </Trigger>
                <Trigger Property=""UIElement.IsMouseOver"" Value=""True"">
                    <Setter Property=""Background"" Value=""{Binding RelativeSource={RelativeSource AncestorType={x:Type UserControl}, AncestorLevel=2},Path=BaseFrame.AdornerActiveRenderBrush}"" TargetName=""Border""></Setter>
                    <Setter Property=""Cursor"" Value=""Hand""/>
                </Trigger>
            </ControlTemplate.Triggers>
        </ControlTemplate>

        <Style TargetType=""Button"" x:Key=""NavigationButton"">
            <Setter Property=""Control.Template"">
                <Setter.Value>
                    <ControlTemplate TargetType=""Button"">
                        <Grid Name=""grid"">
                            <ContentPresenter HorizontalAlignment=""Center"" VerticalAlignment=""Center"" ContentTemplate=""{TemplateBinding ContentControl.ContentTemplate}"" Content=""{TemplateBinding ContentControl.Content}"" />
                        </Grid>
                        <ControlTemplate.Triggers>
                            <Trigger Property=""IsMouseOver"" Value=""True"">
                                <Setter Property=""Cursor"" Value=""Hand""></Setter>
                            </Trigger>                           
                        </ControlTemplate.Triggers>
                    </ControlTemplate>
                </Setter.Value>
            </Setter>
            <Setter Property=""FocusVisualStyle""  Value=""{x:Null}""/>
        </Style>

        <Style TargetType=""ToggleButton"" x:Key=""NavigationToggleButton"">
            <Setter Property=""Control.Template"">
                <Setter.Value>
                    <ControlTemplate TargetType=""ToggleButton"">
                        <Grid Name=""grid"">
                            <ContentPresenter HorizontalAlignment=""Center"" VerticalAlignment=""Center"" ContentTemplate=""{TemplateBinding ContentControl.ContentTemplate}"" Content=""{TemplateBinding ContentControl.Content}"" />
                        </Grid>
                        <ControlTemplate.Triggers>
                            <Trigger Property=""IsMouseOver"" Value=""True"">
                                <Setter Property=""Cursor"" Value=""Hand""></Setter>
                            </Trigger>                            
                        </ControlTemplate.Triggers>
                    </ControlTemplate>
                </Setter.Value>
            </Setter>
            <Setter Property=""FocusVisualStyle""  Value=""{x:Null}""/>
        </Style>

        <Style TargetType=""{x:Type WindowManagement:PanelGridButton}"">
            <Setter Property=""Background"" Value=""Transparent"" />
            <Setter Property=""Margin"" Value=""0""/>
            <Setter Property=""FocusVisualStyle"" Value=""{x:Null}""/>
            <Setter Property=""HorizontalAlignment"" Value=""Center""/>
            <Setter Property=""Control.Template"">
                <Setter.Value>
                    <ControlTemplate TargetType=""Button"">
                        <ContentPresenter x:Name=""PART_Content"" Content=""{TemplateBinding ContentControl.Content}""/>
                        <ControlTemplate.Triggers>
                            <Trigger Property=""IsMouseOver"" Value=""True"">
                                <Setter Property=""Cursor"" Value=""Hand""/>
                            </Trigger>
                            <Trigger Property=""IsKeyboardFocused"" Value=""True"">
                                <Setter TargetName=""PART_Content"" Property=""BitmapEffect"" Value=""{DynamicResource focus_outerglow_bitmapeffect}""></Setter>
                            </Trigger>
                            
                        </ControlTemplate.Triggers>
                    </ControlTemplate>
                </Setter.Value>
            </Setter>
        </Style>

        <Style x:Key=""actions_hover_style"" TargetType=""{x:Type WindowManagement:PanelGridButton}"">
            <Setter Property=""Background"" Value=""Transparent"" />
            <Setter Property=""Margin"" Value=""0""/>
            <Setter Property=""FocusVisualStyle"" Value=""{x:Null}""/>
            <Setter Property=""HorizontalAlignment"" Value=""Center""/>
            <Setter Property=""Control.Template"">
                <Setter.Value>
                    <ControlTemplate TargetType=""Button"">
                        <Viewbox Name=""view_box"" Width=""16"" Height=""16"" Stretch=""Fill"">
                            <ContentPresenter x:Name=""PART_Content"" Content=""{TemplateBinding ContentControl.Content}""/>
                        </Viewbox>
                        <ControlTemplate.Triggers>
                            <Trigger Property=""IsMouseOver"" Value=""True"">
                                <Setter Property=""Cursor"" Value=""Hand""/>
                            </Trigger>
                            <Trigger Property=""IsKeyboardFocused"" Value=""True"">
                                <Setter TargetName=""PART_Content"" Property=""BitmapEffect"" Value=""{DynamicResource focus_outerglow_bitmapeffect}""></Setter>
                            </Trigger>
                            <DataTrigger Binding=""{Binding IsMouseOver, RelativeSource={RelativeSource FindAncestor, AncestorType={x:Type Controls:DataGridRow}, AncestorLevel=1}}""
                                         Value=""True"">
                                <Setter TargetName=""view_box"" Property=""Visibility"" Value=""Visible""/>
                            </DataTrigger>


                            <DataTrigger Binding=""{Binding IsSelected, RelativeSource={RelativeSource FindAncestor, AncestorType={x:Type Controls:DataGridRow}, AncestorLevel=1}}""
                                         Value=""True"">
                                <Setter TargetName=""view_box"" Property=""Visibility"" Value=""Visible""/>
                            </DataTrigger>

                            <MultiDataTrigger>
                                <MultiDataTrigger.Conditions>
                                    <Condition Binding=""{Binding IsMouseOver, RelativeSource={RelativeSource FindAncestor, AncestorType={x:Type Controls:DataGridRow}, AncestorLevel=1}}"" Value=""False""/>
                                    <Condition Binding=""{Binding IsSelected, RelativeSource={RelativeSource FindAncestor, AncestorType={x:Type Controls:DataGridRow}, AncestorLevel=1}}"" Value=""False""/>
                                </MultiDataTrigger.Conditions>
                                <Setter TargetName=""view_box"" Property=""Visibility"" Value=""Hidden""/>
                            </MultiDataTrigger>
                            
                        </ControlTemplate.Triggers>
                    </ControlTemplate>
                </Setter.Value>
            </Setter>
            <Style.Triggers>


            </Style.Triggers>
        </Style>

        <Style TargetType=""Path"" x:Key=""newitem_path_style"">
            <Setter Property=""Width"" Value=""8.8""/>
            <Setter Property=""Height"" Value=""12.50""/>
            <Setter Property=""Stretch"" Value=""Fill""/>
            <Setter Property=""Fill"" Value=""{Binding RelativeSource={RelativeSource AncestorType={x:Type UserControl}, AncestorLevel=2},Path=SubMenuIconBgBrush}"" />
            <Setter Property=""Data"" Value=""F1 M 1076.01,405.235L 1082.23,405.235L 1084.84,407.396L 1084.84,417.735L 1076.01,417.735L 1076.01,405.235 Z M 1076.99,413.608L 1076.99,415.578L 1083.53,415.578L 1083.53,413.608L 1076.99,413.608 Z M 1077.05,410.916L 1077.05,412.886L 1083.59,412.886L 1083.59,410.916L 1077.05,410.916 Z M 1077.15,408.308L 1077.15,410.278L 1083.7,410.278L 1083.7,408.308L 1077.15,408.308 Z M 1077.12,406.277L 1077.12,407.705L 1082.05,407.705L 1082.05,406.277L 1077.12,406.277 Z M 1076.97,416.209L 1076.97,416.996L 1083.52,416.996L 1083.52,416.209L 1076.97,416.209 Z""/>
        </Style>

        <Style TargetType=""Path"" x:Key=""searchlupe_path_style"">
            <Setter Property=""Width"" Value=""12""/>
            <Setter Property=""Height"" Value=""11""/>
            <Setter Property=""Stretch"" Value=""Fill""/>
            <Setter Property=""Fill"" Value=""{Binding RelativeSource={RelativeSource AncestorType={x:Type UserControl}, AncestorLevel=2},Path=SubMenuIconBgBrush}"" />
            <Setter Property=""Data"" Value=""F1 M 1032.85,368.487L 1037.88,363.459C 1037.36,362.579 1037.06,361.551 1037.06,360.454C 1037.06,357.187 1039.71,354.537 1042.98,354.537C 1046.25,354.537 1048.9,357.187 1048.9,360.454C 1048.9,363.722 1046.25,366.371 1042.98,366.371C 1041.65,366.371 1040.43,365.935 1039.44,365.198L 1034.5,370.137C 1034.05,370.592 1033.31,370.592 1032.85,370.137C 1032.4,369.681 1032.4,368.942 1032.85,368.487 Z M 1040.29,362.7C 1040.39,362.803 1040.47,362.922 1040.53,363.048C 1041.16,363.628 1042,363.983 1042.92,363.983C 1044.87,363.983 1046.45,362.403 1046.45,360.454C 1046.45,358.506 1044.87,356.926 1042.92,356.926C 1040.97,356.926 1039.4,358.506 1039.4,360.454C 1039.4,361.227 1039.64,361.942 1040.06,362.523C 1040.15,362.571 1040.22,362.63 1040.29,362.7 Z""/>
        </Style>

        <Style TargetType=""Path"" x:Key=""arrowright_path_style"">
            <Setter Property=""Width"" Value=""15.6182""/>
            <Setter Property=""Height"" Value=""8.71942""/>
            <Setter Property=""Stretch"" Value=""Fill""/>
            <Setter Property=""Fill"" Value=""{Binding RelativeSource={RelativeSource AncestorType={x:Type UserControl}, AncestorLevel=2},Path=SubMenuIconBgBrush}"" />
            <Setter Property=""Data"" Value=""F1 M 1152.04,409.865L 1160.1,409.865L 1160.1,407.931L 1167.56,412.24L 1160.1,416.55L 1160.1,414.615L 1152.04,414.615L 1152.04,409.865 Z""/>
        </Style>

        <Style TargetType=""Path"" x:Key=""arrowleft_path_style"">
            <Setter Property=""Width"" Value=""15.6182""/>
            <Setter Property=""Height"" Value=""8.71942""/>
            <Setter Property=""Stretch"" Value=""Fill""/>
            <Setter Property=""Fill"" Value=""{Binding RelativeSource={RelativeSource AncestorType={x:Type UserControl}, AncestorLevel=2},Path=SubMenuIconBgBrush}"" />
            <Setter Property=""Data"" Value=""F1 M 1138.18,414.616L 1130.12,414.616L 1130.12,416.55L 1122.66,412.24L 1130.12,407.931L 1130.12,409.866L 1138.18,409.866L 1138.18,414.616 Z""/>
        </Style>

        <Style TargetType=""Path"" x:Key=""orderlayout1_path_style"">
            <Setter Property=""Width"" Value=""25.0518""/>
            <Setter Property=""Height"" Value=""10.2795""/>
            <Setter Property=""Stretch"" Value=""Fill""/>
            <Setter Property=""Fill"" Value=""{Binding RelativeSource={RelativeSource AncestorType={x:Type UserControl}, AncestorLevel=2},Path=SubMenuIconBgBrush}"" />
            <Setter Property=""Data"" Value=""F1 M 1183.73,406.444L 1188.15,406.444L 1188.15,410.539L 1183.73,410.539L 1183.73,406.444 Z M 1203.77,406.475L 1208.73,406.475L 1208.73,410.57L 1203.77,410.57L 1203.77,406.475 Z M 1190.37,406.44L 1195.33,406.44L 1195.33,410.535L 1190.37,410.535L 1190.37,406.44 Z M 1197.12,406.44L 1202.08,406.44L 1202.08,410.535L 1197.12,410.535L 1197.12,406.44 Z M 1183.68,412.593L 1188.1,412.593L 1188.1,416.689L 1183.68,416.689L 1183.68,412.593 Z M 1203.72,412.624L 1208.68,412.624L 1208.68,416.719L 1203.72,416.719L 1203.72,412.624 Z M 1190.32,412.589L 1195.27,412.589L 1195.27,416.684L 1190.32,416.684L 1190.32,412.589 Z M 1197.07,412.589L 1202.02,412.589L 1202.02,416.684L 1197.07,416.684L 1197.07,412.589 Z""/>
        </Style>

        <Style TargetType=""TextBlock"" x:Key=""arrowup_tb_style"">
            <Setter Property=""Text"" Value=""""/>
            <Setter Property=""FontFamily"" Value=""Wingdings 3""/>
            <Setter Property=""FontSize"" Value=""24""/>
        </Style>

        <Style TargetType=""TextBlock"" x:Key=""arrowdown_tb_style"">
            <Setter Property=""Text"" Value=""""/>
            <Setter Property=""FontFamily"" Value=""Wingdings 3""/>
            <Setter Property=""FontSize"" Value=""24""/>
        </Style>

        <Style TargetType=""Path"" x:Key=""orderlayout2_path_style"">
            <Setter Property=""Width"" Value=""24.9998""/>
            <Setter Property=""Height"" Value=""8.05994""/>
            <Setter Property=""Stretch"" Value=""Fill""/>
            <Setter Property=""Fill"" Value=""{Binding RelativeSource={RelativeSource AncestorType={x:Type UserControl}, AncestorLevel=2},Path=SubMenuIconBgBrush}"" />
            <Setter Property=""Data"" Value=""F1 M 1217.76,407.663L 1222.18,407.663L 1222.18,415.663L 1217.76,415.663L 1217.76,407.663 Z M 1224.13,407.698L 1235.8,407.698L 1235.8,415.698L 1224.13,415.698L 1224.13,407.698 Z M 1237.8,407.722L 1242.76,407.722L 1242.76,415.722L 1237.8,415.722L 1237.8,407.722 Z ""/>
        </Style>

        <Style TargetType=""Path"" x:Key=""orderlayout3_path_style"">
            <Setter Property=""Width"" Value=""17.0419""/>
            <Setter Property=""Height"" Value=""7.26038""/>
            <Setter Property=""Stretch"" Value=""Fill""/>
            <Setter Property=""Fill"" Value=""{Binding RelativeSource={RelativeSource AncestorType={x:Type UserControl}, AncestorLevel=2},Path=SubMenuIconBgBrush}"" />
            <Setter Property=""Data"" Value=""F1 M 1254.89,408.075L 1271.93,408.075L 1271.93,408.825L 1254.89,408.825L 1254.89,408.075 Z M 1254.89,411.304L 1271.93,411.304L 1271.93,412.054L 1254.89,412.054L 1254.89,411.304 Z M 1254.89,414.585L 1271.93,414.585L 1271.93,415.335L 1254.89,415.335L 1254.89,414.585 Z""/>
        </Style>

        <Style TargetType=""Path"" x:Key=""pin_path_style"">
            <Setter Property=""Width"" Value=""8.26038""/>
            <Setter Property=""Height"" Value=""12.6685""/>
            <Setter Property=""Stretch"" Value=""Fill""/>
            <Setter Property=""Fill"" Value=""{Binding RelativeSource={RelativeSource AncestorType={x:Type UserControl}, AncestorLevel=2},Path=SubMenuIconBgBrush}"" />
            <Setter Property=""Data"" Value=""F1 M 1300.12,416.281L 1299.75,416.281L 1297.12,416.281L 1296.5,416.281L 1296.5,418.563L 1295.25,418.563L 1295.25,416.281L 1293.06,416.281L 1291.87,416.281L 1291.87,415.094L 1291.87,407.082L 1291.87,406.094L 1291.87,405.894L 1300.13,405.894L 1300.13,407.082L 1300.12,407.082L 1300.12,416.281 Z M 1297.12,415.094L 1297.12,407.082L 1293.06,407.082L 1293.06,415.094L 1297.12,415.094 Z""/>
        </Style>

        <Style TargetType=""Button"" x:Key=""dg_link_button_style"">
            <Setter Property=""OverridesDefaultStyle"" Value=""True""/>
            <Setter Property=""HorizontalAlignment"" Value=""Left""/>
            <Setter Property=""HorizontalContentAlignment"" Value=""Left""/>
            <Setter Property=""Width"" Value=""Auto""/>
            <Setter Property=""Control.Template"">
                <Setter.Value>
                    <ControlTemplate TargetType=""Button"">
                        <TextBlock Name=""tb"" Style=""{x:Null}"" Text=""{TemplateBinding ContentControl.Content}"" Foreground=""{TemplateBinding Foreground}"" 
                                   FontWeight=""Bold"" FontSize=""12"" FontFamily=""Calibri"" Margin=""0"" Padding=""0"" HorizontalAlignment=""Left"" TextAlignment=""Left"">
                            <!--<ContentPresenter HorizontalAlignment=""Left""  Content=""{TemplateBinding ContentControl.Content}"" />-->
                            
                        </TextBlock>
                        <ControlTemplate.Triggers>
                            <Trigger Property=""IsMouseOver"" Value=""True"">
                                <Setter Property=""Cursor"" Value=""Hand""></Setter>
                                <Setter TargetName=""tb"" Property=""TextDecorations"" Value=""Underline""></Setter>
                            </Trigger>
                            <Trigger Property=""IsKeyboardFocused"" Value=""True"">
                                <!--<Setter TargetName=""tb"" Property=""Foreground"" Value=""White""></Setter>-->
                            </Trigger>
                        </ControlTemplate.Triggers>
                    </ControlTemplate>
                </Setter.Value>
            </Setter>
            <Setter Property=""FocusVisualStyle""  Value=""{x:Null}""/>
        </Style>

        <Style x:Key=""grid_action_icon_button_style"" TargetType=""{x:Type Button}"">
            <Setter Property=""Background"" Value=""Transparent"" />
            <Setter Property=""Margin"" Value=""0""/>
            <Setter Property=""FocusVisualStyle"" Value=""{x:Null}""/>
            <Setter Property=""HorizontalAlignment"" Value=""Center""/>
            <Setter Property=""Control.Template"">
                <Setter.Value>
                    <ControlTemplate TargetType=""Button"">
                        <ContentControl Name=""btn_image"" VerticalAlignment=""Center"" HorizontalAlignment=""Center"" Content=""{TemplateBinding ContentControl.Content}""/>
                        <ControlTemplate.Triggers>
                            <Trigger Property=""IsMouseOver"" Value=""True"">
                                <Setter Property=""Cursor"" Value=""Hand""/>
                            </Trigger>
                            <Trigger Property=""IsKeyboardFocused"" Value=""True"">
                                <Setter TargetName=""btn_image"" Property=""BitmapEffect"" Value=""{DynamicResource focus_outerglow_bitmapeffect}""></Setter>
                            </Trigger>
                        </ControlTemplate.Triggers>
                    </ControlTemplate>
                </Setter.Value>
            </Setter>
        </Style>

        <Style x:Key=""grid_action_button_style"" TargetType=""{x:Type Button}"">
            <Setter Property=""Background"" Value=""Transparent"" />
            <Setter Property=""Width"" Value=""24""/>
            <Setter Property=""Height"" Value=""24""/>
            <Setter Property=""Margin"" Value=""0""/>
            <Setter Property=""FocusVisualStyle"" Value=""{x:Null}""/>
            <Setter Property=""HorizontalAlignment"" Value=""Center""/>
            <Setter Property=""Control.Template"">
                <Setter.Value>
                    <ControlTemplate TargetType=""Button"">
                        <TextBlock Name=""tb_content""  VerticalAlignment=""Center"" HorizontalAlignment=""Center"" Text=""{TemplateBinding ContentControl.Content}""/>
                        <ControlTemplate.Triggers>
                            <Trigger Property=""IsMouseOver"" Value=""True"">
                                <Setter Property=""Cursor"" Value=""Hand""/>
                            </Trigger>
                            <Trigger Property=""IsKeyboardFocused"" Value=""True"">
                                <Setter TargetName=""tb_content"" Property=""BitmapEffect"" Value=""{DynamicResource focus_outerglow_bitmapeffect}""></Setter>
                            </Trigger>
                        </ControlTemplate.Triggers>
                    </ControlTemplate>
                </Setter.Value>
            </Setter>
        </Style>

        <RoutedUICommand x:Key=""LeftArrowCommand"" Text=""vorheriges Element""/>
        <RoutedUICommand x:Key=""RightArrowCommand"" Text=""nächstes Element""/>
    </UserControl.Resources>

    <UserControl.CommandBindings>
        <CommandBinding x:Name=""cleft"" Command=""{StaticResource LeftArrowCommand}""/>
        <CommandBinding x:Name=""cright"" Command=""{StaticResource RightArrowCommand}""/>      
    </UserControl.CommandBindings>

    <Grid Height=""{Binding ElementName=submenu_border,Path=ActualHeight}"" Name=""base_grid"">
        <Border Name=""submenu_border"" Height=""0"" Background=""{Binding RelativeSource={RelativeSource AncestorType={x:Type UserControl}, AncestorLevel=2},Path=PanelBackground}"">
            <Grid>
                <Grid.ColumnDefinitions>
                    <ColumnDefinition Width=""Auto"" />
                    <ColumnDefinition Width=""*"" />
                </Grid.ColumnDefinitions>
                <Grid.RowDefinitions>
                    <RowDefinition Height=""Auto""/>
                    <RowDefinition Height=""*""/>
                </Grid.RowDefinitions>

                <StackPanel Orientation=""Horizontal"" Grid.Row=""0"" Grid.Column=""0"" Name=""sub_breadcrumb_stack"" VerticalAlignment=""Center"" 
                        HorizontalAlignment=""Left"" Height=""30"">
                    <StackPanel.Resources>
                         <Style TargetType=""ListBox"">
                            <Setter Property=""ItemsPanel"">
                                <Setter.Value>
                                    <ItemsPanelTemplate>
                                        <StackPanel Orientation=""Horizontal""
                                            VerticalAlignment=""Center""
                                            HorizontalAlignment=""Left""/>
                                    </ItemsPanelTemplate>
                                </Setter.Value>
                            </Setter>
                        </Style>
                        
                        <Style x:Key=""listbox_border_style"" TargetType=""{x:Type Border}"">
                            <Setter Property=""Background"" Value=""Transparent"" />
                            <Setter Property=""BorderBrush"" Value=""Transparent""/>
                            <Setter Property=""BorderThickness"" Value=""1""/>
                            <Setter Property=""CornerRadius"" Value=""0""/>
                            <Setter Property=""Padding"" Value=""2""/>
                        </Style>


                        <Style TargetType=""{x:Type ListBoxItem}"">
                            <Setter Property=""Background"" Value=""Transparent"" />

                            <Setter Property=""Control.Template"">
                                <Setter.Value>
                                    <ControlTemplate TargetType=""ListBoxItem"">
                                        <ContentPresenter></ContentPresenter>

                                    </ControlTemplate>
                                </Setter.Value>
                            </Setter>
                        </Style>                     
       
                    </StackPanel.Resources>
                    <ListBox Name=""lb_breadcrumb"" Visibility=""Collapsed"" Width=""Auto"" Height=""30"" BorderThickness=""0"" Background=""Transparent"">
                        
                    </ListBox>
                </StackPanel>
                
                <StackPanel Orientation=""Horizontal"" Grid.Row=""0"" Grid.Column=""1"" Name=""sub_header_stack"" VerticalAlignment=""Center"" HorizontalAlignment=""Right"" 
                    Height=""30"">
                    <StackPanel.Resources>
                        <Style TargetType=""Button"">
                            <Setter Property=""Background"" Value=""Transparent"" />
                            <Setter Property=""BorderThickness"" Value=""0"" />
                            <Setter Property=""BorderBrush"" Value=""Transparent"" />
                            <Setter Property=""Width"" Value=""35"" />
                            <Setter Property=""Height"" Value=""25"" />
                            <Setter Property=""VerticalAlignment"" Value=""Center"" />
                            <Setter Property=""Template"">
                                <Setter.Value>
                                    <ControlTemplate TargetType=""Button"">
                                        <Border BorderBrush=""{TemplateBinding BorderBrush}"" BorderThickness=""{TemplateBinding BorderThickness}"" 
                                                Background=""{TemplateBinding Background}"">
                                            <ContentPresenter />
                                        </Border>
                                    </ControlTemplate>
                                </Setter.Value>
                            </Setter>
                        </Style>

                        <Style TargetType=""{x:Type TouchButton:TouchButton}"">
                            <Setter Property=""Background"" Value=""Transparent"" />
                            <Setter Property=""BorderThickness"" Value=""0"" />
                            <Setter Property=""BorderBrush"" Value=""Transparent"" />
                            <Setter Property=""Width"" Value=""35"" />
                            <Setter Property=""Height"" Value=""25"" />
                            <Setter Property=""VerticalAlignment"" Value=""Center"" />
                            <Setter Property=""Template"">
                                <Setter.Value>
                                    <ControlTemplate TargetType=""Button"">
                                        <Border BorderBrush=""{TemplateBinding BorderBrush}"" BorderThickness=""{TemplateBinding BorderThickness}"" 
                                            Background=""{TemplateBinding Background}"">
                                            <ContentPresenter />
                                        </Border>
                                    </ControlTemplate>
                                </Setter.Value>
                            </Setter>
                        </Style>

                        <Style TargetType=""Button"" x:Key=""search_button_style_old"">
                            <Setter Property=""Background"" Value=""Transparent"" />
                            <Setter Property=""BorderThickness"" Value=""0"" />
                            <Setter Property=""BorderBrush"" Value=""Transparent"" />
                            <Setter Property=""Width"" Value=""35"" />
                            <Setter Property=""Height"" Value=""25"" />
                            <Setter Property=""VerticalAlignment"" Value=""Center"" />
                            <Setter Property=""Template"">
                                <Setter.Value>
                                    <ControlTemplate TargetType=""Button"">
                                        <Border BorderBrush=""{TemplateBinding BorderBrush}"" BorderThickness=""{TemplateBinding BorderThickness}"" 
                                            Background=""{TemplateBinding Background}"">
                                            <ContentPresenter />
                                        </Border>
                                    </ControlTemplate>
                                </Setter.Value>
                            </Setter>
                            <Style.Triggers>
                                <Trigger Property=""IsMouseOver"" Value=""True"">
                                    <Setter Property=""Cursor"" Value=""Hand""/>
                                </Trigger>
                            </Style.Triggers>
                        </Style>


                        <Style TargetType=""{x:Type TouchButton:TouchButton}"" x:Key=""search_button_style"">
                            <Setter Property=""Background"" Value=""Transparent"" />
                            <Setter Property=""BorderThickness"" Value=""0"" />
                            <Setter Property=""BorderBrush"" Value=""Transparent"" />
                            <Setter Property=""Width"" Value=""35"" />
                            <Setter Property=""Height"" Value=""25"" />
                            <Setter Property=""VerticalAlignment"" Value=""Center"" />
                            <Setter Property=""Template"">
                                <Setter.Value>
                                    <ControlTemplate TargetType=""Button"">
                                        <Border BorderBrush=""{TemplateBinding BorderBrush}"" BorderThickness=""{TemplateBinding BorderThickness}"" Background=""{TemplateBinding Background}"">
                                            <ContentPresenter />
                                        </Border>
                                    </ControlTemplate>
                                </Setter.Value>
                            </Setter>
                            <Style.Triggers>
                                <Trigger Property=""IsMouseOver"" Value=""True"">
                                    <Setter Property=""Cursor"" Value=""Hand""/>
                                </Trigger>
                            </Style.Triggers>
                        </Style>

                        <Style TargetType=""ToggleButton"">
                            <Setter Property=""Background"" Value=""Transparent"" />
                            <Setter Property=""BorderThickness"" Value=""0"" />
                            <Setter Property=""BorderBrush"" Value=""Gray"" />
                            <Setter Property=""Width"" Value=""35"" />
                            <Setter Property=""Height"" Value=""25"" />
                            <Setter Property=""VerticalAlignment"" Value=""Center"" />
                            <Setter Property=""Template"">
                                <Setter.Value>
                                    <ControlTemplate TargetType=""ToggleButton"">
                                        <Border Name=""border"" Width=""{TemplateBinding Width}"" Height=""{TemplateBinding Height}"" BorderBrush=""{TemplateBinding BorderBrush}"" BorderThickness=""{TemplateBinding BorderThickness}"" Background=""{TemplateBinding Background}"">
                                            <ContentPresenter />
                                        </Border>
                                        <ControlTemplate.Triggers>
                                            <Trigger Property=""IsChecked"" Value=""true"">
                                                <Setter TargetName=""border""  Property=""Background"" Value=""White"" />
                                                <Setter TargetName=""border""  Property=""BorderThickness"" Value=""0.5"" />
                                            </Trigger>
                                        </ControlTemplate.Triggers>
                                    </ControlTemplate>
                                </Setter.Value>
                            </Setter>
                        </Style>

                        <Style TargetType=""{x:Type TouchButton:TouchToggleButton}"">
                            <Setter Property=""Background"" Value=""Transparent"" />
                            <Setter Property=""BorderThickness"" Value=""0"" />
                            <Setter Property=""BorderBrush"" Value=""Gray"" />
                            <Setter Property=""Width"" Value=""35"" />
                            <Setter Property=""Height"" Value=""25"" />
                            <Setter Property=""VerticalAlignment"" Value=""Center"" />
                            <Setter Property=""Template"">
                                <Setter.Value>
                                    <ControlTemplate TargetType=""ToggleButton"">
                                        <Border Name=""border"" Width=""{TemplateBinding Width}"" Height=""{TemplateBinding Height}"" BorderBrush=""{TemplateBinding BorderBrush}"" BorderThickness=""{TemplateBinding BorderThickness}"" Background=""{TemplateBinding Background}"">
                                            <ContentPresenter />
                                        </Border>
                                        <ControlTemplate.Triggers>
                                            <Trigger Property=""IsChecked"" Value=""true"">
                                                <Setter TargetName=""border""  Property=""Background"" Value=""White"" />
                                                <Setter TargetName=""border""  Property=""BorderThickness"" Value=""0.5"" />
                                            </Trigger>
                                        </ControlTemplate.Triggers>
                                    </ControlTemplate>
                                </Setter.Value>
                            </Setter>
                        </Style>

                        <Style TargetType=""TextBlock"">
                            <Setter Property=""VerticalAlignment"" Value=""Center"" />
                            <Setter Property=""Foreground"" Value=""{Binding RelativeSource={RelativeSource AncestorType={x:Type UserControl}, AncestorLevel=2},Path=SubMenuIconBgBrush}""/>
                        </Style>
                    </StackPanel.Resources>


                    <TouchButton:TouchButton x:Name=""custom_button1"" Width=""Auto"" FocusManager.IsFocusScope=""True"">
                        <StackPanel Orientation=""Horizontal"">
                            <TextBlock Margin=""15,0,15,0"" Text=""{Binding RelativeSource={RelativeSource AncestorType={x:Type UserControl}, AncestorLevel=2},Path=CustomButtonText1}""></TextBlock>
                        </StackPanel>
                    </TouchButton:TouchButton>

                    <Rectangle Name=""custom_rect1"" Margin=""5,0,5,0"" Fill=""{Binding RelativeSource={RelativeSource AncestorType={x:Type UserControl}, AncestorLevel=2},Path=SubMenuIconBgBrush}"" Width=""1"" Height=""15"" HorizontalAlignment=""Stretch"" VerticalAlignment=""Center""/>

                    <TouchButton:TouchButton x:Name=""custom_button"" Width=""Auto"" FocusManager.IsFocusScope=""True"">
                        <StackPanel Orientation=""Horizontal"">
                            <TextBlock Margin=""15,0,15,0"" Text=""{Binding RelativeSource={RelativeSource AncestorType={x:Type UserControl}, AncestorLevel=2},Path=CustomButtonText}""></TextBlock>
                        </StackPanel>
                    </TouchButton:TouchButton>

                    <Rectangle Name=""custom_rect"" Margin=""5,0,5,0"" Fill=""{Binding RelativeSource={RelativeSource AncestorType={x:Type UserControl}, AncestorLevel=2},Path=SubMenuIconBgBrush}"" Width=""1"" Height=""15"" HorizontalAlignment=""Stretch"" VerticalAlignment=""Center""/>


                    <!--<TextBox Name=""search_cbx"" Width=""150"" Height=""20"" BorderBrush=""{Binding RelativeSource={RelativeSource AncestorType={x:Type UserControl}, AncestorLevel=2},Path=SubMenuIconBgBrush}"" Background=""Transparent""></TextBox>-->
                    <Combobox:AcCbx  x:Name=""search_cbx"" Width=""150"" Height=""20"" BorderBrush=""{Binding RelativeSource={RelativeSource AncestorType={x:Type UserControl}, AncestorLevel=2},Path=SubMenuIconBgBrush}"" Background=""Transparent""/>
                    <TouchButton:TouchButton x:Name=""search_button"" ToolTip=""{Binding RelativeSource={RelativeSource AncestorType={x:Type UserControl}, AncestorLevel=2},Path=SearchButtonTooltip, Mode=OneWay}"" FocusManager.IsFocusScope=""True""
                            Style=""{StaticResource search_button_style}"">

                        <Border Background=""Transparent"">
                            <Path Style=""{StaticResource searchlupe_path_style}""></Path>
                        </Border>
                    </TouchButton:TouchButton>

                    <Rectangle Name=""search_rect"" Margin=""5,0,5,0"" Fill=""{Binding RelativeSource={RelativeSource AncestorType={x:Type UserControl}, AncestorLevel=2},Path=SubMenuIconBgBrush}"" Width=""1"" Height=""15"" HorizontalAlignment=""Stretch"" VerticalAlignment=""Center""/>

                    <TouchButton:TouchButton x:Name=""newelem_button"" Width=""Auto"" FocusManager.IsFocusScope=""True"">
                        <StackPanel Orientation=""Horizontal"">
                            <Path Style=""{StaticResource newitem_path_style}"" Margin=""15,0,4,0""></Path>
                            <TextBlock Margin=""0,0,15,0"" Text=""{Binding RelativeSource={RelativeSource AncestorType={x:Type UserControl}, AncestorLevel=2},Path=CustomNewElementButtonText}""></TextBlock>
                        </StackPanel>
                    </TouchButton:TouchButton>

                    <Rectangle Name=""newelem_rect"" Margin=""5,0,5,0"" Fill=""{Binding RelativeSource={RelativeSource AncestorType={x:Type UserControl}, AncestorLevel=2},Path=SubMenuIconBgBrush}"" Width=""1"" Height=""15"" HorizontalAlignment=""Stretch"" VerticalAlignment=""Center""/>

                    <TouchButton:TouchButton x:Name=""arrowleft_button""  ToolTip=""vorheriges Element"" FocusManager.IsFocusScope=""True"">
                        <Path Style=""{StaticResource arrowleft_path_style}""></Path>
                    </TouchButton:TouchButton>
                    <TouchButton:TouchButton x:Name=""arrowright_button"" ToolTip=""nächstes Element"" FocusManager.IsFocusScope=""True"">
                        <Path Style=""{StaticResource arrowright_path_style}"">
                        </Path>
                    </TouchButton:TouchButton>

                    <TouchButton:TouchButton x:Name=""arrowup_button"" ToolTip=""nächstes Element"" Visibility=""Collapsed"" FocusManager.IsFocusScope=""True"">
                        <Path Style=""{StaticResource arrowleft_path_style}"">
                            <Path.RenderTransform>
                                <RotateTransform Angle=""90"" CenterX=""8.5"" CenterY=""3.6""/>
                            </Path.RenderTransform>
                        </Path>
                    </TouchButton:TouchButton>

                    <TouchButton:TouchButton x:Name=""arrowdown_button"" ToolTip=""vorheriges Element"" Visibility=""Collapsed"" FocusManager.IsFocusScope=""True"">
                        <Path Style=""{StaticResource arrowright_path_style}"">
                            <Path.RenderTransform>
                                <RotateTransform Angle=""90"" CenterX=""8.5"" CenterY=""3.6""/>
                            </Path.RenderTransform>
                        </Path>
                    </TouchButton:TouchButton>


                    <TouchButton:TouchToggleButton x:Name=""orderlayout1_button"" Margin=""10,0,0,0"" ToolTip=""Kleine Symbole"" FocusManager.IsFocusScope=""True"">
                        <Path Style=""{StaticResource orderlayout1_path_style}""></Path>
                    </TouchButton:TouchToggleButton>
                    <TouchButton:TouchToggleButton x:Name=""orderlayout2_button"" FocusManager.IsFocusScope=""True"">
                        <Path Style=""{StaticResource orderlayout2_path_style}"" ToolTip=""Grosse Symbole""></Path>
                    </TouchButton:TouchToggleButton>
                    <TouchButton:TouchToggleButton x:Name=""orderlayout3_button"" FocusManager.IsFocusScope=""True"">
                        <Path Style=""{StaticResource orderlayout3_path_style}"" ToolTip=""Tabelle""></Path>
                    </TouchButton:TouchToggleButton>

                    <Rectangle Visibility=""Collapsed"" Margin=""5,0,5,0"" Fill=""{Binding RelativeSource={RelativeSource AncestorType={x:Type UserControl}, AncestorLevel=2},Path=SubMenuIconBgBrush}"" Width=""1"" Height=""15"" HorizontalAlignment=""Stretch"" VerticalAlignment=""Center""/>

                    <TouchButton:TouchToggleButton x:Name=""pin_button"" Margin=""10,0,0,0"" ToolTip=""Leinwand fixieren"" FocusManager.IsFocusScope=""True"">
                        <Path Style=""{StaticResource pin_path_style}""></Path>
                    </TouchButton:TouchToggleButton>
                    
                    <!-- Placeholder, dass immer garantiert 10px Abstand zum rechten Rand sind-->
                    <Rectangle Visibility=""Hidden"" Width=""10""></Rectangle> 

                </StackPanel>

                <Grid Name=""submenu_overall_grid"" Background=""Transparent"" Grid.Row=""1"" Grid.ColumnSpan=""2"" ClipToBounds=""true"">
                    <Grid Name=""submenu_base_grid"" Background=""Transparent"" ClipToBounds=""true"" Cursor=""ScrollAll"">
                    <Canvas Name=""submenu_base_canvas"" HorizontalAlignment=""Stretch"" VerticalAlignment=""Stretch"" FocusManager.IsFocusScope=""True"">
                        <ItemsControl Name=""animating_tile_panel_items_control"" Focusable=""False""
                                    Height=""{Binding ElementName=submenu_base_canvas,Path=ActualHeight}""
                                    ItemsSource=""{Binding}""
                        >
                            <ItemsControl.ItemsPanel>
                                <ItemsPanelTemplate>
                                        <Wpf:AnimatingTilePanel Name=""animated_tile_panel_template"" />
                                </ItemsPanelTemplate>
                            </ItemsControl.ItemsPanel>
                        </ItemsControl>

                    

                        </Canvas>
                    </Grid>
                    <Controls:DataGrid x:Name=""submenu_items_datagrid"" Visibility=""Collapsed""
                                       Height=""{Binding ElementName=submenu_overall_grid,Path=ActualHeight}""
                                       Width=""{Binding ElementName=submenu_overall_grid,Path=ActualWidth}""
                                       ItemsSource=""{Binding}""
                                       CanUserDeleteRows=""False""
                                       CanUserResizeColumns=""False""
                                       CanUserReorderColumns=""False""
                                       CanUserSortColumns=""True""
                                       CanUserResizeRows=""True""
                                       CanUserAddRows=""False""
                                       AutoGenerateColumns=""False""
                                       RowDetailsVisibilityMode=""VisibleWhenSelected""
                                       SelectionMode=""Single""
                                       VirtualizingStackPanel.IsVirtualizing=""True""
                                       VirtualizingStackPanel.VirtualizationMode=""Standard""                                       
                                       ScrollViewer.CanContentScroll=""True"">

                    </Controls:DataGrid>
                </Grid>
            </Grid>
        </Border>
        <Rectangle Fill=""{Binding RelativeSource={RelativeSource AncestorType={x:Type UserControl}, AncestorLevel=2},Path=BorderBrush}"" Height=""1"" HorizontalAlignment=""Stretch"" 
            VerticalAlignment=""Top"" Visibility=""{Binding ElementName=sizechanged_rect, Path=Visibility}""/>
        <Rectangle Name=""sizechanged_rect"" Fill=""Transparent"" Height=""5"" HorizontalAlignment=""Stretch"" VerticalAlignment=""Top"" Cursor=""SizeNS""/>
    </Grid>

</UserControl>";
            try
            {
                this.Content = XamlReader.Parse(xaml_string);
            }
            catch (Exception ex) { }

            SearchButton.Click += new RoutedEventHandler(search_button_Click);
            NewelemButton.Click += new RoutedEventHandler(newelem_button_Click);
            CustomButton.Click += new RoutedEventHandler(custom_button_Click);
            CustomButton1.Click += new RoutedEventHandler(custom_button1_Click);
            BaseGrid.StylusDown += new StylusDownEventHandler(base_grid_StylusDown);
            BaseGrid.StylusUp += new StylusEventHandler(base_grid_StylusUp);
            BaseGrid.StylusMove += new StylusEventHandler(base_grid_StylusMove);
            BaseGrid.MouseUp += new MouseButtonEventHandler(base_grid_MouseUp);
            BaseGrid.MouseMove += new MouseEventHandler(base_grid_MouseMove);
            BaseGrid.MouseDown += new MouseButtonEventHandler(base_grid_MouseDown);
            BaseGrid.MouseLeave += new MouseEventHandler(base_grid_MouseLeave);
            SubmenuBorder.SizeChanged += new SizeChangedEventHandler(submenu_base_canvas_SizeChanged);
            ArrowleftButton.Click += new RoutedEventHandler(arrowleft_button_Click);
            ArrowrightButton.Click += new RoutedEventHandler(arrowright_button_Click);
            ArrowupButton.Click += new RoutedEventHandler(arrowup_button_Click);
            ArrowdownButton.Click += new RoutedEventHandler(arrowdown_button_Click);
            SubmenuItemsDatagrid.SelectionChanged += new SelectionChangedEventHandler(submenu_items_datagrid_SelectionChanged);
            SubmenuItemsDatagrid.SizeChanged += new SizeChangedEventHandler(submenu_items_datagrid_SizeChanged);
            SubmenuItemsDatagrid.LoadingRow += new EventHandler<DataGridRowEventArgs>(submenu_items_datagrid_LoadingRow);
            SizeChangeRectangle.MouseDown += new MouseButtonEventHandler(Rectangle_MouseDown);
            CommRight.Executed += new ExecutedRoutedEventHandler(CommandBinding_NavigateRight);
            CommLeft.Executed += new ExecutedRoutedEventHandler(CommandBinding_NavigateLeft);

        }
        
        #region Finding Xaml Elements
        public UserControl ContentControl
        {
            get
            {
                return (UserControl)Content;
            }
        }    
        public Rectangle SizeChangeRectangle
        {
            get {
                return _size_change_rectangle ??
                       (_size_change_rectangle = (Rectangle) ContentControl.FindName("sizechanged_rect"));
            }
        }

        public Grid base_grid
        {
            get { return _base_grid ?? (_base_grid = (Grid) ContentControl.FindName("base_grid")); }
        }
        
        public ListBox LbBreadCrumb
        {
            get { return _lb_bread_crumb ?? (_lb_bread_crumb = (ListBox) ContentControl.FindName("lb_breadcrumb")); }
        }
        
        public TouchButton ArrowleftButton
        {
            get {
                return _arrow_left_button ??
                       (_arrow_left_button = (TouchButton) ContentControl.FindName("arrowleft_button"));
            }
        }

        public TouchButton ArrowrightButton
        {
            get {
                return _arrow_right_button ??
                       (_arrow_right_button = (TouchButton) ContentControl.FindName("arrowright_button"));
            }
        }

        public TouchButton ArrowupButton
        {
            get { return _arrow_up_button ?? (_arrow_up_button = (TouchButton) ContentControl.FindName("arrowup_button")); }
        }

        public TouchButton ArrowdownButton
        {
            get {
                return _arrow_down_button ??
                       (_arrow_down_button = (TouchButton) ContentControl.FindName("arrowdown_button"));
            }
        }

        public TouchToggleButton OrderLayout1Button
        {
            get 
            { 
                return _order_layout1_button ?? (_order_layout1_button = (TouchToggleButton) ContentControl.FindName("orderlayout1_button"));
            }
        }

        public TouchToggleButton OrderLayout2Button
        {
            get
            {
                return _order_layout2_button ??
                       (_order_layout2_button = (TouchToggleButton) ContentControl.FindName("orderlayout2_button"));
            }
        }

        public TouchToggleButton OrderLayout3Button
        {
            get
            {
                return _order_layout3_button ?? (_order_layout3_button =(TouchToggleButton)ContentControl.FindName("orderlayout3_button"));
            }
        }

        public TouchToggleButton PinButton
        {
            get
            {
                return _pin_button ?? (_pin_button=(TouchToggleButton)ContentControl.FindName("pin_button"));
            }
        }

        public TouchButton NewelemButton
        {
            get
            {
                return _newelem_button ?? (_newelem_button=(TouchButton)ContentControl.FindName("newelem_button"));
            }
        }

        public TouchButton CustomButton1
        {
            get
            {
                return _custom_button1 ?? (_custom_button1=(TouchButton)ContentControl.FindName("custom_button1"));
            }
        }

        public TouchButton TemplateButton
        {
            get
            {
                return _template_button ?? (_template_button=(TouchButton)ContentControl.FindName("template_button"));
            }
        }

        public TouchButton CustomButton
        {
            get
            {
                return _custom_button ?? (_custom_button =(TouchButton)ContentControl.FindName("custom_button"));
            }
        }

        public TouchButton SearchButton
        {
            get
            {
                return _search_button ?? (_search_button = (TouchButton)ContentControl.FindName("search_button"));
            }
        }
        
        public ItemsControl AnimatingTilePanelItemsControl
        {
            get
            {
                return _animating_tile_panel_items_control ?? (_animating_tile_panel_items_control =(ItemsControl)ContentControl.FindName("animating_tile_panel_items_control"));
            }
        }
        
        public Grid SubmenuBaseGrid
        {
            get
            {
                return _submenu_base_grid ?? (_submenu_base_grid = (Grid)ContentControl.FindName("submenu_base_grid"));
            }
        }
        
        public Canvas SubmenuBaseCanvas
        {
            get
            {
                return _submenu_base_canvas ?? (_submenu_base_canvas=(Canvas)ContentControl.FindName("submenu_base_canvas"));
            }
        }

        public StackPanel SubBreadcrumbStack
        {
            get
            {
                return _sub_bread_crumb_stack ?? (_sub_bread_crumb_stack =(StackPanel)ContentControl.FindName("sub_breadcrumb_stack"));
            }
        }

        public StackPanel SubHeaderStack
        {
            get
            {
                return _sub_header_stack ?? (_sub_header_stack=(StackPanel)ContentControl.FindName("sub_header_stack"));
            }
        }

        public DataGrid SubmenuItemsDatagrid
        {
            get
            {
                return _submenu_items_datagrid ?? (_submenu_items_datagrid = (DataGrid)ContentControl.FindName("submenu_items_datagrid"));
            }
        }
        
        public Border SubmenuBorder
        {
            get
            {
                return _submenu_border ?? (_submenu_border = (Border)ContentControl.FindName("submenu_border"));
            }
        }
        
        public AcCbx SearchCbx
        {
            get
            {
                return _search_cbx ?? (_search_cbx =(AcCbx)ContentControl.FindName("search_cbx"));
            }
        }

        public Rectangle NewelemRect
        {
            get
            {
                return _newelem_rect ?? (_newelem_rect = (Rectangle)ContentControl.FindName("newelem_rect"));
            }
        }

        public Rectangle SearchRect
        {
            get
            {
                return _search_rect ?? (_search_rect = (Rectangle)ContentControl.FindName("search_rect"));
            }
        }

        public Rectangle CustomRect1
        {
            get
            {
                return _custom_rect1 ?? (_custom_rect1 =(Rectangle)ContentControl.FindName("custom_rect1"));
            }
        }

        public Rectangle CustomRect
        {
            get
            {
                return _custom_rect ?? (_custom_rect = (Rectangle)ContentControl.FindName("custom_rect"));
            }
        }

        public CommandBinding CommLeft
        {
            get
            {
                return _comm_left ?? (_comm_left = (CommandBinding)ContentControl.FindName("cleft"));
            }
        }

        public CommandBinding CommRight
        {
            get
            {
                return _comm_right ?? (_comm_right = (CommandBinding)ContentControl.FindName("cright"));
            }
        }
        #endregion
      
        private void InitDefaultValues()
        {
            WithCustomButton = false;
        }

        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            if (e.Property == ButtonHighlightBackgroundProperty)
            {
                if (HighlightSearchButton)
                {
                    SearchButton.Background = e.NewValue as Brush;
                }
            }
            else if (e.Property == HighlightSearchButtonProperty)
            {
                if ((bool)e.NewValue)
                {
                    SearchButton.Background = ButtonHighlightBackground;
                }
                else
                {
                    SearchButton.Background = Brushes.Transparent;
                }
            }
            base.OnPropertyChanged(e);
        }

        void RegisterLayoutCheckedEvents()
        {
            OrderLayout1Button.Checked += new RoutedEventHandler(orderlayout1_button_Checked);
            OrderLayout1Button.Unchecked += new RoutedEventHandler(orderlayout1_button_Unchecked);
            OrderLayout2Button.Checked += new RoutedEventHandler(orderlayout2_button_Checked);
            OrderLayout2Button.Unchecked += new RoutedEventHandler(orderlayout2_button_Unchecked);
            OrderLayout3Button.Checked += new RoutedEventHandler(orderlayout3_button_Checked);
            OrderLayout3Button.Unchecked += new RoutedEventHandler(orderlayout3_button_Unchecked);
        }

        void UnRegisterLayoutCheckedEvents()
        {
            OrderLayout1Button.Checked -= new RoutedEventHandler(orderlayout1_button_Checked);
            OrderLayout1Button.Unchecked -= new RoutedEventHandler(orderlayout1_button_Unchecked);
            OrderLayout2Button.Checked -= new RoutedEventHandler(orderlayout2_button_Checked);
            OrderLayout2Button.Unchecked -= new RoutedEventHandler(orderlayout2_button_Unchecked);
            OrderLayout3Button.Checked -= new RoutedEventHandler(orderlayout3_button_Checked);
            OrderLayout3Button.Unchecked -= new RoutedEventHandler(orderlayout3_button_Unchecked);
        }

        public ISubMenuPanelBreadCrumbItem CreateBreadCrumbItem(ISubMenuPanelBase parent_panel, string text, ISubMenuPanelMenuItem menu_item)
        {
            if(WithCustomBreadCrumbItemGeneration && RequestBreadCrumbItemCreation != null)
            {
                CreateSubMenuBreadCrumbItemEventArgs event_args = new CreateSubMenuBreadCrumbItemEventArgs(parent_panel, text, menu_item);
                RequestBreadCrumbItemCreation(this, event_args);

                if (event_args.CreatedItem != null)
                    return event_args.CreatedItem;
            }

            return OnCreateBreadCrumbItem(parent_panel, text, menu_item);
        }

        protected virtual ISubMenuPanelBreadCrumbItem OnCreateBreadCrumbItem(ISubMenuPanelBase parent_panel, string text, ISubMenuPanelMenuItem menu_item)
        {
            return new SubMenuPanelBreadCrumbItem(parent_panel, text, menu_item);
        }

        public bool WithCustomBreadCrumbItemGeneration
        {
            get
            {
                return _with_custom_breadcrumb_item_creation;    
            }
            set
            {
                if(_with_custom_breadcrumb_item_creation != value)
                {
                    _with_custom_breadcrumb_item_creation = value;
                    RecreateBreadCrumbItems();
                }
            }
        }
        
        void RecreateBreadCrumbItems()
        {
            List<ISubMenuPanelBreadCrumbItem> change_list = _breadcrumb_path.ToList();

            foreach (ISubMenuPanelBreadCrumbItem current_item in change_list)
            {
                ISubMenuPanelBreadCrumbItem newItem = CreateBreadCrumbItem(current_item.ParentPanel, current_item.Text, current_item.MenuItem);
                newItem.IsLastItem = current_item.IsLastItem;

                current_item.Click -= BreadCrumbButton_Click;
                newItem.Click += new RoutedEventHandler(BreadCrumbButton_Click);
                int idx = _breadcrumb_path.IndexOf(current_item);

                _breadcrumb_path.Remove(current_item);
                _breadcrumb_path.Insert(idx, newItem);

                if (_root_bread_crumb_item == current_item)
                    _root_bread_crumb_item = newItem;
            }
        }

        void UpdateBreadCrumbVisibility()
        {
            List<ISubMenuPanelBreadCrumbItem> rem_items = new List<ISubMenuPanelBreadCrumbItem>();
            bool found_current_item = false;

            foreach (ISubMenuPanelBreadCrumbItem check_item in _breadcrumb_path)
            {
                if (check_item == CurrentBreadCrumbItem)
                {
                    found_current_item = true;
                }
                else if (found_current_item)
                {
                    rem_items.Add(check_item);
                }
            }

            foreach (ISubMenuPanelBreadCrumbItem rem_item in rem_items)
            {
                rem_item.Click -= BreadCrumbButton_Click;
                _breadcrumb_path.Remove(rem_item);
            }

            if (CurrentBreadCrumbItem == _root_bread_crumb_item || _breadcrumb_path.Count <= 1)
            {
                LbBreadCrumb.Visibility = Visibility.Collapsed;
            }
            else
            {
                LbBreadCrumb.Visibility = Visibility.Visible;
            }

            for (int i = 0; i < _breadcrumb_path.Count; i++)
            {
                if (i == _breadcrumb_path.Count - 1)
                    _breadcrumb_path[i].IsLastItem = true;
                else
                    _breadcrumb_path[i].IsLastItem = false;
            }
        }

        void Menu_Unloaded(object sender, RoutedEventArgs e)
        {
            ActiveStyleAdornerManager.Instance.DeRegister(ArrowleftButton);
            ActiveStyleAdornerManager.Instance.DeRegister(ArrowrightButton);
            ActiveStyleAdornerManager.Instance.DeRegister(ArrowupButton);
            ActiveStyleAdornerManager.Instance.DeRegister(ArrowdownButton);
            ActiveStyleAdornerManager.Instance.DeRegister(OrderLayout1Button);
            ActiveStyleAdornerManager.Instance.DeRegister(OrderLayout2Button);
            ActiveStyleAdornerManager.Instance.DeRegister(OrderLayout3Button);
            ActiveStyleAdornerManager.Instance.DeRegister(PinButton);
            ActiveStyleAdornerManager.Instance.DeRegister(NewelemButton);
            ActiveStyleAdornerManager.Instance.DeRegister(CustomButton);
            ActiveStyleAdornerManager.Instance.DeRegister(CustomButton1);
            ActiveStyleAdornerManager.Instance.DeRegister(SearchButton);

            this.MouseLeave -= _animating_tile_panel_MouseLeave;
            this.MouseEnter -= _animating_tile_panel_MouseEnter;
            this.MouseMove -= _animating_tile_panel_MouseMove;
            this.SizeChanged -= SubMenuPanel_SizeChanged;

            this.Height = 0;

            if (Window != null)
                Window.SizeChanged -= Window_SizeChanged;

            ClearFoundElements();
        }

        void Menu_Loaded(object sender, RoutedEventArgs e)
        {
            if (_root_bread_crumb_item == null)
            {
                _root_bread_crumb_item = CreateBreadCrumbItem(this, _taskbar_item_name, null);
                _root_bread_crumb_item.Click += new RoutedEventHandler(BreadCrumbButton_Click);

                CurrentBreadCrumbItem = _root_bread_crumb_item;
                LbBreadCrumb.ItemsSource = _breadcrumb_path;

                _breadcrumb_path.Add(_root_bread_crumb_item);
            }

            UpdateBreadCrumbVisibility();

            ActiveStyleAdornerManager.Instance.Register(ArrowleftButton, BaseFrame.AdornerActiveRenderBrush, BaseFrame.AdornerActiveRenderPen);
            ActiveStyleAdornerManager.Instance.Register(ArrowrightButton, BaseFrame.AdornerActiveRenderBrush, BaseFrame.AdornerActiveRenderPen);
            ActiveStyleAdornerManager.Instance.Register(ArrowupButton, BaseFrame.AdornerActiveRenderBrush, BaseFrame.AdornerActiveRenderPen);
            ActiveStyleAdornerManager.Instance.Register(ArrowdownButton, BaseFrame.AdornerActiveRenderBrush, BaseFrame.AdornerActiveRenderPen);
            ActiveStyleAdornerManager.Instance.Register(OrderLayout1Button, BaseFrame.AdornerActiveRenderBrush, BaseFrame.AdornerActiveRenderPen);
            ActiveStyleAdornerManager.Instance.Register(OrderLayout2Button, BaseFrame.AdornerActiveRenderBrush, BaseFrame.AdornerActiveRenderPen);
            ActiveStyleAdornerManager.Instance.Register(OrderLayout3Button, BaseFrame.AdornerActiveRenderBrush, BaseFrame.AdornerActiveRenderPen);
            ActiveStyleAdornerManager.Instance.Register(PinButton, BaseFrame.AdornerActiveRenderBrush, BaseFrame.AdornerActiveRenderPen);
            ActiveStyleAdornerManager.Instance.Register(NewelemButton, BaseFrame.AdornerActiveRenderBrush, BaseFrame.AdornerActiveRenderPen);
            ActiveStyleAdornerManager.Instance.Register(CustomButton, BaseFrame.AdornerActiveRenderBrush, BaseFrame.AdornerActiveRenderPen);
            ActiveStyleAdornerManager.Instance.Register(CustomButton1, BaseFrame.AdornerActiveRenderBrush, BaseFrame.AdornerActiveRenderPen);
            ActiveStyleAdornerManager.Instance.Register(SearchButton, BaseFrame.AdornerActiveRenderBrush, BaseFrame.AdornerActiveRenderPen);

            _animating_tile_panel = UIHelper.FindVisualChildByType<AnimatingTilePanel>(AnimatingTilePanelItemsControl);

            if (_animating_tile_panel == null && AnimatingTilePanelItemsControl.IsLoaded == false)
            {
                AnimatingTilePanelItemsControl.Loaded += new RoutedEventHandler(animating_tile_panel_items_control_Loaded);
                return;
            }
            else if (_animating_tile_panel == null)
                return;

            InitControlProperties();
        }
        /// <summary>
        /// Diese Methode löschte alle Referenzen der Elemente, die man aus dem geladenen XAML-File referenziert hat.
        /// </summary>
        private void ClearFoundElements()
        {
            _size_change_rectangle = null;
            _base_grid = null;
            _lb_bread_crumb = null;
            _arrow_left_button = null;
            _arrow_right_button = null;
            _arrow_up_button = null;
            _arrow_down_button = null;
            _order_layout1_button = null;
            _order_layout2_button = null;
            _order_layout3_button = null;
            _pin_button = null;
            _newelem_button = null;
            _custom_button1 = null;
            _template_button = null;
            _custom_button = null;
            _search_button = null;
            _animating_tile_panel_items_control = null;
            _submenu_base_grid = null;
            _submenu_base_canvas = null;
            _sub_bread_crumb_stack = null;
            _sub_header_stack = null;
            _submenu_items_datagrid = null;
            _submenu_border = null;
            _search_cbx = null;
            _newelem_rect = null;
            _search_rect = null;
            _custom_rect1 = null;
            _custom_rect = null;
            _comm_left = null;
            _comm_right = null;
        }

        private void InitControlProperties()
        {
            if (_animating_tile_panel == null)
                _animating_tile_panel = UIHelper.FindVisualChildByType<AnimatingTilePanel>(AnimatingTilePanelItemsControl);

            if (_animating_tile_panel == null)
                return;

            this.MouseLeave += new MouseEventHandler(_animating_tile_panel_MouseLeave);
            this.MouseEnter += new MouseEventHandler(_animating_tile_panel_MouseEnter);
            this.MouseMove += new MouseEventHandler(_animating_tile_panel_MouseMove);

            this.SizeChanged += new SizeChangedEventHandler(SubMenuPanel_SizeChanged);

            if (Window != null)
                Window.SizeChanged += Window_SizeChanged;

            this.Height = double.NaN;

            if (_map_zp != null)
            {
                StartFadeIn();
                return;
            }

            _map_zp = new MapZP(SubmenuBaseGrid, SubmenuBaseCanvas);

            _map_zp.DeRegisterEventHandler();
            _map_zp.WithBorderCheckWhileTranslationActive = false;
            _map_zp.WithBorderCheck = true;
            _map_zp.TranslationAnimationStarted += new EventHandler(_map_zp_TranslationAnimationStarted);
            _map_zp.TranslationAnimationCompleted += new EventHandler(_map_zp_TranslationAnimationCompleted);
            _map_zp.ZoomAnimationCompleted += new EventHandler(_map_zp_ZoomAnimationCompleted);

            _map_zp.BorderMaxZoom = 1;
            _map_zp.BorderMinZoom = 1;
            _map_zp.BorderCheckerHeight = PanelHeight - SubHeaderStack.Height;
            _map_zp.BorderCheckerWidth = BigItemSize.Width * _items.Count;

            _scale_x_reset = new DoubleAnimation(1, new Duration(new TimeSpan(0, 0, 0, 0, 500))) { DecelerationRatio = 1 };
            _scale_y_reset = new DoubleAnimation(1, new Duration(new TimeSpan(0, 0, 0, 0, 500))) { DecelerationRatio = 1 };
            _transl_x_reset = new DoubleAnimation(0, new Duration(new TimeSpan(0, 0, 0, 0, 500))) { DecelerationRatio = 1 };
            _transl_y_reset = new DoubleAnimation(0, new Duration(new TimeSpan(0, 0, 0, 0, 500))) { DecelerationRatio = 1 };

            _scale_x_reset.Completed += new EventHandler(_scale_reset_x_Completed);
            _scale_y_reset.Completed += new EventHandler(_scale_reset_y_Completed);
            _transl_x_reset.Completed += new EventHandler(_transl_reset_x_Completed);
            _transl_y_reset.Completed += new EventHandler(_transl_reset_y_Completed);


            BigItemSize = BigItemSize;
            SmallItemSize = SmallItemSize;
            AnimatingTilePanelItemsControl.DataContext = _items_filtered;
            SubmenuItemsDatagrid.DataContext = _items_filtered;

            //_animating_tile_panel.SetBinding(AnimatingTilePanel.ItemHeightProperty, (string)null);
            //_animating_tile_panel.SetBinding(AnimatingTilePanel.ItemWidthProperty, (string)null);
            Binding height_binding = new Binding("ActualHeight");
            height_binding.Source = SubmenuBaseCanvas;
            // Edit AlexO: Ich hab einen Konverter eingebunden, weil wenn die ItemHeightProperty auf 0 gesetzt wird, wirft das _animating_tile_panel eine BindingException
            // Statt null gibt der Konverter einen sehr kleinen Wert zurück
            height_binding.Converter = new DoubleNotNullConverter();
            if (_animating_tile_panel != null)
            {
                _animating_tile_panel.SetBinding(AnimatingTilePanel.ItemHeightProperty, height_binding);
                _animating_tile_panel.ItemWidth = BigItemSize.Width;
            }

            RegisterLayoutCheckedEvents();

            StartFadeIn();

            //if(BaseFrame != null)
            //    BaseFrame.KeyUp += new KeyEventHandler(BaseFrame_KeyUp);
        }


        void animating_tile_panel_items_control_Loaded(object sender, RoutedEventArgs e)
        {
            AnimatingTilePanelItemsControl.Loaded -= animating_tile_panel_items_control_Loaded;
            InitControlProperties();
        }

        void SubMenuPanel_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            UpdateMapZPBorders();
        }

        void _map_zp_ZoomAnimationCompleted(object sender, EventArgs e)
        {
            UpdateMapZPBorders();
        }

        void BaseFrame_KeyUp(object sender, KeyEventArgs e)
        {
            //KeyStates state_m = Keyboard.GetKeyStates(Key.M);
            //KeyStates state_plus = Keyboard.GetKeyStates(Key.OemPlus);
            //KeyStates state_minus = Keyboard.GetKeyStates(Key.OemMinus);

            //KeyStates state_num_plus = Keyboard.GetKeyStates(Key.Add);          // num-block +
            //KeyStates state_num_minus = Keyboard.GetKeyStates(Key.Subtract);    // num-block -

            //bool m_down = (state_m & KeyStates.Down) > 0;
            //bool plus_down = (state_plus & KeyStates.Down) > 0 || (state_num_plus & KeyStates.Down) > 0;
            //bool minus_down = (state_minus & KeyStates.Down) > 0 || (state_num_minus & KeyStates.Down) > 0;

            //m_down = true;
            //plus_down = true;
            //minus_down = true;
            //if (!plus_down && !minus_down)
            //    return;

            //if (m_down)
            //{
            //    int i = 0;
            //    bool found_focused = false;
            //    for (i = 0; i < Items.Count; i++)
            //    {
            //        if (Items[i].IsFocused)
            //            found_focused = true;
            //    }

            //    if (!found_focused)
            //        i = 0;

            //    if (plus_down)
            //    {
            //        if (i < Items.Count - 1)
            //        {
            //            Items[i + 1].Focus();
            //        }
            //    }
            //    else if (minus_down)
            //    {
            //        if (i > 0)
            //            Items[i - 1].Focus();
            //    }
            //    else
            //        return;
            //}
            //else
            //{
            //    return;
            //}
        }

        void SubMenuPanel_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Left && _active_layout_mode == LayoutMode.Line)
            {
                //KeyLeftClicked();
                //arrowleft_button_Click(arrowleft_button, null);
            }
            else if (e.Key == Key.Right && _active_layout_mode == LayoutMode.Line)
            {
                //KeyRightClicked();
                //arrowright_button_Click(arrowright_button, null);
            }
            else if (e.Key == Key.Down && _active_layout_mode != LayoutMode.Grid)
            {
                arrowdown_button_Click(ArrowdownButton, null);
            }
            else if (e.Key == Key.Up && _active_layout_mode != LayoutMode.Grid)
            {
                arrowup_button_Click(ArrowupButton, null);
            }

            e.Handled = true;
        }

        #region KeyNavigation
        private void KeyLeftClicked()
        {
            SU selected_item = FindSelectedMenuItem();

            if (selected_item == null && _items_filtered.Count > 0)
                _items_filtered[_items_filtered.Count - 1].IsSelected = true;
            else if (selected_item != null)
            {
                int index_of_element = _items_filtered.IndexOf(selected_item);
                if (index_of_element != -1)
                {
                    selected_item.IsSelected = false;
                    if (index_of_element > 0)
                        //_items_filtered[index_of_element - 1].IsSelected = true;
                        _items_filtered[index_of_element - 1].Focus();
                    else
                        //_items_filtered[_items_filtered.Count - 1].IsSelected = true;
                        _items_filtered[_items_filtered.Count - 1].Focus();

                }
            }

            MoveSelectedItemIntoView();
        }

        private void KeyRightClicked()
        {
            SU selected_item = FindSelectedMenuItem();

            if (selected_item == null && _items_filtered.Count > 0)
            {
                _items_filtered[0].IsSelected = true;
            }
            else if (selected_item != null)
            {
                int index_of_element = _items_filtered.IndexOf(selected_item);
                if (index_of_element != -1)
                {
                    if (_items_filtered.Count - 1 > index_of_element)
                    {
                        selected_item.IsSelected = false;
                        //_items_filtered[index_of_element + 1].IsSelected = true;
                        _items_filtered[index_of_element + 1].Focus();
                    }
                    else
                    {
                        selected_item.IsSelected = false;
                        //_items_filtered[0].IsSelected = true;
                        _items_filtered[0].Focus();
                    }
                }
            }

            MoveSelectedItemIntoView();
        }

        private SU FindSelectedMenuItem()
        {
            SU selected_item = null;
            foreach (SU subMenuPanelMenuItem in _items_filtered)
            {
                if (selected_item == null && (subMenuPanelMenuItem.IsSelected || subMenuPanelMenuItem.ManualIsMouseOver))
                {
                    selected_item = subMenuPanelMenuItem;
                    subMenuPanelMenuItem.ManualIsMouseOver = false;
                    continue;
                }

                subMenuPanelMenuItem.ManualIsMouseOver = false;
                subMenuPanelMenuItem.IsSelected = false;
            }

            return selected_item;
        }

        private bool ContainsItem(SU search_menu_item)
        {
            if (_items.Contains(search_menu_item))
                return true;

            return RecursiveSearchItem(search_menu_item, _root_items.ToList());
        }

        private bool RecursiveSearchItem(SU search_menu_item, List<SU> search_list)
        {
            if (search_list == null)
                return false;

            if (search_list.Contains(search_menu_item))
                return true;

            foreach (SU check_item in search_list)
            {
                if (check_item.Children != null)
                {
                    if (RecursiveSearchItem(search_menu_item, check_item.Children.Cast<SU>().ToList()))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        private void MoveSelectedItemIntoView()
        {
            MoveItemIntoView(FindSelectedMenuItem());
        }

        private void MoveItemIntoView(SU item)
        {
            if (item == null)
                return;

            Point item_pos = UIHelper.GetElementPosition(item, this);

            float view_width = (float)SubmenuBaseGrid.ActualWidth;

            if (item_pos.X > view_width)
            {
                _map_zp.TranslateTransform.X -= item_pos.X;
            }
            else if (item_pos.X < 0)
            {
                _map_zp.TranslateTransform.X += Math.Abs(item_pos.X);
            }

            item.InvalidateArrange();
        }
        #endregion

        void SubMenuPanel_KeyDown(object sender, KeyEventArgs e)
        {
            //KeyStates state_m = Keyboard.GetKeyStates(Key.M);
            //KeyStates state_plus = Keyboard.GetKeyStates(Key.OemPlus);
            //KeyStates state_minus = Keyboard.GetKeyStates(Key.OemMinus);

            //KeyStates state_num_plus = Keyboard.GetKeyStates(Key.Add);          // num-block +
            //KeyStates state_num_minus = Keyboard.GetKeyStates(Key.Subtract);    // num-block -

            //bool m_down = (state_m & KeyStates.Down) > 0;
            //bool plus_down = (state_plus & KeyStates.Down) > 0 || (state_num_plus & KeyStates.Down) > 0;
            //bool minus_down = (state_minus & KeyStates.Down) > 0 || (state_num_minus & KeyStates.Down) > 0;

            //if(!plus_down && !minus_down)
            //    return;

            //if (m_down)
            //{            
            //    int i = 0;
            //    for(i=0; i < Items.Count; i++)
            //    {
            //        if(Items[i].IsFocused)
            //            break;
            //    }

            //    if (plus_down)
            //    {
            //        if(i < Items.Count-1)
            //        {
            //            Items[i + 1].Focus();
            //        }
            //    }
            //    else if (minus_down)
            //    {
            //        if(i >0)
            //            Items[i - 1].Focus();
            //    }
            //    else
            //        return;
            //}
            //else
            //{
            //    return;
            //}
        }
        #endregion

        #region FadeIn
        public void StartFadeIn()
        {
            if (!IsLoaded)
                return;

            BaseFrame.DisableMouseClicks();
            _animation_active = true;
            _submenu_fadein_da = new DoubleAnimation(0, PanelHeight, new Duration(new TimeSpan(0, 0, 0, 0, 500)));
            _submenu_fadein_da.AccelerationRatio = 0.5;
            _submenu_fadein_da.DecelerationRatio = 0.5;
            _submenu_fadein_da.Completed += new EventHandler(_submenu_fadein_Completed);
            SubmenuBorder.BeginAnimation(HeightProperty, _submenu_fadein_da, HandoffBehavior.Compose);

            CompositionTarget.Rendering += new EventHandler(CompositionTarget_Rendering);
            _map_zp.RegisterEventHandler();
        }
        #endregion

        public void StartFadeOut()
        {
            BaseFrame.DisableMouseClicks();
            _animation_active = true;
            _submenu_fadeout_da = new DoubleAnimation(0, new Duration(new TimeSpan(0, 0, 0, 0, 500)));
            _submenu_fadeout_da.AccelerationRatio = 0.5;
            _submenu_fadeout_da.DecelerationRatio = 0.5;
            _submenu_fadeout_da.Completed += new EventHandler(_submenu_fadeout_da_Completed);
            SubmenuBorder.BeginAnimation(HeightProperty, _submenu_fadeout_da, HandoffBehavior.Compose);

            CompositionTarget.Rendering -= CompositionTarget_Rendering;
            if (_map_zp != null)
                _map_zp.DeRegisterEventHandler();
        }
        
        #region Add and Remove Items
        public void AddItem(SU item)
        {
            if (item == null)
                throw new ArgumentNullException("item");

            if (item.Disposed)
                throw new ObjectDisposedException("item");

            FrameworkElement container = null;

            if (item.ItemTyp == SubMenuPanelMenuItem.SubMenuItemType.Button)
            {
                //Button b = new Button();
                TouchButton b = new TouchButton();
                FocusManager.SetFocusedElement(item, b);
                b.Style = (Style)ContentControl.FindResource("NavigationButton");
                UIElement old_child = item.Child;
                item.Child = null;
                b.Content = old_child;
                item.Child = b;
                b.Click += new RoutedEventHandler(b_Click);
                b.Tag = item;
                container = b;
                item.InitChildStore();
            }
            else if (item.ItemTyp == SubMenuPanelMenuItem.SubMenuItemType.ToggleButton)
            {
                ToggleButton t = new ToggleButton();
                FocusManager.SetFocusedElement(item, t);
                t.Style = (Style)ContentControl.FindResource("NavigationToggleButton");
                UIElement old_child = item.Child;
                item.Child = null;
                t.Content = old_child;
                item.Child = t;
                t.Tag = item;
                t.Checked += new RoutedEventHandler(t_Checked);
                t.Unchecked += new RoutedEventHandler(t_Unchecked);
                container = t;
                item.InitChildStore();
            }

            item.CheckRenderSize();

            //to send here the container as an argument is a hack
            //grund, mit try find parent hab ich das teil nicht gefunden
            //auch nicht nach dem loaded event
            item.OnMenuItemInitialized(container);
            //item.IsHitTestVisible = false;
            //item.Focusable = false;

            item.IsHitTestVisible = false;
            item.BaseFrame = BaseFrame;
            item.SubMenuPanel = this;

            _items.Add(item);

            item.ActionClicked += new EventHandler<SubMenuPanelMenuItem.MenuItemActionEventArgs>(item_ActionClicked);
            item.GotKeyboardFocus += new KeyboardFocusChangedEventHandler(item_GotKeyboardFocus);
            item.LostKeyboardFocus += new KeyboardFocusChangedEventHandler(item_LostKeyboardFocus);
            //_raw_elems.Add(item);
            if (ItemCountFilterExpression == null)
            {
                ItemCount = ItemCount + 1;
            }
            else
            {
                // if a IteCountFilterExpression has been set
                // check if the current item 
                if (ItemCountFilterExpression(item))
                {
                    ItemCount = ItemCount + 1;
                }
            }

            item._disable_child_update = true;
            if (ShowActionsByLeftMouse)
                item.HideActionMenu = true;
            else
                item.HideActionMenu = false;
            item._disable_child_update = false;

            GenerateDataGridColumns();

            AdjustCustomFilters(item);

            if (!WithSearchField || string.IsNullOrEmpty(SearchCbx.Text) || IsFiltered(item))
            {
                _items_filtered.Add(item);
                _items_filtered.Sort((x, y) => x.CompareTo(y));
            }

            if (ItemAdded != null)
                ItemAdded(this, new SubMenuPanelMenuItem.MenuItemManagementEventArgs(item));

            UpdateMapZPBorders();
        }

        public void RemoveItem(FrameworkElement item)
        {
            SU m_item = _items.Where(i => i == item).SingleOrDefault();

            if (m_item == null)
                return;

            ResetItemState(ref m_item);

            m_item.OnMenuItemRemoving();

            _items.Remove(m_item);

            m_item.ActionClicked -= item_ActionClicked;
            m_item.GotKeyboardFocus -= item_GotKeyboardFocus;
            m_item.LostKeyboardFocus -= item_LostKeyboardFocus;

            if (ItemCountFilterExpression == null)
            {
                ItemCount = ItemCount - 1;
            }
            else
            {
                // if a IteCountFilterExpression has been set
                // check if the current item 
                if (ItemCountFilterExpression(m_item))
                {
                    ItemCount = ItemCount + 1;
                }
            }


            if (m_item.ItemTyp == SubMenuPanelMenuItem.SubMenuItemType.Button)
            {
                if (m_item.Child is Button)
                {
                    ((Button)m_item.Child).Click -= b_Click;
                    UIElement old_child = ((Button)m_item.Child).Content as UIElement;

                    BindingOperations.ClearAllBindings(((Button)m_item.Child));
                    ((Button)m_item.Child).Content = null;
                    m_item.Child = old_child;
                    m_item.ResetChildStore();
                }
            }
            else if (m_item.ItemTyp == SubMenuPanelMenuItem.SubMenuItemType.ToggleButton)
            {
                if (m_item.Child is ToggleButton)
                {
                    ((ToggleButton)m_item.Child).Checked -= t_Checked;
                    ((ToggleButton)m_item.Child).Unchecked -= t_Unchecked;

                    UIElement old_child = ((ToggleButton)m_item.Child).Content as UIElement;
                    BindingOperations.ClearAllBindings(((ToggleButton)m_item.Child));
                    ((ToggleButton)m_item.Child).Content = null;
                    m_item.Child = old_child;
                    m_item.ResetChildStore();


                }
            }


            if (_items_filtered.Contains(m_item))
                _items_filtered.Remove(m_item);

            if (ItemRemoved != null)
                ItemRemoved(this, new SubMenuPanelMenuItem.MenuItemManagementEventArgs(m_item));

            //m_item.Dispose();
        }

        private void ResetItemState(ref SU item)
        {
            item.ManualIsMouseOver = false;
            item.IsActive = false;
            item.IsSelected = false;
            item.HideActionMenu = false;
        }
        public void RemoveAllItemsOfCurrentBreadcrumbLayer()
        {
            _custom_filter_values.Clear();
            SearchCbx.SelectedValue = null;
            SearchCbx.ItemsSource = null;

            List<SU> copy_list = new List<SU>();
            copy_list.AddRange(_items);

            foreach (SU rem_item in copy_list)
                RemoveItem(rem_item);

            ItemCount = 0;
        }

        public void RemoveAllItems()
        {
            _ignore_collection_change = true;
            RemoveAllItemsOfCurrentBreadcrumbLayer();

            List<SU> copy_list = new List<SU>();
            copy_list.AddRange(_root_items);

            foreach (SU rem_item in copy_list)
            {
                rem_item.Dispose();
            }

            _root_items.Clear();
            CurrentBreadCrumbItem = _root_bread_crumb_item;
            UpdateBreadCrumbVisibility();
            _ignore_collection_change = false;
        }

        private bool IsFiltered(SU item)
        {
            if (SearchCbx.SelectedItem != null)
            {
                return item.ApplyCustomFilter(SearchCbx.SelectedItem as string);
            }
            else
            {
                string[] split_filter = SearchCbx.Text.Split(" ".ToCharArray());
                string lower_item_string = item.ToString().ToLower();
                foreach (string filter_i in split_filter)
                {
                    string lower_filter_i = filter_i.ToLower();
                    if (lower_item_string.IndexOf(lower_filter_i) >= 0)
                        return true;
                }
            }

            return false;
        }

        public void ResetHoverElementState()
        {
            _mouse_down = false;
            if (_last_hover_element != null)
                _last_hover_element.ManualIsMouseOver = false;
        }

        private void AdjustCustomFilters(SU item)
        {
            List<string> item_custom_filter = item.GetCustomFilters();
            if (item_custom_filter != null && item_custom_filter.Count > 0)
            {
                _custom_filter_values = _custom_filter_values.Union(item.GetCustomFilters()).ToList();
                SearchCbx.SelectedValue = null;
                SearchCbx.ItemsSource = _custom_filter_values;
            }
        }
        #endregion

        #region GetElement
        public FrameworkElement GetItemByElement(FrameworkElement elem)
        {
            var query = from view in _items
                        where view == elem
                        select view;

            if (query.Count() == 1)
                return query.First();

            return null;
        }

        public SU FindMenuItemByKey(string key)
        {
            SU item = _items.Where(i => i.Key.Equals(key)).SingleOrDefault();
            return item;
        }
        #endregion


        void CompositionTarget_Rendering(object sender, EventArgs e)
        {
            _map_zp.UpdateMap();
        }


        #region Layout Switching
        private void SwitchToLine()
        {
            _autohide_timer_after_action = false;
            _ignore_autohide_after_action = true; // nach der animation wird nicht der auto-hide timer ausgelöst sollte sich die mouse ausserhalb befinden

            SubmenuItemsDatagrid.Visibility = Visibility.Collapsed;
            SubmenuBaseGrid.Visibility = Visibility.Visible;

            _animating_tile_panel.SetBinding(AnimatingTilePanel.ItemHeightProperty, (string)null);
            _animating_tile_panel.SetBinding(AnimatingTilePanel.ItemWidthProperty, (string)null);

            if (SubmenuBaseCanvas.ActualHeight > 0)
            {
                DoubleAnimation upsize_itemheight_da = new DoubleAnimation(BigItemSize.Height, new Duration(new TimeSpan(0, 0, 0, 0, 500)));
                upsize_itemheight_da.DecelerationRatio = 1;
                _animating_tile_panel.BeginAnimation(AnimatingTilePanel.ItemHeightProperty, upsize_itemheight_da);

                DoubleAnimation upsize_itemwidth_da = new DoubleAnimation(BigItemSize.Width, new Duration(new TimeSpan(0, 0, 0, 0, 500)));
                upsize_itemwidth_da.DecelerationRatio = 1;
                _animating_tile_panel.BeginAnimation(AnimatingTilePanel.ItemWidthProperty, upsize_itemwidth_da);

                UpdateAndAnimatePanelToHeight(PanelHeight);

                DoubleAnimation rescale_da = new DoubleAnimation(SmallItemSize.Width, BigItemSize.Width, new Duration(new TimeSpan(0, 0, 0, 0, 500)));
                rescale_da.DecelerationRatio = 1;
                foreach (Viewbox view_box in _items)
                    view_box.BeginAnimation(WidthProperty, rescale_da);
            }
            else
            {
                _animating_tile_panel.SetBinding(AnimatingTilePanel.ItemHeightProperty, new Binding("ActualHeight") { Source = SubmenuBaseCanvas });
                _animating_tile_panel.ItemWidth = BigItemSize.Width;
            }


            _animating_tile_panel.SetBinding(WidthProperty, (string)null);
            _animating_tile_panel.Width = BigItemSize.Width * _items.Count;

            UpdateLineMapZP();

            SubmenuBaseCanvas.Height = double.NaN;

            ResetMapZp();
            EnbaleLeftRightArrows();
            UpdateMapZPBorders();
        }

        private void SwitchToKachel()
        {
            _autohide_timer_after_action = false;
            _ignore_autohide_after_action = true; // nach der animation wird nicht der auto-hide timer ausgelöst sollte sich die mouse ausserhalb befinden

            SubmenuItemsDatagrid.Visibility = Visibility.Collapsed;
            SubmenuBaseGrid.Visibility = Visibility.Visible;

            _animating_tile_panel.SetBinding(AnimatingTilePanel.ItemHeightProperty, (string)null);
            _animating_tile_panel.SetBinding(AnimatingTilePanel.ItemWidthProperty, (string)null);

            if (SubmenuBaseCanvas.ActualHeight > 0)
            {
                DoubleAnimation downsize_itemheight_da = new DoubleAnimation(SmallItemSize.Height, new Duration(new TimeSpan(0, 0, 0, 0, 500)));
                downsize_itemheight_da.DecelerationRatio = 1;
                _animating_tile_panel.BeginAnimation(AnimatingTilePanel.ItemHeightProperty, downsize_itemheight_da);

                DoubleAnimation downsize_itemwidth_da = new DoubleAnimation(SmallItemSize.Width, new Duration(new TimeSpan(0, 0, 0, 0, 500)));
                downsize_itemwidth_da.DecelerationRatio = 1;
                _animating_tile_panel.BeginAnimation(AnimatingTilePanel.ItemWidthProperty, downsize_itemwidth_da);

                if (SubmenuBorder.ActualHeight < PanelHeight)
                    UpdateAndAnimatePanelToHeight(PanelHeight);

                DoubleAnimation rescale_da = new DoubleAnimation(BigItemSize.Width, SmallItemSize.Width, new Duration(new TimeSpan(0, 0, 0, 0, 500)));
                rescale_da.DecelerationRatio = 1;
                foreach (Viewbox view_box in _items)
                    view_box.BeginAnimation(WidthProperty, rescale_da);
            }
            else
            {
                _animating_tile_panel.ItemHeight = SmallItemSize.Height * 3;
                _animating_tile_panel.ItemWidth = SmallItemSize.Width;
            }

            _animating_tile_panel.SetBinding(WidthProperty, new Binding("ActualWidth") { Source = SubmenuBaseCanvas });


            int items_per_row = (int)Math.Floor(SubmenuBaseCanvas.ActualWidth / SmallItemSize.Width);
            int rows = (int)Math.Ceiling((double)_items_filtered.Count / (double)items_per_row);
            double panel_canvas_height = (SmallItemSize.Height * rows);
            SubmenuBaseCanvas.Height = panel_canvas_height;

            UpdateKachelMapZP();

            ResetMapZp();
            EnableUpDownArrows();
            UpdateMapZPBorders();
        }

        private void SwitchToGrid()
        {
            EnableUpDownArrows();
            // Grid disabled
            //return;

            SubmenuItemsDatagrid.Visibility = Visibility.Visible;
            if (SubmenuItemsDatagrid.Items.Count > 0)
            {
                if (SubmenuItemsDatagrid.SelectedItems.Count == 0)
                    SubmenuItemsDatagrid.SelectedIndex = 0;
            }
            SubmenuBaseGrid.Visibility = Visibility.Collapsed;

            _autohide_timer_after_action = false;
            _ignore_autohide_after_action = true; // nach der animation wird nicht der auto-hide timer ausgelöst sollte sich die mouse ausserhalb befinden

            UpdateAndAnimatePanelToHeight(PanelHeight);
        }

        private void GenerateDataGridColumns()
        {
            if (_grid_columns_generated)
                return;

            if (_items != null && _items.Count > 0)
            {
                SU newest_item = _items[_items.Count - 1];

                List<SubMenuPanelMenuItem.MenuItemAttribute> attribs = newest_item.GetAttributes();

                if (attribs != null && attribs.Count > 0)
                {
                    int nColCount = 0;
                    foreach (SubMenuPanelMenuItem.MenuItemAttribute cur_attribute in attribs)
                    {
                        DataGridTemplateColumn template_col = null;
                        //if(nColCount == 0)
                        //{
                        //    template_col = GenerateLinkTemplateColumn(cur_attribute);
                        //}
                        //else
                        //{
                        //    template_col = GenerateTextTemplateColumn(cur_attribute);    
                        //}

                        if (cur_attribute.Type == SubMenuPanelMenuItem.MenuItemAttributeType.LinkButton)
                        {
                            template_col = GenerateLinkTemplateColumn(cur_attribute);
                        }
                        else if (cur_attribute.Type == SubMenuPanelMenuItem.MenuItemAttributeType.Button)
                        {
                            template_col = GenerateButtonTemplateColumn(cur_attribute);
                        }
                        else if (cur_attribute.Type == SubMenuPanelMenuItem.MenuItemAttributeType.ToggleButton)
                        {
                            template_col = GenerateToggleButtonTemplateColumn(cur_attribute);
                        }
                        else if (cur_attribute.Type == SubMenuPanelMenuItem.MenuItemAttributeType.Label)
                        {
                            template_col = GenerateTextTemplateColumn(cur_attribute);
                        }
                        else if (cur_attribute.Type == SubMenuPanelMenuItem.MenuItemAttributeType.CheckBox)
                        {
                            template_col = GenerateCheckBoxTemplateColumn(cur_attribute);
                        }
                        else if (cur_attribute.Type == SubMenuPanelMenuItem.MenuItemAttributeType.TextBox)
                        {
                            template_col = GenerateTextBoxTemplateColumn(cur_attribute);
                        }
                        else if (cur_attribute.Type == SubMenuPanelMenuItem.MenuItemAttributeType.Image)
                        {
                            template_col = GenerateImageTemplateColumn(cur_attribute);
                        }
                        else if (cur_attribute.Type == SubMenuPanelMenuItem.MenuItemAttributeType.Icon)
                        {
                            template_col = GenerateIconTemplateColumn(cur_attribute);
                        }


                        if (template_col != null)
                            SubmenuItemsDatagrid.Columns.Add(template_col);

                        nColCount++;
                    }

                    _grid_columns_generated = true;

                    GenerateActionsColumns(newest_item);
                    //submenu_items_datagrid.Columns.Add(actions_col);
                    //submenu_items_datagrid.Items.Refresh();
                }
            }
        }

        private DataGridTemplateColumn GenerateLinkTemplateColumn(SubMenuPanelMenuItem.MenuItemAttribute attribute)
        {
            DataGridTemplateColumn template_col = new DataGridTemplateColumn();
            template_col.IsReadOnly = true;
            template_col.Header = attribute.Name;
            template_col.Header = attribute.ShowHeader ? attribute.Name : "";

            if (!double.IsNaN(attribute.MaxWidth))
                template_col.MaxWidth = attribute.MaxWidth;
            if (!double.IsNaN(attribute.MinWidth))
                template_col.MinWidth = attribute.MinWidth;
            if (!double.IsNaN(attribute.Width))
                template_col.Width = attribute.Width;
            //template_col.SortMemberPath = attribute_name;

            if (attribute.HorizontalAlignment == System.Windows.HorizontalAlignment.Right)
            {
                template_col.HeaderStyle = GetRightAlignColumnHeaderStyle();
            }
            else if (attribute.HorizontalAlignment == System.Windows.HorizontalAlignment.Center)
            {
                template_col.HeaderStyle = GetCenterAlignColumnHeaderStyle();
            }
            else
            {
                template_col.HeaderStyle = GetLeftAlignColumnHeaderStyle();
            }

            MultiBinding tb_content_binding = new MultiBinding();
            tb_content_binding.Converter = new SubMenuPanelGridValueConverter();
            tb_content_binding.ConverterParameter = attribute;
            tb_content_binding.Bindings.Add(new Binding());
            tb_content_binding.Bindings.Add(new Binding("TriggerGridUpdate"));

            Binding command_binding = new Binding();

            FrameworkElementFactory elementFactory = new FrameworkElementFactory(typeof(PanelGridButton));

            elementFactory.SetValue(Button.NameProperty, "standard_action_button");
            elementFactory.SetValue(Button.StyleProperty, (Style)ContentControl.FindResource("dg_link_button_style"));
            elementFactory.SetValue(Button.MarginProperty, new Thickness(0, 2, 0, 2));
            elementFactory.SetValue(Button.PaddingProperty, new Thickness(0));
            elementFactory.SetValue(Button.HorizontalContentAlignmentProperty, attribute.HorizontalAlignment);
            elementFactory.SetValue(Button.HorizontalAlignmentProperty, attribute.HorizontalAlignment);
            elementFactory.SetValue(TextElement.FontWeightProperty, FontWeights.Bold);

            elementFactory.SetValue(TagProperty, attribute);

            MultiBinding btn_enabled_binding = new MultiBinding();
            btn_enabled_binding.Converter = new SubMenuPanelGridActionEnabledConverter();
            btn_enabled_binding.ConverterParameter = attribute;
            btn_enabled_binding.Bindings.Add(new Binding());
            btn_enabled_binding.Bindings.Add(new Binding("TriggerGridUpdate"));

            MultiBinding btn_visibility_binding = new MultiBinding();
            btn_visibility_binding.Converter = new SubMenuPanelGridActionVisibilityConverter();
            btn_visibility_binding.ConverterParameter = attribute;
            btn_visibility_binding.Bindings.Add(new Binding());
            btn_visibility_binding.Bindings.Add(new Binding("TriggerGridUpdate"));

            elementFactory.SetBinding(IsEnabledProperty, btn_enabled_binding);
            elementFactory.SetBinding(VisibilityProperty, btn_visibility_binding);


            elementFactory.AddHandler(Button.ClickEvent, new RoutedEventHandler(datagrid_button_column_click));
            elementFactory.AddHandler(PanelGridButton.InternalKeyUpEvent, new KeyEventHandler(grid_button_KeyUp));
            elementFactory.AddHandler(PanelGridButton.InternalKeyDownEvent, new KeyEventHandler(grid_button_KeyDown));

            elementFactory.SetBinding(Button.ContentProperty, tb_content_binding);
            elementFactory.SetBinding(Button.CommandParameterProperty, command_binding);

            DataTemplate data_template = new DataTemplate();
            data_template.VisualTree = elementFactory;

            template_col.CellTemplate = data_template;

            return template_col;
        }

        private DataGridTemplateColumn GenerateButtonTemplateColumn(SubMenuPanelMenuItem.MenuItemAttribute attribute)
        {
            DataGridTemplateColumn template_col = new DataGridTemplateColumn();
            template_col.IsReadOnly = true;
            template_col.Header = attribute.Name;
            template_col.Header = attribute.ShowHeader ? attribute.Name : "";

            //template_col.SortMemberPath = attribute_name;

            if (attribute.HorizontalAlignment == System.Windows.HorizontalAlignment.Right)
            {
                template_col.HeaderStyle = GetRightAlignColumnHeaderStyle();
            }
            else if (attribute.HorizontalAlignment == System.Windows.HorizontalAlignment.Center)
            {
                template_col.HeaderStyle = GetCenterAlignColumnHeaderStyle();
            }
            else
            {
                template_col.HeaderStyle = GetLeftAlignColumnHeaderStyle();
            }

            MultiBinding tb_content_binding = new MultiBinding();
            tb_content_binding.Converter = new SubMenuPanelGridValueConverter();
            tb_content_binding.ConverterParameter = attribute;
            tb_content_binding.Bindings.Add(new Binding());
            tb_content_binding.Bindings.Add(new Binding("TriggerGridUpdate"));

            Binding command_binding = new Binding();

            FrameworkElementFactory elementFactory = new FrameworkElementFactory(typeof(PanelGridButton));

            elementFactory.SetValue(Button.NameProperty, "standard_action_button");
            elementFactory.SetValue(Button.StyleProperty, null);
            elementFactory.SetValue(Button.MarginProperty, new Thickness(2));
            elementFactory.SetValue(Button.PaddingProperty, new Thickness(0));
            elementFactory.SetValue(Button.HorizontalContentAlignmentProperty, attribute.HorizontalAlignment);
            elementFactory.SetValue(Button.HorizontalAlignmentProperty, attribute.HorizontalAlignment);
            //elementFactory.SetValue(TextElement.FontWeightProperty, FontWeights.Bold);

            elementFactory.SetValue(TagProperty, attribute);

            MultiBinding btn_enabled_binding = new MultiBinding();
            btn_enabled_binding.Converter = new SubMenuPanelGridActionEnabledConverter();
            btn_enabled_binding.ConverterParameter = attribute;
            btn_enabled_binding.Bindings.Add(new Binding());
            btn_enabled_binding.Bindings.Add(new Binding("TriggerGridUpdate"));

            MultiBinding btn_visibility_binding = new MultiBinding();
            btn_visibility_binding.Converter = new SubMenuPanelGridActionVisibilityConverter();
            btn_visibility_binding.ConverterParameter = attribute;
            btn_visibility_binding.Bindings.Add(new Binding());
            btn_visibility_binding.Bindings.Add(new Binding("TriggerGridUpdate"));

            elementFactory.SetBinding(IsEnabledProperty, btn_enabled_binding);
            elementFactory.SetBinding(VisibilityProperty, btn_visibility_binding);


            elementFactory.AddHandler(Button.ClickEvent, new RoutedEventHandler(datagrid_button_column_click));
            elementFactory.AddHandler(PanelGridButton.InternalKeyUpEvent, new KeyEventHandler(grid_button_KeyUp));
            elementFactory.AddHandler(PanelGridButton.InternalKeyDownEvent, new KeyEventHandler(grid_button_KeyDown));

            elementFactory.SetBinding(Button.ContentProperty, tb_content_binding);
            elementFactory.SetBinding(Button.CommandParameterProperty, command_binding);

            DataTemplate data_template = new DataTemplate();
            data_template.VisualTree = elementFactory;

            template_col.CellTemplate = data_template;

            return template_col;
        }

        private DataGridTemplateColumn GenerateToggleButtonTemplateColumn(SubMenuPanelMenuItem.MenuItemAttribute attribute)
        {
            DataGridTemplateColumn template_col = new DataGridTemplateColumn();
            template_col.IsReadOnly = true;
            template_col.Header = attribute.Name;
            template_col.Header = attribute.ShowHeader ? attribute.Name : "";

            //template_col.SortMemberPath = attribute_name;

            if (attribute.HorizontalAlignment == System.Windows.HorizontalAlignment.Right)
            {
                template_col.HeaderStyle = GetRightAlignColumnHeaderStyle();
            }
            else if (attribute.HorizontalAlignment == System.Windows.HorizontalAlignment.Center)
            {
                template_col.HeaderStyle = GetCenterAlignColumnHeaderStyle();
            }
            else
            {
                template_col.HeaderStyle = GetLeftAlignColumnHeaderStyle();
            }

            MultiBinding tb_content_binding = new MultiBinding();
            tb_content_binding.Converter = new SubMenuPanelGridValueConverter();
            tb_content_binding.ConverterParameter = attribute;
            tb_content_binding.Bindings.Add(new Binding());
            tb_content_binding.Bindings.Add(new Binding("TriggerGridUpdate"));

            Binding command_binding = new Binding();

            FrameworkElementFactory elementFactory = new FrameworkElementFactory(typeof(ToggleButton));

            elementFactory.SetValue(NameProperty, "toggle_action_button");
            elementFactory.SetValue(Button.StyleProperty, null);
            elementFactory.SetValue(MarginProperty, new Thickness(2));
            elementFactory.SetValue(PaddingProperty, new Thickness(0));
            elementFactory.SetValue(Button.HorizontalContentAlignmentProperty, attribute.HorizontalAlignment);
            elementFactory.SetValue(Button.HorizontalAlignmentProperty, attribute.HorizontalAlignment);

            elementFactory.SetValue(TagProperty, attribute);

            MultiBinding btn_enabled_binding = new MultiBinding();
            btn_enabled_binding.Converter = new SubMenuPanelGridActionEnabledConverter();
            btn_enabled_binding.ConverterParameter = attribute;
            btn_enabled_binding.Bindings.Add(new Binding());
            btn_enabled_binding.Bindings.Add(new Binding("TriggerGridUpdate"));

            MultiBinding btn_visibility_binding = new MultiBinding();
            btn_visibility_binding.Converter = new SubMenuPanelGridActionVisibilityConverter();
            btn_visibility_binding.ConverterParameter = attribute;
            btn_visibility_binding.Bindings.Add(new Binding());
            btn_visibility_binding.Bindings.Add(new Binding("TriggerGridUpdate"));

            elementFactory.SetBinding(IsEnabledProperty, btn_enabled_binding);
            elementFactory.SetBinding(VisibilityProperty, btn_visibility_binding);

            if (attribute.ValueBinding == null)
            {
                elementFactory.AddHandler(ToggleButton.ClickEvent, new RoutedEventHandler(datagrid_button_column_click));
            }
            else
            {
                elementFactory.SetBinding(ToggleButton.IsCheckedProperty, attribute.ValueBinding);
            }

            //elementFactory.AddHandler(PanelGridButton.InternalKeyUpEvent, new KeyEventHandler(grid_button_KeyUp));
            //elementFactory.AddHandler(PanelGridButton.InternalKeyDownEvent, new KeyEventHandler(grid_button_KeyDown));

            elementFactory.SetBinding(ContentProperty, tb_content_binding);
            elementFactory.SetBinding(ToggleButton.CommandParameterProperty, command_binding);

            DataTemplate data_template = new DataTemplate();
            data_template.VisualTree = elementFactory;

            template_col.CellTemplate = data_template;

            return template_col;
        }

        private DataGridTemplateColumn GenerateCheckBoxTemplateColumn(SubMenuPanelMenuItem.MenuItemAttribute attribute)
        {
            DataGridTemplateColumn template_col = new DataGridTemplateColumn();
            template_col.IsReadOnly = true;
            template_col.Header = attribute.Name;
            template_col.Header = attribute.ShowHeader ? attribute.Name : "";

            //template_col.SortMemberPath = attribute_name;

            if (attribute.HorizontalAlignment == System.Windows.HorizontalAlignment.Right)
            {
                template_col.HeaderStyle = GetRightAlignColumnHeaderStyle();
            }
            else if (attribute.HorizontalAlignment == System.Windows.HorizontalAlignment.Center)
            {
                template_col.HeaderStyle = GetCenterAlignColumnHeaderStyle();
            }
            else
            {
                template_col.HeaderStyle = GetLeftAlignColumnHeaderStyle();
            }

            //Binding tb_content_binding = new Binding();
            //tb_content_binding.Converter = new SubMenuPanelGridValueConverter();
            //tb_content_binding.ConverterParameter = attribute;

            Binding command_binding = new Binding();

            FrameworkElementFactory elementFactory = new FrameworkElementFactory(typeof(CheckBox));

            elementFactory.SetValue(NameProperty, "checkbox_action_button");
            elementFactory.SetValue(MarginProperty, new Thickness(2));
            elementFactory.SetValue(PaddingProperty, new Thickness(0));
            elementFactory.SetValue(HorizontalContentAlignmentProperty, attribute.HorizontalAlignment);
            elementFactory.SetValue(HorizontalAlignmentProperty, attribute.HorizontalAlignment);

            elementFactory.SetValue(TagProperty, attribute);

            MultiBinding btn_enabled_binding = new MultiBinding();
            btn_enabled_binding.Converter = new SubMenuPanelGridActionEnabledConverter();
            btn_enabled_binding.ConverterParameter = attribute;
            btn_enabled_binding.Bindings.Add(new Binding());
            btn_enabled_binding.Bindings.Add(new Binding("TriggerGridUpdate"));

            MultiBinding btn_visibility_binding = new MultiBinding();
            btn_visibility_binding.Converter = new SubMenuPanelGridActionVisibilityConverter();
            btn_visibility_binding.ConverterParameter = attribute;
            btn_visibility_binding.Bindings.Add(new Binding());
            btn_visibility_binding.Bindings.Add(new Binding("TriggerGridUpdate"));

            elementFactory.SetBinding(IsEnabledProperty, btn_enabled_binding);
            elementFactory.SetBinding(VisibilityProperty, btn_visibility_binding);

            if (attribute.ValueBinding == null)
            {
                elementFactory.AddHandler(CheckBox.ClickEvent, new RoutedEventHandler(datagrid_button_column_click));
            }
            else
            {
                elementFactory.SetBinding(CheckBox.IsCheckedProperty, attribute.ValueBinding);
            }

            //elementFactory.AddHandler(PanelGridButton.InternalKeyUpEvent, new KeyEventHandler(grid_button_KeyUp));
            //elementFactory.AddHandler(PanelGridButton.InternalKeyDownEvent, new KeyEventHandler(grid_button_KeyDown));

            //elementFactory.SetBinding(ContentProperty, tb_content_binding);
            elementFactory.SetBinding(CheckBox.CommandParameterProperty, command_binding);

            DataTemplate data_template = new DataTemplate();
            data_template.VisualTree = elementFactory;

            template_col.CellTemplate = data_template;

            return template_col;
        }
        private DataGridTemplateColumn GenerateTextTemplateColumn(SubMenuPanelMenuItem.MenuItemAttribute attribute)
        {
            DataGridTemplateColumn template_col = new DataGridTemplateColumn();
            template_col.IsReadOnly = true;
            template_col.Header = attribute.ShowHeader ? attribute.Name : "";

            if (!double.IsNaN(attribute.MaxWidth))
                template_col.MaxWidth = attribute.MaxWidth;
            if (!double.IsNaN(attribute.MinWidth))
                template_col.MinWidth = attribute.MinWidth;
            if (!double.IsNaN(attribute.Width))
                template_col.Width = attribute.Width;
            //template_col.SortMemberPath = attribute_name;

            if (attribute.HorizontalAlignment == System.Windows.HorizontalAlignment.Right)
            {
                template_col.HeaderStyle = GetRightAlignColumnHeaderStyle();
            }
            else if (attribute.HorizontalAlignment == System.Windows.HorizontalAlignment.Center)
            {
                template_col.HeaderStyle = GetCenterAlignColumnHeaderStyle();
            }
            else
            {
                template_col.HeaderStyle = GetLeftAlignColumnHeaderStyle();
            }

            MultiBinding tb_text_binding = new MultiBinding();
            tb_text_binding.Converter = new SubMenuPanelGridValueConverter();
            tb_text_binding.ConverterParameter = attribute;
            tb_text_binding.Bindings.Add(new Binding());
            tb_text_binding.Bindings.Add(new Binding("TriggerGridUpdate"));

            FrameworkElementFactory elementFactory = new FrameworkElementFactory(typeof(TextBlock));
            elementFactory.SetValue(TextBlock.MarginProperty, new Thickness(2, 2, 15, 2));
            if (attribute.HorizontalAlignment == System.Windows.HorizontalAlignment.Right)
            {
                elementFactory.SetValue(TextBlock.TextAlignmentProperty, TextAlignment.Right);
            }
            else if (attribute.HorizontalAlignment == System.Windows.HorizontalAlignment.Center)
            {
                elementFactory.SetValue(TextBlock.TextAlignmentProperty, TextAlignment.Center);
            }
            else
            {
                elementFactory.SetValue(TextBlock.TextAlignmentProperty, TextAlignment.Left);
            }

            elementFactory.SetValue(HorizontalAlignmentProperty, HorizontalAlignment.Stretch);

            if (attribute.ValueBinding == null)
            {
                elementFactory.SetBinding(TextBlock.TextProperty, tb_text_binding);
            }
            else
            {
                elementFactory.SetBinding(TextBlock.TextProperty, attribute.ValueBinding);
            }


            MultiBinding btn_enabled_binding = new MultiBinding();
            btn_enabled_binding.Converter = new SubMenuPanelGridActionEnabledConverter();
            btn_enabled_binding.ConverterParameter = attribute;
            btn_enabled_binding.Bindings.Add(new Binding());
            btn_enabled_binding.Bindings.Add(new Binding("TriggerGridUpdate"));

            MultiBinding btn_visibility_binding = new MultiBinding();
            btn_visibility_binding.Converter = new SubMenuPanelGridActionVisibilityConverter();
            btn_visibility_binding.ConverterParameter = attribute;
            btn_visibility_binding.Bindings.Add(new Binding());
            btn_visibility_binding.Bindings.Add(new Binding("TriggerGridUpdate"));

            elementFactory.SetBinding(IsEnabledProperty, btn_enabled_binding);
            elementFactory.SetBinding(VisibilityProperty, btn_visibility_binding);

            DataTemplate data_template = new DataTemplate();
            data_template.VisualTree = elementFactory;

            template_col.CellTemplate = data_template;

            return template_col;
        }

        private DataGridTemplateColumn GenerateImageTemplateColumn(SubMenuPanelMenuItem.MenuItemAttribute attribute)
        {
            DataGridTemplateColumn template_col = new DataGridTemplateColumn();
            template_col.IsReadOnly = true;
            template_col.Header = attribute.Name;
            template_col.Header = attribute.ShowHeader ? attribute.Name : "";

            //template_col.SortMemberPath = attribute_name;

            if (attribute.HorizontalAlignment == System.Windows.HorizontalAlignment.Right)
            {
                template_col.HeaderStyle = GetRightAlignColumnHeaderStyle();
            }
            else if (attribute.HorizontalAlignment == System.Windows.HorizontalAlignment.Center)
            {
                template_col.HeaderStyle = GetCenterAlignColumnHeaderStyle();
            }
            else
            {
                template_col.HeaderStyle = GetLeftAlignColumnHeaderStyle();
            }

            MultiBinding tb_content_binding = new MultiBinding();
            tb_content_binding.Converter = new SubMenuPanelGridValueConverter();
            tb_content_binding.ConverterParameter = attribute;
            tb_content_binding.Bindings.Add(new Binding());
            tb_content_binding.Bindings.Add(new Binding("TriggerGridUpdate"));

            Binding command_binding = new Binding();

            FrameworkElementFactory elementFactory = new FrameworkElementFactory(typeof(Image));

            elementFactory.SetValue(Image.NameProperty, "standard_image");
            elementFactory.SetValue(Image.StyleProperty, null);
            elementFactory.SetValue(Image.MarginProperty, new Thickness(2));
            elementFactory.SetValue(Image.HorizontalAlignmentProperty, attribute.HorizontalAlignment);
            //elementFactory.SetValue(TextElement.FontWeightProperty, FontWeights.Bold);

            elementFactory.SetValue(TagProperty, attribute);

            MultiBinding btn_enabled_binding = new MultiBinding();
            btn_enabled_binding.Converter = new SubMenuPanelGridActionEnabledConverter();
            btn_enabled_binding.ConverterParameter = attribute;
            btn_enabled_binding.Bindings.Add(new Binding());
            btn_enabled_binding.Bindings.Add(new Binding("TriggerGridUpdate"));

            MultiBinding btn_visibility_binding = new MultiBinding();
            btn_visibility_binding.Converter = new SubMenuPanelGridActionVisibilityConverter();
            btn_visibility_binding.ConverterParameter = attribute;
            btn_visibility_binding.Bindings.Add(new Binding());
            btn_visibility_binding.Bindings.Add(new Binding("TriggerGridUpdate"));

            elementFactory.SetBinding(IsEnabledProperty, btn_enabled_binding);
            elementFactory.SetBinding(VisibilityProperty, btn_visibility_binding);

            if (attribute.ValueBinding == null)
            {
                elementFactory.SetBinding(Image.SourceProperty, tb_content_binding);
            }
            else
            {
                elementFactory.SetBinding(Image.SourceProperty, attribute.ValueBinding);
            }

            DataTemplate data_template = new DataTemplate();
            data_template.VisualTree = elementFactory;

            template_col.CellTemplate = data_template;

            return template_col;
        }

        private DataGridTemplateColumn GenerateIconTemplateColumn(SubMenuPanelMenuItem.MenuItemAttribute attribute)
        {
            DataGridTemplateColumn template_col = new DataGridTemplateColumn();
            template_col.IsReadOnly = true;
            template_col.Header = attribute.Name;
            template_col.Header = attribute.ShowHeader ? attribute.Name : "";

            //template_col.SortMemberPath = attribute_name;

            if (attribute.HorizontalAlignment == System.Windows.HorizontalAlignment.Right)
            {
                template_col.HeaderStyle = GetRightAlignColumnHeaderStyle();
            }
            else if (attribute.HorizontalAlignment == System.Windows.HorizontalAlignment.Center)
            {
                template_col.HeaderStyle = GetCenterAlignColumnHeaderStyle();
            }
            else
            {
                template_col.HeaderStyle = GetLeftAlignColumnHeaderStyle();
            }

            MultiBinding tb_content_binding = new MultiBinding();
            tb_content_binding.Converter = new SubMenuPanelGridValueConverter();
            tb_content_binding.ConverterParameter = attribute;
            tb_content_binding.Bindings.Add(new Binding());
            tb_content_binding.Bindings.Add(new Binding("TriggerGridUpdate"));

            Binding command_binding = new Binding();

            FrameworkElementFactory elementFactory = new FrameworkElementFactory(typeof(Image));

            elementFactory.SetValue(Image.NameProperty, "standard_image");
            elementFactory.SetValue(Image.StyleProperty, null);
            elementFactory.SetValue(Image.MarginProperty, new Thickness(2));
            elementFactory.SetValue(Image.HorizontalAlignmentProperty, attribute.HorizontalAlignment);
            elementFactory.SetValue(Image.StretchProperty, Stretch.UniformToFill);
            elementFactory.SetValue(Image.WidthProperty, (double)16);
            elementFactory.SetValue(Image.HeightProperty, (double)16);
            //elementFactory.SetValue(TextElement.FontWeightProperty, FontWeights.Bold);

            elementFactory.SetValue(TagProperty, attribute);

            MultiBinding btn_enabled_binding = new MultiBinding();
            btn_enabled_binding.Converter = new SubMenuPanelGridActionEnabledConverter();
            btn_enabled_binding.ConverterParameter = attribute;
            btn_enabled_binding.Bindings.Add(new Binding());
            btn_enabled_binding.Bindings.Add(new Binding("TriggerGridUpdate"));

            MultiBinding btn_visibility_binding = new MultiBinding();
            btn_visibility_binding.Converter = new SubMenuPanelGridActionVisibilityConverter();
            btn_visibility_binding.ConverterParameter = attribute;
            btn_visibility_binding.Bindings.Add(new Binding());
            btn_visibility_binding.Bindings.Add(new Binding("TriggerGridUpdate"));

            elementFactory.SetBinding(IsEnabledProperty, btn_enabled_binding);
            elementFactory.SetBinding(VisibilityProperty, btn_visibility_binding);

            if (attribute.ValueBinding == null)
            {
                elementFactory.SetBinding(Image.SourceProperty, tb_content_binding);
            }
            else
            {
                elementFactory.SetBinding(Image.SourceProperty, attribute.ValueBinding);
            }

            DataTemplate data_template = new DataTemplate();
            data_template.VisualTree = elementFactory;

            template_col.CellTemplate = data_template;

            return template_col;
        }

        private DataGridTemplateColumn GenerateTextBoxTemplateColumn(SubMenuPanelMenuItem.MenuItemAttribute attribute)
        {
            DataGridTemplateColumn template_col = new DataGridTemplateColumn();
            template_col.IsReadOnly = true;
            template_col.Header = attribute.ShowHeader ? attribute.Name : "";
            if (!double.IsNaN(attribute.MaxWidth))
                template_col.MaxWidth = attribute.MaxWidth;
            if (!double.IsNaN(attribute.MinWidth))
                template_col.MinWidth = attribute.MinWidth;
            if (!double.IsNaN(attribute.Width))
                template_col.Width = attribute.Width;
            //template_col.SortMemberPath = attribute_name;

            if (attribute.HorizontalAlignment == System.Windows.HorizontalAlignment.Right)
            {
                template_col.HeaderStyle = GetRightAlignColumnHeaderStyle();
            }
            else if (attribute.HorizontalAlignment == System.Windows.HorizontalAlignment.Center)
            {
                template_col.HeaderStyle = GetCenterAlignColumnHeaderStyle();
            }
            else
            {
                template_col.HeaderStyle = GetLeftAlignColumnHeaderStyle();
            }


            MultiBinding tb_text_binding = new MultiBinding();
            tb_text_binding.Converter = new SubMenuPanelGridValueConverter();
            tb_text_binding.ConverterParameter = attribute;
            tb_text_binding.Bindings.Add(new Binding());
            tb_text_binding.Bindings.Add(new Binding("TriggerGridUpdate"));

            FrameworkElementFactory elementFactory = new FrameworkElementFactory(typeof(TextBox));

            elementFactory.SetValue(NameProperty, "textbox_action_button");
            elementFactory.SetValue(MarginProperty, new Thickness(2));
            elementFactory.SetValue(PaddingProperty, new Thickness(0));
            if (attribute.HorizontalAlignment == System.Windows.HorizontalAlignment.Right)
            {
                elementFactory.SetValue(TextBox.TextAlignmentProperty, TextAlignment.Right);
            }
            else if (attribute.HorizontalAlignment == System.Windows.HorizontalAlignment.Center)
            {
                elementFactory.SetValue(TextBox.TextAlignmentProperty, TextAlignment.Center);
            }
            else
            {
                elementFactory.SetValue(TextBox.TextAlignmentProperty, TextAlignment.Left);
            }
            elementFactory.SetValue(HorizontalAlignmentProperty, HorizontalAlignment.Stretch);

            elementFactory.SetValue(TagProperty, attribute);

            MultiBinding btn_enabled_binding = new MultiBinding();
            btn_enabled_binding.Converter = new SubMenuPanelGridActionEnabledConverter();
            btn_enabled_binding.ConverterParameter = attribute;
            btn_enabled_binding.Bindings.Add(new Binding());
            btn_enabled_binding.Bindings.Add(new Binding("TriggerGridUpdate"));

            MultiBinding btn_visibility_binding = new MultiBinding();
            btn_visibility_binding.Converter = new SubMenuPanelGridActionVisibilityConverter();
            btn_visibility_binding.ConverterParameter = attribute;
            btn_visibility_binding.Bindings.Add(new Binding());
            btn_visibility_binding.Bindings.Add(new Binding("TriggerGridUpdate"));

            elementFactory.SetBinding(IsEnabledProperty, btn_enabled_binding);
            elementFactory.SetBinding(VisibilityProperty, btn_visibility_binding);

            if (attribute.ValueBinding == null)
            {
                elementFactory.SetBinding(TextBox.TextProperty, tb_text_binding);
            }
            else
            {
                elementFactory.SetBinding(TextBox.TextProperty, attribute.ValueBinding);
            }

            //elementFactory.AddHandler(PanelGridButton.InternalKeyUpEvent, new KeyEventHandler(grid_button_KeyUp));
            //elementFactory.AddHandler(PanelGridButton.InternalKeyDownEvent, new KeyEventHandler(grid_button_KeyDown));

            DataTemplate data_template = new DataTemplate();
            data_template.VisualTree = elementFactory;

            template_col.CellTemplate = data_template;

            return template_col;
        }

        private void GenerateActionsColumns(SU item)
        {
            List<SubMenuPanelMenuItem.MenuItemAction> actions = item.GetActions();

            if (actions != null && actions.Count > 0)
            {

                RelativeSource relParentSource = new RelativeSource(RelativeSourceMode.FindAncestor, typeof(DataGridRow), 1);
                int action_count = 0;
                foreach (SubMenuPanelMenuItem.MenuItemAction action in actions)
                {
                    DataGridTemplateColumn template_col = new DataGridTemplateColumn();
                    template_col.IsReadOnly = true;
                    template_col.Header = null;

                    Binding command_binding = new Binding();


                    FrameworkElementFactory actionButton = new FrameworkElementFactory(typeof(PanelGridButton));

                    FrameworkElement actionIcon = action.IconGrid; // item.GetActionIcon(action);

                    MultiBinding btn_icon_binding = new MultiBinding();
                    btn_icon_binding.Converter = new SubMenuPanelGridActionIconConverter();
                    btn_icon_binding.ConverterParameter = action;
                    btn_icon_binding.Bindings.Add(new Binding());
                    btn_icon_binding.Bindings.Add(new Binding("TriggerGridUpdate"));

                    actionButton.SetBinding(ContentProperty, btn_icon_binding);

                    string button_name = "action_" + action_count.ToString();


                    actionButton.SetValue(TagProperty, action);


                    actionButton.SetValue(NameProperty, button_name);
                    actionButton.SetBinding(Button.CommandParameterProperty, command_binding);
                    actionButton.SetValue(MarginProperty, new Thickness(0));
                    actionButton.SetValue(ToolTipProperty, action.Name);
                    actionButton.AddHandler(Button.ClickEvent, new RoutedEventHandler(datagrid_action_click));
                    actionButton.AddHandler(PanelGridButton.InternalKeyUpEvent, new KeyEventHandler(grid_button_KeyUp));
                    actionButton.AddHandler(PanelGridButton.InternalKeyDownEvent, new KeyEventHandler(grid_button_KeyDown));


                    try
                    {
                        Style ab_style = (Style)ContentControl.FindResource("actions_hover_style");
                        if (ab_style != null)
                            actionButton.SetValue(StyleProperty, ab_style);
                    }
                    catch
                    {

                    }

                    MultiBinding btn_enabled_binding = new MultiBinding();
                    btn_enabled_binding.Converter = new SubMenuPanelGridActionEnabledConverter();
                    btn_enabled_binding.ConverterParameter = action;
                    btn_enabled_binding.Bindings.Add(new Binding());
                    btn_enabled_binding.Bindings.Add(new Binding("TriggerGridUpdate"));

                    MultiBinding btn_opacity_binding = new MultiBinding();
                    btn_opacity_binding.Converter = new SubMenuPanelGridActionOpacityConverter();
                    btn_opacity_binding.ConverterParameter = action;
                    btn_opacity_binding.Bindings.Add(new Binding());
                    btn_opacity_binding.Bindings.Add(new Binding("TriggerGridUpdate"));

                    MultiBinding btn_visibility_binding = new MultiBinding();
                    btn_visibility_binding.Converter = new SubMenuPanelGridActionVisibilityConverter();
                    btn_visibility_binding.ConverterParameter = action;
                    btn_visibility_binding.Bindings.Add(new Binding());
                    btn_visibility_binding.Bindings.Add(new Binding("TriggerGridUpdate"));

                    actionButton.SetBinding(IsEnabledProperty, btn_enabled_binding);
                    actionButton.SetBinding(OpacityProperty, btn_opacity_binding);
                    actionButton.SetBinding(VisibilityProperty, btn_visibility_binding);

                    //elementFactory.AppendChild(actionButton);
                    action_count++;

                    DataTemplate data_template = new DataTemplate();
                    data_template.VisualTree = actionButton;

                    template_col.CellTemplate = data_template;

                    SubmenuItemsDatagrid.Columns.Add(template_col);
                }
            }
        }

        #region panel grid view - event handler
        private void datagrid_action_click(object sender, RoutedEventArgs e)
        {
            Button btn_sender = sender as Button;

            if (btn_sender != null)
            {
                //string action = btn_sender.Tag as string;
                SubMenuPanelMenuItem.MenuItemAction btn_action = btn_sender.Tag as SubMenuPanelMenuItem.MenuItemAction;

                if (btn_action != null)
                    RaiseAction(btn_sender.CommandParameter, btn_action);
                else
                    RaiseAction(btn_sender.CommandParameter);
            }
        }
        private void datagrid_button_column_click(object sender, RoutedEventArgs e)
        {
            Button btn_sender = sender as Button;
            CheckBox chx_sender = sender as CheckBox;

            if (btn_sender != null)
            {
                SubMenuPanelMenuItem.MenuItemAttribute btn_attribute = btn_sender.Tag as SubMenuPanelMenuItem.MenuItemAttribute;

                if (btn_attribute != null)
                {
                    if (btn_attribute.RaiseClickAction != null)
                        RaiseAction(btn_sender.CommandParameter, btn_attribute.RaiseClickAction);
                    else
                        RaiseAction(btn_sender.CommandParameter, ((SU)btn_sender.CommandParameter).DefaultAction);
                }
                else
                {
                    RaiseAction(btn_sender.CommandParameter);
                }
            }
            else if (chx_sender != null)
            {
                SubMenuPanelMenuItem.MenuItemAttribute btn_attribute = chx_sender.Tag as SubMenuPanelMenuItem.MenuItemAttribute;

                if (btn_attribute != null)
                {
                    if (btn_attribute.RaiseClickAction != null)
                        RaiseAction(chx_sender.CommandParameter, btn_attribute.RaiseClickAction);
                    else
                        RaiseAction(chx_sender.CommandParameter, ((SU)chx_sender.CommandParameter).DefaultAction);
                }
                else
                {
                    RaiseAction(chx_sender.CommandParameter);
                }
            }
        }

        private void submenu_items_datagrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (_ignore_grid_selection_change)
                return;

            int sel_row_indowx = SubmenuItemsDatagrid.SelectedIndex;

            if (sel_row_indowx >= 0)
            {
                // set the keyboard-focus to the first input control in the newly selected row
                DataGridRow new_selected_row = DataGridHelper.GetRow(SubmenuItemsDatagrid, sel_row_indowx);

                if (new_selected_row != null)
                {
                    if (new_selected_row.IsKeyboardFocusWithin)
                        return;

                    bool b_focused = false;
                    List<TextBox> text_boxes = UIHelper.GetVisualChildrenByType<TextBox>(new_selected_row);

                    foreach (TextBox cur_tb in text_boxes)
                    {
                        if (cur_tb.IsEnabled && !cur_tb.IsReadOnly && cur_tb.Visibility == System.Windows.Visibility.Visible)
                        {
                            _comes_from_parent_row = true;
                            cur_tb.Focus();
                            b_focused = true;
                            break;
                        }
                    }
                    if (!b_focused)
                    {
                        List<PanelGridButton> buttons = UIHelper.GetVisualChildrenByType<PanelGridButton>(new_selected_row);
                        foreach (PanelGridButton cur_btn in buttons)
                        {
                            if (cur_btn.IsEnabled && cur_btn.Visibility == System.Windows.Visibility.Visible)
                            {
                                _comes_from_parent_row = true;
                                cur_btn.Focus();
                                b_focused = true;
                                break;
                            }
                        }
                    }
                }
            }
        }

        private void grid_button_KeyUp(object sender, KeyEventArgs e)
        {
            if (_comes_from_parent_row)
            {
                _comes_from_parent_row = false;
                return;
            }

            PanelGridButton sender_btn = sender as PanelGridButton;
            if (sender_btn == null)
                return;

            if (e.Key != Key.Down && e.Key != Key.Up)
                return;

            DataGridRow parent_row = UIHelper.TryFindParent<DataGridRow>(sender_btn);

            if (parent_row != null)
            {
                int row_index = DataGridHelper.FindRowIndex(parent_row);

                if (e.Key == Key.Up)
                    row_index--;
                else if (e.Key == Key.Down)
                    row_index++;
                else
                    return;

                if (row_index >= 0 && row_index < SubmenuItemsDatagrid.Items.Count)
                {
                    DataGridRow new_selected_row = DataGridHelper.GetRow(SubmenuItemsDatagrid, row_index);

                    if (new_selected_row != null)
                    {
                        _ignore_grid_selection_change = true;
                        new_selected_row.IsSelected = true;

                        string button_name = sender_btn.Name;
                        Button btn_new_row = UIHelper.FindVisualChildByName<PanelGridButton>(new_selected_row, button_name) as PanelGridButton;
                        if (btn_new_row != null && btn_new_row.IsEnabled && btn_new_row.Visibility == System.Windows.Visibility.Visible)
                            btn_new_row.Focus();
                        else
                        {
                            bool b_focused = false;
                            List<PanelGridButton> buttons = UIHelper.GetVisualChildrenByType<PanelGridButton>(new_selected_row);
                            foreach (PanelGridButton cur_btn in buttons)
                            {
                                if (cur_btn.IsEnabled && cur_btn.Visibility == System.Windows.Visibility.Visible)
                                {
                                    cur_btn.Focus();
                                    b_focused = true;
                                    break;
                                }
                            }

                            if (!b_focused)
                            {
                                List<TextBox> text_boxes = UIHelper.GetVisualChildrenByType<TextBox>(new_selected_row);

                                foreach (TextBox cur_tb in text_boxes)
                                {
                                    if (cur_tb.IsEnabled && !cur_tb.IsReadOnly && cur_tb.Visibility == System.Windows.Visibility.Visible)
                                    {
                                        cur_tb.Focus();
                                        b_focused = true;
                                        break;
                                    }
                                }
                            }
                        }
                        _ignore_grid_selection_change = false;
                    }
                }
            }
        }

        private void grid_button_KeyDown(object sender, KeyEventArgs e)
        {
            PanelGridButton sender_btn = sender as PanelGridButton;
            if (sender_btn == null)
                return;

            bool first_control_in_next_line = false;

            if (sender_btn.Name == _last_action_button_name)
            {
                if (e.Key == Key.Right || e.Key == Key.Tab)
                {
                    first_control_in_next_line = true;
                    e.Handled = true;
                }
            }
            else
            {
                return;
            }

            DataGridRow parent_row = UIHelper.TryFindParent<DataGridRow>(sender_btn);

            if (parent_row != null)
            {
                int row_index = DataGridHelper.FindRowIndex(parent_row);

                if (first_control_in_next_line)
                    row_index++;
                else
                    return;

                if (row_index >= 0 && row_index < SubmenuItemsDatagrid.Items.Count)
                {
                    DataGridRow new_selected_row = DataGridHelper.GetRow(SubmenuItemsDatagrid, row_index);

                    if (new_selected_row != null)
                    {
                        _ignore_grid_selection_change = true;
                        new_selected_row.IsSelected = true;

                        bool b_focused = false;
                        List<TextBox> text_boxes = UIHelper.GetVisualChildrenByType<TextBox>(new_selected_row);

                        foreach (TextBox cur_tb in text_boxes)
                        {
                            if (cur_tb.IsEnabled && !cur_tb.IsReadOnly && cur_tb.Visibility == System.Windows.Visibility.Visible)
                            {
                                _comes_from_parent_row = true;
                                cur_tb.Focus();
                                b_focused = true;
                                break;
                            }
                        }
                        if (!b_focused)
                        {
                            List<PanelGridButton> buttons = UIHelper.GetVisualChildrenByType<PanelGridButton>(new_selected_row);
                            foreach (PanelGridButton cur_btn in buttons)
                            {
                                if (cur_btn.IsEnabled && cur_btn.Visibility == System.Windows.Visibility.Visible)
                                {
                                    _comes_from_parent_row = true;
                                    cur_btn.Focus();
                                    b_focused = true;
                                    break;
                                }
                            }
                        }
                        _ignore_grid_selection_change = false;
                    }
                }
                else if (row_index == SubmenuItemsDatagrid.Items.Count)
                {
                    _ignore_grid_selection_change = true;
                    parent_row.IsSelected = false;
                    _ignore_grid_selection_change = false;
                }
            }
        }

        private void submenu_items_datagrid_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            double star_min_width = StarColumnMinWidth;
            double space = 45; // column header, scroll bar
            double all_col_width = 0;

            int nStarColumnIndex = StarColumnAttributeIndex;

            double act_width = SubmenuItemsDatagrid.ActualWidth;

            if (double.IsNaN(act_width) || double.IsInfinity(act_width))
                return;

            if (SubmenuItemsDatagrid == null || SubmenuItemsDatagrid.Columns.Count == 0)
                return;

            DataGridColumn start_column = SubmenuItemsDatagrid.Columns[nStarColumnIndex];
            all_col_width = SubmenuItemsDatagrid.Columns.Sum(c => c.ActualWidth);
            all_col_width += space;

            if (start_column != null)
            {
                double cur_bezeichnung_width = start_column.ActualWidth;



                if (double.IsNaN(cur_bezeichnung_width) || double.IsInfinity(cur_bezeichnung_width))
                    return;

                double diff_width = e.NewSize.Width - all_col_width;

                //Debug.WriteLine("e.NewSize.Width=" + e.NewSize.Width.ToString() + " - all_col_width=" + all_col_width.ToString());

                if ((cur_bezeichnung_width + diff_width) < star_min_width)
                    start_column.Width = star_min_width;
                else
                    start_column.Width = (cur_bezeichnung_width + diff_width);
            }
        }

        private void submenu_items_datagrid_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            if (e.Row.Item is SU)
            {
                ((SU)e.Row.Item).UpdateActionState();
                ((SU)e.Row.Item).UpdateAttributeState();
            }
        }

        #endregion

        private void EnbaleLeftRightArrows()
        {
            ArrowdownButton.Visibility = Visibility.Collapsed;
            ArrowupButton.Visibility = Visibility.Collapsed;

            if (!_with_navigation_buttons)
                return;

            ArrowleftButton.Visibility = Visibility.Visible;
            ArrowrightButton.Visibility = Visibility.Visible;

        }

        private void EnableUpDownArrows()
        {
            ArrowleftButton.Visibility = Visibility.Collapsed;
            ArrowrightButton.Visibility = Visibility.Collapsed;

            if (!_with_navigation_buttons)
                return;

            ArrowdownButton.Visibility = Visibility.Visible;
            ArrowupButton.Visibility = Visibility.Visible;
        }

        private void SendNavigationDepthChanged()
        {
            if (NavigationDepthChanged != null)
                NavigationDepthChanged(this, EventArgs.Empty);
        }

        #region styles
        private Style GetLeftAlignColumnHeaderStyle()
        {
            Style ret = null;
            try
            {
                ret = (Style)ContentControl.TryFindResource("left_align_col_header");
                //ret = (Style) FindResource("left_align_col_header");
            }
            catch
            {

            }
            finally
            {
                if (ret == null)
                    ret = DefaultLeftAlignColumnHeaderStyle();
            }

            return ret;
        }

        private Style DefaultLeftAlignColumnHeaderStyle()
        {
            Style defaultStyle = new Style(typeof(DataGridColumnHeader), (Style)ContentControl.FindResource(typeof(DataGridColumnHeader)));
            defaultStyle.Setters.Add(new Setter(HorizontalAlignmentProperty, HorizontalAlignment.Left));
            defaultStyle.Setters.Add(new Setter(HorizontalContentAlignmentProperty, HorizontalAlignment.Left));
            return defaultStyle;
        }

        private Style GetRightAlignColumnHeaderStyle()
        {
            Style ret = null;
            try
            {
                ret = (Style)ContentControl.TryFindResource("right_align_col_header");
                //ret = (Style)FindResource("right_align_col_header");
            }
            catch
            {

            }
            finally
            {
                if (ret == null)
                    ret = DefaultRightAlignColumnHeaderStyle();
            }

            return ret;
        }

        private Style DefaultRightAlignColumnHeaderStyle()
        {
            Style defaultStyle = new Style(typeof(DataGridColumnHeader), (Style)ContentControl.FindResource(typeof(DataGridColumnHeader)));
            defaultStyle.Setters.Add(new Setter(HorizontalAlignmentProperty, HorizontalAlignment.Right));
            defaultStyle.Setters.Add(new Setter(HorizontalContentAlignmentProperty, HorizontalAlignment.Right));
            return defaultStyle;
        }

        private Style GetCenterAlignColumnHeaderStyle()
        {
            Style ret = null;
            try
            {
                ret = (Style)ContentControl.TryFindResource("center_align_col_header");
                //ret = (Style)FindResource("center_align_col_header");
            }
            catch
            {

            }
            finally
            {
                if (ret == null)
                    ret = DefaultCenterAlignColumnHeaderStyle();
            }

            return ret;
        }

        private Style DefaultCenterAlignColumnHeaderStyle()
        {
            Style defaultStyle = new Style(typeof(DataGridColumnHeader), (Style)ContentControl.FindResource(typeof(DataGridColumnHeader)));
            defaultStyle.Setters.Add(new Setter(HorizontalAlignmentProperty, HorizontalAlignment.Center));
            defaultStyle.Setters.Add(new Setter(HorizontalContentAlignmentProperty, HorizontalAlignment.Center));
            return defaultStyle;
        }
        #endregion

        #region Reset MAP ZP
        void UpdateLineMapZP()
        {
            _map_zp.BorderMaxZoom = 1;
            _map_zp.BorderMinZoom = 1;
            _map_zp.WithBorderCheckWhileTranslationActive = false;
            _map_zp.WithBorderCheck = true;
            _map_zp.IgnoreYTranslation = true;
        }

        void UpdateKachelMapZP()
        {
            _map_zp.BorderMaxZoom = 10;
            _map_zp.BorderMinZoom = 1;
            _map_zp.WithBorderCheckWhileTranslationActive = false;
            _map_zp.IgnoreYTranslation = false;
            _map_zp.WithBorderCheck = true;
        }

        void ResetMapZp()
        {
            ResetMapZp(false);
        }
        void ResetMapZp(bool without_scale)
        {
            if (_map_zp == null)
                return;

            if (!without_scale)
            {
                _map_zp.ScaleTransform.BeginAnimation(ScaleTransform.ScaleXProperty, _scale_x_reset);
                _map_zp.ScaleTransform.BeginAnimation(ScaleTransform.ScaleYProperty, _scale_y_reset);
            }
            _map_zp.TranslateTransform.BeginAnimation(TranslateTransform.XProperty, _transl_x_reset);
            _map_zp.TranslateTransform.BeginAnimation(TranslateTransform.YProperty, _transl_y_reset);
        }
        void _transl_reset_x_Completed(object sender, EventArgs e)
        {
            _map_zp.TranslateTransform.BeginAnimation(TranslateTransform.XProperty, null);
            _map_zp.TranslateTransform.X = 0;
        }
        void _transl_reset_y_Completed(object sender, EventArgs e)
        {
            _map_zp.TranslateTransform.BeginAnimation(TranslateTransform.YProperty, null);
            _map_zp.TranslateTransform.Y = 0;
        }
        void _scale_reset_x_Completed(object sender, EventArgs e)
        {
            _map_zp.ScaleTransform.BeginAnimation(ScaleTransform.ScaleXProperty, null);
            _map_zp.ScaleTransform.ScaleX = 1;
        }
        void _scale_reset_y_Completed(object sender, EventArgs e)
        {
            _map_zp.ScaleTransform.BeginAnimation(ScaleTransform.ScaleYProperty, null);
            _map_zp.ScaleTransform.ScaleY = 1;
        }

        private void UpdateMapZPBorders()
        {
            if (_map_zp == null)
                return;

            if (ActiveLayoutMode == LayoutMode.Line || ActiveLayoutMode == LayoutMode.Kachel)
            {
                int items_per_row = 0;
                double item_width = 0;
                double item_height = 0;

                if (ActiveLayoutMode == LayoutMode.Line)
                {
                    items_per_row = _items_filtered.Count;
                    item_width = BigItemSize.Width;
                    item_height = BigItemSize.Height;
                }
                else
                {
                    items_per_row = (int)Math.Floor(SubmenuBaseCanvas.ActualWidth / SmallItemSize.Width);
                    item_width = SmallItemSize.Width;
                    item_height = SmallItemSize.Height;
                }

                int rows = (int)Math.Ceiling((double)_items_filtered.Count / (double)items_per_row);
                double panel_canvas_height = 0;
                double panel_canvas_width = 0;

                panel_canvas_height = (item_height * rows);
                if (_items_filtered.Count >= items_per_row)
                    panel_canvas_width = (item_width * items_per_row);
                else
                    panel_canvas_width = _items_filtered.Count * item_width;

                //_map_zp.BorderCheckerWidth = BigItemSize.Width * _items_filtered.Count;
                //_map_zp.BorderCheckerHeight = BigItemSize.Height;                

                //_map_zp.BorderCheckerXMax = 50 / _map_zp.ScaleTransform.ScaleX;
                //_map_zp.BorderCheckerYMax = 50 / _map_zp.ScaleTransform.ScaleY;  

                _map_zp.BorderCheckerXMax = 0;
                _map_zp.BorderCheckerYMax = 0;

                if (ActiveLayoutMode == LayoutMode.Line)
                {
                    //if (_animating_tile_panel.Width > panel_canvas_width)
                    //    _map_zp.BorderCheckerWidth = _animating_tile_panel.Width;
                    //else
                    //    _map_zp.BorderCheckerWidth = panel_canvas_width + 50 / _map_zp.ScaleTransform.ScaleX;
                    _map_zp.BorderCheckerWidth = panel_canvas_width;
                    _map_zp.BorderCheckerHeight = BigItemSize.Height;
                    _map_zp.BorderCheckerXMax = 0;
                    _map_zp.BorderCheckerYMax = 0;
                }
                else
                {
                    //_map_zp.BorderCheckerWidth = panel_canvas_width + 50 / _map_zp.ScaleTransform.ScaleX;                   
                    _map_zp.BorderCheckerWidth = panel_canvas_width;
                    //_map_zp.BorderCheckerWidth = (SmallItemSize.Width * _items_filtered.Count) + 50 / _map_zp.ScaleTransform.ScaleX;
                    if (panel_canvas_height < base_grid.ActualHeight / _map_zp.ScaleTransform.ScaleY)
                        _map_zp.BorderCheckerHeight = (base_grid.ActualHeight / _map_zp.ScaleTransform.ScaleY) + 1 / _map_zp.ScaleTransform.ScaleY;
                    else
                        _map_zp.BorderCheckerHeight = panel_canvas_height;

                    SubmenuBaseCanvas.Height = panel_canvas_height;
                }


            }

            else if (ActiveLayoutMode == LayoutMode.Grid)
            {

            }
        }
        #endregion
        /// <summary>
        /// diese methode holt sich ausgehend vom erforderlichplatz die gebrauchte höhe
        /// und animiert das panel auf die passende höhe
        /// der erforderlich platz wird berechnet abhängig von der anzahl der elemente die dargestellt werden
        /// </summary>
        private void UpdateAndAnimatePanelToHeight(double height)
        {
            _animation_active = true;
            DoubleAnimation canvasheight_da = new DoubleAnimation(height, new Duration(new TimeSpan(0, 0, 0, 0, 500)));
            canvasheight_da.DecelerationRatio = 1;
            canvasheight_da.Completed += new EventHandler(canvasheight_da_Completed);
            SubmenuBorder.Tag = height;
            SubmenuBorder.BeginAnimation(HeightProperty, canvasheight_da);
        }

        private void canvasheight_da_Completed(object sender, EventArgs e)
        {
            SubmenuBorder.BeginAnimation(HeightProperty, null);
            SubmenuBorder.Height = (double)SubmenuBorder.Tag;
            _animation_active = false;

            if (_autohide_timer_after_action)
                StartAutoHideTimer();
        }
        #endregion

        #region EventHandler
        void Window_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            _resize_active_new_size = e.NewSize;
            bool panel_visible = (SubmenuBorder.Height > 0);
            if (panel_visible)
            {
                UpdateAndAnimatePanelToHeight(PanelHeight);
            }
            _resize_active_new_size = null;
        }

        private void search_button_Click(object sender, RoutedEventArgs e)
        {
            if (SearchButtonClick != null)
                SearchButtonClick(this, new EventArgs());
        }

        private void newelem_button_Click(object sender, RoutedEventArgs e)
        {
            if (NewElemClick != null)
                NewElemClick(this, new EventArgs());
        }

        private void custom_button_Click(object sender, RoutedEventArgs e)
        {
            if (CustomButtonClick != null)
                CustomButtonClick(this, new EventArgs());
        }

        private void custom_button1_Click(object sender, RoutedEventArgs e)
        {
            if (CustomButton1Click != null)
                CustomButton1Click(this, new EventArgs());
        }

        void search_cbx_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (SearchCbx.SelectedItem != null)
            {
                ApplyFilterText(true);
            }
        }

        void search_cbx_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (SearchCbx.SelectedItem != null)
                return;

            ApplyFilterText(false);
        }

        public void SetSearchFilter(string text)
        {
            SearchCbx.SelectedItem = null;
            SearchCbx.Text = text;
        }

        public string GetSearchFilter()
        {
            if (SearchCbx.SelectedItem != null)
                return "";

            return SearchCbx.Text;
        }

        private void ApplyFilterText(bool custom_filter)
        {
            _was_custom_filter = custom_filter;
            //#if DEBUG
            //            if(custom_filter)
            //            {
            //                Debug.WriteLine("*** Applying custom SubmenuPanelItem filter ...");
            //                Debug.WriteLine("*** Filter: " + (search_cbx.SelectedItem as string) ?? "<null>");
            //            }
            //            else
            //            {
            //                Debug.WriteLine("*** Applying SubmenuPanelItem Textfilter ...");
            //                Debug.WriteLine("*** Filter: " + (string.IsNullOrEmpty(search_cbx.Text) ? "<empty>" : search_cbx.Text));
            //            }
            //#endif

            var query_filtered_items = _items.Select(i => i);
            bool b_text_check = string.IsNullOrEmpty(SearchCbx.Text);

            if (custom_filter)
                b_text_check = SearchCbx.SelectedItem == null;

            if (!(!WithSearchField || b_text_check))
                query_filtered_items = _items.Where(i => IsFiltered(i));

            List<SU> items_to_remove = _items_filtered.Where(i => !IsFiltered(i)).ToList();

            foreach (SU filtered_item in query_filtered_items)
            {
                if (!_items_filtered.Contains(filtered_item))
                    _items_filtered.Add(filtered_item);
            }

            foreach (SU viewbox in items_to_remove)
            {
                _items_filtered.Remove(viewbox);
            }

            _items_filtered.Sort((x, y) => x.CompareTo(y));

            ResetMapZp(true);

            UpdateMapZPBorders();

            if (_animating_tile_panel == null)
                return;

            _animating_tile_panel.InvalidateVisual();
            _animating_tile_panel.UpdateLayout();
        }

        public void ApplySort()
        {
            _items_filtered.Sort((x, y) => x.CompareTo(y));

            // update itemcount if filter is set
            if (WithShowItemCount && ItemCountFilterExpression != null)
            {
                ItemCount = _items.Where(i => ItemCountFilterExpression(i)).Count();
            }
        }

        void _submenu_fadein_Completed(object sender, EventArgs e)
        {
            BaseFrame.EnableMouseClicks();
            _autohide_timer_after_action = false;
            _submenu_fadein_da.Completed -= _submenu_fadein_Completed;
            //_submenu_fadein_da = null;
            SubmenuBorder.BeginAnimation(HeightProperty, null);
            SubmenuBorder.Height = PanelHeight;
            _animation_active = false;


            SetMode();

            if (FadeInCompleted != null)
                FadeInCompleted(this, new EventArgs());

        }

        void SetMode()
        {
            if (ActiveLayoutMode == LayoutMode.Line)
                UpdateLineMapZP();
        }

        void _submenu_fadeout_da_Completed(object sender, EventArgs e)
        {
            BaseFrame.EnableMouseClicks();
            _autohide_timer_after_action = false;
            _submenu_fadeout_da.Completed -= _submenu_fadein_Completed;
            //_submenu_fadeout_da = null;
            SubmenuBorder.BeginAnimation(HeightProperty, null);
            SubmenuBorder.Height = 0;
            _animation_active = false;

            if (FadeOutCompleted != null)
                FadeOutCompleted(this, new EventArgs());
        }

        private void Rectangle_MouseDown(object sender, MouseButtonEventArgs e)
        {
            _resize_active = true;
            BaseFrame.MouseUp += BaseWindow_MouseUp;
            BaseFrame.MouseMove += BaseWindow_MouseMove;
            BaseFrame.DisableClicksMainArea();
            IsHitTestVisible = false;
            BaseFrame.OverlayManager.IsHitTestVisible = false;
            _submenu_resize_start_pos = e.GetPosition(BaseFrame);
            _start_height_resize = SubmenuBorder.ActualHeight;
            BaseFrame.SetAppCursor(Cursors.SizeNS);
        }

        void BaseWindow_MouseMove(object sender, MouseEventArgs e)
        {
            Point cur_pos = e.GetPosition(BaseFrame);
            Vector v = cur_pos - _submenu_resize_start_pos;

            double new_height = _start_height_resize - v.Y;
            CheckPanelHeight(ref new_height);
            SubmenuBorder.Height = new_height;
        }

        private void CheckPanelHeight(ref double new_height)
        {
            if (new_height < BaseFrame.MinHeightMainContent)
                new_height = BaseFrame.MinHeightMainContent;

            double max_height = BaseFrame.WindowBorder.ActualHeight - BaseFrame.ActualTitleBarHeight - BaseFrame.MinHeightMainContent - BaseFrame.ActualTaskBarBaseBorderHeight;
            if (new_height > max_height)
                new_height = max_height;
        }

        void BaseWindow_MouseUp(object sender, MouseButtonEventArgs e)
        {
            _resize_active = false;
            BaseFrame.MouseUp -= BaseWindow_MouseUp;
            BaseFrame.MouseMove -= BaseWindow_MouseMove;
            BaseFrame.EnableClicksMainArea();
            IsHitTestVisible = true;
            BaseFrame.OverlayManager.IsHitTestVisible = true;
            BaseFrame.SetDefaultAppCursor();
        }

        void _animating_tile_panel_MouseLeave(object sender, MouseEventArgs e)
        {
            //if (ToggleButtonReference.IsChecked == true && WithAutoHideMenu == true && pin_button.IsChecked != true)
            //{
            //    if (e.GetPosition(submenu_base_canvas).Y < 0 && !_resize_active)
            //        ToggleButtonReference.IsChecked = false;
            //}

            if (ToggleButtonReference != null && ToggleButtonReference.IsChecked == true && WithAutoHideMenu == true && PinButton.IsChecked != true)
            {
                StartAutoHideTimer();
            }
        }

        void _animating_tile_panel_MouseEnter(object sender, MouseEventArgs e)
        {
            StopAutoHideTimer();
        }

        void _animating_tile_panel_MouseMove(object sender, MouseEventArgs e)
        {
            StopAutoHideTimer();
        }

        #region LayoutMode Setting
        void orderlayout1_button_Unchecked(object sender, RoutedEventArgs e)
        {
            ToggleButton button = (ToggleButton)sender;
            button.IsChecked = true;
        }

        void orderlayout1_button_Checked(object sender, RoutedEventArgs e)
        {
            ActiveLayoutMode = LayoutMode.Kachel;
        }

        void orderlayout2_button_Unchecked(object sender, RoutedEventArgs e)
        {
            ToggleButton button = (ToggleButton)sender;
            button.IsChecked = true;

        }

        void orderlayout2_button_Checked(object sender, RoutedEventArgs e)
        {
            ActiveLayoutMode = LayoutMode.Line;
        }

        void orderlayout3_button_Unchecked(object sender, RoutedEventArgs e)
        {
            ToggleButton button = (ToggleButton)sender;
            button.IsChecked = true;
        }

        void orderlayout3_button_Checked(object sender, RoutedEventArgs e)
        {
            ActiveLayoutMode = LayoutMode.Grid;
        }
        #endregion

        #region AutoHide

        private void StopAutoHideTimer()
        {
            if (_auto_hide_delay_timer != null)
            {
                _auto_hide_delay_timer.Stop();
                _auto_hide_delay_timer.Elapsed -= _auto_hide_delay_timer_Elapsed;
                _auto_hide_delay_timer = null;
            }
        }

        private void StartAutoHideTimer()
        {
            StopAutoHideTimer();

            if (_resize_active || _animation_active || Mouse.GetPosition(SubmenuBaseCanvas).Y >= 0)
            {
                if (!_ignore_autohide_after_action)
                    _autohide_timer_after_action = true;
                else
                    _ignore_autohide_after_action = false;

                return;
            }


            _auto_hide_delay_timer = new Timer(AutoHideTimerInterval);
            _auto_hide_delay_timer.Elapsed += new ElapsedEventHandler(_auto_hide_delay_timer_Elapsed);
            _auto_hide_delay_timer.Start();
        }

        void _auto_hide_delay_timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            StopAutoHideTimer();

            Dispatcher.Invoke(DispatcherPriority.Normal, (NoParas)delegate
            {

                if (ToggleButtonReference != null)
                    ToggleButtonReference.IsChecked = false;
            });

        }
        #endregion
        #endregion


        void _map_zp_TranslationAnimationCompleted(object sender, EventArgs e)
        {
            AnimatingTilePanelItemsControl.IsHitTestVisible = true;
        }

        void _map_zp_TranslationAnimationStarted(object sender, EventArgs e)
        {
            AnimatingTilePanelItemsControl.IsHitTestVisible = false;
        }

        #region Button movement
        private void CommandBinding_NavigateLeft(object sender, RoutedEventArgs e)
        {
            GoLeft();
        }

        private void CommandBinding_NavigateRight(object sender, RoutedEventArgs e)
        {
            GoRight();
        }

        public void GoLeft()
        {
            if (_items_filtered.Count == 0)
                return;

            if (_items_filtered.Count(i => i.IsSelected == true) == 0)
            {
                _items_filtered[0].IsSelected = true;
                FocusHelper.Focus(_items_filtered[0]);
                return;
            }

            SU item = _items_filtered.Where(i => i.IsSelected).FirstOrDefault();
            int curpos = _items_filtered.IndexOf(item);

            DeselectAllItems();

            if (curpos == 0)
            {
                _items_filtered[_items_filtered.Count - 1].IsSelected = true;
                FocusHelper.Focus(_items_filtered[_items_filtered.Count - 1]);
            }
            else
            {
                _items_filtered[curpos - 1].IsSelected = true;
                FocusHelper.Focus(_items_filtered[curpos - 1]);
            }

            return;

            #region OLD
            //double cur_transform_x = _map_zp.TranslateTransform.X;
            //double dMaxItems = MaxItemCountWidth();

            //double new_x = cur_transform_x + (dMaxItems) * _big_item_size.Width;

            //if (new_x > 0)
            //    new_x = 0;

            //DoubleAnimation transl_x = new DoubleAnimation(new_x, new Duration(new TimeSpan(0, 0, 0, 0, 500))) { DecelerationRatio = 1 };
            //transl_x.Completed += new EventHandler(transl_x_Completed);

            //_map_zp.TranslateTransform.BeginAnimation(TranslateTransform.XProperty, transl_x);
            #endregion
        }

        public void GoRight()
        {
            if (_items_filtered.Count == 0)
                return;

            if (_items_filtered.Count(i => i.IsSelected == true) == 0)
            {
                _items_filtered[0].IsSelected = true;
                FocusHelper.Focus(_items_filtered[0]);
                return;
            }

            SU item = _items_filtered.Where(i => i.IsSelected).FirstOrDefault();
            int curpos = _items_filtered.IndexOf(item);

            DeselectAllItems();

            if (curpos == _items_filtered.Count - 1)
            {
                _items_filtered[0].IsSelected = true;
                FocusHelper.Focus(_items_filtered[0]);
            }
            else
            {
                _items_filtered[curpos + 1].IsSelected = true;
                FocusHelper.Focus(_items_filtered[curpos + 1]);
            }

            return;

            #region OLD
            //SubMenuPanelMenuItem m_item = FindSelectedMenuItem();
            //m_item.IsSelected = true;

            //    if (_map_zp == null)
            //    return;

            //if (_map_zp.TranslateTransform == null)
            //    return;

            //double cur_transform_x = _map_zp.TranslateTransform.X;
            //double dMaxItems = MaxItemCountWidth();


            //if (_items_filtered.Count < dMaxItems)
            //    return;

            //double new_x = cur_transform_x - (dMaxItems) * _big_item_size.Width;

            //double complete_width = _map_zp.BorderCheckerWidth;
            //if ((submenu_base_grid.ActualWidth + Math.Abs(new_x)) > complete_width)
            //{
            //    new_x = submenu_base_grid.ActualWidth - complete_width;
            //}

            //DoubleAnimation transl_x = new DoubleAnimation(new_x, new Duration(new TimeSpan(0, 0, 0, 0, 500))) { DecelerationRatio = 1 };
            //transl_x.Completed += new EventHandler(transl_x_Completed);

            //_map_zp.TranslateTransform.BeginAnimation(TranslateTransform.XProperty, transl_x);
            ////_map_zp.TranslateTransform.BeginAnimation(TranslateTransform.YProperty, _transl_y_reset);
            #endregion
        }

        private void arrowleft_button_Click(object sender, RoutedEventArgs e)
        {
            GoLeft();
        }

        private void arrowright_button_Click(object sender, RoutedEventArgs e)
        {
            GoRight();
        }

        private void arrowdown_button_Click(object sender, RoutedEventArgs e)
        {
            if (_map_zp == null)
                return;

            if (_map_zp.TranslateTransform == null)
                return;

            if (_active_layout_mode == LayoutMode.Kachel)
            {
                double cur_transform_y = _map_zp.TranslateTransform.Y;
                double dMaxItems = MaxItemCountHeight();


                if (_items_filtered.Count < dMaxItems)
                    return;

                double new_y = cur_transform_y - (dMaxItems) * _small_item_size.Height;

                double complete_height = _map_zp.BorderCheckerHeight;
                if ((SubmenuBaseGrid.ActualHeight + Math.Abs(new_y)) > complete_height)
                {
                    new_y = SubmenuBaseGrid.ActualHeight - complete_height;
                }

                DoubleAnimation transl_y = new DoubleAnimation(new_y, new Duration(new TimeSpan(0, 0, 0, 0, 500))) { DecelerationRatio = 1 };
                transl_y.Completed += new EventHandler(transl_y_Completed);

                _map_zp.TranslateTransform.BeginAnimation(TranslateTransform.YProperty, transl_y);
            }
            else if (_active_layout_mode == LayoutMode.Grid)
            {
                // sel. item im grid verändern
                if (SubmenuItemsDatagrid.SelectedItem == null)
                {
                    SubmenuItemsDatagrid.SelectedIndex = 0;
                    return;
                }
                int curpos = SubmenuItemsDatagrid.SelectedIndex;

                if (curpos == SubmenuItemsDatagrid.Items.Count - 1)
                {
                    SubmenuItemsDatagrid.SelectedIndex = 0;
                }
                else
                {
                    SubmenuItemsDatagrid.SelectedIndex += 1;
                }
            }
        }

        private void arrowup_button_Click(object sender, RoutedEventArgs e)
        {
            if (_map_zp == null)
                return;

            if (_map_zp.TranslateTransform == null)
                return;

            if (_active_layout_mode == LayoutMode.Kachel)
            {
                double cur_transform_y = _map_zp.TranslateTransform.Y;
                double dMaxItems = MaxItemCountHeight();

                double new_y = cur_transform_y + (dMaxItems) * _small_item_size.Height;

                if (new_y > 0)
                    new_y = 0;

                DoubleAnimation transl_y = new DoubleAnimation(new_y, new Duration(new TimeSpan(0, 0, 0, 0, 500))) { DecelerationRatio = 1 };
                transl_y.Completed += new EventHandler(transl_y_Completed);

                _map_zp.TranslateTransform.BeginAnimation(TranslateTransform.YProperty, transl_y);
            }
            else if (_active_layout_mode == LayoutMode.Grid)
            {
                // sel. item im grid verändern
                // sel. item im grid verändern
                if (SubmenuItemsDatagrid.SelectedItem == null)
                {
                    SubmenuItemsDatagrid.SelectedIndex = SubmenuItemsDatagrid.Items.Count - 1;
                    return;
                }
                int curpos = SubmenuItemsDatagrid.SelectedIndex;

                if (curpos == 0)
                {
                    SubmenuItemsDatagrid.SelectedIndex = SubmenuItemsDatagrid.Items.Count - 1;
                }
                else
                {
                    SubmenuItemsDatagrid.SelectedIndex -= 1;
                }
            }
        }

        void transl_x_Completed(object sender, EventArgs e)
        {
            if (_map_zp == null || _map_zp.TranslateTransform == null)
                return;

            double cur_x = _map_zp.TranslateTransform.X;
            _map_zp.TranslateTransform.BeginAnimation(TranslateTransform.XProperty, null);
            _map_zp.TranslateTransform.X = cur_x;
        }

        void transl_y_Completed(object sender, EventArgs e)
        {
            if (_map_zp == null || _map_zp.TranslateTransform == null)
                return;

            double cur_y = _map_zp.TranslateTransform.Y;
            _map_zp.TranslateTransform.BeginAnimation(TranslateTransform.YProperty, null);
            _map_zp.TranslateTransform.Y = cur_y;
        }

        private double MaxItemCountWidth()
        {
            Size szCheck = new Size(0, 0);
            double max_item_width = szCheck.Width + ItemMargin.Left;

            if (_active_layout_mode == LayoutMode.Line)
            {
                szCheck = _big_item_size;
                double dMaxWidth = SubmenuBaseGrid.ActualWidth;
                max_item_width = szCheck.Width + ItemMargin.Left;

                if (max_item_width == 0)
                    return 0;

                return dMaxWidth / max_item_width;
            }

            if (max_item_width == 0)
                return 0;

            return SubmenuBaseGrid.ActualWidth / max_item_width;
        }

        private double MaxItemCountHeight()
        {
            Size szCheck = new Size(0, 0);
            double max_item_width = szCheck.Width + ItemMargin.Left;
            double max_item_height = szCheck.Height + ItemMargin.Top;

            if (_active_layout_mode == LayoutMode.Kachel)
            {
                szCheck = _big_item_size;
                double dMaxHeight = SubmenuBaseGrid.ActualHeight;
                max_item_height = szCheck.Height + ItemMargin.Top;

                if (max_item_height == 0)
                    return 0;

                return dMaxHeight / max_item_height;
            }

            if (max_item_height == 0)
                return 0;

            return SubmenuBaseGrid.ActualHeight / max_item_height;
        }

        /// <summary>
        /// Überschreibt den Root namen des Breadcrumb controls für sub items
        /// </summary>
        /// <param name="breadcrumb_root_item_name"></param>
        public void SetBreadCrumbRootItemName(string breadcrumb_root_item_name)
        {
            TaskbarItemName = breadcrumb_root_item_name;
        }
        #endregion

        #region Element EventHandler
        void item_LostKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            //SubMenuPanelMenuItem m_item = sender as SubMenuPanelMenuItem;

            //if (m_item != null)
            //    m_item.IsSelected = false;
        }

        void item_GotKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            SU m_item = sender as SU;

            if (m_item != null)
            {
                //DeselectAllItems();

                //m_item.IsSelected = true;
                //if (ShowActionsByLeftMouse)
                //    m_item.HideActionMenu = !m_item.HideActionMenu;

                this.MoveItemIntoView(m_item);
#if DEBUG
                //object f_element = FocusManager.GetFocusedElement(m_item);
                //Console.WriteLine("Got_Focus:" + f_element.ToString() + " Name:" + f_element.GetHashCode() + " KeyboardFocusWithin: " + IsKeyboardFocusWithin);
#endif
            }
        }

        private void submenu_base_canvas_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            UpdateMapZPBorders();
        }

        private void RaiseAction(object tag)
        {
            SU m_i = tag as SU;
            if (m_i != null)
                RaiseAction(tag, m_i.DefaultAction);
            else
                RaiseAction(tag, null);
        }

        private void RaiseAction(object tag, SubMenuPanelMenuItem.MenuItemAction action)
        {
            if (this.Action != null)
                this.Action(tag, new SubMenuPanelMenuItem.MenuItemActionEventArgs(action));
        }

        void item_ActionClicked(object sender, SubMenuPanelMenuItem.MenuItemActionEventArgs e)
        {
            CheckSubNavigation(sender, e.ClickedAction);
            RaiseAction(sender, e.ClickedAction);
        }

        void t_Unchecked(object sender, RoutedEventArgs e)
        {
            RaiseAction(((FrameworkElement)sender).Tag);
        }

        void t_Checked(object sender, RoutedEventArgs e)
        {
            RaiseAction(((FrameworkElement)sender).Tag);
        }

        void b_Click(object sender, RoutedEventArgs e)
        {
            SU m_i = sender as SU;
            if (m_i != null)
                CheckSubNavigation(sender, m_i.DefaultAction);
            else
                CheckSubNavigation(sender, null);

            RaiseAction(((FrameworkElement)sender).Tag);
        }

        private void CheckSubNavigation(object sender, SubMenuPanelMenuItem.MenuItemAction action)
        {
            SU m_i = sender as SU;
            if (m_i != null && action == m_i.DefaultAction && (m_i.HasChildren || m_i.ForceShowChildren))
            {
                // switch to subitem layer
                NavigateToSubItems(m_i as ISubMenuPanelMenuItem);
            }
        }
        #endregion

        #region Dependency Properties



        public string CustomNewElementButtonText
        {
            get { return (string)GetValue(CustomNewElementButtonTextProperty); }
            set { SetValue(CustomNewElementButtonTextProperty, value); }
        }

        public static readonly DependencyProperty CustomNewElementButtonTextProperty =
            DependencyProperty.Register("CustomNewElementButtonText", typeof(string), typeof(SubMenuPanelBase<B, S, T, U, O, SU>), new UIPropertyMetadata("Neues Element Hinzufügen"));


        public string CustomButtonText
        {
            get { return (string)GetValue(CustomButtonTextProperty); }
            set { SetValue(CustomButtonTextProperty, value); }
        }

        public static readonly DependencyProperty CustomButtonTextProperty = DependencyProperty.Register("CustomButtonText", typeof(string), typeof(SubMenuPanelBase<B, S, T, U, O, SU>), new UIPropertyMetadata("Custom Button"));

        public string CustomButtonText1
        {
            get { return (string)GetValue(CustomButtonText1Property); }
            set { SetValue(CustomButtonText1Property, value); }
        }

        public static readonly DependencyProperty CustomButtonText1Property = DependencyProperty.Register("CustomButtonText1", typeof(string), typeof(SubMenuPanelBase<B, S, T, U, O, SU>), new UIPropertyMetadata("Custom Button1"));


        public string SearchButtonTooltip
        {
            get { return (string)GetValue(SearchButtonTooltipProperty); }
            set { SetValue(SearchButtonTooltipProperty, value); }
        }

        public static readonly DependencyProperty SearchButtonTooltipProperty = DependencyProperty.Register("SearchButtonTooltip", typeof(string), typeof(SubMenuPanelBase<B, S, T, U, O, SU>), new UIPropertyMetadata("Gehe zu Element"));

        public int StarColumnAttributeIndex
        {
            get { return (int)GetValue(StarColumnAttributeIndexProperty); }
            set { SetValue(StarColumnAttributeIndexProperty, value); }
        }
        public static readonly DependencyProperty StarColumnAttributeIndexProperty = DependencyProperty.Register("StarColumnAttributeIndex", typeof(int), typeof(SubMenuPanelBase<B, S, T, U, O, SU>), new UIPropertyMetadata(0));
        public double StarColumnMinWidth
        {
            get { return (double)GetValue(StarColumnMinWidthProperty); }
            set { SetValue(StarColumnMinWidthProperty, value); }
        }
        public static readonly DependencyProperty StarColumnMinWidthProperty = DependencyProperty.Register("StarColumnMinWidth", typeof(double), typeof(SubMenuPanelBase<B, S, T, U, O, SU>), new UIPropertyMetadata((double)125));


        public Brush SubMenuIconBgBrush
        {
            get { return (Brush)GetValue(SubMenuIconBgBrushProperty); }
            set { SetValue(SubMenuIconBgBrushProperty, value); }
        }
        public static readonly DependencyProperty SubMenuIconBgBrushProperty = DependencyProperty.Register("SubMenuIconBgBrush", typeof(Brush), typeof(SubMenuPanelBase<B, S, T, U, O, SU>), new UIPropertyMetadata(Brushes.Black));

        public Brush PanelBackground
        {
            get { return (Brush)GetValue(PanelBackgroundProperty); }
            set { SetValue(PanelBackgroundProperty, value); }
        }
        public static readonly DependencyProperty PanelBackgroundProperty = DependencyProperty.Register("PanelBackground", typeof(Brush), typeof(SubMenuPanelBase<B, S, T, U, O, SU>), new UIPropertyMetadata(Brushes.White));

        public Brush ButtonHighlightBackground
        {
            get { return (Brush)GetValue(ButtonHighlightBackgroundProperty); }
            set { SetValue(ButtonHighlightBackgroundProperty, value); }
        }
        public static readonly DependencyProperty ButtonHighlightBackgroundProperty = DependencyProperty.Register("ButtonHighlightBackground", typeof(Brush), typeof(SubMenuPanelBase<B, S, T, U, O, SU>), new UIPropertyMetadata(Brushes.Transparent));

        public int ItemCount
        {
            get { return (int)GetValue(ItemCountProperty); }
            set { SetValue(ItemCountProperty, value); }
        }
        public static readonly DependencyProperty ItemCountProperty = DependencyProperty.Register("ItemCount", typeof(int), typeof(SubMenuPanelBase<B, S, T, U, O, SU>), new UIPropertyMetadata(0));

        public Func<SU, bool> ItemCountFilterExpression
        {
            get { return (Func<SU, bool>)GetValue(ItemCountFilterExpressionProperty); }
            set { SetValue(ItemCountFilterExpressionProperty, value); }
        }
        public static readonly DependencyProperty ItemCountFilterExpressionProperty = DependencyProperty.Register("ItemCountFilterExpression", typeof(Func<SU, bool>), typeof(SubMenuPanelBase<B, S, T, U, O, SU>), new UIPropertyMetadata(null));

        public bool WithShowItemCount
        {
            get { return (bool)GetValue(WithShowItemCountProperty); }
            set { SetValue(WithShowItemCountProperty, value); }
        }
        public static readonly DependencyProperty WithShowItemCountProperty = DependencyProperty.Register("WithShowItemCount", typeof(bool), typeof(SubMenuPanelBase<B, S, T, U, O, SU>), new UIPropertyMetadata(false));

        public bool ShowInfoIndicator
        {
            get { return (bool)GetValue(ShowInfoIndicatorProperty); }
            set { SetValue(ShowInfoIndicatorProperty, value); }
        }
        public static readonly DependencyProperty ShowInfoIndicatorProperty = DependencyProperty.Register("ShowInfoIndicator", typeof(bool), typeof(SubMenuPanelBase<B, S, T, U, O, SU>), new UIPropertyMetadata(false));

        public bool HighlightSearchButton
        {
            get { return (bool)GetValue(HighlightSearchButtonProperty); }
            set { SetValue(HighlightSearchButtonProperty, value); }
        }
        public static readonly DependencyProperty HighlightSearchButtonProperty = DependencyProperty.Register("HighlightSearchButton", typeof(bool), typeof(SubMenuPanelBase<B, S, T, U, O, SU>), new UIPropertyMetadata(false));


        public ISubMenuPanelBreadCrumbItem CurrentBreadCrumbItem
        {
            get { return (ISubMenuPanelBreadCrumbItem)GetValue(CurrentBreadCrumbItemProperty); }
            set { SetValue(CurrentBreadCrumbItemProperty, value); }
        }
        public static readonly DependencyProperty CurrentBreadCrumbItemProperty = DependencyProperty.Register("CurrentBreadCrumbItem", typeof(ISubMenuPanelBreadCrumbItem), typeof(SubMenuPanelBase<B, S, T, U, O, SU>), new UIPropertyMetadata(null, new PropertyChangedCallback(OnCurrentBreadCrumbItem)));

        #endregion

        #region Properties
        public Window Window
        {
            get
            {
                return UIHelper.TryFindParent<Window>(this);
            }
        }

        public string TaskbarItemName
        {
            get { return _taskbar_item_name; }
            internal set
            {
                if (_taskbar_item_name != value)
                {
                    _taskbar_item_name = value;
                    if(_root_bread_crumb_item != null)
                        _root_bread_crumb_item.Text = _taskbar_item_name;
                }
            }
        }

        public bool WithSearchField
        {
            get
            {
                return SearchCbx.Visibility == Visibility.Visible;
            }
            set
            {
                if (value)
                {
                    SearchRect.Visibility = Visibility.Visible;
                    SearchButton.Visibility = Visibility.Visible;
                    SearchCbx.Visibility = Visibility.Visible;

                    SearchCbx.TextChanged -= search_cbx_TextChanged;
                    SearchCbx.TextChanged += search_cbx_TextChanged;
                    SearchCbx.SelectionChanged -= search_cbx_SelectionChanged;
                    SearchCbx.SelectionChanged += search_cbx_SelectionChanged;
                }
                else
                {
                    SearchRect.Visibility = Visibility.Collapsed;
                    SearchButton.Visibility = Visibility.Collapsed;
                    SearchCbx.Visibility = Visibility.Collapsed;

                    SearchCbx.TextChanged -= search_cbx_TextChanged;
                    SearchCbx.SelectionChanged -= search_cbx_SelectionChanged;
                }
            }
        }

        public bool WithSearchButton
        {
            get
            {
                return SearchButton.Visibility == Visibility.Visible;
            }
            set
            {
                if (value)
                {
                    SearchRect.Visibility = Visibility.Visible;
                    SearchButton.Visibility = Visibility.Visible;
                }
                else
                {
                    if (!WithSearchField)
                        SearchRect.Visibility = Visibility.Collapsed;
                    SearchButton.Visibility = Visibility.Collapsed;

                }
            }
        }

        public bool WithNewElementField
        {
            set
            {
                if (value)
                {
                    NewelemRect.Visibility = Visibility.Visible;
                    NewelemButton.Visibility = Visibility.Visible;
                }
                else
                {
                    NewelemRect.Visibility = Visibility.Collapsed;
                    NewelemButton.Visibility = Visibility.Collapsed;
                }
            }
        }

        public bool WithCustomButton
        {
            set
            {
                if (value)
                {
                    CustomRect.Visibility = Visibility.Visible;
                    CustomButton.Visibility = Visibility.Visible;
                }
                else
                {
                    CustomRect.Visibility = Visibility.Collapsed;
                    CustomButton.Visibility = Visibility.Collapsed;
                }
            }
        }

        public bool WithCustomButton1
        {
            set
            {
                if (value)
                {
                    CustomRect1.Visibility = Visibility.Visible;
                    CustomButton1.Visibility = Visibility.Visible;
                }
                else
                {
                    CustomRect1.Visibility = Visibility.Collapsed;
                    CustomButton1.Visibility = Visibility.Collapsed;
                }
            }
        }

        public bool WithNavigationButtons
        {
            set
            {
                _with_navigation_buttons = value;
                if (value)
                {
                    //nav_rect.Visibility = Visibility.Visible;
                    if (_active_layout_mode == LayoutMode.Line)
                        EnbaleLeftRightArrows();
                    else
                        EnableUpDownArrows();
                }
                else
                {
                    //nav_rect.Visibility = Visibility.Collapsed;
                    ArrowleftButton.Visibility = Visibility.Collapsed;
                    ArrowrightButton.Visibility = Visibility.Collapsed;
                    ArrowdownButton.Visibility = Visibility.Collapsed;
                    ArrowupButton.Visibility = Visibility.Collapsed;
                }
            }
        }

        public bool WithGridPanelLayout
        {
            set
            {
                if (value)
                {
                    OrderLayout3Button.Visibility = Visibility.Visible;
                }
                else
                {
                    OrderLayout3Button.Visibility = Visibility.Collapsed;
                }
            }
        }

        public bool WithKachelPanelLayout
        {
            set
            {
                if (value)
                {
                    OrderLayout1Button.Visibility = Visibility.Visible;
                }
                else
                {
                    OrderLayout1Button.Visibility = Visibility.Collapsed;
                }
            }
        }

        public bool WithLinePanelLayout
        {
            set
            {
                if (value)
                {
                    OrderLayout2Button.Visibility = Visibility.Visible;
                }
                else
                {
                    OrderLayout2Button.Visibility = Visibility.Collapsed;
                }
            }
        }

        protected bool WithPinButton
        {
            set
            {
                if (value)
                {
                    PinButton.Visibility = Visibility.Visible;
                }
                else
                {
                    PinButton.Visibility = Visibility.Collapsed;
                }
            }
        }

        public bool WithAutoHideMenu
        {
            get
            {
                return _with_auto_hide;
            }
            set
            {
                _with_auto_hide = value;
                WithPinButton = value;
            }
        }

        public double AutoHideTimerInterval
        {
            get { return _auto_hide_timer_interval; }
            set { _auto_hide_timer_interval = value; }
        }

        public bool AllowResize
        {
            get { return _allow_resize; }
            set
            {
                if(_allow_resize != value)
                {
                    _allow_resize = value;
                    if (_allow_resize)
                        SizeChangeRectangle.Visibility = System.Windows.Visibility.Visible;
                    else
                        SizeChangeRectangle.Visibility = System.Windows.Visibility.Collapsed;
                }
            }

        }

        public Grid BaseGrid
        {
            get
            {
                return base_grid;
            }
        }

        public AnimatingTilePanel AnimatingTilePanel
        {
            get
            {
                return _animating_tile_panel;
            }
        }

        public BaseFrameBase<B, S, T, U, O, SU> BaseFrame
        {
            get;
            set;
        }

        /// <summary>
        /// das submenu panel ist verbunden mit einem togglebutton
        /// dieser sollte in der taskbar aufscheinen
        /// </summary>
        public ToggleButton ToggleButtonReference
        {
            get;
            set;
        }

        public LayoutMode ActiveLayoutMode
        {
            get { return _active_layout_mode; }
            set
            {
                if (value == _active_layout_mode)
                    return;

                _active_layout_mode = value;
                UnRegisterLayoutCheckedEvents();
                switch (_active_layout_mode)
                {
                    case LayoutMode.Kachel:
                        OrderLayout1Button.IsChecked = true;
                        OrderLayout2Button.IsChecked = false;
                        OrderLayout3Button.IsChecked = false;

                        SwitchToKachel();
                        break;
                    case LayoutMode.Line:
                        OrderLayout1Button.IsChecked = false;
                        OrderLayout2Button.IsChecked = true;
                        OrderLayout3Button.IsChecked = false;

                        SwitchToLine();
                        break;
                    case LayoutMode.Grid:
                        OrderLayout1Button.IsChecked = false;
                        OrderLayout2Button.IsChecked = false;
                        OrderLayout3Button.IsChecked = true;

                        SwitchToGrid();
                        break;
                    default:
                        throw new Exception("unknown layout mode");
                }
                RegisterLayoutCheckedEvents();
            }
        }


        public Thickness ItemMargin
        {
            set;
            get;
        }

        public Size BigItemSize
        {
            get
            {
                return _big_item_size;
            }
            set
            {
                _big_item_size = value;
                if (_active_layout_mode == LayoutMode.Line)
                {
                    if (_animating_tile_panel != null)
                    {
                        _animating_tile_panel.ItemWidth = _big_item_size.Width;
                        _animating_tile_panel.ItemHeight = _big_item_size.Height;
                    }
                }
            }
        }

        public bool ShowActionsByLeftMouse
        {
            set;
            get;
        }

        public Size SmallItemSize
        {
            get
            {
                return _small_item_size;
            }
            set
            {
                _small_item_size = value;
                if (_active_layout_mode == LayoutMode.Kachel)
                {
                    if (_animating_tile_panel != null)
                    {
                        _animating_tile_panel.ItemWidth = _small_item_size.Width;
                        _animating_tile_panel.ItemHeight = _small_item_size.Height;
                    }
                }
            }
        }

        public double PanelHeight
        {
            get
            {
                //if (_custom_panel_size)
                //{
                //    return submenu_base_canvas.ActualHeight;
                //}

                if (_active_layout_mode == LayoutMode.Line)
                {
                    double height = BigItemSize.Height + SubHeaderStack.Height;
                    CheckPanelHeight(ref height);
                    return height;
                }
                else if (_active_layout_mode == LayoutMode.Kachel)
                {
                    double panel_width = GetPanelWidth();

                    int items_per_row = (int)Math.Floor(panel_width / SmallItemSize.Width);
                    int rows = (int)Math.Ceiling((double)_items_filtered.Count / (double)items_per_row);
                    double height = (SmallItemSize.Height * rows) + SubHeaderStack.Height;
                    CheckPanelHeight(ref height);
                    return height;
                }
                else if (_active_layout_mode == LayoutMode.Grid)
                {
                    double height = 400;
                    CheckPanelHeight(ref height);
                    return height;
                }
                else
                    throw new Exception("Layout mode unknown");
            }
        }

        protected double GetPanelWidth()
        {
            if (_resize_active_new_size.HasValue)
                return _resize_active_new_size.Value.Width;

            double panel_width = SubmenuBaseCanvas.ActualWidth;
            if (panel_width == 0)
            {
                if (Window != null && Window.ActualWidth > 0)
                    panel_width = Window.ActualWidth;
                else
                    panel_width = 1000;
            }
            return panel_width;
        }


        /// <summary>
        /// dont use this list for writing
        /// instead use method additem
        /// removeitem
        /// </summary>
        public ObservableCollection<SU> Items
        {
            get
            {
                return _items;
            }
        }

        public ReadOnlyObservableCollection<SU> RootItems
        {
            get
            {
                return _root_items_readonly;
            }
        }

        public bool IsAnimationActive
        {
            get
            {
                return _animation_active;
            }
        }


        #endregion

        #region Items Helper Methods
        private void DeselectAllItems()
        {
            foreach (SU item in _items)
            {
                item.IsSelected = false;
                item.ManualIsMouseOver = false;
                if (ShowActionsByLeftMouse)
                    item.HideActionMenu = true;
            }
        }
        #endregion

        #region Dependencyproeprty changed callbacks

        private static void OnCurrentBreadCrumbItem(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is S)
            {
                S panel = d as S;

                if (e.OldValue is ISubMenuPanelBreadCrumbItem)
                {
                    ISubMenuPanelBreadCrumbItem old_bread_crumb_item = e.OldValue as ISubMenuPanelBreadCrumbItem;

                    if (old_bread_crumb_item.MenuItem != null)
                    {
                        panel.DeRegisterSubCollectionChanged(old_bread_crumb_item.MenuItem);
                    }
                }

                if (e.NewValue is ISubMenuPanelBreadCrumbItem)
                {
                    ISubMenuPanelBreadCrumbItem new_bread_crumb_item = e.NewValue as ISubMenuPanelBreadCrumbItem;

                    if (new_bread_crumb_item.MenuItem != null)
                    {
                        panel.RegisterSubCollectionChanged(new_bread_crumb_item.MenuItem);
                    }
                }
            }
        }
        #endregion

        #region MouseClick Simulation

        private void base_grid_StylusDown(object sender, StylusDownEventArgs e)
        {

            _stylus_down = true;

            if (_stylus_down_id == null)
            {
                _stylus_down_id = e.StylusDevice.Id;
                _stylusdown_pos = e.GetPosition(base_grid);
            }

            _mouse_down = true;
            _mousedown_pos = e.GetPosition(base_grid);

            e.Handled = true;
        }

        private void base_grid_StylusUp(object sender, StylusEventArgs e)
        {
            return;
            if (_reset_stylus)
            {
                e.Handled = true;
                _reset_stylus = false;
            }

            if (_stylus_down && _stylus_down_id.HasValue && _stylus_down_id.Value == e.StylusDevice.Id)
            {
                e.Handled = true;
                _stylus_down = false;
                _stylus_down_id = null;

                Point current_pos = e.GetPosition(base_grid);

                if ((current_pos - _stylusdown_pos).Length > 5)
                    return;

                _stylusdown_pos = new Point(0, 0);

                SU item;
                CollisionDetected(current_pos, out item);


                if (item != null)
                {
                    ToggleButton external_tg = null;
                    if (IsExternalToggleButton(item, out external_tg))
                        external_tg.IsChecked = external_tg.IsChecked == null ? false : !external_tg.IsChecked.Value;
                    else
                    {
                        CheckSubNavigation(item, null);
                        RaiseAction(item);
                    }

                    //if (ShowActionsByLeftMouse && e.ChangedButton == MouseButton.Right && e.RightButton == MouseButtonState.Released)
                    //{
                    //    item.HideActionMenu = !item.HideActionMenu;
                    //    item.ManualIsMouseOver = true;
                    //    item.IsHitTestVisible = true;
                    //}
                    //else
                    //{
                    //    // Sonderfall, wenn das Control direkt ein ToggleButton ist. Ist nicht schön. Sollte eigentlich ausgelagert werden
                    //    if ((e.ChangedButton == MouseButton.Left && e.LeftButton == MouseButtonState.Released) ||
                    //        (!ShowActionsByLeftMouse && e.ChangedButton == MouseButton.Right && e.RightButton == MouseButtonState.Released && !item.HasChildren))
                    //    {
                    //        ToggleButton external_tg = null;
                    //        if (IsExternalToggleButton(item, out external_tg))
                    //            external_tg.IsChecked = external_tg.IsChecked == null ? false : !external_tg.IsChecked.Value;
                    //        else
                    //        {
                    //            CheckSubNavigation(item, null);
                    //            RaiseAction(item);
                    //        }
                    //    }

                    //    if (!ShowActionsByLeftMouse && e.ChangedButton == MouseButton.Right && e.RightButton == MouseButtonState.Released)
                    //        CheckSubNavigation(item, null);
                    //}
                }
            }
        }

        private void base_grid_StylusMove(object sender, StylusEventArgs e)
        {

            if (_stylus_down && _stylus_down_id.HasValue && _stylus_down_id.Value == e.StylusDevice.Id)
            {
                e.Handled = true;
                Point current_pos = e.GetPosition(base_grid);

                Vector pos_diff = current_pos - _last_mousemove_pos;

                if (pos_diff.X == 0 && pos_diff.Y == 0)
                    return;

                if ((current_pos - _stylusdown_pos).Length > 5)
                {
                    _stylus_down = false;
                    _stylus_down_id = null;
                    _reset_stylus = true;
                }

                _last_mousemove_pos = current_pos;
            }
        }

        private void base_grid_MouseUp(object sender, MouseButtonEventArgs e)
        {
            Point pos = e.GetPosition(base_grid);

            // Bei einer großen Positionsveränderung, wird das Mouseup ignoriert und nicht an die Elemente weitergereicht
            if ((_mousedown_pos - pos).Length > 5 && _mouse_down)
            {
                _mouse_down = false;
                _stylus_down = false;
                _stylus_down_id = null;
                return;
            }
            _stylus_down = false;
            _stylus_down_id = null;
            _mouse_down = false;
            SU item;
            CollisionDetected(pos, out item);


            if (item != null)
            {
                if (ShowActionsByLeftMouse && e.ChangedButton == MouseButton.Right && e.RightButton == MouseButtonState.Released)
                {
                    item.HideActionMenu = !item.HideActionMenu;
                    item.ManualIsMouseOver = true;
                    item.IsHitTestVisible = true;
                }
                else
                {
                    // Sonderfall, wenn das Control direkt ein ToggleButton ist. Ist nicht schön. Sollte eigentlich ausgelagert werden
                    if ((e.ChangedButton == MouseButton.Left && e.LeftButton == MouseButtonState.Released) ||
                        (!ShowActionsByLeftMouse && e.ChangedButton == MouseButton.Right && e.RightButton == MouseButtonState.Released && !item.HasChildren))
                    {
                        ToggleButton external_tg = null;
                        if (IsExternalToggleButton(item, out external_tg))
                            external_tg.IsChecked = external_tg.IsChecked == null ? false : !external_tg.IsChecked.Value;
                        else
                        {
                            CheckSubNavigation(item, null);
                            RaiseAction(item);
                        }
                    }

                    if (!ShowActionsByLeftMouse && e.ChangedButton == MouseButton.Right && e.RightButton == MouseButtonState.Released)
                        CheckSubNavigation(item, null);
                }
            }
        }

        private bool IsExternalToggleButton(SU item, out ToggleButton o_tg)
        {
            o_tg = null;

            if (item.Child is Grid)
            {
                Grid g = item.Child as Grid;
                if (g.Children.Count > 0)
                {
                    foreach (var child in g.Children)
                    {
                        if (child is ToggleButton)
                            o_tg = child as ToggleButton;
                    }
                }
            }

            return o_tg != null;
        }

        private void base_grid_MouseMove(object sender, MouseEventArgs e)
        {
            Point pos = e.GetPosition(base_grid);
            Vector pos_diff = pos - _last_mousemove_pos;

            if (pos_diff.X == 0 && pos_diff.Y == 0)
                return;

            SU item;
            CollisionDetected(pos, out item);

            if (item != null)
                SubmenuBaseGrid.Cursor = Cursors.Hand;
            else
                SubmenuBaseGrid.Cursor = Cursors.ScrollAll;

            if (!_mouse_down) // IsMouseOver setter
            {
                if (item != _last_hover_element && _last_hover_element != null)
                    _last_hover_element.ManualIsMouseOver = false;

                if (item != null)
                {
                    _last_hover_element = item;
                    if (!ShowActionsByLeftMouse)
                    {
                        _last_hover_element.IsHitTestVisible = true;
                    }

                    _last_hover_element.ManualIsMouseOver = true;
                }
            }

            _last_mousemove_pos = pos;
        }

        private void base_grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            _mouse_down = true;
            _mousedown_pos = e.GetPosition(base_grid);
        }

        private void base_grid_MouseLeave(object sender, MouseEventArgs e)
        {
            _mouse_down = false;
        }

        private void CollisionDetected(Point rel_mouse_pos, out SU found_item)
        {
            Vector2d mouse_pos = new Vector2d(rel_mouse_pos.X, rel_mouse_pos.Y);
            found_item = null;

            foreach (SU sub_menu_panel_menu_item in _items_filtered)
            {
                Point elem_pos = UIHelper.GetElementPosition(sub_menu_panel_menu_item, base_grid);


                double bottom = elem_pos.Y + sub_menu_panel_menu_item.ActualHeight * _map_zp.ScaleTransform.ScaleY;
                double right = elem_pos.X + sub_menu_panel_menu_item.ActualWidth * _map_zp.ScaleTransform.ScaleX;

                BoundingBox bbox = new BoundingBox(new Vector2d(elem_pos.X, elem_pos.Y), new Vector2d(right, bottom));

                if (found_item == null && bbox.PointInside(mouse_pos))
                {
                    found_item = sub_menu_panel_menu_item;
                }
                else if (ShowActionsByLeftMouse)
                    sub_menu_panel_menu_item.HideActionMenu = true;
                else
                    sub_menu_panel_menu_item.HideActionMenu = false;

                //sub_menu_panel_menu_item.IsSelected = false;

            }
        }

        void _items_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (_ignore_collection_change)
                return;

            if (CurrentBreadCrumbItem == _root_bread_crumb_item)
            {
                // if the collection of the root was modified
                // sync the collection changes with the root store
                if (e.NewItems != null)
                {
                    foreach (SU new_item in e.NewItems)
                    {
                        if (!_root_items.Contains(new_item))
                            _root_items.Add(new_item);
                    }
                }

                if (e.OldItems != null)
                {
                    foreach (SU del_item in e.OldItems)
                    {
                        if (_root_items.Contains(del_item))
                            _root_items.Remove(del_item);
                    }
                }
            }
            else
            {
                // if a sub item's collection has been modified
                // sync the changes to the submenu item
                if (CurrentBreadCrumbItem != null && CurrentBreadCrumbItem.MenuItem != null)
                {
                    if (e.NewItems != null)
                    {
                        foreach (SU new_item in e.NewItems)
                        {
                            if (!CurrentBreadCrumbItem.MenuItem.Children.Contains(new_item))
                                CurrentBreadCrumbItem.MenuItem.Children.Add(new_item);
                        }
                    }

                    if (e.OldItems != null)
                    {
                        foreach (SU del_item in e.OldItems)
                        {
                            if (CurrentBreadCrumbItem.MenuItem.Children.Contains(del_item))
                                CurrentBreadCrumbItem.MenuItem.Children.Remove(del_item);
                        }
                    }
                }
            }
        }

        public void DeRegisterSubCollectionChanged(ISubMenuPanelMenuItem sub_item)
        {
            if (sub_item == null)
                return;

            sub_item.Children.CollectionChanged -= SubMenuItem_Children_CollectionChanged;
        }

        private void RegisterSubCollectionChanged(ISubMenuPanelMenuItem sub_item)
        {
            if (sub_item == null)
                return;

            sub_item.Children.CollectionChanged += new System.Collections.Specialized.NotifyCollectionChangedEventHandler(SubMenuItem_Children_CollectionChanged);
        }

        void SubMenuItem_Children_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            _ignore_collection_change = true;

            // synchronize submenu item children with current items
            if (e.NewItems != null)
            {
                foreach (SU new_item in e.NewItems)
                {
                    if (!_items.Contains(new_item))
                    {
                        AddItem(new_item);
                        //_items.Add(new_item);

                        //if (ItemCountFilterExpression == null)
                        //{
                        //    ItemCount = ItemCount + 1;
                        //}
                        //else
                        //{
                        //    // if a IteCountFilterExpression has been set
                        //    // check if the current item 
                        //    if(ItemCountFilterExpression(new_item))
                        //    {
                        //        ItemCount = ItemCount + 1;
                        //    }
                        //}

                        //if (ShowActionsByLeftMouse)
                        //    new_item.HideActionMenu = true;
                        //else
                        //    new_item.HideActionMenu = false;

                        //GenerateDataGridColumns();

                        //AdjustCustomFilters(new_item);

                        //if (!WithSearchField || string.IsNullOrEmpty(search_cbx.Text) || IsFiltered(new_item))
                        //{
                        //    _items_filtered.Add(new_item);
                        //    _items_filtered.Sort((x, y) => x.CompareTo(y));
                        //}

                        //if (ItemAdded != null)
                        //    ItemAdded(this, new MenuItemManagementEventArgs(new_item));

                        //UpdateMapZPBorders();
                    }
                }
            }

            if (e.OldItems != null)
            {
                foreach (SU del_item in e.OldItems)
                {
                    if (_items.Contains(del_item))
                    {
                        RemoveItem(del_item);
                        //_items.Remove(del_item);

                        //if (ItemCountFilterExpression == null)
                        //{
                        //    ItemCount = ItemCount - 1;
                        //}
                        //else
                        //{
                        //    // if a IteCountFilterExpression has been set
                        //    // check if the current item 
                        //    if(ItemCountFilterExpression(del_item))
                        //    {
                        //        ItemCount = ItemCount - 1;
                        //    }
                        //}

                        //if (!WithSearchField || string.IsNullOrEmpty(search_cbx.Text) || IsFiltered(del_item))
                        //{
                        //    if(_items_filtered.Contains(del_item))
                        //    {
                        //        _items_filtered.Remove(del_item);
                        //        _items_filtered.Sort((x, y) => x.CompareTo(y));
                        //    }
                        //}

                        //UpdateMapZPBorders();
                    }
                }
            }

            _ignore_collection_change = false;
        }

        

        public void NavigateToSubItems(ISubMenuPanelMenuItem menu_item)
        {
            SU cast_elem = menu_item as SU;
            bool need_to_recrate_breadcrumb = false;

            if (!ContainsItem(cast_elem))
                return;

            if (!_items.Contains(cast_elem))
            {
                // wenn dieses item nicht in der aktuellen items-collection ist
                // dann muss das gesamte breadcrumb control neu aufgebaut werden
                need_to_recrate_breadcrumb = true;
            }

            ISubMenuPanelBreadCrumbItem bread_crumb_item = _breadcrumb_path.SingleOrDefault(i => i.MenuItem == menu_item);

            Mouse.OverrideCursor = Cursors.Wait;

            if (need_to_recrate_breadcrumb)
            {
                List<ISubMenuPanelBreadCrumbItem> rem_items = _breadcrumb_path.Where(bi => bi != _root_bread_crumb_item).ToList();

                foreach (ISubMenuPanelBreadCrumbItem rem_item in rem_items)
                {
                    rem_item.Click -= BreadCrumbButton_Click;
                    _breadcrumb_path.Remove(rem_item);
                }

                _ignore_collection_change = true;

                ISubMenuPanelBreadCrumbItem new_bread_crumb_item = CreateBreadCrumbItem(this, menu_item.ToString(), menu_item);
                new_bread_crumb_item.Click += new RoutedEventHandler(BreadCrumbButton_Click);

                ISubMenuPanelMenuItem parent_item = menu_item.ParentItem;

                while(parent_item != null)
                {
                    ISubMenuPanelBreadCrumbItem new_bread_crumb_parent_item = CreateBreadCrumbItem(this, parent_item.ToString(), parent_item);
                    new_bread_crumb_parent_item.Click += new RoutedEventHandler(BreadCrumbButton_Click);

                    _breadcrumb_path.Insert(1, new_bread_crumb_parent_item);

                    parent_item = parent_item.ParentItem;
                }

                _breadcrumb_path.Add(new_bread_crumb_item);

                RemoveAllItemsOfCurrentBreadcrumbLayer();

                foreach (SU root_item in new_bread_crumb_item.MenuItem.Children)
                    AddItem(root_item);

                CurrentBreadCrumbItem = new_bread_crumb_item;
                LbBreadCrumb.ItemsSource = _breadcrumb_path;

                CurrentBreadCrumbItem = bread_crumb_item;
            }
            else
            {
                if (bread_crumb_item != null)
                {
                    // item existiert schon
                    _ignore_collection_change = true;

                    RemoveAllItemsOfCurrentBreadcrumbLayer();

                    foreach (SU root_item in bread_crumb_item.MenuItem.Children)
                        AddItem(root_item);


                    CurrentBreadCrumbItem = bread_crumb_item;

                    _ignore_collection_change = false;
                }
                else
                {
                    if (menu_item != null)
                    {
                        ISubMenuPanelBreadCrumbItem new_bread_crumb_item = CreateBreadCrumbItem(this, menu_item.ToString(), menu_item);
                        new_bread_crumb_item.Click += new RoutedEventHandler(BreadCrumbButton_Click);

                        _ignore_collection_change = true;

                        RemoveAllItemsOfCurrentBreadcrumbLayer();


                        foreach (SU root_item in menu_item.Children)
                            AddItem(root_item);

                        _breadcrumb_path.Add(new_bread_crumb_item);

                        CurrentBreadCrumbItem = new_bread_crumb_item;
                        LbBreadCrumb.ItemsSource = _breadcrumb_path;

                        _ignore_collection_change = false;
                    }
                }
            }

            Mouse.OverrideCursor = null;

            UpdateBreadCrumbVisibility();
            SetSearchFilter(string.Empty); // clear search filter
            SendNavigationDepthChanged();
        }

        private void BreadCrumbButton_Click(object sender, RoutedEventArgs e)
        {
            Button sender_btn = sender as Button;

            if (sender_btn != null)
            {
                ISubMenuPanelBreadCrumbItem bread_crumb_item = sender_btn.CommandParameter as ISubMenuPanelBreadCrumbItem;

                Mouse.OverrideCursor = Cursors.Wait;

                if (bread_crumb_item != null)
                {
                    if (bread_crumb_item == _root_bread_crumb_item)
                    {
                        _ignore_collection_change = true;

                        RemoveAllItemsOfCurrentBreadcrumbLayer();


                        foreach (SU root_item in _root_items)
                            AddItem(root_item);


                        SetSearchFilter(string.Empty); // clear search filter
                        ApplyFilterText(false);
                        ApplySort();

                        CurrentBreadCrumbItem = _root_bread_crumb_item;

                        _ignore_collection_change = false;
                    }
                    else
                    {
                        _ignore_collection_change = true;

                        if (bread_crumb_item.MenuItem != null)
                        {
                            RemoveAllItemsOfCurrentBreadcrumbLayer();

                            _items.Clear();
                            _items_filtered.Clear();

                            foreach (SU root_item in bread_crumb_item.MenuItem.Children)
                                AddItem(root_item);


                            //ApplyFilterText(_was_custom_filter);
                            //ApplySort();
                        }
                        else
                        {
                            _items.Clear();
                            _items_filtered.Clear();
                        }

                        CurrentBreadCrumbItem = bread_crumb_item;

                        _ignore_collection_change = false;
                    }
                }

                Mouse.OverrideCursor = null;

                UpdateBreadCrumbVisibility();
                SetSearchFilter(string.Empty); // clear search filter
                SendNavigationDepthChanged();
            }
        }
        #endregion

        #region Attributes
        protected delegate void NoParas();
        private bool _resize_active;
        private bool _animation_active;
        private bool _autohide_timer_after_action;
        private bool _ignore_autohide_after_action;
        private bool _allow_resize = true;
        //private bool _resize_active;

        private bool _with_custom_breadcrumb_item_creation = false;

        private Timer _auto_hide_delay_timer = null;
        private double _auto_hide_timer_interval = 450;

        private DoubleAnimation _submenu_fadein_da;
        private DoubleAnimation _submenu_fadeout_da;
        //private bool _custom_panel_size;
        private double _start_height_resize;
        private Point _submenu_resize_start_pos;
        private bool _with_auto_hide;
        private LayoutMode _active_layout_mode;

        private Size? _resize_active_new_size;

        public event EventHandler FadeOutCompleted;
        public event EventHandler FadeInCompleted;
        public event EventHandler<SubMenuPanelMenuItem.MenuItemActionEventArgs> Action;
        public event EventHandler NewElemClick;
        public event EventHandler NavigationDepthChanged;
        public event EventHandler CustomButtonClick;
        public event EventHandler CustomButton1Click;
        public event EventHandler SearchButtonClick;
        public event EventHandler<SubMenuPanelMenuItem.MenuItemManagementEventArgs> ItemRemoved;
        public event EventHandler<SubMenuPanelMenuItem.MenuItemManagementEventArgs> ItemAdded;

        public event EventHandler<CreateSubMenuBreadCrumbItemEventArgs> RequestBreadCrumbItemCreation;

        protected Size _big_item_size;
        protected Size _small_item_size;

        protected ObservableCollection<SU> _items = new ObservableCollection<SU>();
        protected ObservableCollection<SU> _items_filtered = new ObservableCollection<SU>();
        protected ObservableCollection<SU> _root_items = new ObservableCollection<SU>();
        protected ReadOnlyObservableCollection<SU> _root_items_readonly = null;

        protected AnimatingTilePanel _animating_tile_panel;

        private bool _with_navigation_buttons = true;

        protected MapZP _map_zp;
        protected DoubleAnimation _scale_x_reset;
        protected DoubleAnimation _scale_y_reset;
        protected DoubleAnimation _transl_x_reset;
        protected DoubleAnimation _transl_y_reset;

        protected Point _mousedown_pos;
        private bool _mouse_down = false;
        private SU _last_hover_element = null;
        private Point _last_mousemove_pos = new Point();

        private bool _stylus_down = false;
        private int? _stylus_down_id = null;
        protected Point _stylusdown_pos;
        private bool _reset_stylus = false;

        private List<string> _custom_filter_values = new List<string>();
        private bool _grid_columns_generated = false;
        private bool _comes_from_parent_row = false;
        private bool _ignore_grid_selection_change = false;
        private string _last_action_button_name = "";

        private bool _was_custom_filter = false;
        private bool _ignore_collection_change = false;
        private string _taskbar_item_name = string.Empty;
        private ISubMenuPanelBreadCrumbItem _root_bread_crumb_item = null;
        private ObservableCollection<ISubMenuPanelBreadCrumbItem> _breadcrumb_path = new ObservableCollection<ISubMenuPanelBreadCrumbItem>();

        #region Everything for finding Xaml Elements
        private Rectangle _size_change_rectangle;
        private Grid _base_grid;
        private ListBox _lb_bread_crumb;
        private TouchButton _arrow_left_button;
        private TouchButton _arrow_right_button;
        private TouchButton _arrow_up_button;
        private TouchButton _arrow_down_button;
        private TouchToggleButton _order_layout1_button;
        private TouchToggleButton _order_layout2_button;
        private TouchToggleButton _order_layout3_button;
        private TouchToggleButton _pin_button;
        private TouchButton _newelem_button;
        private TouchButton _custom_button1;
        private TouchButton _template_button;
        private TouchButton _custom_button;
        private TouchButton _search_button;
        private ItemsControl _animating_tile_panel_items_control;
        private Grid _submenu_base_grid;
        private Canvas _submenu_base_canvas;
        private StackPanel _sub_bread_crumb_stack;
        private StackPanel _sub_header_stack;
        private DataGrid _submenu_items_datagrid;
        private Border _submenu_border;
        private AcCbx _search_cbx;
        private Rectangle _newelem_rect;
        private Rectangle _search_rect;
        private Rectangle _custom_rect1;
        private Rectangle _custom_rect;
        private CommandBinding _comm_left;
        private CommandBinding _comm_right;
        #endregion
        #endregion




    }
}
