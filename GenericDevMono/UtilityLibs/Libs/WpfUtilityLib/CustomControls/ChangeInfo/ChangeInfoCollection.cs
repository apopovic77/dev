﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;

namespace Logicx.WpfUtility.CustomControls.ChangeInfo
{
    public class ChangeInfoCollection : ObservableCollection<ChangeInfo>
    {
         #region Construction and initialisation
        /// <summary>
        /// Constructor of the class
        /// </summary>
        static ChangeInfoCollection()
        {
            Empty = new ReadOnlyObservableCollection<ChangeInfo>(new ChangeInfoCollection());
        }
        #endregion

        #region Operations

        /// <summary>
        /// Finds an error for a control
        /// </summary>
        /// <param name="control"></param>
        /// <returns></returns>
        private int FindChangeForControl(DependencyObject control)
        {
            for (int i = 0; i < base.Count; i++)
            {
                if (base[i].SourceControl == control)
                {
                    return i;
                }
            }
            return -1;
        }

        /// <summary>
        /// Inserts an item
        /// </summary>
        /// <param name="index"></param>
        /// <param name="item"></param>
        protected override void InsertItem(int index, ChangeInfo item)
        {
            base.InsertItem(index, item);
        }



        /// <summary>
        /// Gets the readonly error collection for a dependency object
        /// </summary>
        /// <param name="d"></param>
        /// <returns></returns>
        internal static ReadOnlyObservableCollection<ChangeInfo> GetReadOnlyErrors(DependencyObject d)
        {
            ChangeInfoCollection errorsInternal = ChangeInformationProvider.GetChangesInternal(d);
            if (errorsInternal == null)
            {
                return Empty;
            }
            if (errorsInternal._readonly_wrapper == null)
            {
                errorsInternal._readonly_wrapper = new ReadOnlyObservableCollection<ChangeInfo>(errorsInternal);
            }
            return errorsInternal._readonly_wrapper;
        }


        #endregion

        #region Properties
        /// <summary>
        /// Empty collection
        /// </summary>
        public static readonly ReadOnlyObservableCollection<ChangeInfo> Empty;
        #endregion

        #region Attribs
        private ReadOnlyObservableCollection<ChangeInfo> _readonly_wrapper;
        #endregion
    }
}
