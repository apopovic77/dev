﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Logicx.WpfUtility.CustomControls.OverlayManager
{
    /// <summary>
    /// Interaction logic for OverlayInputBoxText.xaml
    /// </summary>
    public partial class OverlayInputBoxText // : IOverlayContent
    {
        public OverlayInputBoxText()
        {
            InitializeComponent();

            DialogTitle = "Texteingabe";
            DialogSubTitle = "Bitte geben Sie ihren Text ein";

            AddButton(new OverlayDialogButton("Ok", OverlayButtonAlignment.Right, Key.Enter));
            AddButton(new OverlayDialogButton("Abbrechen", OverlayButtonAlignment.Right, Key.Escape));

            DefaultWindowState = OverlayDialogState.Modal;
            ModalBehaviour = OverlayContent.ModalBehaviourEnum.DarkenBackground;
            InitializationBehaviour = OverlayContent.DataInitializationBehaviourEnum.Immediate;

        }

        #region IOverlayContent interface implementation

        public bool Show(bool firstTime)
        {
            return true;
        }

        public bool Close(bool forceClose)
        {
            return true;
        }

        public bool Print()
        {
            return true;
        }

        public bool Help()
        {
            return true;
        }

        /// <summary>
        /// Eigenschaft welche definiert ob der content das Drucken unterstützt oder nicht
        /// </summary>
        public bool CanPrint
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Eigenschaft welche dem Hostdialog mitteilt ob der Content geschlossen werden kann.
        /// </summary>
        public bool CanClose
        {
            get
            {
                return true;
            }
        }

        public bool CanHelp
        {
            get
            {
                return false;
            }
        }

        public bool AutoResized
        {
            get { return true;}
        }

        #endregion

        #region Properties
        public string InputText
        {
            get { return textbox_input.Text; }
            set { textbox_input.Text = value; }
        }

        public string OriginalText
        {
            get { return _original_text; }
            set { _original_text = value; }
        }

        public string DescriptionText
        {
            get { return lbl_text.Text; }
            set
            {
                lbl_text.Text = value;
                if (string.IsNullOrEmpty(value))
                    lbl_text.Visibility = System.Windows.Visibility.Collapsed;
                else
                    lbl_text.Visibility = System.Windows.Visibility.Visible;
            }
        }
        #endregion

        #region Virtuelle methoden welche überschrieben werden können
        /// <summary>
        /// Wird aufgerufen, wenn der Benutzer einen der OverlayButtons klicked
        /// </summary>
        /// <param name="button"></param>
        public override bool OverlayButtonClicked(OverlayDialogButton button)
        {
            if (button.Content == "Ok")
            {
                // OK - aktuellen dialog schliessen (nicht erzwingen)
                if (OkClicked != null)
                {
                    OkClicked(this, new EventArgs());
                }
                else
                {
                    if (OverlayManager != null)
                        OverlayManager.HideContent(this, false);
                }
            }

            if (button.Content == "Abbrechen")
            {
                // abbrechen - aktuellen dialog schliessen (erzwingen)
                if (CancelClicked != null)
                {
                    CancelClicked(this, new EventArgs());
                }
                else
                {
                    if (OverlayManager != null)
                         OverlayManager.HideContent(this, true);
                }
            }

            return true;
        }

        /// <summary>
        /// Wurd aufgerufen bevor das Control angezeigt wird
        /// </summary>
        /// <param name="firstTime">true wenn dieser Content in der Dialoginstanz das erste Mal angezeigt wird</param>
        /// <returns>false wenn das control nicht angezeigt werden soll.</returns>
        public override bool BeforeShow(bool firstTime)
        {
            return true;
        }
        /// <summary>
        /// Wird aufgerufen nachdem das Control angezeigt wurde
        /// </summary>
        public override void AfterShow()
        {
            textbox_input.Focus();
        }

        /// <summary>
        /// Wird aufgerufen bevor das control geschlossen wird
        /// </summary>
        /// <param name="forceClose">True wenn das Control auf jeden Fall geschlossen wird.</param>
        /// <returns>false wenn das Control nicht geschlossen werden kann.</returns>
        /// <remarks>Wenn <see cref="forceClose"/> True ist, dann wird der 
        /// Rückgabewert ignoriert und das Control auf jeden Fall geschlossen.</remarks>
        public override bool BeforeClose(bool forceClose)
        {
            return true;
        }
        /// <summary>
        /// Wird aufgerufen nachdem das Control geschlossen wurde
        /// </summary>
        public override void AfterClose()
        {

        }

        /// <summary>
        /// Wurd aufgerufen bevor das Control gedruckt wird wird
        /// </summary>
        /// <returns>false wenn das control nicht gedruckt werden soll.</returns>
        public override bool BeforePrint()
        {
            return true;
        }
        /// <summary>
        /// wird aufgerufen nachdem das Control gedruckt wurde
        /// </summary>
        public override void AfterPrint()
        {

        }
        #endregion

        #region Operations
        public void SetMultiLineEditMode()
        {
            textbox_input.AcceptsReturn = true;
            textbox_input.AllowDrop = true;
            textbox_input.Height = 100;
        }

        public void SetSingleLineEditMode()
        {
            textbox_input.AcceptsReturn = false;
            textbox_input.AllowDrop = true;
            textbox_input.ClearValue(HeightProperty);
        }
        #endregion

        #region Attribs
        public event EventHandler OkClicked;
        public event EventHandler CancelClicked;

        private string _original_text = "";
        private string _description_text = "";
        #endregion
    }
}
