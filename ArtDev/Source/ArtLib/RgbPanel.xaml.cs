﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ArtLib
{
    /// <summary>
    /// Interaction logic for RgbPanel.xaml
    /// </summary>
    public partial class RgbPanel : UserControl
    {
        public RgbPanel()
        {
            InitializeComponent();
            FillPanel();




            this.Loaded += OnLoaded;
        }

        private void OnLoaded(object sender, RoutedEventArgs routed_event_args)
        {
            //Window w = WpfHelper.FindAncestor<Window>(this);
            //w.KeyUp += KeyUp;
        }

        void KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.F2)
            {
                scale_transform.ScaleX = 50;
                scale_transform.ScaleY = 50;
            }

            if (e.Key == Key.F1)
            {
                scale_transform.ScaleX = 1;
                scale_transform.ScaleY = 1;
            }

        }


        public void FillPanel()
        {
            panel.Children.Clear();

            double rect_widht = 3;
            double recht_height = 3;


            for (int i = 0; i < 50000; i++)
            {

                panel.Children.Add(GetRect(rect_widht, recht_height, Color.FromRgb(0xFF, 0x00, 0x00)));
                panel.Children.Add(GetRect(rect_widht, recht_height, Color.FromRgb(0x00, 0xFF, 0x00)));
                panel.Children.Add(GetRect(rect_widht, recht_height, Color.FromRgb(0x00, 0x00, 0xFF)));



                //Rgb rgb = new Rgb();
                //panel.Children.Add(rgb);
            }
        }

        public Rectangle GetRect(double width, double height, Color c)
        {
            Rectangle rect1 = new Rectangle();
            rect1.Width = width;
            rect1.Height = height;
            rect1.Fill = new SolidColorBrush(c);
            return rect1;
        }
    }
}
