using System.ComponentModel;
using System.Windows.Forms;

namespace Logicx.DirectX.Management
{
    partial class DirectXPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        protected BackgroundWorker backgroundworker;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.backgroundworker = new System.ComponentModel.BackgroundWorker();
            this.SuspendLayout();
            // 
            // DirectXPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DirectXPanel";
            this.Size = new System.Drawing.Size(640, 480);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.DirectXPanel_MouseMove);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.DirectXPanel_KeyUp);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.DirectXPanel_MouseDown);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.DirectXPanel_MouseUp);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DirectXPanel_KeyDown);
            this.ResumeLayout(false);

        }
        #endregion
    }
}
