﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TouchSliderTest
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            List<int> zahlen = new List<int>() {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
            //slider_test_1.ItemsSource = zahlen;
            //slider_test_1.SelectionChanged += new SelectionChangedEventHandler(slider_test_1_SelectionChanged);

            this.Loaded += new RoutedEventHandler(MainWindow_Loaded);
        }

        void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            end_time_slider.Value = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 7, 0, 0);
        }

        void slider_test_1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //Debug.WriteLine("### SLIDER SELECTED ITEM: " + slider_test_1.SelectedItem);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //int value = string.IsNullOrEmpty(tb_value.Text) ? 0 : Int32.Parse(tb_value.Text);
            //numeric_slider.Value = value;
        }
    }
}
