﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Data;

namespace Logicx.WpfUtility.Converters
{
    [ValueConversion(typeof(double), typeof(double))]
    public class StringCharSplitterConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return value;

            string sRet = "";
            char[] chars = System.Convert.ToString(value).ToCharArray();
            int nIdx = 0;

            foreach (char c in chars)
            {
                if (nIdx > 0)
                    sRet += Environment.NewLine;
                sRet += c;

                nIdx++;
            }

            return sRet;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return value;

            return System.Convert.ToString(value).Replace(Environment.NewLine, "");
        }
    }
}
