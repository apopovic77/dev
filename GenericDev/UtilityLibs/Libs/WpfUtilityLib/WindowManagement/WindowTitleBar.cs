﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;
using System.Windows.Media;
using Logicx.WpfUtility.CustomControls.Combobox;
using Logicx.WpfUtility.CustomControls.OverlayManager;
using Logicx.WpfUtility.CustomControls.TouchButton;
using Logicx.WpfUtility.WindowManagement.Adorners;

namespace Logicx.WpfUtility.WindowManagement
{
    public class WindowTitleBar : WindowTitleBarBase<BaseFrame, SubMenuPanel, WindowTaskBar, WindowTitleBar, OverlayManager, SubMenuPanelMenuItem>
    {
        public WindowTitleBar(){}
    }
}
