﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Logicx.WpfUtility.PieMenu
{
    //public interface IPieMenuEntryBase
    //{
    //    void SetColor(string bg_pie_color, string bg_active_pie_color, string stroke_pie_color);
    //    void SetColor(Color bg_pie_color, Color bg_active_pie_color, Color stroke_pie_color);

    //    void UpdatePieStyle();
    //    string ToString();

    //    IPieMenuEntryBase Parent { set; get; }

    //    List<IPieMenuEntryBase> SubEntries { set; get; }

    //    /// <summary>
    //    /// winken in radianten
    //    /// </summary>
    //    double AnglePie { get; }


    //    Path Shape { set; get; }
    //    Path ShapeText { set; get; }
    //    Canvas ShapeTextBoundingBox { set; get; }
    //    bool IsHidden { set; get; }
    //    void UnHideChildren();

    //    string Name { set; get; }

    //    /// <summary>
    //    /// winkel start des pie elements in radianten
    //    /// </summary>
    //    double AngleStart { set; get; }

    //    /// <summary>
    //    /// winkel ende des pie elements in radianten
    //    /// </summary>
    //    double AngleEnd { set; get; }

    //    string Key { set; get; }

    //    bool HidesMenuOnClick { set; get; }
    //}

    [Serializable]
    public class PieMenuEntryBase<PE> where PE : PieMenuEntryBase<PE>
    {
        public PieMenuEntryBase(Color bg_active_c, string name)
        {
            Name = name;

            _bg_pie_active_color = bg_active_c;
            _bg_pie_color = DefaultColorBorderPie;
            _bg_pie_stroke_color = DefaultColorBorderPie;

            Init();
        }

        public PieMenuEntryBase(Color bg_active_c, string name, string key)
        {
            Name = name;
            Key = key;

            _bg_pie_active_color = bg_active_c;
            _bg_pie_color = DefaultColorBorderPie;
            _bg_pie_stroke_color = DefaultColorBorderPie;

            Init();
        }

        public PieMenuEntryBase(string name)
        {
            Name = name;

            _bg_pie_color = DefaultColorPieBackground;
            _bg_pie_active_color = DefaultColorPieBackgroundActive;
            _bg_pie_stroke_color = DefaultColorBorderPie;

            Init();
        }

        public PieMenuEntryBase(string name, string key)
        {
            Name = name;
            Key = key;

            _bg_pie_color = DefaultColorPieBackground;
            _bg_pie_active_color = DefaultColorPieBackgroundActive;
            _bg_pie_stroke_color = DefaultColorBorderPie;


            Init();
        }

        public void SetColor(string bg_pie_color, string bg_active_pie_color, string stroke_pie_color)
        {
            _bg_pie_color = ColorGenerator.HexToColor(bg_pie_color);
            _bg_pie_active_color = ColorGenerator.HexToColor(bg_active_pie_color);
            _bg_pie_stroke_color = ColorGenerator.HexToColor(stroke_pie_color);
            Init();
        }

        public void SetColor(Color bg_pie_color, Color bg_active_pie_color, Color stroke_pie_color)
        {
            _bg_pie_color = bg_pie_color;
            _bg_pie_active_color = bg_active_pie_color;
            _bg_pie_stroke_color = stroke_pie_color;
            Init();
        }

        protected void Init()
        {
            _pie_bg_brush = new SolidColorBrush(_bg_pie_color);
            _pie_stroke_brush = new SolidColorBrush();
            HidesMenuOnClick = true;
        }
        public void Add(PE entry)
        {
            if (SubEntries == null)
                SubEntries = new List<PE>();

            SubEntries.Add(entry);
            entry.Parent = this as PE;
        }

        public void UpdatePieStyle()
        {
            if (Shape == null || ShapeText == null)
                return;

            if (!IsEnabled)
            {
                Shape.Fill = Brushes.Transparent;
                Shape.Stroke = new SolidColorBrush(_bg_pie_color);
                ShapeText.Fill = Brushes.White;
                return;
            }

            if (IsHidden)
            {
                Shape.Visibility = System.Windows.Visibility.Hidden;
                ShapeText.Visibility = System.Windows.Visibility.Hidden;
            }
            else
            {
                Shape.Visibility = System.Windows.Visibility.Visible;
                ShapeText.Visibility = System.Windows.Visibility.Visible;
            }

            DoUpdatePieStyle();
        }

        protected virtual void DoUpdatePieStyle()
        {
            Shape.Fill = _pie_bg_brush;
            Shape.Stroke = _pie_stroke_brush;
        }



        protected virtual void pie_item_MouseLeave(object sender, MouseEventArgs e)
        {
            Path pie;
            if (sender is Path)
                pie = (Path)sender;
            //else if (sender is Canvas && _shape_text != null)
            //    pie = _shape_text;
            else if (Shape != null)
                pie = Shape;
            else
                return;

            if (!IsEnabled)
                return;
            Color c_target_anim = _bg_pie_color;
            ColorAnimation ca_mouse_leave = new ColorAnimation(c_target_anim, new TimeSpan(0, 0, 0, 1));
            ca_mouse_leave.DecelerationRatio = 1;
            pie.Fill.BeginAnimation(SolidColorBrush.ColorProperty, ca_mouse_leave);
        }


        protected virtual void pie_item_MouseEnter(object sender, MouseEventArgs e)
        {
            Path pie;
            if (sender is Path)
                pie = (Path)sender;
            //else if (sender is Canvas && _shape_text != null)
            //    pie = _shape_text;
            else if (Shape != null)
                pie = Shape;
            else
                return;

            if (!IsEnabled)
                return;

            ColorAnimation ca_mouse_enter = new ColorAnimation(_bg_pie_active_color, new TimeSpan(0, 0, 0, 0, 250));
            ca_mouse_enter.DecelerationRatio = 1;
            pie.Fill.BeginAnimation(SolidColorBrush.ColorProperty, ca_mouse_enter);
        }

        protected virtual void _shape_MouseUp(object sender, MouseButtonEventArgs e)
        {
        }

        public override string ToString()
        {
            return "AngleStart: " + AngleStart + " AngleEnd: " + AngleEnd + " Name: " + Name;
        }

        /// <summary>
        /// winken in radianten
        /// </summary>
        public double AnglePie
        {
            get { return AngleEnd - AngleStart; }
        }

        public List<PE> SubEntries
        {
            set
            {
                _sub_entries = value;
                //assert subentries connected to this parent
                foreach (PE entry in _sub_entries)
                {
                    entry.Parent = this as PE;
                }
            }
            get
            {
                return _sub_entries;
            }
        }



        public Path Shape
        {
            set
            {
                if (_shape != null)
                {
                    _shape.MouseEnter -= pie_item_MouseEnter;
                    _shape.MouseLeave -= pie_item_MouseLeave;
                }

                _shape = value;
                _shape.MouseEnter += pie_item_MouseEnter;
                _shape.MouseLeave += pie_item_MouseLeave;
            }
            get
            {
                return _shape;
            }
        }
        public Path ShapeText
        {
            set
            {
                if (_shape_text != null)
                {
                    _shape_text.MouseEnter -= pie_item_MouseEnter;
                    _shape_text.MouseLeave -= pie_item_MouseLeave;
                }

                _shape_text = value;
                _shape_text.MouseEnter += pie_item_MouseEnter;
                _shape_text.MouseLeave += pie_item_MouseLeave;
            }
            get
            {
                return _shape_text;
            }
        }

        public Canvas ShapeTextBoundingBox
        {
            set
            {
                if (_shape_text_boundingbox != null)
                {
                    _shape_text_boundingbox.MouseEnter -= pie_item_MouseEnter;
                    _shape_text_boundingbox.MouseLeave -= pie_item_MouseLeave;
                }

                _shape_text_boundingbox = value;
                _shape_text_boundingbox.MouseEnter += pie_item_MouseEnter;
                _shape_text_boundingbox.MouseLeave += pie_item_MouseLeave;
            }

            get
            {
                return _shape_text_boundingbox;
            }
        }


        public bool IsHidden
        {
            set
            {
                _is_hidden = value;
                UpdatePieStyle();
                UpdateHiddenStateOnSubEntries();
            }
            get
            {
                return _is_hidden;
            }
        }

        private void UpdateHiddenStateOnSubEntries()
        {
            if (_is_hidden)
            {
                if (SubEntries != null)
                    foreach (PE pie_menu_entry in SubEntries)
                        pie_menu_entry.IsHidden = true;
            }
            else
            {
                if (Parent != null && Parent.IsHidden)
                    Parent.IsHidden = false;
            }
        }


        public void UnHideChildren()
        {
            IsHidden = false;
            if (_sub_entries != null)
                foreach (PE pie_menu_entry in _sub_entries)
                    pie_menu_entry.UnHideChildren();
        }

        protected Path _shape;
        protected Path _shape_text;
        protected Canvas _shape_text_boundingbox;

        public PE Parent { set; get; }
        protected List<PE> _sub_entries;
        public string Name { set; get; }

        /// <summary>
        /// winkel start des pie elements in radianten
        /// </summary>
        public double AngleStart { set; get; }

        /// <summary>
        /// winkel ende des pie elements in radianten
        /// </summary>
        public double AngleEnd { set; get; }

        /// <summary>
        /// wenn man auf dieses pie menu entry clicked
        /// egal ob toggle button oder button, wenn auf true 
        /// dann verschwindet das ganze menü nach dem click
        /// </summary>
        public bool HidesMenuOnClick { set; get; }

        public bool IsEnabled = true;
        protected bool _is_hidden;

        public string Key { set; get; }

        public static Color DefaultColorPieBackground = Color.FromArgb(0x55, 0xff, 0, 0);
        public static Color DefaultColorBorderPie = Colors.White;
        public static Color DefaultColorPieBackgroundActive = Colors.White;

        protected Brush _pie_bg_brush;
        protected Brush _pie_stroke_brush;

        protected Color _bg_pie_color;
        protected Color _bg_pie_active_color;
        protected Color _bg_pie_stroke_color;

        public object Tag;
        public object Parameter;

    }
}