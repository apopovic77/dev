﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using agsXMPP;

namespace Logicx.JabberLib
{

    public class PresenceEventArg : EventArgs
    {
        public PresenceEventArg(string username_presence_changed, PresenceState pstate)
        {
            UsernamePresenceChanged = username_presence_changed;
            PresenceState = pstate;
        }

        public string UsernamePresenceChanged;
        public PresenceState PresenceState;
    }

    public class MessageEventArg : EventArgs
    {
        public MessageEventArg(string from_user, string message)
        {
            FromUser = from_user;
            Message = message;
        }

        public string FromUser;
        public string Message;
    }

    public class JabberClient : MessagingClient, IDisposable
    {
        #region MemberVars
        private XmppClientConnection _connection;
        private bool m_connections_successful = false;
        private string m_autherror_msg = "";


        #region Events
        public event PresenceEventHandler OnPresence;
        public delegate void PresenceEventHandler(object sender, PresenceEventArg e);
        public event MessageEventHandler OnIncomingMessage;
        public delegate void MessageEventHandler(object sender, MessageEventArg e);
        #endregion

        public string AuthErrorMsg
        {
            get { return m_autherror_msg; }
        }
        #endregion

        #region Constructor Impl
        public JabberClient(string Username, string Password, string Server, int Port)
        {

            this.Username = Username;
            this.Password = Password;
            this.Server = Server;
            this.Port = Port;

            Jid jid = new Jid(Username + "@" + Server);

            _connection = new XmppClientConnection();

            _connection.OnAuthError += new OnXmppErrorHandler(_connection_OnAuthError);
            _connection.OnClose += new ObjectHandler(_connection_OnClose);
            _connection.OnError += new ErrorHandler(_connection_OnError);
            _connection.OnLogin += new ObjectHandler(_connection_OnLogin);
            _connection.OnMessage += new XmppClientConnection.MessageHandler(_connection_OnMessage);
            _connection.OnPresence += new XmppClientConnection.PresenceHandler(_connection_OnPresence);
            _connection.OnRosterEnd += new ObjectHandler(_connection_OnRosterEnd);
            _connection.OnRosterItem += new XmppClientConnection.RosterHandler(_connection_OnRosterItem);
            _connection.OnRosterStart += new ObjectHandler(_connection_OnRosterStart);
            _connection.OnSocketError += new ErrorHandler(_connection_OnSocketError);
            _connection.OnXmppError += new OnXmppErrorHandler(_connection_OnXmppError);
            _connection.OnXmppConnectionStateChanged += new XmppConnection.XmppConnectionStateHandler(_connection_OnXmppConnectionStateChanged);

            _connection.Server = jid.Server;
            _connection.Username = jid.User;
            _connection.Password = Password;
            _connection.Resource = "Custom XMPP Client";
            _connection.Priority = 10;
            _connection.Port = Port;
            _connection.UseSSL = true;
            _connection.AutoResolveConnectServer = true;
            _connection.KeepAlive = true;
            _connection.ConnectServer = null;
            _connection.SocketConnectionType = agsXMPP.net.SocketConnectionType.Direct;
            _connection.UseCompression = true;
            _connection.UseStartTLS = true;
            _connection.RegisterAccount = false;
        }
        #endregion

        #region Destruction
        private bool alreadyDisposed = false;
        ~JabberClient()
        {
            Dispose(false);
        }

        public virtual void Dispose()
        {
            Dispose(true);
            System.GC.SuppressFinalize(this);
        }

        public virtual void Dispose(bool explicitCall)
        {
            if (!this.alreadyDisposed)
            {
                Disconnect();
            }
            alreadyDisposed = true;
        }
        #endregion

        #region IMessaginClient Implementation
        private void ThreadFunReconnectTillConnected()
        {
            while (true)
            {
                System.Threading.Thread.Sleep(60 * 1000);
                if (Connect())
                    break;
                else
                    Console.Write(".");
            }
            Console.WriteLine();
        }
        public void ReconnectTillConnected()
        {
            Console.WriteLine("Connect till reconnected");
            ThreadStart ts = new ThreadStart(ThreadFunReconnectTillConnected);
            System.Threading.Thread t = new Thread(ts);
            t.Start();
        }
        public override bool Connect()
        {
            lock (this)
            {
                m_connections_successful = false;
                _connection.Open();
                if (!Monitor.Wait(this, 10000))
                    return false;
                else
                    return m_connections_successful;
            }
        }
        public override void Disconnect()
        {
            try
            {
                _connection.Close();
            }
            catch { }
        }

        public override bool IsInContactList(string name)
        {
            for (int i = 0; i < m_contactlist.Count; i++)
            {
                if (m_contactlist[i] == name)
                    return true;
            }
            return false;
        }
        public override void OnMessage(string msg, string from)
        {
        }
        public override PresenceState GetPrecenseState(string from)
        {
            return PresenceState.Offline;
        }
        public override bool SendMessage(string s_msg, string to)
        {
            try
            {
                agsXMPP.protocol.client.Message msg = new agsXMPP.protocol.client.Message();
                Jid jid_to = new Jid(to);
                msg.Type = agsXMPP.protocol.client.MessageType.chat;
                msg.To = jid_to;
                msg.Body = s_msg;
                _connection.Send(msg);
                return true;
            }
            catch
            {
                return false;
            }
        }
        public override void AddContact(string name)
        {
            try
            {
                Jid jid = new Jid(name);
                //// Add the Rosteritem using the Rostermanager			
                //if (txtNickname.Text.Length > 0)
                //    _connection.RosterManager.AddRosterItem(jid, txtNickname.Text);
                //else
                //    _connection.RosterManager.AddRosterItem(jid);
                m_contactlist.Add(name);
                // Ask for subscription now
                _connection.PresenceManager.Subcribe(jid);
            }
            catch (Exception ex)
            {
                //SendExceptionMail(ex);
            }
        }
        public override void AuthSubscription(string name)
        {
            if (!IsInContactList(name))
                AddContact(name);

            agsXMPP.protocol.client.PresenceManager pm = new agsXMPP.protocol.client.PresenceManager(_connection);
            pm.ApproveSubscriptionRequest(new Jid(name));
        }
        #endregion

        #region XMPP Event Handling Functions
        private void _connection_OnXmppError(object sender, agsXMPP.Xml.Dom.Element e)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        private void _connection_OnXmppConnectionStateChanged(object sender, XmppConnectionState state)
        {
            try
            {
                //throw new Exception("The method or operation is not implemented.");
                Console.WriteLine(state.ToString());
            }
            catch { }
        }

        private void _connection_OnSocketError(object sender, Exception ex)
        {
            try
            {
                Console.WriteLine("OnSocketError");
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.StackTrace);
            }
            catch { }
        }

        private void _connection_OnRosterStart(object sender)
        {
            lock (this)
            {
                //Console.WriteLine("OnRosterStart");
                m_contactlist = new ArrayList();
            }
        }

        private void _connection_OnRosterItem(object sender, agsXMPP.protocol.iq.roster.RosterItem item)
        {
            lock (this)
            {
                try
                {
                    //Console.WriteLine(item.ToString());
                    m_contactlist.Add(item.Jid.Bare);
                }
                catch (Exception ex)
                {
                    //SendExceptionMail(ex);
                }
            }
        }

        //public void SendExceptionMail(Exception ex)
        //{
        //    try
        //    {
        //        CEMail mail = new CEMail("error provider jabber", ex.Message + "\n\n\n" + ex.StackTrace, "alert@PushX.at", "apopovic@logicx.at", System.Web.Mail.MailFormat.Text, System.Web.Mail.MailPriority.Normal, "mail.linea7.net");
        //        mail.send();
        //    }
        //    catch { }
        //}

        private void _connection_OnRosterEnd(object sender)
        {
            lock (this)
            {
                try
                {
                    //Console.WriteLine("OnRosterEnd");
                    _connection.SendMyPresence();
                    m_connections_successful = true;
                    Monitor.Pulse(this);
                }
                catch (Exception ex)
                {
                    //SendExceptionMail(ex);
                }
            }
        }

        private void _connection_OnPresence(object sender, agsXMPP.protocol.client.Presence pres)
        {
            try
            {
                //check if precense if for subscription
                if (pres.Type == agsXMPP.protocol.client.PresenceType.subscribe)
                {
                    AuthSubscription(pres.From.Bare);
                }

                PresenceState pstate = PresenceState.Offline;
                switch(pres.Type)
                {
                    case agsXMPP.protocol.client.PresenceType.available:
                        pstate = PresenceState.Online;
                        break;
                    case agsXMPP.protocol.client.PresenceType.invisible:
                        pstate = PresenceState.Invisible;
                        break;
                    case agsXMPP.protocol.client.PresenceType.unavailable:
                        pstate = PresenceState.Offline;
                        break;
                    //case agsXMPP.protocol.client.PresenceType.:
                    //    pstate = PresenceState.Away;
                    //    break;
                    default:
                        return;

                }
                m_precensestate.Remove(pres.From.Bare);
                m_precensestate.Add(pres.From.Bare, pstate);

                string username_presence_changed = pres.From.Bare;
                PresenceEventArg e = new PresenceEventArg(username_presence_changed,pstate);
                if (OnPresence != null)
                    OnPresence(this, e);
            }
            catch (Exception ex)
            {
                //SendExceptionMail(ex);
            }
        }

        private void _connection_OnMessage(object sender, agsXMPP.protocol.client.Message msg)
        {
            try
            {
                if (msg != null && msg.Body != "" && msg.From.Bare != null && msg.From.Bare != "")
                {
                    OnMessage(msg.Body, msg.From.Bare);

                    string from_user = msg.From.Bare;
                    string msg_body = msg.Body;
                    MessageEventArg e = new MessageEventArg(from_user, msg_body);
                    if (OnIncomingMessage != null)
                        OnIncomingMessage(this, e);
                }
            }
            catch (Exception ex)
            {
                //SendExceptionMail(ex);
            }
        }

        private void _connection_OnLogin(object sender)
        {
            Console.WriteLine("Login Successful");
        }


        private void _connection_OnError(object sender, Exception ex)
        {
            Console.WriteLine(ex.Message);
            Console.WriteLine(ex.StackTrace);
        }

        private void _connection_OnClose(object sender)
        {
            try
            {
                Console.WriteLine("closing ...");
                ReconnectTillConnected();
            }
            catch (Exception ex)
            {
                //SendExceptionMail(ex);
            }
        }

        private void _connection_OnAuthError(object sender, agsXMPP.Xml.Dom.Element e)
        {
            lock (this)
            {
                try
                {
                    m_autherror_msg = e.ToString();
                    Monitor.Pulse(this);
                }
                catch (Exception ex)
                {
                    //SendExceptionMail(ex);
                }
            }
        }
        #endregion
    }
}
