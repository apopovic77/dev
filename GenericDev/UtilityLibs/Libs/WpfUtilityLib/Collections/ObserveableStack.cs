﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows.Threading;

namespace Logicx.WpfUtility.Collections
{
    /// <summary>
    /// took from http://stackoverflow.com/questions/3127136/observable-stack-and-queue
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ObservableStack<T> : Stack<T>, INotifyCollectionChanged, INotifyPropertyChanged
    {
        public ObservableStack(Dispatcher event_dispatcher = null)
        {
            _event_dispatcher = event_dispatcher;
        }

        public ObservableStack(IEnumerable<T> collection, Dispatcher event_dispatcher = null)
        {
            _event_dispatcher = event_dispatcher;

            foreach (var item in collection)
                base.Push(item);
        }

        public ObservableStack(List<T> list, Dispatcher event_dispatcher = null)
        {
            _event_dispatcher = event_dispatcher;

            foreach (var item in list)
                base.Push(item);
        }


        public new virtual void Clear()
        {
            base.Clear();

            Action raise_event = () => this.OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));

            if (_event_dispatcher != null)
                _event_dispatcher.BeginInvoke(raise_event);
            else
                raise_event();
        }

        public new virtual T Pop()
        {
            var item = base.Pop();

            Action raise_event = () => OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, item, this.Count));

            if (_event_dispatcher != null)
                //_event_dispatcher.BeginInvoke(raise_event);
                _event_dispatcher.Invoke(raise_event);
            else
                raise_event();

            return item;
        }

        public new virtual void Push(T item)
        {
            base.Push(item);

            Action raise_event = () => OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, item));

            if (_event_dispatcher != null)
                _event_dispatcher.BeginInvoke(raise_event);
            else
                raise_event();
        }

        public virtual event NotifyCollectionChangedEventHandler CollectionChanged;
        
        protected virtual void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
        {
            this.RaiseCollectionChanged(e);
        }

        protected virtual void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            this.RaisePropertyChanged(e);
        }


        protected virtual event PropertyChangedEventHandler PropertyChanged;


        private void RaiseCollectionChanged(NotifyCollectionChangedEventArgs e)
        {
            if (this.CollectionChanged != null)
                this.CollectionChanged(this, e);
        }

        private void RaisePropertyChanged(PropertyChangedEventArgs e)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, e);
        }


        event PropertyChangedEventHandler INotifyPropertyChanged.PropertyChanged
        {
            add { this.PropertyChanged += value; }
            remove { this.PropertyChanged -= value; }
        }

        private Dispatcher _event_dispatcher;
    }

}
