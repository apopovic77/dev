using System;
using System.Collections.Generic;
using System.Text;
using Logicx.Geo.Geometries;
using Microsoft.DirectX;
using Logicx.DirectX.Engine;
using GeoLib;
using Microsoft.DirectX.Direct3D;

namespace DirectXLib.Engine
{
    public class Camera
    {
        public Camera(Vector3 position) 
        {
            Position = position;
            FieldOfView = (float)(Math.PI / 4.0f);
            AspectRatio = (float)Engine3d.DirectXManager.RenderTarget.ClientSize.Width / Engine3d.DirectXManager.RenderTarget.ClientSize.Height;
            CameraTarget = new Vector3(1000000f,0f,0f);
            CampertUpVector = new Vector3(0f,0f,-1f);

            vecFrustum = new Vector3[8];    // corners of the view frustum
            planeFrustum = new Plane[6];    // planes of the view frustum

            Attitude = new Logicx.DirectX.Objects.Object3d.Attitude(0f,0f,0f);
            Attitude.Heading = Microsoft.DirectX.Direct3D.Geometry.DegreeToRadian(90);
            Attitude.Roll = Microsoft.DirectX.Direct3D.Geometry.DegreeToRadian(-90);
            Attitude.Pitch = Microsoft.DirectX.Direct3D.Geometry.DegreeToRadian(0);

            _viewport = new Viewport();
            _viewport.X = 0;
            _viewport.Y = 0;
            _viewport.Width = Engine3d.Device3d.PresentationParameters.BackBufferWidth;
            _viewport.Height = Engine3d.Device3d.PresentationParameters.BackBufferHeight;
        }
		



        #region Camera Movement
        public void SetProjectionMatrix()
        {
            // Perspektive
            _matProj = Matrix.PerspectiveFovLH(FieldOfView, // Blickfeldwinkel  (PI/4 = 45°)
                                                                    AspectRatio, // Seitenverhältnis
                                                                    1f,
                                                                    100000f);
            Engine3d.Device3d.Transform.Projection = _matProj;
        }
        public void MoveCamera()
        {
            MoveCamera(Position.X,Position.Y,Position.Z);
        }
        public void MoveCamera(float x, float y, float z)
        {
            //if (z != Position.Z)
            //{
            //    rel_dist_border_view_x = Convert.ToSingle((Math.Tan(FieldOfView / 2) * z) * border_frustum_corr_factor_x);
            //    rel_dist_border_view_y = Convert.ToSingle((Math.Tan(FieldOfView / Camera_AspectRatio / 2) * z) * border_frustum_corr_factor_y);
            //}

            Position.X = x;
            Position.Y = y;
            Position.Z = z;

            // Vordere u. hintere Z-Ebene
            // Kamera
            _matView = Matrix.LookAtLH(Position,         // Kameraposition
                                       CameraTarget,     // Punkt, auf den geschaut wird
                                       CampertUpVector); // Vektor, der nach "oben" zeigt


            //roation for roll and heading
            _matView = _matView * Matrix.RotationY(Attitude.Heading);
            _matView = _matView * Matrix.RotationX(Attitude.Roll);
            _matView = _matView * Matrix.RotationZ(Attitude.Pitch);
            //apply View matrix
            Engine3d.Device3d.Transform.View = _matView;

            //Frustum Planes
            Matrix mat = Matrix.Multiply(_matView, _matProj);
            mat.Invert();
            vecFrustum[0] = new Vector3(-1.0f, -1.0f, 0.0f); // xyz
            vecFrustum[1] = new Vector3(1.0f, -1.0f, 0.0f); // Xyz
            vecFrustum[2] = new Vector3(-1.0f, 1.0f, 0.0f); // xYz
            vecFrustum[3] = new Vector3(1.0f, 1.0f, 0.0f); // XYz
            vecFrustum[4] = new Vector3(-1.0f, -1.0f, 1.0f); // xyZ
            vecFrustum[5] = new Vector3(1.0f, -1.0f, 1.0f); // XyZ
            vecFrustum[6] = new Vector3(-1.0f, 1.0f, 1.0f); // xYZ
            vecFrustum[7] = new Vector3(1.0f, 1.0f, 1.0f); // XYZ
            for (int i = 0; i < 8; i++)
                vecFrustum[i] = Vector3.TransformCoordinate(vecFrustum[i], mat);
            planeFrustum[0] = Plane.FromPoints(vecFrustum[7], vecFrustum[3], vecFrustum[5]); // Right
            planeFrustum[1] = Plane.FromPoints(vecFrustum[2], vecFrustum[6], vecFrustum[4]); // Left
            planeFrustum[2] = Plane.FromPoints(vecFrustum[6], vecFrustum[7], vecFrustum[5]); // Far
            planeFrustum[3] = Plane.FromPoints(vecFrustum[0], vecFrustum[1], vecFrustum[2]); // Near
            planeFrustum[4] = Plane.FromPoints(vecFrustum[2], vecFrustum[3], vecFrustum[6]); // Top
            planeFrustum[5] = Plane.FromPoints(vecFrustum[1], vecFrustum[0], vecFrustum[4]); // Bottom
        }
        public void MoveRelCamera(Vector3 cam_rel_move)
        {

            MoveCamera(Position.X + cam_rel_move.X, Position.Y + cam_rel_move.Y, Position.Z + cam_rel_move.Z);
        }

        public void MoveRelCamera(float x, float y, float z)
        {
            MoveCamera(Position.X + x, Position.Y + y, Position.Z + z);
        }

        public void AdjustHeading(float deltaHeading)
        {
            Attitude.Heading += (deltaHeading * (float)Math.PI / 180.0f);
            if (Attitude.Heading > (2.0f * Math.PI))
                Attitude.Heading -= (float)(2.0f * Math.PI);
            if (Attitude.Heading < 0.0f)
                Attitude.Heading += (float)(2.0f * Math.PI);
        }

        public void AdjustRoll(float deltaRoll)
        {
            Attitude.Roll += (deltaRoll * (float)Math.PI / 180.0f);
            if (Attitude.Roll <= -Math.PI / 2)
                Attitude.Roll = Convert.ToSingle(-Math.PI / 2);
            if (Attitude.Roll > Microsoft.DirectX.Direct3D.Geometry.DegreeToRadian(0))
                Attitude.Roll = Microsoft.DirectX.Direct3D.Geometry.DegreeToRadian(0);
        }


        //public void AdjustPitch(float deltaPitch)
        //{
        //    Attitude.Pitch += (deltaPitch * (float)Math.PI / 180.0f);
        //    if (Attitude.Pitch <= -Math.PI / 2)
        //        Attitude.Pitch = Convert.ToSingle(-Math.PI / 2);
        //    if (Attitude.Pitch > Microsoft.DirectX.Direct3D.Geometry.DegreeToRadian(0))
        //        Attitude.Pitch = Microsoft.DirectX.Direct3D.Geometry.DegreeToRadian(0);
        //}

        #endregion

        /// <summary>
        /// Projiziert die Cam FOV auf die Z=0 Ebene und gibt das rect zurück
        /// </summary>
        /// <returns></returns>
        public SimpleRectangle GetRectCoveringZZeroFrustum()
        {

            float roll_rad = Engine3d.Camera.Attitude.Roll;
            float heading_rad = Engine3d.Camera.Attitude.Heading;
            float max_roll = Microsoft.DirectX.Direct3D.Geometry.DegreeToRadian(-5);

            //get dir vec of down left cam
            Vector3 v_inertial = new Vector3(1, 0, 0);
            v_inertial.TransformCoordinate(Matrix.RotationZ(heading_rad));
            //get orthogonal version of v_inertial // z is always zero here ortho vec would have wrong value if z was not zero
            Vector3 v_ortho = new Vector3(-v_inertial.Y, v_inertial.X, 0);
            float roll = roll_rad - (((Engine3d.Camera.FieldOfView) / (Engine3d.Camera.AspectRatio)) / 2) * _corr_factor_x;
            if (roll > max_roll) roll = max_roll;
            v_inertial.TransformCoordinate(Matrix.RotationAxis(v_ortho, roll));
            Vector3 v_copy = new Vector3(v_inertial.X, v_inertial.Y, v_inertial.Z);
            v_copy.TransformCoordinate(Matrix.RotationAxis(v_ortho, -(float)Math.PI / 2));
            v_inertial.TransformCoordinate(Matrix.RotationAxis(v_copy, (Engine3d.Camera.FieldOfView / 2f) * _corr_factor_y));
            v_inertial.Normalize();
            //berechne den durchstosspunkt auf der z=0 ebene
            //-cp_z / cd_z = len_dir_vec_bis_zum_durchstosspunkt
            float len_dir_vec_bis_zum_durchstosspunkt = -Engine3d.Camera.Position.Z / v_inertial.Z;
            Vector3 v_dpunkt = new Vector3();
            v_dpunkt.X = Engine3d.Camera.Position.X + v_inertial.X * len_dir_vec_bis_zum_durchstosspunkt;
            v_dpunkt.Y = Engine3d.Camera.Position.Y + v_inertial.Y * len_dir_vec_bis_zum_durchstosspunkt;
            v_dpunkt.Z = 0.0f;
            MathLib.Vector2f v_rect_bottomleft = new MathLib.Vector2f(v_dpunkt.X, v_dpunkt.Y);

            //get dir vec of top left cam
            v_inertial = new Vector3(1, 0, 0);
            v_inertial.TransformCoordinate(Matrix.RotationZ(heading_rad));
            //get orthogonal version of v_inertial // z is always zero here ortho vec would have wrong value if z was not zero
            v_ortho = new Vector3(-v_inertial.Y, v_inertial.X, 0);
            roll = roll_rad + (((Engine3d.Camera.FieldOfView) / (Engine3d.Camera.AspectRatio)) / 2) * _corr_factor_x;
            if (roll > max_roll) roll = max_roll;
            v_inertial.TransformCoordinate(Matrix.RotationAxis(v_ortho, roll));
            v_copy = new Vector3(v_inertial.X, v_inertial.Y, v_inertial.Z);
            v_copy.TransformCoordinate(Matrix.RotationAxis(v_ortho, -(float)Math.PI / 2));
            v_inertial.TransformCoordinate(Matrix.RotationAxis(v_copy, (Engine3d.Camera.FieldOfView / 2f) * _corr_factor_y));
            v_inertial.Normalize();
            //berechne den durchstosspunkt auf der z=0 ebene
            //-cp_z / cd_z = len_dir_vec_bis_zum_durchstosspunkt
            len_dir_vec_bis_zum_durchstosspunkt = -Engine3d.Camera.Position.Z / v_inertial.Z;
            v_dpunkt = new Vector3();
            v_dpunkt.X = Engine3d.Camera.Position.X + v_inertial.X * len_dir_vec_bis_zum_durchstosspunkt;
            v_dpunkt.Y = Engine3d.Camera.Position.Y + v_inertial.Y * len_dir_vec_bis_zum_durchstosspunkt;
            v_dpunkt.Z = 0.0f;
            MathLib.Vector2f v_rect_topleft = new MathLib.Vector2f(v_dpunkt.X, v_dpunkt.Y);

            //get dir vec of top right cam
            v_inertial = new Vector3(1, 0, 0);
            v_inertial.TransformCoordinate(Matrix.RotationZ(heading_rad));
            //get orthogonal version of v_inertial // z is always zero here ortho vec would have wrong value if z was not zero
            v_ortho = new Vector3(-v_inertial.Y, v_inertial.X, 0);
            roll = roll_rad + (((Engine3d.Camera.FieldOfView) / (Engine3d.Camera.AspectRatio)) / 2) * _corr_factor_x;
            if (roll > max_roll) roll = max_roll;
            v_inertial.TransformCoordinate(Matrix.RotationAxis(v_ortho, roll));
            v_copy = new Vector3(v_inertial.X, v_inertial.Y, v_inertial.Z);
            v_copy.TransformCoordinate(Matrix.RotationAxis(v_ortho, -(float)Math.PI / 2));
            v_inertial.TransformCoordinate(Matrix.RotationAxis(v_copy, -(Engine3d.Camera.FieldOfView / 2f) * _corr_factor_y));
            v_inertial.Normalize();
            //berechne den durchstosspunkt auf der z=0 ebene
            //-cp_z / cd_z = len_dir_vec_bis_zum_durchstosspunkt
            len_dir_vec_bis_zum_durchstosspunkt = -Engine3d.Camera.Position.Z / v_inertial.Z;
            v_dpunkt = new Vector3();
            v_dpunkt.X = Engine3d.Camera.Position.X + v_inertial.X * len_dir_vec_bis_zum_durchstosspunkt;
            v_dpunkt.Y = Engine3d.Camera.Position.Y + v_inertial.Y * len_dir_vec_bis_zum_durchstosspunkt;
            v_dpunkt.Z = 0.0f;
            MathLib.Vector2f v_rect_topright = new MathLib.Vector2f(v_dpunkt.X, v_dpunkt.Y);

            //get dir vec of right cam
            v_inertial = new Vector3(1, 0, 0);
            v_inertial.TransformCoordinate(Matrix.RotationZ(heading_rad));
            //get orthogonal version of v_inertial // z is always zero here ortho vec would have wrong value if z was not zero
            v_ortho = new Vector3(-v_inertial.Y, v_inertial.X, 0);
            roll = roll_rad - (((Engine3d.Camera.FieldOfView) / (Engine3d.Camera.AspectRatio)) / 2) * _corr_factor_x;
            if (roll > max_roll) roll = max_roll;
            v_inertial.TransformCoordinate(Matrix.RotationAxis(v_ortho, roll));
            v_copy = new Vector3(v_inertial.X, v_inertial.Y, v_inertial.Z);
            v_copy.TransformCoordinate(Matrix.RotationAxis(v_ortho, -(float)Math.PI / 2));
            v_inertial.TransformCoordinate(Matrix.RotationAxis(v_copy, -(Engine3d.Camera.FieldOfView / 2f) * _corr_factor_y));
            v_inertial.Normalize();
            //berechne den durchstosspunkt auf der z=0 ebene
            //-cp_z / cd_z = len_dir_vec_bis_zum_durchstosspunkt
            len_dir_vec_bis_zum_durchstosspunkt = -Engine3d.Camera.Position.Z / v_inertial.Z;
            v_dpunkt = new Vector3();
            v_dpunkt.X = Engine3d.Camera.Position.X + v_inertial.X * len_dir_vec_bis_zum_durchstosspunkt;
            v_dpunkt.Y = Engine3d.Camera.Position.Y + v_inertial.Y * len_dir_vec_bis_zum_durchstosspunkt;
            v_dpunkt.Z = 0.0f;
            MathLib.Vector2f v_rect_bottomright = new MathLib.Vector2f(v_dpunkt.X, v_dpunkt.Y);

            SimpleRectangle rect = new SimpleRectangle(v_rect_topleft, v_rect_topright, v_rect_bottomright, v_rect_bottomleft);
            return rect;
        }

        public Vector3 GetCameraDirectionVector()
        {
            Vector3 v_camdir = new Vector3(1, 0, 0);
            float roll_rad = Attitude.Roll;
            float heading_rad = Attitude.Heading;
            v_camdir.TransformCoordinate(Matrix.RotationZ(heading_rad));
            //get orthogonal version of v_inertial // z is always zero here ortho vec would have wrong value if z was not zero
            Vector3 v_ortho = new Vector3(-v_camdir.Y, v_camdir.X, 0);
            v_camdir.TransformCoordinate(Matrix.RotationAxis(v_ortho, roll_rad));
            v_camdir.Normalize();
            return v_camdir;
        }

        public Matrix Projection
        {
            set
            {
                _matProj = value;
            }

            get
            {
                return _matProj;
            }
        }
        public Matrix View
        {
            set
            {
                _matView = value;
            }

            get
            {
                return _matView;
            }
        }
        public Viewport Viewport
        {
            set
            {
                _viewport = value;
            }

            get
            {
                return _viewport;
            }
        }
        
        public Vector3 Position;
        public Vector3 CameraTarget;
        public Vector3 CampertUpVector;
        public float FieldOfView;
        public float AspectRatio;
        public Logicx.DirectX.Objects.Object3d.Attitude Attitude;

        private Matrix _matView;
        private Matrix _matProj;
        private Vector3[] vecFrustum;    // corners of the view frustum
        private Plane[] planeFrustum;    // planes of the view frustum
        private Viewport _viewport;

        float _corr_factor_x = 1.155f;
        float _corr_factor_y = 1.061f;




    }
}
