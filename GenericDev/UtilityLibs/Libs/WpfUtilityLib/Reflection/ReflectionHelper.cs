﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Logicx.WpfUtility.Reflection
{
    public class ReflectionHelper
    {
        /// <summary>
        /// Gets an IEnumerator interface object of the given sourceObject.
        /// Returns null if the sourceObject doesn't implement IEnumerator
        /// </summary>
        /// <param name="sourceObject"></param>
        /// <returns></returns>
        public static IEnumerator GetEnumerator(object sourceObject)
        {
            if (sourceObject != null)
            {
                MethodInfo miEnumerator = GetEnumeratorMethodInfo(sourceObject.GetType());

                if (miEnumerator != null)
                {
                    // invoke the GetEnumerator() method of the table object
                    object obEnumerator = miEnumerator.Invoke(sourceObject,
                                                             new object[]
                                                                             {
                                                                             }
                                                       );

                    return obEnumerator as IEnumerator;
                }
            }

            return null;
        }

        public static MethodInfo GetGenericAnlagenFilterQueryMethod(Type dataType)
        {
            MethodInfo[] methods = dataType.GetMethods();

            foreach (MethodInfo mI in methods)
            {
                if (mI.Name == "GenericAnlagenFilterQuery" && mI.IsGenericMethod)
                    return mI;
            }

            return null;
        }

        public static MethodInfo GetPredicateExtensionTrue(Type dataType)
        {
            MethodInfo[] methods = dataType.GetMethods();

            foreach (MethodInfo mI in methods)
            {
                if (mI.Name == "True" && mI.IsGenericMethod)
                    return mI;
            }

            return null;
        }

        /// <summary>
        /// Gets the methodInfo of the static, generic SingleOrDefault(Func&lt;T, R&gt;) method of the Enumerable class
        /// </summary>
        /// <returns></returns>
        public static MethodInfo GetQueryableCastMethodInfo()
        {
            MethodInfo[] methods = typeof(Queryable).GetMethods();

            foreach (MethodInfo mI in methods)
            {
                if (mI.Name == "Cast")
                    return mI;
            }

            return null;
        }

        /// <summary>
        /// Gets the property of a DataContext which represents a given data table
        /// </summary>
        /// <param name="tableType">Type of the data table</param>
        /// <param name="dc">DataContext to use</param>
        /// <returns>Returns the property of the data context which represents the table or null if not exists</returns>
        public static PropertyInfo GetTablePropertyOfDataContext(Type tableType, DataContext dc)
        {
            // loop through all properties
            foreach (PropertyInfo property in dc.GetType().GetProperties())
            {
                // if the property is a generic type (System.Linq.Table<type>)
                if (property.PropertyType.IsGenericType)
                {
                    // get the generic types of this proeprty
                    Type[] genericTypes = property.PropertyType.GetGenericArguments();

                    // search for the type of the table
                    foreach (Type curType in genericTypes)
                    {
                        if (curType == tableType)
                            return property;
                    }
                }
            }

            return null;
        }

        #region Get PropertyInfo/MethodInfo
        /// <summary>
        /// Gets the methodInfo of the FIRST static, generic GetEnumerator() method of the IEnumerable&lt;T&gt; implementing type
        /// </summary>
        /// <returns></returns>
        private static MethodInfo GetEnumeratorMethodInfo(Type dataType)
        {
            MethodInfo[] methods = dataType.GetMethods();

            foreach (MethodInfo mI in methods)
            {
                if (mI.Name == "GetEnumerator" && mI.GetParameters().Count() == 0)
                    return mI;
            }

            return null;
        }
        #endregion

        #region Get PropertyInfo/MethodInfo - interface IEnumerator
        /// <summary>
        /// Gets the methodInfo of the MoveNet() method of the IEnumerator implementing type
        /// </summary>
        /// <returns></returns>
        private static MethodInfo GetIEnumeratorMoveNextMethodInfo(Type dataType)
        {
            MethodInfo[] methods = dataType.GetMethods();

            foreach (MethodInfo mI in methods)
            {
                if (mI.Name == "MoveNext" && mI.GetParameters().Count() == 0)
                    return mI;
            }

            return null;
        }

        /// <summary>
        /// Gets the methodInfo of the Reset() method of the IEnumerator implementing type
        /// </summary>
        /// <returns></returns>
        private static MethodInfo GetIEnumeratorResetMethodInfo(Type dataType)
        {
            MethodInfo[] methods = dataType.GetMethods();

            foreach (MethodInfo mI in methods)
            {
                if (mI.Name == "Reset" && mI.GetParameters().Count() == 0)
                    return mI;
            }

            return null;
        }

        /// <summary>
        /// Gets the propertyInfo of the Current property of the IEnumerator implementing type
        /// </summary>
        /// <returns></returns>
        private static PropertyInfo GetIEnumeratorCurrentPropertyInfo(Type dataType)
        {
            PropertyInfo[] properties = dataType.GetProperties();

            foreach (PropertyInfo pI in properties)
            {
                if (pI.Name == "Current")
                    return pI;
            }

            return null;
        }
        #endregion
    }
}
