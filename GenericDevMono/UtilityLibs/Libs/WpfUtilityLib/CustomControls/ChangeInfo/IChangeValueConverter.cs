﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace Logicx.WpfUtility.CustomControls.ChangeInfo
{
    public enum ValueType
    {
        Original,
        Old,
        New
    }

    public interface IChangeValueConverter
    {
        string ConvertValue(DependencyObject sourceElement, ChangeInfo changeInfo, ValueType type);
    }
}
