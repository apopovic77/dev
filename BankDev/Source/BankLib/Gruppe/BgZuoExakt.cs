﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BankLib.Data;

namespace BankLib.Gruppe
{
    public class BgZuoExakt : BgZuo
    {
        internal override void WriteToStream(BinaryWriter writer)
        {
            BinaryHelper.WriteString(writer,"BgZuoExakt");
            base.WriteToStream(writer);
            BinaryHelper.WriteString(writer, BuchungsType);
            BinaryHelper.WriteString(writer, UmsatzType);
        }

        internal override void LoadFromStream(BinaryReader reader)
        {
            base.LoadFromStream(reader);
            BuchungsType = BinaryHelper.ReadString(reader);
            UmsatzType = BinaryHelper.ReadString(reader);
        }

        protected override bool CheckZuo(Buchung b)
        {
            if (b.BuchungsType == BuchungsType && b.UmsatzType == UmsatzType)
                return true;
            return false;
        }

        public override string ToString()
        {
            return String.Format("[BgZuoExact] BuchungsTyp [{0}] UmatzTyp [{1}]", BuchungsType, UmsatzType);
        }

        public string BuchungsType { get; set; }
        public string UmsatzType { get; set; }
    }
}
