﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Logicx.WpfUtility.Converters
{
    public class IntToStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //if (value == null || !(value is int))
            //    return "0";

            //return ((int) value).ToString();

            if (value == null || !(value is int))
                return "00";

            string str_value = ((int)value).ToString();
            if (str_value.Length == 1)
                str_value = "0" + str_value;

            return str_value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (string.IsNullOrEmpty((string)value))
                return 0;

            return System.Convert.ToInt16(value); // ,culture.NumberFormat);
        }
    }
}
