﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Awanto.WhatsAppCore.Entities;
using Awanto.WhatsAppCore.Services;
using Awanto.WhatsAppCore.Utilities;
using Microsoft.Extensions.Logging;

namespace WhatsAppBotConsoleApp
{
    public class CognitiveWhatsAppBot : WhatsAppBot
    {
        public CognitiveWhatsAppBot() : base("CognitiveWhatsAppBot")
        {
        }

        protected override WhatsAppNachricht CheckForBotQuery(Nachricht nachricht)
        {
            try
            {


                if (nachricht.Medien != null && nachricht.Medien.Count > 0)
                {
                    //eine image nachricht
                    Thread t = new Thread(new ParameterizedThreadStart(StartImageAnalysis));
                    t.IsBackground = true;
                    t.Name = "Background Image Analysis";
                    t.Start(nachricht);
                    return CreateAntwortNachricht(nachricht, "starte image analyse ... ");
                }

                if (string.IsNullOrEmpty(nachricht.Body))
                    return null;

                string body = nachricht.Body.ToLower();

                switch (body)
                {
                    case "time":
                        return CreateAntwortNachricht(nachricht, DateTime.Now.ToString());
                    default:
                        return base.CheckForBotQuery(nachricht);
                }
            }
            catch (Exception ex)
            {
                _logger.LogDebug("CheckForBotQuery: " + ex.ToString());
            }
            return null;
        }

        protected void StartImageAnalysis(object parameter)
        {
            if(!(parameter is WhatsAppImageNachricht))
                throw new ArgumentException("FATAL impossible?!??!?! no whatsappnachricht");
            WhatsAppImageNachricht img_nachricht = parameter as WhatsAppImageNachricht;

            try
            {
                foreach (NachrichtMedium nm in img_nachricht.Medien)
                {
                    if (!(nm.Medium is Bild))
                        continue;

                    Bild b = nm.Medium as Bild;
                    b.UpdateImageAnnotation();

                    WhatsAppNachricht nachricht = CreateAntwortNachricht(img_nachricht, b.Annotation);
                    WhatsAppService.SendNachricht(nachricht);
                }
            }
            catch (Exception ex)
            {
                WhatsAppNachricht nachricht = CreateAntwortNachricht(img_nachricht, ex.Message);
                WhatsAppService.SendNachricht(nachricht);
            }
        }
    }
}
