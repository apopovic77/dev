﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using BankLib.Data;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.PhantomJS;
using OpenQA.Selenium.Support.UI;

namespace BankLib.WebImporter
{
    public class Selenium
    {
        public Selenium(Konto kto, DateTime from_date)
        {
            _kto = kto;
            _from_date = from_date;
            Buchungen = new List<Buchung>();
        }

        

        public bool Connect(ref string error_msg, out float? giro_kto_stand)
        {
            giro_kto_stand = null;
            Driver = null;
            try
            {
                //Driver = new PhantomJSDriver();
                //Driver = new FirefoxDriver();
                Driver = new ChromeDriver();

                //Notice navigation is slightly different than the Java version
                //This is because 'get' is a keyword in C#
                Driver.Navigate().GoToUrl(UrlToBanklogin);

                //just wait, makes everthing sure
                Thread.Sleep(_sleep_beforequery_next);

                // Find the text input element by its name
                IWebElement query = Driver.FindElement(By.Name("verfueger"));
                query.SendKeys(_kto.VerfuegerName);

                query = Driver.FindElement(By.Name("verfuegerName"));
                query.SendKeys(_kto.VerfuegerNr);

                query = Driver.FindElement(By.Name("pin"));
                query.SendKeys(_kto.Pin);

                // Now submit the form. WebDriver will find the form for us from the element
                query.Submit();

                // Google's search is rendered dynamically with JavaScript.
                // Wait for the page to load, timeout after 10 seconds
                WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(40));
                try
                {
                    wait.Until<IWebElement>((d) =>
                    {
                        return d.FindElement(By.Id("foldAnchorgesamtsumme"));
                    });
                }
                catch
                {
                    //timeout occured - critical exception
                    //throw new ApplicationException("critical exception webpage cannot be loaded", ex);
                    Console.Error.WriteLine("critical exception webpage cannot be loaded");
                }


                IWebElement giro = Driver.FindElement(By.Id("finanzgiro"));
                string[] giro_sarr = giro.Text.Replace("\r\n", "\n").Split("\n".ToCharArray(),4);
                giro_kto_stand = null;
                string eur_giro = null;
                try
                {
                    string giro_text = giro.Text.Replace("\r\n", "\n");
                    int index_start = giro_text.IndexOf("EUR", System.StringComparison.Ordinal);
                    int index_end = giro_text.Substring(index_start + 4).IndexOf("\n", System.StringComparison.Ordinal) + index_start;
                    eur_giro = giro_text.Substring(index_start + 4, index_end - index_start);
                    giro_kto_stand = (float)Math.Round(Convert.ToSingle(eur_giro, CultureInfo.GetCultureInfo("de-DE").NumberFormat), 3);
                    AppHelper.WriteFileLog("\r\nSucces Fetch Giro Betrag" + giro_kto_stand);
                }
                catch
                {
                    AppHelper.WriteFileLog("\r\nCannot Extract Giro Konto Summe, vielleicht hat sich das HTML Format geändert. Check new Format.");
                    try
                    {
                        AppHelper.WriteFileLog("\r\n" + giro.Text);
                        AppHelper.WriteFileLog("\r\n-->" + eur_giro);
                        //AppHelper.WriteFileLog("\r\n");
                        //foreach (string s in giro_sarr)
                        //    AppHelper.WriteFileLog("\r\n"+s);
                    }catch{}
                    giro_kto_stand = null;
                }


                //find konto anchor and download konto data
                IWebElement kto_anchor = GetKontoAnchor(_kto);
                if (kto_anchor == null)
                {
                    throw new ApplicationException("critical Exception cannot find konto anchor");
                }
                kto_anchor.SendKeys(Keys.Enter);


                wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(40));
                try
                {
                    IWebElement myDynamicElement = wait.Until<IWebElement>((d) =>
                    {
                        return d.FindElement(By.Id("disclaimer"));
                    });
                }
                catch (Exception ex)
                {
                    //timeout occured - critical exception
                    throw new ApplicationException("critical exception webpage cannot be loaded", ex);
                }

                //select erweiterete suche
                Thread.Sleep(_sleep_beforequery_next);
                SelectElement select = new SelectElement(Driver.FindElement(By.Id("reqAttrSearchSelectBoxOptions")));
                select.Options[6].Click();


                wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(40));
                try
                {
                    IWebElement myDynamicElement = wait.Until<IWebElement>((d) =>
                    {
                        return d.FindElement(By.Id("calReqParStartBookingDate"));
                    });
                }
                catch (Exception ex)
                {
                    //timeout occured - critical exception
                    throw new ApplicationException("critical exception webpage cannot be loaded", ex);
                }
                
                IWebElement datum_von = Driver.FindElement(By.Id("calReqParStartBookingDate"));

                for (int i = 0; i < 13; i++)
                    datum_von.SendKeys(Keys.Backspace);

                datum_von.SendKeys(_from_date.ToShortDateString());
                datum_von.SendKeys(Keys.Enter);

                wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(40));
                try
                {
                    IWebElement myDynamicElement = wait.Until<IWebElement>((d) =>
                    {
                        return d.FindElement(By.Id("disclaimer"));
                    });
                }
                catch (Exception ex)
                {
                    //timeout occured - critical exception
                    throw new ApplicationException("critical exception webpage cannot be loaded", ex);
                }



                return true;
            }
            catch (Exception ex)
            {
                error_msg = ex.Message + "\n\n" + ex.ToString();
                Console.WriteLine(error_msg);
                return false;
            }
        }

        public void Disconnect()
        {
            //Close the browser
            if (Driver != null)
            {
                Buchungen = null;
                Driver.Quit();
                Driver.Dispose();
            }
        }

        public bool NavWeiter()
        {
            //nav weiter wenn noch welche da sind
            IWebElement nav_weiter = null;
            try
            {
                nav_weiter = Driver.FindElement(By.Id("sbtnNavWeiter"));
                if (nav_weiter == null)
                    return false;
            }
            catch
            {
                return false;
            }


            nav_weiter.SendKeys(Keys.Enter);

            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(40));
            try
            {
                IWebElement myDynamicElement = wait.Until<IWebElement>((d) =>
                {
                    return d.FindElement(By.Id("disclaimer"));
                });
            }
            catch (Exception ex)
            {
                //timeout occured - critical exception
                throw new ApplicationException("critical exception webpage cannot be loaded", ex);
            }

            return true;
        }


        public List<Buchung> ExtractBuchungen(bool is_kreditkarte)
        {
            if (Driver == null)
                return null;



            Thread.Sleep(_sleep_beforequery_next);


            IWebElement table = Driver.FindElement(By.Id("tblgiroktnumsatzdata"));
            IWebElement tbody = table.FindElement(By.TagName("TBody"));

            ReadOnlyCollection<IWebElement> rows = tbody.FindElements(By.TagName("tr"));
            foreach (IWebElement row in rows)
            {

                /*
                 * 0 Kontonummer	
                 * 1 Auszugsnummer	
                 * 2 Buchungsdatum	
                 * 3 Valutadatum	
                 * 4 Umsatzzeit	
                 * 5 Zahlungsreferenz	
                 * 6 Waehrung	
                 * 7 Betrag	
                 * 8 Buchungstext	
                 * 9 Umsatztext	
                */


                string csv_row = _kto.KtoNummer + ";";
                ReadOnlyCollection<IWebElement> cols = row.FindElements(By.TagName("td"));

                if (cols.Count <= 2)
                    continue;

                //add Auszugsnr
                csv_row += cols[1].Text + ";";

                //add buchungsdatum
                csv_row += cols[2].Text + ";";

                //add valutadatum
                csv_row += cols[4].Text + ";";

                //add umsatzzeit
                csv_row += ";";

                //add zahlungsref
                csv_row += ";";

                string[] betragtexte = cols[5].Text.Replace("\r", "").Split("\n".ToCharArray(), 2);

                //Add Waehrung	
                csv_row += betragtexte[0] + ";";

                //Add Betrag	
                csv_row += betragtexte[1] + ";";

                string[] buchungstexte = cols[3].Text.Replace("\r", "").Replace("/", "\n").Split("\n".ToCharArray(), 3);

                if (buchungstexte.Length >= 2)
                {
                    int last_index = buchungstexte.Length - 1;
                    buchungstexte[last_index] = buchungstexte[last_index].Replace(";", "|");
                    buchungstexte[last_index] = buchungstexte[last_index].Replace(",", "|");
                }

                //Add Buchungstext	
                csv_row += buchungstexte[0] + ";";

                //Add Umsatztext
                csv_row += ((buchungstexte.Length > 1) ? buchungstexte[1] : "") + ";";

                //Add Umsatztext2
                csv_row += ((buchungstexte.Length > 2) ? buchungstexte[2] : "");

                Buchung b = new Buchung();
                b.Owner = _kto.Owner;
                b.Kreditkarte = is_kreditkarte;
                b.LoadFromCsvLine(csv_row);
                Buchungen.Add(b);
            }

            return Buchungen;
        }

        private IWebElement GetKontoAnchor(Konto konto)
        {
            ReadOnlyCollection<IWebElement> anchors = Driver.FindElements(By.TagName("a"));
            foreach (var anchor in anchors)
            {
                string value = anchor.GetAttribute("href");

                Console.WriteLine(value);
                if (value.IndexOf("aktuell.html?konto=" + konto.KtoNummer) >= 0)
                    return anchor;
            }
            return null;
        }

        private int _sleep_beforequery_next = 10000;
        public List<Buchung> Buchungen;
        protected DateTime _from_date { get; set; }
        protected Konto _kto;
        public IWebDriver Driver;
        public static string UrlToBanklogin = "https://www.banking.co.at/appl/ebp/login.html?resource=015&lang=de";
    }
}


//FirefoxProfile profile = new FirefoxProfile();
//profile.AcceptUntrustedCertificates = true;
//profile.SetPreference("browser.download.dir", path_to_filedownload);
//profile.SetPreference("browser.download.folderList", 2);
//profile.SetPreference("browser.download.manager.showWhenStarting", false);
//profile.SetPreference("browser.helperApps.alwaysAsk.force", false);
//profile.SetPreference("browser.helperApps.neverAsk.saveToDisk","text/csv");
//profile.SetPreference("pdfjs.disabled", true);
// Further note that other drivers (InternetExplorerDriver,
// ChromeDriver, etc.) will require further configuration 
// before this example will work. See the wiki pages for the
// individual drivers at http://code.google.com/p/selenium/wiki
// for further information.
//_driver = new FirefoxDriver();


//SelectElement select = new SelectElement(_driver.FindElement(By.Id("printOptions")));

//string rel_url = null;
//foreach (IWebElement option in select.Options)
//{

//    string text = option.Text;
//    if (text == "Textdatei/CSV")
//        rel_url = option.GetAttribute("Value");
//}

//if (rel_url == null)
//    throw new ApplicationException("Critical Excption cannot find CSV file");

//string url = "https://www.banking.co.at" + rel_url;
//_driver.Navigate().GoToUrl(url);


//select.SelectByIndex(5);
//select.SelectByText("Textdatei/CSV");
//string link_to_csv = select.SelectedOption.GetAttribute("Value");
//select.deselectAll();   
//select.selectByVisibleText("Value1");
//Thread.Sleep(2000);
//IWebElement body = _driver.FindElement(By.TagName("Body"));
//string value_body = body.ToString();
