﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Logicx.WpfUtility.CustomControls
{
    /// <summary>
    /// Interaction logic for AllgemeineInformation.xaml
    /// </summary>
    public partial class AllgemeineInformation : UserControl
    {
        public AllgemeineInformation()
        {
            InitializeComponent();
        }

        #region Properties
        public string Applikation
        {
            set
            {
                app_tbl.Text = value;
            }
        }

        public string Version
        {
            set
            {
                version_tbl.Text = value;
            }
        }

        public string FullName
        {
            set
            {
                fullname_tbl.Text = value;
            }
        }

        public string Anmeldeinformation
        {
            set
            {
                login_tbl.Text = value;
            }
        }

        public string DbServerName
        {
            set
            {
                db_server_tbl.Text = value;
            }
        }

        public string DbName
        {
            set
            {
                db_dbname_tbl.Text = value;
            }
        }

        public bool ShowDbInfo
        {
            set
            {
                if (value)
                    db_info_sp.Visibility = System.Windows.Visibility.Visible;
                else
                    db_info_sp.Visibility = System.Windows.Visibility.Collapsed;
            }
        }

        public bool ShowUserInfo
        {
            set
            {
                if (value)
                    user_info_sp.Visibility = System.Windows.Visibility.Visible;
                else
                    user_info_sp.Visibility = System.Windows.Visibility.Collapsed;
            }
        }

        public bool ShowApplikationInfo
        {
            set
            {
                if (value)
                    app_info_sp.Visibility = System.Windows.Visibility.Visible;
                else
                    app_info_sp.Visibility = System.Windows.Visibility.Collapsed;
            }
        }
        #endregion
    }
}
