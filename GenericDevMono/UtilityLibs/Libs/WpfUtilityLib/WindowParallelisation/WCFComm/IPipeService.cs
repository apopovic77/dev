using System;
using System.Collections.Generic;
using System.Text;
using System.ServiceModel;

namespace NamedPipe.Communication
{
    [ServiceContract(Namespace = "IdontKnowForWhatPurposeINeedThis")]
    public interface IPipeService
    {
        [OperationContract]
        void PipeMessage(List<string> data);

        [OperationContract]
        void SetWindowDimension(double left, double top, double width, double height);

    }
}
