﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Threading;
using Logicx.DiagramLib.Controls;
using Logicx.DiagramLib.Resources.Converter;
using Logicx.DiagramLib.Util;
using Logicx.DiagramLib.ViewConfig;
using Logicx.WpfUtility.UiManipulation;
using Logicx.WpfUtility.WpfHelpers;

namespace Logicx.DiagramLib.DiagramObjects.Objects
{
    public abstract class PointObject<Tx, Ty> : DependencyObject, INotifyPropertyChanged, INotifyPropertyChanging, IDisposable
    {
        #region Constants
        #endregion

        #region Construction and Initialization
        /// <summary>
        /// Constructor of the class
        /// </summary>
        /// <param name="x_value"></param>
        /// <param name="y_value"></param>
        public PointObject(Tx x_value, Ty y_value)
        {
            _x_value = _last_x_value = x_value;
            _y_value = _last_y_value = y_value;
        }
        #endregion

        #region Operations

        #region abstract operations
        /// <summary>
        /// Create the main shape visual
        /// </summary>
        /// <returns></returns>
        protected abstract Decorator CreateShapeVisual();
        /// <summary>
        /// Create the dictionary of view mode content visuals
        /// </summary>
        /// <returns></returns>
        protected abstract Dictionary<ObjectViewMode, FrameworkElement> CreateContentVisuals();
        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            if (_disposed || _disposing)
                return;

            _disposing = true;

            Dispose(true);

            EventHelper.UnregisterEvents(PropertyChanged);
            EventHelper.UnregisterEvents(PropertyChanging);

            _disposed = true;
        }

        protected virtual void Dispose(bool disposing)
        {

        }

        #endregion

        #region Propertychanging
        /// <summary>
        /// Send the proeprty changing event
        /// </summary>
        /// <param name="property_name"></param>
        protected void SendPropertyChanging(string property_name)
        {
            if (PropertyChanging != null)
                PropertyChanging(this, new PropertyChangingEventArgs(property_name));
        }
        /// <summary>
        /// Send the property changed event
        /// </summary>
        /// <param name="property_name"></param>
        protected void SendPropertyChanged(string property_name)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property_name));
        }
        /// <summary>
        /// Internal method for handling property changes
        /// </summary>
        /// <param name="change_info"></param>
        private void InternalOnPropertyChanged(PropertyChangedMeta change_info)
        {
            if (change_info.PropertyName == "ViewMode")
            {
                if (_shape_visual != null && !change_info.OldValue.Equals(change_info.NewValue))
                {
                    //FrameworkElement content_child = _shape_visual.Child as FrameworkElement;
                    _shape_visual.Child = null;

                    if (_content_visuals != null && _content_visuals.ContainsKey((ObjectViewMode)change_info.NewValue))
                    {
                        _shape_visual.Child = _content_visuals[(ObjectViewMode)change_info.NewValue];
                    }
                }
            }
            else if (change_info.PropertyName == "IsHitTestVisible")
            {
                if (_shape_visual != null)
                    _shape_visual.IsHitTestVisible = _is_hittest_visible;

                //if (_content_visuals != null)
                //{
                //    foreach (ObjectViewMode key in _content_visuals.Keys)
                //    {
                //        _content_visuals[key].IsHitTestVisible = _is_hittest_visible;
                //    }
                //}
            }
            else if (change_info.PropertyName == "IsBackgroundObject")
            {
                InternalUpdateBindings();
            }
            else if (change_info.PropertyName == "IsXDiscrete")
            {
                if ((bool)change_info.NewValue == true && (bool)change_info.OldValue == false)
                    SetDiscreteWidth();
                else if ((bool)change_info.NewValue == false && (bool)change_info.OldValue == true)
                    ClearDiscreteWidth();
            }
            else if (change_info.PropertyName == "IsYDiscrete")
            {
                if ((bool)change_info.NewValue == true && (bool)change_info.OldValue == false)
                    SetDiscreteHeight();
                else if ((bool)change_info.NewValue == false && (bool)change_info.OldValue == true)
                    ClearDiscreteHeight();
            }

            OnPropertyChanged(change_info);
        }

        #endregion

        #region Event registration and binding helpers
        /// <summary>
        /// Registeres events for the current visual
        /// </summary>
        private void InternalRegisterEvents()
        {
            if (_shape_visual == null)
                return;

            _shape_visual.MouseEnter += new System.Windows.Input.MouseEventHandler(_shape_visual_MouseEnter);
            _shape_visual.MouseLeave += new System.Windows.Input.MouseEventHandler(_shape_visual_MouseLeave);
            _shape_visual.MouseDown += new System.Windows.Input.MouseButtonEventHandler(_shape_visual_MouseDown);
            _shape_visual.MouseUp += new System.Windows.Input.MouseButtonEventHandler(_shape_visual_MouseUp);
            _shape_visual.MouseMove += new System.Windows.Input.MouseEventHandler(_shape_visual_MouseMove);
            _shape_visual.MouseWheel += new System.Windows.Input.MouseWheelEventHandler(_shape_visual_MouseWheel);
            _shape_visual.SizeChanged += new SizeChangedEventHandler(_shape_visual_SizeChanged);

            // stylus - touch integration
            _shape_visual.StylusUp += new System.Windows.Input.StylusEventHandler(_shape_visual_StylusUp);
            _shape_visual.StylusDown += new System.Windows.Input.StylusDownEventHandler(_shape_visual_StylusDown);
            _shape_visual.StylusMove += new System.Windows.Input.StylusEventHandler(_shape_visual_StylusMove);
            _shape_visual.StylusLeave += new System.Windows.Input.StylusEventHandler(_shape_visual_StylusLeave);
            _shape_visual.StylusEnter += new System.Windows.Input.StylusEventHandler(_shape_visual_StylusEnter);

            OnRegisterVisualEvents();
        }

        /// <summary>
        /// Clear position bindings
        /// </summary>
        public void ClearPositionBindings()
        {
            InternalClearBindings();
        }
        /// <summary>
        /// Establish position bindings
        /// </summary>
        public void EstablishPositionBindings()
        {
            InternalUpdateBindings();
        }
        /// <summary>
        /// Reset last known good values
        /// </summary>
        private void InternalResetLastValues()
        {
            SetValues(_last_x_value, _last_y_value);
            OnResetLastValues();
        }
        /// <summary>
        /// Clear data bindings
        /// </summary>
        private void InternalClearDataBindings()
        {
            OnClearDataBindings();
        }
        /// <summary>
        /// Clear visual bindings
        /// </summary>
        private void InternalClearBindings()
        {
            if (_shape_visual == null)
                return;

            BindingOperations.ClearBinding(_shape_visual, Canvas.LeftProperty);
            BindingOperations.ClearBinding(_shape_visual, Canvas.TopProperty);

            _last_x_value = XValue;
            _last_y_value = YValue;

            OnClearVisualBindings();
        }
        /// <summary>
        /// Update data bindings
        /// </summary>
        private void InternalUpdateDataBindings()
        {
            OnUpdateDataBindings();
        }
        /// <summary>
        /// Update visual AND data bindings
        /// </summary>
        private void InternalUpdateBindings()
        {
            UpdateXYBindingOfVisual();
            OnUpdateVisualBindings();

            InternalUpdateDataBindings();
        }
        /// <summary>
        /// Update XY position binding of visual
        /// </summary>
        private void UpdateXYBindingOfVisual()
        {
            if (_shape_visual == null)
                return;

            BindingOperations.ClearBinding(_shape_visual, Canvas.LeftProperty);
            BindingOperations.ClearBinding(_shape_visual, Canvas.TopProperty);

            _last_x_value = XValue;
            _last_y_value = YValue;

            MultiBinding left_position_binding = new MultiBinding();

            Binding x_value_binding = new Binding("XValue");
            x_value_binding.Source = this;
            left_position_binding.Bindings.Add(x_value_binding);

            Binding x_scale_binding = new Binding("XAxisControl.ScaleUnitSize");
            x_scale_binding.RelativeSource = DiagramBindingSource;
            left_position_binding.Bindings.Add(x_scale_binding);

            Binding x_axis_binding = new Binding("XAxisControl");
            x_axis_binding.RelativeSource = DiagramBindingSource;
            left_position_binding.Bindings.Add(x_axis_binding);

            Binding x_is_background_binding = new Binding("IsBackgroundObject");
            x_is_background_binding.Source = this;
            left_position_binding.Bindings.Add(x_is_background_binding);

            left_position_binding.Converter = new ValueToPositionConverter();
            _shape_visual.SetBinding(Canvas.LeftProperty, left_position_binding);

            MultiBinding top_position_binding = new MultiBinding();

            Binding y_value_binding = new Binding("YValue");
            y_value_binding.Source = this;
            top_position_binding.Bindings.Add(y_value_binding);

            Binding y_scale_binding = new Binding("YAxisControl.ScaleUnitSize");
            y_scale_binding.RelativeSource = DiagramBindingSource;
            top_position_binding.Bindings.Add(y_scale_binding);

            Binding y_axis_binding = new Binding("YAxisControl");
            y_axis_binding.RelativeSource = DiagramBindingSource;
            top_position_binding.Bindings.Add(y_axis_binding);

            Binding y_is_background_binding = new Binding("IsBackgroundObject");
            y_is_background_binding.Source = this;
            top_position_binding.Bindings.Add(y_is_background_binding);

            top_position_binding.Converter = new ValueToPositionConverter();
            _shape_visual.SetBinding(Canvas.TopProperty, top_position_binding);
        }
        /// <summary>
        /// Set binding implementation
        /// </summary>
        /// <param name="dp"></param>
        /// <param name="binding"></param>
        public void SetBinding(DependencyProperty dp, Binding binding)
        {
            BindingOperations.SetBinding(this, dp, binding);
        }
        #endregion

        #region discrete helpers
        private void SetDiscreteWidth()
        {
            if (_shape_visual == null)
                return;

            MultiBinding width_binding = new MultiBinding();

            Binding unit_size_binding = new Binding("XAxisControl.ScaleUnitSize");
            unit_size_binding.RelativeSource = DiagramBindingSource;
            width_binding.Bindings.Add(unit_size_binding);

            Binding x_axis_binding = new Binding("XAxisControl");
            x_axis_binding.RelativeSource = DiagramBindingSource;
            width_binding.Bindings.Add(x_axis_binding);

            width_binding.Converter = new DiscreteSizeConverter();

            _shape_visual.SetBinding(FrameworkElement.WidthProperty, width_binding);
        }

        private void ClearDiscreteWidth()
        {
            if (_shape_visual != null)
                BindingOperations.ClearBinding(_shape_visual, FrameworkElement.WidthProperty);
        }

        private void SetDiscreteHeight()
        {
            if (_shape_visual == null)
                return;

            MultiBinding height_binding = new MultiBinding();

            Binding unit_size_binding = new Binding("YAxisControl.ScaleUnitSize");
            unit_size_binding.RelativeSource = DiagramBindingSource;
            height_binding.Bindings.Add(unit_size_binding);

            Binding y_axis_binding = new Binding("YAxisControl");
            y_axis_binding.RelativeSource = DiagramBindingSource;
            height_binding.Bindings.Add(y_axis_binding);

            height_binding.Converter = new DiscreteSizeConverter();

            _shape_visual.SetBinding(FrameworkElement.HeightProperty, height_binding);
        }

        private void ClearDiscreteHeight()
        {

        }
        #endregion

        #region event raising
        protected void OnObjectMoved()
        {
            if (ObjectMoved != null)
                ObjectMoved(this, EventArgs.Empty);
        }

        protected void OnObjectResized()
        {
            if (ObjectResized != null)
                ObjectResized(this, EventArgs.Empty);
        }

        private void OnLButtonClicked()
        {
            if (LButtonClicked != null)
                LButtonClicked(this, EventArgs.Empty);
        }

        private void OnRButtonClicked()
        {
            if (RButtonClicked != null)
                RButtonClicked(this, EventArgs.Empty);
        }
        private void OnDoubleClicked()
        {
            if (DoubleClicked != null)
                DoubleClicked(this, EventArgs.Empty);
        }
        #endregion

        #region protected virtual operations
        /// <summary>
        /// Virtual method for handling property changes after the internal method was executed
        /// </summary>
        /// <param name="change_info"></param>
        protected virtual void OnPropertyChanged(PropertyChangedMeta change_info)
        {

        }

        /// <summary>
        /// Register visual events
        /// </summary>
        protected virtual void OnRegisterVisualEvents()
        {

        }
        /// <summary>
        /// Update bindings
        /// </summary>
        protected virtual void OnUpdateVisualBindings()
        {
        }

        /// <summary>
        /// Update bindings
        /// </summary>
        protected virtual void OnUpdateDataBindings()
        {
        }

        /// <summary>
        /// Clear bindings
        /// </summary>
        protected virtual void OnClearVisualBindings()
        {
        }


        /// <summary>
        /// Clear data bindings
        /// </summary>
        protected virtual void OnClearDataBindings()
        {
        }

        /// <summary>
        /// Reset last good values
        /// </summary>
        protected virtual void OnResetLastValues()
        {
        }

        /// <summary>
        /// Mouse Enter event handler
        /// </summary>
        /// <param name="e"></param>
        protected virtual void OnMouseEnter(System.Windows.Input.MouseEventArgs e)
        {

        }
        /// <summary>
        /// Mouse leave event handler
        /// </summary>
        /// <param name="e"></param>
        protected virtual void OnMouseLeave(System.Windows.Input.MouseEventArgs e)
        {

        }
        /// <summary>
        /// Mouse down event handler
        /// </summary>
        /// <param name="e"></param>
        protected virtual void OnMouseDown(System.Windows.Input.MouseButtonEventArgs e)
        {

        }
        /// <summary>
        /// Mouse up event handler
        /// </summary>
        /// <param name="e"></param>
        protected virtual void OnMouseUp(System.Windows.Input.MouseButtonEventArgs e)
        {

        }
        /// <summary>
        /// Mouse move event handler
        /// </summary>
        /// <param name="e"></param>
        protected virtual void OnMouseMove(System.Windows.Input.MouseEventArgs e)
        {

        }
        /// <summary>
        /// Mouse wheel event handler
        /// </summary>
        /// <param name="e"></param>
        protected virtual void OnMouseWheel(System.Windows.Input.MouseWheelEventArgs e)
        {
        }

        /// <summary>
        /// Stylus leave event handler
        /// </summary>
        /// <param name="e"></param>
        protected virtual void OnStylusLeave(System.Windows.Input.StylusEventArgs e)
        {
        }

        /// <summary>
        /// Stylus enter event handler
        /// </summary>
        /// <param name="e"></param>
        protected virtual void OnStylusEnter(System.Windows.Input.StylusEventArgs e)
        {
        }

        /// <summary>
        /// Stylus move event handler
        /// </summary>
        /// <param name="e"></param>
        protected virtual void OnStylusMove(System.Windows.Input.StylusEventArgs e)
        {
        }
        /// <summary>
        /// Stylus down event handler
        /// </summary>
        /// <param name="e"></param>
        protected virtual void OnStylusDown(System.Windows.Input.StylusDownEventArgs e)
        {
        }
        /// <summary>
        /// Stylus up event handler
        /// </summary>
        /// <param name="e"></param>
        protected virtual void OnStylusUp(System.Windows.Input.StylusEventArgs e)
        {
        }


        /// <summary>
        /// Size changed event handler
        /// </summary>
        /// <param name="e"></param>
        protected virtual void OnSizeChanged(SizeChangedEventArgs e)
        {

        }

        /// <summary>
        /// Called before the object gets moved to the given position
        /// </summary>
        /// <param name="target_x_value"></param>
        /// <param name="target_y_value"></param>
        /// <param name="y_docking_offest"></param>
        /// <param name="can_move"></param>
        /// <param name="x_docking_offset"></param>
        /// <param name="ignore_not_can_move_on_multi_selection"></param>
        protected virtual void OnPreviewObjectMove(Tx target_x_value, Ty target_y_value, double x_docking_offset, double y_docking_offest, out bool can_move, out bool ignore_not_can_move_on_multi_selection)
        {
            //Debug.WriteLine("Pointobject OnPreviewObjectMove");
            SetValues(target_x_value, target_y_value);

            can_move = true;
            ignore_not_can_move_on_multi_selection = false;
        }

        /// <summary>
        /// Called after the object has been moved to the given position
        /// </summary>
        /// <param name="target_x_value"></param>
        /// <param name="target_y_value"></param>
        /// <param name="x_offset_in_pixel"></param>
        /// <param name="y_offest_in_pixel"></param>
        protected virtual void OnObjectMoved(Tx target_x_value, Ty target_y_value, double x_offset_in_pixel, double y_offest_in_pixel)
        {
            //Debug.WriteLine(" PointObject OnObjectMoved target values " + target_x_value + " " + target_y_value + ", offsets " + x_offset_in_pixel + " " + y_offest_in_pixel);

            SetValues(target_x_value, target_y_value);

            _last_x_value = XValue;
            _last_y_value = YValue;
        }

        /// <summary>
        /// Called before the object gets resized to the given dimensions
        /// </summary>
        /// <param name="from_x_value"></param>
        /// <param name="from_y_value"></param>
        /// <param name="to_x_value"></param>
        /// <param name="to_y_value"></param>
        /// <param name="can_size"></param>
        protected virtual void OnPreviewObjectResize(Tx from_x_value, Ty from_y_value, Tx to_x_value, Ty to_y_value, out bool can_size)
        {
            SetValues(from_x_value, from_y_value);

            can_size = true;
        }

        /// <summary>
        /// Called after an object has been resized to the given dimentions
        /// </summary>
        /// <param name="from_x_value"></param>
        /// <param name="from_y_value"></param>
        /// <param name="to_x_value"></param>
        /// <param name="to_y_value"></param>
        protected virtual void OnObjectResized(Tx from_x_value, Ty from_y_value, Tx to_x_value, Ty to_y_value)
        {
            SetValues(from_x_value, from_y_value);
            _last_x_value = XValue;
            _last_y_value = YValue;
        }

        /// <summary>
        /// Called to calculate the view mode of the current object
        /// </summary>
        /// <param name="visual_size"></param>
        /// <returns></returns>
        protected virtual ObjectViewMode CalculateViewMode(Size visual_size)
        {
            double square_pixel = visual_size.Width * visual_size.Height;

            if (square_pixel < 81)
                return ObjectViewMode.Invisible;
            else if (square_pixel >= 81 && square_pixel <= 2500)
                return ObjectViewMode.Detail1;
            else if (square_pixel > 2500 && square_pixel <= 7000)
                return ObjectViewMode.Detail2;
            else if (square_pixel > 7000 && square_pixel <= 10000)
                return ObjectViewMode.Detail3;
            else if (square_pixel > 10000 && square_pixel <= 40000)
                return ObjectViewMode.Detail4;
            else
                return ObjectViewMode.Detail5;
        }


        /// <summary>
        /// Called after the associated object marker has been changed
        /// </summary>
        protected virtual void OnAssociatedMarkerChanged()
        {
        }
        /// <summary>
        /// Called if the object requests custom gripper objects
        /// </summary>
        /// <param name="result"></param>
        protected virtual void OnQueryCustomGrippers(ObjectSelectionMarker.CustomGripperQueryEventArgs result)
        {

        }
        #endregion

        #region Object movement and resizing
        public void BeginMoveOrResize(bool moving, bool resizing)
        {
            _is_moving = moving;
            _is_resizing = resizing;
            WasMoved = false;

            InternalClearDataBindings();
        }

        public void ResetMoveAndResize()
        {
            _is_moving = false;
            _is_resizing = false;

            InternalResetLastValues();
        }

        public virtual bool OverlapX(PointObject<Tx, Ty> other)
        {
            return false;
        }

        public virtual bool OverlapY(PointObject<Tx, Ty> other)
        {
            return false;
        }

        public virtual bool Overlap(PointObject<Tx, Ty> other)
        {
            return OverlapX(other) && OverlapY(other);
        }

        /// <summary>
        /// Calculates the distance in pixel to the reference object. If the objects do not overlap in y-direction
        /// or if the object is not to the right, the method returns double.MaxValue.
        /// </summary>
        /// <param name="right_object"></param>
        /// <returns></returns>
        public virtual double GetDistanceToRightObject(PointObject<Tx, Ty> right_object)
        {
            return double.MaxValue;
        }

        /// <summary>
        /// Calculates the distance in pixel to the reference object. If the objects do not overlap in y-direction
        /// or if the object is not to the left, the method returns double.MaxValue.
        /// </summary>
        /// <param name="left_object"></param>
        /// <returns></returns>
        public virtual double GetDistanceToLeftObject(PointObject<Tx, Ty> left_object)
        {
            return double.MaxValue;
        }

        /// <summary>
        /// Calculates the distance in pixel to the reference object. If the objects do not overlap in x-direction
        /// or if the object is not to the bottom, the method returns double.MaxValue.
        /// </summary>
        /// <param name="bottom_object"></param>
        /// <returns></returns>
        public virtual double GetDistanceToBottomObject(PointObject<Tx, Ty> bottom_object)
        {
            return double.MaxValue;
        }

        /// <summary>
        /// Checks if the object may be moved to the given position
        /// </summary>
        /// <param name="target_x_value"></param>
        /// <param name="target_y_value"></param>
        /// <param name="x_docking_offset"></param>
        /// <param name="y_docking_offest"></param>
        /// <param name="ignore_not_can_move_on_multi_selection"></param>
        /// <returns></returns>
        public bool CanMoveTo(Tx target_x_value, Ty target_y_value, double x_docking_offset, double y_docking_offest, out bool ignore_not_can_move_on_multi_selection)
        {
            //Debug.WriteLine("  CanMoveTo ");
            bool can_move = true;
            OnPreviewObjectMove(target_x_value, target_y_value, x_docking_offset, y_docking_offest, out can_move, out ignore_not_can_move_on_multi_selection);
            return can_move;
        }

        internal virtual void SetValues(Tx target_x_value, Ty target_y_value)
        {
            //Debug.WriteLine("  Point Object SetValues=" + target_x_value + ", Y=" + target_y_value + ", old values " + XValue + " " + YValue);

            XValue = target_x_value;
            YValue = target_y_value;
        }

        /// <summary>
        /// Moves the object to a given position
        /// </summary>
        /// <param name="target_x_value"></param>
        /// <param name="target_y_value"></param>
        /// <param name="x_offset_in_pixel"></param>
        /// <param name="y_offest_in_pixel"></param>
        public void MoveTo(Tx target_x_value, Ty target_y_value, double x_offset_in_pixel, double y_offest_in_pixel)
        {
            //Debug.WriteLine(" Pointobject MoveTo");
            bool ignore_not_can_move_on_multi_selection;
            if (CanMoveTo(target_x_value, target_y_value, x_offset_in_pixel, y_offest_in_pixel, out ignore_not_can_move_on_multi_selection))
            {
                _is_moving = false;
                _is_resizing = false;

                OnObjectMoved(target_x_value, target_y_value, x_offset_in_pixel, y_offest_in_pixel);
                InternalUpdateDataBindings();
                OnObjectMoved();
            }
        }

        /// <summary>
        /// Checks if the object may be resized to the given dimensions
        /// </summary>
        /// <param name="target_from_x_value"></param>
        /// <param name="target_from_y_value"></param>
        /// <param name="target_to_x_value"></param>
        /// <param name="target_to_y_value"></param>
        /// <returns></returns>
        public bool CanResizeTo(Tx target_from_x_value, Ty target_from_y_value, Tx target_to_x_value, Ty target_to_y_value)
        {
            if (!AmIAnArea)
                return false;

            bool can_resize = true;
            OnPreviewObjectResize(target_from_x_value, target_from_y_value, target_to_x_value, target_to_y_value, out can_resize);
            return can_resize;
        }
        /// <summary>
        /// Commits the resizing operation
        /// </summary>
        /// <param name="target_from_x_value"></param>
        /// <param name="target_from_y_value"></param>
        /// <param name="target_to_x_value"></param>
        /// <param name="target_to_y_value"></param>
        public void ResizeTo(Tx target_from_x_value, Ty target_from_y_value, Tx target_to_x_value, Ty target_to_y_value)
        {
            if (CanResizeTo(target_from_x_value, target_from_y_value, target_to_x_value, target_to_y_value))
            {
                _is_moving = false;
                _is_resizing = false;

                OnObjectResized(target_from_x_value, target_from_y_value, target_to_x_value, target_to_y_value);
                InternalUpdateDataBindings();
                OnObjectResized();
            }
        }
        #endregion

        #region Touch manipulation helpers
        //public void InitializeTouchManipulation(Canvas hosting_canvas)
        //{
        //    if(_touch_manipulator != null && _manipulator_host_canvas != hosting_canvas)
        //        RemoveTouchManipulation();

        //    if(_touch_manipulator != null)
        //        return;

        //    _manipulator_host_canvas = hosting_canvas;

        //    ReInitTouchManipulation();
        //}

        //internal void ReInitTouchManipulation()
        //{
        //    if (_touch_manipulator != null)
        //    {
        //        _touch_manipulator.DeRegister();
        //        _touch_manipulator.Dispose();
        //        _touch_manipulator = null;
        //    }

        //    if (AssociatedObjectMarker == null || _manipulator_host_canvas == null)
        //        return;

        //    if (HostingDiagram == null)
        //        return;

        //    UiManipulator.EnabledAxes axes = UiManipulator.EnabledAxes.All;

        //    _touch_manipulator = new DiagramTouchInertiaProcessManipulation(_shape_visual, _manipulator_host_canvas, _manipulator_host_canvas, axes, HostingDiagram.XAxisControl,
        //                                                                    HostingDiagram.YAxisControl, CanMove, CanResizeX, CanResizeY);

        //    RegisterTouchManipulatorEvents();
        //}

        //public void RemoveTouchManipulation()
        //{
        //    if(_touch_manipulator != null)
        //    {
        //        DeregisterTouchManipulatorEvents();
        //        _touch_manipulator.DeRegister();
        //        _touch_manipulator.Dispose();
        //        _touch_manipulator = null;
        //    }

        //    _manipulator_host_canvas = null;
        //}

        //private void RegisterTouchManipulatorEvents()
        //{
        //    if (_touch_manipulator == null)
        //        return;

        //    _touch_manipulator.UiManipulationStarted += new EventHandler<UiManipulationEventArgs>(_touch_manipulator_UiManipulationStarted);
        //    _touch_manipulator.UiManipulationInertiaCompleted += new EventHandler<UiManipulationEventArgs>(_touch_manipulator_UiManipulationInertiaCompleted);
        //    _touch_manipulator.UiManipulationCompleted += new EventHandler<UiManipulationEventArgs>(_touch_manipulator_UiManipulationCompleted);
        //    _touch_manipulator.UiElementPositionChanged += new EventHandler<UiManipulationEventArgs>(_touch_manipulator_UiElementPositionChanged);
        //}


        //private void DeregisterTouchManipulatorEvents()
        //{
        //    if (_touch_manipulator == null)
        //        return;

        //    _touch_manipulator.UiManipulationStarted -= _touch_manipulator_UiManipulationStarted;
        //    _touch_manipulator.UiManipulationInertiaCompleted -= _touch_manipulator_UiManipulationInertiaCompleted;
        //    _touch_manipulator.UiManipulationCompleted -= _touch_manipulator_UiManipulationCompleted;
        //    _touch_manipulator.UiElementPositionChanged -= _touch_manipulator_UiElementPositionChanged;
        //}


        //#region Wire through touch events
        //void _touch_manipulator_UiElementPositionChanged(object sender, UiManipulationEventArgs e)
        //{
        //    if (UiElementPositionChanged != null)
        //        UiElementPositionChanged(this, e);
        //}

        //void _touch_manipulator_UiManipulationCompleted(object sender, UiManipulationEventArgs e)
        //{
        //    if (UiManipulationCompleted != null)
        //        UiManipulationCompleted(this, e);
        //}

        //void _touch_manipulator_UiManipulationInertiaCompleted(object sender, UiManipulationEventArgs e)
        //{
        //    if (UiManipulationInertiaCompleted != null)
        //        UiManipulationInertiaCompleted(this, e);
        //}

        //void _touch_manipulator_UiManipulationStarted(object sender, UiManipulationEventArgs e)
        //{
        //    if (UiManipulationStarted != null)
        //        UiManipulationStarted(this, e);
        //}
        //#endregion

        private void RegisterStylusAction(int stylus_id)
        {
            if (!_active_stylus_ids.Contains(stylus_id))
                _active_stylus_ids.Add(stylus_id);
        }

        private void DeregisterStylusAction(int stylus_id)
        {
            if (_active_stylus_ids.Contains(stylus_id))
                _active_stylus_ids.Remove(stylus_id);
        }

        private bool IsStylusActionRegistered(int stylus_id)
        {
            return _active_stylus_ids.Contains(stylus_id);
        }
        #endregion

        #endregion

        #region Event Handlers
        void _shape_visual_MouseWheel(object sender, System.Windows.Input.MouseWheelEventArgs e)
        {
            if (e.StylusDevice != null && e.StylusDevice.Id != 0)
                return;

            OnMouseWheel(e);
            e.Handled = false;
        }

        void _shape_visual_MouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        {
            if (e.StylusDevice != null && e.StylusDevice.Id != 0)
                return;

            if (_is_moving || _is_resizing)
            {
                _is_lbutton_down = false;
                _is_rbutton_down = false;
            }

            OnMouseMove(e);
            e.Handled = false; // force routed event wireing
        }

        void _shape_visual_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (e.StylusDevice != null && e.StylusDevice.Id != 0)
                return;

            Debug.WriteLine("### MOUSE (--) UP (" + this.GetHashCode() + ")");

            OnMouseUp(e);
            e.Handled = false; // force routed event wireing

            _last_up_stylus = int.MinValue;

            if (_fire_double_click)
            {
                _is_rbutton_down = false;
                _is_lbutton_down = false;
                OnDoubleClicked();
            }
            else if (e.ChangedButton == System.Windows.Input.MouseButton.Left && _is_lbutton_down)
            {
                _is_lbutton_down = false;
                OnLButtonClicked();
            }
            else if (e.ChangedButton == System.Windows.Input.MouseButton.Right && _is_rbutton_down)
            {
                _is_rbutton_down = false;
                OnRButtonClicked();
            }
        }

        void _shape_visual_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (e.StylusDevice != null && e.StylusDevice.Id != 0)
                return;

            _last_up_stylus = int.MinValue;

            if (e.ClickCount == 2)
                _fire_double_click = true;
            else
                _fire_double_click = false;


            if (e.ChangedButton == System.Windows.Input.MouseButton.Left)
                _is_lbutton_down = true;
            else if (e.ChangedButton == System.Windows.Input.MouseButton.Right)
                _is_rbutton_down = true;

            Debug.WriteLine("### MOUSE (--) DOWN (" + this.GetHashCode() + ")");

            OnMouseDown(e);
            e.Handled = false; // force routed event wireing
        }

        void _shape_visual_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            if (e.StylusDevice != null && e.StylusDevice.Id != 0)
                return;

            //Debug.WriteLine("### Mouse LEAVE (" + this.GetHashCode() + ")");
            _is_mouse_over = false;
            _is_lbutton_down = false;
            _is_rbutton_down = false;

            OnMouseLeave(e);
            e.Handled = false; // force routed event wireing
        }

        void _shape_visual_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            if (e.StylusDevice != null && e.StylusDevice.Id != 0)
                return;

            //Debug.WriteLine("### Mouse ENTER (" + this.GetHashCode() + ")");
            _is_mouse_over = true;
            _is_lbutton_down = false;
            _is_rbutton_down = false;

            OnMouseEnter(e);
            e.Handled = false; // force routed event wireing
        }

        void _shape_visual_StylusEnter(object sender, System.Windows.Input.StylusEventArgs e)
        {
            if (e.StylusDevice == null)
                return;

            RegisterStylusAction(e.StylusDevice.Id);

            //Debug.WriteLine("### STYLUS (" + e.StylusDevice.Id + ") ENTER (" + this.GetHashCode() + ")");

            OnStylusEnter(e);
            e.Handled = false;
        }

        void _shape_visual_StylusLeave(object sender, System.Windows.Input.StylusEventArgs e)
        {
            if (e.StylusDevice == null)
                return;

            if (!IsStylusActionRegistered(e.StylusDevice.Id))
                return;

            //Debug.WriteLine("### STYLUS (" + e.StylusDevice.Id + ") LEAVE (" + this.GetHashCode() + ")");

            //_is_mouse_over = false;
            //_is_lbutton_down = false;
            //_is_rbutton_down = false;


            OnStylusLeave(e);
            e.Handled = false;

            DeregisterStylusAction(e.StylusDevice.Id);
        }

        void _shape_visual_StylusMove(object sender, System.Windows.Input.StylusEventArgs e)
        {
            if (e.StylusDevice == null)
                return;

            if (!IsStylusActionRegistered(e.StylusDevice.Id))
                return;

            //Debug.WriteLine("### STYLUS (" + e.StylusDevice.Id + ") MOVE (" + this.GetHashCode() + ")");

            if (_is_moving || _is_resizing)
            {
                _is_lbutton_down = false;
                _is_rbutton_down = false;
            }

            OnStylusMove(e);
            e.Handled = false; // force routed event wireing
        }

        void _shape_visual_StylusDown(object sender, System.Windows.Input.StylusDownEventArgs e)
        {
            if (e.StylusDevice == null)
                return;

            RegisterStylusAction(e.StylusDevice.Id);

            Debug.WriteLine("### STYLUS (" + e.StylusDevice.Id + ") DOWN (" + this.GetHashCode() + ")");

            OnStylusDown(e);
            e.Handled = false;
        }

        void _shape_visual_StylusUp(object sender, System.Windows.Input.StylusEventArgs e)
        {
            if (e.StylusDevice == null)
                return;

            if (!IsStylusActionRegistered(e.StylusDevice.Id))
                return;

            Debug.WriteLine("### STYLUS (" + e.StylusDevice.Id + ") UP (" + this.GetHashCode() + ")");


            OnStylusUp(e);
            e.Handled = false; // force routed event wireing

            if (_t4 != null)
            {
                _t4.Elapsed -= _t4_Elapsed;
                _t4.Stop();
                _t4.Close();
                _t4.Dispose();
                _t4 = null;
            }

            if (_last_up_stylus == e.StylusDevice.Id)
            {
                // wenn innerhalb von 200ms 2x der selbe stylus-up kommt 
                // doppelklick

                OnDoubleClicked();
            }
            else
            {
                _last_up_stylus = e.StylusDevice.Id;

                _last_active_count = _active_stylus_ids.Count;
                _t4 = new Timer();
                _t4.Interval = 200;
                _t4.Elapsed += new ElapsedEventHandler(_t4_Elapsed);
                _t4.Start();
            }

            DeregisterStylusAction(e.StylusDevice.Id);
        }

        void _t4_Elapsed(object sender, ElapsedEventArgs e)
        {
            _t4.Elapsed -= _t4_Elapsed;
            _t4.Stop();
            _t4.Close();
            _t4.Dispose();
            _t4 = null;

            _last_up_stylus = int.MinValue;

            if (_last_active_count == 1 && !_is_moving && !_is_resizing)
            {
                // linksclick wenn nur 1 stylus getapped wurde
                _is_rbutton_down = false;
                _is_lbutton_down = false;
                if (Dispatcher.CheckAccess())
                {
                    OnLButtonClicked();
                }
                else
                {
                    Dispatcher.Invoke(DispatcherPriority.Normal, (NoParas)delegate
                    {
                        OnLButtonClicked();
                    });
                }
            }
        }

        void _shape_visual_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            OnSizeChanged(e);
            ForceRedraw(e.NewSize);
            //ViewMode = CalculateViewMode(e.NewSize);
            //if (ViewMode == ObjectViewMode.Culled || ViewMode == ObjectViewMode.Invisible)
            //{
            //    _shape_visual.Visibility = Visibility.Hidden;
            //}
            //else
            //{
            //    _shape_visual.Visibility = Visibility.Visible;

            //    //Debug.WriteLine("************** CHANGE SIZE FROM " + ShapeWidth + " " + ShapeHeight + " TO " + _shape_visual.ActualWidth + " " + _shape_visual.ActualHeight);
            //    ShapeWidth = _shape_visual.ActualWidth;
            //    ShapeHeight = _shape_visual.ActualHeight;

            //    //_content_visuals = null;
            //    Decorator object_visual = ObjectVisual;
            //}

            e.Handled = true;
        }

        public void ForceRedraw()
        {
            ForceRedraw(new Size(ObjectVisual.ActualWidth, ObjectVisual.ActualHeight));
        }

        private void ForceRedraw(Size size)
        {
            //Debug.WriteLine("Force Redraw");
            //if(size.Width == 0 && size.Height == 0)
            //size = new Size(ObjectVisual.ActualWidth, ObjectVisual.ActualHeight);

            ViewMode = CalculateViewMode(size);
            if (ViewMode == ObjectViewMode.Culled || ViewMode == ObjectViewMode.Invisible)
            {
                _shape_visual.Visibility = Visibility.Hidden;
            }
            else
            {
                _shape_visual.Visibility = Visibility.Visible;

                //Debug.WriteLine("************** CHANGE SIZE FROM " + ShapeWidth + " " + ShapeHeight + " TO " + _shape_visual.ActualWidth + " " + _shape_visual.ActualHeight);
                ShapeWidth = _shape_visual.ActualWidth;
                ShapeHeight = _shape_visual.ActualHeight;

                //_content_visuals = null;
                Decorator object_visual = ObjectVisual;
            }
        }

        private static void OnIsXDiscreteChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ((PointObject<Tx, Ty>)d).InternalOnPropertyChanged(new PropertyChangedMeta("IsXDiscrete", e.OldValue, e.NewValue));
        }

        private static void OnIsYDiscreteChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ((PointObject<Tx, Ty>)d).InternalOnPropertyChanged(new PropertyChangedMeta("IsYDiscrete", e.OldValue, e.NewValue));
        }


        void _associated_object_marker_QueryCustomGrippers(object sender, ObjectSelectionMarker.CustomGripperQueryEventArgs e)
        {
            OnQueryCustomGrippers(e);
        }

        #endregion

        #region Properties

        #region abstract properties
        /// <summary>
        /// Gets a property if the current object may be moved
        /// </summary>
        public abstract bool CanMove { get; }
        /// <summary>
        /// Gets a property if the current object may be selected
        /// </summary>
        public abstract bool CanSelect { get; }

        #endregion

        /// <summary>
        /// Gets a flag if the object is an area object
        /// </summary>
        internal bool AmIAnArea
        {
            get
            {
                return (this is AreaObject<Tx, Ty>);
            }
        }
        /// <summary>
        /// Gets the shape width
        /// </summary>
        protected double ShapeWidth
        {
            get;
            set;
        }
        /// <summary>
        /// Gets the shape height
        /// </summary>
        protected double ShapeHeight
        {
            get;
            set;
        }

        /// <summary>
        /// Gets a flag indicating if the object may be resized
        /// </summary>
        public bool CanResize
        {
            get { return CanResizeX || CanResizeY; }
        }
        /// <summary>
        /// Gets a property if the current object may be resized in x-axis direction
        /// </summary>
        public virtual bool CanResizeX
        {
            get { return false; }
        }
        /// <summary>
        /// Gets a property if the current object may be resized in y-axis direction
        /// </summary>
        public virtual bool CanResizeY
        {
            get { return false; }
        }

        /// <summary>
        /// Gets/Sets a flag if the current object is selected
        /// </summary>
        public bool IsSelected
        {
            get { return _is_selected; }
            set
            {
                if (_is_selected != value)
                {
                    bool oldValue = _is_selected;
                    SendPropertyChanging("IsSelected");
                    _is_selected = value;
                    SendPropertyChanged("IsSelected");

                    InternalOnPropertyChanged(new PropertyChangedMeta("IsSelected", oldValue, _is_selected));

                    Debug.WriteLine("### IsSelected changed (" + this.GetHashCode() + "): " + _is_selected.ToString());
                }
            }
        }

        /// <summary>
        /// Gets/Sets a flag if the current object is hittest visible or not
        /// </summary>
        public bool IsHitTestVisible
        {
            get { return _is_hittest_visible; }
            set
            {
                if (_is_hittest_visible != value)
                {
                    bool oldValue = _is_hittest_visible;
                    SendPropertyChanging("IsHitTestVisible");
                    _is_hittest_visible = value;
                    SendPropertyChanged("IsHitTestVisible");

                    InternalOnPropertyChanged(new PropertyChangedMeta("IsHitTestVisible", oldValue, _is_hittest_visible));
                }
            }
        }

        /// <summary>
        /// Gets/Sets the object's view mode
        /// </summary>
        public ObjectViewMode ViewMode
        {
            get { return _view_mode; }
            set
            {
                if (_view_mode != value)
                {
                    ObjectViewMode oldValue = _view_mode;
                    SendPropertyChanging("ViewMode");
                    _view_mode = value;
                    SendPropertyChanged("ViewMode");

                    InternalOnPropertyChanged(new PropertyChangedMeta("ViewMode", oldValue, _view_mode));
                }
            }
        }

        /// <summary>
        /// Gets the current shape visual
        /// </summary>
        public Decorator ObjectVisual
        {
            get
            {
                if (_shape_visual == null)
                {
                    _shape_visual = CreateShapeVisual();
                    _shape_visual.IsHitTestVisible = IsHitTestVisible;
                    InternalRegisterEvents();
                    InternalUpdateBindings();
                }

                if (_shape_visual == null)
                    throw new ArgumentException("Shape visual of diagram object may not be null");

                if (_content_visuals == null)
                {
                    _content_visuals = CreateContentVisuals();

                    if (_content_visuals == null)
                        _content_visuals = new Dictionary<ObjectViewMode, FrameworkElement>();
                    //foreach (ObjectViewMode curMode in _content_visuals.Keys)
                    //    _content_visuals[curMode].IsHitTestVisible = IsHitTestVisible;
                }

                if (_content_visuals.ContainsKey(_view_mode))
                {
                    if (_shape_visual.Child != null)
                        _shape_visual.Child = null;

                    _shape_visual.Child = _content_visuals[_view_mode];

                    TextBlock text_block = _content_visuals[_view_mode] as TextBlock;
                    if (text_block != null && text_block.ToolTip != null)
                    {
                        _shape_visual.ToolTip = text_block.ToolTip;
                    }
                }
                return _shape_visual;
            }
        }

        /// <summary>
        /// Gets/sets the x value
        /// </summary>
        public Tx XValue
        {
            get { return _x_value; }
            set
            {
                Tx oldValue = _x_value;
                SendPropertyChanging("XValue");
                _x_value = value;
                SendPropertyChanged("XValue");

                InternalOnPropertyChanged(new PropertyChangedMeta("XValue", oldValue, _x_value));
            }
        }
        /// <summary>
        /// Gets/Sets the y value
        /// </summary>
        public Ty YValue
        {
            get { return _y_value; }
            set
            {
                Ty oldValue = _y_value;
                SendPropertyChanging("YValue");
                _y_value = value;
                SendPropertyChanged("YValue");

                InternalOnPropertyChanged(new PropertyChangedMeta("YValue", oldValue, _y_value));
            }
        }

        /// <summary>
        /// Gets the relative binding source for the parents
        /// </summary>
        protected RelativeSource DiagramBindingSource
        {
            get
            {
                if (_diagram_binding_source != null)
                    return _diagram_binding_source;

                _diagram_binding_source = new RelativeSource(RelativeSourceMode.FindAncestor, typeof(DiagramBase<Tx, Ty>), 1);

                return _diagram_binding_source;
            }
        }

        /// <summary>
        /// Gets a flag indicating if the object is currently moving
        /// </summary>
        public bool IsMoving
        {
            get { return _is_moving; }
        }

        /// <summary>
        /// Gets a flag indicating if the object is currently resizing
        /// </summary>
        public bool IsResizing
        {
            get { return _is_resizing; }
        }

        /// <summary>
        /// Returns a flag if this object is currently hidden
        /// </summary>
        public bool IsHidden
        {
            get
            {
                return ViewMode == ObjectViewMode.Invisible || ViewMode == ObjectViewMode.Culled;
            }
        }

        public bool IsDockingMoveObject(out double left_distance, out double right_distance, out double bottom_distance)
        {
            bool is_docking_move_object = false;
            left_distance = double.MaxValue;
            right_distance = double.MaxValue;
            bottom_distance = double.MaxValue;

            if (LeftDockingCandidate.Key != null && LeftDockingCandidate.Value != default(double))
            {
                is_docking_move_object = true;
                left_distance = LeftDockingCandidate.Value;
            }
            if (RightDockingCandidate.Key != null && RightDockingCandidate.Value != default(double))
            {
                is_docking_move_object = true;
                right_distance = RightDockingCandidate.Value;
            }
            if (BottomDockingCandidate.Key != null && BottomDockingCandidate.Value != default(double))
            {
                is_docking_move_object = true;
                bottom_distance = BottomDockingCandidate.Value;
            }

            return is_docking_move_object;
        }

        public bool IsDockingMoveObject()
        {
            bool is_docking_move_object = false;

            if (LeftDockingCandidate.Key != null && LeftDockingCandidate.Value != default(double))
            {
                is_docking_move_object = true;
            }
            if (RightDockingCandidate.Key != null && RightDockingCandidate.Value != default(double))
            {
                is_docking_move_object = true;
            }
            if (BottomDockingCandidate.Key != null && BottomDockingCandidate.Value != default(double))
            {
                is_docking_move_object = true;
            }

            return is_docking_move_object;
        }

        public virtual KeyValuePair<PointObject<Tx, Ty>, double> LeftDockingCandidate
        {
            get { return _left_docking_candidate; }
            set
            {
                if (!value.Equals(_left_docking_candidate))
                {
                    _left_docking_candidate = value;

                    SendPropertyChanged("LeftDockingCandidate");
                }

                if (AssociatedObjectMarker != null)
                {
                    AssociatedObjectMarker.DockLeft = _left_docking_candidate.Key == null ? false : true;
                    AssociatedObjectMarker.SetLeftDistance(_left_docking_candidate.Value);
                }
            }
        }

        public virtual KeyValuePair<PointObject<Tx, Ty>, double> RightDockingCandidate
        {
            get { return _right_docking_candidate; }
            set
            {
                if (!value.Equals(_right_docking_candidate))
                {
                    _right_docking_candidate = value;

                    SendPropertyChanged("RightDockingCandidate");
                }

                if (AssociatedObjectMarker != null)
                {
                    AssociatedObjectMarker.DockRight = _right_docking_candidate.Key == null ? false : true;
                    AssociatedObjectMarker.SetRightDistance(_right_docking_candidate.Value);
                }
            }
        }

        public virtual KeyValuePair<PointObject<Tx, Ty>, double> BottomDockingCandidate
        {
            get { return _bottom_docking_candidate; }
            set
            {
                if (!value.Equals(_bottom_docking_candidate))
                {
                    _bottom_docking_candidate = value;

                    SendPropertyChanged("BottomDockingCandidate");
                }

                if (AssociatedObjectMarker != null)
                {
                    AssociatedObjectMarker.DockBottom = _bottom_docking_candidate.Key == null ? false : true;
                    AssociatedObjectMarker.SetBottomDistance(_bottom_docking_candidate.Value);
                }
            }
        }

        public virtual KeyValuePair<PointObject<Tx, Ty>, double> TopDockingCandidate
        {
            get { return _top_docking_candidate; }
            set
            {
                if (!value.Equals(_top_docking_candidate))
                {
                    _top_docking_candidate = value;

                    SendPropertyChanged("TopDockingCandidate");
                }

                if (AssociatedObjectMarker != null)
                    AssociatedObjectMarker.DockTop = _top_docking_candidate.Key == null ? false : true;
            }
        }

        public bool HasSameStylusId(PointObject<Tx, Ty> second)
        {
            if (UiManipulationMeta != null && UiManipulationMeta.RegisteredStylusId != null && second.UiManipulationMeta != null && second.UiManipulationMeta.RegisteredStylusId != null)
            {
                return UiManipulationMeta.RegisteredStylusId.Value == second.UiManipulationMeta.RegisteredStylusId;
            }

            return false;
        }

        public bool HasStylusId(int? stylus_id)
        {
            if (UiManipulationMeta != null)
            {
                if (UiManipulationMeta.RegisteredStylusId == null)
                {
                    if (stylus_id == null) 
                        return true;
                }
                else
                    return UiManipulationMeta.RegisteredStylusId.Value == stylus_id;
            }

            return false;
        }

        public int? GetStylusId()
        {
            if (UiManipulationMeta != null && UiManipulationMeta.RegisteredStylusId != null)
            {
                return UiManipulationMeta.RegisteredStylusId.Value;
            }

            return null;
        }

        /// <summary>
        /// Gets the last known good X value
        /// </summary>
        internal Tx LastX
        {
            get
            {
                return _last_x_value;
            }
        }
        /// <summary>
        /// Gets the last known good Y value
        /// </summary>
        internal Ty LastY
        {
            get
            {
                return _last_y_value;
            }
        }

        /// <summary>
        /// Gets the hosting driagram instance
        /// </summary>
        /// <remarks>May return null if the object if not part of a diagram</remarks>
        public DiagramBase<Tx, Ty> HostingDiagram
        {
            get { return _hosting_diagram; }
            internal set
            {
                if (_hosting_diagram != value)
                {
                    _hosting_diagram = value;
                    //if (_hosting_diagram != null)
                    //    InitializeTouchManipulation(_hosting_diagram.ElementCanvas);
                    //else
                    //    RemoveTouchManipulation();
                }
            }
        }

        /// <summary>
        /// Gets a flag indicating if this object is currently invisible due to an active filter
        /// </summary>
        /// <remarks>False means this item didn't pass the filter check and is currently invisible</remarks>
        public bool IsFiltered
        {
            get { return _is_filtered; }
            internal set
            {
                bool oldValue = _is_filtered;
                SendPropertyChanging("IsFiltered");
                _is_filtered = value;
                SendPropertyChanged("IsFiltered");

                InternalOnPropertyChanged(new PropertyChangedMeta("IsFiltered", oldValue, _is_filtered));
            }
        }

        /// <summary>
        /// Gets/Serts a flag indicating if the object uses custom sizing grippers for the ObjectSelectionMarker
        /// </summary>
        public bool UsesCustomGrippers
        {
            get { return _uses_custom_grippers; }
            set
            {
                bool oldValue = _uses_custom_grippers;
                SendPropertyChanging("UsesCustomGrippers");
                _uses_custom_grippers = value;
                SendPropertyChanged("UsesCustomGrippers");

                InternalOnPropertyChanged(new PropertyChangedMeta("UsesCustomGrippers", oldValue, _uses_custom_grippers));
                if (_associated_object_marker != null)
                    _associated_object_marker.UsesCustomGrippers = _uses_custom_grippers;
            }
        }

        /// <summary>
        /// Gets a flag indicating if the current object is displayed in the background
        /// </summary>
        public bool IsBackgroundObject
        {
            get { return _is_background_object; }
            internal set
            {
                bool oldValue = _is_background_object;
                SendPropertyChanging("IsBackgroundObject");
                _is_background_object = value;
                SendPropertyChanged("IsBackgroundObject");

                InternalOnPropertyChanged(new PropertyChangedMeta("IsBackgroundObject", oldValue, _is_background_object));
            }
        }
        /// <summary>
        /// Gets the associated object marker control
        /// </summary>
        public ObjectSelectionMarker AssociatedObjectMarker
        {
            get { return _associated_object_marker; }
            set
            {
                if (_associated_object_marker != null)
                    _associated_object_marker.QueryCustomGrippers -= _associated_object_marker_QueryCustomGrippers;

                if (_associated_object_marker != value)
                {
                    _associated_object_marker = value;
                    _associated_object_marker.QueryCustomGrippers += new EventHandler<ObjectSelectionMarker.CustomGripperQueryEventArgs>(_associated_object_marker_QueryCustomGrippers);

                    if (_uses_custom_grippers)
                        _associated_object_marker.UsesCustomGrippers = _uses_custom_grippers;

                    //ReInitTouchManipulation();
                    OnAssociatedMarkerChanged();
                }
            }
        }

        /// <summary>
        /// Gets the internal reference for storing ui manipulation metainformation
        /// </summary>
        internal ObjectUiManupulationMeta UiManipulationMeta
        {
            get { return _object_ui_manipulation_meta; }
        }

        internal bool WasMoved
        {
            get;
            set;
        }
        #endregion

        #region Dependency Properties

        public bool IsXDiscrete
        {
            get { return (bool)GetValue(IsXDiscreteProperty); }
            set
            {
                //bool oldValue = IsXDiscrete;
                //SendPropertyChanging("IsXDiscrete");
                SetValue(IsXDiscreteProperty, value);
                //SendPropertyChanged("IsXDiscrete");
            }
        }

        // Using a DependencyProperty as the backing store for IsXDiscrete.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsXDiscreteProperty =
            DependencyProperty.Register("IsXDiscrete", typeof(bool), typeof(PointObject<Tx, Ty>), new UIPropertyMetadata(false, new PropertyChangedCallback(PointObject<Tx, Ty>.OnIsXDiscreteChanged)));

        public bool IsYDiscrete
        {
            get { return (bool)GetValue(IsYDiscreteProperty); }
            set
            {
                //bool oldValue = IsYDiscrete;
                //SendPropertyChanging("IsYDiscrete");
                SetValue(IsYDiscreteProperty, value);
                //SendPropertyChanged("IsYDiscrete");
            }
        }

        // Using a DependencyProperty as the backing store for IsXDiscrete.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsYDiscreteProperty =
            DependencyProperty.Register("IsYDiscrete", typeof(bool), typeof(PointObject<Tx, Ty>), new UIPropertyMetadata(false, new PropertyChangedCallback(PointObject<Tx, Ty>.OnIsYDiscreteChanged)));


        #endregion

        #region Events

        public event EventHandler LButtonClicked;
        public event EventHandler RButtonClicked;
        public event EventHandler DoubleClicked;

        public event EventHandler ObjectMoved;
        public event EventHandler ObjectResized;

        public event PropertyChangingEventHandler PropertyChanging;
        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Routed Events
        #endregion

        #region Attributes

        protected delegate void NoParas();

        private RelativeSource _diagram_binding_source = null;
        private DiagramBase<Tx, Ty> _hosting_diagram = null;

        private Tx _x_value;
        private Ty _y_value;

        private Tx _last_x_value;
        private Ty _last_y_value;

        private bool _is_selected = false;
        private bool _is_hittest_visible = true;

        private bool _is_lbutton_down = false;
        private bool _is_rbutton_down = false;
        private bool _is_mouse_over = false;
        private bool _is_moving = false;
        private bool _is_resizing = false;

        private bool _is_x_discrete = false;
        private bool _is_y_discrete = false;
        private bool _is_background_object = false;

        private bool _is_filtered = true;

        private bool _uses_custom_grippers = false;

        private Decorator _shape_visual = null;
        private Dictionary<ObjectViewMode, FrameworkElement> _content_visuals = null;
        private ObjectViewMode _view_mode = ObjectViewMode.Detail1;

        private ObjectSelectionMarker _associated_object_marker;
        private ObjectUiManupulationMeta _object_ui_manipulation_meta = new ObjectUiManupulationMeta();

        private bool _disposed = false;
        private bool _disposing = false;

        private List<int> _active_stylus_ids = new List<int>();
        private Canvas _manipulator_host_canvas = null;

        private KeyValuePair<PointObject<Tx, Ty>, double> _left_docking_candidate;
        private KeyValuePair<PointObject<Tx, Ty>, double> _right_docking_candidate;
        private KeyValuePair<PointObject<Tx, Ty>, double> _bottom_docking_candidate;
        private KeyValuePair<PointObject<Tx, Ty>, double> _top_docking_candidate;

        private int _last_up_stylus = int.MinValue;
        private int _last_active_count;
        private Timer _t4;
        private bool _fire_double_click = false;
        #endregion

        #region Tests

        #endregion

    }
}
