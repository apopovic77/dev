﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Logicx.WpfUtility.CustomControls
{
    /// <summary>
    /// Interaktionslogik für OneLevelTreeViewItem.xaml
    /// </summary>
    public partial class OneLevelTreeViewItem : UserControl
    {
        #region Constructors
        public OneLevelTreeViewItem()
        {
            InitializeComponent();
        }

        public OneLevelTreeViewItem(IOneLevelTreeViewItem ref_item)
        {
            InitializeComponent();
            _ref_treeview_item = ref_item;
            item_header.Text = ref_item.ItemHeader;
        }

        public OneLevelTreeViewItem(String display_text)
        {
            InitializeComponent();
            item_header.Text = display_text;
            _is_always_active = false;
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Set Active presentation
        /// </summary>
        public void SetActive()
        {
            pointer_active.Opacity = 1;
            _is_set_active = true;
        }

        /// <summary>
        /// set inactive presentation
        /// </summary>
        public void SetInactive()
        {
            if (!_is_always_active)
            {
                pointer_active.Opacity = 0;
                _is_set_active = false;
            }
        }

        /// <summary>
        /// zeigt an ob das Item aktiviert ist oder nicht
        /// </summary>
        public bool IsSetActive
        {
            get
            {
                return _is_set_active;
            }
        }

        /// <summary>
        /// gibt das Daten-Element zudem dieses visuelle Element gehört
        /// </summary>
        public IOneLevelTreeViewItem GetRefTreeViewItem
        {
            get { return _ref_treeview_item; }
        }
        #endregion

        #region EventHandler
        /// <summary>
        /// Wenn das Control geladen ist, werden die Effekte zum Markieren der einzelnen Elemente in 
        /// Bilder konvertiert, und es werden anschließend nur noch die Bilder verwendet. Somit
        /// muss der Glow-Effekt nicht ständig neu berechnet werden. 
        /// Da für alle Items dieselben Bilder verwendet werden, werden diese in statische Variablen abgespeichert. 
        /// Somit können alle erzeugten Items dieselben Bilder verwenden
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            //int offset = 3;
            //if (_mouse_over_image == null)
            //{
            //    pointer_sym_torender.Measure(new Size(pointer_sym_torender.ActualWidth + offset, pointer_sym_torender.ActualHeight + offset));
            //    pointer_sym_torender.Arrange(new Rect(0, 0, pointer_sym_torender.ActualWidth + offset, pointer_sym_torender.ActualHeight + offset));
            //    pointer_sym_torender.UpdateLayout();

            //    _mouse_over_image = new RenderTargetBitmap((int)pointer_sym_torender.ActualWidth + offset, (int)pointer_sym_torender.ActualHeight + offset, 96, 96, PixelFormats.Pbgra32);
            //    _mouse_over_image.Render(pointer_sym_torender);
            //    pointer_sym.Source = _mouse_over_image;
            //}
            //else
            //    pointer_sym.Source =_mouse_over_image;

            //main_grid.Children.Remove(pointer_sym_torender);

            //if (_active_image == null)
            //{
            //    pointer_active_torender.Measure(new Size(pointer_active_torender.ActualWidth + offset, pointer_active_torender.ActualHeight + offset));
            //    pointer_active_torender.Arrange(new Rect(0, 0, pointer_active_torender.ActualWidth + offset, pointer_active_torender.ActualHeight + offset));
            //    pointer_active_torender.UpdateLayout();

            //    _active_image = new RenderTargetBitmap((int)pointer_active_torender.ActualWidth + offset, (int)pointer_active_torender.ActualHeight + offset, 96, 96, PixelFormats.Pbgra32);
            //    _active_image.Render(pointer_active_torender);
            //    pointer_active.Source = _active_image;
            //}
            //else
            //    pointer_active.Source = _active_image;

            //main_grid.Children.Remove(pointer_active_torender);
        }
        #endregion

        #region Static Attributes
        private static RenderTargetBitmap _mouse_over_image;
        private static RenderTargetBitmap _active_image;
        #endregion

        #region Attributes
        protected IOneLevelTreeViewItem _ref_treeview_item;
        protected bool _is_always_active;
        protected bool _is_set_active;
        #endregion      

    }
}
