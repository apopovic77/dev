﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Microsoft.Windows.Controls;
using DataGrid = Microsoft.Windows.Controls.DataGrid;
using DataGridCell = Microsoft.Windows.Controls.DataGridCell;
using DataGridRow = Microsoft.Windows.Controls.DataGridRow;

namespace Logicx.WpfUtility.WpfHelpers
{
    /// <summary> 
    /// Helper methods for UI-related tasks. 
    /// </summary> 
    public class UIHelper
    {
        #region Resource helpers
        /// <summary>
        /// Walks through all mapped ressource dictionaries
        /// </summary>
        /// <param name="resources"></param>
        public static void WalkDictionary(ResourceDictionary resources)
        {
            if (resources == null)
                return;

            foreach (DictionaryEntry entry in resources)
            {
            }

            if (resources.MergedDictionaries == null)
                return;

            foreach (ResourceDictionary rd in resources.MergedDictionaries)
                WalkDictionary(rd);
        }
        #endregion

        #region Check control container
        /// <summary>
        /// Returns true if a check element is part of a tab control's visual tree
        /// </summary>
        /// <param name="checkElement">Element/Control to check</param>
        /// <param name="containingTabItem">A reference receiving the containging tab item</param>
        /// <param name="containingTabControl">A reference receiving the containging tab control</param>
        /// <returns>True if the item is in the visual tree of a tab control</returns>
        public static bool IsInTabControl(DependencyObject checkElement, ref TabItem containingTabItem, ref TabControl containingTabControl)
        {
            bool bRet = false;

            TabItem parentTabItem = TryFindParent<TabItem>(checkElement);

            if (parentTabItem != null)
            {
                TabControl parentTabControl = TryFindParent<TabControl>(parentTabItem);

                if (parentTabControl != null)
                {
                    bRet = true;
                    containingTabItem = parentTabItem;
                    containingTabControl = parentTabControl;
                }
            }

            return bRet;
        }

        /// <summary>
        /// Returns true if the check element is part of an expander control's visual tree
        /// </summary>
        /// <param name="checkElement">Element/Control to check</param>
        /// <param name="containingExpander">A reference receiving the containing expander control</param>
        /// <returns>true if the item is in the visual tree of an expander control</returns>
        public static bool IsInExpander(DependencyObject checkElement, ref Expander containingExpander)
        {
            bool bRet = false;

            Expander parentExpander = TryFindParent<Expander>(checkElement);

            if (parentExpander != null)
            {

                bRet = true;
                containingExpander = parentExpander;

            }

            return bRet;
        }

        /// <summary>
        /// Returns true if the check element is in part of the visual tree of a data grid
        /// </summary>
        /// <param name="checkElement">Element/Control to check</param>
        /// <param name="containingCell">A reference receiving the containing data grid cell</param>
        /// <param name="containingRow">A reference receiving the containing data grid row</param>
        /// <param name="containingDataGrid">A reference receiving the containing data grid control</param>
        /// <returns>True if the item is in the visual tree of a data grid control</returns>
        public static bool IsInDataGrid(DependencyObject checkElement, ref DataGridCell containingCell, ref DataGridRow containingRow, ref DataGrid containingDataGrid)
        {
            bool bRet = false;

            DataGridCell parentCell = null;

            if (checkElement is DataGridRow)
            {
                containingCell = null;

                DataGridRow parentRow = checkElement as DataGridRow;

                if (parentRow != null)
                {
                    DataGrid parentGrid = DataGridHelper.GetDataGridFromRow(parentRow);

                    if (parentGrid != null)
                    {
                        bRet = true;
                        containingRow = parentRow;
                        containingDataGrid = parentGrid;
                    }
                }
            }
            else
            {
                if (checkElement is DataGridCell)
                    parentCell = checkElement as DataGridCell;
                else
                    parentCell = TryFindParent<DataGridCell>(checkElement);

                if (parentCell != null)
                {
                    DataGridRow parentRow = DataGridHelper.GetCellRow(parentCell);

                    if (parentRow != null)
                    {
                        DataGrid parentGrid = DataGridHelper.GetDataGridFromRow(parentRow);

                        if (parentGrid != null)
                        {
                            bRet = true;
                            containingCell = parentCell;
                            containingRow = parentRow;
                            containingDataGrid = parentGrid;
                        }
                    }
                }
            }

            return bRet;
        }
        #endregion

        #region Visual tree helpers (Parent, Child)
        public static Point GetElementPosition(FrameworkElement elem, Visual ref_visual)
        {
            GeneralTransform transform = elem.TransformToAncestor(ref_visual);

            Point rootPoint = transform.Transform(new Point(0, 0));

            return rootPoint;
        }

        public static Point GetRelationalElementPosition(FrameworkElement elem, FrameworkElement ref_element)
        {
            return elem.TranslatePoint(new Point(0, 0), ref_element);
        }

        public static Vector GetOffsetOverall(FrameworkElement framework_element)
        {
            Vector offset = new Vector(0, 0);
            while (framework_element != null)
            {
                offset += VisualTreeHelper.GetOffset(framework_element);
                framework_element = framework_element.Parent as FrameworkElement;
            }
            return offset;
        }
        /// <summary>   
        /// Finds a parent of a given item on the visual tree.   
        /// </summary>   
        /// <typeparam name="T">The type of the queried item.</typeparam>   
        /// <param name="child">A direct or indirect child of the   
        /// queried item.</param>   
        /// <returns>The first parent item that matches the submitted   
        /// type parameter. If not matching item can be found, a null   
        /// reference is being returned.</returns>   
        public static T TryFindParent<T>(DependencyObject child)
            where T : DependencyObject
        {
            //get parent item     
            DependencyObject parentObject = GetParentObject(child);
            //we've reached the end of the tree     
            if (parentObject == null) return null;
            //check if the parent matches the type we're looking for     
            T parent = parentObject as T;
            if (parent != null)
            {
                return parent;
            }
            else
            {
                //use recursion to proceed with next level       
                return TryFindParent<T>(parentObject);
            }
        }

        public static DependencyObject TryFindParent(DependencyObject child, Type parentType)
        {
            //get parent item     
            DependencyObject parentObject = GetParentObject(child);
            //we've reached the end of the tree     
            if (parentObject == null) return null;
            //check if the parent matches the type we're looking for     

            if (parentObject != null && parentObject.GetType() == parentType)
            {
                return parentObject;
            }
            else
            {
                //use recursion to proceed with next level       
                return TryFindParent(parentObject, parentType);
            }
        }

        /// <summary>
        /// TryFindParentGeneric sucht einen VisualParent und achtet dabei auf den generic type
        /// der mitgeliefert wird. Die Typenparameter des generic types werden NICHT beachtet !!!
        /// wenn der genaue Typ gesucht werden soll, dann die Methode TryFindParent  verwenden!!
        /// </summary>
        /// <param name="child"></param>
        /// <param name="parentType"></param>
        /// <returns></returns>
        public static DependencyObject TryFindParentGeneric(DependencyObject child, Type parentType)
        {
            if (!parentType.IsGenericType)
                return null;

            //get parent item     
            DependencyObject parentObject = GetParentObject(child);
            //we've reached the end of the tree     
            if (parentObject == null) return null;
            //check if the parent matches the type we're looking for     

            Type check_parent_type = GetParentGenericType(parentObject.GetType());
            bool is_same_generic_type = false;

            if (check_parent_type != null && check_parent_type.IsGenericType)
            {
                is_same_generic_type = check_parent_type.GetGenericTypeDefinition() == parentType.GetGenericTypeDefinition();
            }

            if (is_same_generic_type)
            {
                return parentObject;
            }
            else
            {
                //use recursion to proceed with next level       
                return TryFindParentGeneric(parentObject, parentType);
            }
        }

        private static Type GetParentGenericType(Type child_type)
        {
            if (child_type == null)
                return null;

            Type base_type = child_type.BaseType;

            if (base_type == null)
                return null;

            if (base_type.IsGenericType)
                return base_type;

            return GetParentGenericType(base_type);
        }

        /// <summary>   
        /// This method is an alternative to WPF's   
        /// <see cref="VisualTreeHelper.GetParent"/> method, which also   
        /// supports content elements. Do note, that for content element,   
        /// this method falls back to the logical tree of the element!   
        /// </summary>   
        /// <param name="child">The item to be processed.</param>   
        /// <returns>The submitted item's parent, if available. Otherwise   
        /// null.</returns>   
        public static DependencyObject GetParentObject(DependencyObject child)
        {
            if (child == null) return null;
            ContentElement contentElement = child as ContentElement;
            if (contentElement != null)
            {
                DependencyObject parent = ContentOperations.GetParent(contentElement);
                if (parent != null) return parent;

                FrameworkContentElement fce = contentElement as FrameworkContentElement;
                return fce != null ? fce.Parent : null;
            }

            FrameworkElement frameWorkElement = child as FrameworkElement;

            if (frameWorkElement != null)
            {
                DependencyObject parent = frameWorkElement.Parent;

                if (parent != null) return parent;
            }

            //if it's not a ContentElement, rely on VisualTreeHelper     
            return VisualTreeHelper.GetParent(child);
        }

        /// <summary>
        /// Get the visual child by name
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="parent"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public static T FindVisualChildByName<T>(DependencyObject parent, string name) where T : DependencyObject
        {
            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(parent); i++)
            {
                var child = VisualTreeHelper.GetChild(parent, i);
                string controlName = child.GetValue(Control.NameProperty) as string;
                if (controlName == name)
                {
                    return child as T;
                }
                else
                {
                    T result = FindVisualChildByName<T>(child, name);
                    if (result != null)
                        return result;
                }
            }
            return null;
        }

        /// <summary>
        /// Get the visual child by type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="parent"></param>
        /// <returns></returns>
        public static T FindVisualChildByType<T>(DependencyObject parent) where T : DependencyObject
        {
            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(parent); i++)
            {
                var child = VisualTreeHelper.GetChild(parent, i);

                if (child is T)
                {
                    return child as T;
                }
                else
                {
                    T result = FindVisualChildByType<T>(child);
                    if (result != null)
                        return result;
                }
            }
            return null;
        }

        /// <summary>
        /// Get the visual child by type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="parent"></param>
        /// <returns></returns>
        public static List<T> GetVisualChildrenByType<T>(DependencyObject parent) where T : DependencyObject
        {
            List<T> retList = new List<T>();

            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(parent); i++)
            {
                var child = VisualTreeHelper.GetChild(parent, i);

                if (child is T)
                {
                    retList.Add(child as T);
                }

                if (VisualTreeHelper.GetChildrenCount(child) > 0)
                    retList.AddRange(GetVisualChildrenByType<T>(child));
            }
            return retList;
        }

        /// <summary>
        /// Get all visual children
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="parent"></param>
        /// <returns></returns>
        public static List<DependencyObject> GetAllVisualChildren(DependencyObject parent)
        {
            List<DependencyObject> retList = new List<DependencyObject>();

            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(parent); i++)
            {
                var child = VisualTreeHelper.GetChild(parent, i);
                retList.Add(child);

                if (VisualTreeHelper.GetChildrenCount(child) > 0)
                    retList.AddRange(GetAllVisualChildren(child));
            }
            return retList;
        }

        /// <summary>
        /// Checks if a given element is a visual child of a given parent element
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="element_to_check"></param>
        /// <returns></returns>
        public static bool IsVisualChild(DependencyObject parent, DependencyObject element_to_check)
        {
            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(parent); i++)
            {
                var child = VisualTreeHelper.GetChild(parent, i);

                if (child != null)
                {
                    if (object.Equals(child, element_to_check))
                    {
                        return true;
                    }
                    else
                    {
                        bool bRet = IsVisualChild(child, element_to_check);
                        if (bRet)
                            return bRet;
                    }
                }
            }

            return false;
        }
        #endregion

        #region Focus helpers

        /// <summary>
        /// Try to set the input focus to an element which may be within the visual tree of a compley control like TabControl, Expander, DataGrid
        /// and tries to visually select the correct tab items, expander states etc to move to the control.
        /// </summary>
        /// <param name="control"></param>
        /// <returns></returns>
        public static bool SetFocusComplex(DependencyObject control)
        {
            if (CanSetFocus(control))
            {
                RecusiveTrySelectParents(control);

                bool bFocused = SetInputFocus(control, true);

                if (!bFocused && control is IInputElement)
                {
                    Keyboard.Focus(control as IInputElement);
                }
            }

            return false;
        }

        /// <summary>
        /// Trys to select tab items, tab controls, expanders and data grid cells in order to find the control in the form
        /// </summary>
        /// <param name="control"></param>
        private static void RecusiveTrySelectParents(DependencyObject control)
        {
            DependencyObject controlToCheck = control;

            DataGrid containingGrid = null;
            while (CheckParentDataGrid(controlToCheck, ref containingGrid))
            {
                controlToCheck = containingGrid;
                containingGrid = null;
            }

            controlToCheck = control;

            TabControl containingTabCtrl = null;
            while (CheckParentTabControl(controlToCheck, ref containingTabCtrl))
            {
                controlToCheck = containingTabCtrl;
                containingTabCtrl = null;
            }

            Expander containingExpander = null;
            while (CheckParentExpanderControl(controlToCheck, ref containingExpander))
            {
                controlToCheck = containingExpander;
                containingExpander = null;
            }
        }

        /// <summary>
        /// Gets the parent data grid if any
        /// </summary>
        /// <param name="control"></param>
        /// <param name="containingDataGrid"></param>
        /// <returns></returns>
        private static bool CheckParentDataGrid(DependencyObject control, ref DataGrid containingDataGrid)
        {
            // DataGrid checks
            DataGridCell containingCell = null;
            DataGridRow containingRow = null;
            DataGrid containingGrid = null;

            if (IsInDataGrid(control, ref containingCell, ref containingRow, ref containingGrid))
            {
                containingGrid.Focus();

                if (containingCell != null)
                {
                    //containingCell.Focus();
                    //if (containingGrid.SelectionUnit != DataGridSelectionUnit.FullRow)
                    //    containingCell.IsSelected = true;
                    //else
                    //{
                    //    containingRow.IsSelected = true;
                    //}

                    containingGrid.CurrentCell = new Microsoft.Windows.Controls.DataGridCellInfo(containingCell);
                    containingCell.Focus();

                    int nIdx = DataGridHelper.FindRowIndex(containingRow);
                    if (nIdx >= 0 && nIdx < containingGrid.Items.Count)
                    {
                        containingGrid.ScrollIntoView(containingGrid.Items[nIdx]);
                        containingGrid.SelectedItem = containingGrid.Items[nIdx];
                    }
                }
                else
                {
                    //if (containingGrid.SelectionUnit != DataGridSelectionUnit.FullRow)
                    //{
                    //    DataGridCell curCell = DataGridHelper.GetCell(containingRow, 0);
                    //    if (curCell != null)
                    //        curCell.IsSelected = true;
                    //}
                    //else
                    //    containingRow.IsSelected = true;
                    DataGridCell curCell = DataGridHelper.GetCell(containingRow, 0);

                    if (curCell != null)
                        containingGrid.CurrentCell = new Microsoft.Windows.Controls.DataGridCellInfo(curCell);

                    int nIdx = DataGridHelper.FindRowIndex(containingRow);
                    if (nIdx >= 0 && nIdx < containingGrid.Items.Count)
                    {
                        containingGrid.ScrollIntoView(containingGrid.Items[nIdx]);
                        containingGrid.SelectedItem = containingGrid.Items[nIdx];
                    }
                }

                containingDataGrid = containingGrid;
                return true;
            }

            return false;
        }
        /// <summary>
        /// Gets the parent tab control if any
        /// </summary>
        /// <param name="control"></param>
        /// <param name="containingTabControl"></param>
        /// <returns></returns>
        private static bool CheckParentTabControl(DependencyObject control, ref TabControl containingTabControl)
        {
            // TabControl checks
            TabControl containingTabctrl = null;
            TabItem containingTabItem = null;

            if (IsInTabControl(control, ref containingTabItem, ref containingTabctrl))
            {
                containingTabctrl.SelectedItem = containingTabItem;

                containingTabControl = containingTabctrl;
            }

            return false;
        }
        /// <summary>
        /// Gets the parent expander control if any
        /// </summary>
        /// <param name="control"></param>
        /// <param name="containingExpander"></param>
        /// <returns></returns>
        private static bool CheckParentExpanderControl(DependencyObject control, ref Expander containingExpander)
        {
            // Expander checks
            Expander containingExp = null;

            if (IsInExpander(control, ref containingExp))
            {
                containingExp.IsExpanded = true;

                containingExpander = containingExp;
            }

            return false;
        }

        /// <summary>
        /// Checks if a control may receive a focus
        /// </summary>
        /// <param name="control"></param>
        /// <returns></returns>
        public static bool CanSetFocus(DependencyObject control)
        {
            bool bRet = false;

            if (control != null)
            {
                UIElement element2 = control as UIElement;
                if (element2 != null)
                {
                    bRet = element2.Focusable;
                }
                else
                {
                    ContentElement element = control as ContentElement;
                    if (element != null)
                    {
                        bRet = element.Focusable;
                    }
                    else
                    {
                        UIElement3D elementd = control as UIElement3D;
                        if (elementd == null)
                        {
                            throw new ArgumentException("Invalid input element");
                        }
                        bRet = elementd.Focusable;
                    }
                }
            }

            return bRet;
        }

        /// <summary>
        /// sets the focus to a control
        /// </summary>
        /// <param name="control"></param>
        /// <param name="allowDeferredFocus"></param>
        /// <returns></returns>
        public static bool SetInputFocus(DependencyObject control, bool allowDeferredFocus)
        {
            bool bRet = false;

            if (control != null)
            {
                UIElement element2 = control as UIElement;
                if (element2 != null)
                {
                    if (!element2.IsVisible && allowDeferredFocus)
                    {
                        // deferred focus
                        DependencyPropertyChangedEventHandler deferredFocus = null;
                        deferredFocus = delegate
                        {
                            element2.Focus();
                            element2.IsVisibleChanged -= deferredFocus;
                        };
                        element2.IsVisibleChanged += deferredFocus;
                        bRet = true;
                    }
                    else
                    {
                        bRet = element2.Focus();
                    }
                }
                else
                {
                    ContentElement element = control as ContentElement;
                    if (element != null)
                    {
                        bRet = element.Focus();
                    }
                    else
                    {
                        UIElement3D elementd = control as UIElement3D;
                        if (elementd == null)
                        {
                            throw new ArgumentException("Invalid input element");
                        }

                        if (!elementd.IsVisible && allowDeferredFocus)
                        {
                            // deferred focus
                            DependencyPropertyChangedEventHandler deferredFocus = null;
                            deferredFocus = delegate
                            {
                                elementd.Focus();
                                elementd.IsVisibleChanged -= deferredFocus;
                            };
                            elementd.IsVisibleChanged += deferredFocus;
                            bRet = true;
                        }
                        else
                        {
                            bRet = elementd.Focus();
                        }
                    }
                }
            }

            return bRet;
        }

        /// <summary>
        /// Returns the UI Element which currently holds the input focus
        /// </summary>
        /// <param name="control">control which children will be checked</param>
        /// <returns>Returns the UI element with the input focus or null</returns>
        public static UIElement GetControlWithInputfocus(DependencyObject control)
        {
            return RecursiveGetControlWithFocus(control);
        }
        /// <summary>
        /// Recursively checks the control with the input focus
        /// </summary>
        /// <param name="current"></param>
        /// <returns></returns>
        private static UIElement RecursiveGetControlWithFocus(DependencyObject current)
        {
            foreach (object child in LogicalTreeHelper.GetChildren(current))
            {
                if (child is UIElement)
                {
                    if (((UIElement)child).IsFocused)
                        return child as UIElement;
                    else
                    {
                        UIElement childFocus = RecursiveGetControlWithFocus(((UIElement)child));
                        if (childFocus != null)
                            return childFocus;
                    }
                }
            }

            return null;
        }

        #endregion

        #region Graphics helpers
        public static Image CaptureImage(FrameworkElement visual)
        {
            int render_width_px = Convert.ToInt32(Math.Round(visual.ActualWidth));
            int render_height_px = Convert.ToInt32(Math.Round(visual.ActualHeight));
            return CaptureImage(visual, render_width_px, render_height_px);
        }
        public static Image CaptureImage(FrameworkElement visual, int max_height_width_px)
        {
            double scale_down = max_height_width_px;
            if (visual.ActualWidth > visual.ActualHeight)
                scale_down = max_height_width_px / visual.ActualWidth;
            else
                scale_down = max_height_width_px / visual.ActualHeight;
            int render_width_px = Convert.ToInt32(Math.Floor(visual.ActualWidth * scale_down));
            int render_height_px = Convert.ToInt32(Math.Floor(visual.ActualHeight * scale_down));
            return CaptureImage(visual, render_width_px, render_height_px);
        }

        public static RenderTargetBitmap GetImageSource(Visual visual, int render_width_px, int render_height_px)
        {
            try
            {
                if (render_height_px == 0 || render_width_px == 0)
                    return null;

                RenderTargetBitmap rtb = new RenderTargetBitmap(render_width_px, render_height_px, 96, 96, PixelFormats.Pbgra32);
                DrawingVisual dv = new DrawingVisual();
                using (DrawingContext dc = dv.RenderOpen())
                {
                    VisualBrush vb = new VisualBrush(visual);
                    vb.ViewboxUnits = BrushMappingMode.Absolute;
                    vb.Viewbox = new Rect(new Point(), new Size(render_width_px, render_height_px));
                    dc.DrawRectangle(vb, null, new Rect(0, 0, render_width_px, render_height_px));
                }
                rtb.Render(dv);
                return rtb;
            }
            catch
            {
                return null;
            }
        }

        public static Image CaptureImage(Visual visual, int render_width_px, int render_height_px)
        {
            try
            {
                RenderTargetBitmap rtb = GetImageSource(visual, render_width_px, render_height_px);
                Image img = new Image();
                img.Width = render_width_px;
                img.Height = render_height_px;
                img.Source = rtb;
                return img;
            }catch
            {
                return null;
            }
        }

        public static MemoryStream CaptureImage(Visual visual, int render_width_px, int render_height_px, PixelFormat pixel_format)
        {
            try
            {
                RenderTargetBitmap rtb = new RenderTargetBitmap(render_width_px,
                                                                render_height_px,
                                                                96,
                                                                96,
                                                                PixelFormats.Pbgra32);
                DrawingVisual dv = new DrawingVisual();
                using (DrawingContext ctx = dv.RenderOpen())
                {
                    VisualBrush vb = new VisualBrush(visual);
                    vb.ViewboxUnits = BrushMappingMode.Absolute;
                    vb.Viewbox = new Rect(new Point(), new Size(render_width_px, render_height_px));
                    ctx.DrawRectangle(vb, null, new Rect(new Point(), new Size(render_width_px, render_height_px)));
                }
                rtb.Render(dv);

                BmpBitmapEncoder encoder = new BmpBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create(rtb));

                MemoryStream stream = new MemoryStream();
                encoder.Save(stream);
                return stream;
            }
            catch
            {
                return null;
            }
        }

        public static Image CaptureImage(Visual visual, double render_width_px, double render_height_px)
        {
            if (render_height_px == 0 || render_width_px == 0)
                return null;

            return CaptureImage(visual, (int)Math.Round(render_width_px), (int)Math.Round(render_height_px));
        }

        public static bool CaptureAndSaveScreen(string path, Visual visual, int render_width_px, int render_height_px)
        {
            try
            {
                PngBitmapEncoder encoder = GetPngEncoder(visual, render_width_px, render_height_px);

                FileStream fs = File.OpenWrite(path);
                encoder.Save(fs);
                fs.Flush();
                fs.Close();
                fs.Dispose();
                fs = null;
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static PngBitmapEncoder GetPngEncoder(Visual visual, int render_width_px, int render_height_px)
        {
            try
            {
                RenderTargetBitmap rtb = new RenderTargetBitmap(render_width_px,
                                                                render_height_px,
                                                                96,
                                                                96,
                                                                PixelFormats.Pbgra32);
                DrawingVisual dv = new DrawingVisual();
                using (DrawingContext ctx = dv.RenderOpen())
                {
                    VisualBrush vb = new VisualBrush(visual);
                    // clear background with nearly completely transparent white color
                    ctx.DrawRectangle(new SolidColorBrush(new Color { A = 1, R = 255, G = 255, B = 255 }), null, new Rect(0, 0, render_width_px, render_height_px));
                    vb.ViewboxUnits = BrushMappingMode.Absolute;
                    vb.Viewbox = new Rect(new Point(), new Size(render_width_px, render_height_px));
                    ctx.DrawRectangle(vb, null, new Rect(new Point(), new Size(render_width_px, render_height_px)));
                }
                rtb.Render(dv);

                PngBitmapEncoder encoder = new PngBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create(rtb));
                return encoder;
            }
            catch
            {
                return null;
            }
        }
        #endregion

    }
}
