﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Logicx.WpfUtility.CustomControls.TouchButton;

namespace Logicx.WpfUtility.CustomControls.OverlayManager
{
    public enum OverlayButtonAlignment
    {
        Left,
        Center, 
        Right
    }

    public class OverlayDialogButton : TouchButton.TouchButton
    {
        #region Construction and initialization
        /// <summary>
        /// Static constructor
        /// </summary>
        static OverlayDialogButton()
        {
            FrameworkPropertyMetadata metadataButtonDisplayPosition = new FrameworkPropertyMetadata(OverlayButtonAlignment.Right);
            ButtonDisplayPositionProperty = DependencyProperty.Register("ButtonDisplayPosition",
                                                            typeof(OverlayButtonAlignment), typeof(OverlayDialogButton), metadataButtonDisplayPosition);

            FrameworkPropertyMetadata metadataButtonKeyAssignmentPosition = new FrameworkPropertyMetadata(Key.None);
            ButtonKeyAssignmentProperty = DependencyProperty.Register("ButtonKeyAssignment",
                                                            typeof(Key), typeof(OverlayDialogButton), metadataButtonKeyAssignmentPosition);
        }
        /// <summary>
        /// Default constructor for design support
        /// </summary>
        public OverlayDialogButton()
        {
            Style btnStyle = null;
            if(Application.Current != null)
                btnStyle = (Style)Application.Current.FindResource("overlay_button_style");
            else
                btnStyle = (Style)FindResource("overlay_button_style");

            if (btnStyle != null)
                this.Style = btnStyle;

            Margin = new Thickness(5,5,5,5);
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="text">Button text</param>
        /// <param name="alignment">Alignment</param>
        public OverlayDialogButton(string text, OverlayButtonAlignment alignment)
        {
            Style btnStyle = (Style)FindResource("overlay_button_style");

            if (btnStyle != null)
                this.Style = btnStyle;

            Margin = new Thickness(5, 5, 5, 5);

            Content = text;
            ButtonAlignment = alignment;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="text">Button text</param>
        /// <param name="alignment">Alignment</param>
        /// <param name="keyAssignment">Shortcut key assignment in overlay dialogs</param>
        public OverlayDialogButton(string text, OverlayButtonAlignment alignment, Key keyAssignment)
        {
            Style btnStyle = (Style)FindResource("overlay_button_style");

            if (btnStyle != null)
                this.Style = btnStyle;

            Margin = new Thickness(5, 5, 5, 5);

            Content = text;
            ButtonAlignment = alignment;
            ButtonKeyAssignment = keyAssignment;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="content">Beliebiger Button content</param>
        /// <param name="alignment"></param>
        public OverlayDialogButton(object content, OverlayButtonAlignment alignment )
        {
            Style btnStyle = (Style)FindResource("overlay_button_style");

            if (btnStyle != null)
                this.Style = btnStyle;

            Margin = new Thickness(5, 5, 5, 5);

            Content = content;
            ButtonAlignment = alignment;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Dependency Eigenschaft welche die Ausrichtung des Buttons im OverlayDialog angibt
        /// </summary>
        /// <remarks>Wenn dieser Text null oder leer ist, dann wird der Button automatisch nicht angezeigt</remarks>
        public OverlayButtonAlignment ButtonAlignment
        {
            get
            {
                return (OverlayButtonAlignment)GetValue(ButtonDisplayPositionProperty);
            }
            set
            {
                SetValue(ButtonDisplayPositionProperty, value);
            }
        }

        /// <summary>
        /// Dependency Eigenschaft welche dem Button eine Tastaturkombination zuordnet
        /// </summary>
        /// <remarks>Wenn dieser Text null oder leer ist, dann wird der Button automatisch nicht angezeigt</remarks>
        public Key ButtonKeyAssignment
        {
            get
            {
                return (Key)GetValue(ButtonKeyAssignmentProperty);
            }
            set
            {
                SetValue(ButtonKeyAssignmentProperty, value);
            }
        }

        public bool UsesCustomStyleing { get; set; }



        public bool ShowActionIndicator
        {
            get { return (bool)GetValue(ShowActionIndicatorProperty); }
            set { SetValue(ShowActionIndicatorProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ShowActionIndicator.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ShowActionIndicatorProperty =
            DependencyProperty.Register("ShowActionIndicator", typeof(bool), typeof(OverlayDialogButton), new UIPropertyMetadata(false));

        
        #endregion

        #region Attribs

        #region Dependency properties
        public static readonly DependencyProperty ButtonDisplayPositionProperty;
        public static readonly DependencyProperty ButtonKeyAssignmentProperty;
        #endregion

        #endregion
    }
}
