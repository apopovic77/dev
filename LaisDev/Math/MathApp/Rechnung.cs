﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MathApp
{

    public class Rechnung
    {

    }

    public class QuadratischeGleichung : Rechnung
    {
        public QuadratischeGleichung(double p, double q)
        {
            P = p;
            Q = q;
        }

        public QuadratischeGleichung(string p_s, string q_s)
        {

            double p = Convert.ToDouble(p_s);
            double q = Convert.ToDouble(q_s);

            P = p;
            Q = q;
        }

        public double Diskriminante
        {
            get
            {
                double diskriminante = Math.Pow(P, 2) - 4 * Q;
                return diskriminante;
            }
        }

        public double X1
        {
            get
            {
                double x1 = P / 2 + Math.Sqrt(Math.Pow(P / 2, 2) - Q);
                return x1;
            }
        }
        public double X2
        {
            get
            {
                double x2 = P / 2 - Math.Sqrt(Math.Pow(P / 2, 2) - Q);
                return x2;
            }
        }

        public double P { get; set; }
        public double Q { get; set; }

    }

}
