﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;
using Logicx.WpfUtility.CustomControls.OverlayManager;

namespace Logicx.WpfUtility.WindowManagement
{
    public class TaskbarItemBase<B, S, T, U, O,SU> : UserControl
        where S : SubMenuPanelBase<B, S, T, U, O, SU>, new()
        where B : BaseFrameBase<B, S, T, U, O, SU>
        where T : WindowTaskBarBase<B, S, T, U, O, SU>, new()
        where U : WindowTitleBarBase<B, S, T, U, O, SU>, new()
        where O : OverlayManagerBase<B, S, T, U, O, SU>, new()
        where SU : SubMenuPanelMenuItem
    {
        public TaskbarItemBase(FrameworkElement framework_element, string title)
        {
            InitializeComponent();

            this.Unloaded += new RoutedEventHandler(TaskbarItemBase_Unloaded);

            ItemTextBlock.Text = title;
            ItemBorder.Child = framework_element;
        }

        void TaskbarItemBase_Unloaded(object sender, RoutedEventArgs e)
        {
            ClearFoundElements();
        }

        /// <summary>
        /// Hier wird der XAML Code geladen. Das wurde so gemacht, das man in einer anderes Lib von dieser Klasse ableiten kann. Bei "partial" Klassen werden die Resourcen über mehrere Libs nicht gefunden.
        /// 
        /// <remarks> 
        /// * Der zu ladende Xaml String darf keine Events haben. 
        /// * Bei XAML Namespaces muss assembly angegeben werden.
        /// * class Attribut darf nicht vorkommen.
        /// * Erzeugte Elemente muss man von außerhalb mit FindName suchen
        /// 
        /// </remarks>
        /// </summary>
        private void InitializeComponent()
        {
            this.Content = XamlReader.Parse(@"<UserControl
                xmlns=""http://schemas.microsoft.com/winfx/2006/xaml/presentation""
                xmlns:x=""http://schemas.microsoft.com/winfx/2006/xaml""
                HorizontalAlignment=""Center""
                VerticalAlignment=""Center""
                MinWidth=""60""
                Height=""70""
                Background=""Transparent""
                >
                <StackPanel HorizontalAlignment=""Center"" VerticalAlignment=""Center"">
                <Border Name=""item_border"" Style=""{Binding RelativeSource={RelativeSource AncestorType={x:Type UserControl}, AncestorLevel=2},Path=ContainerBorderStyle}"">
            
                </Border>
                <TextBlock Name=""item_text_tb"" TextAlignment=""Center"" HorizontalAlignment=""Center""  Style=""{Binding RelativeSource={RelativeSource AncestorType={x:Type UserControl}, AncestorLevel=2},Path=TextStyle}"">
                </TextBlock>
                </StackPanel>
                </UserControl>");
        }

        #region Finding Xaml Elements
        private void ClearFoundElements()
        {
            _item_text_block = null;
            _item_border = null;
        }

        public TextBlock ItemTextBlock
        {
            get
            {
                return _item_text_block ?? (_item_text_block = (TextBlock)((UserControl)Content).FindName("item_text_tb"));
            }
        }

        public Border ItemBorder
        {
            get
            {
                return _item_border ?? (_item_border = (Border)((UserControl)Content).FindName("item_border"));
            }
        }
        #endregion

        public override string ToString()
        {
            return "Taskbar Item " + Title;
        }

        public Style ContainerBorderStyle
        {
            get { return (Style)GetValue(ContainerBorderStyleProperty); }
            set { SetValue(ContainerBorderStyleProperty, value); }
        }
        public static readonly DependencyProperty ContainerBorderStyleProperty = DependencyProperty.Register("ContainerBorderStyle", typeof(Style), typeof(TaskbarItemBase<B, S, T, U, O, SU>), new UIPropertyMetadata(null));

        public Style TextStyle
        {
            get { return (Style)GetValue(TextStyleProperty); }
            set { SetValue(TextStyleProperty, value); }
        }
        public static readonly DependencyProperty TextStyleProperty = DependencyProperty.Register("TextStyle", typeof(Style), typeof(TaskbarItemBase<B, S, T, U, O, SU>), new UIPropertyMetadata(null));

        public int? InfoCounter
        {
            get { return (int?)GetValue(InfoCounterProperty); }
            set { SetValue(InfoCounterProperty, value); }
        }
        public static readonly DependencyProperty InfoCounterProperty = DependencyProperty.Register("InfoCounter", typeof(int?), typeof(TaskbarItemBase<B, S, T, U, O, SU>), new UIPropertyMetadata(null));

        public bool InfoIndicator
        {
            get { return (bool)GetValue(InfoIndicatorProperty); }
            set { SetValue(InfoIndicatorProperty, value); }
        }
        public static readonly DependencyProperty InfoIndicatorProperty = DependencyProperty.Register("InfoIndicator", typeof(bool), typeof(TaskbarItemBase<B, S, T, U, O, SU>), new UIPropertyMetadata(false));


        public FrameworkElement FrameworkElement
        {
            get
            {
                return (FrameworkElement)ItemBorder.Child;
            }
        }


        public string Title
        {
            get
            {
                return ItemTextBlock.Text;
            }
        }

        public FrameworkElement Container
        {
            get;
            set;
        }

        public WindowTaskBarBase<B, S, T, U, O, SU>.ItemType ItemType
        {
            get;
            set;
        }

        #region Attributes
        #region Everything for finding Xaml Elements
        private TextBlock _item_text_block;
        private Border _item_border;
        #endregion
        #endregion
    }
}
