﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Logicx.DiagramLib.Controls;
using Logicx.DiagramLib.Resources.Converter;

namespace Logicx.DiagramLib.DiagramObjects.Objects
{
    public abstract class VerticalBarPointObject<Tx, Ty> : PointObject<Tx, Ty>
    {
        #region Construction and Initialization

        /// <summary>
        /// Constructor of the class
        /// </summary>
        /// <param name="x_value"></param>
        /// <param name="y_value"></param>
        public VerticalBarPointObject(Tx x_value, Ty y_value)
            : base(x_value, y_value)
        {
            MinHeight = 5;
        }
        #endregion

        #region Operations
        protected override void OnUpdateVisualBindings()
        {
            base.OnUpdateVisualBindings();
            if (ObjectVisual == null)
                return;

            //// always begins at 0
            BindingOperations.ClearBinding(ObjectVisual, Canvas.TopProperty);
            Canvas.SetTop(ObjectVisual, 0);

            MultiBinding top_position_binding = new MultiBinding();

            Binding y_value_binding = new Binding("YValue");
            y_value_binding.Source = this;
            top_position_binding.Bindings.Add(y_value_binding);

            Binding y_scale_binding = new Binding("YAxisControl.ScaleUnitSize");
            y_scale_binding.RelativeSource = DiagramBindingSource;
            top_position_binding.Bindings.Add(y_scale_binding);

            Binding y_axis_binding = new Binding("YAxisControl");
            y_axis_binding.RelativeSource = DiagramBindingSource;
            top_position_binding.Bindings.Add(y_axis_binding);

            Binding y_height_binding = new Binding("ActualHeight");
            RelativeSource source = new RelativeSource(RelativeSourceMode.FindAncestor, typeof(Canvas), 1);
            y_height_binding.RelativeSource = source;
            top_position_binding.Bindings.Add(y_height_binding);

            top_position_binding.ConverterParameter = MinHeight;
            top_position_binding.Converter = new MinSizeValueToPositionConverter();
            ObjectVisual.SetBinding(Canvas.TopProperty, top_position_binding);


            //// always uses full height

            MultiBinding top_binding = new MultiBinding();

            Binding y_top_binding = new Binding();
            y_top_binding.Path = new PropertyPath(Canvas.TopProperty);
            y_top_binding.Source = ObjectVisual;
            top_binding.Bindings.Add(y_top_binding);

            y_height_binding = new Binding("ActualHeight");
            source = new RelativeSource(RelativeSourceMode.FindAncestor, typeof(Canvas), 1);
            y_height_binding.RelativeSource = source;
            top_binding.Bindings.Add(y_height_binding);

            top_binding.ConverterParameter = MinHeight;
            top_binding.Converter = new YValueToPositionConverter();
            ObjectVisual.SetBinding(FrameworkElement.HeightProperty, top_binding);
        }

        #endregion

        #region Properties
        public override bool CanResizeY
        {
            get { return false; }
        }

        public double MinHeight { get; set; }
        #endregion

  
    }
}
