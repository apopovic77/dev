﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;

namespace PoCWebApp.Core.Configuration
{
    public interface IWhcServiceBuilder
    {
        IServiceCollection Services { get; }
    }
}
