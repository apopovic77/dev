﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Logicx.WpfUtility.PieMenu
{
    public class PieMenuSlice : PieMenuSliceBase<PieMenuEntry, ButtonPieMenuEntry, TogglePieMenuEntry>
    {
        public PieMenuSlice() : base()
        {}

        public PieMenuSlice(bool close_on_outer_ellipse_click) : base(close_on_outer_ellipse_click)
        { }
    }
}
