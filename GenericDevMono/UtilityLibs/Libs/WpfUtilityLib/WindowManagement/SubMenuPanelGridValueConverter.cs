﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace Logicx.WpfUtility.WindowManagement
{
    public class SubMenuPanelGridValueConverter : IMultiValueConverter
    {
        #region IMultiValueConverter Members
        public object Convert(object[] values, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            if (values == null || values.Length != 2)
                return " ";

            if (values[0] is SubMenuPanelMenuItem && parameter is SubMenuPanelMenuItem.MenuItemAttribute)
            {
                SubMenuPanelMenuItem.MenuItemAttribute cur_attribute = parameter as SubMenuPanelMenuItem.MenuItemAttribute;
                return ((SubMenuPanelMenuItem)values[0]).GetValue(cur_attribute);
            }

            return " ";
        }

        public object[] ConvertBack(object value, Type[] targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            return null;
        }
        #endregion
    }

    public class SubMenuPanelGridActionEnabledConverter : IMultiValueConverter
    {
        #region IMultiValueConverter Members
        public object Convert(object[] values, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            if (values == null || values.Length != 2)
                return " ";

            if (values[0] is SubMenuPanelMenuItem && parameter is SubMenuPanelMenuItem.MenuItemAction)
            {
                // das action item er aktuellen zeile auslesen
                SubMenuPanelMenuItem.MenuItemAction cur_action = ((SubMenuPanelMenuItem)values[0]).GetActionByName(((SubMenuPanelMenuItem.MenuItemAction)parameter).Name);
                return cur_action.IsEnabled;
            }
            else if (values[0] is SubMenuPanelMenuItem && parameter is SubMenuPanelMenuItem.MenuItemAttribute)
            {
                // das attribut der aktuellen zeile auslesen
                SubMenuPanelMenuItem.MenuItemAttribute cur_attribute = ((SubMenuPanelMenuItem)values[0]).GetAttributeByName(((SubMenuPanelMenuItem.MenuItemAttribute)parameter).Name);
                return cur_attribute.IsEnabled;
            }

            return true;
        }

        public object[] ConvertBack(object value, Type[] targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            return null;
        }
        #endregion
    }

    public class SubMenuPanelGridActionOpacityConverter : IMultiValueConverter
    {
        #region IMultiValueConverter Members
        public object Convert(object[] values, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            if (values == null || values.Length != 2)
                return " ";

            if (values[0] is SubMenuPanelMenuItem && parameter is SubMenuPanelMenuItem.MenuItemAction)
            {
                // das action item er aktuellen zeile auslesen
                SubMenuPanelMenuItem.MenuItemAction cur_action = ((SubMenuPanelMenuItem)values[0]).GetActionByName(((SubMenuPanelMenuItem.MenuItemAction)parameter).Name);
                return cur_action.IsEnabled ? (double)1 : (double)0.5;
            }
            else if (values[0] is SubMenuPanelMenuItem && parameter is SubMenuPanelMenuItem.MenuItemAttribute)
            {
                // das attribut der aktuellen zeile auslesen
                SubMenuPanelMenuItem.MenuItemAttribute cur_attribute = ((SubMenuPanelMenuItem)values[0]).GetAttributeByName(((SubMenuPanelMenuItem.MenuItemAttribute)parameter).Name);
                return cur_attribute.IsEnabled ? (double)1 : (double)0.5;
            }

            return (double)1;
        }

        public object[] ConvertBack(object value, Type[] targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            return null;
        }
        #endregion
    }

    public class SubMenuPanelGridActionVisibilityConverter : IMultiValueConverter
    {
        #region IMultiValueConverter Members
        public object Convert(object[] values, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            if (values == null || values.Length != 2)
                return " ";

            if (values[0] is SubMenuPanelMenuItem && parameter is SubMenuPanelMenuItem.MenuItemAction)
            {
                // das action item er aktuellen zeile auslesen
                SubMenuPanelMenuItem.MenuItemAction cur_action = ((SubMenuPanelMenuItem)values[0]).GetActionByName(((SubMenuPanelMenuItem.MenuItemAction)parameter).Name);
                return cur_action.IsVisible ? Visibility.Visible : Visibility.Collapsed;
            }
            else if (values[0] is SubMenuPanelMenuItem && parameter is SubMenuPanelMenuItem.MenuItemAttribute)
            {
                // das attribut der aktuellen zeile auslesen
                SubMenuPanelMenuItem.MenuItemAttribute cur_attribute = ((SubMenuPanelMenuItem)values[0]).GetAttributeByName(((SubMenuPanelMenuItem.MenuItemAttribute)parameter).Name);
                return cur_attribute.IsVisible ? Visibility.Visible : Visibility.Collapsed;
            }

            return (double)1;
        }

        public object[] ConvertBack(object value, Type[] targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            return null;
        }
        #endregion
    }

    public class SubMenuPanelGridActionIconConverter : IMultiValueConverter
    {
        #region IMultiValueConverter Members
        public object Convert(object[] values, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            if (values == null || values.Length != 2)
                return " ";

            if (values[0] is SubMenuPanelMenuItem && parameter is SubMenuPanelMenuItem.MenuItemAction)
            {
                // das action item er aktuellen zeile auslesen
                SubMenuPanelMenuItem.MenuItemAction cur_action = ((SubMenuPanelMenuItem)values[0]).GetActionByName(((SubMenuPanelMenuItem.MenuItemAction)parameter).Name);
                return cur_action.IconGrid;
            }

            //if (value is SubMenuPanelMenuItem)
            //{
            //    //Viewbox vb = new Viewbox();
            //    //vb.Width = 18;
            //    //vb.Height = 18;
            //    //vb.Stretch = System.Windows.Media.Stretch.Fill;
            //    //vb.Child = ((SubMenuPanelMenuItem) value).GetActionIcon(parameter as string);
            //    return ((SubMenuPanelMenuItem)value).GetActionIcon(parameter as string);
            //}

            return parameter;
        }

        public object[] ConvertBack(object value, Type[] targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            return null;
        }
        #endregion
    }

}
