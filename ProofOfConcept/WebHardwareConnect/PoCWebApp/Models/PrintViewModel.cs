﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Web.Mvc;

namespace PoCWebApp.Models
{
    public class PrintViewModel
    {
        [DisplayName("Select a printer from your computer")]
        public int SelectedPrinter { get; set; }
        public List<SelectListItem> AvailablePrinters { get; set; }

        public bool ClientReachable { get; set; }
    }
}