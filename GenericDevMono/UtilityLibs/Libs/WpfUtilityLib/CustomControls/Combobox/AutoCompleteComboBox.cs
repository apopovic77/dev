﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using Logicx.Utilities;
using System.Linq.Dynamic;

namespace Logicx.WpfUtility.CustomControls.Combobox
{
    public enum StringFilterBehaviours
    {
        StartsWith,
        EndsWith,
        Contains
    }

    public class BackgroundDataFetchingEventArgs : EventArgs
    {
        public BackgroundDataFetchingEventArgs(string current_textbox_content, IEnumerable current_items_source)
        {
            _current_textbox_content = current_textbox_content;
            _current_items_source = current_items_source;
        }

        public string CurrentTextBoxContent
        {
            get { return _current_textbox_content; }
        }

        public IEnumerable CurrentItemsSource
        {
            get { return _current_items_source; }
        }

        public List<object> DataFetchingOutput
        {
            get { return _data_fetching_output;}
            set { _data_fetching_output = value; }
        }

        public Type DataFetchingOutputType
        {
            get { return _data_fetching_output_type; }
            set { _data_fetching_output_type = value; }
        }

        public string StringTag
        {
            get { return _string_tag; }
            set { _string_tag = value; }
        }

        public object Tag
        {
            get { return _tag; }
            set { _tag = value; }
        }

        private string _current_textbox_content = string.Empty;
        private IEnumerable _current_items_source = null;
        private List<object> _data_fetching_output = null;
        private Type _data_fetching_output_type = typeof (object);
        private string _string_tag = string.Empty;
        private object _tag = null;
    }

    /// <summary>
    /// Interaktionslogik für AutoCompleteComboBox.xaml
    /// </summary>
    public class AutoCompleteComboBox : ComboBox
    {
        /// <summary>
        /// Note: Die derzeitige Implementierung unterstüzt kein TextSearch Attribut. Die zu filternden Objekte müssen daher eine ToString Methode haben und mit diesem Wert wird dann auch gefiltert
        /// </summary>
        public AutoCompleteComboBox()
        {
            //DependencyPropertyDescriptor descr = DependencyPropertyDescriptor.FromProperty(BackgroundItemsSourceProperty, typeof (AutoCompleteComboBox));
            //descr.AddValueChanged(this, background_items_source_changed);

            //System.ComponentModel.DependencyPropertyDescriptor descr1 = System.ComponentModel.DependencyPropertyDescriptor.FromProperty(ItemsSourceProperty, typeof(AutoCompleteComboBox));
            //descr1.AddValueChanged(this, items_source_changed);            

            Binding b = new Binding();
            b.Source = this;
            b.Path = new PropertyPath("WithCustomValue");
            b.Mode = BindingMode.TwoWay;
            this.SetBinding(ComboBox.IsEditableProperty, b);

            this.Loaded += new RoutedEventHandler(AutoCompleteComboBox_Loaded);
            this.Unloaded += new RoutedEventHandler(AutoCompleteComboBox_Unloaded);
            this.SizeChanged += new SizeChangedEventHandler(AutoCompleteComboBox_SizeChanged);
            this.SelectionChanged += new SelectionChangedEventHandler(AutoCompleteComboBox_SelectionChanged);
            this.IsTextSearchEnabled = false;
        }


        #region operations
        private void FindButton(DependencyObject vis_obj)
        {
            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(vis_obj); i++)
            {
                DependencyObject child = VisualTreeHelper.GetChild(vis_obj, i);
                if (child is ToggleButton)
                    _button = (ToggleButton)child;
                else
                    FindButton(child);
            }
        }
        
        private void SetMultilineTextEdit()
        {
            _textBox.TextWrapping = TextWrapping.Wrap;
            _textBox.AcceptsReturn = true;
            _textBox.AllowDrop = true;
        }

        
        private void CheckToggleButton()
        {
            if (_button != null && IsEnabled)
            {
                if (IsFetchingData)
                {
                    _button.Visibility = Visibility.Visible;
                }
                else
                {
                    if ((Items.Count < 1 && (IsEditable || WithCustomValue))) // || (Items.Count == 1 && OnlyListEntries && (IsEditable || WithCustomValue)) )
                        _button.Visibility = Visibility.Collapsed;
                    else
                        _button.Visibility = Visibility.Visible;
                }
            }
        }

        private void SetIsFetchingData(bool value)
        {
            SetValue(IsFetchingDataPropertyKey, value);
        }

        public void UpdateBackgroundItemsSource()
        {
            if (IsBackgroundLoadingEnabled)
            {
                if (!IsFetchingData)
                {
                    AbortBackgroundWorker();
                    _background_items_source_worker = new QueuedBackgroundWorker();
                    _background_items_source_worker.Name = "AutoCompleteBox: BackgroundSource Thread";
                    _background_items_source_worker.DoWork += new QueuedBackgroundWorker.DoWorkEventHandler(background_items_source_DoWork);
                    _background_items_source_worker.RunWorkerCompleted += new QueuedBackgroundWorker.RunWorkerCompletedEventHandler(background_items_source_RunWorkerCompleted);
                    _background_items_source_worker.RunWorkerAsync();
                }
            }
            else
                throw new InvalidOperationException("Background data loading is not enabled!");
        }

        private void UpdateBackgroundItemsFilter()
        {
            if(IsBackgroundLoadingEnabled)
            {
                if (IsFetchingData)
                {
                    // update filter later after initial data loading has been finished
                    _must_update_filter = true; 
                }
                else
                {
                    AbortBackgroundFilterWorker();

                    _background_filter_worker = new QueuedBackgroundWorker();
                    _background_filter_worker.Name = "AutoCompleteBox: BackgroundFilter Thread";
                    _background_filter_worker.DoWork += new QueuedBackgroundWorker.DoWorkEventHandler(background_filter_DoWork);
                    _background_filter_worker.RunWorkerCompleted += new QueuedBackgroundWorker.RunWorkerCompletedEventHandler(background_filter_RunWorkerCompleted);
                    _background_filter_worker.RunWorkerAsync();
                }

            }
        }

        public void AbortBackgroundWorker()
        {
            if (_background_items_source_worker != null && _background_items_source_worker.IsBusy)
            {
                _background_items_source_worker.Abort();
                //_background_items_source_worker.Join();
                _background_items_source_worker = null;
            }

            AbortBackgroundFilterWorker();
        }

        private void AbortBackgroundFilterWorker()
        {
            if (_background_filter_worker != null && _background_filter_worker.IsBusy)
            {
                _background_filter_worker.Abort();
                //_background_filter_worker.Join();
                _background_filter_worker = null;
            }
        }

        private void StartTextFilterDelayTimer()
        {
            if (_text_filter_delay_timer == null)
            {
                _text_filter_delay_timer = new System.Timers.Timer(FILTER_DELAY);
                _text_filter_delay_timer.Elapsed += new System.Timers.ElapsedEventHandler(filter_timer_elapsed);
            }
            else
            {
                _text_filter_delay_timer.Stop();
            }

            _text_filter_delay_timer.Start();
        }

        /// <summary>
        /// Gets the methodInfo of the static, generic Cast(Func&lt;T, R&gt;) method of the Enumerable class
        /// </summary>
        /// <returns></returns>
        private MethodInfo GetEnumerableCastMethodInfo()
        {
            if (_cast_method != null)
                return _cast_method;

            MethodInfo[] methods = typeof(Enumerable).GetMethods();

            foreach (MethodInfo mI in methods)
            {
                if (mI.Name == "Cast")
                {
                    _cast_method = mI;
                    return mI;
                }
            }

            return null;
        }
        #endregion

        #region event handler

        #region background data source loading

        void filter_timer_elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            if (sender == _text_filter_delay_timer)
            {
                _text_filter_delay_timer.Stop();
                _text_filter_delay_timer.Dispose();
                _text_filter_delay_timer = null;
            }

            Dispatcher.Invoke(DispatcherPriority.Normal, (NoParas)delegate
            {
                UpdateBackgroundItemsFilter();
            });
        }

        private void background_filter_DoWork(object sender, DoWorkEventArgs e)
        {
            string sText = "";
            _ignore_items_source_changed = true;
            bool bOnlyEntries = true;
            bool bOldIgnoreSelChange = _ignore_selection_changed;

            Dispatcher.Invoke(DispatcherPriority.Normal, (NoParas)delegate
              {
                  bOnlyEntries = OnlyListEntries;
                  SetIsFetchingData(true);
                  sText = _textBox.Text;
            });


            if (!_selectionchanged && !_moved_key_down)
            {
                if (String.IsNullOrEmpty(sText))
                {
                    Dispatcher.Invoke(DispatcherPriority.Normal, (NoParas)delegate
                    {
                        SelectedItem = null;
                        if (BackgroundItemsSource == null)
                            ItemsSource = null;
                        else
                            ItemsSource = BackgroundItemsSource.AsQueryable();

                        _selectionchanged = false;
                        CheckToggleButton();
                        ClosePopup();
                        _ignore_items_source_changed = false;
                    });
                    return;
                }

                string temp_text = sText;

                //Items.Filter = a => a.ToString().ToLower().StartsWith(_textBox.Text.ToLower());

                List<object> data = null;
                StringFilterBehaviours filterBehaviour = StringFilterBehaviours.StartsWith;

                Dispatcher.Invoke(DispatcherPriority.Normal, (NoParas)delegate
                {
                    data = BackgroundItemsSource;
                    filterBehaviour = StringFilterBehaviour;
                });

                _ignore_text_changed = true;
                _ignore_selection_changed = true;
                if (bOnlyEntries)
                {
                    // wenn nur vorhandene werte erlaubt sind, dann den stringfilter behaviour immer als startswith verwenden
                    // zwecks autocomplete
                    data = data.Where(x => (x == null && string.IsNullOrEmpty(sText)) || (Convert.ToString(x).ToLower().StartsWith(sText.ToLower()))).ToList();
                }
                else
                {
                    switch (filterBehaviour)
                    {
                        case StringFilterBehaviours.StartsWith:
                            data = data.Where(x => (x == null && string.IsNullOrEmpty(sText)) || (Convert.ToString(x).ToLower().StartsWith(sText.ToLower()))).ToList();
                            break;
                        case StringFilterBehaviours.EndsWith:
                            data = data.Where(x => (x == null && string.IsNullOrEmpty(sText)) || (Convert.ToString(x).ToLower().EndsWith(sText.ToLower()))).ToList();
                            break;
                        case StringFilterBehaviours.Contains:
                            data = data.Where(x => (x == null && string.IsNullOrEmpty(sText)) || (Convert.ToString(x).ToLower().Contains(sText.ToLower()))).ToList();
                            break;
                    }
                }
                _ignore_selection_changed = false;
                _ignore_text_changed = false;
                

                Dispatcher.Invoke(DispatcherPriority.Normal, (NoParas)delegate
                {
                    _ignore_selection_changed = true;
                    if (_background_itemsource_type != typeof(object))
                    {
                        // item source list type casting
                        MethodInfo miCast = GetEnumerableCastMethodInfo();
                        MethodInfo miCastType = miCast.MakeGenericMethod(new Type[] { _background_itemsource_type });
                        ItemsSource = (IEnumerable)miCastType.Invoke(null, new object[] { data });
                    }
                    else
                    {
                        ItemsSource = data;
                    }
                    _ignore_selection_changed = false;

                    CheckToggleButton();

                    if (Items.Count <= 1 || Items.IsEmpty)
                    {
                        if (Items.IsEmpty || bOnlyEntries)
                            ClosePopup();

                        if (Items.Count == 1 && bOnlyEntries)
                        {
                            _last_text_match = sText;

                            temp_text = Convert.ToString(Items[0]);

                            if (temp_text != null)
                            {
                                if (_textBox.Text != temp_text)
                                {
                                    _ignore_text_changed = true;
                                    _ignore_selection_changed = true;
                                    _textBox.Text = temp_text;
                                    _ignore_selection_changed = false;
                                    _textBox.CaretIndex = temp_text.Length;
                                    _ignore_text_changed = false;
                                }
                            }
                            _ignore_selection_changed = true;
                            SelectedIndex = 0; // fire selection changed
                            _selectionchanged = false;
                            _ignore_selection_changed = false;
                        }
                        else
                        {
                            if (Items.Count == 1)
                            {
                                // do nothing
                                _last_text_match = sText;
                            }
                            else
                            {
                                if (bOnlyEntries && !string.IsNullOrEmpty(_last_text_match))
                                {
                                    if (temp_text.Length <= 1)
                                        _last_text_match = "";

                                    _ignore_selection_changed = true;
                                    _ignore_text_changed = true;
                                    _textBox.Text = _last_text_match;
                                    _ignore_text_changed = false;
                                    _ignore_selection_changed = false;
                                    background_filter_DoWork(sender, e);
                                }
                                else
                                {
                                    _ignore_selection_changed = true;
                                    SelectedIndex = -1; // fire selection changed
                                    _selectionchanged = false;
                                    _ignore_selection_changed = false;

                                    if (_textBox.Text != temp_text && !bOnlyEntries)
                                    {
                                        _ignore_text_changed = true;
                                        _ignore_selection_changed = true;
                                        _textBox.Text = temp_text;
                                        _ignore_selection_changed = false;
                                        _textBox.CaretIndex = temp_text.Length;
                                        _ignore_text_changed = false;
                                    }

                                    if (bOnlyEntries)
                                    {
                                        //_textBox.Text = string.Empty;
                                        _ignore_selection_changed = true;
                                        _textBox.Text = _last_text_match;
                                        _ignore_selection_changed = false;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {


                        int nCaretIndex = 0;
                        int nCurlength = 0;
                        int nSelLength = 0;
                        bool bSetSelection = false;

                        _last_text_match = sText;

                        if (bOnlyEntries)
                        {
                            temp_text = Convert.ToString(Items[0]);

                            if (temp_text != null)
                            {
                                if (_textBox.Text != temp_text)
                                {
                                    // vorschlagswert in der textbox anzeigen

                                    nCaretIndex = _textBox.CaretIndex;
                                    nCurlength = _textBox.Text.Length;

                                    _ignore_text_changed = true;

                                    _ignore_selection_changed = true;
                                    SelectedIndex = 0; // fire selection changed
                                    _selectionchanged = false;

                                    nSelLength = temp_text.Length - nCurlength;
                                    _textBox.Text = temp_text;
                                    _ignore_selection_changed = false;

                                    bSetSelection = true;

                                    _ignore_text_changed = false;
                                }
                            }
                        }

                        OpenPopup();

                        if (bSetSelection)
                        {
                            _textBox.CaretIndex = nCaretIndex;
                            _textBox.SelectionStart = nCurlength;
                            _textBox.SelectionLength = nSelLength;
                        }
                    }
                });
                
            }
            else
            {
                //if (String.IsNullOrEmpty(_textBox.Text) && Items.Filter != null)
                //    Items.Filter = null;

                
                _moved_key_down = false;

                if (!_selectionchanged)
                {
                    Dispatcher.Invoke(DispatcherPriority.Normal, (NoParas) delegate
                   {
                       OpenPopup();
                   });
                }
                else
                {
                    Dispatcher.Invoke(DispatcherPriority.Normal, (NoParas)delegate
                    {
                        List<object> tmp = new List<object>(1);
                        tmp.Add(SelectedItem);
                        ItemsSource = tmp;
                        ClosePopup();
                    });
                }

                _selectionchanged = false;
                
            }
            

            Dispatcher.Invoke(DispatcherPriority.Normal, (NoParas)delegate
            {
                CheckToggleButton();
                _ignore_items_source_changed = false;
            });

            _ignore_selection_changed = bOldIgnoreSelChange;
        }

        private void background_filter_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Dispatcher.Invoke(DispatcherPriority.Normal, (NoParas)delegate
            {
                SetIsFetchingData(false);
                _must_update_filter = false;

            });
        }

        private void background_items_source_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundDataFetchingEventArgs args = null;

            Dispatcher.Invoke(DispatcherPriority.Normal, (NoParas) delegate
               {
                   SetIsFetchingData(true);
                   string sText = "";

                   if (_textBox != null)
                       sText = _textBox.Text;

                   args = new BackgroundDataFetchingEventArgs(sText, ItemsSource);
               });

            if(BackgroundDataFetching != null)
            {
                BackgroundDataFetching(this, args);
            }

            e.Result = args;
        }

        private void background_items_source_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if(e.Error == null)
            {
                Dispatcher.Invoke(DispatcherPriority.Normal, (NoParas)delegate
                {
                    BackgroundDataFetchingEventArgs args = e.Result as BackgroundDataFetchingEventArgs;

                    if (args != null)
                    {
                        BackgroundItemsSource = args.DataFetchingOutput;
                        _background_itemsource_type = args.DataFetchingOutputType;

                        if(MinCharsForItemsourceFiltering <= 0)
                        {
                            _ignore_items_source_changed = true;
                            if (args.DataFetchingOutputType != typeof(object))
                            {
                                // item source list type casting
                                MethodInfo miCast = GetEnumerableCastMethodInfo();
                                MethodInfo miCastType = miCast.MakeGenericMethod(new Type[] { args.DataFetchingOutputType });
                                ItemsSource = (IEnumerable)miCastType.Invoke(null, new object[] { BackgroundItemsSource });
                            }
                            else
                            {
                                ItemsSource = BackgroundItemsSource.AsEnumerable();
                            }

                            _ignore_items_source_changed = false;
                        }

                        CheckToggleButton();
                    }
                });

            }

            Dispatcher.Invoke(DispatcherPriority.Normal, (NoParas)delegate
            {
                SetIsFetchingData(false);
            });

            if (BackgroundDataFetchingFinished != null)
                BackgroundDataFetchingFinished(this, e.Result as BackgroundDataFetchingEventArgs);

            if (_must_update_filter)
            {
                Dispatcher.Invoke(DispatcherPriority.Normal, (NoParas) delegate
               {
                   UpdateBackgroundItemsFilter();
               });
            }
        }
        #endregion

        protected override void OnPreviewKeyDown(KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Down)
            {
                OpenPopup();

                if (SelectedIndex == -1)
                    SelectedIndex = 0;
                else if (SelectedIndex == Items.Count - 1)
                    SelectedIndex = 0;
                else
                    SelectedIndex += 1;

                _moved_key_down = true;
            }
            else if (e.Key == System.Windows.Input.Key.Up)
            {
                OpenPopup();

                if (SelectedIndex == -1)
                    SelectedIndex = 0;
                else if (SelectedIndex == 0)
                    SelectedIndex = Items.Count - 1;
                else
                    SelectedIndex -= 1;

                _moved_key_down = true;
            }
            else
            {
                base.OnPreviewKeyDown(e);
            }
        }

        void AutoCompleteComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (_ignore_selection_changed)
                return;

            // Damit soll verhindert werden, dass der Filter geändert wird, wenn in der Drop-Down ein Eintrag gewählt wurde
            //if(this.SelectedIndex != -1)
            _selectionchanged = true;
            //else
            //    _selectionchanged = false;


            ClosePopup();
        }

        void AutoCompleteComboBox_Loaded(object sender, RoutedEventArgs e)
        {
            _textBox = Template.FindName("PART_EditableTextBox", this) as TextBox;
            _popup = Template.FindName("PART_Popup", this) as Popup;

            FindButton(this);

            if (IsMultilineTextedit)
            {
                SetMultilineTextEdit();
            }

            if (_textBox != null && _popup != null)
            {
                _textBox.TabIndex = 0;
                _textBox.TextChanged -= new TextChangedEventHandler(textBox_TextChanged);
                _textBox.TextChanged += new TextChangedEventHandler(textBox_TextChanged);
            }

            CheckToggleButton();
        }

        void AutoCompleteComboBox_Unloaded(object sender, RoutedEventArgs e)
        {
            System.ComponentModel.DependencyPropertyDescriptor descr1 = System.ComponentModel.DependencyPropertyDescriptor.FromProperty(ItemsSourceProperty, typeof(AutoCompleteComboBox));
            if (descr1 != null)
                descr1.RemoveValueChanged(this, items_source_changed);
        }

        void AutoCompleteComboBox_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if(_textBox == null)
                _textBox = Template.FindName("PART_EditableTextBox", this) as TextBox;

            if(_textBox != null)
            {
                _textBox.HorizontalAlignment = HorizontalAlignment.Left;
                _textBox.MaxWidth = e.NewSize.Width - 26;
                _textBox.Width = _textBox.MaxWidth;

                ContentPresenter cp = Template.FindName("ContentSite", this) as ContentPresenter;
                if(cp != null)
                {
                    cp.MaxWidth = _textBox.MaxWidth;
                    cp.Width = _textBox.MaxWidth;
                }
            }


        }

        void textBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            bool bOldIgnoreSelChange = _ignore_selection_changed;

            if (!WithCustomValue)
                return;

            if (_ignore_text_changed)
                return;

            if (TextChanged != null)
                TextChanged(this, e);

            if (!IsBackgroundLoadingEnabled)
            {

                if (!_selectionchanged && !_moved_key_down)
                {
                    if (String.IsNullOrEmpty(_textBox.Text))
                    {
                        SelectedItem = null;
                        Items.Filter = null;
                        _selectionchanged = false;
                        CheckToggleButton();
                        ClosePopup();
                        return;
                    }

                    string temp_text = _textBox.Text;

                    _ignore_text_changed = true;
                    _ignore_selection_changed = true;
                    if (OnlyListEntries)
                    {
                        // wenn nur vorhandene werte erlaubt sind, dann den stringfilter behaviour immer als startswith verwenden
                        // zwecks autocomplete
                        Items.Filter = a => a.ToString().ToLower().StartsWith(temp_text.ToLower());
                    }
                    else
                    {
                        switch (StringFilterBehaviour)
                        {
                            case StringFilterBehaviours.StartsWith:
                                Items.Filter = a => a.ToString().ToLower().StartsWith(temp_text.ToLower());
                                break;
                            case StringFilterBehaviours.EndsWith:
                                Items.Filter = a => a.ToString().ToLower().EndsWith(temp_text.ToLower());
                                break;
                            case StringFilterBehaviours.Contains:
                                Items.Filter = a => a.ToString().ToLower().Contains(temp_text.ToLower());
                                break;
                        }
                    }
                    _ignore_text_changed = false;
                    _ignore_selection_changed = false;
                    CheckToggleButton();


                    if (Items.Count <= 1 || Items.IsEmpty)
                    {
                        if (Items.IsEmpty || OnlyListEntries)
                            ClosePopup();

                        if (Items.Count == 1 && OnlyListEntries)
                        {
                            _last_text_match = _textBox.Text;
                            temp_text = Convert.ToString(Items[0]);

                            if (temp_text != null)
                            {
                                if (_textBox.Text != temp_text)
                                {
                                    _ignore_text_changed = true;
                                    _ignore_selection_changed = true;
                                    _textBox.Text = temp_text;
                                    _ignore_selection_changed = false;
                                    _textBox.CaretIndex = temp_text.Length;
                                    _ignore_text_changed = false;
                                }
                            }

                            _ignore_selection_changed = true;
                            SelectedIndex = 0; // fire selection changed
                            _selectionchanged = false;
                            _ignore_selection_changed = false;
                        }
                        else
                        {
                            if (Items.Count == 1)
                            {
                                // do nothing
                                _last_text_match = _textBox.Text;
                            }
                            else
                            {
                                if (OnlyListEntries && !string.IsNullOrEmpty(_last_text_match))
                                {
                                    if (temp_text.Length <= 1)
                                        _last_text_match = "";

                                    _ignore_selection_changed = true;
                                    _ignore_text_changed = true;
                                    _textBox.Text = _last_text_match;
                                    _ignore_text_changed = false;
                                    _ignore_selection_changed = false;
                                    textBox_TextChanged(sender, e);
                                }
                                else
                                {
                                    _ignore_selection_changed = true;
                                    SelectedIndex = -1; // fire selection changed
                                    _selectionchanged = false;
                                    _ignore_selection_changed = false;

                                    if (_textBox.Text != temp_text && !OnlyListEntries)
                                    {
                                        _ignore_text_changed = true;
                                        _ignore_selection_changed = true;
                                        _textBox.Text = temp_text;
                                        _ignore_selection_changed = false;
                                        _textBox.CaretIndex = temp_text.Length;
                                        _ignore_text_changed = false;
                                    }

                                    if (OnlyListEntries)
                                    {
                                        //_textBox.Text = string.Empty;
                                        _ignore_selection_changed = true;
                                        _textBox.Text = _last_text_match;
                                        _ignore_selection_changed = false;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        int nCaretIndex = 0;
                        int nCurlength = 0;
                        int nSelLength = 0;
                        bool bSetSelection = false;

                        _last_text_match = _textBox.Text;

                        if (OnlyListEntries)
                        {
                            temp_text = Convert.ToString(Items[0]);

                            if (temp_text != null)
                            {
                                if (_textBox.Text != temp_text)
                                {
                                    // vorschlagswert in der textbox anzeigen

                                    nCaretIndex = _textBox.CaretIndex;
                                    nCurlength = _textBox.Text.Length;

                                    _ignore_text_changed = true;
                                    _ignore_selection_changed = true;
                                    SelectedIndex = 0; // fire selection changed
                                    _selectionchanged = false;
                                    _ignore_selection_changed = false;

                                    nSelLength = temp_text.Length - nCurlength;
                                    _ignore_selection_changed = true;
                                    _textBox.Text = temp_text;
                                    _ignore_selection_changed = false;

                                    bSetSelection = true;

                                    _ignore_text_changed = false;
                                }
                            }
                        }

                        OpenPopup();

                        if(bSetSelection)
                        {
                            _textBox.CaretIndex = nCaretIndex;
                            _textBox.SelectionStart = nCurlength;
                            _textBox.SelectionLength = nSelLength;
                        }
                    }
                }
                else
                {
                    //if (String.IsNullOrEmpty(_textBox.Text) && Items.Filter != null)
                    //    Items.Filter = null;

                    string temp_text = _textBox.Text;
                    _ignore_text_changed = true;
                    switch(StringFilterBehaviour)
                    {
                        case StringFilterBehaviours.StartsWith:
                            Items.Filter = a => a.ToString().ToLower().StartsWith(temp_text.ToLower());
                            break;
                        case StringFilterBehaviours.EndsWith:
                            Items.Filter = a => a.ToString().ToLower().EndsWith(temp_text.ToLower());
                            break;
                        case StringFilterBehaviours.Contains:
                            Items.Filter = a => a.ToString().ToLower().Contains(temp_text.ToLower());
                            break;
                    }
                    _ignore_text_changed = false;
                    

                    if (_selectionchanged)
                        ClosePopup();
                    else
                        OpenPopup();

                    _selectionchanged = false;
                    _moved_key_down = false;
                }
                CheckToggleButton();
            } 
            else
            {
                _must_update_filter = false;
                if(_textBox.Text != null && _textBox.Text.Length >= MinCharsForItemsourceFiltering)
                {
                    StartTextFilterDelayTimer();
                }
                else
                {
                    if(Items != null)
                    {
                        int nCaretIndex = _textBox.CaretIndex;
                        string curText = _textBox.Text;

                        _ignore_items_source_changed = true;
                        _ignore_text_changed = true;
                        SelectedItem = null;
                        ItemsSource = null;
                        _selectionchanged = false;
                        CheckToggleButton();
                        ClosePopup();
                        _textBox.Text = curText;
                        _textBox.CaretIndex = nCaretIndex;
                        _ignore_text_changed = false;
                        _ignore_items_source_changed = false;
                    }
                }
            }

            _ignore_selection_changed = bOldIgnoreSelChange;
        }

        #region dependency property changed events
        void background_items_source_changed(object sender, EventArgs e)
        {
            //_ignore_items_source_changed = true;
            //ItemsSource = null;

            //_ignore_items_source_changed = false;
        }

        void items_source_changed(object sender, EventArgs e)
        {
            //if (_ignore_items_source_changed)
            //    return;

            //if (IsBackgroundLoadingEnabled)
            //    throw new InvalidOperationException("ItemsSource may not be set directly if background loading mode is enabled!");

            //CheckToggleButton();
        }
        #endregion

        #endregion

        #region Dependency Properties
        public bool WithCustomValue
        {
            get { return (bool)GetValue(WithCustomValueProperty); }
            set { SetValue(WithCustomValueProperty, value); }
        }
        public static readonly DependencyProperty WithCustomValueProperty = DependencyProperty.Register("WithCustomValue", typeof(bool), typeof(AutoCompleteComboBox), new UIPropertyMetadata(true));

        public bool OnlyListEntries
        {
            get { return (bool)GetValue(OnlyListEntriesProperty); }
            set { SetValue(OnlyListEntriesProperty, value); }
        }
        public static readonly DependencyProperty OnlyListEntriesProperty = DependencyProperty.Register("OnlyListEntries", typeof(bool), typeof(AutoCompleteComboBox), new UIPropertyMetadata(false));


        public bool IsMultilineTextedit
        {
            get { return (bool)GetValue(IsMultilineTexteditProperty); }
            set { SetValue(IsMultilineTexteditProperty, value); }
        }
        public static readonly DependencyProperty IsMultilineTexteditProperty = DependencyProperty.Register("IsMultilineTextedit", typeof(bool), typeof(AutoCompleteComboBox), new UIPropertyMetadata(false));

        public List<object> BackgroundItemsSource
        {
            get { return (List<object>)GetValue(BackgroundItemsSourceProperty); }
            set { SetValue(BackgroundItemsSourceProperty, value); }
        }

        public static readonly DependencyProperty BackgroundItemsSourceProperty = DependencyProperty.Register("BackgroundItemsSource", typeof(List<object>), typeof(AutoCompleteComboBox), new UIPropertyMetadata(null));

        public bool IsBackgroundLoadingEnabled
        {
            get { return (bool)GetValue(IsBackgroundLoadingEnabledProperty); }
            set { SetValue(IsBackgroundLoadingEnabledProperty, value); }
        }
        public static readonly DependencyProperty IsBackgroundLoadingEnabledProperty = DependencyProperty.Register("IsBackgroundLoadingEnabled", typeof(bool), typeof(AutoCompleteComboBox), new UIPropertyMetadata(false));


        public int MinCharsForItemsourceFiltering
        {
            get { return (int)GetValue(MinCharsForItemsourceFilteringProperty); }
            set { SetValue(MinCharsForItemsourceFilteringProperty, value); }
        }
        public static readonly DependencyProperty MinCharsForItemsourceFilteringProperty = DependencyProperty.Register("MinCharsForItemsourceFiltering", typeof(int), typeof(AutoCompleteComboBox), new UIPropertyMetadata(0));

        public StringFilterBehaviours StringFilterBehaviour
        {
            get { return (StringFilterBehaviours)GetValue(StringFilterBehaviourProperty); }
            set { SetValue(StringFilterBehaviourProperty, value); }
        }
        public static readonly DependencyProperty StringFilterBehaviourProperty = DependencyProperty.Register("StringFilterBehaviour", typeof(StringFilterBehaviours), typeof(AutoCompleteComboBox), new UIPropertyMetadata(StringFilterBehaviours.StartsWith));


        /// <summary>
        /// Private dependency property for setting the IsFetchingData property
        /// </summary>
        private static readonly DependencyPropertyKey IsFetchingDataPropertyKey = DependencyProperty.RegisterReadOnly("IsFetchingData", typeof(bool), typeof(AutoCompleteComboBox), new FrameworkPropertyMetadata(false, FrameworkPropertyMetadataOptions.NotDataBindable));
        
        public bool IsFetchingData
        {
            get { return (bool)GetValue(IsFetchingDataProperty); }
        }
        /// <summary>
        /// Dependency property 
        /// </summary>
        public static readonly DependencyProperty IsFetchingDataProperty = IsFetchingDataPropertyKey.DependencyProperty;

        #endregion

        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            if(e.Property == ItemsSourceProperty)
            {
                if (_ignore_items_source_changed)
                    return;

                if (IsBackgroundLoadingEnabled)
                    throw new InvalidOperationException("ItemsSource may not be set directly if background loading mode is enabled!");

                CheckToggleButton();
            }

            base.OnPropertyChanged(e);

            if(e.Property.Name == "IsMultilineTextedit")
            {
                if (_textBox != null)
                {
                    if ((bool) e.NewValue)
                    {
                        _textBox.TextWrapping = TextWrapping.Wrap;
                        _textBox.AcceptsReturn = true;
                        _textBox.AllowDrop = true;
                    }
                    else
                    {
                        _textBox.TextWrapping = TextWrapping.NoWrap;
                        _textBox.AcceptsReturn = false;
                        _textBox.AllowDrop = false;
                    }
                }
            }
        }


        protected void OpenPopup()
        {
            if (!IsDropDownOpen)
            {
                IsDropDownOpen = true;
                if (_textBox != null && WithCustomValue)
                    _textBox.SelectionStart = _textBox.Text.Length;
            }

            //if (_popup == null)
            //    return;
            //if (_popup.IsOpen == false)
            //{
            //    _popup.IsOpen = true;
            //    if(_textBox != null && WithCustomValue)
            //        _textBox.SelectionStart = _textBox.Text.Length;
            //}
        }

        protected void ClosePopup()
        {
            if(IsDropDownOpen)
                IsDropDownOpen = false;

            //if (_popup == null)
            //    return;

            //if (_popup.IsOpen)
            //    _popup.IsOpen = false;
        }



        public OuterGlowBitmapEffect FocusOuterGlowBitmapEffect
        {
            get { return (OuterGlowBitmapEffect)GetValue(FocusOuterGlowBitmapEffectProperty); }
            set { SetValue(FocusOuterGlowBitmapEffectProperty, value); }
        }
        public static readonly DependencyProperty FocusOuterGlowBitmapEffectProperty = DependencyProperty.Register("FocusOuterGlowBitmapEffect", typeof(OuterGlowBitmapEffect), typeof(AutoCompleteComboBox), new UIPropertyMetadata(null));




        protected delegate void NoParas();

        private TextBox _textBox;
        private Popup _popup;
        private ToggleButton _button;
        private bool _selectionchanged;
        private bool _filterchanged;
        private bool _moved_key_down;
        public event EventHandler<TextChangedEventArgs> TextChanged;



        private bool _ignore_items_source_changed = false;
        private bool _ignore_text_changed = false;
        private bool _ignore_selection_changed = false;

        private Type _background_itemsource_type = typeof (object);
        private MethodInfo _cast_method = null;

        private QueuedBackgroundWorker _background_items_source_worker = null;
        private QueuedBackgroundWorker _background_filter_worker = null;
        private bool _must_update_filter = false;

        public event EventHandler<BackgroundDataFetchingEventArgs> BackgroundDataFetching;
        public event EventHandler<BackgroundDataFetchingEventArgs> BackgroundDataFetchingFinished;

        private System.Timers.Timer _text_filter_delay_timer = null;
        private const double FILTER_DELAY = 400;

        private string _last_text_match = "";
    }
}
