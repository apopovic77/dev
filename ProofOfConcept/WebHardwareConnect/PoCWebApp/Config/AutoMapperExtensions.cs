﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;

namespace PoCWebApp.Config
{
    public static class AutoMapperExtensions
    {
        public static List<TResult> MapTo<TResult>(this IEnumerable self, IMapper mapper)
        {
            if (self == null)
                throw new ArgumentNullException();

            return (List<TResult>)mapper.Map(self, self.GetType(), typeof(List<TResult>));
        }

        public static TResult MapTo<TResult>(this object self, IMapper mapper)
        {
            if (self == null)
                throw new ArgumentNullException();

            return (TResult)mapper.Map(self, self.GetType(), typeof(TResult));
        }

        public static TResult MapPropertiesToInstance<TResult>(this object self, TResult value, IMapper mapper)
        {
            if (self == null)
                throw new ArgumentNullException();

            return (TResult)mapper.Map(self, value, self.GetType(), typeof(TResult));
        }
    }
}