﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Effects;
using System.Windows.Shapes;
using Logicx.DiagramLib.DiagramObjects.Objects;
using Logicx.DiagramLib.Resources.Converter;
using Logicx.WpfUtility;
using BooleanToVisibilityConverter = Logicx.DiagramLib.Resources.Converter.BooleanToVisibilityConverter;

namespace Logicx.DiagramLib.Controls
{
    public class ObjectSelectionMarker : Grid, IDisposable
    {
        #region nested types
        public enum ResizingMode
        {
            None,
            HorizontalLeft,
            HorizontalRight,
            VerticalTop,
            VerticalBottom,
            BothLT,
            BothRT,
            BothRB,
            BothLB
        }

        private enum GripperPosition
        {
            TopLeft,
            Top,
            TopRight,
            Right,
            BottomRight,
            Bottom,
            BottomLeft,
            Left
        }

        public class StartResizingEventArgs : EventArgs
        {
            public StartResizingEventArgs(ResizingMode resize_mode, Cursor resize_cursor, int stylus_id, bool is_mouse)
            {
                Mode = resize_mode;
                ResizeCursor = resize_cursor;
                StylusId = stylus_id;
                IsMouse = is_mouse;
            }

            public ResizingMode Mode
            {
                get;
                private set;
            }

            public Cursor ResizeCursor
            {
                get;
                private set;
            }

            public int StylusId
            {
                get;
                private set;
            }

            public bool IsMouse
            {
                get;
                private set;
            }
        }

        public class CustomGripperQueryEventArgs : EventArgs
        {
            public CustomGripperQueryEventArgs(bool canResizeX, bool canResizeY)
            {
                CanResizeX = canResizeX;
                CanResizeY = canResizeY;
            }

            public bool CanResizeX
            {
                get;
                private set;
            }

            public bool CanResizeY
            {
                get;
                private set;
            }

            public FrameworkElement GripperLeftTop
            {
                get;
                set;
            }

            public FrameworkElement GripperLeft
            {
                get;
                set;
            }

            public FrameworkElement GripperLeftBottom
            {
                get;
                set;
            }

            public FrameworkElement GripperRightTop
            {
                get;
                set;
            }

            public FrameworkElement GripperRight
            {
                get;
                set;
            }

            public FrameworkElement GripperRightBottom
            {
                get;
                set;
            }

            public FrameworkElement GripperTop
            {
                get;
                set;
            }

            public FrameworkElement GripperBottom
            {
                get;
                set;
            }
        }
        #endregion

        #region Constants
        #endregion

        #region Construction and Initialization
        public ObjectSelectionMarker(Decorator child_visual, bool canresize_x, bool canresize_y)
        {
            _child_visual = child_visual;
            CanResizeX = canresize_x;
            CanResizeY = canresize_y;

            this.ClipToBounds = false;

            Binding left_binding = new Binding("(Canvas.Left)");
            left_binding.Source = child_visual;
            this.SetBinding(Canvas.LeftProperty, left_binding);

            Binding top_binding = new Binding("(Canvas.Top)");
            top_binding.Source = child_visual;
            this.SetBinding(Canvas.TopProperty, top_binding);

            Binding width_binding = new Binding("ActualWidth");
            width_binding.Source = child_visual;
            this.SetBinding(WidthProperty, width_binding);

            Binding height_binding = new Binding("ActualHeight");
            height_binding.Source = child_visual;
            this.SetBinding(HeightProperty, height_binding);

            this.Children.Add(child_visual);

            InitializeMoveGrippers();

            InitializeGrippers();

            InitDefaultStyles();
        }
        #endregion

        #region Operations

        public void Dispose()
        {
            BindingOperations.ClearAllBindings(this);
            this.Children.Clear();
            _child_visual = null;
            AssociatedPointObject = null;
        }

        private void InitDefaultStyles()
        {
            if (GripperStyle == null)
            {
                Style new_default_style = new Style(typeof(Rectangle));
                new_default_style.Setters.Add(new Setter(WidthProperty, (double)5));
                new_default_style.Setters.Add(new Setter(HeightProperty, (double)5));
                new_default_style.Setters.Add(new Setter(Rectangle.FillProperty, new SolidColorBrush(Color.FromArgb(0x7F, 0x00, 0x00, 0x00))));
                //new_default_style.Setters.Add(new Setter(MarginProperty, new Thickness(2)));

                GripperStyle = new_default_style;
            }

            if (Style == null)
            {
                Style default_selection_style = new Style(typeof(Grid));

                #region selected=true, out of range=false
                MultiDataTrigger selected_data_trigger = new MultiDataTrigger();

                Binding isselected_binding = new Binding("IsSelected");
                isselected_binding.Source = this;
                Condition is_selected_condition = new Condition(isselected_binding, true);

                Binding is_out_of_range_binding = new Binding("IsOutOfXRange");
                is_out_of_range_binding.Source = this;
                Condition is_out_of_range_condition = new Condition(is_out_of_range_binding, false);

                selected_data_trigger.Conditions.Add(is_selected_condition);
                selected_data_trigger.Conditions.Add(is_out_of_range_condition);

                DropShadowEffect bitmap_effect = new DropShadowEffect();
                bitmap_effect.ShadowDepth = 0;
                bitmap_effect.BlurRadius = 6;
                bitmap_effect.Opacity = 1;
                bitmap_effect.Color = Colors.Blue;

                selected_data_trigger.Setters.Add(new Setter(EffectProperty, bitmap_effect));
                default_selection_style.Triggers.Add(selected_data_trigger);
                #endregion

                #region selected=true, out of range=true
                MultiDataTrigger selected_data_trigger_1 = new MultiDataTrigger();

                Binding isselected_binding_1 = new Binding("IsSelected");
                isselected_binding_1.Source = this;
                Condition is_selected_condition_1 = new Condition(isselected_binding_1, true);

                Binding is_out_of_range_binding_1 = new Binding("IsOutOfXRange");
                is_out_of_range_binding_1.Source = this;
                Condition is_out_of_range_condition_1 = new Condition(is_out_of_range_binding_1, true);

                selected_data_trigger_1.Conditions.Add(is_selected_condition_1);
                selected_data_trigger_1.Conditions.Add(is_out_of_range_condition_1);

                DropShadowEffect bitmap_effect_1 = new DropShadowEffect();
                bitmap_effect_1.ShadowDepth = 0;
                bitmap_effect_1.BlurRadius = 6;
                bitmap_effect_1.Opacity = 1;
                bitmap_effect_1.Color = Colors.MediumVioletRed;

                selected_data_trigger_1.Setters.Add(new Setter(EffectProperty, bitmap_effect_1));
                default_selection_style.Triggers.Add(selected_data_trigger_1);
                #endregion

                #region selected=false, out of range=true
                MultiDataTrigger selected_data_trigger_2 = new MultiDataTrigger();

                Binding isselected_binding_2 = new Binding("IsSelected");
                isselected_binding_2.Source = this;
                Condition is_selected_condition_2 = new Condition(isselected_binding_2, false);

                Binding is_out_of_range_binding_2 = new Binding("IsOutOfXRange");
                is_out_of_range_binding_2.Source = this;
                Condition is_out_of_range_condition_2 = new Condition(is_out_of_range_binding_2, true);

                selected_data_trigger_2.Conditions.Add(is_selected_condition_2);
                selected_data_trigger_2.Conditions.Add(is_out_of_range_condition_2);

                DropShadowEffect bitmap_effect_2 = new DropShadowEffect();
                bitmap_effect_2.ShadowDepth = 0;
                bitmap_effect_2.BlurRadius = 10;
                bitmap_effect_2.Opacity = 1;
                bitmap_effect_2.Color = Colors.Red;

                selected_data_trigger_2.Setters.Add(new Setter(EffectProperty, bitmap_effect_2));
                default_selection_style.Triggers.Add(selected_data_trigger_2);
                #endregion

                this.Style = default_selection_style;
            }
        }

        private void InitializeGrippers(CustomGripperQueryEventArgs custom_grippers)
        {
            MultiBinding visibility_binding = new MultiBinding();
            visibility_binding.Converter = new ResizeGripVisibilityConverter();

            visibility_binding.Bindings.Add(new Binding("IsSelected") { Source = this });
            visibility_binding.Bindings.Add(new Binding("CanResizeX") { Source = this });
            visibility_binding.Bindings.Add(new Binding("CanResizeY") { Source = this });

            if (custom_grippers.GripperLeft != null)
            {
                _custom_grippers[custom_grippers.GripperLeft] = GripperPosition.Left;

                custom_grippers.GripperLeft.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
                custom_grippers.GripperLeft.VerticalAlignment = System.Windows.VerticalAlignment.Center;
                custom_grippers.GripperLeft.Cursor = Cursors.SizeWE;
                custom_grippers.GripperLeft.SetBinding(VisibilityProperty, visibility_binding);

                if (!CanResizeX)
                {
                    custom_grippers.GripperLeft.Cursor = Cursors.No;
                    custom_grippers.GripperLeft.IsHitTestVisible = false;
                    custom_grippers.GripperLeft.Opacity = 0.3;
                }
                else
                {
                    custom_grippers.GripperLeft.MouseDown += new MouseButtonEventHandler(rc_lm_MouseDown);
                    custom_grippers.GripperLeft.StylusDown += new StylusDownEventHandler(rc_lm_StylusDown);
                }

                Children.Add(custom_grippers.GripperLeft);
            }

            if (custom_grippers.GripperLeftTop != null)
            {
                _custom_grippers[custom_grippers.GripperLeftTop] = GripperPosition.TopLeft;

                custom_grippers.GripperLeftTop.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
                custom_grippers.GripperLeftTop.VerticalAlignment = System.Windows.VerticalAlignment.Top;
                custom_grippers.GripperLeftTop.Cursor = Cursors.SizeNWSE;
                custom_grippers.GripperLeftTop.SetBinding(VisibilityProperty, visibility_binding);
                custom_grippers.GripperLeftTop.MouseDown += new MouseButtonEventHandler(rc_lt_MouseDown);
                custom_grippers.GripperLeftTop.StylusDown += new StylusDownEventHandler(rc_lt_StylusDown);

                Children.Add(custom_grippers.GripperLeftTop);
            }

            if (custom_grippers.GripperTop != null)
            {
                _custom_grippers[custom_grippers.GripperTop] = GripperPosition.Top;

                custom_grippers.GripperTop.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
                custom_grippers.GripperTop.VerticalAlignment = System.Windows.VerticalAlignment.Top;
                custom_grippers.GripperTop.Cursor = Cursors.SizeNS;
                custom_grippers.GripperTop.SetBinding(VisibilityProperty, visibility_binding);

                if (!CanResizeY)
                {
                    custom_grippers.GripperTop.Cursor = Cursors.No;
                    custom_grippers.GripperTop.IsHitTestVisible = false;
                    custom_grippers.GripperTop.Opacity = 0.3;
                }
                else
                {
                    custom_grippers.GripperTop.MouseDown += new MouseButtonEventHandler(rc_tm_MouseDown);
                    custom_grippers.GripperTop.StylusDown += new StylusDownEventHandler(rc_tm_StylusDown);
                }

                Children.Add(custom_grippers.GripperTop);
            }

            if (custom_grippers.GripperRightTop != null)
            {
                _custom_grippers[custom_grippers.GripperRightTop] = GripperPosition.TopRight;

                custom_grippers.GripperRightTop.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
                custom_grippers.GripperRightTop.VerticalAlignment = System.Windows.VerticalAlignment.Top;
                custom_grippers.GripperRightTop.Cursor = Cursors.SizeNESW;
                custom_grippers.GripperRightTop.SetBinding(VisibilityProperty, visibility_binding);
                custom_grippers.GripperRightTop.MouseDown += new MouseButtonEventHandler(rc_rt_MouseDown);
                custom_grippers.GripperRightTop.StylusDown += new StylusDownEventHandler(rc_rt_StylusDown);

                Children.Add(custom_grippers.GripperRightTop);
            }

            if (custom_grippers.GripperRight != null)
            {
                _custom_grippers[custom_grippers.GripperRight] = GripperPosition.Right;

                custom_grippers.GripperRight.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
                custom_grippers.GripperRight.VerticalAlignment = System.Windows.VerticalAlignment.Center;
                custom_grippers.GripperRight.Cursor = Cursors.SizeWE;
                custom_grippers.GripperRight.SetBinding(VisibilityProperty, visibility_binding);

                if (!CanResizeX)
                {
                    custom_grippers.GripperRight.Cursor = Cursors.No;
                    custom_grippers.GripperRight.IsHitTestVisible = false;
                    custom_grippers.GripperRight.Opacity = 0.3;
                }
                else
                {
                    custom_grippers.GripperRight.MouseDown += new MouseButtonEventHandler(rc_rm_MouseDown);
                    custom_grippers.GripperRight.StylusDown += new StylusDownEventHandler(rc_rm_StylusDown);
                }

                Children.Add(custom_grippers.GripperRight);
            }

            if (custom_grippers.GripperLeftBottom != null)
            {
                _custom_grippers[custom_grippers.GripperLeftBottom] = GripperPosition.BottomLeft;

                custom_grippers.GripperLeftBottom.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
                custom_grippers.GripperLeftBottom.VerticalAlignment = System.Windows.VerticalAlignment.Bottom;
                custom_grippers.GripperLeftBottom.Cursor = Cursors.SizeNESW;
                custom_grippers.GripperLeftBottom.SetBinding(VisibilityProperty, visibility_binding);
                custom_grippers.GripperLeftBottom.MouseDown += new MouseButtonEventHandler(rc_lb_MouseDown);
                custom_grippers.GripperLeftBottom.StylusDown += new StylusDownEventHandler(rc_lb_StylusDown);

                Children.Add(custom_grippers.GripperLeftBottom);
            }

            if (custom_grippers.GripperBottom != null)
            {
                _custom_grippers[custom_grippers.GripperBottom] = GripperPosition.Bottom;

                custom_grippers.GripperBottom.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
                custom_grippers.GripperBottom.VerticalAlignment = System.Windows.VerticalAlignment.Bottom;
                custom_grippers.GripperBottom.Cursor = Cursors.SizeNS;
                custom_grippers.GripperBottom.SetBinding(VisibilityProperty, visibility_binding);

                if (!CanResizeY)
                {
                    custom_grippers.GripperBottom.Cursor = Cursors.No;
                    custom_grippers.GripperBottom.IsHitTestVisible = false;
                    custom_grippers.GripperBottom.Opacity = 0.3;
                }
                else
                {
                    custom_grippers.GripperBottom.MouseDown += new MouseButtonEventHandler(rc_bm_MouseDown);
                    custom_grippers.GripperBottom.StylusDown += new StylusDownEventHandler(rc_bm_StylusDown);
                }

                Children.Add(custom_grippers.GripperBottom);
            }

            if (custom_grippers.GripperRightBottom != null)
            {
                _custom_grippers[custom_grippers.GripperRightBottom] = GripperPosition.BottomRight;

                custom_grippers.GripperRightBottom.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
                custom_grippers.GripperRightBottom.VerticalAlignment = System.Windows.VerticalAlignment.Bottom;
                custom_grippers.GripperRightBottom.Cursor = Cursors.SizeNWSE;
                custom_grippers.GripperRightBottom.SetBinding(VisibilityProperty, visibility_binding);
                custom_grippers.GripperRightBottom.MouseDown += new MouseButtonEventHandler(rc_rb_MouseDown);
                custom_grippers.GripperRightBottom.StylusDown += new StylusDownEventHandler(rc_rb_StylusDown);

                Children.Add(custom_grippers.GripperRightBottom);
            }
        }

        private void InitializeMoveGrippers()
        {
            #region top elements
            //Rectangle r_dock_top = new Rectangle();
            //r_dock_top.Height = 10;
            //r_dock_top.VerticalAlignment = VerticalAlignment.Top;
            //r_dock_top.Margin = new Thickness(0, 0, 0, -3);
            //r_dock_top.Fill = new SolidColorBrush(ColorGenerator.HexToColor("#FF8D363B"));

            //Binding rectangle_width_binding = new Binding("ActualWidth");
            //rectangle_width_binding.Source = this;
            //r_dock_top.SetBinding(Rectangle.WidthProperty, rectangle_width_binding);

            //Binding rectangle_visibility_binding = new Binding("DockTop");
            //rectangle_visibility_binding.Source = this;
            //rectangle_visibility_binding.Converter = new BooleanToVisibilityConverter();
            //r_dock_top.SetBinding(Rectangle.VisibilityProperty, rectangle_visibility_binding);

            //Children.Add(r_dock_top);


            Polygon p_top = new Polygon();
            p_top.Points.Add(new Point(0, 5));
            p_top.Points.Add(new Point(10, 5));
            p_top.Points.Add(new Point(5, 0));

            Viewbox vb_top = new Viewbox();
            vb_top.Child = p_top;
            vb_top.MaxHeight = 10;
            vb_top.Margin = new Thickness(0, -10, 0, 0);
            vb_top.VerticalAlignment = VerticalAlignment.Top;
            Binding top_width_binding = new Binding("ActualWidth");
            top_width_binding.Source = this;
            vb_top.SetBinding(Viewbox.WidthProperty, top_width_binding);

            Binding visibility_binding = new Binding("IsMoving");
            visibility_binding.Source = this;
            visibility_binding.Converter = new BooleanToVisibilityConverter();
            vb_top.SetBinding(Viewbox.VisibilityProperty, visibility_binding);

            Binding can_move_binding = new Binding("CanMove");
            can_move_binding.Source = this;
            can_move_binding.Converter = new MovingGrippersColorConverter();
            p_top.SetBinding(Polygon.FillProperty, can_move_binding);

            Children.Add(vb_top);
            #endregion

            #region bottom elements
            _rectangle_dock_bottom = new Rectangle();
            _rectangle_dock_bottom.Height = 10;
            _rectangle_dock_bottom.VerticalAlignment = VerticalAlignment.Bottom;
            _rectangle_dock_bottom.Margin = new Thickness(0, 0, 0, -3);
            _rectangle_dock_bottom.Fill = new SolidColorBrush(ColorGenerator.HexToColor("#FF8D363B"));

            Binding rectangle_width_binding = new Binding("ActualWidth");
            rectangle_width_binding.Source = this;
            _rectangle_dock_bottom.SetBinding(Rectangle.WidthProperty, rectangle_width_binding);

            Binding rectangle_visibility_binding = new Binding("DockBottom");
            rectangle_visibility_binding.Source = this;
            rectangle_visibility_binding.Converter = new BooleanToVisibilityConverter();
            _rectangle_dock_bottom.SetBinding(Rectangle.VisibilityProperty, rectangle_visibility_binding);

            Children.Add(_rectangle_dock_bottom);


            Polygon p_bottom = new Polygon();
            p_bottom.Points.Add(new Point(0, 0));
            p_bottom.Points.Add(new Point(10, 0));
            p_bottom.Points.Add(new Point(5, 5));

            Viewbox vb_bottom = new Viewbox();
            vb_bottom.Child = p_bottom;
            vb_bottom.MaxHeight = 10;
            vb_bottom.Margin = new Thickness(0, 0, 0, -10);
            vb_bottom.VerticalAlignment = VerticalAlignment.Bottom;
            Binding bottom_width_binding = new Binding("ActualWidth");
            bottom_width_binding.Source = this;
            vb_bottom.SetBinding(Viewbox.WidthProperty, bottom_width_binding);

            visibility_binding = new Binding("IsMoving");
            visibility_binding.Source = this;
            visibility_binding.Converter = new BooleanToVisibilityConverter();
            vb_bottom.SetBinding(Viewbox.VisibilityProperty, visibility_binding);

            can_move_binding = new Binding("CanMove");
            can_move_binding.Source = this;
            can_move_binding.Converter = new MovingGrippersColorConverter();
            p_bottom.SetBinding(Polygon.FillProperty, can_move_binding);

            Children.Add(vb_bottom);
            #endregion

            #region left elements

            _rectangle_dock_left = new Rectangle();
            _rectangle_dock_left.Width = 10;
            _rectangle_dock_left.HorizontalAlignment = HorizontalAlignment.Left;
            _rectangle_dock_left.Margin = new Thickness(0, 0, -3, 0);
            _rectangle_dock_left.Fill = new SolidColorBrush(ColorGenerator.HexToColor("#FF8D363B"));

            Binding rectangle_height_binding = new Binding("ActualHeight");
            rectangle_height_binding.Source = this;
            _rectangle_dock_left.SetBinding(Rectangle.HeightProperty, rectangle_height_binding);

            rectangle_visibility_binding = new Binding("DockLeft");
            rectangle_visibility_binding.Source = this;
            rectangle_visibility_binding.Converter = new BooleanToVisibilityConverter();
            _rectangle_dock_left.SetBinding(Rectangle.VisibilityProperty, rectangle_visibility_binding);

            Children.Add(_rectangle_dock_left);


            Polygon p_left = new Polygon();
            p_left.Points.Add(new Point(5, 0));
            p_left.Points.Add(new Point(5, 10));
            p_left.Points.Add(new Point(0, 5));

            Viewbox vb_left = new Viewbox();
            vb_left.Child = p_left;
            vb_left.MaxWidth = 10;
            vb_left.Margin = new Thickness(-10, 0, 0, 0);
            vb_left.HorizontalAlignment = HorizontalAlignment.Left;
            Binding left_height_binding = new Binding("ActualHeight");
            left_height_binding.Source = this;
            vb_left.SetBinding(Viewbox.HeightProperty, left_height_binding);

            visibility_binding = new Binding("IsMoving");
            visibility_binding.Source = this;
            visibility_binding.Converter = new BooleanToVisibilityConverter();
            vb_left.SetBinding(Viewbox.VisibilityProperty, visibility_binding);

            can_move_binding = new Binding("CanMove");
            can_move_binding.Source = this;
            can_move_binding.Converter = new MovingGrippersColorConverter();
            p_left.SetBinding(Polygon.FillProperty, can_move_binding);

            Children.Add(vb_left);
            #endregion

            #region right elements

            _rectangle_dock_right = new Rectangle();
            _rectangle_dock_right.Width = 10;
            _rectangle_dock_right.HorizontalAlignment = HorizontalAlignment.Right;
            _rectangle_dock_right.Margin = new Thickness(0, 0, -3, 0);
            _rectangle_dock_right.Fill = new SolidColorBrush(ColorGenerator.HexToColor("#FF8D363B"));

            rectangle_height_binding = new Binding("ActualHeight");
            rectangle_height_binding.Source = this;
            _rectangle_dock_right.SetBinding(Rectangle.HeightProperty, rectangle_height_binding);

            rectangle_visibility_binding = new Binding("DockRight");
            rectangle_visibility_binding.Source = this;
            rectangle_visibility_binding.Converter = new BooleanToVisibilityConverter();
            _rectangle_dock_right.SetBinding(Rectangle.VisibilityProperty, rectangle_visibility_binding);

            Children.Add(_rectangle_dock_right);


            Polygon p_right = new Polygon();
            p_right.Points.Add(new Point(0, 0));
            p_right.Points.Add(new Point(0, 10));
            p_right.Points.Add(new Point(5, 5));

            Viewbox vb_right = new Viewbox();
            vb_right.Child = p_right;
            vb_right.MaxWidth = 10;
            vb_right.Margin = new Thickness(0, 0, -10, 0);
            vb_right.HorizontalAlignment = HorizontalAlignment.Right;
            Binding right_height_binding = new Binding("ActualHeight");
            right_height_binding.Source = this;
            vb_right.SetBinding(Viewbox.HeightProperty, right_height_binding);

            visibility_binding = new Binding("IsMoving");
            visibility_binding.Source = this;
            visibility_binding.Converter = new BooleanToVisibilityConverter();
            vb_right.SetBinding(Viewbox.VisibilityProperty, visibility_binding);

            can_move_binding = new Binding("CanMove");
            can_move_binding.Source = this;
            can_move_binding.Converter = new MovingGrippersColorConverter();
            p_right.SetBinding(Polygon.FillProperty, can_move_binding);

            Children.Add(vb_right);
            #endregion
        }

        public void SetLeftDistance(double distance)
        {
            if (_rectangle_dock_left != null)
            {
                if (DockLeft)
                {
                    _rectangle_dock_left.Width = distance + 3;
                    _rectangle_dock_left.Margin = new Thickness(-1 * distance, 0, 0, 0);
                }
            }

        }

        public void SetRightDistance(double distance)
        {
            if (_rectangle_dock_right != null)
            {
                if (DockRight)
                {
                    _rectangle_dock_right.Width = distance + 3;
                    _rectangle_dock_right.Margin = new Thickness(0, 0, -1 * distance, 0);
                }
            }

        }

        public void SetBottomDistance(double distance)
        {
            if (_rectangle_dock_bottom != null)
            {
                if (DockBottom)
                {
                    _rectangle_dock_bottom.Height = distance + 3;
                    _rectangle_dock_bottom.Margin = new Thickness(0, 0, 0, -1 * distance);
                }
            }

        }

        private void InitializeGrippers()
        {
            MultiBinding visibility_binding = new MultiBinding();
            visibility_binding.Converter = new ResizeGripVisibilityConverter();

            visibility_binding.Bindings.Add(new Binding("IsSelected") { Source = this });
            visibility_binding.Bindings.Add(new Binding("CanResizeX") { Source = this });
            visibility_binding.Bindings.Add(new Binding("CanResizeY") { Source = this });

            Binding gripper_style_binding = new Binding("GripperStyle");
            gripper_style_binding.Source = this;

            if (rc_lm == null)
            {
                rc_lm = new Rectangle();
                rc_lm.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
                rc_lm.VerticalAlignment = System.Windows.VerticalAlignment.Center;
                rc_lm.IsHitTestVisible = true;
                rc_lm.Cursor = Cursors.SizeWE;
            }

            rc_lm.SetBinding(VisibilityProperty, visibility_binding);
            rc_lm.SetBinding(StyleProperty, gripper_style_binding);

            if (!CanResizeX)
            {
                rc_lm.Cursor = Cursors.No;
                rc_lm.IsHitTestVisible = false;
                rc_lm.Opacity = 0.3;
            }
            else
            {
                rc_lm.MouseDown += new MouseButtonEventHandler(rc_lm_MouseDown);
                rc_lm.StylusDown += new StylusDownEventHandler(rc_lm_StylusDown);
            }

            if (rc_lt == null)
            {
                rc_lt = new Rectangle();
                rc_lt.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
                rc_lt.VerticalAlignment = System.Windows.VerticalAlignment.Top;
                rc_lt.IsHitTestVisible = true;
                rc_lt.Cursor = Cursors.SizeNWSE;
            }

            rc_lt.SetBinding(VisibilityProperty, visibility_binding);
            rc_lt.SetBinding(StyleProperty, gripper_style_binding);
            rc_lt.MouseDown += new MouseButtonEventHandler(rc_lt_MouseDown);
            rc_lt.StylusDown += new StylusDownEventHandler(rc_lt_StylusDown);

            if (rc_tm == null)
            {
                rc_tm = new Rectangle();
                rc_tm.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
                rc_tm.VerticalAlignment = System.Windows.VerticalAlignment.Top;
                rc_tm.IsHitTestVisible = true;
                rc_tm.Cursor = Cursors.SizeNS;
            }

            rc_tm.SetBinding(VisibilityProperty, visibility_binding);
            rc_tm.SetBinding(StyleProperty, gripper_style_binding);


            if (!CanResizeY)
            {
                rc_tm.Cursor = Cursors.No;
                rc_tm.IsHitTestVisible = false;
                rc_tm.Opacity = 0.3;
            }
            else
            {
                rc_tm.MouseDown += new MouseButtonEventHandler(rc_tm_MouseDown);
                rc_tm.StylusDown += new StylusDownEventHandler(rc_tm_StylusDown);
            }

            if (rc_rt == null)
            {
                rc_rt = new Rectangle();
                rc_rt.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
                rc_rt.VerticalAlignment = System.Windows.VerticalAlignment.Top;
                rc_rt.IsHitTestVisible = true;
                rc_rt.Cursor = Cursors.SizeNESW;
            }

            rc_rt.SetBinding(VisibilityProperty, visibility_binding);
            rc_rt.SetBinding(StyleProperty, gripper_style_binding);
            rc_rt.MouseDown += new MouseButtonEventHandler(rc_rt_MouseDown);
            rc_rt.StylusDown += new StylusDownEventHandler(rc_rt_StylusDown);

            if (rc_rm == null)
            {
                rc_rm = new Rectangle();
                rc_rm.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
                rc_rm.VerticalAlignment = System.Windows.VerticalAlignment.Center;
                rc_rm.IsHitTestVisible = true;
                rc_rm.Cursor = Cursors.SizeWE;
            }

            rc_rm.SetBinding(VisibilityProperty, visibility_binding);
            rc_rm.SetBinding(StyleProperty, gripper_style_binding);

            if (!CanResizeX)
            {
                rc_rm.Cursor = Cursors.No;
                rc_rm.IsHitTestVisible = false;
                rc_rm.Opacity = 0.3;
            }
            else
            {
                rc_rm.MouseDown += new MouseButtonEventHandler(rc_rm_MouseDown);
                rc_rm.StylusDown += new StylusDownEventHandler(rc_rm_StylusDown);
            }

            if (rc_lb == null)
            {
                rc_lb = new Rectangle();
                rc_lb.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
                rc_lb.VerticalAlignment = System.Windows.VerticalAlignment.Bottom;
                rc_lb.IsHitTestVisible = true;
                rc_lb.Cursor = Cursors.SizeNESW;
            }

            rc_lb.SetBinding(VisibilityProperty, visibility_binding);
            rc_lb.SetBinding(StyleProperty, gripper_style_binding);
            rc_lb.MouseDown += new MouseButtonEventHandler(rc_lb_MouseDown);
            rc_lb.StylusDown += new StylusDownEventHandler(rc_lb_StylusDown);

            if (rc_bm == null)
            {
                rc_bm = new Rectangle();
                rc_bm.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
                rc_bm.VerticalAlignment = System.Windows.VerticalAlignment.Bottom;
                rc_bm.IsHitTestVisible = true;
                rc_bm.Cursor = Cursors.SizeNS;
            }

            rc_bm.SetBinding(VisibilityProperty, visibility_binding);
            rc_bm.SetBinding(StyleProperty, gripper_style_binding);


            if (!CanResizeY)
            {
                rc_bm.Cursor = Cursors.No;
                rc_bm.IsHitTestVisible = false;
                rc_bm.Opacity = 0.3;
            }
            else
            {
                rc_bm.MouseDown += new MouseButtonEventHandler(rc_bm_MouseDown);
                rc_bm.StylusDown += new StylusDownEventHandler(rc_bm_StylusDown);
            }

            if (rc_rb == null)
            {
                rc_rb = new Rectangle();
                rc_rb.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
                rc_rb.VerticalAlignment = System.Windows.VerticalAlignment.Bottom;
                rc_rb.IsHitTestVisible = true;
                rc_rb.Cursor = Cursors.SizeNWSE;
            }

            rc_rb.SetBinding(VisibilityProperty, visibility_binding);
            rc_rb.SetBinding(StyleProperty, gripper_style_binding);
            rc_rb.MouseDown += new MouseButtonEventHandler(rc_rb_MouseDown);
            rc_rb.StylusDown += new StylusDownEventHandler(rc_rb_StylusDown);

            this.Children.Add(rc_lm);
            this.Children.Add(rc_tm);
            this.Children.Add(rc_rm);
            this.Children.Add(rc_bm);

            this.Children.Add(rc_lt);
            this.Children.Add(rc_rt);
            this.Children.Add(rc_lb);
            this.Children.Add(rc_rb);
        }





        public void ReleaseChildVisual()
        {
            if (Children.Contains(_child_visual))
                Children.Remove(_child_visual);
        }

        private void FireStartResizing(StartResizingEventArgs e)
        {
            _is_resizing = true;
            _resize_mode = e.Mode;
            _resizing_cursor = e.ResizeCursor;

            if (StartResizing != null)
                StartResizing(this, e);
        }

        public void StopResizing()
        {
            _is_resizing = false;
            _resize_mode = ResizingMode.None;
            _resizing_cursor = null;
        }

        private CustomGripperQueryEventArgs OnQueryCustomGrippers()
        {
            CustomGripperQueryEventArgs query = new CustomGripperQueryEventArgs(CanResizeX, CanResizeY);
            if (QueryCustomGrippers != null)
                QueryCustomGrippers(this, query);

            return query;
        }

        private void ClearCustomGrippers()
        {
            if (_custom_grippers.Keys.Count > 0)
            {
                foreach (FrameworkElement current_gripper in _custom_grippers.Keys)
                {
                    GripperPosition position = _custom_grippers[current_gripper];

                    if (Children.Contains(current_gripper))
                        this.Children.Remove(current_gripper);

                    BindingOperations.ClearBinding(current_gripper, VisibilityProperty);
                    BindingOperations.ClearBinding(current_gripper, StyleProperty);

                    switch (position)
                    {
                        case GripperPosition.Left:
                            if (CanResizeX)
                            {
                                current_gripper.MouseDown -= rc_lm_MouseDown;
                                current_gripper.StylusDown -= rc_lm_StylusDown;
                            }
                            break;
                        case GripperPosition.Right:
                            if (CanResizeX)
                            {
                                current_gripper.MouseDown -= rc_rm_MouseDown;
                                current_gripper.StylusDown -= rc_rm_StylusDown;
                            }
                            break;
                        case GripperPosition.Top:
                            if (CanResizeY)
                            {
                                current_gripper.MouseDown -= rc_tm_MouseDown;
                                current_gripper.StylusDown -= rc_tm_StylusDown;
                            }
                            break;
                        case GripperPosition.Bottom:
                            if (CanResizeY)
                            {
                                current_gripper.MouseDown -= rc_bm_MouseDown;
                                current_gripper.StylusDown -= rc_bm_StylusDown;
                            }
                            break;
                        case GripperPosition.TopLeft:
                            current_gripper.MouseDown -= rc_lt_MouseDown;
                            current_gripper.StylusDown -= rc_lt_StylusDown;
                            break;
                        case GripperPosition.TopRight:
                            current_gripper.MouseDown -= rc_rt_MouseDown;
                            current_gripper.StylusDown -= rc_rt_StylusDown;
                            break;
                        case GripperPosition.BottomLeft:
                            current_gripper.MouseDown -= rc_lb_MouseDown;
                            current_gripper.StylusDown -= rc_lb_StylusDown;
                            break;
                        case GripperPosition.BottomRight:
                            current_gripper.MouseDown -= rc_rb_MouseDown;
                            current_gripper.StylusDown -= rc_rb_StylusDown;
                            break;
                    }
                }

                _custom_grippers.Clear();
            }
        }

        private void ClearDefaultGrippers()
        {
            if (rc_lm != null)
            {
                if (Children.Contains(rc_lm))
                    this.Children.Remove(rc_lm);

                BindingOperations.ClearBinding(rc_lm, VisibilityProperty);
                BindingOperations.ClearBinding(rc_lm, StyleProperty);
                if (CanResizeX)
                {
                    rc_lm.MouseDown -= rc_lm_MouseDown;
                    rc_lm.StylusDown -= rc_lm_StylusDown;
                }
            }

            if (rc_tm != null)
            {
                if (Children.Contains(rc_tm))
                    this.Children.Remove(rc_tm);

                BindingOperations.ClearBinding(rc_tm, VisibilityProperty);
                BindingOperations.ClearBinding(rc_tm, StyleProperty);
                if (CanResizeY)
                {
                    rc_tm.MouseDown -= rc_tm_MouseDown;
                    rc_tm.StylusDown -= rc_tm_StylusDown;
                }
            }

            if (rc_rm != null)
            {
                if (Children.Contains(rc_rm))
                    this.Children.Remove(rc_rm);

                BindingOperations.ClearBinding(rc_rm, VisibilityProperty);
                BindingOperations.ClearBinding(rc_rm, StyleProperty);
                if (CanResizeX)
                {
                    rc_rm.MouseDown -= rc_rm_MouseDown;
                    rc_rm.StylusDown -= rc_rm_StylusDown;
                }
            }
            if (rc_bm != null)
            {
                if (Children.Contains(rc_bm))
                    this.Children.Remove(rc_bm);

                BindingOperations.ClearBinding(rc_bm, VisibilityProperty);
                BindingOperations.ClearBinding(rc_bm, StyleProperty);
                if (CanResizeY)
                {
                    rc_bm.MouseDown -= rc_bm_MouseDown;
                    rc_bm.StylusDown -= rc_bm_StylusDown;
                }
            }

            if (rc_lt != null)
            {
                if (Children.Contains(rc_lt))
                    this.Children.Remove(rc_lt);

                BindingOperations.ClearBinding(rc_lt, VisibilityProperty);
                BindingOperations.ClearBinding(rc_lt, StyleProperty);
                rc_lt.MouseDown -= rc_lt_MouseDown;
                rc_lt.StylusDown -= rc_lt_StylusDown;
            }
            if (rc_rt != null)
            {
                if (Children.Contains(rc_rt))
                    this.Children.Remove(rc_rt);

                BindingOperations.ClearBinding(rc_rt, VisibilityProperty);
                BindingOperations.ClearBinding(rc_rt, StyleProperty);
                rc_rt.MouseDown -= rc_rt_MouseDown;
                rc_rt.StylusDown -= rc_rt_StylusDown;
            }
            if (rc_lb != null)
            {
                if (Children.Contains(rc_lb))
                    this.Children.Remove(rc_lb);

                BindingOperations.ClearBinding(rc_lb, VisibilityProperty);
                BindingOperations.ClearBinding(rc_lb, StyleProperty);
                rc_lb.MouseDown -= rc_lb_MouseDown;
                rc_lb.StylusDown -= rc_lb_StylusDown;
            }
            if (rc_rb != null)
            {
                if (Children.Contains(rc_rb))
                    this.Children.Remove(rc_rb);

                BindingOperations.ClearBinding(rc_rb, VisibilityProperty);
                BindingOperations.ClearBinding(rc_rb, StyleProperty);
                rc_rb.MouseDown -= rc_rb_MouseDown;
                rc_rb.StylusDown -= rc_rb_StylusDown;
            }
        }

        private void UpdateCustomGripperSetting(bool newValue)
        {
            if (newValue)
            {
                ClearDefaultGrippers();
                CustomGripperQueryEventArgs query = OnQueryCustomGrippers();

                InitializeGrippers(query);
            }
            else
            {
                ClearCustomGrippers();
                InitializeGrippers();
            }
        }
        #endregion

        #region Event Handlers

        void rc_rb_StylusDown(object sender, StylusDownEventArgs e)
        {
            if (e.StylusDevice == null)
                return;

            StartResizingEventArgs sizeArgs = new StartResizingEventArgs(ResizingMode.BothRB, rc_rb.Cursor, e.StylusDevice.Id, false);
            FireStartResizing(sizeArgs);
            e.Handled = true;
        }

        void rc_lb_StylusDown(object sender, StylusDownEventArgs e)
        {
            if (e.StylusDevice == null)
                return;

            StartResizingEventArgs sizeArgs = new StartResizingEventArgs(ResizingMode.BothLB, rc_lb.Cursor, e.StylusDevice.Id, false);
            FireStartResizing(sizeArgs);
            e.Handled = true;
        }

        void rc_bm_StylusDown(object sender, StylusDownEventArgs e)
        {
            if (e.StylusDevice == null)
                return;

            StartResizingEventArgs sizeArgs = new StartResizingEventArgs(ResizingMode.VerticalBottom, rc_bm.Cursor, e.StylusDevice.Id, false);
            FireStartResizing(sizeArgs);
            e.Handled = true;
        }

        void rc_rm_StylusDown(object sender, StylusDownEventArgs e)
        {
            if (e.StylusDevice == null)
                return;

            StartResizingEventArgs sizeArgs = new StartResizingEventArgs(ResizingMode.HorizontalRight, rc_rm.Cursor, e.StylusDevice.Id, false);
            FireStartResizing(sizeArgs);
            e.Handled = true;
        }

        void rc_rt_StylusDown(object sender, StylusDownEventArgs e)
        {
            if (e.StylusDevice == null)
                return;

            StartResizingEventArgs sizeArgs = new StartResizingEventArgs(ResizingMode.BothRT, rc_rt.Cursor, e.StylusDevice.Id, false);
            FireStartResizing(sizeArgs);
            e.Handled = true;
        }

        void rc_tm_StylusDown(object sender, StylusDownEventArgs e)
        {
            if (e.StylusDevice == null)
                return;

            StartResizingEventArgs sizeArgs = new StartResizingEventArgs(ResizingMode.VerticalTop, rc_tm.Cursor, e.StylusDevice.Id, false);
            FireStartResizing(sizeArgs);
            e.Handled = true;
        }

        void rc_lt_StylusDown(object sender, StylusDownEventArgs e)
        {
            if (e.StylusDevice == null)
                return;

            StartResizingEventArgs sizeArgs = new StartResizingEventArgs(ResizingMode.BothLT, rc_lt.Cursor, e.StylusDevice.Id, false);
            FireStartResizing(sizeArgs);
            e.Handled = true;
        }

        void rc_lm_StylusDown(object sender, StylusDownEventArgs e)
        {
            if (e.StylusDevice == null)
                return;

            StartResizingEventArgs sizeArgs = new StartResizingEventArgs(ResizingMode.HorizontalLeft, rc_lm.Cursor, e.StylusDevice.Id, false);
            FireStartResizing(sizeArgs);
            e.Handled = true;
        }

        void rc_rb_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.StylusDevice != null)
                return;

            if (e.ChangedButton == MouseButton.Left)
            {
                StartResizingEventArgs sizeArgs = new StartResizingEventArgs(ResizingMode.BothRB, rc_rb.Cursor, int.MinValue, true);
                FireStartResizing(sizeArgs);
                e.Handled = true;
            }
        }

        void rc_bm_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.StylusDevice != null)
                return;

            if (e.ChangedButton == MouseButton.Left)
            {
                StartResizingEventArgs sizeArgs = new StartResizingEventArgs(ResizingMode.VerticalBottom, rc_bm.Cursor, int.MinValue, true);
                FireStartResizing(sizeArgs);
                e.Handled = true;
            }
        }

        void rc_lb_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.StylusDevice != null)
                return;

            if (e.ChangedButton == MouseButton.Left)
            {
                StartResizingEventArgs sizeArgs = new StartResizingEventArgs(ResizingMode.BothLB, rc_lb.Cursor, int.MinValue, true);
                FireStartResizing(sizeArgs);
                e.Handled = true;
            }
        }

        void rc_rm_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.StylusDevice != null)
                return;

            if (e.ChangedButton == MouseButton.Left)
            {
                StartResizingEventArgs sizeArgs = new StartResizingEventArgs(ResizingMode.HorizontalRight, rc_rm.Cursor, int.MinValue, true);
                FireStartResizing(sizeArgs);
                e.Handled = true;
            }
        }

        void rc_rt_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.StylusDevice != null)
                return;

            if (e.ChangedButton == MouseButton.Left)
            {
                StartResizingEventArgs sizeArgs = new StartResizingEventArgs(ResizingMode.BothRT, rc_rt.Cursor, int.MinValue, true);
                FireStartResizing(sizeArgs);
                e.Handled = true;
            }
        }

        void rc_tm_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.StylusDevice != null)
                return;

            if (e.ChangedButton == MouseButton.Left)
            {
                StartResizingEventArgs sizeArgs = new StartResizingEventArgs(ResizingMode.VerticalTop, rc_tm.Cursor, int.MinValue, true);
                FireStartResizing(sizeArgs);
                e.Handled = true;
            }
        }

        void rc_lt_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.StylusDevice != null)
                return;

            if (e.ChangedButton == MouseButton.Left)
            {
                StartResizingEventArgs sizeArgs = new StartResizingEventArgs(ResizingMode.BothLT, rc_lt.Cursor, int.MinValue, true);
                FireStartResizing(sizeArgs);
                e.Handled = true;
            }
        }

        void rc_lm_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.StylusDevice != null)
                return;

            if (e.ChangedButton == MouseButton.Left)
            {
                StartResizingEventArgs sizeArgs = new StartResizingEventArgs(ResizingMode.HorizontalLeft, rc_lm.Cursor, int.MinValue, true);
                FireStartResizing(sizeArgs);
                e.Handled = true;
            }
        }

        private static void OnIsSelectedChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            int current_index = Canvas.GetZIndex((UIElement)d);
            if (!(bool)e.NewValue)
                Canvas.SetZIndex((UIElement)d, ((ObjectSelectionMarker)d).OriginalZIndex);
            else
            {
                ((ObjectSelectionMarker)d).OriginalZIndex = current_index;
                Canvas.SetZIndex((UIElement)d, current_index + 1);
            }
        }

        private static void OnUsesCustomGrippersChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is ObjectSelectionMarker)
            {
                ((ObjectSelectionMarker)d).UpdateCustomGripperSetting((bool)e.NewValue);
            }
        }

        #endregion

        #region Properties

        public object AssociatedPointObject
        {
            get;
            set;
        }

        public Decorator ChildVisual
        {
            get { return _child_visual; }
        }

        public bool IsResizing
        {
            get { return _is_resizing; }
        }

        public Cursor ResizingCursor
        {
            get { return _resizing_cursor; }
        }

        public ResizingMode ResizeMode
        {
            get { return _resize_mode; }
        }

        private int OriginalZIndex
        {
            get;
            set;
        }

        #endregion

        #region Dependency Properties
        public bool CanResizeX
        {
            get { return (bool)GetValue(CanResizeXProperty); }
            set { SetValue(CanResizeXProperty, value); }
        }

        public static readonly DependencyProperty CanResizeXProperty =
            DependencyProperty.Register("CanResizeX", typeof(bool), typeof(ObjectSelectionMarker), new UIPropertyMetadata(false));


        public bool CanResizeY
        {
            get { return (bool)GetValue(CanResizeYProperty); }
            set { SetValue(CanResizeYProperty, value); }
        }

        public static readonly DependencyProperty CanResizeYProperty =
            DependencyProperty.Register("CanResizeY", typeof(bool), typeof(ObjectSelectionMarker), new UIPropertyMetadata(false));


        public bool IsSelected
        {
            get { return (bool)GetValue(IsSelectedProperty); }
            set
            {
                SetValue(IsSelectedProperty, value);
            }
        }

        public static readonly DependencyProperty IsSelectedProperty =
            DependencyProperty.Register("IsSelected", typeof(bool), typeof(ObjectSelectionMarker),
                                        new UIPropertyMetadata(false, new PropertyChangedCallback(OnIsSelectedChanged)));

        public Style GripperStyle
        {
            get { return (Style)GetValue(GripperStyleProperty); }
            set { SetValue(GripperStyleProperty, value); }
        }

        public static readonly DependencyProperty GripperStyleProperty =
            DependencyProperty.Register("GripperStyle", typeof(Style), typeof(ObjectSelectionMarker), new UIPropertyMetadata(null));



        public bool IsOutOfXRange
        {
            get
            {
                return (bool)GetValue(IsOutOfXRangeProperty);
            }
            set { SetValue(IsOutOfXRangeProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsOutOfXRange.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsOutOfXRangeProperty =
            DependencyProperty.Register("IsOutOfXRange", typeof(bool), typeof(ObjectSelectionMarker), new UIPropertyMetadata(false));




        //public Visibility MoveGripperVisibility
        //{
        //    get { return (Visibility)GetValue(MoveGripperVisibilityProperty); }
        //    set { SetValue(MoveGripperVisibilityProperty, value); }
        //}

        //// Using a DependencyProperty as the backing store for MoveGripperVisibility.  This enables animation, styling, binding, etc...
        //public static readonly DependencyProperty MoveGripperVisibilityProperty =
        //    DependencyProperty.Register("MoveGripperVisibility", typeof(Visibility), typeof(ObjectSelectionMarker), new UIPropertyMetadata(System.Windows.Visibility.Collapsed));




        public bool IsMoving
        {
            get { return (bool)GetValue(IsMovingProperty); }
            set { SetValue(IsMovingProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsMoving.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsMovingProperty =
            DependencyProperty.Register("IsMoving", typeof(bool), typeof(ObjectSelectionMarker), new UIPropertyMetadata(false));



        public bool CanMove
        {
            get { return (bool)GetValue(CanMoveProperty); }
            set { SetValue(CanMoveProperty, value); }
        }

        // Using a DependencyProperty as the backing store for CanMove.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CanMoveProperty =
            DependencyProperty.Register("CanMove", typeof(bool), typeof(ObjectSelectionMarker), new UIPropertyMetadata(false));


        ///// <summary>
        /////  a 4-dimensional array indication whether the direction is in dock mode or not for the directions left, top, right, bottom
        ///// </summary>
        //public bool[] Dock
        //{
        //    get { return (bool[])GetValue(DockProperty); }
        //    set { SetValue(DockProperty, value); }
        //}

        //// Using a DependencyProperty as the backing store for Dock.  This enables animation, styling, binding, etc...
        //public static readonly DependencyProperty DockProperty =
        //    DependencyProperty.Register("Dock", typeof(bool[]), typeof(ObjectSelectionMarker), new UIPropertyMetadata(new bool[]{false, false, false, false}));



        public bool DockLeft
        {
            get { return (bool)GetValue(DockLeftProperty); }
            set { SetValue(DockLeftProperty, value); }
        }

        // Using a DependencyProperty as the backing store for DockLeft.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DockLeftProperty =
            DependencyProperty.Register("DockLeft", typeof(bool), typeof(ObjectSelectionMarker), new UIPropertyMetadata(false));



        public bool DockTop
        {
            get { return (bool)GetValue(DockTopProperty); }
            set { SetValue(DockTopProperty, value); }
        }

        // Using a DependencyProperty as the backing store for DockTop.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DockTopProperty =
            DependencyProperty.Register("DockTop", typeof(bool), typeof(ObjectSelectionMarker), new UIPropertyMetadata(false));



        public bool DockRight
        {
            get { return (bool)GetValue(DockRightProperty); }
            set { SetValue(DockRightProperty, value); }
        }

        // Using a DependencyProperty as the backing store for DockRight.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DockRightProperty =
            DependencyProperty.Register("DockRight", typeof(bool), typeof(ObjectSelectionMarker), new UIPropertyMetadata(false));



        public bool DockBottom
        {
            get { return (bool)GetValue(DockBottomProperty); }
            set { SetValue(DockBottomProperty, value); }
        }

        // Using a DependencyProperty as the backing store for DockBottom.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DockBottomProperty =
            DependencyProperty.Register("DockBottom", typeof(bool), typeof(ObjectSelectionMarker), new UIPropertyMetadata(false));





        public bool UsesCustomGrippers
        {
            get { return (bool)GetValue(UsesCustomGrippersProperty); }
            set
            {
                SetValue(UsesCustomGrippersProperty, value);
            }
        }

        public static readonly DependencyProperty UsesCustomGrippersProperty =
            DependencyProperty.Register("UsesCustomGrippers", typeof(bool), typeof(ObjectSelectionMarker),
                                        new UIPropertyMetadata(false, new PropertyChangedCallback(OnUsesCustomGrippersChanged)));

        #endregion

        #region Events

        public event EventHandler<StartResizingEventArgs> StartResizing;
        public event EventHandler<CustomGripperQueryEventArgs> QueryCustomGrippers;
        #endregion

        #region Routed Events
        #endregion

        #region Attributes

        private Rectangle rc_lm;
        private Rectangle rc_lt;
        private Rectangle rc_tm;
        private Rectangle rc_rt;
        private Rectangle rc_rm;
        private Rectangle rc_lb;
        private Rectangle rc_bm;
        private Rectangle rc_rb;

        private Dictionary<FrameworkElement, GripperPosition> _custom_grippers = new Dictionary<FrameworkElement, GripperPosition>();

        private Decorator _child_visual = null;
        private bool _is_resizing = false;
        private Cursor _resizing_cursor = null;
        private ResizingMode _resize_mode = ResizingMode.None;

        private Rectangle _rectangle_dock_bottom;
        private Rectangle _rectangle_dock_left;
        private Rectangle _rectangle_dock_right;

        #endregion

        #region Tests
        #endregion


    }
}
