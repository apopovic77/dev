﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Logicx.DiagramLib.Controls;

namespace Logicx.DiagramLib.DiagramObjects.Objects
{
    public abstract class VerticalStrechedPointObject<Tx, Ty> : PointObject<Tx, Ty>
    {
        #region Construction and Initialization
        /// <summary>
        /// Constructor of the class
        /// </summary>
        /// <param name="x_value"></param>
        public VerticalStrechedPointObject(Tx x_value)
            : base(x_value, default(Ty))
        {

        }
        #endregion

        #region Operations
        protected override void OnUpdateVisualBindings()
        {
            base.OnUpdateVisualBindings();
            if (ObjectVisual == null)
                return;

            // always begins at 0
            BindingOperations.ClearBinding(ObjectVisual, Canvas.TopProperty);
            Canvas.SetTop(ObjectVisual, 0);

            //MultiBinding top_binding = new MultiBinding();

            //Binding y_top_binding = new Binding("(Canvas.Top)");
            //y_top_binding.Source = this;
            //top_binding.Bindings.Add(y_top_binding);

            //Binding y_height_binding = new Binding("ActualHeight");
            //y_height_binding.Source = this;
            //top_binding.Bindings.Add(y_height_binding);



            // always uses full height
            BindingOperations.ClearBinding(ObjectVisual, FrameworkElement.HeightProperty);

            Binding full_height = new Binding("ActualHeight");
            //full_height.RelativeSource = new RelativeSource(RelativeSourceMode.FindAncestor, typeof(DiagramBase<Tx, Ty>), 1);
            full_height.RelativeSource = new RelativeSource(RelativeSourceMode.FindAncestor, typeof(Canvas), 1);
            ObjectVisual.SetBinding(FrameworkElement.HeightProperty, full_height);

            //BindingOperations.ClearBinding(ObjectVisual, FrameworkElement.WidthProperty);

            //Binding width_binding = new Binding("XAxisControl.ScaleUnitSize");
            //width_binding.RelativeSource = DiagramBindingSource;
            //ObjectVisual.SetBinding(FrameworkElement.WidthProperty, width_binding);
        }
        #endregion

        #region Properties
        public override bool CanResizeY
        {
            get { return false; }
        }
        #endregion
    }
}
