﻿using System;
using System.Collections.Generic;
using System.Text;
using MathLib;

namespace Logicx.Geo.Geometries
{
    public enum PolygonEndings
    {
        Normal = 0,
        /// <summary>
        /// runder abschluss
        /// </summary>
        Round = 1,
        /// <summary>
        /// linie schliesst mit einem querbalken ab
        /// </summary>
        Hammer = 2,

        Triangle = 3
    }

    /// <summary>
    /// line bedeutet hier das das polygon auf basis eines 
    /// linien elements erstellt wird bzw. wurde
    /// </summary>
    public class LinePolygon : SimplePolygon
    {
        public LinePolygon(SimpleLine base_line, float line_thickness)
        {
            _index_mode = IndexMode.List;

            _back_ending_addedpoints_index = -1;
            _front_ending_addedpoints_index = -1;
            _hammer_thickness = -1;
            _front_ending_style = PolygonEndings.Normal;
            _back_ending_style = PolygonEndings.Normal;
            _max_points_to_add = 5;
          
            _vertices_list = base_line.GetSimplePolygon(line_thickness).Vertices;
        }

        public LinePolygon(List<Vector2f> polygon_vertices) :base(polygon_vertices)
        {
            _index_mode = IndexMode.List;

            _back_ending_addedpoints_index = -1;
            _front_ending_addedpoints_index = -1;
            _hammer_thickness = -1;
            _front_ending_style = PolygonEndings.Normal;
            _back_ending_style = PolygonEndings.Normal;
            _max_points_to_add = 5;
        }

        public void AddHammerFrontEnding()
        {
            AddHammerFrontEnding(-1);
        }
        public void AddHammerFrontEnding(float hammer_thickness)
        {
            _hammer_thickness = hammer_thickness;
            AddFrontEnding(PolygonEndings.Hammer);
        }
        public void AddRoundFrontEnding(int max_additional_points)
        {
            _max_points_to_add = max_additional_points;
            AddFrontEnding(PolygonEndings.Round);
        }
        public void AddTriangleEnding()
        {
            _max_points_to_add = 1;
            AddEnding(PolygonEndings.Triangle, Count - 2, ref _front_ending_points_to_delete, ref _front_ending_addedpoints_index);
            AddEnding(PolygonEndings.Triangle, 1, ref _front_ending_points_to_delete, ref _front_ending_addedpoints_index);
        }
        /// <summary>
        /// Es wird ein neuer FrontEnding Style gesetzt. Falls ein alter vorhanden ist, wird dieser aus der bestehenden Liste entfernt
        /// und anschließend der neue gesetzt
        /// </summary>
        /// <param name="front_ending_style"></param>
        private void AddFrontEnding(PolygonEndings front_ending_style)
        {
            _front_ending_style = front_ending_style;
            DeleteFrontEndingPoints();
            AddEnding(_front_ending_style, Count - 2, ref _front_ending_points_to_delete, ref _front_ending_addedpoints_index);
        }
        
        /// <summary>
        /// Löscht alle Punkte, die durch ein vorheriges AddFrontEnding der Vektorenliste hinzugefügt wurden
        /// </summary>
        private void DeleteFrontEndingPoints()
        {
            if (_index_mode == IndexMode.Array)
                throw new Exception("Poly is readlonly");

            if(_front_ending_addedpoints_index!=-1)
            {
                for(int i=0; i<_front_ending_points_to_delete; i++)
                {
                    _vertices_list.RemoveAt(Count - 2);
                }
                _front_ending_addedpoints_index = -1;
            }
        }
        /// <summary>
        /// Abhänging von der Linethickness wird eine hammer_thichness berechnet
        /// </summary>
        public void AddHammerBackEnding()
        {
            AddHammerBackEnding(-1);
        }
        public void AddHammerBackEnding(float hammer_thickness)
        {
            _hammer_thickness = hammer_thickness;
            AddBackEnding(PolygonEndings.Hammer);
        }
        public void AddRoundBackEnding(int max_additional_points)
        {
            _max_points_to_add = max_additional_points;
            AddBackEnding(PolygonEndings.Round);
        }
        /// <summary>
        /// Es wird ein neuer BackEnding Style gesetzt. Falls ein alter vorhanden ist, wird dieser aus der bestehenden Liste entfernt
        /// und anschließend der neue gesetzt
        /// </summary>
        /// <param name="back_ending_style"></param>
        private void AddBackEnding(PolygonEndings back_ending_style)
        {
            _back_ending_style = back_ending_style;
            DeleteBackEndingPoints();
            int start_index;
            // Wenn an der Spitze bereits Vektoren für den Abschluss hinzugefügt wurden, 
            // verschiebt sich die Mitte und muss daher bei der Indexberechnung berücksichtigt werden
            if (_front_ending_addedpoints_index != -1)
                start_index = (Count - _front_ending_points_to_delete ) / 2 - 1;
            else
                start_index = (Count)/2 - 1;
            AddEnding(back_ending_style, start_index, ref _back_ending_points_to_delete, ref _back_ending_addedpoints_index);
        }

        /// <summary>
        /// Löscht alle Punkte, die durch ein vorheriges AddBackEnding der Vektorenliste hinzugefügt wurden
        /// </summary>
        private void DeleteBackEndingPoints()
        {
            if (_index_mode == IndexMode.Array)
                throw new Exception("Poly is readlonly");

            if(_back_ending_addedpoints_index!=-1)
            {
                for (int i = 0; i < _back_ending_points_to_delete; i++)
                {
                    _vertices_list.RemoveAt(_back_ending_addedpoints_index);
                }
                _back_ending_addedpoints_index = -1;
            }
        }

        /// <summary>
        /// An einer bestimmten Stelle, angegeben durch den startindex, wird ein bestimmter Abschluss in Form von Punkten
        /// der _vertecis Liste hinzugefügt
        /// </summary>
        /// <param name="ending_style"></param>
        /// <param name="startindex"></param>
        /// <param name="points_to_delete">dient zur Speicherung der neu hinzugefügten Punkte. Notwendig zur späteren Löschung dieser Punkte</param>
        ///  <param name="start_adding_index"></param>
        private void AddEnding(PolygonEndings ending_style, int startindex, ref int points_to_delete, ref int start_adding_index)
        {
            if (_index_mode == IndexMode.Array)
                throw new Exception("Poly is readlonly");

            if(ending_style == PolygonEndings.Normal)
            {
                return;
            }
            else if (ending_style == PolygonEndings.Round)
            {
                AddCirclePoints(_vertices_list, startindex, _max_points_to_add);
                points_to_delete = _max_points_to_add;
                start_adding_index = startindex + 1;
            }
            else if(ending_style == PolygonEndings.Hammer)
            {
                AddHammerPoints(_vertices_list, startindex,_hammer_thickness);
                start_adding_index = startindex + 1;
                points_to_delete = 4;
            }
            else if(ending_style == PolygonEndings.Triangle)
            {
                AddCirclePoints(_vertices_list, startindex, 1);
                points_to_delete = 1;
                start_adding_index = startindex + 1;
            }
        }


    
        #region Atttributes
        protected PolygonEndings _front_ending_style;
        protected PolygonEndings _back_ending_style;
        protected int _max_points_to_add;
        protected float _hammer_thickness;

        protected int _back_ending_addedpoints_index;
        /// <summary>
        /// Anzahl der Punkte, die am Ende gelöscht werden müssen, um ursprünglichen Zustand wieder zu bekommen
        /// </summary>
        protected int _back_ending_points_to_delete;
        protected int _front_ending_addedpoints_index;
        /// <summary>
        /// Anzahl der Punkte, die am Anfang gelöscht werden müssen, um ursprünglichen Zustand wieder zu bekommen
        /// </summary>
        protected int _front_ending_points_to_delete;
        #endregion

    }
}
