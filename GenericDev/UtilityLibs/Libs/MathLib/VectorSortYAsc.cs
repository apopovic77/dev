using System;
using System.Collections;

namespace MathLib
{
	/// <summary>
	/// 
	/// </summary>
	public class VectorSortYAsc: IComparer  
	{
		int IComparer.Compare( Object y1, Object y2 )  
		{
			double Y1 = ((Vector2d)y1).Y;
			double Y2 = ((Vector2d)y2).Y;
			if(Y1 < Y2)
				return -1;
			else if(Y1 == Y2)
				return 0;
			else
				return 1;
		}
	}
}
