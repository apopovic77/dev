﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using FDL.Library.Numeric;
using GeneticProgrammingLib;
using Logicx.Geo.Geometries;
using MathLib;
using Microsoft.CSharp;
using ShortestPathAgentsApp.GeneticAlgorithm;

namespace ShortestPathAgentsApp
{

    public class SmartAgent : Dna<Vector2d>
    {
        public SmartAgent()
        {
            _died_at_frame = SmartAgent.DnaGeneSize;
        }

        public override void CalcFitness()
        {
            _distance_to_target = (TargetPos - _shape_pos).GetLen();
            Fitness = (Math.Pow(((1 / _distance_to_target) * 1000), 2)) * _died_at_frame;
            //Fitness = _died_at_frame * _died_at_frame;
            FitnessProbality = 1 / _distance_to_target;
            //if (_is_dead)
            //{
            //    Fitness = Fitness * 0.1;
            //}
        }


        private Vector2d RotateRandom(Vector2d dir,int fov = 360)
        {
            //RandomNumber.Between(0, fov);
            double rot_angle = (_r.NextDouble() * fov) - (fov / 2.0);
            dir.RotateClockwise( rot_angle* Math.PI / 180);

            //Random r = new Random();
            //double rot_angle = (r.NextDouble() * fov) - (fov/2.0);
            //dir.RotateClockwise(rot_angle * Math.PI / 180);
            return dir;
        }

        public override void InitializeRandomGen()
        {
            _genes = new List<Vector2d>();
            _genes.Add(Vector2d.GetRandomDirectionVector(_r));
            for (int i = 1; i < DnaGeneSize; i++)
                _genes.Add(RotateRandom(_genes[i - 1], FovRotateAngleMax));
        }

        public override void Mutate(int i)
        {
            if (i == 0)
            {
                _genes[0] = Vector2d.GetRandomDirectionVector(_r);
                return;
            }
            _genes[i] = RotateRandom(_genes[i-1], FovRotateAngleMax);
        }

        public double Width
        {
            get { return Shape.Width; }
        }

        public double Height
        {
            get { return Shape.Height; }
        }

        #region WPF 
        public Vector2d ShapePos
        {
            get
            {
                return new Vector2d(Canvas.GetLeft(Shape), Canvas.GetTop(Shape));
            }
            set
            {
                _shape_pos = value;
                Canvas.SetLeft(Shape, value.X);
                Canvas.SetTop(Shape, value.Y);
            }
        }

        public Vector2d ShapeDir
        {
            get { return Vector2d.GetVector2dFromAngleDegree(((RotateTransform)Shape.RenderTransform).Angle); }
            set { ((RotateTransform)Shape.RenderTransform).Angle = value.GetAngleDegree(); }
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="with_wpf_render">die routine kann auch laufen ohne das dann was dargestellt wird, also sie kann ausgeführt werden auch ohne wpf</param>
        /// <param name="thread_safe"></param>
        public void Render(bool with_wpf_render = true, bool thread_safe = false)
        {
            if (_disposed)
                return;

            if (thread_safe)
                Monitor.Enter(Wpf.Canvas);

            if (!_is_shape_added)
            {
                _is_shape_added = true;
                ShapePos = InitialPos;
                ShapeDir = _genes[0];

                if (with_wpf_render)
                {
                    Wpf.Canvas.Children.Add(Shape);
                    if (IsBest)
                        Shape.Fill = Brushes.Yellow;
                }
            }

            Vector2d new_dir = GetNextDir();
            Vector2d curr_pos = _shape_pos;
            Vector2d new_pos = _shape_pos + new_dir * SmartAgentConfig.AgentVelocity.Value;
            ShapeDir = new_dir;
            ShapePos = new_pos;

            //check for collision
            IsDead = CheckForCollision(new_pos);
            if (IsDead)
                _died_at_frame = _active_frame;

            if (with_wpf_render)
            {
                //draw a line to see the movement
                Line l = new Line();
                l.X1 = curr_pos.X;
                l.Y1 = curr_pos.Y;
                l.Stroke = new SolidColorBrush(Color.FromArgb(0x55, 0xFF, 0xFF, 0xFF));
                l.StrokeThickness = 1;
                l.X2 = new_pos.X;
                l.Y2 = new_pos.Y;
                if (IsBest)
                {
                    l.Stroke = Brushes.Yellow;
                    l.StrokeThickness = 3;
                    Wpf.Canvas.Children.Add(l);
                }
                else
                {
                    Wpf.Bmp.DrawLineBresenham((int)Math.Round(curr_pos.X), (int)Math.Round(curr_pos.Y), (int)Math.Round(new_pos.X), (int)Math.Round(new_pos.Y), Color.FromArgb(0xBB, 0xFF, 0xFF, 0xFF));
                    
                    //Wpf.Canvas.Children.Add(l);
                }
            }

            if (thread_safe)
                Monitor.Exit(Wpf.Canvas);
        }

        protected Vector2d GetNextDir()
        {
            Vector2d curr_dir = ShapeDir;
            
            double next_angle = _dir.GetAngleDegree();
            double curr_angle = curr_dir.GetAngleDegree();

            double rot_full = Vector2d.CircularDist(next_angle, curr_angle);
            double rot_step = rot_full * SmartAgentConfig.AgentRotationVelocityFactor.Value;
            Vector2d.RotationDirection rot_direction = Vector2d.TurnDir(next_angle, curr_angle);

            if (rot_direction == Vector2d.RotationDirection.CounterClockwise || rot_direction == Vector2d.RotationDirection.Undefined)
                curr_dir.RotateCounterClockwise(rot_step);
            else
                curr_dir.RotateClockwise(rot_step);

            return curr_dir;
        }

        protected bool CheckForCollision(Vector2d new_pos)
        {

            foreach (var child in Wpf.Canvas.Children)
            {
                if (!(child is Rectangle))
                    continue;

                Rectangle rect = (Rectangle)child;
                if (!(rect.Tag != null && rect.Tag is RectangleObstacle))
                    continue;

                RectangleObstacle rect_obstacle = (RectangleObstacle)rect.Tag;
                Vector2d top_left = new Vector2d(rect_obstacle.X, rect_obstacle.Y);
                Vector2d bottom_right = top_left;
                bottom_right.X += rect_obstacle.Width;
                bottom_right.Y += rect_obstacle.Height;
                BoundingBox bbox = new BoundingBox(top_left, bottom_right);

                if (bbox.PointInside(new_pos))
                    return true;

            }

            return false;
        }

        public void ApplyForce(double force)
        {
            _acc += force;
        }

        public override void SimulationUpdate(int frame)
        {
            if (frame < Genes.Count)
            {
                _dir = Genes[frame];
                //Velocity += _acc;
                _active_frame = frame;
                //_shape_pos += _dir * Velocity;
            }
        }

        public override void Dispose()
        {
            base.Dispose();
            RemoveShape();
        }

        public void RemoveShape()
        {
            if (_is_shape_added)
            {
                lock (Wpf.Canvas)
                {
                    Wpf.Canvas.Children.Remove(_shape);
                }
            }

            _shape = null;
            _is_shape_added = false;
        }

        public Shape Shape
        {
            get
            {
                if (_shape == null)
                {
                    Path body = Triangle.CreateTriangle();
                    body.Width = 20;
                    body.Height = 10;
                    body.Fill = Brushes.White;
                    //body.Stroke = Brushes.Black;
                    body.StrokeThickness = 0;
                    body.RenderTransform = new RotateTransform(0, 7 / 2, 5 / 2);

                    _shape = body;
                }
                return _shape;
            }
        }
        #endregion

        public override string ToString()
        {
            return "Id " + Id + " DistanceToTarget: " + _distance_to_target + " DiedAtFrame: " + _died_at_frame + " Fitness: " + Fitness;
        }


        #region Attributes
        private Shape _shape;
        private Vector2d _shape_pos;
        private Vector2d _dir;
        private bool _is_shape_added;
        private double _distance_to_target;
        private double _acc;
        
        public static Vector2d InitialPos;
        public static Vector2d TargetPos = new Vector2d(600, 600);
        public static int FovRotateAngleMax = 360;
        #endregion


    }
}

