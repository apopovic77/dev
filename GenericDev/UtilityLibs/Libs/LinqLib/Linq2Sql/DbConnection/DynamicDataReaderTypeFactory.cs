﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;
using System.Threading;

namespace LinqLib.Linq2Sql.DbConnection
{
    public class DynamicDataReaderTypeFactory
    {
        #region dynamic type and instance generation

        public static IaDbDataReader GetThreadDependentReaderInstance(SqlDataReader sql_reader, IaDbCommand command)
        {
            int nThreadId = 0;

            if (Thread.CurrentThread != null)
            {
                nThreadId = Thread.CurrentThread.GetHashCode();
            }

            return GetDynamicReaderInstance("dynIaDbReader" + "_" + nThreadId.ToString(), sql_reader, command);
        }

        public static IaDbDataReader GetDynamicReaderInstance(SqlDataReader sql_reader, IaDbCommand command)
        {
            return GetDynamicReaderInstance("dynIaDbReader", sql_reader, command);
        }

        public static IaDbDataReader GetThreadDependentReaderInstance(string type_class_name, SqlDataReader sql_reader, IaDbCommand command)
        {
            int nThreadId = 0;

            if (Thread.CurrentThread != null)
            {
                nThreadId = Thread.CurrentThread.GetHashCode();
            }

            return GetDynamicReaderInstance(type_class_name + "_" + nThreadId.ToString(), sql_reader, command);
        }

        public static IaDbDataReader GetDynamicReaderInstance(string type_class_name, SqlDataReader sql_reader, IaDbCommand command)
        {
            Type readerType = GetDynamicReaderType(type_class_name);

            if(readerType != null)
            {
                object objInstance = Activator.CreateInstance(readerType, new object[] {sql_reader, command});

                if (objInstance != null && objInstance is IaDbDataReader)
                    return objInstance as IaDbDataReader;
            }

            return null;
        }

        public static Type GetThreadDependentReaderType(string type_class_name)
        {
            int nThreadId = 0;
    
            if(Thread.CurrentThread != null)
            {
                nThreadId = Thread.CurrentThread.GetHashCode();
            }

            return GetDynamicReaderType(type_class_name + "_" + nThreadId.ToString());
        }

        public static Type GetDynamicReaderType(string type_class_name)
        {
            return GenerateDynamicDataReaderType(type_class_name);
        }


        private static Type GenerateDynamicDataReaderType(string typeName)
        {
            EnsureModuleBuilder();

            Type typeCheck = _factory_module_builder.GetType(typeName, false, false);

            if(typeCheck == null)
            {
                // base type
                Type baseType = typeof(IaDbDataReader);
                // constructor parameter types
                Type[] ctorParamTypes = new Type[] {typeof (SqlDataReader), typeof (IaDbCommand)};

                ConstructorInfo baseCtor = baseType.GetConstructor(ctorParamTypes);

                // create the dynamic data reader type and set the base class to IaDbDataReader
                TypeBuilder tb = _factory_module_builder.DefineType(typeName, TypeAttributes.Public, baseType);
                
                // create the constructors
                ConstructorBuilder c1 = tb.DefineConstructor(MethodAttributes.Public,
                                                             CallingConventions.Standard,
                                                             ctorParamTypes);
                // IL code generator for the constructor
                ILGenerator ctorIL = c1.GetILGenerator();
                // first constructor argument = this instance of the new object
                ctorIL.Emit(OpCodes.Ldarg_0);
                // push the 2 constructor arguments on the stack
                ctorIL.Emit(OpCodes.Ldarg_1);
                ctorIL.Emit(OpCodes.Ldarg_2);
                // call the base constructor
                ctorIL.Emit(OpCodes.Call, baseCtor);
                // return from the constructor
                ctorIL.Emit(OpCodes.Ret);

                // generate new dynamic type
                typeCheck = tb.CreateType();
            }

            return typeCheck;
        }
        #endregion


        #region internal helper methods
        private static void EnsureModuleBuilder()
        {
            if (_factory_module_builder != null)
                return;

            // 1. Get Application domain reference
            AppDomain ad = AppDomain.CurrentDomain;
            // 2. create new dynamic temporary assembly
            AssemblyName an = new AssemblyName();
            an.Name = "dynTmp_IaDataLib";
            // 3. load the dynamic assembl yinto the current application domain
            AssemblyBuilder ab = ad.DefineDynamicAssembly(
             an, AssemblyBuilderAccess.Run);
            // 4. create dynamic assembly module
            _factory_module_builder = ab.DefineDynamicModule("dynTmp_IaDataLibModule");
        }
        #endregion

        private static ModuleBuilder _factory_module_builder = null;
    }
}
