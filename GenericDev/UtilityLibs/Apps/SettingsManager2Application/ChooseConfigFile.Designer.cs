﻿namespace SettingsManagerApplication
{
    partial class ChooseConfigFile
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lb_configfiles = new System.Windows.Forms.ListBox();
            this.b_ok = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lb_configfiles
            // 
            this.lb_configfiles.FormattingEnabled = true;
            this.lb_configfiles.Location = new System.Drawing.Point(13, 4);
            this.lb_configfiles.Name = "lb_configfiles";
            this.lb_configfiles.Size = new System.Drawing.Size(616, 147);
            this.lb_configfiles.TabIndex = 0;
            // 
            // b_ok
            // 
            this.b_ok.Location = new System.Drawing.Point(536, 160);
            this.b_ok.Name = "b_ok";
            this.b_ok.Size = new System.Drawing.Size(93, 27);
            this.b_ok.TabIndex = 1;
            this.b_ok.Text = "OK";
            this.b_ok.UseVisualStyleBackColor = true;
            this.b_ok.Click += new System.EventHandler(this.b_ok_Click);
            // 
            // ChooseConfigFile
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(641, 197);
            this.Controls.Add(this.b_ok);
            this.Controls.Add(this.lb_configfiles);
            this.Name = "ChooseConfigFile";
            this.Text = "Welches Config File soll bearbeitet werden?";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox lb_configfiles;
        private System.Windows.Forms.Button b_ok;
    }
}