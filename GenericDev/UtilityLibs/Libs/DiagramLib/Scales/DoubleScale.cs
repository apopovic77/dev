﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Logicx.DiagramLib.Util;

namespace Logicx.DiagramLib.Scales
{
    internal class DoubleScale : ScaleType
    {

        #region Constants

        private const long MIN_DOUBLE_UNIT = 1;
        private const long MIN_DOUBLE_UNITS_QUARTER = 4;
        private const long MIN_DOUBLE_UNITS_HALF = 7;
        private const long MIN_DOUBLE_UNITS_ONE = 10;
        private const long MIN_DOUBLE_UNITS_TWO = 20;
        private const long MIN_DOUBLE_UNITS_FIVE = 50;
        private const long MIN_DOUBLE_UNITS_TEN = 90;
        private const long MIN_DOUBLE_UNITS_FIFTY = 500;
        private const long MIN_DOUBLE_UNITS_HUNDRED = 1000;

        private const double TEXT_LOD_1_10 = 0.1;
        private const double TEXT_LOD_1_4 = 0.25;
        private const double TEXT_LOD_1_2 = 0.5;
        private const double TEXT_LOD_1 = 1;
        private const double TEXT_LOD_2 = 2;
        private const double TEXT_LOD_5 = 5;
        private const double TEXT_LOD_10 = 10;
        private const double TEXT_LOD_50 = 50;
        private const double TEXT_LOD_100 = 100;

        private readonly double[] _text_lods =
        {
            TEXT_LOD_100,
            TEXT_LOD_50,
            TEXT_LOD_10,
            TEXT_LOD_5,
            TEXT_LOD_2,
            TEXT_LOD_1,
            TEXT_LOD_1_2,
            TEXT_LOD_1_4,
            TEXT_LOD_1_10
        };

        private const int MIN_UNIT_DISTANCE = 50;

        #endregion

        #region Construction and Initialization
        #endregion

        #region Operations

        public override void Init(double size, bool generate_always)
        {
            double old_text_level = _text_lod;

            double start = _double_scale_control.Config.Start;
            double end = _double_scale_control.Config.End;

            int num_units = (int)((end - start) / MIN_DOUBLE_UNIT);

            int lod_index;
            if (num_units >= MIN_DOUBLE_UNITS_HUNDRED)
                lod_index = 0;
            else if (num_units >= MIN_DOUBLE_UNITS_FIFTY)
                lod_index = 1;
            else if (num_units >= MIN_DOUBLE_UNITS_TEN)
                lod_index = 2;
            else if (num_units >= MIN_DOUBLE_UNITS_FIVE)
                lod_index = 3;
            else if (num_units >= MIN_DOUBLE_UNITS_TWO)
                lod_index = 4;
            else if (num_units >= MIN_DOUBLE_UNITS_ONE)
                lod_index = 5;
            else if (num_units >= MIN_DOUBLE_UNITS_HALF)
                lod_index = 6;
            else if (num_units >= MIN_DOUBLE_UNITS_QUARTER)
                lod_index = 7;
            else
                lod_index = 8;

            _text_lod = _text_lods[lod_index];
            double unit_size = GetUnitSize(_double_scale_control.Config.Orientation);
            while (unit_size < MIN_UNIT_DISTANCE)
            {
                lod_index--;
                if (lod_index < 0)
                    break;

                _text_lod = _text_lods[lod_index];
                unit_size = GetUnitSize(_double_scale_control.Config.Orientation);
            }

            _unit_offset = GetUnitOffset(_double_scale_control.Config.Orientation);

            if (generate_always || old_text_level != _text_lod)
            {
                _double_scale_control.ClearChildren();
                GenerateData();
                AddChildren();
            }
        }

        private void GenerateData()
        {
            _long_texts.Clear();
            _description_text = null;

            GenerateDayAndSubData();
        }

        private void GenerateDayAndSubData()
        {
            double start = _double_scale_control.Config.Start;
            double end = _double_scale_control.Config.End;

            string in_percent = _double_scale_control.InPercent ? " [%]" : "";
            _description_text = GeometryUtils.GenerateTextBlock(_double_scale_control.Config.Description + in_percent, TextStyleType.InfoText, typeof(DoubleScaleControl), _double_scale_control);
            if (_double_scale_control.Config.Orientation == Orientation.Vertical)
                _description_text.LayoutTransform = new RotateTransform(270);

            double increment = _text_lod;

            double rounded_start = RoundToTextLod(start);
            double start_min = start - rounded_start > 0.0 ? rounded_start + increment : rounded_start;
            double unit_start = RoundToTextLod(_unit_offset);

            double unit_size = GetUnitSize(_double_scale_control.Config.Orientation);
            double offset_unit = unit_start - _double_scale_control.Config.Start;
            double offset_first = start_min - _double_scale_control.Config.Start;

            if (_double_scale_control.Config.Orientation == Orientation.Horizontal)
            {
                //double current_position = _double_scale_control.Config.Padding.Left + offset_first * unit_size / _text_lod;
                double current_position = offset_first * unit_size / _text_lod;

                if (offset_unit != offset_first)
                {
                    string str = _double_scale_control.InPercent ? (start_min * 100).ToString() : start_min.ToString();
                    TextBlock b = GeometryUtils.GenerateTextBlock(str, TextStyleType.ScaleText, typeof(DoubleScaleControl), _double_scale_control);
                    _long_texts.Add(b);
                    //current_position = _double_scale_control.Config.Padding.Left + offset_unit * unit_size / _text_lod;
                    current_position = offset_unit * unit_size / _text_lod;
                }

                double rounded_end = RoundToTextLod(end);
                double upper = end - rounded_end == 0.0 ? rounded_end : rounded_end + increment;
                for (double i = unit_start; i < upper; i++)
                {
                    string str = _double_scale_control.InPercent ? (i * 100).ToString() : i.ToString();
                    TextBlock b = GeometryUtils.GenerateTextBlock(str, TextStyleType.ScaleText, typeof(DoubleScaleControl), _double_scale_control);
                    FormattedText ft = new FormattedText(b.Text, Thread.CurrentThread.CurrentCulture, FlowDirection.LeftToRight, new Typeface(TextBlock.FontStyleProperty.Name), 6, Brushes.Black);
                    if (current_position + ft.Width * 1.5 > _double_scale_control.GetRenderWidth())
                        break;

                    _long_texts.Add(b);
                    current_position += unit_size;
                }
            }
            else
            {
                //double current_position = _double_scale_control.Config.Padding.Bottom + offset_first * unit_size / _text_lod;
                double current_position = offset_first * unit_size / _text_lod;

                if (offset_unit != offset_first)
                {
                    string str = _double_scale_control.InPercent ? (start_min * 100).ToString() : start_min.ToString();
                    TextBlock b = GeometryUtils.GenerateTextBlock(str, TextStyleType.ScaleText, typeof(DoubleScaleControl), _double_scale_control);
                    _long_texts.Add(b);
                    //current_position = _double_scale_control.Config.Padding.Bottom + offset_unit * unit_size / _text_lod;
                    current_position = offset_unit * unit_size / _text_lod;
                }

                double rounded_end = RoundToTextLod(end);
                double upper = end - rounded_end == 0.0 ? rounded_end : rounded_end + increment;
                for (double i = unit_start; i < upper; i += increment)
                {
                    string str = _double_scale_control.InPercent ? (i * 100).ToString() : i.ToString();
                    TextBlock b = GeometryUtils.GenerateTextBlock(str, TextStyleType.ScaleText, typeof(DoubleScaleControl), _double_scale_control);
                    FormattedText ft = new FormattedText(b.Text, Thread.CurrentThread.CurrentCulture, FlowDirection.LeftToRight, new Typeface(TextBlock.FontStyleProperty.Name), 6, Brushes.Black);

                    //if (current_position + 5 + ft.Height * 1.5 > _double_scale_control.GetRenderHeight() + _double_scale_control.Config.Padding.Bottom)
                    if (current_position + 5 + ft.Height * 1.5 > _double_scale_control.GetRenderHeight())
                        break;

                    //Debug.WriteLine(GetHashCode() + ": " + i + " ADDED at " + (current_position + 5) + ", HEIGHT OF CANVAS " + _double_scale_control.GetRenderHeight() + ", text height " + ft.Height + ", NEXT POS " + (current_position + unit_size) + ", longtexts size " + _long_texts.Count);

                    _long_texts.Add(b);
                    current_position += unit_size;
                }
            }
        }

        private void AddChildren()
        {
            List<UIElement> elements = new List<UIElement>();

            elements.Add(_description_text);
            elements.AddRange(_long_texts.Cast<UIElement>());

            _double_scale_control.AddToChildren(elements);
        }

        public override void AlignData(Orientation orientation)
        {
            // this check is required on initial start because RenderHeigth is 0 and no texts will be added
            if (_long_texts.Count == 0)
                return;

            double start = _double_scale_control.Config.Start;
            double end = _double_scale_control.Config.End;

            double unit_size = GetUnitSize(_double_scale_control.Config.Orientation);

            int index = 0;
            double increment = _text_lod;
            double rounded_start = RoundToTextLod(start);
            double start_min = start - rounded_start > 0.0 ? rounded_start + increment : rounded_start;
            double unit_start = RoundToTextLod(_unit_offset);

            double offset_unit = unit_start - _double_scale_control.Config.Start;
            double offset_first = start_min - _double_scale_control.Config.Start;

            double centered_position = _double_scale_control.GetVisibleCenteredPosition();

            if (orientation == Orientation.Horizontal)
            {
                FormattedText ft_desc = new FormattedText(_description_text.Text, Thread.CurrentThread.CurrentCulture, FlowDirection.LeftToRight, new Typeface(TextBlock.FontStyleProperty.Name), 12, Brushes.Black);
                //Canvas.SetLeft(_description_text, _double_scale_control.Config.Padding.Left + (_double_scale_control.GetRenderWidth() - ft_desc.Width) / 2);
                //Canvas.SetBottom(_description_text, _double_scale_control.Config.Padding.Bottom);

                //double current_position = _double_scale_control.Config.Padding.Left + offset_first * unit_size / _text_lod;

                //Canvas.SetLeft(_description_text, (_double_scale_control.GetRenderWidth() - ft_desc.Width) / 2);
                Canvas.SetLeft(_description_text, centered_position - ft_desc.Width / 2);
                Canvas.SetBottom(_description_text, 0);

                double current_position = offset_first * unit_size / _text_lod;

                if (offset_unit != offset_first)
                {
                    TextBlock text_block = _long_texts.ToArray()[index++];
                    Canvas.SetLeft(text_block, current_position + 5);
                    //Canvas.SetTop(text_block, _double_scale_control.Config.Padding.Top + 10);
                    //current_position = _double_scale_control.Config.Padding.Left + offset_unit * unit_size / _text_lod;
                    Canvas.SetTop(text_block, 10);
                    current_position = offset_unit * unit_size / _text_lod;
                }

                double rounded_end = RoundToTextLod(end);
                double upper = end - rounded_end == 0.0 ? rounded_end : rounded_end + increment;
                for (double i = unit_start; i < upper; i++)
                {
                    TextBlock text_block = _long_texts.ToArray()[index++];

                    FormattedText ft = new FormattedText(text_block.Text, Thread.CurrentThread.CurrentCulture, FlowDirection.LeftToRight, new Typeface(TextBlock.FontStyleProperty.Name), 6, Brushes.Black);

                    if (current_position + ft.Width * 1.5 > _double_scale_control.GetRenderWidth())
                        break;

                    Canvas.SetLeft(text_block, current_position + 5);
                    //Canvas.SetTop(text_block, _double_scale_control.Config.Padding.Top + 10);
                    Canvas.SetTop(text_block, 10);

                    current_position += unit_size;
                }
            }
            else
            {
                //Canvas.SetLeft(_description_text, _double_scale_control.Config.Padding.Left);
                Canvas.SetLeft(_description_text, 0);
                FormattedText ft_desc = new FormattedText(_description_text.Text, Thread.CurrentThread.CurrentCulture, FlowDirection.LeftToRight, new Typeface(TextBlock.FontStyleProperty.Name), 12, Brushes.Black);
                //Canvas.SetBottom(_description_text, _double_scale_control.Config.Padding.Bottom + (_double_scale_control.GetRenderHeight() - ft_desc.Width) / 2);
                //Canvas.SetBottom(_description_text, (_double_scale_control.GetRenderHeight() - ft_desc.Width) / 2);
                Canvas.SetTop(_description_text, centered_position - ft_desc.Width / 2);

                double maxsize = GetMaxDescriptionSize(start, end);
                //double current_position = _double_scale_control.Config.Padding.Bottom + offset_first * unit_size / _text_lod;
                double current_position = offset_first * unit_size / _text_lod;

                if (offset_unit != offset_first)
                {
                    TextBlock text_block = _long_texts.ToArray()[index++];
                    FormattedText ft = new FormattedText(text_block.Text, Thread.CurrentThread.CurrentCulture, FlowDirection.LeftToRight, new Typeface(TextBlock.FontStyleProperty.Name), 6, Brushes.Black);

                    Canvas.SetBottom(text_block, current_position + 5);
                    //Canvas.SetLeft(text_block, _double_scale_control.Config.Padding.Left + 20 + maxsize - ft.Width / 2);
                    Canvas.SetLeft(text_block, 20 + maxsize - ft.Width / 2);

                    //current_position = _double_scale_control.Config.Padding.Bottom + offset_unit * unit_size / _text_lod;
                    current_position = offset_unit * unit_size / _text_lod;
                }

                double rounded_end = RoundToTextLod(end);
                double upper = end - rounded_end == 0.0 ? rounded_end : rounded_end + 1;
                for (double i = unit_start; i < upper; i += increment)
                {
                    TextBlock text_block = _long_texts.ToArray()[index++];
                    FormattedText ft = new FormattedText(text_block.Text, Thread.CurrentThread.CurrentCulture, FlowDirection.LeftToRight, new Typeface(TextBlock.FontStyleProperty.Name), 6, Brushes.Black);

                    Canvas.SetBottom(text_block, current_position + 5);
                    //Canvas.SetLeft(text_block, _double_scale_control.Config.Padding.Left + 20 + maxsize - ft.Width / 2);
                    Canvas.SetLeft(text_block, 20 + maxsize - ft.Width / 2);

                    //Debug.WriteLine(GetHashCode() + ": " + i + " positioned at " + (current_position + 5) + ", HEIGHT OF CANVAS " + _double_scale_control.GetRenderHeight() + ", text height " + ft.Height + ", NEXT POS " + (current_position + unit_size) + ", longtexts size " + _long_texts.Count);
                    current_position += unit_size;

                    //if (current_position + 5 + ft.Height * 1.5 > _double_scale_control.GetRenderHeight() + _double_scale_control.Config.Padding.Bottom)
                    if (current_position + 5 + ft.Height * 1.5 > _double_scale_control.GetRenderHeight())
                        break;
                }
            }
        }

        private double GetMaxDescriptionSize(double first, double last)
        {
            double max = 0;
            double increment = _text_lod;
            for (double i = RoundToTextLod(first); i < RoundToTextLod(last); i += increment)
            {
                FormattedText ft = new FormattedText(i.ToString(), Thread.CurrentThread.CurrentCulture, FlowDirection.LeftToRight, new Typeface(TextBlock.FontStyleProperty.Name), 10, Brushes.Black);
                if (ft.Width > max)
                    max = ft.Width;
            }

            return max;
        }

        public override double GetUnitSize(Orientation orientation)
        {
            double num_units = (_double_scale_control.Config.End - _double_scale_control.Config.Start) / _text_lod;

            if (orientation == Orientation.Horizontal)
                return _double_scale_control.GetRenderWidth() / num_units;

            return _double_scale_control.GetRenderHeight() / num_units;
        }

        public override double GetUnitOffset(Orientation orientation)
        {
            double start = _double_scale_control.Config.Start;
            double rounded_start = RoundToTextLod(start);
            double frac = start - rounded_start;
            double offset = start;

            double increment = _text_lod;
            double diff = RoundToTextLod(offset - frac);
            while (diff % _text_lod != 0.0)
            {
                offset += increment;
                diff = RoundToTextLod(offset - frac);
            }

            _unit_offset = offset;

            return _unit_offset;
        }

        public override double GetUnitOffset()
        {
            return _unit_offset;
        }

        public override List<double> GetGridLinePositions()
        {
            List<double> positions = new List<double>();

            double unit_size = GetUnitSize(_double_scale_control.Config.Orientation);
            double start = _double_scale_control.Config.Start;
            double increment = _text_lod;
            double rounded_start = RoundToTextLod(start);
            double start_min = start - rounded_start > 0.0 ? rounded_start + increment : rounded_start;
            double unit_start = rounded_start;

            double offset_unit = unit_start - _double_scale_control.Config.Start;
            double offset_first = start_min - _double_scale_control.Config.Start;

            //double limit = _double_scale_control.Config.Padding.Left + _double_scale_control.GetRenderWidth();
            double limit = _double_scale_control.GetRenderWidth();
            if (_double_scale_control.Config.Orientation == Orientation.Horizontal)
            {
                double current_pos = offset_first * unit_size / _text_lod;

                if (offset_first != offset_unit)
                {
                    positions.Add(current_pos);
                    current_pos = offset_unit * unit_size / _text_lod;
                }

                //current_pos += _double_scale_control.Config.Padding.Left;
                while (current_pos < limit)
                {
                    positions.Add(current_pos);
                    current_pos += unit_size;
                }
            }
            else
            {
                double current_pos = _double_scale_control.GetRenderHeight() - offset_first * unit_size / _text_lod;

                if (offset_first != offset_unit)
                {
                    positions.Add(current_pos);
                    current_pos = _double_scale_control.GetRenderHeight() - offset_unit * unit_size / _text_lod; ;
                }

                limit = 0;
                while (current_pos > limit)
                {
                    positions.Add(current_pos);
                    current_pos -= unit_size;
                }
            }

            return positions;
        }

        private double RoundToTextLod(double number)
        {
            if (_text_lod >= 1)
                return (int)number;

            double factor = 1 / _text_lod;
            double dbl = number * factor;

            return ((int)dbl) / factor;
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            _double_scale_control = null;
            _description_text = null;
            _long_texts.Clear();
        }
        #endregion

        #region Event Handlers
        #endregion

        #region Properties

        internal override Object Control
        {
            get
            {
                return _double_scale_control;
            }
            set
            {
                if (value == null)
                    throw new ArgumentException("Control must not be null!");

                if (value is DoubleScaleControl)
                {
                    _double_scale_control = (DoubleScaleControl)value;
                }
                else
                    throw new ArgumentException("Type " + value.GetType());
            }
        }

        #endregion

        #region Dependency Properties
        #endregion

        #region Events
        #endregion

        #region Routed Events
        #endregion

        #region Attributes

        private DoubleScaleControl _double_scale_control;

        private double _text_lod = -1;
        private double _unit_offset;

        private readonly List<TextBlock> _long_texts = new List<TextBlock>();
        private TextBlock _description_text;

        #endregion

        #region Tests
        #endregion

    }
}
