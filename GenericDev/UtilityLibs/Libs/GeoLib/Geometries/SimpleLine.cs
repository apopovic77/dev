using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using MathLib;

namespace Logicx.Geo.Geometries
{
    public class SimpleLine
    {
        protected enum IndexMode
        {
            List = 1,
            Array = 2
        }

        public SimpleLine() {
            _index_mode = IndexMode.List;
        }

        public SimpleLine(string vertices)
        {
            _index_mode = IndexMode.List;
            string[] verts_arr = vertices.Split(",".ToCharArray());
            _vertices_list = new List<Vector2f>();
            for (int i = 0; i < verts_arr.Length; i++) {
                string[] verts_coords = verts_arr[i].Split(" ".ToCharArray(),2);
                float x = Convert.ToSingle(verts_coords[0].Replace(".",","));
                float y = Convert.ToSingle(verts_coords[1].Replace(".", ","));
                _vertices_list.Add(new Vector2f(x, y));
            }
        }

        public SimpleLine(List<Vector2f> vertices)
        {
            _index_mode = IndexMode.List;
            _vertices_list = vertices;
        }

        public SimpleLine(Vector2f[] vertices)
        {
            _index_mode = IndexMode.Array;
            _vertices_arr = vertices;
        }

        public List<Vector2f> Vertices
        {
            set
            {
                if (_index_mode == IndexMode.Array)
                    throw new Exception("SimpleLine is readlonly");
                _vertices_list = value;
            }

            get
            {
                return _vertices_list;
            }
        }

        public Vector2f this[int index]
        {
            get
            {
                if (_index_mode == IndexMode.Array)
                    return _vertices_arr[index];
                else
                    return _vertices_list[index];
            }
            set
            {
                if (_index_mode == IndexMode.Array)
                    throw new Exception("SimpleLine is readlonly");
                _vertices_list[index] = value;
            }
        }


        public float LineLength {
            get {
                float len = 0;
                for (int i = 1; i < Count; i++) { 
                    Vector2f v0 = this[i-1];
                    Vector2f v1 = this[i];
                    len += (v1 - v0).GetLen();
                }
                return len;
            }
        }


        /// <summary>
        /// gibt die anzahl der Vertexte in der Line zur�ck
        /// </summary>
        public int Count
        {
            get
            {
                if (_index_mode == IndexMode.Array)
                    return (_vertices_arr == null) ? 0 : _vertices_arr.Length;
                else
                    return (_vertices_list == null) ? 0 : _vertices_list.Count;
            }
        }

        public SimpleLine GetSmoothLine(double degreeThreshold)
        {
            if (_index_mode == IndexMode.Array)
                throw new Exception("Smoothline only available for Non Readonly Simplelines");

            List<Vector2f> smoothlist = GetSmoothLine(degreeThreshold, _vertices_list);
            if (smoothlist == null)
                return null;
            else
                return new SimpleLine(smoothlist); 
        }


        /// <summary>
        /// Benachbarte Richtungsvektoren, die einen Winkel von weniger als die �bergebene Threshold einschlie�en werden gegl�ttet, indem 
        /// der Vektor der beiden ausgehenden Richtungsvektoren entfernt wird
        /// </summary>
        /// <param name="degreeThreshold">Winkel in Grad, ab dem der eingeschlossene Vektor aus der Liste entfernt werden soll</param>
        /// <param name="toSmoothLine">Linie die gegl�ttet werden soll</param>
        /// <returns></returns>
        protected List<Vector2f> GetSmoothLine(double degreeThreshold, List<Vector2f> toSmoothLine)
        {
            /* Bei 2 Vektoren hat eine Gl�ttung keinen Sinn */
            if (toSmoothLine.Count < 3)
                return null;
            
            List<Vector2f> pointList = new List<Vector2f>();

            /* Umrechnung Grad in Radiant */
            double radThreshold = degreeThreshold * Math.PI / 180;
            pointList.Add(toSmoothLine[0]);
            for (int i = 0; i < toSmoothLine.Count - 2; i++)
            {
                /* Winkel wird immer in Bezug, zu dem letzten aufgenommen Vektor berechnet */
                Vector2f v_left = pointList[pointList.Count-1] - toSmoothLine[i + 1];
                Vector2f v_right = toSmoothLine[i + 2] - toSmoothLine[i + 1];

                /* Winkel, zwischen den beiden Richtungsvektoren */
                double radwinkel = Math.Acos(v_left.GetSkalarProduct(v_right) / (v_left.GetLen() * v_right.GetLen()));
                if (radwinkel > radThreshold)
                    pointList.Add(toSmoothLine[i + 1]);
            }
            pointList.Add(this[toSmoothLine.Count - 1]);
            return pointList;
        }

        public void Add(Vector2f[] v_arr)
        {
            if (_index_mode == IndexMode.Array)
                throw new Exception("SimpleLine is readlonly");

            if (v_arr != null)
            {
                if (_vertices_list == null)
                    _vertices_list = new List<Vector2f>();

                for (int i = 0; i < v_arr.Length; i++)
                    _vertices_list.Add(v_arr[i]);
            }
        }

        public void Add(Vector2f v)
        {
            if (_index_mode == IndexMode.Array)
                throw new Exception("SimpleLine is readlonly");
            if (_vertices_list == null)
                _vertices_list = new List<Vector2f>();
            _vertices_list.Add(v);
        }

        public void Clear()
        {
            if (_index_mode == IndexMode.Array)
                throw new Exception("SimpleLine is readlonly");
            _vertices_list.Clear();
        }

        public SimpleLine[] GetParallelLines(float margin, out Vector2f start_seg, out Vector2f end_seg)
        {
            return GetParallelLines(margin, false, out start_seg, out end_seg);
        }

        public SimpleLine[] GetParallelLines(float margin)
        {
            Vector2f start_seg, end_seg;
            return GetParallelLines(margin, false, out start_seg, out end_seg);
        }

        /// <summary>
        /// Liefert in Bezug zur aktuellen SimpleLine, die parallele obere- bzw. untere-Linie
        /// </summary>
        /// <param name="margin">Beschreibt den Abstand der beiden parallelen Linie in Bezug zur Originalen</param>
        /// <param name="forpolygon">True, wenn R�ckgabearray f�r Polygon gedacht ist (Anfangs und Endwert sind ident</param>
        /// <param name="start_seg">Speichert den Richtungsvektor am Anfang der zu Grunde liegenden SimpleLine</param>
        /// <param name="end_seg">Speichert den Richtungsvektor am Ende der zu Grunde liegenden SimpleLine</param>
        /// <returns></returns>
        public SimpleLine[] GetParallelLines(float margin, bool forpolygon, out Vector2f start_seg, out Vector2f end_seg)
        {
            List<Vector2f> v_para_verts_fw = new List<Vector2f>();
            List<Vector2f> v_para_verts_bw = new List<Vector2f>();
            List<Vector2f> v_org_verts_fw = new List<Vector2f>();
            List<Vector2f> v_org_verts_bw = new List<Vector2f>();
            v_para_verts_bw.Add(new Vector2f());
            start_seg = this[1] - this[0];
            end_seg = this[Count - 1] - this[Count - 2];

            if (RelTranslationVec != null && (RelTranslationVec.X != 0 && RelTranslationVec.Y != 0))
            {
                for (int e = 0; e < Count; e++)
                {
                    v_para_verts_fw.Add(this[e] + RelTranslationVec.GetNorm() * margin);
                    if (e == 0 && forpolygon)
                        v_para_verts_bw.Insert(0, v_para_verts_fw[0]);
                    v_para_verts_bw.Insert(0, this[e] - RelTranslationVec.GetNorm() * margin);
                }

                v_para_verts_bw.RemoveAt(v_para_verts_bw.Count - 1);
                RelTranslationVec = new Vector2f(0,0);
            }
            else
            {
                //first and last vec
                Vector2f[] v_first_last = GetConnection(this[0], this[1], margin);

                v_para_verts_fw.Add(v_first_last[0]);
                if (forpolygon)
                    v_para_verts_bw.Insert(0, v_first_last[0]);
                v_para_verts_bw.Insert(0, v_first_last[1]);

                v_org_verts_fw.Add(this[0]);
                v_org_verts_bw.Add(this[0]);

                //in between verts
                int i = 1;
                for (; i < Count - 1; i++)
                {
                    Vector2f v_a = this[i - 1];
                    Vector2f v_b = this[i];
                    Vector2f v_c = this[i + 1];

                    Vector2f neighb1 = v_a - v_b;
                    Vector2f neighb2 = v_c - v_b;

                    #region check for skip

                    //1. same verts
                    if (v_a == v_b)
                    {
                        if (_index_mode == IndexMode.Array)
                            throw new Exception("SimpleLine is readlonly");
                        _vertices_list.Remove(v_a);
                        i--;
                        continue;
                    }

                    if (v_b == v_c)
                    {
                        if (_index_mode == IndexMode.Array)
                            throw new Exception("SimpleLine is readlonly");
                        _vertices_list.Remove(v_b);
                        i--;
                        continue;
                    }

                    if (v_a == v_c)
                    {
                        if (_index_mode == IndexMode.Array)
                            throw new Exception("SimpleLine is readlonly");
                        _vertices_list.Remove((v_a));
                        i--;
                        continue;
                    }

                    #endregion

                    //check for redundant vector
                    if (Math.Round((neighb1.GetNorm() + neighb2.GetNorm()).GetLen(), 6) == 0 ||
                        Math.Round((neighb1.GetNorm() - neighb2.GetNorm()).GetLen(), 6) == 0)
                        continue;

                    Vector2f[] v_connection = GetConnection(v_a, v_b, v_c, margin);

                    #region check for overcutting verts
                    {

                        //if ((v_b - v_connection[0]).GetLen() > margin * 2)
                        //{
                        //    if (Math.Abs(neighb1.GetAngle(v_connection[0] - v_b)) > Math.PI / 2)
                        //    {
                        //        Vector2f v1 = (v_connection[0] - v_a).GetNorm();
                        //        Vector2f v2 = (v_connection[0] - v_c).GetNorm();
                        //        Vector2f[] v_approx = GetConnection(v_b, v_a, margin);
                        //        //v_connection[0] = v_a + v1*(neighb1.GetLen() + margin);
                        //        v_connection[0] = v_approx[1];
                        //        v_para_verts_fw.Add(v_connection[0]);
                        //        CheckforOvercuttingVerts(v_org_verts_fw[v_org_verts_fw.Count - 1], v_para_verts_fw[v_para_verts_fw.Count - 1], v_b, v_connection[0], v_para_verts_fw, false);

                        //        v_approx = GetConnection(v_b, v_c, margin);
                        //        //v_connection[0] = v_c + v2*(neighb2.GetLen() + margin);
                        //        v_connection[0] = v_approx[0];
                        //        v_para_verts_fw.Add(v_connection[0]);
                        //        CheckforOvercuttingVerts(v_org_verts_fw[v_org_verts_fw.Count - 1], v_para_verts_fw[v_para_verts_fw.Count - 1], v_b, v_connection[0], v_para_verts_fw, false);
                        //    }
                        //}
                        //else
                            CheckforOvercuttingVerts(v_org_verts_fw[v_org_verts_fw.Count - 1],v_para_verts_fw[v_para_verts_fw.Count - 1], v_b,
                                                           v_connection[0], v_para_verts_fw, false);
                        
                        v_org_verts_fw.Add(v_b);

                    }
                    {
                        //if ((v_b - v_connection[1]).GetLen() > margin * 2)
                        //{
                        //    if (Math.Abs(neighb1.GetAngle(v_connection[1] - v_b)) > Math.PI / 2)
                        //    {
                        //        Vector2f v1 = (v_connection[1] - v_a).GetNorm();
                        //        Vector2f v2 = (v_connection[1] - v_c).GetNorm();
                        //        Vector2f[] v_approx = GetConnection(v_b, v_a, margin);
                        //        //v_connection[1] = v_a + v1*(neighb1.GetLen() + margin);
                        //        v_connection[1] = v_approx[0];
                        //        v_para_verts_bw.Insert(0, v_connection[1]);
                        //        CheckforOvercuttingVerts(v_org_verts_bw[v_org_verts_bw.Count - 1], v_para_verts_bw[0], v_b, v_connection[1], v_para_verts_bw, true);

                        //        v_approx = GetConnection(v_b, v_c, margin);
                        //        //v_connection[1] = v_c + v2*(neighb2.GetLen() + margin);
                        //        v_connection[1] = v_approx[1];
                        //        v_para_verts_bw.Insert(0, v_connection[1]);
                        //        CheckforOvercuttingVerts(v_org_verts_bw[v_org_verts_bw.Count - 1], v_para_verts_bw[0], v_b, v_connection[1], v_para_verts_bw, true);
                        //    }

                        //}
                        //else
                            CheckforOvercuttingVerts(v_org_verts_bw[v_org_verts_bw.Count - 1], v_para_verts_bw[0], v_b, v_connection[1], v_para_verts_bw, true);
                        

                        v_org_verts_bw.Add(v_b);
                    }

                    #endregion
                }

                //last vec
                Vector2f[] newvector = GetConnection(this[Count - 1], this[Count - 2],
                                                     margin);

                #region Last_check for overcutting verts

                {
                   
                    Vector2f newpoint;
                    bool is_inside = PointToDelete(v_org_verts_fw[v_org_verts_fw.Count - 1],
                                                   v_para_verts_fw[v_para_verts_fw.Count - 1],
                                                   this[Count - 1], newvector[1], out newpoint);

                    if (is_inside)
                        v_para_verts_fw.RemoveAt(v_para_verts_fw.Count - 1);

                    //Der letzte Punkt muss immer der originale sein. Der interpolierte w�rde unter Umst�nden zu fr�h enden
                    v_para_verts_fw.Add(newvector[1]);
                }
                {
                    Vector2f newpoint;
                    bool is_inside = PointToDelete(v_org_verts_bw[v_org_verts_bw.Count - 1],
                                                   v_para_verts_bw[0], this[Count - 1], newvector[0],
                                                   out newpoint);
                    if (is_inside)
                        v_para_verts_bw.RemoveAt(0);

                    //Der letzte Punkt muss immer der originale sein. Der interpolierte w�rde unter Umst�nden zu fr�h enden
                    v_para_verts_bw.Insert(0, newvector[0]);
                }

                #endregion

                //combine fw and bw and return
                v_para_verts_bw.RemoveAt(v_para_verts_bw.Count - 1);
            }

            SimpleLine[] myoutput = new SimpleLine[] { new SimpleLine(v_para_verts_fw), new SimpleLine(v_para_verts_bw) };
            return myoutput;


        }

        private void CheckforOvercuttingVerts(Vector2f v_org, Vector2f v_para, Vector2f new_vorg, Vector2f new_vpara, List<Vector2f> para_list, bool addfirst)
        {
            Vector2f newpoint;
            bool is_inside = PointToDelete(v_org,
                                           v_para, new_vorg, new_vpara, out newpoint);

            if (is_inside)
            {
                if (addfirst)
                {
                    para_list.RemoveAt(0);
                    para_list.Insert(0, newpoint);
                }
                else
                {
                    para_list.RemoveAt(para_list.Count - 1);
                    para_list.Add(newpoint);
                }
            }
            else
            {
                if (addfirst)
                    para_list.Insert(0, newpoint);
                else
                    para_list.Add(newpoint);
            }
                
        }

        /// <summary>
        /// Berechnet den Schnittpunkt von 2 Geraden in Parameterform
        /// </summary>
        /// <param name="r1"></param>
        /// <param name="k1"></param>
        /// <param name="r2"></param>
        /// <param name="k2"></param>
        /// <returns></returns>
        private Vector2f CalcInterSectPoint(Vector2f r1, Vector2f k1, Vector2f r2, Vector2f k2)
        {
            float l_1 = (r2.Y*k2.X + k2.Y*(r1.X - r2.X) - r1.Y*k2.X)/(k1.Y*k2.X - k1.X*k2.Y);
            
            return r1 + new Vector2f(k1.X*l_1, k1.Y*l_1);

            double l_2 = (r1.X - r2.X + l_1*k1.X)/k2.X;
        }

        /// <summary>
        /// �berpr�ft ob zwei Geraden, innerhalb ihrer Intervallgrenzen einen Schnittpunkt haben.
        /// Ist dies der Fall, wird der Interpolierte Werte zwischen line1_para und line2_para �ber InterpolPoint zur�ckgegeben.
        /// Anderenfalls wird line2_para zur�ckgegeben.
        /// </summary>
        /// <param name="line1_org">OriginalPunkt der 1. Gerade</param>
        /// <param name="line1_para">Parallelisierter Punkt der 1. Geraden</param>
        /// <param name="line2_org">OriginalPunkt der 2. Gerade</param>
        /// <param name="line2_para">Parallelisierter Punkt der 2. Geraden (des eventuell neu hinzukommenden PUnktes</param>
        /// <param name="InterpolPoint">Inperpolierter Punkt falls �berschneidung. Sonst der Originalpunkt</param>
        /// <returns>true, wenn sich die Geraden innerhalb der Intervallgrenzen �berschneiden. False sonst</returns>
        private bool PointToDelete(Vector2f line1_org, Vector2f line1_para, Vector2f line2_org, Vector2f line2_para, out Vector2f InterpolPoint)
        {
            Edge e1 = new Edge((Vector2d)line1_org, (Vector2d)line1_para);
            Edge e2 = new Edge((Vector2d)line2_org, (Vector2d)line2_para);

            Vector2d cut;
            bool is_inside = LinearAlgebra.GetCutPointInSegment(e1, e2, out cut);
            
            if (is_inside)
            {
                Vector2f temp_vect = line1_para;
                Vector2f temp2 = temp_vect - line2_para;
                InterpolPoint = line1_para + (temp2.GetNorm() * (temp2.GetLen() / 2));
            }
            else
                InterpolPoint = line2_para;

            return is_inside;
        }

        public SimpleLine GetParallelLine(float margin)
        {
            if (margin == 0)
                return this;

            if (Count == 2)
                return GetParallelLine2Verts(margin);

            SimpleLine[] mylines = GetParallelLines(Math.Abs(margin));
            if (margin>0)
                return mylines[0];
            else
            {
                mylines[1].Vertices.Reverse();
                return mylines[1];
            }
        }

        private SimpleLine GetParallelLine2Verts(float margin)
        {
            Vector2f v = this[1] - this[0];
            Vector2f dir_v = v.GetOrthogonal2().GetNorm();
            List<Vector2f> ords = new List<Vector2f>(2);
            ords.Add(this[0] + dir_v * margin);
            ords.Add(this[1] + dir_v * margin);
            return new SimpleLine(ords);
        }

        private SimpleLine GetParallelLine3Verts(float margin)
        {
            Vector2f v1 = this[2] - this[1];
            Vector2f v2 = this[0] - this[1];

            Vector2f dir_v = (v2 + v1).GetNorm().GetMirror();
            List<Vector2f> ords = new List<Vector2f>(3);
            ords.Add(this[0] + dir_v * margin);
            ords.Add(this[1] + dir_v * margin);
            ords.Add(this[2] + dir_v * margin);

            return new SimpleLine(ords);
        }

        public SimpleLine GetParallelLine(bool positivNorm, float margin)
        {
            SimpleLine[] mylines = GetParallelLines(Math.Abs(margin));
            if (positivNorm)
                return mylines[0];
            else
                return mylines[1];
        }

        public SimpleLine GetParallelLine(Vector2f trans_vec, float margin)
        {
            RelTranslationVec = trans_vec;
            return GetParallelLine(margin);
        }

        public Vector2f[] GetParallelVertexes(float margin)
        {
            Vector2f s_vec, e_vec;
            return GetParallelVertexes(margin, out s_vec, out e_vec);
        }

        public Vector2f[] GetParallelVertexes(float margin, out Vector2f start_seg_vec, out Vector2f end_seg_vec)
        {
            SimpleLine[] myline = GetParallelLines(margin, out start_seg_vec, out end_seg_vec);
            Vector2f[] myarray = new Vector2f[myline[0].Vertices.Count + myline[1].Vertices.Count];
            myline[0].Vertices.CopyTo(myarray, 0);
            myline[1].Vertices.CopyTo(myarray, myline[0].Vertices.Count);

            return myarray;
        }

        public SimplePolygon GetSimplePolygon(float margin)
        {
            Vector2f start_seg_vec, end_seg_vec;
            Vector2f[] myarray = GetParallelVertexes(margin, out start_seg_vec, out end_seg_vec);
            SimplePolygon mypoly = new SimplePolygon(new List<Vector2f>(myarray), start_seg_vec, end_seg_vec);
           
            return mypoly;
        }

        public static Vector2f[] GetConnection(Vector2f A, Vector2f B, float radius)
        {
            Vector2f[] conns = new Vector2f[2];
            Vector2f temp = B - A;
            conns[1] = (temp.GetOrthogonal1().GetNorm() * radius)+A;
            conns[0] = (temp.GetOrthogonal2().GetNorm() * radius)+A;
            return conns;
        }

        public static Vector2f[] GetConnection(Vector2f A, Vector2f B, Vector2f C, Vector2f[] oldResult, float radius)
        {
            Vector2f[] newresult = GetConnection(A, B, C, radius);
            if((newresult[0]-oldResult[0]).GetLen() > (newresult[0]-oldResult[1]).GetLen())
            {
                Vector2f[] newresult2 = new Vector2f[2];
                newresult2[0] = newresult[1];
                newresult2[1] = newresult[0];
                return newresult2;
            }

            return newresult;
        }

        public static Vector2f[] GetConnection(Vector2f A, Vector2f B, Vector2f C, float radius)
        {
            Vector2f[] conns = new Vector2f[2];
            Vector2f neighb1 = A - B;
            Vector2f neighb2 = C - B;

            if (Math.Round((neighb1.GetNorm() + neighb2.GetNorm()).GetLen(), 6) == 0 || Math.Round((neighb1.GetNorm() - neighb2.GetNorm()).GetLen(), 6) == 0 || A == B || B == C)
                throw new Exception("Selber Richtungsvektor");

            Vector2f v1 = (neighb1.GetNorm() + neighb2.GetNorm());
            v1 = (v1 * -1).GetNorm();
            Vector2f n1, n2;
            
            n1 = neighb1.GetOrthogonal1().GetNorm();
            n2 = neighb2.GetOrthogonal2().GetNorm();
            
            float len1 = (radius * n1.GetSkalarProduct()) / (v1.GetSkalarProduct(n1));
            float len2 = (radius * n2.GetSkalarProduct()) / (v1.GetSkalarProduct(n2));
            conns[0] = (v1 * len1) + B;
            conns[1] = (v1 * -len2) + B;

            return conns;
        }
        
        public Vector2f[] GetTriangledShapeArroundLine(float margin)
        {
            List<Vector2f> vertices_res = new List<Vector2f>();

            //do for all edges in line
            for (int i = 0; i < this.Count - 1; i++)
            {
                Vector2f v = this[i + 1] - this[i];

                if (v.GetLen() == 0)
                    continue;

                Vector2f ortho2 = v.GetOrthogonal2().GetNorm() * margin;

                vertices_res.Add(this[i] + ortho2);
                vertices_res.Add(this[i] + ortho2.GetMirror());

                vertices_res.Add(this[i + 1] + ortho2);
                vertices_res.Add(this[i + 1] + ortho2.GetMirror());
            }

            return vertices_res.ToArray();
        }


        public Vector2f GetNearestPositionInLine(Vector2f xs, out int node_s, out float entfernung_x_von_line, out float len_till_pos_online, out float len_node_s_to_x)
        {

            //BERECHNUNG DER ORTH.VECTOREN ZU XS UND XE
            len_node_s_to_x = 0;
            len_till_pos_online = 0;
            int weglen = this.Count;

            float c = 0;
            Vector2f t_vec;
            int xs_shortest_knoten = 0;
            float len_till_shortest_knoten_if_needed = 0;
            Vector2f xs_shortest_umweg = xs - this[0];
            Vector2f xs_shortest_wegabzweigung = new Vector2f(0, 0);
            float xs_len_shortest_umweg = xs_shortest_umweg.GetLen();


            for (int i = 0; i < this.Count - 1; i++)
            {
                Vector2f wegteil = this[i + 1] - this[i];
                float wegteil_len = wegteil.GetLen();
                len_till_shortest_knoten_if_needed += wegteil_len;
                t_vec = xs - this[i];
                c = t_vec.GetSkalarProduct(wegteil) / wegteil.GetSkalarProduct();
                float curr_wegteil_len = (wegteil * c).GetLen();
                if (c > 0 && curr_wegteil_len <= wegteil_len)
                {
                    Vector2f v_wegbisabzweigung = wegteil * c;
                    Vector2f v_umweg = t_vec - v_wegbisabzweigung;
                    float v_umweg_len = v_umweg.GetLen();
                    if (v_umweg_len < xs_len_shortest_umweg)
                    {
                        xs_shortest_umweg = v_umweg;
                        xs_len_shortest_umweg = v_umweg_len;
                        xs_shortest_knoten = i;
                        xs_shortest_wegabzweigung = v_wegbisabzweigung;
                        len_till_pos_online = len_till_shortest_knoten_if_needed - wegteil_len + v_wegbisabzweigung.GetLen();
                        len_node_s_to_x = v_wegbisabzweigung.GetLen();
                    }
                }
            }

            //check ob ein orthogonaler vektor gefunden wurde
            //wenn nicht nimm den knoten mit der k�rzesten verbindung zum xs 
            float len_t_vec = 0;
            len_till_shortest_knoten_if_needed = 0;
            for (int i = 0; i < this.Count; i++)
            {
                Vector2f wegteil;
                float wegteil_len=0;
                if (i < this.Count - 1)
                {
                    wegteil = this[i + 1] - this[i];
                    wegteil_len = wegteil.GetLen();
                    len_till_shortest_knoten_if_needed += wegteil_len;
                }

                t_vec = xs - this[i];
                len_t_vec = t_vec.GetLen();
                if (len_t_vec < xs_len_shortest_umweg)
                {
                    xs_shortest_umweg = t_vec;
                    xs_shortest_knoten = i;
                    xs_len_shortest_umweg = len_t_vec;
                    xs_shortest_wegabzweigung = new Vector2f(0, 0);
                    len_till_pos_online = len_till_shortest_knoten_if_needed - wegteil_len;
                    len_node_s_to_x = 0;
                }
            }

            node_s = xs_shortest_knoten;
            entfernung_x_von_line = xs_len_shortest_umweg;
            return this[xs_shortest_knoten] + xs_shortest_wegabzweigung;
        }

        /// <summary>
        /// l�uft durch alle line vertices, und garantiert das keine line die l�nge 0 hat, dies w�re genau dann der fall wenn zwei aufeinander folgende
        /// vertices die selben coords haben
        /// </summary>
        public void DeleteIdenticalLineVertices()
        {
            for (int i = 1; i < Vertices.Count; i++)
            {
                Vector2f v1 = Vertices[i - 1];
                Vector2f v2 = Vertices[i];
                if (v1 == v2)
                {
                    Vertices.RemoveAt(i);
                    i--;
                    continue;
                }
            }
        }

        #region implemented by Alex for IA: Version2
        /// <summary>
        /// dieser alg generiert schnell in einem durchlauf das polygon
        /// und erm�glicht die angabe von eine corner round step der f�r rundere kanten sorgt
        /// dieser parameter wird nicht ausgew�rtet wenn der wert gr�sser 2 ist, da die rundung ausreichend gut ist
        /// </summary>
        /// <param name="ordinates"></param>
        /// <param name="corner_rounding_steps"></param>
        /// <param name="stroke_thickness"></param>
        /// <returns></returns>
        public static List<Vector2f> GetPolygonVertices(Vector2f[] ordinates, int corner_rounding_steps, float stroke_thickness, bool round_endings)
        {
            List<Vector2f> _poly = new List<Vector2f>();
            float radius = stroke_thickness/2;
            
            //SimpleLine l = new SimpleLine(ordinates);
            //Vector2f start_seg;
            //Vector2f end_seg;
            //SimpleLine[] lines = l.GetParallelLines(radius, true, out start_seg, out end_seg);
            //_poly.AddRange(lines[0].Vertices);
            //_poly.AddRange(lines[1].Vertices);
            //return _poly;

            //insert front
            Vector2f v0 = ordinates[0];
            Vector2f v1 = ordinates[1];
            Vector2f v_start = (v1 - v0).GetNorm();
            _poly.Insert(0, v0 + (v_start.GetOrthogonal2() * radius));
            _poly.Add(v0 + (v_start.GetOrthogonal1() * radius));

            for (int i = 1; i < ordinates.Length - 1; i++)
            {
                Vector2f v_a = ordinates[i - 1];
                Vector2f v_b = ordinates[i];
                Vector2f v_c = ordinates[i + 1];

                Vector2f neighb1 = v_a - v_b;
                Vector2f neighb2 = v_c - v_b;

                //check for redundant vector
                if (Math.Round((neighb1.GetNorm() + neighb2.GetNorm()).GetLen(), 6) == 0 ||
                    Math.Round((neighb1.GetNorm() - neighb2.GetNorm()).GetLen(), 6) == 0)
                    continue;
                
                if(corner_rounding_steps > 1)
                    SetRoundedConnectionVertices(_poly, corner_rounding_steps, v_a, v_b, v_c, radius);
                else
                    SetConnectionVertices(_poly, v_a, v_b, v_c, radius);
            }

            //insert ending
            v0 = ordinates[ordinates.Length - 2];
            v1 = ordinates[ordinates.Length - 1];
            int insert_index = _poly.Count / 2;
            Vector2f v_end = (v1 - v0).GetNorm();
            _poly.Insert(insert_index, v1 + (v_end.GetOrthogonal1() * radius));
            _poly.Insert(insert_index, v1 + (v_end.GetOrthogonal2() * radius));


            //add ending rounded edges
            if(round_endings)
                AddCirclePoints(_poly, (_poly.Count / 2)-1, 4);

            //close poly
            _poly.Add(_poly[0]);

            //add front rounded edges
            if (round_endings)
                AddCirclePoints(_poly, _poly.Count - 2, 4);

            return _poly;
        }

        private static void SetRoundedConnectionVertices(List<Vector2f> poly_verts, int corner_rounding_steps, Vector2f A, Vector2f B, Vector2f C, float radius)
        {
            Vector2f[] conns = new Vector2f[2];
            Vector2f neighb1 = A - B;
            Vector2f neighb2 = C - B;

            Vector2f v1 = (neighb1.GetNorm() + neighb2.GetNorm());
            v1 = (v1 * -1).GetNorm();
            Vector2f n1, n2;

            n1 = neighb1.GetOrthogonal1().GetNorm();
            n2 = neighb2.GetOrthogonal2().GetNorm();

            int insert_index = Convert.ToInt32(Math.Floor(poly_verts.Count/2f));

            //von hinten nach vorne inserten
            poly_verts.Insert(insert_index, B + n2*-radius);
            poly_verts.Insert(insert_index, B + n1*-radius);
            poly_verts.Insert(insert_index, B + n2*radius);
            poly_verts.Insert(insert_index, B + n1*radius);
        }

        private static void SetConnectionVertices(List<Vector2f> poly_verts,Vector2f A, Vector2f B, Vector2f C, float radius)
        {
            Vector2f[] conns = new Vector2f[2];
            Vector2f neighb1 = A - B;
            Vector2f neighb2 = C - B;

            Vector2f v1 = (neighb1.GetNorm() + neighb2.GetNorm());
            v1 = (v1 * -1).GetNorm();
            Vector2f n1, n2;

            n1 = neighb1.GetOrthogonal1().GetNorm();
            n2 = neighb2.GetOrthogonal2().GetNorm();

            float len1 = (radius * n1.GetSkalarProduct()) / (v1.GetSkalarProduct(n1));
            float len2 = (radius * n2.GetSkalarProduct()) / (v1.GetSkalarProduct(n2));

            int insert_index = Convert.ToInt32(Math.Floor(poly_verts.Count / 2f));

            #region Spitzen korrigieren
            if (poly_verts.Count != 0 && (Math.Abs(len2) > 3 * radius || Math.Abs(len1) > 3 * radius))
            {
                Vector2f preview1 = poly_verts[insert_index];
                Vector2f preview2 = poly_verts[insert_index-1];

                Vector2f new1 = (v1*-len2) + B;
                Vector2f new2 = (v1*len1) + B;

                Vector2f new_neighb1 = new1 - B;
                Vector2f new_neighb2 = new2 - B;

                if(neighb1.GetAngle(new_neighb1) > neighb1.GetAngle(new_neighb2))
                {
                    Vector2f richtung = (v1 * -len2).GetNorm();
                    Vector2f richtung_ortho = richtung.GetOrthogonal1();
                    Vector2f middle_richtung = B + richtung.GetNorm()*2*radius;
                    Edge e1 = new Edge((Vector2d)A, (Vector2d)new1);
                    Edge e2 = new Edge((Vector2d)middle_richtung, (Vector2d)(middle_richtung + richtung_ortho));

                    Vector2d cut;
                    LinearAlgebra.GetCutPoint(e1, e2, out cut);

                    Vector2d cut2 = (Vector2d)((cut - (Vector2d)middle_richtung)*-1 + (Vector2d)middle_richtung);
                                        
                    poly_verts.Insert(insert_index, (Vector2f)cut);

                    //poly_verts.Insert(insert_index, (Vector2f)new2);  
                    poly_verts.Insert(insert_index, (Vector2f)preview2);                    
                    insert_index++;
                    poly_verts.Insert(insert_index, (Vector2f)cut2);

                    //poly_verts.Insert(insert_index, (Vector2f)new2);  
                    poly_verts.Insert(insert_index, (Vector2f)preview2);                    
                }
                else
                {
                    Vector2f richtung = (v1 * len1).GetNorm();
                    Vector2f richtung_ortho = richtung.GetOrthogonal1();

                    Vector2f middle_richtung = B + richtung.GetNorm() * 2 * radius;
                    Edge e1 = new Edge((Vector2d)A, (Vector2d)new2);
                    Edge e2 = new Edge((Vector2d)middle_richtung, (Vector2d)(middle_richtung + richtung_ortho));

                    Vector2d cut;
                    LinearAlgebra.GetCutPoint(e1, e2, out cut);

                    Vector2d cut2 = (Vector2d)((cut - (Vector2d)middle_richtung) * -1 + (Vector2d)middle_richtung);
                                                         
                    poly_verts.Insert(insert_index, (Vector2f)preview1);  
                    //poly_verts.Insert(insert_index, (Vector2f)new1);   

                    poly_verts.Insert(insert_index, (Vector2f)cut);
                    insert_index++;                    
                    poly_verts.Insert(insert_index, (Vector2f)preview1);
                    //poly_verts.Insert(insert_index, (Vector2f)new1);  

                    poly_verts.Insert(insert_index, (Vector2f)cut2);  
                }

                Vector2f ortho = v1.GetOrthogonal1();
            }
            #endregion

            else
            {
                poly_verts.Insert(insert_index, (v1 * -len2) + B);
                poly_verts.Insert(insert_index, (v1 * len1) + B);
            }

        }
        #endregion

        #region Util Methods
        /// <summary>
        /// Ab einem gewissen Index, werden zwischen diesem und den darauffolgenden Vektor zus�tzlich Punkte eingef�gt,
        /// sodass ein Halbkreis zwischen diesen Punkten entsteht, mit Radius der zugrunde liegenden Kante
        /// </summary>
        /// <param name="start_index"></param>
        public static void AddCirclePoints(List<Vector2f> vertices, int start_index, int points_to_add)
        {
            Vector2f richtung_line_ende = vertices[start_index + 1] - vertices[start_index];
            Vector2f richtung_line_orthogonal = richtung_line_ende.GetOrthogonal2();
            Vector2f mittel_punkt_ende_1 = vertices[start_index] + (richtung_line_ende).GetNorm() * (richtung_line_ende.GetLen() / 2);
            double radius = (vertices[start_index] - mittel_punkt_ende_1).GetLen();
            int stepsize = 180 / (points_to_add + 1);
            int pointsadded_ende_1 = 0;
            for (int i = 1; i <= points_to_add; i++)
            {
                //rotation f�r keispunkt
                double cur_winkel = (180 - i * stepsize);
                double cur_winkel_radian = cur_winkel * Math.PI / 180;
                double ak = Math.Cos(cur_winkel_radian) * radius;
                double gk = Math.Sin(cur_winkel_radian) * radius;
                Vector2f kreis_punkt = mittel_punkt_ende_1 + richtung_line_ende.GetNorm() * (float)(ak) + richtung_line_orthogonal.GetNorm() * (float)gk;

                vertices.Insert(start_index + 1 + pointsadded_ende_1, kreis_punkt);
                pointsadded_ende_1++;
            }
        }

        /// <summary>
        /// Ab einem gewissen Index, werden zwischen diesem und den darauffolgenden Vektor zus�tzlich Punkte eingef�gt,
        /// sodass ein Halbkreis zwischen diesen Punkten entsteht, mit Radius der zugrunde liegenden Kante
        /// </summary>
        /// <param name="start_index"></param>
        public static void AddCirclePoints(List<Vector2d> vertices, int start_index, int points_to_add)
        {
            Vector2d richtung_line_ende = vertices[start_index + 1] - vertices[start_index];
            Vector2d richtung_line_orthogonal = richtung_line_ende.GetOrthogonal2();
            Vector2d mittel_punkt_ende_1 = vertices[start_index] + (richtung_line_ende).GetNorm() * (richtung_line_ende.GetLen() / 2);
            double radius = (vertices[start_index] - mittel_punkt_ende_1).GetLen();
            int stepsize = 180 / (points_to_add + 1);
            int pointsadded_ende_1 = 0;
            for (int i = 1; i <= points_to_add; i++)
            {
                //rotation f�r keispunkt
                double cur_winkel = (180 - i * stepsize);
                double cur_winkel_radian = cur_winkel * Math.PI / 180;
                double ak = Math.Cos(cur_winkel_radian) * radius;
                double gk = Math.Sin(cur_winkel_radian) * radius;
                Vector2d kreis_punkt = mittel_punkt_ende_1 + richtung_line_ende.GetNorm() * (float)(ak) + richtung_line_orthogonal.GetNorm() * (float)gk;

                vertices.Insert(start_index + 1 + pointsadded_ende_1, kreis_punkt);
                pointsadded_ende_1++;
            }
        }

        /// <summary>
        /// Ab dem start_index Wert, werden neue Vektoren in die Liste eingef�gt, so dass an der jeweiligen stelle ein stumpfer Abschluss entsteht
        /// (in der Form eines Hammers)
        /// </summary>
        /// <param name="start_index"></param>
        public static void AddHammerPoints(List<Vector2f> vertices, int start_index, float hammer_thickness_desired)
        {
            Vector2f richtung_line_ende_org = (vertices[start_index + 1] - vertices[start_index]);
            Vector2f richtung_line_ende_norm = richtung_line_ende_org.GetNorm();
            Vector2f richtung_line_orthogonal = richtung_line_ende_norm.GetOrthogonal2();
            float richtung_line_ende_length = richtung_line_ende_org.GetLen();
            float hammer_offset = richtung_line_ende_length * 0.2f;
            float hammer_thickness;
            if (hammer_thickness_desired == -1)
                hammer_thickness = richtung_line_ende_length * 0.5f;
            else
                hammer_thickness = hammer_thickness_desired;

            Vector2f hammer_p1 = vertices[start_index] + richtung_line_ende_norm * -hammer_offset;
            Vector2f hammer_p2 = hammer_p1 + richtung_line_orthogonal * hammer_thickness;
            Vector2f hammer_p4 = vertices[start_index + 1] + richtung_line_ende_norm * hammer_offset;
            Vector2f hammer_p3 = hammer_p4 + richtung_line_orthogonal * hammer_thickness;
            vertices.Insert(start_index + 1, hammer_p4);
            vertices.Insert(start_index + 1, hammer_p3);
            vertices.Insert(start_index + 1, hammer_p2);
            vertices.Insert(start_index + 1, hammer_p1);
        }
        #endregion

        /// <summary>
        /// Dieser Vektor soll f�r die Parallelisierung verwendet werden
        /// </summary>
        public Vector2f RelTranslationVec
        {
            set; get;
        }

        #region 
        protected List<Vector2f> _vertices_list;
        protected Vector2f[] _vertices_arr;
        protected IndexMode _index_mode;
        #endregion
    }
}
