using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.DirectX;
using D3d = Microsoft.DirectX.Direct3D;
using System.Drawing;
using Logicx.DirectX.Engine;

namespace Logicx.DirectX.Objects.GeometricPrimitives
{
    public class Cube3d : Object3d
    {

        #region Attributes
        protected D3d.VertexBuffer m_VB = null;
        protected int _cubecolor;
        #endregion


        public Cube3d()
        {
            //set default scaling
            m_vScaleing = new Vector3(10f, 10f, 10f);
            //set default position
            this.North = 5162557f;
            this.East = 443356f;
            //set default rotation
            this.Roll = (float)Math.PI / 2f;
        }
		
		public Color Color
		{
			set {
				_cubecolor = value.ToArgb();
			}
			
			get {
				return Color.FromArgb(_cubecolor);
			}
		}

        public override void Init()
        {
            CreateVertexBuffer();
        }
        public override void Dispose()
        {
            if (m_VB != null) m_VB.Dispose();
        }

        public void CreateVertexBuffer()
        {

            int count_vertices = 4 * 5;

            //create vertex buffer
            m_VB = new D3d.VertexBuffer(typeof(D3d.CustomVertex.PositionNormalColored),
                                        count_vertices,
                                        Engine3d.Device3d,
                                        Engine3d.GetUsageFlagForVB(false),
                                        D3d.CustomVertex.PositionNormalColored.Format,
                                        Engine3d.GetPoolFlagForVB());

            //lock and fill vertices values
            D3d.CustomVertex.PositionNormalColored[] vb_vertices = (D3d.CustomVertex.PositionNormalColored[])m_VB.Lock(0, typeof(D3d.CustomVertex.PositionNormalColored), D3d.LockFlags.None, count_vertices);

            //rect top
            vb_vertices[0].X = -1;
            vb_vertices[0].Y = -1;
            vb_vertices[0].Z = -1;
            vb_vertices[0].Nx = 0;
            vb_vertices[0].Ny = 0;
            vb_vertices[0].Nz = -1;
            vb_vertices[0].Color = _cubecolor;

            vb_vertices[1].X = -1;
            vb_vertices[1].Y = 1;
            vb_vertices[1].Z = -1;
            vb_vertices[1].Nx = 0;
            vb_vertices[1].Ny = 0;
            vb_vertices[1].Nz = -1;
            vb_vertices[1].Color = _cubecolor;

            vb_vertices[2].X = 1;
            vb_vertices[2].Y = -1;
            vb_vertices[2].Z = -1;
            vb_vertices[2].Nx = 0;
            vb_vertices[2].Ny = 0;
            vb_vertices[2].Nz = -1;
            vb_vertices[2].Color = _cubecolor;

            vb_vertices[3].X = 1;
            vb_vertices[3].Y = 1;
            vb_vertices[3].Z = -1;
            vb_vertices[3].Nx = 0;
            vb_vertices[3].Ny = 0;
            vb_vertices[3].Nz = -1;
            vb_vertices[3].Color = _cubecolor;

            //rect front
            vb_vertices[4 + 0].X = -1;
            vb_vertices[4 + 0].Y = -1;
            vb_vertices[4 + 0].Z = 1;
            vb_vertices[4 + 0].Nx = 0;
            vb_vertices[4 + 0].Ny = -1;
            vb_vertices[4 + 0].Nz = 0;
            vb_vertices[4 + 0].Color = _cubecolor;

            vb_vertices[4 + 1].X = -1;
            vb_vertices[4 + 1].Y = -1;
            vb_vertices[4 + 1].Z = -1;
            vb_vertices[4 + 1].Nx = 0;
            vb_vertices[4 + 1].Ny = -1;
            vb_vertices[4 + 1].Nz = 0;
            vb_vertices[4 + 1].Color = _cubecolor;

            vb_vertices[4 + 2].X = 1;
            vb_vertices[4 + 2].Y = -1;
            vb_vertices[4 + 2].Z = 1;
            vb_vertices[4 + 2].Nx = 0;
            vb_vertices[4 + 2].Ny = -1;
            vb_vertices[4 + 2].Nz = 0;
            vb_vertices[4 + 2].Color = _cubecolor;

            vb_vertices[4 + 3].X = 1;
            vb_vertices[4 + 3].Y = -1;
            vb_vertices[4 + 3].Z = -1;
            vb_vertices[4 + 3].Nx = 0;
            vb_vertices[4 + 3].Ny = -1;
            vb_vertices[4 + 3].Nz = 0;
            vb_vertices[4 + 3].Color = _cubecolor;


            //rect back
            vb_vertices[8 + 0].X = 1;
            vb_vertices[8 + 0].Y = 1;
            vb_vertices[8 + 0].Z = 1;
            vb_vertices[8 + 0].Nx = 0;
            vb_vertices[8 + 0].Ny = 0;
            vb_vertices[8 + 0].Nz = 0;
            vb_vertices[8 + 0].Color = _cubecolor;

            vb_vertices[8 + 1].X = 1;
            vb_vertices[8 + 1].Y = 1;
            vb_vertices[8 + 1].Z = -1;
            vb_vertices[8 + 1].Nx = 0;
            vb_vertices[8 + 1].Ny = 1;
            vb_vertices[8 + 1].Nz = 0;
            vb_vertices[8 + 1].Color = _cubecolor;

            vb_vertices[8 + 2].X = -1;
            vb_vertices[8 + 2].Y = 1;
            vb_vertices[8 + 2].Z = 1;
            vb_vertices[8 + 2].Nx = 0;
            vb_vertices[8 + 2].Ny = 1;
            vb_vertices[8 + 2].Nz = 0;
            vb_vertices[8 + 2].Color = _cubecolor;

            vb_vertices[8 + 3].X = -1;
            vb_vertices[8 + 3].Y = 1;
            vb_vertices[8 + 3].Z = -1;
            vb_vertices[8 + 3].Nx = 0;
            vb_vertices[8 + 3].Ny = 1;
            vb_vertices[8 + 3].Nz = 0;
            vb_vertices[8 + 3].Color = _cubecolor;

            //rect side right
            vb_vertices[12 + 0].X = 1;
            vb_vertices[12 + 0].Y = -1;
            vb_vertices[12 + 0].Z = 1;
            vb_vertices[12 + 0].Nx = 1;
            vb_vertices[12 + 0].Ny = 0;
            vb_vertices[12 + 0].Nz = 0;
            vb_vertices[12 + 0].Color = _cubecolor;

            vb_vertices[12 + 1].X = 1;
            vb_vertices[12 + 1].Y = -1;
            vb_vertices[12 + 1].Z = -1;
            vb_vertices[12 + 1].Nx = 1;
            vb_vertices[12 + 1].Ny = 0;
            vb_vertices[12 + 1].Nz = 0;
            vb_vertices[12 + 1].Color = _cubecolor;

            vb_vertices[12 + 2].X = 1;
            vb_vertices[12 + 2].Y = 1;
            vb_vertices[12 + 2].Z = 1;
            vb_vertices[12 + 2].Nx = 1;
            vb_vertices[12 + 2].Ny = 0;
            vb_vertices[12 + 2].Nz = 0;
            vb_vertices[12 + 2].Color = _cubecolor;

            vb_vertices[12 + 3].X = 1;
            vb_vertices[12 + 3].Y = 1;
            vb_vertices[12 + 3].Z = -1;
            vb_vertices[12 + 3].Nx = 1;
            vb_vertices[12 + 3].Ny = 0;
            vb_vertices[12 + 3].Nz = 0;
            vb_vertices[12 + 3].Color = _cubecolor;


            //rect side left
            vb_vertices[16 + 0].X = -1;
            vb_vertices[16 + 0].Y = 1;
            vb_vertices[16 + 0].Z = 1;
            vb_vertices[16 + 0].Nx = -1;
            vb_vertices[16 + 0].Ny = 0;
            vb_vertices[16 + 0].Nz = 0;
            vb_vertices[16 + 0].Color = _cubecolor;

            vb_vertices[16 + 1].X = -1;
            vb_vertices[16 + 1].Y = 1;
            vb_vertices[16 + 1].Z = -1;
            vb_vertices[16 + 1].Nx = -1;
            vb_vertices[16 + 1].Ny = 0;
            vb_vertices[16 + 1].Nz = 0;
            vb_vertices[16 + 1].Color = _cubecolor;

            vb_vertices[16 + 2].X = -1;
            vb_vertices[16 + 2].Y = -1;
            vb_vertices[16 + 2].Z = 1;
            vb_vertices[16 + 2].Nx = -1;
            vb_vertices[16 + 2].Ny = 0;
            vb_vertices[16 + 2].Nz = 0;
            vb_vertices[16 + 2].Color = _cubecolor;

            vb_vertices[16 + 3].X = -1;
            vb_vertices[16 + 3].Y = -1;
            vb_vertices[16 + 3].Z = -1;
            vb_vertices[16 + 3].Nx = -1;
            vb_vertices[16 + 3].Ny = 0;
            vb_vertices[16 + 3].Nz = 0;
            vb_vertices[16 + 3].Color = _cubecolor;


            //Unlock the vb before you can use it elsewhere
            m_VB.Unlock();
        }

        public override void Render(float elapsedTime)
        {
            if (m_VB == null) return;

            //rotate scale and translate obj if specified
            Matrix m = Microsoft.DirectX.Matrix.Identity;
            m.Multiply(Matrix.RotationYawPitchRoll(Yaw, Pitch, Roll));
            if (m_vScaleing != null)
                m.Multiply(Matrix.Scaling(m_vScaleing));

            if (m_vPosition != null)
                m.Multiply(Matrix.Translation(m_vPosition));
            Engine3d.Device3d.Transform.World = m;

            Engine3d.Device3d.SetTexture(0, null);
            Engine3d.Device3d.SetStreamSource(0, m_VB, 0);
            Engine3d.Device3d.VertexFormat = D3d.CustomVertex.PositionNormalColored.Format;
            Engine3d.Device3d.RenderState.FillMode = Microsoft.DirectX.Direct3D.FillMode.Solid;

            //draw back
            Engine3d.Device3d.DrawPrimitives(D3d.PrimitiveType.TriangleStrip, 8, 2);
            //draw side
            Engine3d.Device3d.DrawPrimitives(D3d.PrimitiveType.TriangleStrip, 12, 2);
            Engine3d.Device3d.DrawPrimitives(D3d.PrimitiveType.TriangleStrip, 16, 2);
            //draw front
            Engine3d.Device3d.DrawPrimitives(D3d.PrimitiveType.TriangleStrip, 4, 2);
            //draw top
            Engine3d.Device3d.DrawPrimitives(D3d.PrimitiveType.TriangleStrip, 0, 2);
        }
    }
}
