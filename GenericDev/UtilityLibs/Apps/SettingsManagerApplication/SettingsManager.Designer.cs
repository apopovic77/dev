namespace SettingsManagerApplication
{
    partial class SettingsManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingsManager));
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadSettingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveSettingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.b_addsetting = new System.Windows.Forms.Button();
            this.tb_kommentar = new System.Windows.Forms.TextBox();
            this.rb_bool = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.rb_string = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.tb_settingsname = new System.Windows.Forms.TextBox();
            this.b_updatesetting = new System.Windows.Forms.Button();
            this.rb_int = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.rb_float = new System.Windows.Forms.RadioButton();
            this.tb_settingsdefvalue = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.menuStrip.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // treeView1
            // 
            this.treeView1.Location = new System.Drawing.Point(0, 25);
            this.treeView1.Name = "treeView1";
            this.treeView1.Size = new System.Drawing.Size(165, 216);
            this.treeView1.TabIndex = 14;
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileDialog";
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(532, 24);
            this.menuStrip.TabIndex = 19;
            this.menuStrip.Text = "menuStrip";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadSettingsToolStripMenuItem,
            this.saveSettingsToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(35, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // loadSettingsToolStripMenuItem
            // 
            this.loadSettingsToolStripMenuItem.Name = "loadSettingsToolStripMenuItem";
            this.loadSettingsToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.loadSettingsToolStripMenuItem.Text = "Load Settings";
            this.loadSettingsToolStripMenuItem.Click += new System.EventHandler(this.loadSettingsToolStripMenuItem_Click);
            // 
            // saveSettingsToolStripMenuItem
            // 
            this.saveSettingsToolStripMenuItem.Name = "saveSettingsToolStripMenuItem";
            this.saveSettingsToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.saveSettingsToolStripMenuItem.Text = "Save Settings";
            this.saveSettingsToolStripMenuItem.Click += new System.EventHandler(this.saveSettingsToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.aboutToolStripMenuItem.Text = "About";
            // 
            // b_addsetting
            // 
            this.b_addsetting.Location = new System.Drawing.Point(441, 206);
            this.b_addsetting.Name = "b_addsetting";
            this.b_addsetting.Size = new System.Drawing.Size(75, 23);
            this.b_addsetting.TabIndex = 30;
            this.b_addsetting.Text = "add setting";
            this.b_addsetting.UseVisualStyleBackColor = true;
            this.b_addsetting.Click += new System.EventHandler(this.b_addsetting_Click);
            // 
            // tb_kommentar
            // 
            this.tb_kommentar.Location = new System.Drawing.Point(286, 69);
            this.tb_kommentar.Name = "tb_kommentar";
            this.tb_kommentar.Size = new System.Drawing.Size(230, 20);
            this.tb_kommentar.TabIndex = 28;
            // 
            // rb_bool
            // 
            this.rb_bool.AutoSize = true;
            this.rb_bool.Location = new System.Drawing.Point(11, 93);
            this.rb_bool.Name = "rb_bool";
            this.rb_bool.Size = new System.Drawing.Size(45, 17);
            this.rb_bool.TabIndex = 27;
            this.rb_bool.TabStop = true;
            this.rb_bool.Text = "bool";
            this.rb_bool.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(283, 92);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 13);
            this.label2.TabIndex = 23;
            this.label2.Text = "Setting Name";
            // 
            // rb_string
            // 
            this.rb_string.AutoSize = true;
            this.rb_string.Location = new System.Drawing.Point(11, 21);
            this.rb_string.Name = "rb_string";
            this.rb_string.Size = new System.Drawing.Size(50, 17);
            this.rb_string.TabIndex = 24;
            this.rb_string.TabStop = true;
            this.rb_string.Text = "string";
            this.rb_string.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(283, 131);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 13);
            this.label1.TabIndex = 21;
            this.label1.Text = "Default Value";
            // 
            // tb_settingsname
            // 
            this.tb_settingsname.Location = new System.Drawing.Point(286, 108);
            this.tb_settingsname.Name = "tb_settingsname";
            this.tb_settingsname.Size = new System.Drawing.Size(230, 20);
            this.tb_settingsname.TabIndex = 22;
            // 
            // b_updatesetting
            // 
            this.b_updatesetting.Location = new System.Drawing.Point(319, 206);
            this.b_updatesetting.Name = "b_updatesetting";
            this.b_updatesetting.Size = new System.Drawing.Size(116, 23);
            this.b_updatesetting.TabIndex = 31;
            this.b_updatesetting.Text = "update setting";
            this.b_updatesetting.UseVisualStyleBackColor = true;
            this.b_updatesetting.Visible = false;
            // 
            // rb_int
            // 
            this.rb_int.AutoSize = true;
            this.rb_int.Location = new System.Drawing.Point(11, 45);
            this.rb_int.Name = "rb_int";
            this.rb_int.Size = new System.Drawing.Size(36, 17);
            this.rb_int.TabIndex = 25;
            this.rb_int.TabStop = true;
            this.rb_int.Text = "int";
            this.rb_int.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(283, 53);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 13);
            this.label3.TabIndex = 29;
            this.label3.Text = "Kommentar";
            // 
            // rb_float
            // 
            this.rb_float.AutoSize = true;
            this.rb_float.Location = new System.Drawing.Point(11, 69);
            this.rb_float.Name = "rb_float";
            this.rb_float.Size = new System.Drawing.Size(45, 17);
            this.rb_float.TabIndex = 26;
            this.rb_float.TabStop = true;
            this.rb_float.Text = "float";
            this.rb_float.UseVisualStyleBackColor = true;
            // 
            // tb_settingsdefvalue
            // 
            this.tb_settingsdefvalue.Location = new System.Drawing.Point(286, 147);
            this.tb_settingsdefvalue.Name = "tb_settingsdefvalue";
            this.tb_settingsdefvalue.Size = new System.Drawing.Size(230, 20);
            this.tb_settingsdefvalue.TabIndex = 20;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rb_bool);
            this.groupBox1.Controls.Add(this.rb_string);
            this.groupBox1.Controls.Add(this.rb_int);
            this.groupBox1.Controls.Add(this.rb_float);
            this.groupBox1.Location = new System.Drawing.Point(185, 53);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(78, 129);
            this.groupBox1.TabIndex = 32;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Datatype";
            // 
            // SettingsManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(532, 241);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.b_addsetting);
            this.Controls.Add(this.tb_kommentar);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tb_settingsname);
            this.Controls.Add(this.b_updatesetting);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tb_settingsdefvalue);
            this.Controls.Add(this.treeView1);
            this.Controls.Add(this.menuStrip);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip;
            this.Name = "SettingsManager";
            this.Text = "Geo Simul 3d Code Settings Manager";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SettingsManager_FormClosing);
            this.Load += new System.EventHandler(this.SettingsManager_Load);
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadSettingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveSettingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.Button b_addsetting;
        private System.Windows.Forms.TextBox tb_kommentar;
        private System.Windows.Forms.RadioButton rb_bool;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton rb_string;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tb_settingsname;
        private System.Windows.Forms.Button b_updatesetting;
        private System.Windows.Forms.RadioButton rb_int;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RadioButton rb_float;
        private System.Windows.Forms.TextBox tb_settingsdefvalue;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}

