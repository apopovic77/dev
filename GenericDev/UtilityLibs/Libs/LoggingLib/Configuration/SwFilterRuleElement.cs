﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace LoggingLib.Configuration
{
    /// <summary>
    /// software filter rule element
    /// </summary>
    public class SwFilterRuleElement : ConfigurationElement
    {
        /// <summary>
        /// Type of the software rule
        /// </summary>
        [ConfigurationProperty("Type", DefaultValue = SoftwareFilterType.Exact, IsRequired = true)]
        public SoftwareFilterType Type
        {
            get
            {
                return ((SoftwareFilterType)(base["Type"]));
            }

            set
            {
                base["Type"] = value;
            }
        }

        /// <summary>
        /// Name of the software field to compare
        /// </summary>
        [ConfigurationProperty("CompareField", IsRequired = true)]
        public string CompareFieldName
        {
            get
            {
                return ((string)(base["CompareField"]));
            }

            set
            {
                base["CompareField"] = value;
            }
        }

        /// <summary>
        /// value to compare against
        /// </summary>
        [ConfigurationProperty("CompareValue", IsRequired = true)]
        public string CompareValue
        {
            get
            {
                return ((string)(base["CompareValue"]));
            }

            set
            {
                base["CompareValue"] = value;
            }
        }
    }
}
