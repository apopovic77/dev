﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Logicx.WpfUtility.CustomControls.OverlayManager;

namespace Logicx.WpfUtility.WpfHelpers
{
    public class UserControlConverterFunctions
    {
        public static RenderTargetBitmap GenerateImage(FrameworkElement ui_element)
        {
            return GenerateImage(ui_element, 0, 0, 96);
        }

        public static RenderTargetBitmap GenerateImage(FrameworkElement ui_element, double dpi)
        {
            return GenerateImage(ui_element, 0, 0, dpi);
        }

        public static RenderTargetBitmap GenerateImage(FrameworkElement ui_element, double offset_width, double offset_height)
        {
            return GenerateImage(ui_element, offset_width, offset_height, 96d);
        }

        public static RenderTargetBitmap GenerateImage(FrameworkElement ui_element, double offset_width, double offset_height, double dpi)
        {
            try
            {
                ui_element.Measure(new Size(ui_element.ActualWidth, ui_element.ActualHeight));
                ui_element.Arrange(new Rect(0, 0, ui_element.ActualWidth, ui_element.ActualHeight));
                ui_element.UpdateLayout();
                //Image myImage = new Image();
                //myImage.Width = ui_element.ActualWidth;
                //myImage.Height = ui_element.ActualHeight;

                double dx, dy;
                try
                {
                    //Matrix m = PresentationSource.FromVisual(Application.Current.MainWindow).CompositionTarget.TransformToDevice;
                    //dx = m.M11 * dpi;
                    //dy = m.M22 * dpi;

                    dx = dpi;
                    dy = dpi;
                }
                catch (Exception ex)
                {
                    dx = dpi;
                    dy = dpi;
                }

                

                RenderTargetBitmap bmp = new RenderTargetBitmap((int)Math.Round((ui_element.ActualWidth + offset_width) * (dx/96d)),
                                                                (int)Math.Round((ui_element.ActualHeight + offset_height) * (dy/96d)), 
                                                                dx, dy,
                                                                PixelFormats.Pbgra32);
               
                bmp.Render(ui_element);



                //if (bmp.CanFreeze)
                //    bmp.Freeze();

#if DEBUG
                //PngBitmapEncoder png = new PngBitmapEncoder();
                //png.Frames.Add(BitmapFrame.Create(bmp));
                //using (Stream stm = File.Create("C:\\temp\\new.png"))
                //{
                //    png.Save(stm);
                //}
#endif


                return bmp;
            
            }
            catch (Exception ex)
            {
                Debug.WriteLine("**** Imagegeneration Error: " + ex.Message);
                return null;
            }
        }

        public static BitmapImage GenerateBitmapImage(FrameworkElement ui_element)
        {
            RenderTargetBitmap rtb = UserControlConverterFunctions.GenerateImage(ui_element);

            if (rtb == null)
                return null;

            //Now encode and convert to a gdi+ Image object
            PngBitmapEncoder png = new PngBitmapEncoder();
            png.Frames.Add(BitmapFrame.Create(rtb));
            //using (MemoryStream stream = new MemoryStream())
            MemoryStream stream = new MemoryStream();
            {
                png.Save(stream);
                BitmapImage bmpImage = new BitmapImage();
                bmpImage.BeginInit();
                bmpImage.StreamSource = stream;
                bmpImage.EndInit();

                if(bmpImage.CanFreeze)
                    bmpImage.Freeze();

                //return System.Drawing.Image.FromStream(stream);
                return bmpImage;
            }
        }

        public static System.Drawing.Image GenerateDrawingImage(FrameworkElement ui_element)
        {
            RenderTargetBitmap rtb = UserControlConverterFunctions.GenerateImage(ui_element);

            //Now encode and convert to a gdi+ Image object
            PngBitmapEncoder png = new PngBitmapEncoder();
            png.Frames.Add(BitmapFrame.Create(rtb));
            //using (MemoryStream stream = new MemoryStream())
            MemoryStream stream = new MemoryStream();
            {
                png.Save(stream);
                System.Drawing.Image retImg = System.Drawing.Image.FromStream(stream);

                //return System.Drawing.Image.FromStream(stream);)
                return retImg;
            }
        }

        #region Native screen shots
        public static void SaveToBitmapNative(Window window, FrameworkElement element, string fileName) 
        {     
            WindowInteropHelper helper = new WindowInteropHelper(window);      
            // detect the window client area position if rendering a child element     
            double incX = 0, incY = 0;     
            if (window != element)
            {
                System.Drawing.Point pos = new System.Drawing.Point(0, 0);         
                ClientToScreen(helper.Handle, ref pos);         
                incX = pos.X - (int)window.Left;         
                incY = pos.Y - (int)window.Top;
            }      
            // transform child position to window coordinates     
            GeneralTransform transform = element.TransformToVisual(window);     
            Point point = transform.Transform(new Point(0, 0));     
            Rect rect = new Rect(point.X + incX, point.Y + incY, element.ActualWidth, element.ActualHeight);      
            
            // render window into bitmap     
            
            using (System.Drawing.Bitmap bitmap = 
                new System.Drawing.Bitmap((int)rect.Width, (int)rect.Height, System.Drawing.Imaging.PixelFormat.Format32bppArgb))     
            {         
                using (System.Drawing.Graphics memoryGraphics = System.Drawing.Graphics.FromImage(bitmap))
                {
                    IntPtr dc = memoryGraphics.GetHdc();             
                    IntPtr windowDC = GetWindowDC(helper.Handle);              
                    BitBlt(dc, 0, 0, (int)rect.Width, (int)rect.Height,                     
                        windowDC, (int)rect.Left, (int)rect.Top, TernaryRasterOperations.SRCCOPY);              
                    
                    memoryGraphics.ReleaseHdc(dc);             
                    ReleaseDC(helper.Handle, windowDC);
                }         
                // save bitmap to file         
                bitmap.Save(fileName);     
            } 
        }  
        
        [DllImport("gdi32.dll")] 
        static extern bool BitBlt(IntPtr hdcDest, int xDest, int yDest, int wDest, int hDest, IntPtr hdcSource, int xSrc, int ySrc, TernaryRasterOperations rop); 
        [DllImport("user32.dll")] 
        public static extern IntPtr GetWindowDC(IntPtr ptr); 
        [DllImport("user32.dll")] 
        static extern bool ReleaseDC(IntPtr hWnd, IntPtr hDc); 
        [DllImport("user32.dll")] 
        static extern bool ClientToScreen(IntPtr hWnd, ref System.Drawing.Point lpPoint);  
        
        public enum TernaryRasterOperations : uint
        {
            SRCCOPY = 0x00CC0020,     
            SRCPAINT = 0x00EE0086,     
            SRCAND = 0x008800C6,     
            SRCINVERT = 0x00660046,     
            SRCERASE = 0x00440328,     
            NOTSRCCOPY = 0x00330008,     
            NOTSRCERASE = 0x001100A6,     
            MERGECOPY = 0x00C000CA,     
            MERGEPAINT = 0x00BB0226,     
            PATCOPY = 0x00F00021,     
            PATPAINT = 0x00FB0A09,     
            PATINVERT = 0x005A0049,     
            DSTINVERT = 0x00550009,     
            BLACKNESS = 0x00000042,     
            WHITENESS = 0x00FF0062
        }
        #endregion
    }
}
