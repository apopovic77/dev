﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CognitiveLib.ApiService;

namespace CognitiveLib.Ocr.Azure
{
    public class AzureOcr
    {
        public static string GetText(string path_to_img)
        {
            dynamic obj = null;
            obj = AzureService.MakeAnalysisRequest(path_to_img, out _);
            return GetTextFromJsonResponse(obj);
        }

        public static string GetText(byte[] data)
        {
            dynamic obj = null;
            obj = AzureService.MakeAnalysisRequest(data, out _);
            return GetTextFromJsonResponse(obj);
        }

        private static string GetTextFromJsonResponse(dynamic obj)
        {
            string analysation_summary = "";
            foreach (dynamic region in obj.regions)
            {
                foreach (dynamic line in region.lines)
                {
                    if (!string.IsNullOrEmpty(analysation_summary))
                        analysation_summary += "\n";
                    string text = "";
                    foreach (dynamic word in line.words)
                    {
                        text += word.text + " ";
                    }
                    analysation_summary += text;
                }
            }
            return analysation_summary;
        }
    }
}
