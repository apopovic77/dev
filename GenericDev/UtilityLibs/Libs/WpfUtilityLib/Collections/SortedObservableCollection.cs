﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Logicx.WpfUtility.Collections
{
    public class SortedObservableCollection<TValue> : SortedCollection<TValue>, INotifyPropertyChanged, INotifyCollectionChanged, IDisposable
    {
        // Events
        public event NotifyCollectionChangedEventHandler CollectionChanged;

        public event PropertyChangedEventHandler PropertyChanged;

        // Methods
        public SortedObservableCollection()
        {
        }

        public SortedObservableCollection(IComparer<TValue> comparer)
            : base(comparer)
        {
        }

        public override void Clear()
        {
            base.Clear();
            this.OnCollectionReset();
        }

        public override void Insert(int index, TValue value)
        {
            base.Insert(index, value);
            this.OnPropertyChanged("Count");
            this.OnPropertyChanged("Item[]");
            this.OnCollectionChanged(NotifyCollectionChangedAction.Add, value, index);
        }

        protected virtual void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
        {
            if (this.CollectionChanged != null)
            {
                this.CollectionChanged(this, e);
            }
        }

        private void OnCollectionChanged(NotifyCollectionChangedAction action, object item, int index)
        {
            this.OnCollectionChanged(new NotifyCollectionChangedEventArgs(action, item, index));
        }

        private void OnCollectionChanged(NotifyCollectionChangedAction action, object item, int index, int oldIndex)
        {
            this.OnCollectionChanged(new NotifyCollectionChangedEventArgs(action, item, index, oldIndex));
        }

        private void OnCollectionChanged(NotifyCollectionChangedAction action, object oldItem, object newItem, int index)
        {
            this.OnCollectionChanged(new NotifyCollectionChangedEventArgs(action, newItem, oldItem, index));
        }

        private void OnCollectionReset()
        {
            this.OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        protected virtual void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, e);
            }
        }

        private void OnPropertyChanged(string propertyName)
        {
            this.OnPropertyChanged(new PropertyChangedEventArgs(propertyName));
        }

        public override void RemoveAt(int index)
        {
            TValue item = this[index];
            base.RemoveAt(index);
            this.OnPropertyChanged("Item[]");
            this.OnPropertyChanged("Count");
            this.OnCollectionChanged(NotifyCollectionChangedAction.Remove, item, index);
        }

        // Properties
        public override TValue this[int index]
        {
            get
            {
                return base[index];
            }
            set
            {
                TValue oldItem = base[index];
                base[index] = value;
                this.OnPropertyChanged("Item[]");
                this.OnCollectionChanged(NotifyCollectionChangedAction.Replace, oldItem, value, index);
            }
        }

        private void UnregisterEvents(NotifyCollectionChangedEventHandler event_handler)
        {
            if (event_handler == null)
                return;

            foreach (NotifyCollectionChangedEventHandler eventDelegate in event_handler.GetInvocationList())
                event_handler -= eventDelegate;

        }

        private void UnregisterEvents(PropertyChangedEventHandler event_handler)
        {
            if (event_handler == null)
                return;

            foreach (PropertyChangedEventHandler eventDelegate in event_handler.GetInvocationList())
                event_handler -= eventDelegate;

        }

        #region IDisposable Members

        public void Dispose()
        {
            Dispose(true);
        }

        private void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if (!this._disposed)
            {
                UnregisterEvents(PropertyChanged);
                UnregisterEvents(CollectionChanged);

                _disposed = true;

            }
        }

        private bool _disposed = false;
        #endregion
    }


}
