﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Security;
using System.Security.Permissions;
using System.Security.Principal;
using System.Text;
using System.Threading;
using LoggingLib.Trace;

namespace LoggingLib.SystemInterop
{
    /// <summary>
    /// The ApplicationStatisticHelper class implements methods for retrieving
    /// application specific information.
    /// </summary>
    public class ApplicationStatisticHelper
    {
        /// <summary>
        /// Gets the current utc date and time
        /// </summary>
        public static DateTime TimeStamp
        {
            get { return DateTime.UtcNow; }
        }

        ///// <summary>
        ///// Gets the activity id of the current activity Thread-Trace
        ///// </summary>
        ///// <remarks>Requires permissions to run unmanaged code</remarks>
        //public static Guid ActivityId
        //{
        //    get
        //    {
        //        try
        //        {
        //            return GetActivityId();
        //        }
        //        catch (Exception)
        //        {
        //            return Guid.Empty;
        //        }
        //    }
        //}

        ///// <summary>
        ///// Gets the activity id of the current activity Thread-Trace
        ///// </summary>
        ///// <remarks>Requires permissions to run unmanaged code</remarks>
        //public static string ActivityDescription
        //{
        //    get
        //    {
        //        try
        //        {
        //            return GetActivityDescription();
        //        }
        //        catch (Exception)
        //        {
        //            return string.Empty;
        //        }
        //    }
        //}

        ///// <summary>
        ///// Gets the related activity id of the current activity Thread-Trace
        ///// </summary>
        ///// <remarks>Requires permissions to run unmanaged code</remarks>
        //public static Guid RelatedActivityId
        //{
        //    get
        //    {
        //        try
        //        {
        //            return GetRelatedActivityId();
        //        }
        //        catch (Exception)
        //        {
        //            return Guid.Empty;
        //        }
        //    }
        //}

        /// <summary>
        /// Gets the machine name
        /// </summary>
        public static string MachineName
        {
            get
            {
                try
                {
                    return Environment.MachineName;
                }
                catch (Exception e)
                {
                    return String.Format(Properties.Resources.Culture, Properties.Resources.PropertyError, "MachineName: " + e.Message);
                }
            }
        }

        /// <summary>
        /// Gets the current application domain name
        /// </summary>
        public static string AppDomainName
        {
            get
            {
                try
                {
                    return AppDomain.CurrentDomain.FriendlyName;
                }
                catch (Exception e)
                {
                    return String.Format(Properties.Resources.Culture, Properties.Resources.PropertyError, "AppDomainName: " + e.Message);
                }
            }
        }

        /// <summary>
        /// Gets the current process id
        /// </summary>
        public static string ProcessId
        {
            get
            {
                if (UnmanagedCodePermissionAvailable)
                {
                    try
                    {
                        return GetCurrentProcessId();
                    }
                    catch (Exception e)
                    {
                        return String.Format(Properties.Resources.Culture, Properties.Resources.PropertyError, "ProcessId: " + e.Message);
                    }
                }
                else
                {
                    return Process.GetCurrentProcess().Id.ToString();
                }
            }
        }

        /// <summary>
        /// Gets the process name
        /// </summary>
        public static string ProcessName
        {
            get
            {
                if (UnmanagedCodePermissionAvailable)
                {
                    try
                    {
                        return GetProcessName();
                    }
                    catch (Exception e)
                    {
                        return String.Format(Properties.Resources.Culture, Properties.Resources.PropertyError, "ProcessName: " + e.Message);
                    }
                }
                else
                {
                    return Process.GetCurrentProcess().ProcessName;
                }
            }
        }

        /// <summary>
        /// Gets the current thread's name
        /// </summary>
        public static string ThreadName
        {
            get
            {
                try
                {
                    return Thread.CurrentThread.Name;
                }
                catch (Exception e)
                {
                    return String.Format(Properties.Resources.Culture, Properties.Resources.PropertyError, "ThreadName: " + e.Message);
                }
            }
        }

        /// <summary>
        /// Gets the current native win32 thread id
        /// </summary>
        /// <remarks>Assembly must have permissions to run unmanaged code. 
        /// If not, the property will return the managed id of the current thread 
        /// instead of the native one.</remarks>
        public static string Win32ThreadId
        {
            get
            {
                if (UnmanagedCodePermissionAvailable)
                {
                    try
                    {
                        return GetCurrentThreadId();
                    }
                    catch (Exception e)
                    {
                        return String.Format(Properties.Resources.Culture, Properties.Resources.PropertyError, "Win32ThreadId: " + e.Message);
                    }
                }
                else
                {
                    // if no unmanaged code permissions are available
                    // return the managed thread id instead of the native win32 thread id
                    return string.Format(Properties.Resources.ManagedThreadId, Thread.CurrentThread.ManagedThreadId);
                }
            }
        }

        /// <summary>
        /// Gets the full qualified class name of the calling method
        /// </summary>
        /// <remarks>Method calls of the ApplicationStatisticHelper and LogManager class will be ignored</remarks>
        public static string CallingClassName
        {
            get
            {
                string className = "";
                MethodBase method = null;

                StackFrame stackFrame = null;
                StackTrace stackTrace = new StackTrace(true);
                for (int i = 0; i < stackTrace.FrameCount; i++)
                {
                    StackFrame sf = stackTrace.GetFrame(i);
                    method = sf.GetMethod();
                    string typeName = method.ReflectedType.Name;
                    if (String.Compare(typeName, "LogManager") != 0 &&
                        String.Compare(typeName, "ApplicationStatisticHelper") != 0)
                    {
                        stackFrame = sf;
                        break;
                    }
                }

                if (stackFrame != null)
                {
                    method = stackFrame.GetMethod();
                    className = method.ReflectedType.AssemblyQualifiedName;
                }

                return className;
            }
        }

        /// <summary>
        /// Gets the name of the calling method
        /// </summary>
        /// <remarks>Method calls of the ApplicationStatisticHelper and LogManager class will be ignored</remarks>
        public static string CallingMethodName
        {
            get
            {
                string methodName = "";
                MethodBase method = null;

                StackFrame stackFrame = null;
                StackTrace stackTrace = new StackTrace(true);
                for (int i = 0; i < stackTrace.FrameCount; i++)
                {
                    StackFrame sf = stackTrace.GetFrame(i);
                    method = sf.GetMethod();
                    string typeName = method.ReflectedType.Name;

                    if (String.Compare(typeName, "LogManager") != 0 &&
                        String.Compare(typeName, "ApplicationStatisticHelper") != 0)
                    {
                        stackFrame = sf;
                        break;
                    }
                }

                if (stackFrame != null)
                {
                    method = stackFrame.GetMethod();
                    methodName = method.Name;
                }

                return methodName;
            }
        }

        /// <summary>
        /// Gets the full signature of the calling method
        /// </summary>
        /// <remarks>Method calls of the ApplicationStatisticHelper and LogManager class will be ignored</remarks>
        public static string CallingMethodSignature
        {
            get
            {
                string methodSignature = "";
                MethodBase method = null;

                StackFrame stackFrame = null;
                StackTrace stackTrace = new StackTrace(false);
                for (int i = 0; i < stackTrace.FrameCount; i++)
                {
                    StackFrame sf = stackTrace.GetFrame(i);
                    method = sf.GetMethod();
                    string typeName = method.ReflectedType.Name;

                    if (String.Compare(typeName, "LogManager") != 0 &&
                        String.Compare(typeName, "ApplicationStatisticHelper") != 0)
                    {
                        stackFrame = sf;
                        break;
                    }
                }

                if (stackFrame != null)
                {
                    method = stackFrame.GetMethod();
                    methodSignature = method.ToString();
                }

                return methodSignature;
            }
        }

        /// <summary>
        /// Gets the current windows user name
        /// </summary>
        public static string CurrentUserName
        {
            get
            {

                WindowsIdentity current_identity = WindowsIdentity.GetCurrent();

                if (current_identity != null)
                    return current_identity.Name;

                return "n/a";
            }
        }

        /// <summary>
        /// Private property for checking if the current assembly is able to run unmanaged code
        /// </summary>
        private static bool UnmanagedCodePermissionAvailable
        {
            get
            {
                if (!_unmanaged_code_permission_available_initialized)
                {
                    // check whether the unmanaged code permission is available to avoid three potential stack walks
                    bool internalUnmanagedCodePermissionAvailable = false;
                    SecurityPermission unmanagedCodePermission = new SecurityPermission(SecurityPermissionFlag.UnmanagedCode);
                    // avoid a stack walk by checking for the permission on the current assembly. this is safe because there are no
                    // stack walk modifiers before the call.
                    if (SecurityManager.IsGranted(unmanagedCodePermission))
                    {
                        try
                        {
                            unmanagedCodePermission.Demand();
                            internalUnmanagedCodePermissionAvailable = true;
                        }
                        catch (SecurityException)
                        { }
                    }

                    UnmanagedCodePermissionAvailable = internalUnmanagedCodePermissionAvailable;
                }

                return _unmanaged_code_permission_available;
            }
            set
            {
                _unmanaged_code_permission_available = value;
                _unmanaged_code_permission_available_initialized = true;
            }
        }

        #region Operations and methods

        /// <summary>
        /// Gets the current process name.
        /// </summary>
        /// <returns>The process name.</returns>
        private static string GetProcessName()
        {
            StringBuilder buffer = new StringBuilder(1024);
            int length = NativeMethods.GetModuleFileName(NativeMethods.GetModuleHandle(null), buffer, buffer.Capacity);
            return buffer.ToString();
        }

        /// <summary>
        /// Gets the current trace activity id
        /// </summary>
        /// <returns>A GUID identifying the current trace activity</returns>
        public static Guid GetActivityId(object scope)
        {
            ActivityInfo ainfo = Trace.LoggingCorrelationManager.GetActivityInfoForScope(scope);

            if (ainfo != null)
                return ainfo.ActivityId;

            return Guid.Empty;
        }

        /// <summary>
        /// Gets the current related activity id
        /// </summary>
        /// <returns>A GUID identifying the related activity</returns>
        public static Guid GetRelatedActivityId(object scope)
        {
            Stack<ActivityInfo> activity_stack = Trace.LoggingCorrelationManager.GetActivityStackForScope(scope);
            if (activity_stack != null)
            {
                ActivityInfo[] activities = activity_stack.ToArray();
                if (activities.Length > 1)
                    return activities[1].ActivityId;
            }
            

            return Guid.Empty;
        }
        /// <summary>
        /// Gets the current activity description
        /// </summary>
        /// <returns></returns>
        public static string GetActivityDescription(object scope)
        {
            ActivityInfo ainfo = Trace.LoggingCorrelationManager.GetActivityInfoForScope(scope);

            if (ainfo != null)
                return ainfo.ActivityDescrpition;

            return string.Empty;
        }

        /// <summary>
        /// Gets the native process id
        /// </summary>
        /// <returns>string representing the current process id</returns>
        /// <remarks>Requires permissions to run unmanaged code</remarks>
        private static string GetCurrentProcessId()
        {
            return NativeMethods.GetCurrentProcessId().ToString(NumberFormatInfo.InvariantInfo);
        }
        /// <summary>
        /// Gets the native thread id
        /// </summary>
        /// <returns>string representing the current thread id</returns>
        /// <remarks>Requires permissions to run unmanaged code</remarks>
        private static string GetCurrentThreadId()
        {
            return NativeMethods.GetCurrentThreadId().ToString(NumberFormatInfo.InvariantInfo);
        }
        #endregion

        #region attribs
        private static bool _unmanaged_code_permission_available = false;
        private static bool _unmanaged_code_permission_available_initialized = false;
        #endregion
    }
}