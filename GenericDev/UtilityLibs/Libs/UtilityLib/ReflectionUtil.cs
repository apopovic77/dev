﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;

namespace Utility
{
    /// <summary>
    /// Utility class providing reflection utilities
    /// </summary>
    public class ReflectionUtil
    {
        /// <summary>
        /// gets the type of a property
        /// </summary>
        /// <param name="objectType">type of the object which implements the property</param>
        /// <param name="propertyName">name of the property</param>
        /// <returns></returns>
        public static Type GetPropertyType(Type objectType, string propertyName)
        {
            foreach (PropertyInfo property in objectType.GetProperties())
            {
                if (property.Name == propertyName)
                {
                    return property.PropertyType;
                }
            }

            return null;
        }

        /// <summary>
        /// Gets the value of an objects' property by name using reflection
        /// </summary>
        /// <param name="obj">Object instance to get the property from</param>
        /// <param name="propertyName">Name of the property (case sensitive)</param>
        /// <returns>Value of the property. null if not found</returns>
        public static object GetPropertyValue(object obj, string propertyName)
        {
            if (obj != null)
            {
                foreach (PropertyInfo property in obj.GetType().GetProperties())
                {
                    if (property.Name == propertyName)
                    {
                        return property.GetValue(obj, null);
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Gets the value of an objects' property by name using reflection
        /// </summary>
        /// <param name="obj">Object instance to get the property from</param>
        /// <param name="propertyName">Name of the property (case sensitive)</param>
        /// <param name="bindingAttr">binding flags to specify property access</param>
        /// <returns>Value of the property. null if not found</returns>
        public static object GetPropertyValue(object obj, string propertyName, BindingFlags bindingAttr)
        {
            if (obj != null)
            {
                foreach (PropertyInfo property in obj.GetType().GetProperties(bindingAttr))
                {
                    if (property.Name == propertyName)
                    {
                        return property.GetValue(obj, null);
                    }
                }
            }

            return null;
        }

        public static void SetPropertyValue(object obj, string propertyName, object value)
        {
            if (obj != null)
            {
                foreach (PropertyInfo property in obj.GetType().GetProperties())
                {
                    if (property.Name == propertyName)
                    {
                        property.SetValue(obj, value, null);
                        return;
                    }
                }
            }
        }

        public static void SetPropertyValue(object obj, string propertyName, object value, BindingFlags bindingAttr)
        {
            if (obj != null)
            {
                foreach (PropertyInfo property in obj.GetType().GetProperties(bindingAttr))
                {
                    if (property.Name == propertyName)
                    {
                        property.SetValue(obj, value, null);
                        return;
                    }
                }
            }
        }

        /// <summary>
        /// gets the type of a field
        /// </summary>
        /// <param name="objectType">type of the object which implements the property</param>
        /// <param name="fieldName">name of the field</param>
        /// <returns></returns>
        public static Type GetFieldType(Type objectType, string fieldName)
        {
            foreach (FieldInfo field in objectType.GetFields())
            {
                if (field.Name == fieldName)
                {
                    return field.FieldType;
                }
            }

            return null;
        }

        /// <summary>
        /// Gets the value of an objects' field by name using reflection
        /// </summary>
        /// <param name="obj">Object instance to get the property from</param>
        /// <param name="fieldName">Name of the property (case sensitive)</param>
        /// <returns>Value of the property. null if not found</returns>
        public static object GetFieldValue(object obj, string fieldName)
        {
            if (obj != null)
            {
                foreach (FieldInfo field in obj.GetType().GetFields())
                {
                    if (field.Name == fieldName)
                    {
                        return field.GetValue(obj);
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Gets the value of an objects' field by name using reflection
        /// </summary>
        /// <param name="obj">Object instance to get the property from</param>
        /// <param name="fieldName">Name of the property (case sensitive)</param>
        /// <param name="bindingAttr">binding flags to specify field access</param>
        /// <returns>Value of the property. null if not found</returns>
        public static object GetFieldValue(object obj, string fieldName, BindingFlags bindingAttr)
        {
            if (obj != null)
            {
                foreach (FieldInfo field in obj.GetType().GetFields(bindingAttr))
                {
                    if (field.Name == fieldName)
                    {
                        return field.GetValue(obj);
                    }
                }
            }

            return null;
        }
    }
}
