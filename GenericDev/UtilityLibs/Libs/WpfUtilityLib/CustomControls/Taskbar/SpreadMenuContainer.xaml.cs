﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;

namespace Logicx.WpfUtility.CustomControls.Taskbar
{
    /// <summary>
    /// Interaktionslogik für SpreadMenuContainer.xaml
    /// </summary>
    public partial class SpreadMenuContainer : UserControl
    {
        public SpreadMenuContainer(SingleSpreadMenu.SpreadDirs spread_dirs)
        {
            InitializeComponent();

            SpreadDirs = spread_dirs;

            if(SpreadDirs == SingleSpreadMenu.SpreadDirs.ToLeft)
            {
                Grid.SetColumn(container_grid, 0);
                Grid.SetColumn(border, 1);
            }
            else
            {
                Grid.SetColumn(container_grid, 1);
                Grid.SetColumn(border, 0);
            }

            container_grid.Width = MAX_WIDTH;
            container_grid.Height = MAX_HEIGHT;
        }


        public void SetActiveStyle(Brush bg_brush, Brush border_brush, Brush foreground_brush)
        {
            if (bg_brush != null)
            {
                _inactive_bg_brush = border.Background;
                border.Background = bg_brush;
            }
            if (border_brush != null)
            {
                _inactive_border_brush = border.BorderBrush;
                border.BorderBrush = border_brush;
            }
            if (foreground_brush != null)
            {
                _inactive_fg_brush = text_block.Foreground;
                text_block.Foreground = foreground_brush;
            }
        }

        public void UnSetActiveStyle()
        {
            if (_inactive_bg_brush != null)
                border.Background = _inactive_bg_brush;
            if (_inactive_border_brush != null)
                border.BorderBrush = _inactive_border_brush;
            if (_inactive_fg_brush != null)
                text_block.Foreground = _inactive_fg_brush;

            //_inactive_bg_brush = null;
            //_inactive_border_brush = null;
            //_inactive_fg_brush = null;
        }

        public void StartCloseAnim(double curve_height, double curve_width)
        {
            DoubleAnimation transl_anim_x = new DoubleAnimation();
            if (SpreadDirs == SingleSpreadMenu.SpreadDirs.ToLeft)
            {
                transl_anim_x.To = -10; // +10 wegen dem margin
                transl_anim_x.From = -10 + (curve_width); // +10 wegen dem margin
            }
            else
            {
                transl_anim_x.To = MAX_WIDTH + 10 - base_grid.ActualWidth; // +10 wegen dem margin
                transl_anim_x.From = (MAX_WIDTH + 10 - base_grid.ActualWidth) + (curve_width); // +10 wegen dem margin
            }
            transl_anim_x.Duration = new Duration(new TimeSpan(0, 0, 0, 0, 500));
            transl_anim_x.AccelerationRatio = 0.3;
            transl_anim_x.DecelerationRatio = 0.7;
            translate_transform.BeginAnimation(TranslateTransform.XProperty, transl_anim_x);

            DoubleAnimation transl_anim_y = new DoubleAnimation();
            transl_anim_y.To = 0;
            transl_anim_y.From = curve_height;
            transl_anim_y.Duration = new Duration(new TimeSpan(0, 0, 0, 0, 500));
            transl_anim_y.AccelerationRatio = 0.5;
            transl_anim_y.DecelerationRatio = 0.5;
            translate_transform.BeginAnimation(TranslateTransform.YProperty, transl_anim_y);

            DoubleAnimation rot_anim = new DoubleAnimation();
            rot_anim.To = 0;
            double alpha_deg = Math.Atan(curve_width / curve_height) * (180 / Math.PI);
            rot_anim.From = -(alpha_deg) * 1.7;
            rot_anim.Duration = new Duration(new TimeSpan(0, 0, 0, 0, 500));
            rot_anim.AccelerationRatio = 0.5;
            rot_anim.DecelerationRatio = 0.5;
            rotate_transform.BeginAnimation(RotateTransform.AngleProperty, rot_anim);
        }

        public void StartOpenAnim(double curve_height, double curve_width)
        {
            DoubleAnimation transl_anim_x = new DoubleAnimation();
            if (SpreadDirs == SingleSpreadMenu.SpreadDirs.ToLeft)
            {
                transl_anim_x.From = -10; // +10 wegen dem margin
                transl_anim_x.To = -10 + (curve_width); // +10 wegen dem margin
            }
            else
            {
                transl_anim_x.From = MAX_WIDTH + 10 - base_grid.ActualWidth; // +10 wegen dem margin
                transl_anim_x.To = (MAX_WIDTH + 10 - base_grid.ActualWidth) + (curve_width); // +10 wegen dem margin
            }
            transl_anim_x.Duration = new Duration(new TimeSpan(0, 0, 0, 0, 500));
            transl_anim_x.AccelerationRatio = 0.7;
            transl_anim_x.DecelerationRatio = 0.3;
            translate_transform.BeginAnimation(TranslateTransform.XProperty, transl_anim_x);

            DoubleAnimation transl_anim_y = new DoubleAnimation();
            transl_anim_y.From = 0;
            transl_anim_y.To = curve_height;
            transl_anim_y.Duration = new Duration(new TimeSpan(0, 0, 0, 0, 500));
            transl_anim_y.AccelerationRatio = 0.5;
            transl_anim_y.DecelerationRatio = 0.5;
            translate_transform.BeginAnimation(TranslateTransform.YProperty, transl_anim_y);

            DoubleAnimation rot_anim = new DoubleAnimation();
            rot_anim.From = 0;
            double alpha_deg = Math.Atan(curve_width / curve_height) * (180 / Math.PI);
            rot_anim.To = -(alpha_deg) * 1.7;
            rot_anim.Duration = new Duration(new TimeSpan(0, 0, 0, 0, 500));
            rot_anim.AccelerationRatio = 0.5;
            rot_anim.DecelerationRatio = 0.5;
            rotate_transform.BeginAnimation(RotateTransform.AngleProperty, rot_anim);
        }

        #region EventHandler
        private void base_grid_MouseEnter(object sender, MouseEventArgs e)
        {
            border.Tag = border.Background;
            border.Background = (Brush)FindResource("colored_3dbg_brush");
        }

        private void base_grid_MouseLeave(object sender, MouseEventArgs e)
        {
            if (border.Tag != null)
                border.Background = (Brush)border.Tag;
            else
                border.Background = (Brush)FindResource("gray_3dbg_brush");
        }

        private void base_grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            border.Background = (Brush)FindResource("colored_bg_brush");
        }

        private void base_grid_MouseUp(object sender, MouseButtonEventArgs e)
        {
            border.Background = (Brush)FindResource("colored_3dbg_brush");
        }
        #endregion

        public const double MAX_WIDTH = 40;
        public const double MAX_HEIGHT = 40;
        protected SingleSpreadMenu.SpreadDirs SpreadDirs;
        protected Brush _inactive_bg_brush;
        protected Brush _inactive_fg_brush;
        protected Brush _inactive_border_brush;
    }
}



