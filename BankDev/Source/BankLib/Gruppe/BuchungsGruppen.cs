﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using BankLib.Data;

namespace BankLib.Gruppe
{
    public class BuchungsGruppen 
    {
        public static void ClearAddedBuchungen()
        {
            ClearAddedBuchungen(BgTop);
        }

        private static void ClearAddedBuchungen(BuchungsGruppe bg_top)
        {
            if (bg_top == null)
                return;
            
            if( bg_top.BgZuos() != null)
                foreach (BgZuo bg_zuo in bg_top.BgZuos())
                    bg_zuo.Buchungen = null;

            foreach (BuchungsGruppe bg in bg_top.Children())
                ClearAddedBuchungen(bg);
        }

        /// <summary>
        /// added eine ganze liste von buchungen in die gruppen
        /// </summary>
        /// <param name="buchungen"></param>
        /// <returns>gibt alle buchungen zurück die keinen gruppen zugeordnet werden konnten</returns>
        private static List<Buchung> AddBuchungen(List<Buchung> buchungen )
        {
            return AddBuchungen(buchungen, false);
        }
        public static List<Buchung> AddBuchungen(List<Buchung> buchungen, bool replace_existing)
        {
            if(replace_existing)
                ClearAddedBuchungen();

            List<Buchung> not_added_buchungen = new List<Buchung>(3);
            foreach (Buchung b in buchungen)
            {
                if (b.UmsatzType == "UNSER KIND" && b.Datum > new DateTime(2014, 8, 1))
                {
                    Console.WriteLine(b);
                }

                if (!AddBuchung(b))
                    not_added_buchungen.Add(b);
            }

            //im tree wurden buchungen hinzugefügt, der tree is dirty aktualisiere alle werte
            UpdateTreeSum(BgTop);

            return not_added_buchungen;
        }


        /// <summary>
        /// diese methode sucht die passende gruppe und fügt sie ein
        /// </summary>
        /// <returns>true if success</returns>
        public static bool AddBuchung(Buchung b)
        {
            //iterate through bg_zuo rule priority
            foreach (int key in _bgzuos.Keys.OrderBy(i => i))
            {
                //iterate through bg_zuo from rule_priority [key]
                foreach (BgZuo bg_zuo in _bgzuos[key])
                {
                    ////if(bg_zuo is BgZuoIndexOf && ((BgZuoIndexOf)bg_zuo).SearchStringRaw=="")
                    //if(b.UmsatzType.IndexOf("STEX")>=0)
                    //    Console.WriteLine("");
                    bool added = bg_zuo.AddBuchungIfFit(b);
                    if (added)
                        return true;
                }
            }
            return false;
        }

        private static float UpdateTreeSum(BuchungsGruppe bg)
        {
            if (bg == null)
                return 0;

            float children_sum_betrag = 0;
            if (bg.HasChildren())
                foreach (BuchungsGruppe bg_child in bg.Children())
                {
                    float child_sum_betrag = UpdateTreeSum(bg_child);
                    children_sum_betrag += child_sum_betrag;
                }

            //i am a leaf, sum up
            if (bg.BgZuos() != null)
                bg.SumBetrag = bg.BgZuos().Where(b => b.Buchungen != null).Sum(b => b.SumBetrag);
            
            bg.SumBetrag += children_sum_betrag;
            return bg.SumBetrag;
        }



        #region Database File IO
        public static void SaveGruppenToFile()
        {
            if (BgTop == null) 
               return;
            SaveGruppenToFile(BgTop, PathToGruppenFile);
        }

        public static void SaveGruppenToFile(BuchungsGruppe bg_top, string path_to_gruppen_file)
        {
            if (bg_top.Children().Count == 0)
                return;

            if (File.Exists(path_to_gruppen_file))
                File.Delete(path_to_gruppen_file);

            using (BinaryWriter writer = new BinaryWriter(File.Open(path_to_gruppen_file, FileMode.CreateNew)))
                bg_top.WriteToStream(writer);
        }

        public static void LoadGruppenFromFile()
        {
            LoadGruppenFromFile(PathToGruppenFile);
        }

        public static void LoadGruppenFromFile(string path_to_gruppen_file)
        {
            if (!File.Exists(path_to_gruppen_file))
                return;

            using (BinaryReader reader = new BinaryReader(File.Open(path_to_gruppen_file, FileMode.Open)))
            {
                _bg_top = new BuchungsGruppe();
                _bg_top.LoadFromStream(reader);
                AddUnbekanntBuchungsGruppen();
            }

            //load bg_zuos
            _bgzuos = BgTop.ExtractBgZuosFlatList();
        }

        private static void AddUnbekanntBuchungsGruppen()
        {
            //if (FindBuchungsGruppeByName("Unbekannte variable Kosten") == null)
            //{
            //    _bg_unbekannte_varkosten = new BuchungsGruppe();
            //    _bg_unbekannte_varkosten.Name = "Unbekannte variable Kosten";
            //    _bg_unbekannte_varkosten.Beschreibung = "Hier landen alle Kosten Buchungen die in keine andere Gruppe eingeordnet werden konnten.";
            //    _bg_unbekannte_varkosten.AddBgZuo(new BgZuoUnknownCost());

            //    BuchungsGruppe bg_var_kost = FindBuchungsGruppeByName("Variablekosten");
            //    bg_var_kost.AddChild(_bg_unbekannte_varkosten);
            //}

            //if (FindBuchungsGruppeByName("Unbekannte variable Einnahmen") == null)
            //{
            //    _bg_unbekannte_einnahmen = new BuchungsGruppe();
            //    _bg_unbekannte_einnahmen.Name = "Unbekannte variable Einnahmen";
            //    _bg_unbekannte_einnahmen.Beschreibung = "Hier landen alle Einnahmen Buchungen die in keine andere Gruppe eingeordnet werden konnten.";
            //    _bg_unbekannte_einnahmen.AddBgZuo(new BgZuoUnknownIncome());

            //    BuchungsGruppe bg_einnahmen = FindBuchungsGruppeByName("Einnahmen");
            //    bg_einnahmen.AddChild(_bg_unbekannte_einnahmen);
            //}
        }
        #endregion


        private static BuchungsGruppe FindBuchungsGruppeByName(string find_name)
        {
            return FindBuchungsGruppeByName(BgTop,find_name);
        }

        private static BuchungsGruppe FindBuchungsGruppeByName(BuchungsGruppe bg_top, string find_name)
        {
            if (bg_top == null)
                return null;
            if (bg_top.Name == find_name)
                return bg_top;
            if (!bg_top.HasChildren())
                return null;

            foreach (var bg in bg_top.Children())
            {
                BuchungsGruppe bg_found = FindBuchungsGruppeByName(bg, find_name);
                if (bg_found != null)
                    return bg_found;
            }
            return null;
        }

        public static BuchungsGruppe BgTop
        {
            get
            {
                return _bg_top;
            }
            set
            {
                _bg_top = value;
                if (value == null)
                    return;
                _bg_top.ExtractBgZuosFlatList();
                AddUnbekanntBuchungsGruppen();
            }
        }



        private static BuchungsGruppe _bg_unbekannte_varkosten;
        private static BuchungsGruppe _bg_unbekannte_einnahmen;
        private static BuchungsGruppe _bg_top;
        private static Dictionary<int, List<BgZuo>> _bgzuos; 
        public const string PathToGruppenFile = AppHelper.PathToBankData+"BuchungsGruppen.dat";
    }
}
