﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Logicx.WpfUtility.CustomControls
{
    public interface IOneLevelTreeViewItem
    {
        List<IOneLevelTreeViewItem> ChildrenList { set; get; }
        string ItemHeader
        { 
            get;
        }
    }
}
