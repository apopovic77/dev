﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using LoggingLib.Interfaces;

namespace LoggingLib.Configuration
{
    /// <summary>
    /// Configuration section class which describes the configuration section
    /// used bei the configuration manager to load data from the app.config file
    /// </summary>
    public class LoggingConfigSection : ConfigurationSection
    {
        #region Configuration methods
        /// <summary>
        /// Gets the connection string depending on the property values
        /// </summary>
        /// <returns>Returns the connection string</returns>
        public string GetConnectionString()
        {
            if (!string.IsNullOrEmpty(ConnectionString))
                return ConnectionString;

            if (!string.IsNullOrEmpty(ConnectionStringProvider))
            {
                Type connStringProviderType = Type.GetType(ConnectionStringProvider);

                if(connStringProviderType != null)
                {
                    IConnectionStringProvider provider = Activator.CreateInstance(connStringProviderType) as IConnectionStringProvider;

                    if (provider == null)
                        throw new NotSupportedException("The supplied provider class does not implement the IConnectionStringProvider interface!");

                    return provider.ConnectionString;
                }
            }

            return string.Empty;
        }
        #endregion

        #region Configuration properties
        /// <summary>
        /// The product or application name which is using the logging.
        /// </summary>
        /// <remarks>You may use this parameter to allow multiple applications log into the same database tables.</remarks>
        [ConfigurationProperty("ProductName", DefaultValue = "LogicxLogging", IsRequired = true)]
        public string ProductName
        {
            get { return (string)this["ProductName"]; }
            set { this["ProductName"] = value; }
        }

        /// <summary>
        /// Filename used for serialization of data context change sets
        /// </summary>
        /// <remarks>This file will automatically be stored in the LocalapplicationData folder.</remarks>
        [ConfigurationProperty("SerializationFileName", DefaultValue = "LoggingChangeSetData.xml", IsRequired = true)]
        public string SerializationFileName
        {
            get { return (string)this["SerializationFileName"]; }
            set { this["SerializationFileName"] = value; }
        }

        /// <summary>
        /// The connection string used by the logging DataContext
        /// </summary>
        /// <remarks>You may also use the ConnectionStringProvider property which allows you to retrieve the 
        /// connection string synamically by code.</remarks>
        [ConfigurationProperty("ConnectionString", DefaultValue = "", IsRequired = false)]
        public string ConnectionString
        {
            get { return (string)this["ConnectionString"]; }
            set { this["ConnectionString"] = value; }
        }

        /// <summary>
        /// The connection string provider full qualified type name
        /// </summary>
        [ConfigurationProperty("ConnectionStringProvider", DefaultValue = "", IsRequired = false)]
        public string ConnectionStringProvider
        {
            get { return (string)this["ConnectionStringProvider"]; }
            set { this["ConnectionStringProvider"] = value; }
        }

        /// <summary>
        /// The number of changes to the logging DataContext required to trigger
        /// an update to the database
        /// </summary>
        /// <remarks>If you set this value to 0, all logging will be directly send to the
        /// database (offline logging will be disabled).<br/>
        /// Maximum value is 10000.</remarks>
        [ConfigurationProperty("NumberOfChangesRequiredForDatabaseUpdate", DefaultValue = 200, IsRequired = false)]
        [IntegerValidator(MinValue = 0, MaxValue = 10000)]
        public int NumberOfChangesRequiredForDatabaseUpdate
        {
            get { return (int)this["NumberOfChangesRequiredForDatabaseUpdate"]; }
            set { this["NumberOfChangesRequiredForDatabaseUpdate"] = value; }
        }

        /// <summary>
        /// The number of changes to the logging DataContext required to trigger
        /// an update to the database after the first update attempt failed
        /// </summary>
        /// <remarks>If you set this value to 0, the logging system will use the value of <see cref="NumberOfChangesRequiredForDatabaseUpdate">NumberOfChangesRequiredForDatabaseUpdate</see>
        /// for triggering the next update attempt.<br/>
        /// Maximum value is 1000.</remarks>
        [ConfigurationProperty("NumberOfChangesRequiredAfterFailedUpdate", DefaultValue = 50, IsRequired = false)]
        [IntegerValidator(MinValue = 0, MaxValue = 1000)]
        public int NumberOfChangesRequiredAfterFailedUpdate
        {
            get { return (int)this["NumberOfChangesRequiredAfterFailedUpdate"]; }
            set { this["NumberOfChangesRequiredAfterFailedUpdate"] = value; }
        }

        /// <summary>
        /// The number of maximal minutes between submission tries
        /// </summary>
        /// <remarks>If you set this value to 0, the timed log submission will be disabled.<br/>
        /// Default value is 10 minutes. <br/>
        /// Maximum value is 120 minutes (2 hours).</remarks>
        [ConfigurationProperty("TimedSubmissionTriggerMinutes", DefaultValue = 10, IsRequired = false)]
        [IntegerValidator(MinValue = 0, MaxValue = 120)]
        public int TimedSubmissionTriggerMinutes
        {
            get { return (int)this["TimedSubmissionTriggerMinutes"]; }
            set { this["TimedSubmissionTriggerMinutes"] = value; }
        }

        /// <summary>
        /// The value of the property here "Categories" needs to match that of the config file section
        /// </summary>
        [ConfigurationProperty("Categories")]
        public CategoriesCollection Categories
        {
            get { return ((CategoriesCollection)(base["Categories"])); }
        }

        /// <summary>
        /// The value of the property here "LogFilter" needs to match that of the config file section
        /// </summary>
        [ConfigurationProperty("LogFilter", IsRequired = false)]
        public LogFilterElement LogFilter
        {
            get { return ((LogFilterElement)(base["LogFilter"])); }
        }

        /// <summary>
        /// The value of the property here "SoftwareScanFilter" needs to match that of the config file section
        /// </summary>
        [ConfigurationProperty("SoftwareScanFilter", IsRequired = false)]
        public SwFilterRuleCollection SoftwareScanFilter
        {
            get { return ((SwFilterRuleCollection)(base["SoftwareScanFilter"])); }
        }

        /// <summary>
        /// The value of the property here "LogFormatters" needs to match that of the config file section
        /// </summary>
        [ConfigurationProperty("LogFormatters")]
        public LogFormatterCollection LogFormatters
        {
            get { return ((LogFormatterCollection)(base["LogFormatters"])); }
        }

        /// <summary>
        /// The value of the property here "DataFinalizers" needs to match that of the config file section
        /// </summary>
        [ConfigurationProperty("DataFinalizers")]
        public DataFinalizersCollection DataFinalizers
        {
            get { return ((DataFinalizersCollection)(base["DataFinalizers"])); }
        }

        /// <summary>
        /// The value of the property here "DataFinalizerConfigSettings" needs to match that of the config file section
        /// </summary>
        [ConfigurationProperty("DataFinalizerConfigSettings")]
        public KeyValueConfigurationCollection DataFinalizerConfigSettings
        {
            get { return ((KeyValueConfigurationCollection)(base["DataFinalizerConfigSettings"])); }
        }

        #endregion
    }
}
