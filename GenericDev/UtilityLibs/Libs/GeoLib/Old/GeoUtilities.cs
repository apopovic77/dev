using System;
using System.Collections;
using MathLib;


namespace GeoLib
{

	/// <summary>
	/// 
	/// </summary>
	public class GeoUtilities
	{
//		public struct Vector{
//			public Vector(double longitude,double latitude){
//				this.X = longitude;
//				this.Y = latitude;
//			}
//			public double longitude;
//			public double latitude;
//			public override string ToString() {
//				return "long="+longitude+"; lat="+latitude+";";
//			}
//
//		}
//
//		public struct Vector{
//			public Vector(double x,double y){
//				this.X = x;
//				this.Y = y;
//			}
//			public double x;
//			public double y;
//
//			public override string ToString() {
//				return "x="+x+"; y="+y+";";
//			}
//
//		}
//		public static Vector[] GetRectAroundLine(Vector a,Vector b,double radius){
//			
//			Vector dist = b-a;
//
//			Vector dist_normiert = dist.GetNorm();
//			Vector dist_normiert_gespiegelt = dist_normiert.GetMirror();
//			Vector orthogonall = dist_normiert_gespiegelt.GetOrthogonal1();
//			Vector orthogonal2 = dist_normiert_gespiegelt.GetOrthogonal2();
//
//			Vector[] poly = new Vector[5];
//			poly[0].X = a.X +  (dist_normiert_gespiegelt.X*radius) + (orthogonall.X*radius);
//			poly[0].Y = a.Y +  (dist_normiert_gespiegelt.Y*radius) + (orthogonall.Y*radius);
//
//			poly[1].X = a.X +  (dist_normiert_gespiegelt.X*radius) + (orthogonal2.X*radius);
//			poly[1].Y = a.Y +  (dist_normiert_gespiegelt.Y*radius) + (orthogonal2.Y*radius);
//
//			poly[2].X = b.X +  (dist_normiert.X*radius) + (orthogonal2.X*radius);
//			poly[2].Y = b.Y +  (dist_normiert.Y*radius) + (orthogonal2.Y*radius);
//
//			poly[3].X = b.X +  (dist_normiert.X*radius) + (orthogonall.X*radius);
//			poly[3].Y = b.Y +  (dist_normiert.Y*radius) + (orthogonall.Y*radius);
//
//			poly[4].X = poly[0].X;
//			poly[4].Y = poly[0].Y;
//
//			return poly;
//		}

		public static bool PointInPolygon(Vector2d p, Vector2d[] polygon){
			double x = p.X;
			double y = p.Y;
			bool c = false;

			for (int i = 0, j = polygon.Length-1; i < polygon.Length; j = i++) {
				if ((((polygon[i].Y <= y) && (y < polygon[j].Y)) || ((polygon[j].Y <= y) && (y < polygon[i].Y))) && (x < (polygon[j].X - polygon[i].X) * (y - polygon[i].Y) / (polygon[j].Y - polygon[i].Y) + polygon[i].X))
					c = !c;
			}
			return c;
		}

		private static bool PointInPolygon2(Vector2d p, Vector2d[] polygon){
			int i;
			double angle=0;
			int n = polygon.Length;
			Vector2d p1 = new Vector2d(0,0);
			Vector2d p2 = new Vector2d(0,0);

			for (i=0;i<n;i++) {
				p1.X = polygon[i].X - p.X;
				p1.Y  = polygon[i].Y - p.Y;
				p2.X = polygon[(i+1)%n].X - p.X;
				p2.Y  = polygon[(i+1)%n].Y - p.Y;
				angle += Angle2D(p1.X,p1.Y,p2.X,p2.Y);
			}

			if (Math.Abs(angle) < Math.PI)
				return false;
			else
				return true;
		}

		/*
		   Return the angle between two vectors on a plane
		   The angle is from vector 1 to vector 2, positive anticlockwise
		   The result is between -pi -> pi
		*/
		protected static double Angle2D(double x1, double y1, double x2, double y2) {
			double dtheta,theta1,theta2;

			theta1 = Math.Atan2(y1,x1);
			theta2 = Math.Atan2(y2,x2);
			dtheta = theta2 - theta1;
			while (dtheta > Math.PI)
				dtheta -= (2*Math.PI);
			while (dtheta < -Math.PI)
				dtheta += (2*Math.PI);

			return(dtheta);
		}


//		/// <summary>
//		/// North is positive
//		/// East is positive
//		/// </summary>
//		/// <param name="from"></param>
//		/// <param name="to"></param>
//		/// <returns></returns>
//		public static double CalculateDistance(GeoCoordinates from, GeoCoordinates to){
//			//ave. radius = 6371.315 (someone said more accurate is 6366.707)
//			//equatorial radius = 6378.388
//			//nautical mile = 1.15078
//			double avg_earth_radius = 6366.707;
//			double radlat1 = System.Math.PI * from.Latitude / 180;
//			double radlat2 = System.Math.PI * to.Latitude /180;
//			double radlong1 = Math.PI * from.Longitude / 180;
//			double radlong2 = Math.PI * to.Longitude / 180;
//			
//			//spherical coordinates x=r*cos(ag)sin(at), y=r*sin(ag)*sin(at), z=r*cos(at)
//			//zero ag is up so reverse lat
////			if (tr[nq]=="N")
//			radlat1=Math.PI/2-radlat1;
////			if (tr[nq]=="S")radlat1=Math.PI/2+radlat1;
////			if (gr[nq]=="W")
////			radlong1=Math.PI*2-radlong1;
////
////			if (tr[nj]=="N")
//			radlat2=Math.PI/2-radlat2;
////			if (tr[nj]=="S")radlat2=Math.PI/2+radlat2;
////			if (gr[nj]=="W")radlong2=Math.PI*2-radlong2;
//
//			double x1 = avg_earth_radius * Math.Cos(radlong1)*Math.Sin(radlat1);
//			double y1 = avg_earth_radius * Math.Sin(radlong1)*Math.Sin(radlat1);
//			double z1 = avg_earth_radius * Math.Cos(radlat1);
//
//			double x2 = avg_earth_radius * Math.Cos(radlong2)*Math.Sin(radlat2);
//			double y2 = avg_earth_radius * Math.Sin(radlong2)*Math.Sin(radlat2);
//			double z2 = avg_earth_radius * Math.Cos(radlat2);
//
//			double d = Math.Sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2)+(z1-z2)*(z1-z2));
//
//			//side, side, side, law of cosines and arccos
//			double theta = Math.Acos((avg_earth_radius*avg_earth_radius+avg_earth_radius*avg_earth_radius-d*d)/(2*avg_earth_radius*avg_earth_radius));
//			double distance = theta*avg_earth_radius;
//
//			return distance; 
//			//return distance/1.609344; 
//			//return distance/1.852; 
//		}

//		public static double GetLen(Vector a){
//			return Math.Sqrt(Math.Pow(a.X,2)+Math.Pow(a.Y,2));
//		}
//		public static Vector GetNorm(Vector a){
//			Vector b = new Vector(0,0);
//			double len_kehrwert = (1/GetLen(a));
//			b.X = a.X * len_kehrwert;
//			b.Y = a.Y * len_kehrwert;
//			return b;
//		}
//		public static double GetSkalarProduct(Vector a,Vector b){
//			return a.X*b.X+a.Y*b.Y;
//		}
//		public static Vector GetMirror(Vector a){
//			Vector b = new Vector(0,0);
//			b.X = (-a.X);
//			b.Y = (-a.Y);
//			return b;
//		}
//		public static Vector GetOrthogonal1(Vector a){
//			Vector b = new Vector(0,0);
//			b.X = (-a.Y);
//			b.Y = (a.X);
////			if((-a.X)>b.Y)
//				return b;
////			b.X = (a.Y);
////			b.Y = (-a.X);
////			return b;
//		}
//		public static Vector GetOrthogonal2(Vector a){
//			Vector b = new Vector(0,0);
//			b.X = (a.Y);
//			b.Y = (-a.X);
////			if(a.X<b.Y)
//				return b;
////			b.X = (-a.Y);
////			b.Y = (a.X);
////			return b;
//		}
//		public static Vector OperationAddition(Vector a,Vector b){
//			Vector c = new Vector(0,0);
//			c.X = a.X + b.X;
//			c.Y = a.Y + b.Y;
//			return c;
//		}
//		public static Vector OperationMul(Vector a,double skalar){
//			return new Vector(a.X*skalar,a.Y*skalar);
//		}
//		public static Vector OperationSub(Vector a, Vector b){
//			Vector c = new Vector(a.X-b.X,a.Y-b.Y);
//			return c;
//		}




		
		public static Vector2d debug1 = new Vector2d(0,0);
		public static Vector2d debug2 = new Vector2d(0,0);




	}
}
