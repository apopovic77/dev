﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PoCShared.Core
{
    public abstract class HardwareItem
    {
        public abstract string Type { get; set; }
        public string Name { get; set; }
    }
}
