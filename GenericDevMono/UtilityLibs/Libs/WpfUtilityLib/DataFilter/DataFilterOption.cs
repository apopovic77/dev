﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Logicx.WpfUtility.DataFilter
{
    [Serializable]
    [XmlInclude(typeof(DataFilterOption<int>)), XmlInclude(typeof(DataFilterOption<string>))]
    public abstract class DataFilterOption
    {
        public abstract void RegisterForPropertiesChanged();
        public abstract void DeRegisterForPropertiesChanged();

        protected void RaiseEvent(object sender, PropertyChangedEventArgs e)
        {
            if (PropertyChanged != null)
                PropertyChanged(sender, e);
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }

    [Serializable]
    public class DataFilterOption<T> : DataFilterOption
    {
        #region Construction
        public DataFilterOption()
        {
        }

        public DataFilterOption(List<T> values) : this(values, true)
        {
        }

        public DataFilterOption(List<T> values, bool selection_presetting)
        {
            _data_filter_properties = new List<DataFilterProperty<T>>(values.Count);
            for (int i = 0; i < values.Count; i++)
            {
                DataFilterProperty<T> dfp = new DataFilterProperty<T>(values[i], selection_presetting);
                dfp.PropertyChanged += DataFilterProperty_SelectionChanged;
                _data_filter_properties.Add(dfp);
            }
        }
        #endregion

        public void Add(DataFilterProperty<T> data_filter_property)
        {
            data_filter_property.PropertyChanged += DataFilterProperty_SelectionChanged;
            _data_filter_properties.Add(data_filter_property);
        }

        public void Remove(DataFilterProperty<T> data_filter_property)
        {
            data_filter_property.PropertyChanged -= DataFilterProperty_SelectionChanged;
            _data_filter_properties.Remove(data_filter_property);
        }

        public List<T> ToList()
        {
            List<T> list = new List<T>();
            for (int i = 0; i < _data_filter_properties.Count; i++)
            {
                list.Add(_data_filter_properties[i].Value);
            }
            return list;
        }

        public bool Contains(DataFilterProperty<T> prop)
        {
            return _data_filter_properties.Contains(prop);
        }

        public void Sort()
        {
            _data_filter_properties.Sort();
        }


        #region Event Handlers
        public override void RegisterForPropertiesChanged()
        {
            for (int i = 0; i < _data_filter_properties.Count; i++)
            {
                DataFilterProperty<T> dfp = _data_filter_properties[i];
                dfp.PropertyChanged += DataFilterProperty_SelectionChanged;
            }
        }

        public override void DeRegisterForPropertiesChanged()
        {
            for (int i = 0; i < _data_filter_properties.Count; i++)
            {
                DataFilterProperty<T> dfp = _data_filter_properties[i];
                dfp.PropertyChanged -= DataFilterProperty_SelectionChanged;
            }
        }

        protected void DataFilterProperty_SelectionChanged(object sender, PropertyChangedEventArgs e)
        {
            RaiseEvent(sender,e);
        }
        #endregion

        #region Properties
        [XmlIgnore]        
        public int Count
        {
            get
            {
                if (_data_filter_properties == null)
                    return 0;
                return _data_filter_properties.Count;
            }
        }
        [XmlIgnore]
        public DataFilterProperty<T> this[int index]
        {
            get
            {
                return _data_filter_properties[index];
            }
            set
            {
                _data_filter_properties[index] = value;
            }
        }
        public List<DataFilterProperty<T>> DataFilterProperies
        {
            get { return _data_filter_properties; }
            set
            {
                _data_filter_properties = value;
                RegisterForPropertiesChanged();
            }
        }
        #endregion

        #region Attributes
        protected List<DataFilterProperty<T>> _data_filter_properties;
        #endregion
    }
}
