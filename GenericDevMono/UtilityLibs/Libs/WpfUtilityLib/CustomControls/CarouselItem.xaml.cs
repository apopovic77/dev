﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Logicx.WpfUtility.CustomControls {
    /// <summary>
    /// Interaktionslogik für CarouselItem.xaml
    /// </summary>
    public partial class CarouselItem : UserControl {
        public CarouselItem(Carousel parent) {
            InitializeComponent();
            this.TBAngle.Visibility = (_debugVisible ? System.Windows.Visibility.Visible : System.Windows.Visibility.Hidden);
            this.TBX.Visibility = (_debugVisible ? System.Windows.Visibility.Visible : System.Windows.Visibility.Hidden);
            this.TBY.Visibility = (_debugVisible ? System.Windows.Visibility.Visible : System.Windows.Visibility.Hidden);
            _car = parent;
        }

        private void OnMouseClick(object sender, MouseButtonEventArgs e) {
            _car.OnCarouselClick(sender, e);
        }

        private void OnMouseWheel(object sender, MouseWheelEventArgs e) {
            _car.OnMouseWheel(sender, e);
        }

        private void OnMouseMove(object sender, MouseEventArgs e) {
            if (e.LeftButton == MouseButtonState.Pressed) {
                _car.OnMouseMove(sender, e);
            }
        }

        public float Scale {
            get { return _scale; }
            set { _scale = value; }
        }

        public float Angle {
            get { return _angle; }
            set {
                _angle = value;
                this.TBAngle.Text = "A:" + _angle;
            }
        }

        public string Path {
            get { return _path; }
            set {
                _path = value;
                SetLabel();
            }
        }

        public string Target {
            get { return _target; }
            set {
                _target = value;
            }
        }

        public Point ActPos {
            get { return _actPos; }
            set {
                _actPos = value;
                this.TBX.Text = "X:" + _actPos.X;
                this.TBY.Text = "Y:" + _actPos.Y;
            }
        }

        public string Itemname {
            get { return _itemName; }
            set { _itemName = value; }
        }

        private void SetLabel() {
            try {
                //this.Label.Text = _path.Substring(_path.LastIndexOf("-") + 1);
                string path_name = _path.Substring(_path.LastIndexOf("\\") + 1);
                this.Label.Text = path_name.Substring(path_name.IndexOf("-")+1);
            }
            catch (Exception ex) {
            }
        }

        #region Attributes
        Carousel _car;
        float _scale;
        float _angle;
        Point _actPos;
        bool _debugVisible = false;
        string _path;
        string _target;
        string _itemName;
        #endregion
    }
}
