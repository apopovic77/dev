﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Logicx.Geo.Algorithms;
using Logicx.Geo.Geometries;
using Logicx.WpfUtility.WpfHelpers;
using MathLib;

namespace Logicx.WpfUtility.CustomControls
{
    public enum FreeSelectionMode
    {
        Free = 1,
        ConvexHull=2
    }

    public class FreeSelectionEventArgs : EventArgs
    {
        public FreeSelectionEventArgs(List<Vector2d> vertices)
        {
            Vertices = vertices;
        }

        public List<Vector2d> Vertices;
    }

    /// <summary>
    /// Interaction logic for FreeSelector.xaml
    /// </summary>
    public partial class FreeSelector : UserControl
    {
        public FreeSelector()
        {
            InitializeComponent();
            path.StrokeDashArray = new DoubleCollection() { 3, 1 };

        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            if(!_sel_active)
                return;

            Vector2d curr_pos = GetVector2d(e.GetPosition(this));
            Vector2d last_pos = _vertices[_vertices.Count-1];

            double dist_last_pos = (last_pos - curr_pos).GetLen();
            if(dist_last_pos < 4)
                return;

            _vertices.Add(curr_pos);

            if (_free_selection_mode == FreeSelectionMode.ConvexHull)
                if(_vertices.Count() > 3)
                    _vertices = PolyUtilities.GetConvexHull(_vertices.OrderBy(a=>a.Y).OrderBy(a=>a.X).ToList());

            BoundingBox bbox;
            StreamGeometry geo = DataTypeConverterFunctions.SimplePolygon2StreamGeometry(_vertices, out bbox);
            geo.FillRule = FillRule.Nonzero;
            path.Data = geo;

        }
        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            _sel_active = true;
            _vertices = new List<Vector2d>();
            _vertices.Add(GetVector2d(e.GetPosition(this)));
            path.Visibility = System.Windows.Visibility.Visible;
        }
        protected override void OnMouseUp(MouseButtonEventArgs e)
        {
            if(RegionSelected != null)
                RegionSelected(this, new FreeSelectionEventArgs(_vertices));

            path.Data = null;
            path.Visibility = System.Windows.Visibility.Hidden;
            _sel_active = false;
            _vertices = null;
            DisableFreeSelector();
        }

        public void EnableFreeSelector(MouseButtonEventArgs e)
        {
            Background = Brushes.Transparent;
            OnMouseDown(e);
        }
        public void DisableFreeSelector()
        {
            Background = null;
        }

        public void SetSelectionMode(FreeSelectionMode mode)
        {
            _free_selection_mode = mode;
        }

        protected Vector2d GetVector2d(Point p)
        {
            return new Vector2d(p.X,p.Y);
        }

        private List<Vector2d> _vertices;
        private bool _sel_active;

        private FreeSelectionMode _free_selection_mode = FreeSelectionMode.ConvexHull;

        public event EventHandler<FreeSelectionEventArgs> RegionSelected;


    }
}