﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Logicx.WpfUtility.CustomControls.OverlayManager.StatusInfo
{
    /// <summary>
    /// Interaktionslogik für ConfirmInfo.xaml
    /// </summary>
    public partial class ConfirmInfo : UserControl
    {
        public ConfirmInfo()
        {
            InitializeComponent();
        }

        public string InformationTitle
        {
            get { return info_title_tb.Text; }
            set { info_title_tb.Text = value; }
        }
        public string SubInformationTitle
        {
            get { return subinfo_title_tb.Text; }
            set { subinfo_title_tb.Text = value; }
        }
        public StackPanel ContenStackPanel
        {
            get
            {
                return content_stackpanel;
            }
        }
    }
}
