using System;
using System.Collections.Generic;

namespace MathLib
{
	/// <summary>
	/// 
	/// </summary>
	public class LinearAlgebra
	{
		/// <summary>
		/// this doesent seem to work with doubles, only tested with integers
		/// </summary>
		/// <param name="coefficients"></param>
		/// <param name="constants"></param>
		/// <returns></returns>
		public static double[] Solve2DimEquation(double[,] A, double[] b){
			double[] result = {0,0};
			double dev = 0;
			dev = (A[1,0]*-1/A[0,0]);
			A[1,1]     = dev * A[0,1] + A[1,1];
			b[1]       = dev * b[0] + b[1];
			result[1]  = b[1] / A[1,1];
			dev = (A[0,1]*-1/A[1,1]);
			b[0]       = dev * b[1] + b[0];
			result[0]  = b[0] / A[0,0];
			return result;
		}


		/// <summary>
		/// gibt ein false zur�ck wenn der betrag zum cutpoint netgativ ist
		/// </summary>
		/// <param name="vertexA"></param>
		/// <param name="directionVecA"></param>
		/// <param name="vertexB"></param>
		/// <param name="directionVecB"></param>
		/// <param name="cutpoint"></param>
		/// <returns></returns>
		public static bool GetCutPoint(Vector2d vertexA, Vector2d directionVecA, Vector2d vertexB, Vector2d directionVecB, out Vector2d cutpoint){
			double[] xy = {0,0};
			double[] constants    = {vertexA.X - vertexB.X,vertexA.Y - vertexB.Y};
			double[,] coefficients ={{-directionVecA.X,directionVecB.X},{-directionVecA.Y,directionVecB.Y}};
			try
			{


//				//ber�cksichtige ausnahmen
//				if(A[0,0]==0 && A[0,1]!=0)
//				{
//					result[1] = b[0] / A[0,1];
//					result[0] = (b[1]-A[1,1])*(-1);
//					return result;
//				}
				if(coefficients[0,0] == 0)
					coefficients[0,0] = Math.Pow(10,-12);
				if(coefficients[0,1] == 0)
					coefficients[0,1] = Math.Pow(10,-12);
				if(coefficients[1,0] == 0)
					coefficients[1,0] = Math.Pow(10,-12);
				if(coefficients[1,1] == 0)
					coefficients[1,1] = Math.Pow(10,-12);


				 xy = Solve2DimEquation(coefficients,constants);
				if(Double.IsNaN(xy[0]) || Double.IsInfinity(xy[0]) || Double.IsNaN(xy[1]) || Double.IsInfinity(xy[1])){
					cutpoint = new Vector2d(0,0);
					return false;
				}
			}
			catch{
				cutpoint = new Vector2d(0,0);
				return false;
			}
			cutpoint = vertexA+(directionVecA * xy[0]);
			if(xy[0]<0 || xy[1]<0)
				return false;
			else
				return true;
		}

		public static Vector2d GetCutPoint(Edge e1, Edge e2)
		{
			Vector2d cutpoint;
			GetCutPoint(e1,e2,out cutpoint);
			return cutpoint;
		}
		public static bool GetCutPoint(Edge e1, Edge e2,out Vector2d cutpoint)
		{
			if( e1.V1 == e2.V1 || e1.V1== e2.V2 || e2.V1 == e1.V2 || e2.V2 == e1.V2)
			{
				cutpoint = e1.V1;
				return true;
				
			}
			if(e2.V1 == e1.V2 )
			{
				cutpoint = e1.V2;
				return true;
			}

			Vector2d vertexA = e1.V1;
			Vector2d vertexB = e2.V1;

			Vector2d directionVecA = (e1.V2 - e1.V1).GetNorm();
			Vector2d directionVecB = (e2.V2 - e2.V1).GetNorm();

			return GetCutPoint(vertexA,directionVecA,vertexB,directionVecB,out cutpoint);
		}

		/// <summary>
		/// returniert einen Vector wenn sich der cutpoint in den jweiligen segementen befindet
		/// also cutpoints ausserhalb der kanten werden nicht retuniert---> false 
		/// der zur�ckgegebene vektor enth�lt nur 0 Werte
		/// </summary>
		/// <param name="e1"></param>
		/// <param name="e2"></param>
		/// <param name="cutpoint"></param>
		/// <returns></returns>
		public static bool GetCutPointInSegment(Edge e1, Edge e2,out Vector2d cutpoint)
		{
			if(e1.V1 == e2.V1 || e1.V1== e2.V2 || e2.V1 == e1.V2 || e2.V2 == e1.V2)
			{
				cutpoint = new Vector2d(0,0);
				return false;
			}

			bool positive_offset = GetCutPoint(e1,e2,out cutpoint);
			
			double len_e1_cp = (cutpoint - e1.V1).GetLen();
			double len_e2_cp = (cutpoint - e2.V1).GetLen();
			double len_e1 = e1.GetLen();
			double len_e2 = e2.GetLen();

			if (!positive_offset  ||
				len_e1_cp >= len_e1 || len_e2_cp >= len_e2)
			{
				cutpoint = new Vector2d(0,0);
				return false;
			}
			else{
				return true;
			}
															  
		}

        public static double GetTriangleArea(Vector2f a_vec, Vector2f b_vec, Vector2f c_vec)
        {
            if (a_vec == b_vec || a_vec == c_vec || b_vec == c_vec)
                return 0;

            double alpha = (c_vec - a_vec).GetAngle(b_vec - a_vec);
            double b = (c_vec - a_vec).GetLen();
            double c = (b_vec - a_vec).GetLen();
            double hc = Math.Sin(alpha)*b;

            return c*hc*0.5;
        }
    }
}
