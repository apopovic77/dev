using System;
using System.Collections.Generic;
using System.Text;

namespace Logicx.DirectX.Management
{
    /// <summary>
    /// The default sample exception type
    /// </summary>
    public class Exception3d : System.ApplicationException
    {

        public static String[] eMessages = {   /*0*/				 "no id available, query not successful, specify an id an requery!",
											   /*1*/				 "dbconnection missing! data cannot be synchronized with db! ",
											   /*2*/				 "no data found! line cannot be fetched!",
											   /*3*/				 "no data found! scalar cannot be fetched!",
											   /*4*/				 "datatype not valid, value is of antoher datatype --> action cannot be performed",
											   /*5*/				 "datatype missing cannot perfom action",
											   /*6*/				 "Value not set, cannot insert Parameter!",
											   /*7*/				 "DataType cannot be evaluated, Value cannot be set!",
											   /*8*/				 "Messagestatus unknown!",
											   /*9*/				 "no id available, update cannot be done!",
											   /*10*/				 "PolyCreation failed lifelock erkannt, algorithmus bricht nicht ab!",
                                               /*11*/				 "Route Discretisation erfolglos, wahrscheinlich sind keine routeninformationen vorhanden!",
                                               /*12*/                "remot host not available",
                                               /*13*/                 "no more free object this cant be, bug in code",
                                               /*14*/                "route discretisation cannot be performed via mapx, as no remote object is available",
                                               /*15*/                "UserX konnte nicht angelegt werden auf der PushX Db"
										   };

        private int m_errorCode = -1;
        public Exception3d(int errorCode)
            : base(eMessages[errorCode])
        {
            m_errorCode = errorCode;
        }
        public Exception3d(string errormsg)
            : base(errormsg)
        {
        }
        public Exception3d(){}

        public int ErrorCode
        {
            get
            {
                return m_errorCode;
            }
        }


        /// <summary>
        /// Return information about the exception
        /// </summary>
        public override string Message
        {
            get
            {
                string strMsg = string.Empty;

                strMsg = "Generic application error. Enable\n";
                strMsg += "debug output for detailed information.";

                return strMsg;
            }
        }
    }


    /// <summary>
    /// Exception informing user no compatible devices were found
    /// </summary>
    public class NoCompatibleDevicesException : Exception3d
    {


        /// <summary>
        /// Return information about the exception
        /// </summary>
        public override string Message
        {
            get
            {
                string strMsg = string.Empty;
                strMsg = "This sample cannot run in a desktop\n";
                strMsg += "window with the current display settings.\n";
                strMsg += "Please change your desktop settings to a\n";
                strMsg += "16- or 32-bit display mode and re-run this\n";
                strMsg += "sample.";

                return strMsg;
            }
        }
    }


    /// <summary>
    /// An exception for when the ReferenceDevice is null
    /// </summary>
    public class NullReferenceDeviceException : Exception3d
    {
        /// <summary>
        /// Return information about the exception
        /// </summary>
        public override string Message
        {
            get
            {
                string strMsg = string.Empty;
                strMsg = "Warning: Nothing will be rendered.\n";
                strMsg += "The reference rendering device was selected, but your\n";
                strMsg += "computer only has a reduced-functionality reference device\n";
                strMsg += "installed.  Install the DirectX SDK to get the full\n";
                strMsg += "reference device.\n";

                return strMsg;
            }
        }
    }

    /// <summary>
    /// An exception for when reset fails
    /// </summary>
    public class ResetFailedException : Exception3d
    {
        /// <summary>
        /// Return information about the exception
        /// </summary>
        public override string Message
        {
            get
            {
                string strMsg = string.Empty;
                strMsg = "Could not reset the Direct3D device.";

                return strMsg;
            }
        }
    }

    /// <summary>
    /// The exception thrown when media couldn't be found
    /// </summary>
    public class MediaNotFoundException : Exception3d
    {
        private string mediaFile;
        public MediaNotFoundException(string filename)
            : base()
        {
            mediaFile = filename;
        }
        public MediaNotFoundException()
            : base()
        {
            mediaFile = string.Empty;
        }
        /// <summary>
        /// Return information about the exception
        /// </summary>
        public override string Message
        {
            get
            {
                string strMsg = string.Empty;
                strMsg = "Could not load required media.";
                if (mediaFile.Length > 0)
                    strMsg += string.Format("\r\nFile: {0}", mediaFile);

                return strMsg;
            }
        }
    }
}


