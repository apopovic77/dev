﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BankLib.Data;
using BankLib.Gruppe;

namespace BankOverviewApp
{
    /// <summary>
    /// Interaction logic for Gruppen.xaml
    /// </summary>
    public partial class Gruppen : UserControl
    {
        public Gruppen(GruppenCreateParser gcp, Gruppenzuordnung gz)
        {
            InitializeComponent();
            GruppenCreateParser = gcp;
            Gruppenzuordnung = gz;
        }


        private void ButtonSave_OnClick(object sender, RoutedEventArgs e)
        {
            BuchungsGruppen.BgTop = _bgtop;
            BuchungsGruppen.SaveGruppenToFile();
        }

        private void ButtonLoad_OnClick(object sender, RoutedEventArgs e)
        {
            if (_tree_view != null)
            {
                grid.Children.Remove(_tree_view);
                _tree_view = null;
            }

            _tree_view = new TreeView();
            Grid.SetRow(_tree_view, 1);
            grid.Children.Add(_tree_view);

            CreatGroups();
            CreateTreeView(_bgtop);
        }

        private void ButtonLoadHdd_OnClick(object sender, RoutedEventArgs e)
        {
            if (_tree_view != null)
            {
                grid.Children.Remove(_tree_view);
                _tree_view = null;
            }

            _tree_view = new TreeView();
            Grid.SetRow(_tree_view, 1);
            grid.Children.Add(_tree_view);


            _bgtop = BuchungsGruppen.BgTop;
            CreateTreeView(BuchungsGruppen.BgTop);
        }


        public void CreatGroups()
        {
            string content = GruppenCreateParser.gruppen_tbox.Text;
            if (string.IsNullOrEmpty(content))
            {
                MessageBox.Show("kein gruppen text vorhanden, klick vorher mal auf den Reiter ->Gruppenübersicht<-");
                return;
            }

            int curr_hier_level = -1;
            string[] rows = content.Split("\n".ToCharArray());

            _bgtop = new BuchungsGruppe();
            BuchungsGruppe curr_parent = _bgtop;
            BuchungsGruppe last_gruppe = curr_parent;
            foreach (string row in rows)
            {
                int hier_level = GetHierarchieLevel(row);
                string group_name = row.Trim();
                string comment;
                string command_type;
                string command_value;
                GetGnameCommentInfo(ref group_name, out comment, out command_type, out command_value);
                if (string.IsNullOrEmpty(group_name))
                    continue;

                BuchungsGruppe bg = new BuchungsGruppe();
                bg.Beschreibung = comment;
                bg.Name = group_name;
                bg.AddBgZuo(Gruppenzuordnung.GetBgZuos(bg.Name));
                bg.AddBgZuo(GetBgZuosByCommandType(command_type, command_value));


                if (hier_level == curr_hier_level)
                {
                    curr_parent.AddChild(bg);
                }
                else if (curr_hier_level > hier_level)
                {
                    //find corr parent
                    while (true)
                    {
                        //switch back to old parent
                        if (curr_parent.Parent == null)
                            throw new Exception("unknown error, this seems to be a programm error, this must not be");

                        curr_parent = curr_parent.Parent;
                        curr_hier_level--;
                        if (curr_hier_level == hier_level)
                        {
                            curr_parent.AddChild(bg);
                            break;
                        }      
                    }
                }
                else
                {
                    curr_parent = last_gruppe;
                    curr_parent.AddChild(bg);
                    curr_hier_level++;
                }

                last_gruppe = bg;
            }


            //add unbekannt gruppen

        }

        private BgZuo GetBgZuosByCommandType(string command_type, string command_value)
        {
            if (string.IsNullOrEmpty(command_type))
                return null;
            //if (string.IsNullOrEmpty(command_value))
            //    return null;

            switch (command_type)
            {
                case "BgZuoBuchungsType":
                {
                    BgZuoBuchungsType bg_zuo = new BgZuoBuchungsType();
                    bg_zuo.BuchungsType = command_value;
                    return bg_zuo;
                }
                case "BgZuoUnknownCost":
                {
                    BgZuoUnknownCost bg_zuo = new BgZuoUnknownCost();
                    return bg_zuo;
                }
                case "BgZuoUnknownIncome":
                {
                    BgZuoUnknownIncome bg_zuo = new BgZuoUnknownIncome();
                    return bg_zuo;
                }
                default:
                    throw new Exception("unknown command type "+ command_type);
            }
        }

        private void CreateTreeView(BuchungsGruppe top)
        {
            if (top == null)
                return;

            foreach (BuchungsGruppe buchungs_gruppe in top.Children())
            {
                TreeViewItem tv = new TreeViewItem();
                tv.Header = buchungs_gruppe.Name;
                tv.IsExpanded = true;
                _tree_view.Items.Add(tv);
                AddBgZuoToTree(tv, buchungs_gruppe);
                CreateTreeView(tv, buchungs_gruppe);
            }
        }

        private void AddBgZuoToTree(TreeViewItem tv, BuchungsGruppe bg)
        {
            if (bg.BgZuos().Count == 0)
                return;

            TreeViewItem tv_child_rules = new TreeViewItem();
            tv_child_rules.IsExpanded = false;
            tv.Items.Add(tv_child_rules);
            tv_child_rules.Header = "Rules";
            
            foreach (BgZuo bg_zuo in bg.BgZuos())
            {
                TreeViewItem tv_child = new TreeViewItem();
                tv_child.IsExpanded = true;
                tv_child_rules.Items.Add(tv_child);
                tv_child.Header = bg_zuo.ToString();
            }
        }

        private void CreateTreeView(TreeViewItem tv, BuchungsGruppe buchungs_gruppe)
        {
            if (!buchungs_gruppe.HasChildren())
                return;


            foreach (BuchungsGruppe bg_child in buchungs_gruppe.Children())
            {
                TreeViewItem tv_child = new TreeViewItem();
                tv_child.IsExpanded = true;
                tv.Items.Add(tv_child);
                tv_child.Header = bg_child.Name;
                AddBgZuoToTree(tv_child, bg_child);
                CreateTreeView(tv_child, bg_child);
            }
        }


        private void GetGnameCommentInfo(ref string full_name_string, out string comment, out string comment_type, out string comment_value)
        {
            comment = null;
            comment_type = null;
            comment_value = null;

            try
            {
                int comment_index = full_name_string.IndexOf("//", StringComparison.Ordinal);
                if (comment_index >= 0)
                {
                    comment = full_name_string.Substring(comment_index + 2);
                    full_name_string = full_name_string.Substring(0, comment_index).Trim();
                }

                if (string.IsNullOrEmpty(comment))
                    return;

                int indexof_beg = comment.IndexOf("[", System.StringComparison.Ordinal);
                if (indexof_beg >= 0)
                {
                    //metatag given
                    int indexof_end = comment.IndexOf("]", System.StringComparison.Ordinal);
                    string meta_info = comment.Substring(indexof_beg + 1, indexof_end - indexof_beg - 1);
                    if(indexof_beg > 0)
                        comment = comment.Substring(0, indexof_beg - 1);

                    if (string.IsNullOrEmpty(meta_info))
                        return;

                    string[] meta_arr = meta_info.Split("|".ToCharArray(), 3);

                    comment_type = meta_arr[0];
                    if(meta_arr.Length > 1)
                        comment_value = meta_arr[1];
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private int GetHierarchieLevel(string s)
        {
            int hier_level = 0;
            for (int i = 0; i < s.Length; i++)
            {
                if (s[i] != '\t')
                    break;
                hier_level++;
            }
            return hier_level;
        }

        public GruppenCreateParser GruppenCreateParser;
        public Gruppenzuordnung Gruppenzuordnung;
        private BuchungsGruppe _bgtop;
        private TreeView _tree_view;
    }
}
