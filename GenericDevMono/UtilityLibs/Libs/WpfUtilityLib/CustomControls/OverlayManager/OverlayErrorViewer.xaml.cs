﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Logicx.WpfUtility.CustomControls.Validation;
using Logicx.WpfUtility.WpfHelpers;

namespace Logicx.WpfUtility.CustomControls.OverlayManager
{
    /// <summary>
    /// Interaction logic for OverlayErrorViewer.xaml
    /// </summary>
    public partial class OverlayErrorViewer : UserControl, IDisposable
    {
        #region construction and initialization
        static OverlayErrorViewer()
        {
            FrameworkPropertyMetadata metadataErrorCount = new FrameworkPropertyMetadata(0);
            ErrorCountPropertyKey = DependencyProperty.RegisterReadOnly("ErrorCount",
                                                            typeof(int), typeof(OverlayErrorViewer), metadataErrorCount);
            ErrorCountProperty = ErrorCountPropertyKey.DependencyProperty;
        }
        public OverlayErrorViewer()
        {
            InitializeComponent();
        }
        #endregion

        #region operations
        public void SetHelpAndErrorViewer(OverlayHelpAndErrorViewer help_and_error_viewer)
        {
            _help_and_error_viewer = help_and_error_viewer;
        }

        public void SetErrorProvider(ErrorProvider error_provider)
        {
            if(_error_provider != null)
            {
                _error_provider.ProviderErrorList.CollectionChanged -= ProviderErrorList_CollectionChanged;
                tasks_listbox.DataContext = null;
            }

            _error_provider = error_provider;

            if(_error_provider != null)
            {
                _error_provider.ProviderErrorList.CollectionChanged += new System.Collections.Specialized.NotifyCollectionChangedEventHandler(ProviderErrorList_CollectionChanged);
                tasks_listbox.DataContext = _error_provider.ProviderErrorList;
            }
        }



        #region IDisposable Members

        public void Dispose()
        {
            Debug.WriteLine("### Disposing OverlayErrorViewer...");
            if (_error_provider != null)
                _error_provider.ProviderErrorList.CollectionChanged -= ProviderErrorList_CollectionChanged;

            tasks_listbox.DataContext = null;
            _error_provider = null;
            _help_and_error_viewer = null;
        }

        #endregion
        #endregion

        #region event handler
        private void button_hide_Click(object sender, RoutedEventArgs e)
        {
            if(_help_and_error_viewer != null)
            {
                _help_and_error_viewer.HideFullErrorInfo();
            }
        }

        private void JumpToControl_Click(object sender, RoutedEventArgs e)
        {
            Button btnSender = sender as Button;

            if(btnSender != null && btnSender.CommandParameter is ErrorInfo)
            {
                ErrorInfo selError = btnSender.CommandParameter as ErrorInfo;
                
                if (selError != null && selError.SourceControl != null && tasks_listbox.SelectedItem == selError)
                {
                    UIHelper.SetFocusComplex(selError.SourceControl);
                }

                if (selError != null && tasks_listbox.SelectedItem != selError)
                    tasks_listbox.SelectedItem = selError;
            }
        }

        private void tasks_listbox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ErrorInfo selError = tasks_listbox.SelectedItem as ErrorInfo;
            if (selError != null && selError.SourceControl != null)
            {
                UIHelper.SetFocusComplex(selError.SourceControl);
            }
        }

        private void ListBoxItem_MouseDown(object sender, MouseButtonEventArgs e)
        {
            ErrorInfo selError = tasks_listbox.SelectedItem as ErrorInfo;
            if (selError != null && selError.SourceControl != null)
            {
                UIHelper.SetFocusComplex(selError.SourceControl);
            }
        }

        /// <summary>
        /// Called if the parent error provider's error collection changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void ProviderErrorList_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (_error_provider != null)
            {
                int curCount = _error_provider.ProviderErrorList.Count;
                SetValue(ErrorCountPropertyKey, curCount);
            }
        }
        #endregion

        #region properties
        /// <summary>
        /// gets the error count
        /// </summary>
        public int ErrorCount
        {
            get { return (int)GetValue(ErrorCountProperty); }
        }
        #endregion

        #region Dependency properties
        public static readonly DependencyProperty ErrorCountProperty;
        private static readonly DependencyPropertyKey ErrorCountPropertyKey;
        #endregion

        #region attribs
        private OverlayHelpAndErrorViewer _help_and_error_viewer = null;
        private ErrorProvider _error_provider = null;
        #endregion

    }
}
