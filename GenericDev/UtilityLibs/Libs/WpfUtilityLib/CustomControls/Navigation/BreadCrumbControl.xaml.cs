﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Logicx.WpfUtility.CustomControls.Navigation
{
    public class CreateBreadCrumbItemEventArgs : EventArgs
    {
        public CreateBreadCrumbItemEventArgs(BreadCrumbControl parent_control, string text, object additional_object)
        {
            ParentControl = parent_control;
            Text = text;
            AdditionalObject = additional_object;
        }

        public BreadCrumbControl ParentControl
        {
            get;
            private set;
        }

        public string Text
        {
            get;
            private set;
        }

        public IBreadCrumbItem CreatedItem
        {
            get;
            set;
        }

        public object AdditionalObject
        {
            get;
            private set;
        }
    }

    /// <summary>
    /// Interaction logic for BreadCrumbControl.xaml
    /// </summary>
    public partial class BreadCrumbControl : UserControl
    {
        #region Constants
        #endregion

        #region Construction and Initialization
        public BreadCrumbControl()
        {
            InitializeComponent();
            this.Loaded += new RoutedEventHandler(BreadCrumbControl_Loaded);
        }

        
        #endregion

        #region Operations

        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            if(e.Property == WithNavigateDeeperButtonProperty)
            {
                if((bool)e.NewValue)
                {
                    if (_navigate_deeper_item == null)
                    {
                        _navigate_deeper_item = CreateBreadCrumbItem(">>", null);
                        _navigate_deeper_item.IsNavigateDeeperItem = true;
                        //_navigate_deeper_item.Click += new RoutedEventHandler(BreadCrumbButton_Click);
                    }
                }
                else
                {
                    if (_navigate_deeper_item != null && _breadcrumb_path.Contains(_navigate_deeper_item))
                        _breadcrumb_path.Remove(_navigate_deeper_item);
                }

                UpdateBreadCrumbVisibility();
            }
            else if(e.Property == WithCustomBreadCrumbItemCreationProperty)
            {
                RecreateBreadCrumbItems();
            }
            else if(e.Property == CurrentBreadCrumbItemProperty)
            {
                UpdateBreadCrumbVisibility();
            }

            base.OnPropertyChanged(e);
        }

        public IBreadCrumbItem CreateBreadCrumbItem(string text, object additional_object)
        {
            if (WithCustomBreadCrumbItemCreation && RequestBreadCrumbItemCreation != null)
            {
                CreateBreadCrumbItemEventArgs event_args = new CreateBreadCrumbItemEventArgs(this, text, additional_object);
                RequestBreadCrumbItemCreation(this, event_args);

                if (event_args.CreatedItem != null)
                    return event_args.CreatedItem;
            }

            return OnCreateBreadCrumbItem(text, additional_object);
        }

        protected virtual IBreadCrumbItem OnCreateBreadCrumbItem(string text, object additional_object)
        {
            return new BreadCrumbItem(this, text, additional_object);
        }

        void RecreateBreadCrumbItems()
        {
            List<IBreadCrumbItem> change_list = _breadcrumb_path.ToList();

            foreach (IBreadCrumbItem current_item in change_list)
            {
                IBreadCrumbItem newItem = CreateBreadCrumbItem(current_item.Text, current_item.AdditionalObject);
                newItem.IsLastItem = current_item.IsLastItem;

                current_item.Click -= BreadCrumbButton_Click;
                newItem.Click += new RoutedEventHandler(BreadCrumbButton_Click);
                int idx = _breadcrumb_path.IndexOf(current_item);

                _breadcrumb_path.Remove(current_item);
                _breadcrumb_path.Insert(idx, newItem);

                if (_root_bread_crumb_item == current_item)
                    _root_bread_crumb_item = newItem;
            }
        }

        void UpdateBreadCrumbVisibility()
        {
            List<IBreadCrumbItem> rem_items = new List<IBreadCrumbItem>();
            bool found_current_item = false;

            foreach (IBreadCrumbItem check_item in _breadcrumb_path)
            {
                if (check_item == CurrentBreadCrumbItem)
                {
                    found_current_item = true;
                }
                else if (found_current_item)
                {
                    rem_items.Add(check_item);
                }
            }

            if(!found_current_item)
            {
                rem_items.AddRange(_breadcrumb_path);
            }

            foreach (IBreadCrumbItem rem_item in rem_items)
            {
                rem_item.Click -= BreadCrumbButton_Click;
                _breadcrumb_path.Remove(rem_item);
            }

            if (!found_current_item && CurrentBreadCrumbItem != null)
            {
                IBreadCrumbItem parent_item = CurrentBreadCrumbItem.ParentItem;
                //if(CurrentBreadCrumbItem !=  _navigate_deeper_item)
                    CurrentBreadCrumbItem.Click += new RoutedEventHandler(BreadCrumbButton_Click);

                _breadcrumb_path.Insert(0, CurrentBreadCrumbItem);

                while(parent_item != null)
                {
                    //if (parent_item != _navigate_deeper_item)
                    parent_item.Click += new RoutedEventHandler(BreadCrumbButton_Click);

                    _breadcrumb_path.Insert(0, parent_item);
                    parent_item = parent_item.ParentItem;
                }
            }

            bool navigation_button_added = false;

            if (WithNavigateDeeperButton && _navigate_deeper_item != null && CurrentBreadCrumbItem != null && CurrentBreadCrumbItem.ChildItems.Count > 0)
            {
                if (!_breadcrumb_path.Contains(_navigate_deeper_item))
                {
                    _navigate_deeper_item.Click += new RoutedEventHandler(BreadCrumbButton_Click);
                    _breadcrumb_path.Add(_navigate_deeper_item);
                }

                navigation_button_added = true;
            }

            IsNavigateDeeperButtonVisible = navigation_button_added;

            if ((CurrentBreadCrumbItem == _root_bread_crumb_item || _breadcrumb_path.Count <= 1) && !AlwaysShowRootNode)
            {
                lb_breadcrumb.Visibility = Visibility.Collapsed;
            }
            else
            {
                lb_breadcrumb.Visibility = Visibility.Visible;
            }

            int last_item_index = _breadcrumb_path.Count - 1;

            if (navigation_button_added)
                last_item_index--;

            for (int i = 0; i < _breadcrumb_path.Count; i++)
            {
                if (i == last_item_index)
                    _breadcrumb_path[i].IsLastItem = true;
                else
                    _breadcrumb_path[i].IsLastItem = false;
            }
        }

        protected virtual void OnCurrentBreadCrumbItemChanged(IBreadCrumbItem oldItem, IBreadCrumbItem newItem)
        {
            
        }

        private void RaiseBreadCrumbItemClicked(IBreadCrumbItem bread_crumb_item)
        {
            if (BreadCrumbItemClicked != null)
                BreadCrumbItemClicked(bread_crumb_item, EventArgs.Empty);
        }

        private void RaiseNavigateDeeperButtonClicked(IBreadCrumbItem bread_crumb_item)
        {
            if (NavigateDeeperButtonClicked != null)
                NavigateDeeperButtonClicked(bread_crumb_item, EventArgs.Empty);
        }
        #endregion

        #region Event Handlers
        void BreadCrumbControl_Loaded(object sender, RoutedEventArgs e)
        {
            if (_root_bread_crumb_item == null)
            {
                _root_bread_crumb_item = CreateBreadCrumbItem(string.Empty, null);
                //_root_bread_crumb_item.Click += new RoutedEventHandler(BreadCrumbButton_Click);

                CurrentBreadCrumbItem = _root_bread_crumb_item;
                lb_breadcrumb.ItemsSource = _breadcrumb_path;

                _breadcrumb_path.Add(_root_bread_crumb_item);
            }

            if(_navigate_deeper_item == null)
            {
                _navigate_deeper_item = CreateBreadCrumbItem(">>", null);
                _navigate_deeper_item.IsNavigateDeeperItem = true;
                //_navigate_deeper_item.Click += new RoutedEventHandler(BreadCrumbButton_Click);

                if(WithNavigateDeeperButton)
                {
                    _breadcrumb_path.Add(_root_bread_crumb_item);
                }
            }

            UpdateBreadCrumbVisibility();
        }

        private void BreadCrumbButton_Click(object sender, RoutedEventArgs e)
        {
            Button sender_btn = sender as Button;
            e.Handled = true;

            if (sender_btn != null)
            {
                IBreadCrumbItem bread_crumb_item = sender_btn.CommandParameter as IBreadCrumbItem;

                Mouse.OverrideCursor = Cursors.Wait;

                if (bread_crumb_item != null)
                {
                    if (bread_crumb_item == _root_bread_crumb_item)
                    {
                        CurrentBreadCrumbItem = _root_bread_crumb_item;
                        RaiseBreadCrumbItemClicked(bread_crumb_item);
                    }
                    else if(bread_crumb_item != _navigate_deeper_item)
                    {
                        CurrentBreadCrumbItem = bread_crumb_item;
                        RaiseBreadCrumbItemClicked(bread_crumb_item);
                    }
                    else
                    {
                        RaiseNavigateDeeperButtonClicked(bread_crumb_item);
                    }
                }

                Mouse.OverrideCursor = null;

                UpdateBreadCrumbVisibility();
            }
        }

        #endregion

        #region Properties
        public IBreadCrumbItem RootBreadCrumbItem
        {
            get { return _root_bread_crumb_item; }
        }
        #endregion

        #region Dependency Properties
        public IBreadCrumbItem CurrentBreadCrumbItem
        {
            get { return (IBreadCrumbItem)GetValue(CurrentBreadCrumbItemProperty); }
            set { SetValue(CurrentBreadCrumbItemProperty, value); }
        }
        public static readonly DependencyProperty CurrentBreadCrumbItemProperty = DependencyProperty.Register("CurrentBreadCrumbItem", typeof(IBreadCrumbItem), typeof(BreadCrumbControl), new UIPropertyMetadata(null, new PropertyChangedCallback(OnCurrentBreadCrumbItem)));

        public bool WithNavigateDeeperButton
        {
            get { return (bool)GetValue(WithNavigateDeeperButtonProperty); }
            set { SetValue(WithNavigateDeeperButtonProperty, value); }
        }
        public static readonly DependencyProperty WithNavigateDeeperButtonProperty = DependencyProperty.Register("WithNavigateDeeperButton", typeof(bool), typeof(BreadCrumbControl), new UIPropertyMetadata(false));

        public bool IsNavigateDeeperButtonVisible
        {
            get { return (bool)GetValue(IsNavigateDeeperButtonVisibleProperty); }
            private set { SetValue(IsNavigateDeeperButtonVisiblePropertyKey, value); }
        }
        public static readonly DependencyPropertyKey IsNavigateDeeperButtonVisiblePropertyKey = DependencyProperty.RegisterReadOnly("IsNavigateDeeperButtonVisible", typeof(bool), typeof(BreadCrumbControl), new UIPropertyMetadata(false));
        public static readonly DependencyProperty IsNavigateDeeperButtonVisibleProperty = IsNavigateDeeperButtonVisiblePropertyKey.DependencyProperty; // DependencyProperty.Register("IsNavigateDeeperButtonVisible", typeof(bool), typeof(BreadCrumbControl), new UIPropertyMetadata(false));


        public bool WithCustomBreadCrumbItemCreation
        {
            get { return (bool)GetValue(WithCustomBreadCrumbItemCreationProperty); }
            set { SetValue(WithCustomBreadCrumbItemCreationProperty, value); }
        }
        public static readonly DependencyProperty WithCustomBreadCrumbItemCreationProperty = DependencyProperty.Register("WithCustomBreadCrumbItemCreation", typeof(bool), typeof(BreadCrumbControl), new UIPropertyMetadata(false));

        public bool AlwaysShowRootNode
        {
            get { return (bool)GetValue(AlwaysShowRootNodeProperty); }
            set { SetValue(AlwaysShowRootNodeProperty, value); }
        }
        public static readonly DependencyProperty AlwaysShowRootNodeProperty = DependencyProperty.Register("AlwaysShowRootNode", typeof(bool), typeof(BreadCrumbControl), new UIPropertyMetadata(false));
        #endregion

        #region Dependency property callbacks
        private static void OnCurrentBreadCrumbItem(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if(d is BreadCrumbControl)
                ((BreadCrumbControl) d).OnCurrentBreadCrumbItemChanged(e.OldValue as IBreadCrumbItem, e.NewValue as IBreadCrumbItem);
        }

        #endregion

        #region Events
        public event EventHandler<CreateBreadCrumbItemEventArgs> RequestBreadCrumbItemCreation;
        public event EventHandler BreadCrumbItemClicked;
        public event EventHandler NavigateDeeperButtonClicked;
        #endregion

        #region Routed Events
        #endregion

        #region Attributes
        private IBreadCrumbItem _root_bread_crumb_item = null;
        private IBreadCrumbItem _navigate_deeper_item = null;
        private ObservableCollection<IBreadCrumbItem> _breadcrumb_path = new ObservableCollection<IBreadCrumbItem>();
        #endregion

        #region Tests
        #endregion
    }
}
