using System;
using System.Collections;

namespace MathLib
{
	/// <summary>
	/// dabei wird davon ausgegangen dass der vector in der arraylist als object[2] gespeichert wird
	/// object[1] handelt es sich dabei um einen double
	/// </summary>
	public class VectorSortLenAsc : IComparer  
	{
		int IComparer.Compare( Object x1, Object x2 )  
		{
			object[] o1 = (object[])x1;
			object[] o2 = (object[])x2;

			
			double X1 = Convert.ToDouble(o1[0]);
			double X2 = Convert.ToDouble(o2[0]);
			if(X1 < X2)
				return -1;
			else if(X1 == X2)
				return 0;
			else
				return 1;
		}
	}
}
