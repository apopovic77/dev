﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Logicx.Utility;
using MathLib;

namespace Logicx.WpfUtility.Panels3d
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class Carousel3d : UserControl
    {
        public class SelectedItemArgs : EventArgs
        {
            public  SelectedItemArgs(object key)
            {
                Key = key;
            }

            public object Key;
        }

        public Carousel3d(double width, double height)
        {
            InitializeComponent();

            main_canvas.Width = width;
            main_canvas.Height = height;
            viewport.Width = width;
            viewport.Height = height;

            // Set the edge mode to aliased for the visual and any descendant drawing primitives it has.
            //RenderOptions.SetEdgeMode(viewport, EdgeMode.Aliased);

            //gen the camera
            _cam = new PerspectiveCamera();
            viewport.Camera = _cam;
            _cam.LookDirection = new Vector3D(0,0,-1);
            _cam.UpDirection = new Vector3D(0,1,0);
            _cam.Position = new Point3D(0, 0, _cam_pos_z);
            _cam.NearPlaneDistance = _cam_near_plane;
            _cam.FarPlaneDistance = _cam_far_plane;
            _cam.FieldOfView = _cam_fov;
            TranslateCamY(_transl_cam_y);
            _cam.Freeze();

            CompositionTarget.Rendering += new EventHandler(RenderLoop);
        }


        public void TranslateCamY(float transl_y)
        {
            _transl_cam_y = transl_y;
            float rot_to_center = (float)Math.Atan(transl_y / _cam_pos_z);
            Vector2f v_updir = new Vector2f(0, 1);
            v_updir.Rotate(rot_to_center);
            Vector2f v_dir = new Vector2f(1, 0);
            v_dir.Rotate(rot_to_center);
            _cam.UpDirection = new Vector3D(0, v_updir.Y, v_updir.X);
            _cam.LookDirection = new Vector3D(0, v_dir.Y, -v_dir.X);
            _cam.Position = new Point3D(0, transl_y, _cam_pos_z);
        }

        #region Carousel Creation
        private void InitItemPositionsRadial()
        {

            int count_items = _items.Count;
            double rot_angle_per_item = 360 / (double)count_items;

            int i = 0;
            foreach (DictionaryEntry entry in _items)
            {
                object key = entry.Key;

                Visual3D visual = (Visual3D)entry.Value;

                RotateTransform3D rot1 = (RotateTransform3D)((Transform3DGroup)visual.Transform).Children[1];
                RotateTransform3D rot2 = (RotateTransform3D)((Transform3DGroup)visual.Transform).Children[3];

                double rot_angle = rot_angle_per_item * i;
                rot1.Rotation = new AxisAngleRotation3D(new Vector3D(0, 1, 0), -rot_angle);
                rot2.Rotation = new AxisAngleRotation3D(new Vector3D(0, 1, 0), rot_angle);

                i++;
            }
        }

        public void AddItem(object key, BitmapSource bmp, double width, double height)
        {
            Transform3DGroup transformgroup = new Transform3DGroup();
            transformgroup.Children.Add(new ScaleTransform3D(1, 1, 1));
            transformgroup.Children.Add(new RotateTransform3D());
            transformgroup.Children.Add(new TranslateTransform3D(0, 0, _circle_radius));
            transformgroup.Children.Add(new RotateTransform3D());

            // Now use that TextBlock as the brush for a material
            DiffuseMaterial mat = new DiffuseMaterial();
            ImageBrush brush = new ImageBrush(bmp);
            brush.Opacity = 1f;
            mat.Brush = brush;

            MeshGeometry3D geom_3d = CreateMeshGeometryRectangle(width, height);

            ModelVisual3D visual = new ModelVisual3D();
            visual.Content = new GeometryModel3D(geom_3d, mat);
            visual.Transform = transformgroup;

            viewport.Children.Add(visual);
            _items.Add(key, visual);
            _items_inv.Add(visual, key);

            InitItemPositionsRadial();
        }

        public void AddItem(object key, Canvas canvas, double width, double height)
        {
            canvas.Width = width;
            canvas.Height = height;
            canvas.ClipToBounds = true;

            Transform3DGroup transformgroup = new Transform3DGroup();
            transformgroup.Children.Add(new ScaleTransform3D(1,1,1));
            transformgroup.Children.Add(new RotateTransform3D());
            transformgroup.Children.Add(new TranslateTransform3D(0, 0, _circle_radius));
            transformgroup.Children.Add(new RotateTransform3D());


            // Now use that TextBlock as the brush for a material
            DiffuseMaterial mat = new DiffuseMaterial();
            VisualBrush brush = new VisualBrush(canvas);
            brush.Opacity = 1f;
            mat.Brush = brush;

            MeshGeometry3D geom_3d = CreateMeshGeometryRectangle(width, height);

            ModelVisual3D visual = new ModelVisual3D();
            visual.Content = new GeometryModel3D(geom_3d, mat);
            visual.Transform = transformgroup;

            model_visual.Children.Add(visual);
            _items.Add(key, visual);
            _items_inv.Add(visual, key);

            InitItemPositionsRadial();
        }



        public void ReplaceItem(object key, BitmapSource bmp, int width, int height)
        {
            if (!_items.Contains(key))
                throw new Exception("item not existing in the list you have to <add> instead of <set> if it is a new item");

            //get the old visual
            ModelVisual3D old_visual = (ModelVisual3D)_items[key];

            //create the new visual obj
            DiffuseMaterial mat = new DiffuseMaterial();
            ImageBrush brush = new ImageBrush(bmp);
            brush.Opacity = 1f;
            mat.Brush = brush;
            ModelVisual3D visual = new ModelVisual3D();
            MeshGeometry3D geom_3d = CreateMeshGeometryRectangle(width, height); 
            visual.Content = new GeometryModel3D(geom_3d, mat);
            visual.Transform = old_visual.Transform;

            //replace the old visual
            viewport.Children[viewport.Children.IndexOf(old_visual)] = visual;
            _items[key] = visual;
            _items_inv.Remove(old_visual);
            _items_inv.Add(visual,key);
        }

        private MeshGeometry3D CreateMeshGeometryRectangle(double width, double height)
        {
            Point3DCollection myPositionCollection = new Point3DCollection();
            myPositionCollection.Add(new Point3D(width * -0.5, height * -0.5, 0));
            myPositionCollection.Add(new Point3D(width * 0.5, height * -0.5, 0));
            myPositionCollection.Add(new Point3D(width * -0.5, height * 0.5, 0));
            myPositionCollection.Add(new Point3D(width * 0.5, height * 0.5, 0));
            PointCollection myTextureCoordinatesCollection = new PointCollection();
            myTextureCoordinatesCollection.Add(new Point(0, 1));
            myTextureCoordinatesCollection.Add(new Point(1, 1));
            myTextureCoordinatesCollection.Add(new Point(0, 0));
            myTextureCoordinatesCollection.Add(new Point(1, 0));
            Int32Collection myTriangleIndicesCollection = new Int32Collection();
            myTriangleIndicesCollection.Add(0);
            myTriangleIndicesCollection.Add(1);
            myTriangleIndicesCollection.Add(2);
            myTriangleIndicesCollection.Add(2);
            myTriangleIndicesCollection.Add(1);
            myTriangleIndicesCollection.Add(3);
            MeshGeometry3D geom_3d = new MeshGeometry3D();
            geom_3d.Positions = myPositionCollection;
            geom_3d.TextureCoordinates = myTextureCoordinatesCollection;
            geom_3d.TriangleIndices = myTriangleIndicesCollection;
            geom_3d.Freeze();
            return geom_3d;
        }
        #endregion

        #region Selected Item Centration Animation
        private void StartItemSelectedAnimation()
        {
            //get curr angle
            RotateTransform3D rot = (RotateTransform3D)((Transform3DGroup)_selected_visual.Transform).Children[1];
            AxisAngleRotation3D axis_rot = rot.Rotation as AxisAngleRotation3D;



            _item_centration_animation_active = true;
            
            float start_val = 0;
            float end_val = 0;
            float curr_angle = 0;
            float rot_todo = 0;

            axis_rot.Angle = axis_rot.Angle % 360;
            if (axis_rot.Angle < 0)
                axis_rot.Angle = 360 + axis_rot.Angle;

            curr_angle = (float)axis_rot.Angle;



            //get the angle rotation to do
            end_val = 0;
            rot_todo = 0;
            if (curr_angle < 180 )
            {
                end_val = 0; //backward rotation
                rot_todo = -curr_angle;
            }
            else 
            {
                end_val = 360f; //forward rotation
                rot_todo = 360f - curr_angle;

            }

            if (rot_todo < 0.00001 && rot_todo > -0.00001)
            {
                ResetCentrationAnimation();
                return;
            }


            //create the a bezier curve
            start_val = curr_angle;

            _item_centration_animation_last_rot = start_val;
            _item_centration_animation_duration_ms = _item_centration_animation_time_animation_max_ms*(float)Math.Abs(rot_todo)/180f;


            Vector2f p0 = new Vector2f(0, start_val);
            Vector2f p1 = new Vector2f(_item_centration_animation_duration_ms / 2, start_val + (end_val - start_val) * 0.01f);
            Vector2f p2 = new Vector2f(_item_centration_animation_duration_ms / 2, end_val - (end_val - start_val) * 0.01f);
            Vector2f p3 = new Vector2f(_item_centration_animation_duration_ms, end_val);
            _item_centration_animation_curve = new CubicBezierCurve((Vector2d)p0, (Vector2d)p1, (Vector2d)p2, (Vector2d)p3);
        }
        private void DoSelectedItemAnimation()
        {
            //do nothing if there is no animation active
            if (_item_centration_animation_active)
            {

                //update current animation time
                _item_centration_animation_currtime_ms += _elapsed_frame_time;

                if (_item_centration_animation_currtime_ms > _item_centration_animation_duration_ms)
                    _item_centration_animation_currtime_ms = _item_centration_animation_duration_ms;

                //get the next wheel rot value
                float curve_x = _item_centration_animation_currtime_ms / _item_centration_animation_duration_ms;
                float rot_curr = (float)_item_centration_animation_curve.Calc((double)curve_x).Y;
                _velocity_wheel_spin = -(rot_curr - _item_centration_animation_last_rot);
                _item_centration_animation_last_rot = rot_curr;
            }
        }

        private void CheckCentrationAnimationFinished()
        {
            if (_item_centration_animation_active)
                if (_item_centration_animation_currtime_ms >= _item_centration_animation_duration_ms)
                    ResetCentrationAnimation();
        }

        private void ResetCentrationAnimation()
        {
            //quit and reset values 
            _velocity_wheel_spin = 0;
            _item_centration_animation_active = false;
            _item_centration_animation_curve = null;
            _item_centration_animation_duration_ms = 0;
            _item_centration_animation_currtime_ms = 0;
            _item_centration_animation_last_rot = 0;
        }
        #endregion

        private void RenderLoop(object sender, EventArgs e)
        {
            //update fps stats
            _elapsed_frame_time = _fps_counter.Tick();

            DoSelectedItemAnimation();

            if (_velocity_wheel_spin == 0)
                return;

            foreach (DictionaryEntry entry in _items)
            {
                ModelVisual3D visual = (ModelVisual3D)entry.Value;



                ScaleTransform3D scale = (ScaleTransform3D)((Transform3DGroup)visual.Transform).Children[0];
                RotateTransform3D rot1 = (RotateTransform3D)((Transform3DGroup)visual.Transform).Children[1];
                RotateTransform3D rot2 = (RotateTransform3D)((Transform3DGroup)visual.Transform).Children[3];

                AxisAngleRotation3D axis_rot1 = rot1.Rotation as AxisAngleRotation3D;
                AxisAngleRotation3D axis_rot2 = rot2.Rotation as AxisAngleRotation3D;

                axis_rot1.Angle -= _velocity_wheel_spin;
                axis_rot2.Angle += _velocity_wheel_spin;


                //if(axis_rot2.Angle >= 360)
                //{
                //    axis_rot2.Angle -= 360;
                //    axis_rot1.Angle += 360;
                //}
                //else if (axis_rot2.Angle <= -360)
                //{
                //    axis_rot2.Angle += 360;
                //    axis_rot1.Angle -= 360;
                //}




                //if(axis_rot2.Angle % 360 < 15 && axis_rot2.Angle % 360 > -15)
                //{
                //    scale.ScaleX = 1.1;
                //    scale.ScaleY = 1.1;
                //    scale.ScaleZ = 1.1;
                //}
                //else
                //{
                //    scale.ScaleX = 1;
                //    scale.ScaleY = 1;
                //    scale.ScaleZ = 1;
                //}
            }

            //nur wenn der maus button los gelassen wird gibts eine fadeout animation
            if (_item_centration_animation_active || _left_mouse_down)
            {
                _velocity_wheel_spin = 0;
            }
            else if (!_left_mouse_down)
            {
                //fade out animation
                _velocity_wheel_spin *= _reibung_wheel_spin;
            }


            CheckCentrationAnimationFinished();
        }




        private void Canvas_MouseMove(object sender, MouseEventArgs e)
        {
            Canvas wheel_canvas = (Canvas) sender;
            double wheel_canvas_widht = wheel_canvas.ActualWidth;

            _currpos_mouse = e.GetPosition(wheel_canvas);
            if (e.LeftButton == MouseButtonState.Pressed && !_item_centration_animation_active)
            {

                double rel_move_x = _currpos_mouse.X - _lastpos_mouse.X;

                double near_plane = (wheel_canvas_widht/2)/Math.Tan(_cam_fov/2);

                double alpha_curr = Math.Atan((_currpos_mouse.X - (wheel_canvas_widht/2))/near_plane);
                double alpha_last = Math.Atan((_lastpos_mouse.X - (wheel_canvas_widht/2))/near_plane);

                double mouse_x_curr_z0 = Math.Tan(alpha_curr)*_cam_pos_z;
                double mouse_x_last_z0 = Math.Tan(alpha_last)*_cam_pos_z;
            
                double alpha2 = 0;
                double alpha1 = 0;

                double circle_rad = Math.Tan(_cam_fov/2) * _cam_pos_z;

                alpha2 = Math.Acos(mouse_x_last_z0 / circle_rad);
                alpha1 = Math.Acos(mouse_x_curr_z0 / circle_rad);

                _velocity_wheel_spin = Vector2f.RadToDeg((float) (alpha2 - alpha1))*4;
                _lastpos_mouse = _currpos_mouse;

            }
        }

        private void Canvas_MouseDown(object sender, MouseButtonEventArgs e)
        {
            _left_mouse_down = true;
            _lastpos_mouse = e.GetPosition((Canvas)sender);


            //dblclick, check if an item is selected
            if(e.ClickCount == 2)
            {
                ModelVisual3D selected_visual=null;
                HitTestResult hitResult = VisualTreeHelper.HitTest(viewport, _currpos_mouse);
                foreach (DictionaryEntry entry in _items)
                {
                    ModelVisual3D visual = (ModelVisual3D)entry.Value;
                    if (hitResult != null && hitResult.VisualHit == visual)
                    {
                        if (visual != _selected_visual)
                            selected_visual = visual;
                        _selected_visual = visual;
                        StartItemSelectedAnimation();
                        


                        break;
                    }
                }
                if (selected_visual != null)
                    if (SelectedItemChanged != null)
                        SelectedItemChanged(this, new SelectedItemArgs(_items_inv[_selected_visual]));
            }
        }



        private void Canvas_MouseUp(object sender, MouseButtonEventArgs e)
        {
            _left_mouse_down = false;
        }

        public ICollection Keys
        {
            get
            {
                return _items.Keys;
            }
        }

        protected FpsCounter _fps_counter = new FpsCounter();
        protected float _elapsed_frame_time=0;

        protected PerspectiveCamera _cam;
        protected Hashtable _items = new Hashtable();
        protected Hashtable _items_inv = new Hashtable();
        
        protected int _initial_item_index = 0;
        protected double _circle_radius = 400;
        protected float _cam_pos_z = 700f;
        protected float _transl_cam_y = 0f;

        protected float _velocity_wheel_spin = 0f;
        protected float _reibung_wheel_spin = 0.5f;

        protected Point _lastpos_mouse;
        protected Point _currpos_mouse;
        protected bool _left_mouse_down = false;

        protected double _cam_near_plane = 10;
        protected double _cam_far_plane = 10000;
        protected double _cam_fov = 90;

        protected ModelVisual3D _selected_visual;
        public event SelectedVisualEventHandler SelectedItemChanged;
        public delegate void SelectedVisualEventHandler(object sender,SelectedItemArgs selitem_args);


        #region Item Centration Animation
        protected bool _item_centration_animation_active = false;
        protected CubicBezierCurve _item_centration_animation_curve;
        protected float _item_centration_animation_time_animation_max_ms = 2000;
        protected float _item_centration_animation_duration_ms = 0;
        protected float _item_centration_animation_currtime_ms = 0;
        protected float _item_centration_animation_last_rot = 0;
        #endregion


    }
}
