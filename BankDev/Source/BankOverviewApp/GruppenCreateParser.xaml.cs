﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BankLib.Data;
using BankLib.Gruppe;

namespace BankOverviewApp
{
    /// <summary>
    /// Interaction logic for GruppenCreateParser.xaml
    /// </summary>
    public partial class GruppenCreateParser : UserControl
    {
        public GruppenCreateParser(Gruppenzuordnung gzuo)
        {
            _gzuo = gzuo;
            InitializeComponent();
            Loaded += GruppenCreateParser_Loaded;
        }

        void GruppenCreateParser_Loaded(object sender, RoutedEventArgs e)
        {
            TabItem parent = WpfHelper.FindAncestor<TabItem>(this);
            parent.GotFocus += parent_GotFocus;
            parent.LostFocus += parent_LostFocus;
        }

        void parent_LostFocus(object sender, RoutedEventArgs e)
        {
            AppHelper.SaveTextFile(AppHelper.PathToBankData + "GruppenZuordnungen.txt", gruppen_tbox.Text);
        }

        void parent_GotFocus(object sender, RoutedEventArgs e)
        {
            string all_base_gruppen = "";
            foreach (var b in _gzuo.RawGroupInfos.Select(g => g.Value.Name + "\n").Distinct().OrderBy(b => b))
                all_base_gruppen += b;
            gruppen_base_tbox.Text = all_base_gruppen;


            gruppen_tbox.Text = AppHelper.LoadTextFile(AppHelper.PathToBankData + "GruppenZuordnungen.txt");
        }

        private void Gruppen_tbox_OnLostFocus(object sender, RoutedEventArgs e)
        {
            AppHelper.SaveTextFile(AppHelper.PathToBankData + "GruppenZuordnungen.txt", gruppen_tbox.Text);
        }

        private Gruppenzuordnung _gzuo;
    }
}
