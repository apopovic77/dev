using System;
using System.Collections.Generic;
using System.Text;

namespace MathLib
{
    public class Spline
    {


        public Spline(uint j)
        {
            _n = j;
            x = new double[_n];
            y = new double[_n];
            h = new double[_n - 1];
            A = new SparseMatrix(_n, _n);
            B = new double[_n];
            a = new double[_n - 2];
            b = new double[_n - 2];
            C = new double[_n];
            Z = new double[_n - 1];
            D = new double[_n - 1];
        }

        ~Spline() { }

        public double[] X
        {
            get { return x; }
            set { x = value; }
        }

        public double[] Y
        {
            get { return y; }
            set { y = value; }
        }

        public void CreateSpline()
        {

            for (int i = 0; i < _n - 1; i++)
                if (x[i + 1] <= x[i])
                    throw new Exception("x[n+1] should be greater than x[n]");

            for (int i = 0; i <= _n - 2; i++)
            {
                h[i] = x[i + 1] - x[i];
            }

            for (uint i = 0; i < _n; i++)
            {
                for (uint j = 0; j < _n; j++)
                {
                    if ((i == 0 && j == 0) || (i == _n - 1 && j == _n - 1))
                        A.SetValue(i, j, 1);
                    if ((i != 0 && i != _n - 1) && (j == i - 1))
                        A.SetValue(i, j, h[i - 1]);
                    if ((i != 0 && i != _n - 1) && (j == i + 1))
                        A.SetValue(i, j, h[i]);
                    if ((i != 0 && i != _n - 1) && (j == i))
                        A.SetValue(i, j, 2 * (h[i - 1] + h[i]));
                }
            }

            for (int i = 0; i < _n; i++)
            {
                if (i == 0 || i == _n - 1)
                    B[i] = 0;
                else
                    B[i] = 3 * (((y[i + 1] - y[i]) / h[i]) - ((y[i] - y[i - 1]) / h[i - 1]));
            }

            a[0] = 0;
            b[0] = 0;
            a[1] = B[1] / A.GetValue(1, 1);
            b[1] = A.GetValue(1, 2) / A.GetValue(1, 1);

            for (uint i = 2; i < _n - 2; i++)
            {
                a[i] = (B[i] - A.GetValue(i, i - 1) * a[i - 1]) / (A.GetValue(i, i) - A.GetValue(i, i - 1) * b[i - 1]);
                b[i] = A.GetValue(i, i + 1) / (A.GetValue(i, i) - A.GetValue(i, i - 1) * b[i - 1]);
            }

            C[_n - 1] = 0; C[0] = 0;
            C[_n - 2] = (B[_n - 2] - a[_n - 3] * A.GetValue(_n - 2, _n - 3)) / (A.GetValue(_n - 2, _n - 2) - A.GetValue(_n - 2, _n - 3) * b[_n - 3]);

            for (uint i = _n - 3; i > 0; i--)
                C[i] = a[i] - b[i] * C[i + 1];

            for (int i = 0; i < _n - 1; i++)
            {
                Z[i] = ((y[i + 1] - y[i]) / h[i]) - (h[i] * (C[i + 1] + 2 * C[i]) / 3);
                D[i] = (C[i + 1] - C[i]) / (3 * h[i]);
            }

        }

        public double interpolate(double v)
        {
            double u = 0;

            if (v < x[0] || v > x[_n - 1])
                throw new Exception("the value to be interpolated is out of range ");
            else
            {
                for (int i = 0; i < _n - 1; i++)
                {
                    if (v >= x[i] && v <= x[i + 1])
                    {
                        u = y[i] + (Z[i] * (v - x[i])) + (C[i] * (v - x[i]) * (v - x[i])) + (D[i] * (v - x[i]) * (v - x[i]) * (v - x[i]));
                        break;
                    }
                }
            }
            return u;
        }

        private uint _n;
        private double[] x;
        private double[] y;
        private double[] h;
        private SparseMatrix A;
        private double[] B;
        private double[] a;
        private double[] b;
        private double[] C;
        private double[] Z;
        private double[] D;


    }
}
