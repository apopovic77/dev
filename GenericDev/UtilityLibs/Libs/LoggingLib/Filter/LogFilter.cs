﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

using LoggingLib.Configuration;
using LoggingLib.Data;

namespace LoggingLib.Filter
{
    /// <summary>
    /// Class implementing the main log filtering
    /// </summary>
    internal class LogFilter
    {
        #region Nested data classes
        /// <summary>
        /// enumeration for identifying a FilterRule type
        /// </summary>
        internal enum FilterRuleType
        {
            GlobalPriorityFilter,
            GlobalSeverityFilter,
            CategoryNameFilter,
            CategoryDetailFilter
        }
        /// <summary>
        /// enumeration for identifying a FilterRule combination type
        /// </summary>
        internal enum FilterRuleCombinationType
        {
            And,
            Or
        }
        /// <summary>
        /// enumeration for filter rule check return value
        /// </summary>
        internal enum FilterCheckReturnValue
        {
            True,
            False,
            OverrideTrue,
            OverrideFalse
        }
        /// <summary>
        /// Internal filter rule class
        /// </summary>
        internal class FilterRule
        {
            #region Construction and initialization
            /// <summary>
            /// Make sure the default constructor may only be called within 
            /// this assembly. Object of these type are only for <see cref="LogFilter"/> internal
            /// data processing.
            /// </summary>
            internal FilterRule()
            {}

            /// <summary>
            /// Internal constructor generating a new filter rule instance
            /// </summary>
            /// <param name="type">Type of the filter rule</param>
            /// <param name="matchValues">Array of match values depending on the type</param>
            internal FilterRule(FilterRuleType type, string[] matchValues)
            {
                _type = type;
                _match_values = matchValues;
            }

            /// <summary>
            /// Internal constructor generating a new filter rule instance
            /// </summary>
            /// <param name="type">Type of the filter rule</param>
            /// <param name="combinationType">Rule combination type</param>
            /// <param name="matchValues">Array of match values depending on the type</param>
            internal FilterRule(FilterRuleType type, FilterRuleCombinationType combinationType, string[] matchValues)
            {
                _type = type;
                _combination_type = combinationType;
                _match_values = matchValues;
            }
            #endregion

            #region Operations
            /// <summary>
            /// Checks if the given log entry matches the current filter rule
            /// </summary>
            /// <param name="entry">Log entry for checking the rule</param>
            /// <returns>Returns false if the rule doesn't apply at the log entry</returns>
            internal FilterCheckReturnValue CheckFilterRule(LogEntry entry)
            {
                FilterCheckReturnValue bRet = FilterCheckReturnValue.True;

                switch(_type)
                {
                    case FilterRuleType.GlobalPriorityFilter:
                        {
                            // global priority filter
                            if(CheckMatchValues(1))
                            {
                                // transform the semicolon separated list of priority names
                                // to a priority array
                                Priority[] priorities = GetPriorityFromString(_match_values[0]);
                                
                                if(priorities.Length > 0)
                                {
                                    bool ruleMatch = false;

                                    foreach(Priority check in priorities)
                                    {
                                        ruleMatch |= entry.Priority == check;
                                    }

                                    if (ruleMatch)
                                        bRet = FilterCheckReturnValue.True;
                                    else
                                        bRet = FilterCheckReturnValue.False;
                                }
                            }
                        }; break;

                    case FilterRuleType.GlobalSeverityFilter:
                        {
                            // global severity filter
                            if (CheckMatchValues(1))
                            {
                                // transform the semicolon separated list of severity names
                                // to a priority array
                                Severity[] severities = GetSeveritiesFromString(_match_values[0]);

                                if (severities.Length > 0)
                                {
                                    bool ruleMatch = false;

                                    foreach (Severity check in severities)
                                    {
                                        ruleMatch |= entry.Severity == check;
                                    }

                                    if (ruleMatch)
                                        bRet = FilterCheckReturnValue.True;
                                    else
                                        bRet = FilterCheckReturnValue.False;
                                }
                            }
                        }; break;

                    case FilterRuleType.CategoryNameFilter:
                        {
                            // global severity filter
                            if (CheckMatchValues(1))
                            {
                                string matchCategoryName = _match_values[0];

                                if (!string.IsNullOrEmpty(matchCategoryName))
                                {
                                    bool ruleMatch = false;

                                    foreach(CategoryLogEntry cle in entry.CategoryLogEntries)
                                    {
                                        if(cle.CategoryName == matchCategoryName && cle.ProductName == LogManager.ProductName)
                                        {
                                            ruleMatch = true;
                                            break;
                                        }
                                    }

                                    if (ruleMatch)
                                        bRet = FilterCheckReturnValue.True;
                                    else
                                        bRet = FilterCheckReturnValue.False;
                                }
                            }
                        }; break;


                    case FilterRuleType.CategoryDetailFilter:
                        {
                            bool bcategoryFound = false;

                            // priority filter per category
                            if (CheckMatchValues(3))
                            {
                                // check if the current log entry matches the category name
                                string matchCategoryName = _match_values[0];

                                if (!string.IsNullOrEmpty(matchCategoryName))
                                {
                                    bool ruleMatch = false;

                                    foreach (CategoryLogEntry cle in entry.CategoryLogEntries)
                                    {
                                        if (cle.CategoryName == matchCategoryName && cle.ProductName == LogManager.ProductName)
                                        {
                                            ruleMatch = true;
                                            break;
                                        }
                                    }

                                    bcategoryFound = ruleMatch;

                                    if (bcategoryFound)
                                    {
                                        // transform the semicolon separated list of priority names
                                        // to a priority array
                                        Priority[] priorities = GetPriorityFromString(_match_values[1]);

                                        if (priorities.Length > 0)
                                        {
                                            bool subruleMatch = false;

                                            foreach (Priority check in priorities)
                                            {
                                                subruleMatch |= entry.Priority == check;
                                            }

                                            ruleMatch &= subruleMatch;
                                        }

                                        if (ruleMatch)
                                        {
                                            // transform the semicolon separated list of severity names
                                            // to a priority array
                                            Severity[] severities = GetSeveritiesFromString(_match_values[2]);

                                            if (severities.Length > 0)
                                            {
                                                bool subruleMatch = false;

                                                foreach (Severity check in severities)
                                                {
                                                    subruleMatch |= entry.Severity == check;
                                                }

                                                ruleMatch &= subruleMatch;
                                            }
                                        }
                                    }

                                    if(bcategoryFound)
                                    {
                                        if (ruleMatch)
                                            bRet = FilterCheckReturnValue.OverrideTrue;
                                        else
                                            bRet = FilterCheckReturnValue.OverrideFalse;
                                    } 
                                    else
                                    {
                                        bRet = FilterCheckReturnValue.False;    
                                    }
                                }
                            }
                        }; break;

                }
                return bRet;
            }

            /// <summary>
            /// Private method for checking if the machvalues array
            /// </summary>
            /// <returns>True if the array exists and has the expected number of entries</returns>
            private bool CheckMatchValues(int expectedNumberOfEntries)
            {
                if (_match_values == null || _match_values.Length != expectedNumberOfEntries)
                    return false;

                return true;
            }

            /// <summary>
            /// Generates an array of priority entries based on a semicolon
            /// separated list of priority names
            /// </summary>
            /// <param name="semicolonSeparatedPriorityList">Semicolon separated list of priority names</param>
            /// <returns>An array of priority values</returns>
            /// <remarks>Priority values which does not match the Priority enumeration type will be ignored. 
            /// You may see a debug message in the debug output window if this happens.</remarks>
            private Priority[] GetPriorityFromString(string semicolonSeparatedPriorityList)
            {
                if(string.IsNullOrEmpty(semicolonSeparatedPriorityList))
                    return new Priority[0];

                string[] priorities = semicolonSeparatedPriorityList.Split(new char[]{';', ','});

                List<Priority> priorityList = new List<Priority>();

                foreach(string curPriority in priorities)
                {
                    try
                    {
                        Priority priority = (Priority) Enum.Parse(typeof (Priority), curPriority);
                        priorityList.Add(priority);
                    }
                    catch
                    {
#if DEBUG
                        Debug.WriteLine("Priority transformation error.");
                        Debug.Indent();
                        Debug.WriteLine(string.Format("Priority '{0}' is not a valid value.", curPriority));
                        Debug.Unindent();
#endif
                    }
                }

                return priorityList.ToArray<Priority>();
            }

            /// <summary>
            /// Generates an array of severity entries based on a semicolon
            /// separated list of severity names
            /// </summary>
            /// <param name="semicolonSeparatedSeverityList">Semicolon separated list of severity names</param>
            /// <returns>An array of severity values</returns>
            /// <remarks>Severity values which does not match the Severity enumeration type will be ignored. 
            /// You may see a debug message in the debug output window if this happens.</remarks>
            private Severity[] GetSeveritiesFromString(string semicolonSeparatedSeverityList)
            {
                if (string.IsNullOrEmpty(semicolonSeparatedSeverityList))
                    return new Severity[0];

                string[] severities = semicolonSeparatedSeverityList.Split(new char[] { ';', ',' });

                List<Severity> severityList = new List<Severity>();

                foreach (string curSeverity in severities)
                {
                    try
                    {
                        Severity severity = (Severity)Enum.Parse(typeof(Severity), curSeverity);
                        severityList.Add(severity);
                    }
                    catch
                    {
#if DEBUG
                        Debug.WriteLine("Severity transformation error.");
                        Debug.Indent();
                        Debug.WriteLine(string.Format("Severity '{0}' is not a valid value.", curSeverity));
                        Debug.Unindent();
#endif
                    }
                }

                return severityList.ToArray<Severity>();
            }
            #endregion

            #region Properties
            /// <summary>
            /// Gets the filter rule type
            /// </summary>
            internal FilterRuleType RuleType
            {
                get { return _type; }
            }
            /// <summary>
            /// Gets the rule combination type
            /// </summary>
            internal FilterRuleCombinationType CombinationType
            {
                get { return _combination_type; }
            }
            #endregion

            #region Attribs
            private FilterRuleType _type = FilterRuleType.GlobalPriorityFilter;
            private FilterRuleCombinationType _combination_type = FilterRuleCombinationType.And;
            private string[] _match_values = new string[0];
            #endregion
        }
        #endregion

        #region Construction and initialization
        /// <summary>
        /// Constructor of the log filter
        /// </summary>
        /// <param name="settings">Logging configuration settings</param>
        public LogFilter(LoggingConfigSection settings)
        {
            if (settings == null)
                throw new ArgumentException("Settings must not be null.", "settings");

            _logging_enabled = settings.LogFilter.LoggingEnabled;

            BuildFilterRules(settings);
        }
        #endregion

        #region Operations and Methods
        /// <summary>
        /// Applies the log filter to a log entry
        /// </summary>
        /// <param name="entry">Log entry to apply the filter</param>
        /// <returns>Returns true is the entry should be logged, otherwise false</returns>
        public bool ApplyFilter(LogEntry entry)
        {
            // if logging is disabled - do nothing
            if (!_logging_enabled)
                return false;

            bool bGlobalRuleCheck = true;
            bool bDetailRuleCheck = true;
            bool bOverrideWithDetail = false;
            bool bFirst = true, bFirstDetail = true; ;
            FilterCheckReturnValue tmp = FilterCheckReturnValue.True;

            foreach(FilterRule curRule in _filter_rule_list)
            {
                if(curRule.RuleType == FilterRuleType.CategoryDetailFilter)
                {
                    // first rule sets the initial value of the rule check result
                    if (bFirstDetail)
                    {
                        tmp = curRule.CheckFilterRule(entry);
                        switch(tmp)
                        {
                            case FilterCheckReturnValue.True: bDetailRuleCheck = true; break;
                            case FilterCheckReturnValue.False: bDetailRuleCheck = false; break;
                            case FilterCheckReturnValue.OverrideTrue: {
                                bDetailRuleCheck = true; bOverrideWithDetail = true;
                            }
                                ; break;
                            case FilterCheckReturnValue.OverrideFalse:
                                {
                                    bDetailRuleCheck = false; bOverrideWithDetail = true;
                                }
                                ; break;
                        }
                        bFirstDetail = false;
                    }
                    else
                    {
                        switch (curRule.CombinationType)
                        {
                            case FilterRuleCombinationType.And:
                                {
                                    tmp = curRule.CheckFilterRule(entry);

                                    switch (tmp)
                                    {
                                        case FilterCheckReturnValue.True:
                                            {
                                                bDetailRuleCheck &= true;
                                            }
                                            ; break;
                                        case FilterCheckReturnValue.OverrideTrue:
                                            {
                                                bDetailRuleCheck &= true;
                                                bOverrideWithDetail = true;
                                            }
                                            ; break;
                                        case FilterCheckReturnValue.False:
                                            {
                                                bDetailRuleCheck = false;
                                            }
                                            ; break;
                                        case FilterCheckReturnValue.OverrideFalse:
                                            {
                                                bDetailRuleCheck = false;
                                                bOverrideWithDetail = true;
                                            }
                                            ; break;
                                    }
                                }; break;

                            case FilterRuleCombinationType.Or:
                                {
                                    tmp = curRule.CheckFilterRule(entry);
                                    switch (tmp)
                                    {
                                        case FilterCheckReturnValue.True:
                                            {
                                                bDetailRuleCheck = true;
                                            }
                                            ; break;
                                        case FilterCheckReturnValue.OverrideTrue:
                                            {
                                                bDetailRuleCheck = true;
                                                bOverrideWithDetail = true;
                                            }
                                            ; break;
                                        case FilterCheckReturnValue.False:
                                            {
                                                bDetailRuleCheck |= false;
                                            }
                                            ; break;
                                        case FilterCheckReturnValue.OverrideFalse:
                                            {
                                                bDetailRuleCheck |= false;
                                                bOverrideWithDetail = true;
                                            }
                                            ; break;
                                    }
                                }; break;
                        }
                    }
                } 
                else
                {
                    // first rule sets the initial value of the rule check result
                    if (bFirst)
                    {
                        tmp = curRule.CheckFilterRule(entry);
                        bGlobalRuleCheck = tmp == FilterCheckReturnValue.True;

                        bFirst = false;
                    }
                    else
                    {
                        switch (curRule.CombinationType)
                        {
                            case FilterRuleCombinationType.And:
                                {
                                    tmp = curRule.CheckFilterRule(entry);
                                    bGlobalRuleCheck &= (tmp == FilterCheckReturnValue.True);
                                }; break;

                            case FilterRuleCombinationType.Or:
                                {
                                    tmp = curRule.CheckFilterRule(entry);
                                    bGlobalRuleCheck |= (tmp == FilterCheckReturnValue.True);
                                }; break;
                        }
                    }
                }
                
            }

            if (bOverrideWithDetail)
                return bDetailRuleCheck;
            else
                return bGlobalRuleCheck;
        }
        /// <summary>
        /// Builds the internal filter rules collection
        /// </summary>
        /// <param name="settings">Logging configuration settings</param>
        private void BuildFilterRules(LoggingConfigSection settings)
        {
            if(_filter_rule_list == null)
                _filter_rule_list = new List<FilterRule>();

            // global priority filters
            if(!string.IsNullOrEmpty(settings.LogFilter.PriorityFilter))
            {
                FilterRule globalPriorityFilter = new FilterRule(FilterRuleType.GlobalPriorityFilter, FilterRuleCombinationType.And, new string[] { settings.LogFilter.PriorityFilter });
                _filter_rule_list.Add(globalPriorityFilter);
            }

            // global severity filters
            if (!string.IsNullOrEmpty(settings.LogFilter.SeverityFilter))
            {
                FilterRule globalSeverityFilter = new FilterRule(FilterRuleType.GlobalSeverityFilter, FilterRuleCombinationType.And, new string[] { settings.LogFilter.SeverityFilter });
                _filter_rule_list.Add(globalSeverityFilter);
            }

            // global category name filters
            if (!string.IsNullOrEmpty(settings.LogFilter.CategoryFilter))
            {
                FilterRule globalCategoryNameFilter = new FilterRule(FilterRuleType.CategoryNameFilter, FilterRuleCombinationType.And, new string[] { settings.LogFilter.CategoryFilter });
                _filter_rule_list.Add(globalCategoryNameFilter);
            }

            // build the category detail filters
            // these filter have a combination rule or Or 
            if( settings.LogFilter.CategoryFilterDetails.Count > 0)
            {
                foreach(CategoryFilterDetailElement cfDetail in settings.LogFilter.CategoryFilterDetails)
                {
                    if (!string.IsNullOrEmpty(cfDetail.CategoryName))
                    {
                        FilterRule categorydetailFilter = new FilterRule(FilterRuleType.CategoryDetailFilter, FilterRuleCombinationType.Or, new string[] { cfDetail.CategoryName, cfDetail.PriorityFilter, cfDetail.SeverityFilter });
                        _filter_rule_list.Add(categorydetailFilter);
                    }
                }
            }
        }
        #endregion

        #region Attribs
        private bool _logging_enabled = true;
        private List<FilterRule> _filter_rule_list = new List<FilterRule>();
        #endregion
    }
}