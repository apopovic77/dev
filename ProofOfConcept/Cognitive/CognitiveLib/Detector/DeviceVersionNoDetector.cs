﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CognitiveLib.Scanner;

namespace CognitiveLib.Detector
{
    public class VersionNoDetectionResult
    {
        public WeightedString VersionNo
        {
            get
            {
                if (VersionNoCandidates != null && VersionNoCandidates.Count > 0)
                    return VersionNoCandidates.MaxBy(o => o.Weight);
                return new WeightedString();
            }
        }
        public List<WeightedString> VersionNoCandidates;
    }
    public class DeviceVersionNoDetector : DeviceCodeDetector
    {
        public static VersionNoDetectionResult Detect(string text)
        {
            if (string.IsNullOrEmpty(text))
                throw new Exception("cannot detect, no text given");

            // die ordnung der liste spielt eine rolle
            List<WeightedString> look_for = new List<WeightedString>(5);
            look_for.Add(new WeightedString("Version No", 2));
            look_for.Add(new WeightedString("VersionNo", 2));
            look_for.Add(new WeightedString("Version"));

            VersionNoDetectionResult sres = new VersionNoDetectionResult();
            sres.VersionNoCandidates = GetDeviceCodeCandidates(text, look_for, DeviceCodeDetector.GetBlackList(new List < string > { "Code", "No" }));
            return sres;
        }
    }
}
