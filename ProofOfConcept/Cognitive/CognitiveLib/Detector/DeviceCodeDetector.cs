﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CognitiveLib.Utilities;

namespace CognitiveLib.Detector
{


    public abstract class DeviceCodeDetector
    {
        public static List<string> GetBlackList()
        {
            return _model_word_black_list;
        }
        public static List<string> GetBlackList(List<string> black_list)
        {
            black_list.AddRange(_model_word_black_list);
            return black_list;
        }

        private static List<string> _model_word_black_list = new List<string>()
        {
            "220-240V",
            "50/60hz",
            "AC220-240V",
            "AC220",
            "240V",
            "60Hz",
        };

        public static bool IsBlackListed(string maybe_model_no)
        {
            foreach (string s in _model_word_black_list)
            {
                if (maybe_model_no.IndexOf(s) >= 0)
                    return true;
            }
            return false;
        }

        public static void AddIf(List<WeightedString> list, WeightedString s_to_add, int min_wordlen = 5, int max_word_len = 30, int min_numbers_count = 3, int min_letters_count = 3)
        {
            if (!StringDetector.IsAlphaNumericWord(s_to_add.String, min_wordlen, max_word_len, min_numbers_count, min_letters_count))
                return;

            if (IsBlackListed(s_to_add.String))
                return;

            if(!list.Any(o => o.String == s_to_add.String))
                list.Add(s_to_add);
        }


        public static string RemoveAntiWords(string text, List<string> anit_words )
        {
            List<string> words = text.Split(" ".ToCharArray()).ToList();
            foreach (string anit_word in anit_words)
            {
                for (int i = 0; i < words.Count; i++)
                {
                    if (string.IsNullOrEmpty(words[i]))
                    {
                        words.RemoveAt(i);
                        i--;
                        continue;
                    }
                    if (words[i].ToLower() == anit_word.ToLower())
                    {
                        words.RemoveAt(i);
                        i--;
                        continue;
                    }
                }
            }

            return string.Join(" ", words);
        }

        /// <summary>
        /// diese methode geht durch die einzelnen wörter, wenn ein wort nicht in der liste ist wird gestoppt und alles dahinter wird angenommen ist keine code number mehr
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string TrimFromRealWords(string text)
        {
            List<string> words = text.Split(" ".ToCharArray()).ToList();
                for (int i = 0; i < words.Count; i++)
                {
                    if (string.IsNullOrEmpty(words[i]))
                    {
                        words.RemoveAt(i);
                        i--;
                        continue;
                    }
                    if (EnglishWordList.IsEnglishWord(words[i].ToLower()))
                    {
                        for (int j = words.Count-1; j >= i; j--)
                            words.RemoveAt(j);
                        break;
                    }
                }

            return string.Join(" ", words);
        }

        

        protected static List<WeightedString> GetDeviceCodeCandidates(string text, List<WeightedString> look_for, List<string> anti_words_list = null, int min_wordlen =5, int max_word_len = 30, int min_numbers_count = 3, int min_letters_count = 1)
        {
            look_for.Sort((x, y) => y.Weight.CompareTo(x.Weight));
            List<WeightedString> alpha_num_words = new List<WeightedString>(3);

            StringDetectionResults string_res = StringDetector.DetectFromList(text, look_for, StringDetector.DetectionStrategy.AllToLower);
            foreach (StringDetectionResult sdr in string_res)
                foreach (var dts in sdr.DetectedStrings)
                {
                    //check for model no right next to the detected word
                    string maybe_codeno = text.Substring(dts.DetectedStringIndex + dts.LookFor.String.Length).Split("\n".ToCharArray(), 2)[0].Replace(".", " ").Replace(":", " ").Trim();
                    maybe_codeno = RemoveAntiWords(maybe_codeno, anti_words_list);
                    maybe_codeno = TrimFromRealWords(maybe_codeno);

                    WeightedString ws_sameline = new WeightedString();
                    ws_sameline.String = maybe_codeno;
                    //the weight factor here is higher because its a value found in the same line of the look for
                    ws_sameline.WeightFactor = 5;
                    ws_sameline.Attribute = "InSameLine";
                    ws_sameline.AddWeight(dts.LookFor);

                    AddIf(alpha_num_words, ws_sameline, min_wordlen, max_word_len, min_numbers_count, min_letters_count);

                    List<string> found = StringDetector.DetectAlphaNumericWords(text, dts.DetectedStringIndex, 70, min_wordlen, max_word_len, min_numbers_count, min_letters_count);
                    foreach (string f in found)
                    {
                        string f_clear = RemoveAntiWords(f, anti_words_list);
                        WeightedString ws_alphanum = new WeightedString();
                        ws_alphanum.String = f_clear;
                        //the weight factor here is lower because its just looking around the found index, means the word must not be from the same line
                        ws_alphanum.WeightFactor = 1;
                        ws_alphanum.Attribute = "AroundLookFor";
                        ws_alphanum.AddWeight(dts.LookFor);

                        AddIf(alpha_num_words, ws_alphanum, min_wordlen, max_word_len, min_numbers_count, min_letters_count);
                    }
                }

            if (alpha_num_words.Count > 0)
                return alpha_num_words;

            List<string> found2 = StringDetector.DetectAlphaNumericWords(text, 0, text.Length, min_wordlen, max_word_len, min_numbers_count, min_letters_count);
            foreach (string f in found2)
            {
                string removed_s = RemoveAntiWords(f, anti_words_list);
                removed_s = TrimFromRealWords(removed_s);

                if (string.IsNullOrEmpty(removed_s))
                    continue;

                WeightedString ws_alphanum = new WeightedString();
                ws_alphanum.String = removed_s;
                ws_alphanum.WeightFactor = 1;
                ws_alphanum.Attribute = "ByAlphaNumPattern";

                AddIf(alpha_num_words, ws_alphanum, min_wordlen, max_word_len, min_numbers_count, min_letters_count);
            }

            return alpha_num_words;
        }
    }
}
