﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media.Animation;

namespace Logicx.WpfUtility.WindowManagement
{
    public delegate void ReportInitStatusDelegate(double progress, string state);

    public class MultiFrameWindow : Window
    {
        #region Nested classes and types
        /// <summary>
        /// Event arguments if a frame switch was completed
        /// </summary>
        public class SwitchToFrameFinishedArgs : EventArgs
        {
            public SwitchToFrameFinishedArgs(IBaseFrameBase target_frame, int target_index)
            {
                TargetFrame = target_frame;
                TargetIndex = target_index;
            }
            /// <summary>
            /// Target frame which has been acivated
            /// </summary>
            public IBaseFrameBase TargetFrame
            {
                get;
                private set;
            }
            /// <summary>
            /// Target frame index
            /// </summary>
            public int TargetIndex
            {
                get;
                private set;
            }
        }
        #endregion

        #region Constants

        private const int FRAME_SPACE_OFFSET = -2;
        private const int ANIMATION_TIME_IN_MS = 600;
        #endregion

        #region Construction and Initialization
        /// <summary>
        /// Constructor of the class
        /// </summary>
        public MultiFrameWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Constructor of the class
        /// </summary>
        public MultiFrameWindow(ReportInitStatusDelegate status_delegate)
        {
            _report_init_status = status_delegate;
            InitializeComponent();
        }
        /// <summary>
        /// Initializes the window component
        /// </summary>
        private void InitializeComponent()
        {
            _main_container_canvas = new Canvas();
            _main_container_canvas.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
            _main_container_canvas.VerticalAlignment = System.Windows.VerticalAlignment.Stretch;
            _main_container_canvas.ClipToBounds = true;
            _main_container_canvas.Background = null;
            _main_container_canvas.IsHitTestVisible = true;

            Binding binding_width = new Binding("ActualWidth");
            binding_width.Source = this;
            _main_container_canvas.SetBinding(WidthProperty, binding_width);

            Binding binding_height = new Binding("ActualHeight");
            binding_height.Source = this;
            _main_container_canvas.SetBinding(HeightProperty, binding_height);

            this.Content = _main_container_canvas;

            this.Closing += new System.ComponentModel.CancelEventHandler(MultiFrameWindow_Closing);
        }
        #endregion

        #region Operations
        public void ReportInitStatus(double perc, string status)
        {
            if (_report_init_status != null)
                _report_init_status(perc, status);
        }

        /// <summary>
        /// Adds a new BaseFrame instance to the multiframe window
        /// </summary>
        /// <param name="frame_content">New base frame instance</param>
        /// <returns>The index of the base frame</returns>
        public int AddFrame(IBaseFrameBase frame_content)
        {
            UIElement elem = frame_content as UIElement;
            if (elem == null)
                throw new ArgumentException("MultiFrameWindow kann nur BaseFrames vom Typ UIElement hinzufügen");

            if (_main_container_canvas.Children.Contains(elem))
            {
                return _child_frames.IndexOf(frame_content);
            }
            else
            {
                int newFrameIndex = 0;
                int nNewZIndex = _zindex_back;
                bool fire_switch_completed = false;

                Canvas.SetLeft(elem, 0);
                Canvas.SetTop(elem, 0);

                if (_current_frame_index == -1)
                {
                    _current_frame_index = 0;
                    nNewZIndex = _zindex_front;
                    fire_switch_completed = true;
                }
                else
                {
                    if(!double.IsNaN(this.ActualWidth) && !double.IsInfinity(this.ActualWidth))
                        Canvas.SetLeft(elem, this.ActualWidth + FRAME_SPACE_OFFSET);
                    newFrameIndex = _child_frames.Count - 1;
                }

                _child_frames.Add(frame_content);

                //frame_content.VerticalAlignment = System.Windows.VerticalAlignment.Stretch;
                //frame_content.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;

                Canvas.SetZIndex(elem, nNewZIndex);
                _main_container_canvas.Children.Add(elem);

                Binding binding_width = new Binding("ActualWidth");
                binding_width.Source = this;
                frame_content.SetBinding(WidthProperty, binding_width);

                Binding binding_height = new Binding("ActualHeight");
                binding_height.Source = this;
                frame_content.SetBinding(HeightProperty, binding_height);

                if (fire_switch_completed)
                    OnSwitchToFrameCompleted(new SwitchToFrameFinishedArgs(frame_content, 0));

                return newFrameIndex;
            }
        }
        /// <summary>
        /// Removes a base frame index from the children
        /// </summary>
        /// <param name="frame_content">Base frame to remove</param>
        public void RemoveFrame(IBaseFrameBase frame_content)
        {
            UIElement elem = frame_content as UIElement;

            if (_main_container_canvas.Children.Contains(elem))
            {
                int frame_index = _child_frames.IndexOf(frame_content);

                if(frame_index == _current_frame_index && UseSlideAnimation && _child_frames.Count > 1)
                {
                    // select frame 0
                    // delete this one afterwards
                    _remove_frame_after_switch = frame_content;
                    if(frame_index == 0)
                        SwitchToFrame(_child_frames[1]);
                    else
                        SwitchToFrame(_child_frames[0]);

                    return;
                }

                _child_frames.Remove(frame_content);
                _main_container_canvas.Children.Remove(elem);

                if(frame_index == _current_frame_index)
                {
                    _current_frame_index = -1;

                    if (_child_frames.Count > 0)
                    {
                        if (frame_index == 0)
                            SwitchToFrame(_child_frames[1]);
                        else
                            SwitchToFrame(_child_frames[0]);
                    }
                }
            }
        }
        /// <summary>
        /// Switch to the frame at the given index
        /// </summary>
        /// <param name="index">Index of the frame to activate</param>
        public void SwitchToFrame(int index)
        {
            if (index < _child_frames.Count && index >= 0 && _current_frame_index != index)
            {
                IBaseFrameBase target_frame = _child_frames[index];
                SwitchToFrame(target_frame);
            }
        }
        /// <summary>
        /// Switch to the given frame instance
        /// </summary>
        /// <param name="frame_content">Frame instance to activate</param>
        public void SwitchToFrame(IBaseFrameBase frame_content)
        {
            if (!_child_frames.Contains(frame_content))
                return;

            frame_content.Visibility = Visibility.Visible;

            if(!UseSlideAnimation)
            {
                if(_current_frame_index > -1)
                {
                    IBaseFrameBase current_frame = _child_frames[_current_frame_index];
                    if(current_frame == frame_content)
                        return;

                    if (!double.IsNaN(this.ActualWidth) && !double.IsInfinity(this.ActualWidth))
                        Canvas.SetLeft((UIElement)current_frame, this.ActualWidth + FRAME_SPACE_OFFSET);
                    Canvas.SetZIndex((UIElement)current_frame, _zindex_back);

                    current_frame.IsHitTestVisible = false;
                    current_frame.Visibility = Visibility.Collapsed;
                }

                _current_frame_index = _child_frames.IndexOf(frame_content);
                Canvas.SetLeft((UIElement)frame_content, 0);
                Canvas.SetZIndex((UIElement)frame_content, _zindex_front);

                OnSwitchToFrameCompleted(new SwitchToFrameFinishedArgs(frame_content, _current_frame_index));
            }
            else
            {
                if (_animation_running)
                    return;

                IBaseFrameBase current_frame = null;
                if (_current_frame_index > -1)
                {
                    current_frame = _child_frames[_current_frame_index];
                    if (current_frame == frame_content)
                        return;

                }

                _switch_target = frame_content;
                _switch_target_index = _child_frames.IndexOf(frame_content);

                foreach(IBaseFrameBase other_frames in _child_frames)
                {
                    if (other_frames != current_frame && other_frames != _switch_target)
                        Canvas.SetZIndex((UIElement)other_frames, _zindex_back);
                }

                if(current_frame != null)
                    Canvas.SetZIndex((UIElement)current_frame, _zindex_front);

                Canvas.SetZIndex((UIElement)_switch_target, _zindex_front+1);

                Storyboard move_out = null;
                Storyboard move_in = AnimationMoveIn();

                if(_current_frame_index < _switch_target_index)
                {
                    // den frame von rechts reinschieben
                    move_out = AnimationMoveLeftOut();

                    if (!double.IsNaN(this.ActualWidth) && !double.IsInfinity(this.ActualWidth))
                        Canvas.SetLeft((UIElement)frame_content, this.ActualWidth + FRAME_SPACE_OFFSET);

                }
                else
                {
                    // den frame von links reinschieben
                    move_out = AnimationMoveRightOut();

                    if (!double.IsNaN(this.ActualWidth) && !double.IsInfinity(this.ActualWidth))
                        Canvas.SetLeft((UIElement)frame_content, (-1) * (this.ActualWidth + FRAME_SPACE_OFFSET));
                }

                if(current_frame != null)
                {
                    current_frame.IsHitTestVisible = false;
                    move_out.Completed += new EventHandler(move_out_Completed);
                    current_frame.BeginStoryboard(move_out);
                }

                move_in.Completed += new EventHandler(move_in_Completed);
                frame_content.BeginStoryboard(move_in);

                ActivateAnimationProtection();
            }
        }

        /// <summary>
        /// Activates a mouse/kexboard protection during animation
        /// </summary>
        private void ActivateAnimationProtection()
        {
            _animation_running = true;
            //_animation_helper_canvas.IsHitTestVisible = true;
            //Canvas.SetZIndex(_animation_helper_canvas, 1000);
            _main_container_canvas.IsHitTestVisible = false;
            this.IsHitTestVisible = false;
        }
        /// <summary>
        /// Deactivate the mouse/keybord protection after the animation
        /// </summary>
        private void DeactivateAnimationProtection()
        {
            _animation_running = false;
            //_animation_helper_canvas.IsHitTestVisible = false;
            //Canvas.SetZIndex(_animation_helper_canvas, 0);
            _main_container_canvas.IsHitTestVisible = true;
            this.IsHitTestVisible = true;
        }
        /// <summary>
        /// Generates a Storyboard for moving a frame out on the left side
        /// </summary>
        /// <returns></returns>
        private Storyboard AnimationMoveLeftOut()
        {
            double target = 0;

            if (!double.IsNaN(this.ActualWidth) && !double.IsInfinity(this.ActualWidth))
                target = (-1) * (this.ActualWidth + FRAME_SPACE_OFFSET);

            DoubleAnimation left_out_animation = new DoubleAnimation(target, new Duration(TimeSpan.FromMilliseconds(ANIMATION_TIME_IN_MS)), FillBehavior.Stop);
            Storyboard.SetTargetProperty(left_out_animation, new PropertyPath( Canvas.LeftProperty));

            Storyboard sb_return = new Storyboard();
            sb_return.AccelerationRatio = 0.1;
            sb_return.DecelerationRatio = 0.4;
            sb_return.Children.Add(left_out_animation);

            return sb_return;
        }
        /// <summary>
        /// Generates a Storyboard for moving a frame out on the right side
        /// </summary>
        /// <returns></returns>
        private Storyboard AnimationMoveRightOut()
        {
            double target = 0;

            if (!double.IsNaN(this.ActualWidth) && !double.IsInfinity(this.ActualWidth))
                target = (this.ActualWidth + FRAME_SPACE_OFFSET);

            DoubleAnimation right_out_animation = new DoubleAnimation(target, new Duration(TimeSpan.FromMilliseconds(ANIMATION_TIME_IN_MS)), FillBehavior.Stop);
            Storyboard.SetTargetProperty(right_out_animation, new PropertyPath(Canvas.LeftProperty));

            Storyboard sb_return = new Storyboard();
            sb_return.AccelerationRatio = 0.1;
            sb_return.DecelerationRatio = 0.4;
            sb_return.Children.Add(right_out_animation);

            return sb_return;
        }
        /// <summary>
        /// Generates a Storyboard for moving a frame in
        /// </summary>
        /// <returns></returns>
        private Storyboard AnimationMoveIn()
        {
            double target = 0;

            DoubleAnimation in_animation = new DoubleAnimation(target, new Duration(TimeSpan.FromMilliseconds(ANIMATION_TIME_IN_MS)), FillBehavior.Stop);
            Storyboard.SetTargetProperty(in_animation, new PropertyPath(Canvas.LeftProperty));

            Storyboard sb_return = new Storyboard();
            sb_return.AccelerationRatio = 0.1;
            sb_return.DecelerationRatio = 0.4;
            sb_return.Children.Add(in_animation);

            return sb_return;
        }
        /// <summary>
        /// Fires the SwitchToFrameCompleted event
        /// </summary>
        /// <param name="e"></param>
        private void OnSwitchToFrameCompleted(SwitchToFrameFinishedArgs e)
        {
            if (e.TargetFrame != null)
                e.TargetFrame.IsHitTestVisible = true;

            if (SwitchToFrameCompleted != null)
                SwitchToFrameCompleted(this, e);

            _switch_target = null;
            _switch_target_index = -1;
        }

        internal void SyncMaximizedButtonState(IBaseFrameBase sender_frame, bool is_checked)
        {
            foreach (IBaseFrameBase cur_frame in _child_frames)
            {
                if (cur_frame != sender_frame)
                {
                    cur_frame.SetMaximizedCheck(is_checked);
                    cur_frame.SeFullscreenCheck(is_checked);
                }
            }
        }

        #endregion

        #region Event Handlers
        /// <summary>
        /// Move In animation completed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void move_in_Completed(object sender, EventArgs e)
        {
            DeactivateAnimationProtection();

            IBaseFrameBase current_frame = null;
            if (_current_frame_index > -1)
                current_frame = _child_frames[_current_frame_index];

            if (current_frame != null)
            {
                Canvas.SetZIndex((UIElement)current_frame, _zindex_back);
                current_frame.Visibility = Visibility.Collapsed;
            }

            _current_frame_index = _child_frames.IndexOf(_switch_target);
            Canvas.SetLeft((UIElement)_switch_target, 0);
            Canvas.SetZIndex((UIElement)_switch_target, _zindex_front);

            OnSwitchToFrameCompleted(new SwitchToFrameFinishedArgs(_switch_target, _switch_target_index));
        }
        /// <summary>
        /// Move out animation completed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void move_out_Completed(object sender, EventArgs e)
        {
            DeactivateAnimationProtection();
        }

        /// <summary>
        /// Main window is about to close
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void MultiFrameWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            bool canclose = true;

            foreach (IBaseFrameBase frame in _child_frames)
            {
                canclose &= frame.CanClose();
            }

            e.Cancel = !canclose;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets/Sets a flag if an animation should be used to switch between frames
        /// </summary>
        public bool UseSlideAnimation
        {
            get { return _use_slide_animation; }
            set { _use_slide_animation = value; }
        }
        /// <summary>
        /// Gets the list of child frames
        /// </summary>
        public List<IBaseFrameBase> Frames { get { return _child_frames; } }
        #endregion

        #region Dependency Properties
        #endregion

        #region Events

        public event EventHandler<SwitchToFrameFinishedArgs> SwitchToFrameCompleted;
        #endregion

        #region Routed Events
        #endregion

        #region Attributes

        private int _zindex_front = 10;
        private int _zindex_back = 5;

        private bool _animation_running = false;

        private IBaseFrameBase _remove_frame_after_switch = null;

        private IBaseFrameBase _switch_target = null;
        private int _switch_target_index = -1;

        private bool _use_slide_animation = true;
        private int _current_frame_index = -1;
        private List<IBaseFrameBase> _child_frames = new List<IBaseFrameBase>();
        private Canvas _main_container_canvas = null;

        private ReportInitStatusDelegate _report_init_status = null;
        #endregion
    }
}
