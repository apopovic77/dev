﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using Logicx.Utilities;
using Logicx.WpfUtility.Converters;
using Logicx.WpfUtility.CustomControls.OverlayManager.StatusInfo;
using Logicx.WpfUtility.CustomControls.Taskbar;
using Logicx.WpfUtility.CustomControls.Validation;
using Logicx.WpfUtility.WindowManagement;
using Logicx.WpfUtility.WpfHelpers;
using MathLib;
using TaskbarPanel = Logicx.WpfUtility.CustomControls.Taskbar.TaskbarPanel;
using Windows7.Multitouch;

namespace Logicx.WpfUtility.CustomControls.OverlayManager
{
    public delegate void OverlayWriteDebugMessageDelegate(string message);

    public class OverlayShowParams
    {
        public Point StartPoint;
        public double StartScale;
        public FrameworkElement Pic;
    }

    /// <summary>
    /// Interaction logic for OverlayManager.xaml
    /// </summary>
    public partial class OverlayManager // : IOverlayManager
    {
        public const double OVERLAY_SHADOW_IMAGE_OFFSET_X = 18;
        public const double OVERLAY_SHADOW_IMAGE_OFFSET_Y = 18;

        #region Contruction and Initialization

        private static bool? _is_multitouch_enabled = null;

        internal static bool IsMultiTouchEnabled
        {
            get
            {
                if (!_is_multitouch_enabled.HasValue)
                    _is_multitouch_enabled = TouchHandler.DigitizerCapabilities.IsMultiTouchReady;

                return _is_multitouch_enabled.Value;
            }
        }

        /// <summary>
        /// Static constructor
        /// </summary>
        static OverlayManager()
        {
            FrameworkPropertyMetadata metadataCloseButtonStyle = new FrameworkPropertyMetadata();
            CloseButtonStyleProperty = DependencyProperty.Register("CloseButtonStyle",
                                                            typeof(Style), typeof(OverlayManager), metadataCloseButtonStyle);

            FrameworkPropertyMetadata metadataPrintButtonStyle = new FrameworkPropertyMetadata();
            PrintButtonStyleProperty = DependencyProperty.Register("PrintButtonStyle",
                                                            typeof(Style), typeof(OverlayManager), metadataPrintButtonStyle);

            FrameworkPropertyMetadata metadataHelpButtonStyle = new FrameworkPropertyMetadata();
            HelpButtonStyleProperty = DependencyProperty.Register("HelpButtonStyle",
                                                            typeof(Style), typeof(OverlayManager), metadataHelpButtonStyle);

            FrameworkPropertyMetadata metadataMinimizeButtonStyle = new FrameworkPropertyMetadata();
            MinimizeButtonStyleProperty = DependencyProperty.Register("MinimizeButtonStyle",
                                                            typeof(Style), typeof(OverlayManager), metadataMinimizeButtonStyle);

            FrameworkPropertyMetadata metadataMaximizeButtonStyle = new FrameworkPropertyMetadata();
            MaximizeButtonStyleProperty = DependencyProperty.Register("MaximizeButtonStyle",
                                                            typeof(Style), typeof(OverlayManager), metadataMaximizeButtonStyle);

            FrameworkPropertyMetadata metadataNormalizeButtonStyle = new FrameworkPropertyMetadata();
            NormalizeButtonStyleProperty = DependencyProperty.Register("NormalizeButtonStyle",
                                                            typeof(Style), typeof(OverlayManager), metadataNormalizeButtonStyle);

            Style defaultBorderStyle = new Style(typeof(Border));
            defaultBorderStyle.Setters.Add(new Setter(Border.BorderBrushProperty, new SolidColorBrush(Color.FromArgb(255, 148, 148, 148))));
            defaultBorderStyle.Setters.Add(new Setter(Border.BackgroundProperty, new SolidColorBrush(Color.FromArgb(200, 150, 150, 150))));
            defaultBorderStyle.Setters.Add(new Setter(Border.BorderThicknessProperty, new Thickness(1)));
            defaultBorderStyle.Setters.Add(new Setter(Border.OpacityProperty, (double)1));
            defaultBorderStyle.Setters.Add(new Setter(Border.CornerRadiusProperty, new CornerRadius(8)));
            defaultBorderStyle.Setters.Add(new Setter(Border.PaddingProperty, new Thickness(8)));

            FrameworkPropertyMetadata metadataDialogBorderStyle = new FrameworkPropertyMetadata(defaultBorderStyle);
            DialogBorderStyleProperty = DependencyProperty.Register("DialogBorderStyle",
                                                            typeof(Style), typeof(OverlayManager), metadataDialogBorderStyle);

 
            FrameworkPropertyMetadata metadataDialogContentBorderStyle = new FrameworkPropertyMetadata(null);
            DialogContentBorderStyleProperty = DependencyProperty.Register("DialogContentBorderStyle",
                                                            typeof(Style), typeof(OverlayManager), metadataDialogContentBorderStyle);

            FrameworkPropertyMetadata metadatDialogScreenshotStyle = new FrameworkPropertyMetadata();
            DialogScreenshotStyleProperty = DependencyProperty.Register("DialogScreenshotStyle",
                                                            typeof(Style), typeof(OverlayManager), metadatDialogScreenshotStyle);

            
            FrameworkPropertyMetadata metadataOverlayBackgroundBrush = new FrameworkPropertyMetadata(new SolidColorBrush(Color.FromArgb(100, 50, 50, 50)));
            OverlayBackgroundBrushProperty = DependencyProperty.Register("OverlayBackgroundBrush",
                                                            typeof(SolidColorBrush), typeof(OverlayManager), metadataOverlayBackgroundBrush);

            ModalModePropertyKey = DependencyProperty.RegisterAttachedReadOnly("ModalMode", typeof(bool), typeof(OverlayManager), new FrameworkPropertyMetadata(false, FrameworkPropertyMetadataOptions.NotDataBindable));
            ModalModeProperty = ModalModePropertyKey.DependencyProperty;

            FrameworkPropertyMetadata metadatRightMouseClickEventReceiver = new FrameworkPropertyMetadata(null);
            RightMouseClickEventReceiverProperty = DependencyProperty.Register("RightMouseClickEventReceiver",
                                                            typeof(UIElement), typeof(OverlayManager), metadatRightMouseClickEventReceiver);

            

            FrameworkPropertyMetadata metadataFilledBackgroundBrush = new FrameworkPropertyMetadata(new SolidColorBrush(Color.FromArgb(0xFF, 0xFF, 0xFF, 0xFF)));
            StatusFilledBackgroundBrushProperty = DependencyProperty.Register("StatusFilledBackgroundBrush",
                                                            typeof(Brush), typeof(OverlayManager), metadataFilledBackgroundBrush);

            FrameworkPropertyMetadata metadataColorBgBrushBrush = new FrameworkPropertyMetadata(new SolidColorBrush(Color.FromArgb(0xFF, 0xB5, 0xE0, 0xF0)));
            StatusColorBgBrushBrushProperty = DependencyProperty.Register("StatusColorBgBrushBrush",
                                                            typeof(Brush), typeof(OverlayManager), metadataColorBgBrushBrush);

            FrameworkPropertyMetadata metadataTextBrush = new FrameworkPropertyMetadata(new SolidColorBrush(Color.FromArgb(0xFF, 0x77, 0x77, 0x77)));
            StatusTextBrushProperty = DependencyProperty.Register("StatusTextBrush",
                                                            typeof(Brush), typeof(OverlayManager), metadataTextBrush);

            FrameworkPropertyMetadata metadataLongOperationIndicatorTextBrush = new FrameworkPropertyMetadata(new SolidColorBrush(Color.FromArgb(0xFF, 0xB5, 0xE0, 0xF0)));
            StatusLongOperationIndicatorTextBrushProperty = DependencyProperty.Register("StatusLongOperationIndicatorTextBrush",
                                                            typeof(Brush), typeof(OverlayManager), metadataLongOperationIndicatorTextBrush);  
        }

        /// <summary>
        /// Konstruktor der Klasse
        /// </summary>
        public OverlayManager()
        {
            _position_array = new int[POS_DIM_Y, POS_DIM_X];
            SearchFreePositionTopOffset = POS_TOP_OFFSET;
            InitializeComponent();

            // initialisiere die liste welche die aktiven fenster verwaltet
            //_DialogWindowList = new List<OverlayDialog>();
            _DialogWindowList = new List<OverlayWindowMeta>();

            _layer_manager = new OverlayLayerManager(this);

            Loaded += new RoutedEventHandler(OverlayManager_Loaded);
            Unloaded += new RoutedEventHandler(OverlayManager_Unloaded);

            RegisterViewerEventHandlers(overlay_manager_canvas);

            Binding client_size_width_binding = new Binding("ActualWidth");
            client_size_width_binding.Source = overlay_manager_canvas;
            this.SetBinding(AvailableWindowClientWidthProperty, client_size_width_binding);

            Binding client_size_height_binding = new Binding("ActualHeight");
            client_size_height_binding.Source = overlay_manager_canvas;
            this.SetBinding(AvailableWindowClientWidthProperty, client_size_height_binding);

#if DEBUG
            WriteDebugInfo("OverlayManager initialization complete");
#endif
        }

        public void RegisterViewerEventHandlers()
        {
            RegisterViewerEventHandlers(this);
        }
        public void RegisterViewerEventHandlers(UIElement event_base)
        {
            event_base.MouseMove += new MouseEventHandler(overlay_manager_canvas_MouseMove);
            event_base.MouseUp += new MouseButtonEventHandler(overlay_manager_canvas_MouseUp);
            event_base.MouseDown += new MouseButtonEventHandler(overlay_manager_canvas_MouseDown);
            event_base.MouseEnter += new MouseEventHandler(overlay_manager_canvas_MouseEnter);
            event_base.MouseLeave += new MouseEventHandler(overlay_manager_canvas_MouseLeave);
        }

        public void DeRegisterViewerEventHandlers()
        {
            DeRegisterViewerEventHandlers(this);
        }
        public void DeRegisterViewerEventHandlers(UIElement event_base)
        {
            event_base.MouseMove -= overlay_manager_canvas_MouseMove;
            event_base.MouseUp -= overlay_manager_canvas_MouseUp;
            event_base.MouseDown -= overlay_manager_canvas_MouseDown;
            event_base.MouseEnter -= overlay_manager_canvas_MouseEnter;
            event_base.MouseLeave -= overlay_manager_canvas_MouseLeave;
        }

        private void RegisterDialogEventHandlers(OverlayWindowMeta dlgmeta)
        {
            //UiManipulation.UiManipulationManager.Instance.Register(dlgmeta.Dialog, overlay_manager_canvas, UiManipulation.UiManipulationType.MouseDragTouchDrag);

            dlgmeta.Dialog.StylusDown += new StylusDownEventHandler(Dialog_StylusDown);
            dlgmeta.Dialog.StylusMove += new StylusEventHandler(Dialog_StylusMove);
            dlgmeta.Dialog.StylusUp += new StylusEventHandler(Dialog_StylusUp);

            dlgmeta.Dialog.MouseDown += new MouseButtonEventHandler(OverlayDialog_MouseDown);
            dlgmeta.Dialog.MouseUp += new MouseButtonEventHandler(OverlayDialog_MouseUp);

            dlgmeta.DialogWindowScreenshot.MouseDown += new MouseButtonEventHandler(OverlayDialog_MouseDown);
            dlgmeta.DialogWindowScreenshot.MouseUp += new MouseButtonEventHandler(OverlayDialog_MouseUp);

            dlgmeta.Dialog.Loaded += new RoutedEventHandler(dialog_Loaded);
            dlgmeta.Dialog.Initialized += new EventHandler(dialog_Initialized);
            dlgmeta.Dialog.ActiveStateChanged += new EventHandler<ActiveStateChangeArgs>(dialog_ActiveStateChanged);
            dlgmeta.Dialog.ActiveStateChanging += new EventHandler<ActiveStateChangeingArgs>(dialog_ActiveStateChanging);
            dlgmeta.Dialog.WindowStateChanged += new EventHandler<WindowStateChangeArgs>(dialog_WindowStateChanged);
            dlgmeta.Dialog.WindowStateChanging += new EventHandler<WindowStateChangeingArgs>(dialog_WindowStateChanging);
        }
        private void DeRegisterDialogEventHandlers(OverlayWindowMeta dlgmeta)
        {
            //UiManipulation.UiManipulationManager.Instance.DeRegister(dlgmeta.Dialog);

            dlgmeta.Dialog.StylusDown -= Dialog_StylusDown;
            dlgmeta.Dialog.StylusMove -= Dialog_StylusMove;
            dlgmeta.Dialog.StylusUp -= Dialog_StylusUp;

            dlgmeta.Dialog.MouseDown -= OverlayDialog_MouseDown;
            dlgmeta.Dialog.MouseUp -= OverlayDialog_MouseUp;

            dlgmeta.DialogWindowScreenshot.MouseDown -= OverlayDialog_MouseDown;
            dlgmeta.DialogWindowScreenshot.MouseUp -= OverlayDialog_MouseUp;

            dlgmeta.Dialog.ActiveStateChanged -= dialog_ActiveStateChanged;
            dlgmeta.Dialog.ActiveStateChanging -= dialog_ActiveStateChanging;
            dlgmeta.Dialog.WindowStateChanged -= dialog_WindowStateChanged;
            dlgmeta.Dialog.WindowStateChanging -= dialog_WindowStateChanging;
        }

        private void RegisterKeyEvents(OverlayWindowMeta dlgmeta)
        {
            dlgmeta.Dialog.KeyUp += new KeyEventHandler(OverlayDialog_KeyUp);
            dlgmeta.Dialog.KeyDown += new KeyEventHandler(OverlayDialog_KeyDown);
        }

        private void DeRegisterKeyEvents(OverlayWindowMeta dlgmeta)
        {
            dlgmeta.Dialog.KeyUp -= new KeyEventHandler(OverlayDialog_KeyUp);
            dlgmeta.Dialog.KeyDown -= new KeyEventHandler(OverlayDialog_KeyDown);
        }

        private void RegisterWindowTaskBarEventHandler()
        {
            //if(this.WindowTaskBar != null)
            //{
            //    WindowTaskBar.MenuButtonClick += new EventHandler<WindowTaskBar.TaskbarButtonClickEventArgs>(WindowTaskBar_MenuButtonClick);
            //}
        }

        private void DeregisterWindowTaskBarEventHandler()
        {
            //if (this.WindowTaskBar != null)
            //    WindowTaskBar.MenuButtonClick -= new EventHandler<WindowTaskBar.TaskbarButtonClickEventArgs>(WindowTaskBar_MenuButtonClick);
        }
        #endregion

        #region Properties
        public bool WithDialogShadow
        {
            get;
            set;
        }
        public bool WithBackgroundShadow
        {
            get;
            set;
        }
        /// <summary>
        /// liefert den errorprovider des aktive dialogs
        /// </summary>
        public ErrorProvider ErrorProvider
        {
            get
            {
                if (ActiveDialogs.Count > 0)
                {
                    return UIHelper.TryFindParent<ErrorProvider>(ActiveDialogs[0].Dialog);
                }

                return null;
            }
        }

        /// <summary>
        /// Dependency Eigenschaft welche den style des Close-Buttons setzt
        /// </summary>
        public Style CloseButtonStyle
        {
            get
            {
                return (Style)GetValue(CloseButtonStyleProperty);
            }
            set
            {
                SetValue(CloseButtonStyleProperty, value);
            }
        }

        /// <summary>
        /// Dependency Eigenschaft welche den style des Print-Buttons setzt
        /// </summary>
        public Style PrintButtonStyle
        {
            get
            {
                return (Style)GetValue(PrintButtonStyleProperty);
            }
            set
            {
                SetValue(PrintButtonStyleProperty, value);
            }
        }

        /// <summary>
        /// Dependency Eigenschaft welche den style des Help-Buttons setzt
        /// </summary>
        public Style HelpButtonStyle
        {
            get
            {
                return (Style)GetValue(HelpButtonStyleProperty);
            }
            set
            {
                SetValue(HelpButtonStyleProperty, value);
            }
        }

        /// <summary>
        /// Dependency Eigenschaft welche den style des Minimize-Buttons setzt
        /// </summary>
        public Style MinimizeButtonStyle
        {
            get
            {
                return (Style)GetValue(MinimizeButtonStyleProperty);
            }
            set
            {
                SetValue(MinimizeButtonStyleProperty, value);
            }
        }

        /// <summary>
        /// Dependency Eigenschaft welche den style des Maximize-Buttons setzt
        /// </summary>
        public Style MaximizeButtonStyle
        {
            get
            {
                return (Style)GetValue(MaximizeButtonStyleProperty);
            }
            set
            {
                SetValue(MaximizeButtonStyleProperty, value);
            }
        }

        /// <summary>
        /// Dependency Eigenschaft welche den style des Maximize-Buttons setzt
        /// </summary>
        public Style NormalizeButtonStyle
        {
            get
            {
                return (Style)GetValue(NormalizeButtonStyleProperty);
            }
            set
            {
                SetValue(NormalizeButtonStyleProperty, value);
            }
        }

        /// <summary>
        /// Dependency Eigenschaft welche den style des Dialog-Borders setzt
        /// </summary>
        public Style DialogBorderStyle
        {
            get
            {
                return (Style)GetValue(DialogBorderStyleProperty);
            }
            set
            {
                SetValue(DialogBorderStyleProperty, value);
            }
        }

        /// <summary>
        /// Dependency Eigenschaft welche den style des Dialog-Borders setzt
        /// </summary>
        public Style DialogContentBorderStyle
        {
            get
            {
                return (Style)GetValue(DialogContentBorderStyleProperty);
            }
            set
            {
                SetValue(DialogContentBorderStyleProperty, value);
            }
        }

        /// <summary>
        /// Dependency Eigenschaft welche den style des dialog window screenshots
        /// </summary>
        public Style DialogScreenshotStyle
        {
            get
            {
                return (Style)GetValue(DialogScreenshotStyleProperty);
            }
            set
            {
                SetValue(DialogScreenshotStyleProperty, value);
            }
        }


                /// <summary>
        /// Dependency Eigenschaft welche das receiver element für clicks mit der rechten maustaste wird
        /// </summary>
        public UIElement RightMouseClickEventReceiver
        {
            get
            {
                return (UIElement)GetValue(RightMouseClickEventReceiverProperty);
            }
            set
            {
                SetValue(RightMouseClickEventReceiverProperty, value);
            }
        }

        

        /// <summary>
        /// Dependency Eigenschaft welche den Hintergrundbrush für den OverlayCanvas setzt
        /// </summary>
        public SolidColorBrush OverlayBackgroundBrush
        {
            get
            {
                return (SolidColorBrush)GetValue(OverlayBackgroundBrushProperty);
            }
            set
            {
                SetValue(OverlayBackgroundBrushProperty, value);
            }
        }

        public Brush StatusFilledBackgroundBrush
        {
            get { return (Brush)GetValue(StatusFilledBackgroundBrushProperty); }
            set { SetValue(StatusFilledBackgroundBrushProperty, value); }
        }

        public Brush StatusColorBgBrushBrush
        {
            get { return (Brush)GetValue(StatusColorBgBrushBrushProperty); }
            set { SetValue(StatusColorBgBrushBrushProperty, value); }
        }

        public Brush StatusTextBrush
        {
            get { return (Brush)GetValue(StatusTextBrushProperty); }
            set { SetValue(StatusTextBrushProperty, value); }
        }

        public Brush StatusLongOperationIndicatorTextBrush
        {
            get { return (Brush)GetValue(StatusLongOperationIndicatorTextBrushProperty); }
            set { SetValue(StatusLongOperationIndicatorTextBrushProperty, value); }
        }

        //internal SolidColorBrush InactiveWindowOverlayBrush
        //{
        //    get
        //    {
        //        if (!ContainsModalDialog)
        //            return OverlayBackgroundBrush;
        //        else
        //            return Brushes.Transparent; // transparenter brush um das drücken von buttons zu vermeiden
        //    }
        //}




        public double AvailableWindowClientWidth
        {
            get { return (double)GetValue(AvailableWindowClientWidthProperty); }
            set { SetValue(AvailableWindowClientWidthProperty, value); }
        }

        // Using a DependencyProperty as the backing store for AvailableWindowClientWidth.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty AvailableWindowClientWidthProperty =
            DependencyProperty.Register("AvailableWindowClientWidth", typeof(double), typeof(OverlayManager), new UIPropertyMetadata((double)0));

        public double AvailableWindowClientHeight
        {
            get { return (double)GetValue(AvailableWindowClientHeightProperty); }
            set { SetValue(AvailableWindowClientHeightProperty, value); }
        }

        // Using a DependencyProperty as the backing store for AvailableWindowClientWidth.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty AvailableWindowClientHeightProperty =
            DependencyProperty.Register("AvailableWindowClientHeight", typeof(double), typeof(OverlayManager), new UIPropertyMetadata((double)0));
        
        public OverlayWriteDebugMessageDelegate DebugWriter
        {
            get { return _debugWriter; }
            set {_debugWriter = value;}
        }


        /// <summary>
        /// Liefert true wenn der OverlayManager momentan mind. einen modalen Dialog anzeigt/beinhaltet
        /// </summary>
        public bool ContainsModalDialog
        {
            get
            {
                if (_DialogWindowList == null || _DialogWindowList.Count == 0)
                    return false;

                return _DialogWindowList.Where(x => x.Dialog.WindowState == OverlayDialogState.Modal).Count() > 0;
            }
        }

        public double SearchFreePositionTopOffset
        {
            get;
            set;
        }

        public bool ModalMode
        {
            get { return (bool) GetValue(ModalModeProperty); } 
        }

        private bool InternalModalMode
        {
            get
            {
                return (bool) GetValue(ModalModeProperty);
            }
            set
            {
                bool old_value = (bool) GetValue(ModalModeProperty);

                SetValue(ModalModePropertyKey, value);

                if(value)
                {
                    //overlay_wm_canvas.Background = OverlayBackgroundBrush;
                    Canvas.SetZIndex(overlay_manager_canvas, 1000); // modale fenster über window manager schieben
                    Canvas.SetZIndex(overlay_window_manager, 10);
                    Canvas.SetZIndex(overlay_wm_canvas, 20);
                }
                else
                {
                    //overlay_wm_canvas.Background = null;
                    Canvas.SetZIndex(overlay_manager_canvas, 1); // nicht-modale fenster hinter window manager schieben
                    Canvas.SetZIndex(overlay_window_manager, 10);
                    Canvas.SetZIndex(overlay_wm_canvas, 20);

                    if(old_value != value)
                    {
                        // wenn der modal mode ausgeschalten wurde
                        // prüfen ob es noch aktive fenster gibt
                        // wenn ja dann bei diesen fenstern den input focus wiederherstellen
                        OverlayWindowMeta lastActiveDialog = LastActiveDialogForLayer(OverlayWindowLayer.Active);

                        if (lastActiveDialog != null && lastActiveDialog.Dialog != null)
                            lastActiveDialog.Dialog.RestoreFocusedElement();
                    }
                }
            }
        }

        private OverlayLayerManager LayerManager
        {
            get { return _layer_manager; }
        }

        public Canvas MainOverlayCanvas
        {
            get { return overlay_manager_canvas; }
        }
        
        public bool MouseOverDialog
        {
            get
            {
                return _mouse_over_dialog;
            }
            set
            {
                _mouse_over_dialog = value;
                if (MouseOverDialogStateChanged != null)
                    MouseOverDialogStateChanged(this, null);
            }
        }

        public int ActiveDialogCount
        {
            get
            {
                return _DialogWindowList.Where(w => w.IsActive).Count();
            }
        }

        public bool HasActiveDialog
        {
            get
            {
                return ActiveDialogCount > 0;
            }
        }

        public IBaseFrameBase BaseFrame
        {
            get
            {
                return UIHelper.TryFindParent<BaseFrame>(this);
            }
        }

        public OverlayBlockingProgressStatusInfo StatusInformationProvider
        {
            get { return _status_information_provider; }
            set { _status_information_provider = value; }
        }
        #endregion

        #region Debug writing
        private void WriteDebugInfo(string message)
        {
#if DEBUG
            //XXX alexp, gegenwärtig deaktiviert
            //wenn der debug_writer wieder laufen soll
            //dann so implementieren das er nicht von aussen 
            //einen writer gesetzt bekommen soll
            //statt dess addframeworkelement selber verwenden
            //if (_debugWriter != null)
            //    _debugWriter(message);
            //else
            //    System.Diagnostics.Debug.WriteLine(message);

            System.Diagnostics.Debug.WriteLine(message);
#endif

        }
        #endregion

        #region Overlay management
        private void ShowOverlay()
        {
            if (ContainsModalDialog)
            {
                OverlayWindowMeta metaActiveModal = GetMetaForActiveModalDialog();

                if (metaActiveModal != null)
                {
                    if(metaActiveModal.Dialog == null || metaActiveModal.Dialog.DialogContent == null ||
                        metaActiveModal.Dialog.DialogContent.ModalBehaviour == ModalBehaviour.DarkenBackground ||
                        metaActiveModal.Dialog.DialogContent.ModalBehaviour == ModalBehaviour.DarkenBackgroundDeactivateAll ||
                        metaActiveModal.Dialog.DialogContent.ModalBehaviour == ModalBehaviour.DarkenBackgroundMinimizeAll)
                    {
                        overlay_manager_modal_canvas.Background = OverlayBackgroundBrush;

                        //overlay_manager_canvas.Background = OverlayBackgroundBrush;
                    }
                    else if (metaActiveModal.Dialog.DialogContent.ModalBehaviour == ModalBehaviour.TransparentBackground ||
                        metaActiveModal.Dialog.DialogContent.ModalBehaviour == ModalBehaviour.TransparentBackgroundDeactivateAll ||
                        metaActiveModal.Dialog.DialogContent.ModalBehaviour == ModalBehaviour.TransparentBackgroundMinimizeAll)
                    {
                        overlay_manager_modal_canvas.Background = Brushes.Transparent;
                    }
                    else
                    {
                        overlay_manager_modal_canvas.Background = OverlayBackgroundBrush;
                    }

                    Canvas.SetZIndex(overlay_manager_modal_canvas, metaActiveModal.DialogLayerZindex[OverlayWindowLayer.Modal] - 1);
                }
                else
                {
                    overlay_manager_modal_canvas.Background = OverlayBackgroundBrush;
                }
            }

#if DEBUG
            WriteDebugInfo("OverlayManager.ShowOverlay()");
#endif
        }

        private void HideOverlay()
        {
            //overlay_manager_canvas.Background = null;
            overlay_manager_modal_canvas.Background = null;

#if DEBUG
            WriteDebugInfo("OverlayManager.HideOverlay()");
#endif
        }
        #endregion

        #region Mouse-Click management
        public void DisableMouseClicks()
        {
            disable_mouse_clicks.Visibility = System.Windows.Visibility.Visible;
        }

        public void DisableMouseClicks(Color back_color)
        {
            disable_mouse_clicks.Background = new SolidColorBrush(back_color);
            disable_mouse_clicks.Visibility = System.Windows.Visibility.Visible;
        }

        public void EnableMouseClicks()
        {
            disable_mouse_clicks.Background = Brushes.Transparent;
            disable_mouse_clicks.Visibility = System.Windows.Visibility.Collapsed;
        }
        #endregion

        #region Dialog/Content management

        #region OLD single active dialog
        ///// <summary>
        ///// Liefert die Instanz des aktuell aktive Content control
        ///// </summary>
        //public OverlayContent ActiveContent
        //{
        //    get
        //    {
        //        if (_ActiveDialog == null || _ActiveDialog.Dialog == null)
        //            return null;

        //        return _ActiveDialog.Dialog.DialogContent;
        //    }
        //}

        ///// <summary>
        ///// Liefert die Instanz des aktuell aktiven OverlayDialog
        ///// </summary>
        //public OverlayWindowMeta ActiveDialogMeta
        //{
        //    get
        //    {
        //        return _ActiveDialog;
        //    }
        //}

        ///// <summary>
        ///// Liefert die Instanz des aktuell aktiven OverlayDialog
        ///// </summary>
        //public OverlayDialog ActiveDialog
        //{
        //    get
        //    {
        //        if (_ActiveDialog == null || _ActiveDialog.Dialog == null)
        //            return null;

        //        return _ActiveDialog.Dialog;
        //    }
        //}

        public List<OverlayWindowMeta> ActiveDialogs
        {
            get
            {
                return GetActiveWindows();
            }
        }
        #endregion

        public bool StylusOrigin
        {
            get { return _stylus_origin; }
        }

        public bool ContainsWindowWithContentId(string content_id)
        {
            OverlayWindowMeta contentIdMeta = _DialogWindowList.FirstOrDefault(m => m.Dialog != null && m.Dialog.DialogContent != null && m.Dialog.DialogContent.ContentId == content_id);

            return contentIdMeta != null;
        }

        public void ActivateDialogWithContentId(string content_id)
        {
            OverlayWindowMeta contentIdMeta = _DialogWindowList.FirstOrDefault(m => m.Dialog != null && m.Dialog.DialogContent != null && m.Dialog.DialogContent.ContentId == content_id);

            if(contentIdMeta != null)
            {
                SetActiveDialog(contentIdMeta, true);
            }
        }

        private List<OverlayWindowMeta> GetActiveWindows()
        {
            return _DialogWindowList.Where(x => x.IsActive).ToList();    
        }

        private List<OverlayWindowMeta> GetActiveWindowsForLayer(OverlayWindowLayer layer)
        {
            if(layer == OverlayWindowLayer.Active || layer == OverlayWindowLayer.Normal)
                return _DialogWindowList.Where(x => x.IsActive && (x.Layer == OverlayWindowLayer.Active || x.Layer == OverlayWindowLayer.Normal || (x.Layer == OverlayWindowLayer.Presentation && (x.FallbackLayer == OverlayWindowLayer.Active || x.FallbackLayer == OverlayWindowLayer.Normal)))).ToList();

            return _DialogWindowList.Where(x => x.IsActive && (x.Layer == layer || (x.Layer == OverlayWindowLayer.Presentation && (x.FallbackLayer == layer)))).ToList();
        }

        private OverlayWindowMeta LastActiveDialogForLayer(OverlayWindowLayer layer)
        {
            OverlayWindowMeta ret = null;

            if (layer == OverlayWindowLayer.Presentation)
            {
                foreach (OverlayWindowMeta dlgMeta in _DialogWindowList.OrderByDescending(x => x.DialogLayerZindex[x.FallbackLayer]).ToList())
                {
                    if (dlgMeta.Dialog.WindowState != OverlayDialogState.Minimized)
                    {
                        ret = dlgMeta;
                        break;
                    }
                }

                if (ret == null)
                {
                    foreach (OverlayWindowMeta dlgMeta in _DialogWindowList.OrderByDescending(x => x.DialogLayerZindex[OverlayWindowLayer.Active]).ToList())
                    {
                        if (dlgMeta.Dialog.WindowState != OverlayDialogState.Minimized)
                        {
                            ret = dlgMeta;
                            break;
                        }
                    }
                }

                if (ret == null)
                {
                    foreach (OverlayWindowMeta dlgMeta in _DialogWindowList.OrderByDescending(x => x.DialogLayerZindex[OverlayWindowLayer.Normal]).ToList())
                    {
                        if (dlgMeta.Dialog.WindowState != OverlayDialogState.Minimized)
                        {
                            ret = dlgMeta;
                            break;
                        }
                    }
                }
            }
            else
            {
                foreach (OverlayWindowMeta dlgMeta in _DialogWindowList.OrderByDescending(x => x.DialogLayerZindex[layer]).ToList())
                {
                    if (dlgMeta.Dialog.WindowState != OverlayDialogState.Minimized)
                    {
                        ret = dlgMeta;
                        break;
                    }
                }

                if (ret == null && layer == OverlayWindowLayer.Active)
                {
                    foreach (OverlayWindowMeta dlgMeta in _DialogWindowList.OrderByDescending(x => x.DialogLayerZindex[OverlayWindowLayer.Normal]).ToList())
                    {
                        if (dlgMeta.Dialog.WindowState != OverlayDialogState.Minimized)
                        {
                            ret = dlgMeta;
                            break;
                        }
                    }
                }

                if (ret == null && layer == OverlayWindowLayer.Normal)
                {
                    foreach (OverlayWindowMeta dlgMeta in _DialogWindowList.OrderByDescending(x => x.DialogLayerZindex[OverlayWindowLayer.Active]).ToList())
                    {
                        if (dlgMeta.Dialog.WindowState != OverlayDialogState.Minimized)
                        {
                            ret = dlgMeta;
                            break;
                        }
                    }
                }
            }
  
            return ret;
        }

        private List<OverlayWindowMeta> GetDragActiveWindows(MouseDevice device)
        {
            return _DialogWindowList.Where(x => x.IsDragActive && x.DragMouseDevice == device).ToList();
        }

        public bool HasActiveWindows()
        {
            return _DialogWindowList.FirstOrDefault(x => x.IsActive) != null;
        }

        public bool HasDragActiveWindows()
        {
            return _DialogWindowList.FirstOrDefault(x => x.IsDragActive) != null;
        }

        public bool HasResizingWindows()
        {
            return _DialogWindowList.FirstOrDefault(x => x.Dialog != null && x.Dialog.IsResizing) != null;
        }

        public List<OverlayWindowMeta> DialogWindowList
        {
            get { return _DialogWindowList; }
        }

        public List<OverlayWindowMeta> GetDialogMetasOfType<T>()
        {
            List<OverlayWindowMeta> retList = new List<OverlayWindowMeta>();

            foreach(OverlayWindowMeta meta in DialogWindowList)
            {
                if (meta.Dialog != null && meta.Dialog.DialogContent is T)
                    retList.Add(meta);
            }

            return retList;
        }

        /// <summary>
        /// Liefert/Setzt die Eigenschaft ob der aktive Dialog, welcher keine Buttons besitzt
        /// deschlossen werden soll sobald der Benutzer in den deaktivierten, grauen Hintergrundcanvas klickt.
        /// </summary>
        /// <remarks>Dialog wird nur beim Linksklick geschlossen !</remarks>
        public bool CloseButtonlessActiveDialogOnCanvasClick
        {
            get { return _close_buttonless_activedialog_on_canvas_click; }
            set { _close_buttonless_activedialog_on_canvas_click = value; }
        }


        public OverlayWindowMeta GetMetaForDialog(OverlayDialog d)
        {
            return _DialogWindowList.Where(x => x.Dialog == d).SingleOrDefault();
        }

        public OverlayWindowMeta GetMetaForDialogScreenshot(OverlayPictureHost screen)
        {
            return _DialogWindowList.Where(x => x.DialogWindowScreenshot == screen).SingleOrDefault();
        }

        public OverlayWindowMeta GetMetaForContent(ContentControl c)
        {
            return _DialogWindowList.Where(x => x.Dialog != null && x.Dialog.DialogContent == c).SingleOrDefault();
        }

        //private OverlayWindowMeta GetMetaForLoadingStoryboard(Storyboard sb)
        //{
        //    return _DialogWindowList.Where(x => x.Dialog != null && x.Dialog.DialogContent != null && 
        //        x.Dialog.DialogContent.LoadingStoryboard == sb).SingleOrDefault();
        //}

        private OverlayWindowMeta GetMetaForActiveModalDialog()
        {
            return _DialogWindowList.Where(x => x.Dialog != null && x.Dialog.WindowState == OverlayDialogState.Modal && x.IsActive).OrderByDescending(x => x.DialogLayerZindex[OverlayWindowLayer.Modal]).FirstOrDefault();
            //return _DialogWindowList.Where(x => x.Dialog != null && x.Dialog.WindowState == OverlayDialogState.Modal && x.IsActive).SingleOrDefault();
        }

        public void CloseAllDialogs()
        {
            int count = _DialogWindowList.Count;

            for (int i = 0; i < count; i++ )
            {
                CloseDialog(_DialogWindowList[0]);
            }
        }

        /// <summary>
        /// Schliesst den angegebenen Dialog
        /// </summary>
        /// <param name="dialog">Dialog der versteckt werden soll</param>
        /// <returns></returns>
        /// <remarks>Diese Methode wird von dem Eventhandler "Closed" des OverlayDialogs aufgerufen. 
        /// Die Checks bezüglich CanClose sowie forceClose müssen zu diesem Zeitpunkt bereits abgeschlossen sein.</remarks>
        private void CloseDialog(OverlayDialog dialog)
        {
            if (dialog == null)
                throw new ArgumentNullException("dialog");

            OverlayWindowMeta metaInfo = GetMetaForDialog(dialog);

            if (metaInfo == null)
                throw new ArgumentException("Dieser Dialog ist nicht im OverlayManager registriert!", "dialog");

            CloseDialog(metaInfo);
        }


        /// <summary>
        /// Schliesst den angegebenen Dialog
        /// </summary>
        /// <param name="dialog">Dialog der versteckt werden soll</param>
        /// <returns></returns>
        /// <remarks>Diese Methode wird von dem Eventhandler "Closed" des OverlayDialogs aufgerufen. 
        /// Die Checks bezüglich CanClose sowie forceClose müssen zu diesem Zeitpunkt bereits abgeschlossen sein.</remarks>
        private void CloseDialog(OverlayWindowMeta metaInfo)
        {
            if (metaInfo == null)
                throw new ArgumentNullException("metaInfo");

#if DEBUG
            WriteDebugInfo("OverlayManager.HideDialog() - Position ()");
#endif

            metaInfo.Dialog.Visibility = Visibility.Hidden;
            metaInfo.DialogThumbScreenshot.Visibility = Visibility.Hidden;
            metaInfo.DialogWindowScreenshot.Visibility = Visibility.Hidden;

            metaInfo.WaitForAnimationFinished();

            // entlade das control
            //_DialogWindowList.Remove(dialog);
            _DialogWindowList.Remove(metaInfo);

            // abmelden vom closed event
            metaInfo.Dialog.Closed -= overlayDialog_Closed;

            if (_layouted_elements.Contains(metaInfo.Dialog))
                _layouted_elements.Remove(metaInfo.Dialog);

            metaInfo.DialogMoved = false;

            DeRegisterDialogEventHandlers(metaInfo);
            DeRegisterKeyEvents(metaInfo);

            try
            {
                List<Key> keys = _key_down_origins.Where(k => k.Value == metaInfo.Dialog).Select(k => k.Key).ToList();
                foreach (Key rem_key in keys)
                    _key_down_origins.Remove(rem_key);
            }
            catch(Exception ex)
            {
                Debug.WriteLine("###### REMOVE KEY DOWN ORIGINS ERROR ####");
                Debug.WriteLine(ex);
            }

            // wenn dieser dialog gerade aktiv ist
            //if (metaInfo.IsActive)
            {
                //metaInfo.IsActive = false;
                overlay_manager_canvas.Children.Remove(metaInfo.Dialog);
                overlay_manager_canvas.Children.Remove(metaInfo.DialogWindowScreenshot);
                overlay_window_manager.RemoveMinimizedWindow(metaInfo);

                metaInfo.DialogWindowScreenshot.FreeImageResource(true);
                metaInfo.DialogThumbScreenshot.FreeImageResource(true);
                metaInfo.DialogThumbScreenshot.RemoveInformationControl(false);
            }



            if (metaInfo.Layer == OverlayWindowLayer.Modal)
            {
                // modal behaviour berücksichtigen wenn der dialog aktiv wird
                if (metaInfo.Dialog.DialogContent.ModalBehaviour == ModalBehaviour.DarkenBackgroundDeactivateAll ||
                    metaInfo.Dialog.DialogContent.ModalBehaviour == ModalBehaviour.TransparentBackgroundDeactivateAll)
                {
                    if (metaInfo.ReactivateAfterModalClose != null && metaInfo.ReactivateAfterModalClose.Count > 0)
                    {
                        foreach(OverlayWindowMeta ractivate in metaInfo.ReactivateAfterModalClose)
                        {
                            ractivate.IsActive = true;
                        }
                    }

                    if (metaInfo.ReactivateAfterModalClose != null)
                        metaInfo.ReactivateAfterModalClose.Clear();

                }
                else if (metaInfo.Dialog.DialogContent.ModalBehaviour == ModalBehaviour.DarkenBackgroundMinimizeAll ||
                    metaInfo.Dialog.DialogContent.ModalBehaviour == ModalBehaviour.TransparentBackgroundMinimizeAll)
                {
                    if (metaInfo.NormalizeAfterModalClose != null && metaInfo.NormalizeAfterModalClose.Count > 0)
                    {
                        foreach (OverlayWindowMeta normalize in metaInfo.NormalizeAfterModalClose)
                        {
                            normalize.Dialog.NormalStateDialog();
                        }
                    }

                    if (metaInfo.NormalizeAfterModalClose != null)
                        metaInfo.NormalizeAfterModalClose.Clear();

                    if (metaInfo.ModalAfterModalClose != null && metaInfo.ModalAfterModalClose.Count > 0)
                    {
                        foreach (OverlayWindowMeta normalize in metaInfo.ModalAfterModalClose)
                        {
                            normalize.Dialog.ModalStateDialog();
                        }
                    }

                    if (metaInfo.ModalAfterModalClose != null)
                        metaInfo.ModalAfterModalClose.Clear();
                }
            }

            // wenn noch andere dialoge aktiv sind, dann zeige den vorherigen
            // Dialog wierder an
            if (_DialogWindowList.Count > 0)
            {
                OverlayWindowMeta lastDialogMeta = LastActiveDialogForLayer(metaInfo.Layer);


                if (lastDialogMeta == null)
                {
                    lastDialogMeta = _DialogWindowList.Where(d => d.Dialog.WindowState != OverlayDialogState.Minimized).LastOrDefault();
                }

                if ((lastDialogMeta != null) && (lastDialogMeta.Dialog != null) && (!ActiveDialogs.Contains(lastDialogMeta) || lastDialogMeta.Dialog.WindowState == OverlayDialogState.Modal))
                {
                    OverlayDialog lastDialog = lastDialogMeta.Dialog;

                    // den content wieder auf die Anzeige vorbereiten
                    if (lastDialog.DialogContent.BeforeShow(false))
                    {
                        if (lastDialogMeta.IsActive)
                            lastDialogMeta.IsActive = false;

                        lastDialogMeta.IsActive = true;
                        lastDialog.SetInputFocus();

#if DEBUG
                        WriteDebugInfo("OverlayManager.HideDialog() - Last Dialog activated");
#endif
                    }
                }
            }
            //else

            if(!ContainsModalDialog)
            {
                // wenn dies der letzte offene Dialog war
                // dann das overlay wieder verstecken
                HideOverlay();
            }
            else
            {
                ShowOverlay();
            }

            InternalModalMode = ContainsModalDialog;

            // fire closed event
            metaInfo.Dialog.DialogContent.InternalAfterClose();
            metaInfo.OverlayAnimationFinished -= OverlayAnimation_Completed;

            if (_running_minimizing_animations.Contains(metaInfo.Dialog))
                _running_minimizing_animations.Remove(metaInfo.Dialog);

            BindingOperations.ClearBinding(metaInfo.Dialog, OverlayDialog.HelpButtonStyleProperty);
            BindingOperations.ClearBinding(metaInfo.Dialog, OverlayDialog.MaximizeButtonStyleProperty);
            BindingOperations.ClearBinding(metaInfo.Dialog, OverlayDialog.MaximizingAnimationFinalPosProperty);
            BindingOperations.ClearBinding(metaInfo.Dialog, OverlayDialog.MaximizingAnimationProperty);
            BindingOperations.ClearBinding(metaInfo.Dialog, OverlayDialog.MaxWidthProperty);
            BindingOperations.ClearBinding(metaInfo.Dialog, OverlayDialog.CloseButtonStyleProperty);
            BindingOperations.ClearBinding(metaInfo.Dialog, OverlayDialog.IsActiveProperty);
            BindingOperations.ClearBinding(metaInfo.Dialog, OverlayDialog.MinimalSideLengthProperty);
            BindingOperations.ClearBinding(metaInfo.Dialog, OverlayDialog.MinimizeButtonStyleProperty);
            BindingOperations.ClearBinding(metaInfo.Dialog, OverlayDialog.MinimizingAnimationFinalPosProperty);
            BindingOperations.ClearBinding(metaInfo.Dialog, OverlayDialog.MinimizingAnimationProperty);
            BindingOperations.ClearBinding(metaInfo.Dialog, OverlayDialog.NormalizeButtonStyleProperty);
            BindingOperations.ClearBinding(metaInfo.Dialog, OverlayDialog.NormalizingAnimationFinalPosProperty);
            BindingOperations.ClearBinding(metaInfo.Dialog, OverlayDialog.NormalizingAnimationProperty);
            BindingOperations.ClearBinding(metaInfo.Dialog, OverlayDialog.OverlayBackgroundBrushProperty);
            BindingOperations.ClearBinding(metaInfo.Dialog, OverlayDialog.OverlayManagerModalModeProperty);
            BindingOperations.ClearBinding(metaInfo.Dialog, OverlayDialog.PrintButtonStyleProperty);
            BindingOperations.ClearBinding(metaInfo.Dialog, OverlayDialog.WindowStateProperty);

            metaInfo.Dispose();

            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.Collect();

#if DEBUG
            WriteDebugInfo("OverlayManager.HideDialog() - Active Dialog removed");
#endif

        }

        #endregion

        #region IOverlayManager interface implementation
        public void BeginAnimation(FrameworkElement elem, Storyboard sb)
        {
            AddFrameworkElement(elem);
      
            sb.Begin(elem);
        }

        public void AddFrameworkElement(FrameworkElement elem)
        {
            if (!overlay_manager_canvas.Children.Contains(elem))
                overlay_manager_canvas.Children.Add(elem);
        }

        public void RemoveFrameworkElement(FrameworkElement elem)
        {
            if (overlay_manager_canvas.Children.Contains(elem))
                overlay_manager_canvas.Children.Remove(elem);
        }

        public OverlayWindowManager OverlayWindowManager
        {
           get
           {
               return overlay_window_manager;
           }
        }

        /// <summary>
        /// Aktiviert den Overlay und zeigt das Control als modalen Dialog
        /// </summary>
        /// <param name="content">Content control welches von OverlayContent (siehe</param>
        /// <returns>true wenn der content angezeigt wurde</returns>
        public bool Show(ContentControl content)
        {
            OverlayDialogState defState = OverlayDialogState.Modal;

            if (content is OverlayContent)
                defState = ((OverlayContent) content).DefaultWindowState;

            return Show(content, defState, null);

        }

        public bool Show(ContentControl content, OverlayDialogState state)
        {
            return Show(content, state, null);
        }


        /// <summary>
        /// Aktiviert den Overlay und zeigt das Control als Dialog
        /// </summary>
        /// <param name="content">Content control welches von OverlayContent (siehe</param>
        /// <param name="state">Dialog window state</param>
        /// <returns>true wenn der content angezeigt wurde</returns>
        public bool Show(ContentControl content, OverlayDialogState state, OverlayShowParams param)
        {
            if (content == null)
                throw new ArgumentNullException("content");

            OverlayContent contentControl = content as OverlayContent;

            if (contentControl == null)
                throw new Exception("Der 'content' Parameter ist nicht vom Typ OverlayControl!");

           OverlayContent icontentControl = content as OverlayContent;

            if (icontentControl == null)
                throw new Exception("Der 'content' Parameter implementiert nicht das interface IOverlayContent!");

            if (icontentControl.Disposed)
                throw new ObjectDisposedException("OverlayContent");

            contentControl.DialogImageDirty = true;

            if(!_overlay_manager_loaded)
            {
#if DEBUG
                Debug.WriteLine("*** MARKED SHOW() AS DELAYED");
#endif
                // delayed show after overlaymanager has been loaded into visual tree
                KeyValuePair<ContentControl, OverlayDialogState> dlgData = new KeyValuePair<ContentControl, OverlayDialogState>(content, state);
                _delayed_show.Add(dlgData);
                return true;
            }

            OverlayDialog hostDialog = new OverlayDialog(state == OverlayDialogState.Modal ? state : OverlayDialogState.Normal);
            hostDialog.AutoResized = icontentControl.AutoResized;
            hostDialog.SearchFreePosition = icontentControl.SearchFreePosition;
            hostDialog.SearchFreePositionMargin = icontentControl.SearchFreePositionMargin;
            hostDialog.SearchPositionAlignment = icontentControl.SearchPositionAlignment;
            
            #region style bindings
            // binding for the close button style
            Binding bindingCloseButtonStyle = new Binding("CloseButtonStyle");
            bindingCloseButtonStyle.Mode = BindingMode.Default;
            bindingCloseButtonStyle.Source = this;

            if (contentControl.CloseButtonStyle != null)
                bindingCloseButtonStyle.Source = contentControl;

            hostDialog.SetBinding(OverlayDialog.CloseButtonStyleProperty, bindingCloseButtonStyle);

            // binding for the help button style
            Binding bindingHelpButtonStyle = new Binding("HelpButtonStyle");
            bindingHelpButtonStyle.Mode = BindingMode.Default;
            bindingHelpButtonStyle.Source = this;
            
            if (contentControl.HelpButtonStyle != null)
                bindingHelpButtonStyle.Source = contentControl;

            hostDialog.SetBinding(OverlayDialog.HelpButtonStyleProperty, bindingHelpButtonStyle);

            // binding for the print button style
            Binding bindingPrintButtonStyle = new Binding("PrintButtonStyle");
            bindingPrintButtonStyle.Mode = BindingMode.Default;
            bindingPrintButtonStyle.Source = this;

            if (contentControl.PrintButtonStyle != null)
                bindingPrintButtonStyle.Source = contentControl;

            hostDialog.SetBinding(OverlayDialog.PrintButtonStyleProperty, bindingPrintButtonStyle);

            // binding for the minimize button style
            Binding bindingMinimizeButtonStyle = new Binding("MinimizeButtonStyle");
            bindingMinimizeButtonStyle.Mode = BindingMode.Default;
            bindingMinimizeButtonStyle.Source = this;

            if (contentControl.MinimizeButtonStyle != null)
                bindingMinimizeButtonStyle.Source = contentControl;

            hostDialog.SetBinding(OverlayDialog.MinimizeButtonStyleProperty, bindingMinimizeButtonStyle);

            // binding for the maximize button style
            Binding bindingMaximizeButtonStyle = new Binding("MaximizeButtonStyle");
            bindingMaximizeButtonStyle.Mode = BindingMode.Default;
            bindingMaximizeButtonStyle.Source = this;

            if (contentControl.MaximizeButtonStyle != null)
                bindingMaximizeButtonStyle.Source = contentControl;

            hostDialog.SetBinding(OverlayDialog.MaximizeButtonStyleProperty, bindingMaximizeButtonStyle);

            // binding for the normalize button style
            Binding bindingNormalizeButtonStyle = new Binding("NormalizeButtonStyle");
            bindingNormalizeButtonStyle.Mode = BindingMode.Default;
            bindingNormalizeButtonStyle.Source = this;

            if (contentControl.NormalizeButtonStyle != null)
                bindingNormalizeButtonStyle.Source = contentControl;

            hostDialog.SetBinding(OverlayDialog.NormalizeButtonStyleProperty, bindingNormalizeButtonStyle);
           
            // binding for the overlay background brush
            Binding bindingOverlayBackgroundBrush = new Binding("OverlayBackgroundBrush");
            bindingOverlayBackgroundBrush.Mode = BindingMode.Default;
            bindingOverlayBackgroundBrush.Source = this;
            hostDialog.SetBinding(OverlayDialog.OverlayBackgroundBrushProperty, bindingOverlayBackgroundBrush);

            // binding for the overlay background brush
            Binding bindingResizeGripBrush = new Binding("ResizeGripBrush");
            bindingResizeGripBrush.Mode = BindingMode.Default;
            bindingResizeGripBrush.Source = this;
            hostDialog.SetBinding(OverlayDialog.ResizeGripBrushProperty, bindingResizeGripBrush);

            Binding bindingModalMode = new Binding("ModalMode");
            bindingModalMode.Mode = BindingMode.OneWay;
            bindingModalMode.Source = this;
            hostDialog.SetBinding(OverlayDialog.OverlayManagerModalModeProperty, bindingModalMode);

            Binding binding_dialog_title = new Binding("OverlayDialogTitleStyle");
            binding_dialog_title.Mode = BindingMode.OneWay;
            binding_dialog_title.Source = content;
            hostDialog.overlaydialog_bold_header_text.SetBinding(TextBlock.StyleProperty, binding_dialog_title);
            if (contentControl.OverlayDialogTitleStyle == null)
            {
                hostDialog.overlaydialog_bold_header_text.Style = (Style) FindResource("overlaydialog_bold_header_style");
                if (DefaultOverlayDialogTitleStyle != null)
                    hostDialog.overlaydialog_bold_header_text.Style = DefaultOverlayDialogTitleStyle;
            }


            Binding binding_dialog_subtitle = new Binding("OverlayDialogSubTitleStyle");
            binding_dialog_subtitle.Mode = BindingMode.OneWay;
            binding_dialog_subtitle.Source = content;
            hostDialog.overlaydialog_normal_header_text.SetBinding(TextBlock.StyleProperty, binding_dialog_subtitle);
            if (contentControl.OverlayDialogSubTitleStyle == null)
                hostDialog.overlaydialog_normal_header_text.Style = (Style)FindResource("overlaydialog_normal_header_style");

            bool with_dialog_shadow = WithDialogShadow;
            bool with_background_shadow = WithBackgroundShadow;
            if (contentControl.EnableDialogShadow.HasValue)
                with_dialog_shadow = contentControl.EnableDialogShadow.Value;
            if (contentControl.EnableBackgroundShadow.HasValue)
                with_background_shadow = contentControl.EnableBackgroundShadow.Value;

            if (DialogContentBorderStyle != null || contentControl.DialogContentBorderStyle != null)
            {
                hostDialog.DialogBorderStyle = null;

                // binding for the dialog border style
                Binding bindingDialogContentBorderStyle = new Binding("DialogContentBorderStyle");
                bindingDialogContentBorderStyle.Mode = BindingMode.Default;
                bindingDialogContentBorderStyle.Source = this;
                if (contentControl.DialogContentBorderStyle != null)
                    bindingDialogContentBorderStyle.Source = contentControl;
                hostDialog.SetBinding(OverlayDialog.DialogContentBorderStyleProperty, bindingDialogContentBorderStyle);

                if (with_background_shadow)
                {
                    hostDialog.overlay_border_shadow_grid.SetBinding(StyleProperty, bindingDialogContentBorderStyle);
                    hostDialog.overlay_border_shadow_grid.Visibility = System.Windows.Visibility.Visible;
                    hostDialog.dialog_border.Background = Brushes.Transparent;
                    hostDialog.dialog_border.BorderThickness = new Thickness(0);
                }
                else if (with_dialog_shadow)
                {
                    hostDialog.overlay_border_shadow_grid.Visibility = System.Windows.Visibility.Collapsed;
                    hostDialog.dialog_border.Effect = (Effect)FindResource("shadow_effect");
                }
            }
            else
            {
                // binding for the dialog border style
                Binding bindingDialogBorderStyle = new Binding("DialogBorderStyle");
                bindingDialogBorderStyle.Mode = BindingMode.Default;
                bindingDialogBorderStyle.Source = this;
                if (contentControl.DialogBorderStyle != null)
                    bindingDialogBorderStyle.Source = contentControl;
                hostDialog.SetBinding(OverlayDialog.DialogBorderStyleProperty, bindingDialogBorderStyle);

                if (with_background_shadow)
                {
                    hostDialog.overlay_border_shadow_grid.SetBinding(StyleProperty, bindingDialogBorderStyle);
                    hostDialog.overlay_border_shadow_grid.Visibility = System.Windows.Visibility.Visible;
                    hostDialog.dialog_border.Background = Brushes.Transparent;
                    hostDialog.dialog_border.BorderThickness = new Thickness(0);
                }
                else if (with_dialog_shadow)
                {
                    hostDialog.overlay_border_shadow_grid.Visibility = System.Windows.Visibility.Collapsed;
                    hostDialog.dialog_border.Effect = (Effect)FindResource("shadow_effect");
                }
            }

            #endregion

            hostDialog.InitializeContent(contentControl);

            if (contentControl.InitializationBehaviour == DataInitializationBehaviour.Immediate)
            {
                contentControl.InternalInitialize(!Dispatcher.CheckAccess());
            }

            if (contentControl.BeforeShow(true))
            {
                hostDialog.Closed += new EventHandler(overlayDialog_Closed);

                OverlayWindowMeta newMeta = new OverlayWindowMeta();
                Binding bindingScreenshotStyle = new Binding("DialogScreenshotStyle");
                bindingScreenshotStyle.Mode = BindingMode.OneWay;
                bindingScreenshotStyle.Source = this;
                newMeta.DialogWindowScreenshot.SetBinding(OverlayPictureHost.StyleProperty, bindingScreenshotStyle);

                newMeta.ShowParams = param;
                newMeta.Dialog = hostDialog;
                hostDialog.DialogMeta = newMeta;
                newMeta.OverlayAnimationFinished += new EventHandler<OverlayWindowMeta.OverlayAnimationCompletedArgs>(OverlayAnimation_Completed);
                
                #region Error-/changeInfo Indicator für thumbnail screenshot image
                OverlayThumbErrorChangeIndicator thumbErrorIndicator = new OverlayThumbErrorChangeIndicator();
                Binding bindShowError = new Binding("IsValid");
                bindShowError.Source = hostDialog.ErrorProvider;
                bindShowError.Converter = new InverseBooleanConverter();
                thumbErrorIndicator.SetBinding(OverlayThumbErrorChangeIndicator.ShowErrorProperty, bindShowError);

                Binding bindShowChangeInfo = new Binding("HasChangeData");
                bindShowChangeInfo.Source = hostDialog.ChangeInfoProvider;
                thumbErrorIndicator.SetBinding(OverlayThumbErrorChangeIndicator.ShowChangeInfoProperty, bindShowChangeInfo);

                Binding bindErrorCount = new Binding("ErrorCount");
                bindErrorCount.Source = hostDialog.ErrorProvider;
                thumbErrorIndicator.SetBinding(OverlayThumbErrorChangeIndicator.ErrorCountProperty, bindErrorCount);
                #endregion

                newMeta.DialogThumbScreenshot.SetInformationControl(thumbErrorIndicator);

                if(param != null)
                {
                    newMeta.Dialog.DialogContent.LoadingStartStoryboard = null;
                    contentControl.AutoCenteredDialog = false;
                    ScaleTransform s_tran = param.Pic.RenderTransform as ScaleTransform;
                    Image img = (Image) param.Pic;
                    newMeta.DialogWindowScreenshot.SetImage(img);
                    newMeta.DialogWindowScreenshot.Width = img.ActualWidth;
                    newMeta.DialogWindowScreenshot.Height = img.ActualHeight;
                    newMeta.DialogWindowScreenshot.ImageRenderOffsetWidth = contentControl.ImageRenderOffsetWidth;
                    newMeta.DialogWindowScreenshot.ImageRenderOffsetHeight = contentControl.ImageRenderOffsetHeight;

                    if(s_tran != null)
                    {
                        newMeta.DialogWindowScreenshot.Width *= s_tran.ScaleX;
                        newMeta.DialogWindowScreenshot.Height *= s_tran.ScaleY;
                        newMeta.DialogWindowScreenshot.ImageRenderOffsetWidth *= s_tran.ScaleX;
                        newMeta.DialogWindowScreenshot.ImageRenderOffsetHeight *= s_tran.ScaleY;
                    }
                    newMeta.DialogWindowScreenshot.ScreenshotImage.Stretch = Stretch.Uniform;
                    newMeta.Dialog.Visibility = Visibility.Hidden;
                    newMeta.DialogWindowScreenshot.Visibility = Visibility.Visible;
                    newMeta.IgnoreMouseInputs = true;
                    
                    //if (param.OverlayStatusInformation != null)
                    //    newMeta.StatusInformationControl = param.OverlayStatusInformation;
                }

                // den neuen dialog in die liste der dialoge aufnehmen
                _DialogWindowList.Add(newMeta);

                if (contentControl.InitializationBehaviour == DataInitializationBehaviour.Immediate)
                    newMeta.InitializationFinshed = true;

                switch(state)
                {
                    case OverlayDialogState.Modal:
                        newMeta.Layer = OverlayWindowLayer.Modal;
                        break;
                    default:
                        newMeta.Layer = OverlayWindowLayer.Normal;
                        break;
                }
              
                //if(_DialogWindowList.Count > 0)
                //{
                //    // markiere alle dialoge ausser den neuen als inaktiv
                //    foreach (OverlayWindowMeta metaDlg in _DialogWindowList)
                //    {
                //        if (metaDlg.Layer == newMeta.Layer && metaDlg != newMeta)
                //        {
                //            metaDlg.IsActive = false;
                //            LayerManager.SetLastActiveDialogPerLayer(metaDlg.Layer, metaDlg);
                //        }
                //    }
                //}

                // den aktiven dialog speichern
                //newMeta.IsActive = true;
                newMeta.IsDragActive = false;

                if (newMeta.Layer == OverlayWindowLayer.Modal)
                {
                    if(_animate_all_modal_dialogs)
                    {
                        newMeta.Dialog.DialogContent.InitializationBehaviour = DataInitializationBehaviour.Background;
                        newMeta.DialogWindowScreenshot.Visibility = System.Windows.Visibility.Hidden;
                    }
                    
                    // modal behaviour berücksichtigen wenn der dialog aktiv wird
                    if (newMeta.Dialog.DialogContent.ModalBehaviour == ModalBehaviour.DarkenBackgroundDeactivateAll ||
                        newMeta.Dialog.DialogContent.ModalBehaviour == ModalBehaviour.TransparentBackgroundDeactivateAll)
                    {
                        if (newMeta.ReactivateAfterModalClose == null)
                            newMeta.ReactivateAfterModalClose = new List<OverlayWindowMeta>();

                        newMeta.ReactivateAfterModalClose.Clear();

                        // wenn dieser dialog modal wird, werden alle anderen deaktiviert
                        // auch die aktiven dialoge auf den unteren ebenen im layer manager
                        foreach (OverlayWindowMeta dlgcheck in _DialogWindowList)
                        {
                            if (dlgcheck != newMeta && dlgcheck.Layer != OverlayWindowLayer.Modal && dlgcheck.IsActive)
                            {
                                newMeta.ReactivateAfterModalClose.Add(dlgcheck);
                                dlgcheck.IsActive = false;
                            }
                        }
                    }
                    else if (newMeta.Dialog.DialogContent.ModalBehaviour == ModalBehaviour.DarkenBackgroundMinimizeAll ||
                        newMeta.Dialog.DialogContent.ModalBehaviour == ModalBehaviour.TransparentBackgroundMinimizeAll)
                    {
                        if (newMeta.NormalizeAfterModalClose == null)
                            newMeta.NormalizeAfterModalClose = new List<OverlayWindowMeta>();

                        newMeta.NormalizeAfterModalClose.Clear();

                        if (newMeta.ModalAfterModalClose == null)
                            newMeta.ModalAfterModalClose = new List<OverlayWindowMeta>();

                        newMeta.ModalAfterModalClose.Clear();

                        foreach (OverlayWindowMeta dlgcheck in _DialogWindowList)
                        {
                            if (dlgcheck != newMeta && dlgcheck.Dialog.WindowState != OverlayDialogState.Minimized)
                            {
                                if(dlgcheck.Dialog.WindowState == OverlayDialogState.Modal)
                                    newMeta.ModalAfterModalClose.Add(dlgcheck);
                                else
                                    newMeta.NormalizeAfterModalClose.Add(dlgcheck);
                                dlgcheck.Dialog.MinimizeStateDialog();
                            }
                        }
                    }
                }

                // calculate zindex of all layers for this dialog and other dialogs
                LayerManager.RecalculateLayers(newMeta);

                // den aktuellen dialog in die child controls aufnehmen
                overlay_manager_canvas.Children.Add(newMeta.Dialog);
                overlay_manager_canvas.Children.Add(newMeta.DialogWindowScreenshot);

                #region style binding status control
                //Binding bindingFilledBackgroundBrush = new Binding("StatusFilledBackgroundBrush");
                //bindingFilledBackgroundBrush.Mode = BindingMode.Default;
                //bindingFilledBackgroundBrush.Source = this;
                //newMeta.StatusInformationControl.SetBinding(OverlayBlockingProgressStatusInfo.FilledBackgroundBrushProperty, bindingFilledBackgroundBrush);

                //Binding bindingColorBgBrushBrush = new Binding("StatusColorBgBrushBrush");
                //bindingColorBgBrushBrush.Mode = BindingMode.Default;
                //bindingColorBgBrushBrush.Source = this;
                //newMeta.StatusInformationControl.SetBinding(OverlayBlockingProgressStatusInfo.ColorBgBrushBrushProperty, bindingColorBgBrushBrush);

                //Binding bindingTextBrush = new Binding("StatusTextBrush");
                //bindingTextBrush.Mode = BindingMode.Default;
                //bindingTextBrush.Source = this;
                //newMeta.StatusInformationControl.SetBinding(OverlayBlockingProgressStatusInfo.TextBrushProperty, bindingTextBrush);

                //Binding bindingLongOperationIndicatorTextBrush = new Binding("StatusLongOperationIndicatorTextBrush");
                //bindingLongOperationIndicatorTextBrush.Mode = BindingMode.Default;
                //bindingLongOperationIndicatorTextBrush.Source = this;
                //newMeta.StatusInformationControl.SetBinding(OverlayBlockingProgressStatusInfo.LongOperationIndicatorTextBrushProperty, bindingLongOperationIndicatorTextBrush);
                #endregion

                //if (!overlay_manager_canvas.Children.Contains(newMeta.StatusInformationControl))
                //    overlay_manager_canvas.Children.Add(newMeta.StatusInformationControl);

                RegisterDialogEventHandlers(newMeta);

                if(param != null)
                {
                    SetDialogPosition(newMeta, new Vector2f((float)param.StartPoint.X, (float)param.StartPoint.Y));
                }

                hostDialog.SetInputFocus();

                // will center the dialog on the canvas after the first initialization
                hostDialog.SizeChanged += new SizeChangedEventHandler(_ActiveDialog_SizeChanged);

                if (state == OverlayDialogState.Minimized)
                    hostDialog.MinimizeStateDialog();
                else if (state == OverlayDialogState.Maximized)
                    hostDialog.MaximizeStateDialog();

                SetActiveDialog(newMeta);

                ShowOverlay();

                InternalModalMode = ContainsModalDialog;
               
#if DEBUG
                WriteDebugInfo("OverlayManager.Show() - Dialog activated");
#endif
            }
            else
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Deaktiviert den Overlay und schließt alle aktiven Dialoge
        /// </summary>
        /// <returns>true wenn alle dialoge geschlossen wurden</returns>
        public bool Hide()
        {
            return Hide(false);
        }
        /// <summary>
        /// Deaktiviert den Overlay und schließt alle aktiven Dialoge
        /// </summary>
        /// <param name="forceClose">True wenn das Schließen der Dialog erzwungen werden soll egal 
        /// ob noch Daten zur Speicherung vorhanden sind</param>
        /// <returns>true wenn die Dialoge geschlossen wurden</returns>
        public bool Hide(bool forceClose)
        {
            bool bCanHideOverlay = true;

            // schliesst alle offenen dialoge
            for (int i = _DialogWindowList.Count; i > 0; i = _DialogWindowList.Count)
            {
                //OverlayDialog curDialog = _DialogWindowList[i - 1];
                OverlayWindowMeta curMeta = _DialogWindowList[i - 1];
                OverlayDialog curDialog = curMeta.Dialog;

                DeRegisterDialogEventHandlers(curMeta);
                DeRegisterKeyEvents(curMeta);
                // versuche den aktuellen Dialog zu schliessen
                if (curDialog.CloseDialog(forceClose) || forceClose)
                {
                    // inactive canvvas zeigen
                    curMeta.IsActive = false;
                    // wenn erfolgreich oder erzwungen dann entlade das control
                    //_DialogWindowList.Remove(curDialog);
                    _DialogWindowList.Remove(curMeta);
                    // abmelden vom closed event
                    curDialog.Closed -= overlayDialog_Closed;

                    // den dialog aus der Children collection des Grids entfernen
                    overlay_manager_canvas.Children.Remove(curDialog);
                    overlay_manager_canvas.Children.Remove(curMeta.DialogWindowScreenshot);
                    overlay_window_manager.RemoveMinimizedWindow(curMeta);

                    curMeta.DialogWindowScreenshot.FreeImageResource();
                    curMeta.DialogThumbScreenshot.FreeImageResource();

                    curMeta.DialogMoved = false;

                    // fire closed event
                    curDialog.DialogContent.InternalAfterClose();

                    curMeta.OverlayAnimationFinished -= OverlayAnimation_Completed;
                }
                else
                {
                    // wenn das schliessen nicht erfolgreich war
                    // den aktuellen dialog welcher nicht geschlossen werden kann anzeigen
                    curMeta.IsActive = true;
                    bCanHideOverlay = false;
                    RegisterDialogEventHandlers(curMeta);
                    SetActiveDialog(curMeta);
                    //if(!curDialog.DialogContent.SetInputFocus())
                        curDialog.SetInputFocus();
                    break;
                }
            }

            if (bCanHideOverlay)
                HideOverlay();

            return true;
        }

        /// <summary>
        /// Schließt nur den aktuell aktiven Dialog falls vorhanden
        /// </summary>
        /// <param name="forceClose">True wenn das Schließen der Dialog erzwungen werden soll egal 
        /// ob noch Daten zur Speicherung vorhanden sind</param>
        /// <returns>True wenn der Dialog geschlossen wurde</returns>
        public bool HideActiveDialogOnly(bool forceClose)
        {
            if(ContainsModalDialog)
            {
                OverlayWindowMeta modalActive = LastActiveDialogForLayer(OverlayWindowLayer.Modal);

                if(modalActive != null)
                {
                    OverlayDialog curDialog = modalActive.Dialog;

                    // versuche den aktuellen Dialog zu schliessen
                    if (curDialog.CloseDialog(forceClose) || forceClose)
                    {
                        // will be hidden after close event
                        CloseDialog(modalActive.Dialog);
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            else
            {
                OverlayWindowMeta activeActive = LastActiveDialogForLayer(OverlayWindowLayer.Active);

                if (activeActive != null)
                {
                    OverlayDialog curDialog = activeActive.Dialog;

                    // versuche den aktuellen Dialog zu schliessen
                    if (curDialog.CloseDialog(forceClose) || forceClose)
                    {
                        // will be hidden after close event
                        CloseDialog(activeActive.Dialog);
                    }
                    else
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        public bool HideContent(OverlayContent content, bool forceClose)
        {
            if (content == null)
                return false;

            if (_DialogWindowList.Count > 0)
            {
                OverlayDialog curDialog = content.ParentDialog;

                if (curDialog != null)
                {
                    // versuche den aktuellen Dialog zu schliessen
                    if (curDialog.CloseDialog(forceClose) || forceClose)
                    {
                        // will be hidden after close event
                        CloseDialog(curDialog);
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Druckt den Inhalt des Overlay Dialoges
        /// </summary>
        public void Print()
        {
#if DEBUG
            WriteDebugInfo("*** PRINT() called");
#endif
        }

        /// <summary>
        /// Explizite interface Implementierung welche die Classeninterne Version der Eigenschaft zurückgibt
        /// </summary>
        public DependencyObject LastActiveDialog
        {
            get
            {
                OverlayWindowMeta meta = LastActiveDialogForLayer(OverlayWindowLayer.Active);

                return meta==null ? null : meta.Dialog;
            }
        }

        public DependencyObject LastActiveModalDialog
        {
            get
            {
                OverlayWindowMeta meta = LastActiveDialogForLayer(OverlayWindowLayer.Modal);

                return meta == null ? null : meta.Dialog;
            }
        }

        /// <summary>
        /// Checks if a content control is currently in the display chain of the overlaymanager
        /// </summary>
        /// <param name="content">Content to check</param>
        /// <returns>Returns true if the content control is in the display chain of the overlay manager</returns>
        public bool IsInDisplayChain(ContentControl content)
        {
            OverlayWindowMeta metaInfo = GetMetaForContent(content);

            return metaInfo != null;

            //foreach(OverlayDialog curDialog in _DialogWindowList)
            //{
            //    if (curDialog.DialogContent == content)
            //        return true;
            //}

            //return false;
        }

        internal void SetActiveDialog(OverlayWindowMeta dlgMeta)
        {
            SetActiveDialog(dlgMeta, false);
        }
        internal void SetActiveDialog(OverlayWindowMeta dlgMeta, bool normalize_if_minimized)
        {
            List<OverlayWindowMeta> activeDialogs = null;
            
            if(dlgMeta == null)
            {
                activeDialogs = GetActiveWindowsForLayer(OverlayWindowLayer.Active);    
            }
            else if(dlgMeta.Layer == OverlayWindowLayer.Presentation)
            {
                activeDialogs = GetActiveWindowsForLayer(dlgMeta.FallbackLayer == OverlayWindowLayer.Modal ? OverlayWindowLayer.Modal : OverlayWindowLayer.Active);    
            }
            else
            {
                activeDialogs = GetActiveWindowsForLayer(dlgMeta.Layer == OverlayWindowLayer.Modal ? OverlayWindowLayer.Modal : OverlayWindowLayer.Active);    
            }

            foreach (OverlayWindowMeta meta in _DialogWindowList)
                DeRegisterKeyEvents(meta);


            if (activeDialogs != null)
            {
                foreach (OverlayWindowMeta deactivateMeta in activeDialogs)
                    if (deactivateMeta != dlgMeta)
                    {
                        deactivateMeta.IsActive = false;
                        LayerManager.SetLastActiveDialogPerLayer(deactivateMeta.Layer, deactivateMeta);
                    }
            }

            if (dlgMeta != null && dlgMeta.Dialog != null)
            {
                if (dlgMeta.Dialog.WindowState == OverlayDialogState.Minimized)
                {
                    if (normalize_if_minimized)
                    {
                        System.Diagnostics.Debug.WriteLine("**** NORMALIZE since MINIMIZED");
                        dlgMeta.SetActiveAfterStateChange = true;
                        dlgMeta.Dialog.NormalStateDialog();
                    }
                    else
                    {
                        // wenn der letzte minimized ist, dann in der winowliste den letzten inaktiven suchen 
                        OverlayWindowMeta newActiveMeta = null;
                        bool onlyMinimizedWindows = true;

                        foreach (OverlayWindowMeta searchMeta in DialogWindowList.OrderByDescending(x => x.DialogLayerZindex[OverlayWindowLayer.Normal]).ToList())
                        {
                            if (searchMeta.Dialog.WindowState != OverlayDialogState.Minimized)
                                onlyMinimizedWindows = false;

                            if (!searchMeta.IsActive && searchMeta.Dialog.WindowState != OverlayDialogState.Minimized)
                            {
                                newActiveMeta = searchMeta;
                                break;
                            }
                        }

                        if (newActiveMeta == null && onlyMinimizedWindows)
                        {
                            // wenn es nur minimierte fenster gibt dann 
                            // den last active dialog wieder noramalisieren

                            //dlgMeta.SetActiveAfterStateChange = true;
                            //dlgMeta.Dialog.NormalStateDialog();

                        }
                        else if (newActiveMeta != null)
                        {
                            if(!newActiveMeta.IsAnimationRunning)
                                newActiveMeta.IsActive = true;
                            else
                            {
#if DEBUG
                                Debug.WriteLine("*** Ignore active switch sinde animatin is running !!");
#endif
                            }

                            LayerManager.RecalculateLayers(newActiveMeta);
                        }

                        if (newActiveMeta != null)
                            RegisterKeyEvents(newActiveMeta);
                    }
                }
                else
                {
                    if(!dlgMeta.IsAnimationRunning)
                        dlgMeta.IsActive = true;
                    else
                    {
#if DEBUG
                        Debug.WriteLine("*** Ignore active switch sinde animatin is running 22!!");
#endif
                    }

                    LayerManager.RecalculateLayers(dlgMeta);

                    if (dlgMeta != null)
                        RegisterKeyEvents(dlgMeta);
                }
            }

            ShowOverlay();
        }

        private bool StartMinimizingAnimations(OverlayWindowMeta dlgMeta, OverlayDialogState startState)
        {            
#if DEBUG
            System.Diagnostics.Debug.WriteLine("*** Start minimizing animation");
#endif
            // Dialog wird minimiert

            // ACHTUNG: Wenn Animationen gestartet werden muss diese Methode TRUE
            // zurückgeben. Nach abschluss der Animationen MUSS für den Dialog  dlgMeta.Dialog
            // die explizite Methode ExplizitSetState(OverlayDialogState.Minimized) aufgerufen werden.
            // erst diese Methode feuert dann das WindowStateChanged() event und setzt den state am Dialog

            // Der Dialog ist zum jetzigen Zeitpunkt im Canvas bereits auf Hidden gesetzt
            // dlgMeta.DialogWindowScreenshot ist das ImageControl welches bereits den Dialog als PNG im Canvas anzeigt
            // dieses Control muss animiert werden

            // dlgMeta.DialogThumbScreenshot ist das 100x100 große ImageControl für das Thumbnail

            Storyboard myStoryboard = new Storyboard();
            OverlayWindowMeta.OverlayAnimationProperties properties = new OverlayWindowMeta.OverlayAnimationProperties();
            if (dlgMeta.Dialog.MinimizingAnimation != null)
            {
                myStoryboard = dlgMeta.Dialog.MinimizingAnimation;
                properties = dlgMeta.Dialog.MinimizingAnimationFinalPos;

            }
            else if(WindowTaskBar != null)
            {
                double scale = 1;
                double cur_width = dlgMeta.Dialog.ActualViewboxWidth;//dlgMeta.Dialog.ActualWidth;
                double cur_height = dlgMeta.Dialog.ActualViewboxHeight;//dlgMeta.Dialog.ActualHeight;
                dlgMeta.WidthBeforeAnimtion = cur_width;
                dlgMeta.HeightBeforeAnimation = cur_height;

                Canvas.SetZIndex(dlgMeta.DialogWindowScreenshot, int.MaxValue);
                       
                double est_height;
                double est_width;

                if (cur_height >= WindowTaskBar.ActualHeight)
                {
                    scale = WindowTaskBar.ActualHeight / cur_height;
                    est_height = WindowTaskBar.ActualHeight;
                    est_width = cur_width * scale;
                }
                else
                {
                    est_height = cur_height;
                    est_width = cur_width;
                }

                DoubleAnimation d = new DoubleAnimation();
                d.DecelerationRatio = 1;
                d.From = 0;
                d.To = est_height;
                d.Duration = new Duration(TimeSpan.FromSeconds(1));
                Storyboard.SetTargetProperty(d, new PropertyPath(FrameworkElement.HeightProperty));

                DoubleAnimation d2 = new DoubleAnimation();
                d2.DecelerationRatio = 1;
                d2.From = 0;
                d2.To = est_width;
                d2.Duration = new Duration(TimeSpan.FromSeconds(1));
                Storyboard.SetTargetProperty(d2, new PropertyPath(FrameworkElement.WidthProperty));

                Storyboard sb = new Storyboard();
                sb.Children.Add(d2);
                sb.Children.Add(d);

                Point root_point = new Point();
                bool with_fade_out = false;
                bool minimize_to_dialog_icon = false;
                //SubMenuPanel p = this.WindowTaskBar.GetSubMenuPanelByTitle("Dialoge");
                ISubMenuPanelBase p = this.WindowTaskBar.GetSubMenuPanelByTitle(dlgMeta.Dialog.DialogContent.MinimizedWindowPanelName);
                if(p.ToggleButtonReference.IsChecked == true)
                {
                    Point ref_point = UIHelper.GetElementPosition(p.AnimatingTilePanel, this);

                    ref_point.X -= Canvas.GetLeft(p.AnimatingTilePanel);
                    root_point = new Point(p.Items.Count * p.BigItemSize.Width , ref_point.Y);
                    if(WindowTaskBar.BaseFrame.ActualWidth < root_point.X)
                        minimize_to_dialog_icon = true;
                }
                else
                    minimize_to_dialog_icon = true;
                
                if(minimize_to_dialog_icon)
                {
                    //FrameworkElement elem = this.WindowTaskBar.GetSubMenuContainerByTitle("Dialoge");
                    FrameworkElement elem = this.WindowTaskBar.GetSubMenuContainerByTitle(dlgMeta.Dialog.DialogContent.MinimizedWindowPanelName);
                    root_point = WindowTaskBar.GetElementPosition(elem, this);
                    with_fade_out = true;
                }

                double from_left = Canvas.GetLeft(dlgMeta.Dialog);
                from_left = Double.IsNaN(from_left) ? 0 : from_left;

                double from_top = Canvas.GetTop(dlgMeta.Dialog);
                from_top = Double.IsNaN(from_top) ? 0 : from_top;

                dlgMeta.DialogWindowScreenshot.Opacity = 1;
                myStoryboard = overlay_window_manager.GetDefaultMinimizeAnimation(dlgMeta, new Point(from_left, from_top), new Point(root_point.X, root_point.Y), false, ref scale, true, with_fade_out);
                _running_minimizing_animations.Add(dlgMeta.Dialog);
                properties.FinalLeft = root_point.X;
                properties.FinalTop = root_point.Y;
                properties.FinalScalex = scale;
                properties.FinalScaley = scale;
            }
            else if(TaskbarPanel != null)
            {
                double scale = 1;
                double cur_width = dlgMeta.Dialog.ActualViewboxWidth;
                double cur_height = dlgMeta.Dialog.ActualViewboxHeight;
                dlgMeta.WidthBeforeAnimtion = cur_width;
                dlgMeta.HeightBeforeAnimation = cur_height;

                Image newimage = new Image();
                
                double est_height;
                double est_width;

                if(cur_height >= TaskbarPanel.ITEM_HEIGHT)
                {
                    scale = TaskbarPanel.ITEM_HEIGHT/cur_height;
                    est_height = TaskbarPanel.ITEM_HEIGHT;
                    est_width = cur_width * scale;
                }
                else
                {
                    est_height = cur_height;
                    est_width = cur_width;
                }

                DoubleAnimation d = new DoubleAnimation();
                d.DecelerationRatio = 1;
                d.From = 0;
                d.To = est_height;
                d.Duration = new Duration(TimeSpan.FromSeconds(1));
                Storyboard.SetTargetProperty(d, new PropertyPath(FrameworkElement.HeightProperty));

                DoubleAnimation d2 = new DoubleAnimation();
                d2.DecelerationRatio = 1;
                d2.From = 0;
                d2.To = est_width;
                d2.Duration = new Duration(TimeSpan.FromSeconds(1));
                Storyboard.SetTargetProperty(d2, new PropertyPath(FrameworkElement.WidthProperty));

                Storyboard sb = new Storyboard();
                sb.Children.Add(d2);
                sb.Children.Add(d);

                StackPanel sp = TaskbarPanel.AddPlaceholderItem(newimage, 5, 5);
                
                dlgMeta.TaskbarPlaceholder = sp;

                Point root_point = TaskbarPanel.GetElementPosition(sp);
                
                Point rel_point = TaskbarPanel.GetElementPosition(sp, TaskbarPanel);
                Point r_p = TaskbarPanel.GetElementPosition(TaskbarPanel);
                double new_taskbar_scale = TaskbarPanel.GetNewScale(est_width);
                scale *= new_taskbar_scale;

                if (new_taskbar_scale == 1)
                {
                    root_point.X -= est_width / 2;
                    root_point.Y += rel_point.Y;
                }
                else
                {
                    //root_point.Y = rel_point.Y * new_taskbar_scale;
                    root_point.X = rel_point.X * new_taskbar_scale + r_p.X;
                }

                Debug.WriteLine("Punkt ohneScale" + root_point);
                
                newimage.BeginStoryboard(sb);

                double from_left = Canvas.GetLeft(dlgMeta.Dialog);
                from_left = Double.IsNaN(from_left) ? 0 : from_left;

                double from_top = Canvas.GetTop(dlgMeta.Dialog);
                from_top = Double.IsNaN(from_top) ? 0 : from_top;

                
                myStoryboard = overlay_window_manager.GetDefaultMinimizeAnimation(dlgMeta, new Point(from_left, from_top), new Point(root_point.X, root_point.Y), ref scale);
                _running_minimizing_animations.Add(dlgMeta.Dialog);
                properties.FinalLeft = root_point.X;
                properties.FinalTop = root_point.Y;
                properties.FinalScalex = scale;
                properties.FinalScaley = scale;
               
            }
            else
            {
                double cur_width = dlgMeta.Dialog.ActualViewboxWidth;//dlgMeta.Dialog.ActualWidth;
                double cur_height = dlgMeta.Dialog.ActualViewboxHeight;//dlgMeta.Dialog.ActualHeight;
                dlgMeta.WidthBeforeAnimtion = cur_width;
                dlgMeta.HeightBeforeAnimation = cur_height;

                Vector offset = VisualTreeHelper.GetOffset(overlay_window_manager.content_control);

                double from_left = Canvas.GetLeft(dlgMeta.Dialog);
                from_left = Double.IsNaN(from_left) ? 0 : from_left;

                double from_top = Canvas.GetTop(dlgMeta.Dialog);
                from_top = Double.IsNaN(from_top) ? 0 : from_top;

                Point next_elem_offset = new Point(0,0);
                if (overlay_window_manager.content_control.IsAddingAnimationActive == false)
                    next_elem_offset = overlay_window_manager.content_control.GetNextElementOffset(_running_minimizing_animations.Count);

                double to_left = overlay_manager_overall_canvas.ActualWidth -  offset.X - next_elem_offset.X;
                double to_top = overlay_manager_overall_canvas.ActualHeight -  offset.Y - next_elem_offset.Y;

                double scale = 1;
                myStoryboard = overlay_window_manager.GetDefaultMinimizeAnimation(dlgMeta, new Point(from_left, from_top), new Point(to_left, to_top), ref scale);
                _running_minimizing_animations.Add(dlgMeta.Dialog);
                properties.FinalLeft = to_left;
                properties.FinalTop = to_top;
                properties.FinalScalex = scale;
                properties.FinalScaley = scale;
            }
            
            //myStoryboard.Completed += new EventHandler(myStoryboard_Completed);
            //myStoryboard.Begin(dlgMeta.DialogWindowScreenshot);
            if(startState == OverlayDialogState.Normal)
                dlgMeta.StartAnimation(myStoryboard, OverlayAnimationType.NormalToMinimize, properties);
            else if(startState == OverlayDialogState.Maximized)
                dlgMeta.StartAnimation(myStoryboard, OverlayAnimationType.MaximizeToMinimize, properties);
            else if(startState == OverlayDialogState.Modal)
                dlgMeta.StartAnimation(myStoryboard, OverlayAnimationType.ModalToMinimize, properties);

            return true;
        }

        private bool StartNormalAnimations(OverlayWindowMeta dlgMeta, OverlayDialogState startState)
        {
#if DEBUG
            System.Diagnostics.Debug.WriteLine("*** Start normal animation");
#endif
            // Dialog wird normal angezeigt
            // kann von minimized->normal oder maximized->normal sein

            // ACHTUNG: Wenn Animationen gestartet werden muss diese Methode TRUE
            // zurückgeben. Nach abschluss der Animationen MUSS für den Dialog  dlgMeta.Dialog
            // die explizite Methode ExplizitSetState(OverlayDialogState.Normal) aufgerufen werden.
            // erst diese Methode feuert dann das WindowStateChanged() event und setzt den state am Dialog

            //dlgMeta.Dialog.DialogContent.DialogImageDirty = true;
            //CreateDialogScreenshots(dlgMeta);
            Storyboard myStoryboard = new Storyboard();
            OverlayWindowMeta.OverlayAnimationProperties properties = new OverlayWindowMeta.OverlayAnimationProperties();
            if (dlgMeta.Dialog.NormalizingAnimation != null)
            {
                myStoryboard = dlgMeta.Dialog.NormalizingAnimation;
                properties = dlgMeta.Dialog.NormalizingAnimationFinalPos;
            }
            else if(startState == OverlayDialogState.Maximized)
            {
                double from_left = Canvas.GetLeft(dlgMeta.DialogWindowScreenshot);
                double from_top = Canvas.GetTop(dlgMeta.DialogWindowScreenshot);
                //if (Math.Round(dlgMeta.WidthBeforeAnimtion / dlgMeta.HeightBeforeAnimation, 4) != Math.Round(dlgMeta.DialogWindowScreenshot.ActualWidth / dlgMeta.DialogWindowScreenshot.ActualHeight, 4))
                {
                    dlgMeta.WidthBeforeAnimtion = dlgMeta.Dialog.ActualViewboxWidth*0.8;
                    dlgMeta.HeightBeforeAnimation = dlgMeta.Dialog.ActualViewboxHeight*0.8;
                }
                double to_left = (overlay_manager_overall_canvas.ActualWidth - dlgMeta.WidthBeforeAnimtion)/2;
                double to_top = (overlay_manager_overall_canvas.ActualHeight - dlgMeta.HeightBeforeAnimation)/2;
                double to_scalex = dlgMeta.WidthBeforeAnimtion/(dlgMeta.DialogWindowScreenshot.ActualWidth);
                double to_scaley = dlgMeta.HeightBeforeAnimation / (dlgMeta.DialogWindowScreenshot.ActualHeight);

                if (to_scalex != to_scaley)
                    to_scaley = to_scalex;

                ScaleTransform s = dlgMeta.DialogWindowScreenshot.RenderTransform as ScaleTransform;

                //if (s != null)
                //    to_scale *= 1/s.ScaleX;
                myStoryboard = overlay_window_manager.GetDefaultNormalizeAnimation(new Point(from_left, from_top), new Point(to_left, to_top), new Point(to_scalex, to_scaley), true);

                properties.FinalLeft = to_left;
                properties.FinalTop = to_top;
                properties.FinalScalex = to_scalex;
                properties.FinalScaley = to_scaley;
                
                //myStoryboard = overlay_window_manager.GetDefaultMaximizeAnimation(dlgMeta, new Point(to_left, to_top), new Point(dlgMeta.WidthBeforeAnimtion/cur_w, dlgMeta.HeightBeforeAnimation/cur_w));
            }
            else
            {
                Point offset = overlay_window_manager.content_control.GetElementOffset(dlgMeta);
                

                double to_left = Canvas.GetLeft(dlgMeta.Dialog);
                to_left = Double.IsNaN(to_left) ? 0 : to_left;
                double to_top = Canvas.GetTop(dlgMeta.Dialog);
                to_top = Double.IsNaN(to_top) ? 0 : to_top;

                if (dlgMeta.Dialog.SearchFreePosition && !dlgMeta.Dialog.FoundFreePosition &&  !_layouted_elements.Contains(dlgMeta.Dialog))
                {
                    _layouted_elements.Add(dlgMeta.Dialog);
                    Monitor.Enter(this);

                    GeneratePositionSnapshot(dlgMeta.Dialog);

                    Vector2f free_pos = SearchFreePosition((int)dlgMeta.Dialog.ActualViewboxWidth, (int)dlgMeta.Dialog.ActualViewboxHeight, dlgMeta.Dialog.SearchPositionAlignment);

                    if (free_pos.X != -1)
                    {
                        free_pos.X += (float)(dlgMeta.Dialog.SearchFreePositionMargin.Left + dlgMeta.Dialog.SearchFreePositionMargin.Right);
                        free_pos.Y += (float)(dlgMeta.Dialog.SearchFreePositionMargin.Top + dlgMeta.Dialog.SearchFreePositionMargin.Bottom);
                        SetDialogPosition(dlgMeta, free_pos);
                        to_left = free_pos.X;
                        to_top = free_pos.Y;
                    }

                    Monitor.Exit(this);


                    dlgMeta.Dialog.FoundFreePosition = true;
                }

                

                double from_left, from_top;

                if (WindowTaskBar != null && dlgMeta.ClickedWindowTaskBarElement != null)
                {
                    //SubMenuPanel elem = WindowTaskBar.GetSubMenuPanelByTitle("Dialoge");
                    ISubMenuPanelBase elem = WindowTaskBar.GetSubMenuPanelByTitle(dlgMeta.Dialog.DialogContent.MinimizedWindowPanelName);
                    
                    FrameworkElement displ_elem = elem.GetItemByElement(dlgMeta.ClickedWindowTaskBarElement);

                    if (displ_elem == null)
                    {
                        from_left = Canvas.GetLeft(dlgMeta.DialogWindowScreenshot);
                        from_top = Canvas.GetTop(dlgMeta.DialogWindowScreenshot);
                    }
                    else
                    {
                        Point root_point = WindowTaskBar.GetElementPosition(displ_elem, this);

                        ScaleTransform s = dlgMeta.DialogWindowScreenshot.RenderTransform as ScaleTransform;
                        if (dlgMeta.DialogWindowScreenshot.ActualWidth > dlgMeta.DialogWindowScreenshot.ActualHeight)
                        {
                            s.ScaleX = displ_elem.ActualWidth / dlgMeta.DialogWindowScreenshot.ActualWidth;
                            s.ScaleY = s.ScaleX;
                        }
                        else
                        {
                            s.ScaleY = displ_elem.ActualHeight / dlgMeta.DialogWindowScreenshot.ActualHeight;
                            s.ScaleX = s.ScaleY;
                        }                    
                        from_left = root_point.X;
                        from_top = root_point.Y;
                    }   

                    Canvas.SetZIndex(dlgMeta.DialogWindowScreenshot, int.MaxValue);
                }
                else if (TaskbarPanel == null)
                {
                    from_left = overlay_manager_overall_canvas.ActualWidth -
                                       overlay_window_manager.content_control.ItemWidth;
                    from_left = Double.IsNaN(from_left) ? 0 : from_left;
                    from_left -= offset.X;
                    from_top = overlay_manager_overall_canvas.ActualHeight -
                                      overlay_window_manager.content_control.ItemWidth;
                    from_top = Double.IsNaN(from_top) ? 0 : from_top;
                    from_top -= offset.Y;
                }
                else
                {
                    Point root_point = TaskbarPanel.GetElementPosition(dlgMeta.TaskbarPlaceholder);
                    from_left = root_point.X;
                    from_top = root_point.Y;
                }
                
                overlay_window_manager.RemoveMinimizedWindow(dlgMeta);

                #region AddTestImages
                //ScaleTransform s_tran = dlgMeta.DialogWindowScreenshot.RenderTransform as ScaleTransform;

                //Rectangle rect = new Rectangle();
                //rect.Fill = Brushes.Black;
                //rect.Width = dlgMeta.DialogWindowScreenshot.ActualWidth * s_tran.ScaleX;
                //rect.Height = dlgMeta.DialogWindowScreenshot.ActualHeight * s_tran.ScaleY;
                //this.overlay_manager_canvas.Children.Add(rect);
                //Canvas.SetLeft(rect, from_left);
                //Canvas.SetTop(rect, from_top);
                
                #endregion
                
                myStoryboard = overlay_window_manager.GetDefaultNormalizeAnimation(new Point(from_left, from_top), new Point(to_left, to_top), new Point(1,1));
                properties.FinalLeft = to_left;
                properties.FinalTop = to_top;
                properties.FinalScalex = 1;
                properties.FinalScaley = 1;
            }

            // make sure the window will be shown in the topmost canvas layer
            LayerManager.PlaceWindowInLayer(dlgMeta, OverlayWindowLayer.Presentation);

            if(dlgMeta.SetActiveAfterStateChange)
            {
                dlgMeta.DialogLayerZindex[OverlayWindowLayer.Active] = OverlayLayerManager.MaxActiveDialogLayer;
                dlgMeta.ScreenshotLayerZindex[OverlayWindowLayer.Active] = OverlayLayerManager.MaxActiveDialogLayer + 1;
                dlgMeta.FallbackLayer = OverlayWindowLayer.Active;
            }
            
            //myStoryboard.Completed += new EventHandler(normalize_anim_completed);
            //myStoryboard.Begin(dlgMeta.DialogWindowScreenshot);
            if(startState == OverlayDialogState.Minimized)
                dlgMeta.StartAnimation(myStoryboard, OverlayAnimationType.MinimizedToNormal, properties);
            else if(startState == OverlayDialogState.Maximized)
                dlgMeta.StartAnimation(myStoryboard, OverlayAnimationType.MaximizeToNormal, properties);

            return true;
        }

        private bool StartModalAnimations(OverlayWindowMeta dlgMeta, OverlayDialogState startState)
        {
#if DEBUG
            System.Diagnostics.Debug.WriteLine("*** Start modal animation");
#endif
            // Dialog wird normal angezeigt
            // kann von minimized->normal oder maximized->normal sein

            // ACHTUNG: Wenn Animationen gestartet werden muss diese Methode TRUE
            // zurückgeben. Nach abschluss der Animationen MUSS für den Dialog  dlgMeta.Dialog
            // die explizite Methode ExplizitSetState(OverlayDialogState.Normal) aufgerufen werden.
            // erst diese Methode feuert dann das WindowStateChanged() event und setzt den state am Dialog

            Storyboard myStoryboard = new Storyboard();
            OverlayWindowMeta.OverlayAnimationProperties properties = new OverlayWindowMeta.OverlayAnimationProperties();
            if (dlgMeta.Dialog.NormalizingAnimation != null)
            {
                myStoryboard = dlgMeta.Dialog.NormalizingAnimation;
                properties = dlgMeta.Dialog.NormalizingAnimationFinalPos;
            }
            else
            {
                overlay_window_manager.RemoveMinimizedWindow(dlgMeta);

                double to_left = Canvas.GetLeft(dlgMeta.Dialog);
                to_left = Double.IsNaN(to_left) ? 0 : to_left;
                double to_top = Canvas.GetTop(dlgMeta.Dialog);
                to_top = Double.IsNaN(to_top) ? 0 : to_top;

                double offset = overlay_window_manager.content_control.GetVerticalOffset(dlgMeta.DialogWindowScreenshot);

                double from_left = overlay_manager_overall_canvas.ActualWidth -
                                   overlay_window_manager.content_control.ItemWidth;
                from_left = Double.IsNaN(from_left) ? 0 : from_left;
                from_left -= offset;
                double from_top = overlay_manager_overall_canvas.ActualHeight -
                                  overlay_window_manager.content_control.ItemWidth;
                from_top = Double.IsNaN(from_top) ? 0 : from_top;

                myStoryboard = overlay_window_manager.GetDefaultNormalizeAnimation(new Point(from_left, from_top), new Point(to_left, to_top), new Point(1,1));
                properties.FinalLeft = to_left;
                properties.FinalTop = to_top;
                properties.FinalScalex = 1;
                properties.FinalScaley = 1;
            }

            // make sure the window will be shown in the topmost canvas layer
            LayerManager.PlaceWindowInLayer(dlgMeta, OverlayWindowLayer.Presentation);


            if (dlgMeta.SetActiveAfterStateChange)
            {
               dlgMeta.DialogLayerZindex[OverlayWindowLayer.Modal] = OverlayLayerManager.MaxModalDialogLayer;
               dlgMeta.ScreenshotLayerZindex[OverlayWindowLayer.Modal] = OverlayLayerManager.MaxModalDialogLayer + 1;
               dlgMeta.FallbackLayer = OverlayWindowLayer.Modal;
            }

            //myStoryboard.Completed += new EventHandler(normalize_anim_completed);
            //myStoryboard.Begin(dlgMeta.DialogWindowScreenshot);
            if (startState == OverlayDialogState.Minimized)
                dlgMeta.StartAnimation(myStoryboard, OverlayAnimationType.MinimizedToModal, properties);
            else if (startState == OverlayDialogState.Maximized)
                dlgMeta.StartAnimation(myStoryboard, OverlayAnimationType.MaximizedToModal, properties);

            return true;
        }
        
        private bool StartMaximizeAnimations(OverlayWindowMeta dlgMeta, OverlayDialogState startState)
        {
#if DEBUG
            System.Diagnostics.Debug.WriteLine("*** Start maximize animation");
#endif
            // Dialog wird normal angezeigt
            // kann von minimized->normal oder maximized->normal sein

            // ACHTUNG: Wenn Animationen gestartet werden muss diese Methode TRUE
            // zurückgeben. Nach abschluss der Animationen MUSS für den Dialog  dlgMeta.Dialog
            // die explizite Methode ExplizitSetState(OverlayDialogState.Normal) aufgerufen werden.
            // erst diese Methode feuert dann das WindowStateChanged() event und setzt den state am Dialog

            // make sure the window will be shown in the topmost canvas layer
            //LayerManager.PlaceWindowInLayer(dlgMeta, OverlayWindowLayer.Presentation);

            //dlgMeta.Dialog.DialogContent.DialogImageDirty = true;
            //CreateDialogScreenshots(dlgMeta);
            Storyboard myStoryboard = new Storyboard();
            
            if (double.IsNaN(dlgMeta.Dialog.ActualViewboxWidth) || double.IsNaN(dlgMeta.Dialog.ActualViewboxHeight))
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("*** Maximize Animation cannot be startet: ActualWidth = NaN, AcualHeight = NaN");
#endif
                return false;
            }

            Point to_topleft = new Point(0, 0);
            double to_scale_x = 1;
            double to_scale_y = 1;
            double new_width = 0;
            double new_height = 0;
            //double available_width = this.ActualVisibleWidth;
            //double available_height = this.ActualVisibleHeight;
            double cur_width = dlgMeta.Dialog.ActualWidthErrorHelpVisibility * dlgMeta.Dialog.ActualScaleX;
            double cur_height = dlgMeta.Dialog.ActualViewboxHeight;

            //if (dlgMeta.Dialog.MaximizeMode == OverlayDialogMaximizeMode.Normal)
            //{               
            //    //double available_width = overlay_manager_overall_canvas.ActualWidth;
            //    //double available_height = overlay_manager_overall_canvas.ActualHeight;


            //    double to_scale = 1;

            //    bool iswidth;
            //    double est_width = CalculateOptimalSize(dlgMeta, out iswidth);

            //    double est_to_scale_width = available_width / cur_width;
            //    double est_to_scale_height = available_height / cur_height;

            //    if (Math.Round(cur_width * est_to_scale_width, 6) <= Math.Round(available_width, 6) && Math.Round(cur_height * est_to_scale_width, 6) <= Math.Round(available_height, 6))
            //        to_scale = est_to_scale_width;
            //    else if (Math.Round(cur_width * est_to_scale_height, 6) <= Math.Round(available_width, 6) && Math.Round(cur_height * est_to_scale_height, 6) <= Math.Round(available_height, 6))
            //        to_scale = est_to_scale_height;
            //    else if (cur_width > cur_height)
            //        to_scale = est_to_scale_width;
            //    else
            //        to_scale = est_to_scale_height;

            //    if (est_width != 0)
            //    {
            //        if (iswidth)
            //            to_scale = est_width / cur_width;
            //        else
            //            to_scale = est_width / cur_height;
            //    }

            //    double new_width = cur_width * to_scale;
            //    double new_height = cur_height * to_scale;

            //    // Dialog soll in die Mitte des Bildschirms fahren               
            //    if (new_width < available_width)
            //        to_topleft.X = (available_width - new_width) / 2;
            //    if (new_height < available_height)
            //        to_topleft.Y = (available_height - new_height) / 2;

            //    to_topleft.X += AcutalVisibleTopLeft.X;
            //    to_topleft.Y += AcutalVisibleTopLeft.Y;

            //    to_scale_x = to_scale;
            //    to_scale_y = to_scale;
            //}
            //else if (dlgMeta.Dialog.MaximizeMode == OverlayDialogMaximizeMode.ReserveErrorViewerSpace)
            //{
            //    double new_width = 0;
            //    if (dlgMeta.Dialog.HelpOrErrorProviderVisible)
            //        new_width = available_width;
            //    else
            //        new_width = available_width - dlgMeta.Dialog.HelpErrorProviderMaxWidth;

            //    to_scale_x = new_width / cur_width;
            //    to_scale_y = available_height / cur_height;

            //    to_topleft.Y += 30;
            //    to_topleft.X += 5;

               
                
            //} 
            
            CalculateMaximizedWindowProperties(dlgMeta, out to_topleft, out to_scale_x, out to_scale_y, out new_width, out new_height);
            
            myStoryboard = overlay_window_manager.GetDefaultMaximizeAnimation(dlgMeta, to_topleft, new Point(to_scale_x, to_scale_y));

            OverlayWindowMeta.OverlayAnimationProperties prop = new OverlayWindowMeta.OverlayAnimationProperties();
            prop.FinalLeft = to_topleft.X;
            prop.FinalTop = to_topleft.Y;
            prop.FinalScalex = to_scale_x;
            prop.FinalScaley = to_scale_y;

            dlgMeta.WidthBeforeAnimtion = cur_width;
            dlgMeta.HeightBeforeAnimation = cur_height;
            
            LayerManager.PlaceWindowInLayer(dlgMeta, OverlayWindowLayer.Presentation);
            if (startState == OverlayDialogState.Normal)
                dlgMeta.StartAnimation(myStoryboard, OverlayAnimationType.NormalToMaximize, prop);

            return true;
        }

        private bool StartOpacityAnimation(OverlayWindowMeta dlgMeta, bool startState)
        {
#if DEBUG
            System.Diagnostics.Debug.WriteLine("*** Start opacity active/deactive animation");
#endif
            OverlayWindowMeta.OverlayAnimationProperties properties = new OverlayWindowMeta.OverlayAnimationProperties();
            if(startState)
            {
                // go inactive
                Storyboard fadeout = overlay_window_manager.GetFadeoutOpacityAnmiation();
                properties.FinalOpacity = 0.3;

                if (dlgMeta.Dialog.DialogContent.GoInactiveBehaviuor == GoInactiveBehaviuor.OpacityAnimationDialog)
                {
                    // der dialog wird als control gefaded, nicht der screenshot
                    dlgMeta.StartAnimation(fadeout, dlgMeta.Dialog, OverlayAnimationType.FadOutOpacity, properties);
                }
                else
                {
                    dlgMeta.StartAnimation(fadeout, OverlayAnimationType.FadOutOpacity, properties);
                }
                return true;
            }
            else
            {
                Storyboard fadeout = overlay_window_manager.GetFadeinOpacityAnimation();
                properties.FinalOpacity = 1;
                if (dlgMeta.Dialog.DialogContent.GoInactiveBehaviuor == GoInactiveBehaviuor.OpacityAnimationDialog)
                {
                    // der dialog wird als control gefaded, nicht der screenshot
                    dlgMeta.StartAnimation(fadeout, dlgMeta.Dialog, OverlayAnimationType.FadeInOpacity, properties);
                }
                else
                {
                    dlgMeta.StartAnimation(fadeout, OverlayAnimationType.FadeInOpacity, properties);
                }
                
                return true;
            }

            return false;
        }

        private bool StartCustomActiveAnimation(OverlayWindowMeta dlgMeta, bool startState)
        {
#if DEBUG
            System.Diagnostics.Debug.WriteLine("*** Start custom active/deactive animation");
#endif

            if(startState)
            {
                if (dlgMeta.Dialog.DialogContent.DeactivateStoryboard != null)
                {
                    // go inactive
                    dlgMeta.StartAnimation(dlgMeta.Dialog.DialogContent.DeactivateStoryboard, OverlayAnimationType.CustomDeactivate, null);
                    return true;
                }
            }
            else
            {
                if (dlgMeta.Dialog.DialogContent.ActivateStoryboard != null)
                {
                    // go inactive
                    dlgMeta.StartAnimation(dlgMeta.Dialog.DialogContent.ActivateStoryboard, OverlayAnimationType.CustomActivate, null);
                    return true;
                }
            }

            return false;
        }
        
        private void ExplizitSetWindowState(OverlayWindowMeta dlgMeta, OverlayDialogState state)
        {
            if (dlgMeta != null && dlgMeta.Dialog != null)
                dlgMeta.Dialog.ExplizitSetState(state);
        }

        private void ExplizitSetActiveState(OverlayWindowMeta dlgMeta, bool state)
        {
            if (dlgMeta != null && dlgMeta.Dialog != null)
                dlgMeta.Dialog.ExplizitSetActiveState(state);
        }

        private bool DialogLoadDelayed(OverlayContent content)
        {
            KeyValuePair<ContentControl, OverlayDialogState> delayObj = _delayed_show.Where(x => x.Key == (ContentControl) content).SingleOrDefault();

            return !delayObj.Equals(default(KeyValuePair<ContentControl, OverlayDialogState>));
        }

        private void RemoveDelayedLoadedInfo(OverlayContent content)
        {
            KeyValuePair<ContentControl, OverlayDialogState> delayObj = _delayed_show.Where(x => x.Key == (ContentControl)content).SingleOrDefault();

            if(! delayObj.Equals(default(KeyValuePair<ContentControl, OverlayDialogState>)))
            {
                _delayed_show.Remove(delayObj);
            }
        }

        private void CreateDialogScreenshots(OverlayWindowMeta dlgMeta)
        {
            if (!dlgMeta.IsAnimationRunning && dlgMeta.Dialog != null)
            {
                if (dlgMeta.Dialog.DialogContent.ImageRenderBehaviour == ImageRenderBehaviour.Always ||
                    ((dlgMeta.Dialog.DialogContent.ImageRenderBehaviour == ImageRenderBehaviour.OnlyIfDirty
                      || dlgMeta.Dialog.DialogContent.ImageRenderBehaviour == ImageRenderBehaviour.Intelligent) &&
                    dlgMeta.Dialog.DialogContent.DialogImageDirty) ||
                    dlgMeta.DialogWindowScreenshot.ScreenshotImage == null ||
                    dlgMeta.DialogThumbScreenshot.ScreenshotImage == null)
                {
                    dlgMeta.ResetAnimatedProperties();

                    double dActualWidth = dlgMeta.Dialog.ActualWidth;
                    double dActualHeight = dlgMeta.Dialog.ActualHeight;

                    //dlgMeta.DialogWindowScreenshot.SetImage(dlgMeta.Dialog.RenderToBitmapImage(ref dActualWidth, ref dActualHeight));
                    dlgMeta.DialogWindowScreenshot.SetImage(dlgMeta.Dialog.RenderToImage(ref dActualWidth, ref dActualHeight));

                    double old_render_offset_width = dlgMeta.DialogWindowScreenshot.ImageRenderOffsetWidth;
                    dlgMeta.DialogWindowScreenshot.ImageRenderOffsetWidth = 0;

                    if (dlgMeta.Dialog.Visibility != System.Windows.Visibility.Hidden)
                    {
                        if(dlgMeta.Dialog.HelpOrErrorProviderVisible && dlgMeta.Dialog.HelpErrorProviderWidth < dlgMeta.Dialog.DialogContent.ImageRenderOffsetWidth) 
                            dlgMeta.DialogWindowScreenshot.ImageRenderOffsetWidth = dlgMeta.Dialog.DialogContent.ImageRenderOffsetWidth;

                        dlgMeta.DialogWindowScreenshot.ImageRenderOffsetHeight = dlgMeta.Dialog.DialogContent.ImageRenderOffsetHeight;
                        dlgMeta.DialogWindowScreenshot.RendersShadow = WithDialogShadow || WithBackgroundShadow;
                        dlgMeta.DialogWindowScreenshot.ShadowScale = dlgMeta.Dialog.ActualScaleX;
                        //dlgMeta.DialogWindowScreenshot.Width = (dActualWidth+dlgMeta.Dialog.DialogContent.ImageRenderOffsetWidth) * dlgMeta.Dialog.ActualScaleX; //.ActualViewboxWidth; 
                        if(dlgMeta.Dialog.HelpOrErrorProviderVisible)
                            dlgMeta.DialogWindowScreenshot.Width = (dActualWidth) * dlgMeta.Dialog.ActualScaleX; //.ActualViewboxWidth;    
                        else
                            dlgMeta.DialogWindowScreenshot.Width = (dActualWidth + dlgMeta.Dialog.DialogContent.ImageRenderOffsetWidth) * dlgMeta.Dialog.ActualScaleX; //.ActualViewboxWidth; 
                      
                        dlgMeta.DialogWindowScreenshot.Height = (dActualHeight+dlgMeta.Dialog.DialogContent.ImageRenderOffsetHeight) * dlgMeta.Dialog.ActualScaleY; //.ActualViewboxHeight;
                        ScaleTransform s_tran = dlgMeta.DialogWindowScreenshot.RenderTransform as ScaleTransform;
                        if (s_tran != null)
                        {
                            s_tran.BeginAnimation(ScaleTransform.ScaleXProperty, null);
                            s_tran.BeginAnimation(ScaleTransform.ScaleYProperty, null);
                            s_tran.ScaleX = 1;
                            s_tran.ScaleY = 1;
                        }
                    }
                    dlgMeta.DialogWindowScreenshot.ScreenshotImage.Stretch = Stretch.Fill;

                    //dlgMeta.DialogThumbScreenshot.SetImage(dlgMeta.Dialog.RenderToBitmapImage(ref dActualWidth, ref dActualHeight));
                    dlgMeta.DialogThumbScreenshot.SetImage(dlgMeta.Dialog.RenderToImage(ref dActualWidth, ref dActualHeight));
                    dlgMeta.DialogThumbScreenshot.ImageRenderOffsetWidth = dlgMeta.Dialog.DialogContent.ImageRenderOffsetWidth;
                    dlgMeta.DialogThumbScreenshot.ImageRenderOffsetHeight = dlgMeta.Dialog.DialogContent.ImageRenderOffsetHeight;
                    dlgMeta.DialogThumbScreenshot.ScreenshotImage.Stretch = Stretch.Uniform;

                    dlgMeta.Dialog.DialogContent.DialogImageDirty = false;
                    dlgMeta.DialogWindowScreenshot.ImageRenderOffsetWidth = old_render_offset_width;
                }
            }
        }

        #region Message Box
        public void ShowMessageBox(string title, string text)
        {
            ShowMessageBox(title, text, null);
        }

        public void ShowMessageBox(string title, string text, MessageBoxResultDelegate resultDelegate)
        {
            ShowMessageBox(title, text, MessageBoxButton.OK, MessageBoxImage.None, resultDelegate);
        }

        public void ShowMessageBox(string title, string text, MessageBoxButton buttons, MessageBoxImage image)
        {
            ShowMessageBox(title, text, buttons, image, null);
        }

        public void ShowMessageBox(string title, string text, MessageBoxButton buttons, MessageBoxImage image, MessageBoxResultDelegate resultDelegate)
        {
            OverlayMessageBox msgBox = new OverlayMessageBox();
            msgBox.DialogTitle = title;
            msgBox.MessageBoxText = text;
            msgBox.MessageBoxButtons = buttons;
            msgBox.MessageBoxImage = image;
            _message_box_delegates.Push(resultDelegate);
            msgBox.OverlayContentClosed += new EventHandler(MessageBox_OverlayContentClosed);

            Show(msgBox);
        }

        public void ShowMessageBox(string title, string text, MessageBoxButton buttons, MessageBoxImage image, MessageBoxResultDelegate resultDelegate, object tag)
        {
            OverlayMessageBox msgBox = new OverlayMessageBox();
            msgBox.DialogTitle = title;
            msgBox.MessageBoxText = text;
            msgBox.MessageBoxButtons = buttons;
            msgBox.MessageBoxImage = image;
            msgBox.Tag = tag;
            _message_box_delegates.Push(resultDelegate);
            msgBox.OverlayContentClosed += new EventHandler(MessageBox_OverlayContentClosed);

            Show(msgBox);
        }

        #endregion

        #endregion

        #region Event handler

        void OverlayManager_Loaded(object sender, RoutedEventArgs e)
        {
            _overlay_manager_loaded = true;

            if(_delayed_show.Count > 0)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("*** execute delayed SHOW");

#endif
                foreach(KeyValuePair<ContentControl, OverlayDialogState> dlgInfo in _delayed_show)
                {
                    if (!Show(dlgInfo.Key, dlgInfo.Value))
                        throw new ApplicationException("Delayed show error!");
                }
                //_delayed_show.Clear();
            }

            RegisterWindowTaskBarEventHandler();
        }

        void OverlayManager_Unloaded(object sender, RoutedEventArgs e)
        {
            DeregisterWindowTaskBarEventHandler();
        }



        /// <summary>
        /// Will be called from a dialog if its active state is about to change
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void dialog_ActiveStateChanging(object sender, ActiveStateChangeingArgs e)
        {
            OverlayDialog dlg = sender as OverlayDialog;
            e.CanChange = true;

            if (dlg != null)
            {
                OverlayWindowMeta dlgMeta = GetMetaForDialog(dlg);
                
                if (dlgMeta != null)
                {
                    //if (DialogLoadDelayed(dlgMeta.Dialog.DialogContent))
                    //{
                    //    e.DelayedChange = false;
                    //    return;
                    //}

                    if(e.NewState)
                    {
                        // from inactive to active
                        switch (dlgMeta.Dialog.DialogContent.GoInactiveBehaviuor)
                        {
                            case GoInactiveBehaviuor.OpacityAnimation:
                                {
                                    if (dlgMeta.Dialog.WindowState == OverlayDialogState.Minimized)
                                    {
                                        e.DelayedChange = false;
                                    }
                                    else
                                    {
                                        //CreateDialogScreenshots(dlgMeta);
                                        dlgMeta.DialogWindowScreenshot.Visibility = Visibility.Visible;
                                        dlgMeta.Dialog.Visibility = Visibility.Hidden;
                                        dlgMeta.AnimationTriggerSource = OverlayAnimationTriggerSource.ActiveStateChange;
                                        dlgMeta.Dialog.IsAnimatingActiveState = true;
                                        e.DelayedChange = StartOpacityAnimation(dlgMeta, e.CurState);
                                    }
                                }
                                ; break;
                            case GoInactiveBehaviuor.OpacityAnimationDialog:
                                {
                                    if (dlgMeta.Dialog.WindowState == OverlayDialogState.Minimized)
                                    {
                                        e.DelayedChange = false;
                                    }
                                    else
                                    {
                                        //CreateDialogScreenshots(dlgMeta);
                                        dlgMeta.DialogWindowScreenshot.Visibility = Visibility.Hidden;
                                        dlgMeta.Dialog.Visibility = Visibility.Visible;
                                        dlgMeta.AnimationTriggerSource = OverlayAnimationTriggerSource.ActiveStateChange;
                                        dlgMeta.Dialog.IsAnimatingActiveState = true;
                                        e.DelayedChange = StartOpacityAnimation(dlgMeta, e.CurState);
                                    }
                                }
                                ; break;
                            case GoInactiveBehaviuor.CustomAnimation:
                                {
                                    if (dlgMeta.Dialog.WindowState == OverlayDialogState.Minimized)
                                    {
                                        e.DelayedChange = false;
                                    }
                                    else
                                    {
                                        if (dlgMeta.Dialog.DialogContent.DeactivateStoryboard != null)
                                        {
                                            //CreateDialogScreenshots(dlgMeta);

                                            dlgMeta.DialogWindowScreenshot.Visibility = Visibility.Visible;
                                            dlgMeta.Dialog.Visibility = Visibility.Hidden;
                                            dlgMeta.AnimationTriggerSource = OverlayAnimationTriggerSource.ActiveStateChange;
                                            dlgMeta.Dialog.IsAnimatingActiveState = true;
                                            e.DelayedChange = StartCustomActiveAnimation(dlgMeta, e.CurState);
                                        }
                                        else
                                        {
                                            e.DelayedChange = false;
                                        }
                                    }
                                }
                                ; break;
                            case GoInactiveBehaviuor.Minimize:
                                {
                                    e.DelayedChange = false; // StartNormalActivateAnimation(dlgMeta, e.CurState);
                                }
                                ; break;
                            default:
                                e.DelayedChange = false;
                                break;
                        }
                    }
                    else
                    {
                        
                        // from active to inactive
                        switch(dlgMeta.Dialog.DialogContent.GoInactiveBehaviuor)
                        {
                            case GoInactiveBehaviuor.OpacityAnimation:
                                {
                                    if (dlgMeta.Dialog.WindowState == OverlayDialogState.Minimized)
                                    {
                                        e.DelayedChange = false;
                                    }
                                    else
                                    {
                                        CreateDialogScreenshots(dlgMeta);

                                        dlgMeta.DialogWindowScreenshot.Visibility = Visibility.Visible;
                                        dlgMeta.Dialog.Visibility = Visibility.Hidden;
                                        dlgMeta.AnimationTriggerSource = OverlayAnimationTriggerSource.ActiveStateChange;
                                        dlgMeta.Dialog.IsAnimatingActiveState = true;
                                        e.DelayedChange = StartOpacityAnimation(dlgMeta, e.CurState);
                                    }
                                }
                                ; break;
                            case GoInactiveBehaviuor.OpacityAnimationDialog:
                                {
                                    if (dlgMeta.Dialog.WindowState == OverlayDialogState.Minimized)
                                    {
                                        e.DelayedChange = false;
                                    }
                                    else
                                    {
                                        CreateDialogScreenshots(dlgMeta);

                                        dlgMeta.DialogWindowScreenshot.Visibility = Visibility.Hidden;
                                        dlgMeta.Dialog.Visibility = Visibility.Visible;
                                        dlgMeta.AnimationTriggerSource = OverlayAnimationTriggerSource.ActiveStateChange;
                                        dlgMeta.Dialog.IsAnimatingActiveState = true;
                                        e.DelayedChange = StartOpacityAnimation(dlgMeta, e.CurState);
                                    }
                                }
                                ; break;
                            case GoInactiveBehaviuor.CustomAnimation:
                                {
                                    if (dlgMeta.Dialog.WindowState == OverlayDialogState.Minimized)
                                    {
                                        e.DelayedChange = false;
                                    }
                                    else
                                    {
                                        if (dlgMeta.Dialog.DialogContent.DeactivateStoryboard != null)
                                        {
                                            CreateDialogScreenshots(dlgMeta);

                                            dlgMeta.DialogWindowScreenshot.Visibility = Visibility.Visible;
                                            dlgMeta.Dialog.Visibility = Visibility.Hidden;
                                            dlgMeta.AnimationTriggerSource = OverlayAnimationTriggerSource.ActiveStateChange;
                                            dlgMeta.Dialog.IsAnimatingActiveState = true;
                                            e.DelayedChange = StartCustomActiveAnimation(dlgMeta, e.CurState);
                                        }
                                        else
                                        {
                                            e.DelayedChange = false;
                                        }
                                    }
                                }
                                ; break;
                            case GoInactiveBehaviuor.Minimize:
                                {
                                    if (dlgMeta.Dialog.WindowState == OverlayDialogState.Minimized)
                                    {
                                        e.DelayedChange = false;
                                    }
                                    else
                                    {
                                        dlgMeta.AnimationTriggerSource = OverlayAnimationTriggerSource.ActiveStateChange;
                                        dlgMeta.Dialog.IsAnimatingActiveState = true;
                                        e.DelayedChange = StartMinimizingAnimations(dlgMeta, dlgMeta.Dialog.WindowState);
                                    }
                                }
                                ; break;
                            case GoInactiveBehaviuor.ImageOnly:
                                {
                                    if (dlgMeta.Dialog.WindowState == OverlayDialogState.Minimized)
                                    {
                                        e.DelayedChange = false;
                                    }
                                    else
                                    {
                                        //CreateDialogScreenshots(dlgMeta);
                                        dlgMeta.DialogWindowScreenshot.Visibility = Visibility.Visible;
                                        dlgMeta.Dialog.Visibility = Visibility.Hidden;
                                        e.DelayedChange = false;
                                    }
                                }
                                ; break;
                            case GoInactiveBehaviuor.DoNothing:
                                {
                                    if (dlgMeta.Dialog.WindowState == OverlayDialogState.Minimized)
                                    {
                                        e.DelayedChange = false;
                                    }
                                    else
                                    {
                                        //CreateDialogScreenshots(dlgMeta);
                                        dlgMeta.DialogWindowScreenshot.Visibility = Visibility.Hidden;
                                        dlgMeta.Dialog.Visibility = Visibility.Visible;
                                        e.DelayedChange = false;
                                    }
                                }
                                ; break;
                            default:
                                e.DelayedChange = false;
                                break;
                        }
                    }
                    
                }
            }
        }
        /// <summary>
        /// Will be called from a dialog if the active state has been changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void dialog_ActiveStateChanged(object sender, ActiveStateChangeArgs e)
        {
            OverlayDialog dlg = sender as OverlayDialog;

            if (dlg != null)
            {
                OverlayWindowMeta dlgMeta = GetMetaForDialog(dlg);

                if (dlgMeta != null)
                {
                    if(e.NewState)
                    {
                        // activated
#if DEBUG
                        WriteDebugInfo(string.Format("OverlayDialog Active - {0}", dlgMeta == null ? 0 : dlgMeta.GetHashCode()));
                        System.Diagnostics.Debug.WriteLine(string.Format("OverlayDialog Active - {0}", dlgMeta == null ? 0 : dlgMeta.GetHashCode()));
#endif
                        dlgMeta.IsActive = true;

                        if (!dlgMeta.IsAnimationRunning)
                        {
                            if (dlgMeta.Layer != OverlayWindowLayer.Modal)
                            {
                                if (dlgMeta.Layer != OverlayWindowLayer.Presentation)
                                    dlgMeta.FallbackLayer = dlgMeta.Layer;
                                else if (dlgMeta.Dialog.WindowState == OverlayDialogState.Modal)
                                    dlgMeta.FallbackLayer = OverlayWindowLayer.Modal;
                                else
                                    dlgMeta.FallbackLayer = OverlayWindowLayer.Normal;

                                dlgMeta.Layer = OverlayWindowLayer.Active;
                            }
                            LayerManager.RecalculateLayers(dlgMeta); 
                            
                            dlgMeta.Dialog.Visibility = Visibility.Visible;
                            dlgMeta.DialogWindowScreenshot.Visibility = Visibility.Hidden;

                            double l = Canvas.GetLeft(dlgMeta.DialogWindowScreenshot);
                            double t = Canvas.GetTop(dlgMeta.DialogWindowScreenshot);

                            dlgMeta.ResetAnimatedProperties();
                            // BUG im Canvas?
                            // Die folgenden Zeilen verschieben den Dialog kurz um 0.5 einheiten
                            // dies bewirkt, dass der dialog immer richtig gezeichnet wird
                            // Problem: obwohl canvas.left und canvas.top die richtigen werte hatten
                            // zeigte der canvas das dialog control auf position 0,0 an bis man es verschoben hat
                            // ohne das animierte properties aktiv waren
                            // durch die verschiebung unten wird sichergestellt, dass sich die attached properties ändern
                            SetDialogPosition(dlgMeta, new Vector2f((float)l, (float)(t + 0.5)));
                            SetDialogPosition(dlgMeta, new Vector2f((float)l, (float)(t)));
                        }

                        dlgMeta.Dialog.RestoreFocusedElement();
                    }
                    else
                    {
                        // deactivated
#if DEBUG
                        WriteDebugInfo(string.Format("OverlayDialog InActive - {0}", dlgMeta == null ? 0 : dlgMeta.GetHashCode()));
                        System.Diagnostics.Debug.WriteLine(string.Format("OverlayDialog InActive - {0}", dlgMeta == null ? 0 : dlgMeta.GetHashCode()));
#endif
                        dlgMeta.IsActive = false;
                        LayerManager.SetLastActiveDialogPerLayer(dlgMeta.Layer, dlgMeta);
                        if (dlgMeta.Layer != OverlayWindowLayer.Modal)
                        {
                            if (dlgMeta.Layer != OverlayWindowLayer.Presentation)
                                dlgMeta.FallbackLayer = dlgMeta.Layer;
                            else if (dlgMeta.Dialog.WindowState == OverlayDialogState.Modal)
                                dlgMeta.FallbackLayer = OverlayWindowLayer.Modal;
                            else
                                dlgMeta.FallbackLayer = OverlayWindowLayer.Normal;

                            dlgMeta.Layer = OverlayWindowLayer.Normal;
                        }

                        LayerManager.RecalculateLayers(dlgMeta);

                        //if (!DialogLoadDelayed(dlg.DialogContent))
                        {
                            if (dlgMeta.Dialog.WindowState != OverlayDialogState.Minimized && 
                                dlgMeta.Dialog.DialogContent.GoInactiveBehaviuor == GoInactiveBehaviuor.DoNothing)
                            {
                                if (!dlgMeta.IsAnimationRunning)
                                {
                                    CreateDialogScreenshots(dlgMeta);
                                    
                                    // hide dialog
                                    dlgMeta.Dialog.Visibility = Visibility.Visible;
                                    // show image
                                    dlgMeta.DialogWindowScreenshot.Visibility = Visibility.Hidden;
                                }
                            }
                            else if (dlgMeta.Dialog.WindowState != OverlayDialogState.Minimized &&
                                dlgMeta.Dialog.DialogContent.GoInactiveBehaviuor == GoInactiveBehaviuor.ImageOnly)
                            {
                                if (!dlgMeta.IsAnimationRunning)
                                {
                                    CreateDialogScreenshots(dlgMeta);
                                    // hide dialog
                                    dlgMeta.Dialog.Visibility = Visibility.Hidden;
                                    // show image
                                    dlgMeta.DialogWindowScreenshot.Visibility = Visibility.Visible;
                                }
                            }
                        }
                    }

                    ShowOverlay();
                }
            }
        }

        /// <summary>
        /// Will be called from a dialog if its state is about to change
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void dialog_WindowStateChanging(object sender, WindowStateChangeingArgs e)
        {
            OverlayDialog dlg = sender as OverlayDialog;
            e.CanChange = true;

            if(dlg != null)
            {
                if (WindowStateChanging != null)
                    WindowStateChanging(dlg, e);

                OverlayWindowMeta dlgMeta = GetMetaForDialog(dlg);

                if(dlgMeta != null)
                {
                    CreateDialogScreenshots(dlgMeta);

                    if(e.NewState == OverlayDialogState.Minimized)
                    {
                        dlgMeta.DialogWindowScreenshot.Visibility = Visibility.Visible;
                        dlgMeta.Dialog.Visibility = Visibility.Hidden;

                        // wenn eine minimierungsanimation gestartet wurde dann
                        dlgMeta.AnimationTriggerSource = OverlayAnimationTriggerSource.WindowStateChange;
                        e.DelayedChange = StartMinimizingAnimations(dlgMeta, e.CurState);
                    }
                    else if (e.NewState == OverlayDialogState.Normal)
                    {
                        // wenn eine normalisierungsanimation gestartet wurde dann
                        dlgMeta.DialogWindowScreenshot.Visibility = Visibility.Visible;
                        dlgMeta.Dialog.Visibility = Visibility.Hidden;

                        dlgMeta.AnimationTriggerSource = OverlayAnimationTriggerSource.WindowStateChange;
                        e.DelayedChange = StartNormalAnimations(dlgMeta, e.CurState);
                    }
                    else if (e.NewState == OverlayDialogState.Modal)
                    {
                        // wenn eine normalisierungsanimation gestartet wurde dann
                        dlgMeta.DialogWindowScreenshot.Visibility = Visibility.Visible;

                        dlgMeta.AnimationTriggerSource = OverlayAnimationTriggerSource.WindowStateChange;
                        e.DelayedChange = StartModalAnimations(dlgMeta, e.CurState);
                    }
                    else if(e.NewState == OverlayDialogState.Maximized)
                    {
                        // wenn eine maximierungsanimation gestartet wurde dann
                        dlgMeta.DialogWindowScreenshot.Visibility = Visibility.Visible;
                        dlgMeta.Dialog.Visibility = Visibility.Hidden;

                        dlgMeta.AnimationTriggerSource = OverlayAnimationTriggerSource.WindowStateChange;
                        e.DelayedChange = StartMaximizeAnimations(dlgMeta, e.CurState);
                    }
                }
            }
        }
        /// <summary>
        /// Will be called from a dialog if a state has been changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void dialog_WindowStateChanged(object sender, WindowStateChangeArgs e)
        {
            OverlayDialog dlg = sender as OverlayDialog;

            if (dlg != null)
            {
                if (WindowStateChanged != null)
                    WindowStateChanged(dlg, e);

                OverlayWindowMeta dlgMeta = GetMetaForDialog(dlg);

                if (dlgMeta != null)
                {
#if DEBUG
                    System.Diagnostics.Debug.WriteLine("*** Dialog window state changed to: " + e.NewState.ToString());
#endif
                    if(e.NewState == OverlayDialogState.Minimized)
                    {
                        if(dlg.MinimizingAnimation == null)
                        {
                            overlay_window_manager.AddMinizedWindow(dlgMeta);
                            dlgMeta.DialogWindowScreenshot.Visibility = Visibility.Hidden;

                            OverlayWindowMeta lastActive = LastActiveDialogForLayer(dlgMeta.Layer);
                            if (lastActive != dlgMeta && lastActive != null)
                                SetActiveDialog(lastActive);
                        }

                    }
                    else
                    {
                        overlay_window_manager.RemoveMinimizedWindow(dlgMeta);

                        if (dlgMeta.IsActive)
                        {
                            dlgMeta.DialogWindowScreenshot.Visibility = Visibility.Hidden;
                            dlgMeta.Dialog.Visibility = Visibility.Visible;
                            EqualizeDialogScreenshot(dlgMeta);
                        }
                        else if(!dlgMeta.IsActive && !dlgMeta.IsAnimationRunning)
                        {
                            EqualizeDialogScreenshot(dlgMeta);
                        }

                        if(dlgMeta.SetActiveAfterStateChange || e.NewState == OverlayDialogState.Modal)
                        {
                            dlgMeta.DialogWindowScreenshot.Visibility = Visibility.Hidden;
                            dlgMeta.Dialog.Visibility = Visibility.Visible;

                            SetActiveDialog(dlgMeta);
                            dlgMeta.SetActiveAfterStateChange = false;

                            if (e.NewState == OverlayDialogState.Modal)
                                ShowOverlay(); // overlay canvas auf die richtige z-ebene schieben

                            dlgMeta.Dialog.RestoreFocusedElement();
                        }

                        if(e.NewState == OverlayDialogState.Normal && e.OldState == OverlayDialogState.Minimized && dlgMeta.Dialog.NeedsSizeOptimization &&
                            dlgMeta.Dialog.DialogContent.ResizesWithWindowInMaximizedMode)
                        {
                            ShowDialogMaximized(dlgMeta);
                        }
                    }
                }
            }
        }

        private void EqualizeDialogScreenshot(OverlayWindowMeta dlgMeta)
        {
            dlgMeta.Dialog.DialogContent.DialogImageDirty = true;
            dlgMeta.ResetAnimatedProperties();
            //dlgMeta.Dialog.BeginAnimation(Canvas.LeftProperty, null);
            //dlgMeta.Dialog.BeginAnimation(Canvas.TopProperty, null);

            double l = Canvas.GetLeft(dlgMeta.DialogWindowScreenshot);
            double t = Canvas.GetTop(dlgMeta.DialogWindowScreenshot);
            Canvas.SetLeft(dlgMeta.Dialog, l);
            Canvas.SetTop(dlgMeta.Dialog, t);

            if (!dlgMeta.Dialog.DialogContent.AdjustDialogToScreenshot)
                return;

            ScaleTransform s = dlgMeta.DialogWindowScreenshot.RenderTransform as ScaleTransform;
            if (s != null)
            {
                if (dlgMeta.Dialog.DialogContent.ResizeMode == OverlayDialogResizeMode.ScaleResize)
                {

                    //if (EnableDialogShadow)
                    //{
                    //    dlgMeta.Dialog.ResizeToHeight((dlgMeta.DialogWindowScreenshot.ActualHeight - OVERLAY_SHADOW_IMAGE_OFFSET_Y) * s.ScaleY);
                    //}
                    //else
                    {
                        dlgMeta.Dialog.ResizeToHeight(dlgMeta.DialogWindowScreenshot.CalculatedHeight*s.ScaleY);
                        dlgMeta.Dialog.DialogContent.OnResizeFinished();
                    }
                }
                else
                {
                    //dlgMeta.Dialog.ResizeToHeight(dlgMeta.DialogWindowScreenshot.ActualHeight * s.ScaleY, dlgMeta.Dialog.ActualHeight);
                    //dlgMeta.Dialog.ResizeToWidth(dlgMeta.Dialog.ActualWidthErrorHelpVisibility * s.ScaleX, dlgMeta.Dialog.ActualWidthErrorHelpVisibility);

                    dlgMeta.Dialog.ResizeToHeight(dlgMeta.DialogWindowScreenshot.CalculatedHeight * s.ScaleY);
                    
                    //if(dlgMeta.Dialog.WindowState == OverlayDialogState.Maximized)
                    //dlgMeta.Dialog.ResizeToWidth((dlgMeta.Dialog.ActualWidthErrorHelpVisibility - dlgMeta.Dialog.DialogContent.ImageRenderOffsetWidth) * s.ScaleX);
                                        
                    //if(dlgMeta.Dialog.overlaydialog_content_control.Width.IsNaN())
                    if (!dlgMeta.Dialog.HelpOrErrorProviderVisible)
                    {
                        if (dlgMeta.Dialog.DialogContent.ImageRenderOffsetWidth > 0)
                        {
                            double render_offset_proc = dlgMeta.Dialog.DialogContent.ImageRenderOffsetWidth / dlgMeta.Dialog.ActualWidth;
                            dlgMeta.Dialog.ResizeToWidth(dlgMeta.DialogWindowScreenshot.ActualWidth * s.ScaleX - dlgMeta.DialogWindowScreenshot.ActualWidth * render_offset_proc * s.ScaleX);
                        }
                        else
                            dlgMeta.Dialog.ResizeToWidth(dlgMeta.DialogWindowScreenshot.ActualWidth * s.ScaleX);
                    }
                    else
                        dlgMeta.Dialog.ResizeToWidth(dlgMeta.DialogWindowScreenshot.ActualWidth * s.ScaleX - dlgMeta.Dialog.HelpErrorProviderMaxWidth * s.ScaleX);
                    //else
                        //dlgMeta.Dialog.ResizeToWidth((dlgMeta.Dialog.ActualVisibleDialogWidth) * s.ScaleX);
                        //dlgMeta.Dialog.ResizeToWidth((dlgMeta.Dialog.ActualWidthErrorHelpVisibility) * s.ScaleX);
                    
                    dlgMeta.Dialog.DialogContent.OnResizeFinished();
                }
            }
        }

        /// <summary>
        /// Messagebox closed event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void MessageBox_OverlayContentClosed(object sender, EventArgs e)
        {
            OverlayMessageBox msgBox = sender as OverlayMessageBox;

            if(msgBox != null)
            {
                MessageBoxResultDelegate resultDelegate = _message_box_delegates.Pop();
                msgBox.OverlayContentClosed -= MessageBox_OverlayContentClosed;

                if(resultDelegate != null)
                {
                    MessageBoxResultArgs args = new MessageBoxResultArgs(msgBox.Result);
                    args.Tag = msgBox.Tag;
                    resultDelegate(msgBox, args);
                }
            }
        }


        /// <summary>
        /// Mouse events filtern sollte der overlay canvas über dem windowmanager sichtbar sein
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void WindowManager_OverlayCanvas_MouseDown(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
        }

        #region canvas mouse events
        /// <summary>
        /// Unterbricht die Routed-Event chain am overlay für die Mouse-Buttons
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void overlay_manager_canvas_MouseDown(object sender, MouseButtonEventArgs e)
        {
            _stylus_origin = e.StylusDevice != null;
            //if (!_canvas_mouse_capute_mode && !ModalMode)
            //{
            //    e.Handled = true;
            //    MouseButtonEventArgs newArgs = new MouseButtonEventArgs(e.MouseDevice, e.Timestamp, e.ChangedButton, e.StylusDevice);
            //    newArgs.RoutedEvent = UIElement.MouseDownEvent;
            //    newArgs.Handled = false;
            //    RaiseEvent(newArgs);
            //    return;
            //}

            _canvas_mousedown = true;

#if DEBUG
            WriteDebugInfo("OverlayManager.Canvas.MouseDown()");
#endif
            e.Handled = true;

        }

        /// <summary>
        /// Unterbricht die Routed-Event chain am overlay für die Mouse-Buttons
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void overlay_manager_canvas_MouseUp(object sender, MouseButtonEventArgs e)
        {
            //if (!_canvas_mouse_capute_mode && !ModalMode)
            //{
            //    e.Handled = true;
            //    MouseButtonEventArgs newArgs = new MouseButtonEventArgs(e.MouseDevice, e.Timestamp, e.ChangedButton, e.StylusDevice);
            //    newArgs.RoutedEvent = UIElement.MouseUpEvent;
            //    RaiseEvent(newArgs);
            //    return;
            //}

            List<OverlayWindowMeta> drag_active_windows = GetDragActiveWindows(e.MouseDevice);
            _stylus_origin = e.StylusDevice != null;

#if DEBUG
            WriteDebugInfo("OverlayManager.Canvas.MouseUp()");
#endif

            if (drag_active_windows.Count > 0)
            {
                foreach (OverlayWindowMeta dragmeta in drag_active_windows)
                {
                    WriteDebugInfo("OverlayManager.Canvas.MouseMove() - Moved Dialog '" + dragmeta.GetHashCode() + "' to " + Canvas.GetLeft(dragmeta.Dialog).ToString() + " : " + Canvas.GetTop(dragmeta.Dialog).ToString());

                    if (dragmeta.Dialog.DialogContent.DragVisualMode == DragVisualMode.Picture)
                    {
                        if (dragmeta.Dialog.Visibility == Visibility.Hidden && dragmeta.IsActive)
                        {
                            dragmeta.Dialog.Visibility = Visibility.Visible;
                            dragmeta.DialogWindowScreenshot.Visibility = Visibility.Hidden;
                        }
                    }
                }
            }

            if (ContainsModalDialog)
            {
                OverlayWindowMeta modalMeta = LastActiveDialogForLayer(OverlayWindowLayer.Modal);

                if (modalMeta != null && modalMeta.Dialog != null)
                {
                    if (_close_buttonless_activedialog_on_canvas_click && _canvas_mousedown && e.ChangedButton == MouseButton.Left &&
                        !modalMeta.Dialog.EventOriginatesInHelpErrorControl(sender, e) && modalMeta.Dialog.WindowState == OverlayDialogState.Modal)
                    {
                        if (!modalMeta.Dialog.AreButtonsVisible)
                        {
#if DEBUG
                            WriteDebugInfo("OverlayManager.Canvas.MouseUp() - Closing active buttonless dialog");
#endif
                            HideContent(modalMeta.Dialog.DialogContent, false);
                        }
                    }
                }
            }

            foreach (OverlayWindowMeta dragmeta in drag_active_windows)
            {
                dragmeta.IsDragActive = false;
                if (dragmeta.Dialog != null)
                    dragmeta.Dialog.SetDragInactive();

                //TODO: Eventuell place in fallback layer

            }

            _canvas_mousedown = false;
            e.Handled = true;

            foreach(OverlayWindowMeta metaInfo in _DialogWindowList)
            {
                if (metaInfo.Dialog != null && metaInfo.Dialog.WindowState != OverlayDialogState.Minimized)
                    metaInfo.Dialog.Resizing_MouseUp(sender, e);
            }

            if (!HasDragActiveWindows() && !HasResizingWindows())
                StopCanvasMouseCapture();
        }

        void overlay_manager_canvas_MouseMove(object sender, MouseEventArgs e)
        {
            //if (!_canvas_mouse_capute_mode && !ModalMode)
            //{
            //    e.Handled = true;
            //    MouseEventArgs newArgs = new MouseEventArgs(e.MouseDevice, e.Timestamp, e.StylusDevice);
            //    newArgs.RoutedEvent = UIElement.MouseMoveEvent;
            //    RaiseEvent(newArgs);
            //    return;
            //}

            _stylus_origin = e.StylusDevice != null;

            Point curr_mouse_pos = e.GetPosition(this);
            List<OverlayWindowMeta> drag_active_windows = GetDragActiveWindows(e.MouseDevice);


            if (drag_active_windows.Count > 0)
            {
                BeginCanvasMouseCapture(); // auch wenn man mit der mouse aus dem dialog fährt soll gemoved werden

                Vector2f new_pos = new Vector2f((float) curr_mouse_pos.X, (float) curr_mouse_pos.Y);
                //new_pos -= _drag_offset;

                if (new_pos.X != 0 || new_pos.Y != 0)
                {
                    foreach (OverlayWindowMeta dragmeta in drag_active_windows)
                    {
                        Vector2f new_pos_dlg = new_pos - dragmeta.DragOffset;
                        dragmeta.DialogMoved = true;

                        if (dragmeta.Dialog.DialogContent.DragVisualMode == DragVisualMode.Picture)
                        {
                            if (dragmeta.Dialog.Visibility == Visibility.Visible)
                            {
                                CreateDialogScreenshots(dragmeta);
                                dragmeta.Dialog.Visibility = Visibility.Hidden;
                                dragmeta.DialogWindowScreenshot.Visibility = Visibility.Visible;
                            }
                        }
                        SetDialogPosition(dragmeta, new_pos_dlg);
                        
                    }

                }
            }

            foreach (OverlayWindowMeta metaInfo in _DialogWindowList)
            {
                if (metaInfo.Dialog != null && metaInfo.Dialog.WindowState != OverlayDialogState.Minimized)
                    metaInfo.Dialog.Resizing_MouseMove(sender, e);
            }

        }
        void overlay_manager_canvas_MouseEnter(object sender, MouseEventArgs e)
        {
            //if (!_canvas_mouse_capute_mode && !ModalMode)
            //{
            //    e.Handled = true;
            //    MouseEventArgs newArgs = new MouseEventArgs(e.MouseDevice, e.Timestamp, e.StylusDevice);
            //    newArgs.RoutedEvent = UIElement.MouseEnterEvent;
            //    RaiseEvent(newArgs);
            //    return;
            //}

            _stylus_origin = e.StylusDevice != null;
            MouseOverDialog = true;

#if DEBUG
            WriteDebugInfo("OverlayManager.Canvas.MouseEnter()");
#endif
            if (e.LeftButton == MouseButtonState.Released)
            {
                List<OverlayWindowMeta> drag_active_windows = GetDragActiveWindows(e.MouseDevice);

                if (drag_active_windows.Count > 0)
                    foreach (OverlayWindowMeta dragmeta in drag_active_windows)
                    {
                        dragmeta.IsDragActive = false;
                        if (dragmeta.Dialog != null)
                            dragmeta.Dialog.SetDragInactive();

                        if (dragmeta.Dialog.DialogContent.DragVisualMode == DragVisualMode.Picture)
                        {
                            if (dragmeta.Dialog.Visibility == Visibility.Hidden && dragmeta.IsActive)
                            {
                                dragmeta.Dialog.Visibility = Visibility.Visible;
                                dragmeta.DialogWindowScreenshot.Visibility = Visibility.Hidden;
                            }
                        }

                        if (dragmeta.DialogWindowScreenshot.IsDraging)
                            dragmeta.DialogWindowScreenshot.SetIsDraging(false);
                    }

                if( HasResizingWindows())
                {
                    foreach (OverlayWindowMeta curMeta in _DialogWindowList)
                        curMeta.Dialog.StopResizing();
                }

                StopCanvasMouseCapture();
            }
        }

        void overlay_manager_canvas_MouseLeave(object sender, MouseEventArgs e)
        {
            MouseOverDialog = false;

            //foreach (OverlayDialog inactiveDlg in _DialogWindowList)
            //    inactiveDlg.Resizing_MouseUp(sender, null);
            //foreach (OverlayWindowMeta metaInfo in _DialogWindowList)
            //{
            //    if (metaInfo.Dialog != null && metaInfo.Dialog.WindowState != OverlayDialogState.Minimized)
            //        metaInfo.Dialog.Resizing_MouseUp(sender, null);
            //}
            _stylus_origin = e.StylusDevice != null;
        }

        #endregion

        #region Dialog EventHandlers


        /// <summary>
        /// Aufgerufen wenn ein Benutzer einen Dialog per button click schliessen möchte
        /// </summary>
        /// <param name="sender">OverlayDialog instanz welche das event gefeuert hat</param>
        /// <param name="e">Eventparameter</param>
        /// <remarks>Dieses event wird nur dann gefeuert, wenn der Dialog auch geschlossen 
        /// werden kann. Die Überprüfung auf CanClose bzw. forceClose ist in diesem Stadium 
        /// bereits abgeschlossen.</remarks>
        void overlayDialog_Closed(object sender, EventArgs e)
        {
            // hide the dialog
            CloseDialog(sender as OverlayDialog);
        }

        private void OverlayDialog_KeyDown(object sender, KeyEventArgs e)
        {
            OverlayDialog dlg = sender as OverlayDialog;

            if (dlg != null && dlg.DialogContent != null)
            {
                _key_down_origins[e.Key] = dlg;
            }
        }

        /// <summary>
        /// Key pressed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OverlayDialog_KeyUp(object sender, KeyEventArgs e)
        {
                OverlayDialog dlg = sender as OverlayDialog;
            //OverlayWindowMeta dlgMeta = GetMetaForDialog(dlg);

            if (!_key_down_origins.ContainsKey(e.Key))
                return;

            if (_key_down_origins[e.Key] != dlg)
            {
                _key_down_origins.Remove(e.Key);
                return;
            }

            if (dlg != null && dlg.DialogContent != null)
            {
                UIElement focusElement = UIHelper.GetControlWithInputfocus(dlg);

                if (focusElement != null)
                {
                    if (e.Key == Key.Enter)
                    {
                        if ((focusElement is TextBox) && ((TextBox)focusElement).AcceptsReturn)
                        {
                            // if the textbox accepts return, we will not send the enter key through
                            return;
                        }
                    }

                    if (e.Key == Key.Back)
                    {
                        if (focusElement is TextBox)
                            return; // text box uses Backspace for text manipulation so do not wire the event through
                    }

                    dlg.DialogContent.OverlayKeyUp(e);
                }
                else
                {
                    dlg.DialogContent.OverlayKeyUp(e);
                }
            }
        }

        void dialog_Loaded(object sender, RoutedEventArgs e)
        {
            OverlayDialog dlgSender = sender as OverlayDialog;

            if(dlgSender != null && dlgSender.DialogContent != null)
            {
                RemoveDelayedLoadedInfo(dlgSender.DialogContent);

                OverlayWindowMeta dlgMeta = GetMetaForDialog(dlgSender);

                if(dlgMeta == null)
                    return;

                try
                {
                    dlgSender.UpdateLayout();
                }
                catch
                {
                }
                
                CreateDialogScreenshots(dlgMeta);
                //dlgMeta.DialogWindowScreenshot.SetImage(dlgSender.RenderToImage());
                dlgMeta.ShowLoadingInfoControl();
                if((dlgMeta.Dialog.DialogContent.GoInactiveBehaviuor != GoInactiveBehaviuor.DoNothing &&
                    dlgMeta.Dialog.DialogContent.GoInactiveBehaviuor != GoInactiveBehaviuor.OpacityAnimationDialog) || dlgMeta.ShowParams != null)
                    dlgSender.Visibility = Visibility.Hidden;
                else
                    dlgMeta.DialogWindowScreenshot.Visibility = Visibility.Hidden;
                

                if (dlgMeta.Layer != OverlayWindowLayer.Presentation)
                    dlgMeta.FallbackLayer = dlgMeta.Layer;
                else if (dlgMeta.Dialog.WindowState == OverlayDialogState.Modal)
                    dlgMeta.FallbackLayer = OverlayWindowLayer.Modal;
                else
                    dlgMeta.FallbackLayer = OverlayWindowLayer.Normal;

                // screenshot auf die präsentations ebene setzen
                LayerManager.PlaceWindowInLayer(dlgMeta, OverlayWindowLayer.Presentation);

                // dialog nach dem laden aktivieren
                if (dlgMeta.Dialog.WindowState == OverlayDialogState.Modal)
                {
                    dlgMeta.DialogLayerZindex[OverlayWindowLayer.Modal] = OverlayLayerManager.MaxModalDialogLayer;
                    dlgMeta.ScreenshotLayerZindex[OverlayWindowLayer.Modal] = OverlayLayerManager.MaxModalDialogLayer + 1;
                    dlgMeta.FallbackLayer = OverlayWindowLayer.Modal;
                }
                else
                {
                    dlgMeta.DialogLayerZindex[OverlayWindowLayer.Active] = OverlayLayerManager.MaxActiveDialogLayer;
                    dlgMeta.ScreenshotLayerZindex[OverlayWindowLayer.Active] = OverlayLayerManager.MaxActiveDialogLayer + 1;
                    dlgMeta.FallbackLayer = OverlayWindowLayer.Active;
                }

                dlgMeta.AnimationTriggerSource = OverlayAnimationTriggerSource.Loading;
                dlgMeta.StartLoadingStartAnimation();

            }
        }

        void dialog_Initialized(object sender, EventArgs e)
        {
             OverlayDialog dlgSender = sender as OverlayDialog;

             if (dlgSender != null && dlgSender.DialogContent != null)
             {
                 //RemoveDelayedLoadedInfo(dlgSender.DialogContent);

                 OverlayWindowMeta dlgMeta = GetMetaForDialog(dlgSender);

                 if (dlgMeta == null)
                     return;

                 try
                 {
                     dlgSender.UpdateLayout();
                 }
                 catch
                 {
                 }
                 CreateDialogScreenshots(dlgMeta);

                 HideBlockingStatusInfo();
             }
        }

        void OverlayAnimation_Completed(object sender, OverlayWindowMeta.OverlayAnimationCompletedArgs e)
        {
            OverlayWindowMeta dlgMeta = sender as OverlayWindowMeta;

            if(dlgMeta != null)
            {
                if (e.AnimationType == OverlayAnimationType.LoadingStart)
                {                   

                    if(dlgMeta.Dialog.DialogContent.InitializationBehaviour == DataInitializationBehaviour.Normal || 
                        dlgMeta.Dialog.DialogContent.InitializationBehaviour == DataInitializationBehaviour.Immediate)
                    {
                        dlgMeta.Dialog.DialogContent.InternalInitialize(false);
                        dlgMeta.InitializationFinshed = true;

                        dlgMeta.ResetAnimatedProperties();
                        GenerateEndLoadingAnimation(dlgMeta);
                        dlgMeta.StartLoadingEndAnimation();
                    }
                    else if(dlgMeta.Dialog.DialogContent.InitializationBehaviour == DataInitializationBehaviour.Background)
                    {
                        _background_initializer = new QueuedBackgroundWorker();
                        _background_initializer.Name = "OverlayManager: BackgroundInitialize Thread";
                        _background_initializer.DoWork += new QueuedBackgroundWorker.DoWorkEventHandler(background_initialize_DoWork);
                        _background_initializer.RunWorkerCompleted += new QueuedBackgroundWorker.RunWorkerCompletedEventHandler(background_initialize_RunWorkerCompleted);
                        _background_initializer.RunWorkerAsync(dlgMeta);
                    }
                }
                else if(e.AnimationType == OverlayAnimationType.LoadingEnd)
                {
                    FinalizeLoadingCycle(dlgMeta);
                }
                else if(e.AnimationType == OverlayAnimationType.NormalToMinimize ||
                    e.AnimationType == OverlayAnimationType.MaximizeToMinimize ||
                    e.AnimationType == OverlayAnimationType.ModalToMinimize)
                {
                    ExplizitSetWindowState(dlgMeta, OverlayDialogState.Minimized);

                    if (_running_minimizing_animations.Contains(dlgMeta.Dialog))
                        _running_minimizing_animations.Remove(dlgMeta.Dialog);

                    if (e.TriggerSource == OverlayAnimationTriggerSource.ActiveStateChange)
                        ExplizitSetActiveState(dlgMeta, false);

                    dlgMeta.Dialog.MinimizingAnimation = null;
                }
                else if(e.AnimationType == OverlayAnimationType.MinimizedToNormal ||
                    e.AnimationType == OverlayAnimationType.MaximizeToNormal)
                {
                    ExplizitSetWindowState(dlgMeta, OverlayDialogState.Normal);
                    // put the window back at the original layer
                    LayerManager.PlaceWindowInLayer(dlgMeta, dlgMeta.FallbackLayer);

                    dlgMeta.Dialog.NormalizingAnimation = null;
                }
                else if (e.AnimationType == OverlayAnimationType.MinimizedToModal ||
                    e.AnimationType == OverlayAnimationType.MaximizedToModal)
                {
                    ExplizitSetWindowState(dlgMeta, OverlayDialogState.Modal);
                    // put the window back at the original layer
                    LayerManager.PlaceWindowInLayer(dlgMeta, dlgMeta.FallbackLayer);
                }
                else if(e.AnimationType == OverlayAnimationType.MinimizeToMaximize ||
                    e.AnimationType == OverlayAnimationType.NormalToMaximize ||
                    e.AnimationType == OverlayAnimationType.MaximizedToModal)
                {
                    ExplizitSetWindowState(dlgMeta, OverlayDialogState.Maximized);
                    // put the window back at the original layer
                    LayerManager.PlaceWindowInLayer(dlgMeta, dlgMeta.FallbackLayer);
                }
                else if(e.AnimationType == OverlayAnimationType.FadeInOpacity)
                {
                    if (e.TriggerSource == OverlayAnimationTriggerSource.ActiveStateChange)
                        ExplizitSetActiveState(dlgMeta, true);
                }
                else if (e.AnimationType == OverlayAnimationType.FadOutOpacity)
                {
                    if (dlgMeta.AnimationTriggerSource == OverlayAnimationTriggerSource.ActiveStateChange)
                        ExplizitSetActiveState(dlgMeta, false);
                }
                else if (e.AnimationType == OverlayAnimationType.CustomActivate)
                {
                    if (e.TriggerSource == OverlayAnimationTriggerSource.ActiveStateChange)
                        ExplizitSetActiveState(dlgMeta, true);
                }
                else if (e.AnimationType == OverlayAnimationType.CustomDeactivate)
                {
                    if (e.TriggerSource == OverlayAnimationTriggerSource.ActiveStateChange)
                        ExplizitSetActiveState(dlgMeta, false);
                }

                // reset animation trigger
                dlgMeta.AnimationTriggerSource = OverlayAnimationTriggerSource.Undefined;

                dlgMeta.ResetAnimatedProperties();

                if(e.AnimationType == OverlayAnimationType.LoadingEnd)
                {
                    ScaleTransform s_tran = dlgMeta.DialogWindowScreenshot.RenderTransform as ScaleTransform;
                    if(s_tran != null)
                    {
                        s_tran.ScaleX = 1;
                        s_tran.ScaleY = 1;
                        dlgMeta.Dialog.DialogContent.DialogImageDirty = true;
                    }
                }
            }
        }

        private void GenerateEndLoadingAnimation(OverlayWindowMeta dlgMeta)
        {
            if (dlgMeta.ShowParams != null && dlgMeta.Dialog != null)
            {
                CreateDialogScreenshots(dlgMeta);
                double to_scale_x;
                double to_scale_y;
                if(dlgMeta.DialogWindowScreenshot.ActualWidth != 0)
                    to_scale_x = dlgMeta.Dialog.ActualViewboxWidth / (dlgMeta.DialogWindowScreenshot.ActualWidth - dlgMeta.DialogWindowScreenshot.ImageRenderOffsetWidth);
                else
                    to_scale_x = dlgMeta.Dialog.ActualViewboxWidth / 100;

                if (dlgMeta.DialogWindowScreenshot.ActualHeight != 0)
                    to_scale_y = dlgMeta.Dialog.ActualViewboxHeight / (dlgMeta.DialogWindowScreenshot.ActualHeight - dlgMeta.DialogWindowScreenshot.ImageRenderOffsetHeight);
                else
                    to_scale_y = dlgMeta.Dialog.ActualViewboxHeight / 100;


                Vector2f endpos = GetStartupPosition(dlgMeta);

                dlgMeta.Dialog.DialogContent.LoadingEndStoryboard = overlay_window_manager.GetDefaultStartupAnimation(dlgMeta, dlgMeta.ShowParams.StartPoint, new Point(endpos.X, endpos.Y), new Point(to_scale_x, to_scale_y));
                OverlayWindowMeta.OverlayAnimationProperties prop = new OverlayWindowMeta.OverlayAnimationProperties();
                prop.FinalScalex = to_scale_x;
                prop.FinalScaley = to_scale_y;
                prop.FinalLeft = endpos.X;
                prop.FinalTop = endpos.Y;
                prop.FinalOpacity = 1;
                dlgMeta.Dialog.DialogContent.LoadingEndStoryboardFinalPos = prop;
                //XXX ist der hide blocking status info hier erforderlich????? alexp
                HideBlockingStatusInfo();
            }
            else if (dlgMeta.Dialog != null && dlgMeta.Dialog.WindowState == OverlayDialogState.Modal)
            {                
                //CreateDialogScreenshots(dlgMeta);
                //double to_scale_x = dlgMeta.Dialog.ActualViewboxWidth / dlgMeta.DialogWindowScreenshot.Width;
                //double to_scale_y = dlgMeta.Dialog.ActualViewboxHeight / dlgMeta.DialogWindowScreenshot.Height;
                double to_scale_x = 1;
                double to_scale_y = 1;

                Vector2f endpos = GetStartupPosition(dlgMeta);

                dlgMeta.Dialog.DialogContent.LoadingEndStoryboard = overlay_window_manager.GetDefaultStartupAnimation(dlgMeta, Mouse.GetPosition(this), new Point(endpos.X, endpos.Y), new Point(0, 0), new Point(to_scale_x, to_scale_y));
                OverlayWindowMeta.OverlayAnimationProperties prop = new OverlayWindowMeta.OverlayAnimationProperties();
                prop.FinalScalex = to_scale_x;
                prop.FinalScaley = to_scale_y;
                prop.FinalLeft = endpos.X;
                prop.FinalTop = endpos.Y;
                prop.FinalOpacity = 1;
                dlgMeta.Dialog.DialogContent.LoadingEndStoryboardFinalPos = prop;
                //dlgMeta.Dialog.DialogMeta.OverlayManager.HideBlockingStatusInfo();
            }
        }

        //void myStoryboard_Completed(object sender, EventArgs e)
        //{
        //    if (_anim_overlay_dialog.Count > 0)
        //    {
        //        _anim_overlay_dialog[0].ExplizitSetState(OverlayDialogState.Minimized);
        //        OverlayDialog d = _anim_overlay_dialog[0];
        //        _anim_overlay_dialog.Remove(_anim_overlay_dialog[0]);
        //    }
        //    ((ClockGroup)sender).Completed -= new EventHandler(myStoryboard_Completed);
        //}

        //void normalize_anim_completed(object sender, EventArgs e)
        //{
        //    if (_anim_normalize.Count > 0)
        //    {
        //        _anim_normalize[0].ExplizitSetState(OverlayDialogState.Normal);
        //        _anim_normalize.Remove(_anim_normalize[0]);
        //    }
        //    ((ClockGroup)sender).Completed -= new EventHandler(normalize_anim_completed);
        //}


        void FinalizeLoadingCycle(OverlayWindowMeta metaData)
        {
            if(metaData.LoadingStartAnimationFinished && metaData.LoadingEndAnimationFinished && metaData.InitializationFinshed)
            {
                if (!this.Dispatcher.CheckAccess())
                {
                    Dispatcher.Invoke(DispatcherPriority.Normal, (NoParas) delegate
                    {
                        EqualizeDialogScreenshot(metaData);
                        LayerManager.PlaceWindowInLayer(metaData, metaData.FallbackLayer);
                       if (metaData.IsActive)
                       {
                           metaData.DialogWindowScreenshot.Visibility = Visibility.Hidden;

                           if (metaData.Dialog.WindowState != OverlayDialogState.Minimized)
                               metaData.Dialog.Visibility = Visibility.Visible;
                       }

                       metaData.RemoveWindowScreenControl();
                       if (!(metaData.Dialog.DialogContent).AutoResized)
                            metaData.Dialog.AutoResized = false;
                       //metaData.Dialog.SizeChanged += new SizeChangedEventHandler(_ActiveDialog_SizeChanged);
                       metaData.Dialog.DialogContent.InternalAfterShow();
                       metaData.Dialog.DialogContent.UpdateImageRenderBehaviourRegistrations();
                       try
                       {
                           metaData.Dialog.UpdateLayout();
                       }
                       catch
                       {
                       }
                       _ActiveDialog_SizeChanged(metaData.Dialog, null);
                       LayerManager.RecalculateLayers(metaData);
                        ShowOverlay();
                        metaData.IgnoreMouseInputs = false;
                    });
                }
                else
                {
                    EqualizeDialogScreenshot(metaData);
                    LayerManager.PlaceWindowInLayer(metaData, metaData.FallbackLayer);
                    if (metaData.IsActive)
                    {
                        SetDialogPosition(metaData, new Vector2f((float)Canvas.GetLeft(metaData.DialogWindowScreenshot), (float)Canvas.GetTop(metaData.DialogWindowScreenshot)));
                        
                        metaData.DialogWindowScreenshot.Visibility = Visibility.Hidden;

                        if (metaData.Dialog.WindowState != OverlayDialogState.Minimized)
                            metaData.Dialog.Visibility = Visibility.Visible;
                    }

                    metaData.RemoveWindowScreenControl();
                    if (!(metaData.Dialog.DialogContent).AutoResized)
                        metaData.Dialog.AutoResized = false;
                    //metaData.Dialog.SizeChanged += new SizeChangedEventHandler(_ActiveDialog_SizeChanged);
                    metaData.Dialog.DialogContent.InternalAfterShow();
                    metaData.Dialog.DialogContent.UpdateImageRenderBehaviourRegistrations();
                    try
                    {
                        metaData.Dialog.UpdateLayout();
                    }
                    catch
                    {
                    }
                    _ActiveDialog_SizeChanged(metaData.Dialog, null);
                    LayerManager.RecalculateLayers(metaData);
                    ShowOverlay();
                    metaData.IgnoreMouseInputs = false;
                }
            }
        }

        #region background initializer thread
        private void background_initialize_DoWork(object sender, Logicx.Utilities.DoWorkEventArgs e)
        {
            OverlayWindowMeta dlgMeta = e.Argument as OverlayWindowMeta;
            e.Result = e.Argument;

            if(dlgMeta != null)
            {
                dlgMeta.Dialog.DialogContent.InternalInitialize(true);
            }
        }

        private void background_initialize_RunWorkerCompleted(object sender, Logicx.Utilities.RunWorkerCompletedEventArgs e)
        {
            OverlayWindowMeta dlgMeta = e.Result as OverlayWindowMeta;

            _background_initializer.DoWork -= background_initialize_DoWork;
            _background_initializer.RunWorkerCompleted -= background_initialize_RunWorkerCompleted;

            if (dlgMeta != null)
            {
                System.Diagnostics.Debug.WriteLine("*** Background INIT FINSIHED");
                dlgMeta.InitializationFinshed = true;

                Dispatcher.Invoke(DispatcherPriority.Normal, (NoParas) delegate
                   {
                       GenerateEndLoadingAnimation(dlgMeta);
                       dlgMeta.StartLoadingEndAnimation();
                   });

            }

            _background_initializer.Dispose();
        }

        #endregion


        void Dialog_StylusDown(object sender, StylusDownEventArgs e)
        {
            e.Handled = true;
            _stylus_origin = true;
        }


        void Dialog_StylusUp(object sender, StylusEventArgs e)
        {
            //e.Handled = true;
            _stylus_origin = true;
        }

        void Dialog_StylusMove(object sender, StylusEventArgs e)
        {
            //e.Handled = true;
            _stylus_origin = true;
        }



        void OverlayDialog_MouseDown(object sender, MouseButtonEventArgs e)
        {
#if DEBUG
            WriteDebugInfo("OverlayManager.Dialog.MouseDown()");
#endif
            _stylus_origin = e.StylusDevice != null;

            if (e.ChangedButton == MouseButton.Right && e.RightButton == MouseButtonState.Pressed)
            {
                if (RightMouseClickEventReceiver != null)
                {
                    MouseButtonEventArgs newArgs = new MouseButtonEventArgs(e.MouseDevice, e.Timestamp, e.ChangedButton, e.StylusDevice);
                    newArgs.RoutedEvent = UIElement.MouseDownEvent;
                    newArgs.Handled = false;
                    RightMouseClickEventReceiver.RaiseEvent(newArgs);
                    return;
                }
            }

            if (_running_minimizing_animations.Count > 0)
                return;

            OverlayWindowMeta dlgMeta = null;
            
            if(sender is OverlayDialog)
            {
                dlgMeta = GetMetaForDialog((OverlayDialog)sender);
            }
            else if (sender is OverlayPictureHost)
            {
                dlgMeta = GetMetaForDialogScreenshot((OverlayPictureHost)sender);
            }
            
            if (dlgMeta == null || dlgMeta.Dialog == null)
                return;

            if (dlgMeta.IsAnimationRunning)
                return;

            dlgMeta.IsDragActive = true;
            if (dlgMeta.Dialog != null)
                dlgMeta.Dialog.SetDragActive();

            dlgMeta.DragMouseDevice = e.MouseDevice;

            //Point offset_drag_elem = e.GetPosition(dlgMeta.Dialog);
            //if(dlgMeta.Dialog.Visibility != Visibility.Visible)
            //    offset_drag_elem = e.GetPosition(dlgMeta.DialogWindowScreenshot);

            //_drag_offset.X = 0;
            //_drag_offset.Y = 0;

            dlgMeta.CalculateDragOffset(e, overlay_manager_canvas);

            if (!dlgMeta.Dialog.EventOriginatesInHelpErrorArea(e.Source, e))
            {
                if (dlgMeta.Layer != OverlayWindowLayer.Presentation)
                    dlgMeta.FallbackLayer = dlgMeta.Layer;
                else if (dlgMeta.Dialog.WindowState == OverlayDialogState.Modal)
                    dlgMeta.FallbackLayer = OverlayWindowLayer.Modal;
                else
                    dlgMeta.FallbackLayer = OverlayWindowLayer.Normal;

                LayerManager.PlaceWindowInLayer(dlgMeta, OverlayWindowLayer.Presentation);

                if (dlgMeta.Dialog.WindowState == OverlayDialogState.Modal)
                {
                    dlgMeta.DialogLayerZindex[OverlayWindowLayer.Modal] = OverlayLayerManager.MaxModalDialogLayer;
                    dlgMeta.ScreenshotLayerZindex[OverlayWindowLayer.Modal] = OverlayLayerManager.MaxModalDialogLayer + 1;
                    dlgMeta.FallbackLayer = OverlayWindowLayer.Modal;

                    LayerManager.AdjustCustomControlZIndex();
                }
                else
                {
                    dlgMeta.DialogLayerZindex[OverlayWindowLayer.Active] = OverlayLayerManager.MaxActiveDialogLayer;
                    dlgMeta.ScreenshotLayerZindex[OverlayWindowLayer.Active] = OverlayLayerManager.MaxActiveDialogLayer + 1;
                    dlgMeta.FallbackLayer = OverlayWindowLayer.Active;

                    // die Custom controls eine Z-Ebene über die dragging dialoge setzen
                    // damit diese nicht drüber gezeichnet werden
                    LayerManager.AdjustCustomControlZIndex(dlgMeta.DialogLayerZindex[OverlayWindowLayer.Presentation] + 1);
                }

                dlgMeta.DialogWindowScreenshot.Opacity = 1;
                dlgMeta.DialogWindowScreenshot.SetIsDraging(true);
                //_drag_offset = new Vector2f((float) offset_drag_elem.X, (float) offset_drag_elem.Y);
                e.Handled = true;

#if DEBUG
                WriteDebugInfo("OverlayManager.Dialog.MouseDown() - Dialog '" + dlgMeta.GetHashCode() + "' is drag active.");
#endif
            }
            else
            {
                dlgMeta.IsDragActive = false;
                if (dlgMeta.Dialog != null)
                    dlgMeta.Dialog.SetDragInactive();

                if (dlgMeta.DialogWindowScreenshot.IsDraging)
                    dlgMeta.DialogWindowScreenshot.SetIsDraging(false);

#if DEBUG
                WriteDebugInfo("OverlayManager.Dialog.MouseDown() - Ignoring event since it's origin is in the help/error area.");
#endif
            }
        }

        void OverlayDialog_MouseUp(object sender, MouseButtonEventArgs e)
        {
#if DEBUG
            WriteDebugInfo("OverlayManager.Dialog.OverlayDialog_MouseUp()");
#endif
            _stylus_origin = e.StylusDevice != null;

            if (e.ChangedButton == MouseButton.Right && e.RightButton == MouseButtonState.Released)
            {
                if (RightMouseClickEventReceiver != null)
                {
                    MouseButtonEventArgs newArgs = new MouseButtonEventArgs(e.MouseDevice, e.Timestamp, e.ChangedButton, e.StylusDevice);
                    newArgs.RoutedEvent = UIElement.MouseUpEvent;
                    newArgs.Handled = false;
                    RightMouseClickEventReceiver.RaiseEvent(newArgs);
                    return;
                }
            }

            if (_running_minimizing_animations.Count > 0)
                return;



            OverlayWindowMeta dlgMeta = null;

            if (sender is OverlayDialog)
            {
                dlgMeta = GetMetaForDialog((OverlayDialog)sender);
            }
            else if (sender is OverlayPictureHost)
            {
                dlgMeta = GetMetaForDialogScreenshot((OverlayPictureHost)sender);
            }

            if (dlgMeta == null || dlgMeta.Dialog == null)
                return;

            if (dlgMeta.IsAnimationRunning)
                return;

            bool was_drag_active = dlgMeta.IsDragActive;

            //_drag_offset = new Vector2f();
            dlgMeta.DragOffset = new Vector2f();
            dlgMeta.IsDragActive = false;
            if (dlgMeta.Dialog != null)
                dlgMeta.Dialog.SetDragInactive();

            if (dlgMeta.Dialog.Visibility == Visibility.Hidden && dlgMeta.IsActive)
            {
                dlgMeta.Dialog.Visibility = Visibility.Visible;
                dlgMeta.DialogWindowScreenshot.Visibility = Visibility.Hidden;
            }

            if (!dlgMeta.Dialog.EventOriginatesInHelpErrorArea(e.Source, e))
            {
                e.Handled = false; // das event zum canvas weiterschicken !

                if (!dlgMeta.IsActive)
                {
                    SetActiveDialog(dlgMeta);
#if DEBUG
                    WriteDebugInfo("OverlayManager.Dialog.MouseUp() - Dialog '" + dlgMeta.GetHashCode() + "' being activated.");
#endif
                }

                dlgMeta.Dialog.RestoreFocusedElement();
            }

            if (was_drag_active)
            {
                if (dlgMeta.DialogWindowScreenshot.IsDraging)
                    dlgMeta.DialogWindowScreenshot.SetIsDraging(false);

                LayerManager.PlaceWindowInLayer(dlgMeta, dlgMeta.FallbackLayer);

            }
        }

        private void overlay_manager_overall_canvas_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            overlay_window_manager.RepositionWindowManager(e.NewSize);
            ClearPosArray();

            foreach(OverlayWindowMeta meta in _DialogWindowList)
            {
                if(meta.Dialog.DialogContent.ResizesWithWindowInMaximizedMode)
                {
                    // force auto sizing of dialog
                    meta.Dialog.NeedsSizeOptimization = true;

                    if(meta.Dialog.WindowState == OverlayDialogState.Maximized)
                    {
                        ShowDialogMaximized(meta);
                    }
                }
                else
                {
                    double dleft = Canvas.GetLeft(meta.Dialog);
                    double dtop = Canvas.GetTop(meta.Dialog);

                    if ((dleft.IsNaN() || dtop.IsNaN()) ||
                        (dleft >= e.NewSize.Width || dtop >= e.NewSize.Height))
                    {
                        if (meta.Dialog.SearchFreePosition && meta.Dialog.FoundFreePosition)
                            meta.Dialog.FoundFreePosition = false; // force repositioning after size change

                        _ActiveDialog_SizeChanged(meta.Dialog, null);
                    }
                }
            }
        }

        private void ShowDialogMaximized(OverlayWindowMeta meta)
        {
            Point to_topleft = new Point(0, 0);
            double to_scale_x = 0;
            double to_scale_y = 0;
            double new_height = 0;
            double new_width = 0;
            CalculateMaximizedWindowProperties(meta, out to_topleft, out to_scale_x, out to_scale_y, out new_width, out new_height);

            meta.Dialog.StartupPosition = to_topleft;
            //SetDialogPosition(dlgMeta, new Vector2f((float)to_topleft.X, (float)to_topleft.Y));

            meta.Dialog.ResizeToHeight(new_height);
            meta.Dialog.ResizeToWidth(new_width);

            meta.Dialog.WindowState = OverlayDialogState.Maximized;
            meta.Dialog.NeedsSizeOptimization = false;
        }


        void _ActiveDialog_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            OverlayDialog dlg = sender as OverlayDialog;
            OverlayWindowMeta dlgMeta = GetMetaForDialog(dlg);

            if (dlgMeta == null || dlg == null)
                return;

            if (dlg.InitialShowMode == OverlayDialogInitialShowMode.Optimized)
            {
                SetOptimalSize(dlgMeta);
                if (dlgMeta.Dialog.SearchFreePosition && !dlgMeta.Dialog.FoundFreePosition)
                {
                    FindFreePosition(dlgMeta);
                    dlgMeta.Dialog.FoundFreePosition = true;
                }
                else if (!dlgMeta.Dialog.AutoCenteredDialog)
                {
                    if (dlgMeta.Dialog.DialogContent.IsInitialized)
                    {
                        // only react once
                        dlgMeta.Dialog.SizeChanged -= _ActiveDialog_SizeChanged;
                    }
                    else
                    {
                        // wenn der content das autorezize haben will
                        if (!(dlgMeta.Dialog.DialogContent).AutoResized)
                            dlgMeta.Dialog.AutoResized = false;
                    }
                    // center dialog in parent canvas
                    double dleft = Canvas.GetLeft(dlg);
                    double dtop = Canvas.GetTop(dlg);

                    if (dleft.IsNaN() || dtop.IsNaN())
                        CenterDialog(dlgMeta);
                }
                else
                {
                    if (!dlgMeta.DialogMoved)
                    {
                        // center dialog in parent canvas
                        CenterDialog(dlgMeta);
                    }
                }
            }
            else if(dlg.InitialShowMode == OverlayDialogInitialShowMode.Custom)
            {
                SetOptimalSize(dlgMeta);
                if (!dlgMeta.DialogMoved)
                {
                    // center dialog in parent canvas
                    SetDialogPosition(dlgMeta, dlgMeta.Dialog.DialogContent.InitialPosition);
                }
            }
            else if (dlg.InitialShowMode == OverlayDialogInitialShowMode.Maximized)
            {
                if (dlgMeta.Dialog.DialogContent.IsInitialized)
                {
                    // only react once
                    dlgMeta.Dialog.SizeChanged -= _ActiveDialog_SizeChanged;
                    return;
                }
                ShowDialogMaximized(dlgMeta);
            }
        }
        #endregion

        #region WindowTaskbar EventHandler
        void WindowTaskBar_MenuButtonClick(object sender, WindowTaskBar.TaskbarButtonClickEventArgs e)
        {
            if(e.ButtonTitle.StartsWith("minimized"))
            {
                
            }
        }
        #endregion
        #endregion

        #region Animation Functions
        public void MinimizeDialogToPosition(OverlayContent content, Point to)
        {
            OverlayWindowMeta metaInfo = GetMetaForContent(content);

            overlay_window_manager.MinimizeDialogToPosition(metaInfo, new Point(Canvas.GetLeft(metaInfo.Dialog), Canvas.GetTop(metaInfo.Dialog)), to, true);
        }
        public void NormalizeDialogToPosition(OverlayContent content, Point to)
        {
            OverlayWindowMeta metaInfo = GetMetaForContent(content);

            overlay_window_manager.NormalizeDialogToPosition(metaInfo, new Point(Canvas.GetLeft(metaInfo.DialogWindowScreenshot), Canvas.GetTop(metaInfo.DialogWindowScreenshot)), to, false);
        }
        public void NormalizeDialogToPosition(OverlayContent content)
        {
            OverlayWindowMeta metaInfo = GetMetaForContent(content);
            if(metaInfo!= null && metaInfo.Dialog!= null)
                NormalizeDialogToPosition(content, new Point(Canvas.GetLeft(metaInfo.Dialog), Canvas.GetTop(metaInfo.Dialog)));
        }
        #endregion

        #region Dialog Positioning
        public Vector2f GetStartupPosition(OverlayWindowMeta dialogMeta)
        {
            if (dialogMeta.Dialog.InitialShowMode == OverlayDialogInitialShowMode.Optimized)
                return GetCenteredPosition(dialogMeta);
            else if (dialogMeta.Dialog.InitialShowMode == OverlayDialogInitialShowMode.Maximized)
                return new Vector2f((float)dialogMeta.Dialog.StartupPosition.X, (float)dialogMeta.Dialog.StartupPosition.Y);

            return new Vector2f();
        }

        public Vector2f GetCenteredPosition(OverlayWindowMeta dialogMeta)
        {
            if (dialogMeta == null || dialogMeta.Dialog == null)
                return new Vector2f(0,0);

            OverlayDialog dialog = dialogMeta.Dialog;

            double cWidth = overlay_manager_canvas.ActualWidth;
            double cHeight = overlay_manager_canvas.ActualHeight;

            if(dialogMeta.Dialog.DialogContent.ClientAreaSizeSensitive)
            {
                cWidth = AvailableWindowClientWidth;
                cHeight = AvailableWindowClientHeight;
            }

            double dWidth = dialog.GetCenteringWidth(); //dialog.ActualWidth;
            double dHeight = dialog.GetCenteringHeight(); // dialog.ActualHeight;

            //dWidth = dialog.ActualViewboxWidth;
            //dHeight = dialog.ActualViewboxHeight;

            double lPos = 0;
            if (cWidth > dWidth)
                lPos = (cWidth - dWidth) / 2;

            double tPos = 0;
            if (cHeight > dHeight)
                tPos = (cHeight - dHeight) / 2;

            return new Vector2f((float) lPos, (float) tPos);
        }

        private void CenterDialog(OverlayWindowMeta dialogMeta)
        {
            if (dialogMeta == null || dialogMeta.Dialog == null)
                return;

            SetDialogPosition(dialogMeta, GetCenteredPosition(dialogMeta));
        }

        /// <summary>
        /// Verändert die dargestellte Größe des Dialogs, wenn das Hauptfenster nicht genügend
        /// Platz bietet
        /// </summary>
        /// <param name="dialog"></param>
        private void SetOptimalSize(OverlayWindowMeta dialogMeta)
        {
            if (dialogMeta == null || dialogMeta.Dialog == null)
                return;

            OverlayDialog dialog = dialogMeta.Dialog;

            // Wenn der Dialog bereits automatisch in seiner Dimension verändert wurde bzw. an einer freien Position plaziert werden soll
            // soll keine Größenveränderung durchgeführt werden
            if (dialog.AutoResized || dialog.SearchFreePosition)
                return;

            bool iswidth;
            double new_size = CalculateOptimalSize(dialogMeta, out iswidth);

            if(new_size == 0)
                return;

            if(iswidth)
                dialog.ResizeToWidth(new_size, dialog.ActualWidthErrorHelpVisibility);
            else
                dialog.ResizeToHeight(new_size, dialog.ActualWidthErrorHelpVisibility);
          
            dialog.AutoResized = true;
        }

        /// <summary>
        /// Berechnet die optimale Größe eine Dialoges, falls dieser zu groß ist, für die aktuelle Fenstergröße.
        /// Wenn der Dialog kleiner ist, als der Fensterausschnitt wird 0 zurückgegeben
        /// 
        /// Da die Seitenverhältnisse des Dialogs erhalten bleiben sollen, reicht es nur eine Dimensionsgröße zurückzugeben. Es wird daher mit der Variablen iswidth
        /// angegeben, ob sich der Rückgabewert auf die Breite oder auf die Höhe des Dialogs bezieht
        /// </summary>
        /// <param name="dialogMeta"></param>
        /// <param name="iswidth"></param>
        /// <returns></returns>
        private double CalculateOptimalSize(OverlayWindowMeta dialogMeta, out bool iswidth)
        {
            iswidth = true;

            OverlayDialog dialog = dialogMeta.Dialog;

            double cWidth = dialog.ActualWidthErrorHelpVisibility; // dialog.ActualWidth;
            double cHeight = dialog.ActualHeight;

            //double parent_width = ParentWidth;
            //double parent_height = ParentHeight;
            double parent_width = ActualVisibleWidth;
            double parent_height = ActualVisibleHeight;

            if (WithDialogShadow || WithBackgroundShadow)
            {
                parent_width -= OVERLAY_SHADOW_IMAGE_OFFSET_X;
                parent_height -= OVERLAY_SHADOW_IMAGE_OFFSET_Y;

                cWidth += OVERLAY_SHADOW_IMAGE_OFFSET_X;
                cHeight += OVERLAY_SHADOW_IMAGE_OFFSET_Y;
            }

            if (cWidth == 0 || double.IsNaN(cWidth) || double.IsNaN(cHeight) || cHeight == 0)
                return 0;

            // Dialog ist sowohl in Breite, als auch in der Höhe zu groß
            if (cWidth > parent_width && cHeight > parent_height)
            {
                if ((cWidth - parent_width) < (cHeight - parent_height))
                {
                    iswidth = false;
                    return parent_height;
                }
                else
                    return parent_width;
            }
            else if (cWidth > parent_width)
                return parent_width;
            else if (cHeight > parent_height)
            {
                iswidth = false;
                return parent_height;
            }            
            
            return 0;
        }

        private void CalculateMaximizedWindowProperties(OverlayWindowMeta dlgMeta, out Point to_topleft, out double to_scale_x, out double to_scale_y, out double new_width, out double new_height)
        {
            to_topleft = new Point(0, 0);
            to_scale_x = 1;
            to_scale_y = 1;
            new_width = 0;
            new_height = 0;

            double available_width = this.ActualVisibleWidth;
            double available_height = this.ActualVisibleHeight;
            double cur_width = dlgMeta.Dialog.ActualWidthErrorHelpVisibility * dlgMeta.Dialog.ActualScaleX;
            double cur_height = dlgMeta.Dialog.ActualViewboxHeight;

            
            if (dlgMeta.Dialog.MaximizeMode == OverlayDialogMaximizeMode.Normal)
            {               
                //double available_width = overlay_manager_overall_canvas.ActualWidth;
                //double available_height = overlay_manager_overall_canvas.ActualHeight;


                double to_scale = 1;

                bool iswidth;
                double est_width = CalculateOptimalSize(dlgMeta, out iswidth);

                double est_to_scale_width = available_width / cur_width;
                double est_to_scale_height = available_height / cur_height;

                if (Math.Round(cur_width * est_to_scale_width, 6) <= Math.Round(available_width, 6) && Math.Round(cur_height * est_to_scale_width, 6) <= Math.Round(available_height, 6))
                    to_scale = est_to_scale_width;
                else if (Math.Round(cur_width * est_to_scale_height, 6) <= Math.Round(available_width, 6) && Math.Round(cur_height * est_to_scale_height, 6) <= Math.Round(available_height, 6))
                    to_scale = est_to_scale_height;
                else if (cur_width > cur_height)
                    to_scale = est_to_scale_width;
                else
                    to_scale = est_to_scale_height;

                if (est_width != 0)
                {
                    if (iswidth)
                        to_scale = est_width / cur_width;
                    else
                        to_scale = est_width / cur_height;
                }

                new_width = cur_width * to_scale;
                new_height = cur_height * to_scale;

                

                // Dialog soll in die Mitte des Bildschirms fahren               
                if (new_width < available_width)
                    to_topleft.X = (available_width - new_width) / 2;
                if (new_height < available_height)
                    to_topleft.Y = (available_height - new_height) / 2;

                to_topleft.X += AcutalVisibleTopLeft.X;
                to_topleft.Y += AcutalVisibleTopLeft.Y;

                to_scale_x = to_scale;
                to_scale_y = to_scale;
            }
            else if (dlgMeta.Dialog.MaximizeMode == OverlayDialogMaximizeMode.ReserveErrorViewerSpace)
            {
                new_width = 0;
                new_height = available_height;
                if (dlgMeta.Dialog.HelpOrErrorProviderVisible)
                    new_width = available_width;
                else
                    new_width = available_width - dlgMeta.Dialog.HelpErrorProviderMaxWidth;

                to_scale_x = new_width / cur_width;
                to_scale_y = new_height / cur_height;

                to_topleft.Y += 30;
                to_topleft.X += 5;
            }
        }

        public void SetDialogPosition(OverlayWindowMeta dialogMeta, Vector2f pos)
        {
            if (dialogMeta == null || dialogMeta.Dialog == null)
                return;

            OverlayDialog dialog = dialogMeta.Dialog;

            Canvas.SetLeft(dialog, pos.X);
            Canvas.SetTop(dialog, pos.Y);

            // screenshot bild "mitbewegen"
            if (dialogMeta.DialogWindowScreenshot != null)
            {
                Canvas.SetLeft(dialogMeta.DialogWindowScreenshot, pos.X);
                Canvas.SetTop(dialogMeta.DialogWindowScreenshot, pos.Y);
            }
        }

        public Vector2f GetDialogPosition(OverlayDialog dialog)
        {
            if (dialog == null)
                return new Vector2f();

            if (overlay_manager_canvas.Children.Contains(dialog))
            {
                double x = Canvas.GetLeft(dialog);
                double y = Canvas.GetTop(dialog);
                return new Vector2f((float)x, (float)y);
            }

            return new Vector2f();
        }

        internal void BeginCanvasMouseCapture()
        {
            overlay_manager_canvas.CanvasMouseCapute = true;
        }

        internal void StopCanvasMouseCapture()
        {
            overlay_manager_canvas.CanvasMouseCapute = false;
        }
        #endregion

        #region Calculate Free Position for DashCards

        public void PositonAndAlignDialog(OverlayWindowMeta last_dialog)
        {
            last_dialog.Dialog.FoundFreePosition = FindFreePosition(last_dialog);
        }

        /// <summary>
        /// Methode findet für das zuletzt hinzugefügt Element in die _visible_dash_cards eine Position,
        /// sodass sich die Elements nicht gegenseitig überdecken.
        /// </summary>
        protected bool FindFreePosition(OverlayWindowMeta last_dialog)
        {
            // Wenn die Dashcard keine gültigen Werte für die Höhe bzw. Weite hat, ist eine Positionsbestimmung nicht von Sinn
            if (last_dialog.Dialog.ActualViewboxHeight == 0 || last_dialog.Dialog.ActualViewboxWidth == 0)
                return false;

            _layouted_elements.Add(last_dialog.Dialog);
            Monitor.Enter(this);

            GeneratePositionSnapshot(last_dialog.Dialog);

            Vector2f free_pos = SearchFreePosition((int)last_dialog.Dialog.ActualViewboxWidth, (int)last_dialog.Dialog.ActualViewboxHeight, last_dialog.Dialog.SearchPositionAlignment);
            if (free_pos.X != -1)
            {
                free_pos.X += (float)(last_dialog.Dialog.SearchFreePositionMargin.Left + last_dialog.Dialog.SearchFreePositionMargin.Right);
                free_pos.Y += (float)(last_dialog.Dialog.SearchFreePositionMargin.Top + last_dialog.Dialog.SearchFreePositionMargin.Bottom);
                SetDialogPosition(last_dialog, free_pos);
            }
            
            
            Monitor.Exit(this);

            return (free_pos.X != -1);
        }
       
        /// <summary>
        /// Ausgehend von den aktuell dargestellten Elementen wird ein Positionsarray angelegt, das die abgedeckten Flächen durch 1er markiert,
        /// bzw. die freien Positionen durch 0er. Mit diesem Array ist es anschließend einfacher möglich freie Positionen zu finden
        /// </summary>
        protected void GeneratePositionSnapshot(OverlayDialog dialog_to_ignore)
        {
            ClearPosArray();
            //wenn nicht zumindest 2 Overlay-Dialoge vorhanden sind ist das Layouten nicht notwendig
            if (_layouted_elements.Count < 2)
                return;
                        
            for (int dc_count = 0; dc_count < _layouted_elements.Count; dc_count++)
            {
                OverlayDialog dialog = _layouted_elements[dc_count];
                if (dialog == dialog_to_ignore)
                    continue;

                Vector2f pos = GetDialogPosition(dialog);
                for (int i = (int)(pos.Y>0?pos.Y:0); i <= (int)pos.Y + dialog.ActualViewboxHeight + dialog.SearchFreePositionMargin.Bottom + dialog.SearchFreePositionMargin.Top; i++)
                {
                    for (int e = (int)(pos.X>0?pos.X:0); e <= (int)pos.X + dialog.ActualViewboxWidth + dialog.SearchFreePositionMargin.Left + dialog.SearchFreePositionMargin.Right; e++)
                    {
                        // An jeder Position an der sich das Positionsarray mit dem Element überschneidet wird im Array der Wert gesetzt
                        if (e < POS_DIM_X && i < POS_DIM_Y)
                            _position_array[i, e]++;
                    }
                }
            }
        }

        /// <summary>
        /// Ausgehend von einem initialisierten Positionsarrays, wird nach freien Positionen gesucht für ein Element mit den 
        /// übergebenen width und height Properties. Zurückgegeben wird der TopLeft Punkt des Elements wo man dieses positionieren kann
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <returns></returns>
        private Vector2f SearchFreePosition(int width, int height, SearchPositionAlignment alignment)
        {
            bool possible_start_found = false;

            
            for (int i = 0; i < POS_DIM_Y; i++)
            {
                if (alignment == SearchPositionAlignment.Left)
                {
                    for (int e = 0; e < POS_DIM_X; e++)
                    {
                        if (_position_array[i, e] == 0)
                        {
                            // Wenn genügend 0er in der Zeile folgen ist der Startpunkt ein möglicher Kandidat
                            if (FollowingZeros(i, e, width, alignment))
                            {
                                possible_start_found = true;
                                for (int cur_spalte = e; cur_spalte < e + width; cur_spalte++)
                                {
                                    if (!DeeperZeros(i, cur_spalte, height))
                                    {
                                        possible_start_found = false;
                                        break;
                                    }
                                }
                                if (possible_start_found)
                                    return new Vector2f(e, i);
                            }
                        }
                    }
                }
                else
                {
                    for (int e = (POS_DIM_X-1); e >= 0; e--)
                    {
                        if (_position_array[i, e] == 0)
                        {
                            // Wenn genügend 0er in der Zeile folgen ist der Startpunkt ein möglicher Kandidat
                            if (FollowingZeros(i, e, width, alignment))
                            {
                                possible_start_found = true;
                                for (int cur_spalte = e; cur_spalte > e - width; cur_spalte--)
                                {
                                    if (!DeeperZeros(i, cur_spalte, height))
                                    {
                                        possible_start_found = false;
                                        break;
                                    }
                                }
                                if (possible_start_found)
                                    return new Vector2f(e-width, i);
                            }
                        }
                    }
                }
            }
            //Es konnte keine freie position gefunden werden. Zum Anzeigen wird diese ungültige Position zurückgegeben.
            return new Vector2f(-1, -1);
        }

        /// <summary>
        /// Bestimmt ob die folgenden Elemente im Positionsarray 0en sind oder nicht
        /// </summary>
        /// <param name="start_zeile">Zeilenindex des Positionsarray ab dem die Überprüfung stattfinden soll</param>
        /// <param name="start_spalte">Spaltenindex des Positionsarrays ab dem die Überprüfung stattfinden soll</param>
        /// <param name="width">Der Breitenwert der überprüft werden soll</param>
        /// <returns>True, wenn in der Zeilen nach dem übergebenen Startvektor nur 0en kommen. False sonst</returns>
        private bool FollowingZeros(int start_zeile, int start_spalte, int width, SearchPositionAlignment alignment)
        {
            if (alignment == SearchPositionAlignment.Left)
            {
                // Wenn die Breite des Elementes über den Index des Positionsarrays hinausgehen sollte, kann es nicht die notwendigen 0en geben
                if (start_spalte + width > POS_DIM_X)
                    return false;

                for (int i = start_spalte; i < start_spalte + width; i++)
                {
                    if (_position_array[start_zeile, i] != 0)
                        return false;
                }
            }
            else
            {
                if (start_spalte - width < 0)
                    return false;

                for (int i = start_spalte; i >= start_spalte - width; i--)
                {
                    if (_position_array[start_zeile, i] != 0)
                        return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Überprüft ob unter einer bestimmten Position im Positionsarray nur 0en vorkommen.
        /// Die Überprüfung erfolgt solange bis mindestens der height entsprechende viele Positionen überprüft wurden
        /// </summary>
        /// <param name="start_zeile">Zeilenindex im Positionsarray</param>
        /// <param name="start_spalte">Spaltenindex im Positionsarray</param>
        /// <param name="height">Höhe die angibt, wieviele 0en unter dem Startpunkt mindestens 0en sein sollen</param>
        /// <returns>true, wenn height 0en unter dem Startpunkt sind. False, sonst</returns>
        private bool DeeperZeros(int start_zeile, int start_spalte, int height)
        {
            if (start_zeile + height > POS_DIM_Y)
                return false;

            for (int i = start_zeile; i < start_zeile + height; i++)
            {
                if (_position_array[i, start_spalte] != 0)
                    return false;
            }

            return true;
        }

        /// <summary>
        /// Zurücksetzen des Positionsarrays.
        /// </summary>
        private void ClearPosArray()
        {
            POS_DIM_X = (int)ActualVisibleWidth;

            _position_array = new int[POS_DIM_Y, POS_DIM_X];
            for (int i = 0; i < POS_DIM_Y; i++)
            {
                for (int e = 0; e < POS_DIM_X; e++)
                {
                    // Gewisse Positionen sollen nicht verwendet werden. Daher wird hier initial der Platz als belegt markiert
                    if (i < SearchFreePositionTopOffset) //POS_TOP_OFFSET)
                        _position_array[i, e] = 1;
                    else
                        _position_array[i, e] = 0;
                }
            }
        }

        public void ResetAutoPositioning()
        {
            ClearPosArray();
            _layouted_elements.Clear();
        }

        #endregion

        public int GetOpenDialogCount()
        {
            if (_DialogWindowList == null)
                return 0;
            return _DialogWindowList.Count;
        }

        #region Custom control methods
        public void AddCustomControl(UIElement control)
        {
            if(!_custom_controls.Contains(control))
            {
                overlay_manger_custom_control_host.Children.Add(control);
                _custom_controls.Add(control);
                int zindex_ctrl = LayerManager.AdjustCustomControlZIndex();

                
                int zindex = Canvas.GetZIndex(overlay_manager_modal_canvas);
                if (zindex < zindex_ctrl)
                    Canvas.SetZIndex(overlay_manager_modal_canvas, zindex_ctrl+1);
            }
        }

        public void RemoveCustomControl(UIElement control)
        {
            if (_custom_controls.Contains(control))
            {
                overlay_manger_custom_control_host.Children.Remove(control);
                _custom_controls.Add(control);
            }
        }

        public bool ContainsCustomControl(UIElement control)
        {
            return _custom_controls.Contains(control);
        }

        internal List<UIElement> CustomControls
        {
            get { return _custom_controls; }
        }

        internal Canvas CustomControlHost
        {
            get { return overlay_manger_custom_control_host; }
        }
        #endregion

        #region Status Info
        public void ShowStatusInfo(string title, string text)
        {
            overlay_status_panel.ShowStatusInfo<OverlayTextStatusItem>(DateTime.Now.ToString(), title, text, 6000);
        }

        public T GetStatusItem<T>(string id) where T : OverlayStatusItem
        {
            return overlay_status_panel.GetStatusItem<T>(id);
        }
        /// <summary>
        /// zeigt eine status information in einem eigenen panel an
        /// </summary>
        /// <typeparam name="T">overlaytextstatusitem, overlayprogressstatusitem, overlayconfirmstatusitem</typeparam>
        /// <param name="id">identifiziert eindeutig das angezeigte item</param>
        /// <param name="title"></param>
        /// <param name="text"></param>
        /// <param name="autohide_in_msec">-1 ... Info Item wird nicht automatisch geschlossen</param>
        public void ShowStatusInfo<T>(string id, string title, string text, int autohide_in_msec) where T : OverlayStatusItem
        {
            overlay_status_panel.ShowStatusInfo<T>(id, title, text, autohide_in_msec);
        }

        public void HideStatusInfo<T>(string id) where T : OverlayStatusItem
        {
            overlay_status_panel.HideStatusInfo<T>(id);
        }

        public void HideAllStatusInfo()
        {
            overlay_status_panel.HideAllStatusInfo();
        }
        #endregion

        #region Blocking Status Info

        public OverlayBlockingProgressStatusInfo CurrentBlockingStatusInformation
        {
            get { return _blocking_status_info; }
        }
        #region Dispatcher Access Stubs
        public void ShowBlockingStatusInfo(string title, string subtitle)
        {
            if (this.Dispatcher.CheckAccess())
            {
                ExecuteShowBlockingStatusInfo(title, subtitle);
            }
            else
            {
                Dispatcher.Invoke(DispatcherPriority.Normal, (NoParas)delegate
                {
                    ExecuteShowBlockingStatusInfo(title, subtitle);
                });
            }
        }


        private void ClearBlockingStatusInfo()
        {
            if (this.Dispatcher.CheckAccess())
            {
                ExecuteClearBlockingStatusInfo();
            }
            else
            {
                Dispatcher.Invoke(DispatcherPriority.Normal, (NoParas)ExecuteClearBlockingStatusInfo);
            }
        }


        void _blocking_status_info_StatusControlHidden(object sender, EventArgs e)
        {
            ClearBlockingStatusInfo();
        }

        public void UpdateBlockingStatusInfo(double perc, string info)
        {
            if (_blocking_status_info == null)
                throw new Exception("cannot update blocking status info, no blocking status info visible, call ShowBlockingStatusInfo");

            if (this.Dispatcher.CheckAccess())
            {
                ExecuteUpdateBlockingStatusInfo(perc , info);
            }
            else
            {
                Dispatcher.Invoke(DispatcherPriority.Normal, (NoParas)delegate
                {
                    ExecuteUpdateBlockingStatusInfo(perc, info);
                });
            }
        }


        public void HideBlockingStatusInfo()
        {
            if (_blocking_status_info == null)
                return;

            if (this.Dispatcher.CheckAccess())
            {
                _blocking_status_info.StartHidingAnimation();
            }
            else
            {
                Dispatcher.Invoke(DispatcherPriority.Normal, (NoParas)delegate
                {
                    _blocking_status_info.StartHidingAnimation();
                });
            }
        }

        public bool IsBlockingStatusInfoVisible()
        {
            bool bRet = false;

            if (this.Dispatcher.CheckAccess())
            {
                bRet = (_blocking_status_info != null);
            }
            else
            {
                Dispatcher.Invoke(DispatcherPriority.Normal, (NoParas)delegate
                {
                    bRet = (_blocking_status_info != null);
                });
            }
            return bRet;
        }
        #endregion

        private void ExecuteUpdateBlockingStatusInfo(double perc, string info)
        {
            _blocking_status_info.ShowPercentage = true;
            _blocking_status_info.CurrentActionProgress = perc;
            _blocking_status_info.ShowCurrentAction = true;
            _blocking_status_info.CurrentAction = info;
        }


        private void ExecuteClearBlockingStatusInfo()
        {
            _blocking_status_info.StatusControlHidden -= _blocking_status_info_StatusControlHidden;
            RemoveFrameworkElement(_blocking_status_info);
            _blocking_status_info.Dispose();
            _blocking_status_info = null;
            EnableMouseClicks();
            if (BaseFrame != null)
                BaseFrame.SetDefaultAppCursor();
        }


        private void ExecuteShowBlockingStatusInfo(string title, string subtitle)
        {
            if (_blocking_status_info != null)
            {
                //do title update
                _blocking_status_info.InformationTitle = title;
                _blocking_status_info.SubInformationTitle = subtitle;
                _blocking_status_info.StopHidingAnimation();

                //XXX vielleicht hier das return weglöschen
                return;
            }

            _blocking_status_info = new OverlayBlockingProgressStatusInfo();
            _blocking_status_info.StatusControlHidden += new EventHandler(_blocking_status_info_StatusControlHidden);
            Point end_pos = new Point(650, this.ActualHeight / 2);
            Canvas.SetTop(_blocking_status_info, end_pos.Y);
            Canvas.SetZIndex(_blocking_status_info, int.MaxValue);

            _blocking_status_info.InformationTitle = title;
            _blocking_status_info.SubInformationTitle = subtitle;
            _blocking_status_info.EnableLongOperationIndicator = true;
            _blocking_status_info.TimeIndicatorVisibility = Visibility.Collapsed;

            AddFrameworkElement(_blocking_status_info);
            DisableMouseClicks();

            if (BaseFrame != null)
                BaseFrame.SetAppCursor(Cursors.Wait);
        }
        #endregion

        #region constants
        #endregion

        #region Visible Dialog Area
        public double ActualVisibleWidth
        {
            get
            {
                double width = overlay_manager_overall_canvas.ActualWidth;

                foreach (FrameworkElement decrease_elem in DecreasingVisibleLeftPositionElements)
                    width -= decrease_elem.ActualWidth;
                foreach (FrameworkElement decreasingVisibleRightPositionElement in DecreasingVisibleRightPositionElements)
                    width -= decreasingVisibleRightPositionElement.ActualWidth;

                return width;
            }
        }

        public double ActualVisibleHeight
        {
            get
            {
                double height = overlay_manager_overall_canvas.ActualHeight;

                foreach (FrameworkElement decrease_elem in DecreasingVisibleTopPositionElements)
                    height -= decrease_elem.ActualHeight;
                foreach (FrameworkElement decreasingVisibleBottomPositionElement in DecreasingVisibleBottomPositionElements)
                    height -= decreasingVisibleBottomPositionElement.ActualHeight;
                
                return height;
            }
        }

        public Point AcutalVisibleTopLeft
        {
            get
            {
                Point topleft = new Point();
                foreach (FrameworkElement decreasingVisibleTopPositionElement in DecreasingVisibleTopPositionElements)
                    topleft.Y += decreasingVisibleTopPositionElement.ActualHeight;
                foreach (FrameworkElement decreasingVisibleLeftPositionElement in DecreasingVisibleLeftPositionElements)
                    topleft.X += decreasingVisibleLeftPositionElement.ActualWidth;

                return topleft;
            }
        }

        public List<FrameworkElement> DecreasingVisibleTopPositionElements = new List<FrameworkElement>();
        public List<FrameworkElement> DecreasingVisibleLeftPositionElements = new List<FrameworkElement>();
        public List<FrameworkElement> DecreasingVisibleBottomPositionElements = new List<FrameworkElement>();
        public List<FrameworkElement> DecreasingVisibleRightPositionElements = new List<FrameworkElement>();
        #endregion

        #region Attribs
        private List<OverlayDialog> _layouted_elements = new List<OverlayDialog>();
        private int POS_DIM_Y = 1000;
        private int POS_DIM_X = 1000;
        private const int POS_TOP_OFFSET = 25;
        private int[,] _position_array = new int[0, 0];

        private bool _animate_all_modal_dialogs = true;

        private bool _overlay_manager_loaded = false;
        private List<OverlayDialog> _running_minimizing_animations = new List<OverlayDialog>();

        private List<KeyValuePair<ContentControl, OverlayDialogState>> _delayed_show = new List<KeyValuePair<ContentControl, OverlayDialogState>>();

        private List<OverlayWindowMeta> _DialogWindowList;

        private OverlayWriteDebugMessageDelegate _debugWriter = null;
        private Brush _old_canvas_brush = null;

        private bool _canvas_mousedown = false;
        private bool _close_buttonless_activedialog_on_canvas_click = true;

        private Stack<MessageBoxResultDelegate> _message_box_delegates = new Stack<MessageBoxResultDelegate>();

        private QueuedBackgroundWorker _background_initializer = null;

        private OverlayLayerManager _layer_manager;

        private OverlayBlockingProgressStatusInfo _status_information_provider = null;

        protected delegate void NoParas();

        private Dictionary<Key, OverlayDialog> _key_down_origins = new Dictionary<Key, OverlayDialog>();

        private List<UIElement> _custom_controls = new List<UIElement>();

        #region Dependency properties
        public static readonly DependencyProperty CloseButtonStyleProperty;
        public static readonly DependencyProperty PrintButtonStyleProperty;
        public static readonly DependencyProperty HelpButtonStyleProperty;
        public static readonly DependencyProperty MinimizeButtonStyleProperty;
        public static readonly DependencyProperty MaximizeButtonStyleProperty;
        public static readonly DependencyProperty NormalizeButtonStyleProperty;
        public static readonly DependencyProperty DialogBorderStyleProperty;
        public static readonly DependencyProperty DialogContentBorderStyleProperty;

        public static readonly DependencyProperty DialogScreenshotStyleProperty;

        public static readonly DependencyProperty ModalModeProperty;
        internal static readonly DependencyPropertyKey ModalModePropertyKey;

        public static readonly DependencyProperty OverlayBackgroundBrushProperty;


        public Brush ResizeGripBrush
        {
            get { return (Brush)GetValue(ResizeGripBrushProperty); }
            set { SetValue(ResizeGripBrushProperty, value); }
        }
        public static readonly DependencyProperty ResizeGripBrushProperty = DependencyProperty.Register("ResizeGripBrush", typeof(Brush), typeof(OverlayManager), new UIPropertyMetadata(Brushes.White));


        public Style StatusSmallButtonStyle
        {
            get { return (Style)GetValue(StatusSmallButtonStyleProperty); }
            set { SetValue(StatusSmallButtonStyleProperty, value); }
        }
        public static readonly DependencyProperty StatusSmallButtonStyleProperty = DependencyProperty.Register("StatusSmallButtonStyle", typeof(Style), typeof(OverlayManager), new UIPropertyMetadata(null));

        public Style StatusConfirmButtonStyle
        {
            get { return (Style)GetValue(StatusConfirmButtonStyleProperty); }
            set { SetValue(StatusConfirmButtonStyleProperty, value); }
        }
        public static readonly DependencyProperty StatusConfirmButtonStyleProperty = DependencyProperty.Register("StatusConfirmButtonStyle", typeof(Style), typeof(OverlayManager), new UIPropertyMetadata(null));

        public WindowTaskBar WindowTaskBar
        {
            get
            {
            
                // TryFindParentGeneric sucht einen VisualParent und achtet dabei auf den generic type
                // der mitgeliefert wird. Die Typenparameter des generic types werden NICHT beachtet !!!
                // wenn der genaue Typ gesucht werden soll, dann die Methode TryFindParent  verwenden!!
                DependencyObject diagram_host = UIHelper.TryFindParentGeneric(this, typeof(BaseFrameBase<SubMenuPanel, WindowTaskBar, WindowTitleBar>));
                Type t = diagram_host.GetType();
                PropertyInfo p_info = t.GetProperty("TaskBar");
                if(p_info != null)
                {
                    object taskbar = p_info.GetValue(diagram_host, null);

                    if (taskbar != null && taskbar.GetType() == typeof(Logicx.WpfUtility.WindowManagement.WindowTaskBar))
                        return taskbar as WindowTaskBar;
                }
              
                return null;
            }
        }

        public TaskbarPanel TaskbarPanel
        { get; set; }


        public double ParentWidth
        {
            get { return (double)GetValue(ParentWidthProperty); }
            set { SetValue(ParentWidthProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ParentWidth.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ParentWidthProperty =
            DependencyProperty.Register("ParentWidth", typeof(double), typeof(OverlayManager), new UIPropertyMetadata(0.0d));



        public Style DefaultOverlayDialogTitleStyle
        {
            get { return (Style)GetValue(DefaultOverlayDialogTitleStyleProperty); }
            set { SetValue(DefaultOverlayDialogTitleStyleProperty, value); }
        }
        public static readonly DependencyProperty DefaultOverlayDialogTitleStyleProperty = DependencyProperty.Register("DefaultOverlayDialogTitleStyle", typeof(Style), typeof(OverlayManager), new UIPropertyMetadata(null));


        
        public double ParentHeight
        {
            get { return (double)GetValue(ParentHeightProperty); }
            set { SetValue(ParentHeightProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ParentHeight.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ParentHeightProperty =
            DependencyProperty.Register("ParentHeight", typeof(double), typeof(OverlayManager), new UIPropertyMetadata(0.0d));


        public static readonly DependencyProperty StatusFilledBackgroundBrushProperty;
        public static readonly DependencyProperty StatusColorBgBrushBrushProperty;
        public static readonly DependencyProperty StatusTextBrushProperty;
        public static readonly DependencyProperty StatusLongOperationIndicatorTextBrushProperty;


        public static readonly DependencyProperty RightMouseClickEventReceiverProperty;
        #endregion

        private bool _stylus_origin = false;

        private bool _mouse_over_dialog;
        public event EventHandler MouseOverDialogStateChanged;
        public event EventHandler<WindowStateChangeArgs> WindowStateChanged;
        public event EventHandler<WindowStateChangeingArgs> WindowStateChanging;

        protected OverlayBlockingProgressStatusInfo _blocking_status_info;
        #endregion
    }
}

