﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using Microsoft.Windows.Controls;
using Microsoft.Windows.Controls.Primitives;


namespace Logicx.WpfUtility.WpfHelpers
{
    public class DataGridHelper
    {
        public static DataGridRow GetCellRow(DataGridCell cell)
        {
            DependencyObject dep = cell;

            // navigate further up the tree
            while ((dep != null) && !(dep is DataGridRow))
            {
                dep = UIHelper.GetParentObject(dep);
            }

            return dep as DataGridRow;
        }

        public static int FindRowIndex(DataGridRow row)
        {
            DataGrid dataGrid =
                GetDataGridFromRow(row);

            int index = dataGrid.ItemContainerGenerator.
                IndexFromContainer(row);

            return index;
        }

        public static DataGrid GetDataGridFromRow(DataGridRow row)
        {
            return ItemsControl.ItemsControlFromItemContainer(row)
                   as DataGrid;
        }

        public static DataGridCell GetCell(DataGrid grid, int row, int column)
        {
            DataGridRow rowContainer = GetRow(grid, row);

            if (rowContainer != null)
            {
                DataGridCellsPresenter presenter = UIHelper.FindVisualChildByType<DataGridCellsPresenter>(rowContainer);

                // try to get the cell but it may possibly be virtualized
                DataGridCell cell = (DataGridCell)presenter.ItemContainerGenerator.ContainerFromIndex(column);
                if (cell == null)
                {
                    // now try to bring into view and retreive the cell
                    grid.ScrollIntoView(rowContainer, grid.Columns[column]);
                    cell = (DataGridCell)presenter.ItemContainerGenerator.ContainerFromIndex(column);
                }
                return cell;
            }
            return null;
        }

        public static DataGridCell GetCell(DataGridRow row, int column)
        {
            if (row == null)
                return null;

            DataGridCellsPresenter presenter = UIHelper.FindVisualChildByType<DataGridCellsPresenter>(row);

            // try to get the cell but it may possibly be virtualized
            DataGridCell cell = (DataGridCell)presenter.ItemContainerGenerator.ContainerFromIndex(column);
            if (cell == null)
            {
                DataGrid grid = GetDataGridFromRow(row);

                // now try to bring into view and retreive the cell
                grid.ScrollIntoView(row, grid.Columns[column]);
                cell = (DataGridCell)presenter.ItemContainerGenerator.ContainerFromIndex(column);
            }

            return cell;
        }

        public static DataGridRow GetRow(DataGrid grid, int index)
        {
            DataGridRow row = (DataGridRow)grid.ItemContainerGenerator.ContainerFromIndex(index);
            if (row == null)
            {
                // may be virtualized, bring into view and try again
                grid.ScrollIntoView(grid.Items[index]);
                row = (DataGridRow)grid.ItemContainerGenerator.ContainerFromIndex(index);
            }
            return row;
        }
    }
}
