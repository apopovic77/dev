﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BankLib.Data;
using BankLib.Gruppe;

namespace BankOverviewApp
{


    /// <summary>
    /// Interaction logic for Gruppenzuordnung.xaml
    /// </summary>
    public partial class Gruppenzuordnung : UserControl
    {
        public Gruppenzuordnung()
        {
            InitializeComponent();

            LoadRgiFromFile();

            var q = from b in Buchungen.AlleBuchungen
                select new {b.UmsatzType, b.BuchungsType};

            var umsatz_types = q.Distinct().OrderBy(b=>b.BuchungsType).ThenBy(b=>b.UmsatzType).ToList();
            foreach (var umsatz_type in umsatz_types)
            {
                StackPanel sp = new StackPanel();
                sp.Orientation = Orientation.Horizontal;

                TextBlock tb0 = new TextBlock();
                tb0.Width = 150;
                tb0.Text = umsatz_type.BuchungsType;

                TextBlock tb = new TextBlock();
                tb.Width = 300;
                tb.Text = umsatz_type.UmsatzType;
                tb.ToolTip = Buchungen.AlleBuchungen.Count(b => b.UmsatzType == umsatz_type.UmsatzType) + " mal\n\n" +
                             Buchungen.AlleBuchungen.Where(b => b.UmsatzType == umsatz_type.UmsatzType).First().TooltipText;

                TextBox tbox = new TextBox();
                tbox.Width = 120;
                tbox.LostFocus += tbox_LostFocus;
                RawGroupInfo rgi = GetRawGroupInfo(umsatz_type.BuchungsType, umsatz_type.UmsatzType);
                if (rgi != null)
                    tbox.Text = rgi.Name;
                tbox.TextChanged += tbox_TextChanged;

                ComboBox comptype_cb = GetCbx();
                comptype_cb.Width = 80;
                if (rgi != null)
                    comptype_cb.SelectedValue = rgi.CompareType;

                comptype_cb.SelectionChanged += comptype_cb_SelectionChanged;

                TextBox compare_tbox = new TextBox();
                compare_tbox.Width = 120;
                if(rgi != null)
                    compare_tbox.Text = rgi.SearchString;
                compare_tbox.TextChanged += tbox_TextChanged;
                compare_tbox.LostFocus += tbox_LostFocus;


                sp.Children.Add(tb0);
                sp.Children.Add(tb);
                sp.Children.Add(tbox);
                sp.Children.Add(comptype_cb);
                sp.Children.Add(compare_tbox);

                stack.Children.Add(sp);
            }

            UpdateGruppenLv();
            
        }

        private RawGroupInfo GetRawGroupInfo(string buchungs_type, string umsatz_type)
        {
            string key = RawGroupInfo.GetKey(buchungs_type, umsatz_type);

            if (!RawGroupInfos.ContainsKey(key))
                return null;

            return RawGroupInfos[key];
        }

        private RawGroupInfo GetRgi(StackPanel parent_stack)
        {
            RawGroupInfo rgi = new RawGroupInfo();
            rgi.BuchungsType = ((TextBlock)parent_stack.Children[0]).Text;
            rgi.UmsatzType = ((TextBlock)parent_stack.Children[1]).Text;
            rgi.Name = ((TextBox)parent_stack.Children[2]).Text;
            rgi.CompareType = (string)((ComboBox)parent_stack.Children[3]).SelectedValue;
            rgi.SearchString = ((TextBox)parent_stack.Children[4]).Text;
            return rgi;
        }


        void tbox_TextChanged(object sender, TextChangedEventArgs e)
        {
            RawGroupInfo rgi = GetRgi((StackPanel)((TextBox)sender).Parent);

            if (string.IsNullOrEmpty(rgi.Name))
            {
                if (RawGroupInfos.ContainsKey(rgi.Key))
                    RawGroupInfos.Remove(rgi.Key);
            }
            else
            {
                if (!RawGroupInfos.ContainsKey(rgi.Key))
                    RawGroupInfos.Add(rgi.Key, rgi);
                else
                    RawGroupInfos[rgi.Key] = rgi;
            }
        }

        private ComboBox GetCbx()
        {
            List<string> zuotype = typeof (BuchungsGruppenZuordnungsTyp).GetMembers().Where(b=>b.DeclaringType != null && b.DeclaringType.Name == "BuchungsGruppenZuordnungsTyp" && b.Name != "value__").Select(b => b.Name).ToList();
            zuotype.Insert(0,"");
            ComboBox cbx = new ComboBox();
            cbx.ItemsSource = zuotype;
            return cbx;
        }

        private void UpdateGruppenLv()
        {
            gruppen_lv.ItemsSource = RawGroupInfos.Select(g => g.Value.Name).Distinct().OrderBy(b=>b).ToList();
        }

        public List<BgZuo> GetBgZuos(string name)
        {
            List<BgZuo> zuos = new List<BgZuo>();
            foreach (StackPanel parent_stack in stack.Children)
            {
                RawGroupInfo rgi = GetRgi(parent_stack);
                
                if (!string.IsNullOrEmpty(rgi.Name))
                {
                    if (name == rgi.Name)
                    {
                        BgZuo bg_zuo = BgZuoFactory.CreateBgZuo(rgi);
                        zuos.Add(bg_zuo);
                    }
                }
            }
            return zuos;
        }



        void comptype_cb_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SaveRgiToFile();
            UpdateGruppenLv();
        }

        void tbox_LostFocus(object sender, RoutedEventArgs e)
        {
            SaveRgiToFile();
            UpdateGruppenLv();
        }


        public int SaveRgiToFile()
        {
            return SaveRgiToFile(RawGroupInfos, PathToRawGruppenFile);
        }

        public int SaveRgiToFile(Dictionary<string, RawGroupInfo> gruppen, string path_to_gruppen_file)
        {
            if (gruppen.Count == 0)
                return 0;

            if (File.Exists(path_to_gruppen_file))
                File.Delete(path_to_gruppen_file);

            using (BinaryWriter writer = new BinaryWriter(File.Open(path_to_gruppen_file, FileMode.CreateNew)))
            {
                writer.Write(gruppen.Count);
                foreach (RawGroupInfo gruppe in gruppen.Values)
                {
                    gruppe.WriteToStream(writer);
                }
            }
            return gruppen.Count;
        }

        public int LoadRgiFromFile()
        {
            RawGroupInfos = new Dictionary<string, RawGroupInfo>();
            return LoadRgiFromFile(PathToRawGruppenFile, RawGroupInfos);
        }

        public int LoadRgiFromFile(string path_to_gruppen_file, Dictionary<string, RawGroupInfo> gruppen)
        {
            if (!File.Exists(path_to_gruppen_file))
                return -1;

            using (BinaryReader reader = new BinaryReader(File.Open(path_to_gruppen_file, FileMode.Open)))
            {
                int b_count = reader.ReadInt32();
                for (int i = 0; i < b_count; i++)
                {
                    RawGroupInfo b = new RawGroupInfo();
                    b.LoadFromStream(reader);
                    string key = RawGroupInfo.GetKey(b);
                    gruppen.Add(key, b);
                }
                return b_count;
            }
        }


        public Dictionary<string, RawGroupInfo> RawGroupInfos = new Dictionary<string, RawGroupInfo>();
        public const string PathToRawGruppenFile = AppHelper.PathToBankData + "RawGruppenInfo.dat";
    }
}
