using System;
using System.Collections;
using System.Drawing;
using MathLib;

namespace GeoLib
{
	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	public class Polygon : Line
	{

		protected bool autoClose = false;
		protected bool autoOpen = false;

		public int EdgesCount{
			get{
				return vertices.Count-1;
			}
		}

		/// <summary>
		/// uberpr�ft ob es sich bei dem polygon um ein polygon mit �berschneidungen handelt
		/// </summary>
		/// <param name="p"></param>
		/// <returns></returns>
		public static bool WithIntersection(Polygon p){
			//polygon �berschneidungs validation
			for(int i=0;i<p.Length-1;i++)
			{
				Vector2d v1_e1 = p[i];
				Vector2d v2_e1 = p[i+1];
				Edge e1 = new Edge(v1_e1,v2_e1);

				for(int j=0;j<p.Length-1;j++)
				{
					if(i!=j && j!=i-1 && j!=i+1)
					{
						Vector2d v1_e2 = p[j];
						Vector2d v2_e2 = p[j+1];
						Edge e2 = new Edge(v1_e2,v2_e2);
						
						Vector2d cutpoint;
						if(MathLib.LinearAlgebra.GetCutPointInSegment(e1,e2,out cutpoint))
							return true;
					}
				}
			}	
			return false;
		}

		public Polygon(){}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="closed">true - if the poly should be closed automatically
		/// which means that the last elem = first elem</param>
		public Polygon(bool autoClose,bool autoOpen){
			this.autoClose = autoClose;
			this.autoOpen = autoOpen;
		}
		public Polygon(string polygonWKT){
			WKT = polygonWKT;
		}
		public Polygon(Vector2d[] v_arr){
			ResetPolygon(v_arr);
		}

		public Edge GetEdge(int i)
		{
			return new Edge((Vector2d)vertices[i],(Vector2d)vertices[i+1]);
		}


		public Edge GetNextEdge(int active_edge_index)
		{
			active_edge_index++;
			if(active_edge_index >= this.Length-1)
				active_edge_index = 0;
			Edge e = new Edge((Vector2d)vertices[active_edge_index],(Vector2d)vertices[active_edge_index+1]);
			return e;
		}
		public Edge GetPrevEdge(int active_edge_index)
		{
			active_edge_index--;
			if(active_edge_index < 0)
				active_edge_index = this.Length-2;
			Edge e = new Edge((Vector2d)vertices[active_edge_index],(Vector2d)vertices[active_edge_index+1]);
			return e;
		}

		public Edge GetNextEdge(int active_edge_index, Direction dir,ref int beg_index)
		{
			if(dir == Direction.Forward)
			{
				beg_index++;
				if(beg_index >= this.Length-1)
					beg_index = 0;
			}
			else
			{
				beg_index--;
				if(beg_index < 0)
					beg_index = this.Length-2;
			}
				
			Edge e = new Edge((Vector2d)vertices[beg_index],(Vector2d)vertices[beg_index+1]);
			return e;
		}



		public void IntersectWithRectangle(GeoLib.Rectangle r){
			ArrayList cutpoints = new ArrayList();
			ArrayList vertices_to_delete = new ArrayList();

			Polygon p = r.GetPolygon();

			//1. schau ob im alten polygon vertexe im rechteck liegen
			for(int i=0;i<vertices.Count;i++)
				if(p.PointInPolygon((Vector2d)vertices[i]))	
					vertices_to_delete.Add(vertices[i]);


			//2. schau ob die neuen vertexte im polygon liegen
			//get vectors of rectangle from array
			Vector2d v1 = p[0];
			Vector2d v2 = p[1];
			Vector2d v3 = p[2];
			Vector2d v4 = p[3];

			//vector in polgon
			bool[] v_iP = new bool[4];
			v_iP[0] = PointInPolygon(v1);
			v_iP[1] = PointInPolygon(v2);
			v_iP[2] = PointInPolygon(v3);
			v_iP[3] = PointInPolygon(v4);

			//oh
			Edge[] edges = GetEdges();
			for(int i=0;i<4;i++)
			{
				//f�ge die punkte ein dei ausserhalb liegen
				if(!v_iP[i])
					Add(p[i]);

				Edge e_tocut;
				if(i<3)	e_tocut = new Edge(p[i],p[i+1]);
				else    e_tocut = new Edge(p[3],p[0]);
				Vector2d[] cp_arr = GeoAlgorithms.GetCutpoints(edges,e_tocut);
				cutpoints.Add(cp_arr);
			}

			//f�ge cutpoints ein
			for(int i=0;i<cutpoints.Count;i++){
				Vector2d[] cp_arr = (Vector2d[])cutpoints[i];
				Add(cp_arr);
			}

			//l�sche die zu l�schenden vertices
			for(int i=0;i<vertices_to_delete.Count;i++)
				vertices.Remove(vertices_to_delete[i]);

			//generiere die vektor ordnung neu
			CreateShortestClosedPath();
		}


		private class AngleSortAsc : IComparer  
		{
			int IComparer.Compare( Object x1, Object x2 )  
			{
				object[] oarr1 = (object[])x1;
				object[] oarr2 = (object[])x2;

				double X1 = (double)oarr1[0];
				double X2 = (double)oarr2[0];
				if(X1 < X2)
					return -1;
				else if(X1 == X2)
					return 0;
				else
					return 1;
			}
		}
		protected ArrayList BuildSortedAngleList(Vector2d anchor,ArrayList list_to_sort)
		{
			//create temporary arraylist holding all vertices in polygon
			ArrayList v_al = new ArrayList();

			//calculate angle in fraction to anchor and build arraylist
			for(int i=0;i<list_to_sort.Count;i++)
			{
				Vector2d dirVec = ((Vector2d)list_to_sort[i])-anchor;
				double alpha = dirVec.GetAngle();
				object[] oarr = new object[2];
				oarr[0] = alpha;
				oarr[1] = list_to_sort[i];
				v_al.Add(oarr);
			}

			//sort angle list
			v_al.Sort(new AngleSortAsc());

			//arraylist
			return v_al;
		}
		public void CreateShortestClosedPath()
		{
			//create temporary arraylist holding all vertices in polygon
			ArrayList v_al;
			
			//anchor per default first elem
			Vector2d anchor = (Vector2d)vertices[0];

			//hol dir den anker --> kleinster y wert
			for(int i=1;i<vertices.Count;i++)
				if(anchor.Y>((Vector2d)vertices[i]).Y)
					anchor = (Vector2d)vertices[i];
			
			//l�sche das anchor elem
			vertices.Remove(anchor);

			//calculate angle in fraction to anchor and build arraylist
			v_al = BuildSortedAngleList(anchor,vertices);

			//start to build new polygon
			vertices.Clear();
			vertices.Add(anchor);

			for(int i=0;i<v_al.Count;i++)
				vertices.Add(((object[])v_al[i])[1]);

		}
//		private class SortByAngleToAnchor : IComparer  
//		{
//			int IComparer.Compare( Object x1, Object x2 )  
//			{
//				Vector v1 = (Vector)x1;
//				Vector v2 = (Vector)x2;
//
//				Vector dirVec = v1-anchor;
//				double X1 = dirVec.GetAngle();
//				dirVec = v2-anchor;
//				double X2 = dirVec.GetAngle();
//
//				if(X1 < X2)
//					return -1;
//				else if(X1 == X2)
//					return 0;
//				else
//					return 1;
//			}
//		}
//		protected Vector anchor;
//		public void CreateShortestClosedPath()
//		{
//			//create temporary arraylist holding all vertices in polygon
//			ArrayList v_al;
//			
//			//anchor per default first elem
//			anchor = (Vector)vertices[0];
//
//			//hol dir den anker --> kleinster y wert
//			for(int i=1;i<vertices.Count;i++)
//				if(anchor.Y>((Vector)vertices[1]).Y)
//					anchor = (Vector)vertices[i];
//			
//			//l�sche das anchor elem
//			vertices.Remove(anchor);
//
//			//sortiere die vertexte nach winkel zum einem ancker --> create shortest path
//			vertices.Sort(new SortByAngleToAnchor());
//
//			//f�ge das anchor element ein
//			vertices.Insert(0,anchor);
//		}



		protected override string CreateLineStringT()
		{
			string s = base.CreateLineStringT();

			//close or leave poly open
			if(autoClose){
				Vector2d v_first = (Vector2d)vertices[0];
				Vector2d v_last = (Vector2d)vertices[vertices.Count-1];
				if(v_first != v_last)
					s += ","+v_first.X+" "+v_first.Y;
			}else if(autoOpen){
				Vector2d v_first = (Vector2d)vertices[0];
				Vector2d v_last = (Vector2d)vertices[vertices.Count-1];
				if(v_first == v_last)
					s = s.Substring(0,s.LastIndexOf(",")-1);
			}
			//return serialized poly
			return s;
		}

		public override string WKT{
			 get{
				//return a serialized poly surrounded with the WKT format
				string polyWKT = "Polygon((";
				polyWKT += CreateLineStringT();
				polyWKT += "))";
				return polyWKT;
			}
		}


		public bool PointInPolygon(double x, double y){
			CreateArray();
			bool c = false;
			for (int i = 0, j = vertices_arr.Length-1; i < vertices_arr.Length; j = i++) {
				if ((((vertices_arr[i].Y <= y) && (y < vertices_arr[j].Y)) || ((vertices_arr[j].Y <= y) && (y < vertices_arr[i].Y))) && (x < (vertices_arr[j].X - vertices_arr[i].X) * (y - vertices_arr[i].Y) / (vertices_arr[j].Y - vertices_arr[i].Y) + vertices_arr[i].X))
					c = !c;
			}
			return c;
		}

		public bool PointInPolygon(Vector2d v){
			return PointInPolygon(v.X,v.Y);
		}

		public Vector2d[] ToArray(){
			CreateArray();
			return vertices_arr;
		}

//		private static bool PointInPolygon2(Vector p, Vector[] polygon){
//			int i;
//			double angle=0;
//			int n = polygon.Length;
//			Vector p1 = new Vector(0,0);
//			Vector p2 = new Vector(0,0);
//
//			for (i=0;i<n;i++) {
//				p1.X = polygon[i].X - p.X;
//				p1.Y  = polygon[i].Y - p.Y;
//				p2.X = polygon[(i+1)%n].X - p.X;
//				p2.Y  = polygon[(i+1)%n].Y - p.Y;
//				angle += Angle2D(p1.X,p1.Y,p2.X,p2.Y);
//			}
//
//			if (Math.Abs(angle) < Math.PI)
//				return false;
//			else
//				return true;
//		}
	}
}
