﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Utility
{
    public class SimpleCacheManager
    {
        public bool Contains(string key)
        {
            return _cache_dictionary.ContainsKey(key);
        }

        public void Set(string key, object obj)
        {
            _cache_dictionary.Add(key, obj);
        }

        public object Get(string key)
        {
            return _cache_dictionary[key];
        }

        public void Clear()
        {
            _cache_dictionary.Clear();
        }

        private Dictionary<string, object> _cache_dictionary = new Dictionary<string, object>();
    }
}
