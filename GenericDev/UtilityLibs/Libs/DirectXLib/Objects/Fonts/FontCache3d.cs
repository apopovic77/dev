using System;
using System.Collections.Generic;
using System.Text;
using D3d = Microsoft.DirectX.Direct3D;
using System.Drawing;
using GeoLib;
using System.Collections;
using MathLib;
using Logicx.DirectX.Objects;
using Logicx.DirectX.Engine;

namespace Logicx.DirectX.Objects.Fonts
{
    public enum FontSize3d
    {
        Smallest = 1,
        Small = 2,
        Medium = 3,
        Big = 4 ,
        Supersize = 5,
        SmallestBold = 6,
        SmallBold = 7,
        MediumBold = 8,
        BigBold = 9,
        SupersizeBold = 10
    }

    /// <summary>
    /// diese klasse wird nur als render test verwendet 
    /// </summary>
    public class FontCache3d : Object3d
    {
        public FontCache3d()
        {
            ClearCache();
        }

        #region Initialization & Disposition
        public override void Init()
        {

            m_sprite = new D3d.Sprite(Engine3d.Device3d);
            m_font_2d_smallest = new D3d.Font(Engine3d.Device3d, m_font_height_smallest, m_font_width_smallest, Microsoft.DirectX.Direct3D.FontWeight.Normal, 5, false, Microsoft.DirectX.Direct3D.CharacterSet.Default, Microsoft.DirectX.Direct3D.Precision.Default, Microsoft.DirectX.Direct3D.FontQuality.ClearType, Microsoft.DirectX.Direct3D.PitchAndFamily.DefaultPitch, "Arial");
            m_font_2d_small = new D3d.Font(Engine3d.Device3d, m_font_height_small, m_font_width_small, Microsoft.DirectX.Direct3D.FontWeight.Normal, 5, false, Microsoft.DirectX.Direct3D.CharacterSet.Default, Microsoft.DirectX.Direct3D.Precision.Default, Microsoft.DirectX.Direct3D.FontQuality.ClearType, Microsoft.DirectX.Direct3D.PitchAndFamily.DefaultPitch, "Arial");
            m_font_2d_medium = new D3d.Font(Engine3d.Device3d, m_font_height_medium, m_font_width_medium, Microsoft.DirectX.Direct3D.FontWeight.Normal, 5, false, Microsoft.DirectX.Direct3D.CharacterSet.Default, Microsoft.DirectX.Direct3D.Precision.Default, Microsoft.DirectX.Direct3D.FontQuality.ClearType, Microsoft.DirectX.Direct3D.PitchAndFamily.DefaultPitch, "Arial");
            m_font_2d_big = new D3d.Font(Engine3d.Device3d, m_font_height_big, m_font_width_big, Microsoft.DirectX.Direct3D.FontWeight.Normal, 5, false, Microsoft.DirectX.Direct3D.CharacterSet.Default, Microsoft.DirectX.Direct3D.Precision.Default, Microsoft.DirectX.Direct3D.FontQuality.ClearType, Microsoft.DirectX.Direct3D.PitchAndFamily.DefaultPitch, "Arial");
            m_font_2d_supersize = new D3d.Font(Engine3d.Device3d, m_font_height_supersize, m_font_width_supersize, Microsoft.DirectX.Direct3D.FontWeight.Normal, 5, false, Microsoft.DirectX.Direct3D.CharacterSet.Default, Microsoft.DirectX.Direct3D.Precision.Default, Microsoft.DirectX.Direct3D.FontQuality.ClearType, Microsoft.DirectX.Direct3D.PitchAndFamily.DefaultPitch, "Arial");

            m_font_2d_smallest_bold = new D3d.Font(Engine3d.Device3d, m_font_height_smallest, m_font_width_smallest_bold, Microsoft.DirectX.Direct3D.FontWeight.Bold, 5, false, Microsoft.DirectX.Direct3D.CharacterSet.Default, Microsoft.DirectX.Direct3D.Precision.Default, Microsoft.DirectX.Direct3D.FontQuality.ClearType, Microsoft.DirectX.Direct3D.PitchAndFamily.DefaultPitch, "Arial");
            m_font_2d_small_bold = new D3d.Font(Engine3d.Device3d, m_font_height_small, m_font_width_small_bold, Microsoft.DirectX.Direct3D.FontWeight.Bold, 5, false, Microsoft.DirectX.Direct3D.CharacterSet.Default, Microsoft.DirectX.Direct3D.Precision.Default, Microsoft.DirectX.Direct3D.FontQuality.ClearType, Microsoft.DirectX.Direct3D.PitchAndFamily.DefaultPitch, "Arial");
            m_font_2d_medium_bold = new D3d.Font(Engine3d.Device3d, m_font_height_medium, m_font_width_medium_bold, Microsoft.DirectX.Direct3D.FontWeight.Bold, 5, false, Microsoft.DirectX.Direct3D.CharacterSet.Default, Microsoft.DirectX.Direct3D.Precision.Default, Microsoft.DirectX.Direct3D.FontQuality.ClearType, Microsoft.DirectX.Direct3D.PitchAndFamily.DefaultPitch, "Arial");
            m_font_2d_big_bold = new D3d.Font(Engine3d.Device3d, m_font_height_big, m_font_width_big_bold, Microsoft.DirectX.Direct3D.FontWeight.Bold, 5, false, Microsoft.DirectX.Direct3D.CharacterSet.Default, Microsoft.DirectX.Direct3D.Precision.Default, Microsoft.DirectX.Direct3D.FontQuality.ClearType, Microsoft.DirectX.Direct3D.PitchAndFamily.DefaultPitch, "Arial");
            m_font_2d_supersize_bold = new D3d.Font(Engine3d.Device3d, m_font_height_supersize, m_font_width_supersize_bold, Microsoft.DirectX.Direct3D.FontWeight.Bold, 5, false, Microsoft.DirectX.Direct3D.CharacterSet.Default, Microsoft.DirectX.Direct3D.Precision.Default, Microsoft.DirectX.Direct3D.FontQuality.ClearType, Microsoft.DirectX.Direct3D.PitchAndFamily.DefaultPitch, "Arial");

        }

        public override void Dispose()
        {

            if (m_sprite != null)
            {
                m_sprite.Dispose();
            }

            if (m_font_2d_smallest != null)
                m_font_2d_smallest.Dispose();
            if (m_font_2d_small != null)
                m_font_2d_small.Dispose();// = new D3d.Font(Engine3d.Device3d, m_font_height_small, m_font_width_small, Microsoft.DirectX.Direct3D.FontWeight.Normal, 5, false, Microsoft.DirectX.Direct3D.CharacterSet.Default, Microsoft.DirectX.Direct3D.Precision.Default, Microsoft.DirectX.Direct3D.FontQuality.ClearType, Microsoft.DirectX.Direct3D.PitchAndFamily.DefaultPitch, "Arial");
            if (m_font_2d_medium != null)
                m_font_2d_medium.Dispose();// = new D3d.Font(Engine3d.Device3d, m_font_height_medium, m_font_width_medium, Microsoft.DirectX.Direct3D.FontWeight.Normal, 5, false, Microsoft.DirectX.Direct3D.CharacterSet.Default, Microsoft.DirectX.Direct3D.Precision.Default, Microsoft.DirectX.Direct3D.FontQuality.ClearType, Microsoft.DirectX.Direct3D.PitchAndFamily.DefaultPitch, "Arial");
            if (m_font_2d_big != null)
                m_font_2d_big.Dispose();// = new D3d.Font(Engine3d.Device3d, m_font_height_big, m_font_width_big, Microsoft.DirectX.Direct3D.FontWeight.Normal, 5, false, Microsoft.DirectX.Direct3D.CharacterSet.Default, Microsoft.DirectX.Direct3D.Precision.Default, Microsoft.DirectX.Direct3D.FontQuality.ClearType, Microsoft.DirectX.Direct3D.PitchAndFamily.DefaultPitch, "Arial");
            if (m_font_2d_supersize != null)
                m_font_2d_supersize.Dispose();// = new D3d.Font(Engine3d.Device3d, m_font_height_supersize, m_font_width_supersize, Microsoft.DirectX.Direct3D.FontWeight.Normal, 5, false, Microsoft.DirectX.Direct3D.CharacterSet.Default, Microsoft.DirectX.Direct3D.Precision.Default, Microsoft.DirectX.Direct3D.FontQuality.ClearType, Microsoft.DirectX.Direct3D.PitchAndFamily.DefaultPitch, "Arial");

            if (m_font_2d_smallest_bold != null)
                m_font_2d_smallest_bold.Dispose();// = new D3d.Font(Engine3d.Device3d, m_font_height_smallest, m_font_width_smallest_bold, Microsoft.DirectX.Direct3D.FontWeight.Bold, 5, false, Microsoft.DirectX.Direct3D.CharacterSet.Default, Microsoft.DirectX.Direct3D.Precision.Default, Microsoft.DirectX.Direct3D.FontQuality.ClearType, Microsoft.DirectX.Direct3D.PitchAndFamily.DefaultPitch, "Arial");
            if (m_font_2d_small_bold != null)
                m_font_2d_small_bold.Dispose();// = new D3d.Font(Engine3d.Device3d, m_font_height_small, m_font_width_small_bold, Microsoft.DirectX.Direct3D.FontWeight.Bold, 5, false, Microsoft.DirectX.Direct3D.CharacterSet.Default, Microsoft.DirectX.Direct3D.Precision.Default, Microsoft.DirectX.Direct3D.FontQuality.ClearType, Microsoft.DirectX.Direct3D.PitchAndFamily.DefaultPitch, "Arial");
            if (m_font_2d_medium_bold != null)
                m_font_2d_medium_bold.Dispose();// = new D3d.Font(Engine3d.Device3d, m_font_height_medium, m_font_width_medium_bold, Microsoft.DirectX.Direct3D.FontWeight.Bold, 5, false, Microsoft.DirectX.Direct3D.CharacterSet.Default, Microsoft.DirectX.Direct3D.Precision.Default, Microsoft.DirectX.Direct3D.FontQuality.ClearType, Microsoft.DirectX.Direct3D.PitchAndFamily.DefaultPitch, "Arial");
            if (m_font_2d_big_bold != null)
                m_font_2d_big_bold.Dispose();// = new D3d.Font(Engine3d.Device3d, m_font_height_big, m_font_width_big_bold, Microsoft.DirectX.Direct3D.FontWeight.Bold, 5, false, Microsoft.DirectX.Direct3D.CharacterSet.Default, Microsoft.DirectX.Direct3D.Precision.Default, Microsoft.DirectX.Direct3D.FontQuality.ClearType, Microsoft.DirectX.Direct3D.PitchAndFamily.DefaultPitch, "Arial");
            if (m_font_2d_supersize_bold != null)
                m_font_2d_supersize_bold.Dispose();// = new D3d.Fo

        }
        #endregion

        public override void Render(float elapsedTime)
        {
            //dont do anything if there is no text do render
            if (m_arr_cached_text.Count == 0)
                return;

            //iterate through each cached text obj and print the text
            //create a std rotation matrix, this rotation has to be made otherwise we ar looking at the back of the sprite
            Microsoft.DirectX.Matrix rotmatrix = Microsoft.DirectX.Matrix.RotationYawPitchRoll(0.0f, Convert.ToSingle(Math.PI), 0.0f);
            foreach (CachedText ct in m_arr_cached_text)
            {
                float scaling = 1;
                if (WithFontScaling)
                {

                    //get scaling factor for font
                    scaling = GetScalingValuesForFontRendering(ct.FontSize);
                }
                //calc font dimensions
                float font_width_ges = ct.Text.Length * GetFontWidth(ct.FontSize) * scaling;
                float font_height_ges = GetFontHeight(ct.FontSize) * scaling;

                //set world coord system back to identity
                Engine3d.Device3d.Transform.World = Microsoft.DirectX.Matrix.Identity
                                                    *
                                                    Microsoft.DirectX.Matrix.Scaling(scaling, scaling, scaling)
                                                    *
                                                    rotmatrix
                                                    *
                                                    Microsoft.DirectX.Matrix.Translation(ct.X - (font_width_ges / 2.0f), ct.Y + (font_height_ges / 2.0f), ct.Z);

                //begin with sprite
                m_sprite.Begin(D3d.SpriteFlags.ObjectSpace | D3d.SpriteFlags.SortTexture | D3d.SpriteFlags.AlphaBlend);
                switch (ct.FontSize)
                {
                    case FontSize3d.Supersize:
                        m_font_2d_supersize.DrawText(m_sprite, ct.Text, 0, 0, System.Drawing.Color.FromArgb(FontTransparency, System.Drawing.Color.Black) );
                        break;
                    case FontSize3d.Big:
                        m_font_2d_big.DrawText(m_sprite, ct.Text, 0, 0, System.Drawing.Color.FromArgb(FontTransparency, System.Drawing.Color.Black));
                        break;
                    case FontSize3d.Medium:
                        m_font_2d_medium.DrawText(m_sprite, ct.Text, 0, 0, System.Drawing.Color.FromArgb(FontTransparency, System.Drawing.Color.Black));
                        break;
                    case FontSize3d.Small:
                        m_font_2d_small.DrawText(m_sprite, ct.Text, 0, 0, System.Drawing.Color.FromArgb(FontTransparency, System.Drawing.Color.Black));
                        break;
                    case FontSize3d.Smallest:
                        m_font_2d_smallest.DrawText(m_sprite, ct.Text, 0, 0, System.Drawing.Color.FromArgb(FontTransparency, System.Drawing.Color.Black));
                        break;

                    case FontSize3d.SupersizeBold:
                        m_font_2d_supersize_bold.DrawText(m_sprite, ct.Text, 0, 0, System.Drawing.Color.FromArgb(FontTransparency, System.Drawing.Color.Black));
                        break;
                    case FontSize3d.BigBold:
                        m_font_2d_big_bold.DrawText(m_sprite, ct.Text, 0, 0, System.Drawing.Color.FromArgb(FontTransparency, System.Drawing.Color.Black));
                        break;
                    case FontSize3d.MediumBold:
                        m_font_2d_medium_bold.DrawText(m_sprite, ct.Text, 0, 0, System.Drawing.Color.FromArgb(FontTransparency, System.Drawing.Color.Black));
                        break;
                    case FontSize3d.SmallBold:
                        m_font_2d_small_bold.DrawText(m_sprite, ct.Text, 0, 0, System.Drawing.Color.FromArgb(FontTransparency, System.Drawing.Color.Black));
                        break;
                    case FontSize3d.SmallestBold:
                        m_font_2d_smallest_bold.DrawText(m_sprite, ct.Text, 0, 0, System.Drawing.Color.FromArgb(FontTransparency, System.Drawing.Color.Black));
                        break;

                }
                m_sprite.End();
            }
        }

        #region Font Scaling Helper Methods
        private int GetFontHeight(FontSize3d fontSize3d)
        {
            switch (fontSize3d)
            {
                case FontSize3d.Supersize:
                    return m_font_height_supersize;
                case FontSize3d.Big:
                    return m_font_height_big;
                case FontSize3d.Medium:
                    return m_font_height_medium;
                case FontSize3d.Small:
                    return m_font_height_small;
                case FontSize3d.Smallest:
                    return m_font_height_smallest;
                case FontSize3d.SupersizeBold:
                    return m_font_height_supersize;
                case FontSize3d.BigBold:
                    return m_font_height_big;
                case FontSize3d.MediumBold:
                    return m_font_height_medium;
                case FontSize3d.SmallBold:
                    return m_font_height_small;
                case FontSize3d.SmallestBold:
                    return m_font_height_smallest;
            }
            return 0;
        }

        private int GetFontWidth(FontSize3d fontSize3d)
        {
            switch (fontSize3d)
            {
                case FontSize3d.Supersize:
                    return m_font_width_supersize;
                case FontSize3d.Big:
                    return m_font_width_big;
                case FontSize3d.Medium:
                    return m_font_width_medium;
                case FontSize3d.Small:
                    return m_font_width_small;
                case FontSize3d.Smallest:
                    return m_font_width_smallest;
                case FontSize3d.SupersizeBold:
                    return m_font_width_supersize_bold;
                case FontSize3d.BigBold:
                    return m_font_width_big_bold;
                case FontSize3d.MediumBold:
                    return m_font_width_medium_bold;
                case FontSize3d.SmallBold:
                    return m_font_width_small_bold;
                case FontSize3d.SmallestBold:
                    return m_font_width_smallest_bold;
            }
            return 0;
        }

        private float GetScalingValuesForFontRendering(FontSize3d fontsize)
        {
            float cam_z = Engine3d.Camera.Position.Z;

            float[,] margin_discrete_val = new float[10, 2];
            float[] margin_discrete_xpos = new float[2];

            margin_discrete_xpos[0] = -476;
            margin_discrete_xpos[1] = -41900;

            margin_discrete_val[0, 0] = 1f; // font_size = 1 , alt =  -476
            margin_discrete_val[1, 0] = 1f;
            margin_discrete_val[2, 0] = 1f;
            margin_discrete_val[3, 0] = 1f;
            margin_discrete_val[4, 0] = 1f;
            margin_discrete_val[5, 0] = 1f;
            margin_discrete_val[6, 0] = 1f;
            margin_discrete_val[7, 0] = 1f;
            margin_discrete_val[8, 0] = 1f;
            margin_discrete_val[9, 0] = 1f;

            margin_discrete_val[0, 1] = 38; // font_size = 1 , alt =  -41000
            margin_discrete_val[1, 1] = 38;  // font_size = 2 , alt =  -41000
            margin_discrete_val[2, 1] = 40;
            margin_discrete_val[3, 1] = 38;
            margin_discrete_val[4, 1] = 38;
            margin_discrete_val[5, 1] = 38; // font_size = 1 , alt =  -41000
            margin_discrete_val[6, 1] = 38;  // font_size = 2 , alt =  -41000
            margin_discrete_val[7, 1] = 38;
            margin_discrete_val[8, 1] = 38;
            margin_discrete_val[9, 1] = 38;

            float val_at_z_top = margin_discrete_val[(int)fontsize - 1, 1];
            float val_at_z_bottom = margin_discrete_val[(int)fontsize - 1, 0];
            float z_top = margin_discrete_xpos[1];
            float z_bottom = margin_discrete_xpos[0];


            float k = (val_at_z_top - val_at_z_bottom) / (z_top - z_bottom);
            float d = val_at_z_bottom - k * z_bottom;

            return k * cam_z + d;
        }
        #endregion

        public void ClearCache()
        {
            //clear cache
            m_rects_already_drawn_text.Clear();
            m_arr_cached_text.Clear();
            //state clear cached in order to reinit culled text info aso
            m_overlaping_text_removed = false;
        }

        #region Attribs
        private struct CachedText
        {
            public string Text;
            public float X;
            public float Y;
            public float Z;
            public FontSize3d FontSize;
        }

        public int FontTransparency { get; set; }

        private D3d.Font m_font_2d_smallest;
        private D3d.Font m_font_2d_small;
        private D3d.Font m_font_2d_medium;
        private D3d.Font m_font_2d_big;
        private D3d.Font m_font_2d_supersize;

        private D3d.Font m_font_2d_smallest_bold;
        private D3d.Font m_font_2d_small_bold;
        private D3d.Font m_font_2d_medium_bold;
        private D3d.Font m_font_2d_big_bold;
        private D3d.Font m_font_2d_supersize_bold;

        private const int m_font_height_smallest = 12;
        private const int m_font_height_small = 14;
        private const int m_font_height_medium = 18;
        private const int m_font_height_big = 24;
        private const int m_font_height_supersize = 32;

        private int m_font_width_smallest = Convert.ToInt32(Math.Floor(m_font_height_smallest * 0.40));
        private int m_font_width_small = Convert.ToInt32(Math.Floor(m_font_height_small * 0.40));
        private int m_font_width_medium = Convert.ToInt32(Math.Floor(m_font_height_medium * 0.40));
        private int m_font_width_big = Convert.ToInt32(Math.Floor(m_font_height_big * 0.40));
        private int m_font_width_supersize = Convert.ToInt32(Math.Floor(m_font_height_supersize * 0.40));

        private int m_font_width_smallest_bold = Convert.ToInt32(Math.Floor(m_font_height_smallest * 0.45));
        private int m_font_width_small_bold = Convert.ToInt32(Math.Floor(m_font_height_small * 0.45));
        private int m_font_width_medium_bold = Convert.ToInt32(Math.Floor(m_font_height_medium * 0.45));
        private int m_font_width_big_bold = Convert.ToInt32(Math.Floor(m_font_height_big * 0.45));
        private int m_font_width_supersize_bold = Convert.ToInt32(Math.Floor(m_font_height_supersize * 0.45));


        private D3d.Sprite m_sprite;
        private List<CachedText> m_arr_cached_text = new List<CachedText>();
        //use an array list to evaluate if there has been a drawing of the text already
        private ArrayList m_rects_already_drawn_text = new ArrayList();
        private bool m_overlaping_text_removed = false;


        private string m_text = "";
        //private string m_path_to_images = ;
        private Hashtable m_letters = new Hashtable();
        private Line m_curve;
        //private float startpos_x=0, startpos_y=0, startpos_z=0;
        private float m_font_height_suggested = 10;
        //private FreeTypeFont3d m_font_bitmaps = new FreeTypeFont3d(@"C:\windows\fonts\arial.ttf", 14);
        private GdiFont3d m_font_bitmaps = new GdiFont3d("Arial", 18);
        public bool WithFontScaling { get; set; }

        #endregion


    }
}
