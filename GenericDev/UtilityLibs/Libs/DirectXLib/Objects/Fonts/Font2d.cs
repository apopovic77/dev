﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using Logicx.DirectX.Engine;
using Logicx.DirectX.Objects;
using Logicx.Drawing.Objects2d;
using Microsoft.DirectX.Direct3D;
using Font=Microsoft.DirectX.Direct3D.Font;

namespace DirectXLib.Objects.Fonts
{
    public abstract class Font2d : Object3d
    {
       public struct FontObject
       {
           public FontStyleDescription FontStyleDescription;
           public Font Font3d;
           public System.Drawing.Font Font2d;

       }

        #region Contruction
        public Font2d(string text, string font_fam, int font_size, FontStyle fontweight)
        {
            Text = text;

            FontStyleDescription fsd = new FontStyleDescription();
            fsd.FontName = font_fam;
            fsd.FontSize = font_size;
            fsd.FontWeight = fontweight;

            _active_fsd = fsd;

            //check if font is already created
            if(!_fontcache.ContainsKey(fsd.ToString()))
            {
                //create font and add it to the cache
                FontObject font = new FontObject();
                font.Font2d = new System.Drawing.Font(fsd.FontName,fsd.FontSize,fsd.FontWeight);
                font.FontStyleDescription = fsd;
                font.Font3d = new Font(Engine3d.Device3d,font.Font2d);
                _fontcache.Add(fsd.ToString(),font);
                _active_font = font;
            }
            else
            {
                _active_font = (FontObject)_fontcache[fsd.ToString()];
            }

        }
        #endregion

        public override void Init()
        {
            if(_sprite == null)
                _sprite = new Sprite(Engine3d.Device3d);
            if(_fontcache == null)
                _fontcache = new Hashtable();
        }

        public override void Dispose()
        {
            if (_sprite != null)
            {
                _sprite.Dispose();
                _sprite = null;
            }
            if(_fontcache != null)
            {
                foreach (FontObject fobj in _fontcache)
                {
                    try
                    {
                        fobj.Font2d.Dispose();
                        fobj.Font3d.Dispose();
                    }
                    catch{}
                }
                _fontcache = null;
            }
        }

        #region Properties
        #endregion

        #region Attribs
        protected static Hashtable _fontcache = new Hashtable();
        protected static Sprite _sprite;
        public string Text;
        public Color Color = Color.Black;

        protected FontObject _active_font;
        protected FontStyleDescription _active_fsd;
        #endregion
    }
}
