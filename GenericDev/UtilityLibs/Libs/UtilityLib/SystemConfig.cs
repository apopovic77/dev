﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Text;
using Utility;

namespace Logicx.Utility
{
    public class ConfigPropertyChangedEventArgs : EventArgs
    {
        public ConfigPropertyChangedEventArgs(string name)
        {
            PropertyName = name;
        }

        public string PropertyName;
    }
    public class IntConfigPropertyChangedEventArgs : ConfigPropertyChangedEventArgs
    {
        public IntConfigPropertyChangedEventArgs(string name, IntConfigValue conf_value) : base(name)
        {
            IntConfigValue = conf_value;
        }
        public IntConfigValue IntConfigValue;
    }
    public class StringConfigPropertyChangedEventArgs : ConfigPropertyChangedEventArgs
    {
        public StringConfigPropertyChangedEventArgs(string name, StringConfigValue conf_value)
            : base(name)
        {
            StringConfigValue = conf_value;
        }
        public StringConfigValue StringConfigValue;
    }
    public class BoolConfigPropertyChangedEventArgs : ConfigPropertyChangedEventArgs
    {
        public BoolConfigPropertyChangedEventArgs(string name, BoolConfigValue conf_value)
            : base(name)
        {
            BoolConfigValue = conf_value;
        }
        public BoolConfigValue BoolConfigValue;
    }
    public class FloatConfigPropertyChangedEventArgs : ConfigPropertyChangedEventArgs
    {
        public FloatConfigPropertyChangedEventArgs(string name, FloatConfigValue conf_value)
            : base(name)
        {
            FloatConfigValue = conf_value;
        }

        public FloatConfigValue FloatConfigValue;
    }

    public abstract class ConfigValue : INotifyPropertyChanged
    {
        public ConfigValue(string name, string bezeichnung)
        {
            _name = name;
            _bezeichnung = bezeichnung;
            PropertyChanged = null;
        }

        protected void Notify(string name)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(name));
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; Notify("Name"); }
        }

        public string Bezeichnung
        {
            get { return _bezeichnung; }
            set { _bezeichnung = value; Notify("Bezeichnung"); }
        }

        private string _name;
        private string _bezeichnung;

        public event PropertyChangedEventHandler PropertyChanged;
    }

    public class BoolConfigValue : ConfigValue
    {
        public BoolConfigValue(string name, string bezeichnung, bool value) : base(name,bezeichnung)
        {
            _value = value;
        }

        public override string ToString()
        {
            return "BoolConfigValue: Value = " + _value;
        }

        public bool Value
        {
            get { return _value; }
            set { _value = value; Notify("Value"); }
        }

        private bool _value;
    }
    public class FloatConfigValue : ConfigValue
    {
        public FloatConfigValue(string name, string bezeichnung, float value)
            : base(name, bezeichnung)
        {
            _value = value;
            _minimum = float.MinValue;
            _maximum = float.MaxValue;
        }

        public FloatConfigValue(string name, string bezeichnung, float value, float min_value, float max_value)
            : base(name, bezeichnung)
        {
            _value = value;
            _minimum = min_value;
            _maximum = max_value;
        }

        public override string ToString()
        {
            return "FloatConfigValue: Value = " + _value + (HasMinMaxValues ? " Min: " + Minimum + " Max: " + Maximum : "");
        }

        public bool HasMinMaxValues { 
            get
            {
                if (_minimum == float.MinValue || _maximum == float.MaxValue)
                    return false;
                else
                    return true;
            }
        }

        public float Value
        {
            get { return _value; }
            set { _value = value; Notify("Value"); }
        }

        public float Minimum
        {
            get { return _minimum; }
            set { _minimum = value; Notify("Minimum"); }
        }

        public float Maximum
        {
            get { return _maximum; }
            set { _maximum = value; Notify("Maximum"); }
        }

        private float _value;
        private float _minimum;
        private float _maximum;
    }
    public class Vector2FloatConfigValue : ConfigValue
    {
        public Vector2FloatConfigValue(string name, string bezeichnung, float value1, float value2)
            : base(name, bezeichnung)
        {
            _value1 = value1;
            _value2 = value2;
            _minimum = float.MinValue;
            _maximum = float.MaxValue;
        }
        public Vector2FloatConfigValue(string name, string bezeichnung, float value1, float value2, float min_value, float max_value)
            : base(name, bezeichnung)
        {
            _value1 = value1;
            _value2 = value2;
            _minimum = min_value;
            _maximum = max_value;
        }

        public bool HasMinMaxValues
        {
            get
            {
                if (_minimum == float.MinValue || _maximum == float.MaxValue)
                    return false;
                else
                    return true;
            }
        }

        public float ValueStart
        {
            get { return _value1; }
            set { _value1 = value; Notify("ValueStart"); }
        }
        public float ValueEnd
        {
            get { return _value2; }
            set { _value2 = value; Notify("ValueEnd"); }
        }
        public float Minimum
        {
            get { return _minimum; }
            set { _minimum = value; Notify("Minimum"); }
        }

        public float Maximum
        {
            get { return _maximum; }
            set { _maximum = value; Notify("Maximum"); }
        }

        private float _value1,_value2;
        private float _minimum;
        private float _maximum;
    }
    public class IntConfigValue : ConfigValue
    {
        public IntConfigValue(string name, string bezeichnung, int value)
            : base(name, bezeichnung)
        {
            _value = value;
            _minimum = int.MinValue;
            _maximum = int.MaxValue;
        }

        public IntConfigValue(string name, string bezeichnung, int value, int min_value, int max_value)
            : base(name, bezeichnung)
        {
            _value = value;
            _minimum = min_value;
            _maximum = max_value;
        }

        public override string ToString()
        {
            return "IntConfigValue: Value = " + _value + (HasMinMaxValues ? " Min: " + Minimum + " Max: " + Maximum : "");
        }

        public bool HasMinMaxValues
        {
            get
            {
                if (_minimum == int.MinValue || _maximum == int.MaxValue)
                    return false;
                else
                    return true;
            }
        }

        public int Value
        {
            get { return _value; }
            set { _value = value; Notify("Value"); }
        }

        public int Minimum
        {
            get { return _minimum; }
            set { _minimum = value; Notify("Minimum"); }
        }

        public int Maximum
        {
            get { return _maximum; }
            set { _maximum = value; Notify("Maximum"); }
        }

        private int _value;
        private int _minimum;
        private int _maximum;
    }
    public class Vector2IntConfigValue : ConfigValue
    {
        public Vector2IntConfigValue(string name, string bezeichnung, int value1, int value2)
            : base(name, bezeichnung)
        {
            _value1 = value1;
            _value2 = value2;
            _minimum = int.MinValue;
            _maximum = int.MaxValue;
        }
        public Vector2IntConfigValue(string name, string bezeichnung, int value1, int value2, int min_value, int max_value)
            : base(name, bezeichnung)
        {
            _value1 = value1;
            _value2 = value2;
            _minimum = min_value;
            _maximum = max_value;
        }

        public bool HasMinMaxValues
        {
            get
            {
                if (_minimum == int.MinValue || _maximum == int.MaxValue)
                    return false;
                else
                    return true;
            }
        }

        public int ValueStart
        {
            get { return _value1; }
            set { _value1 = value; Notify("ValueStart"); }
        }
        public int ValueEnd
        {
            get { return _value2; }
            set { _value2 = value; Notify("ValueEnd"); }
        }
        public int Minimum
        {
            get { return _minimum; }
            set { _minimum = value; Notify("Minimum"); }
        }

        public int Maximum
        {
            get { return _maximum; }
            set { _maximum = value; Notify("Maximum"); }
        }

        private int _value1, _value2;
        private int _minimum;
        private int _maximum;
    }
    public class StringConfigValue : ConfigValue
    {
        public StringConfigValue(string name, string bezeichnung, string value)
            : base(name, bezeichnung)
        {
            _value = value;
        }

        public override string ToString()
        {
            return "StringConfigValue: Value = " + _value;
        }

        public string Value
        {
            get { return _value; }
            set { _value = value; Notify("Value"); }
        }
        private string _value;
    }

    public abstract class SystemConfig
    {
        public SystemConfig(string app_config_name)
        {
            LoadConfigFile("", app_config_name);

            lock (_system_configs)
            {
                _system_configs.Add(this);
            }
        }

        public SystemConfig(string app_config_folder, string app_config_name)
        {
            LoadConfigFile(app_config_folder, app_config_name);
        }

        private void LoadConfigFile(string app_config_folder, string app_config_name)
        {
            if (_config_loaded)
                return;

            _config_loaded = true;

            #region Deployed Check
            //ApplicationDeployment app_deployment;
            RunningDeployedApplication = false;
            //try
            //{
            //    app_deployment = ApplicationDeployment.CurrentDeployment;
            //    RunningDeployedApplication = true;
            //}
            //catch
            //{
            //    app_deployment = null;
            //    RunningDeployedApplication = false;
            //}
            #endregion

            #region Loadin Application Configuration
            //lade configs aus der app.statusConfig
            string config_file_path;
            if (!RunningDeployedApplication)
            {
                config_file_path = app_config_name;
            }
            else
            {
                string path = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\";
                config_file_path = path + app_config_folder + "\\" + app_config_name;
            }
            #endregion

            ConfigProperties = new Dictionary<string, object>();
            RegisterConfigProperties();


            if (!File.Exists(config_file_path))
            {
                Debug.WriteLine("Loading ConfigFile fehlgeschlagen. File " + config_file_path + " existiert nicht");
                return;
            }

            ExeConfigurationFileMap fileMap = new ExeConfigurationFileMap();
            fileMap.ExeConfigFilename = config_file_path;
            _config = _org_config = ConfigurationManager.OpenMappedExeConfiguration(fileMap, ConfigurationUserLevel.None);

            ExecLoadValues();
        }

        public void UpdateConfigValues(string rel_path_to_config)
        {
            //string abs_path_to_config = Directory.GetCurrentDirectory() + rel_path_to_config;

            if (!File.Exists(rel_path_to_config))
                throw new Exception("Configfile not there??!");

            ExeConfigurationFileMap fileMap = new ExeConfigurationFileMap();
            fileMap.ExeConfigFilename = rel_path_to_config;
            _config = ConfigurationManager.OpenMappedExeConfiguration(fileMap, ConfigurationUserLevel.None);

            if (_org_config == null)
                _org_config = _config;

            ExecLoadValues();

            _config = _org_config;
        }



        protected void Config_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (ConfigPropertyChanged != null)
            {
                ConfigPropertyChangedEventArgs event_args;

                if (sender is BoolConfigValue)
                    event_args = new BoolConfigPropertyChangedEventArgs(e.PropertyName, (BoolConfigValue)sender);
                else if (sender is IntConfigValue)
                    event_args = new IntConfigPropertyChangedEventArgs(e.PropertyName, (IntConfigValue)sender);
                else if (sender is FloatConfigValue)
                    event_args = new FloatConfigPropertyChangedEventArgs(e.PropertyName, (FloatConfigValue)sender);
                else if (sender is StringConfigValue)
                    event_args = new StringConfigPropertyChangedEventArgs(e.PropertyName, (StringConfigValue)sender);
                else 
                    throw new NotImplementedException();

                ConfigPropertyChanged(this, event_args);
            }
        }

        protected abstract void RegisterConfigProperties();
        protected abstract void ExecLoadValues();
        protected abstract void ExecSaveSettings();

        public void SaveSettings()
        {
            if (_config == null)
                return;

            ExecSaveSettings();

            // Save the _configuration file.
            _config.Save(ConfigurationSaveMode.Full);

            // Force a reload of the changed section.
            ConfigurationManager.RefreshSection("appSettings");
        }

        protected void AddKey(Configuration config, string key, string value)
        {
            if (config.AppSettings.Settings[key] != null)
                config.AppSettings.Settings[key].Value = value;
            else
                config.AppSettings.Settings.Add(key, value);
        }

        public static string GetPathToConfigFile()
        {
            string full_filename_curr_proc = Process.GetCurrentProcess().MainModule.FileName;
            int index_last_filename = full_filename_curr_proc.LastIndexOf("\\");
            string filename = full_filename_curr_proc.Substring(index_last_filename + 1);
            string path_to_config_filename = filename + ".config";
            if (File.Exists(path_to_config_filename))
                return (path_to_config_filename);
            else
                return null;
        }

        public static List<SystemConfig> SystemConfigs
        {
            get { return _system_configs; }
        }

        /// <summary>
        /// this class holds all instantiated config files in an private static variable
        /// via this method one can check if a property with the given name is there
        /// </summary>
        /// <param name="property_name"></param>
        /// <returns></returns>
        public static bool AssertConfigProperty(string property_name)
        {
            foreach (SystemConfig system_config in SystemConfig.SystemConfigs)
                if (system_config.ConfigProperties.ContainsKey(property_name))
                    return true;
            //string msg = "Unable to find config property with name \"" + property_name + "\", config property can not be retrieved.";
            //LdxContext.WriteDebugInfo(msg);
            return false;
        }

        public static T FindConfigProperty<T>(string name) where T : ConfigValue
        {
            foreach (SystemConfig system_config in SystemConfig.SystemConfigs)
                if (system_config.ConfigProperties.ContainsKey(name))
                {
                    ConfigValue config_prop = (ConfigValue)system_config.ConfigProperties[name];
                    if (!(config_prop is T))
                    {
                        //throw new Exception("config prop is of wrong type");
                        return null;
                    }
                    T t_config_prop = (T) config_prop;
                    return t_config_prop;
                }
            return null;
        }

        protected bool _config_loaded = false;
        private static List<SystemConfig> _system_configs = new List<SystemConfig>();
        protected Configuration _org_config;
        protected Configuration _config;
        public Dictionary<string, object> ConfigProperties;
        public bool RunningDeployedApplication;

        public event EventHandler<ConfigPropertyChangedEventArgs> ConfigPropertyChanged;


    }
}
