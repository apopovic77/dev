﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Logicx.WpfUtility.CustomControls.ActivityInfo
{
    public class ActivityInfoManager
    {
        public ActivityInfoManager(Grid placeholder)
        {
            _placeholder = placeholder;
            _placeholder.Resources.MergedDictionaries.Add((ResourceDictionary)Application.LoadComponent(new Uri("/WpfUtilityLib;component/Resources/Themes/ActivityInfoStyles.xaml", UriKind.Relative)));

            _base_border = new Border();
            _base_border.Style = (Style)_placeholder.FindResource("actinfo_placeholder_border_style");
            _placeholder.Children.Add(_base_border);

            _placeholder.Visibility = Visibility.Hidden;
        }

        public void ShowActivityInfo(FrameworkElement activity_info)
        {
            _placeholder.Visibility = Visibility.Visible;

            _base_border.Child = activity_info;
            _base_border.IsHitTestVisible = false;
        }

        public void HideActivityInfo()
        {
            _base_border.Child = null;
            _placeholder.Visibility = Visibility.Hidden;
        }

        protected Grid _placeholder;
        protected Border _base_border;
    }
}
