﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Utility
{
    public class StructReference<T> where T : struct
    {
        public StructReference(T value)
        {
            Value = value;
        }

        public T Value;
    }
}
