using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace Utility
{
    public class DrawUtilities
    {
        public static DrawUtilities Instance
        {
             
            get
            {
                lock (_padlock)
                {
                    if (_instance == null)
                    {
                        _instance = new DrawUtilities();

                        _instance._font          = new Font("Arial", 10, FontStyle.Regular);
                        _instance._font_bold     = new Font("Arial", 10, FontStyle.Bold);
                        _instance._font_big      = new Font("Arial", 12, FontStyle.Regular);
                        _instance._font_big_bold = new Font("Arial", 12, FontStyle.Bold);
                        _instance._font_small    = new Font("Arial", 7, FontStyle.Regular);

                        _instance._text_alighnment_left = new StringFormat(StringFormatFlags.NoClip);
                        _instance._text_alighnment_left.Alignment = StringAlignment.Near;
                        _instance._text_alighnment_right = new StringFormat(StringFormatFlags.NoClip); 
                        _instance._text_alighnment_right.Alignment = StringAlignment.Far;
                    }
                    return _instance;
                }
            }
        }



        #region Attribs
        private static DrawUtilities _instance = null;
        private static readonly object _padlock = new object();

        protected StringFormat _text_alighnment_left;
        protected StringFormat _text_alighnment_right;
        protected Font _font_big, _font_big_bold;
        protected Font _font, _font_bold;
        protected Font _font_small;
        #endregion

        #region Properties
        public Font FontSmall
		{
			set {
				_font_small = value;
			}
			
			get {
				return _font_small;
			}
		}
		
		public Font Font
		{
			set {
				_font = value;
			}
			
			get {
				return _font;
			}
		}
		
		public Font FontBold
		{
			set {
				_font_bold = value;
			}
			
			get {
				return _font_bold;
			}
		}

		public Font FontBig
		{
			set {
				_font_big = value;
			}
			
			get {
				return _font_big;
			}
		}
		
		public Font FontBigBold
		{
			set {
				_font_big_bold = value;
			}
			
			get {
				return _font_big_bold;
			}
		}
		
		
		public StringFormat TextAlighnmentRight
		{
			set {
				_text_alighnment_right = value;
			}
			
			get {
				return _text_alighnment_right;
			}
		}
		
		public StringFormat TextAlighnmentLeft
		{
			set {
				_text_alighnment_left = value;
			}
			
			get {
				return _text_alighnment_left;
			}
        }
        #endregion
    }
}
