USE [IaStore]
GO
/****** Object:  Table [dbo].[Logging_LogEntry]    Script Date: 08/05/2009 09:37:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Logging_LogEntry](
	[LogId] [bigint] IDENTITY(1,1) NOT NULL,
	[ProductName] [nvarchar](256) NOT NULL,
	[Diskriminator] [nvarchar](64) NOT NULL,
	[SessionId] [nvarchar](40) NULL,
	[ActivityId] [nvarchar](40) NULL,
	[ActivityDescription] [nvarchar](512) NULL,
	[RelatedActivityId] [nvarchar](40) NULL,
	[EventId] [bigint] NOT NULL,
	[Priority] [int] NOT NULL,
	[Severity] [int] NOT NULL,
	[Timestamp] [datetime] NOT NULL,
	[MachineName] [nvarchar](64) NOT NULL,
	[DomainLoginName] [nvarchar](64) NOT NULL,
	[Message] [nvarchar](4000) NULL,
	[FormattedMessage] [ntext] NULL,
	[AppDomainName] [nvarchar](512) NULL,
	[ProcedssId] [nvarchar](256) NULL,
	[ProcessName] [nvarchar](512) NULL,
	[ThreadName] [nvarchar](512) NULL,
	[Win32ThreadId] [nvarchar](128) NULL,
	[MethodName] [nvarchar](128) NULL,
	[ClassName] [nvarchar](512) NULL,
	[IsStartEntry] [bit] NOT NULL,
	[AppName] [nvarchar](512) NULL,
	[AppType] [nvarchar](512) NULL,
	[AppVersion] [nvarchar](512) NULL,
	[InstallMode] [nvarchar](512) NULL,
	[ActionType] [nvarchar](64) NULL,
	[ActionInfo1] [nvarchar](512) NULL,
	[ActionInfo2] [nvarchar](512) NULL,
	[ActionInfo3] [nvarchar](512) NULL,
	[ActionInfo4] [nvarchar](512) NULL,
	[ActionInfo5] [nvarchar](512) NULL,
	[ActionData1] [ntext] NULL,
	[ActionData2] [ntext] NULL,
	[ActionData3] [ntext] NULL,
	[OperatingSystem] [nvarchar](512) NULL,
	[SerivcePackInfo] [nvarchar](512) NULL,
	[FreeSysDiskSpace] [float] NULL,
	[CPU_Speed] [nvarchar](256) NULL,
	[CPU_Type] [nvarchar](256) NULL,
	[CPU_ID] [nvarchar](256) NULL,
	[Total_RAM] [float] NULL,
	[Available_RAM] [float] NULL,
	[PC_Modell] [nvarchar](256) NULL,
	[PC_Manufacturer] [nvarchar](256) NULL,
	[SoundCard] [nvarchar](256) NULL,
	[NIC_1] [nvarchar](256) NULL,
	[NIC_2] [nvarchar](256) NULL,
	[NIC_3] [nvarchar](256) NULL,
	[NIC_4] [nvarchar](256) NULL,
	[VGACard] [nvarchar](256) NULL,
	[LogonServer] [nvarchar](256) NULL,
	[Domain] [nvarchar](64) NULL,
	[NumberPhysicalCPU] [int] NULL,
	[NumberCPUCore] [int] NULL,
	[NumberLogicalCPU] [int] NULL,
	[PagefileSize] [nvarchar](128) NULL,
	[SerialNumber] [nvarchar](256) NULL,
 CONSTRAINT [PK_Logging_LogEntry] PRIMARY KEY CLUSTERED 
(
	[LogId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [Idx_Diskriminator] ON [dbo].[Logging_LogEntry] 
(
	[Diskriminator] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [Idx_EventId] ON [dbo].[Logging_LogEntry] 
(
	[EventId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [Idx_MachineName] ON [dbo].[Logging_LogEntry] 
(
	[MachineName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [Idx_MachineName_DomainLoginName] ON [dbo].[Logging_LogEntry] 
(
	[MachineName] ASC,
	[DomainLoginName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [Idx_Priority] ON [dbo].[Logging_LogEntry] 
(
	[Priority] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [Idx_ProductName] ON [dbo].[Logging_LogEntry] 
(
	[ProductName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [Idx_SessionId] ON [dbo].[Logging_LogEntry] 
(
	[SessionId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [Idx_Severity] ON [dbo].[Logging_LogEntry] 
(
	[Severity] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [Idx_Timestamp] ON [dbo].[Logging_LogEntry] 
(
	[Timestamp] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Logging_Category]    Script Date: 08/05/2009 09:37:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Logging_Category](
	[ProductName] [nvarchar](256) NOT NULL,
	[CategoryName] [nvarchar](64) NOT NULL,
 CONSTRAINT [PK_Logging_Category] PRIMARY KEY CLUSTERED 
(
	[ProductName] ASC,
	[CategoryName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Logging_SQLServer]    Script Date: 08/05/2009 09:37:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Logging_SQLServer](
	[SQLServerId] [bigint] IDENTITY(1,1) NOT NULL,
	[LogId] [bigint] NOT NULL,
	[InstanceName] [nvarchar](512) NULL,
	[Version] [nvarchar](64) NULL,
	[Edition] [nvarchar](128) NULL,
	[Licensemode] [nvarchar](512) NULL,
	[BinPath] [nvarchar](512) NULL,
	[StartMode] [nvarchar](64) NULL,
	[ServiceAccount] [nvarchar](128) NULL,
	[InstanceStatus] [nvarchar](64) NULL,
	[FileVersion] [nvarchar](32) NULL,
	[Clustered] [nvarchar](32) NULL,
 CONSTRAINT [PK_Logging_SQLServer] PRIMARY KEY CLUSTERED 
(
	[SQLServerId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Logging_InstalledSoftware]    Script Date: 08/05/2009 09:37:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Logging_InstalledSoftware](
	[SoftwareId] [bigint] IDENTITY(1,1) NOT NULL,
	[LogId] [bigint] NOT NULL,
	[Title] [nvarchar](512) NULL,
	[Publisher] [nvarchar](512) NULL,
	[Version] [nvarchar](64) NULL,
 CONSTRAINT [PK_Logging_InstalledSoftware] PRIMARY KEY CLUSTERED 
(
	[SoftwareId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Logging_CategoryLogEntry]    Script Date: 08/05/2009 09:37:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Logging_CategoryLogEntry](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[ProductName] [nvarchar](256) NULL,
	[CategoryName] [nvarchar](64) NOT NULL,
	[LogId] [bigint] NOT NULL,
 CONSTRAINT [PK_Logging_CategoryLogEntry] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Default [DF_Logging_LogEntry_IsStartEntry]    Script Date: 08/05/2009 09:37:47 ******/
ALTER TABLE [dbo].[Logging_LogEntry] ADD  CONSTRAINT [DF_Logging_LogEntry_IsStartEntry]  DEFAULT ((0)) FOR [IsStartEntry]
GO
/****** Object:  ForeignKey [FK_Logging_CategoryLogEntry_Logging_Category]    Script Date: 08/05/2009 09:37:47 ******/
ALTER TABLE [dbo].[Logging_CategoryLogEntry]  WITH CHECK ADD  CONSTRAINT [FK_Logging_CategoryLogEntry_Logging_Category] FOREIGN KEY([ProductName], [CategoryName])
REFERENCES [dbo].[Logging_Category] ([ProductName], [CategoryName])
GO
ALTER TABLE [dbo].[Logging_CategoryLogEntry] CHECK CONSTRAINT [FK_Logging_CategoryLogEntry_Logging_Category]
GO
/****** Object:  ForeignKey [FK_Logging_CategoryLogEntry_Logging_LogEntry]    Script Date: 08/05/2009 09:37:47 ******/
ALTER TABLE [dbo].[Logging_CategoryLogEntry]  WITH CHECK ADD  CONSTRAINT [FK_Logging_CategoryLogEntry_Logging_LogEntry] FOREIGN KEY([LogId])
REFERENCES [dbo].[Logging_LogEntry] ([LogId])
GO
ALTER TABLE [dbo].[Logging_CategoryLogEntry] CHECK CONSTRAINT [FK_Logging_CategoryLogEntry_Logging_LogEntry]
GO
/****** Object:  ForeignKey [FK_Logging_InstalledSoftware_Logging_LogEntry]    Script Date: 08/05/2009 09:37:47 ******/
ALTER TABLE [dbo].[Logging_InstalledSoftware]  WITH CHECK ADD  CONSTRAINT [FK_Logging_InstalledSoftware_Logging_LogEntry] FOREIGN KEY([LogId])
REFERENCES [dbo].[Logging_LogEntry] ([LogId])
GO
ALTER TABLE [dbo].[Logging_InstalledSoftware] CHECK CONSTRAINT [FK_Logging_InstalledSoftware_Logging_LogEntry]
GO
/****** Object:  ForeignKey [FK_Logging_SQLServer_Logging_LogEntry]    Script Date: 08/05/2009 09:37:47 ******/
ALTER TABLE [dbo].[Logging_SQLServer]  WITH CHECK ADD  CONSTRAINT [FK_Logging_SQLServer_Logging_LogEntry] FOREIGN KEY([LogId])
REFERENCES [dbo].[Logging_LogEntry] ([LogId])
GO
ALTER TABLE [dbo].[Logging_SQLServer] CHECK CONSTRAINT [FK_Logging_SQLServer_Logging_LogEntry]
GO
