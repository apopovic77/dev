﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using LoggingLib.Configuration;
using LoggingLib.Data.Finalizers;

namespace LoggingLib.Configuration.Activators
{
    /// <summary>
    /// Instance activator class for configured data finalizers
    /// </summary>
    public class DataFinalizerActivator
    {
        #region Construction and initialization
        /// <summary>
        /// Default constructor building the data finalizers depending on the configuration settings
        /// </summary>
        /// <param name="settings"></param>
        public DataFinalizerActivator(LoggingConfigSection settings)
        {
            if (settings == null)
                throw new ArgumentException("Settings must not be null.", "settings");
            
            BuildFinalizers(settings);
        }
        #endregion

        #region Operations

        /// <summary>
        /// Returns the finalizer linked to the given data type
        /// </summary>
        /// <param name="linkedType"></param>
        /// <returns></returns>
        public DataFinalizerBase GetFinalizerForType(Type linkedType)
        {
            foreach(DataFinalizerBase finalizer in _finalizerList)
            {
                if (finalizer.LinkedType == linkedType)
                    return finalizer;
            }

            return null;
        }

        /// <summary>
        /// Build a list of data finalizer instances
        /// </summary>
        /// <param name="settings"></param>
        private void BuildFinalizers(LoggingConfigSection settings)
        {
            foreach(DataFinalizerElement dfElement in settings.DataFinalizers)
            {
                Type linkedElementType = Type.GetType(dfElement.LogEntryDataTypeName);
                Type finalizerDataTypeName = Type.GetType(dfElement.FinalizerDataTypeName);

                object objFinalizer = Activator.CreateInstance(finalizerDataTypeName);

                SetLinkedTypePropertyValue(objFinalizer, linkedElementType);
                _finalizerList.Add(objFinalizer as DataFinalizerBase);
            }
        }
        /// <summary>
        /// Sets the linked type value property
        /// </summary>
        /// <param name="finalizer">finalizer where to set the property</param>
        /// <param name="linkedTypeValue">value to set</param>
        private void SetLinkedTypePropertyValue(object finalizer, Type linkedTypeValue)
        {
            if (finalizer == null)
                return;

            foreach(PropertyInfo pI in finalizer.GetType().GetProperties())
            {
                if(pI.Name.Equals("LinkedType"))
                {
                    pI.SetValue(finalizer, linkedTypeValue, null);
                    return;
                }
            }
        }
        /// <summary>
        /// Gets the linked type property of a finalizer
        /// </summary>
        /// <param name="finalizer"></param>
        /// <returns></returns>
        private Type GetLinkedTypePropertyValue(object finalizer)
        {
            if (finalizer == null)
                return null;

            foreach (PropertyInfo pI in finalizer.GetType().GetProperties())
            {
                if (pI.Name.Equals("LinkedType"))
                {
                    return (Type)pI.GetValue(finalizer, null);
                }
            }

            return null;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets a reference to the data finalizer list
        /// </summary>
        public List<DataFinalizerBase> FinalizerList
        {
            get { return _finalizerList; }
        }
        #endregion

        #region Attribs
        private List<DataFinalizerBase> _finalizerList = new List<DataFinalizerBase>();
        #endregion
    }
}