﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Logicx.WpfUtility.CustomControls.OverlayManager;
using Logicx.WpfUtility.Data;
using Logicx.WpfUtility.Properties;
using Logicx.WpfUtility.WpfHelpers;

namespace Logicx.WpfUtility.CustomControls.HelpProvider
{
    /// <summary>
    /// Interaktionslogik für HelpProvider.xaml
    /// </summary>
    public class HelpProvider : Grid, IDisposable
    {

        #region Static Constructor
        /// <summary>
        /// Static Constructor
        /// </summary>
        static HelpProvider()
        {
            UIPropertyMetadata metadata_helpindex = new UIPropertyMetadata(string.Empty, new PropertyChangedCallback(OnHelpIndexChanged));
            HelpIndexProperty = DependencyProperty.RegisterAttached("HelpIndex", typeof(string), typeof(HelpProvider), metadata_helpindex);

            UIPropertyMetadata metadata_helpscope = new UIPropertyMetadata(string.Empty, new PropertyChangedCallback(OnHelpScopeChanged));
            HelpScopeProperty = DependencyProperty.RegisterAttached("HelpScope", typeof(string), typeof(HelpProvider), metadata_helpscope);
        }
        #endregion

        #region Static Properties
        /// <summary>
        /// Zur Speicherung des Verbindungsstrings mit der Datenbank
        /// </summary>
        public static string ServerConnectionString
        {
            set
            {
                DisposeDataContext();
                _server_connection_string = value;
            }
            get
            {
                return _server_connection_string;
            }
        }
        public static string LocalConnectionString
        {
            set
            {
                DisposeDataContext();
                _local_connection_string = value;
            }
            get
            {
                return _local_connection_string;
            }
        }

        /// <summary>
        /// Verbindungsaufbau mit der Datenbank und wenn erforderlich, Erzeugung eines DataContext für die Datenbank
        /// </summary>
        public static HelpDataContext HelpDB
        {
            get
            {
                if (_data_context != null)
                    return _data_context;

                if (String.IsNullOrEmpty(ServerConnectionString))
                    return null;
                else
                {
                    _data_context = new HelpDataContext(ServerConnectionString);
                    if(_data_context.DatabaseExists())
                        return _data_context;
                }

                if (String.IsNullOrEmpty(LocalConnectionString))
                    return null;
                else
                {
                    _data_context = new HelpDataContext(LocalConnectionString);
                    if (_data_context.DatabaseExists())
                        return _data_context;
                }

                return null;
            }
        }

        /// <summary>
        /// Abfragen eines Datenbankeintrages, basierend auf den Schlüsseleinträgen
        /// </summary>
        /// <param name="scope_key"></param>
        /// <param name="element_key"></param>
        /// <returns></returns>
        public static AppHelp GetHelpEntry(string scope_key, string element_key)
        {
            if (HelpDB == null)
                return null;

            try
            {
                var help_entry = from help in HelpDB.AppHelp
                                 where help.Scope == scope_key && help.ElementName == element_key
                                 select help;

                if (help_entry.Count() == 1)
                    return help_entry.First();
            }
            catch (Exception)
            {
            }

            AppHelp nodata_found_help = new AppHelp();
            nodata_found_help.Scope = scope_key;
            nodata_found_help.ElementName = element_key;
            nodata_found_help.Title = "Eintrag nicht vorhanden - T";
            nodata_found_help.ShortDescription = "nicht vorhanden - SD";
            return nodata_found_help;
        }

        /// <summary>
        /// Abfragen aller Datenbankeinträge in einem bestimmten Scope
        /// </summary>
        /// <param name="scope_key">Query-Ergebnis der Datenbankabfrage. Da der Scope für alle returnierten Elemente gleich ist, kann
        /// mit dem ElementNamen abgefragt werden. Daher wird dieser im Dictonary als Schlüssel gewählt</param>
        /// <returns></returns>
        public static Dictionary<string, AppHelp> GetHelpEntriesFromScope(string scope_key)
        {
            if (HelpDB == null)
                return null;

            try
            {
                var help_entries = from help in HelpDB.AppHelp
                                   where help.Scope == scope_key
                                   select help;
                if (help_entries.Count() > 0)
                    return help_entries.ToDictionary(v => v.ElementName);
                else
                    return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
         
        #endregion

        #region Static Methods
        public static HelpProvider GetHelpProviderForElement(DependencyObject element)
        {
            return UIHelper.TryFindParent<HelpProvider>(element);
        }
        /// <summary>
        /// Get Value of Dependency Property HelpIndex
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        public static string GetHelpIndex(DependencyObject element)
        {
            return (string)element.GetValue(HelpIndexProperty);
        }

        /// <summary>
        /// Set Value of Dependency Property HelpIndex
        /// </summary>
        /// <param name="element"></param>
        /// <param name="value"></param>
        public static void SetHelpIndex(DependencyObject element, string value)
        {
            element.SetValue(HelpIndexProperty, value);
        }

        /// <summary>
        /// get Value of Dependency Property HelpScope
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        public static string GetHelpScope(DependencyObject element)
        {
            return (string)element.GetValue(HelpScopeProperty);
        }

        /// <summary>
        /// set value of dependency Property HelpScope
        /// </summary>
        /// <param name="element"></param>
        /// <param name="value"></param>
        public static void SetHelpScope(DependencyObject element, string value)
        {
            element.SetValue(HelpScopeProperty, value);
        }

        /// <summary>
        /// zeigt an, ob für ein bestimmtes Element das AttachedProperty HelpScope gesetzt wurde
        /// </summary>
        /// <param name="dp_o">true, wenn HelpScope für das DepenedencyObject gesetzt wurde. False sonst</param>
        /// <returns></returns>
        protected static bool HelpScopeDefined(DependencyObject dp_o)
        {
            if (_attached_property_validation.ContainsKey(dp_o))
            {
                if (_attached_property_validation[dp_o] is string)
                    return true;
            }

            return false;
        }

        /// <summary>
        /// zeigt an, ob für ein bestimmtes Element das AttachedProperty HelpIndex gesetzt wurde
        /// </summary>
        /// <param name="dp_o">true, wenn HelpIndex für das DependencyObject gesetzt wurde. False sonst</param>
        /// <returns></returns>
        protected static bool HelpIndexDefined(DependencyObject dp_o)
        {
            if (_attached_property_validation.ContainsKey(dp_o))
            {
                if (_attached_property_validation[dp_o] is int)
                    return true;
            }

            return false;
        }

        protected static bool HelpDefined(DependencyObject dp_o)
        {
            if (_attached_property_validation.ContainsKey(dp_o))
                return true;

            return false;
        }

        protected static void AddToValidation(DependencyObject dp_o, object value)
        {
            if (_attached_property_validation.ContainsKey(dp_o))
                throw new Exception("Dieses Element kann nicht zur Validation Liste hinzugefügt werden, das es bereits eingetragen ist");
            else
                _attached_property_validation.Add(dp_o, value);
        }

        /// <summary>
        /// Entfernt ein bestimmtes Element wieder aus der Validation Liste. 
        /// </summary>
        /// <param name="dp_o"></param>
        protected static void RemoveFromValidation(DependencyObject dp_o)
        {
            if (_attached_property_validation.ContainsKey(dp_o))
                _attached_property_validation.Remove(dp_o);
        }

        protected static void DisposeDataContext()
        {
            if(_data_context != null)
            {
#if DEBUG
                System.Diagnostics.Debug.WriteLine("*** DISPOSING DataContext of HelpProvider");
#endif
                _data_context.Dispose();
                _data_context = null;
            }
        }
        #endregion

        #region Static EventHandler
        /// <summary>
        /// wenn sich der Wert des Dependency Property verändert. In diesem Fall registriere ich mich auf das Focused Event, wenn das nicht schon passiert ist
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void OnHelpIndexChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (!HelpDefined(d))
            {
                if (d is UIElement)
                {
                    HelpProvider provider = GetHelpProviderForElement(d);
                    if (provider != null && !provider._got_focus_subscribers.Contains(d as UIElement))
                    {
                        ((UIElement) d).GotFocus += new RoutedEventHandler(HelpProvider_GotFocus);
                        provider._got_focus_subscribers.Add(d as UIElement);
                    }
                }
            }
        }
        /// <summary>
        /// wenn sich der Wert des Dependency Property verändert. In diesem Fall registriere ich mich auf das Focused Event, wenn das nicht schon passiert ist
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void OnHelpScopeChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (HelpDefined(d))
            {
                if (d is UIElement)
                {
                    HelpProvider provider = GetHelpProviderForElement(d);
                    if (provider != null && !provider._got_focus_subscribers.Contains(d as UIElement))
                    {
                        ((UIElement) d).GotFocus += new RoutedEventHandler(HelpProvider_GotFocus);
                        provider._got_focus_subscribers.Add(d as UIElement);
                    }
                }
            }
        }
        /// <summary>
        /// Auf ein für die Hilfe-registriertes Element wurde der Focus gesetzt
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        static void HelpProvider_GotFocus(object sender, RoutedEventArgs e)
        {
            Visual dp = null;
            if (sender is Visual)
                dp = (Visual)sender;
            else
                return;

            // inform all HelpProvider Objects of the changed Focus
            if(HelpFocusChanged != null)
                HelpFocusChanged(null, new HelpEventArgs((Visual)dp));

            e.Handled = true;
        }
        #endregion

        #region Static Attributes
        protected static string _local_connection_string;
        protected static string _server_connection_string;
        #endregion

        #region Class Constructor
        public HelpProvider()
        {
            this.LayoutUpdated += new EventHandler(HelpProvider_LayoutUpdated);
            this.MouseUp += new MouseButtonEventHandler(HelpProvider_MouseUp);
            HelpProvider.HelpFocusChanged += HelpProvider_HelpFocusChanged;

            Init();
        }

        /// <summary>
        /// Init class attributes
        /// </summary>
        private void Init()
        {
            _focus_able_objects = new List<Visual>();
        }

        #endregion

        #region Class EventHandler
        /// <summary>
        /// Wenn ein Element den Focus erhält
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void HelpProvider_HelpFocusChanged(object sender, HelpEventArgs e)
        {
            if (_focus_able_objects.Contains(e.RequestedHelpVisual))
                InformHelpOverlay(e.RequestedHelpVisual);
        }

        /// <summary>
        /// Dient zur Unterstützung des FocusEvents, da dieser bei anderen Elementen (TextBlock etc.) nicht geworfen wird.
        /// In diesem Fall wird der Visuelle, als auch der logische Baum durchwandert, bis ein Element mit registrierter Hilfe
        /// gefunden wurde. Das gefunde Element wird anschließend dem Hilfedialog übergeben
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void HelpProvider_MouseUp(object sender, MouseButtonEventArgs e)
        {
            object focused_elem = e.OriginalSource;
            while (!_help_node_dictonary.ContainsKey(focused_elem))
            {
                DependencyObject focused_parent = null;
                if (focused_elem is Visual)
                {
                    focused_parent = VisualTreeHelper.GetParent((Visual)focused_elem);

                }
                else if(focused_elem is DependencyObject)
                {
                    focused_parent = LogicalTreeHelper.GetParent((DependencyObject)focused_elem);
                }
                
                if (focused_parent == null || focused_parent == this)
                    return;

                focused_elem = focused_parent;
            }
            
            InformHelpOverlay(focused_elem);
        }

        /// <summary>
        /// Wenn das Layout zum ersten Mal gerendert wurde, kann der visuelle Baum durchwandert werden, und somit
        /// der Hilfebaum erstellt werden
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void HelpProvider_LayoutUpdated(object sender, EventArgs e)
        {
            LayoutUpdated -= HelpProvider_LayoutUpdated;
            ClearOldTree();
            GenerateHelpTree(_help_tree, this, "", null);
            if (_ref_overlay_help != null)
                _ref_overlay_help.ShowHelpTree(_help_tree);
        }

        private void ClearOldTree()
        {
            _help_node_dictonary.Clear();
            _helpnode_by_elementkey_dictonary.Clear();
            _attached_property_validation.Clear();
        }

        private void ClearHelpNode(HelpNode node)
        {
            if (node == null)
                return;

            foreach (IOneLevelTreeViewItem tree_view_item in node.ChildrenList)
            {
                HelpNode tempNode = tree_view_item as HelpNode;
                if (tempNode != null)
                {
                    ClearHelpNode(tempNode);
                }

                tempNode.Dispose();
                tempNode = null;
            }

            node.Dispose();
            node = null;
        }
        #endregion

        #region Methods
        
        /// <summary>
        /// HelpProvider Objekt kann das zugehörige OverlayHelp Objekt zugewiesen werden.
        /// Nach der Zuweisung wird das Objekt anschließend der zugehörige Baum übergeben
        /// </summary>
        /// <param name="ref_help"></param>
        public void SetHelpOverlay(OverlayHelp ref_help)
        {
            _ref_overlay_help = ref_help;

            if (_ref_overlay_help != null && _help_tree != null)
                _ref_overlay_help.ShowHelpTree(_help_tree);
        }
        
        /// <summary>
        /// Liefert das aktuell zugewiesene OverlayHelp viewer control
        /// </summary>
        /// <returns></returns>
        public OverlayHelp GetHelpOverlay()
        {
            return _ref_overlay_help;
        }

        /// <summary>
        /// Informiert die Hilfedarstellung, dass sich das fokusierte Element geändert hat.
        /// </summary>
        /// <param name="dp"></param>
        private void InformHelpOverlay(object dp)
        {
            if (!_help_node_dictonary.ContainsKey(dp))
                return;

            if (_ref_overlay_help != null)
                _ref_overlay_help.ShowHelpElementAuto(_help_node_dictonary[dp]);
        }

        /// <summary>
        /// Erzeugt ausgehend vom aktuellen Layout den zugehörigen Hilfe-Baum
        /// Der erste Knoten ist der HelpProider Knoten selbst. Anschließend wird 
        /// rekursive nach unten gewandert. 
        /// Ein gesetzter Scope gilt für alle Elemente unterhalb ebenfalls (bis zu dem Element, bei dem er neu gesetzt wird)
        /// Elemente ohne gesetzten HelpIndex, werden nicht in den Baum aufgenommen. 
        /// </summary>
        /// <param name="root"></param>
        /// <param name="v"></param>
        /// <param name="lastscope"></param>
        protected void GenerateHelpTree(HelpNode root, Visual v, string lastscope, Dictionary<string, AppHelp> scope_elements_from_root)
        {
            Dictionary<string,AppHelp> scope_elems = scope_elements_from_root;
            bool scope_root = false;

            if (root == null)
            {
                // first Node of the tree
                _help_tree = root = new HelpNode(root, v, "", "");
                scope_root = true;
            }

            // rekursive den Baum durchwandern
            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(v); i++)
            {
                Visual vis_child = (Visual)VisualTreeHelper.GetChild(v, i);
                string cur_help_index = GetHelpIndex(vis_child);
                string cur_help_scope = GetHelpScope(vis_child);

                if (!String.IsNullOrEmpty(cur_help_scope))
                {
                    if (cur_help_scope != lastscope)
                    {
                        scope_elems = GetHelpEntriesFromScope(cur_help_scope);
                        scope_root = true;
                        if (String.IsNullOrEmpty(root.Scope))
                            root.Scope = cur_help_scope;
                    }
                }
                else
                    cur_help_scope = lastscope;

                if (String.IsNullOrEmpty(cur_help_index) || (String.IsNullOrEmpty(cur_help_scope) && String.IsNullOrEmpty(lastscope)))
                    GenerateHelpTree(root, vis_child, cur_help_scope, scope_elems);
                else
                {
                    HelpNode new_help_node = new HelpNode(root, vis_child, cur_help_scope, cur_help_index);
                    _helpnode_by_elementkey_dictonary.Add(cur_help_index, new_help_node);
                    _focus_able_objects.Add(vis_child);
                    //_help_node_dictonary.Add( vis_child, new_help_node);
                    _help_node_dictonary[vis_child] = new_help_node;

                    root.AddChildNode(new_help_node);
                    if (scope_elems != null && scope_elems.ContainsKey(cur_help_index))
                        scope_elems.Remove(cur_help_index);

                    _help_node_dictonary.Add(cur_help_index, new_help_node);

                    GenerateHelpTree(new_help_node, vis_child, cur_help_scope, scope_elems);
                }
            }

            #region Einträge in der Datenbank mit selben Scope (die im VisualTree nicht gefunden wurden) werden in Hilfebaum aufgenommen
            //TODO: "einfacherer" Algorithmus wäre wahrscheinlich besser (Brainstorming)
            // Nicht aufgenommene Einträge in der Datenbank werden in den Baum aufgenommen (das darf aber nur einmal für jeden Scope durchgeführt werden 
            if (scope_root && scope_elems != null)
            {
                //foreach(AppHelp a_help in scope_elems.Values)
                for (int i = 0; i < scope_elems.Values.Count; i++)
                {
                    // immer das erste element nehmen, da auf jeden Fall eines aus der Liste gelöscht wird
                    AppHelp a_help = scope_elems.ElementAt(0).Value;

                    // Der Parent-Knoten existiert bereits im Baum, daher wird das Element einfach drangehängt
                    if (a_help.Parent_ElementName != null && _helpnode_by_elementkey_dictonary.ContainsKey(a_help.Parent_ElementName))
                    {
                        HelpNode h_node = _helpnode_by_elementkey_dictonary[a_help.Parent_ElementName];
                        HelpNode new_node = new HelpNode(h_node, h_node.RefVisual, h_node.Scope, a_help.ElementName);
                        h_node.AddChildNode(new HelpNode(h_node, h_node.RefVisual, h_node.Scope, a_help.ElementName));
                        _helpnode_by_elementkey_dictonary.Add(a_help.ElementName, new_node);
                        scope_elems.Remove(a_help.ElementName);
                    }
                    // Der Knoten existiert noch nicht im Baum und hat auch keinen definierten Parent => Konten wird unter Root gehängt
                    else if (a_help.Parent_ElementName == null)
                    {
                        HelpNode new_node = new HelpNode(root, root.RefVisual, root.Scope, a_help.ElementName);
                        root.AddChildNode(new_node);
                        _helpnode_by_elementkey_dictonary.Add(a_help.ElementName, new_node);
                        scope_elems.Remove(a_help.ElementName);
                    }
                    // Der Knoten existiert noch nicht im Baum, hat aber einen Parent-Knoten
                    else if (a_help.Parent_ElementName != null)
                    {
                        // Suche den Parent der bereits im Baum vorhanden ist oder den Knoten der keinen Parent hat
                        bool found_next_element = false;
                        string parent_key = a_help.Parent_ElementName;
                        while (!found_next_element)
                        {
                            // dieser parent ist im Baum bereits vorhanden
                            if (_helpnode_by_elementkey_dictonary.ContainsKey(parent_key))
                            {
                                // wenn es das Element auch tatsächlich in den verbleibenden Elementen gibt, wird es hinzugefügt, anderenfalls wird das ursprüngliche aus der Liste entfernt
                                if (scope_elems.ContainsKey(parent_key))
                                {
                                    HelpNode h_node = _helpnode_by_elementkey_dictonary[parent_key];
                                    AppHelp a_help_found = scope_elems[parent_key];
                                    h_node.AddChildNode(new HelpNode(h_node, h_node.RefVisual, h_node.Scope, a_help_found.ElementName));
                                }
                                else
                                    scope_elems.Remove(a_help.ElementName);

                                found_next_element = true;
                            }
                            // der parent ist im Baum noch nicht vorhanden
                            else
                            {
                                // wenn der parent nicht bei verbleibenden Elementen ist, wird dieser gelöscht
                                if (scope_elems.ContainsKey(parent_key))
                                {
                                    // der parent hat wieder einen parent
                                    AppHelp a_help_found = scope_elems[parent_key];
                                    if (a_help_found.Parent_ElementName != null)
                                    {
                                        parent_key = a_help_found.Parent_ElementName;
                                    }
                                    // der parent hat keinen parent mehr, wird daher an die aktuelle Root gehängt. Das zugehörige Element wird anschließend gelöscht
                                    else
                                    {
                                        root.AddChildNode(new HelpNode(root, root.RefVisual, root.Scope, a_help_found.ElementName));
                                        scope_elems.Remove(parent_key);
                                        found_next_element = true;
                                    }
                                }
                                else
                                {
                                    scope_elems.Remove(a_help.ElementName);
                                    found_next_element = true;
                                }
                            }
                        }
                    }
                }
            }
            #endregion
        }

        #endregion

        #region Properties
        /// <summary>
        /// returniert den zuletzt generierten Hilfe-Baum
        /// </summary>
        /// <returns></returns>
        public HelpNode GetHelpTree()
        {
            return _help_tree;
        }
        
        public static event EventHandler<HelpEventArgs> HelpFocusChanged;
        /// <summary>
        /// Definiert die Datenquelle, von der aus die Hilfeeiträge geholt werden sollen
        /// </summary>
        public static System.Data.Linq.DataContext HelpDataContext;

        #endregion

        #region Dependency Properties
        public static DependencyProperty HelpIndexProperty;
        public static DependencyProperty HelpScopeProperty;
        #endregion

        #region IDisposable Implementation
        public void Dispose()
        {
            HelpProvider.HelpFocusChanged -= this.HelpProvider_HelpFocusChanged;
            //UnregisterEvents(HelpProvider.HelpFocusChanged);

            this.LayoutUpdated -= HelpProvider_LayoutUpdated;
            this.MouseUp -= HelpProvider_MouseUp;

            ClearHelpNode(_help_tree);
            _ref_overlay_help = null;
            ClearOldTree();
            _help_tree = null;
            _focus_able_objects.Clear();

            foreach(UIElement got_focus_elem in _got_focus_subscribers)
            {
                got_focus_elem.GotFocus -= HelpProvider_GotFocus;
            }

            _got_focus_subscribers.Clear();
        }

        private void UnregisterEvents(EventHandler<HelpEventArgs> event_handler)
        {
            if (event_handler == null)
                return;

            foreach (EventHandler<HelpEventArgs> eventDelegate in event_handler.GetInvocationList())
                event_handler -= eventDelegate;

        }
        #endregion

        #region Attributes

        protected HelpNode _help_tree;
        protected OverlayHelp _ref_overlay_help;

        protected List<Visual> _focus_able_objects;
        private List<UIElement> _got_focus_subscribers = new List<UIElement>();

        protected static Dictionary<object, HelpNode> _help_node_dictonary = new Dictionary<object, HelpNode>();
        protected static Dictionary<string, HelpNode> _helpnode_by_elementkey_dictonary = new Dictionary<string, HelpNode>();
        protected static Dictionary<DependencyObject, object> _attached_property_validation = new Dictionary<DependencyObject, object>();
        protected static HelpDataContext _data_context = null;
        #endregion

        
    }
}
