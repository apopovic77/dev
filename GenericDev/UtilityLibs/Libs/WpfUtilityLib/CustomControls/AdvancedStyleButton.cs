﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Logicx.WpfUtility.CustomControls
{
    public class AdvancedStyleButton : TouchButton.TouchButton
    {
        public Brush ActiveForegroundBrush
        {
            get { return (Brush)GetValue(ActiveForegroundBrushProperty); }
            set { SetValue(ActiveForegroundBrushProperty, value); }
        }
        public static readonly DependencyProperty ActiveForegroundBrushProperty = DependencyProperty.Register("ActiveForegroundBrush", typeof(Brush), typeof(AdvancedStyleButton), new UIPropertyMetadata(Brushes.Gray));

        public Brush DisabledForegroundBrush
        {
            get { return (Brush)GetValue(DisabledForegroundBrushProperty); }
            set { SetValue(DisabledForegroundBrushProperty, value); }
        }
        public static readonly DependencyProperty DisabledForegroundBrushProperty = DependencyProperty.Register("DisabledForegroundBrush", typeof(Brush), typeof(AdvancedStyleButton), new UIPropertyMetadata(Brushes.Gray));
    }
}
