﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankLib.Data
{
    public class UmsatzTypes
    {
        public static Dictionary<string, string> Values = new Dictionary<string, string>()
        {
            //{/*UmsatzTextOrg#*/"",                         /*UmsatzType*/""},
            //{/*UmsatzTextOrg#*/"",                         /*UmsatzType*/""},
            //{/*UmsatzTextOrg#*/"",                         /*UmsatzType*/""},
            //{/*UmsatzTextOrg#*/"",                         /*UmsatzType*/""},
            //{/*UmsatzTextOrg#*/"",                         /*UmsatzType*/""},
            //{/*UmsatzTextOrg#*/"",                         /*UmsatzType*/""},
            //{/*UmsatzTextOrg#*/"",                         /*UmsatzType*/""},
            //{/*UmsatzTextOrg#*/"",                         /*UmsatzType*/""},
            //{/*UmsatzTextOrg#*/"",                         /*UmsatzType*/""},
            //{/*UmsatzTextOrg#*/"",                         /*UmsatzType*/""},
            //{/*UmsatzTextOrg#*/"",                         /*UmsatzType*/""},
            //{/*UmsatzTextOrg#*/"",                         /*UmsatzType*/""},
            //{/*UmsatzTextOrg#*/"",                         /*UmsatzType*/""},
        };
    }
}


/*
 
A1 SHOP      0075  K1 02.11.UM 11:05
A1 Telekom Austria
Abschluss
Amazon.Services S.A.R.L - EU-AT
AT        5,30 POS 21.11.13 12:00 K1	TOYS 'R' US 3326 KLAGE KLAGENFURT 9029 040
AUTOMAT   00091423 K1 23.11. UM 21.08
BANKOMAT  00003172 K1 01.08.UM 15:05
BILLA DANKT  4270P K1 26.11. UM 11.58
BRL      47,90/30904K + 1,21 309031808K1
CARLO        0001P K1 06.11. UM 12.40
DE        1,99 POS 14.08.13 12:51 K1
DE        3,79 POS 04.09.13 19:14 K1
DE        4,68 POS 14.08.13 12:59 K1
DE      160,00 ATM 27.07.13  13.17 K1
DM-FIL. 0210 0210  K1 03.12.UM 16:49
DM-FIL. 0210 0210  K1 04.12.UM 11:57
DONAU VERSICHERUNG AG
Dr. Kanovsky Werner
ENI 4103     4103  K1 18.11.UM 11:02
EUCO CENTER  0025  K1 29.07.UM 13:50
GAA10104  01510104 K1 07.10 UM 17:07
GIS Gebühren Info Service GmbH
HALLENBAD    0001  K1 13.10.UM 14:37
HOFER DANKT  0614  K1 02.12.UM 17:47
IS REDL      0001  K1 14.10.UM 17:56
JACK & JONES 0032  K1 02.12.UM 11:17
JET 9020     5198P K1 20.09. UM 20.58
KAERNTEN THE 0005  K1 27.10.UM 14:57
KELAG-Kärntner Elektrizitäts- Aktiengesellschaft
KIK 9100     3255P K1 04.12. UM 11.44
KULTERER     0001  K1 06.10.UM 00:27
LH VOELKERMA 2711P K1 17.09. UM 16.28
LIBRO 9100   7960  K1 06.12.UM 17:35
LIBRO 9100   7960  K1 19.10.UM 15:43
LIBRO 9100   7960  K1 19.10.UM 16:07
LIBRO 9100   7960  K1 29.07.UM 15:26
LIBRO 9100   7960P K1 03.12. UM 17.06
LIBRO 9100   7960P K1 04.12. UM 11.28
LIBRO 9100   7960P K1 17.09. UM 15.48
LOGICX consulting & workflow
LUTZ DANKT   8806P K1 21.09. UM 15.19
Martina Popovic und
MCDONALDS    0034P K1 16.10. UM 20.30
MCDONALDS186 0186  K1 20.09.UM 18:41
MEDIA DANKT  0005  K1 12.08.UM 14:27
PLANKENAUER  0004  K1 31.10.UM 10:18
PP WAAGG.    0001  K1 10.10. UM 15.48
PT       70,00 ATM 04.09.13 08:10 K1
PUMPE        0001  K1 07.10.UM 15:44
QUICK-L   20912044 K1 03.08.UM 15:45
REICHART     0001  K1 27.11.UM 12:53
Sozialvers.Anstalt d. gew. Wirtschaft
STAR MOVIE   0001P K1 07.09. UM 20.52
ÜBERWEISUNG INTERNET AM 2013-09-07
UMBUCHUNG   INTERNET AM 2013-08-15
UNSER KIND
VOLKSSCHULE DER HERMAGORAS
WARMBADER TH 0000  K1 27.10.UM 12:58
WERTGARANTIE AG

 
 */
