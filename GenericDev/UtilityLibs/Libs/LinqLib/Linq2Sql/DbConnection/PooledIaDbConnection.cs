﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;

namespace LinqLib.Linq2Sql.DbConnection
{
    public class PooledIaDbConnection : IaDbConnection
    {
        #region constants
        private const int CONNECTION_POOL_SIZE = 10;
        #endregion

        #region construction and initialization
        public PooledIaDbConnection() : base()
        {
            BuildConnectionPool(null);
        }

        public PooledIaDbConnection(string connectionString) : base(connectionString)
        {
            BuildConnectionPool(connectionString);
        }

        public PooledIaDbConnection(int pool_size) : base()
        {
            if (pool_size < 1)
                throw new ArgumentException("pool_size must be >= 1");

            _connection_pool_size = pool_size;
            BuildConnectionPool(null);
        }

        public PooledIaDbConnection(int pool_size, string connectionString) : base(connectionString)
        {
            if (pool_size < 1)
                throw new ArgumentException("pool_size must be >= 1");

            _connection_pool_size = pool_size;

            BuildConnectionPool(connectionString);
        }

#if DEBUG
        public PooledIaDbConnection(bool with_debug) : base(with_debug)
        {
            BuildConnectionPool(null);
        }

        public PooledIaDbConnection(string connectionString, bool with_debug) : base(connectionString, with_debug)
        {
            BuildConnectionPool(connectionString);
        }

        public PooledIaDbConnection(int pool_size, bool with_debug)
            : base(with_debug)
        {
            if (pool_size < 1)
                throw new ArgumentException("pool_size must be >= 1");

            _connection_pool_size = pool_size;

            BuildConnectionPool(null);
        }

        public PooledIaDbConnection(int pool_size, string connectionString, bool with_debug)
            : base(connectionString, with_debug)
        {
            if (pool_size < 1)
                throw new ArgumentException("pool_size must be >= 1");

            _connection_pool_size = pool_size;

            BuildConnectionPool(connectionString);
        }
#endif
        #endregion

        #region operations

        #region Pooling 
        private void BuildConnectionPool(string connectionString)
        {
            _connection_string = connectionString;

            ClearConnectionPool();

            Monitor.Enter(_connection_pool);

            try
            {
                for (int i = 0; i < _connection_pool_size; i++)
                {
                    IaDbConnection pooled_connection = null;

                    if (!string.IsNullOrEmpty(connectionString))
                    {
#if DEBUG
                        pooled_connection = new IaDbConnection(connectionString, WithDebugOutput);
#else
                        pooled_connection = new IaDbConnection(connectionString);
#endif
                    }
                    else
                    {
#if DEBUG
                        pooled_connection = new IaDbConnection(WithDebugOutput);
#else
                        pooled_connection = new IaDbConnection();
#endif
                    }

                    pooled_connection.Name = "PoolPos" + i.ToString();

                    _connection_pool.Push(pooled_connection);
                }
            }
            finally
            {
                Monitor.Exit(_connection_pool);
            }
        }

        private void ClearConnectionPool()
        {
            Monitor.Enter(_connection_pool);

            try
            {
                foreach (IaDbConnection con in _connection_pool)
                {
                    if (con != null && con.State != ConnectionState.Closed)
                        con.Close();

                    if (con != null)
                        con.ForceDispose();
                }

                foreach (IaDbConnection con in _execution_pool)
                {
                    if (con != null && con.State != ConnectionState.Closed)
                        con.Close();

                    if (con != null)
                        con.ForceDispose();
                }

                _connection_pool.Clear();
                _execution_pool.Clear();
            }
            finally
            {
                Monitor.Exit(_connection_pool);
            }
        }

        public IaDbConnection RequestPooledConnection()
        {
            return RequestPooledConnection(false);
        }

        public IaDbConnection RequestPooledConnection(bool auto_open)
        {
            Monitor.Enter(_connection_pool);

            IaDbConnection pooledCon = null;

            try
            {
                if(_connection_pool.Count > 0)
                {
                    pooledCon = _connection_pool.Pop();
                    _execution_pool.Add(pooledCon);
                }
            }
            finally
            {
                Monitor.Exit(_connection_pool);
            }

            if (pooledCon != null)
            {
                if (auto_open && (pooledCon.State == System.Data.ConnectionState.Closed ||
                    pooledCon.State == System.Data.ConnectionState.Broken))
                    pooledCon.Open();
                    
                return pooledCon;
            }

            if (auto_open && (State == System.Data.ConnectionState.Closed ||
                State == System.Data.ConnectionState.Broken))
                Open();

            return this;
        }

        public void ReleasePooledConnection(IaDbConnection pooledConnection)
        {
            ReleasePooledConnection(pooledConnection, true);
        }

        public void ReleasePooledConnection(IaDbConnection pooledConnection, bool auto_close)
        {
            Monitor.Enter(_connection_pool);

            bool fromPool = false;

            try
            {
                if(_execution_pool.Contains(pooledConnection))
                {
                    _execution_pool.Remove(pooledConnection);
                    _connection_pool.Push(pooledConnection);

                    if (auto_close && pooledConnection.State != System.Data.ConnectionState.Closed)
                        pooledConnection.Close();

                    fromPool = true;
                }
            }
            finally
            {
                Monitor.Exit(_connection_pool);
            }

            if (!fromPool && auto_close && State != System.Data.ConnectionState.Closed)
                Close();
        }
        #endregion

        #region DbConnection implementation
        protected override DbTransaction BeginDbTransaction(IsolationLevel isolationLevel)
        {
            throw new NotSupportedException("Transactions not supported on pooled connections!");
        }

        public override void ChangeDatabase(string databaseName)
        {
            // change database on all available pooled connections
            Monitor.Enter(_connection_pool);

            try
            {
                foreach (IaDbConnection con in _connection_pool)
                    con.ChangeDatabase(databaseName);
            }
            finally
            {
                Monitor.Exit(_connection_pool);
            }
            base.ChangeDatabase(databaseName);
        }

        //public override string ConnectionString
        //{
        //    get
        //    {
        //        return _connection_string;
        //    }
        //    set
        //    {
        //        if(_connection_string != value)
        //        {
        //            Monitor.Enter(_connection_pool);
        //            _connection_string = value;
                    
        //            try
        //            {
        //                foreach (IaDbConnection con in _connection_pool)
        //                    con.ConnectionString = value;
        //            }
        //            finally
        //            {
        //                Monitor.Exit(_connection_pool);
        //            }
        //        }

        //        base.ConnectionString = value;
        //    }
        //}

        //public override int ConnectionTimeout
        //{
        //    get
        //    {
        //        int nRet = 0;

        //        Monitor.Enter(_connection_pool);

        //        try
        //        {
        //            if (_connection_pool.Count > 0)
        //                nRet = _connection_pool.ElementAt(0).ConnectionTimeout;
        //            else if (_execution_pool.Count > 0)
        //                nRet = _execution_pool.ElementAt(0).ConnectionTimeout;
        //        }
        //        finally
        //        {
        //            Monitor.Exit(_connection_pool);
        //        }

        //        return nRet;
        //    }
        //}

        protected override DbCommand CreateDbCommand()
        {
            return base.CreateDbCommand();
        }

        protected override void Dispose(bool disposing)
        {
            //ClearConnectionPool();
            //base.Dispose(disposing);
        }

        public new void ForceDispose()
        {
            ClearConnectionPool();
            base.ForceDispose();
        }

        //public override void Open()
        //{
        //}

        //public override void Close()
        //{
        //}

        #endregion

        #endregion

        #region properties
        public int PoolSize
        {
            get { return _connection_pool_size; }
        }

        public int FreeConnections
        {
            get
            {
                if(Monitor.TryEnter(_connection_pool))
                {
                    int nRet = _connection_pool.Count;
                    Monitor.Exit(_connection_pool);
                    return nRet;
                }

                return PoolSize;
            }
        }

        public int InUseConnections
        {
            get
            {
                if (Monitor.TryEnter(_connection_pool))
                {
                    int nRet = _execution_pool.Count;
                    Monitor.Exit(_connection_pool);
                    return nRet;
                }

                return 0;
            }
        }

        public override string DebugIdentifier
        {
            get
            {
                return "###PC" + this.GetHashCode().ToString();
            }
        }
        #endregion

        #region attributes

        private int _connection_pool_size = CONNECTION_POOL_SIZE;
        private string _connection_string = "";
        private Stack<IaDbConnection> _connection_pool = new Stack<IaDbConnection>();
        private List<IaDbConnection> _execution_pool = new List<IaDbConnection>();
        private List<IaDbCommand> _generated_commands = new List<IaDbCommand>();
        #endregion
    }
}
