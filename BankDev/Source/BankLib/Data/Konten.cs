﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankLib.Data
{
    public class Konto
    {
        public float CurrentKtoStand
        {
            get
            {
                return Konten.GetGiroKontoStand(Owner);
            }
        }

        public string Owner { get; set; }
        public bool IsCreditCard { get; set; }
        public string KtoNummer { get; set; }
        public float AnfangsKtoStand { get; set; }
        public string VerfuegerName { get; set; }
        public string VerfuegerNr { get; set; }
        public string Pin { get; set; }
        public string Email { get; set; }
        /// <summary>
        /// hab das prob eigentlich derweil nur eingeführt wegen der einfärbung von buchungszeilen im wpf, alle von main konto erhalten eine eigene farbe
        /// </summary>
        public bool IsMainKonto { get; set; }

        /// <summary>
        /// dieser wert wird eingetragen vom selenium nach dem webupdate, dieser wert entspricht dem wert der auf der hp angezeigt wird.
        /// </summary>
        public float? GiroKtoStandWeb { get; set; }
    }

    public class Konten
    {
        public static float GetGiroKontoStand(string owner)
        {
            Konto kto = GetKontoByOwner(owner);
            return Buchungen.AlleBuchungen.Where(b => b.Owner == owner).Sum(b => b.Betrag) + kto.AnfangsKtoStand;
        }

        public static float GetGiroKontoStandMonat(string owner, int month, int year)
        {
            Konto kto = GetKontoByOwner(owner);
            return Buchungen.AlleBuchungen.Where(b => b.Owner == owner && b.BuchungsDatum.Month == month && b.BuchungsDatum.Year == year).Sum(b => b.Betrag);
        }

        public static List<Konto> AlleKonten = new List<Konto>()
        {
            new Konto(){Owner="Alex", IsMainKonto = true, Email="apopovic.aut@gmail.com", IsCreditCard = false, KtoNummer = "540609", AnfangsKtoStand = -4513.29f-543.12f, VerfuegerName = "231380", VerfuegerNr = "batman01", Pin="#1inetpass"},
            new Konto(){Owner="Martina", Email="mpopovic.klu@gmail.com", IsCreditCard = false, KtoNummer = "1562045", AnfangsKtoStand = -2283.98f+3701.08f, VerfuegerName = "146992", VerfuegerNr = "firstaid", Pin="220627m"},

        };

        public static Konto GetKontoByKtoNr(string kto_nr)
        {
            Konto kto = Konten.AlleKonten.FirstOrDefault(k => k.KtoNummer == kto_nr);
            if (kto == null)
                throw new Exception("Konto vom Owner " + kto_nr + " nicht gefunden, bzw. Owner nicht regisitriert.");
            return kto;
        }
        public static Konto GetKontoByOwner(string owner)
        {
            Konto kto = Konten.AlleKonten.FirstOrDefault(k => k.Owner == owner);
            if (kto == null)
                throw new Exception("Konto vom Owner " + owner + " nicht gefunden, bzw. Owner nicht regisitriert.");
            return kto;
        }
    }
}
