﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CognitiveLib.Scanner;
using CognitiveLib.WebSearch;

namespace CognitiveLib.Detector
{


    public class SerialDetectionResult
    {
        public WeightedString SerialNo
        {
            get
            {
                if(SerialNoCandidates != null && SerialNoCandidates.Count > 0)
                    return SerialNoCandidates.MaxBy(o => o.Weight);

                return new WeightedString();
            }
        }
        public List<WeightedString> SerialNoCandidates;
    }

    public class DeviceSerialDetector : DeviceCodeDetector
    {
        public static SerialDetectionResult Detect(string text)
        {
            if (string.IsNullOrEmpty(text))
                throw new Exception("cannot detect, no text given");

            // die ordnung der liste spielt eine rolle
            List<WeightedString> look_for = new List<WeightedString>(5);
            look_for.Add(new WeightedString("S/N", 2));
            look_for.Add(new WeightedString("S / N", 2));
            look_for.Add(new WeightedString("S I N", 2));
            look_for.Add(new WeightedString("SIN", 1));
            look_for.Add(new WeightedString("SER. NO.", 10));
            look_for.Add(new WeightedString("SER . NO.", 10));
            look_for.Add(new WeightedString("SER . NO", 3));
            look_for.Add(new WeightedString("SN", 0.1f));
            look_for.Add(new WeightedString("Serial No", 10));
            look_for.Add(new WeightedString("SerialNo", 10));
            look_for.Add(new WeightedString("Serial", 1));

            SerialDetectionResult sres = new SerialDetectionResult();
            sres.SerialNoCandidates = GetDeviceCodeCandidates(text, look_for, DeviceCodeDetector.GetBlackList(new List <string> { "No" }), 5, 30, 3, 0);
            return sres;
        }
    }
}
