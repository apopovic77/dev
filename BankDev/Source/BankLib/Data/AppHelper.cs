﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace BankLib.Data
{
    public class AppHelper
    {
        public static void WriteFileLog(string msg)
        {
            try
            {
                if (!File.Exists(PathToLogFile))
                    using (File.CreateText(PathToLogFile))
                    {
                    }

                using (StreamWriter sw = File.AppendText(PathToLogFile))
                {
                    sw.Write(msg);
                }
            }
            catch
            {
            }
        }

        public static void SendEmail(string subject, List<Konto> to, string from, string s_body)
        {
            MailMessage msg = new MailMessage();
            msg.Subject = subject;
            msg.Body = s_body;
            foreach (Konto kto in to)
            {
                msg.To.Add(new MailAddress(kto.Email));
            }
            msg.From = new MailAddress(from);
            msg.IsBodyHtml = true;

            SmtpClient client = new SmtpClient();
            client.Host = "smtp.gmail.com";
            client.Port = 587;
            client.UseDefaultCredentials = false;
            client.Credentials = new NetworkCredential("apopovic.aut", "#1inetpass");
            client.EnableSsl = true;
            client.Send(msg);
        }

        public static string ReadLogFile()
        {
            try
            {
                if (!File.Exists(PathToLogFile))
                    return "";


                using (FileStream fs = File.OpenRead(PathToLogFile))
                {
                    using (StreamReader sr = new StreamReader(fs))
                    {
                        return sr.ReadToEnd();
                    }
                    
                }
            }
            catch
            {
            }
            return "";
        }

        public static string LoadTextFile(string path)
        {
            if (!File.Exists(path))
                return null;

            using (StreamReader sr = File.OpenText(path))
            {
                return sr.ReadToEnd();
            }
        }


        public static void SaveTextFile(string path, string content)
        {
            if(File.Exists(path))
                File.Delete(path);

            using (StreamWriter sw = File.CreateText(path))
            {
                sw.Write(content);
            }
        }

        public const string PathToLogFile = PathToBankData + "logfile.txt";
        public const string PathToBankData = @"C:\Dev\BankDev\Release\BankData\";

    }
}
