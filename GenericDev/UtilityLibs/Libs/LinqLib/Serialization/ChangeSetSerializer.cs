﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Reflection;
using System.Runtime.Serialization;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using System.IO;

using LinqLib.Reflection;

namespace LinqLib.Serialization
{
    public class ChangeSetSerializer
    {
        #region ChangeSetDataItem nested class
        /// <summary>
        /// Nested private class which holds information about the Containing data item
        /// </summary>
        public class ChangeSetDataItem : IXmlSerializable
        {
            /// <summary>
            /// the full qualified type name
            /// </summary>
            [XmlAttribute]
            public string QualifiedTypeName
            {
                get { return _qualified_type_name; }
                set { _qualified_type_name = value; }
            }

            /// <summary>
            /// Data of type QualifiedTypeName
            /// </summary>
            [XmlElement]
            public object Data
            {
                get { return _data; }
                set
                {
                    _data = value;

                    if (_data != null)
                    {
                        if (_data.GetType().AssemblyQualifiedName != _qualified_type_name)
                            _qualified_type_name = _data.GetType().AssemblyQualifiedName;
                    }
                }
            }

            /// <summary>
            /// Gets/Sets the data type
            /// </summary>
            [XmlIgnore]
            public System.Type DataType
            {
                get
                {
                    return Type.GetType(_qualified_type_name);
                }
                set
                {
                    if (value != null)
                        QualifiedTypeName = value.AssemblyQualifiedName;
                }
            }

            /// <summary>
            /// Gets/Sets a data association object used while deserialising data
            /// </summary>
            [XmlIgnore]
            internal object DataObjectAssociation
            {
                get { return _data_association; }
                set { _data_association = value; }
            }

            /// <summary>
            /// Gets/Sets a flag which indicates if the Data parameter
            /// represents an associated object which is a part of
            /// another objects item collection.
            /// (for example: Order_Detai object may be an associated 
            /// item to an Order object). These items will be ignored
            /// for InsertOnSubmit() calls because they will be inserted
            /// through object association to their parents.
            /// </summary>
            internal bool IsAssosicatedObject
            {
                get { return _is_associated_object; }
                set { _is_associated_object = value; }
            }
            /// <summary>
            /// Gets the XmlSerialization-Schema of the current object
            /// </summary>
            /// <returns></returns>
            public XmlSchema GetSchema()
            {
                // not used
                return null;
            }

            /// <summary>
            /// Manually reads in the current object's data
            /// </summary>
            /// <param name="reader">current reader instance</param>
            public void ReadXml(XmlReader reader)
            {
                // Read attributes
                _qualified_type_name = reader.GetAttribute("QualifiedTypeName");

                // advance to first child note ( Data )
                reader.Read();

                // Deserialize Data depending on the object definition from above
                DataContractSerializer dcs = new DataContractSerializer(DataType);
                Data = dcs.ReadObject(reader);

                // Advance to next object's node !
                reader.Read();
            }

            /// <summary>
            /// Manually write current object's data
            /// </summary>
            /// <param name="writer">current writer instance</param>
            public void WriteXml(XmlWriter writer)
            {
                // create serializer instance for data object
                XmlSerializer ser = new XmlSerializer(this.DataType);

                // Write node attributes
                writer.WriteAttributeString("QualifiedTypeName", _qualified_type_name);

                // serialize dynmaic object
                //ser.Serialize(writer, Data);

                DataContractSerializer dcs = new DataContractSerializer(DataType);
                dcs.WriteObject(writer, Data);
            }

            #region Attribs

            private string _qualified_type_name = "";
            private object _data = null;
            private object _data_association = null;
            private bool _is_associated_object = false;
            #endregion
        }
        #endregion

        #region ChangeSetEx nested class
        /// <summary>
        /// nested class which holds an extended ChangeSet representation needed
        /// for some serialization functions
        /// </summary>
        public class ChangeSetEx
        {
            #region Construction and initialisation
            /// <summary>
            /// Default constructor needed for deserialization
            /// </summary>
            public ChangeSetEx()
            {

            }

            /// <summary>
            /// Constructor of the class
            /// </summary>
            /// <param name="cs">Base standard ChangeSet from DataContext</param>
            public ChangeSetEx(ChangeSet cs)
            {
                if (cs.Inserts != null)
                {
                    _inserts = new List<ChangeSetDataItem>();
                    foreach (object obj in cs.Inserts)
                    {
                        ChangeSetDataItem itm = new ChangeSetDataItem();
                        itm.Data = obj;

                        _inserts.Add(itm);
                    }
                }

                if (cs.Updates != null)
                {
                    _updates = new List<ChangeSetDataItem>();
                    foreach (object obj in cs.Updates)
                    {
                        ChangeSetDataItem itm = new ChangeSetDataItem();
                        itm.Data = obj;

                        _updates.Add(itm);
                    }
                }

                if (cs.Deletes != null)
                {
                    _deletes = new List<ChangeSetDataItem>();
                    foreach (object obj in cs.Deletes)
                    {
                        ChangeSetDataItem itm = new ChangeSetDataItem();
                        itm.Data = obj;

                        _deletes.Add(itm);
                    }
                }
            }
            #endregion

            #region Serialization
            /// <summary>
            /// Serializes the current instance to a file
            /// </summary>
            /// <param name="fileName"></param>
            public void Serialize(string fileName)
            {
                StreamWriter writer = File.CreateText(fileName);

                XmlSerializer serializer = new XmlSerializer(typeof(ChangeSetEx));
                serializer.Serialize(writer, (ChangeSetEx)this);

                writer.Close();
            }

            /// <summary>
            /// Serializes the current instance to a string
            /// </summary>
            /// <returns></returns>
            public override string ToString()
            {
                StringWriter writer = new StringWriter();

                XmlSerializer serializer = new XmlSerializer(typeof(ChangeSetEx));
                serializer.Serialize(writer, (ChangeSetEx)this);

                writer.Close();

                return writer.ToString();
            }
            #endregion

            #region Deserialization
            /// <summary>
            /// Deserializes a ChangeSetEx object from a file
            /// </summary>
            /// <param name="fileName">file name to read the serialized changeset</param>
            /// <returns>New changeSet instance based on the file</returns>
            public static ChangeSetEx Deserialize(string fileName)
            {
                // if the file doesn't exist return a null value
                if (!File.Exists(fileName))
                    return null;

                FileInfo fi = new FileInfo(fileName);

                if (fi.Length <= 0)
                    return null;

                StreamReader reader = File.OpenText(fileName);

                ChangeSetEx data = null;

                try
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(ChangeSetEx));
                    data = (ChangeSetEx)serializer.Deserialize(reader);
                }
                catch (Exception)
                {
                    
                }
                finally
                {
                    reader.Close();

                    try
                    {
                        if (data == null)
                            File.Delete(fileName);
                    }
                    catch
                    {
                    }
                }

                

                return data;
            }

            /// <summary>
            /// Deserializes a ChangeSetEx object from an Xml data string
            /// </summary>
            /// <param name="dataString">Xml data string containing the changeset data to deserialize</param>
            /// <returns>New changeset instance based of the contents of the string</returns>
            public static ChangeSetEx DeserializeFromString(string dataString)
            {
                // if no string data is present, return an empty changeset
                if (string.IsNullOrEmpty(dataString))
                    return new ChangeSetEx();

                StringReader reader = new StringReader(dataString);

                XmlSerializer serializer = new XmlSerializer(typeof(ChangeSetEx));
                ChangeSetEx data = (ChangeSetEx)serializer.Deserialize(reader);

                reader.Close();

                return data;
            }
            #endregion

            #region Data reconstruction and checks
            /// <summary>
            /// Merges all Inserts, Updates and Deletes from the inChangeSet to the current instance
            /// </summary>
            /// <param name="inChangeSet">ChangeSetex instance which should be merged into the current one</param>
            internal void Merge(ChangeSetEx inChangeSet)
            {
                foreach(ChangeSetDataItem iData in inChangeSet.Inserts)
                {
                    // if this obejct isn't already part of the changeset
                    if(GetDataItemPerId(((IdentifyableObjectBase)iData.Data).SerializedObjectId) == null)
                    {
                        Inserts.Add(iData);
                    }
                }

                foreach (ChangeSetDataItem iData in inChangeSet.Updates)
                {
                    // if this obejct isn't already part of the changeset
                    if (GetDataItemPerId(((IdentifyableObjectBase)iData.Data).SerializedObjectId) == null)
                    {
                        Updates.Add(iData);
                    }
                }

                foreach (ChangeSetDataItem iData in inChangeSet.Deletes)
                {
                    // if this obejct isn't already part of the changeset
                    if (GetDataItemPerId(((IdentifyableObjectBase)iData.Data).SerializedObjectId) == null)
                    {
                        Deletes.Add(iData);
                    }
                }
            }
            /// <summary>
            /// Scans through all change set lists and returns the item
            /// which holds a data object with the specified object id
            /// </summary>
            /// <param name="objectId"></param>
            private ChangeSetDataItem GetDataItemPerId(Guid objectId)
            {
                foreach (ChangeSetDataItem curItem in Inserts)
                {
                    IdentifyableObjectBase data = curItem.Data as IdentifyableObjectBase;

                    if (data != null)
                    {
                        if (data.SerializedObjectId.Equals(objectId))
                            return curItem;
                    }
                }

                foreach (ChangeSetDataItem curItem in Updates)
                {
                    IdentifyableObjectBase data = curItem.Data as IdentifyableObjectBase;

                    if (data != null)
                    {
                        if (data.SerializedObjectId.Equals(objectId))
                            return curItem;
                    }
                }

                foreach (ChangeSetDataItem curItem in Deletes)
                {
                    IdentifyableObjectBase data = curItem.Data as IdentifyableObjectBase;

                    if (data != null)
                    {
                        if (data.SerializedObjectId.Equals(objectId))
                            return curItem;
                    }
                }

                return null;
            }

            /// <summary>
            /// Reconstructs a changeset on a given DataContext
            /// </summary>
            /// <param name="toDC">Destination DataContext</param>
            internal void ReconstructChangeSet(DataContext toDC)
            {
                ReconstructInserts(toDC);
                ReconstructUpdates(toDC);
                ReconstructDeletes(toDC);
            }

            /// <summary>
            /// Reconstructs the inserts of the changeset on a given destination DataContext
            /// </summary>
            /// <param name="toDC">Destination DataContext</param>
            private void ReconstructInserts(DataContext toDC)
            {
                // STEP 1: iterate through all Insert objects and create
                // NEW corresponding objects which are attached to the 
                // destination data context
                foreach (ChangeSetDataItem data in Inserts)
                {
                    // read the datatype
                    Type dataType = data.DataType;

                    // create a NEW instance of the type
                    object newDataObject = Activator.CreateInstance(dataType);

                    // copy all the public, serialized column properties
                    // from the deserialized object to the new one
                    CopySerializedColumnPropertiesForInsert(toDC, data.Data, newDataObject);

                    // save the new object in the current ChangeSetdataItem instance
                    data.DataObjectAssociation = newDataObject;
                }

                // STEP 2: iterate through all Insert objects and
                // inspect objects with have a serialized EntitySet propertie.
                // Go through the entries in these properties and rebuild the
                // associations at the newly created objects
                foreach (ChangeSetDataItem data in Inserts)
                {
                    // read the datatype
                    Type dataType = data.DataType;
                    // get the correct type by resolving inherited types
                    dataType = LinqReflectionHelper.ResolveTableInheritanceType(toDC, dataType);
                    // get a reference to the table for the deserialized object
                    ITable table = toDC.GetTable(dataType);

                    if (table != null)
                    {
                        // if the current data type has at least one 
                        // property with a serialized EntitySet
                        if (LinqReflectionHelper.HasSerializedEntitiySetProperty(dataType))
                        {
                            // get a list of all properties which are serialized
                            // EntitySet<T> generics
                            List<PropertyInfo> entitySetProperties = LinqReflectionHelper.GetSerializedEntitiySetProperties(dataType);

                            // loop through all these properties
                            foreach (PropertyInfo esProperty in entitySetProperties)
                            {
                                // type of the property which is EntitySet<T>
                                Type propertyType = esProperty.PropertyType;
                                // get the <T> type of the EntitySet<T> property type
                                // which represents the children type of the entity set
                                // we don't need to check if the current property is a generic
                                // because LinqReflectionHelper.GetSerializedEntitiySetProperties()
                                // already did this.
                                Type entitySetChildrenType = propertyType.GetGenericArguments()[0];

                                // get an instance reference to the property of the
                                // deserialized object and the one created in SET 1
                                object serializedObjProperty = esProperty.GetValue(data.Data, null);
                                object newObjProperty = esProperty.GetValue(data.DataObjectAssociation, null);

                                // gets the method info object of the GetEnumerator() method
                                // of the enumerable type
                                MethodInfo miEnumerator = LinqReflectionHelper.GetEnumeratorMethodInfo(serializedObjProperty.GetType());
                                // set the generic datatypes of the generic method
                                //miEnumerator = miEnumerator.MakeGenericMethod(new Type[] { entitySetChildrenType });

                                // invoke the GetEnumerator() method of the table object
                                object obEnumerator = miEnumerator.Invoke(serializedObjProperty,
                                                                         new object[]
                                                                             {
                                                                             }
                                                                   );
                                // obRet = IEnumerator<T> instance
                                if (obEnumerator != null)
                                {
                                    // get the method info of the MoveNext method of the enumerator
                                    MethodInfo miMoveNext = LinqReflectionHelper.GetIEnumeratorMoveNextMethodInfo(obEnumerator.GetType());
                                    // get the property info of the Current property of the enumerator
                                    PropertyInfo piCurrent = LinqReflectionHelper.GetIEnumeratorCurrentPropertyInfo(obEnumerator.GetType());
                                    // get the method info of the Add method of the EntitySet<T>
                                    MethodInfo miAdd = LinqReflectionHelper.GetEntitySetAddMethodInfo(newObjProperty.GetType());

                                    // iterate through the enumeration
                                    // as long as MoveNext returns true there are elements in the enumeration
                                    while ((bool)miMoveNext.Invoke(obEnumerator, new object[] { }))
                                    {
                                        // gets the current object of the enumerator
                                        object currData = piCurrent.GetValue(obEnumerator, null);

                                        if (currData != null)
                                        {
                                            // try to cast the current object to an identifyable object
                                            IdentifyableObjectBase currIdentifyableData = currData as IdentifyableObjectBase;

                                            if (currIdentifyableData != null)
                                            {
                                                // get the Id of the object
                                                Guid objectId = currIdentifyableData.SerializedObjectId;

                                                // get the deserialized ChangeSetDataItem with the object Id
                                                ChangeSetDataItem assObject = GetDataItemPerId(objectId);

                                                if (assObject != null)
                                                {
                                                    // Add the associated data item to the new created instance
                                                    // (see STEP 1)
                                                    miAdd.Invoke(newObjProperty,
                                                                 new object[]
                                                                     {
                                                                         assObject.DataObjectAssociation
                                                                     });

                                                    // mark the associated object item as association
                                                    assObject.IsAssosicatedObject = true;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                // STEP 3: iterate through all Inserts and mark
                // not associated objects for InsertOnSubmit()
                foreach (ChangeSetDataItem data in Inserts)
                {
                    // read the datatype
                    Type dataType = data.DataType;
                    // get the correct type by resolving inherited types
                    dataType = LinqReflectionHelper.ResolveTableInheritanceType(toDC, dataType);
                    // get a reference to the table for the deserialized object
                    ITable table = toDC.GetTable(dataType);

                    if (!data.IsAssosicatedObject)
                    {
                        List<PropertyInfo> foreinKeyProperties = LinqReflectionHelper.GetAssociationProperties(dataType, true);

                        if (table != null)
                        {
                            if (foreinKeyProperties != null && foreinKeyProperties.Count > 0)
                            {
                                // if the current data object's table contains foreign key associations
                                // add the current data object to the foreign key - objects
                                // do this to have the new association objects available in the datacontext object instances
                                foreach (PropertyInfo fkProperty in foreinKeyProperties)
                                {
                                    // get the association key parameters
                                    string thisKeyName = LinqReflectionHelper.GetAssociationAttributeThisKeyName(fkProperty);
                                    string otherKeyName = LinqReflectionHelper.GetAssociationAttributeOtherKeyName(fkProperty);

                                    // data type of the foreign table
                                    Type foreignTableType = fkProperty.PropertyType;

                                    // get a property info reference to the search property in the forein table data type
                                    PropertyInfo otherSearchProperty = LinqReflectionHelper.GetPropertyInfo(foreignTableType, otherKeyName);

                                    // the property info instance which will be used to identify the search value
                                    // by default this is the current key property but if we have an original value
                                    // property configured for the key value, this property will be used
                                    // to retrieve the value for searching
                                    PropertyInfo thisSearchProperty = LinqReflectionHelper.GetPropertyInfo(dataType, thisKeyName);

                                    // get the property of the DataContext which is represinting the table of the given type
                                    PropertyInfo tableProp = LinqReflectionHelper.GetTablePropertyOfDataContext(foreignTableType, toDC);

                                    // get the instance of the roperty representing the table
                                    object tableInstance = tableProp.GetValue(toDC, null);


                                    // get the SingleOrDefault method for calling a data filter
                                    MethodInfo miSingleOrDefault = LinqReflectionHelper.GetSingleOrDefaultWithPredicateMethodInfo();
                                    // set the generic datatypes of the generic method
                                    miSingleOrDefault = miSingleOrDefault.MakeGenericMethod(new Type[] { foreignTableType });

                                    // gets the method info of the static method which returns the this-/foreign key check
                                    // predicate function which will be used to search for the related object
                                    MethodInfo miTFKeyPredicate = LinqReflectionHelper.GetThisKeyForeignKeySearchPredicate();
                                    // set the generic datatypes of the generic method
                                    miTFKeyPredicate = miTFKeyPredicate.MakeGenericMethod(new Type[] { foreignTableType });

                                    // invoke the static method which returns the predicate function
                                    var o2 = miTFKeyPredicate.Invoke(null, new object[]
                                                                                   {
                                                                                       otherSearchProperty, // this property will be used to get the compare value of the list item
                                                                                       thisSearchProperty, // this property will be used to get the compare value out of the Source object
                                                                                       data.DataObjectAssociation
                                                                                   });

                                    // invoke the SingleOrDefault() method of the table object
                                    object obRet = miSingleOrDefault.Invoke(tableInstance,
                                                                            new object[]
                                                                                    {
                                                                                        tableInstance,
                                                                                        o2
                                                                                    }
                                                                );

                                    // obRet contains the foreign object
                                    if (obRet != null)
                                    {
                                        // search all serialized EntitySet<T> properties of the foreign type
                                        List<PropertyInfo> serializedEntitySets = LinqReflectionHelper.GetSerializedEntitiySetProperties(foreignTableType);

                                        foreach (PropertyInfo esProperty in serializedEntitySets)
                                        {
                                            Type propertyType = esProperty.PropertyType;

                                            if (propertyType.IsGenericType)
                                            {
                                                // get a type reference to the generic EntitySet<T> type of Linq
                                                Type genericEntitySet = typeof(System.Data.Linq.EntitySet<>);
                                                // make a generic type using the current objects table DataType as the current propertie's generic data type
                                                Type checkType = genericEntitySet.MakeGenericType(dataType);

                                                if (checkType == propertyType)
                                                {
                                                    // if the EntitySet<T> holds a list of items of the same type as the current item
                                                    object destObjESProperty = esProperty.GetValue(obRet, null);

                                                    if (destObjESProperty != null)
                                                    {
                                                        // get the method info of the Add method of the EntitySet<T>
                                                        MethodInfo miAdd = LinqReflectionHelper.GetEntitySetAddMethodInfo(destObjESProperty.GetType());

                                                        // Add the associated data item to the new created instance
                                                        // (see STEP 1)
                                                        miAdd.Invoke(destObjESProperty,
                                                                     new object[]
                                                                         {
                                                                             data.DataObjectAssociation
                                                                         });
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                // insert the instances created in STEP 1
                                // this will attach the object to the data context
                                table.InsertOnSubmit(data.DataObjectAssociation);
                            }
                        }
                    }
                    //else
                    //{
                    //    // new association items have to be attached to the data context
                    //    // this is necessary to let the data context keep track of these objects
                    //    // during life time
                    //
                    //    // FAILS if inserting entitys with duplicate key values
                    //    // which may happen very often on SQL tables with identity columns
                    //    AttachObjectToDataContext(toDC, data.DataObjectAssociation);
                    //}
                }
            }

            /// <summary>
            /// Reconstructs the updates of the changeset on a given destination DataContext
            /// </summary>
            /// <param name="toDC">Destination DataContext</param>
            private void ReconstructUpdates(DataContext toDC)
            {
                // STEP 1: iterate through all Updates objects and
                // copy the values to the corresponding existing objects
                foreach (ChangeSetDataItem data in Updates)
                {
                    //if (!data.IsAssosicatedObject)
                    {
                        // find the data object to update in the current DataContext
                        object updateObject = FindObjectInDC(data.Data, toDC);

                        // if an object was found
                        if (updateObject != null)
                        {
                            // copy all the public, serialized column properties
                            // from the deserialized object to one found in the data context
                            CopySerializedColumnPropertiesForUpdate(data, updateObject, toDC);

                            // save the new object in the current ChangeSetdataItem instance
                            data.DataObjectAssociation = updateObject;
                        }
                    }
                }
            }


            /// <summary>
            /// Reconstructs the deletes of a changeset on a given destination DataContext
            /// </summary>
            /// <param name="toDC">Destination DataContext</param>
            private void ReconstructDeletes(DataContext toDC)
            {
                foreach (ChangeSetDataItem data in Deletes)
                {
                    // get the data type of the item
                    Type dataType = data.Data.GetType();

                    // get the correct type by resolving inherited types
                    dataType = LinqReflectionHelper.ResolveTableInheritanceType(toDC, dataType);

                    // get a reference to the table for the deserialized object
                    ITable table = toDC.GetTable(dataType);

                    if (table != null)
                    {
                        try
                        {
                            table.Attach(data.Data);
                        }
                        catch
                        {
                            // will throw an exception if the data object is already
                            // existend at the data context. catch it here - we donÄt care about this error
                        }
                        // delete the 
                        table.DeleteOnSubmit(data.Data);
                    }

                    #region old - very slow (FindObjectInDC() )
                    //// finds the attached data item in the current data context
                    //// using primary keys to identify the object
                    //object deleteObject = FindObjectInDC(data.Data, toDC);

                    //// if an object was found in the current DC
                    //if (deleteObject != null)
                    //{
                    //    // get the data type of the item
                    //    Type dataType = deleteObject.GetType();

                    //    // get the correct type by resolving inherited types
                    //    dataType = LinqReflectionHelper.ResolveTableInheritanceType(toDC, dataType);

                    //    // get a reference to the table for the deserialized object
                    //    ITable table = toDC.GetTable(dataType);

                    //    if (table != null)
                    //    {
                    //        // delete the 
                    //        table.DeleteOnSubmit(deleteObject);
                    //    }
                    //}
                    #endregion
                }
            }
            /// <summary>
            /// Attaches an object to the data context's data table of the object's type
            /// </summary>
            /// <param name="toDC">DataContext where the item should be attached to</param>
            /// <param name="originalEntityData">Data item which should be attached. Must be a new/original, unchanged entity.</param>
            private void AttachObjectToDataContext(DataContext toDC, object originalEntityData)
            {
                // get the object's data type
                Type dataType = originalEntityData.GetType();

                // get the correct type by resolving inherited types
                dataType = LinqReflectionHelper.ResolveTableInheritanceType(toDC, dataType);

                // get a reference to the table for the deserialized object
                ITable table = toDC.GetTable(dataType);

                // attach the object
                table.Attach(originalEntityData);
            }

            /// <summary>
            /// Attaches an object to the data context's data table of the object's type
            /// </summary>
            /// <param name="toDC">DataContext where the item should be attached to</param>
            /// <param name="newEntityData">Data item which should be attached</param>
            /// <param name="bModified">true if the <paramref name="newEntityData"/> is a modifies entity</param>
            private void AttachObjectToDataContext(DataContext toDC, object newEntityData, bool bModified)
            {
                // get the object's data type
                Type dataType = newEntityData.GetType();

                // get the correct type by resolving inherited types
                dataType = LinqReflectionHelper.ResolveTableInheritanceType(toDC, dataType);

                // get a reference to the table for the deserialized object
                ITable table = toDC.GetTable(dataType);

                // attach the object
                table.Attach(newEntityData, bModified);
            }

            /// <summary>
            /// Attaches an object to the data context's data table of the object's type
            /// </summary>
            /// <param name="toDC">DataContext where the item should be attached to</param>
            /// <param name="newEntityData">Data item which should be attached</param>
            /// <param name="originalEntityData">data item which represnts the original object entity of <paramref name="newEntityData"/>.</param>
            private void AttachObjectToDataContext(DataContext toDC, object newEntityData, object originalEntityData)
            {
                // get the object's data type
                Type dataType = newEntityData.GetType();

                // get the correct type by resolving inherited types
                dataType = LinqReflectionHelper.ResolveTableInheritanceType(toDC, dataType);

                // get a reference to the table for the deserialized object
                ITable table = toDC.GetTable(dataType);

                // attach the object
                table.Attach(newEntityData, originalEntityData);
            }

            /// <summary>
            /// Copies all serialized column properties from a source object to a destination object.
            /// Also copies foreign key columns which is only allowed for new objects, so this method
            /// should only be used if copy properties from an existing data instance to a brand new one
            /// where data associations haven't been set.
            /// </summary>
            /// <param name="Source">Source object for the copy process</param>
            /// <param name="Dest">Destination property for the copy process</param>
            private void CopySerializedColumnPropertiesForInsert(DataContext toDc, object Source, object Dest)
            {
                Type dataType = Source.GetType();

                if (Source.GetType() != Dest.GetType())
                    throw new NotSupportedException("Copy column properties only works for objects of the same type.");

                Type checkType = LinqReflectionHelper.ResolveTableInheritanceType(toDc, dataType);
                // check if the data type is a Linq table
                if (LinqReflectionHelper.IsLinqTable(checkType))
                {
                    // iterate through all properties of the data type
                    foreach (PropertyInfo property in dataType.GetProperties())
                    {
                        bool dataMemberAttribute = false;
                        bool columnAttribute = false;

                        // check if the property is marked with the 
                        // column (= linq colum mapping) and 
                        // datamember (= serialization attribute) attribute
                        foreach (object attribute in property.GetCustomAttributes(false))
                        {
                            if (attribute is ColumnAttribute)
                                columnAttribute = true;

                            if (attribute is DataMemberAttribute)
                                dataMemberAttribute = true;
                        }

                        if (dataMemberAttribute && columnAttribute && property.CanWrite)
                        {
                            // copy the value from the source to the destination object
                            property.SetValue(Dest, property.GetValue(Source, null), null);
                        }
                    }
                }
            }


            /// <summary>
            /// Copies all serialized column properties from a source object to a destination object
            /// </summary>
            /// <param name="sourceDataItem">Source data item for the copy process</param>
            /// <param name="Dest">Destination property for the copy process</param>
            /// <param name="toDC">The Datacontext for searching destination objects</param>
            private void CopySerializedColumnPropertiesForUpdate(ChangeSetDataItem sourceDataItem, object Dest, DataContext toDC)
            {
                object Source = sourceDataItem.Data;
                Type dataType = Source.GetType();

                if (Source.GetType() != Dest.GetType())
                    throw new NotSupportedException("Copy column properties only works for objects of the same type.");

                Type checkType = LinqReflectionHelper.ResolveTableInheritanceType(toDC, dataType);
                // check if the data type is a Linq table
                if (LinqReflectionHelper.IsLinqTable(checkType))
                {
                    #region Mapping of association properties to this-key properties
                    // get a list of properties which are representing the primary key of the table type
                    List<PropertyInfo> primaryKeys = LinqReflectionHelper.GetPrimaryKeys(dataType);

                    // get a list of all foreign-key association properties
                    List<PropertyInfo> associationColumns = LinqReflectionHelper.GetAssociationProperties(dataType, true);

                    // this hashtable will receive a PropertyInfo as key. This property info instance
                    // is bound to a this-key column. The value will be a property info instance
                    // of the association column for this key column
                    Hashtable htAssociationKeyToDataType = new Hashtable();

                    // loop through all association properties
                    foreach (PropertyInfo pI in associationColumns)
                    {
                        string thisKeyName = LinqReflectionHelper.GetAssociationAttributeThisKeyName(pI);

                        if (thisKeyName != null)
                        {
                            // get the name of the related this-key column
                            PropertyInfo keyProperty = LinqReflectionHelper.GetPropertyInfo(dataType, thisKeyName);

                            if (keyProperty != null)
                            {
                                //associationKeyProperties.Add(keyProperty);
                                // maps the association column to the this-key column
                                htAssociationKeyToDataType[keyProperty] = pI;
                            }
                        }
                    }
                    #endregion

                    // iterate through all properties of the data type and start copying
                    // data from the source object to the destination object
                    foreach (PropertyInfo property in dataType.GetProperties())
                    {
                        bool dataMemberAttribute = false;
                        bool columnAttribute = false;
                        bool associationAttribute = false;

                        // check if the property is marked with the 
                        // column (= linq colum mapping) and 
                        // datamember (= serialization attribute) attribute
                        foreach (object attribute in property.GetCustomAttributes(false))
                        {
                            if (attribute is ColumnAttribute)
                                columnAttribute = true;

                            if (attribute is DataMemberAttribute)
                                dataMemberAttribute = true;

                            if (attribute is AssociationAttribute)
                                associationAttribute = true;
                        }


                        if (dataMemberAttribute && associationAttribute && property.CanWrite)
                        {
                            // if the current property represents an association AND a data member
                            // which means a serialized association which will be an EntitySet<T>

                            PropertyInfo esProperty = property;

                            // if this property is a serialized EntitySet<T> property
                            if (LinqReflectionHelper.IsSerializedEntitiySetProperty(esProperty))
                            {
                                // type of the property which is EntitySet<T>
                                Type propertyType = esProperty.PropertyType;

                                // get an instance reference to the property of the
                                // deserialized object and the one created in SET 1
                                object serializedObjProperty = esProperty.GetValue(Source, null);
                                object destObjProperty = esProperty.GetValue(Dest, null);

                                // gets the method info object of the GetEnumerator() method
                                // of the enumerable type
                                MethodInfo miEnumerator = LinqReflectionHelper.GetEnumeratorMethodInfo(serializedObjProperty.GetType());

                                // invoke the GetEnumerator() method of the table object
                                object obEnumerator = miEnumerator.Invoke(serializedObjProperty,
                                                                         new object[]
                                                                             {
                                                                             }
                                                                   );
                                // obRet = IEnumerator<T> instance
                                if (obEnumerator != null)
                                {
                                    // get the method info of the MoveNext method of the enumerator
                                    MethodInfo miMoveNext = LinqReflectionHelper.GetIEnumeratorMoveNextMethodInfo(obEnumerator.GetType());
                                    // get the property info of the Current property of the enumerator
                                    PropertyInfo piCurrent = LinqReflectionHelper.GetIEnumeratorCurrentPropertyInfo(obEnumerator.GetType());

                                    // get the method info of the Add method of the EntitySet<T>
                                    MethodInfo miAdd = LinqReflectionHelper.GetEntitySetAddMethodInfo(destObjProperty.GetType());

                                    // iterate through the enumeration
                                    // as long as MoveNext returns true there are elements in the enumeration
                                    while ((bool)miMoveNext.Invoke(obEnumerator, new object[] { }))
                                    {
                                        // gets the current object of the enumerator
                                        object currData = piCurrent.GetValue(obEnumerator, null);

                                        if (currData != null)
                                        {
                                            // curData is a child item of the enumeration

                                            // search the deserialized object in the current data context
                                            // using the primary keys and original value indicators
                                            object currDataObjectInDC = FindObjectInDC(currData, toDC);

                                            if (currDataObjectInDC != null)
                                            {
                                                // Add the associated data item to the new created instance
                                                // (see STEP 1)
                                                miAdd.Invoke(destObjProperty,
                                                             new object[]
                                                                 {
                                                                     currDataObjectInDC
                                                                 });

                                                // try to cast the current object to an identifyable object
                                                IdentifyableObjectBase currIdentifyableData = currData as IdentifyableObjectBase;

                                                if (currIdentifyableData != null)
                                                {
                                                    // get the Id of the object
                                                    Guid objectId = currIdentifyableData.SerializedObjectId;

                                                    // get the deserialized ChangeSetDataItem with the object Id
                                                    ChangeSetDataItem assObject = GetDataItemPerId(objectId);

                                                    if (assObject != null)
                                                    {
                                                        // mark the associated object item as association
                                                        assObject.IsAssosicatedObject = true;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else if (dataMemberAttribute && columnAttribute && property.CanWrite)
                        {
                            // get the association column for the current data column property
                            PropertyInfo piAssociationColumn = (PropertyInfo)htAssociationKeyToDataType[property];

                            // if an association column exists
                            if (piAssociationColumn != null)
                            {
                                // only perform an action if the the key value changed
                                // skip null-check because we assume that key columns cannot have a null value
                                if (!property.GetValue(Source, null).Equals(property.GetValue(Dest, null)))
                                {
                                    // the value of the current key property can not be set directly
                                    // we have to find the object in the data context which is represented by the 
                                    // primary key and assign this object to the association property
                                    Type associationTableType = piAssociationColumn.PropertyType;

                                    // gets the name of the association key of the foreign table
                                    string otherKeyName = LinqReflectionHelper.GetAssociationAttributeOtherKeyName(piAssociationColumn);

                                    if (otherKeyName != null)
                                    {
                                        // get a property info reference to the search property in the forein table data type
                                        PropertyInfo otherSearchProperty = LinqReflectionHelper.GetPropertyInfo(associationTableType, otherKeyName);

                                        // the property info instance which will be used to identify the search value
                                        // by default this is the current key property but if we have an original value
                                        // property configured for the key value, this property will be used
                                        // to retrieve the value for searching
                                        PropertyInfo thisSearchProperty = property;

                                        // get the property of the DataContext which is represinting the table of the given type
                                        PropertyInfo tableProp = LinqReflectionHelper.GetTablePropertyOfDataContext(associationTableType, toDC);

                                        // get the instance of the roperty representing the table
                                        object tableInstance = tableProp.GetValue(toDC, null);


                                        // get the SingleOrDefault method for calling a data filter
                                        MethodInfo miSingleOrDefault = LinqReflectionHelper.GetSingleOrDefaultWithPredicateMethodInfo();
                                        // set the generic datatypes of the generic method
                                        miSingleOrDefault = miSingleOrDefault.MakeGenericMethod(new Type[] { associationTableType });

                                        // gets the method info of the static method which returns the this-/foreign key check
                                        // predicate function which will be used to search for the related object
                                        MethodInfo miTFKeyPredicate = LinqReflectionHelper.GetThisKeyForeignKeySearchPredicate();
                                        // set the generic datatypes of the generic method
                                        miTFKeyPredicate = miTFKeyPredicate.MakeGenericMethod(new Type[] { associationTableType });

                                        // invoke the static method which returns the predicate function
                                        var o2 = miTFKeyPredicate.Invoke(null, new object[]
                                                                                   {
                                                                                       otherSearchProperty, // this property will be used to get the compare value of the list item
                                                                                       thisSearchProperty, // this property will be used to get the compare value out of the Source object
                                                                                       Source
                                                                                   });

                                        // invoke the SingleOrDefault() method of the table object
                                        object obRet = miSingleOrDefault.Invoke(tableInstance,
                                                                                new object[]
                                                                                    {
                                                                                        tableInstance,
                                                                                        o2
                                                                                    }
                                            );

                                        if (obRet != null)
                                        {
                                            // if we found an object using the keys
                                            // we will set this object as value of the association property
                                            piAssociationColumn.SetValue(Dest, obRet, null);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                // only perform an action if the the value changed
                                object val1 = property.GetValue(Source, null);
                                object val2 = property.GetValue(Dest, null);

                                if (val1 == null)
                                {
                                    if (val2 != null)
                                    {
                                        // copy the value from the source to the destination object
                                        property.SetValue(Dest, null, null);
                                    }
                                }
                                else if (val2 == null)
                                {
                                    property.SetValue(Dest, val1, null);
                                }
                                else
                                {
                                    if (!val1.Equals(val2))
                                        property.SetValue(Dest, val1, null);
                                }
                            }
                        }
                    }
                }
            }

            /// <summary>
            /// Searches an object in the data context using the primary key fields
            /// </summary>
            /// <param name="data">Data object to search. Must be an instance of one of the data tables of the DataContext.</param>
            /// <param name="toDC">Data context</param>
            /// <returns>The object with the same primary key values if existent in the given data context</returns>
            /// <remarks>If the table type has properties marked with the [OriginalValueIndicator] attribute
            /// these fields will be used search the object.</remarks>
            private object FindObjectInDC(object data, DataContext toDC)
            {
                // read the datatype
                Type dataType = data.GetType();
                // get the correct type by resolving inherited types
                dataType = LinqReflectionHelper.ResolveTableInheritanceType(toDC, dataType);

                // get a reference to the table for the deserialized object
                ITable table = toDC.GetTable(dataType);

                // get a list of properties which are representing the primary key of the table type
                List<PropertyInfo> primaryKeys = LinqReflectionHelper.GetPrimaryKeys(dataType);

                // get a list of original value properties for primary key fields
                // this is only needed if it is possible to change the primary key
                // during runtime which may result in a changeset
                List<PropertyInfo> originalValueProperties = LinqReflectionHelper.GetOriginalValueProperties(dataType, primaryKeys);

                // get the property of the DataContext which is represinting the table of the given type
                PropertyInfo tableProp = LinqReflectionHelper.GetTablePropertyOfDataContext(dataType, toDC);

                // get the instance of the roperty representing the table
                object tableInstance = tableProp.GetValue(toDC, null);


                // get the SingleOrDefault method for calling a data filter
                MethodInfo miSingleOrDefault = LinqReflectionHelper.GetSingleOrDefaultWithPredicateMethodInfo();
                // set the generic datatypes of the generic method
                miSingleOrDefault = miSingleOrDefault.MakeGenericMethod(new Type[] { dataType });

                // gets the method info of the static method which returns the primary key check predicate function
                // we cannot call this method directly because we need to dynamically assign the 
                // generic data type
                MethodInfo miPKPredicate = LinqReflectionHelper.GetPrimaryKeyPredicate();
                // set the generic datatypes of the generic method
                miPKPredicate = miPKPredicate.MakeGenericMethod(new Type[] { dataType });

                // invoke the static method which returns the predicate function
                // if the type has properties which are marked as the original
                // primary key values, use these properties for primary key searching
                var o2 = miPKPredicate.Invoke(null, new object[]
                                                        {
                                                            (originalValueProperties.Count > 0 ? originalValueProperties : primaryKeys), 
                                                            data
                                                        });

                // invoke the SingleOrDefault() method of the table object
                object obRet = miSingleOrDefault.Invoke(tableInstance,
                                         new object[]
                                             {
                                                 tableInstance,
                                                 o2 
                                             }
                                        );

                return obRet;
            }

            /// <summary>
            /// Check if a specified object with the same ObjectId exists in the data context's change set
            /// </summary>
            /// <param name="data">Deserialized data item</param>
            /// <param name="toDC">DataContext where the item should be searched</param>
            /// <returns>Returns true if the DataContext already contains an object with the same object identifier within its ChangeSet.</returns>
            /// <remarks>Object identifier field will be used to find the object in the data context's change set</remarks>
            private bool ObjectExistsAtChangeSet(ChangeSetDataItem data, DataContext toDC)
            {
                // get the current change set of the data context
                ChangeSet csCurrent = toDC.GetChangeSet();

                // check the Insert commands
                foreach (IdentifyableObjectBase obj in csCurrent.Inserts)
                {
                    if (obj.SerializedObjectId.Equals(((IdentifyableObjectBase)data.Data).SerializedObjectId))
                        return true;
                }
                // check the Update commands
                foreach (IdentifyableObjectBase obj in csCurrent.Updates)
                {
                    if (obj.SerializedObjectId.Equals(((IdentifyableObjectBase)data.Data).SerializedObjectId))
                        return true;
                }
                // check the Delete commands
                foreach (IdentifyableObjectBase obj in csCurrent.Deletes)
                {
                    if (obj.SerializedObjectId.Equals(((IdentifyableObjectBase)data.Data).SerializedObjectId))
                        return true;
                }

                return false;
            }

            /// <summary>
            /// Check if a specified object with the same ObjectId exists in the data table
            /// </summary>
            /// <param name="data">Deserialized data item</param>
            /// <param name="toDC">DataContext where the item should be searched</param>
            /// <returns>Returns true if the DataContext already contains an object with the same object identifier.</returns>
            /// <remarks>Object identifier field will be used to find the object in the data context</remarks>
            private bool ObjectExistsAtDC(ChangeSetDataItem data, DataContext toDC)
            {
                // read the datatype
                Type dataType = data.DataType;
                // get the correct type by resolving inherited types
                dataType = LinqReflectionHelper.ResolveTableInheritanceType(toDC, dataType);

                // get a reference to the table for the deserialized object
                ITable table = toDC.GetTable(dataType);

                // get the property of the DataContext which is represinting the table of the given type
                PropertyInfo tableProp = LinqReflectionHelper.GetTablePropertyOfDataContext(dataType, toDC);

                // get the instance of the roperty representing the table
                object tableInstance = tableProp.GetValue(toDC, null);

                // get the SingleOrDefault method for calling a data filter
                MethodInfo miSingleOrDefault = LinqReflectionHelper.GetSingleOrDefaultWithPredicateMethodInfo();
                // set the generic datatypes of the generic method
                miSingleOrDefault = miSingleOrDefault.MakeGenericMethod(new Type[] { dataType });

                // gets the method info of the static method which returns the object identifier check predicate function
                // we cannot call this method directly because we need to dynamically assign the 
                // generic data type
                MethodInfo miOIdPredicate = LinqReflectionHelper.GetObjectIdentifierPredicate();
                // set the generic datatypes of the generic method
                miOIdPredicate = miOIdPredicate.MakeGenericMethod(new Type[] { dataType });

                // invoke the static method which returns the predicate function
                var o2 = miOIdPredicate.Invoke(null, new object[] { data.Data });

                // invoke the Where() method of the table object
                object obRet = miSingleOrDefault.Invoke(tableInstance,
                                         new object[]
                                             {
                                                 tableInstance,
                                                 o2 
                                             }
                                        );

                // if the objdata object is not null a record was found
                return (obRet != null);
            }

            /// <summary>
            /// Check if a deserialized object already exists in the data context.
            /// </summary>
            /// <param name="data">Deserialized data item</param>
            /// <param name="toDC">DataContext where the item should be searched</param>
            /// <returns>Returns true if the DataContext already contains an object with the same primary key values of the ame type.</returns>
            /// <remarks>Primary key fields will be used to find the object in the data context</remarks>
            private bool ObjectWithPKAlreadyExists(ChangeSetDataItem data, DataContext toDC)
            {
                // read the datatype
                Type dataType = data.DataType;
                // get the correct type by resolving inherited types
                dataType = LinqReflectionHelper.ResolveTableInheritanceType(toDC, dataType);

                // get a reference to the table for the deserialized object
                ITable table = toDC.GetTable(dataType);

                // get a list of properties which are representing the primary key of the table type
                List<PropertyInfo> primaryKeys = LinqReflectionHelper.GetPrimaryKeys(dataType);

                // get a list of original value properties for primary key fields
                // this is only needed if it is possible to change the primary key
                // during runtime which may result in a changeset
                List<PropertyInfo> originalValueProperties = LinqReflectionHelper.GetOriginalValueProperties(dataType, primaryKeys);

                // get the property of the DataContext which is represinting the table of the given type
                PropertyInfo tableProp = LinqReflectionHelper.GetTablePropertyOfDataContext(dataType, toDC);

                // get the instance of the roperty representing the table
                object tableInstance = tableProp.GetValue(toDC, null);


                // get the SingleOrDefault method for calling a data filter
                MethodInfo miSingleOrDefault = LinqReflectionHelper.GetSingleOrDefaultWithPredicateMethodInfo();
                // set the generic datatypes of the generic method
                miSingleOrDefault = miSingleOrDefault.MakeGenericMethod(new Type[] { dataType });

                // gets the method info of the static method which returns the primary key check predicate function
                // we cannot call this method directly because we need to dynamically assign the 
                // generic data type
                MethodInfo miPKPredicate = LinqReflectionHelper.GetPrimaryKeyPredicate();
                // set the generic datatypes of the generic method
                miPKPredicate = miPKPredicate.MakeGenericMethod(new Type[] { dataType });

                // invoke the static method which returns the predicate function
                // if the type has properties which are marked as the original
                // primary key values, use these properties for primary key searching
                var o2 = miPKPredicate.Invoke(null, new object[] 
                                                        {
                                                            (originalValueProperties.Count > 0 ? originalValueProperties : primaryKeys), 
                                                            data.Data
                                                        });

                // invoke the Where() method of the table object
                object obRet = miSingleOrDefault.Invoke(tableInstance,
                                         new object[]
                                             {
                                                 tableInstance,
                                                 o2 
                                             }
                                        );

                // if the objdata object is not null a record was found
                return (obRet != null);
            }

            /// <summary>
            /// checks if the change set has data
            /// </summary>
            /// <returns></returns>
            public bool HasData()
            {
                if ((_inserts == null) &&
                    (_updates == null) &&
                    (_deletes == null))
                    return false;

                if (((_inserts != null) && (_inserts.Count == 0)) &&
                     ((_updates != null) && (_updates.Count == 0)) &&
                     ((_deletes != null) && (_deletes.Count == 0)))
                    return false;


                return true;
            }
            #endregion

            #region Properties
            /// <summary>
            /// Gets/Sets the list of Insert data objects
            /// </summary>
            [XmlArray]
            public List<ChangeSetDataItem> Inserts
            {
                get { return _inserts; }
                set { _inserts = value; }
            }
            /// <summary>
            /// Gets/Sets the list of Update data objects
            /// </summary>
            [XmlArray]
            public List<ChangeSetDataItem> Updates
            {
                get { return _updates; }
                set { _updates = value; }
            }
            /// <summary>
            /// Gets/Sets the list of Delete data objects
            /// </summary>
            [XmlArray]
            public List<ChangeSetDataItem> Deletes
            {
                get { return _deletes; }
                set { _deletes = value; }
            }
            #endregion

            #region attribs
            private List<ChangeSetDataItem> _inserts;
            private List<ChangeSetDataItem> _updates;
            private List<ChangeSetDataItem> _deletes;
            #endregion
        }
        #endregion

        /// <summary>
        /// Serializes the changes of a given DataContext object to a filename
        /// </summary>
        /// <param name="fromDC">Source data context</param>
        /// <param name="fileName">filename to store the serialized data</param>
        /// <param name="appendToFile">true if the contents of the current changeset shouöd be appended</param>
        /// <remarks>Please make sure that the you set the Serialization Mode to Unidirectional in your dbml file
        /// AND make sure, that all your data classes inherit from <see cref="IdentifyableObjectBase"/>. Only in this
        /// case the serialization and deserialization will work correctly.</remarks>
        public static void SerializetoFile(DataContext fromDC, string fileName, bool appendToFile)
        {
            if (!LinqReflectionHelper.IsDataContextSerializeable(fromDC))
                throw new NotSupportedException("DataContext Serialization Mode property not set.\nOpen the BML designer and set the Serialization Mode property to Unidirectional.");

            if (!LinqReflectionHelper.AreDataClassesIdendifyable(fromDC))
                throw new NotSupportedException("All data table classes from the serializable DataContext must inherit from the IdentifyableObjectBase class.\nThis is required for reconstructing data object associations after deserialization.");

            ChangeSetEx csEx = new ChangeSetEx(fromDC.GetChangeSet());

            if(appendToFile)
            {
                ChangeSetEx csCurrInfile = ChangeSetEx.Deserialize(fileName);

                if (csCurrInfile != null && csCurrInfile.HasData())
                {
                    csEx.Merge(csCurrInfile);
                }
            }

            if (csEx.HasData())
            {
                csEx.Serialize(fileName);
            }
        }

        /// <summary>
        /// Serializes the changes of a given DataContext object to a filename
        /// </summary>
        /// <param name="fromDC">Source data context</param>
        /// <param name="fileName">filename to store the serialized data (will be overwritten)</param>
        /// <remarks>Please make sure that the you set the Serialization Mode to Unidirectional in your dbml file
        /// AND make sure, that all your data classes inherit from <see cref="IdentifyableObjectBase"/>. Only in this
        /// case the serialization and deserialization will work correctly.</remarks>
        public static void SerializetoFile(DataContext fromDC, string fileName)
        {
            SerializetoFile(fromDC, fileName, false);
        }

        /// <summary>
        /// Deserialiazes a given string and reconstructs the ChangeSet of a DataContext
        /// </summary>
        /// <param name="fromDC">Destination data context</param>
        /// <returns>A string containing an Xml representation of the DataContext changes</returns>
        /// <remarks>Please make sure that the you set the Serialization Mode to Unidirectional in your dbml file
        /// AND make sure, that all your data classes inherit from <see cref="IdentifyableObjectBase"/>. Only in this
        /// case the serialization and deserialization will work correctly.</remarks>
        public static string SerializeToString(DataContext fromDC)
        {
            ChangeSetEx csEx = new ChangeSetEx(fromDC.GetChangeSet());


            if (csEx.HasData())
            {
                return csEx.ToString();
            }

            return "";
        }

        /// <summary>
        /// Deserializes a changeset from a file and reconstructs it on the given DataContext
        /// </summary>
        /// <param name="fileName">Source file name containing the serialized ChangeSet</param>
        /// <param name="toDC">Destination DataContext</param>
        public static void DeserializeFromFile(string fileName, DataContext toDC)
        {
            ChangeSetEx cs = ChangeSetEx.Deserialize(fileName);

            if (cs != null && cs.HasData())
            {
                cs.ReconstructChangeSet(toDC);
            }
        }

        /// <summary>
        /// Deserializes a changeset from a Xml string and reconstructs it on the given DataContext
        /// </summary>
        /// <param name="changeSetExString">Xml string containing the changeset information</param>
        /// <param name="toDC">Destination DataContext</param>
        public static void DeserializeFromString(string changeSetExString, DataContext toDC)
        {
            ChangeSetEx cs = ChangeSetEx.DeserializeFromString(changeSetExString);

            if (cs != null && cs.HasData())
            {
                cs.ReconstructChangeSet(toDC);
            }
        }
    }
}
