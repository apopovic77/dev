﻿using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace OpenCvDocScannerTestApp
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            string[] files = Directory.GetFiles(@"C:\AwantoRepo\Experiments\ProofOfConcept\Cognitive\Data\images");
            
            image_cb.ItemsSource = files;
            image_cb.SelectedIndex = 28;
            image_cb.SelectionChanged += Image_cb_SelectionChanged;

            _ocv_tester = new OpenCvTest((string)image_cb.SelectedValue);
            expander_stack.Children.Add(_ocv_tester);

            //add ability to resize
            //_ocv_tester.InitResize("Resize");
            _ocv_tester.InitCanny("Canny");

            //DocumentScanner.Scan();
            Loaded += MainWindow_Loaded;
        }

        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            _ocv_tester.Run();
        }

        private void Image_cb_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            _ocv_tester.SetImage((string)image_cb.SelectedValue);
            _ocv_tester.Run();
        }

        private OpenCvTest _ocv_tester;

    }
}
