﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Controls;
using CognitiveLib.Scanner;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using Point = System.Drawing.Point;
using Size = System.Drawing.Size;

namespace OpenCvDocScannerTestApp
{
    /// <summary>
    /// Interaktionslogik für OpenCvTest.xaml
    /// </summary>
    public partial class OpenCvTest : UserControl
    {
        public OpenCvTest(IImage image)
        {
            InitializeComponent();
            _image = image;
        }

        public OpenCvTest(string path_to_image)
        {
            InitializeComponent();
            SetImage(path_to_image);
        }

        public void SetImage(string path_to_image)
        {
            if (_org_image_gray != null)
                _org_image_gray.Dispose();
            if (_image != null)
                _image.Dispose();
            if (_org_image != null)
                _org_image_gray.Dispose();


            _org_image = OpenCvHelper.LoadImage(path_to_image, 2000);
            _org_image_gray = ((Image<Bgr, byte>)_org_image).Convert<Gray, byte>();
            _image = (IImage)_org_image.Clone();
        }

        public void Run()
        {
            CannyCs_ValueChanged(null, null);
        }

        #region Document Scan Test

        public void InitCanny(string name, int min_slide = 0, int max_slide = 255, int tickfreq = 1)
        {
            ConfigSlider cs1 = InitConfigSlider("Threshold 1", 0, 255, 1);
            ConfigSlider cs2 = InitConfigSlider("Threshold 2", 0, 255, 1);
            ConfigSlider cs3 = InitConfigSlider("Resize", 100, 2000, 100);
            ConfigSlider cs4 = InitConfigSlider("Smooth1", 0, 100, 1);
            ConfigSlider cs5 = InitConfigSlider("ApproxPolyDpFaktor", 1, 1000, 1);
            ConfigSlider cs6 = InitConfigSlider("Min Arc Length", 1, 10000, 1);
            ConfigSlider cs7 = InitConfigSlider("Min Contour Area", 1, 10000, 1);

            cs1.Value = 50;
            cs2.Value = 150;
            cs3.Value = 1000;
            cs4.Value = 7;
            cs5.Value = 3;
            cs6.Value = 50;
            cs7.Value = 1;

            cs1.Value = 96;
            cs2.Value = 255;
            cs3.Value = 1000;
            cs4.Value = 9;
            cs5.Value = 20;
            cs6.Value = 50;
            cs7.Value = 20;


            cs1.ValueChanged += CannyCs_ValueChanged;
            cs2.ValueChanged += CannyCs_ValueChanged;
            cs3.ValueChanged += CannyCs_ValueChanged;
            cs4.ValueChanged += CannyCs_ValueChanged;
            cs5.ValueChanged += CannyCs_ValueChanged;
            cs6.ValueChanged += CannyCs_ValueChanged;
            cs7.ValueChanged += CannyCs_ValueChanged;
        }

        private void CannyCs_ValueChanged(object sender, EventArgs e)
        {
            ConfigSlider cs_thresh1 = base_stack.Children.OfType<ConfigSlider>().First(c => c.Tag != null && (string)c.Tag == "Threshold 1");
            ConfigSlider cs_thresh2 = base_stack.Children.OfType<ConfigSlider>().First(c => c.Tag != null && (string)c.Tag == "Threshold 2");
            ConfigSlider cs_resize = base_stack.Children.OfType<ConfigSlider>().First(c => c.Tag != null && (string)c.Tag == "Resize");
            ConfigSlider cs_smooth1 = base_stack.Children.OfType<ConfigSlider>().First(c => c.Tag != null && (string)c.Tag == "Smooth1");
            ConfigSlider cs_approxpolydp = base_stack.Children.OfType<ConfigSlider>().First(c => c.Tag != null && (string)c.Tag == "ApproxPolyDpFaktor");
            ConfigSlider cs_minarc_len = base_stack.Children.OfType<ConfigSlider>().First(c => c.Tag != null && (string)c.Tag == "Min Arc Length");
            ConfigSlider cs_mincont_area = base_stack.Children.OfType<ConfigSlider>().First(c => c.Tag != null && (string)c.Tag == "Min Contour Area");

            IImage warped_image = DocumentScanner.ScanToGrayDoc(_org_image,
                (int)cs_resize.Value,
                (int)cs_thresh1.Value,
                (int)cs_thresh2.Value,
                (int)cs_smooth1.Value,
                cs_approxpolydp.Value / 1000f,
                (int)cs_minarc_len.Value,
                (int)cs_mincont_area.Value);

            if (warped_image != null)
            {
                if (warped_image.Size.Width > 0 && warped_image.Size.Height > 0)
                {
                    CvInvoke.Imwrite("last_converstion.jpg", warped_image);
                    CvInvoke.Imshow("Warped", warped_image);
                }
                warped_image.Dispose();
            }

        }
        #endregion


        #region Resize
        public void InitResize(string title)
        {
            ConfigSlider cs = InitConfigSlider(title, 100, 2000, 100);
            cs.ValueChanged += ResizeCs_ValueChanged;
        }

        private void ResizeCs_ValueChanged(object sender, EventArgs e)
        {
            ConfigSlider cs = sender as ConfigSlider;
            float ratio = (float)_image.Size.Width / (float)_image.Size.Height;

            int width = (int)cs.Value;
            int height = (int)(width * (float)ratio);
            Size size = new Size(height, width);

            CvInvoke.Resize(_org_image, _image, size, 0, 0, Inter.Area);
            CvInvoke.Imshow("Image", _image);
        }
        #endregion


        public ConfigSlider InitConfigSlider(string title, int min_slide = 100, int max_slide = 2000, int tickfreq = 100)
        {
            ConfigSlider cs = new ConfigSlider(title);
            cs.Minimum.Text = min_slide.ToString();
            cs.Maximum.Text = max_slide.ToString();
            cs.Tickfrequency.Text = tickfreq.ToString();

            base_stack.Children.Add(cs);
            return cs;
        }



        private IImage _image;
        private IImage _org_image;
        private IImage _org_image_gray;


    }
}
