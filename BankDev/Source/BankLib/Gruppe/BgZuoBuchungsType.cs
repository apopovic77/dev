﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BankLib.Data;

namespace BankLib.Gruppe
{
    public class BgZuoBuchungsType : BgZuo
    {
        internal override void WriteToStream(BinaryWriter writer)
        {
            BinaryHelper.WriteString(writer, "BgZuoBuchungsType");
            base.WriteToStream(writer);
            BinaryHelper.WriteString(writer, BuchungsType);
        }

        internal override void LoadFromStream(BinaryReader reader)
        {
            base.LoadFromStream(reader);
            BuchungsType = BinaryHelper.ReadString(reader);
        }

        /// <summary>
        /// Es kann immer eingefügt werden
        /// </summary>
        /// <param name="b"></param>
        /// <returns></returns>
        protected override bool CheckZuo(Buchung b)
        {
            if (b.BuchungsType == BuchungsType)
                return true;
            return false;
        }


        internal override int RulePriority
        {
            get { return 10; }
        }

        public override string ToString()
        {
            return "[BgZuoBuchungsType] "+BuchungsType;
        }

        public string BuchungsType;
    }
}
