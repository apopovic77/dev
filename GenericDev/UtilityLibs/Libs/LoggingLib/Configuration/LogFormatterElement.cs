﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace LoggingLib.Configuration
{
    /// <summary>
    /// log formatter configuration element
    /// </summary>
    public class LogFormatterElement : ConfigurationElement
    {
        /// <summary>
        /// The full qualified type name of a log entry data type. The data formatter defined 
        /// will be applied to log entries of this type.
        /// </summary>
        /// <remarks>leave this value blank for all log entries</remarks>
        [ConfigurationProperty("LogEntryDataTypeName", DefaultValue = "", IsRequired = true, IsKey = true)]
        public string LogEntryDataTypeName
        {
            get
            {
                return ((string)(base["LogEntryDataTypeName"]));
            }

            set
            {
                base["LogEntryDataTypeName"] = value;
            }
        }

        /// <summary>
        /// The full qualified type name of a data formatter class which will be loaded and called
        /// to format a log message's data
        /// </summary>
        [ConfigurationProperty("FormatterDataTypeName", DefaultValue = "", IsRequired = true, IsKey = true)]
        public string FormatterDataTypeName
        {
            get
            {
                return ((string)(base["FormatterDataTypeName"]));
            }

            set
            {
                base["FormatterDataTypeName"] = value;
            }
        }
    }
}
