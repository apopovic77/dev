using System;
using System.Collections.Generic;
using System.Text;
using Logicx.Utilities;
using D3d = Microsoft.DirectX.Direct3D;
using System.Collections;
using System.Drawing;
using Logicx.DirectX.Management;
using Logicx.DirectX.Utilities;
using Logicx.DirectX.Engine;


namespace Logicx.DirectX.Objects.Hud
{
    public class HudDebugWriter3d : Logicx.DirectX.Objects.Object3d, Logicx.Utilities.IDebugWriter
    {
		public int TimeTillTextFadeout
		{
			set {
				m_time_display_text_till_fadeout = value;
			}
			
			get {
				return m_time_display_text_till_fadeout;
			}
		}
        struct HudDebugInfoMsg
        {
            public HudDebugInfoMsg(string msg)
            {
                Msg = msg;
                TimeAdded = DXUtilities3d.Timer(TIMER.GETAPPTIME);
            }
            public string Msg;
            public float TimeAdded;
        }

        protected ArrayList m_msgs = new ArrayList();
        public int MaxMessagesToShow = 50;
        private D3d.Font m_font_2d;
        private D3d.Sprite m_sprite;
        private int m_time_display_text_till_fadeout = 10;
        private int m_fadeout_time = 2;
        private int m_padding_bottom = 40;
        
        public HudDebugWriter3d()
        {
        }


        public override void Init()
        {
            m_sprite = new D3d.Sprite(Engine3d.Device3d);
            m_font_2d = new D3d.Font(Engine3d.Device3d, new System.Drawing.Font("Arial", 8));
        }

        public override void Dispose()
        {
            if (m_sprite != null)
                m_sprite.Dispose();
            if (m_font_2d != null)
                m_font_2d.Dispose();
        }


        public virtual void WriteInfo(string msg)
        {
            m_msgs.Add(new HudDebugInfoMsg(msg));
            if (m_msgs.Count >= MaxMessagesToShow)
                m_msgs.RemoveAt(0);

        }

        public override void Render(float elapsedTime){
            float current_apptime = Engine3d.AppTime;
            DeleteOldMsgs(current_apptime);
            int count_msgs = m_msgs.Count;
            if (count_msgs == 0) return;

            int window_widht = Engine3d.DirectXManager.DxWindowWidth;
            int window_height = Engine3d.DirectXManager.DxWindowHeight;
            int line_height = 10;

            int curr_pos_y_sprite = window_height - line_height * count_msgs;
            int curr_pos_x_sprite = 10;

            m_sprite.Begin(D3d.SpriteFlags.SortTexture | D3d.SpriteFlags.AlphaBlend);

            Microsoft.DirectX.Matrix m = Microsoft.DirectX.Matrix.Identity;
            m.Translate(curr_pos_x_sprite, curr_pos_y_sprite - m_padding_bottom, 0);
            m_sprite.Transform = m;
            for (int i = 0; i < m_msgs.Count;i++ )
            {
                HudDebugInfoMsg msg = (HudDebugInfoMsg)m_msgs[i];
                m_font_2d.DrawText(m_sprite, msg.Msg, 0, line_height * i, GetColorOfMsg(msg, current_apptime));
            }
            m_sprite.End();

        }

        private Color GetColorOfMsg(HudDebugInfoMsg msg,float current_apptime) { 

            //check if we are fading
            if (current_apptime - msg.TimeAdded < m_time_display_text_till_fadeout)
                return Color.FromArgb(255, 0, 255, 0);

            //we are in fading state, calc transparent color value
            float k = -255 / m_fadeout_time;
            float d = 255;
            float y = k * (current_apptime - msg.TimeAdded - m_time_display_text_till_fadeout) + d;
            int color_black_transparent_val = Convert.ToInt32(y);
            if (color_black_transparent_val < 0) color_black_transparent_val = 0;
            if (color_black_transparent_val > 255) color_black_transparent_val = 255;
            return Color.FromArgb(color_black_transparent_val,0,255,0);
        }

        private void DeleteOldMsgs(float current_apptime)
        {
            ArrayList old_elems = new ArrayList();
            foreach (HudDebugInfoMsg msg in m_msgs) {
                if (msg.TimeAdded < current_apptime - (m_time_display_text_till_fadeout + m_fadeout_time))
                {
                    old_elems.Add(msg);
                }
            }

            foreach (HudDebugInfoMsg msg in old_elems)
                m_msgs.Remove(msg);
        }
    }
}
