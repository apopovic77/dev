﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using Logicx.DiagramLib.Util;

namespace Logicx.DiagramLib.Scales
{
    internal class DayScale : ScaleType
    {

        #region Constants

        private const long ONE_OUR_MSEC = 3600L * 1000; // * 10000000;
        private const long ONE_DAY_MSEC = 24 * ONE_OUR_MSEC;

        private const short LOD_1 = 1; // 1 unit is one hour
        private const short LOD_3 = 3; // 1 unit is three ours
        private const short LOD_6 = 6;
        private const short LOD_12 = 12;
        private const short LOD_24 = 24;

        private const double LONG_LINE_HEIGHT = 9;
        private const double MIDDLE_LINE_HEIGHT = 5;
        private const double SHORT_LINE_HEIGHT = 3;

        private const int MIN_LINE_DISTANCE = 10;
        private const double MIN_SCALE_DISTANCE = 30;

        private short[] _hours = { 1, 3, 6, 12 };

        #endregion

        #region Construction and Initialization
        #endregion

        #region Operations
        public override void Init(double size, bool generate_always)
        {
            short old_level = _line_lod;
            short old_text_level = _text_lod;

            DateTime start = _time_scale_control.Config.Start;
            DateTime end = _time_scale_control.Config.End;

            double days = ((end - start).TotalMilliseconds / ONE_DAY_MSEC);

            if (days > 20) // if more than 20 days visible, show just one day (24 hours)
                _lod = LOD_24;
            else if (days > 10)
                _lod = LOD_12;
            else if (days > 5)
                _lod = LOD_6;
            else if (days > 3)
                _lod = LOD_3;
            else
                _lod = LOD_1;

            double number_hours = ((end - start).TotalMilliseconds / ONE_OUR_MSEC);
            double hour_distance = _time_scale_control.GetRenderWidth() / number_hours;
            if (_time_scale_control.Config.Orientation == Orientation.Vertical)
                hour_distance = _time_scale_control.GetRenderHeight() / number_hours;

            int line_index = 0;
            _line_lod = _hours[line_index++];
            while (line_index < _hours.Count() - 1 && hour_distance * _line_lod < MIN_LINE_DISTANCE)
            {
                _line_lod = _hours[line_index++];
            }

            double max_description_size = GetMaxDescriptionSize(24);
            line_index = 0;
            _text_lod = _hours[line_index++];
            while (line_index < _hours.Count() && hour_distance * _text_lod < max_description_size * 1.5)
            {
                _text_lod = _hours[line_index++];
            }


            double pixel_per_unit = size / (days * ((double)24 / _lod));
            while (pixel_per_unit < MIN_SCALE_DISTANCE)
            {
                _lod = (short)((double)_lod * 2);
                pixel_per_unit = size / (days * ((double)24 / _lod));
            }

            if (_lod == 1)
            {
                if (start.Minute == 0 && start.Second == 0 && start.Millisecond == 0)
                    _unit_offset = 0;
                else
                {
                    TimeSpan time_of_day = start.TimeOfDay;
                    time_of_day = time_of_day.Subtract(new TimeSpan(time_of_day.Hours, 0, 0));
                    //TimeSpan minutes = time_of_day.TotalMilliseconds;

                    //minutes.to
                    _unit_offset = -(time_of_day.TotalMilliseconds / 3600000) * pixel_per_unit;
                }
            }
            else
            {
                double offset_hours = GetOffsetHours(start);
                _unit_offset = pixel_per_unit * offset_hours / _lod;
            }

            if (generate_always || old_level != _line_lod || old_text_level != _text_lod)
            {
                _time_scale_control.ClearChildren();
                GenerateData();
                AddChildren();
            }
        }

        private void GenerateData()
        {
            _outer_lines.Clear();
            _long_lines.Clear();
            _middle_lines.Clear();
            _short_lines.Clear();
            _long_texts.Clear();
            _long_dates.Clear();

            GenerateDayAndSubData();
        }

        private void GenerateDayAndSubData()
        {
            //DateTime start = DateTimeUtil.GetFullHour(_time_scale_control.Config.Start);
            //DateTime end = DateTimeUtil.GetFullHour(_time_scale_control.Config.End);
            DateTime start = _time_scale_control.Config.Start;
            DateTime end = _time_scale_control.Config.End;

            double height_days = _time_scale_control.Config.Orientation == Orientation.Horizontal
                                 ? _time_scale_control.GetRenderHeight()
                                 : 1;
            double width_days = _time_scale_control.Config.Orientation == Orientation.Horizontal
                                ? 1
                                : _time_scale_control.GetRenderWidth();
            double long_height = _time_scale_control.Config.Orientation == Orientation.Horizontal
                                ? LONG_LINE_HEIGHT
                                : 1;
            double long_width = _time_scale_control.Config.Orientation == Orientation.Horizontal
                               ? 1
                               : LONG_LINE_HEIGHT;
            double middle_height = _time_scale_control.Config.Orientation == Orientation.Horizontal
                                ? MIDDLE_LINE_HEIGHT
                                : 1;
            double middle_width = _time_scale_control.Config.Orientation == Orientation.Horizontal
                               ? 1
                               : MIDDLE_LINE_HEIGHT;
            double short_height = _time_scale_control.Config.Orientation == Orientation.Horizontal
                                ? SHORT_LINE_HEIGHT
                                : 1;
            double short_width = _time_scale_control.Config.Orientation == Orientation.Horizontal
                               ? 1
                               : SHORT_LINE_HEIGHT;

            RotateTransform trans = new RotateTransform();
            if (_time_scale_control.Config.Orientation == Orientation.Vertical)
                trans.Angle = 270;

            double number_hours = ((end - start).TotalMilliseconds / ONE_OUR_MSEC);
            double hour_distance = _time_scale_control.GetRenderWidth() / number_hours;
            if (_time_scale_control.Config.Orientation == Orientation.Vertical)
            {
                hour_distance = _time_scale_control.GetRenderHeight() / number_hours;
            }

            bool date_string_generated = false;
            double offset_msec = DateTimeUtil.GetMSecOfHour(start);
            for (DateTime hour = start; hour <= end.AddMilliseconds(offset_msec); hour = hour.AddHours(1))
            {
                int int_hours = (int)hour.Hour;

                if ((DateTimeUtil.IsDayChange(hour)) || hour == start) // Generate Date texts
                {
                    string str = GetDate(start, end, hour, hour_distance);
                    if (str.Length != 0)
                    {
                        TextBlock b = GeometryUtils.GenerateTextBlock(str, TextStyleType.InfoText, typeof(TimeScaleControl), _time_scale_control);
                        b.LayoutTransform = trans;
                        _long_dates.Add(b);

                        date_string_generated = true;
                    }
                }

                if (DateTimeUtil.IsDayChange(hour)) // Generate Day Change lines
                {
                    Rectangle outer_line = GeometryUtils.GenerateRectange(height_days, width_days, typeof(TimeScaleControl), _time_scale_control);
                    _outer_lines.Add(outer_line);
                }
                else
                {
                    int hour_modulo = int_hours % _line_lod;
                    if (hour_modulo == 0)  // lines for hours
                    {
                        Rectangle r = GeometryUtils.GenerateRectange(long_height, long_width, typeof(TimeScaleControl), _time_scale_control);
                        _long_lines.Add(r);
                    }

                    int text_modulo = int_hours % _text_lod;
                    if (text_modulo == 0 && _text_lod < 12) // texts for hours
                    {
                        TextBlock b = GeometryUtils.GenerateTextBlock(hour.Hour.ToString(), TextStyleType.ScaleText, typeof(TimeScaleControl), _time_scale_control);
                        _long_texts.Add(b);
                    }
                }

                if (hour == end)
                    break;

                if (_line_lod == 1 && hour_distance / 2 >= MIN_LINE_DISTANCE) // lines for half hours
                {
                    Rectangle r = GeometryUtils.GenerateRectange(middle_height, middle_width, typeof(TimeScaleControl), _time_scale_control);
                    _middle_lines.Add(r);
                }

                if (_line_lod == 1 && hour_distance / 4 >= MIN_LINE_DISTANCE) // lines for quater hours
                {
                    for (int i = 0; i < 2; i++)
                    {
                        Rectangle r = GeometryUtils.GenerateRectange(short_height, short_width, typeof(TimeScaleControl), _time_scale_control);
                        _short_lines.Add(r);
                    }
                }
            }

            if (!date_string_generated)
            {
                string str = start.ToShortDateString();
                TextBlock b = GeometryUtils.GenerateTextBlock(str, TextStyleType.InfoText, typeof(TimeScaleControl), _time_scale_control);
                b.LayoutTransform = trans;
                _long_dates.Add(b);
            }
        }

        private void AddChildren()
        {
            List<UIElement> elements = new List<UIElement>();

            elements.AddRange(_short_lines.Cast<UIElement>());
            elements.AddRange(_middle_lines.Cast<UIElement>());
            elements.AddRange(_long_lines.Cast<UIElement>());
            elements.AddRange(_outer_lines.Cast<UIElement>());
            elements.AddRange(_long_texts.Cast<UIElement>());
            elements.AddRange(_long_dates.Cast<UIElement>());

            _time_scale_control.AddToChildren(elements);
        }

        public override void AlignData(Orientation orientation)
        {
            //DateTime start = DateTimeUtil.GetFullHour(_time_scale_control.Config.Start);
            //DateTime end = DateTimeUtil.GetFullHour(_time_scale_control.Config.End);
            DateTime start = _time_scale_control.Config.Start;
            DateTime end = _time_scale_control.Config.End;

            int date_index = 0;
            int day_index = 0;
            int hour_index = 0;
            int half_hour_index = 0;
            int quater_hour_index = 0;
            int text_index = 0;

            //double current_hour_position = _time_scale_control.Config.Padding.Left;
            double current_hour_position = 0;
            double number_hours = ((end - start).TotalMilliseconds / ONE_OUR_MSEC);
            double hour_distance = _time_scale_control.GetRenderWidth() / number_hours;
            if (orientation == Orientation.Vertical)
            {
                //current_hour_position = _time_scale_control.Config.Padding.Bottom;
                current_hour_position = 0;
                hour_distance = _time_scale_control.GetRenderHeight() / number_hours;
            }

            current_hour_position += _unit_offset;
            double offset_msec = DateTimeUtil.GetMSecOfHour(start);

            //double leftmost_point = _time_scale_control.Config.Padding.Left;
            //double rightmost_point = _time_scale_control.GetRenderWidth() + _time_scale_control.Config.Padding.Left;
            //double bottommost_point = _time_scale_control.Config.Padding.Bottom;
            //double topmost_point = _time_scale_control.GetRenderHeight() + _time_scale_control.Config.Padding.Bottom;
            double leftmost_point = orientation == Orientation.Horizontal ? _time_scale_control.GetVisibleLeftmostPosition() : 0;
            double rightmost_point = _time_scale_control.GetRenderWidth();
            double bottommost_point = orientation == Orientation.Vertical ? _time_scale_control.GetVisibleLeftmostPosition() : 0;
            double topmost_point = _time_scale_control.GetRenderHeight();

            //Debug.WriteLine("leftmost point " + leftmost_point);
            bool date_string_aligned = false;

            for (DateTime hour = start; hour <= end.AddMilliseconds(offset_msec); hour = hour.AddHours(1))
            {
                int int_hours = (int)hour.Hour;

                if ((DateTimeUtil.IsDayChange(hour)) || hour == start) // Align Date texts
                {
                    string str = GetDate(start, end, hour, hour_distance);
                    if (str.Length != 0 && _long_dates.Count > date_index)
                    {
                        TextBlock text_block = _long_dates.ToArray()[date_index++];
                        if (orientation == Orientation.Horizontal)
                        {
                            double left = current_hour_position + 5;
                            if (left < leftmost_point)
                            {
                                if(date_index == 1)
                                {
                                    left = leftmost_point + 5;
                                    date_string_aligned = true;
                                }
                                else
                                text_block.Visibility = Visibility.Hidden;
                            }
                            else
                                date_string_aligned = true;

                            Canvas.SetLeft(text_block, left);
                            //Canvas.SetBottom(text_block, _time_scale_control.Config.Padding.Bottom);
                            Canvas.SetBottom(text_block, 0);

                        }
                        else
                        {
                            double top = current_hour_position + 5;
                            if (top < bottommost_point)
                            {
                                if(date_index == 1)
                                {
                                    top = leftmost_point + 5;
                                    date_string_aligned = true;
                                }
                                else
                                text_block.Visibility = Visibility.Hidden;
                            }
                            else
                                date_string_aligned = true;

                            //Canvas.SetRight(text_block, _time_scale_control.Config.Padding.Right);
                            Canvas.SetRight(text_block, 0);
                            Canvas.SetBottom(text_block, top);
                        }
                    }
                }

                if (DateTimeUtil.IsDayChange(hour) && _outer_lines.Count > day_index) // align date lines
                {
                    Rectangle rectangle = _outer_lines.ToArray()[day_index++];
                    if (orientation == Orientation.Horizontal)
                    {
                        if (current_hour_position + 5 < leftmost_point)
                            rectangle.Visibility = Visibility.Hidden;

                        Canvas.SetLeft(rectangle, current_hour_position);
                        //Canvas.SetTop(rectangle, _time_scale_control.Config.Padding.Top);
                        Canvas.SetTop(rectangle, 0);
                    }
                    else
                    {
                        if (current_hour_position + 5 < bottommost_point)
                            rectangle.Visibility = Visibility.Hidden;

                        //Canvas.SetLeft(rectangle, _time_scale_control.Config.Padding.Left);
                        Canvas.SetLeft(rectangle, 0);
                        Canvas.SetBottom(rectangle, current_hour_position);
                    }
                }
                else
                {
                    int hour_modulo = int_hours % _line_lod;
                    if (hour_modulo == 0 && _long_lines.Count > hour_index) // draw hour lines
                    {
                        Rectangle rectangle = _long_lines.ToArray()[hour_index++];
                        if (orientation == Orientation.Horizontal)
                        {
                            if (current_hour_position > rightmost_point || current_hour_position < leftmost_point)
                                rectangle.Visibility = Visibility.Hidden;

                            Canvas.SetLeft(rectangle, current_hour_position);
                            //Canvas.SetTop(rectangle, _time_scale_control.Config.Padding.Top);
                            Canvas.SetTop(rectangle, 0);
                        }
                        else
                        {
                            if (current_hour_position > topmost_point || current_hour_position < bottommost_point)
                                rectangle.Visibility = Visibility.Hidden;

                            //Canvas.SetLeft(rectangle, _time_scale_control.Config.Padding.Left);
                            Canvas.SetLeft(rectangle, 0);
                            Canvas.SetBottom(rectangle, current_hour_position);
                        }
                    }

                    int text_modulo = int_hours % _text_lod;
                    if (text_modulo == 0 && _text_lod < 12 && _long_texts.Count > text_index) // draw hour texts
                    {
                        TextBlock text_block = _long_texts.ToArray()[text_index++];
                        FormattedText ft = new FormattedText(text_block.Text, Thread.CurrentThread.CurrentCulture, FlowDirection.LeftToRight, new Typeface(TextBlock.FontStyleProperty.Name), 10, Brushes.Black);

                        if (orientation == Orientation.Horizontal)
                        {
                            double pos = current_hour_position - ft.Width / 2;
                            if (pos > rightmost_point || pos < leftmost_point)
                                text_block.Visibility = Visibility.Hidden;

                            Canvas.SetLeft(text_block, current_hour_position - ft.Width / 2);
                            //Canvas.SetTop(text_block, _time_scale_control.Config.Padding.Top + LONG_LINE_HEIGHT + 3);
                            Canvas.SetTop(text_block, LONG_LINE_HEIGHT + 3);
                        }
                        else
                        {
                            double pos = current_hour_position - ft.Width / 2;
                            if (pos > topmost_point || pos < bottommost_point)
                                text_block.Visibility = Visibility.Hidden;

                            //Canvas.SetLeft(text_block, _time_scale_control.Config.Padding.Left + LONG_LINE_HEIGHT + 3);
                            Canvas.SetLeft(text_block, LONG_LINE_HEIGHT + 3);
                            Canvas.SetBottom(text_block, current_hour_position - ft.Width / 2);
                        }
                    }
                }

                if (hour == end)
                    break;

                if (_line_lod == 1 && hour_distance / 2 >= MIN_LINE_DISTANCE && _middle_lines.Count > half_hour_index) // draw half hour lines
                {
                    double current_half_position = current_hour_position + hour_distance / 2;
                    Rectangle rectangle = _middle_lines.ToArray()[half_hour_index++];
                    if (orientation == Orientation.Horizontal)
                    {
                        if (current_half_position > rightmost_point || current_half_position < leftmost_point)
                            rectangle.Visibility = Visibility.Hidden;

                        Canvas.SetLeft(rectangle, current_half_position);
                        //Canvas.SetTop(rectangle, _time_scale_control.Config.Padding.Top);
                        Canvas.SetTop(rectangle, 0);
                    }
                    else
                    {
                        if (current_half_position > topmost_point || current_half_position < bottommost_point)
                            rectangle.Visibility = Visibility.Hidden;

                        //Canvas.SetLeft(rectangle, _time_scale_control.Config.Padding.Left);
                        Canvas.SetLeft(rectangle, 0);
                        Canvas.SetBottom(rectangle, current_half_position);
                    }
                }
                if (_line_lod == 1 && hour_distance / 4 >= MIN_LINE_DISTANCE && _short_lines.Count > quater_hour_index) // draw quater hour lines
                {
                    double current_quater_position = current_hour_position + hour_distance / 4;
                    for (int i = 0; i < 2; i++)
                    {
                        Rectangle rectangle = _short_lines.ToArray()[quater_hour_index++];
                        if (orientation == Orientation.Horizontal)
                        {
                            if (current_quater_position > rightmost_point || current_quater_position < leftmost_point)
                                rectangle.Visibility = Visibility.Hidden;

                            Canvas.SetLeft(rectangle, current_quater_position);
                            //Canvas.SetTop(rectangle, _time_scale_control.Config.Padding.Top);
                            Canvas.SetTop(rectangle, 0);
                        }
                        else
                        {
                            if (current_quater_position > topmost_point || current_quater_position < bottommost_point)
                                rectangle.Visibility = Visibility.Hidden;

                            //Canvas.SetLeft(rectangle, _time_scale_control.Config.Padding.Left);
                            Canvas.SetLeft(rectangle, 0);
                            Canvas.SetBottom(rectangle, current_quater_position);
                        }
                        current_quater_position += hour_distance / 2;
                    }
                }

                current_hour_position += hour_distance;
            }

            if (!date_string_aligned && _long_dates.Count > 0)
            {
                TextBlock text_block = _long_dates.ToArray()[0];
                if (orientation == Orientation.Horizontal)
                {
                    //double pos = _time_scale_control.Config.Padding.Left + 5;
                    double pos = 5;
                    if (pos < leftmost_point)
                        text_block.Visibility = Visibility.Hidden;
                    else
                        text_block.Visibility = Visibility.Visible;

                    Canvas.SetLeft(text_block, pos);
                    //Canvas.SetBottom(text_block, _time_scale_control.Config.Padding.Bottom);
                    Canvas.SetBottom(text_block, 0);
                }
                else
                {
                    //double pos = _time_scale_control.Config.Padding.Bottom + 5;
                    double pos = 5;
                    if (pos < bottommost_point)
                        text_block.Visibility = Visibility.Hidden;
                    else
                        text_block.Visibility = Visibility.Visible;

                    //Canvas.SetRight(text_block, _time_scale_control.Config.Padding.Right);
                    Canvas.SetRight(text_block, 0);
                    Canvas.SetBottom(text_block, pos);
                }

                date_string_aligned = true;
            }

        }

        public override double GetUnitSize(Orientation orientation)
        {
            //DateTime start = DateTimeUtil.GetFullHour(_time_scale_control.Config.Start);
            //DateTime end = DateTimeUtil.GetFullHour(_time_scale_control.Config.End);
            DateTime start = _time_scale_control.Config.Start;
            DateTime end = _time_scale_control.Config.End;
            double number_hours = ((end - start).TotalMilliseconds / ONE_OUR_MSEC);

            double num_units = number_hours;
            if (_lod != 0)
                num_units /= _lod;

            if (orientation == Orientation.Horizontal)
                return _time_scale_control.GetRenderWidth() / num_units;

            return _time_scale_control.GetRenderHeight() / num_units;
        }

        public override double GetUnitOffset(Orientation orientation)
        {
            return _unit_offset;
        }

        public override double GetUnitOffset()
        {
            //double first = _time_scale_control.TransformAxisValueToPixel(_time_scale_control.Config.Start);
            //double second = _time_scale_control.TransformAxisValueToPixel(_time_scale_control.Config.Start.AddHours(1));
            //double hour_distance = (second - first);
            //double minute_offset_in_hours = (double)_time_scale_control.Config.Start.Minute / 60 + (double)_time_scale_control.Config.Start.Second / 3600;

            //return minute_offset_in_hours * hour_distance;

            //return _unit_offset;
            return 0;
        }

        private static double GetMaxDescriptionSize(int number)
        {
            double max = 0;
            for (int i = 0; i < number; i++)
            {
                FormattedText ft = new FormattedText(i.ToString(), Thread.CurrentThread.CurrentCulture, FlowDirection.LeftToRight, new Typeface(TextBlock.FontStyleProperty.Name), 10, Brushes.Black);
                if (ft.Width > max)
                    max = ft.Width;
            }

            return max;
        }

        private double GetOffsetHours(DateTime date_time)
        {
            double hours_to_day_change = Math.Round(DateTimeUtil.GetHoursToDayChange(date_time), 0);
            if (hours_to_day_change % _lod == 0)
                return 0;

            return hours_to_day_change % _lod;
        }

        private static string GetDate(DateTime start, DateTime end, DateTime date, double hour_distance)
        {
            double dist = hour_distance * 24 / 1.5;
            if (date == start)
            {
                dist = DateTimeUtil.GetHoursToDayChange(date) * hour_distance / 1.5;
            }
            if (date.Day == end.Day)
            {
                dist = DateTimeUtil.GetHoursToEnd(date, end) * hour_distance / 1.5;
            }

            string str = date.ToShortDateString();
            FormattedText ft = new FormattedText(str, Thread.CurrentThread.CurrentCulture, FlowDirection.LeftToRight, new Typeface(TextBlock.FontStyleProperty.Name), 10, Brushes.Black);

            if (ft.Width <= dist)
                return str;

            str = date.Day + "." + date.Month + ".";
            ft = new FormattedText(str, Thread.CurrentThread.CurrentCulture, FlowDirection.LeftToRight, new Typeface(TextBlock.FontStyleProperty.Name), 10, Brushes.Black);
            if (ft.Width <= dist)
                return str;

            str = date.Day + ".";
            ft = new FormattedText(str, Thread.CurrentThread.CurrentCulture, FlowDirection.LeftToRight, new Typeface(TextBlock.FontStyleProperty.Name), 10, Brushes.Black);
            if (ft.Width <= dist)
                return str;

            return "";
        }

        public override List<double> GetGridLinePositions()
        {
            List<double> positions = new List<double>();

            if (_time_scale_control.GetRenderHeight() == 0)
                return positions;

            double unit_size = GetUnitSize(_time_scale_control.Config.Orientation);
            double current_pos = GetUnitOffset(_time_scale_control.Config.Orientation);

            if (_time_scale_control.Config.Orientation == Orientation.Horizontal)
            {
                //current_pos += _time_scale_control.Config.Padding.Left;
                //double right_limit = _time_scale_control.GetRenderWidth() + _time_scale_control.Config.Padding.Left;
                double right_limit = _time_scale_control.GetRenderWidth();
                //double left_limit = _time_scale_control.Config.Padding.Left;
                double left_limit = 0;
                while (current_pos < right_limit && current_pos > left_limit)
                {
                    positions.Add(current_pos);
                    current_pos += unit_size;
                }
            }
            else
            {
                //current_pos = _time_scale_control.GetRenderHeight() + _time_scale_control.Config.Padding.Top - (current_pos);
                current_pos = _time_scale_control.GetRenderHeight() - current_pos;
                //double bottom_limit = _time_scale_control.Config.Padding.Bottom;
                double bottom_limit = 0;
                while (current_pos > bottom_limit)
                {
                    positions.Add(current_pos);
                    current_pos -= unit_size;
                }
            }

            return positions;
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            _time_scale_control = null;
            _outer_lines.Clear();
            _long_lines.Clear();
            _middle_lines.Clear();
            _short_lines.Clear();
            _long_texts.Clear();
            _long_dates.Clear();
        }

        #endregion

        #region Event Handlers
        #endregion

        #region Properties

        internal override Object Control
        {
            get
            {
                return _time_scale_control;
            }
            set
            {
                if (value == null)
                    throw new ArgumentException("Control must not be null!");

                if (value is TimeScaleControl)
                {
                    _time_scale_control = (TimeScaleControl)value;
                }
                else
                    throw new ArgumentException("Type " + value.GetType());
            }
        }

        #endregion

        #region Dependency Properties
        #endregion

        #region Events
        #endregion

        #region Routed Events
        #endregion

        #region Attributes

        private short _line_lod;
        private short _text_lod;
        private short _lod;

        private TimeScaleControl _time_scale_control;

        private double _unit_offset;

        private readonly List<Rectangle> _outer_lines = new List<Rectangle>();
        private readonly List<Rectangle> _long_lines = new List<Rectangle>();
        private readonly List<Rectangle> _middle_lines = new List<Rectangle>();
        private readonly List<Rectangle> _short_lines = new List<Rectangle>();

        private readonly List<TextBlock> _long_texts = new List<TextBlock>();
        private readonly List<TextBlock> _long_dates = new List<TextBlock>();

        #endregion

        #region Tests
        #endregion

    }
}
