﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Media;
using Logicx.WpfUtility.WpfHelpers;

namespace Logicx.WpfUtility.AsyncVisualRender
{
    public class RenderHelper
    {
        public static void SetScaling(FrameworkElement visual, WpfRenderTask rti)
        {
            ScaleTransform scale_transform;// = new ScaleTransform(1, 1);
            TranslateTransform transl_transform;// = new TranslateTransform(0, 0);
            TransformGroup transform_group;// = new TransformGroup();

            if(visual.RenderTransform is TransformGroup)
            {
                transform_group = (TransformGroup)visual.RenderTransform;
                scale_transform = (ScaleTransform)transform_group.Children[0];
                transl_transform = (TranslateTransform)transform_group.Children[1];
            }
            else
            {
                scale_transform = new ScaleTransform(1, 1);
                transl_transform = new TranslateTransform(0, 0);
                transform_group = new TransformGroup();
                transform_group.Children.Add(scale_transform);
                transform_group.Children.Add(transl_transform);
                visual.RenderTransform = transform_group;
            }

            transl_transform.X = -rti.Left;
            transl_transform.Y = -rti.Top;

            if (visual.ActualWidth != 0 && visual.ActualHeight != 0)
            {
                if (rti.DesiredLen.HasValue)
                {
                    rti.ActualWidth = (float)visual.ActualWidth;
                    rti.ActualHeight = (float)visual.ActualHeight;
                    float scale = rti.DesiredWidth / (float)visual.ActualWidth;
                    scale_transform.ScaleX = scale;
                    scale_transform.ScaleY = scale;
                }
                else
                {
                    scale_transform.ScaleX = 1;
                    scale_transform.ScaleY = 1;
                }
            }
            else
            {
                scale_transform.ScaleX = 1;
                scale_transform.ScaleY = 1;
            }
        }

        public static void UpdateRti(FrameworkElement visual, WpfRenderTask rti)
        {
            ScaleTransform scale_transform = (ScaleTransform)((TransformGroup)visual.RenderTransform).Children[0];

            //save control dimensions
            float width = (float)visual.ActualWidth;
            float height = (float)visual.ActualHeight;
            float aspect_ratio = width / height;
            rti.ActualWidth = width;
            rti.ActualHeight = height;

            if (scale_transform.ScaleX == 1)
            {
                float scale = 1;
                if(aspect_ratio >= 1)
                    scale = rti.DesiredWidth / rti.ActualWidth;
                else
                    scale = rti.DesiredHeight / rti.ActualHeight; 
                scale_transform.ScaleX = scale;
                scale_transform.ScaleY = scale;
            }

            rti.RenderWidth = (float)(rti.ActualWidth * scale_transform.ScaleX - rti.Left);
            rti.RenderHeight = (float)(rti.ActualHeight * scale_transform.ScaleX - rti.Top);


            if (rti.DesiredWidth < rti.RenderWidth)
            {
                rti.RenderWidth = rti.DesiredWidth;
                rti.RenderHeight = rti.RenderWidth / (rti.ActualWidth / rti.ActualHeight);
            }
            if (rti.DesiredHeight < rti.RenderHeight)
            {
                rti.RenderHeight = rti.DesiredHeight;
                rti.RenderWidth = rti.RenderHeight * (rti.ActualWidth / rti.ActualHeight);
            }



            if(rti.DesiredLenMaxWidth.HasValue && rti.DesiredLenMaxWidth < rti.RenderWidth)
            {
                rti.RenderWidth = rti.DesiredLenMaxWidth.Value;
            }
            else if (rti.DesiredLenMax.HasValue && rti.DesiredLenMax < rti.RenderWidth)
            {
                rti.RenderWidth = rti.DesiredLenMax.Value;
            }

            if (rti.DesiredLenMaxHeight.HasValue && rti.DesiredLenMaxHeight < rti.RenderHeight)
            {
                rti.RenderHeight = rti.DesiredLenMaxHeight.Value;
            }
            else if (rti.DesiredLenMax.HasValue && rti.DesiredLenMax < rti.RenderHeight)
            {
                rti.RenderHeight = rti.DesiredLenMax.Value;
            }
        }




 
        public static void CaptureImage(FrameworkElement visual, WpfRenderTask rti)
        {
            if (rti.RenderToDisk)
                UIHelper.CaptureAndSaveScreen(rti.RenderToDiskPath, visual, rti.RenderWidthPx, rti.RenderHeightPx);
            else
            {
                using (MemoryStream stream = UIHelper.CaptureImage(visual, rti.RenderWidthPx, rti.RenderHeightPx, rti.PixelFormat))
                {
                    rti.Data = stream.ToArray();
                }
            }
        }
    }
}
