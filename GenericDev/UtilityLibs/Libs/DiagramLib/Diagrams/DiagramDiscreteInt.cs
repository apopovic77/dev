﻿using System;
using Logicx.DiagramLib.Controls;
using Logicx.DiagramLib.Scales;
using Logicx.DiagramLib.ViewConfig;

namespace Logicx.DiagramLib.Diagrams
{
    public class DiagramDiscreteInt : DiagramBase<Object, int>
    {
        #region Constants
        #endregion

        #region Construction and Initialization
        public DiagramDiscreteInt(DiagramConfig<Object, int> diagram_config, string int_values_ascending_string)
            : base(diagram_config)
        {
            _int_values_ascending_string = int_values_ascending_string;
        }
        #endregion

        #region Operations

        protected override Scales.TypedScaleControl<Object> GetXAxisControl()
        {
            DiagramConfig.AxisScaleConfigX.Orientation = System.Windows.Controls.Orientation.Horizontal;

            if (_discrete_sct == null)
            {
                _discrete_sct = new DiscreteScaleControl<object>(DiagramConfig.AxisScaleConfigX);
                _discrete_sct.IsHitTestVisible = false;
            }

            return _discrete_sct;
        }

        protected override Scales.TypedScaleControl<int> GetYAxisControl()
        {
            DiagramConfig.AxisScaleConfigY.Orientation = System.Windows.Controls.Orientation.Vertical;

            if (_isc == null)
            {
                _isc = new IntScaleControl(DiagramConfig.AxisScaleConfigY, _int_values_ascending_string);
                _isc.IsHitTestVisible = false;
            }

            return _isc;
        }

        protected override double TransformXAxisValueToPixel(Object value)
        {
            if (XAxisControl != null)
                return XAxisControl.TransformAxisValueToPixel(value);

            return 0;
        }

        protected override double TransformYAxisValueToPixel(int value)
        {
            if (YAxisControl != null)
                return YAxisControl.TransformAxisValueToPixel(value);

            return 0;
        }

        protected override Object TransformXAxisPixelToValue(double pixel)
        {
            if (XAxisControl != null)
                return XAxisControl.TransformAxisPixelToValue(pixel);

            return DateTime.MinValue;
        }

        protected override int TransformYAxisPixelToValue(double pixel)
        {
            if (YAxisControl != null)
                return YAxisControl.TransformAxisPixelToValue(pixel);

            return 0;
        }
        #endregion

        #region Event Handlers
        #endregion

        #region Properties
        #endregion

        #region Dependency Properties
        #endregion

        #region Events
        #endregion

        #region Routed Events
        #endregion

        #region Attributes

        private DiscreteScaleControl<Object> _discrete_sct;
        private IntScaleControl _isc;
        private string _int_values_ascending_string = "1";

        #endregion


        #region Tests

        #endregion


    }
}
