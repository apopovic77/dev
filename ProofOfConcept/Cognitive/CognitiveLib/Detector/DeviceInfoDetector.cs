﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.ModelBinding;
using CognitiveLib.ApiService.EmguCV;
using CognitiveLib.Detector;
using CognitiveLib.Ocr;
using CognitiveLib.Ocr.Azure;
using CognitiveLib.Scanner;
using CognitiveLib.WebSearch;
using FSBlog.GoogleSearch.GoogleClient;
using Logicx.Gis.Data.RenderedGeoData;
using Utilities;
using IronOcr = CognitiveLib.Ocr.IronOcr;

namespace CognitiveLib.Typenschild
{
    public struct TypeDetectionResult
    {
        public StringDetectionResult Brand;
        public ModelDetectionResult ModelNoDR;
        public ModelCodeDetectionResult ModelCodeDR;
        public TypeNoDetectionResult TypeNoDR;
        public VersionNoDetectionResult VersionNoDR;
        public SerialDetectionResult SerialNoDR;
        public BarcodeScanResult BarcodeScanResult;


        public bool IsImageWithBarcode
        {
            get
            {
                if (string.IsNullOrEmpty(OcrText))
                    return false;
                string pattern = "[ILil/ ]{ 5,}";
                Regex rgx = new Regex(pattern, RegexOptions.IgnoreCase);
                return rgx.IsMatch(OcrText);
            }
        }

        public string OcrText { get; set; }

        public string AzureOcrText
        {
            get { return _azure_ocr; }
            set
            {
                if (OcrText == null)
                    OcrText = value;
                _azure_ocr = value;
            }
        }
        public string GoogleOcrText
        {
            get { return _google_ocr; }
            set
            {
                if (OcrText == null)
                    OcrText = value;
                _google_ocr = value;
            }
        }

        public string EmguOcrText
        {
            get { return _emgu_ocr; }
            set
            {
                if (OcrText == null)
                    OcrText = value;
                _emgu_ocr = value;
            }
        }

        public string IronOcrText
        {
            get { return _iron_ocr; }
            set
            {
                if (OcrText == null)
                    OcrText = value;
                _iron_ocr = value;
            }
        }



        private string _azure_ocr, _google_ocr, _emgu_ocr, _iron_ocr;
    }

    public class DeviceInfoDetector
    {


        public static TypeDetectionResult Detect(string path_to_img)
        {
            TypeDetectionResult res = new TypeDetectionResult();
            List<string> sep_words_list = new List<string> { "Model", "S/N", "S / N", " SN ", "SIN ", " SIN", "P/N", "P / N", " PN ", "PIN ", " PIN", "Code", "Type","Version No", "Version", "Ver" };
            //1. warp to front

            //2. cluster test regions, maybe some text is vertical

            //3. run ocr
            //res.OcrText = res.AzureOcrText  = AzureOcr.GetText(path_to_img);
            res.OcrText = res.IronOcrText = Ocr.IronOcr.GetText(path_to_img);
            res.OcrText = StringDetector.SepLines(res.OcrText, sep_words_list);
            res.OcrText = StringDetector.UnSepLines(res.OcrText, sep_words_list);

            //res.EmguOcrText   = EmguOcr.GetText(path_to_img);

            //4. detect brand in char stream
            res.Brand = BrandDetector.DetectBrand(res.OcrText);
            res.Brand.BestStringMatchByWeight.AddWeight(1000,"InOcr");
            
            //6. detect model in char stream
            DetectModel(ref res);
            DetectModelCode(ref res);
            DetectTypeNo(ref res);
            //5. detect serial in char steam
            DetectSerialNo(ref res);
            DetectVersionNo(ref res);

            //scan barcodes
            ScanBarcodes(path_to_img, ref res);



            if (res.Brand.IsEmpty && (res.ModelNoDR == null || res.ModelNoDR.IsEmpty))
            {
                res.OcrText = res.GoogleOcrText = GoogleOcr.DetectDocumentText(path_to_img);
                res.OcrText = StringDetector.SepLines(res.OcrText, sep_words_list);
                res.OcrText = StringDetector.UnSepLines(res.OcrText, sep_words_list);
                res.Brand = BrandDetector.DetectBrand(res.OcrText);
                res.Brand.BestStringMatchByWeight.AddWeight(1000, "InOcr");
                
                DetectModel(ref res);
                DetectModelCode(ref res);
                DetectTypeNo(ref res);
                DetectSerialNo(ref res);
                DetectVersionNo(ref res);
            }


            return res;
        }

        private static void ScanBarcodes(string path_to_img, ref TypeDetectionResult res)
        {
            res.BarcodeScanResult = BarcodeScanner.ReadBarcodes(path_to_img);
        }


        private static void DetectModel(ref TypeDetectionResult res)
        {
            if (string.IsNullOrEmpty(res.OcrText))
                return;

            ModelDetectionResult mdr = DeviceModelNoDetector.Detect(res.OcrText);
            res.ModelNoDR = mdr;
        }

        private static void DetectSerialNo(ref TypeDetectionResult res)
        {
            if (string.IsNullOrEmpty(res.OcrText))
                return;

            SerialDetectionResult sdr = DeviceSerialDetector.Detect(res.OcrText);

            SimilarStringComparer sc = new SimilarStringComparer();
            if (!(res.ModelNoDR.ModelNo.String != null && sc.IsEqual(res.ModelNoDR.ModelNo.String, sdr.SerialNo.String) > 0.8f))
                res.SerialNoDR = sdr;
        }

        private static void DetectModelCode(ref TypeDetectionResult res)
        {
            if (string.IsNullOrEmpty(res.OcrText))
                return;

            ModelCodeDetectionResult mdr = DeviceModelCodeDetector.Detect(res.OcrText);
            res.ModelCodeDR = mdr;
        }

        private static void DetectTypeNo(ref TypeDetectionResult res)
        {
            if (string.IsNullOrEmpty(res.OcrText))
                return;

            TypeNoDetectionResult mdr = DeviceTypeNoDetector.Detect(res.OcrText);
            res.TypeNoDR = mdr;
        }


        private static void DetectVersionNo(ref TypeDetectionResult res)
        {
            if (string.IsNullOrEmpty(res.OcrText))
                return;

            VersionNoDetectionResult mdr = DeviceVersionNoDetector.Detect(res.OcrText);

            SimilarStringComparer sc = new SimilarStringComparer();
            if (!(res.SerialNoDR != null &&  res.SerialNoDR.SerialNo.String != null && sc.IsEqual(res.SerialNoDR.SerialNo.String, mdr.VersionNo.String) > 0.8f))
                res.VersionNoDR = mdr;
        }

    }
}

