using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.DirectX.Direct3D;
using Microsoft.DirectX;
using System.Drawing;

namespace Logicx.DirectX.Objects.Meshes
{

    //-----------------------------------------------------------------------
    // AutoTexture
    //-----------------------------------------------------------------------
    public class AutoTexture
    {
        Texture mTexture;
        Device mD3d;
        int mPixelSizeBits;
        byte[] mTextureData;
        SurfaceDescription mSurfDescription;

        /// <summary>
        /// Unused by Direct3d class.  You can use this field
        /// to store miscellaneous info with the texture.
        /// </summary>
        public AutoTextureInfo Tag;
        public class AutoTextureInfo { }

        /// <summary>
        /// Returns information about level 0 surface
        /// </summary>
        public SurfaceDescription Description
        {
            get { return mSurfDescription; }
        }

        public int Width { get { return mSurfDescription.Width; } }
        public int Height { get { return mSurfDescription.Height; } }

        /// <summary>
        /// Return the mesh (or null if the device is lost)
        /// </summary>
        public Texture T { get { return mTexture; } }

        /// <summary>
        /// Create an AutoTexture
        /// </summary>
        public AutoTexture(Device device3d,Texture texture)
        {
            mD3d = device3d;
            mTexture = texture;
            mSurfDescription = mTexture.GetLevelDescription(0);
            device3d.DeviceLost += new EventHandler(d3d_DxLost);
            device3d.DeviceReset += new EventHandler(d3d_DxRestore);
        }

        /// <summary>
        /// Dispose this object and the mesh it holds
        /// </summary>
        public void Dispose()
        {
            if (mTexture != null)
            {
                mTexture.Dispose();
                mTexture = null;
            }
            if (mD3d != null)
            {
                mD3d.DeviceLost -= new EventHandler(d3d_DxLost);
                mD3d.DeviceReset -= new EventHandler(d3d_DxRestore);
                mD3d = null;
            }
            mTextureData = null;
        }

        /// <summary>
        /// The device must not be lost when this function is called.  This clone function
        /// can copy textures to different DirectX devices and also change the format.
        /// </summary>
        public AutoTexture Clone(Device d3d, Format format, Usage usage, Pool pool)
        {
            // Copy the texture
            Texture toTexture = new Texture(d3d, mSurfDescription.Width, mSurfDescription.Height,
                                            1, usage, format, pool);

            Surface toSurface = toTexture.GetSurfaceLevel(0);
            Surface fromSurface = mTexture.GetSurfaceLevel(0);
            SurfaceLoader.FromSurface(toSurface, fromSurface, Filter.Point, 0);
            toSurface.Dispose();
            fromSurface.Dispose();

            // Copy this AutoTexture
            AutoTexture autoTexture = new AutoTexture(d3d, toTexture);
            autoTexture.Tag = Tag;
            return autoTexture;
        }

        /// <summary>
        /// The device must not be lost when this function is called.
        /// </summary>
        /// <returns></returns>
        public AutoTexture Clone()
        {
            return Clone(mD3d, mSurfDescription.Format, mSurfDescription.Usage, mSurfDescription.Pool);
        }

        /// <summary>
        /// Save the mesh when the DirectX device is lost
        /// </summary>
        void d3d_DxLost(object sender, EventArgs ev)
        {
            if (mTexture == null)
                return;

            // Get surface description
            int width = mSurfDescription.Width;
            int height = mSurfDescription.Height;

            // Isn't there a better way to do this?
            // Just convert everything to 32BppArgb by using the clone method
            switch (mSurfDescription.Format)
            {
                case Format.Dxt1:
                    //Debug.Assert(false); // Strange format
                    mPixelSizeBits = 4;
                    break;

                case Format.A8:
                    mPixelSizeBits = 8;
                    break;

                case Format.A4L4:
                case Format.Dxt2:
                case Format.Dxt3:
                case Format.Dxt4:
                case Format.Dxt5:
                    mPixelSizeBits = 8;
                    //Debug.Assert(false);
                    break;


                case Format.A8R3G3B2:
                case Format.A4R4G4B4:
                case Format.A1R5G5B5:
                    mPixelSizeBits = 16;
                    break;

                case Format.G8R8G8B8:
                case Format.A8R8G8B8:
                case Format.X8R8G8B8:
                case Format.A8B8G8R8:
                case Format.X8B8G8R8:
                    mPixelSizeBits = 32;
                    break;
                default:
                    // Insert your vertexFormat above
                    mPixelSizeBits = 32;
                    //Debug.Assert(false);
                    break;
            }

            // Read the mesh data
            int pitch;
            mTextureData = (byte[])mTexture.LockRectangle(typeof(byte), 0, LockFlags.ReadOnly,
                                                        out pitch, mPixelSizeBits * width * height / 8);
            //Debug.Assert(pitch == mPixelSizeBits*width/8); // New versions of DX fixes Pitch for us
            mTexture.UnlockRectangle(0);
            mTexture.Dispose();
            mTexture = null;
        }

        /// <summary>
        /// Restore the mesh when the DirectX device is restored
        /// </summary>
        void d3d_DxRestore(object sender, EventArgs ev)
        {
            // If the direct3d device wasn't lost in the first place, don't restore it.
            // This happens the first timeMs around.
            if (mTexture != null)
                return;

            // Create mesh
            int width = mSurfDescription.Width;
            int height = mSurfDescription.Height;
            mTexture = new Texture(mD3d, width, height, 1, mSurfDescription.Usage, mSurfDescription.Format, mSurfDescription.Pool);

            // Write the texture data
            int pitch;
            GraphicsStream stream = mTexture.LockRectangle(0, LockFlags.None, out pitch);
            //Debug.Assert(pitch == mPixelSizeBits*width/8); // New versions of DX fixes Pitch for us
            //Debug.Assert(pitch*height == mTextureData.Length);
            stream.Write(mTextureData);
            mTexture.UnlockRectangle(0);
            mTextureData = null;
        }

        /// <summary>
        /// Convert a gray bitmap to white wite with alpha color
        /// </summary>
        public void SetAlphaConstant(int alpha)
        {
            SurfaceDescription description = mTexture.GetLevelDescription(0);
            if (description.Format != Format.A8R8G8B8)
                throw new Exception("SetAlphaConstant: Invalid pixel format (A8R8G8B8 required)");

            // Generate an alphamap
            int width = description.Width;
            int height = description.Height;
            int pitch;
            Color32[] bm = (Color32[])mTexture.LockRectangle(
                                            typeof(Color32), 0, 0, out pitch, width * height);

            for (int i = 0; i < bm.Length; i++)
                bm[i] = new Color32(alpha, bm[i]);
            mTexture.UnlockRectangle(0);
        }

        /// <summary>
        /// Convert a gray bitmap to white with alpha color
        /// </summary>
        public void SetAlphaFromGray()
        {
            SurfaceDescription description = mTexture.GetLevelDescription(0);
            if (description.Format != Format.A8R8G8B8)
                throw new Exception("SetAlphaFromGray: Invalid pixel format (A8R8G8B8 required)");

            // Generate an alphamap
            int width = description.Width;
            int height = description.Height;
            int pitch;
            Color32[] bm = (Color32[])mTexture.LockRectangle(
                                            typeof(Color32), 0, 0, out pitch, width * height);

            for (int i = 0; i < bm.Length; i++)
            {
                Color32 color = bm[i];
                int alpha = (color.iR + color.iG + color.iB) / 3;
                bm[i] = new Color32(alpha, new Color32(255, 255, 255));
            }
            mTexture.UnlockRectangle(0);
        }

        /// <summary>
        /// Reset texture color (leave apha channel alone)
        /// </summary>
        public void SetAlphaColor(Color32 color)
        {
            SurfaceDescription description = mTexture.GetLevelDescription(0);
            if (description.Format != Format.A8R8G8B8)
                throw new Exception("SetAlphaColor: Invalid pixel format (A8R8G8B8 required)");

            // Generate an alphamap
            int width = description.Width;
            int height = description.Height;
            int pitch;
            Color32[] bm = (Color32[])mTexture.LockRectangle(typeof(Color32),
                                            0, LockFlags.Discard, out pitch, width * height);

            for (int i = 0; i < bm.Length; i++)
                bm[i] = new Color32(bm[i].iA, color);
            mTexture.UnlockRectangle(0);
        }

        /// <summary>
        /// Generate an alpha map for the texture.  min/max color and alpha parameters are 0..255.
        /// Typically min/max alpha will be (0, 255), and min/max color will be a small range
        /// like (16, 32) to create a soft edge.
        /// </summary>
        public void SetAlphaFade(int minAlpha, int maxAlpha, int minColor, int maxColor)
        {
            SurfaceDescription description = mTexture.GetLevelDescription(0);
            if (description.Format != Format.A8R8G8B8)
                throw new Exception("SetAlphaColor: Invalid pixel format (A8R8G8B8 required)");

            // Generate an alphamap
            int width = description.Width;
            int height = description.Height;
            int pitch;
            Color32[] bm = (Color32[])mTexture.LockRectangle(typeof(Color32),
                                                            0, 0, out pitch, width * height);

            // Multiply by 3 because alpha calculation is R+G+B
            minColor *= 3;
            maxColor *= 3;

            for (int i = 0; i < bm.Length; i++)
            {
                Color32 color = bm[i];

                int colorSum = color.iR + color.iG + color.iB; // alpha = color*3

                // Calculate alpha
                int alpha;
                if (colorSum >= maxColor)
                    alpha = maxAlpha;
                else if (colorSum <= minColor)
                    alpha = minAlpha;
                else
                {
                    // Convert colorSum from (minColor..maxColor) to (minApha..maxAlpha)
                    alpha = (colorSum - minColor) * (maxAlpha - minAlpha) / (maxColor - minColor) + minAlpha;
                }

                bm[i] = new Color32(alpha, color);
            }
            mTexture.UnlockRectangle(0);
        }

    }


    /// <summary>
    /// Light weight wrapper for a color (32 bits)
    /// </summary>
    public struct Color32
    {
        int mArgb;

        /// <summary>
        /// Convert from an int to a color
        /// </summary>
        public Color32(int argb)
        {
            mArgb = argb;
        }

        /// <summary>
        /// Convert RGB (0..255) to a color
        /// </summary>
        public Color32(int red, int green, int blue)
        {
            mArgb = (red << 16) | (green << 8) | blue | (0xFF << 24);
        }

        /// <summary>
        /// Convert alpha and RGB (0..255) to a color
        /// </summary>
        public Color32(int alpha, int red, int green, int blue)
        {
            mArgb = (alpha << 24) | (red << 16) | (green << 8) | blue;
        }

        /// <summary>
        /// Copy a color
        /// </summary>
        public Color32(Color32 color)
        {
            mArgb = color.mArgb;
        }

        /// <summary>
        /// Calculate a new alpha (0..255) for the color
        /// </summary>
        public Color32(int alpha, Color32 colorBase)
        {
            mArgb = (colorBase.mArgb & 0xFFFFFF) | (alpha << 24);
        }

        /// <summary>
        /// Convert RGB (0..1) to a color
        /// </summary>
        public Color32(float red, float green, float blue)
        {
            red = red * 255.5f;
            red = Math.Min(255.5f, Math.Max(0, red));
            green = green * 255.5f;
            green = Math.Min(255.5f, Math.Max(0, green));
            blue = blue * 255.5f;
            blue = Math.Min(255.5f, Math.Max(0, blue));
            mArgb = new Color32((int)red, (int)green, (int)blue);
        }

        /// <summary>
        /// Convert alpha and RGB (0..1) to a color
        /// </summary>
        public Color32(float alpha, float red, float green, float blue)
        {
            red = red * 255.5f;
            red = Math.Min(255.5f, Math.Max(0, red));
            green = green * 255.5f;
            green = Math.Min(255.5f, Math.Max(0, green));
            blue = blue * 255.5f;
            blue = Math.Min(255.5f, Math.Max(0, blue));
            alpha = alpha * 2545.5f;
            alpha = Math.Min(255.5f, Math.Max(0, alpha));
            mArgb = new Color32((int)alpha, (int)red, (int)green, (int)blue);
        }

        /// <summary>
        /// Fade from one color to another (percent is 0..1, 0 = source color, 1 = to color)
        /// </summary>
        public Color32 Fade(Color32 to, float percent)
        {
            return Fade(to, (int)(percent * 255.999f));
        }

        /// <summary>
        /// Fade from one color to another (factor is 0..255, 0 = source color, 255 = to color)
        /// </summary>
        public Color32 Fade(Color32 to, int factor)
        {
            int invAlpha = 255 - factor;
            to.mArgb = ((((mArgb & 0x00FF00FF) * invAlpha) >> 8) & 0x00FF00FF)
                        + ((((to.mArgb & 0x00FF00FF) * factor) >> 8) & 0x00FF00FF)
                        + ((((mArgb >> 8) & 0x00FF00FF) * invAlpha) & unchecked((int)0xFF00FF00))
                        + ((((to.mArgb >> 8) & 0x00FF00FF) * factor) & unchecked((int)0xFF00FF00));
            return to;
        }

        /// <summary>
        /// Fade from one color to another (percent is 0..1, 0 = source color, 1 = to color)
        /// </summary>
        public Color32 Fade(Color32 to, float alphaPercent, float colorPercent)
        {
            return Fade(to, (int)(alphaPercent * 255.999f), (int)(colorPercent * 255.999f));
        }

        /// <summary>
        /// Fade from one color to another (factor is 0..255, 0 = source color, 255 = to color)
        /// </summary>
        public Color32 Fade(Color32 to, int alphaFactor, int colorFactor)
        {
            int invAlpha = 255 - alphaFactor;
            int invColor = 255 - colorFactor;
            to.mArgb = ((((mArgb & 0x00FF00FF) * invColor) >> 8) & 0x00FF00FF)
                        + ((((to.mArgb & 0x00FF00FF) * colorFactor) >> 8) & 0x00FF00FF)
                        + ((((mArgb & 0x0000FF00) * invColor) >> 8) & 0x0000FF00)
                        + ((((to.mArgb & 0x0000FF00) * colorFactor) >> 8) & 0x0000FF00)
                        + ((((mArgb >> 8) & 0x00FF00FF) * invAlpha) & unchecked((int)0xFF000000))
                        + ((((to.mArgb >> 8) & 0x00FF00FF) * alphaFactor) & unchecked((int)0xFF000000));
            return to;
        }

        // Return RGB or A component of color (0..255)
        public int iA { get { return (mArgb >> 24) & 255; } }
        public int iR { get { return (mArgb >> 16) & 255; } }
        public int iG { get { return (mArgb >> 8) & 255; } }
        public int iB { get { return mArgb & 255; } }

        // Return RGB or A component of color (0..1)
        public float fA { get { return iA * (1f / 255f); } }
        public float fR { get { return iR * (1f / 255f); } }
        public float fG { get { return iG * (1f / 255f); } }
        public float fB { get { return iB * (1f / 255f); } }

        /// Implicitly convert an integer to a Color32
        public static implicit operator Color32(int color)
        {
            return new Color32(color);
        }

        /// Implicitly convert a Color32 to an int
        public static implicit operator int(Color32 color)
        {
            return color.mArgb;
        }

        /// Implicitly convert a Color to a Color32
        public static implicit operator Color32(Color color)
        {
            return new Color32(color.ToArgb());
        }

        /// Implicitly convert a Color32 to a Color
        public static implicit operator Color(Color32 color)
        {
            return Color.FromArgb(color.mArgb);
        }

        public static bool operator ==(Color32 a, Color32 b)
        {
            return a.mArgb == b.mArgb;
        }

        public static bool operator !=(Color32 a, Color32 b)
        {
            return a.mArgb != b.mArgb;
        }

        public override int GetHashCode()
        {
            return mArgb;
        }

        public override bool Equals(object obj)
        {
            return mArgb == (Color32)obj;
        }
    }	
}
