﻿using System;

namespace Awanto.WhatsAppCore.Entities
{
    public class EventLog : DbEntity
    {
        public int? EventId { get; set; }
        public string LogLevel { get; set; }
        public string Message { get; set; }
        public DateTime? CreatedTime { get; set; }
    }
}
