﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Data;

namespace Logicx.DiagramLib.Resources.Converter
{
    public class XAxisLeftPaddingConverter : IMultiValueConverter
    {
        #region IMultiValueConverter Members

        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            Thickness current_padding = new Thickness(0);

            if(parameter != null)
            {
                current_padding = (Thickness)parameter;    
            }

            if(values != null && values.Length == 2)
            {
                Visibility y_scale_visibility = (Visibility) values[0];
                

                if(y_scale_visibility == Visibility.Visible)
                {
                    
                    if(values[1] != DependencyProperty.UnsetValue)
                    {
                        double current_width = (double)values[1];
    
                        if(! double.IsNaN(current_width) && !double.IsInfinity(current_width))
                        {
                            return new Thickness(current_width, current_padding.Top, current_padding.Right, current_padding.Bottom);
                        }
                    }
                }
            }

             return current_padding;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }

        #endregion
    }
}
