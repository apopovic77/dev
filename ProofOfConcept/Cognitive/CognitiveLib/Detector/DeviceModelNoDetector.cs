﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using CognitiveLib.Scanner;
using CognitiveLib.WebSearch;
using FSBlog.GoogleSearch.GoogleClient;
using Logicx.Utility;
using Utilities;

namespace CognitiveLib.Detector
{
    public class ModelDetectionResult
    {
        public bool IsEmpty
        {
            get
            {
                return ModelNo.IsEmpty && GeizhalsBestMatchKategorie == null;
            }
        }


        public string GoogleBestMatchBrand
        {
            get
            {
                if (GoogleSearchResults != null)
                {
                    StringDetectionResult? sdr = GoogleSearchResults.BestMatchByCount;
                    if (!sdr.HasValue)
                        return null;
                    return sdr.Value.BestStringMatchByCount.String;
                }
                return null;
            }
        }

        public string GeizhalsBestMatchModelNo
        {
            get
            {
                if (GeizhalsSearchResults != null)
                    foreach (GeizhalsSearchResult geizhals_search_result in GeizhalsSearchResults)
                    {
                        string brand = geizhals_search_result.Brand;
                        string model_no = geizhals_search_result.Model;
                        return model_no;
                        //if (geizhals_search_result.Produkte != null)
                        //    if (geizhals_search_result.Produkte.Count > 0)
                        //    {
                        //        string modelno = geizhals_search_result.Produkte[0].Split(" ".ToCharArray(), 2)[1];

                        //        SimilarStringComparer ssc = new SimilarStringComparer();
                        //        float accuracy = ssc.IsEqual(modelno, geizhals_search_result.SearchString);
                        //        if (accuracy > 0.8f)
                        //            return modelno;
                        //    }

                    }
                return null;
            }
        }

        public string GeizhalsBestMatchBrand
        {
            get
            {
                if (GeizhalsSearchResults != null)
                    foreach (GeizhalsSearchResult geizhals_search_result in GeizhalsSearchResults)
                        if (geizhals_search_result.Produkte != null)
                            if (geizhals_search_result.Produkte.Count > 0)
                            {
                                string produkt = geizhals_search_result.Produkte[0];
                                return BrandDetector.DetectBrand(produkt).BestStringMatchByCount.String;
                            }
                return null;
            }
        }

        public string Brand
        {
            get
            {
                if (!string.IsNullOrEmpty(GeizhalsBestMatchBrand))
                    return GeizhalsBestMatchBrand;
                return GoogleBestMatchBrand;
            }
        }


        public WeightedString ModelNo
        {
            get
            {
                if (Candidates == null || Candidates.Count == 0)
                    return new WeightedString();


                return Candidates.MaxBy(o => o.Weight);
            }
        }

        public string GeizhalsBestMatchKategorie
        {
            get
            {
                if (GeizhalsSearchResults != null)
                    foreach (GeizhalsSearchResult geizhals_search_result in GeizhalsSearchResults)
                        if (geizhals_search_result.Kategorien != null)
                            if (geizhals_search_result.Kategorien.Count > 0)
                            {
                                string[] split_kat = geizhals_search_result.Kategorien[0].Split(">".ToCharArray());
                                return split_kat.Last().Trim();
                            }
                return null;
            }
        }

        public override string ToString()
        {
            return ModelNo + " " + GeizhalsBestMatchKategorie;
        }

        public List<WeightedString> Candidates;
        public StringDetectionResults GoogleSearchResults;
        public GeizhalsSearchResults GeizhalsSearchResults;

        public string RawCodeNo = "";
    }

    public class ModelNoDetectionResult
    {
        public bool IsEmpty
        {
            get
            {
                return ModelNo == null && Kategorie == null;
            }
        }

        public string ModelNo
        {
            get
            {
                if (GeizhalsSearchResults != null)
                    foreach (GeizhalsSearchResult geizhals_search_result in GeizhalsSearchResults)
                        if (geizhals_search_result.Produkte != null)
                            if (geizhals_search_result.Produkte.Count > 0)
                            {
                                string produkt = geizhals_search_result.Produkte[0];
                                return produkt;
                            }
                return null;
            }
        }

        public string Kategorie
        {
            get
            {
                if (GeizhalsSearchResults != null)
                    foreach (GeizhalsSearchResult geizhals_search_result in GeizhalsSearchResults)
                        if (geizhals_search_result.Kategorien != null)
                            if (geizhals_search_result.Kategorien.Count > 0)
                            {
                                string[] split_kat = geizhals_search_result.Kategorien[0].Split(">".ToCharArray());
                                return split_kat.Last().Trim();
                            }
                return null;
            }
        }

        public override string ToString()
        {
            return ModelNo + " " + Kategorie;
        }

        public StringDetectionResult GoogleSearchResult;
        public List<GeizhalsSearchResult> GeizhalsSearchResults;

        public string RawModelNo = "";
    }

    public class DeviceModelNoDetector : DeviceCodeDetector
    {
        public static ModelDetectionResult Detect(string text)
        {
            if (string.IsNullOrEmpty(text))
                throw new Exception("cannot detect, no text given");

            ModelDetectionResult mdr = new ModelDetectionResult
            {
                GeizhalsSearchResults = new GeizhalsSearchResults(),
                GoogleSearchResults = new StringDetectionResults()
            };

            List<WeightedString> look_for = new List<WeightedString>(5);
            look_for.Add(new WeightedString("Model No", 20));
            look_for.Add(new WeightedString("ModelNo", 20));
            look_for.Add(new WeightedString("Mod No", 20));
            look_for.Add(new WeightedString("ModNo", 20));
            look_for.Add(new WeightedString("Model"));


            List<WeightedString> candidates_model_no = GetDeviceCodeCandidates(text, look_for, DeviceCodeDetector.GetBlackList(new List <string> { "Code", "No" }));

            for (int i = 0; i < candidates_model_no.Count; i++)
            {
                WeightedString candidate_model_no = candidates_model_no[i];
                StringDetectionResult? sdr_google_search = null;
                //über üprüfe ob dieser string vor dynamisch eingefügt würde druch eine google überprüfung wenn dies der fall ist braucht er hier nicht verarbeitet zu werden
                if (candidate_model_no.Attribute.IndexOf("ShortenedString") < 0)
                {
                    //check on google for suggest info
                    List<string> suggest_model_numbers = GoogleSearch.GetSearchSuggestions(candidate_model_no.String);
                    if (suggest_model_numbers != null)
                        foreach (string suggest_model_no in suggest_model_numbers)
                        {
                            WeightedString ws = new WeightedString(suggest_model_no);
                            ws.Attribute = "InGoogleSuggest";
                            candidates_model_no.Add(ws);
                        }

                    //check if model no candidates can be found on google
                    var google_search_result = GoogleSearch.GetSearchResultAsText(candidates_model_no[i].String, 3, false);
                    
                    //todo analyse json response of google search
                    if (string.IsNullOrEmpty(google_search_result))
                    {
                        #region suche nocheinmal mit dem selben string aber mit einem buchstaben weniger
                        if (candidates_model_no[i].String.Length > 12 && candidates_model_no[i].String.IndexOf(" ")<0)
                        {
                            string candidate_model_shortened = null;
                            string shortened_candidate_modelno = candidates_model_no[i].String;
                            //while (shortened_candidate_modelno.Length > 6)
                            //{
                                shortened_candidate_modelno = shortened_candidate_modelno.Substring(0, shortened_candidate_modelno.Length - 4);
                                google_search_result = GoogleSearch.GetSearchResultAsText(shortened_candidate_modelno, 3, false);
                                sdr_google_search = StringDetector.DetectByStringLower(google_search_result, new WeightedString(shortened_candidate_modelno));
                                if (sdr_google_search.HasValue)
                                    if (!sdr_google_search.Value.IsEmpty)
                                    {
                                        WeightedString ws = new WeightedString(shortened_candidate_modelno);
                                        ws.ReferenceString = candidates_model_no[i];
                                        ws.Attribute = "InGoogleSearch;ShortenedString";
                                        // damit er nicht wieder dieses string im for loop verarbeitet gib ich ihn an dieser stelle rein
                                        candidates_model_no.Add(ws);
                                        //shortened_candidate_modelno = ""; // dies stoppt den while loop, wir brauchen nur einen kandidaten
                                        //break;
                                    }
                            //}
                        }
                        #endregion

                        //if one cannot find the info on the web, this is no model no, ich gehe davon aus das man die model nr irgendwie im netz findet
                        //lösch die info daher aus der liste
                        candidates_model_no.RemoveAt(i);
                        i--;
                        continue;
                    }

                    //add a weight to the current string if the string was found in google
                    sdr_google_search = StringDetector.DetectByStringLower(google_search_result, candidate_model_no);
                    if (sdr_google_search.HasValue)
                        if (!sdr_google_search.Value.IsEmpty)
                            candidate_model_no.AddWeight(sdr_google_search.Value.BestStringMatchByCount.Weight, "InGoogleSearch");

                    //and by the way check for the brand of the model if there in the google string
                    StringDetectionResult? sdr_brand = BrandDetector.DetectBrand(google_search_result);
                    if (sdr_brand.HasValue)
                        if (!sdr_brand.Value.IsEmpty)
                            candidate_model_no.AddWeight(sdr_brand.Value.BestStringMatchByCount.Weight, "BrandInGoogleSearch");

                    //save values in history
                    if (sdr_google_search.HasValue && !sdr_google_search.Value.IsEmpty)
                        mdr.GoogleSearchResults.Add(sdr_google_search.Value);
                    if (sdr_brand.HasValue && !sdr_brand.Value.IsEmpty)
                        mdr.GoogleSearchResults.Add(sdr_brand.Value);
                }


                GeizhalsSelenium sel = GeizhalsSelenium.Instance();
                GeizhalsSearchResult gsr = sel.Query(candidate_model_no.String);
                if (gsr != null && !gsr.IsEmpty)
                {
                    mdr.GeizhalsSearchResults.Add(gsr);

                    if (gsr.Model == candidate_model_no.String)
                        candidate_model_no.AddWeight(1000, "InGeizhals");

                    if (sdr_google_search != null && !sdr_google_search.Value.IsEmpty && sdr_google_search.Value.BestStringMatchByCount.String == candidate_model_no.String && candidate_model_no.String == gsr.Model)
                        candidate_model_no.AddWeight(10000, "InGoogleAndInGeizhals");
                }
            }

            mdr.Candidates = candidates_model_no;
            return mdr;
        }
    }
}
