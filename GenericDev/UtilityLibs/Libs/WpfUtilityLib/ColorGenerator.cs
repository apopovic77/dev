﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;

namespace Logicx.WpfUtility
{


    public class ColorGenerator
    {

        protected struct ColorRGB
        {
            public byte R;
            public byte G;
            public byte B;

            public ColorRGB(Color value)
            {
                this.R = value.R;
                this.G = value.G;
                this.B = value.B;
            }
            public static implicit operator Color(ColorRGB rgb)
            {
                Color c = Color.FromRgb(rgb.R, rgb.G, rgb.B);
                return c;
            }
            public static explicit operator ColorRGB(Color c)
            {
                return new ColorRGB(c);
            }

            public string GetHexCode()
            {
                return ColorToHex(Color.FromRgb(R, G, B));
            }
        }


        public static string[] GetMultipleColorsFromConstSatLum(int count_colors_requested, double sat, double lum)
        {
            string[] colors = new string[count_colors_requested];
            double step_size_hue = 1 / (double)count_colors_requested;
            for (int i = 0; i < count_colors_requested; i++)
            {
                ColorRGB color = HSL2RGB(step_size_hue * i, sat, lum);
                colors[i] = color.GetHexCode();
            }
            return colors;
        }


        #region HSL 2 RGB Conversion
        /// <summary>
        /// converts hue, sat, lum to RGB
        /// </summary>
        /// <param name="h">value between 0-1</param>
        /// <param name="sl">value between 0-1</param>
        /// <param name="l">value between 0-1</param>
        /// <returns>a Color (RGB struct) in range of 0-255</returns>
        protected static ColorRGB HSL2RGB(double h, double sl, double l)
        {
           /* double v;
            double r, g, b;

            r = l; // default to gray
            g = l;
            b = l;

            v = (l <= 0.5) ? (l * (1.0 + sl)) : (l + sl - l * sl);

            if (v > 0)
            {
                double m;
                double sv;
                int sextant;

                double fract, vsf, mid1, mid2;

                m = l + l - v;
                sv = (v - m) / v;
                h *= 6.0;
                sextant = (int)h;
                fract = h - sextant;
                vsf = v * sv * fract;
                mid1 = m + vsf;
                mid2 = v - vsf;
                switch (sextant)
                {
                    case 0:
                        r = v;
                        g = mid1;
                        b = m;
                        break;
                    case 1:
                        r = mid2;
                        g = v;
                        b = m;
                        break;
                    case 2:
                        r = m;
                        g = v;
                        b = mid1;
                        break;
                    case 3:
                        r = m;
                        g = mid2;
                        b = v;
                        break;
                    case 4:
                        r = mid1;
                        g = m;
                        b = v;
                        break;
                    case 5:
                        r = v;
                        g = m;
                        b = mid2;
                        break;
                }
            }

            ColorRGB rgb;
            rgb.R = Convert.ToByte(r * 255.0f);
            rgb.G = Convert.ToByte(g * 255.0f);
            rgb.B = Convert.ToByte(b * 255.0f);

            return rgb;*/

            double r = 0, g = 0, b = 0;

            double temp1, temp2;



            if (l == 0)
            {

                r = g = b = 0;

            }

            else
            {

                if (sl == 0)
                {

                    r = g = b = l;

                }

                else
                {

                    temp2 = ((l <= 0.5) ? l * (1.0 + sl) : l + sl - (l * sl));

                    temp1 = 2.0 * l - temp2;



                    double[] t3 = new double[] { h + 1.0 / 3.0, h, h - 1.0 / 3.0 };

                    double[] clr = new double[] { 0, 0, 0 };

                    for (int i = 0; i < 3; i++)
                    {

                        if (t3[i] < 0)

                            t3[i] += 1.0;

                        if (t3[i] > 1)

                            t3[i] -= 1.0;



                        if (6.0 * t3[i] < 1.0)

                            clr[i] = temp1 + (temp2 - temp1) * t3[i] * 6.0;

                        else if (2.0 * t3[i] < 1.0)

                            clr[i] = temp2;

                        else if (3.0 * t3[i] < 2.0)

                            clr[i] = (temp1 + (temp2 - temp1) * ((2.0 / 3.0) - t3[i]) * 6.0);

                        else

                            clr[i] = temp1;

                    }

                    r = clr[0];

                    g = clr[1];

                    b = clr[2];

                }

            }
            ColorRGB rgb;
            rgb.R = Convert.ToByte(r * 255.0f);
            rgb.G = Convert.ToByte(g * 255.0f);
            rgb.B = Convert.ToByte(b * 255.0f);

            return rgb;
            //return Color.FromRgb((byte)(255 * r), (byte)(255 * g), (byte)(255 * b));
        }


        // Given a Color (RGB Struct) in range of 0-255
        // Return H,S,L in range of 0-1
        protected static void RGB2HSL(ColorRGB rgb, out double h, out double s, out double l)
        {
          /*  double r = rgb.R / 255.0;
            double g = rgb.G / 255.0;
            double b = rgb.B / 255.0;
            double v;

            double m;
            double vm;
            double r2, g2, b2;

            h = 0; // default to black
            s = 0;
            l = 0;

            v = Math.Max(r, g);
            v = Math.Max(v, b);
            m = Math.Min(r, g);
            m = Math.Min(m, b);

            l = (m + v) / 2.0;

            if (l <= 0.0)
            {
                return;
            }

            vm = v - m;
            s = vm;

            if (s > 0.0)
            {
                s /= (l <= 0.5) ? (v + m) : (2.0 - v - m);
            }
            else
            {
                return;
            }

            r2 = (v - r) / vm;
            g2 = (v - g) / vm;
            b2 = (v - b) / vm;

            if (r == v)
            {
                h = (g == m ? 5.0 + b2 : 1.0 - g2);
            }
            else if (g == v)
            {
                h = (b == m ? 1.0 + r2 : 3.0 - b2);
            }
            else
            {
                h = (r == m ? 3.0 + g2 : 5.0 - r2);
            }

            h /= 6.0;*/



            System.Drawing.Color c = new System.Drawing.Color();
            c=System.Drawing.Color.FromArgb(255,rgb.R,rgb.G,rgb.B);

            h = c.GetHue() / 360.0; // we store hue as 0-1 as opposed to 0-360

            l = c.GetBrightness();

            s = c.GetSaturation();

 
        }
        #endregion

        #region HexToColor Conversion
        protected static String ReverseString(String inStr)
        //  Helper Method that reverses a String.
        {
            String outStr;
            int counter;
            outStr = "";
            for (counter = inStr.Length - 1; counter >= 0; counter--)
            {
                outStr = outStr + inStr[counter];
            }
            return outStr;
        }

        public static int HexToInt(String hexstr)
        //  This method converts a hexvalues string as 80FF into a integer.
        //	Note that you may not put a '#' at the beginning of string! There
        //  is not much error checking in this method. If the string does not
        //  represent a valid hexadecimal value it returns 0.
        {
            int counter, hexint;
            char[] hexarr;
            hexint = 0;
            hexstr = hexstr.ToUpper();
            hexarr = hexstr.ToCharArray();
            for (counter = hexarr.Length - 1; counter >= 0; counter--)
            {
                if ((hexarr[counter] >= '0') && (hexarr[counter] <= '9'))
                {
                    hexint += (hexarr[counter] - 48) * ((int)(Math.Pow(16, hexarr.Length - 1 - counter)));
                }
                else
                {
                    if ((hexarr[counter] >= 'A') && (hexarr[counter] <= 'F'))
                    {
                        hexint += (hexarr[counter] - 55) * ((int)(Math.Pow(16, hexarr.Length - 1 - counter)));
                    }
                    else
                    {
                        hexint = 0;
                        break;
                    }
                }
            }
            return hexint;
        }

        protected static String IntToHex(int hexint)
        //  This method converts a integer into a hexadecimal string representing the
        //  int value. The returned string will look like this: 55FF. Note that there is
        //  no leading '#' in the returned string! 
        {
            int counter, reminder;
            String hexstr;

            counter = 1;
            hexstr = "";
            while (hexint + 15 > Math.Pow(16, counter - 1))
            {
                reminder = (int)(hexint % Math.Pow(16, counter));
                reminder = (int)(reminder / Math.Pow(16, counter - 1));
                if (reminder <= 9)
                {
                    hexstr = hexstr + (char)(reminder + 48);
                }
                else
                {
                    hexstr = hexstr + (char)(reminder + 55);
                }
                hexint -= reminder;
                counter++;
            }
            return ReverseString(hexstr);
        }

        protected static String IntToHex(int hexint, int length)
        //  This version of the IntToHex method returns a hexadecimal string representing the
        //  int value in the given minimum length. If the hexadecimal string is shorter then the
        //  length parameter the missing characters will be filled up with leading zeroes.
        //  Note that the returend string though is not truncated if the value exeeds the length!
        {
            String hexstr, ret;
            int counter;
            hexstr = IntToHex(hexint);
            ret = "";
            if (hexstr.Length < length)
            {
                for (counter = 0; counter < (length - hexstr.Length); counter++)
                {
                    ret = ret + "0";
                }
            }
            return ret + hexstr;
        }

        public static Color HexToColor(String hexString)
        //  Translates a html hexadecimal definition of a color into a .NET Framework Color.
        //  The input string must start with a '#' character and be followed by 6 hexadecimal
        //  digits. The digits A-F are not case sensitive. If the conversion was not successfull
        //  the color white will be returned.
        {
            Color actColor;
            int a, r, g, b;
            a = 255;
            r = 0;
            g = 0;
            b = 0;

            if ((hexString.StartsWith("#")) && (hexString.Length == 7))
            {
                r = HexToInt(hexString.Substring(1, 2));
                g = HexToInt(hexString.Substring(3, 2));
                b = HexToInt(hexString.Substring(5, 2));
                actColor = Color.FromRgb(Convert.ToByte(r), Convert.ToByte(g), Convert.ToByte(b));
            }
            else if ((hexString.StartsWith("#")) && (hexString.Length == 9))
            {
                a = HexToInt(hexString.Substring(1, 2));
                r = HexToInt(hexString.Substring(3, 2));
                g = HexToInt(hexString.Substring(5, 2));
                b = HexToInt(hexString.Substring(7, 2));
                actColor = Color.FromArgb(Convert.ToByte(a), Convert.ToByte(r), Convert.ToByte(g), Convert.ToByte(b));
            } 
            else
            {
                actColor = Color.FromRgb(0, 0, 0);
            }
            return actColor;
        }

        public static String ColorToHex(Color actColor)
        //  Translates a .NET Framework Color into a string containing the html hexadecimal 
        //  representation of a color. The string has a leading '#' character that is followed 
        //  by 6 hexadecimal digits. 
        {
            return "#" + IntToHex(actColor.A, 2) +  IntToHex(actColor.R, 2) + IntToHex(actColor.G, 2) + IntToHex(actColor.B, 2);
        }
        #endregion

        public static Color ColorFromHSL(double h, double s, double l)
        {
            ColorRGB c = HSL2RGB(h,s,l);
            return c;
        }

        public static void ColorToHSL(Color c, out double h, out double s, out double l)
        {
            ColorRGB crgb = new ColorRGB(c);
            RGB2HSL(crgb,out h,out s,out l);
        }

        #region ColorToColorWithAlpha
        public static Color ColorToColorWithAlpha(double alpha, Color c)
        {
            //System.Drawing.Color, Alpha in ganzzahligen %-Schritten von 0-100 z.B. (50, Color.Green)
            //Rückgabe vom Typ System.Windows.Media.Color

            double r_in = Convert.ToDouble(c.R);
            double g_in = Convert.ToDouble(c.G);
            double b_in = Convert.ToDouble(c.B);

            return ColorToColorWithAlphaCalc(alpha, r_in, g_in, b_in);
        }


        public static Color ColorToColorWithAlpha(string coloralphahex)
        {
            //Farbe als Hex alpha rot grün blau z.B. (FF008000)
            //Rückgabe vom Typ System.Windows.Media.Color

            double alpha = Convert.ToInt32(coloralphahex[0].ToString() + coloralphahex[1].ToString(), 16);
            double r_in = Convert.ToInt32(coloralphahex[2].ToString() + coloralphahex[3].ToString(), 16);
            double g_in = Convert.ToInt32(coloralphahex[4].ToString() + coloralphahex[5].ToString(), 16);
            double b_in = Convert.ToInt32(coloralphahex[6].ToString() + coloralphahex[7].ToString(), 16);

            alpha = alpha / 2.55;

            return ColorToColorWithAlphaCalc(alpha, r_in, g_in, b_in);
        }

        public static Color ColorToColorWithAlpha(double alpha, double r_in, double g_in, double b_in)
        {
            //Farbe als RGB, Alpha in ganzzahligen %-Schritten von 0-100 z.B. (50, 255, 255, 255)
            //Rückgabe vom Typ System.Windows.Media.Color

            return ColorToColorWithAlphaCalc(alpha, r_in, g_in, b_in);
        }

        public static Color ColorToColorWithAlpha(double alpha, string colorhex)
        {
            //Alpha in ganzzahligen %-Schritten von 0-100, Farbe als Hex z.B. (50, FF00FF)
            //Rückgabe vom Typ System.Windows.Media.Color

            double r_in = Convert.ToInt32(colorhex[0].ToString() + colorhex[1].ToString(), 16);
            double g_in = Convert.ToInt32(colorhex[2].ToString() + colorhex[3].ToString(), 16);
            double b_in = Convert.ToInt32(colorhex[4].ToString() + colorhex[5].ToString(), 16);

            return ColorToColorWithAlphaCalc(alpha, r_in, g_in, b_in);
        }

        private static Color ColorToColorWithAlphaCalc(double alpha, double r_in, double g_in, double b_in)
        {
            double r_calc = (r_in / 100 * alpha) + (2.55 * (100 - alpha));
            double g_calc = (g_in / 100 * alpha) + (2.55 * (100 - alpha));
            double b_calc = (b_in / 100 * alpha) + (2.55 * (100 - alpha));

            string r_out = Convert.ToString(Convert.ToInt32(r_calc), 16);
            if (r_calc <= 16)
                r_out = "0" + Convert.ToString(Convert.ToInt32(r_calc), 16);

            string g_out = Convert.ToString(Convert.ToInt32(g_calc), 16);
            if (g_calc <= 16)
                g_out = "0" + Convert.ToString(Convert.ToInt32(g_calc), 16);

            string b_out = Convert.ToString(Convert.ToInt32(b_calc), 16);
            if (b_calc <= 16)
                b_out = "0" + Convert.ToString(Convert.ToInt32(b_calc), 16);

            return (Color)ColorConverter.ConvertFromString("#" + r_out + g_out + b_out);
        }
        #endregion
    }
}
