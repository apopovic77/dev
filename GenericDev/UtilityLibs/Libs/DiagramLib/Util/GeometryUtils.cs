﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Shapes;
using Logicx.DiagramLib.Scales;

namespace Logicx.DiagramLib.Util
{

    internal enum TextStyleType
    {
        ScaleText,
        InfoText
    }

    internal class GeometryUtils
    {
        /// <summary>
        /// Generates a rectangle of the given size. A binding "LineBrush" is added for the FillProperty.
        /// </summary>
        /// <param name="height"></param>
        /// <param name="width"></param>
        /// <param name="control_type"></param>
        /// <param name="binding_source"></param>
        /// <returns></returns>
        public static Rectangle GenerateRectange(double height, double width, Type control_type, Object binding_source)
        {
            Rectangle rectangle = new Rectangle();
            rectangle.Height = height;
            rectangle.Width = width;

            Binding binding = new Binding("LineBrush");
            binding.Source = binding_source;
            rectangle.SetBinding(Rectangle.FillProperty, binding);

            return rectangle;
        }

        /// <summary>
        /// Generates a text block with the given text and type. A binding is addes to the StyleProperty.
        /// The property path of the binding is either "ScaleTextStyle" or "ScaleInfoTextStyle" dependend
        /// on the type.
        /// </summary>
        /// <param name="text"></param>
        /// <param name="type"></param>
        /// <param name="control_type"></param>
        /// <param name="binding_source"></param>
        /// <returns></returns>
        public static TextBlock GenerateTextBlock(string text, TextStyleType type, Type control_type, Object binding_source)
        {
            TextBlock block = new TextBlock();
            block.Text = text;
            //children.Add(block);

            Binding binding = new Binding();
            string style_propterty_name;
            if (type == TextStyleType.InfoText)
                style_propterty_name = "ScaleInfoTextStyle";
            else if (type == TextStyleType.ScaleText)
                style_propterty_name = "ScaleTextStyle";
            else
                throw new ArgumentException("Invalid Text style for TimeScaleCanvas");

            binding.Path = new PropertyPath(style_propterty_name);
            binding.Source = binding_source;

            block.SetBinding(TextBlock.StyleProperty, binding);

            return block;
        }

    }
}
