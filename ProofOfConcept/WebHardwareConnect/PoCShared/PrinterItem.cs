﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PoCShared
{
    public enum PrinterStatus
    {
        Unknown,
        Online,
        Offline,
        Printing,
        Error
    }

    [Serializable]
    public class PrinterItem : HardwareItem
    {
        public PrinterItem()
        {
            Type = "PRINTER";
        }
        
        public string PrintServer { get; set; }
        public PrinterStatus Status { get; set; }

        public bool Network { get; set; }
        public bool Shared { get; set; }
        public string ShareName { get; set; }
    }
}
