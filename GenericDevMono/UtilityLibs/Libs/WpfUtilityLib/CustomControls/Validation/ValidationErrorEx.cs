﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace Logicx.WpfUtility.CustomControls.Validation
{
    /// <summary>
    /// Validation error extended class
    /// </summary>
    public class ValidationErrorEx : ValidationError
    {
        #region Construction and initialization

        #region constructors for databound errors
        /// <summary>
        /// Databound error constructor
        /// </summary>
        /// <param name="ruleInError">ValidationRule which caused the error</param>
        /// <param name="bindingInError">Binding object which produced the error</param>
        public ValidationErrorEx(ValidationRule ruleInError, object bindingInError) 
            : base(ruleInError, bindingInError)
        {
            _error_from_binding_context = true;
        }
        /// <summary>
        /// Databound error constructor
        /// </summary>
        /// <param name="ruleInError">ValidationRule which caused the error</param>
        /// <param name="bindingInError">Binding object which produced the error</param>
        /// <param name="errorContent">Error content object</param>
        /// <param name="exception">Exception causing the error</param>
        public ValidationErrorEx(ValidationRule ruleInError, object bindingInError, object errorContent, Exception exception)
            : base(ruleInError, bindingInError, errorContent, exception)
        {
            _error_from_binding_context = true;

            CheckTargetInvocationException();
        }
        /// <summary>
        /// Databound error constructor
        /// </summary>
        /// <param name="ruleInError">ValidationRule which caused the error</param>
        /// <param name="bindingInError">Binding object which produced the error</param>
        /// <param name="errorProvider">Associated ErrorProvider control</param>
        public ValidationErrorEx(ValidationRule ruleInError, object bindingInError, ErrorProvider errorProvider)
            : base(ruleInError, bindingInError)
        {
            _error_from_binding_context = true;
            _associated_error_provider = errorProvider;
        }
        /// <summary>
        /// Databound error constructor
        /// </summary>
        /// <param name="ruleInError">ValidationRule which caused the error</param>
        /// <param name="bindingInError">Binding object which produced the error</param>
        /// <param name="errorContent">Error content object</param>
        /// <param name="exception">Exception causing the error</param>
        /// <param name="errorProvider">Associated ErrorProvider control</param>
        public ValidationErrorEx(ValidationRule ruleInError, object bindingInError, object errorContent, Exception exception, ErrorProvider errorProvider) 
            : base(ruleInError, bindingInError, errorContent, exception)
        {
            _error_from_binding_context = true;
            _associated_error_provider = errorProvider;

            CheckTargetInvocationException();
        }
        /// <summary>
        /// Databound error constructor
        /// </summary>
        /// <param name="baseError">Base ValidationError instance</param>
        public ValidationErrorEx(ValidationError baseError) : base(baseError.RuleInError, baseError.BindingInError, baseError.ErrorContent, baseError.Exception)
        {
            _error_from_binding_context = true;

            CheckTargetInvocationException();
        }
        /// <summary>
        /// Databound error constructor
        /// </summary>
        /// <param name="baseError">Base ValidationError instance</param>
        /// <param name="errorProvider">Associated ErrorProvider control</param>
        public ValidationErrorEx(ValidationError baseError, ErrorProvider errorProvider)
            : base(baseError.RuleInError, baseError.BindingInError, baseError.ErrorContent, baseError.Exception)
        {
            _error_from_binding_context = true;
            _associated_error_provider = errorProvider;

            CheckTargetInvocationException();
        }
        #endregion

        #region constructors for non-databound errors
        /// <summary>
        /// Non Databound constructor
        /// </summary>
        /// <param name="controlCausingError">The control causing the error</param>
        public ValidationErrorEx(DependencyObject controlCausingError)
            : base(CustomUIValidationRule.NewRule, controlCausingError)
        {
            _error_from_binding_context = false;
        }
        /// <summary>
        /// Non Databound constructor
        /// </summary>
        /// <param name="controlCausingError">The control causing the error</param>
        /// <param name="errorProvider">Associated ErrorProvider control</param>
        public ValidationErrorEx(DependencyObject controlCausingError, ErrorProvider errorProvider)
            : base(CustomUIValidationRule.NewRule, controlCausingError)
        {
            _error_from_binding_context = false;
            _associated_error_provider = errorProvider;
        }
        /// <summary>
        /// Non Databound constructor
        /// </summary>
        /// <param name="controlCausingError">The control causing the error</param>
        /// <param name="errorContent">Error content object</param>
        /// <param name="exception">Exception causing the error</param>
        /// <param name="errorProvider">Associated ErrorProvider control</param>
        public ValidationErrorEx(DependencyObject controlCausingError, object errorContent, Exception exception, ErrorProvider errorProvider)
            : base(CustomUIValidationRule.NewRule, controlCausingError, errorContent, exception)
        {
            _error_from_binding_context = false;
            _associated_error_provider = errorProvider;

            CheckTargetInvocationException();
        }
        #endregion

        #endregion

        #region Operations
        /// <summary>
        /// Checks if the current validation error was caused by a TargetInvocationException (reflection).
        /// If this is the case and the error content is the error message of the TargetInvocationException,
        /// then try to set the inner exception as the error source.
        /// </summary>
        private void CheckTargetInvocationException()
        {
            if (this.Exception is TargetInvocationException)
            {
                if (this.Exception.InnerException != null)
                {
                    if (this.ErrorContent is string && ((string)this.ErrorContent) == this.Exception.Message)
                        this.ErrorContent = this.Exception.InnerException.Message;

                    this.Exception = this.Exception.InnerException;
                }
            }
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets the element instance which caused the error
        /// </summary>
        /// <remarks>Only valid for non data bound errors</remarks>
        public DependencyObject ControlCausingError
        {
            get
            {
                if (BindingInError is UIElement || BindingInError is ContentElement || BindingInError is UIElement3D)
                    return BindingInError as DependencyObject;
                
                return null;
            }
        }
        /// <summary>
        /// Gets a flag if this is an error object from a data binding operation or not
        /// </summary>
        public bool ErrorFromDatabindingContext
        {
            get { return _error_from_binding_context; }
        }

        /// <summary>
        /// Gets the associated error provider control
        /// </summary>
        public ErrorProvider ErrorProvider
        {
            get { return _associated_error_provider; }
            set { _associated_error_provider = value; }
        }
        #endregion

        #region Attribs
        private bool _error_from_binding_context = true;
        private ErrorProvider _associated_error_provider = null;
        #endregion
    }
}
