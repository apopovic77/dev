﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;

namespace Logicx.WpfUtility
{
    public class BrushGenerator
    {
        public static Brush GenerateDarkendBrush(string hex_color_brush, double brush_gewichtung)
        {
            Color c_pointer = ColorGenerator.HexToColor(hex_color_brush);
            return GenerateDarkendBrush(c_pointer, brush_gewichtung);
        }

        public static Brush GenerateDarkendBrush(Color color, double brush_gewichtung)
        {
            Color dark_color = GenerateDarkenColor(color, brush_gewichtung);
            SolidColorBrush pointer_brush = new SolidColorBrush(dark_color);
            pointer_brush.Freeze();
            return pointer_brush;
        }

        public static Color GenerateDarkenColor(Color color, double brush_gewichtung)
        {
            double h;
            double s;
            double l;
            ColorGenerator.ColorToHSL(color, out h, out s, out l);
            s *= brush_gewichtung;// AssetViewConfig.MgmtPointerBrushGewichtung;
            l *= brush_gewichtung;// AssetViewConfig.MgmtPointerBrushGewichtung;
            color = ColorGenerator.ColorFromHSL(h, s, l);

            return color;
        }

        public static Brush GenerateBrush(string hex_color_brush)
        {
            Color color = ColorGenerator.HexToColor(hex_color_brush);
            SolidColorBrush brush = new SolidColorBrush(color);
            brush.Freeze();
            return brush;
        }
    }
}
