﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Data;
using Logicx.DiagramLib.Scales;

namespace Logicx.DiagramLib.Resources.Converter
{
    public class ValueToPositionConverter : IMultiValueConverter
    {
        // parameter at index 1 is just used as trigger
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (values.Length == 4)
            {
                object x_value = values[0];

                if (values[1] == DependencyProperty.UnsetValue || values[2] == DependencyProperty.UnsetValue || !(values[3] is bool))
                    return 0;

                double scalue_unit_size = (double)values[1];
                ScaleControlBase scale_control = values[2] as ScaleControlBase;
                bool isBackgroundObject = (bool)values[3];

                if (x_value != null && scale_control != null)
                {
                    if (scale_control is DiscreteScaleControl<Object>)
                    {
                        double unit_size = scale_control.GetUnitSize();
                        double x = scale_control.GenericTransformAxisValueToPixel(x_value, isBackgroundObject);
                        return x - DiscreteScale.UNIT_SIZE_FACTOR * unit_size / 2;
                    }

                    double dRet = scale_control.GenericTransformAxisValueToPixel(x_value, isBackgroundObject);
                    //if (double.IsInfinity(dRet) || double.IsNaN(dRet))
                    //    Debug.WriteLine("INFINITY OR NAN IN VALUETOPOSITION");
                    return dRet;
                }
            }

            return 0;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }
}
