﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using LinqLib.Linq2Sql.DbConnection;
using LinqLib.Linq2Sql.IaDataExceptions;
using LinqLib.Linq2Sql.Notification;
using LinqLib.Linq2Sql.Progress;
using Logicx.Utilities;

namespace LinqLib.Linq2Sql
{
    public class DataContextEx : DataContext
    {
        #region nested classes
        protected class AsyncStateMode
        {
            public ConflictMode FailureMode;
            public double PercentStart;
            public int MaxSteps;
        }

        #endregion

        #region Construction and initialization
        public DataContextEx(string fileOrServerOrConnection)
            : base(fileOrServerOrConnection)
        {
        }

        public DataContextEx(IDbConnection connection)
            : base(connection)
        { 
        }

        public DataContextEx(IDbConnection connection, MappingSource mapping)
            : base(connection, mapping)
        {
        }

        public DataContextEx(string fileOrServerOrConnection, MappingSource mapping)
            : base(fileOrServerOrConnection, mapping)
        {
        }
        #endregion

        #region Operations

        #region internal cache clear
        public void ClearInternalCache()
        {
            this.GetType().InvokeMember(
                "ClearCache",
                BindingFlags.InvokeMethod | BindingFlags.Instance | BindingFlags.NonPublic,
                null, this, null);
        }
        #endregion

        #region Linq Reflection Helpers
        /// <summary>
        /// Gets all Properties of an object which are representing the primary key of a type
        /// </summary>
        /// <param name="tableType"></param>
        /// <returns></returns>
        private List<PropertyInfo> GetPrimaryKeys(Type tableType)
        {
            List<PropertyInfo> ret = new List<PropertyInfo>();

            foreach (PropertyInfo property in tableType.GetProperties())
            {
                // get all column attributes
                foreach (object attribute in property.GetCustomAttributes(typeof(ColumnAttribute), false))
                {
                    ColumnAttribute col = attribute as ColumnAttribute;

                    if (col != null && col.IsPrimaryKey)
                        ret.Add(property);
                }
            }

            return ret;
        }
        #endregion

        #region Submitting changes
        public new void SubmitChanges()
        {
            SubmitChanges(ConflictMode.FailOnFirstConflict);
        }

        public override void SubmitChanges(ConflictMode failureMode)
        {
            SubmitChanges(failureMode, 0, 0);
        }

        public virtual void SubmitChanges(ConflictMode failureMode, double start_percent, int max_steps)
        {
            _last_percent_done = 0;

            if (this.Connection is PooledIaDbConnection)
                throw new IaDbTransactionNotSupportedException("Submitting changes require a not supported transactional connection. Pooled IaDbConnection does not support transactions.");

            // Make sure the submitchanges method will not be called simultaniously from multiple threads
            // by generating a lock
            Monitor.Enter(LockParallelSystem);

            // generate submission statistics instance
            SubmissionStatistics statistics = new SubmissionStatistics();

            bool blocalTransaction = false;

            try
            {
                _started = DateTime.Now;
                statistics.Started = _started;
                // create log parser for progress reporting
                // note: this may cause an exception because a changeset will be generated in the constructor
                if (start_percent == 0 && max_steps == 0)
                    _log_parser = new LinqOutputProgressParser(this);
                else
                    _log_parser = new LinqOutputProgressParser(start_percent, max_steps, this);
                // subscribe to the parser progress event
                _log_parser.UpdatePercentageChanged += new EventHandler<LinqProgressEventArgs>(_log_parser_SubmitPercentageChanged);
                // redirect log output and start parsing
                _log_parser.StartParsing();

                if (Transaction == null)
                {
                    if (Connection.State == System.Data.ConnectionState.Closed ||
                        Connection.State == System.Data.ConnectionState.Broken)
                        Connection.Open();

                    Transaction = Connection.BeginTransaction();
                    blocalTransaction = true;
                }

                //#region Table-Constraint registration checks
                if (_tables_with_constraints.Count > 0)
                {
                    // if the data context contains tables with contraints
                    // then check the ChangeSet if deletes for these tables are present
                    // if yes, then execute the deletes before submitting the changes

                    ChangeSet cs = GetChangeSet();
                    if (cs != null)
                    {
                        foreach (Type constraintType in _tables_with_constraints)
                        {
                            List<object> deleted_objects = new List<object>();

                            foreach (object oDel in cs.Deletes)
                            {
                                if (oDel != null && oDel.GetType() == constraintType)
                                {
                                    deleted_objects.Add(oDel);
                                }
                            }

                            foreach (object delObject in deleted_objects)
                            {
                                string deleteStatement = "DELETE FROM ";
                                bool bFilter = false;

                                MetaTable meta = this.Mapping.GetTable(constraintType);
                                deleteStatement += meta.TableName + " WHERE ";

                                List<object> param = new List<object>();

                                List<PropertyInfo> primaryKeys = GetPrimaryKeys(constraintType);
                                int paramCnt = 0;

                                foreach (PropertyInfo piPK in primaryKeys)
                                {
                                    string sfilter = "";

                                    foreach (object attribute in piPK.GetCustomAttributes(typeof(ColumnAttribute), false))
                                    {
                                        ColumnAttribute ca = attribute as ColumnAttribute;
                                        if (ca != null)
                                        {
                                            sfilter = "[" + (string.IsNullOrEmpty(ca.Name) ? piPK.Name : ca.Name) + "]";
                                            sfilter += "=@p" + paramCnt.ToString();
                                            param.Add(piPK.GetValue(delObject, null));
                                            paramCnt++;
                                            break;
                                        }
                                    }

                                    if (!string.IsNullOrEmpty(sfilter))
                                    {
                                        if (bFilter)
                                            deleteStatement += " AND ";

                                        deleteStatement += sfilter;
                                        bFilter = true;
                                    }
                                }

                                if (bFilter)
                                    ExecuteCommand(deleteStatement, param.ToArray());
                            }
                        }
                    }
                }
                //#endregion

                base.SubmitChanges(failureMode);

                if (blocalTransaction && Transaction != null)
                {
                    Transaction.Commit();
                    Transaction = null;
                }

                statistics.Succeed = true;
                statistics.LastError = null;

            }
            catch (Exception ex)
            {
                statistics.Succeed = false;
                statistics.LastError = ex;

                if (blocalTransaction && Transaction != null)
                {
                    Transaction.Rollback();
                    Transaction = null;
                }

                if (_async_submitchanges_worker == null)
                {
                    // rethrow exception in sync szenarios
                    throw;
                }
            }
            finally
            {
                statistics.Ended = DateTime.Now;
                statistics.TimeNeeded = statistics.Ended - statistics.Started;

                if (_log_parser != null)
                {
                    statistics.NumberOfDeletes = _log_parser.DeletesDone;
                    statistics.NumberOfInserts = _log_parser.InsertsDone;
                    statistics.NumberOfUpdates = _log_parser.UpdatesDone;
                    statistics.SubmissionHistory = _log_parser.SubmissionHistory;

                    // redirect log output to the old value
                    _log_parser.StopParsing();
                    // unsubscribe from event
                    _log_parser.UpdatePercentageChanged -= _log_parser_SubmitPercentageChanged;
                    // free resources
                    _log_parser.Dispose();
                    _log_parser = null;
                }

                // Release lock
                Monitor.Exit(LockParallelSystem);

                _last_submission_statistics = statistics;

                OnSubmissionCompleted(new SubmissionCompletedEventArgs(statistics));
            }
        }

        public bool SubmitChangesAsync()
        {
            return SubmitChangesAsync(ConflictMode.FailOnFirstConflict);
        }

        public bool SubmitChangesAsync(ConflictMode failureMode)
        {
            return SubmitChangesAsync(failureMode, 0, 0);
        }

        public virtual bool SubmitChangesAsync(ConflictMode failureMode, double start_percent, int max_steps)
        {
            _last_percent_done = 0;

            if (this.Connection is PooledIaDbConnection)
                throw new IaDbTransactionNotSupportedException("Submitting changes require a not supported transactional connection. Pooled IaDbConnection does not support transactions.");

            if (_is_background_submission_running)
                return false;

            _is_background_submission_running = true;

            // create a new background worker thread
            _async_submitchanges_worker = new QueuedBackgroundWorker();

            // subscribe to the worker events
            _async_submitchanges_worker.DoWork += new QueuedBackgroundWorker.DoWorkEventHandler(_async_submitchanges_worker_DoWork);
            _async_submitchanges_worker.RunWorkerCompleted += new QueuedBackgroundWorker.RunWorkerCompletedEventHandler(_async_submitchanges_worker_RunWorkerCompleted);

            _async_submitchanges_worker.RunWorkerAsync(new AsyncStateMode { FailureMode = failureMode, PercentStart = start_percent, MaxSteps = max_steps });

            return true;
        }
        #endregion

        #region Disposing
        protected override void Dispose(bool disposing)
        {
            StopChangeNotificationListeners();
            ClearAllNotificationRegistrations();
            base.Dispose(disposing);
        }
        #endregion

        #region Change notification
        /// <summary>
        /// Initializes the change notification and sets the default notification table name
        /// </summary>
        /// <param name="default_notification_table_name"></param>
        public void InitializeChangeNotification(string default_notification_table_name)
        {
            InitializeChangeNotification(default_notification_table_name, Connection.ConnectionString);
        }

        /// <summary>
        /// Initializes the change notification and sets the default notification table name
        /// </summary>
        /// <param name="default_notification_table_name"></param>
        public void InitializeChangeNotification(string default_notification_table_name, string sqldependency_connection_string)
        {
            _default_notification_table_name = default_notification_table_name;
            _sql_dependency_connection_string = sqldependency_connection_string;
            StartChangeNotificationListeners();
        }

        public void StartChangeNotificationListeners()
        {
            SqlDependency.Stop(_sql_dependency_connection_string); //, "AenderungsNotifizierungenQueue");
            SqlDependency.Start(_sql_dependency_connection_string); //, "AenderungsNotifizierungenQueue");
        }

        public void StopChangeNotificationListeners()
        {
            SqlDependency.Stop(_sql_dependency_connection_string); //, "AenderungsNotifizierungenQueue");
        }

        public void RegisterNotificationListener(string notification_key)
        {
            RegisterNotificationListener(notification_key, _default_notification_table_name);
        }

        public void RegisterNotificationListener(string notification_key, string table_name)
        {
            if (string.IsNullOrEmpty(table_name))
                throw new ArgumentException("table_name must be set");
            if (string.IsNullOrEmpty(notification_key))
                throw new ArgumentException("notification_key must not be null");

            SqlChangeNotificationMeta existing_meta = _change_notification_subscriptions.SingleOrDefault(n => n.NotificationKey.ToLower() == notification_key.ToLower());

            if(existing_meta == null)
            {
                SqlChangeNotificationMeta new_meta = new SqlChangeNotificationMeta(_sql_dependency_connection_string, table_name, notification_key);
                new_meta.OnChange += new EventHandler(change_notification_meta_OnChange);
                new_meta.StartListening();
                _change_notification_subscriptions.Add(new_meta);
            }
        }

        /// <summary>
        /// Clear all notification listeners
        /// </summary>
        public void ClearAllNotificationRegistrations()
        {
            foreach(SqlChangeNotificationMeta meta in _change_notification_subscriptions)
            {
                meta.OnChange -= change_notification_meta_OnChange;
                meta.StopListening();
            }

            _change_notification_subscriptions.Clear();
        }
        #endregion

        #endregion

        #region Event raising
        /// <summary>
        /// Raises the <see cref="SubmissionProgress"/> event
        /// </summary>
        /// <param name="e">event arguments</param>
        private void OnSubmissionProgress(LinqProgressEventArgs e)
        {
            if (SubmissionProgress != null)
                SubmissionProgress(this, e);
        }
        /// <summary>
        /// Raises the <see cref="SubmissionComplted"/> event
        /// </summary>
        /// <param name="e">event arguments</param>
        private void OnSubmissionCompleted(SubmissionCompletedEventArgs e)
        {
            if (SubmissionCompleted != null)
                SubmissionCompleted(this, e);
        }
        /// <summary>
        /// Raises the <see cref="SqlChangeNotification"/> event
        /// </summary>
        /// <param name="e"></param>
        private void OnSqlChangeNotification(SqlChangeNotificationEventArgs e)
        {
            if (SqlChangeNotification != null)
                SqlChangeNotification(this, e);
        }
        #endregion

        #region Event Handler
        /// <summary>
        /// Worker method of the background worker thread
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event arguments</param>
        void _async_submitchanges_worker_DoWork(object sender, DoWorkEventArgs e)
        {
            SubmitChanges(((AsyncStateMode)e.Argument).FailureMode, ((AsyncStateMode)e.Argument).PercentStart, ((AsyncStateMode)e.Argument).MaxSteps);
        }
        /// <summary>
        /// Worker thread completed event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void _async_submitchanges_worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (_async_submitchanges_worker != null)
                _async_submitchanges_worker = null;

            _is_background_submission_running = false;
        }

        /// <summary>
        /// Event handler from the output parser
        /// </summary>
        /// <param name="sender"><see cref="LinqLogParser"/> instance.</param>
        /// <param name="e">Event parameter</param>
        void _log_parser_SubmitPercentageChanged(object sender, LinqProgressEventArgs e)
        {
            _last_percent_done = e.PercentComplete;
            OnSubmissionProgress(e);
        }

        /// <summary>
        /// Even handler from sql change notification eventing
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void change_notification_meta_OnChange(object sender, EventArgs e)
        {
            SqlChangeNotificationMeta meta = sender as SqlChangeNotificationMeta;
            OnSqlChangeNotification(new SqlChangeNotificationEventArgs(meta));
        }
        #endregion

        #region Properties
        public bool HasPooledConnection
        {
            get { return (Connection is PooledIaDbConnection); }
        }

        public bool IsReadOnly
        {
            get { return !ObjectTrackingEnabled || HasPooledConnection; }
        }

        /// <summary>
        /// Gets a flag which indicates if a background submission is currently running
        /// </summary>
        public bool IsBackgroundSubmissionRunning
        {
            get { return _is_background_submission_running; }
        }
        /// <summary>
        /// Gets a reference to the currently active linq log parser.
        /// </summary>
        /// <remarks>Note, that this property will return null if no submission is currently active!</remarks>
        public LinqOutputProgressParser LinqLogParser
        {
            get
            {
                return _log_parser;
            }
        }

        public double LastPercentDone
        {
            get { return _last_percent_done; }
        }

        public SubmissionStatistics LastSubmissionStatistics
        {
            get { return _last_submission_statistics; }
        }

        public List<Type> TablesWithConstraints
        {
            get { return _tables_with_constraints; }
        }
        #endregion

        #region Events
        /// <summary>
        /// Event reporting the progress of the current submission
        /// </summary>
        public event EventHandler<LinqProgressEventArgs> SubmissionProgress;
        /// <summary>
        /// Event reporting that the submission request completed
        /// </summary>
        public event EventHandler<SubmissionCompletedEventArgs> SubmissionCompleted;
        /// <summary>
        /// Event reporting that an sql change notification has happened
        /// </summary>
        public event EventHandler<SqlChangeNotificationEventArgs> SqlChangeNotification;
        #endregion

        #region Attributes
        private DateTime _started = DateTime.MinValue;
        private bool _is_background_submission_running = false;
        private QueuedBackgroundWorker _async_submitchanges_worker = null;
        private LinqOutputProgressParser _log_parser = null;
        private object LockParallelSystem = new object();
        private double _last_percent_done = 0;
        private SubmissionStatistics _last_submission_statistics = null;
        private List<Type> _tables_with_constraints = new List<Type>();

        private string _default_notification_table_name = "";
        private string _sql_dependency_connection_string = "";
        private List<SqlChangeNotificationMeta> _change_notification_subscriptions = new List<SqlChangeNotificationMeta>();
        #endregion
    }
}
