﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Logicx.WpfUtility.WindowManagement;
using Logicx.WpfUtility.WpfHelpers;

namespace Logicx.WpfUtility.CustomControls.OverlayManager.StatusInfo
{
    /// <summary>
    /// Interaction logic for OverlayStatusPanel.xaml
    /// </summary>
    public partial class OverlayStatusPanel : UserControl
    {
        public OverlayStatusPanel()
        {
            InitializeComponent();
            _active_status_items = new Dictionary<string, OverlayStatusItem>();
        }


        #region Generic Status Item Mgmt
        public T GetStatusItem<T>(string id) where T : OverlayStatusItem
        {
            T status_item;

            if (!_active_status_items.ContainsKey(id) || !(_active_status_items[id] is T))
                throw new Exception("Id unbekannt");

            return (T)_active_status_items[id];
        }

        public void ShowStatusInfo<T>(string id, string title, string text, int autohide_in_msec) where T : OverlayStatusItem
        {
            OverlayStatusItem status_item;
            if (_active_status_items.ContainsKey(id))
            {
                status_item = _active_status_items[id];

                if (typeof(T) != status_item.GetType())
                {
                    //replace existing item
                    status_item = CreateStatusInfo<T>(id, title, text, autohide_in_msec);
                    ReplaceStatusItem(_active_status_items[id], status_item);
                    DoLayoutItems();
                    return;
                }

                if (status_item is OverlayTextStatusItem)
                {
                    ((OverlayTextStatusItem)status_item).Title = title;
                    ((OverlayTextStatusItem)status_item).Text = text;
                }
                else if (status_item is OverlayProgressStatusItem)
                {
                    ((OverlayProgressStatusItem)status_item).ProgressInfo.InformationTitle = title;
                    ((OverlayProgressStatusItem)status_item).ProgressInfo.SubInformationTitle = text;
                }
                else if (status_item is OverlayConfirmStatusItem)
                {
                    ((OverlayConfirmStatusItem)status_item).ConfirmInfo.InformationTitle = title;
                    ((OverlayConfirmStatusItem)status_item).ConfirmInfo.SubInformationTitle = text;
                }
                else
                {
                    throw new Exception("Overlay Status Item Type unknown");
                }

                status_item.TimeOutMsec = autohide_in_msec;
                status_item.ResetTimeout();
                return;
            }

            status_item = CreateStatusInfo<T>(id, title, text, autohide_in_msec);
            AddStatusItem(id, status_item);
            DoLayoutItems();
        }

        private T CreateStatusInfo<T>(string id, string title, string text, int autohide_in_msec) where T : OverlayStatusItem
        {
            OverlayStatusItem status_item;
            if (typeof(T) == typeof(OverlayTextStatusItem))
            {
                OverlayTextStatusItem text_status_item = new OverlayTextStatusItem();
                text_status_item.Title = title;
                text_status_item.Text = text;
                status_item = text_status_item;
            }
            else if (typeof(T) == typeof(OverlayProgressStatusItem))
            {
                OverlayProgressStatusItem progress_status_item = new OverlayProgressStatusItem();
                progress_status_item.ProgressInfo.InformationTitle = title;
                progress_status_item.ProgressInfo.SubInformationTitle = text;
                progress_status_item.ProgressInfo.TimeIndicatorVisibility = Visibility.Hidden;
                status_item = progress_status_item;
            }
            else if (typeof(T) == typeof(OverlayConfirmStatusItem))
            {
                OverlayConfirmStatusItem confirm_status_item = new OverlayConfirmStatusItem();
                confirm_status_item.ConfirmInfo.InformationTitle = title;
                confirm_status_item.ConfirmInfo.SubInformationTitle = text;
                status_item = confirm_status_item;
            }
            else if (typeof(T) == typeof(OverlayTextLinkStatusItem))
            {
                OverlayTextLinkStatusItem link_status_item = new OverlayTextLinkStatusItem();
                status_item = link_status_item;
            }
            else
            {
               
                    throw new Exception("Overlay Status Item Type unknown");
            }
          
            try
            {
                status_item.ConfirmButtonStyle = GetOverlayManagerProperty("StatusConfirmButtonStyle") as Style;
            }
            catch (Exception) { }
            try
            {
                status_item.SmallButtonStyle = GetOverlayManagerProperty("StatusSmallButtonStyle") as Style;
            } catch(Exception) {}

            status_item.Id = id;
            status_item.TimeOutMsec = autohide_in_msec;
            status_item.FadeOutCompleted += new EventHandler(status_item_FadeOutCompleted);
            return (T)status_item;
        }

        public void HideStatusInfo<T>(string id) where T : OverlayStatusItem
        {
            T status_item;

            if (!_active_status_items.ContainsKey(id) || !(_active_status_items[id] is T))
                return;

            _active_status_items[id].StartFadeOut();
        }

        public void HideAllStatusInfo()
        {
            int count = _active_status_items.Count;

            foreach(OverlayStatusItem item in _active_status_items.Values)
            {
                HideStatusInfo<OverlayStatusItem>(item.Id);
            }
        }

        #endregion

        private void DoLayoutItems()
        {
            foreach (OverlayStatusItem status_item in panel.Children)
            {
                status_item.HorizontalAlignment = HorizontalAlignment.Left;
                status_item.VerticalAlignment = VerticalAlignment.Top;
            }
        }

        private void AddStatusItem(string id, OverlayStatusItem status_item)
        {
            _active_status_items.Add(id, status_item);
            status_item.Margin = new Thickness(0, 5, 0, 5);
            panel.Children.Add(status_item);
            status_item.Opacity = 0;
            //der fade in kann erst nach dem loaden passieren das erst dann die
            //die size vorhanden ist.
            status_item.Loaded += new RoutedEventHandler(status_item_Loaded);
        }

        private void ReplaceStatusItem(OverlayStatusItem existing_item, OverlayStatusItem new_item)
        {
            existing_item.Loaded -= status_item_Loaded;
            new_item.Margin = existing_item.Margin;
            int index = panel.Children.IndexOf(existing_item);
            panel.Children.Insert(index, new_item);
            panel.Children.RemoveAt(index+1);
            _active_status_items[existing_item.Id] = new_item;
        }

        void status_item_Loaded(object sender, RoutedEventArgs e)
        {
            OverlayStatusItem status_item = (OverlayStatusItem)sender;
            status_item.Loaded -= status_item_Loaded;
            status_item.Opacity = 1;
            status_item.StartFadeIn();
        }

        #region Dependency Properties
        public Style BorderStyle
        {
            get { return (Style)GetValue(BorderStyleProperty); }
            set { SetValue(BorderStyleProperty, value); }
        }
        public static readonly DependencyProperty BorderStyleProperty = DependencyProperty.Register("BackgroundStyle", typeof(Style), typeof(OverlayStatusItem), new UIPropertyMetadata(null));
        #endregion

        #region EventHandler
        private void status_item_FadeOutCompleted(object sender, EventArgs e)
        {
            OverlayStatusItem status_item = (OverlayStatusItem)sender;
            _active_status_items.Remove(status_item.Id);
            panel.Children.Remove(status_item);
        }
        #endregion

        #region Properties
        private IOverlayManagerBase OverlayManager
        {
            get
            {
                if (_overlay_manager != null)
                    return _overlay_manager;

                Object o = UIHelper.TryFindParentGeneric(this, typeof(OverlayManagerBase<BaseFrame, SubMenuPanel, WindowTaskBar, WindowTitleBar, OverlayManager, SubMenuPanelMenuItem>));
                if (o != null)
                    _overlay_manager = o as IOverlayManagerBase;

                return _overlay_manager;
                //return UIHelper.TryFindParent<OverlayManager>(this);
            }
        }
        private object GetOverlayManagerProperty(string property_name)
        {
            if (OverlayManager == null)
                return null;

            Type t = OverlayManager.GetType();
            PropertyInfo p_info = t.GetProperty(property_name);
            if(p_info != null)
            {
                object v = p_info.GetValue(OverlayManager, null);
                return v;
            }

            return null;
        }
        #endregion

        private IOverlayManagerBase _overlay_manager = null;
        protected Dictionary<string, OverlayStatusItem> _active_status_items;
    }
}