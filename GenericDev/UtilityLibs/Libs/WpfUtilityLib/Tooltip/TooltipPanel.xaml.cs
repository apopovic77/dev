﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using Logicx.Utilities;
using MathLib;

namespace Logicx.WpfUtility.Tooltip
{

    /// <summary>
    /// Interaction logic for TooltipPanel.xaml
    /// </summary>
    public partial class TooltipPanel : UserControl
    {
        protected class TooltipData
        {
            public DateTime _last_hide_time;
            public int _curr_tooltip_wait_before_show_time;
            public float _curr_tooltip_pointer_showangle;
            public float _curr_tooltip_pointer_len;
            public FrameworkElement _tooltip;
            public Vector2f _tooltip_reference_pos;
            public LinearGradientBrush _tooltip_pointer_brush;
            public Path pointer;
            public Border border;
            public bool SyncWithMousePos;
        }

        public TooltipPanel()
        {
            InitializeComponent();

            _fade_in_anim = new DoubleAnimation(0, 1, new Duration(new TimeSpan(0, 0, 0, 0, 200)));
            _fade_in_anim.AccelerationRatio = 0.5;
            _fade_in_anim.DecelerationRatio = 0.5;
            _fade_in_anim.BeginTime = new TimeSpan(0, 0, 0, 0, 230);
        }

        public void HideAllTooltips()
        {
            List<FrameworkElement> tooltip_contents = _tooltips.ToList().Select(a => a.Key).ToList();
            foreach (FrameworkElement tooltip_content in tooltip_contents)
                HideTooltip(tooltip_content);
        }

        public void HideTooltip(FrameworkElement tooltip_content)
        {
            if(tooltip_content == null)
                return;

            if (!_tooltips.ContainsKey(tooltip_content))
                return;

            TooltipData tooltip_data = _tooltips[tooltip_content];

            if (tooltip_data.border.Child == null)
                return;

            tooltip_data.border.Opacity = 0;
            tooltip_data.pointer.Opacity = 0;
            tooltip_data.pointer.BeginAnimation(UIElement.OpacityProperty, null);
            tooltip_data.border.BeginAnimation(UIElement.OpacityProperty, null);

            tooltip_content.SizeChanged -= TooltipPanel_SizeChanged;
            tooltip_data.border.SizeChanged -= TooltipPanel_SizeChanged;
            tooltip_data.border.Child = null;

            tooltip_data.border.InvalidateMeasure();
            tooltip_data.border.InvalidateArrange();
            tooltip_data.pointer.Visibility = Visibility.Hidden;
            tooltip_data.border.Visibility = Visibility.Hidden;
            tooltip_data._last_hide_time = DateTime.Now;
            tooltip_data._tooltip = null;

            if (MainWindow != null)
                MainWindow.MouseMove -= MainWindow_MouseMove;


            _tooltips.Remove(tooltip_content);
        }


        public bool IsTooltipVisible(FrameworkElement tooltip_content)
        {
            return _tooltips.ContainsKey(tooltip_content);
        }

        public void ShowTooltip(FrameworkElement tooltip_content)
        {
            ShowTooltip(tooltip_content, _tooltip_wait_before_show_time, _tooltip_pointer_len, _tooltip_pointer_showangle, true);
        }


        public void ShowTooltip(FrameworkElement tooltip_content, int wait_before_show_msec, float pointer_len, float show_angle, Vector2f ref_pos)
        {
            ShowTooltip(tooltip_content, wait_before_show_msec, pointer_len, show_angle, false, ref_pos);
        }

        public void ShowTooltip(FrameworkElement tooltip_content, int wait_before_show_msec, float pointer_len, float show_angle, bool sync_with_mouse_pos)
        {
            ShowTooltip(tooltip_content, wait_before_show_msec, pointer_len, show_angle, sync_with_mouse_pos, new Vector2f());
        }

        private void ShowTooltip(FrameworkElement tooltip_content, int wait_before_show_msec, float pointer_len, float show_angle, bool sync_with_mouse_pos, Vector2f ref_pos)
        {
            if(tooltip_content == null)
                return;

            TooltipData tooltip_data;
            if (_tooltips.ContainsKey(tooltip_content))
            {
                tooltip_data = _tooltips[tooltip_content];
                HideTooltip(tooltip_content);
            }
            else
            {
                tooltip_data = CreateTooltipData(tooltip_content, wait_before_show_msec, pointer_len, show_angle, sync_with_mouse_pos, ref_pos);
                _tooltips.Add(tooltip_content, tooltip_data);
            }

            ShowTooltipExecuteNow(tooltip_data);
        }

        private TooltipData CreateTooltipData(FrameworkElement tooltip_content, int wait_before_show_msec, float pointer_len, float show_angle, bool sync_with_mouse_pos, Vector2f ref_pos)
        {
            TooltipData tooltip_data = new TooltipData();
            tooltip_data._curr_tooltip_wait_before_show_time = wait_before_show_msec;
            tooltip_data._curr_tooltip_pointer_len = pointer_len;
            tooltip_data._curr_tooltip_pointer_showangle = show_angle;
            tooltip_data.SyncWithMousePos = sync_with_mouse_pos;

            tooltip_data._tooltip = tooltip_content;
            if (sync_with_mouse_pos)
            {
                Point curr_mouse_pos = Mouse.GetPosition(canvas);
                tooltip_data._tooltip_reference_pos = new Vector2f((float)curr_mouse_pos.X, (float)curr_mouse_pos.Y);
            }
            else
            {
                tooltip_data._tooltip_reference_pos = ref_pos;
            }

            tooltip_data.pointer = new Path();
            tooltip_data.pointer.Visibility = Visibility.Hidden;
            tooltip_data.pointer.IsHitTestVisible = false;
            tooltip_data.pointer.Focusable = false;
            tooltip_data.pointer.Style = PointerStyle;
            canvas.Children.Add(tooltip_data.pointer);
            Canvas.SetZIndex(tooltip_data.pointer, 1);

            tooltip_data.border = new Border();
            tooltip_data.border.Visibility = Visibility.Hidden;
            tooltip_data.border.IsHitTestVisible = false;
            tooltip_data.border.Focusable = false;
            tooltip_data.border.Style = BorderStyle;
            tooltip_data.border.Child = tooltip_content;
            tooltip_data.border.Tag = "tborder";
            canvas.Children.Add(tooltip_data.border);
            Canvas.SetZIndex(tooltip_data.border, 2);

            tooltip_content.Tag = tooltip_data;

            return tooltip_data;
        }

        private void ShowTooltipExecuteNow(TooltipData tooltip_data)
        {
            if (tooltip_data == null)
                return;

            tooltip_data.border.Opacity = 0;
            tooltip_data.pointer.Opacity = 0;
            tooltip_data.pointer.BeginAnimation(UIElement.OpacityProperty, null);
            tooltip_data.border.BeginAnimation(UIElement.OpacityProperty, null);
            tooltip_data.border.Visibility = Visibility.Visible;

            bool size_known = tooltip_data._tooltip.ActualWidth > 0 || tooltip_data._tooltip.ActualHeight > 0;
            if (size_known && tooltip_data.border.ActualWidth > tooltip_data._tooltip.ActualWidth && tooltip_data.border.ActualHeight > tooltip_data._tooltip.ActualHeight)
                TooltipPanel_SizeChanged(null, null);
            else if (size_known)
                tooltip_data.border.SizeChanged += TooltipPanel_SizeChanged;
            else
                ((FrameworkElement)tooltip_data.border.Child).SizeChanged += TooltipPanel_SizeChanged;
        }


        #region Tooltip Positioning

        #region Update TooltipPos
        public void UpdateTooltipPosition(FrameworkElement tooltip_content,Vector2f ref_pos)
        {
            if (!_tooltips.ContainsKey(tooltip_content))
                return;
            TooltipData tooltip_data = _tooltips[tooltip_content];
            UpdateTooltipPos(tooltip_data, ref_pos);
        }
        public void UpdateTooltipPosition(FrameworkElement tooltip_content, float pointer_len, float show_angle, Vector2f ref_pos)
        {
            if (!_tooltips.ContainsKey(tooltip_content))
                return;
            TooltipData tooltip_data = _tooltips[tooltip_content];
            tooltip_data._curr_tooltip_pointer_len = pointer_len;
            tooltip_data._curr_tooltip_pointer_showangle = show_angle;
            UpdateTooltipPos(tooltip_data, ref_pos);
        }

        public void UpdateTooltipWithMousePos(FrameworkElement tooltip_content)
        {
            if (!_tooltips.ContainsKey(tooltip_content))
                return;
            TooltipData tooltip_data = _tooltips[tooltip_content];
            UpdateTooltipWithMousePos(tooltip_data);
        }

        protected void UpdateTooltipWithMousePos(TooltipData tooltip_data)
        {
            Point curr_mouse_pos = Mouse.GetPosition(canvas);
            UpdateTooltipPos(tooltip_data, new Vector2f((float)curr_mouse_pos.X, (float)curr_mouse_pos.Y));
        }

        protected void UpdateTooltipPos(TooltipData tooltip_data, Vector2f new_ref_pos)
        {
            if (tooltip_data.border.Visibility != Visibility.Visible)
                return;

            Vector2f last_pos = tooltip_data._tooltip_reference_pos;
            tooltip_data._tooltip_reference_pos = new_ref_pos;
            Vector2f rel_move = tooltip_data._tooltip_reference_pos - last_pos;

            double body_pos_left = Canvas.GetLeft(tooltip_data.border);
            double body_pos_top = Canvas.GetTop(tooltip_data.border);
            Vector2f pos_body = new Vector2f((float)body_pos_left, (float)body_pos_top);

            if (last_pos == tooltip_data._tooltip_reference_pos)
                return;

            Vector2f next_pos_body = pos_body + rel_move;
            next_pos_body.X += (float)(tooltip_data.border.ActualWidth / 2);
            next_pos_body.Y += (float)(tooltip_data.border.ActualHeight / 2);
            Canvas.SetLeft(tooltip_data.border, next_pos_body.X - tooltip_data.border.ActualWidth / 2);
            Canvas.SetTop(tooltip_data.border, next_pos_body.Y - tooltip_data.border.ActualHeight / 2);

            tooltip_data.pointer.Data = GetPointerGeometry(tooltip_data, next_pos_body);
        }
        #endregion


        private void TooltipPanel_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            TooltipData tooltip_data;
            if (sender is Border && ((Border)sender).Tag is string && ((Border)sender).Tag.ToString() == "tborder")
                tooltip_data = (TooltipData)((FrameworkElement)((Border)sender).Child).Tag;
            else if (sender is FrameworkElement && ((FrameworkElement)sender).Tag is TooltipData)
                tooltip_data = (TooltipData)((FrameworkElement)sender).Tag;
            else
                throw new Exception("unknown error");


            Vector2f tooltip_pos = GetBestTooltipPos(tooltip_data);

            tooltip_data.pointer.Data = GetPointerGeometry(tooltip_data, tooltip_pos);
            tooltip_data.pointer.Visibility = Visibility.Visible;

            if (tooltip_data.border.Opacity == 0)
            {
                tooltip_data.border.Opacity = 0;
                tooltip_data.pointer.Opacity = 0;
                tooltip_data.border.BeginAnimation(OpacityProperty, _fade_in_anim);
                tooltip_data.pointer.BeginAnimation(OpacityProperty, _fade_in_anim);
            }

            //execute positioning algorithm
            Canvas.SetLeft(tooltip_data.border, tooltip_pos.X - tooltip_data.border.ActualWidth / 2);
            Canvas.SetTop(tooltip_data.border, tooltip_pos.Y - tooltip_data.border.ActualHeight / 2);

            if (MainWindow != null)
                MainWindow.MouseMove += new MouseEventHandler(MainWindow_MouseMove);
        }

        private Vector2f GetBestTooltipPos(TooltipData tooltip_data)
        {
            Vector2f tooltip_pos_center;
            float area_tooltip_outside;

            //try inital pos
            tooltip_pos_center = GetToolTipPosition(tooltip_data);
            area_tooltip_outside = GetTooltipOutsideArea(tooltip_pos_center, (float)tooltip_data.border.ActualWidth, (float)tooltip_data.border.ActualHeight);
            if (area_tooltip_outside == 0)
                return tooltip_pos_center;

            //try inverted initial_pos
            tooltip_data._curr_tooltip_pointer_showangle = -(float)Math.PI;
            tooltip_pos_center = GetToolTipPosition(tooltip_data);
            area_tooltip_outside = GetTooltipOutsideArea(tooltip_pos_center, (float)tooltip_data.border.ActualWidth, (float)tooltip_data.border.ActualHeight);
            if (area_tooltip_outside == 0)
                return tooltip_pos_center;

            //find best pos through rotation of angle
            float angle_rot_einheit = (float)Math.PI / 10f;
            tooltip_data._curr_tooltip_pointer_showangle = angle_rot_einheit;
            Vector2f best_tooltip_pos = new Vector2f();
            float best_angle = 0;
            float curr_min_area_outside = float.MaxValue;
            bool initial_pos_tooltip_outside = false;
            while (tooltip_data._curr_tooltip_pointer_showangle <= Math.PI * 2)
            {
                tooltip_pos_center = GetToolTipPosition(tooltip_data);
                area_tooltip_outside = GetTooltipOutsideArea(tooltip_pos_center, (float)tooltip_data.border.ActualWidth, (float)tooltip_data.border.ActualHeight);
                if (area_tooltip_outside == 0 && !initial_pos_tooltip_outside)
                {
                    best_tooltip_pos = tooltip_pos_center;
                    best_angle = tooltip_data._curr_tooltip_pointer_showangle;
                    break;
                }

                initial_pos_tooltip_outside = true;
                if (area_tooltip_outside < curr_min_area_outside)
                {
                    best_tooltip_pos = tooltip_pos_center;
                    curr_min_area_outside = area_tooltip_outside;
                    best_angle = tooltip_data._curr_tooltip_pointer_showangle;
                }

                tooltip_data._curr_tooltip_pointer_showangle += angle_rot_einheit;
            }
            tooltip_data._curr_tooltip_pointer_showangle = best_angle;
            return best_tooltip_pos;
        }

        private float GetTooltipOutsideArea(Vector2f tooltip_pos_center, float tooltip_width, float tooltip_height)
        {
            float window_width = (float)canvas.ActualWidth;
            float window_height = (float)canvas.ActualHeight;

            Vector2d window_topleft = new Vector2d(0, 0);
            Vector2d window_bottomright = new Vector2d(window_topleft.X + window_width, window_topleft.Y + window_height);
            Rect r_window = new Rect(window_topleft.X, window_topleft.Y, window_width, window_height);

            Rect r_tooltip = new Rect(tooltip_pos_center.X - tooltip_width / 2, tooltip_pos_center.Y - tooltip_height / 2, tooltip_width, tooltip_height);

            float tooltip_area = (float)(r_tooltip.Width * r_tooltip.Height);
            if (!r_tooltip.IntersectsWith(r_window))
                return tooltip_area;

            r_tooltip.Intersect(r_window);
            return tooltip_area - (float)(r_tooltip.Width * r_tooltip.Height);
        }

        private Vector2f GetToolTipPosition(TooltipData tooltip_data)
        {
            Vector2f cutpoint_on_border = GetCutpointOnTooltip(tooltip_data._curr_tooltip_pointer_showangle - (float)Math.PI, (float)tooltip_data.border.ActualWidth, (float)tooltip_data.border.ActualHeight);
            float pointer_len = tooltip_data._curr_tooltip_pointer_len + cutpoint_on_border.GetLen();

            Vector2f v_pointer = new Vector2f(1, 0);
            v_pointer.Rotate(tooltip_data._curr_tooltip_pointer_showangle);
            Vector2f tooltip_pos = tooltip_data._tooltip_reference_pos + v_pointer * pointer_len;

            return tooltip_pos;
        }

        private Vector2f GetCutpointOnTooltip(float angle, float tooltip_width, float tooltip_height)
        {
            Vector2f v_pointer = new Vector2f(1, 0);
            v_pointer.Rotate(angle);

            float ratio_pointer = v_pointer.Y / v_pointer.X;
            float ratio_tooltip = (tooltip_height / 2f) / (tooltip_width / 2f);

            bool cut_with_vertical_border_given = !(ratio_pointer >= ratio_tooltip || ratio_pointer <= -ratio_tooltip);
            bool cut_with_horz_border_given = !cut_with_vertical_border_given;

            if (cut_with_vertical_border_given)
            {
                if (v_pointer.X < 0)
                {
                    float fac = (-tooltip_width / 2f) / v_pointer.X;
                    v_pointer *= fac;
                }
                else
                {
                    float fac = (tooltip_width / 2f) / v_pointer.X;
                    v_pointer *= fac;
                }
            }
            else
            {
                if (v_pointer.Y < 0)
                {
                    float fac = (-tooltip_height / 2f) / v_pointer.Y;
                    v_pointer *= fac;
                }
                else
                {
                    float fac = (tooltip_height / 2f) / v_pointer.Y;
                    v_pointer *= fac;
                }
            }
            return v_pointer;
        }
        #endregion

        #region Tooltip Pointer Positioning
        protected Geometry GetPointerGeometry(TooltipData tooltip_data, Vector2f body_pos)
        {
            Vector2f v_start = tooltip_data._tooltip_reference_pos;

            Vector2f v1;
            Vector2f v2;
            GetV1V2ForPointer(tooltip_data._curr_tooltip_pointer_showangle, 1, body_pos, (float)tooltip_data.border.ActualWidth, (float)tooltip_data.border.ActualHeight, out v1, out v2);

            StreamGeometry streamgeometry = new StreamGeometry();
            using (StreamGeometryContext ctx = streamgeometry.Open())
            {
                Point start_point = new Point(v_start.X, v_start.Y);
                ctx.BeginFigure(start_point, true /* is filled */, true /* is not closed? */);
                ctx.LineTo(new Point(v1.X, v1.Y), false /* is stroked */, true /* is smooth join */);
                ctx.LineTo(new Point(v2.X, v2.Y), false /* is stroked */, true /* is smooth join */);
            }

            streamgeometry.Transform = new RotateTransform(180 / Math.PI * tooltip_data._curr_tooltip_pointer_showangle) { CenterX = v_start.X, CenterY = v_start.Y };
            tooltip_data.pointer.RenderTransform = new RotateTransform(-(180 / Math.PI * tooltip_data._curr_tooltip_pointer_showangle)) { CenterX = v_start.X, CenterY = v_start.Y };

            streamgeometry.FillRule = FillRule.Nonzero;
            streamgeometry.Freeze();
            return streamgeometry;
        }
        protected void GetV1V2ForPointer(float angle, float corner_rad_tooltip, Vector2f body_pos, float body_width, float body_height, out Vector2f v1, out Vector2f v2)
        {
            Vector2f v_pointer = new Vector2f(1, 0);
            v_pointer.Rotate(-angle - (float)Math.PI);
            float ratio_pointer = v_pointer.Y / v_pointer.X;
            float ratio_tooltip = body_height / body_width;
            bool cut_with_vertical_border_given = !(ratio_pointer >= ratio_tooltip || ratio_pointer <= -ratio_tooltip);

            if (cut_with_vertical_border_given)
            {
                if (v_pointer.X < 0)
                {
                    v1 = new Vector2f(body_pos.X - body_width / 2 + corner_rad_tooltip, body_pos.Y - body_height / 2);
                    v2 = new Vector2f(body_pos.X - body_width / 2 + corner_rad_tooltip, body_pos.Y + body_height / 2);
                }
                else
                {
                    v1 = new Vector2f(body_pos.X + body_width / 2 - corner_rad_tooltip, body_pos.Y - body_height / 2);
                    v2 = new Vector2f(body_pos.X + body_width / 2 - corner_rad_tooltip, body_pos.Y + body_height / 2);
                }
            }
            else
            {
                if (v_pointer.Y > 0)
                {
                    v1 = new Vector2f(body_pos.X - body_width / 2, body_pos.Y - body_height / 2 + corner_rad_tooltip);
                    v2 = new Vector2f(body_pos.X + body_width / 2, body_pos.Y - body_height / 2 + corner_rad_tooltip);

                }
                else
                {
                    v1 = new Vector2f(body_pos.X - body_width / 2, body_pos.Y + body_height / 2 - corner_rad_tooltip);
                    v2 = new Vector2f(body_pos.X + body_width / 2, body_pos.Y + body_height / 2 - corner_rad_tooltip);
                }
            }

            v2 += ((v1 - v2).GetNorm() * ((v1 - v2).GetLen() * 0.1f));
            v1 += ((v2 - v1).GetNorm() * ((v1 - v2).GetLen() * 0.1f));

        }
        #endregion

        #region EventHandlers
        void MainWindow_MouseMove(object sender, MouseEventArgs e)
        {
            foreach (var tooltip_data in _tooltips)
            {
                if (tooltip_data.Value.SyncWithMousePos)
                    UpdateTooltipWithMousePos(tooltip_data.Value);
            }
        }
        #endregion

        #region Dependency Properties
        public Style PointerStyle
        {
            get { return (Style)GetValue(PointerStyleProperty); }
            set { SetValue(PointerStyleProperty, value); }
        }
        public static readonly DependencyProperty PointerStyleProperty = DependencyProperty.Register("PointerStyle", typeof(Style), typeof(TooltipPanel), new UIPropertyMetadata(null));

        public Style BorderStyle
        {
            get { return (Style)GetValue(BorderStyleProperty); }
            set { SetValue(BorderStyleProperty, value); }
        }
        public static readonly DependencyProperty BorderStyleProperty = DependencyProperty.Register("BackgroundStyle", typeof(Style), typeof(TooltipPanel), new UIPropertyMetadata(null));
        #endregion

        #region Attributes
        protected DoubleAnimation _fade_in_anim;


        /// <summary>
        /// wenn diese ref gesetzt ist dann wird auf das mouse move registriert
        /// was bedeutet das sich der tooltip mitbewegt wenn sich die mouse bewegt
        /// //solange der tooltip aktiv ist
        /// </summary>
        public Window MainWindow;

        protected Dictionary<FrameworkElement, TooltipData> _tooltips = new Dictionary<FrameworkElement, TooltipData>();

        public const int _tooltip_wait_before_show_time = 1000;
        public const float _tooltip_pointer_showangle = (float)(Math.PI * 0.5);
        public const float _tooltip_pointer_len = 20;
        #endregion



    }
}
