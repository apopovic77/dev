﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using PixelFormat = System.Windows.Media.PixelFormat;
using Point = System.Windows.Point;

namespace Logicx.WpfUtility.ScreenCapture
{
    public class ScreenShot
    {

        #region DllImports
        [DllImport("user32.dll")]
        private static extern IntPtr GetDC(IntPtr ptr);
        [DllImport("user32.dll")]
        private static extern IntPtr GetDesktopWindow();
        [DllImport("user32.dll")]
        private static extern IntPtr ReleaseDC(IntPtr hWnd, IntPtr hDc);
        [DllImport("user32.dll")]
        private static extern int GetSystemMetrics(int abc);
        [DllImport("gdi32")]
        private static extern IntPtr CreateCompatibleDC(IntPtr hDC);
        [DllImport("gdi32")]
        private static extern IntPtr CreateCompatibleBitmap(IntPtr hDC, int nWidth, int nHeight);
        [DllImport("gdi32")]
        private static extern IntPtr SelectObject(IntPtr hDC, IntPtr hObject);
        [DllImport("gdi32")]
        private static extern bool BitBlt(IntPtr hDestDC, int X, int Y, int nWidth, int nHeight, IntPtr hSrcDC, int SrcX, int SrcY, int Rop);
        [DllImport("gdi32")]
        private static extern bool DeleteObject(IntPtr handle);
        [DllImport("gdi32")]
        private static extern IntPtr DeleteDC(IntPtr hDC);
        [DllImport("user32.dll")]
        private static extern IntPtr GetWindowDC(IntPtr hWnd);
        [DllImport("user32.dll")]
        private static extern IntPtr GetWindowRect(IntPtr hWnd, ref RECT rect);
        #endregion

        [StructLayout(LayoutKind.Sequential)]
        private struct RECT
        {
            public int left;
            public int top;
            public int right;
            public int bottom;
        }


        #region Const
        const int SRCCOPY = 0xCC0020;
        const int SCREEN_X = 0;
        const int SCREEN_Y = 1;
        #endregion

        public static byte[] Take(IntPtr hwnd_source, int x, int y, int width, int height, out PixelFormat pixel_format )
        {
            IntPtr device;
            IntPtr memory;
            IntPtr Bitmap;
            byte[] pixels = null;
            pixel_format = PixelFormats.Rgba64;

            // Handle auf Device Context und Speicher
            device = GetDC(hwnd_source);
            memory = CreateCompatibleDC(device);

            // Erstelle ein Bild mit den Größen des Desktops
            Bitmap = CreateCompatibleBitmap(device, width, height);

            if (Bitmap != IntPtr.Zero)
            {
                // Das Bild auf den Speicherbereich zeigen lassen
                SelectObject(memory, Bitmap);

                // Screen aus dem Device in den Speicher schreiben
                BitBlt(memory, 0, 0, width, height, device, 0, 0, SRCCOPY);

                // Speicherobjekt löschen
                DeleteDC(memory);
                // Device freigeben
                ReleaseDC(GetDesktopWindow(), device);

                // Bildquelle erstellen
                BitmapSource bmp_source = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(
                                                        Bitmap,
                                                        IntPtr.Zero,
                                                        Int32Rect.Empty, 
                                                        BitmapSizeOptions.FromEmptyOptions());


                //copy pixel to byte array
                int nStride = (bmp_source.PixelWidth * bmp_source.Format.BitsPerPixel + 7) / 8;
                pixels = new byte[bmp_source.PixelHeight * nStride];
                bmp_source.CopyPixels(pixels, nStride, 0);
                pixel_format = bmp_source.Format;
                


                //System.Windows.Controls.Image img = new System.Windows.Controls.Image();
                //img.Source = bmp_source;
                //img.Width = width;
                //img.Height = height;
                //img.Dispatcher.Invoke(DispatcherPriority.Render, EmptyDelegate);
                
                //// Renderbitmap mit den Bildgrößen erstellen
                //RenderTargetBitmap renderBitmap = new RenderTargetBitmap(
                //                                           (int)img.Width,
                //                                           (int)img.Height,
                //                                           96d,
                //                                           96d,
                //                                           PixelFormats.Pbgra32);
                //// Bild rendern
                //renderBitmap.Render(img);


                //int nStride = (renderBitmap.PixelWidth * renderBitmap.Format.BitsPerPixel + 7) / 8;
                //pixels = new byte[renderBitmap.PixelHeight * nStride];
                //renderBitmap.CopyPixels(pixels, nStride, 0);
                //pixel_format = renderBitmap.Format;



                
                // Bild löschen und Speicher freigeben
                DeleteObject(Bitmap);
            }
            return pixels;
        }

        /// <summary>
        /// Creates an Image object containing a screen shot of the entire desktop
        /// </summary>
        /// <returns></returns>
        public static byte[] CaptureScreen(out int width, out int height,out PixelFormat pixel_format)
        {
            // Breite und Höhe auslesen
            width = GetSystemMetrics(SCREEN_X);
            height = GetSystemMetrics(SCREEN_Y);

            return Take(GetDesktopWindow(), 0, 0, width, height, out pixel_format);
        }

        /// <summary>
        /// Creates an Image object containing a screen shot of a specific window
        /// </summary>
        /// <param name="handle">The handle to the window. (In windows forms, this is obtained by the Handle property)</param>
        /// <returns></returns>
        public static byte[] CaptureWindow(IntPtr handle, out int width, out int height, out PixelFormat pixel_format)
        {
            // get te hDC of the target window
            IntPtr hdcSrc = GetWindowDC(handle);
            // get the size
            RECT windowRect = new RECT();
            GetWindowRect(handle, ref windowRect);
            width = windowRect.right - windowRect.left;
            height = windowRect.bottom - windowRect.top;

            return Take(handle, 0,0,width,height, out pixel_format);
        }

        private static Action EmptyDelegate = delegate() { };
    }
}
