﻿using PoCWebApp.Config;
using PoCWebApp.Data.Infrastructure;

namespace PoCWebApp.Data.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<PoCWebApp.Data.PoCPrinterContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = AppConfig.AutomaticDbMigrationsEnabled;
            ContextKey = "PoCWebApp.Data.PoCPrinterContext";
            MigrationsDirectory = @"Data\Migrations";
        }

        protected override void Seed(PoCWebApp.Data.PoCPrinterContext context)
        {
            //  This method will be called after migrating to the latest version.
            // reuse seeding method of DbInitializer
            DbInitializer initializer = new DbInitializer();
            initializer.InsertSeedData(context, false);
        }
    }
}
