﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace Logicx.WpfUtility.WpfHelpers
{
    public class ScrollViewerUtilities
    {
        /// <summary>
        /// Diese Region dient dazu, einen Scrollviewer per Doubleanimation scrollen zu lassen.
        /// Da die Eigenschaften VerticalOffset bzw. HorizontalOffset readonly Eigenschaften sind, ist es standardmässig nicht möglich 
        /// eine Animation auf diesen Properties laufen zu lassen.
        /// Es werden aus diesen Grund 2 DP-Properties eingeführt, die in ihren ChangeHandlern die Methode ScrollToHorizontalOffset bzw. ScrollToVerticalOffset aufrufen
        /// Diese beiden Properties können nun per DoubleAnimation verändert werden
        /// </summary>
        #region Animating ScrollOffset

        #region HorizontalOffset
        public static readonly DependencyProperty HorizontalOffsetProperty =
            DependencyProperty.RegisterAttached("HorizontalOffset", typeof(double), typeof(ScrollViewerUtilities),
               new FrameworkPropertyMetadata((double)0.0, new PropertyChangedCallback(OnHorizontalOffsetChanged)));

    

  

       public static double GetHorizontalOffset(DependencyObject d)
       {
           return (double)d.GetValue(HorizontalOffsetProperty);
       }

       public static void SetHorizontalOffset(DependencyObject d, double value)
       {
           d.SetValue(HorizontalOffsetProperty, value);
       }

 
       private static void OnHorizontalOffsetChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
       {
          var viewer = (ScrollViewer)d;
          viewer.ScrollToHorizontalOffset((double)e.NewValue * viewer.ScrollableWidth);
       }

  
       #endregion

        #region VerticalOffset

       public static readonly DependencyProperty VerticalOffsetProperty = DependencyProperty.RegisterAttached("VerticalOffset", typeof(double), typeof(ScrollViewerUtilities),
               new FrameworkPropertyMetadata((double)0.0, new PropertyChangedCallback(OnVerticalOffsetChanged)));


       public static double GetVerticalOffset(DependencyObject d)
       {
           return (double)d.GetValue(VerticalOffsetProperty);
       }

       public static void SetVerticalOffset(DependencyObject d, double value)
       {
           d.SetValue(VerticalOffsetProperty, value);
       }

       private static void OnVerticalOffsetChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
       {
           var viewer = (ScrollViewer)d;
           viewer.ScrollToVerticalOffset((double)e.NewValue * viewer.ScrollableHeight);
       }
       #endregion

        #endregion
    }
}
