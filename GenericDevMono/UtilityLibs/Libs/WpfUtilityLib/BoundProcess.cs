﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Windows;
using System.Windows.Forms;
using Logicx.WpfUtility.WindowParallelisation.WCFComm;
using Application = System.Windows.Application;

namespace Logicx.WpfUtility
{
    public class BoundProcess : IDisposable
    {
        protected BoundProcess(string path_to_process, string[] args)
        {
            _path_to_process = path_to_process;
            _args = "";
            if (args != null)
                for (int i = 0; i < args.Length; i++)
                    _args += args[i] + " ";

            if (string.IsNullOrEmpty(_path_to_process))
                throw new Exception("need path to process");

            if (!File.Exists(_path_to_process))
                throw new Exception("path to process error, process executeable not found");
        }

        public BoundProcess(Window window_origin, string path_to_process, string[] args) 
            : this(path_to_process, args)
        {
            _window_origin_wpf = window_origin;
        }

        public BoundProcess(Form form, string path_to_process, string[] args)
            : this(path_to_process, args)
        {
            _window_origin_form = form;
        }

        #region Bonded Window Overlay
        public void AttachBoundedWindowApp()
        {
            if (_bound_process != null)
                throw new Exception("Is already attached, plz detach first");
            
            DoAttachBoundedWindowApp();
        }

        private void DoAttachBoundedWindowApp()
        {

            _bound_process = new Process();
            _bound_process.StartInfo.UseShellExecute = UseShellExecute;
            _bound_process.StartInfo.FileName = _path_to_process;
            _bound_process.StartInfo.CreateNoWindow = CreateNoWindow;
            _bound_process.StartInfo.Arguments = _args;
            _bound_process.Exited += new EventHandler(_bound_window_process_Exited);
            _bound_process.Disposed += new EventHandler(_bound_window_process_Disposed);
            _bound_process.Start();

            if(_window_origin_wpf != null)
                _window_origin_wpf.Closed += new EventHandler(_window_origin_Closed);
            if(_window_origin_form != null)
                _window_origin_form.Closed += new EventHandler(_window_origin_form_Closed);

            if (Application.Current != null && Application.Current.Dispatcher.Thread == Thread.CurrentThread)
                Application.Current.Exit += new ExitEventHandler(Current_Exit);
        }

        void _window_origin_form_Closed(object sender, EventArgs e)
        {
            DetachBoundProcess();
        }

        void _window_origin_Closed(object sender, EventArgs e)
        {
            DetachBoundProcess();
        }


        void _bound_window_process_Disposed(object sender, EventArgs e)
        {
        }

        void _bound_window_process_Exited(object sender, EventArgs e)
        {
        }

        void Current_Exit(object sender, ExitEventArgs e)
        {
            DetachBoundProcess();
        }

        void DetachBoundProcess()
        {
            if (_bound_process != null)
            {
                _bound_process.CloseMainWindow();
                _bound_process.Dispose();
                _bound_process = null;
            }
        }
        #endregion

        public void Dispose()
        {
            DetachBoundProcess();
        }


        #region Attributes
        private Window _window_origin_wpf;
        private Form _window_origin_form;
        public bool CreateNoWindow { get; set; }
        public bool UseShellExecute { get; set; }
        private Process _bound_process;
        private string _path_to_process;
        private string _args;
        #endregion

    }
}
