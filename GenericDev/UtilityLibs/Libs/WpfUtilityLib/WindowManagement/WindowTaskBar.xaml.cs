﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Logicx.WpfUtility.CustomControls.TouchButton;
using Logicx.WpfUtility.WindowManagement.Adorners;
using Logicx.WpfUtility.WpfHelpers;

namespace Logicx.WpfUtility.WindowManagement
{
    /// <summary>
    /// Interaction logic for WindowTaskBar.xaml
    /// </summary>
    public partial class WindowTaskBar
    {
        public class TaskbarButtonClickEventArgs : EventArgs
        {
            public TaskbarButtonClickEventArgs(string title)
            {
                ButtonTitle = title;
            }

            public string ButtonTitle;
        }

        public enum ItemType
        {
            FrameworkElement,
            Button,
            ToggleButton,
            SubMenuButton,
            TabButton
        }

        public enum ItemPlacmentPosition
        {
            Left,
            Center,
            Right
        }

        public WindowTaskBar()
        {
            InitializeComponent();
            _items = new List<TaskbarItem>();

            this.TapActive += WindowTaskBar_TapActive;
            this.TapInActive += WindowTaskBar_TapInActive;
        }

        #region Add and Remove Items
        public void AddItem(ItemType item_type, FrameworkElement framework_element, string title, Style container_border_style, Style text_style)
        {
            AddItem(item_type, framework_element, title, container_border_style, text_style, item_panel_stack_center.Children.Count, ItemPlacmentPosition.Center);
        }

        public void AddItem(ItemType item_type, FrameworkElement framework_element, string title, Style container_border_style, Style text_style, int insert_position)
        {
            AddItem(item_type, framework_element, title, container_border_style, text_style, insert_position, ItemPlacmentPosition.Center);
        }


        public void AddItem(ItemType item_type, FrameworkElement framework_element, string title, Style container_border_style, Style text_style, ItemPlacmentPosition position)
        {
            int nposition = 0;

            switch (position)
            {
                case ItemPlacmentPosition.Left:
                    nposition = item_panel_stack_left.Children.Count;
                    break;
                case ItemPlacmentPosition.Center:
                    nposition = item_panel_stack_center.Children.Count; 
                    break;
                case ItemPlacmentPosition.Right:
                    nposition = item_panel_stack_right.Children.Count; 
                    break;
            }
            AddItem(item_type, framework_element, title, container_border_style, text_style, nposition, position);
        }

        public void AddItem(ItemType item_type, FrameworkElement framework_element, string title, Style container_border_style, Style text_style, int insert_position, ItemPlacmentPosition position)
        {
            TaskbarItem item = new TaskbarItem(framework_element, title);
            _items.Add(item);
            item.item_border.Tag = item;
            item.ItemType = item_type;
            item.ContainerBorderStyle = container_border_style;
            item.TextStyle = text_style;

            FrameworkElement taskbar_container = GetTaskBarContainer(item_type, item);
            item.Container = taskbar_container;

            if(taskbar_container.Tag is SubMenuPanel)
            {
                ((SubMenuPanel) taskbar_container.Tag).TaskbarItemName = title;
            }

            switch (position)
            {
                case ItemPlacmentPosition.Left:
                    item_panel_stack_left.Children.Insert(insert_position, taskbar_container);
                    break;
                case ItemPlacmentPosition.Center:
                    item_panel_stack_center.Children.Insert(insert_position, taskbar_container);
                    break;
                case ItemPlacmentPosition.Right:
                    item_panel_stack_right.Children.Insert(insert_position, taskbar_container);
                    break;
            }
            

            if (ItemAdded != null)
                ItemAdded(this, new EventArgs());

            ActiveStyleAdornerManager.Instance.Register(item, BaseFrame.AdornerActiveRenderBrush, BaseFrame.AdornerActiveRenderPen);

            //if (WithSpreadMenuManagement)
            //{
            //    if (framework_element is SingleSpreadMenu)
            //    {
            //        SingleSpreadMenu menu = (SingleSpreadMenu)framework_element;
            //        menu.ToggleButton.Checked += new RoutedEventHandler(ToggleButton_Checked);
            //        menu.ToggleButton.Unchecked += new RoutedEventHandler(ToggleButton_Unchecked);
            //    }
            //    else if (framework_element is PointerWrapPanelButton)
            //    {
            //        PointerWrapPanelButton menu = (PointerWrapPanelButton)framework_element;
            //        menu.ToggleButton.Checked += new RoutedEventHandler(ToggleButton_Checked);
            //        menu.ToggleButton.Unchecked += new RoutedEventHandler(ToggleButton_Unchecked);
            //    }
            //}
        }

        public void SetApplicationInfo(string app_info1, string app_info2)
        {
            if(!string.IsNullOrEmpty(app_info1))
            {
                app_info_line1.Text = app_info1;
                app_info_line1.Visibility = Visibility.Visible;
                user_info_grid.Visibility = Visibility.Visible;
            }
            else
            {
                app_info_line1.Visibility = Visibility.Collapsed;
            }

            if (!string.IsNullOrEmpty(app_info2))
            {
                app_info_line2.Text = app_info2;
                app_info_line2.Visibility = Visibility.Visible;
                user_info_grid.Visibility = Visibility.Visible;
            }
            else
            {
                app_info_line2.Visibility = Visibility.Collapsed;
            }
        }

        public void SetUserInfo(string user_info1, string user_info2)
        {
            if (!string.IsNullOrEmpty(user_info1))
            {
                user_info_line1.Text = user_info1;
                user_info_line1.Visibility = Visibility.Visible;
                user_info_grid.Visibility = Visibility.Visible;
            }
            else
            {
                user_info_line1.Visibility = Visibility.Collapsed;
            }

            if (!string.IsNullOrEmpty(user_info2))
            {
                user_info_line2.Text = user_info2;
                user_info_line2.Visibility = Visibility.Visible;
                user_info_grid.Visibility = Visibility.Visible;
            }
            else
            {
                user_info_line2.Visibility = Visibility.Collapsed;
            }
        }

        private FrameworkElement GetTaskBarContainer(ItemType item_type, FrameworkElement base_elem)
        {
            switch (item_type)
            {
                case ItemType.FrameworkElement:
                    return base_elem;
                case ItemType.Button:
                    TouchButton b_container = new TouchButton();
                    b_container.Style = (Style)FindResource("submenu_container_button_style");
                    b_container.Content = base_elem;
                    b_container.Click += new RoutedEventHandler(b_container_Click);
                    return b_container;
                case ItemType.ToggleButton:
                    TouchToggleButton tb_container = new TouchToggleButton();
                    tb_container.Content = base_elem;
                    tb_container.Checked += new RoutedEventHandler(tb_container_Checked);
                    tb_container.Unchecked += new RoutedEventHandler(tb_container_Unchecked);
                    return tb_container;
                case ItemType.SubMenuButton:
                    TouchToggleButton stb_container = new TouchToggleButton();
                    stb_container.Style = (Style)FindResource("submenu_container_togglebutton_style");
                    stb_container.Content = base_elem;
                    stb_container.Tag = CreateSubMenuPanel(stb_container);
                    stb_container.DataContext = stb_container.Tag;
                    stb_container.Checked += new RoutedEventHandler(stb_container_Checked);
                    stb_container.Unchecked += new RoutedEventHandler(stb_container_Unchecked);
                    return stb_container;
                case ItemType.TabButton:
                    TouchToggleButton tab_container = new TouchToggleButton();
                    tab_container.Style = (Style)FindResource("tab_togglebutton_style");
                    tab_container.Content = base_elem;
                    tab_container.Tag = tab_item_grid;
                    tab_container.Checked += new RoutedEventHandler(tab_container_Checked);
                    tab_container.Unchecked += new RoutedEventHandler(tab_container_Unchecked);
                    return tab_container;

                default:
                    throw new Exception("unknown item type");
            }
        }

        public void RemoveItem(FrameworkElement framework_element)
        {
            TaskbarItem item = framework_element.Tag as TaskbarItem;
            if (item == null)
                return;

            if (!item_panel_stack_left.Children.Contains(item) &&
                !item_panel_stack_center.Children.Contains(item) &&
                !item_panel_stack_right.Children.Contains(item))
                return;

            ActiveStyleAdornerManager.Instance.DeRegister(item);

            //start hide anim
            throw new NotImplementedException();

        }

        public FrameworkElement AddSeparator()
        {
            return AddSeparator(ItemPlacmentPosition.Center);
        }

        public FrameworkElement AddSeparator(ItemPlacmentPosition position)
        {
            Rectangle rect = new Rectangle();
            rect.Width = 1;
            rect.Height = 25;
            rect.Margin = new Thickness(10, 0, 10, 12);
            rect.Fill = BaseFrame.DefaultForegroundBrush;
            rect.VerticalAlignment = VerticalAlignment.Center;

            switch(position)
            {
                case ItemPlacmentPosition.Left: item_panel_stack_left.Children.Add(rect); break;
                case ItemPlacmentPosition.Center: item_panel_stack_center.Children.Add(rect); break;
                case ItemPlacmentPosition.Right: item_panel_stack_right.Children.Add(rect); break;
            }
            

            return rect;
        }

        public void HidePanel(string title)
        {
            ToggleButton tbutton = GetTabContainerByTitle(title) as ToggleButton;

            if (tbutton != null)
            {
                tbutton.IsChecked = false;
            }
            else
            {
                tbutton = GetSubMenuContainerByTitle(title) as ToggleButton;

                if (tbutton != null)
                {
                    tbutton.IsChecked = false;
                }
                else
                {
                    SubMenuPanel sub_menu_panel = GetSubMenuPanelByTitle(title);
                    if (sub_menu_panel != null)
                        sub_menu_panel.StartFadeOut();
                }
            }
        }
        
        public void ShowPanel(string title)
        {
            DateTime now = DateTime.Now;
            Debug.WriteLine("SHOW " + now.Second + ":" + now.Millisecond);

            SubMenuPanel sub_menu_panel = GetSubMenuPanelByTitle(title);

            SubMenuPanel curr_active_sub_menu_panel = ActiveSubMenuPanel;

            if (curr_active_sub_menu_panel != sub_menu_panel)
            {
                ToggleButton tbutton = GetTabContainerByTitle(title) as ToggleButton;

                if (tbutton == null)
                {
                    tbutton = GetSubMenuContainerByTitle(title) as ToggleButton;

                    if (tbutton != null)
                    {
                        tbutton.IsChecked = true;
                    }
                    else
                    {
                        if (curr_active_sub_menu_panel == null)
                        {
                            if (sub_menu_panel != null)
                                sub_menu_panel.StartFadeIn();
                        }
                        else
                        {
                            _tobeshow_panel = sub_menu_panel;
                            curr_active_sub_menu_panel.FadeOutCompleted += new EventHandler(curr_active_sub_menu_panel_FadeOutCompleted);
                            curr_active_sub_menu_panel.StartFadeOut();
                        }
                    }
                }
            }                  
        }

        void curr_active_sub_menu_panel_FadeOutCompleted(object sender, EventArgs e)
        {
            ((SubMenuPanel)sender).FadeOutCompleted -= curr_active_sub_menu_panel_FadeOutCompleted;
            if (_tobeshow_panel != null)
            {
                _tobeshow_panel.StartFadeIn();
                _tobeshow_panel = null;
            }
        }

        public void HideItem(string title)
        {
            TaskbarItem item = GetTaskbarItemByTitle(title);

            if (item != null)
                item.Visibility = System.Windows.Visibility.Collapsed;

            if (item != null && item.Container != null)
                item.Container.Visibility = System.Windows.Visibility.Collapsed;

            SubMenuPanel panel = GetSubMenuPanelByTitle(title);
            if(panel != null)
            {
                panel.StartFadeOut();
            }
        }

        public void ShowItem(string title)
        {
            TaskbarItem item = GetTaskbarItemByTitle(title);

            if (item != null)
                item.Visibility = System.Windows.Visibility.Visible;
            if (item != null && item.Container != null)
                item.Container.Visibility = System.Windows.Visibility.Visible;
        }

        public List<SubMenuPanel> GetSubMenuPanels()
        {
            List<TaskbarItem> taskbar_items = _items.Where(i => i.ItemType == ItemType.SubMenuButton).ToList();

            if (taskbar_items == null || taskbar_items.Count <= 0)
                return new List<SubMenuPanel>();

            List<SubMenuPanel> retList = new List<SubMenuPanel>();
            foreach(TaskbarItem item in taskbar_items)
            {
                if(item.Container.Tag is SubMenuPanel && !retList.Contains((SubMenuPanel)item.Container.Tag))
                    retList.Add((SubMenuPanel)item.Container.Tag);
            }

            return retList;
        }
        public SubMenuPanel GetSubMenuPanelByFramworkElement(FrameworkElement element)
        {
            TaskbarItem taskbar_item = _items.Where(i => i.FrameworkElement == element && i.ItemType == ItemType.SubMenuButton).SingleOrDefault();

            if (taskbar_item == null)
                return null;

            SubMenuPanel sub_menu_panel = (SubMenuPanel)taskbar_item.Container.Tag;
            return sub_menu_panel;
        }

        public TaskbarItem GetTaskbarItemByTitle(string title)
        {
            TaskbarItem taskbar_item = _items.Where(i => i.Title == title && (i.ItemType == ItemType.SubMenuButton || i.ItemType == ItemType.Button)).SingleOrDefault();

            return taskbar_item;
        }

        public string GetTitleForSubmenuPanel(SubMenuPanel panel)
        {
            TaskbarItem taskbar_item = _items.Where(i => i.Container != null && i.Container.Tag == panel).SingleOrDefault();
            if (taskbar_item == null)
                return string.Empty;

            return taskbar_item.Title;
        }

        public SubMenuPanel GetSubMenuPanelByTitle(string title)
        {
            TaskbarItem taskbar_item = _items.Where(i => i.Title == title && i.ItemType == ItemType.SubMenuButton).SingleOrDefault();

            if (taskbar_item == null)
                return null;

            SubMenuPanel sub_menu_panel = (SubMenuPanel)taskbar_item.Container.Tag;
            return sub_menu_panel;
        }

        public FrameworkElement GetSubMenuContainerByTitle(string title)
        {
            TaskbarItem taskbar_item = _items.Where(i => i.Title == title && i.ItemType == ItemType.SubMenuButton).SingleOrDefault();

            if (taskbar_item == null)
                return null;

            return taskbar_item.Container;
        }

        public Canvas GetTabItemCanvasByTitle(string title)
        {
            TaskbarItem taskbar_item = _items.Where(i => i.Title == title && i.ItemType == ItemType.TabButton).SingleOrDefault();

            if (taskbar_item == null)
                return null;

            Canvas elem_list = (Canvas)taskbar_item.Container.Tag;
            return elem_list;
        }

        public FrameworkElement GetTabContainerByTitle(string title)
        {
            TaskbarItem taskbar_item = _items.Where(i => i.Title == title && i.ItemType == ItemType.TabButton).SingleOrDefault();

            if (taskbar_item == null)
                return null;

            return taskbar_item.Container;
        }

        public void AddTabItemGridByTitle(string title, FrameworkElement g)
        {
            TaskbarItem taskbar_item = _items.Where(i => i.Title == title && i.ItemType == ItemType.TabButton).SingleOrDefault();

            if (taskbar_item == null)
                return;

            tab_item_grid.Children.Clear();
            tab_item_grid.Children.Add(g);
            tab_item_grid.Tag = g;
        }


        #endregion

        #region Add and Remove SubMenu Panels
        protected SubMenuPanel CreateSubMenuPanel(ToggleButton toggle_button)
        {
            SubMenuPanel sub_menu_panel = new SubMenuPanel();
            sub_menu_panel.BaseFrame = BaseFrame;
            sub_menu_panel.ToggleButtonReference = toggle_button;
            sub_menu_panel.FadeOutCompleted += new EventHandler(sub_menu_panel_FadeOutCompleted);
            sub_menu_panel.SetBinding(BorderBrushProperty, new Binding("WindowBorderBrush") { Source = BaseFrame });
            sub_menu_panel.SetBinding(SubMenuPanel.SubMenuIconBgBrushProperty, new Binding("DefaultForegroundBrush") { Source = BaseFrame });
            sub_menu_panel.SetBinding(SubMenuPanel.PanelBackgroundProperty, new Binding("SubMenuPanelBackground") { Source = BaseFrame });
            return sub_menu_panel;
        }

        protected void AddSubMenuPanel(SubMenuPanel sub_menu_panel)
        {

            if (submenu_base_grid.Children.Count > 0)
            {
                SubMenuPanel curr_active_sub_menu_panel = (SubMenuPanel)submenu_base_grid.Children[0];
                if (curr_active_sub_menu_panel != sub_menu_panel)
                {                    
                    curr_active_sub_menu_panel.ToggleButtonReference.IsChecked = false;
                    _tobeadded_panel = sub_menu_panel;                                        
                }
                else
                    sub_menu_panel.StartFadeIn();
            }
            else
            {
                submenu_base_grid.Children.Add(sub_menu_panel);
                submenu_base_grid.SetBinding(HeightProperty, new Binding("ActualHeight") { Source = sub_menu_panel.submenu_border });
                sub_menu_panel.StartFadeIn();
            }
        }

        //protected void AddSubMenuPanel()
        //{
        //}

        //protected void AddSubMenuPanel()
        //{
        //}
        #endregion

        #region Helper Methods
        public Point GetElementPosition(FrameworkElement elem)
        {
            return GetElementPosition(elem, BaseFrame.WindowBorder);
        }

        public Point GetElementPosition(FrameworkElement elem, Visual ref_visual)
        {
            this.UpdateLayout();
            GeneralTransform transform = elem.TransformToAncestor(ref_visual);

            Point rootPoint = transform.Transform(new Point(0, 0));

            Thickness margin = this.Margin;

            rootPoint.X -= margin.Left / 2;
            rootPoint.X += margin.Right / 2;

            rootPoint.Y += margin.Bottom / 2;
            rootPoint.Y -= margin.Top / 2;

            return rootPoint;
        }
        #endregion

        #region Event Handler

        #region SubMenu Panel
        void sub_menu_panel_FadeOutCompleted(object sender, EventArgs e)
        {
            submenu_base_grid.Children.Remove((SubMenuPanel)sender);
            BindingOperations.ClearAllBindings(submenu_base_grid);

            if (_tobeadded_panel != null)
            {
                submenu_base_grid.Children.Add(_tobeadded_panel);
                submenu_base_grid.SetBinding(HeightProperty, new Binding("ActualHeight") { Source = _tobeadded_panel.submenu_border });
                _tobeadded_panel.StartFadeIn();
                _tobeadded_panel = null;
            }
        }

        void stb_container_Unchecked(object sender, RoutedEventArgs e)
        {
            ToggleButton toggle_button = (ToggleButton)sender;
            SubMenuPanel sub_menu_panel = (SubMenuPanel)toggle_button.Tag;
            sub_menu_panel.StartFadeOut();
        }

        void stb_container_Checked(object sender, RoutedEventArgs e)
        {
            ToggleButton toggle_button = (ToggleButton)sender;
            SubMenuPanel sub_menu_panel = (SubMenuPanel)toggle_button.Tag;
            AddSubMenuPanel(sub_menu_panel);
        }
        #endregion

        #region TabContainer
        void tab_container_Unchecked(object sender, RoutedEventArgs e)
        {
            tab_item_grid.Visibility = Visibility.Visible;
            RaiseTapInActiveEvent();
        }

        void tab_container_Checked(object sender, RoutedEventArgs e)
        {
            ToggleButton toggle_button = (ToggleButton)sender;
            Point r_point = UIHelper.GetElementPosition(toggle_button, item_canv);

            //tab_item_grid.Visibility = Visibility.Visible;
            RaiseTapActiveEvent();
            //Canvas.SetLeft(tab_item_grid, r_point.X - toggle_button.ActualWidth + 15);
            //Canvas.SetTop(tab_item_grid, r_point.Y - ((FrameworkElement)tab_item_grid.Tag).Height);

            Canvas.SetLeft(tab_item_grid, r_point.X - tab_item_grid.ActualWidth / 2 + toggle_button.ActualWidth/2);
            Canvas.SetTop(tab_item_grid, r_point.Y - tab_item_grid.ActualHeight);

        }
        #endregion

        void tb_container_Unchecked(object sender, RoutedEventArgs e)
        {
        }

        void tb_container_Checked(object sender, RoutedEventArgs e)
        {
        }

        void b_container_Click(object sender, RoutedEventArgs e)
        {
            Button b_container = (Button)sender;
            TaskbarItem item = (TaskbarItem)b_container.Content;
            if (MenuButtonClick != null)
                MenuButtonClick(sender, new TaskbarButtonClickEventArgs(item.Title));
        }

        #endregion

        #region Properties
        public SubMenuPanel ActiveSubMenuPanel
        {
            get
            {
                if (submenu_base_grid.Children.Count == 0)
                    return null;

                return (SubMenuPanel)submenu_base_grid.Children[0];
            }
        }
        public double BarHeight
        {
            get { return (double)GetValue(BarHeightProperty); }
            set { SetValue(BarHeightProperty, value); }
        }
        public static readonly DependencyProperty BarHeightProperty = DependencyProperty.Register("BarHeight", typeof(double), typeof(WindowTaskBar), new UIPropertyMetadata((double)70));

        public BaseFrame BaseFrame
        {
            get { return _base_frame; }
            set
            {
                _base_frame = value;
            }
        }

        #endregion

        #region RoutedEvents
        public static readonly RoutedEvent TapActiveEvent = EventManager.RegisterRoutedEvent("TapActive", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(WindowTaskBar));

        // Provide CLR accessors for the event
        public event RoutedEventHandler TapActive
        {
            add { AddHandler(TapActiveEvent, value); }
            remove { RemoveHandler(TapActiveEvent, value); }
        }

        // This method raises the Tap event
        void RaiseTapActiveEvent()
        {
            RoutedEventArgs newEventArgs = new RoutedEventArgs(WindowTaskBar.TapActiveEvent);
            RaiseEvent(newEventArgs);
        }

        public static readonly RoutedEvent TapInActiveEvent = EventManager.RegisterRoutedEvent("TapInActive", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(WindowTaskBar));

        // Provide CLR accessors for the event
        public event RoutedEventHandler TapInActive
        {
            add { AddHandler(TapInActiveEvent, value); }
            remove { RemoveHandler(TapInActiveEvent, value); }
        }

        // This method raises the Tap event
        void RaiseTapInActiveEvent()
        {
            RoutedEventArgs newEventArgs = new RoutedEventArgs(WindowTaskBar.TapInActiveEvent);
            RaiseEvent(newEventArgs);
        }
        #endregion

        #region Attributes
        protected List<TaskbarItem> _items;
        public event EventHandler ItemAdded;
        protected BaseFrame _base_frame;
        private SubMenuPanel _tobeadded_panel;
        private SubMenuPanel _tobeshow_panel;

        public event EventHandler<TaskbarButtonClickEventArgs> MenuButtonClick;
        private Storyboard _tap_active_sb = null;
        private Storyboard _tap_inactive_sb = null;

        #endregion

        #region Test Custom Routed Event

        private Storyboard GetTapActiveStoryboard()
        {
            if(_tap_active_sb == null)
            {
                _tap_active_sb = new Storyboard();

                DoubleAnimation anim1 = new DoubleAnimation(0, 1, new Duration(TimeSpan.FromMilliseconds(400)));
                anim1.AccelerationRatio = 0.7;
                anim1.DecelerationRatio = 0.3;
                Storyboard.SetTargetName(anim1, "tab_item_grid");
                Storyboard.SetTargetProperty(anim1, new PropertyPath("(RenderTransform).(ScaleTransform.ScaleX)"));

                DoubleAnimation anim2 = new DoubleAnimation(0, 1, new Duration(TimeSpan.FromMilliseconds(400)));
                anim2.AccelerationRatio = 0.7;
                anim2.DecelerationRatio = 0.3;
                Storyboard.SetTargetName(anim2, "tab_item_grid");
                Storyboard.SetTargetProperty(anim2, new PropertyPath("(RenderTransform).(ScaleTransform.ScaleY)"));

                _tap_active_sb.Children.Add(anim1);
                _tap_active_sb.Children.Add(anim2);
            }

            return _tap_active_sb;
        }

        private Storyboard GetTapInActiveStoryboard()
        {
            if (_tap_inactive_sb == null)
            {
                _tap_inactive_sb = new Storyboard();

                DoubleAnimation anim1 = new DoubleAnimation(1, 0, new Duration(TimeSpan.FromMilliseconds(400)));
                anim1.AccelerationRatio = 0.3;
                anim1.DecelerationRatio = 0.7;
                Storyboard.SetTargetName(anim1, "tab_item_grid");
                Storyboard.SetTargetProperty(anim1, new PropertyPath("(RenderTransform).(ScaleTransform.ScaleX)"));

                DoubleAnimation anim2 = new DoubleAnimation(1, 0, new Duration(TimeSpan.FromMilliseconds(400)));
                anim2.AccelerationRatio = 0.3;
                anim2.DecelerationRatio = 0.7;
                Storyboard.SetTargetName(anim2, "tab_item_grid");
                Storyboard.SetTargetProperty(anim2, new PropertyPath("(RenderTransform).(ScaleTransform.ScaleY)"));

                _tap_inactive_sb.Children.Add(anim1);
                _tap_inactive_sb.Children.Add(anim2);
            }

            return _tap_inactive_sb;
        }

        void WindowTaskBar_TapInActive(object sender, RoutedEventArgs e)
        {
            Storyboard sb_inactive = GetTapInActiveStoryboard();
            if (sb_inactive != null)
                this.BeginStoryboard(sb_inactive, HandoffBehavior.Compose);
        }

        void WindowTaskBar_TapActive(object sender, RoutedEventArgs e)
        {
            Storyboard sb_active = GetTapActiveStoryboard();
            if (sb_active != null)
            {
                this.BeginStoryboard(sb_active, HandoffBehavior.Compose);
            }
        }

        private void DoubleAnimation_Completed(object sender, EventArgs e)
        {

        }

        private void DoubleAnimation_Completed_1(object sender, EventArgs e)
        {

        }
        #endregion


    }
}
