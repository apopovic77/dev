﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace Logicx.WpfUtility.CustomControls
{
    public class WrapPanelEx : WrapPanel
    {
        #region Construction and Initialization
        /// <summary>
        /// Static constructor
        /// </summary>
        static WrapPanelEx()
        {
            FrameworkPropertyMetadata metadataLinebreakBefore = new FrameworkPropertyMetadata(false);
            LineBreakBeforeProperty = DependencyProperty.RegisterAttached("LineBreakBefore",
                                                            typeof(bool), typeof(WrapPanelEx), metadataLinebreakBefore);

        }
        /// <summary>
        /// Default constructor for design support
        /// </summary>
        public WrapPanelEx()
        {
        }

        #endregion

        #region Operations

        public static bool GetLineBreakBefore(UIElement element)
        {
            return (bool) element.GetValue(LineBreakBeforeProperty);
        }

        public static void SetLineBreakBefore(UIElement element, bool value)
        {
            element.SetValue(LineBreakBeforeProperty, value);
        }

        protected override Size MeasureOverride(Size constraint)
        {
            
            return base.MeasureOverride(constraint);
        }

        protected override Size ArrangeOverride(Size finalSize)
        {
            

            Size sizeSoFar = new Size(0, 0);

            foreach (UIElement child in Children)
            {
                if(GetLineBreakBefore(child))
                {
                    // line break;
                    sizeSoFar.Height += child.DesiredSize.Height;
                    sizeSoFar.Width = 0;
                }

                child.Arrange(new Rect(sizeSoFar.Width, sizeSoFar.Height,
                                       child.DesiredSize.Width, child.DesiredSize.Height));

                if (sizeSoFar.Width + child.DesiredSize.Width >= finalSize.Width)
                {
                    sizeSoFar.Height += child.DesiredSize.Height;
                    sizeSoFar.Width = 0;
                }
                else
                {
                    sizeSoFar.Width += child.DesiredSize.Width;
                }
            }

            return finalSize;


            //return base.ArrangeOverride(finalSize);
        }

        #endregion

        #region Attribs

        #region Dependency properties
        public static readonly DependencyProperty LineBreakBeforeProperty;
        #endregion

        #endregion
    }
}
