using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace Logicx.JabberLib
{
    public enum PresenceState { 
        Online = 1,
        Offline = 2,
        Invisible = 3,
        Away = 4
    }

    public abstract class MessagingClient
    {
        #region ServerStuff Server & Connection Settings
        private string m_Username="";
        private string m_Password="";
        private string m_Server="";
        private int m_Port;
        
        public string Username
        {
            set { m_Username = value; }
            get { return m_Username; }
        }
        public string Password
        {
            set { m_Password = value; }
            get { return m_Password; }
        }
        public string Server
        {
            set { m_Server = value; }
            get { return m_Server; }
        }
        public int Port
        {
            set { m_Port = value; }
            get { return m_Port; }
        }
        #endregion

        protected ArrayList m_contactlist = new ArrayList();
        protected Hashtable m_precensestate = new Hashtable();

        public abstract bool Connect();
        public abstract void Disconnect();

        public abstract void OnMessage(string msg,string from);
        public abstract PresenceState GetPrecenseState(string from);
        public abstract bool SendMessage(string msg,string to);
        public abstract bool IsInContactList(string name);
        public abstract void AddContact(string name);
        public abstract void AuthSubscription(string name);
        
    }
}
