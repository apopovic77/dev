﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GeneticProgrammingLib.FindSentence
{
    public class SentenceDna : Dna<char>
    {
        public SentenceDna()
        {
        }

        public SentenceDna(string init_sentence)
        {
            InitializeSentence(init_sentence);
        }

        public override void SimulationUpdate(int frame)
        {
        }

        public override void CalcFitness()
        {
            //find similar letters in target sentence
            int similar_count = 0;
            for (int i = 0; i < _genes.Count; i++)
                if (Equals(_genes[i], GeneticTarget.Genes[i]))
                    similar_count++;

            //calculate fitness for this dna
            Fitness = Math.Pow(similar_count, 4);
            FitnessProbality = similar_count / (double)_genes.Count;
        }

        public override void InitializeRandomGen()
        {
            _genes = new List<char>(GeneticTarget.Genes.Count);
            for (int i = 0; i < GeneticTarget.Genes.Count; i++)
            {
                _genes.Add((char)_r.Next(32, 122));
            }
        }

        public override void Mutate(int i)
        {
            _genes[i] = (char)_r.Next(32, 122);
        }


        public void InitializeSentence(string sentence)
        {
            _genes = sentence.ToCharArray().ToList();
        }

        public override string ToString()
        {
            return new string(_genes.ToArray()) + " [" + Fitness + "]";
        }

        private static Random _r = new Random();
        public static SentenceDna GeneticTarget;



    }
}
