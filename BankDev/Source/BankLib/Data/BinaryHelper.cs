﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankLib.Data
{
    public class BinaryHelper
    {
        public static void WriteString(BinaryWriter writer, string s)
        {
            if (s == null)
            {
                writer.Write(false);
                return;
            }

            writer.Write(true);
            writer.Write(s);
        }
        public static string ReadString(BinaryReader reader)
        {
            if (reader.ReadBoolean())
                return reader.ReadString();
            return null;
        }

        public static void WriteNullableDateTime(BinaryWriter writer, DateTime? date)
        {
            if (date == null)
            {
                writer.Write(false);
                return;
            }

            writer.Write(true);
            writer.Write(date.Value.ToBinary());
        }
        public static DateTime? ReadNullableDateTime(BinaryReader reader)
        {
            if(reader.ReadBoolean())
                return DateTime.FromBinary(reader.ReadInt64());
            return null;
        }

    }
}
