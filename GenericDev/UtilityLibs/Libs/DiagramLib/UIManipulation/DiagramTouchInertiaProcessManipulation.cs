﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using Logicx.DiagramLib.Scales;
using Logicx.WpfUtility.UiManipulation;

namespace Logicx.DiagramLib.UIManipulation
{
    public class DiagramTouchInertiaProcessManipulation : TouchInertiaProcessManipulation
    {
        public DiagramTouchInertiaProcessManipulation(UIElement ui_element, Canvas canvas, UIElement event_base, EnabledAxes enabled_axes,  
            ScaleControlBase x_axis_control, ScaleControlBase y_axis_control, bool can_move, bool can_resize_x, bool can_resize_y)
            : base(ui_element, canvas, event_base, enabled_axes, false)
        {
            //_with_scaling = false;
            _with_rotation = false;
            XAxisControl = x_axis_control;
            YAxisControl = y_axis_control;
            CanMove = can_move;
            CanResizeX = can_resize_x;
            CanResizeY = can_resize_y;
            EnableInertiaAnimation = false;
        }

        
        public ScaleControlBase XAxisControl
        {
            get { return _x_axis_control; }
            set { _x_axis_control = value; }
        }

        public ScaleControlBase YAxisControl
        {
            get { return _y_axis_control; }
            set { _y_axis_control = value; }
        }

        public bool CanMove
        {
            get;
            set;
        }

        public bool CanResizeX
        {
            get;
            set;
        }

        public bool CanResizeY
        {
            get;
            set;
        }

        public override void ProcessManipulationDelta(object sender, Windows7.Multitouch.Manipulation.ManipulationDeltaEventArgs e)
        {
            if (XAxisControl == null || YAxisControl == null)
                return;

            //double left = Canvas.GetLeft(_ui_element);
            //double top = Canvas.GetTop(_ui_element);

            //if (double.IsNaN(left))
            //    left = 0;
            //if (double.IsNaN(top))
            //    top = 0;

            if (CanMove)
            {
                //if (_enabled_axes == EnabledAxes.XAxis || _enabled_axes == EnabledAxes.All)
                //    Canvas.SetLeft(_ui_element, left + e.TranslationDelta.Width);
                //if (_enabled_axes == EnabledAxes.YAxis || _enabled_axes == EnabledAxes.All)
                //    Canvas.SetTop(_ui_element, top + e.TranslationDelta.Height);

                Debug.WriteLine("TOUCH translation width: " + e.TranslationDelta.Width);
                Debug.WriteLine("TOUCH translation height: " + e.TranslationDelta.Height);
            }

            if (_with_rotation)
            {
                RotateTransform.Angle += e.RotationDelta * 180 / Math.PI;
            }

            if (_with_scaling && (CanResizeX || CanResizeY))
            {
                if(CanResizeX)
                    ScaleTransform.ScaleX *= e.ScaleDelta;
                if(CanResizeY)
                    ScaleTransform.ScaleY *= e.ScaleDelta;

                Debug.WriteLine("TOUCH translation SCALE: " + e.ScaleDelta);
            }
        }

        
        private ScaleControlBase _x_axis_control;
        private ScaleControlBase _y_axis_control;
        
    }
}
