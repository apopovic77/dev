﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using Logicx.DiagramLib.Controls.MenuItemControls;
using Logicx.DiagramLib.Diagrams;
using Logicx.DiagramLib.Resources.Converter;
using Logicx.DiagramLib.Scales;
using Logicx.DiagramLib.UIManipulation;
using Logicx.DiagramLib.ViewConfig;
using Logicx.IaApi.Interaction.Overlay.Management;
using Logicx.Utilities;
using Logicx.WpfUtility.CustomControls.OverlayManager;
using Logicx.WpfUtility.WpfHelpers;
using Windows7.Multitouch;
using Timer = System.Timers.Timer;

namespace Logicx.DiagramLib.Controls
{
    public class DiagramManager : IDisposable
    {

        #region Constants
        private const int RESET_POSITION_ANIMATION_TIME_IN_MS = 300;
        #endregion

        #region Construction and Initialization

        private static bool? _is_multitouch_enabled = null;

        internal static bool IsMultiTouchEnabled
        {
            get
            {
                if (!_is_multitouch_enabled.HasValue)
                    _is_multitouch_enabled = TouchHandler.DigitizerCapabilities.IsMultiTouchReady;

                return _is_multitouch_enabled.Value;
            }
        }

        /// <summary>
        /// Constructs the manager.
        /// </summary>
        /// <param name="base_canvas"></param>
        /// <param name="submenu_panel"></param>
        /// <param name="overlay_canvas"></param>
        /// <param name="base_frame"></param>
        public DiagramManager(Canvas base_canvas, SubMenuPanel submenu_panel, Canvas overlay_canvas, BaseFrame base_frame)
        {
            if (base_canvas == null)
                throw new ArgumentNullException("base_canvas");
            if (submenu_panel == null)
                throw new ArgumentNullException("submenu_panel");
            if (overlay_canvas == null)
                throw new ArgumentNullException("overlay_canvas");
            if (base_frame == null)
                throw new ArgumentNullException("base_frame");

            _base_canvas = base_canvas;
            _base_canvas.SizeChanged += new SizeChangedEventHandler(_base_canvas_SizeChanged);
            _sub_menu_panel = submenu_panel;
            _overlay_canvas = overlay_canvas;
            _base_frame = base_frame;
            _diagram_panel_name = submenu_panel.TaskbarItemName;

            // special overlay canvas
            _x_scale_canvas = new SpecialScaleCanvas();
            _x_scale_canvas.MouseDown += new MouseButtonEventHandler(_x_scale_canvas_MouseDown);
            _x_scale_canvas.MouseMove += new MouseEventHandler(_x_scale_canvas_MouseMove);
            _x_scale_canvas.MouseUp += new MouseButtonEventHandler(_x_scale_canvas_MouseUp);
            _x_scale_canvas.MouseLeave += new MouseEventHandler(_x_scale_canvas_MouseLeave);
            _x_scale_canvas.MouseWheel += new MouseWheelEventHandler(_x_scale_canvas_MouseWheel);
            _x_scale_canvas.Background = Brushes.Transparent; // new SolidColorBrush(Color.FromArgb(0x3a, 0xef, 0x20, 0xe4));

            if (SpecialScaleCanvas.IsMultiTouchEnabled)
            {
                _x_scale_touch_intertia_manipulator = new DiagramScaleTouchInertiaProcessManipulation(_x_scale_canvas, _x_scale_canvas, _x_scale_canvas, WpfUtility.UiManipulation.UiManipulator.EnabledAxes.XAxis);
                _x_scale_touch_intertia_manipulator.UiManipulationStarted += new EventHandler<WpfUtility.UiManipulation.UiManipulationEventArgs>(_x_scale_touch_intertia_manipulator_UiManipulationStarted);
                _x_scale_touch_intertia_manipulator.UiManipulationInertiaCompleted += new EventHandler<WpfUtility.UiManipulation.UiManipulationEventArgs>(_x_scale_touch_intertia_manipulator_UiManipulationInertiaCompleted);
                _x_scale_touch_intertia_manipulator.TranslationChanged += new EventHandler<DiagramScaleTouchInertiaProcessManipulation.DiagrammScaleMoveEventArgs>(_x_scale_touch_intertia_manipulator_TranslationChanged);
                _x_scale_touch_intertia_manipulator.ScalingChanged += new EventHandler<DiagramScaleTouchInertiaProcessManipulation.DiagramScaleScaleingEventArgs>(_x_scale_touch_intertia_manipulator_ScalingChanged);
            }

            _x_scale_canvas.MinHeight = 50;

            Binding slide_width_binding = new Binding("ActualWidth");
            slide_width_binding.Source = _base_canvas;
            _x_scale_canvas.SetBinding(Canvas.WidthProperty, slide_width_binding);

            MultiBinding canvas_top_binding = new MultiBinding();

            Binding base_canvas_actual_height = new Binding("ActualHeight");
            base_canvas_actual_height.Source = _base_canvas;

            Binding x_scale_canvas_height = new Binding("ActualHeight");
            x_scale_canvas_height.Source = _x_scale_canvas;

            canvas_top_binding.Bindings.Add(base_canvas_actual_height);
            canvas_top_binding.Bindings.Add(x_scale_canvas_height);

            canvas_top_binding.Converter = new CanvasAlignBottomConverter();
            _x_scale_canvas.SetBinding(Canvas.TopProperty, canvas_top_binding);

            _base_canvas.Children.Add(_x_scale_canvas);


            // a canvas for wheel events
            //_x_scalue_zoom_canvas = new Canvas();
            //_x_scalue_zoom_canvas.MouseUp += new MouseButtonEventHandler(_x_scalue_zoom_canvas_MouseUp);
            //_x_scalue_zoom_canvas.MouseWheel += new MouseWheelEventHandler(_x_scale_zoom_canvas_MouseWheel);
            //_x_scalue_zoom_canvas.IsHitTestVisible = true;
            //_x_scalue_zoom_canvas.MinHeight = 50;
            //_x_scalue_zoom_canvas.Background = Brushes.Transparent; // new SolidColorBrush(Color.FromArgb(0x3a, 0xef, 0x20, 0xe4));

            //Canvas.SetZIndex(_x_scalue_zoom_canvas, -1);

            //slide_width_binding = new Binding("ActualWidth");
            //slide_width_binding.Source = _base_canvas;
            //_x_scalue_zoom_canvas.SetBinding(Canvas.WidthProperty, slide_width_binding);

            //canvas_top_binding = new MultiBinding();

            //base_canvas_actual_height = new Binding("ActualHeight");
            //base_canvas_actual_height.Source = _base_canvas;

            //x_scale_canvas_height = new Binding("ActualHeight");
            //x_scale_canvas_height.Source = _x_scalue_zoom_canvas;

            //canvas_top_binding.Bindings.Add(base_canvas_actual_height);
            //canvas_top_binding.Bindings.Add(x_scale_canvas_height);

            //canvas_top_binding.Converter = new CanvasAlignBottomConverter();
            //_x_scalue_zoom_canvas.SetBinding(Canvas.TopProperty, canvas_top_binding);

            //_base_canvas.Children.Add(_x_scalue_zoom_canvas);

            Canvas.SetZIndex(_x_scale_canvas, 999999);
        }


        #endregion

        #region Operations

        private void AdjustDiagramSlider()
        {
            if (_current_base_control == null)
            {
                _x_scale_canvas.IsHitTestVisible = false;
                //_x_scale_canvas.Visibility = Visibility.Collapsed;
            }
            else
            {
                bool can_scroll_diagrams = CurrentDiagramCanScrollDiagrams();
                bool can_zoom_x_axis = CurrentDiagramCanZoomXAxis();

                if (can_scroll_diagrams || can_zoom_x_axis)
                {
                    _x_scale_canvas.IsHitTestVisible = true;
                    //_x_scale_canvas.Visibility = Visibility.Visible;

                    double current_height = CurrentXAxisHeight();

                    if (current_height > _x_scale_canvas.MinHeight)
                    {
                        _x_scale_canvas.Height = current_height;
                    }
                    else
                    {
                        _x_scale_canvas.Height = _x_scale_canvas.MinHeight;
                    }
                }
                else
                {
                    _x_scale_canvas.IsHitTestVisible = false;
                    //_x_scale_canvas.Visibility = Visibility.Collapsed;
                }
            }
        }

        /// <summary>
        /// Adds the given control to the list of diagrams. If the list of diagrams is empty the diagram 
        /// is added to the base_grid. Otherwise a submenu item is created and added to the submenu panel.
        /// Note that the type of the user control is checked.
        /// </summary>
        /// <param name="control"></param>
        /// <param name="title"></param>
        /// <param name="is_hidden"></param>
        public void AddDiagram(UserControl control, string title, bool is_hidden)
        {
            if (control is DiagramDateTimeInt || control is DiagramDiscreteDouble || control is DiagramIntDateTime || control is DiagramDiscreteInt)
            {
                bool adjust_scroll_diagrams = false;

                SetDiagramManager(control);
                OnDiagramAdded(control, false);

                if (control is INotifyPropertyChanged)
                    ((INotifyPropertyChanged)control).PropertyChanged += new PropertyChangedEventHandler(DiagramManager_Diagram_PropertyChanged);

                DiagramMetaInfo meta_info;

                if (_diagrams.Count == 0)
                {
                    meta_info = new DiagramMetaInfo(control, title, is_hidden);
                    OnBeforeBaseDiagramChange(_current_base_control, meta_info);
                    _current_base_control = meta_info;
                    _base_canvas.Children.Insert(0, control);
                    SetDiagramProperty("IsMinimized", false, meta_info.Diagram);
                    //OnDiagramAdded(control, true);

                    adjust_scroll_diagrams = true;
                    OnBaseDiagramChanged(_current_base_control);
                }
                else
                {
                    //string title_text = GetTitleText(control);
                    //SubMenuPanelMenuDiagramItem item = new SubMenuPanelMenuDiagramItem(title_text, control);
                    //item.Key = title_text;
                    //item.IsActive = false;
                    //_submenu_panel_dictionary.Add(control, item);

                    SubMenuPanelMenuDiagramItem item = null;
                    if (!is_hidden)
                    {

                        //string title_text = GetTitleText(control);
                        item = new SubMenuPanelMenuDiagramItem(title, control, GetMinimizedLegendControl(control), true);
                        item.Key = title;
                        item.IsActive = false;
                        SetDiagramProperty("IsMinimized", true, control);
                        _sub_menu_panel.AddItem(item);
                    }

                    meta_info = new DiagramMetaInfo(control, title, item, is_hidden);
                    //OnDiagramAdded(control, false);
                }

                InitMoveHandlers(control);

                _diagrams.Add(meta_info);

                if (adjust_scroll_diagrams)
                {
                    GenerateScrollDiagrams();
                    AdjustDiagramSlider();
                }
            }
            else
            {
                throw new ArgumentException("Type " + control.GetType() + " is not supported!");
            }
        }

        private void InitMoveHandlers(UserControl control)
        {
            if (control is DiagramDateTimeInt)
            {
                ((DiagramDateTimeInt)control).BeginObjectMove += new EventHandler(DiagramManager_BeginObjectMove);
                ((DiagramDateTimeInt)control).EndObjectMove += new EventHandler(DiagramManager_EndObjectMove);
            }
            else if (control is DiagramDiscreteDouble)
            {
                ((DiagramDiscreteDouble)control).BeginObjectMove += new EventHandler(DiagramManager_BeginObjectMove);
                ((DiagramDiscreteDouble)control).EndObjectMove += new EventHandler(DiagramManager_EndObjectMove);
            }
            else if (control is DiagramDiscreteInt)
            {
                ((DiagramDiscreteInt)control).BeginObjectMove += new EventHandler(DiagramManager_BeginObjectMove);
                ((DiagramDiscreteInt)control).EndObjectMove += new EventHandler(DiagramManager_EndObjectMove);
            }
            else if (control is DiagramIntDateTime)
            {
                ((DiagramIntDateTime)control).BeginObjectMove += new EventHandler(DiagramManager_BeginObjectMove);
                ((DiagramIntDateTime)control).EndObjectMove += new EventHandler(DiagramManager_EndObjectMove);
            }
        }

        private void DeregisterMoveHandlers(UserControl control)
        {
            if (control is DiagramDateTimeInt)
            {
                ((DiagramDateTimeInt)control).BeginObjectMove -= new EventHandler(DiagramManager_BeginObjectMove);
                ((DiagramDateTimeInt)control).EndObjectMove -= new EventHandler(DiagramManager_EndObjectMove);
            }
            else if (control is DiagramDiscreteDouble)
            {
                ((DiagramDiscreteDouble)control).BeginObjectMove -= new EventHandler(DiagramManager_BeginObjectMove);
                ((DiagramDiscreteDouble)control).EndObjectMove -= new EventHandler(DiagramManager_EndObjectMove);
            }
            else if (control is DiagramDiscreteInt)
            {
                ((DiagramDiscreteInt)control).BeginObjectMove -= new EventHandler(DiagramManager_BeginObjectMove);
                ((DiagramDiscreteInt)control).EndObjectMove -= new EventHandler(DiagramManager_EndObjectMove);
            }
            else if (control is DiagramIntDateTime)
            {
                ((DiagramIntDateTime)control).BeginObjectMove -= new EventHandler(DiagramManager_BeginObjectMove);
                ((DiagramIntDateTime)control).EndObjectMove -= new EventHandler(DiagramManager_EndObjectMove);
            }
        }

        public UserControl GetDiagram(Type type)
        {
            return _diagrams.Where(o => o.Diagram.GetType() == type).FirstOrDefault().Diagram;
        }

        protected virtual void OnDiagramAdded(UserControl control, bool in_main_area)
        {

        }

        protected void DiagramManagerFilterChanged(object argument)
        {
            foreach (DiagramMetaInfo current_meta in _diagrams)
            {
                SendDiagramManagerFilterChanged(current_meta.Diagram, argument);
            }

            foreach (DiagramMetaInfo current_meta in _left_diagrams)
            {
                SendDiagramManagerFilterChanged(current_meta.Diagram, argument);
            }

            foreach (DiagramMetaInfo current_meta in _right_diagrams)
            {
                SendDiagramManagerFilterChanged(current_meta.Diagram, argument);
            }
        }

        public void KeyDownPressed(object sender, KeyEventArgs e)
        {
            if (_current_base_control != null)
            {
                Type current_base_type = _current_base_control.Diagram.GetType();

                MethodInfo mi_abort = current_base_type.GetMethod("KeyDownPressed");
                if (mi_abort != null)
                {
                    mi_abort.Invoke(_current_base_control.Diagram, new object[] { sender, e, _in_x_scale_zoom_mode });
                }
            }
        }

        public Point GetElementPositionInSubmenuPanel(FrameworkElement element)
        {
            if (_base_frame.TaskBar.ActiveSubMenuPanel == null || _base_frame.TaskBar.ActiveSubMenuPanel.TaskbarItemName != _diagram_panel_name)
                return new Point();

            return _base_frame.OverlayManager.WindowTaskBar.GetElementPosition(element, _base_frame);
        }

        public Point GetElementPosition(FrameworkElement element)
        {
            return _base_frame.OverlayManager.WindowTaskBar.GetElementPosition(element, _base_frame);
        }

        protected virtual void OnBeforeBaseDiagramChange(DiagramMetaInfo current_diagram_meta_info, DiagramMetaInfo new_base_diagram_meta_info)
        {
            FinishOldDiagramActivities();
        }

        protected virtual void OnBaseDiagramChanged(DiagramMetaInfo new_base_diagram_meta_info)
        {
            if (new_base_diagram_meta_info != null && new_base_diagram_meta_info.Diagram is INotifyPropertyChanged)
                ((INotifyPropertyChanged)new_base_diagram_meta_info.Diagram).PropertyChanged += new PropertyChangedEventHandler(news_base_diagram_property_changed_PropertyChanged);
        }

        void news_base_diagram_property_changed_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "HasLoadedData")
            {
                if (GetHasLoadedData(sender as UserControl))
                {
                    ((INotifyPropertyChanged)sender).PropertyChanged -= news_base_diagram_property_changed_PropertyChanged;

                    if (sender == _current_base_control)
                        ForceRequestSwapDiagramConfigs();
                }
            }
        }

        public void SetNewBaseDiagram(Type diagram_type, bool animated)
        {
            DiagramMetaInfo diagram_meta_info = _diagrams.FirstOrDefault(o => o.Diagram.GetType() == diagram_type && o.SubMenuPanelMenuDiagramItem != null);

            if (diagram_meta_info == null)
                return;

            if (!diagram_meta_info.Diagram.Equals(_current_base_control.Diagram))
                SetNewBaseDiagram(diagram_meta_info.SubMenuPanelMenuDiagramItem, animated);
        }

        /// <summary>
        /// Switches the given diagram from the current position in the submenu panel to the base grid.
        /// </summary>
        /// <param name="item"></param>
        /// <param name="overlay_manager"></param>
        /// <param name="animated"></param>
        public void SetNewBaseDiagram(SubMenuPanelMenuDiagramItem item, bool animated)
        {
            DiagramMetaInfo diagram_meta_info =
                _diagrams.Where(o => o.SubMenuPanelMenuDiagramItem != null && o.SubMenuPanelMenuDiagramItem.Equals(item))
                    .FirstOrDefault();

            //if (!_submenu_panel_dictionary.Values.Contains(item))
            if (diagram_meta_info == null)
                throw new ArgumentException("The item with the key '" + item.Key + "' does not exist in the list of submenu items!");



            if (animated)
                SwitchDiagramsAnimated(item);
            else
                SwitchDiagramsImmediately(item);

            //OnBaseDiagramChanged(diagram_meta_info);
        }

        public void FinishOldDiagramActivities()
        {
            if (_current_base_control != null && _current_base_control.Diagram != null)
            {
                if (_current_base_control.Diagram is DiagramDateTimeInt)
                {
                    ((DiagramDateTimeInt)_current_base_control.Diagram).FinishDiagramActivities();
                }
                else if (_current_base_control.Diagram is DiagramDiscreteDouble)
                {
                    ((DiagramDiscreteDouble)_current_base_control.Diagram).FinishDiagramActivities();
                }
                else if (_current_base_control.Diagram is DiagramDiscreteInt)
                {
                    ((DiagramDiscreteInt)_current_base_control.Diagram).FinishDiagramActivities();
                }
                else if (_current_base_control.Diagram is DiagramIntDateTime)
                {
                    ((DiagramIntDateTime)_current_base_control.Diagram).FinishDiagramActivities();
                }
            }
        }

        public void SwitchIntegratedDiagrams(UserControl new_control, FrameworkElement zoom_element, bool animated, Point destination_point, double loading_size)
        {
            if (animated)
                SwitchIntegratedDiagramsAnimatedNew(new_control, zoom_element, destination_point, loading_size, true);
            else
                SwitchIntegratedDiagramsImmediately(new_control);
        }

        public void SwitchSubmenuPanelDiagrams(UserControl new_control, FrameworkElement zoom_element, bool animated, Point destination_point, double loading_size)
        {
            if (animated)
                //SwitchIntegratedDiagramsAnimated(new_control, zoom_element, destination_point);
                SwitchIntegratedDiagramsAnimatedNew(new_control, zoom_element, destination_point, loading_size, false);
            else
                SwitchIntegratedDiagramsImmediately(new_control);
        }

        private void SwitchDiagramsAnimated(SubMenuPanelMenuDiagramItem item)
        {
            DiagramMetaInfo diagram_meta_info_new = _diagrams.Where(o => o.Diagram.Equals(item.UserControl)).First();
            if (diagram_meta_info_new == null)
                throw new ArgumentException("Diagram not found!");

            _animated_control_small_to_big = diagram_meta_info_new;
            _animated_control_big_to_small = _current_base_control; // ui_element as UserControl;

            SetDiagramProperty("IsMinimized", false, _animated_control_small_to_big.Diagram);

            //calculate animation data
            _animated_image_small_to_big = CreateBitmap(_animated_control_small_to_big.Diagram);
            _overlay_canvas.Children.Add(_animated_image_small_to_big);

            _animated_image_big_to_small = CreateBitmap(_animated_control_big_to_small.Diagram);
            _overlay_canvas.Children.Add(_animated_image_big_to_small);

            Point root_point_sub_start = GetElementPositionInSubmenuPanel(item.UserControl);
            Point root_point_base_grid = GetElementPosition(_animated_control_big_to_small.Diagram);
            double destination_width, destination_height;
            Point root_point_sub_end;
            if (_animated_control_big_to_small.IsHidden)
            {
                root_point_sub_end =
                    new Point(root_point_base_grid.X + _animated_control_big_to_small.Diagram.ActualWidth,
                              root_point_base_grid.Y + _animated_control_big_to_small.Diagram.ActualHeight);
                double relation = _animated_control_big_to_small.Diagram.ActualWidth /
                                  _animated_control_big_to_small.Diagram.ActualHeight;
                destination_width = 10 * relation;
                destination_height = 10;
            }
            else
                root_point_sub_end = GetRightMostPoint(out destination_width, out destination_height, false);

            double scale_width = destination_width / _base_canvas.ActualWidth;
            double scale_height = destination_height / _base_canvas.ActualHeight;
            double scale_submenu = scale_width < scale_height ? scale_width : scale_height;
            const double scale_base_grid = 1;

            // remove from old controls
            _base_canvas.Children.Remove(_animated_control_big_to_small.Diagram);
            _sub_menu_panel.RemoveItem(item);
            item.ResetControls(); // reset viewbox and minimized info

            OnBeforeBaseDiagramChange(_current_base_control, _animated_control_small_to_big);
            _current_base_control = _animated_control_small_to_big;

            //Debug.WriteLine("SwitchDiagramsAnimated");
            // start animation
            Storyboard sb_1 = OverlayWindowManager.GetDefaultTranslateScaleAnimation(root_point_sub_start, root_point_base_grid, true, scale_submenu, scale_submenu, scale_base_grid, scale_base_grid, false, 1);
            sb_1.Completed += new EventHandler(sb_Completed_1);

            Storyboard sb_2 = OverlayWindowManager.GetDefaultTranslateScaleAnimation(root_point_base_grid, root_point_sub_end, true, scale_base_grid, scale_base_grid, scale_submenu, scale_submenu, false, 1);
            sb_2.Completed += new EventHandler(sb_Completed_2);

            _anim_1_complete = false;
            _anim_2_complete = false;

            _sub_menu_panel.IsHitTestVisible = false;
            _animated_image_small_to_big.BeginStoryboard(sb_1);
            _animated_image_big_to_small.BeginStoryboard(sb_2);
            //GenerateScrollDiagrams();
            //AdjustDiagramSlider();
        }

        private Image CreateBitmap(FrameworkElement framework_element)
        {
            BitmapImage bitmap = UserControlConverterFunctions.GenerateBitmapImage(framework_element);
            Image image = new Image();
            image.Source = bitmap;

            ScaleTransform scale_transform = new ScaleTransform();
            image.RenderTransform = scale_transform;

            return image;
        }

        private Point GetRightMostPoint(out double width, out double height, bool one_more_than_last)
        {
            Point point = default(Point);
            Point last_point = default(Point);
            double calculated_width = 0;
            double calculated_height = 0;

            foreach (var item in _sub_menu_panel.Items)
            {
                SubMenuPanelMenuDiagramItem diagram = item as SubMenuPanelMenuDiagramItem;
                if (diagram != null)
                {
                    Point root_point_1 = GetElementPositionInSubmenuPanel(diagram.UserControl);
                    if (root_point_1.X >= point.X)
                    {
                        if (point != default(Point))
                            last_point = point;
                        point = root_point_1;
                    }

                    calculated_width = diagram.ActualWidth;
                    calculated_height = diagram.ActualHeight;
                }
            }

            width = calculated_width;
            height = calculated_height;

            if (one_more_than_last && last_point != point)
                point = new Point(point.X + (point.X - last_point.X), point.Y);

            return point;
        }



        private void SwitchDiagramsImmediately(SubMenuPanelMenuDiagramItem item)
        {
            DiagramMetaInfo diagram_meta_info_new = _diagrams.Where(o => o.Diagram.Equals(item.UserControl)).First();
            DiagramMetaInfo big_control = _current_base_control; // ui_element as UserControl;
            if (diagram_meta_info_new == null)
                throw new ArgumentException("Diagram not found!");

            if (big_control == null)
                throw new ArgumentException("Invalid type in base grid");

            OnBeforeBaseDiagramChange(_current_base_control, diagram_meta_info_new);

            // remove from old controls
            _base_canvas.Children.Remove(big_control.Diagram);
            _sub_menu_panel.RemoveItem(item);
            _current_base_control = diagram_meta_info_new;

            _base_canvas.Children.Insert(0, item.UserControl);
            item.ResetControls(); // reset viewbox and minimized info

            SetDiagramProperty("IsMinimized", false, diagram_meta_info_new.Diagram);

            if (!big_control.IsHidden)
            {
                //string title = GetTitleText(big_control.Diagram);
                SubMenuPanelMenuDiagramItem new_item = new SubMenuPanelMenuDiagramItem(big_control.Title, big_control.Diagram, GetMinimizedLegendControl(big_control.Diagram), true);
                new_item.Key = big_control.Title;
                new_item.IsActive = false;
                big_control.SubMenuPanelMenuDiagramItem = new_item;
                SetDiagramProperty("IsMinimized", true, big_control.Diagram);
                _sub_menu_panel.AddItem(new_item);
            }

            GenerateScrollDiagrams();
            AdjustDiagramSlider();

            OnBaseDiagramChanged(_current_base_control);
        }

        private void SwitchIntegratedDiagramsAnimated(UserControl new_control, FrameworkElement zoom_element, Point destination_point)
        {
            DiagramMetaInfo diagram_meta_info_new = _diagrams.Where(o => o.Diagram.Equals(new_control)).First();
            if (diagram_meta_info_new == null)
                throw new ArgumentException("Diagram not found!");

            _animated_control_small_to_big = diagram_meta_info_new;
            _animated_control_big_to_small = _current_base_control; // ui_element as UserControl;

            SetDiagramProperty("IsMinimized", false, _animated_control_small_to_big.Diagram);

            //calculate animation data
            _animated_image_big_to_small = CreateBitmap(_animated_control_big_to_small.Diagram);
            _overlay_canvas.Children.Add(_animated_image_big_to_small);

            _animated_image_small_to_big = CreateBitmap(zoom_element);
            _overlay_canvas.Children.Add(_animated_image_small_to_big);

            Point root_point_small_start = _base_frame.OverlayManager.WindowTaskBar.GetElementPosition(zoom_element, _base_frame);
            root_point_small_start.X += destination_point.X;
            root_point_small_start.Y += destination_point.Y;
            Point root_point_base_grid = _base_frame.OverlayManager.WindowTaskBar.GetElementPosition(_animated_control_big_to_small.Diagram, _base_frame);
            double destination_width_submenu, destination_height_submenu;
            Point root_point_big_end;
            if (_animated_control_big_to_small.IsHidden)
            {
                root_point_big_end =
                    new Point(root_point_base_grid.X + _animated_control_big_to_small.Diagram.ActualWidth,
                              root_point_base_grid.Y + _animated_control_big_to_small.Diagram.ActualHeight);
                double relation = _animated_control_big_to_small.Diagram.ActualWidth /
                                  _animated_control_big_to_small.Diagram.ActualHeight;
                destination_width_submenu = 10 * relation;
                destination_height_submenu = 10;
            }
            else
            {
                root_point_big_end = GetRightMostPoint(out destination_width_submenu, out destination_height_submenu, false);
                if (root_point_big_end.Y == 0)
                    root_point_big_end.Y = _base_canvas.ActualHeight;
            }

            if (!diagram_meta_info_new.IsHidden)
            {
                _sub_menu_panel.RemoveItem(diagram_meta_info_new.SubMenuPanelMenuDiagramItem);
                diagram_meta_info_new.SubMenuPanelMenuDiagramItem.ResetControls(); // reset viewbox and minimized info
            }

            double scale_width_submenu = destination_width_submenu / _base_canvas.ActualWidth;
            double scale_height_submenu = destination_height_submenu / _base_canvas.ActualHeight;
            double scale_submenu = scale_width_submenu < scale_height_submenu ? scale_width_submenu : scale_height_submenu;

            double scale_width_small = _base_canvas.ActualWidth / zoom_element.ActualWidth;
            double scale_height_small = _base_canvas.ActualHeight / zoom_element.ActualHeight;
            //double scale_small = scale_width_small < scale_height_small ? scale_width_small : scale_height_small;
            const double scale_rectangle_start = 1;

            const double scale_base_grid = 1;

            // remove from old controls
            _base_canvas.Children.Remove(_animated_control_big_to_small.Diagram);
            //_sub_menu_panel.RemoveItem(item);
            //item.ResetControls(); // reset viewbox and minimized info

            OnBeforeBaseDiagramChange(_current_base_control, _animated_control_small_to_big);
            _current_base_control = _animated_control_small_to_big;

            //Debug.WriteLine("SwitchIntegratedDiagramsAnimated");
            // start animation
            Storyboard sb_1 = OverlayWindowManager.GetDefaultTranslateScaleAnimation(root_point_small_start, root_point_base_grid, true, scale_rectangle_start, scale_rectangle_start, scale_width_small, scale_height_small, false, 1);
            sb_1.Completed += new EventHandler(sb_Completed_integrated_1);
            DoubleAnimation opacity_anim = new DoubleAnimation(1, 0, new Duration(TimeSpan.FromMilliseconds(1000)));
            Storyboard.SetTargetProperty(opacity_anim, new PropertyPath(Image.OpacityProperty));
            sb_1.Children.Add(opacity_anim);

            Storyboard sb_2 = OverlayWindowManager.GetDefaultTranslateScaleAnimation(root_point_base_grid, root_point_big_end, true, scale_base_grid, scale_base_grid, scale_submenu, scale_submenu, false, 1);
            sb_2.Completed += new EventHandler(sb_Completed_2);

            Storyboard sb_3 = new Storyboard();
            opacity_anim = new DoubleAnimation(0, 1, new Duration(TimeSpan.FromMilliseconds(1000)));
            opacity_anim.AccelerationRatio = 0.1;
            Storyboard.SetTargetProperty(opacity_anim, new PropertyPath(UserControl.OpacityProperty));
            sb_3.Children.Add(opacity_anim);


            _anim_1_complete = false;
            _anim_2_complete = false;

            _sub_menu_panel.IsHitTestVisible = false;

            _animated_control_small_to_big.Diagram.Opacity = 0;
            _base_canvas.Children.Insert(0, _animated_control_small_to_big.Diagram);

            _animated_image_small_to_big.BeginStoryboard(sb_1);
            _animated_image_big_to_small.BeginStoryboard(sb_2);
            _animated_control_small_to_big.Diagram.BeginStoryboard(sb_3);

            //GenerateScrollDiagrams();
            //AdjustDiagramSlider();
        }

        private void SwitchSubmenuDiagramsAnimatedNew(UserControl new_control, FrameworkElement zoom_element, Point destination_point)
        {
            DiagramMetaInfo diagram_meta_info_new = _diagrams.Where(o => o.Diagram.Equals(new_control)).First();
            if (diagram_meta_info_new == null)
                throw new ArgumentException("Diagram not found!");

            _animated_control_small_to_big = diagram_meta_info_new;
            _animated_control_big_to_small = _current_base_control; // ui_element as UserControl;

            SetDiagramProperty("IsMinimized", false, _animated_control_small_to_big.Diagram);

            //calculate animation data
            _animated_image_big_to_small = CreateBitmap(_animated_control_big_to_small.Diagram);
            _overlay_canvas.Children.Add(_animated_image_big_to_small);

            _animated_image_small_to_big = CreateBitmap(zoom_element);
            _overlay_canvas.Children.Add(_animated_image_small_to_big);

            Point root_point_small_start = _base_frame.OverlayManager.WindowTaskBar.GetElementPosition(zoom_element, _base_frame);
            root_point_small_start.X += destination_point.X;
            root_point_small_start.Y += destination_point.Y;
            Point root_point_base_grid = _base_frame.OverlayManager.WindowTaskBar.GetElementPosition(_animated_control_big_to_small.Diagram, _base_frame);
            double destination_width_submenu, destination_height_submenu;
            Point root_point_big_end;
            if (_animated_control_big_to_small.IsHidden)
            {
                root_point_big_end =
                    new Point(root_point_base_grid.X + _animated_control_big_to_small.Diagram.ActualWidth,
                              root_point_base_grid.Y + _animated_control_big_to_small.Diagram.ActualHeight);
                double relation = _animated_control_big_to_small.Diagram.ActualWidth /
                                  _animated_control_big_to_small.Diagram.ActualHeight;
                destination_width_submenu = 10 * relation;
                destination_height_submenu = 10;
            }
            else
            {
                root_point_big_end = GetRightMostPoint(out destination_width_submenu, out destination_height_submenu, !diagram_meta_info_new.IsHidden);
                if (root_point_big_end.Y == 0)
                    root_point_big_end.Y = _base_canvas.ActualHeight;
            }

            if (!diagram_meta_info_new.IsHidden)
            {
                _sub_menu_panel.RemoveItem(diagram_meta_info_new.SubMenuPanelMenuDiagramItem);
                diagram_meta_info_new.SubMenuPanelMenuDiagramItem.ResetControls(); // reset viewbox and minimized info
            }

            double scale_width_submenu = destination_width_submenu / _base_canvas.ActualWidth;
            double scale_height_submenu = destination_height_submenu / _base_canvas.ActualHeight;
            double scale_submenu = scale_width_submenu < scale_height_submenu ? scale_width_submenu : scale_height_submenu;

            double scale_width_small = _base_canvas.ActualWidth / zoom_element.ActualWidth;
            double scale_height_small = _base_canvas.ActualHeight / zoom_element.ActualHeight;
            //double scale_small = scale_width_small < scale_height_small ? scale_width_small : scale_height_small;
            const double scale_rectangle_start = 1;

            const double scale_base_grid = 1;

            // remove from old controls
            _base_canvas.Children.Remove(_animated_control_big_to_small.Diagram);
            //_sub_menu_panel.RemoveItem(item);
            //item.ResetControls(); // reset viewbox and minimized info

            OnBeforeBaseDiagramChange(_current_base_control, _animated_control_small_to_big);
            _current_base_control = _animated_control_small_to_big;

            //Debug.WriteLine("SwitchSubmenuDiagramsAnimated");
            // start animation
            Storyboard sb_1 = OverlayWindowManager.GetDefaultTranslateScaleAnimation(root_point_small_start, root_point_base_grid, true, scale_rectangle_start, scale_rectangle_start, scale_width_small, scale_height_small, false, 1);
            sb_1.Completed += new EventHandler(sb_Completed_integrated_1);
            DoubleAnimation opacity_anim = new DoubleAnimation(1, 0, new Duration(TimeSpan.FromMilliseconds(1000)));
            Storyboard.SetTargetProperty(opacity_anim, new PropertyPath(Image.OpacityProperty));
            sb_1.Children.Add(opacity_anim);

            Storyboard sb_2 = OverlayWindowManager.GetDefaultTranslateScaleAnimation(root_point_base_grid, root_point_big_end, true, scale_base_grid, scale_base_grid, scale_submenu, scale_submenu, false, 1);
            sb_2.Completed += new EventHandler(sb_Completed_2);

            Storyboard sb_3 = new Storyboard();
            opacity_anim = new DoubleAnimation(0, 1, new Duration(TimeSpan.FromMilliseconds(1000)));
            opacity_anim.AccelerationRatio = 0.1;
            Storyboard.SetTargetProperty(opacity_anim, new PropertyPath(UserControl.OpacityProperty));
            sb_3.Children.Add(opacity_anim);


            _anim_1_complete = false;
            _anim_2_complete = false;

            _sub_menu_panel.IsHitTestVisible = false;

            _animated_control_small_to_big.Diagram.Opacity = 0;
            _base_canvas.Children.Insert(0, _animated_control_small_to_big.Diagram);

            _animated_image_small_to_big.BeginStoryboard(sb_1);
            _animated_image_big_to_small.BeginStoryboard(sb_2);
            _animated_control_small_to_big.Diagram.BeginStoryboard(sb_3);

            //GenerateScrollDiagrams();
            //AdjustDiagramSlider();
        }

        private void SwitchIntegratedDiagramsAnimatedNew(UserControl new_control, FrameworkElement zoom_element, Point destination_point, double loading_size, bool is_new_diagram_hidden)
        {
            DiagramMetaInfo diagram_meta_info_new = _diagrams.Where(o => o.Diagram.Equals(new_control)).First();
            if (diagram_meta_info_new == null)
                throw new ArgumentException("Diagram not found!");

            _animated_control_small_to_big = diagram_meta_info_new;
            _animated_control_big_to_small = _current_base_control;

            _base_frame.DisableMouseClicks();

            SetDiagramProperty("IsMinimized", false, _animated_control_small_to_big.Diagram);

            bool is_loading_before = GetIsLoading(_animated_control_small_to_big.Diagram);
            bool has_loaded_data = GetHasLoadedData(_animated_control_small_to_big.Diagram);

            //Debug.WriteLine("INITIAL isloading value before: " + is_loading_before + ", HAS LOADED " + has_loaded_data + ", HIDDEN? " + diagram_meta_info_new.IsHidden);
            if (diagram_meta_info_new.IsHidden && !has_loaded_data)
            {
                ((INotifyPropertyChanged)_animated_control_small_to_big.Diagram).PropertyChanged +=
                    new PropertyChangedEventHandler(DiagramManager_PropertyChanged);

                AddLoadingControl(zoom_element, loading_size);
            }

            _animated_image_big_to_small = CreateBitmap(_animated_control_big_to_small.Diagram);
            _overlay_canvas.Children.Add(_animated_image_big_to_small);

            //double top = Canvas.GetTop(_animated_image_big_to_small);
            double title_bar_actual_height = _base_frame.TitleBar.ActualHeight;
            //Debug.WriteLine("Canvas top: " + top + ", CHANGE TO " + title_bar_actual_height);
            Canvas.SetTop(_animated_image_big_to_small, title_bar_actual_height);

            if (!diagram_meta_info_new.IsHidden)
            {
                _sub_menu_panel.RemoveItem(diagram_meta_info_new.SubMenuPanelMenuDiagramItem);
                diagram_meta_info_new.SubMenuPanelMenuDiagramItem.ResetControls(); // reset viewbox and minimized info
            }

            _base_canvas.Children.Add(_animated_control_small_to_big.Diagram);

            OnBeforeBaseDiagramChange(_animated_control_big_to_small, _animated_control_small_to_big);

            _animated_control_small_to_big.Diagram.Opacity = 1.0;
            _animated_control_small_to_big.Diagram.Visibility = Visibility.Hidden;
            _animated_control_big_to_small.Diagram.Visibility = Visibility.Visible;

            _current_zoom_element = zoom_element;
            _current_destination_point = destination_point;

            if (!diagram_meta_info_new.IsHidden || has_loaded_data)
            {
                //SetDiagramProperty("HasLoadedData", true, _animated_control_small_to_big.Diagram);
                StartAfterLoadAnimation();
            }
        }

        private void AddLoadingControl(FrameworkElement zoom_element, double loading_size)
        {
            // create loading control
            Point root_point_small_start = _base_frame.OverlayManager.WindowTaskBar.GetElementPosition(zoom_element, _base_frame);
            if (_current_loading_control == null)
            {
                _current_loading_control = new LoadingControl();
            }
            _overlay_canvas.Children.Add(_current_loading_control);
            Canvas.SetZIndex(_current_loading_control, 999999);
            Canvas.SetLeft(_current_loading_control, root_point_small_start.X + zoom_element.ActualWidth / 2);
            Canvas.SetTop(_current_loading_control, root_point_small_start.Y + zoom_element.ActualHeight / 2);

            double scale_x = zoom_element.ActualWidth / loading_size;
            double scale_y = zoom_element.ActualHeight / loading_size;
            double scale = scale_x < scale_y ? scale_x : scale_y;
            //Debug.WriteLine("Scaletransform " + scale);
            if (scale < 1)
            {
                ScaleTransform scale_transform = new ScaleTransform(scale, scale);
                _current_loading_control.LayoutTransform = scale_transform;
            }
            else
                _current_loading_control.LayoutTransform = null;

        }

        void DiagramManager_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            //bool is_loading_before = GetIsLoading(_animated_control_small_to_big.Diagram);
            //Debug.WriteLine("END LOADINGGGGGGGGGGGGGGGG, name " + e.PropertyName + ", HAS LOADED " + GetHasLoadedData(_animated_control_small_to_big.Diagram));
            if (e.PropertyName == "EndLoading")
            {
                bool is_loading = GetIsLoading(_animated_control_small_to_big.Diagram);
                //Debug.WriteLine("ISLOADING NEW_VALUE " + is_loading);
                if (is_loading == false)
                {
                    StartAfterLoadAnimation();
                    _overlay_canvas.Children.Remove(_current_loading_control);
                }
            }
        }

        private void StartAfterLoadAnimation()
        {
            ((INotifyPropertyChanged)_animated_control_small_to_big.Diagram).PropertyChanged -= new PropertyChangedEventHandler(DiagramManager_PropertyChanged);

            _animated_control_small_to_big.Diagram.Visibility = Visibility.Visible;
            _animated_image_small_to_big = CreateBitmap(_animated_control_small_to_big.Diagram);
            _animated_image_small_to_big.Opacity = 1.0;
            _animated_control_small_to_big.Diagram.Visibility = Visibility.Hidden;
            _overlay_canvas.Children.Add(_animated_image_small_to_big);

            Point root_point_small_start = _base_frame.OverlayManager.WindowTaskBar.GetElementPosition(_current_zoom_element, _base_frame);
            //Debug.WriteLine("Root point: " + root_point_small_start + ", destination point: " + _current_destination_point);
            root_point_small_start.X += _current_destination_point.X;
            root_point_small_start.Y += _current_destination_point.X;
            Point root_point_base_grid = _base_frame.OverlayManager.WindowTaskBar.GetElementPosition(_animated_control_big_to_small.Diagram, _base_frame);
            double destination_width_submenu, destination_height_submenu;
            Point root_point_big_end;
            if (_animated_control_big_to_small.IsHidden)
            {
                root_point_big_end =
                    new Point(root_point_base_grid.X + _animated_control_big_to_small.Diagram.ActualWidth,
                              root_point_base_grid.Y + _animated_control_big_to_small.Diagram.ActualHeight);
                double relation = _animated_control_big_to_small.Diagram.ActualWidth /
                                  _animated_control_big_to_small.Diagram.ActualHeight;
                destination_width_submenu = 10 * relation;
                destination_height_submenu = 10;
            }
            else
            {
                //bool calculate_pos_one_more_than_last = !_animated_control_small_to_big.IsHidden &&
                //                                        !_animated_control_big_to_small.IsHidden
                //                                            ? false
                //                                            : true;
                root_point_big_end = GetRightMostPoint(out destination_width_submenu, out destination_height_submenu, true);
                if (root_point_big_end.Y == 0)
                    root_point_big_end.Y = _base_canvas.ActualHeight;
            }

            double scale_width_submenu = destination_width_submenu / _base_canvas.ActualWidth;
            double scale_height_submenu = destination_height_submenu / _base_canvas.ActualHeight;
            double scale_submenu = scale_width_submenu < scale_height_submenu ? scale_width_submenu : scale_height_submenu;

            double scale_width_small = _base_canvas.ActualWidth / _current_zoom_element.ActualWidth;
            double scale_height_small = _base_canvas.ActualHeight / _current_zoom_element.ActualHeight;

            const double scale_base_grid = 1;

            // remove from old controls
            _base_canvas.Children.Remove(_animated_control_big_to_small.Diagram);

            _current_base_control = _animated_control_small_to_big;

            //Debug.WriteLine("StartAfterLoadAnimation");
            // start animation
            double scale_stb_x = 1 / scale_width_small;
            double scale_stb_y = 1 / scale_height_small;
            Storyboard sb_1 = OverlayWindowManager.GetDefaultTranslateScaleAnimation(root_point_small_start, root_point_base_grid, true, scale_stb_x, scale_stb_y, 1, 1, false, 1.5);
            sb_1.Completed += new EventHandler(sb_Completed_integrated_1New);
            //sb_1.Completed +=new EventHandler(sb_Completed_1);
            //DoubleAnimation opacity_anim = new DoubleAnimation(1, 0, new Duration(TimeSpan.FromMilliseconds(5000)));
            //Storyboard.SetTargetProperty(opacity_anim, new PropertyPath(Image.OpacityProperty));
            //sb_1.Children.Add(opacity_anim);

            Storyboard sb_2 = OverlayWindowManager.GetDefaultTranslateScaleAnimation(root_point_base_grid, root_point_big_end, true, scale_base_grid, scale_base_grid, scale_submenu, scale_submenu, false, 1.5);
            sb_2.Completed += new EventHandler(sb_Completed_2);

            //Storyboard sb_3 = new Storyboard();
            //opacity_anim = new DoubleAnimation(0, 1, new Duration(TimeSpan.FromMilliseconds(1000)));
            //opacity_anim.AccelerationRatio = 0.1;
            //Storyboard.SetTargetProperty(opacity_anim, new PropertyPath(UserControl.OpacityProperty));
            //sb_3.Children.Add(opacity_anim);


            _anim_1_complete = false;
            _anim_2_complete = false;

            _sub_menu_panel.IsHitTestVisible = false;

            //_animated_control_small_to_big.Diagram.Opacity = 1;
            _animated_control_small_to_big.Diagram.Opacity = 0.0;

            _animated_image_small_to_big.BeginStoryboard(sb_1);
            _animated_image_big_to_small.BeginStoryboard(sb_2);

            //_animated_control_small_to_big.Diagram.BeginStoryboard(sb_3);

            //GenerateScrollDiagrams();
            //AdjustDiagramSlider();
        }

        private void SwitchIntegratedDiagramsImmediately(UserControl new_control)
        {
            DiagramMetaInfo diagram_meta_info_new = _diagrams.Where(o => o.Diagram.Equals(new_control)).First();
            if (diagram_meta_info_new == null)
                throw new ArgumentException("Diagram not found!");

            DiagramMetaInfo big_control = _current_base_control; // ui_element as UserControl;
            if (big_control == null)
                throw new ArgumentException("Invalid type in base grid");

            OnBeforeBaseDiagramChange(_current_base_control, diagram_meta_info_new);

            // remove from old controls
            _base_canvas.Children.Remove(big_control.Diagram);

            if (!diagram_meta_info_new.IsHidden)
            {
                _sub_menu_panel.RemoveItem(diagram_meta_info_new.SubMenuPanelMenuDiagramItem);
                diagram_meta_info_new.SubMenuPanelMenuDiagramItem.ResetControls(); // reset viewbox and minimized info
            }
            _current_base_control = diagram_meta_info_new;

            _base_canvas.Children.Insert(0, new_control);

            SetDiagramProperty("IsMinimized", false, diagram_meta_info_new.Diagram);

            if (!big_control.IsHidden)
            {
                //string title = GetTitleText(big_control.Diagram);
                SubMenuPanelMenuDiagramItem new_item = new SubMenuPanelMenuDiagramItem(big_control.Title, big_control.Diagram, GetMinimizedLegendControl(big_control.Diagram), true);
                new_item.Key = big_control.Title;
                new_item.IsActive = false;
                big_control.SubMenuPanelMenuDiagramItem = new_item;
                SetDiagramProperty("IsMinimized", true, big_control.Diagram);
                _sub_menu_panel.AddItem(new_item);
            }

            GenerateScrollDiagrams();
            AdjustDiagramSlider();
            OnBaseDiagramChanged(_current_base_control);
        }

        /// <summary>
        /// Protected method allowing to set the a diagram property
        /// </summary>
        /// <param name="property_name"></param>
        /// <param name="value"></param>
        /// <param name="diagram"></param>
        /// <returns>true if succeed</returns>
        protected bool SetDiagramProperty(string property_name, object value, object diagram)
        {
            if (diagram == null)
                return false;

            try
            {

                Type diagram_type = diagram.GetType();
                PropertyInfo pi = diagram_type.GetProperty(property_name);

                if (pi != null)
                {
                    pi.SetValue(diagram, value, null);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("SetDiagramProperty() error: " + ex);
            }

            return false;
        }
        /// <summary>
        /// Gets a diagram property
        /// </summary>
        /// <param name="property_name"></param>
        /// <param name="diagram"></param>
        /// <returns></returns>
        protected object GetDiagramProperty(string property_name, object diagram)
        {
            if (diagram == null)
                return null;

            try
            {
                Type diagram_type = diagram.GetType();
                PropertyInfo pi = diagram_type.GetProperty(property_name);

                if (pi != null)
                {
                    return pi.GetValue(diagram, null);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("GetDiagramProperty() error: " + ex);
            }

            return null;
        }

        protected bool IsAnyDiagramLoading()
        {
            bool bIsAnyLoading = false;

            foreach (DiagramMetaInfo diagram_meta in _diagrams)
            {
                bIsAnyLoading |= GetIsLoading(diagram_meta.Diagram);
                if (bIsAnyLoading)
                    break;
            }

            if (!bIsAnyLoading)
            {
                foreach (DiagramMetaInfo diagram_meta in _left_diagrams)
                {
                    bIsAnyLoading |= GetIsLoading(diagram_meta.Diagram);
                    if (bIsAnyLoading)
                        break;
                }
            }

            if (!bIsAnyLoading)
            {
                foreach (DiagramMetaInfo diagram_meta in _right_diagrams)
                {
                    bIsAnyLoading |= GetIsLoading(diagram_meta.Diagram);
                    if (bIsAnyLoading)
                        break;
                }
            }

            return bIsAnyLoading;
        }

        protected bool GetIsLoading(object diagram_control)
        {
            if (diagram_control == null)
                return false;

            bool is_loading = false;

            object read_object = GetDiagramProperty("IsLoading", diagram_control);

            if (read_object != null && read_object is bool)
                is_loading = (bool)read_object;

            return is_loading;
        }

        protected DiagramMode GetDiagramMode(object diagram_control)
        {
            if (diagram_control == null)
                return DiagramMode.Normal;

            DiagramMode diagram_mode = DiagramMode.Normal;

            object read_object = GetDiagramProperty("DiagramMode", diagram_control);

            if (read_object != null && read_object is DiagramMode)
                diagram_mode = (DiagramMode)read_object;

            return diagram_mode;
        }

        protected void SetDiagramMode(object diagram_control, DiagramMode new_mode)
        {
            if (diagram_control == null)
                return;

            SetDiagramProperty("DiagramMode", new_mode, diagram_control);
        }

        protected bool GetHasLoadedData(object diagram_control)
        {
            if (diagram_control == null)
                return false;

            bool has_loaded = false;

            object read_object = GetDiagramProperty("HasLoadedData", diagram_control);

            if (read_object != null && read_object is bool)
                has_loaded = (bool)read_object;

            return has_loaded;
        }

        private void SetDiagramManager(object diagram_control)
        {
            if (diagram_control == null)
                return;

            SetDiagramProperty("DiagramManager", this, diagram_control);
        }

        private UserControl GetMinimizedLegendControl(object diagram_control)
        {
            if (diagram_control == null)
                return null;

            object read_object = GetDiagramProperty("MinimizedLegendControl", diagram_control);

            if (read_object != null && read_object is UserControl)
                return read_object as UserControl;

            return null;
        }

        private void AbortBackgroundLoader(object diagram_control)
        {
            if (_diagrams.Count == 0)
                return;
            if (diagram_control == null)
                return;

            Type current_base_type = diagram_control.GetType();

            MethodInfo mi_abort = current_base_type.GetMethod("AbortBackgroundLoading");
            if (mi_abort != null)
            {
                mi_abort.Invoke(diagram_control, null);
            }
        }

        private bool CurrentDiagramCanScrollDiagrams()
        {
            if (_diagrams.Count == 0)
                return false;
            if (_current_base_control == null)
                return false;

            Type current_base_type = _current_base_control.Diagram.GetType();

            bool can_scroll_diagram = false;

            PropertyInfo pi_can_scroll_diagrams = current_base_type.GetProperty("CanScrollDiagrams");
            if (pi_can_scroll_diagrams != null)
            {
                // get the diagram config instance
                object config_object = pi_can_scroll_diagrams.GetValue(_current_base_control.Diagram, null);
                if (config_object is bool)
                    can_scroll_diagram = (bool)config_object;
            }

            return can_scroll_diagram;
        }

        private bool CurrentDiagramCanZoomXAxis()
        {
            if (_diagrams.Count == 0)
                return false;
            if (_current_base_control == null)
                return false;

            Type current_base_type = _current_base_control.Diagram.GetType();

            bool can_zoom_x_axis = false;

            PropertyInfo pi_can_zoom_x_axis = current_base_type.GetProperty("CanZoomXAxis");
            if (pi_can_zoom_x_axis != null)
            {
                // get the diagram config instance
                object config_object = pi_can_zoom_x_axis.GetValue(_current_base_control.Diagram, null);
                if (config_object is bool)
                    can_zoom_x_axis = (bool)config_object;
            }

            return can_zoom_x_axis;
        }

        private void CurrentDiagramXAxisZoom(bool zoomin, EventArgs e)
        {
            if (_diagrams.Count == 0)
                return;
            if (_current_base_control == null)
                return;

            Type current_base_type = _current_base_control.Diagram.GetType();

            bool can_zoom_x_axis = false;

            MethodInfo method;

            if (zoomin)
                method = current_base_type.GetMethod("XAxisZoomIn");
            else
                method = current_base_type.GetMethod("XAxisZoomOut");

            if (method != null)
            {
                // get the diagram config instance
                method.Invoke(_current_base_control.Diagram, new object[] { e });
            }
        }

        public void ResetZoom(bool all_diagrams)
        {
            if (_diagrams.Count == 0)
                return;

            if (!all_diagrams && _current_base_control == null)
                return;

            if (!all_diagrams)
            {
                ResetZoomOfDiagram(_current_base_control.Diagram);
            }
            else
            {
                foreach (DiagramMetaInfo current_meta in _diagrams)
                {
                    ResetZoomOfDiagram(current_meta.Diagram);
                }

                foreach (DiagramMetaInfo current_meta in _left_diagrams)
                {
                    ResetZoomOfDiagram(current_meta.Diagram);
                }

                foreach (DiagramMetaInfo current_meta in _right_diagrams)
                {
                    ResetZoomOfDiagram(current_meta.Diagram);
                }
            }
        }

        private void ResetZoomOfDiagram(UserControl diagram)
        {
            if (diagram == null)
                return;

            Type current_base_type = diagram.GetType();
            MethodInfo method;
            method = current_base_type.GetMethod("ResetZoom");
            if (method != null)
                method.Invoke(diagram, null);
        }

        private void SendDiagramManagerFilterChanged(UserControl diagram_control, object argument)
        {
            if (diagram_control == null)
                return;

            Type current_base_type = diagram_control.GetType();
            if (current_base_type != null)
            {
                MethodInfo miDiagramManagerFilterChanged = current_base_type.GetMethod("DiagramManagerFilterChanged");
                if (miDiagramManagerFilterChanged != null)
                {
                    miDiagramManagerFilterChanged.Invoke(diagram_control, new object[] { argument });
                }
            }
        }


        private double CurrentXAxisHeight()
        {
            if (_diagrams.Count == 0)
                return 0;
            if (_current_base_control == null)
                return 0;

            Type current_base_type = _current_base_control.GetType();

            bool can_scroll_diagram = false;

            PropertyInfo pi_xaxiscontrol = current_base_type.GetProperty("XAxisControl");
            if (pi_xaxiscontrol != null)
            {
                // get the diagram config instance
                object x_axis_control = pi_xaxiscontrol.GetValue(_current_base_control.Diagram, null);

                if (x_axis_control != null)
                {
                    if (x_axis_control is DependencyObject)
                    {
                        object ret_val = ((DependencyObject)x_axis_control).GetValue(ScaleControlBase.ActualHeightProperty);

                        if (ret_val != DependencyProperty.UnsetValue && ret_val is double)
                        {
                            return (double)ret_val;
                        }
                    }
                }
            }

            return 0;
        }

        private void GenerateScrollDiagrams()
        {
            if (_diagrams.Count == 0)
                return;
            if (_current_base_control == null)
                return;

            if (_current_base_control.Diagram.GetType() != _scroll_type && _scroll_type != null)
            {
                // 1. clear current scroll diagrams
                ClearScrollDiagrams();

            }

            if (_scroll_type == null || _left_diagrams.Count != NumberOfScrollDiagrams)
            {
                // 2. create new control instances
                Type current_base_type = _current_base_control.Diagram.GetType();

                bool can_scroll_diagram = CurrentDiagramCanScrollDiagrams();

                PropertyInfo pi_can_scroll_diagrams = current_base_type.GetProperty("CanScrollDiagrams");
                if (pi_can_scroll_diagrams != null)
                {
                    // get the diagram config instance
                    object config_object = pi_can_scroll_diagrams.GetValue(_current_base_control.Diagram, null);
                    if (config_object is bool)
                        can_scroll_diagram = (bool)config_object;
                }

                if (!HasDefaultConstructor(current_base_type) || !can_scroll_diagram)
                {
                    ClearScrollDiagrams();
                    return;
                    //throw new Exception("Scrollable diagrams must have a parameterless constructor");
                }

                for (int i = 0; i < NumberOfScrollDiagrams; i++)
                {
                    UserControl new_left_control = Activator.CreateInstance(current_base_type) as UserControl;

                    if (new_left_control != null)
                    {
                        DiagramMetaInfo new_meta_info_left = new DiagramMetaInfo(new_left_control, _current_base_control.Title, _current_base_control.IsHidden);
                        _left_diagrams.Add(new_meta_info_left);

                        new_left_control.Visibility = Visibility.Collapsed;

                        _base_canvas.Children.Add(new_left_control);
                        new_left_control.SizeChanged += scroll_control_SizeChanged;

                        SetDiagramManager(new_left_control);
                        if (new_left_control is INotifyPropertyChanged)
                            ((INotifyPropertyChanged)new_left_control).PropertyChanged += new PropertyChangedEventHandler(DiagramManager_Diagram_PropertyChanged);

                        InitMoveHandlers(new_left_control);

                        OnScrollDiagramAdded(new_left_control, true);
                    }

                    UserControl new_right_control = Activator.CreateInstance(current_base_type) as UserControl;

                    if (new_right_control != null)
                    {
                        DiagramMetaInfo new_meta_info_right = new DiagramMetaInfo(new_right_control, _current_base_control.Title, _current_base_control.IsHidden);
                        _right_diagrams.Add(new_meta_info_right);

                        new_right_control.Visibility = Visibility.Collapsed;

                        _base_canvas.Children.Add(new_right_control);
                        new_right_control.SizeChanged += scroll_control_SizeChanged;

                        SetDiagramManager(new_right_control);
                        if (new_right_control is INotifyPropertyChanged)
                            ((INotifyPropertyChanged)new_right_control).PropertyChanged += new PropertyChangedEventHandler(DiagramManager_Diagram_PropertyChanged);
                        
                        InitMoveHandlers(new_right_control);

                        OnScrollDiagramAdded(new_right_control, false);
                    }
                }

                _scroll_type = current_base_type;
            }

            ForceRequestSwapDiagramConfigs();
        }

        public void ForceRequestSwapDiagramConfigs()
        {
            if (_scroll_type != null)
            {
                // 3. adjust configurations

                // get the proeprty info of the DiagramConfig property
                PropertyInfo pi_diagram_config = _scroll_type.GetProperty("DiagramConfig");
                if (pi_diagram_config != null)
                {
                    // get the diagram config instance
                    object config_object = pi_diagram_config.GetValue(_current_base_control.Diagram, null);

                    if (config_object != null)
                    {
                        Type diagram_config_type = config_object.GetType();

                        // get the property infos for the X and Y Scale configurations
                        PropertyInfo pi_right_config = diagram_config_type.GetProperty("RightConfig");
                        PropertyInfo pi_left_config = diagram_config_type.GetProperty("LeftConfig");

                        // get the method infos for the StartRequestRightConfig() and StartRequestLeftConfig() methods
                        MethodInfo mi_start_request_right_config = diagram_config_type.GetMethod("StartRequestRightConfig");
                        MethodInfo mi_start_request_left_config = diagram_config_type.GetMethod("StartRequestLeftConfig");

                        if (pi_right_config != null && pi_left_config != null &&
                            mi_start_request_right_config != null && mi_start_request_left_config != null)
                        {
                            object current_center_config = config_object;
                            foreach (DiagramMetaInfo left_diagram in _left_diagrams)
                            {
                                if (current_center_config == null)
                                {
                                    left_diagram.Diagram.Visibility = Visibility.Collapsed;
                                }
                                else
                                {
                                    // invoke "StartRequestLeftConfig" method, this will rise the RequestLeftConfig event 
                                    // which will be handled somewhere
                                    mi_start_request_left_config.Invoke(current_center_config, null);

                                    // read the generated LeftConfig property 
                                    object new_left_config = pi_left_config.GetValue(current_center_config, null);

                                    // if no left config is available, hide the diagram
                                    if (new_left_config == null)
                                    {
                                        left_diagram.Diagram.Visibility = Visibility.Collapsed;
                                    }
                                    else
                                    {
                                        // set new configuration
                                        pi_diagram_config.SetValue(left_diagram.Diagram, new_left_config, null);
                                    }

                                    current_center_config = new_left_config;
                                }
                            }

                            current_center_config = config_object;
                            foreach (DiagramMetaInfo right_diagram in _right_diagrams)
                            {
                                if (current_center_config == null)
                                {
                                    right_diagram.Diagram.Visibility = Visibility.Collapsed;
                                }
                                else
                                {
                                    // invoke "StartRequestRightConfig" method, this will rise the RequestRightConfig event 
                                    // which will be handled somewhere
                                    mi_start_request_right_config.Invoke(current_center_config, null);

                                    // read the generated LeftConfig property 
                                    object new_right_config = pi_right_config.GetValue(current_center_config, null);

                                    // if no left config is available, hide the diagram
                                    if (new_right_config == null)
                                    {
                                        right_diagram.Diagram.Visibility = Visibility.Collapsed;
                                    }
                                    else
                                    {
                                        // set new configuration
                                        pi_diagram_config.SetValue(right_diagram.Diagram, new_right_config, null);
                                    }

                                    current_center_config = new_right_config;
                                }
                            }

                        }
                    }
                }
            }
        }

        private bool HasDefaultConstructor(Type type)
        {
            if (type.IsValueType)
                return true;

            var constructor = type.GetConstructor(Type.EmptyTypes);

            if (constructor == null)
                return false;

            return true;
        }

        private void ClearScrollDiagrams()
        {
            if (_left_diagrams.Count > 0)
            {
                foreach (DiagramMetaInfo meta_info in _left_diagrams)
                {
                    if (_base_canvas.Children.Contains(meta_info.Diagram))
                        _base_canvas.Children.Remove(meta_info.Diagram);

                    meta_info.Diagram.SizeChanged -= scroll_control_SizeChanged;

                    if (meta_info.Diagram is INotifyPropertyChanged)
                        ((INotifyPropertyChanged)meta_info.Diagram).PropertyChanged -= DiagramManager_Diagram_PropertyChanged;

                    OnScrollDiagramRemoved(meta_info.Diagram, true);

                    DeregisterMoveHandlers(meta_info.Diagram);

                    if (meta_info.Diagram is IDisposable)
                    {
                         ((IDisposable)meta_info.Diagram).Dispose();
                    }
                }

                _left_diagrams.Clear();
            }

            if (_right_diagrams.Count > 0)
            {
                foreach (DiagramMetaInfo meta_info in _right_diagrams)
                {
                    if (_base_canvas.Children.Contains(meta_info.Diagram))
                        _base_canvas.Children.Remove(meta_info.Diagram);

                    meta_info.Diagram.SizeChanged -= scroll_control_SizeChanged;

                    if (meta_info.Diagram is INotifyPropertyChanged)
                        ((INotifyPropertyChanged)meta_info.Diagram).PropertyChanged -= DiagramManager_Diagram_PropertyChanged;

                    OnScrollDiagramRemoved(meta_info.Diagram, false);

                    DeregisterMoveHandlers(meta_info.Diagram);

                    if (meta_info.Diagram is IDisposable)
                    {
                        ((IDisposable)meta_info.Diagram).Dispose();
                    }
                }

                _right_diagrams.Clear();
            }

            _scroll_type = null;
        }

        /// <summary>
        /// Forces the async data reload on all diagrams
        /// </summary>
        public void ForceDataReload()
        {
            foreach (DiagramMetaInfo meta_info in _diagrams)
            {
                if (!meta_info.IsHidden || meta_info == _current_base_control)
                {
                    Type diagram_type = meta_info.Diagram.GetType();
                    MethodInfo force_load_data = diagram_type.GetMethod("ForceLoadData", new Type[0]);
                    if (force_load_data != null)
                        force_load_data.Invoke(meta_info.Diagram, null);
                }
            }

            // regenerate scroll diagrams
            ClearScrollDiagrams();
            WaitForMainControlLoadingCompleted();

            //foreach (DiagramMetaInfo meta_info in _left_diagrams)
            //{
            //    Type diagram_type = meta_info.Diagram.GetType();
            //    MethodInfo force_load_data = diagram_type.GetMethod("ForceLoadData", new Type[0]);
            //    if (force_load_data != null)
            //        force_load_data.Invoke(meta_info.Diagram, null);
            //}

            //foreach (DiagramMetaInfo meta_info in _right_diagrams)
            //{
            //    Type diagram_type = meta_info.Diagram.GetType();
            //    MethodInfo force_load_data = diagram_type.GetMethod("ForceLoadData", new Type[0]);
            //    if (force_load_data != null)
            //        force_load_data.Invoke(meta_info.Diagram, null);
            //}
        }

        public void ForceMainDataReload()
        {
            if (_current_base_control != null)
            {
                Type diagram_type = _current_base_control.Diagram.GetType();
                MethodInfo force_load_data = diagram_type.GetMethod("ForceLoadData", new Type[0]);
                if (force_load_data != null)
                    force_load_data.Invoke(_current_base_control.Diagram, null);
            }

            // regenerate scroll diagrams
            ClearScrollDiagrams();
            WaitForMainControlLoadingCompleted();

            //foreach (DiagramMetaInfo meta_info in _left_diagrams)
            //{
            //    Type diagram_type = meta_info.Diagram.GetType();
            //    MethodInfo force_load_data = diagram_type.GetMethod("ForceLoadData", new Type[0]);
            //    if (force_load_data != null)
            //        force_load_data.Invoke(meta_info.Diagram, null);
            //}

            //foreach (DiagramMetaInfo meta_info in _right_diagrams)
            //{
            //    Type diagram_type = meta_info.Diagram.GetType();
            //    MethodInfo force_load_data = diagram_type.GetMethod("ForceLoadData", new Type[0]);
            //    if (force_load_data != null)
            //        force_load_data.Invoke(meta_info.Diagram, null);
            //}
        }

        /// <summary>
        /// Forces the async data reload on all diagrams
        /// </summary>
        public void ForceAllDataReload()
        {
            foreach (DiagramMetaInfo meta_info in _diagrams)
            {
                Type diagram_type = meta_info.Diagram.GetType();
                MethodInfo force_load_data = diagram_type.GetMethod("ForceLoadData", new Type[0]);
                if (force_load_data != null)
                    force_load_data.Invoke(meta_info.Diagram, null);
            }

            // regenerate scroll diagrams
            ClearScrollDiagrams();
            WaitForMainControlLoadingCompleted();
        }


        private void WaitForMainControlLoadingCompleted()
        {
            AbortLoadPending();

            if (_current_base_control == null)
                return;

            _main_control_load_pending = new QueuedBackgroundWorker("Base Diagram load-pending");
            _main_control_load_pending.DoWork += new QueuedBackgroundWorker.DoWorkEventHandler(_main_control_load_pending_DoWork);
            _main_control_load_pending.RunWorkerCompleted += new QueuedBackgroundWorker.RunWorkerCompletedEventHandler(_main_control_load_pending_RunWorkerCompleted);
            _main_control_load_pending.RunWorkerAsync(_current_base_control);
        }

        void _main_control_load_pending_RunWorkerCompleted(object sender, Utilities.RunWorkerCompletedEventArgs e)
        {
            _main_control_load_pending.DoWork -= _main_control_load_pending_DoWork;
            _main_control_load_pending.RunWorkerCompleted -= _main_control_load_pending_RunWorkerCompleted;
            _main_control_load_pending = null;

            _base_frame.Dispatcher.Invoke(DispatcherPriority.Normal, (NoParas)delegate
                                                                       {
                                                                           GenerateScrollDiagrams();
                                                                       });
        }

        void _main_control_load_pending_DoWork(object sender, Utilities.DoWorkEventArgs e)
        {
            bool is_loading = true;

            while (IsAnimating) Thread.Sleep(90);

            Thread.Sleep(300);

            while ((is_loading || IsAnimating) && !_abort_load_pending)
            {
                _base_frame.Dispatcher.Invoke(DispatcherPriority.Normal, (NoParas)delegate
                {
                    is_loading = GetIsLoading(e.Argument);
                });

                if (is_loading || IsAnimating)
                    Thread.Sleep(20);
            }
        }

        private void AbortLoadPending()
        {
            if (_main_control_load_pending != null)
            {
                _abort_load_pending = true;
                _main_control_load_pending.Abort();
                _main_control_load_pending.Join();
            }

            _abort_load_pending = false;
            _main_control_load_pending = null;
        }

        protected virtual void OnScrollDiagramAdded(UserControl control, bool is_left_scroll)
        {

        }

        protected virtual void OnScrollDiagramRemoved(UserControl control, bool is_left_scroll)
        {

        }

        protected bool IsLeftScrollDiagram(UserControl control)
        {
            //return _left_diagrams.Contains(control);
            return _left_diagrams.Where(o => o.Diagram.Equals(control)).Count() > 0;
        }

        protected bool IsRightScrollDiagram(UserControl control)
        {
            //return _right_diagrams.Contains(control);
            return _right_diagrams.Where(o => o.Diagram.Equals(control)).Count() > 0;
        }

        private bool SwapDiagrams(bool to_left_diagram)
        {
            bool current_base_can_scroll = CurrentDiagramCanScrollDiagrams();

            if (_diagrams.Count == 0 || _current_base_control == null || !current_base_can_scroll)
            {
                ResetDiagramPositions();
                return false;
            }

            if (to_left_diagram)
            {
                if (_left_diagrams.Count > 0)
                {
                    DiagramMetaInfo new_base_control = _left_diagrams[0];

                    if (new_base_control == null || new_base_control.Diagram == null || new_base_control.Diagram.Visibility == Visibility.Collapsed)
                    {
                        ResetDiagramPositions();
                        return false;
                    }

                    // das linke diagram wird aus der liste entfernt
                    _left_diagrams.RemoveAt(0);

                    // das neue basis diagram als child 0 einfügen
                    //if (_base_canvas.Children.Contains(new_base_control))
                    //    _base_canvas.Children.Remove(new_base_control);
                    //_base_canvas.Children.Insert(0, new_base_control);

                    // das aktuelle basis diagramm wird in die liste der rechten diagramme geschoben
                    _right_diagrams.Insert(0, _current_base_control);

                    // das aktuelle basisdiagramm wird verändert
                    //_current_base_control.IsHidden = true;
                    //new_base_control.IsHidden = false;
                    _diagrams.Remove(_current_base_control);
                    _diagrams.Add(new_base_control);

                    //OnBeforeBaseDiagramChange(_current_base_control, new_base_control);
                    _before_change_base_control = _current_base_control;
                    _current_base_control = new_base_control;
                    //OnBaseDiagramChanged(_current_base_control);

                    // das äußerst rechte diagramm wird in die linke scroll liste geschoben
                    if (_right_diagrams.Count > 1)
                    {
                        DiagramMetaInfo swap_diagram = _right_diagrams[_right_diagrams.Count - 1];
                        _right_diagrams.RemoveAt(_right_diagrams.Count - 1);
                        _left_diagrams.Add(swap_diagram);

                        bool bIsLoading = false;

                        bIsLoading = GetIsLoading(swap_diagram);

                        if (bIsLoading)
                            AbortBackgroundLoader(swap_diagram);

                        // das neue linke diagramm muss die daten neu laden
                        if (_scroll_type != null)
                        {
                            // get the proeprty info of the DiagramConfig property
                            PropertyInfo pi_diagram_config = _scroll_type.GetProperty("DiagramConfig");
                            if (pi_diagram_config != null)
                            {
                                // get the diagram config instance
                                object config_object = pi_diagram_config.GetValue(_current_base_control.Diagram, null);

                                if (config_object != null)
                                {
                                    Type diagram_config_type = config_object.GetType();

                                    // get the property infos for the X and Y Scale configurations
                                    PropertyInfo pi_left_config = diagram_config_type.GetProperty("LeftConfig");

                                    // get the method infos for the StartRequestRightConfig() and StartRequestLeftConfig() methods
                                    MethodInfo mi_start_request_left_config = diagram_config_type.GetMethod("StartRequestLeftConfig");

                                    if (pi_left_config != null && mi_start_request_left_config != null)
                                    {
                                        if (config_object == null)
                                        {
                                            swap_diagram.Diagram.Visibility = Visibility.Collapsed;
                                        }
                                        else
                                        {
                                            // invoke "StartRequestLeftConfig" method, this will rise the RequestLeftConfig event 
                                            // which will be handled somewhere
                                            mi_start_request_left_config.Invoke(config_object, null);

                                            // read the generated LeftConfig property 
                                            object new_left_config = pi_left_config.GetValue(config_object, null);

                                            // if no left config is available, hide the diagram
                                            if (new_left_config == null)
                                            {
                                                swap_diagram.Diagram.Visibility = Visibility.Collapsed;
                                            }
                                            else
                                            {
                                                // set new configuration
                                                pi_diagram_config.SetValue(swap_diagram.Diagram, new_left_config, null);
                                                swap_diagram.Diagram.Visibility = Visibility.Visible;
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        // das diagram im canvas nach links schieben
                        double dCurrentLeft = Canvas.GetLeft(_current_base_control.Diagram);
                        if (double.IsNaN(dCurrentLeft) || double.IsInfinity(dCurrentLeft))
                            dCurrentLeft = 0.0;

                        Canvas.SetLeft(swap_diagram.Diagram, dCurrentLeft - swap_diagram.Diagram.ActualWidth);
                    }

                    // die positionen sollten nach dem swapen der diagramme richtig berechnet werden
                    _send_base_diagram_changed_after_slide_animation = true;
                    ResetDiagramPositions();
                }
                else
                {
                    ResetDiagramPositions();
                    return false;
                }
            }
            else
            {
                if (_right_diagrams.Count > 0)
                {
                    DiagramMetaInfo new_base_control = _right_diagrams[0];

                    if (new_base_control == null || new_base_control.Diagram == null || new_base_control.Diagram.Visibility == Visibility.Collapsed)
                    {
                        ResetDiagramPositions();
                        return false;
                    }

                    // das rechte diagram wird aus der liste entfernt
                    _right_diagrams.RemoveAt(0);

                    // das neue basis diagram als child 0 einfügen
                    //if (_base_canvas.Children.Contains(new_base_control))
                    //    _base_canvas.Children.Remove(new_base_control);
                    //_base_canvas.Children.Insert(0, new_base_control);

                    // das aktuelle basis diagramm wird in die liste der linken diagramme geschoben
                    _left_diagrams.Insert(0, _current_base_control);

                    // das aktuelle basisdiagramm wird verändert
                    //_current_base_control.IsHidden = true;
                    //new_base_control.IsHidden = false;
                    _diagrams.Remove(_current_base_control);
                    _diagrams.Add(new_base_control);

                    _before_change_base_control = _current_base_control;
                    _current_base_control = new_base_control;


                    // das äußerst linke diagramm wird in die rechte scroll liste geschoben
                    if (_left_diagrams.Count > 1)
                    {
                        DiagramMetaInfo swap_diagram = _left_diagrams[_left_diagrams.Count - 1];
                        _left_diagrams.RemoveAt(_left_diagrams.Count - 1);
                        _right_diagrams.Add(swap_diagram);

                        bool bIsLoading = false;

                        bIsLoading = GetIsLoading(swap_diagram);

                        if (bIsLoading)
                            AbortBackgroundLoader(swap_diagram);

                        // das neue linke diagramm muss die daten neu laden
                        if (_scroll_type != null)
                        {
                            // 3. adjust configurations

                            // get the proeprty info of the DiagramConfig property
                            PropertyInfo pi_diagram_config = _scroll_type.GetProperty("DiagramConfig");
                            if (pi_diagram_config != null)
                            {
                                // get the diagram config instance
                                object config_object = pi_diagram_config.GetValue(_current_base_control.Diagram, null);

                                if (config_object != null)
                                {
                                    Type diagram_config_type = config_object.GetType();

                                    // get the property infos for the X and Y Scale configurations
                                    PropertyInfo pi_right_config = diagram_config_type.GetProperty("RightConfig");

                                    // get the method infos for the StartRequestRightConfig() and StartRequestLeftConfig() methods
                                    MethodInfo mi_start_request_right_config = diagram_config_type.GetMethod("StartRequestRightConfig");

                                    if (pi_right_config != null && mi_start_request_right_config != null)
                                    {
                                        if (config_object == null)
                                        {
                                            swap_diagram.Diagram.Visibility = Visibility.Collapsed;
                                        }
                                        else
                                        {
                                            // invoke "StartRequestRightConfig" method, this will rise the RequestRightConfig event 
                                            // which will be handled somewhere
                                            mi_start_request_right_config.Invoke(config_object, null);

                                            // read the generated LeftConfig property 
                                            object new_right_config = pi_right_config.GetValue(config_object, null);

                                            // if no left config is available, hide the diagram
                                            if (new_right_config == null)
                                            {
                                                swap_diagram.Diagram.Visibility = Visibility.Collapsed;
                                            }
                                            else
                                            {
                                                // set new configuration
                                                pi_diagram_config.SetValue(swap_diagram.Diagram, new_right_config, null);
                                                swap_diagram.Diagram.Visibility = Visibility.Visible;
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        // das diagram im canvas nach links schieben
                        double dCurrentLeft = Canvas.GetLeft(_current_base_control.Diagram);
                        if (double.IsNaN(dCurrentLeft) || double.IsInfinity(dCurrentLeft))
                            dCurrentLeft = 0.0;

                        Canvas.SetLeft(swap_diagram.Diagram, dCurrentLeft + _current_base_control.Diagram.ActualWidth);
                    }

                    // die positionen sollten nach dem swapen der diagramme richtig berechnet werden
                    _send_base_diagram_changed_after_slide_animation = true;
                    ResetDiagramPositions();
                }
                else
                {
                    ResetDiagramPositions();
                    return false;
                }
            }

            return true;
        }

        private void ResetDiagramPositions()
        {
            if (IsAnimating)
                return;

            _base_canvas.IsHitTestVisible = false;

            int nLeftCnt = 1;
            foreach (DiagramMetaInfo left_control in _left_diagrams)
            {
                double dCurrentLeft = Canvas.GetLeft(left_control.Diagram);
                if (double.IsNaN(dCurrentLeft) || double.IsInfinity(dCurrentLeft))
                    dCurrentLeft = 0.0;

                Storyboard slide_anim = GenerateResetPositionStoryboard(dCurrentLeft, left_control.Diagram.ActualWidth * -1 * (nLeftCnt++));
                left_control.Diagram.BeginStoryboard(slide_anim);
                IsAnimating = true;
            }

            int nRightCnt = 1;
            foreach (DiagramMetaInfo right_control in _right_diagrams)
            {
                double dCurrentLeft = Canvas.GetLeft(right_control.Diagram);
                if (double.IsNaN(dCurrentLeft) || double.IsInfinity(dCurrentLeft))
                    dCurrentLeft = 0.0;

                Storyboard slide_anim = GenerateResetPositionStoryboard(dCurrentLeft, right_control.Diagram.ActualWidth * (nRightCnt++));
                right_control.Diagram.BeginStoryboard(slide_anim);
                IsAnimating = true;
            }

            if (_current_base_control != null)
            {
                double dCurrentLeft = Canvas.GetLeft(_current_base_control.Diagram);
                if (double.IsNaN(dCurrentLeft) || double.IsInfinity(dCurrentLeft))
                    dCurrentLeft = 0.0;

                Storyboard slide_anim = GenerateResetPositionStoryboard(dCurrentLeft, 0);
                slide_anim.Completed += new EventHandler(slide_anim_Completed);

                _current_base_control.Diagram.BeginStoryboard(slide_anim);
                IsAnimating = true;
            }
        }

        void slide_anim_Completed(object sender, EventArgs e)
        {
            ((ClockGroup)sender).Completed -= slide_anim_Completed;

            ResetSlideAnimations();

            Timer t1 = new Timer();
            t1.Elapsed += new ElapsedEventHandler(t1_Elapsed);
            t1.Interval = 150;
            t1.Start();
        }

        void t1_Elapsed(object sender, ElapsedEventArgs e)
        {
            Timer t1 = sender as Timer;
            t1.Elapsed -= t1_Elapsed;
            t1.Stop();
            t1.Close();
            t1.Dispose();
            t1 = null;


            try
            {
                if (_send_base_diagram_changed_after_slide_animation)
                {
                    if (Application.Current.Dispatcher.CheckAccess())
                    {
                        if (_before_change_base_control != null)
                            OnBeforeBaseDiagramChange(_before_change_base_control, _current_base_control);

                        if (_current_base_control != null)
                            OnBaseDiagramChanged(_current_base_control);

                        _send_base_diagram_changed_after_slide_animation = false;
                    }
                    else
                    {
                        Application.Current.Dispatcher.Invoke(DispatcherPriority.Normal, (NoParas)delegate
                        {
                            if (_before_change_base_control != null)
                                OnBeforeBaseDiagramChange(_before_change_base_control, _current_base_control);

                            if (_current_base_control != null)
                                OnBaseDiagramChanged(_current_base_control);

                            _send_base_diagram_changed_after_slide_animation = false;
                        });
                    }
                }
            }
            finally
            {
                IsAnimating = false;
            }
        }

        private void ResetSlideAnimations()
        {
            if (_current_base_control != null)
            {
                _current_base_control.Diagram.BeginAnimation(Canvas.LeftProperty, null);
                Canvas.SetLeft(_current_base_control.Diagram, 0);
            }

            int nLeftCnt = 1;
            foreach (DiagramMetaInfo left_control in _left_diagrams)
            {
                left_control.Diagram.BeginAnimation(Canvas.LeftProperty, null);
                if (_current_base_control != null)
                    Canvas.SetLeft(left_control.Diagram, _current_base_control.Diagram.ActualWidth * -1 * (nLeftCnt++));
                else
                    Canvas.SetLeft(left_control.Diagram, left_control.Diagram.ActualWidth * -1 * (nLeftCnt++));
            }

            int nRightCnt = 1;
            foreach (DiagramMetaInfo right_control in _right_diagrams)
            {
                right_control.Diagram.BeginAnimation(Canvas.LeftProperty, null);
                if (_current_base_control != null)
                    Canvas.SetLeft(right_control.Diagram, _current_base_control.Diagram.ActualWidth * (nRightCnt++));
                else
                    Canvas.SetLeft(right_control.Diagram, right_control.Diagram.ActualWidth * (nRightCnt++));
            }

            _base_canvas.IsHitTestVisible = true;
        }

        private Storyboard GenerateResetPositionStoryboard(double current_left, double target_left)
        {
            DoubleAnimation left_out_animation = new DoubleAnimation(current_left, target_left, new Duration(TimeSpan.FromMilliseconds(RESET_POSITION_ANIMATION_TIME_IN_MS)), FillBehavior.Stop);
            Storyboard.SetTargetProperty(left_out_animation, new PropertyPath(Canvas.LeftProperty));

            Storyboard sb_return = new Storyboard();
            sb_return.AccelerationRatio = 0.1;
            sb_return.DecelerationRatio = 0.4;
            sb_return.Children.Add(left_out_animation);

            return sb_return;
        }

        /// <summary>
        /// Gibt an, dass mind ein diagramm im diagram manager daten lädt
        /// </summary>
        protected void SendDiagramLoadCycleStarted()
        {
            if (!Application.Current.Dispatcher.CheckAccess())
            {
                Application.Current.Dispatcher.BeginInvoke((NoParas)delegate { SendDiagramLoadCycleStarted(); }, null);
                return;
            }

            if (DiagramLoadCycleStarted != null)
                DiagramLoadCycleStarted(this, EventArgs.Empty);
        }

        /// <summary>
        /// Gibt an, dass alle diagramme ihren ladecyclus abgeschlossen haben
        /// </summary>
        protected void SendDiagramLoadCycleEnded()
        {
            if (!Application.Current.Dispatcher.CheckAccess())
            {
                Application.Current.Dispatcher.BeginInvoke((NoParas)delegate { SendDiagramLoadCycleEnded(); }, null);
                return;
            }

            if (DiagramLoadCycleEnded != null)
                DiagramLoadCycleEnded(this, EventArgs.Empty);
        }

        #region IDisposable Members

        public void Dispose()
        {
            if (_disposed || _disposing)
                return;

            _disposing = true;

            Dispose(true);

            ClearScrollDiagrams();

            foreach (DiagramMetaInfo uc_diagram in _diagrams)
            {
                if (uc_diagram.Diagram is IDisposable)
                {
                    DeregisterMoveHandlers(uc_diagram.Diagram);
                    ((IDisposable)uc_diagram).Dispose();
                }
            }

            _diagrams.Clear();

            _disposed = true;
        }

        protected virtual void Dispose(bool disposing)
        {

        }

        #endregion

        #endregion

        #region Event Handlers

        void DiagramManager_BeginObjectMove(object sender, EventArgs e)
        {
            if (_x_scale_canvas != null)
                _x_scale_canvas.IsHitTestVisible = false;
        }

        void DiagramManager_EndObjectMove(object sender, EventArgs e)
        {
            if (_x_scale_canvas != null)
                _x_scale_canvas.IsHitTestVisible = true;
        }

        void scroll_control_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (sender is UserControl)
            {
                UserControl sender_control = sender as UserControl;

                bool is_left_diagram = IsLeftScrollDiagram(sender_control);
                bool is_right_diagram = IsRightScrollDiagram(sender_control);

                if (is_left_diagram)
                {
                    Canvas.SetLeft(sender_control, sender_control.ActualWidth * -1);
                }
                else if (is_right_diagram)
                {
                    Canvas.SetLeft(sender_control, sender_control.ActualWidth);
                }
            }
        }

        void _x_scale_canvas_MouseMove(object sender, MouseEventArgs e)
        {
            if (IsMultiTouchEnabled && e.StylusDevice != null)
                return;

            if (!_in_x_scale_zoom_mode && Mouse.LeftButton != MouseButtonState.Pressed)
                return;

            //Debug.WriteLine("Mouse Move " + e.GetPosition(_x_scale_canvas));
            //e.Handled = true;

            Point pt = e.GetPosition(_x_scale_canvas);
            if (_in_x_scale_zoom_mode)
                Mouse.OverrideCursor = Cursors.SizeWE;
            //else
            //    Mouse.OverrideCursor = Cursors.ScrollWE;
            //Debug.WriteLine("Mouse move begin mouse drag old " + _begin_mouse_drag);
            if (_begin_mouse_drag.X == 0 && _begin_mouse_drag.Y == 0)
            {
                _begin_mouse_drag = pt;
                _last_mouse_drag = pt;
                //Debug.WriteLine("Begin drag new: " + _begin_mouse_drag);
            }

            //Debug.WriteLine("MOVE special canvas: " + pt.X + ":" + pt.Y);

            Vector pos_diff = pt - _last_mouse_drag;

            _last_mouse_drag = pt;


            e.Handled = true;
            //Debug.WriteLine("InXScalemode? " + _in_x_scale_zoom_mode + ", pos_diff " + pos_diff);
            //if (_in_x_scale_zoom_mode)// || !CurrentDiagramCanZoomXAxis())
            if (_in_x_scale_zoom_mode || !CurrentDiagramCanScrollDiagrams())
                return;

            if (pos_diff.X == 0 && pos_diff.Y == 0)
                return;

            if (!_in_x_scale_zoom_mode)
                Mouse.OverrideCursor = Cursors.ScrollWE;

            double dCurrentLeft = Canvas.GetLeft(_current_base_control.Diagram);
            if (double.IsNaN(dCurrentLeft) || double.IsInfinity(dCurrentLeft))
                dCurrentLeft = 0.0;

            Canvas.SetLeft(_current_base_control.Diagram, dCurrentLeft + pos_diff.X);

            foreach (DiagramMetaInfo left_control in _left_diagrams)
            {
                dCurrentLeft = Canvas.GetLeft(left_control.Diagram);
                if (double.IsNaN(dCurrentLeft) || double.IsInfinity(dCurrentLeft))
                    dCurrentLeft = 0.0;

                Canvas.SetLeft(left_control.Diagram, dCurrentLeft + pos_diff.X);
            }

            foreach (DiagramMetaInfo right_control in _right_diagrams)
            {
                dCurrentLeft = Canvas.GetLeft(right_control.Diagram);
                if (double.IsNaN(dCurrentLeft) || double.IsInfinity(dCurrentLeft))
                    dCurrentLeft = 0.0;

                Canvas.SetLeft(right_control.Diagram, dCurrentLeft + pos_diff.X);
            }

        }

        void _x_scale_canvas_MouseDown(object sender, MouseButtonEventArgs e)
        {
            _mouse_down = e.GetPosition(_x_scale_canvas);
        }


        void _x_scale_canvas_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (IsMultiTouchEnabled && e.StylusDevice != null)
                return;

            e.Handled = true;
            Point pt = e.GetPosition(_x_scale_canvas);

            Vector pos_diff = pt - _begin_mouse_drag;
            Vector down_diff = pt - _mouse_down;
            double move_percentage = 0;
            double down_percentage = 0;

            if (_x_scale_canvas.ActualWidth > 0)
            {
                move_percentage = pos_diff.X / _x_scale_canvas.ActualWidth;
                down_percentage = down_diff.Length / _x_scale_canvas.ActualWidth;
            }

            if (!_in_x_scale_zoom_mode && CurrentDiagramCanScrollDiagrams() && Math.Abs(move_percentage) > 0.20 && (_begin_mouse_drag.X != 0 || _begin_mouse_drag.Y != 0))
            {
                // swap diagrams
                _x_scale_canvas.ResetCaptureState();
                SwapDiagrams(move_percentage >= 0.0);
            }
            else
            {
                if (Math.Abs(down_percentage) == 0.0 && CurrentDiagramCanZoomXAxis())
                {
                    if (!CurrentDiagramCanZoomXAxis())
                    {
                        _x_scale_canvas.ResetCaptureState();
                        _in_x_scale_zoom_mode = false;

                        return;
                    }

                    if (_in_x_scale_zoom_mode)
                    {
                        _in_x_scale_zoom_mode = false;

                        _x_scale_canvas.ResetCaptureState();
                    }
                    else
                        _in_x_scale_zoom_mode = true;
                }
                else
                {
                    _x_scale_canvas.ResetCaptureState();
                }

                ResetDiagramPositions();
            }

            // reset mouse drag point
            _begin_mouse_drag = new Point(0, 0);
            if (_in_x_scale_zoom_mode)
                Mouse.OverrideCursor = Cursors.SizeWE;
            else
                Mouse.OverrideCursor = null;
        }

        void _x_scale_canvas_MouseLeave(object sender, MouseEventArgs e)
        {
            if (IsMultiTouchEnabled && e.StylusDevice != null)
                return;

            //Debug.WriteLine("Mouse LEAVE Pos: " + e.GetPosition(_x_scale_canvas) + ", begin pos: " + _begin_mouse_drag);
            ResetDiagramPositions();
            // reset mouse drag point
            _begin_mouse_drag = new Point(0, 0);
            Point p = e.GetPosition(_x_scale_canvas);
            if (_current_base_control.Diagram is DiagramBase<DateTime, int>)
            {
                AxisScaleConfig<DateTime> config = ((DiagramBase<DateTime, int>)_current_base_control.Diagram).XAxisControl.Config;
                if (_in_x_scale_zoom_mode && (p.X < config.Padding.Left || p.X > _x_scale_canvas.ActualWidth - config.Padding.Right
                    || p.Y < config.Padding.Top || p.Y > _x_scale_canvas.ActualHeight - config.Padding.Bottom))
                {
                    _in_x_scale_zoom_mode = false;
                    _x_scale_canvas.ResetCaptureState();
                }
            }
            else if (_current_base_control.Diagram is DiagramBase<Object, double>)
            {
                AxisScaleConfig<object> config = ((DiagramBase<Object, double>)_current_base_control.Diagram).XAxisControl.Config;
                if (_in_x_scale_zoom_mode && (p.X < config.Padding.Left || p.X > _x_scale_canvas.ActualWidth - config.Padding.Right
                    || p.Y < config.Padding.Top || p.Y > _x_scale_canvas.ActualHeight - config.Padding.Bottom))
                {
                    _in_x_scale_zoom_mode = false;
                    _x_scale_canvas.ResetCaptureState();
                }
            }

            Mouse.OverrideCursor = null;
        }

        void _x_scale_canvas_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            if (IsMultiTouchEnabled && e.StylusDevice != null)
                return;

            //Debug.WriteLine("Wheel");

            if (!CurrentDiagramCanZoomXAxis())
                return;

            if (_in_x_scale_zoom_mode)
            {
                //double left = e.GetPosition(_x_scale_canvas).X;
                CurrentDiagramXAxisZoom(e.Delta > 0, e);

            }
        }

        #region x-Scale Events TOUCH
        void _x_scale_touch_intertia_manipulator_ScalingChanged(object sender, DiagramScaleTouchInertiaProcessManipulation.DiagramScaleScaleingEventArgs e)
        {

            if (!CurrentDiagramCanZoomXAxis())
                return;

            _in_x_scale_zoom_mode = true;
            _cumulative_position_change = 0;

            if (_in_x_scale_zoom_mode)
            {
                CurrentDiagramXAxisZoom(e.DeltaScaling > 1, e);
            }
        }

        void _x_scale_touch_intertia_manipulator_TranslationChanged(object sender, DiagramScaleTouchInertiaProcessManipulation.DiagrammScaleMoveEventArgs e)
        {

            if (_in_x_scale_zoom_mode)
                Mouse.OverrideCursor = Cursors.SizeWE;

            Vector pos_diff = new Vector(e.DeltaWidth, e.DeltaHeight);

            if (_in_x_scale_zoom_mode || !CurrentDiagramCanScrollDiagrams())
                return;

            if (pos_diff.X == 0 && pos_diff.Y == 0)
                return;

            if (!_in_x_scale_zoom_mode)
                Mouse.OverrideCursor = Cursors.ScrollWE;

            _cumulative_position_change += pos_diff.X;

            double dCurrentLeft = Canvas.GetLeft(_current_base_control.Diagram);
            if (double.IsNaN(dCurrentLeft) || double.IsInfinity(dCurrentLeft))
                dCurrentLeft = 0.0;

            Canvas.SetLeft(_current_base_control.Diagram, dCurrentLeft + pos_diff.X);

            foreach (DiagramMetaInfo left_control in _left_diagrams)
            {
                dCurrentLeft = Canvas.GetLeft(left_control.Diagram);
                if (double.IsNaN(dCurrentLeft) || double.IsInfinity(dCurrentLeft))
                    dCurrentLeft = 0.0;

                Canvas.SetLeft(left_control.Diagram, dCurrentLeft + pos_diff.X);
            }

            foreach (DiagramMetaInfo right_control in _right_diagrams)
            {
                dCurrentLeft = Canvas.GetLeft(right_control.Diagram);
                if (double.IsNaN(dCurrentLeft) || double.IsInfinity(dCurrentLeft))
                    dCurrentLeft = 0.0;

                Canvas.SetLeft(right_control.Diagram, dCurrentLeft + pos_diff.X);
            }
        }

        void _x_scale_touch_intertia_manipulator_UiManipulationInertiaCompleted(object sender, WpfUtility.UiManipulation.UiManipulationEventArgs e)
        {
            Vector pos_diff = new Vector(_cumulative_position_change, 0);

            double move_percentage = 0;

            if (_x_scale_canvas.ActualWidth > 0)
                move_percentage = pos_diff.X / _x_scale_canvas.ActualWidth;

            _cumulative_position_change = 0;

            if (!_in_x_scale_zoom_mode && CurrentDiagramCanScrollDiagrams() && Math.Abs(move_percentage) > 0.10)
            {
                // swap diagrams
                _x_scale_canvas.ResetCaptureState();
                SwapDiagrams(move_percentage >= 0.0);
            }
            else
            {
                if (Math.Abs(move_percentage) == 0.0 && CurrentDiagramCanZoomXAxis())
                {
                    if (!CurrentDiagramCanZoomXAxis())
                    {
                        _x_scale_canvas.ResetCaptureState();
                        _in_x_scale_zoom_mode = false;

                        return;
                    }

                    if (_in_x_scale_zoom_mode)
                    {
                        _in_x_scale_zoom_mode = false;

                        _x_scale_canvas.ResetCaptureState();
                    }
                }
                else
                {
                    _x_scale_canvas.ResetCaptureState();
                }

                ResetDiagramPositions();
            }

            // reset mouse drag point
            _begin_mouse_drag = new Point(0, 0);
            if (_in_x_scale_zoom_mode)
                Mouse.OverrideCursor = Cursors.SizeWE;
            else
                Mouse.OverrideCursor = null;
        }

        void _x_scale_touch_intertia_manipulator_UiManipulationStarted(object sender, WpfUtility.UiManipulation.UiManipulationEventArgs e)
        {

        }
        #endregion

        void _base_canvas_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            _init_size_changed = true;
            ResetSlideAnimations();
        }


        void sb_Completed()
        {
            GenerateScrollDiagrams();
            AdjustDiagramSlider();

            _sub_menu_panel.IsHitTestVisible = true;
            OnBaseDiagramChanged(_current_base_control);
        }

        void sb_Completed_1(object sender, EventArgs e)
        {
            _overlay_canvas.Children.Remove(_animated_image_small_to_big);

            _base_canvas.Children.Insert(0, _animated_control_small_to_big.Diagram);


            _animated_control_small_to_big = null;
            _animated_image_small_to_big = null;

            _anim_1_complete = true;

            if (_anim_1_complete && _anim_2_complete)
                sb_Completed();
        }

        void sb_Completed_integrated_1(object sender, EventArgs e)
        {
            _overlay_canvas.Children.Remove(_animated_image_small_to_big);

            _animated_control_small_to_big = null;
            _animated_image_small_to_big = null;

            _anim_1_complete = true;

            if (_anim_1_complete && _anim_2_complete)
                sb_Completed();
        }

        void sb_Completed_integrated_1New(object sender, EventArgs e)
        {
            _overlay_canvas.Children.Remove(_animated_image_small_to_big);

            _current_base_control.Diagram.Opacity = 1.0;
            _current_base_control.Diagram.Visibility = Visibility.Visible;
            _base_frame.EnableMouseClicks();

            _animated_control_small_to_big = null;
            _animated_image_small_to_big = null;

            _anim_1_complete = true;

            if (_anim_1_complete && _anim_2_complete)
                sb_Completed();
        }

        void sb_Completed_2(object sender, EventArgs e)
        {
            _overlay_canvas.Children.Remove(_animated_image_big_to_small);

            if (!_animated_control_big_to_small.IsHidden)
            {
                //string title = GetTitleText(_animated_control_big_to_small.Diagram);
                DiagramMetaInfo diagram_meta_info_new = _diagrams.Where(o => o.Diagram.Equals(_animated_control_big_to_small.Diagram)).First();

                SetDiagramProperty("IsMinimized", true, _animated_control_big_to_small.Diagram);

                SubMenuPanelMenuDiagramItem new_item = new SubMenuPanelMenuDiagramItem(diagram_meta_info_new.Title, _animated_control_big_to_small.Diagram, GetMinimizedLegendControl(_animated_control_big_to_small.Diagram), true);
                new_item.Key = diagram_meta_info_new.Title;
                new_item.IsActive = false;
                //DiagramMetaInfo diagram_meta_info =
                //    _diagrams.Where(o => o.Equals(_animated_control_big_to_small)).First();
                _animated_control_big_to_small.SubMenuPanelMenuDiagramItem = new_item;

                //if (_submenu_panel_dictionary.ContainsKey(_animated_control_big_to_small))
                //{
                //    _submenu_panel_dictionary.Remove(_animated_control_big_to_small);
                //}
                //else
                //{
                //    new_item = _submenu_panel_dictionary[_animated_control_big_to_small];
                //    new_item.UserControl = _animated_control_big_to_small;
                //}

                //_submenu_panel_dictionary.Add(_animated_control_big_to_small, new_item);
                _sub_menu_panel.AddItem(new_item);
            }

            _animated_control_big_to_small = null;
            _animated_image_big_to_small = null;

            _anim_2_complete = true;

            if (_anim_1_complete && _anim_2_complete)
                sb_Completed();
        }


        void DiagramManager_Diagram_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            UserControl sender_diagram = sender as UserControl;

            if (sender_diagram == null)
                return;

            if (e.PropertyName == "IsLoading")
            {
                bool new_value = GetIsLoading(sender_diagram);

                if (new_value && !IsLoading)
                {
                    IsLoading = true;
                    SendDiagramLoadCycleStarted();
                }
                else if (!new_value && IsLoading)
                {
                    bool is_any_diagram_loading = IsAnyDiagramLoading();

                    if (!is_any_diagram_loading)
                    {
                        IsLoading = false;
                        SendDiagramLoadCycleEnded();
                    }
                }
            }
        }
        #endregion

        #region Properties
        protected List<DiagramMetaInfo> Diagrams
        {
            get { return _diagrams; }
        }

        protected List<DiagramMetaInfo> LeftScrollDiagrams
        {
            get { return _left_diagrams; }
        }

        protected List<DiagramMetaInfo> RightScrollDiagrams
        {
            get { return _right_diagrams; }
        }

        protected Canvas BaseCanvas
        {
            get { return _base_canvas; }
        }

        protected SubMenuPanel MenuPanel
        {
            get { return _sub_menu_panel; }
        }

        public Canvas OverlayCanvas
        {
            get { return _overlay_canvas; }
        }

        protected BaseFrame BaseFrame
        {
            get { return _base_frame; }
        }

        public int NumberOfScrollDiagrams
        {
            get { return _number_of_scroll_diagrams; }
            set
            {
                if (_number_of_scroll_diagrams != value && _number_of_scroll_diagrams >= 0 && _number_of_scroll_diagrams <= 1)
                {
                    _number_of_scroll_diagrams = value;
                    GenerateScrollDiagrams();
                }
            }
        }

        public DiagramMetaInfo CurrentBaseDiagram
        {
            get { return _current_base_control; }
        }


        public bool IsAnimating
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets a flag if the object has been disposed
        /// </summary>
        public bool Disposed
        {
            get { return _disposed; }
        }
        /// <summary>
        /// Gets a flag if the object is disposing
        /// </summary>
        protected bool Disposing
        {
            get { return _disposing; }
        }

        public bool IsLoading
        {
            get
            {
                return _is_loading;
            }

            private set
            {
                _is_loading = value;
            }
        }
        #endregion

        #region Dependency Properties
        #endregion

        #region Events

        public event EventHandler DiagramLoadCycleStarted;
        public event EventHandler DiagramLoadCycleEnded;
        #endregion

        #region Routed Events
        #endregion

        #region Attributes
        protected delegate void NoParas();

        private bool _is_loading = false;

        private Canvas _base_canvas;
        private SubMenuPanel _sub_menu_panel;
        private Canvas _overlay_canvas;
        private BaseFrame _base_frame;

        private bool _init_size_changed = false;
        protected DiagramMetaInfo _before_change_base_control;
        private bool _send_base_diagram_changed_after_slide_animation = false;

        protected DiagramMetaInfo _current_base_control;

        private SpecialScaleCanvas _x_scale_canvas = null;
        //private Canvas _x_scalue_zoom_canvas = null;
        private Point _begin_mouse_drag = new Point(0, 0);
        private Point _last_mouse_drag = new Point(0, 0);
        private Point _mouse_down = new Point(0, 0);

        private bool _in_x_scale_zoom_mode;

        private List<DiagramMetaInfo> _diagrams = new List<DiagramMetaInfo>();
        //private Dictionary<UserControl, SubMenuPanelMenuDiagramItem> _submenu_panel_dictionary = new Dictionary<UserControl, SubMenuPanelMenuDiagramItem>();

        private DiagramMetaInfo _animated_control_small_to_big;
        private DiagramMetaInfo _animated_control_big_to_small;
        private Image _animated_image_small_to_big;
        private Image _animated_image_big_to_small;

        private FrameworkElement _current_zoom_element;
        private Point _current_destination_point;
        private LoadingControl _current_loading_control;

        private bool _anim_1_complete = false;
        private bool _anim_2_complete = false;

        private int _number_of_scroll_diagrams = 0;

        private string _diagram_panel_name = null;

        private Type _scroll_type = null;
        private List<DiagramMetaInfo> _left_diagrams = new List<DiagramMetaInfo>();
        private List<DiagramMetaInfo> _right_diagrams = new List<DiagramMetaInfo>();

        private DiagramScaleTouchInertiaProcessManipulation _x_scale_touch_intertia_manipulator = null;
        private double _cumulative_position_change = 0;

        private bool _disposed = false;
        private bool _disposing = false;

        //private bool _diagram_is_in_move_mode = false;

        private QueuedBackgroundWorker _main_control_load_pending = null;
        private bool _abort_load_pending = false;
        #endregion

        #region Tests

        #endregion
    }
}
