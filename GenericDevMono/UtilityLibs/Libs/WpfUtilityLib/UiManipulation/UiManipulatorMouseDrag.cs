﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Logicx.WpfUtility.UiManipulation
{
    public class MouseDrag : UiManipulator
    {
        public MouseDrag(UIElement ui_element, Canvas canvas, UIElement event_base, EnabledAxes enabled_axes) : this(ui_element, canvas, event_base, enabled_axes, true) { }
        public MouseDrag(UIElement ui_element, Canvas canvas, UIElement event_base, EnabledAxes enabled_axes, bool initialize_canvas_position) : base(ui_element, canvas, event_base, enabled_axes, initialize_canvas_position) { }

        public override void Register()
        {
            _ui_element.MouseDown += MouseDown;
        }

        public override void DeRegister()
        {
            _ui_element.MouseDown -= MouseDown;
        }

        #region Mouse Drag Event Registration Management
        private void RegisterManipulationEvents()
        {
            if (_events_registered)
                return;

            SetCanvasBackground();
            _ui_element.MouseUp += MouseUp;
            _event_base.MouseUp += MouseUp;
            _event_base.MouseMove += MouseMove;
            _event_base.MouseLeave += MouseLeave;

            _events_registered = true;
        }

        private void DeRegisterManipulationEvents()
        {
            if (!_events_registered)
                return;

            UnsetCanvasBackground();
            _ui_element.MouseUp -= MouseUp;
            _event_base.MouseUp -= MouseUp;
            _event_base.MouseMove -= MouseMove;
            _event_base.MouseLeave -= MouseLeave;

            _events_registered = false;
        }
        #endregion

        #region Mouse Drag Event Handling
        public void MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.StylusDevice != null)
                return;

            if (e.LeftButton != MouseButtonState.Pressed)
                return;

            e.Handled = true;

            RegisterManipulationEvents();
            _last_mouse_pos = e.GetPosition(_canvas);
            RaisUiManipulationStarted(_ui_element);
        }

        public void MouseUp(object sender, MouseEventArgs e)
        {
            if (e.StylusDevice != null)
                return;

            e.Handled = true;

            DeRegisterManipulationEvents();
            RaiseUiManipulationCompleted(_ui_element);
        }

        public void MouseLeave(object sender, MouseEventArgs e)
        {
            e.Handled = true;

            MouseUp(sender, e);
        }

        public virtual void MouseMove(object sender, MouseEventArgs e)
        {
            if (e.StylusDevice != null)
                return;

            e.Handled = true;

            Point pos = e.GetPosition(_canvas);
            Vector v_move = pos - _last_mouse_pos;
            _last_mouse_pos = pos;
            if(_enabled_axes == EnabledAxes.YAxis || _enabled_axes == EnabledAxes.All)
                Canvas.SetTop(_ui_element, Canvas.GetTop(_ui_element) + v_move.Y);
            if (_enabled_axes == EnabledAxes.XAxis || _enabled_axes == EnabledAxes.All)
                Canvas.SetLeft(_ui_element, Canvas.GetLeft(_ui_element) + v_move.X);

            RailsUiElementPositionChanged(_ui_element, (Point)v_move);
        }

        #endregion

        private Point _last_mouse_pos;
        private bool _events_registered;
    }
}
