Documentation and examples are available at www.nhforge.org
Any feedback or issue can be sent to NHibernate user group(http://groups.google.com/group/nhusers) and will be greatly anticipated. 

Up-to-date source code available in the SVN:
http://nhcontrib.svn.sourceforge.net/svnroot/nhcontrib/trunk/src/

Website:
http://sourceforge.net/projects/nhcontrib/

Version: 2.0.0.RC1, Build for NHibernate 2.0.1.GA

======================================
What is NHibernate.Caches?
======================================

NHibernate is able to use external caching plugins to minimize the access to the database and improve the performance.
The NHibernate Contrib contains several packages to work with different caching servers and frameworks. It's recommended to research for a while before deciding which one is better for you, since some providers require installing adicional services (which provides an awesome performance, but might be harder to install in some scenarios)

Currently there's 5 providers defined:

- NHibernate.Caches.MemCache
- NHibernate.Caches.Prevalence
- NHibernate.Caches.SharedCache
- NHibernate.Caches.SysCache
- NHibernate.Caches.SysCache2
- NHibernate.Caches.Velocity
