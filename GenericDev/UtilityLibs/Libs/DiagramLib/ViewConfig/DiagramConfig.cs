﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using Logicx.WpfUtility.WpfHelpers;

namespace Logicx.DiagramLib.ViewConfig
{
    public class RequestConfigEventArgs : EventArgs
    {
        public RequestConfigEventArgs()
        {

        }

        public object ConfigObject { get; set; }
    }

    public class DiagramConfig<Tx, Ty> : DependencyObject, INotifyPropertyChanged, IDisposable
    {
        #region Constants
        #endregion

        #region Construction and Initialization
        #endregion

        #region Operations
        protected void SendPropertyChanged(string property_name)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property_name));
        }

        public void StartRequestRightConfig()
        {
            OnRequestRightConfig();
        }

        public void StartRequestLeftConfig()
        {
            OnRequestLeftConfig();
        }

        private void OnRequestLeftConfig()
        {
            RequestConfigEventArgs left_args = new RequestConfigEventArgs();
            if (RequestLeftConfig != null)
            {
                RequestLeftConfig(this, left_args);
                if (left_args.ConfigObject is DiagramConfig<Tx, Ty>)
                    _left_config = left_args.ConfigObject as DiagramConfig<Tx, Ty>;
                else
                    _left_config = null;
            }
        }

        private void OnRequestRightConfig()
        {
            RequestConfigEventArgs right_args = new RequestConfigEventArgs();
            if (RequestRightConfig != null)
            {
                RequestRightConfig(this, right_args);
                if (right_args.ConfigObject is DiagramConfig<Tx, Ty>)
                    _right_config = right_args.ConfigObject as DiagramConfig<Tx, Ty>;
                else
                    _right_config = null;
            }
        }

        public void InitializeFrom(DiagramConfig<Tx, Ty> source_config)
        {
            if (source_config.Disposed)
                throw new ObjectDisposedException("source_config");

            AllowMultipleSelection = source_config.AllowMultipleSelection;
            GridLineVisibility = source_config.GridLineVisibility;
            AxisScaleConfigX.InitializeFrom(source_config.AxisScaleConfigX);
            AxisScaleConfigY.InitializeFrom(source_config.AxisScaleConfigY);
        }

        #region IDisposable Members

        public void Dispose()
        {
            if (_disposed || _disposing)
                return;

            _disposing = true;

            EventHelper.UnregisterEvents(PropertyChanged);

            if(RequestRightConfig != null)
                foreach (EventHandler<RequestConfigEventArgs> eventDelegate in RequestRightConfig.GetInvocationList())
                    RequestRightConfig -= eventDelegate;

            if(RequestLeftConfig != null)
                foreach (EventHandler<RequestConfigEventArgs> eventDelegate in RequestLeftConfig.GetInvocationList())
                    RequestLeftConfig -= eventDelegate;

            if (AxisScaleConfigX != null)
                AxisScaleConfigX.Dispose();

            if (AxisScaleConfigY != null)
                AxisScaleConfigY.Dispose();

            Dispose(true);
            _disposed = true;
        }

        protected virtual void Dispose(bool disposing)
        {

        }


        #endregion
        #endregion

        #region Event Handlers
        #endregion

        #region Properties

        public AxisScaleConfig<Tx> AxisScaleConfigX
        {
            get;
            set;
        }

        public AxisScaleConfig<Ty> AxisScaleConfigY
        {
            get;
            set;
        }

        public bool AllowMultipleSelection
        {
            get;
            set;
        }

        public GridLineVisibility GridLineVisibility
        {
            get { return _grid_line_visibility;}
            set
            {
                if(value != _grid_line_visibility)
                {
                    _grid_line_visibility = value;
                    SendPropertyChanged("GridLineVisibility");
                }
            }
        }

        public DiagramConfig<Tx, Ty> RightConfig
        {
            get { return _right_config; }
        }

        public DiagramConfig<Tx, Ty> LeftConfig
        {
            get { return _left_config; }
        }

        /// <summary>
        /// Gets a flag if the object has been disposed
        /// </summary>
        public bool Disposed
        {
            get { return _disposed; }
        }
        /// <summary>
        /// Gets a flag if the object is disposing
        /// </summary>
        protected bool Disposing
        {
            get { return _disposing; }
        }
        #endregion

        #region Dependency Properties
        #endregion

        #region Events
        public event PropertyChangedEventHandler PropertyChanged;
        public event EventHandler<RequestConfigEventArgs> RequestRightConfig;
        public event EventHandler<RequestConfigEventArgs> RequestLeftConfig;
        #endregion

        #region Routed Events
        #endregion

        #region Attributes

        DiagramConfig<Tx, Ty> _left_config = null;
        DiagramConfig<Tx, Ty> _right_config = null;

        private GridLineVisibility _grid_line_visibility = GridLineVisibility.None;
        private bool _disposed = false;
        private bool _disposing = false;
        #endregion

    }
}
