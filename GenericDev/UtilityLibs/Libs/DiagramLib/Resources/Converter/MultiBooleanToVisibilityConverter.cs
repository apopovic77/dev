﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Data;

namespace Logicx.DiagramLib.Resources.Converter
{
    public class MultiBooleanToVisibilityConverter : IMultiValueConverter
    {
        #region IMultiValueConverter Members

        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            bool bAndCombined = true;
            bool bOrCOmbined = false;

            foreach(object val in values)
            {
                if(val is bool)
                {
                    bAndCombined &= (bool)val;
                    bOrCOmbined |= (bool)val;
                }
            }

            if(bAndCombined)
                return Visibility.Visible;

            return Visibility.Collapsed;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }

        #endregion
    }
}
