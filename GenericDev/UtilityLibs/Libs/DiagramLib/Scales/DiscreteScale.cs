﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using Logicx.DiagramLib.Util;

namespace Logicx.DiagramLib.Scales
{
    class DiscreteScale : ScaleType
    {

        #region Constants

        public const double UNIT_SIZE_FACTOR = 0.9;

        private const long ONE_OUR_MSEC = 3600l * 1000; // * 10000000;
        private const long ONE_DAY_MSEC = 24 * ONE_OUR_MSEC;

        private const short LOD_1 = 1; // 1 unit is one hour
        private const short LOD_3 = 3; // 1 unit is three ours
        private const short LOD_6 = 6;
        private const short LOD_12 = 12;
        private const short LOD_24 = 24;

        private const double LONG_LINE_HEIGHT = 9;
        private const double MIDDLE_LINE_HEIGHT = 5;
        private const double SHORT_LINE_HEIGHT = 3;

        private const int MIN_LINE_DISTANCE = 10;
        private const double MIN_SCALE_DISTANCE = 30;

        private short[] _hours = { 1, 3, 6, 12 };

        #endregion

        #region Construction and Initialization
        #endregion

        #region Operations

        public override void Init(double size, bool generate_always)
        {
            _discrete_scale_control.ClearChildren();
            GenerateData();
            AddChildren();
        }

        private void GenerateData()
        {
            _outer_lines.Clear();
            _long_lines.Clear();
            _middle_lines.Clear();
            _short_lines.Clear();
            _long_texts.Clear();
            _long_dates.Clear();

            _description_text = null;

            GenerateDayAndSubData();
        }

        private void GenerateDayAndSubData()
        {
            //Debug.WriteLine("************************ GENERATE DATA *******************************");
            List<KeyValuePair<object, double>> discrete_objects = _discrete_scale_control.DiscreteObjects;

            double height_outer = _discrete_scale_control.Config.Orientation == Orientation.Horizontal
                                 ? _discrete_scale_control.GetRenderHeight()
                                 : 1;
            double width_outer = _discrete_scale_control.Config.Orientation == Orientation.Horizontal
                                ? 1
                                : _discrete_scale_control.GetRenderWidth();
            double middle_height = _discrete_scale_control.Config.Orientation == Orientation.Horizontal
                               ? MIDDLE_LINE_HEIGHT
                               : 1;
            double middle_width = _discrete_scale_control.Config.Orientation == Orientation.Horizontal
                               ? 1
                               : MIDDLE_LINE_HEIGHT;

            _description_text = GeometryUtils.GenerateTextBlock(_discrete_scale_control.Config.Description, TextStyleType.InfoText, typeof(DiscreteScaleControl<object>), _discrete_scale_control);

            if (_discrete_scale_control.Config.Orientation == Orientation.Vertical)
                _description_text.LayoutTransform = new RotateTransform(270);

            Rectangle rectangle = GeometryUtils.GenerateRectange(height_outer, width_outer, typeof(DiscreteScaleControl<object>), _discrete_scale_control);
            _outer_lines.Add(rectangle);
            rectangle = GeometryUtils.GenerateRectange(height_outer, width_outer, typeof(DiscreteScaleControl<object>), _discrete_scale_control);
            _outer_lines.Add(rectangle);

            for (int obj_count = 0; obj_count < discrete_objects.Count; obj_count++)
            {
                KeyValuePair<object, double> key_value_pair = discrete_objects[obj_count];

                TextBlock b = GeometryUtils.GenerateTextBlock(key_value_pair.Key.ToString(), TextStyleType.ScaleText, typeof(DiscreteScaleControl<object>), _discrete_scale_control);
                _long_texts.Add(b);

                Rectangle r = GeometryUtils.GenerateRectange(middle_height, middle_width, typeof(DiscreteScaleControl<object>), _discrete_scale_control);
                _middle_lines.Add(r);
            }
        }

        private void AddChildren()
        {
            List<UIElement> elements = new List<UIElement>();

            elements.AddRange(_short_lines.Cast<UIElement>());
            elements.AddRange(_middle_lines.Cast<UIElement>());
            elements.AddRange(_long_lines.Cast<UIElement>());
            elements.AddRange(_outer_lines.Cast<UIElement>());
            elements.AddRange(_long_texts.Cast<UIElement>());
            elements.AddRange(_long_dates.Cast<UIElement>());

            elements.Add(_description_text);

            _discrete_scale_control.AddToChildren(elements);
        }

        public override void AlignData(Orientation orientation)
        {
            //Debug.WriteLine("************************ ALIGN DATA *******************************");
            Rectangle rectangle1 = _outer_lines.ToArray()[0];
            Rectangle rectangle2 = _outer_lines.ToArray()[1];
            if (orientation == Orientation.Horizontal)
            {
                //Canvas.SetLeft(rectangle1, _discrete_scale_control.Config.Padding.Left);
                //Canvas.SetBottom(rectangle1, _discrete_scale_control.Config.Padding.Bottom);
                //Canvas.SetLeft(rectangle2, _discrete_scale_control.Config.Padding.Left + _discrete_scale_control.GetRenderWidth());
                //Canvas.SetBottom(rectangle2, _discrete_scale_control.Config.Padding.Bottom);

                //Canvas.SetLeft(_description_text, _discrete_scale_control.Config.Padding.Left + 10);
                //Canvas.SetBottom(_description_text, _discrete_scale_control.Config.Padding.Bottom);
                Canvas.SetLeft(rectangle1, 0);
                Canvas.SetBottom(rectangle1, 0);
                Canvas.SetLeft(rectangle2, _discrete_scale_control.GetRenderWidth() - 1);
                Canvas.SetBottom(rectangle2, 0);

                Canvas.SetLeft(_description_text, 10);
                Canvas.SetBottom(_description_text, 0);
            }
            else
            {
                //Canvas.SetLeft(rectangle1, _discrete_scale_control.Config.Padding.Left);
                //Canvas.SetTop(rectangle1, _discrete_scale_control.Config.Padding.Top);
                //Canvas.SetLeft(rectangle2, _discrete_scale_control.Config.Padding.Left);
                //Canvas.SetBottom(rectangle2, _discrete_scale_control.Config.Padding.Bottom);

                //Canvas.SetLeft(_description_text, _discrete_scale_control.Config.Padding.Left);
                //Canvas.SetBottom(_description_text, _discrete_scale_control.Config.Padding.Bottom + 10);
                Canvas.SetLeft(rectangle1, 0);
                Canvas.SetTop(rectangle1, 0);
                Canvas.SetLeft(rectangle2, 0);
                Canvas.SetBottom(rectangle2, 0);

                Canvas.SetLeft(_description_text, 0);
                Canvas.SetBottom(_description_text, 10);
            }


            if (_long_texts.Count == 0)
                return;

            List<KeyValuePair<object, double>> discrete_objects = _discrete_scale_control.DiscreteObjects;

            for (int obj_count = 0; obj_count < discrete_objects.Count; obj_count++)
            {
                TextBlock text_block = _long_texts.ToArray()[obj_count];
                KeyValuePair<object, double> key_value_pair = discrete_objects[obj_count];

                FormattedText ft = new FormattedText(text_block.Text, Thread.CurrentThread.CurrentCulture, FlowDirection.LeftToRight, new Typeface(TextBlock.FontStyleProperty.Name), 10, Brushes.Black);

                double pos = key_value_pair.Value -_discrete_scale_control.Config.Padding.Left;
                double pos_text = pos - ft.Width / 2;

                if (orientation == Orientation.Horizontal)
                {
                    Canvas.SetLeft(text_block, pos_text);
                    //Canvas.SetTop(text_block, _discrete_scale_control.Config.Padding.Top + LONG_LINE_HEIGHT + 3);
                    Canvas.SetTop(text_block, LONG_LINE_HEIGHT + 3);
                }
                else
                {
                    //Canvas.SetLeft(text_block, _discrete_scale_control.Config.Padding.Left + LONG_LINE_HEIGHT + 3);
                    Canvas.SetLeft(text_block, LONG_LINE_HEIGHT + 3);
                    Canvas.SetBottom(text_block, pos_text);
                }

                Rectangle rectangle = _middle_lines.ToArray()[obj_count];
                if (orientation == Orientation.Horizontal)
                {
                    Canvas.SetLeft(rectangle, pos);
                    //Canvas.SetTop(rectangle, _discrete_scale_control.Config.Padding.Top);
                    Canvas.SetTop(rectangle, 0);
                }
                else
                {
                    //Canvas.SetLeft(rectangle, _discrete_scale_control.Config.Padding.Left);
                    Canvas.SetLeft(rectangle, 0);
                    Canvas.SetBottom(rectangle, pos);
                }
            }
        }

        public override double GetUnitSize(Orientation orientation)
        {
            int num_units = _discrete_scale_control.Config.Values.Count;

            if (orientation == Orientation.Horizontal)
                return _discrete_scale_control.GetRenderWidth() / num_units;

            return _discrete_scale_control.GetRenderHeight() / num_units;
        }

        public override double GetUnitOffset(Orientation orientation)
        {
            return _unit_offset;
        }

        public override double GetUnitOffset()
        {
            throw new NotImplementedException("GetUnitOffset is not required in DiscreteScale");
        }

        private static double GetMaxDescriptionSize()
        {
            return -1;
        }

        public override List<double> GetGridLinePositions()
        {
            List<double> positions = new List<double>();

            double current_pos = GetUnitOffset(_discrete_scale_control.Config.Orientation);
            double unit_size = GetUnitSize(_discrete_scale_control.Config.Orientation);

            if (_discrete_scale_control.Config.Orientation == Orientation.Horizontal)
            {
                //current_pos += _discrete_scale_control.Config.Padding.Left;
                double limit = _discrete_scale_control.GetRenderWidth();
                while (current_pos < limit)
                {
                    positions.Add(current_pos);
                    current_pos += unit_size;
                }
            }
            else
            {
                //current_pos = _discrete_scale_control.GetRenderHeight() + _discrete_scale_control.Config.Padding.Top - (current_pos);
                current_pos = _discrete_scale_control.GetRenderHeight()- current_pos;
                double limit = 0;
                while (current_pos > limit)
                {
                    positions.Add(current_pos);
                    current_pos -= unit_size;
                }
            }

            return positions;
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            _discrete_scale_control = null;
            _description_text = null;
            _outer_lines.Clear();
            _long_lines.Clear();
            _middle_lines.Clear();
            _short_lines.Clear();
            _long_texts.Clear();
            _long_dates.Clear();
        }
        #endregion

        #region Event Handlers
        #endregion

        #region Properties

        internal override Object Control
        {
            get
            {
                return _discrete_scale_control;
            }
            set
            {
                if (value == null)
                    throw new ArgumentException("Control must not be null!");

                if (value is DiscreteScaleControl<Object>)
                {
                    _discrete_scale_control = (DiscreteScaleControl<Object>)value;
                }
                else
                    throw new ArgumentException("Type " + value.GetType());
            }
        }

        #endregion

        #region Dependency Properties
        #endregion

        #region Events
        #endregion

        #region Routed Events
        #endregion

        #region Attributes

        private DiscreteScaleControl<Object> _discrete_scale_control;

        private double _unit_offset;

        private readonly List<Rectangle> _outer_lines = new List<Rectangle>();
        private readonly List<Rectangle> _long_lines = new List<Rectangle>();
        private readonly List<Rectangle> _middle_lines = new List<Rectangle>();
        private readonly List<Rectangle> _short_lines = new List<Rectangle>();

        private readonly List<TextBlock> _long_texts = new List<TextBlock>();
        private readonly List<TextBlock> _long_dates = new List<TextBlock>();

        private TextBlock _description_text;

        #endregion

        #region Tests
        #endregion

    }
}
