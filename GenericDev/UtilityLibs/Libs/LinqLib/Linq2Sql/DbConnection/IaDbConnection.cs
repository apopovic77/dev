﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;

namespace LinqLib.Linq2Sql.DbConnection
{
    public class IaDbConnection : System.Data.Common.DbConnection
    {
        #region contests
        private const string IADATACONTEXT_MUTEXT_NAME = "IaDataContext_MultiProcess_Mutex";
        #endregion

        #region construction and initialization
        public IaDbConnection()
        {
            _internal_connection = new SqlConnection();
        }

        public IaDbConnection(string connectionString)
        {
            _internal_connection = new SqlConnection(connectionString);
        }

#if DEBUG
        public IaDbConnection(bool with_debug)
        {
            _with_debug_output = with_debug;
            _internal_connection = new SqlConnection();
        }

        public IaDbConnection(string connectionString, bool with_debug)
        {
            _with_debug_output = with_debug;
            _internal_connection = new SqlConnection(connectionString);
        }
#endif
        #endregion


        #region Context locking/unlocking methods
        public void LockContext(string debug_info)
        {
            Monitor.Enter(LockParallelSystem);
            _current_lock = Guid.NewGuid().ToString();

            if (debug_info == null)
                debug_info = string.Empty;
#if DEBUG
            if(_with_debug_output)
                System.Diagnostics.Debug.WriteLine(DebugIdentifier + "#### ENTER LOCK - (" + debug_info +") - (" + _current_lock + "), Thread: " + System.Threading.Thread.CurrentThread.Name);
#endif
        }

        public void FreeContext(string debug_info)
        {
            string old_lock_id = _current_lock;
            _current_lock = "";

            Monitor.Pulse(LockParallelSystem);
            Monitor.Exit(LockParallelSystem);

            if (debug_info == null)
                debug_info = string.Empty;
#if DEBUG
            if(_with_debug_output)
                System.Diagnostics.Debug.WriteLine(DebugIdentifier + "#### EXIT LOCK - (" + debug_info + ") - (" + old_lock_id + "), Thread: " + System.Threading.Thread.CurrentThread.Name);
#endif
            
            
        }

        public bool TryLockContext()
        {
            return Monitor.TryEnter(LockParallelSystem);
        }

        public bool IsContextLocked()
        {
            bool bRet = Monitor.TryEnter(LockParallelSystem);
            if (bRet)
            {
                FreeContext("IsContextLocked");
                return false;
            }

            return true;
        }
        #endregion

        #region DbConnection implementation
        protected override DbTransaction BeginDbTransaction(IsolationLevel isolationLevel)
        {
            IaDbTransaction trans = new IaDbTransaction(this, isolationLevel);
            return trans;
            //return _internal_connection.BeginTransaction(isolationLevel);
        }

        public override void ChangeDatabase(string databaseName)
        {
            LockContext("ChangeDatabase");
            EnsureOpenConnection();
            _internal_connection.ChangeDatabase(databaseName);
            FreeContext("ChangeDatabase");
        }

        public override string ConnectionString
        {
            get
            {
                return _internal_connection.ConnectionString;
            }
            set
            {
                _internal_connection.ConnectionString = value;
            }
        }

        public override int ConnectionTimeout
        {
            get
            {
                return _internal_connection.ConnectionTimeout;
            }
        }

        protected override DbCommand CreateDbCommand()
        {
#if DEBUG
            if (_with_debug_output)
            {
                System.Diagnostics.Debug.WriteLine(DebugIdentifier + "#### CreateDbCommand() - IaDbConnection");
                System.Diagnostics.Debug.WriteLine(DebugIdentifier + "####### Conn-Hash: " + this.GetHashCode().ToString() + ", Thread: " + System.Threading.Thread.CurrentThread.Name);
            }
#endif

            return new IaDbCommand(null, this); // _internal_connection.CreateCommand();
        }

        public override System.Runtime.Remoting.ObjRef CreateObjRef(Type requestedType)
        {
            return _internal_connection.CreateObjRef(requestedType);
        }

        protected override void Dispose(bool disposing)
        {
            //try
            //{
            //    _internal_connection.Dispose();
            //}
            //catch
            //{
            //}
        }

        public void ForceDispose()
        {
            try
            {
                _internal_connection.Dispose();
            }
            catch
            {
            }
        }

        public override DataTable GetSchema()
        {
            LockContext("GetSchema");
            EnsureOpenConnection();
            DataTable dtSchema = _internal_connection.GetSchema();
            FreeContext("GetSchema");
            return dtSchema;
        }

        public override DataTable GetSchema(string collectionName)
        {
            LockContext("GetSchema(collectionName)");
            EnsureOpenConnection();
            DataTable dtSchema = _internal_connection.GetSchema(collectionName);
            FreeContext("GetSchema(collectionName)");
            return dtSchema;
        }

        public override DataTable GetSchema(string collectionName, string[] restrictionValues)
        {
            LockContext("GetSchema(collectionName,restrictionValues)");
            EnsureOpenConnection();
            DataTable dtSchema = _internal_connection.GetSchema(collectionName, restrictionValues);
            FreeContext("GetSchema(collectionName,restrictionValues)");
            return dtSchema;
        }

        public override void Open()
        {
            LockContext("Open");
            EnsureOpenConnection();
            FreeContext("Open");
        }

        public override void Close()
        {
//            LockContext("Close");
//#if DEBUG
//            System.Diagnostics.Debug.WriteLine(DebugIdentifier + "#### Close() - IaDbConnection");
//#endif
//            _internal_connection.Close();
//            FreeContext("Close");
        }

        internal void EnsureOpenConnection()
        {
            int nTries = 0;
            int nMaxTries = 1000;

            do
            {
                if (_internal_connection.State == ConnectionState.Closed || _internal_connection.State == ConnectionState.Broken)
                {
                    _internal_connection.Open();
                    Thread.Sleep(10);
                }

                nTries++;
            } while (_internal_connection.State != ConnectionState.Open && nTries < nMaxTries);

        }
        #endregion

        public bool HasRunningReader()
        {
            return _executing_data_reader != null;
        }
       

        #region Properties

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public virtual string DebugIdentifier
        {
            get
            {
                return "###" + (string.IsNullOrEmpty(_name) ? "" : "{"+_name+"}") + "C" + this.GetHashCode().ToString();
            }
        }

        public override string Database
        {
            get
            {
                LockContext("Database");
                EnsureOpenConnection();
                string sRet = _internal_connection.Database;
                FreeContext("Database");
                return sRet;
            }
        }

        public override string DataSource
        {
            get
            {
                LockContext("DataSource");
                EnsureOpenConnection();
                string sRet = _internal_connection.DataSource;
                FreeContext("DataSource");
                return sRet;
            }
        }

        protected override DbProviderFactory DbProviderFactory
        {
            get
            {
                return SqlClientFactory.Instance;
            }
        }

        public override string ServerVersion
        {
            get
            {
                if (string.IsNullOrEmpty(_server_version))
                {
                    LockContext("ServerVersion");
                    EnsureOpenConnection();
                    if (string.IsNullOrEmpty(_server_version))
                    {
                        string sRet = _internal_connection.ServerVersion;
                        _server_version = sRet;
                    }
                    FreeContext("ServerVersion");
                }

                return _server_version;
            }
        }

        public override System.ComponentModel.ISite Site
        {
            get
            {
                return _internal_connection.Site;
            }
            set
            {
                _internal_connection.Site = value;
            }
        }

        public override ConnectionState State
        {
            get
            {
                return _internal_connection.State;
            }
        }

        public SqlConnection SqlConnection
        {
            get { return _internal_connection; }   
        }

        public bool MultiProcessLocking
        {
            get { return _multi_process_locking; }   
            set
            {
                if(_multi_process_locking != value)
                {
                    while (IsContextLocked())
                    {
                        Thread.Sleep(10);
                    }

                    _multi_process_locking = value;
                }
            }
        }

        public DataContextEx AssocDataContext
        {
            get { return _assoc_data_context; }
            set { _assoc_data_context = value; }
        }

#if DEBUG
        public bool WithDebugOutput
        {
            get { return _with_debug_output; }
            set { _with_debug_output = value; }
        }
#endif

        #endregion

        #region Attribs

        private string _name = "";
        private SqlConnection _internal_connection = null;
        private Mutex _multi_process_mutext = null;
        private bool _multi_process_locking = false;
        private IaDbCommand _executing_data_reader = null;
        private DataContextEx _assoc_data_context = null;

        private string _current_lock = "";
        private string _server_version = "";
        
        private bool _with_debug_output = false;

        public object LockParallelSystem = new object();

        #endregion
    }
}