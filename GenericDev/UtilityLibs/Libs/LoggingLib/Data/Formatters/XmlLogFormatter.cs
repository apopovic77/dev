﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using LoggingLib.Interfaces;

namespace LoggingLib.Data.Formatters
{
    /// <summary>
    /// Creates an XML document encapsulating all log information 
    /// </summary>
    public class XmlLogFormatter : ILogFormatter 
    {
        public string FormatLogEntry(LogEntry entry)
        {
            XmlDocument xmlDoc = new XmlDocument();
            XmlNode rootNode;
            rootNode = xmlDoc.CreateElement("FormattedMessage");
            XmlNode typeInfo = xmlDoc.CreateElement("LogType");
            typeInfo.InnerText = entry.GetType().ToString();
            rootNode.AppendChild(typeInfo);


            GenerateBasicXmlNodes(xmlDoc, rootNode, entry);

            if (entry is SystemInfoLogEntry)
                GenerateSystemInfoXmlNodes(xmlDoc, rootNode, entry as SystemInfoLogEntry);
            else if (entry is AppSessionLogEntry)
                GenerateAppSessionXmlNodes(xmlDoc, rootNode, entry as AppSessionLogEntry);
            else if (entry is UserActionLogEntry)
                GenerateUserActionXmlNodes(xmlDoc, rootNode, entry as UserActionLogEntry);
            else if (entry is AppMessageLogEntry)
                GenerateAppMessageXmlNodes(xmlDoc, rootNode, entry as AppMessageLogEntry);

            xmlDoc.AppendChild(rootNode);

            StringBuilder sbRet = new StringBuilder();
            StringWriter writer = new StringWriter(sbRet);
            xmlDoc.Save(writer);
            writer.Close();

            return sbRet.ToString();
        }

        private void GenerateBasicXmlNodes(XmlDocument xmlDoc, XmlNode rootNode, LogEntry entry)
        {
            XmlNode nodeLogId = xmlDoc.CreateElement("LogId");
            nodeLogId.InnerText = entry.LogId.ToString();
            rootNode.AppendChild(nodeLogId);

            XmlNode nodeProductName = xmlDoc.CreateElement("Product");
            nodeProductName.InnerText = entry.ProductName;
            rootNode.AppendChild(nodeProductName);

            XmlNode nodeSessionId = xmlDoc.CreateElement("SessionId");
            nodeSessionId.InnerText = entry.SessionId;
            rootNode.AppendChild(nodeSessionId);

            XmlNode nodeEventId = xmlDoc.CreateElement("EventId");
            nodeEventId.InnerText = entry.EventId.ToString();
            rootNode.AppendChild(nodeEventId);

            XmlNode nodePriority = xmlDoc.CreateElement("Priority");
            nodePriority.InnerText = entry.Priority.ToString();
            rootNode.AppendChild(nodePriority);

            XmlNode nodeSeverity = xmlDoc.CreateElement("Severity");
            nodeSeverity.InnerText = entry.Severity.ToString();
            rootNode.AppendChild(nodeSeverity);

            XmlNode nodeTimeStamp = xmlDoc.CreateElement("Timestamp");
            nodeTimeStamp.InnerText = entry.LogTimestamp.ToString();
            rootNode.AppendChild(nodeTimeStamp);

            XmlNode nodeActivityId = xmlDoc.CreateElement("ActivityId");
            nodeActivityId.InnerText = entry.ActivityId;
            rootNode.AppendChild(nodeActivityId);

            XmlNode nodeActivityDescr = xmlDoc.CreateElement("ActivityDescription");
            nodeActivityDescr.InnerText = entry.ActivityDescription;
            rootNode.AppendChild(nodeActivityDescr);

            XmlNode nodeRelatedActivityId = xmlDoc.CreateElement("RelatedActivityId");
            nodeRelatedActivityId.InnerText = entry.RelatedActivityId;
            rootNode.AppendChild(nodeRelatedActivityId);

            XmlNode nodeCategories = xmlDoc.CreateElement("Categories");

            if (entry.CategoryLogEntries != null)
            {
                foreach (CategoryLogEntry cle in entry.CategoryLogEntries)
                {
                    XmlNode nodeCategory = xmlDoc.CreateElement("Category");
                    nodeCategory.InnerText = cle.CategoryName;
                    nodeCategories.AppendChild(nodeCategory);
                }
            } 
            else
            {
                foreach (string catName in entry.UnattachedCategtories)
                {
                    XmlNode nodeCategory = xmlDoc.CreateElement("Category");
                    nodeCategory.InnerText = catName;
                    nodeCategories.AppendChild(nodeCategory);
                }
            }

            rootNode.AppendChild(nodeCategories);

            XmlNode nodeMachineName = xmlDoc.CreateElement("MachineName");
            nodeMachineName.InnerText = entry.MachineName;
            rootNode.AppendChild(nodeMachineName);

            XmlNode nodeDomainLoginName = xmlDoc.CreateElement("DomainLoginName");
            nodeDomainLoginName.InnerText = entry.DomainLoginName;
            rootNode.AppendChild(nodeDomainLoginName);

            XmlNode nodeMessage = xmlDoc.CreateElement("Message");
            nodeMessage.InnerText = entry.Message;
            rootNode.AppendChild(nodeMessage);

        }

        private void GenerateSystemInfoXmlNodes(XmlDocument xmlDoc, XmlNode rootNode, SystemInfoLogEntry entry)
        {
            XmlNode nodeSerial = xmlDoc.CreateElement("BiosSerial");
            nodeSerial.InnerText = entry.SerialNumber;
            rootNode.AppendChild(nodeSerial);

            XmlNode nodeOS = xmlDoc.CreateElement("OperatingSystem");
            nodeOS.InnerText = entry.OperatingSystem;
            rootNode.AppendChild(nodeOS);

            XmlNode nodeSP = xmlDoc.CreateElement("ServicePack");
            nodeSP.InnerText = entry.SerivcePackInfo;
            rootNode.AppendChild(nodeSP);

            XmlNode nodeFreeSysDisk = xmlDoc.CreateElement("FreeSystemDiskSpace");
            nodeFreeSysDisk.InnerText = entry.FreeSysDiskSpace.ToString();
            rootNode.AppendChild(nodeFreeSysDisk);

            XmlNode nodeCPUSpeed = xmlDoc.CreateElement("CPUSpeed");
            nodeCPUSpeed.InnerText = entry.CPU_Speed;
            rootNode.AppendChild(nodeCPUSpeed);

            XmlNode nodeCPUID = xmlDoc.CreateElement("CPUId");
            nodeCPUID.InnerText = entry.CPU_ID;
            rootNode.AppendChild(nodeCPUID);

            XmlNode nodeCPUType = xmlDoc.CreateElement("CPUType");
            nodeCPUType.InnerText = entry.CPU_Type;
            rootNode.AppendChild(nodeCPUType);

            XmlNode nodeNrPhysicalCPU = xmlDoc.CreateElement("NumberOfPhysicalCPU");
            nodeNrPhysicalCPU.InnerText = entry.NumberPhysicalCPU.ToString();
            rootNode.AppendChild(nodeNrPhysicalCPU);

            XmlNode nodeNrLogicalCPU = xmlDoc.CreateElement("NumberOfLogicalCPU");
            nodeNrLogicalCPU.InnerText = entry.NumberLogicalCPU.ToString();
            rootNode.AppendChild(nodeNrLogicalCPU);

            XmlNode nodeNrCPUCores = xmlDoc.CreateElement("NumberOfCPUCore");
            nodeNrCPUCores.InnerText = entry.NumberCPUCore.ToString();
            rootNode.AppendChild(nodeNrCPUCores);

            XmlNode nodeTotalRAM = xmlDoc.CreateElement("TotalRAM");
            nodeTotalRAM.InnerText = entry.Total_RAM.ToString();
            rootNode.AppendChild(nodeTotalRAM);

            XmlNode nodeAvailRAM = xmlDoc.CreateElement("AvailableRAM");
            nodeAvailRAM.InnerText = entry.Available_RAM.ToString();
            rootNode.AppendChild(nodeAvailRAM);

            XmlNode nodePageFileSize = xmlDoc.CreateElement("PageFileSize");
            nodePageFileSize.InnerText = entry.PagefileSize;
            rootNode.AppendChild(nodePageFileSize);

            XmlNode nodePCModell = xmlDoc.CreateElement("PCModel");
            nodePCModell.InnerText = entry.PC_Modell;
            rootNode.AppendChild(nodePCModell);

            XmlNode nodePCManufacturer = xmlDoc.CreateElement("PCManufacturer");
            nodePCManufacturer.InnerText = entry.PC_Manufacturer;
            rootNode.AppendChild(nodePCManufacturer);

            XmlNode nodeSoundCard = xmlDoc.CreateElement("SoundCard");
            nodeSoundCard.InnerText = entry.SoundCard;
            rootNode.AppendChild(nodeSoundCard);

            XmlNode nodeNic1 = xmlDoc.CreateElement("NetworkCard_1");
            nodeNic1.InnerText = entry.NIC_1;
            rootNode.AppendChild(nodeNic1);

            XmlNode nodeNic2 = xmlDoc.CreateElement("NetworkCard_2");
            nodeNic2.InnerText = entry.NIC_2;
            rootNode.AppendChild(nodeNic2);

            XmlNode nodeNic3 = xmlDoc.CreateElement("NetworkCard_3");
            nodeNic3.InnerText = entry.NIC_3;
            rootNode.AppendChild(nodeNic3);

            XmlNode nodeNic4 = xmlDoc.CreateElement("NetworkCard_4");
            nodeNic4.InnerText = entry.NIC_4;
            rootNode.AppendChild(nodeNic4);

            XmlNode nodeVGACard = xmlDoc.CreateElement("VideoController");
            nodeVGACard.InnerText = entry.VGACard;
            rootNode.AppendChild(nodeVGACard);

            XmlNode nodeLogonServer = xmlDoc.CreateElement("LogonServer");
            nodeLogonServer.InnerText = entry.LogonServer;
            rootNode.AppendChild(nodeLogonServer);

            XmlNode nodeDomain = xmlDoc.CreateElement("LogonDomain");
            nodeDomain.InnerText = entry.Domain;
            rootNode.AppendChild(nodeDomain);

        }

        private void GenerateAppSessionXmlNodes(XmlDocument xmlDoc, XmlNode rootNode, AppSessionLogEntry entry)
        {
            XmlNode nodeMode = xmlDoc.CreateElement("SessionMode");
            nodeMode.InnerText = entry.IsStartEntry ? "Start" : "End";
            rootNode.AppendChild(nodeMode);

            XmlNode nodeAppName = xmlDoc.CreateElement("ApplicationName");
            nodeAppName.InnerText = entry.AppName;
            rootNode.AppendChild(nodeAppName);

            XmlNode nodeAppType = xmlDoc.CreateElement("ApplicationType");
            nodeAppType.InnerText = entry.AppType;
            rootNode.AppendChild(nodeAppType);

            XmlNode nodeVersion = xmlDoc.CreateElement("ApplicationVersion");
            nodeVersion.InnerText = entry.AppVersion;
            rootNode.AppendChild(nodeVersion);

            XmlNode nodeInstallMode = xmlDoc.CreateElement("InstallationMode");
            nodeInstallMode.InnerText = entry.InstallMode;
            rootNode.AppendChild(nodeInstallMode);
        }

        private void GenerateAppMessageXmlNodes(XmlDocument xmlDoc, XmlNode rootNode, AppMessageLogEntry entry)
        {
            XmlNode nodeAppDomain = xmlDoc.CreateElement("ApplicationDomain");
            nodeAppDomain.InnerText = entry.AppDomainName;
            rootNode.AppendChild(nodeAppDomain);

            XmlNode nodeProcessId = xmlDoc.CreateElement("ProcessId");
            nodeProcessId.InnerText = entry.ProcedssId;
            rootNode.AppendChild(nodeProcessId);

            XmlNode nodeProcessName = xmlDoc.CreateElement("ProcessName");
            nodeProcessName.InnerText = entry.ProcessName;
            rootNode.AppendChild(nodeProcessName);

            XmlNode nodeThreadName = xmlDoc.CreateElement("ThreadName");
            nodeThreadName.InnerText = entry.ThreadName;
            rootNode.AppendChild(nodeThreadName);

            XmlNode nodeThreadId = xmlDoc.CreateElement("ThreadId");
            nodeThreadId.InnerText = entry.Win32ThreadId;
            rootNode.AppendChild(nodeThreadId);

            XmlNode nodeMethodName = xmlDoc.CreateElement("MethodName");
            nodeMethodName.InnerText = entry.MethodName;
            rootNode.AppendChild(nodeMethodName);

            XmlNode nodeClassName = xmlDoc.CreateElement("ClassName");
            nodeClassName.InnerText = entry.ClassName;
            rootNode.AppendChild(nodeClassName);
        }

        private void GenerateUserActionXmlNodes(XmlDocument xmlDoc, XmlNode rootNode, UserActionLogEntry entry)
        {
            XmlNode nodeActionType = xmlDoc.CreateElement("ActionType");
            nodeActionType.InnerText = entry.ActionType;
            rootNode.AppendChild(nodeActionType);

            XmlNode nodeAppDomain = xmlDoc.CreateElement("ApplicationDomain");
            nodeAppDomain.InnerText = entry.AppDomainName;
            rootNode.AppendChild(nodeAppDomain);

            XmlNode nodeProcessId = xmlDoc.CreateElement("ProcessId");
            nodeProcessId.InnerText = entry.ProcedssId;
            rootNode.AppendChild(nodeProcessId);

            XmlNode nodeProcessName = xmlDoc.CreateElement("ProcessName");
            nodeProcessName.InnerText = entry.ProcessName;
            rootNode.AppendChild(nodeProcessName);

            XmlNode nodeThreadName = xmlDoc.CreateElement("ThreadName");
            nodeThreadName.InnerText = entry.ThreadName;
            rootNode.AppendChild(nodeThreadName);

            XmlNode nodeThreadId = xmlDoc.CreateElement("ThreadId");
            nodeThreadId.InnerText = entry.Win32ThreadId;
            rootNode.AppendChild(nodeThreadId);

            XmlNode nodeMethodName = xmlDoc.CreateElement("MethodName");
            nodeMethodName.InnerText = entry.MethodName;
            rootNode.AppendChild(nodeMethodName);

            XmlNode nodeClassName = xmlDoc.CreateElement("ClassName");
            nodeClassName.InnerText = entry.ClassName;
            rootNode.AppendChild(nodeClassName);

            XmlNode nodeActionInfos = xmlDoc.CreateElement("ActionInformations");

            XmlNode nodeai1 = xmlDoc.CreateElement("ActionInformation");
            nodeai1.InnerText = entry.ActionInfo1;
            nodeActionInfos.AppendChild(nodeai1);
            XmlNode nodeai2 = xmlDoc.CreateElement("ActionInformation");
            nodeai2.InnerText = entry.ActionInfo2;
            nodeActionInfos.AppendChild(nodeai2);
            XmlNode nodeai3 = xmlDoc.CreateElement("ActionInformation");
            nodeai3.InnerText = entry.ActionInfo3;
            nodeActionInfos.AppendChild(nodeai3);
            XmlNode nodeai4 = xmlDoc.CreateElement("ActionInformation");
            nodeai4.InnerText = entry.ActionInfo4;
            nodeActionInfos.AppendChild(nodeai4);
            XmlNode nodeai5 = xmlDoc.CreateElement("ActionInformation");
            nodeai5.InnerText = entry.ActionInfo5;
            nodeActionInfos.AppendChild(nodeai5);

            rootNode.AppendChild(nodeActionInfos);

            XmlNode nodeActionDatas = xmlDoc.CreateElement("ActionDatas");

            XmlNode nodead1 = xmlDoc.CreateElement("ActionData");
            nodead1.InnerText = entry.ActionData1;
            nodeActionDatas.AppendChild(nodead1);
            XmlNode nodead2 = xmlDoc.CreateElement("ActionData");
            nodead2.InnerText = entry.ActionData2;
            nodeActionDatas.AppendChild(nodead2); 
            XmlNode nodead3 = xmlDoc.CreateElement("ActionData");
            nodead3.InnerText = entry.ActionData3;
            nodeActionDatas.AppendChild(nodead3);

            rootNode.AppendChild(nodeActionDatas);
        }
    }
}
