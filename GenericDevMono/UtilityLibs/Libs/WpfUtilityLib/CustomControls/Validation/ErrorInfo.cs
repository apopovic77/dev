﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace Logicx.WpfUtility.CustomControls.Validation
{
    public enum ErrorInfoType
    {
        Error,
        Warning,
        Information
    }

    /// <summary>
    /// Error information class
    /// </summary>
    public class ErrorInfo
    {
        #region Properties
        /// <summary>
        /// Gets/Sets the error instance
        /// </summary>
        public ValidationErrorEx Error { get; set; }
        /// <summary>
        /// Gets/sets the control which caused the error
        /// </summary>
        public DependencyObject SourceControl { get; set; }

        /// <summary>
        /// Gets/sets the validation group parent control
        /// </summary>
        public DependencyObject ValidationGroupParent { get; set; }
        /// <summary>
        /// Gets/sets the error source name if set (if not set, the source control's name will be used)
        /// </summary>
        public string SourceName { get; set; }
        /// <summary>
        /// Gets/Sets the error info type
        /// </summary>
        public ErrorInfoType Type
        {
            get { return _error_info_type; }
            set { _error_info_type = value; }
        }
        /// <summary>
        /// Error item index
        /// </summary>
        public int ItemIndex { get; set; }

        
        #endregion

        #region operations
        /// <summary>
        /// Get the validation group parent for a control
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        public DependencyObject GetValidationGroupParent(DependencyObject element)
        {
            if (element == SourceControl)
                return ValidationGroupParent;

            return ErrorProvider.GetValidationGroupParentControl(element);
        }
        /// <summary>
        /// Gets a flag indicating if the validation group parent has errors
        /// </summary>
        /// <returns></returns>
        public bool HasErrorValidationGroupParent()
        {
            return HasErrorValidationGroupParent(ValidationGroupParent);
        }
        /// <summary>
        /// Gets a flag indicating if the validation group parent has errors
        /// </summary>
        /// <param name="validationGroupParentElement">validation group parent element</param>
        /// <returns></returns>
        private bool HasErrorValidationGroupParent(DependencyObject validationGroupParentElement)
        {
            if (validationGroupParentElement != null)
                return false;

            return ErrorProvider.GetHasError(validationGroupParentElement);
        }
        #endregion

        #region attribs

        private ErrorInfoType _error_info_type = ErrorInfoType.Error;
        #endregion
    }
}
