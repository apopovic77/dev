﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Linq.Dynamic;
using System.Reflection;
using System.Text;
using LinqLib.Reflection;

namespace LinqLib
{
    public class EntityNotFoundException : Exception
    {
        public EntityNotFoundException() : base("Entity no longer exists!") { }
        public EntityNotFoundException(string msg) : base(msg) { }
        public EntityNotFoundException(string msg, Exception innerException) : base(msg, innerException) { }
    }

    public class LinqHelper
    {
        #region helper methods for full entity refresh
        public static bool RefreshEntity(DataContext data_context, RefreshMode mode, object objEntity)
        {
            return RefreshEntity(data_context, mode, objEntity, true);
        }

        public static bool RefreshEntity(DataContext data_context, RefreshMode mode, object objEntity, bool fullEntityRefresh)
        {
            return RefreshEntity(data_context, mode, objEntity, fullEntityRefresh, true);
        }

        public static bool RefreshEntity(DataContext data_context, RefreshMode mode, object objEntity, bool fullEntityRefresh, bool catchExceptions)
        {
            bool bRet = true;

            if (objEntity == null)
            {
                bRet = false;
                if (!catchExceptions)
                    throw new ArgumentException("objEntity must not be null");
            }
            else
            {
                try
                {
                    // step 1: refresh the given entity value
                    data_context.Refresh(mode, objEntity);
                }
                catch (Exception ex)
                {
                    bRet = false;
                    if (!catchExceptions)
                        throw new EntityNotFoundException("Entity no longer exists", ex);
                }

                if (fullEntityRefresh)
                {
                    // step 2: update all entityrefs
                    // lesen der liste aller Association Properties des Datentyps
                    List<PropertyInfo> assocProperties = LinqReflectionHelper.GetAssociationProperties(objEntity.GetType(), false, true);

                    Dictionary<Type, PropertyInfo> data_context_table_properties = new Dictionary<Type, PropertyInfo>();

                    foreach (PropertyInfo pI in assocProperties)
                    {
                        try
                        {
                            string thisKey = LinqReflectionHelper.GetAssociationAttributeThisKeyName(pI);
                            string otherKey = LinqReflectionHelper.GetAssociationAttributeOtherKeyName(pI);
                            object entitySetInstance = pI.GetValue(objEntity, null);
                            Type tableType = LinqReflectionHelper.GetInnerTypeOfGenericType(pI.PropertyType);

                            #region dynamic linq code

                            if (!string.IsNullOrEmpty(thisKey) && !string.IsNullOrEmpty(otherKey) && entitySetInstance != null)
                            {
                                PropertyInfo thisKeyPropertyInfo = objEntity.GetType().GetProperty(thisKey);
                                PropertyInfo otherKeyPropertyInfo = tableType.GetProperty(otherKey);

                                if (thisKeyPropertyInfo != null && otherKeyPropertyInfo != null)
                                {
                                    if (entitySetInstance is IList && entitySetInstance is IEnumerable)
                                    {
                                        List<object> curEntities = new List<object>();
                                        foreach (object curObj in ((IList)entitySetInstance))
                                        {
                                            curEntities.Add(curObj);
                                        }

                                        ((IList)entitySetInstance).Clear();

                                        if (!data_context_table_properties.ContainsKey(tableType))
                                            data_context_table_properties[tableType] = LinqReflectionHelper.GetTablePropertyOfDataContext(tableType, data_context);

                                        PropertyInfo propertyTableDc = data_context_table_properties[tableType];

                                        IQueryable tableInstance = propertyTableDc.GetValue(data_context, null) as IQueryable;

                                        if (tableInstance != null)
                                        {
                                            string dynamicLinqWhere = otherKey + "=@0";
                                            object[] dynamicLinqWhereParams = new object[1];
                                            dynamicLinqWhereParams[0] = thisKeyPropertyInfo.GetValue(objEntity, null);

                                            var subsequence = tableInstance.Where(dynamicLinqWhere, dynamicLinqWhereParams);

                                            foreach (object objInstance in subsequence)
                                            {
                                                if (objInstance != null)
                                                {
                                                    ((IList)entitySetInstance).Add(objInstance);
                                                }
                                            }
                                        }

                                        List<object> refresh_list = new List<object>();

                                        foreach (object curObj in ((IList)entitySetInstance))
                                        {
                                            // check if the added object was already present prior of the update
                                            // if this is the case, we have to call a Refresh explicilty
                                            if (curEntities.Contains(curObj))
                                                refresh_list.Add(curObj);

                                        }

                                        if (refresh_list.Count > 0)
                                            data_context.Refresh(mode, refresh_list);
                                    }
                                }
                            }

                            #endregion

                            #region Reflection code

                            //if (!string.IsNullOrEmpty(thisKey) && !string.IsNullOrEmpty(otherKey) && entitySetInstance != null)
                            //{
                            //    PropertyInfo thisKeyPropertyInfo = objEntity.GetType().GetProperty(thisKey);
                            //    PropertyInfo otherKeyPropertyInfo = tableType.GetProperty(otherKey);

                            //    if (thisKeyPropertyInfo != null && otherKeyPropertyInfo != null)
                            //    {
                            //        if (entitySetInstance is IList && entitySetInstance is IEnumerable)
                            //        {
                            //            ((IList) entitySetInstance).Clear();

                            //            if (!data_context_table_properties.ContainsKey(tableType))
                            //                data_context_table_properties[tableType] = LinqReflectionHelper.GetTablePropertyOfDataContext(tableType, data_context);

                            //            PropertyInfo propertyTableDc = data_context_table_properties[tableType];

                            //            if (propertyTableDc != null)
                            //            {
                            //                object tablePropInstance = propertyTableDc.GetValue(data_context, null);
                            //                if (tablePropInstance != null)
                            //                {

                            //                    // get the Where method for calling a data filter
                            //                    MethodInfo miWhere = LinqReflectionHelper.GetQueryableWhereWithPredicateMethodInfo();
                            //                    // set the generic datatypes of the generic method
                            //                    miWhere = miWhere.MakeGenericMethod(new Type[] {tableType});

                            //                    // gets the method info of the static method which returns the this key/foreign key search predicate function
                            //                    // we cannot call this method directly because we need to dynamically assign the 
                            //                    // generic data type
                            //                    MethodInfo miKeyComparePredicate = LinqReflectionHelper.GetThisKeyForeignKeySearchPredicate();
                            //                    // set the generic datatypes of the generic method
                            //                    miKeyComparePredicate = miKeyComparePredicate.MakeGenericMethod(new Type[] {tableType});

                            //                    // invoke the static method which returns the predicate function
                            //                    var o2 = miKeyComparePredicate.Invoke(null, new object[] {otherKeyPropertyInfo, thisKeyPropertyInfo, objEntity});

                            //                    // Note: Problem, dass die dynamische predicate function nicht in SQL convertiert werden kann


                            //                    // Func<T,bool> für Queryable in Expression<Func<T, bool>> umwandeln
                            //                    MethodInfo miFuncToExpression = LinqReflectionHelper.GeFuncToExpressionMethod();
                            //                    miFuncToExpression = miFuncToExpression.MakeGenericMethod(new Type[] { tableType });

                            //                    var expr = miFuncToExpression.Invoke(null, new object[] {o2});

                            //                    // invoke the Where() method of the table object
                            //                    object obRet = miWhere.Invoke(tablePropInstance,
                            //                                                  new object[]
                            //                                                      {
                            //                                                          tablePropInstance,
                            //                                                          expr
                            //                                                      }
                            //                        );

                            //                    // re-add all results to the entity ref
                            //                    if (obRet != null && obRet is IEnumerable)
                            //                    {
                            //                        foreach (object objInstance in (IEnumerable)obRet)
                            //                        {
                            //                            if (objInstance != null)
                            //                            {
                            //                                ((IList) entitySetInstance).Add(objInstance);
                            //                            }
                            //                        }
                            //                    }

                            //                    //data_context.Refresh(mode, entitySetInstance as IEnumerable);
                            //                }
                            //            }
                            //        }
                            //    }
                            //}

                            #endregion
                        }
                        catch (Exception ex)
                        {
                            bRet = false;
                            if (!catchExceptions)
                                throw;
                        }
                    }
                }
            }

            return bRet;
        }
        #endregion
    }
}
