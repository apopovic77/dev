﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Media;
using Logicx.WpfUtility.CustomControls.Validation;
using Logicx.WpfUtility.WpfHelpers;

namespace Logicx.WpfUtility.CustomControls.ChangeInfo
{
    public class ChangeInformationProvider : ContentControl, IDisposable
    {
        public enum ChangeInfoFilterFlag
        {
            InsertOnly,
            UpateOnly,
            DeleteOnly,
            InsertAndUpdate,
            InsertAndDelete,
            UpdateAndDelete,
            All
        }

        #region construction and initialization
        static ChangeInformationProvider()
        {

            ChangesPropertyKey = DependencyProperty.RegisterAttachedReadOnly("Changes", typeof(ReadOnlyObservableCollection<ChangeInfo>), typeof(ChangeInformationProvider), new FrameworkPropertyMetadata(ChangeInfoCollection.Empty, FrameworkPropertyMetadataOptions.NotDataBindable));
            ChangesProperty = ChangesPropertyKey.DependencyProperty;

            ChangesInternalProperty = DependencyProperty.RegisterAttached("ChangesInternal", typeof(ChangeInfoCollection), typeof(ChangeInformationProvider), new FrameworkPropertyMetadata(null, new PropertyChangedCallback(ChangeInformationProvider.OnChangesInternalChanged)));

            HasChangePropertyKey = DependencyProperty.RegisterAttachedReadOnly("HasChange", typeof(bool), typeof(ChangeInformationProvider), new FrameworkPropertyMetadata(false, FrameworkPropertyMetadataOptions.NotDataBindable));
            HasChangeProperty = HasChangePropertyKey.DependencyProperty;

            ShowAdornerPropertyKey = DependencyProperty.RegisterAttachedReadOnly("ShowAdorner", typeof(bool), typeof(ChangeInformationProvider), new FrameworkPropertyMetadata(true, FrameworkPropertyMetadataOptions.NotDataBindable));
            ShowAdornerProperty = ShowAdornerPropertyKey.DependencyProperty;

            LastChangeInformationKey = DependencyProperty.RegisterAttachedReadOnly("LastChangeInformation", typeof(string), typeof(ChangeInformationProvider), new FrameworkPropertyMetadata("", FrameworkPropertyMetadataOptions.NotDataBindable));
            LastChangeInformationProperty = LastChangeInformationKey.DependencyProperty;

            ChangeHistoryInformationKey = DependencyProperty.RegisterAttachedReadOnly("ChangeHistoryInformation", typeof(string), typeof(ChangeInformationProvider), new FrameworkPropertyMetadata("", FrameworkPropertyMetadataOptions.NotDataBindable));
            ChangeHistoryInformationProperty = ChangeHistoryInformationKey.DependencyProperty;


            ChangeInfoTemplateProperty = DependencyProperty.RegisterAttached("ChangeInfoTemplate", typeof(ControlTemplate), typeof(ChangeInformationProvider), new FrameworkPropertyMetadata(CreateDefaulChangeInfoTemplate(), FrameworkPropertyMetadataOptions.NotDataBindable, new PropertyChangedCallback(ChangeInformationProvider.OnChangeInfoTemplateChanged)));
            ChangeInfoAdornerSiteProperty = DependencyProperty.RegisterAttached("ChangeInfoAdornerSite", typeof(DependencyObject), typeof(ChangeInformationProvider), new FrameworkPropertyMetadata(null, new PropertyChangedCallback(ChangeInformationProvider.OnChangeInfoAdornerSiteChanged)));
            ChangeInfoAdornerSiteForProperty = DependencyProperty.RegisterAttached("ChangeInfoAdornerSiteFor", typeof(DependencyObject), typeof(ChangeInformationProvider), new FrameworkPropertyMetadata(null, new PropertyChangedCallback(ChangeInformationProvider.OnChangeInfoAdornerSiteForChanged)));

            ChangeInfoAdornerProperty = DependencyProperty.RegisterAttached("ChangeInfoAdorner", typeof(TemplatedAdornerEx), typeof(ChangeInformationProvider), new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.NotDataBindable));

            RegisterProperty = DependencyProperty.RegisterAttached("Register", typeof(bool), typeof(ChangeInformationProvider), new FrameworkPropertyMetadata(false, FrameworkPropertyMetadataOptions.NotDataBindable, new PropertyChangedCallback(ChangeInformationProvider.OnRegisterChanged)));

            ChangeInfoFilterProperty = DependencyProperty.RegisterAttached("ChangeInfoFilter", typeof(ChangeInfoFilterFlag), typeof(ChangeInformationProvider), new FrameworkPropertyMetadata(ChangeInfoFilterFlag.All, FrameworkPropertyMetadataOptions.NotDataBindable, new PropertyChangedCallback(ChangeInformationProvider.OnChangeInfoFilterChanged)));


            ChangeInfoValueConverterProperty = DependencyProperty.RegisterAttached("ChangeInfoValueConverter", typeof(IChangeValueConverter), typeof(ChangeInformationProvider), new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.NotDataBindable));


            ChangeInfoGroupParentControlProperty = DependencyProperty.RegisterAttached("ChangeInfoGroupControlParent", typeof(DependencyObject), typeof(ChangeInformationProvider), new FrameworkPropertyMetadata(null, new PropertyChangedCallback(ChangeInformationProvider.OnChangeInfoGroupParentControlChanged)));
            ChangeInfoGroupParentProperty = DependencyProperty.RegisterAttached("ChangeInfoGroupParent", typeof(string), typeof(ChangeInformationProvider), new FrameworkPropertyMetadata(string.Empty, new PropertyChangedCallback(ChangeInformationProvider.OnChangeInfoGroupParentChanged)));

            InfoSourceNameProperty = DependencyProperty.RegisterAttached("InfoSourceName", typeof(string), typeof(ChangeInformationProvider), new FrameworkPropertyMetadata(string.Empty, new PropertyChangedCallback(ChangeInformationProvider.OnInfoSourceNameChanged)));

            ReportsToChangeInfoProviderProperty = DependencyProperty.RegisterAttached("ReportsToChangeInfoProvider", typeof(bool), typeof(ChangeInformationProvider), new FrameworkPropertyMetadata(true, new PropertyChangedCallback(ChangeInformationProvider.OnReportsToChangeInfoProviderChanged)));

            SourceDataObjectProperty = DependencyProperty.RegisterAttached("SourceDataObject", typeof(object), typeof(ChangeInformationProvider), new FrameworkPropertyMetadata(null, new PropertyChangedCallback(ChangeInformationProvider.OnSourceDataObjectChanged)));
            SourceDataTypeProperty = DependencyProperty.RegisterAttached("SourceDataType", typeof(Type), typeof(ChangeInformationProvider), new FrameworkPropertyMetadata(null, new PropertyChangedCallback(ChangeInformationProvider.OnSourceDataTypeChanged)));
            FieldNameProperty = DependencyProperty.RegisterAttached("FieldName", typeof(string), typeof(ChangeInformationProvider), new FrameworkPropertyMetadata(null, new PropertyChangedCallback(ChangeInformationProvider.OnFieldNameChanged)));


            HasChangeDataPropertyKey = DependencyProperty.RegisterReadOnly("HasChangeData", typeof(bool), typeof(ChangeInformationProvider), new FrameworkPropertyMetadata(false, FrameworkPropertyMetadataOptions.NotDataBindable));
            HasChangeDataProperty = HasChangeDataPropertyKey.DependencyProperty;

            ChangeInfoAdornerVisibleProperty = DependencyProperty.Register("ChangeInfoAdornerVisible", typeof(bool), typeof(ChangeInformationProvider), new FrameworkPropertyMetadata(true));
            
        }

        /// <summary>
        /// Default constructor for design-time support
        /// </summary>
        public ChangeInformationProvider()
        {
            //GenerateListSnapshots();

            this.Loaded += new RoutedEventHandler(ChangeInformationProvider_Loaded);
            this.Unloaded += new RoutedEventHandler(ChangeInformationProvider_Unloaded);

        }
        #endregion

        #region operations

        private static bool ElementHasError(DependencyObject element)
        {
            if (element != null)
            {
                bool bHasError = ErrorProvider.GetHasError(element);
                bHasError |= System.Windows.Controls.Validation.GetHasError(element);

                return bHasError;
            }

            return false;
        }

        #region static attached property / template operations

        /// <summary>
        /// Adds an change information to the changes collection of an element
        /// </summary>
        /// <param name="changeInfo"></param>
        /// <param name="targetElement"></param>
        internal static void AddChangeInformation(ChangeInfo changeInfo, DependencyObject targetElement)
        {
            if (targetElement != null)
            {
                bool flag;
                ChangeInfoCollection changesInternal = GetChangesInternal(targetElement);
                if (changesInternal == null)
                {
                    flag = true;
                    changesInternal = new ChangeInfoCollection();
                    changesInternal.Add(changeInfo);
                    targetElement.SetValue(ChangesInternalProperty, changesInternal);
                    RebuildChangeInformationTexts(targetElement);
                }
                else
                {
                    flag = changesInternal.Count == 0;

                    if (!changesInternal.Contains(changeInfo))
                    {
                        changesInternal.Add(changeInfo);
                        RebuildChangeInformationTexts(targetElement);
                    }
                }
                if (flag)
                {
                    targetElement.SetValue(HasChangePropertyKey, true);
                }

                DependencyObject parentGroupObject = GetChangeInfoGroupParentControl(targetElement);

                if (parentGroupObject != null)
                {
                    // add this error to the parent group element
                    AddChangeInformation(changeInfo, parentGroupObject);
                }

                ShowChangeInfoAdorner(targetElement, true);
            }
        }
        /// <summary>
        /// Removes a change information from the elements changes collection
        /// </summary>
        /// <param name=">"></param>
        /// <param name="targetElement"></param>
        internal static void RemoveChangeInfo(ChangeInfo changeInfo, DependencyObject targetElement)
        {
            if (targetElement != null)
            {
                ChangeInfoCollection changesInternal = GetChangesInternal(targetElement);
                if (((changesInternal != null) && (changesInternal.Count != 0)) && changesInternal.Contains(changeInfo))
                {
                    bool flag = changesInternal.Count == 1;
                    if (flag)
                    {
                        targetElement.ClearValue(HasChangePropertyKey);
                    }
                    changesInternal.Remove(changeInfo);
                    RebuildChangeInformationTexts(targetElement);

                    DependencyObject parentGroupObject = GetChangeInfoGroupParentControl(targetElement);

                    if (parentGroupObject != null)
                    {
                        // remove this change from the parent group element
                        RemoveChangeInfo(changeInfo, parentGroupObject);
                    }

                    if (flag)
                    {
                        targetElement.ClearValue(ChangesInternalProperty);
                        ShowChangeInfoAdorner(targetElement, false);
                    }
                    else
                    {
                        ShowChangeInfoAdorner(targetElement, true);
                    }
                }
            }
        }

        private static void RebuildChangeInformationTexts(DependencyObject targetElement)
        {
            if (targetElement != null)
            {
                ChangeInfoCollection changesInternal = GetChangesInternal(targetElement);


                if((changesInternal != null) && (changesInternal.Count != 0))
                {
                    string fieldName = GetFieldName(targetElement);
                    object sourceObject = GetSourceDataObject(targetElement);
                    string infoSourceName = GetInfoSourceName(targetElement);

                    if (string.IsNullOrEmpty(infoSourceName))
                    {
                        if (targetElement is FrameworkElement)
                            infoSourceName = ((FrameworkElement)targetElement).Name;
                        else
                            infoSourceName = string.Empty;
                    }
                    Type sourceType = GetSourceDataType(targetElement);
                    IChangeValueConverter valueConverter = GetChangeInfoValueConverter(targetElement);

                    if(!string.IsNullOrEmpty(fieldName))
                    {
                        // this element is bound to a specific data field
                        string userName = "";
                        string dateInfo = "";
                        string oldValue = "nicht gesetzt";
                        string newValue = "";
                        string originalValue = "nicht gesetzt";

                        var sortedChangeinfo = changesInternal.OrderBy(x => x.ChangeDate);

                        string sInfoText = "Änderungsinformationen zu '{0}'\n \nLetzte Änderung durchgeführt\nam: {1}\nvon: {2}\n \nAlter Wert:\n{3}\n \nNeuer Wert:\n{4}\n \nOriginalwert:\n{5}\n";
                        string sHistoryText = "";

                        int nChangeCount = 1;

                        foreach(ChangeInfo changeInfo in sortedChangeinfo)
                        {
                            if(sourceObject == changeInfo.SourceObject && fieldName == changeInfo.FieldName)
                            {
                                if (changeInfo.HasOriginalValue)
                                {
                                    if (valueConverter == null)
                                    {
                                        if (changeInfo.OldValue != null)
                                        {
                                            if (changeInfo.ValueType == typeof(double) ||
                                                changeInfo.ValueType == typeof(float) ||
                                                changeInfo.ValueType == typeof(decimal))
                                            {
                                                originalValue = ConvertDoubleString(changeInfo.OldValue);
                                            }
                                            else
                                            {
                                                originalValue = Convert.ToString(changeInfo.OldValue);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        originalValue = valueConverter.ConvertValue(targetElement, changeInfo, ValueType.Original);
                                    }
                                }

                                if (valueConverter == null)
                                {
                                    if (changeInfo.OldValue != null)
                                    {
                                        if (changeInfo.ValueType == typeof(double) ||
                                                changeInfo.ValueType == typeof(float) ||
                                                changeInfo.ValueType == typeof(decimal))
                                        {
                                            oldValue = ConvertDoubleString(changeInfo.OldValue);
                                        }
                                        else
                                        {
                                            oldValue = Convert.ToString(changeInfo.OldValue);
                                        }
                                    }


                                    if (changeInfo.ValueType == typeof(double) ||
                                                changeInfo.ValueType == typeof(float) ||
                                                changeInfo.ValueType == typeof(decimal))
                                    {
                                        newValue = ConvertDoubleString(changeInfo.NewValue);
                                    }
                                    else
                                    {
                                        newValue = Convert.ToString(changeInfo.NewValue);
                                    }
                                }
                                else
                                {
                                    oldValue = valueConverter.ConvertValue(targetElement, changeInfo, ValueType.Old);
                                    newValue = valueConverter.ConvertValue(targetElement, changeInfo, ValueType.New);
                                }

                                
                                dateInfo = changeInfo.ChangeDate.Value.ToString("dd.MM.yyyy HH:mm");
                                userName = changeInfo.ChangeUsername;

                                sHistoryText = string.Format("Änderung #{0}\nAm: {1}\nVon: {2}\n \nAlter Wert:\n{3}\n \nNeuer Wert:\n{4}\n \n", nChangeCount,
                                    dateInfo, userName, oldValue, newValue) + sHistoryText;
                                nChangeCount++;
                            }
                        }

                        sHistoryText += "Originalwert:\n" + originalValue + "\n";
                        sHistoryText = "Änderungshistorie für '" + infoSourceName + "':\n \n" + sHistoryText;

                        SetLastChangeInformation(targetElement, string.Format(sInfoText, infoSourceName, dateInfo, userName, oldValue, newValue, originalValue));
                        SetChangeHistoryInformation(targetElement, sHistoryText);
                    }
                    else
                    {
                        string userName = "";
                        string dateInfo = "";
                        string sSummaryText = "Änderungszusammenfassung für '{0}':\n \nLetzte Änderung durchgeführt\nam: {1}\nvon: {2}\n \nSeit der letzten Synchronisation wurden:\nDatensätze eingefügt: {3}\nDatensätze geändert: {4}\nDatensätze gelöscht: {5}\n \n";
                        int nCntInsert = 0;
                        int nCntUpdate = 0;
                        int nCntDelete = 0;
                        List<object> bufferInsert = new List<object>();
                        List<object> bufferDelete = new List<object>();
                        List<object> bufferUpdate = new List<object>();

                        var sortedChangeinfo = changesInternal.OrderBy(x => x.ChangeDate);

                        foreach (ChangeInfo changeInfo in sortedChangeinfo)
                        {
                            dateInfo = changeInfo.ChangeDate.Value.ToString("dd.MM.yyyy HH:mm");
                            userName = changeInfo.ChangeUsername;

                            switch(changeInfo.ChangeType)
                            {
                                case ChangeType.Insert:
                                    {
                                        if(!bufferInsert.Contains(changeInfo.SourceObject))
                                        {
                                            bufferInsert.Add(changeInfo.SourceObject);
                                            nCntInsert++;
                                        }
                                    }
                                    ; break;
                                case ChangeType.Delete:
                                    {
                                        if (!bufferDelete.Contains(changeInfo.SourceObject))
                                        {
                                            bufferDelete.Add(changeInfo.SourceObject);
                                            nCntDelete++;
                                        }
                                    }
                                    ; break;
                                case ChangeType.Update:
                                    {
                                        if (!bufferUpdate.Contains(changeInfo.SourceObject))
                                        {
                                            bufferUpdate.Add(changeInfo.SourceObject);
                                            nCntUpdate++;
                                        }
                                    }
                                    ; break;
                            }
                        }

                        string sInfoText = string.Format(sSummaryText, infoSourceName, dateInfo, userName, nCntInsert, nCntUpdate, nCntDelete);

                        SetLastChangeInformation(targetElement, sInfoText);
                        SetChangeHistoryInformation(targetElement, sInfoText);
                    }
                }
                else
                {
                    SetLastChangeInformation(targetElement, string.Empty);
                    SetChangeHistoryInformation(targetElement, string.Empty);
                }
            }
        }

        private static string ConvertDoubleString(object value)
        {
            if (value == null)
                return "";

            string sRet = "";

            //sRet = double.Parse(value, new CultureInfo("en-US").NumberFormat).ToString("0,0.00");
            sRet =((double)value).ToString("0,0.00");

            return sRet;
        }

        /// <summary>
        /// Default error template
        /// </summary>
        /// <returns></returns>
        private static ControlTemplate CreateDefaulChangeInfoTemplate()
        {
            ControlTemplate template = new ControlTemplate(typeof(Control));
            FrameworkElementFactory factory = new FrameworkElementFactory(typeof(Border), "Border");
            factory.SetValue(Border.BorderBrushProperty, Brushes.CadetBlue);
            factory.SetValue(Border.BorderThicknessProperty, new Thickness(1.0));
            FrameworkElementFactory child = new FrameworkElementFactory(typeof(AdornedElementPlaceholder), "Placeholder");
            factory.AppendChild(child);
            template.VisualTree = factory;
            template.Seal();
            return template;
        }


        /// <summary>
        /// Get the error template
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        [AttachedPropertyBrowsableForType(typeof(DependencyObject))]
        public static ControlTemplate GetChangeInfoTemplate(DependencyObject element)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }
            return (element.GetValue(ChangeInfoTemplateProperty) as ControlTemplate);
        }

        /// <summary>
        /// Get the register flag
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        [AttachedPropertyBrowsableForType(typeof(DependencyObject))]
        public static bool GetRegister(DependencyObject element)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }
            return ((bool)element.GetValue(RegisterProperty));
        }

        /// <summary>
        /// Get the register flag
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        [AttachedPropertyBrowsableForType(typeof(DependencyObject))]
        public static ChangeInfoFilterFlag GetChangeInfoFilter(DependencyObject element)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }
            return ((ChangeInfoFilterFlag)element.GetValue(ChangeInfoFilterProperty));
        }

        /// <summary>
        /// Get the register flag
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        [AttachedPropertyBrowsableForType(typeof(DependencyObject))]
        public static IChangeValueConverter GetChangeInfoValueConverter(DependencyObject element)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }
            return ((IChangeValueConverter)element.GetValue(ChangeInfoValueConverterProperty));
        }

        /// <summary>
        /// Get the reports to error provider control flag
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        [AttachedPropertyBrowsableForType(typeof(DependencyObject))]
        public static bool GetReportsToChangeInfoProvider(DependencyObject element)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }
            return ((bool)element.GetValue(ReportsToChangeInfoProviderProperty));
        }

        /// <summary>
        /// Get the validation group control property
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        internal static DependencyObject GetChangeInfoGroupParentControl(DependencyObject element)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }
            DependencyObject control = ((DependencyObject)element.GetValue(ChangeInfoGroupParentControlProperty));

            if (control == null)
            {
                string sParentName = (string)element.GetValue(ChangeInfoGroupParentProperty);

                if (!string.IsNullOrEmpty(sParentName))
                {
                    ChangeInformationProvider changeInfoProvider = GetTopMostChangeInfoProviderForElement(element);

                    if (changeInfoProvider != null)
                    {
                        control = UIHelper.FindVisualChildByName<DependencyObject>(changeInfoProvider, sParentName);

                        SetChangeInfoGroupParentControl(element, control);
                    }
                }
            }
            return control;
        }

        /// <summary>
        /// Get the validation group control name property
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        [AttachedPropertyBrowsableForType(typeof(DependencyObject))]
        public static string GetChangeInfoGroupParent(DependencyObject element)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }
            return ((string)element.GetValue(ChangeInfoGroupParentProperty));
        }

        /// <summary>
        /// Get the error source name
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        [AttachedPropertyBrowsableForType(typeof(DependencyObject))]
        public static string GetInfoSourceName(DependencyObject element)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }
            return ((string)element.GetValue(InfoSourceNameProperty));
        }

        /// <summary>
        /// Get the error source name
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        [AttachedPropertyBrowsableForType(typeof(DependencyObject))]
        public static object GetSourceDataObject(DependencyObject element)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }
            return ((object)element.GetValue(SourceDataObjectProperty));
        }

        /// <summary>
        /// Get the error source name
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        [AttachedPropertyBrowsableForType(typeof(DependencyObject))]
        public static Type GetSourceDataType(DependencyObject element)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }
            return ((Type)element.GetValue(SourceDataTypeProperty));
        }

        /// <summary>
        /// Get the error source name
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        [AttachedPropertyBrowsableForType(typeof(DependencyObject))]
        public static string GetFieldName(DependencyObject element)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }
            return ((string)element.GetValue(FieldNameProperty));
        }

        /// <summary>
        /// Gets the HasError flag for an object
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        public static bool GetHasChange(DependencyObject element)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }
            return (bool)element.GetValue(HasChangeProperty);
        }

        /// <summary>
        /// Gets the HasError flag for an object
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        public static bool GetShowAdorner(DependencyObject element)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }
            return (bool)element.GetValue(ShowAdornerProperty);
        }

        /// <summary>
        /// Gets the LastChangeInformation for an object
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        public static string GetLastChangeInformation(DependencyObject element)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }
            return (string)element.GetValue(LastChangeInformationProperty);
        }

        /// <summary>
        /// Gets the Change history information for an object
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        public static string GetChangeHistoryInformation(DependencyObject element)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }
            return (string)element.GetValue(ChangeHistoryInformationProperty);
        }

        /// <summary>
        /// Gets the validation adorner site property for an element
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        [AttachedPropertyBrowsableForType(typeof(DependencyObject))]
        public static DependencyObject GetChangeInfoAdornerSite(DependencyObject element)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }
            return (element.GetValue(ChangeInfoAdornerSiteProperty) as DependencyObject);
        }
        /// <summary>
        /// gets the validation adorner site for for an element
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        [AttachedPropertyBrowsableForType(typeof(DependencyObject))]
        public static DependencyObject GetChangeInfoAdornerSiteFor(DependencyObject element)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }
            return (element.GetValue(ChangeInfoAdornerSiteForProperty) as DependencyObject);
        }

        /// <summary>
        /// Gets the read only errors collection for a framework element
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        public static ReadOnlyObservableCollection<ChangeInfo> GetChanges(DependencyObject element)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }
            return (ReadOnlyObservableCollection<ChangeInfo>)element.GetValue(ChangesProperty);
        }
        /// <summary>
        /// Gets the internal errors collection with modification support
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        internal static ChangeInfoCollection GetChangesInternal(DependencyObject target)
        {
            return (ChangeInfoCollection)target.GetValue(ChangesInternalProperty);
        }

        /// <summary>
        /// Sets the error template
        /// </summary>
        /// <param name="element"></param>
        /// <param name="value"></param>
        public static void SetChangeInfoTemplate(DependencyObject element, ControlTemplate value)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }
            if (!object.Equals(element.ReadLocalValue(ChangeInfoTemplateProperty), value))
            {
                element.SetValue(ChangeInfoTemplateProperty, value);
            }
        }

        /// <summary>
        /// Sets the register flag
        /// </summary>
        /// <param name="element"></param>
        /// <param name="value"></param>
        public static void SetRegister(DependencyObject element, bool value)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }
            if (!object.Equals(element.ReadLocalValue(RegisterProperty), value))
            {

                element.SetValue(RegisterProperty, value);
            }
        }

        /// <summary>
        /// Sets the register flag
        /// </summary>
        /// <param name="element"></param>
        /// <param name="value"></param>
        public static void SetChangeInfoFilter(DependencyObject element, ChangeInfoFilterFlag value)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }
            if (!object.Equals(element.ReadLocalValue(ChangeInfoFilterProperty), value))
            {

                element.SetValue(ChangeInfoFilterProperty, value);
            }
        }

        /// <summary>
        /// Sets the register flag
        /// </summary>
        /// <param name="element"></param>
        /// <param name="value"></param>
        public static void SetChangeInfoValueConverter(DependencyObject element, IChangeValueConverter value)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }
            if (!object.Equals(element.ReadLocalValue(ChangeInfoValueConverterProperty), value))
            {

                element.SetValue(ChangeInfoValueConverterProperty, value);
            }
        }

        /// <summary>
        /// Sets the reports to error provider control
        /// </summary>
        /// <param name="element"></param>
        /// <param name="value"></param>
        public static void SetReportsToChangeInfoProvider(DependencyObject element, bool value)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }
            if (!object.Equals(element.ReadLocalValue(ReportsToChangeInfoProviderProperty), value))
            {

                element.SetValue(ReportsToChangeInfoProviderProperty, value);
            }
        }

        /// <summary>
        /// Sets the error source name
        /// </summary>
        /// <param name="element"></param>
        /// <param name="value"></param>
        public static void SetInfoSourceName(DependencyObject element, string value)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }
            if (!object.Equals(element.ReadLocalValue(InfoSourceNameProperty), value))
            {
                element.SetValue(InfoSourceNameProperty, value);
            }
        }

        /// <summary>
        /// Sets the error source name
        /// </summary>
        /// <param name="element"></param>
        /// <param name="value"></param>
        public static void SetSourceDataObject(DependencyObject element, object value)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }
            if (!object.Equals(element.ReadLocalValue(SourceDataObjectProperty), value))
            {
                element.SetValue(SourceDataObjectProperty, value);
            }
        }

        /// <summary>
        /// Sets the error source name
        /// </summary>
        /// <param name="element"></param>
        /// <param name="value"></param>
        public static void SetSourceDataType(DependencyObject element, Type value)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }
            if (!object.Equals(element.ReadLocalValue(SourceDataTypeProperty), value))
            {
                element.SetValue(SourceDataTypeProperty, value);
            }
        }

        /// <summary>
        /// Sets the error source name
        /// </summary>
        /// <param name="element"></param>
        /// <param name="value"></param>
        public static void SetFieldName(DependencyObject element, string value)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }
            if (!object.Equals(element.ReadLocalValue(FieldNameProperty), value))
            {
                element.SetValue(FieldNameProperty, value);
            }
        }

        /// <summary>
        /// Sets the validation group parent control
        /// </summary>
        /// <param name="element"></param>
        /// <param name="value"></param>
        internal static void SetChangeInfoGroupParentControl(DependencyObject element, DependencyObject value)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }
            DependencyObject oldVal = element.ReadLocalValue(ChangeInfoGroupParentControlProperty) as DependencyObject;

            if (!object.Equals(oldVal, value))
            {
                element.SetValue(ChangeInfoGroupParentControlProperty, value);
            }
        }

        /// <summary>
        /// Sets the validation group parent control name
        /// </summary>
        /// <param name="element"></param>
        /// <param name="value"></param>
        public static void SetChangeInfoGroupParent(DependencyObject element, string value)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }
            string oldVal = element.ReadLocalValue(ChangeInfoGroupParentProperty) as string;

            if (!object.Equals(oldVal, value))
            {
                element.SetValue(ChangeInfoGroupParentProperty, value);
            }
        }

        /// <summary>
        /// Sets the validation adorner site
        /// </summary>
        /// <param name="element"></param>
        /// <param name="value"></param>
        public static void SetChangeInfoAdornerSite(DependencyObject element, DependencyObject value)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }
            element.SetValue(ChangeInfoAdornerSiteProperty, value);
        }
        /// <summary>
        /// Sets the validation adorner site for
        /// </summary>
        /// <param name="element"></param>
        /// <param name="value"></param>
        public static void SetChangeInfoAdornerSiteFor(DependencyObject element, DependencyObject value)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }
            element.SetValue(ChangeInfoAdornerSiteForProperty, value);
        }

        private static void SetLastChangeInformation(DependencyObject element, string value)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }
            element.SetValue(LastChangeInformationKey, value);
        }

        private static void SetChangeHistoryInformation(DependencyObject element, string value)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }
            element.SetValue(ChangeHistoryInformationKey, value);
        }

        /// <summary>
        /// Show the validation adorner
        /// </summary>
        /// <param name="targetElement"></param>
        /// <param name="show"></param>
        internal static void ShowChangeInfoAdorner(DependencyObject targetElement, bool show)
        {
            bool bHasError = ElementHasError(targetElement);

            if (bHasError)
                show = false;

            ChangeInformationProvider changeinfoProvider = GetChangeInfoProviderForElement(targetElement);
            if(changeinfoProvider != null)
            {
                if (!changeinfoProvider.ChangeInfoAdornerVisible)
                    show = false;
            }

            DependencyObject changeInfoAdornerSite = GetChangeInfoAdornerSite(targetElement);
            if (changeInfoAdornerSite == null)
            {
                changeInfoAdornerSite = targetElement;
            }
            ShowChangeInfoAdornerHelper(targetElement, changeInfoAdornerSite, show);
            targetElement.SetValue(ShowAdornerPropertyKey, show);
        }
        /// <summary>
        /// Validation adorner helper
        /// </summary>
        /// <param name="targetElement"></param>
        /// <param name="adornerSite"></param>
        /// <param name="show"></param>
        private static void ShowChangeInfoAdornerHelper(DependencyObject targetElement, DependencyObject adornerSite, bool show)
        {
            UIElement visual = adornerSite as UIElement;
            if (visual != null)
            {
                AdornerLayer adornerLayer = AdornerLayer.GetAdornerLayer(visual);
                if (adornerLayer != null)
                {
                    TemplatedAdornerEx adorner = visual.ReadLocalValue(ChangeInfoAdornerProperty) as TemplatedAdornerEx;
                    if (show && (adorner == null))
                    {
                        ControlTemplate changeinfoTemplate = GetChangeInfoTemplate(visual);
                        if (changeinfoTemplate == null)
                        {
                            changeinfoTemplate = GetChangeInfoTemplate(targetElement);
                        }
                        if (changeinfoTemplate != null)
                        {
                            adorner = new TemplatedAdornerEx(visual, changeinfoTemplate);
                            adornerLayer.Add(adorner);
                            visual.SetValue(ChangeInfoAdornerProperty, adorner);
                        }
                    }
                    else if (!show && (adorner != null))
                    {
                        adorner.ClearChild();
                        adornerLayer.Remove(adorner);
                        visual.ClearValue(ChangeInfoAdornerProperty);
                    }
                }
            }
        }
        #endregion

        #region static change info provider helper methods
        /// <summary>
        /// Gets the instance of the parent ChangeInformationProvider control if any
        /// </summary>
        /// <param name="element">Element which parent change info provider control should be returned</param>
        /// <remarks>This may also return null if the element is not part of a visual tree</remarks>
        public static ChangeInformationProvider GetChangeInfoProviderForElement(DependencyObject element)
        {
            return UIHelper.TryFindParent<ChangeInformationProvider>(element);
        }

        /// <summary>
        /// Gets the topmost error provider control instance in the visual tree
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        public static ChangeInformationProvider GetTopMostChangeInfoProviderForElement(DependencyObject element)
        {
            ChangeInformationProvider provider = GetChangeInfoProviderForElement(element);
            ChangeInformationProvider parentProvider = null;

            do
            {
                parentProvider = GetChangeInfoProviderForElement(provider);

                if (parentProvider != null)
                    provider = parentProvider;

            } while (parentProvider != null);

            return provider;
        }

        #endregion

        #region Change information provider instance scope
        /// <summary>
        /// Gets a flag indicating if the current error provider instance has
        /// control meta information set
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        internal bool ContainsElementKey(DependencyObject element)
        {
            return _associated_control_data.Contains(element);
        }
        /// <summary>
        /// Checks for data bindings in the element if the internal dictionary does not contain a key for the element
        /// </summary>
        /// <param name="element"></param>
        internal void InspectElement(DependencyObject element)
        {
            if (!ContainsElementKey(element))
            {
                _associated_control_data.Add(element);
                RegisterErrorProvider(element);
            }
        }

        private void UnregisterControls()
        {
            foreach (DependencyObject element in _associated_control_data)
            {
                DependencyPropertyDescriptor descr = DependencyPropertyDescriptor.FromProperty(ErrorProvider.HasErrorProperty, element.GetType());
                if(descr != null)
                    descr.RemoveValueChanged(element, OnErrorChangedHandler);

                //_unlinked_elements.Add(element);
            }

            _associated_control_data.Clear();
        }

                /// <summary>
        /// Find databindings with validation rules and subscribes to the error collection changes event
        /// </summary>
        /// <param name="element"></param>
        private void RegisterErrorProvider(DependencyObject element)
        {
            FrameworkElement felement = element as FrameworkElement;

            if (felement != null)
            {
                DependencyPropertyDescriptor descr = DependencyPropertyDescriptor.FromProperty(ErrorProvider.HasErrorProperty, felement.GetType());
                if(descr != null)
                    descr.AddValueChanged(felement, OnErrorChangedHandler);
            }
        }

        /// <summary>
        /// Checks for unlinked controls and registeres any if found
        /// </summary>
        public void CheckForUnlinkedControls()
        {
            // try to resolve visual tree and find unlinked controls for this error provider
            List<DependencyObject> resolvedElement = new List<DependencyObject>();
            foreach (DependencyObject elem in _unlinked_elements)
            {
                ChangeInformationProvider parentProvider = GetChangeInfoProviderForElement(elem);

                if (parentProvider != null && parentProvider == this)
                {
                    if (!_associated_control_data.Contains(elem))
                    {
                        _associated_control_data.Add(elem);
                    }

                    resolvedElement.Add(elem);

                }
            }

            foreach (DependencyObject elem in resolvedElement)
            {
                _unlinked_elements.Remove(elem);
            }

            //// after loading event of error provider
            //// loop through all registered elements and check for the 
            //// data binding event
            //foreach (DependencyObject elemet in _associated_control_data.Keys)
            //{
            //    RegisterDataBoundErrorChanged(elemet);
            //}
        }

        public void ReloadChangeInformationData()
        {
            if (LoadChangeData != null)
                LoadChangeData(this, new EventArgs());
        }

        private void ClearAllChangesFromProviderElements()
        {
            foreach (DependencyObject element in _associated_control_data)
            {
                RemoveElementAllChanges(element);
            }
        }

        private void RemoveElementAllChanges(DependencyObject element)
        {
            List<ChangeInfo> removeList = new List<ChangeInfo>();

            foreach (ChangeInfo info in _provider_change_list)
            {
                if (info.SourceControl == element)
                    removeList.Add(info);
            }

            foreach (ChangeInfo delError in removeList)
                _provider_change_list.Remove(delError);

            // remove databinding errors from the attached error collection property
            ChangeInfoCollection internalCollection = GetChangesInternal(element);

            if (internalCollection != null)
            {
                List<ChangeInfo> removeErrList = new List<ChangeInfo>();

                foreach (ChangeInfo info in internalCollection)
                {
                    removeErrList.Add(info);
                }

                foreach (ChangeInfo delInfo in removeErrList)
                    RemoveChangeInfo(delInfo, element);

            }
        }

        private void PopulateChangeInformation()
        {
            // clear all change information for all elements
            ClearAllChangesFromProviderElements();

            foreach(DependencyObject control in _associated_control_data)
            {
                object source_data_object = GetSourceDataObject(control);
                Type source_data_type = GetSourceDataType(control);
                string field_name = GetFieldName(control);
                ChangeInfoFilterFlag filter = GetChangeInfoFilter(control);

                if(source_data_object != null)
                {
                    foreach(ChangeInfo changeInfo in _provider_change_list)
                    {
                        if(changeInfo.SourceObject == source_data_object)
                        {
                            if( string.Equals(field_name, changeInfo.FieldName) ||
                                string.IsNullOrEmpty(field_name))
                            {
                                if (CheckFilter(filter, changeInfo))
                                    AddChangeInformation(changeInfo, control);
                            }
                        }
                    }
                }
                else if (source_data_type != null)
                {
                    foreach (ChangeInfo changeInfo in _provider_change_list)
                    {
                        if (changeInfo.SourceObject != null && changeInfo.SourceObject.GetType() == source_data_type)
                        {
                            if (string.Equals(field_name, changeInfo.FieldName) ||
                                string.IsNullOrEmpty(field_name))
                            {
                                if (CheckFilter(filter, changeInfo))
                                    AddChangeInformation(changeInfo, control);
                            }
                        }
                    }
                }
            }
        }

        internal void PopulatechangeInformaton(DependencyObject control)
        {
            if (control != null)
            {
                RemoveElementAllChanges(control);

                object source_data_object = GetSourceDataObject(control);
                Type source_data_type = GetSourceDataType(control);
                string field_name = GetFieldName(control);
                ChangeInfoFilterFlag filter = GetChangeInfoFilter(control);

                if (source_data_object != null)
                {
                    foreach (ChangeInfo changeInfo in _provider_change_list)
                    {
                        if (changeInfo.SourceObject == source_data_object)
                        {
                            if (string.Equals(field_name, changeInfo.FieldName) ||
                                string.IsNullOrEmpty(field_name))
                            {
                                if(CheckFilter(filter, changeInfo))
                                    AddChangeInformation(changeInfo, control);
                            }
                        }
                    }
                }
                else if(source_data_type != null)
                {
                    foreach (ChangeInfo changeInfo in _provider_change_list)
                    {
                        if (changeInfo.SourceObject != null && changeInfo.SourceObject.GetType() == source_data_type)
                        {
                            if (string.Equals(field_name, changeInfo.FieldName) ||
                                string.IsNullOrEmpty(field_name))
                            {
                                if (CheckFilter(filter, changeInfo))
                                    AddChangeInformation(changeInfo, control);
                            }
                        }
                    }
                }
            }
        }

        private void UpdateChangeInfoAdorners()
        {
            // reaktivates all adorner layers for changed controls
            // current errors and current ChangeInfoProvider AdornerVisibility settings
            // will be checked in ShowChangeInfoAdorner()
            foreach (DependencyObject control in _associated_control_data)
            {
                UpdateChangeInfoAdornerForControl(control);
            }
        }

        private void UpdateChangeInfoAdornerForControl(DependencyObject control)
        {
            bool bHasChange = GetHasChange(control);
            DependencyObject parentControl = GetChangeInfoGroupParentControl(control);
            
            ShowChangeInfoAdorner(control, bHasChange);

            if(parentControl != null)
            {
                UpdateChangeInfoAdornerForControl(parentControl);
            }
        }

        private bool CheckFilter(ChangeInfoFilterFlag filterFlag, ChangeInfo info)
        {
            bool bRet = false;

            switch(filterFlag)
            {
                case ChangeInfoFilterFlag.All: bRet = true; break;
                case ChangeInfoFilterFlag.DeleteOnly:
                    bRet = info.ChangeType == ChangeType.Delete; break;
                case ChangeInfoFilterFlag.InsertOnly:
                    bRet = info.ChangeType == ChangeType.Insert; break;
                case ChangeInfoFilterFlag.UpateOnly:
                    bRet = info.ChangeType == ChangeType.Update; break;
                case ChangeInfoFilterFlag.InsertAndDelete:
                    bRet = info.ChangeType == ChangeType.Insert || info.ChangeType == ChangeType.Delete; break;
                case ChangeInfoFilterFlag.InsertAndUpdate:
                    bRet = info.ChangeType == ChangeType.Insert || info.ChangeType == ChangeType.Update; break;
                case ChangeInfoFilterFlag.UpdateAndDelete:
                    bRet = info.ChangeType == ChangeType.Update || info.ChangeType == ChangeType.Delete; break;
            }

            return bRet;
        }

        /// <summary>
        /// Sets the internal key dependency proeprty for the IsValid falg
        /// </summary>
        private void SetHasChangeData()
        {
            bool bOldVal = HasChangeData;
            bool newVal = false;

            newVal = _provider_change_list.Count > 0;

            if (newVal != bOldVal)
            {
                SetValue(HasChangeDataPropertyKey, newVal);
                OnHasChangeDataChanged(new EventArgs());
            }
        }

        /// <summary>
        /// Rais HasChangeDataChanged event
        /// </summary>
        /// <param name="e"></param>
        protected void OnHasChangeDataChanged(EventArgs e)
        {
            if (HasChangeDataChanged != null)
                HasChangeDataChanged(this, e);
        }

        #endregion

        #endregion

        #region event Handler

        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            if(e.Property == ChangeInfoAdornerVisibleProperty)
            {
                UpdateChangeInfoAdorners();
            }
            base.OnPropertyChanged(e);
        }
        #region static attached property event handler
        /// <summary>
        /// Called if the register property changes
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void OnRegisterChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue != null && e.NewValue is bool)
            {
                if ((bool)e.NewValue)
                {
                    // Note: If the XAML parser sets the register property
                    // the data bindings may not be set at this time
                    // So make sure to loop through all registered controls
                    // after error provider loading and check for data bindings
                    ChangeInformationProvider parentProvider = GetChangeInfoProviderForElement(d);
                    if (parentProvider != null)
                    {
                        //parentProvider.RegisterDataBoundErrorChanged(d);

                        if (parentProvider.ContainsElementKey(d))
                            if (_unlinked_elements.Contains(d))
                                _unlinked_elements.Remove(d);
                    }
                    else
                    {
                        if (!_unlinked_elements.Contains(d))
                            _unlinked_elements.Add(d);
                    }
                }
            }
        }

        /// <summary>
        /// Called if the reports to error provider property has been changed
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void OnReportsToChangeInfoProviderChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
        }

        /// <summary>
        /// Called if the validation group parent property changes
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void OnChangeInfoGroupParentControlChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (e.OldValue != null && e.OldValue is DependencyObject)
            {
                ChangeInfoCollection internalCollection = GetChangesInternal(d);

                if(internalCollection != null)
                {
                    foreach(ChangeInfo info in internalCollection)
                    {
                        RemoveChangeInfo(info, e.OldValue as DependencyObject);
                    }
                }
            }

            if (e.NewValue != null && e.NewValue is DependencyObject)
            {
                ChangeInfoCollection internalCollection = GetChangesInternal(d);

                if (internalCollection != null)
                {
                    foreach (ChangeInfo info in internalCollection)
                    {
                        AddChangeInformation(info, e.NewValue as DependencyObject);
                    }
                }
            }
        }

        /// <summary>
        /// Called if the validation group parent property changes
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void OnChangeInfoGroupParentChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue != null && e.NewValue is string)
            {
                ChangeInformationProvider changeinfoProvider = GetTopMostChangeInfoProviderForElement(d);

                if (changeinfoProvider != null)
                {
                    DependencyObject element = UIHelper.FindVisualChildByName<DependencyObject>(changeinfoProvider, e.NewValue as string);

                    SetChangeInfoGroupParentControl(d, element);
                }
            }
            else
            {
                SetChangeInfoGroupParentControl(d, null);
            }
        }

        /// <summary>
        /// Called if the register property changes
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void OnInfoSourceNameChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (!object.Equals(e.OldValue, e.NewValue))
            {
                ChangeInformationProvider changeinfoProvider = GetChangeInfoProviderForElement(d);

                if (changeinfoProvider != null)
                {
                    foreach (ChangeInfo info in changeinfoProvider.ProviderChangeList)
                    {
                        if (e.NewValue == null)
                            info.SourceName = string.Empty;
                        else
                            info.SourceName = Convert.ToString(e.NewValue);
                    }
                }
            }
        }

        /// <summary>
        /// Called if the error template changes
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void OnChangeInfoTemplateChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ChangeInformationProvider changeinfoProvider = GetChangeInfoProviderForElement(d);
            if (changeinfoProvider != null)
            {
                changeinfoProvider.InspectElement(d);

                if (changeinfoProvider.ContainsElementKey(d))
                    if (_unlinked_elements.Contains(d))
                        _unlinked_elements.Remove(d);
            }
            else
            {
                if (!_unlinked_elements.Contains(d))
                    _unlinked_elements.Add(d);
            }

            if (GetHasChange(d))
            {
                ShowChangeInfoAdorner(d, false);
                ShowChangeInfoAdorner(d, true);
            }
        }

        /// <summary>
        /// Called if the validation adorner site changes
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void OnChangeInfoAdornerSiteChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ChangeInformationProvider changeinfoProvider = GetChangeInfoProviderForElement(d);
            if (changeinfoProvider != null)
            {
                changeinfoProvider.InspectElement(d);

                if (changeinfoProvider.ContainsElementKey(d))
                    if (_unlinked_elements.Contains(d))
                        _unlinked_elements.Remove(d);
            }
            else
            {
                if (!_unlinked_elements.Contains(d))
                    _unlinked_elements.Add(d);
            }

            DependencyObject oldValue = (DependencyObject)e.OldValue;
            DependencyObject newValue = (DependencyObject)e.NewValue;
            if (oldValue != null)
            {
                oldValue.ClearValue(ChangeInfoAdornerSiteForProperty);
            }
            if ((newValue != null) && (d != GetChangeInfoAdornerSiteFor(newValue)))
            {
                SetChangeInfoAdornerSiteFor(newValue, d);
            }
            if (GetHasChange(d))
            {
                if (oldValue == null)
                {
                    oldValue = d;
                }
                ShowChangeInfoAdornerHelper(d, oldValue, false);
                ShowChangeInfoAdorner(d, true);
            }
        }

        /// <summary>
        /// Called if the validation adorner site for changes
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void OnChangeInfoAdornerSiteForChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ChangeInformationProvider changeinfoProvider = GetChangeInfoProviderForElement(d);
            if (changeinfoProvider != null)
            {
                changeinfoProvider.InspectElement(d);

                if (changeinfoProvider.ContainsElementKey(d))
                    if (_unlinked_elements.Contains(d))
                        _unlinked_elements.Remove(d);
            }
            else
            {
                if (!_unlinked_elements.Contains(d))
                    _unlinked_elements.Add(d);
            }

            DependencyObject oldValue = (DependencyObject)e.OldValue;
            DependencyObject newValue = (DependencyObject)e.NewValue;
            if (oldValue != null)
            {
                oldValue.ClearValue(ChangeInfoAdornerSiteProperty);
            }
            if ((newValue != null) && (d != GetChangeInfoAdornerSite(newValue)))
            {
                SetChangeInfoAdornerSite(newValue, d);
            }
        }

        /// <summary>
        /// Called if the internal changes collection changes
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void OnChangesInternalChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ChangeInfoCollection newValue = e.NewValue as ChangeInfoCollection;
            if (newValue != null)
            {
                d.SetValue(ChangesPropertyKey, new ReadOnlyObservableCollection<ChangeInfo>(newValue));
            }
            else
            {
                d.ClearValue(ChangesPropertyKey);
            }
        }

        /// <summary>
        /// Called if the internal source data object changes
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void OnSourceDataObjectChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
           object newValue = e.NewValue as object;
           
           ChangeInformationProvider changeinfoProvider = GetChangeInfoProviderForElement(d);
           if (changeinfoProvider != null)
           {
               changeinfoProvider.InspectElement(d);

               if (changeinfoProvider.ContainsElementKey(d))
                   if (_unlinked_elements.Contains(d))
                       _unlinked_elements.Remove(d);

               if (changeinfoProvider.IsInitialized)
               {
                   changeinfoProvider.PopulatechangeInformaton(d);
               }
           }
           else
           {
               if (!_unlinked_elements.Contains(d))
                   _unlinked_elements.Add(d);
           }
        }

        /// <summary>
        /// Called if the internal source data type changes
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void OnSourceDataTypeChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            object newValue = e.NewValue as object;

            ChangeInformationProvider changeinfoProvider = GetChangeInfoProviderForElement(d);
            if (changeinfoProvider != null)
            {
                changeinfoProvider.InspectElement(d);

                if (changeinfoProvider.ContainsElementKey(d))
                    if (_unlinked_elements.Contains(d))
                        _unlinked_elements.Remove(d);

                if (changeinfoProvider.IsInitialized)
                {
                    changeinfoProvider.PopulatechangeInformaton(d);
                }
            }
            else
            {
                if (!_unlinked_elements.Contains(d))
                    _unlinked_elements.Add(d);
            }
        }

        /// <summary>
        /// Called if the field name changes
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void OnFieldNameChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            object newValue = e.NewValue as object;

            ChangeInformationProvider changeinfoProvider = GetChangeInfoProviderForElement(d);
            if (changeinfoProvider != null)
            {
                changeinfoProvider.InspectElement(d);

                if (changeinfoProvider.ContainsElementKey(d))
                    if (_unlinked_elements.Contains(d))
                        _unlinked_elements.Remove(d);

                if (changeinfoProvider.IsInitialized)
                {
                    changeinfoProvider.PopulatechangeInformaton(d);
                }
            }
            else
            {
                if (!_unlinked_elements.Contains(d))
                    _unlinked_elements.Add(d);
            }
        }

        /// <summary>
        /// Called if the field name changes
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void OnChangeInfoFilterChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            object newValue = e.NewValue as object;

            ChangeInformationProvider changeinfoProvider = GetChangeInfoProviderForElement(d);
            if (changeinfoProvider != null)
            {
                changeinfoProvider.InspectElement(d);

                if (changeinfoProvider.ContainsElementKey(d))
                    if (_unlinked_elements.Contains(d))
                        _unlinked_elements.Remove(d);

                if (changeinfoProvider.IsInitialized)
                {
                    changeinfoProvider.PopulatechangeInformaton(d);
                }
            }
            else
            {
                if (!_unlinked_elements.Contains(d))
                    _unlinked_elements.Add(d);
            }
        }
        #endregion

        #region Change info provider handler
        void ChangeInformationProvider_Loaded(object sender, RoutedEventArgs e)
        {
            CheckForUnlinkedControls();
            if (LoadChangeData != null)
                LoadChangeData(this, new EventArgs());
        }

        void ChangeInformationProvider_Unloaded(object sender, RoutedEventArgs e)
        {
            UnregisterControls();
        }

        /// <summary>
        /// Called if the element lost its input focus
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void ChangeInformationElement_FocusLost(object sender, RoutedEventArgs e)
        {
            DependencyObject element = sender as DependencyObject;

            if (element != null)
            {
                //if (GetCustomValidationTrigger(element) == CustomValidationTrigger.FocusLost)
                //{
                //    CustomValidateElement(element);
                //}
            }
        }
        /// <summary>
        /// Called if the element gained the input focus
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void ChangeInformationElement_FocusGained(object sender, RoutedEventArgs e)
        {
            DependencyObject element = sender as DependencyObject;

            if (element != null)
            {
                //if (GetCustomValidationTrigger(element) == CustomValidationTrigger.FocusGained)
                //{
                //    CustomValidateElement(element);
                //}
            }
        }
        #endregion

        #region Validation error changed event handler
        /// <summary>
        /// Called if an associated element is showing an validation error using the ErrprProvider control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnErrorChangedHandler(object sender, EventArgs e)
        {
            DependencyObject element = sender as DependencyObject;

            if (element != null)
            {
                //bool bHasError = ErrorProvider.GetHasError(element);
                bool bHasChange = GetHasChange(element);

                if (bHasChange)
                {
                    //if (bHasError)
                    //{
                    //    ShowChangeInfoAdorner(element, false);
                    //}
                    //else
                    //{
                        
                        // Validation error check is included in ShowChangeInfoAdroner() method
                        ShowChangeInfoAdorner(element, true);
                    //}
                }
            }
        }

        
        #endregion

        #endregion

        #region IDisposable Impl

        private void GenerateListSnapshots()
        {
            _instance_snapshot_unlinked_elements = _unlinked_elements.Select(o => o).ToList();
        }

        public void Dispose()
        {
            Loaded -= ChangeInformationProvider_Loaded;
            Unloaded -= ChangeInformationProvider_Unloaded;
            ClearAllChangesFromProviderElements();
            UnregisterControls();

            _associated_control_data.Clear();
            //_manual_validation_hook.Clear();

            List<DependencyObject> rem_object = new List<DependencyObject>();

            foreach (DependencyObject obj in _unlinked_elements)
            {
                if (!_instance_snapshot_unlinked_elements.Contains(obj))
                    rem_object.Add(obj);
            }

            foreach (DependencyObject del in rem_object)
                _unlinked_elements.Remove(del);

            rem_object.Clear();

            EventHelper.RemoveAllCollectionChangedEventHandlers(_provider_change_list);

            _unlinked_elements.Clear();
            _instance_snapshot_unlinked_elements.Clear();

            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if (!this._disposed)
            {
                BindingOperations.ClearAllBindings(this);
                _disposed = true;

            }
        }

        private bool _disposed = false;
        #endregion

        #region Properties

        /// <summary>
        /// Gets a flag if the current provider holds change data
        /// </summary>
        public bool HasChangeData
        {
            get
            {
                return (bool)GetValue(HasChangeDataProperty);
            }
        }

        /// <summary>
        /// Gets the complete cumulated change list of all controls in the provider
        /// </summary>
        public ObservableCollection<ChangeInfo> ProviderChangeList
        {
            get
            {
                return _provider_change_list;
            }
            set
            {
                _provider_change_list = new ObservableCollection<ChangeInfo>();
                SetHasChangeData();

                _provider_change_list = value;
                _is_initialized = true;

                CheckForUnlinkedControls();
                PopulateChangeInformation();

                SetHasChangeData();
            }
        }

        public bool ChangeInfoAdornerVisible
        {
            get { return (bool) GetValue(ChangeInfoAdornerVisibleProperty); }
            set { SetValue(ChangeInfoAdornerVisibleProperty, value); }
        }

        public bool IsInitialized
        {
            get { return _is_initialized; }
        }
        #endregion

        #region Dependency Properties

        /// <summary>
        /// ChangeInfo group parent control
        /// </summary>
        private static readonly DependencyProperty ChangeInfoGroupParentControlProperty;
        /// <summary>
        /// ChangeInfo group parent control
        /// </summary>
        public static readonly DependencyProperty ChangeInfoGroupParentProperty;

        /// <summary>
        /// Reports changeinfos to change info provider control
        /// </summary>
        public static readonly DependencyProperty ReportsToChangeInfoProviderProperty;

        /// <summary>
        /// change info source name
        /// </summary>
        public static readonly DependencyProperty InfoSourceNameProperty;

        /// <summary>
        /// Change info source object
        /// </summary>
        public static readonly DependencyProperty SourceDataObjectProperty;
        /// <summary>
        /// Change info source object
        /// </summary>
        public static readonly DependencyProperty SourceDataTypeProperty;
        /// <summary>
        /// Change info field name
        /// </summary>
        public static readonly DependencyProperty FieldNameProperty;

        /// <summary>
        /// Changes property returning an ReadOnlyObservableCollection
        /// </summary>
        public static readonly DependencyProperty ChangesProperty;
        /// <summary>
        /// Internal Changes collection for collection setting
        /// </summary>
        internal static readonly DependencyPropertyKey ChangesPropertyKey;




        /// <summary>
        /// Attached property holding the change info template content control
        /// </summary>
        public static readonly DependencyProperty ChangeInfoTemplateProperty;

        /// <summary>
        /// Attached property indicating if the control has an change or not
        /// </summary>
        public static readonly DependencyProperty HasChangeProperty;
        /// <summary>
        /// Internal attached property for setting
        /// </summary>
        internal static readonly DependencyPropertyKey HasChangePropertyKey;
        /// <summary>
        /// Attached property indicating if the control has an change or not
        /// </summary>
        public static readonly DependencyProperty ShowAdornerProperty;
        /// <summary>
        /// Internal attached property for setting
        /// </summary>
        internal static readonly DependencyPropertyKey ShowAdornerPropertyKey;

        /// <summary>
        /// Attached property holding a descriptive string for the last change information
        /// </summary>
        public static readonly DependencyProperty LastChangeInformationProperty;
        /// <summary>
        /// Internal attached property for setting
        /// </summary>
        internal static readonly DependencyPropertyKey LastChangeInformationKey;


        /// <summary>
        /// Attached property holding a descriptive string for the complete change history information
        /// </summary>
        public static readonly DependencyProperty ChangeHistoryInformationProperty;
        /// <summary>
        /// Internal attached property for setting
        /// </summary>
        internal static readonly DependencyPropertyKey ChangeHistoryInformationKey;


        /// <summary>
        /// Attached adorner site for property
        /// </summary>
        public static readonly DependencyProperty ChangeInfoAdornerSiteForProperty;
        /// <summary>
        /// Attached adorner site property
        /// </summary>
        public static readonly DependencyProperty ChangeInfoAdornerSiteProperty;
        /// <summary>
        /// Attached internal errors collection
        /// </summary>
        internal static readonly DependencyProperty ChangesInternalProperty;

        /// <summary>
        /// Internal attached property for managing template changes
        /// </summary>
        private static readonly DependencyProperty ChangeInfoAdornerProperty;


        /// <summary>
        /// Attached property registering the control to the error provider
        /// </summary>
        public static readonly DependencyProperty RegisterProperty;


        /// <summary>
        /// Attached property registering the control to the error provider
        /// </summary>
        public static readonly DependencyProperty ChangeInfoValueConverterProperty;

        /// <summary>
        /// Attached property for change information filtering
        /// </summary>
        public static readonly DependencyProperty ChangeInfoFilterProperty;
        /// <summary>
        /// Dependency property data context name
        /// </summary>
        public static readonly DependencyProperty DataContextName;

        /// <summary>
        /// Dependency property 
        /// </summary>
        public static readonly DependencyProperty HasChangeDataProperty;
        /// <summary>
        /// Private dependency property for setting the IsValid property
        /// </summary>
        private static readonly DependencyPropertyKey HasChangeDataPropertyKey;

        /// <summary>
        /// Dependency property 
        /// </summary>
        public static readonly DependencyProperty ChangeInfoAdornerVisibleProperty;
        #endregion

        #region Events

        public event EventHandler LoadChangeData;
        public event EventHandler HasChangeDataChanged;
        #endregion

        #region Attribs
        private bool _is_initialized = false;

        private List<DependencyObject> _associated_control_data = new List<DependencyObject>();
        private List<DependencyObject> _instance_snapshot_unlinked_elements = new List<DependencyObject>();
        

        private static List<DependencyObject> _unlinked_elements = new List<DependencyObject>();
        private ObservableCollection<ChangeInfo> _provider_change_list = new ObservableCollection<ChangeInfo>();

        private bool _change_haschangedata = true;
        #endregion
    }
}
