﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace LoggingLib.Configuration
{
    /// <summary>
    /// filter element which defines logging filters
    /// </summary>
    public class LogFilterElement : ConfigurationElement
    {
        /// <summary>
        /// Flag indicating if the logging is enabled or disabled
        /// </summary>
        [ConfigurationProperty("LoggingEnabled", DefaultValue = true, IsRequired = true)]
        public bool LoggingEnabled
        {
            get
            {
                return ((bool)(base["LoggingEnabled"]));
            }

            set
            {
                base["LoggingEnabled"] = value;
            }
        }

        /// <summary>
        /// Semicolon separated list of priorities which should be logged.
        /// Only log entries matching the priority you specify here will be logged.
        /// Leave blank to log all priorities.
        /// </summary>
        /// <remarks>The global filter settings for priority and severity filters
        /// apply to ALL categories not explictly configured in the CategoryFilterDetails</remarks>
        [ConfigurationProperty("PriorityFilter", DefaultValue = "", IsRequired = true)]
        public string PriorityFilter
        {
            get
            {
                return ((string)(base["PriorityFilter"]));
            }

            set
            {
                base["PriorityFilter"] = value;
            }
        }

        /// <summary>
        /// Semicolon separated list of severities which should be logged.
        /// Only log entries matching the severity you specify here will be logged.
        /// Leave blank to log all severities.
        /// </summary>
        /// <remarks>The global filter settings for priority and severity filters
		/// apply to ALL categories not explictly configured in the CategoryFilterDetails</remarks>
        [ConfigurationProperty("SeverityFilter", DefaultValue = "", IsRequired = true)]
        public string SeverityFilter
        {
            get
            {
                return ((string)(base["SeverityFilter"]));
            }

            set
            {
                base["SeverityFilter"] = value;
            }
        }

        /// <summary>
        /// Semicolon separated list of category names which should be logged.
        /// Only log entries matching the categories you specify here will be logged.
        /// Leave blank to log all categories.
        /// </summary>
        [ConfigurationProperty("CategoryFilter", DefaultValue = "", IsRequired = true)]
        public string CategoryFilter
        {
            get
            {
                return ((string)(base["CategoryFilter"]));
            }

            set
            {
                base["CategoryFilter"] = value;
            }
        }

        /// <summary>
        /// The value of the property here "CategoryFilterDetail" needs to match that of the config file section
        /// </summary>
        [ConfigurationProperty("CategoryFilterDetails")]
        public CategoryFilterDetailCollection CategoryFilterDetails
        {
            get { return ((CategoryFilterDetailCollection)(base["CategoryFilterDetails"])); }
        }
    }
}
