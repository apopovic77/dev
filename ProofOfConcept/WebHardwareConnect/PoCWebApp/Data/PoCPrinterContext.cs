﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net.Mime;
using System.Security.Policy;
using System.Web;
using PoCShared;
using PoCWebApp.Data.Mapping;

namespace PoCWebApp.Data
{
    public class PoCPrinterContext : DbContext
    {
        static PoCPrinterContext()
        {
            Database.SetInitializer<PoCPrinterContext>(null);
        }

        public PoCPrinterContext()
            : base("Name=PoCPrinterContext")
        {
        }

        // using virtual  DbSets to allow UnitTest mocking
        public virtual DbSet<PrinterItem> Printers { get; set; }
        

        public virtual IDbConnection Connection
        {
            get
            {
                return this.Database.Connection;
            }
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //modelBuilder.HasDefaultSchema("globe");

            modelBuilder.Configurations.Add(new PrintersMap());
           
        }
    }
}