﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Logicx.WpfUtility.Collections
{
    public class SortedCollection<TValue> : IList<TValue>, ICollection<TValue>, IEnumerable<TValue>, IEnumerable
    {
        // Fields
        private readonly IComparer<TValue> comparer;
        private const int DEFAULT_CAPACITY = 4;
        private static TValue[] emptyValues;
        private int size;
        private TValue[] values;
        private int version;

        // Methods
        static SortedCollection()
        {
            SortedCollection<TValue>.emptyValues = new TValue[0];
        }

        public SortedCollection()
        {
            this.values = SortedCollection<TValue>.emptyValues;
            this.comparer = Comparer<TValue>.Default;
        }

        public SortedCollection(IComparer<TValue> comparer)
        {
            this.values = SortedCollection<TValue>.emptyValues;
            this.comparer = comparer;
        }

        public void Add(TValue value)
        {
            if (value == null)
            {
                throw new ArgumentException("Value can't be null");
            }
            int index = Array.BinarySearch<TValue>(this.values, 0, this.size, value, this.comparer);
            if (index < 0)
            {
                index = ~index;
            }
            this.Insert(index, value);
        }

        public void AddRange(IEnumerable<TValue> list)
        {
            foreach (TValue value in list)
                Add(value);
        }

        public void InitFromSortedRange(IEnumerable<TValue> list)
        {
            if (Count > 0)
                Clear();

            int index = 0;
            foreach (TValue value in list)
            {
                this.Insert(index++, value);
            }
        }

        private void CheckCapacity(int min)
        {
            int num = (this.values.Length == 0) ? 4 : (this.values.Length * 2);
            if (min > num)
            {
                num = min;
            }
            this.Capacity = num;
        }

        public virtual void Clear()
        {
            this.version++;
            Array.Clear(this.values, 0, this.size);
            this.size = 0;
        }

        public bool Contains(TValue value)
        {
            return (this.IndexOf(value) >= 0);
        }

        public void CopyTo(TValue[] array, int arrayIndex)
        {
            Array.Copy(this.values, 0, array, arrayIndex, this.size);
        }

        public IEnumerator<TValue> GetEnumerator()
        {
            return new SortedCollectionEnumerator<TValue>((SortedCollection<TValue>)this);
        }

        public int IndexOf(TValue value)
        {
            if(typeof(TValue).IsValueType)
            {
                if (value == null)
                {
                    throw new ArgumentException("Value can't be null.");
                }
                int num = Array.BinarySearch<TValue>(this.values, 0, this.size, value, this.comparer);
                if (num >= 0)
                {
                    return num;
                }
                return -1;
            }

            
            // wenn TValue kein value type ist, dann die tatsächliche instanz suchen
            int nret = 0;
            for(int i = 0; i < Count; i++)
            {
                if (this.values[i].GetHashCode() == value.GetHashCode())
                    return i;

            }
            return -1;
        }

        public virtual void Insert(int index, TValue value)
        {
            if (value == null)
            {
                throw new ArgumentException("Value can't be null.");
            }
            if ((index < 0) || (index > this.size))
            {
                throw new ArgumentOutOfRangeException();
            }
            if (this.size == this.values.Length)
            {
                this.CheckCapacity(this.size + 1);
            }
            if (index < this.size)
            {
                Array.Copy(this.values, index, this.values, index + 1, this.size - index);
            }
            this.values[index] = value;
            this.size++;
            this.version++;
        }

        public bool Remove(TValue value)
        {
            int index = this.IndexOf(value);
            if (index < 0)
            {
                return false;
            }
            this.RemoveAt(index);
            return true;
        }

        public virtual void RemoveAt(int index)
        {
            if ((index < 0) || (index >= this.size))
            {
                throw new ArgumentOutOfRangeException();
            }
            this.size--;
            this.version++;
            Array.Copy(this.values, index + 1, this.values, index, this.size - index);
            this.values[this.size] = default(TValue);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return new SortedCollectionEnumerator<TValue>((SortedCollection<TValue>)this);
        }

        // Properties
        public int Capacity
        {
            get
            {
                return this.values.Length;
            }
            set
            {
                if (this.values.Length != value)
                {
                    if (value < this.size)
                    {
                        throw new ArgumentException("Too small capacity.");
                    }
                    if (value > 0)
                    {
                        TValue[] destinationArray = new TValue[value];
                        if (this.size > 0)
                        {
                            Array.Copy(this.values, 0, destinationArray, 0, this.size);
                        }
                        this.values = destinationArray;
                    }
                    else
                    {
                        this.values = SortedCollection<TValue>.emptyValues;
                    }
                }
            }
        }

        public int Count
        {
            get
            {
                return this.size;
            }
        }

        public bool IsReadOnly
        {
            get
            {
                return false;
            }
        }

        public virtual TValue this[int index]
        {
            get
            {
                if ((index < 0) || (index >= this.size))
                {
                    throw new ArgumentOutOfRangeException();
                }
                return this.values[index];
            }
            set
            {
                if ((index < 0) || (index >= this.size))
                {
                    throw new ArgumentOutOfRangeException();
                }
                this.values[index] = value;
                this.version++;
            }
        }

        // Nested Types
        [Serializable]
        private sealed class SortedCollectionEnumerator<TValue> : IEnumerator<TValue>, IDisposable, IEnumerator
        {
            // Fields
            private readonly SortedCollection<TValue> collection;
            private TValue currentValue;
            private int index;
            private int version;

            // Methods
            internal SortedCollectionEnumerator(SortedCollection<TValue> collection)
            {
                this.collection = collection;
                this.version = collection.version;
            }

            public void Dispose()
            {
                this.index = 0;
                this.currentValue = default(TValue);
            }

            public bool MoveNext()
            {
                if (this.version != this.collection.version)
                {
                    throw new ArgumentException("Collection was changed while iterating!");
                }
                if (this.index < this.collection.Count)
                {
                    this.currentValue = this.collection.values[this.index];
                    this.index++;
                    return true;
                }
                this.index = this.collection.Count + 1;
                this.currentValue = default(TValue);
                return false;
            }

            void IEnumerator.Reset()
            {
                if (this.version != this.collection.version)
                {
                    throw new ArgumentException("Collection was changed while iterating!");
                }
                this.index = 0;
                this.currentValue = default(TValue);
            }

            // Properties
            public TValue Current
            {
                get
                {
                    return this.currentValue;
                }
            }

            object IEnumerator.Current
            {
                get
                {
                    if ((this.index == 0) || (this.index == (this.collection.Count + 1)))
                    {
                        throw new ArgumentException("Enumerator not initilized. Call MoveNext first.");
                    }
                    return this.currentValue;
                }
            }
        }
    }


}
