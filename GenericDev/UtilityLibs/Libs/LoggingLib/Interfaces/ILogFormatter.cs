﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LoggingLib.Data;

namespace LoggingLib.Interfaces
{
    /// <summary>
    /// Interface definition for log entry formatter
    /// </summary>
    public interface ILogFormatter
    {
        string FormatLogEntry(LogEntry entry);
    }
}
