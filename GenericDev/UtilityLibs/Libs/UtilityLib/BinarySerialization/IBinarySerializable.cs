﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Utility.BinarySerialization
{
    public class BinarySerializeableHelper
    {
        public enum IdDataType
        {
            Null = 0,
            Int32 = 1,
            Int64 = 2,
            String = 3
        }
        public static object GetObjectValue(object value, IdDataType id_data_type)
        {
            object value_org;
            switch (id_data_type)
            {
                case IdDataType.Null:
                    value_org = null;
                    break;
                case IdDataType.Int32:
                    value_org = Convert.ToInt32(value);
                    break;
                case IdDataType.Int64:
                    value_org = Convert.ToInt64(value);
                    break;
                case IdDataType.String:
                    value_org = value;
                    break;
                default:
                    throw new NotImplementedException();
            }
            return value_org;
        }

        public static int GetIdDataType(object id)
        {
            if (id is Int32)
                return 1;
            if (id is Int64)
                return 2;
            return 3;
        }

        public static object ReadMuteableId(BinaryReader binreader, object id)
        {
            id = binreader.ReadString();
            IdDataType id_data_type = (IdDataType)binreader.ReadInt32();
            id = GetObjectValue(id, id_data_type);
            return id;
        }

        public static void WriteMuteableId(BinaryWriter binwriter, object id)
        {
            string id_str = "";
            if (id != null)
                id_str = id.ToString();
            binwriter.Write(id_str);
            binwriter.Write(BinarySerializeableHelper.GetIdDataType(id));
        }
    }

    public interface IBinarySerializable
    {
        void Serialize(BinaryWriter binwriter);
        void Deserialize(BinaryReader binreader);
    }
}
