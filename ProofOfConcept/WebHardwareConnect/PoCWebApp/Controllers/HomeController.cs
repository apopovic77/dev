﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.SignalR;
using PoCWebApp.Data;
using PoCWebApp.Hubs;
using PoCWebApp.Models;

namespace PoCWebApp.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Print()
        {
            var vm = new PrintViewModel();
            InitPrintingViewBag();
            string remoteHost = (string) ViewBag.RemoteHost;

            using (var db = new PoCPrinterContext())
            {
                vm.AvailablePrinters = LoadAvailablePrinters(db, remoteHost);

                var printers = db.Printers.Where(p => p.PrintServer == remoteHost);
                vm.SelectedPrinter = printers.FirstOrDefault(p => p.Network)?.Id ?? 0;
                if(vm.SelectedPrinter <= 0)
                    vm.SelectedPrinter = printers.FirstOrDefault()?.Id ?? 0;
            }

            return View(vm);
        }

        [HttpPost]
        public ActionResult Print(PrintViewModel vm)
        {
            InitPrintingViewBag();
            string remoteHost = (string)ViewBag.RemoteHost;

            var context = GlobalHost.ConnectionManager.GetHubContext<PrinterCommsHub>();
            using (var db = new PoCPrinterContext())
            {
                vm.AvailablePrinters = LoadAvailablePrinters(db, remoteHost);

                var item = db.Printers.FirstOrDefault(p => p.Id == vm.SelectedPrinter);
                if (item != null)
                {
                    var urlToDocument = "http://awimob02/pocprint/content/NeoSense-Featureliste-2016-09-01.pdf";

                    context.Clients.Group(item.PrintServer).printDocument(item, urlToDocument);
                }
            }
                return View(vm);
        }

       

        private List<SelectListItem> LoadAvailablePrinters(PoCPrinterContext db, string printServer)
        {
            var printers = db.Printers.Where(p => p.PrintServer == printServer);
            return printers.Select(p => new SelectListItem()
            {
                Text = p.Name,
                Value = p.Id.ToString()
            }).ToList();
        }
        private void InitPrintingViewBag()
        {
            // only for local testing
            var hostname = (Dns.GetHostEntry(Request.ServerVariables["REMOTE_ADDR"]).HostName);
            if (hostname.IndexOf(".", StringComparison.Ordinal) >= 0)
                hostname = hostname.Substring(0, hostname.IndexOf(".", StringComparison.Ordinal));

            ViewBag.RemoteHost = hostname.ToUpper();
            ViewBag.RemoteIP = Request.ServerVariables["REMOTE_ADDR"];
        }
    }
}