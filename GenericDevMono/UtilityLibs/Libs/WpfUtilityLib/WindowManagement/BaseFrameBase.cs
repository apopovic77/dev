﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Shapes;
using System.Windows.Threading;
using Logicx.Utilities;
using System.Windows.Interop;
using Logicx.WpfUtility.CustomControls.OverlayManager;
using Logicx.WpfUtility.Tooltip;
using Logicx.WpfUtility.WindowParallelisation.WCFComm;
using Logicx.WpfUtility.WpfHelpers;
using DoWorkEventArgs = Logicx.Utilities.DoWorkEventArgs;
using RunWorkerCompletedEventArgs = Logicx.Utilities.RunWorkerCompletedEventArgs;

namespace Logicx.WpfUtility.WindowManagement
{
    /// <summary>
    /// POINT aka POINTAPI
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct POINT
    {
        /// <summary>
        /// x coordinate of point.
        /// </summary>
        public int x;
        /// <summary>
        /// y coordinate of point.
        /// </summary>
        public int y;

        /// <summary>
        /// Construct a point of coordinates (x,y).
        /// </summary>
        public POINT(int x, int y)
        {
            this.x = x;
            this.y = y;
        }
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct MINMAXINFO
    {
        public POINT ptReserved;
        public POINT ptMaxSize;
        public POINT ptMaxPosition;
        public POINT ptMinTrackSize;
        public POINT ptMaxTrackSize;
    };

    /// <summary>
    /// </summary>
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
    public class MONITORINFO
    {
        /// <summary>
        /// </summary>            
        public int cbSize = Marshal.SizeOf(typeof(MONITORINFO));

        /// <summary>
        /// </summary>            
        public RECT rcMonitor = new RECT();

        /// <summary>
        /// </summary>            
        public RECT rcWork = new RECT();

        /// <summary>
        /// </summary>            
        public int dwFlags = 0;
    }
    
    /// <summary> Win32 </summary>
    [StructLayout(LayoutKind.Sequential, Pack = 0)]
    public struct RECT
    {
        /// <summary> Win32 </summary>
        public int left;
        /// <summary> Win32 </summary>
        public int top;
        /// <summary> Win32 </summary>
        public int right;
        /// <summary> Win32 </summary>
        public int bottom;

        /// <summary> Win32 </summary>
        public static readonly RECT Empty = new RECT();

        /// <summary> Win32 </summary>
        public int Width
        {
            get { return Math.Abs(right - left); }  // Abs needed for BIDI OS
        }
        /// <summary> Win32 </summary>
        public int Height
        {
            get { return bottom - top; }
        }

        /// <summary> Win32 </summary>
        public RECT(int left, int top, int right, int bottom)
        {
            this.left = left;
            this.top = top;
            this.right = right;
            this.bottom = bottom;
        }


        /// <summary> Win32 </summary>
        public RECT(RECT rcSrc)
        {
            this.left = rcSrc.left;
            this.top = rcSrc.top;
            this.right = rcSrc.right;
            this.bottom = rcSrc.bottom;
        }

        /// <summary> Win32 </summary>
        public bool IsEmpty
        {
            get
            {
                // BUGBUG : On Bidi OS (hebrew arabic) left > right
                return left >= right || top >= bottom;
            }
        }
        /// <summary> Return a user friendly representation of this struct </summary>
        public override string ToString()
        {
            if (this == RECT.Empty) { return "RECT {Empty}"; }
            return "RECT { left : " + left + " / top : " + top + " / right : " + right + " / bottom : " + bottom + " }";
        }

        /// <summary> Determine if 2 RECT are equal (deep compare) </summary>
        public override bool Equals(object obj)
        {
            if (!(obj is Rect)) { return false; }
            return (this == (RECT)obj);
        }

        /// <summary>Return the HashCode for this struct (not garanteed to be unique)</summary>
        public override int GetHashCode()
        {
            return left.GetHashCode() + top.GetHashCode() + right.GetHashCode() + bottom.GetHashCode();
        }


        /// <summary> Determine if 2 RECT are equal (deep compare)</summary>
        public static bool operator ==(RECT rect1, RECT rect2)
        {
            return (rect1.left == rect2.left && rect1.top == rect2.top && rect1.right == rect2.right && rect1.bottom == rect2.bottom);
        }

        /// <summary> Determine if 2 RECT are different(deep compare)</summary>
        public static bool operator !=(RECT rect1, RECT rect2)
        {
            return !(rect1 == rect2);
        }


    }
  
    public interface IBaseFrameBase : IInputElement
    {
        bool CanClose();
        void AttachBoundedWindowApp(string path_to_bounded_window_app, string pipe_comm_name);
        void DisableClicksMainArea();
        void EnableClicksMainArea();
        void SetMaximizedCheck(bool is_checked);
        void SeFullscreenCheck(bool is_checked);
        void WriteDebugInfo(string msg);
        Thickness ResizerBorderMargin { get; set; }
        Brush TitleBarBrush { get; set; }
        Brush TitleBarInActiveBrush { get; set; }
        Brush TaskBarBrush { get; set; }
        Brush MainAreaBrush { get; set; }
        Brush WindowBorderBrush { get; set; }
        Brush DefaultForegroundBrush { get; set; }
        Brush HooverBrush { get; set; }
        Brush HooverStrokeBrush { get; set; }
        double HooverStrokeThickness { get; set; }
        Brush SubMenuPanelBackground { get; set; }
        bool AreMouseClicksDisabled { get; }
        double ResizerGripThickness { get; set; }
        int MinHeightMainContent { get; }
        double ActualTaskBarHeight { get; }
        double ActualTitleBarHeight { get; }
        double ActualTaskBarBaseBorderHeight { get; }
        
        /// <summary>
        /// Wenn true dann werden Images für den Border geladen - ausserdem wird allowstransparency auf true gesetzt
        /// das ganze schaut kewl aus kostet es aber massig wpf render performance, für anspruchsvolle main contents
        /// sollte dieser wert auf false gesetzt werden
        /// </summary>
        bool WithTransparentWindowBorder { set; }

        Brush AdornerActiveRenderBrush { get; }
        Pen AdornerActiveRenderPen { get; }
        Grid MainContentBaseGrid { get; }
        Border WindowBorder { get; }
        TooltipPanel TooltipPanel { get; }
        WindowResizer WindowResizer { get; }
        Window BaseWindow { get; }
        Page BasePage { get; }
        object Content { get; set; }
        bool HasContent { get; }
        DataTemplate ContentTemplate { get; set; }
        DataTemplateSelector ContentTemplateSelector { get; set; }
        string ContentStringFormat { get; set; }
        Brush BorderBrush { get; set; }
        Thickness BorderThickness { get; set; }
        Brush Background { get; set; }
        Brush Foreground { get; set; }
        FontFamily FontFamily { get; set; }
        double FontSize { get; set; }
        FontStretch FontStretch { get; set; }
        FontStyle FontStyle { get; set; }
        FontWeight FontWeight { get; set; }
        HorizontalAlignment HorizontalContentAlignment { get; set; }
        VerticalAlignment VerticalContentAlignment { get; set; }
        int TabIndex { get; set; }
        bool IsTabStop { get; set; }
        Thickness Padding { get; set; }
        ControlTemplate Template { get; set; }
        Style Style { get; set; }
        bool OverridesDefaultStyle { get; set; }
        TriggerCollection Triggers { get; }
        DependencyObject TemplatedParent { get; }
        ResourceDictionary Resources { get; set; }
        object DataContext { get; set; }
        BindingGroup BindingGroup { get; set; }
        XmlLanguage Language { get; set; }
        string Name { get; set; }
        object Tag { get; set; }
        InputScope InputScope { get; set; }
        double ActualWidth { get; }
        double ActualHeight { get; }
        Transform LayoutTransform { get; set; }
        double Width { get; set; }
        double MinWidth { get; set; }
        double MaxWidth { get; set; }
        double Height { get; set; }
        double MinHeight { get; set; }
        double MaxHeight { get; set; }
        FlowDirection FlowDirection { get; set; }
        Thickness Margin { get; set; }
        HorizontalAlignment HorizontalAlignment { get; set; }
        VerticalAlignment VerticalAlignment { get; set; }
        Style FocusVisualStyle { get; set; }
        Cursor Cursor { get; set; }
        bool ForceCursor { get; set; }
        bool IsInitialized { get; }
        bool IsLoaded { get; }
        object ToolTip { get; set; }
        ContextMenu ContextMenu { get; set; }
        DependencyObject Parent { get; }
        bool HasAnimatedProperties { get; }
        InputBindingCollection InputBindings { get; }
        CommandBindingCollection CommandBindings { get; }
        bool AllowDrop { get; set; }
        Size DesiredSize { get; }
        bool IsMeasureValid { get; }
        bool IsArrangeValid { get; }
        Size RenderSize { get; set; }
        Transform RenderTransform { get; set; }
        Point RenderTransformOrigin { get; set; }
        bool IsMouseDirectlyOver { get; }
        bool IsMouseOver { get; }
        bool IsStylusOver { get; }
        bool IsKeyboardFocusWithin { get; }
        bool IsMouseCaptured { get; }
        bool IsMouseCaptureWithin { get; }
        bool IsStylusDirectlyOver { get; }
        bool IsStylusCaptured { get; }
        bool IsStylusCaptureWithin { get; }
        bool IsKeyboardFocused { get; }
        bool IsInputMethodEnabled { get; }
        double Opacity { get; set; }
        Brush OpacityMask { get; set; }
        BitmapEffect BitmapEffect { get; set; }
        Effect Effect { get; set; }
        BitmapEffectInput BitmapEffectInput { get; set; }
        string Uid { get; set; }
        Visibility Visibility { get; set; }
        bool ClipToBounds { get; set; }
        Geometry Clip { get; set; }
        bool SnapsToDevicePixels { get; set; }
        bool IsFocused { get; }
        bool IsEnabled { get; set; }
        bool IsHitTestVisible { get; set; }
        bool IsVisible { get; }
        bool Focusable { get; set; }
        int PersistId { get; }
        DependencyObjectType DependencyObjectType { get; }
        bool IsSealed { get; }
        Dispatcher Dispatcher { get; }

        void EnableMouseClicks();
        void DisableMouseClicks();
        void SetFixedCursor(Cursor cursor);
        void SetAppCursor(Cursor cursor);
        void SetDefaultAppCursor();
        void OnApplyTemplate();
        bool ShouldSerializeContent();
        string ToString();
        event MouseButtonEventHandler PreviewMouseDoubleClick;
        event MouseButtonEventHandler MouseDoubleClick;
        bool ShouldSerializeStyle();
        bool ApplyTemplate();
        void BeginStoryboard(Storyboard storyboard);
        void BeginStoryboard(Storyboard storyboard, HandoffBehavior handoffBehavior);
        void BeginStoryboard(Storyboard storyboard, HandoffBehavior handoffBehavior, bool isControllable);
        bool ShouldSerializeTriggers();
        bool ShouldSerializeResources();
        object FindResource(object resourceKey);
        object TryFindResource(object resourceKey);
        void SetResourceReference(DependencyProperty dp, object name);
        BindingExpression GetBindingExpression(DependencyProperty dp);
        BindingExpressionBase SetBinding(DependencyProperty dp, BindingBase binding);
        BindingExpression SetBinding(DependencyProperty dp, string path);
        void BringIntoView();
        void BringIntoView(Rect targetRectangle);
        bool MoveFocus(TraversalRequest request);
        DependencyObject PredictFocus(FocusNavigationDirection direction);
        void BeginInit();
        void EndInit();
        void RegisterName(string name, object scopedElement);
        void UnregisterName(string name);
        object FindName(string name);
        event EventHandler<DataTransferEventArgs> TargetUpdated;
        event EventHandler<DataTransferEventArgs> SourceUpdated;
        event DependencyPropertyChangedEventHandler DataContextChanged;
        event RequestBringIntoViewEventHandler RequestBringIntoView;
        event SizeChangedEventHandler SizeChanged;
        event EventHandler Initialized;
        event RoutedEventHandler Loaded;
        event RoutedEventHandler Unloaded;
        event ToolTipEventHandler ToolTipOpening;
        event ToolTipEventHandler ToolTipClosing;
        event ContextMenuEventHandler ContextMenuOpening;
        event ContextMenuEventHandler ContextMenuClosing;
        void ApplyAnimationClock(DependencyProperty dp, AnimationClock clock);
        void ApplyAnimationClock(DependencyProperty dp, AnimationClock clock, HandoffBehavior handoffBehavior);
        void BeginAnimation(DependencyProperty dp, AnimationTimeline animation);
        void BeginAnimation(DependencyProperty dp, AnimationTimeline animation, HandoffBehavior handoffBehavior);
        object GetAnimationBaseValue(DependencyProperty dp);
        bool ShouldSerializeInputBindings();
        bool ShouldSerializeCommandBindings();
        void RaiseEvent(RoutedEventArgs e);
        void AddHandler(RoutedEvent routedEvent, Delegate handler);
        void AddHandler(RoutedEvent routedEvent, Delegate handler, bool handledEventsToo);
        void RemoveHandler(RoutedEvent routedEvent, Delegate handler);
        void AddToEventRoute(EventRoute route, RoutedEventArgs e);
        void InvalidateMeasure();
        void InvalidateArrange();
        void InvalidateVisual();
        void Measure(Size availableSize);
        void Arrange(Rect finalRect);
        void UpdateLayout();
        Point TranslatePoint(Point point, UIElement relativeTo);
        IInputElement InputHitTest(Point point);
        bool CaptureMouse();
        void ReleaseMouseCapture();
        bool CaptureStylus();
        void ReleaseStylusCapture();
        bool Focus();
        event MouseButtonEventHandler PreviewMouseDown;
        event MouseButtonEventHandler MouseDown;
        event MouseButtonEventHandler PreviewMouseUp;
        event MouseButtonEventHandler MouseUp;
        event MouseButtonEventHandler PreviewMouseLeftButtonDown;
        event MouseButtonEventHandler MouseLeftButtonDown;
        event MouseButtonEventHandler PreviewMouseLeftButtonUp;
        event MouseButtonEventHandler MouseLeftButtonUp;
        event MouseButtonEventHandler PreviewMouseRightButtonDown;
        event MouseButtonEventHandler MouseRightButtonDown;
        event MouseButtonEventHandler PreviewMouseRightButtonUp;
        event MouseButtonEventHandler MouseRightButtonUp;
        event MouseEventHandler PreviewMouseMove;
        event MouseEventHandler MouseMove;
        event MouseWheelEventHandler PreviewMouseWheel;
        event MouseWheelEventHandler MouseWheel;
        event MouseEventHandler MouseEnter;
        event MouseEventHandler MouseLeave;
        event MouseEventHandler GotMouseCapture;
        event MouseEventHandler LostMouseCapture;
        event QueryCursorEventHandler QueryCursor;
        event StylusDownEventHandler PreviewStylusDown;
        event StylusDownEventHandler StylusDown;
        event StylusEventHandler PreviewStylusUp;
        event StylusEventHandler StylusUp;
        event StylusEventHandler PreviewStylusMove;
        event StylusEventHandler StylusMove;
        event StylusEventHandler PreviewStylusInAirMove;
        event StylusEventHandler StylusInAirMove;
        event StylusEventHandler StylusEnter;
        event StylusEventHandler StylusLeave;
        event StylusEventHandler PreviewStylusInRange;
        event StylusEventHandler StylusInRange;
        event StylusEventHandler PreviewStylusOutOfRange;
        event StylusEventHandler StylusOutOfRange;
        event StylusSystemGestureEventHandler PreviewStylusSystemGesture;
        event StylusSystemGestureEventHandler StylusSystemGesture;
        event StylusEventHandler GotStylusCapture;
        event StylusEventHandler LostStylusCapture;
        event StylusButtonEventHandler StylusButtonDown;
        event StylusButtonEventHandler StylusButtonUp;
        event StylusButtonEventHandler PreviewStylusButtonDown;
        event StylusButtonEventHandler PreviewStylusButtonUp;
        event KeyEventHandler PreviewKeyDown;
        event KeyEventHandler KeyDown;
        event KeyEventHandler PreviewKeyUp;
        event KeyEventHandler KeyUp;
        event KeyboardFocusChangedEventHandler PreviewGotKeyboardFocus;
        event KeyboardFocusChangedEventHandler GotKeyboardFocus;
        event KeyboardFocusChangedEventHandler PreviewLostKeyboardFocus;
        event KeyboardFocusChangedEventHandler LostKeyboardFocus;
        event TextCompositionEventHandler PreviewTextInput;
        event TextCompositionEventHandler TextInput;
        event QueryContinueDragEventHandler PreviewQueryContinueDrag;
        event QueryContinueDragEventHandler QueryContinueDrag;
        event GiveFeedbackEventHandler PreviewGiveFeedback;
        event GiveFeedbackEventHandler GiveFeedback;
        event DragEventHandler PreviewDragEnter;
        event DragEventHandler DragEnter;
        event DragEventHandler PreviewDragOver;
        event DragEventHandler DragOver;
        event DragEventHandler PreviewDragLeave;
        event DragEventHandler DragLeave;
        event DragEventHandler PreviewDrop;
        event DragEventHandler Drop;
        event DependencyPropertyChangedEventHandler IsMouseDirectlyOverChanged;
        event DependencyPropertyChangedEventHandler IsKeyboardFocusWithinChanged;
        event DependencyPropertyChangedEventHandler IsMouseCapturedChanged;
        event DependencyPropertyChangedEventHandler IsMouseCaptureWithinChanged;
        event DependencyPropertyChangedEventHandler IsStylusDirectlyOverChanged;
        event DependencyPropertyChangedEventHandler IsStylusCapturedChanged;
        event DependencyPropertyChangedEventHandler IsStylusCaptureWithinChanged;
        event DependencyPropertyChangedEventHandler IsKeyboardFocusedChanged;
        event EventHandler LayoutUpdated;
        event RoutedEventHandler GotFocus;
        event RoutedEventHandler LostFocus;
        event DependencyPropertyChangedEventHandler IsEnabledChanged;
        event DependencyPropertyChangedEventHandler IsHitTestVisibleChanged;
        event DependencyPropertyChangedEventHandler IsVisibleChanged;
        event DependencyPropertyChangedEventHandler FocusableChanged;
        bool IsAncestorOf(DependencyObject descendant);
        bool IsDescendantOf(DependencyObject ancestor);
        DependencyObject FindCommonVisualAncestor(DependencyObject otherVisual);
        GeneralTransform TransformToAncestor(Visual ancestor);
        GeneralTransform2DTo3D TransformToAncestor(Visual3D ancestor);
        GeneralTransform TransformToDescendant(Visual descendant);
        GeneralTransform TransformToVisual(Visual visual);
        Point PointToScreen(Point point);
        Point PointFromScreen(Point point);
        bool Equals(object obj);
        int GetHashCode();
        object GetValue(DependencyProperty dp);
        void SetValue(DependencyProperty dp, object value);
        void SetValue(DependencyPropertyKey key, object value);
        void ClearValue(DependencyProperty dp);
        void ClearValue(DependencyPropertyKey key);
        void CoerceValue(DependencyProperty dp);
        void InvalidateProperty(DependencyProperty dp);
        object ReadLocalValue(DependencyProperty dp);
        LocalValueEnumerator GetLocalValueEnumerator();
        bool CheckAccess();
        void VerifyAccess();
        void ReportInitStatus(double perc, string status);

        //IWindowTaskBarBase TaskBar { get; }
    }

    public abstract class BaseFrameBase<B, S, T, U, O, SU> : ContentControl, IBaseFrameBase
        where S : SubMenuPanelBase<B, S, T, U, O, SU>, new()
        where B : BaseFrameBase<B, S, T, U, O, SU>
        where T : WindowTaskBarBase<B, S, T, U, O, SU>, new()
        where U : WindowTitleBarBase<B, S, T, U, O, SU>, new()
        where O : OverlayManagerBase<B, S, T, U, O, SU>, new()
        where SU : SubMenuPanelMenuItem
    {

        public enum InitialWindowModeEnum
        {
            Normal = 1,
            FullScreen = 2,
            TaskBarVisible = 3
        }

        public enum MaximizedWindowMode
        {
            FullScreen = 1,
            TaskBarVisible = 2
        }

        public enum AppModes
        {
            WpfApp = 1,
            Xbap = 2
        }
        public BaseFrameBase(Window base_window) : this(base_window, null, null)
        {
        }

        public BaseFrameBase(Window base_window, ReportInitStatusDelegate status_delegate)
            : this(base_window, status_delegate, null)
        {
            
        }

        public BaseFrameBase(Window base_window, ReportInitStatusDelegate status_delegate, string[] startup_args)
        {
            _report_init_status = status_delegate;
                StartupArgs = startup_args;
                AppMode = AppModes.WpfApp;
                _base_window = base_window;
                InitWindowHandler();
                _base_window.Loaded += new RoutedEventHandler(_base_window_Loaded);

                FocusVisualStyle = null;
                Focusable = false;

                Initialized += new EventHandler(BaseContent_Initialized);
                Loaded += new RoutedEventHandler(CustomWindow_Loaded);

                //DependencyPropertyDescriptor descr = DependencyPropertyDescriptor.FromProperty(ContentProperty, this.GetType());
                //if (descr != null)
                //    descr.AddValueChanged(this, Content_Changed);
            }

            public BaseFrameBase()
            {
                Initialized += new EventHandler(BaseContent_Initialized);
                Loaded += new RoutedEventHandler(CustomWindow_Loaded);

                FocusVisualStyle = null;
                Focusable = false;

                //DependencyPropertyDescriptor descr = DependencyPropertyDescriptor.FromProperty(ContentProperty, this.GetType());
                //if (descr != null)
                //    descr.AddValueChanged(this, Content_Changed);
            }

            protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
            {
                if (e.Property == ContentProperty)
                {
                    Content_Changed(null, null);
                }
                else if(e.Property == ResizerBorderMarginProperty)
                {
                    if (ResizerBorderMarginChanged != null)
                        ResizerBorderMarginChanged(null,null);
                }

                base.OnPropertyChanged(e);
            }


            void BaseContent_Initialized(object sender, EventArgs e)
            {
                InternalInit();
                if (_base_window == null)
                    return;
                Init();
            }

            void Content_Changed(object sender, EventArgs e)
            {
                if (_base_grid == null)
                    return;

                if (Content == _base_grid)
                    return;

                _main_content.Content = Content;
                Content = _base_grid;
            }

            protected abstract void Init();

            public void ReportInitStatus(double perc, string status)
            {
                if (_report_init_status != null)
                    _report_init_status(perc, status);
            }

            public virtual bool CanClose()
            { return true; }

            protected virtual void InternalInit()
            {
                AppMode = AppModes.WpfApp;
                if (_base_window == null)
                {
                    _base_window = Application.Current.MainWindow;
                    InitWindowHandler();
                }

                if (_base_window == null)
                    return;

                if (_base_window.ToString().ToLower().IndexOf("browser") >= 0)
                    AppMode = AppModes.Xbap;

                _base_grid = new Grid();
                //_base_grid.Background = Brushes.Transparent;
                _base_grid.ClipToBounds = true;
                _base_grid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) });
                _base_grid.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1, GridUnitType.Star) });

                _main_content = new ContentControl();
                _main_content.HorizontalAlignment = HorizontalAlignment.Stretch;
                _main_content.VerticalAlignment = VerticalAlignment.Stretch;
                _main_content.SetBinding(HeightProperty, new Binding("ActualHeight") { Source = _main_content_base });
                _main_content.ClipToBounds = true;

                _main_content_base = new Grid();
                _main_content_base.HorizontalAlignment = HorizontalAlignment.Stretch;
                _main_content_base.SetBinding(BackgroundProperty, new Binding("MainAreaBrush") { Source = this });
                _main_content_base.Children.Add(_main_content);

                InitWindowBorder();
                _base_grid.Children.Add(_window_border);

                InitTaskBar();
                InitTitleBar();

                //_touch_canvas_interceptor = new Canvas();
                //_touch_canvas_interceptor.Background = Brushes.Transparent;
                //_touch_canvas_interceptor.IsHitTestVisible = false;
                //Binding client_width1 = new Binding("ActualWidth");
                //client_width1.Source = _main_content;
                //_touch_canvas_interceptor.SetBinding(WidthProperty, client_width1);

                //Binding client_height1 = new Binding("ActualHeight");
                //client_height1.Source = _main_content;
                //_touch_canvas_interceptor.SetBinding(HeightProperty, client_height1);

                //Grid.SetZIndex(_touch_canvas_interceptor, 9999999);
                //_base_grid.Children.Add(_touch_canvas_interceptor);


                _overlay_manager = new O();
                _overlay_manager.ClipToBounds = true;
                _overlay_manager.HorizontalAlignment = HorizontalAlignment.Center;
                _overlay_manager.VerticalAlignment = VerticalAlignment.Center;

                Binding client_width = new Binding("ActualWidth");
                client_width.Source = _main_content;
                _overlay_manager.SetBinding(OverlayManagerBase<B, S, T, U, O, SU>.AvailableWindowClientWidthProperty, client_width);

                Binding client_height = new Binding("ActualHeight");
                client_height.Source = _main_content;
                _overlay_manager.SetBinding(OverlayManagerBase<B, S, T, U, O, SU>.AvailableWindowClientHeightProperty, client_height);

                _base_grid.Children.Add(_overlay_manager);

                _overlay_grid = new Grid();
                _overlay_grid.HorizontalAlignment = HorizontalAlignment.Center;
                _overlay_grid.VerticalAlignment = VerticalAlignment.Center;
                _overlay_grid.Children.Add(_titlebar);
                _overlay_grid.Children.Add(_taskbar);
                _overlay_manager.AddCustomControl(_overlay_grid);

                _overlay_manager.DecreasingVisibleTopPositionElements.Add(_titlebar);
                //_overlay_manager.DecreasingVisibleBottomPositionElements.Add(_taskbar);
                _overlay_manager.DecreasingVisibleBottomPositionElements.Add(_taskbar.UserInfoGrid);

                _reserved_canvas = new Canvas();
                _reserved_canvas.HorizontalAlignment =HorizontalAlignment.Center;
                _reserved_canvas.VerticalAlignment = VerticalAlignment.Center;
                _reserved_canvas.MouseDown += new MouseButtonEventHandler(_reserved_canvas_MouseDown);
                _reserved_canvas.MouseUp += new MouseButtonEventHandler(_reserved_canvas_MouseUp);
                _reserved_canvas.MouseMove += new MouseEventHandler(_reserved_canvas_MouseMove);
                _reserved_canvas.MouseWheel += new MouseWheelEventHandler(_reserved_canvas_MouseWheel);
                _reserved_canvas.StylusDown += new StylusDownEventHandler(_reserved_canvas_StylusDown);
                _reserved_canvas.StylusUp += new StylusEventHandler(_reserved_canvas_StylusUp);
                _reserved_canvas.StylusMove += new StylusEventHandler(_reserved_canvas_StylusMove);
                _base_grid.Children.Add(_reserved_canvas);

                _tooltip_panel = new TooltipPanel();
                _tooltip_panel.HorizontalAlignment = HorizontalAlignment.Center;
                _tooltip_panel.VerticalAlignment = VerticalAlignment.Center;
                _base_grid.Children.Add(_tooltip_panel);

#if DEBUG
                TextBlock _debug_textblock = new TextBlock();
                _debug_textblock.HorizontalAlignment = HorizontalAlignment.Left;
                _debug_textblock.VerticalAlignment = VerticalAlignment.Bottom;
                _debug_textblock.FontSize = 8;
                _base_grid.Children.Add(_debug_textblock);
                _debug_writer = new TextBlockDebugWriter(_debug_textblock);
                _debug_writer.MaxMessagesToShow = 5;
#endif

                _base_grid.SizeChanged += new SizeChangedEventHandler(BaseGrid_SizeChanged);
                DefaultCursor = Cursor;

                if (AppMode == AppModes.WpfApp)
                {
                    #region Resize Border Init

                    _rect_border_left = new Rectangle();
                    _rect_border_left.HorizontalAlignment = HorizontalAlignment.Left;
                    _rect_border_left.Cursor = Cursors.SizeWE;
                    _base_grid.Children.Add(_rect_border_left);

                    _rect_border_right = new Rectangle();
                    _rect_border_right.HorizontalAlignment = HorizontalAlignment.Right;
                    _rect_border_right.Cursor = Cursors.SizeWE;
                    _base_grid.Children.Add(_rect_border_right);

                    _rect_border_top = new Rectangle();
                    _rect_border_top.VerticalAlignment = VerticalAlignment.Top;
                    _rect_border_top.Cursor = Cursors.SizeNS;
                    _base_grid.Children.Add(_rect_border_top);

                    _rect_border_bottom = new Rectangle();
                    _rect_border_bottom.VerticalAlignment = VerticalAlignment.Bottom;
                    _rect_border_bottom.Cursor = Cursors.SizeNS;
                    _base_grid.Children.Add(_rect_border_bottom);

                    _image_border_topleft = new Rectangle();
                    _image_border_topleft.HorizontalAlignment = HorizontalAlignment.Left;
                    _image_border_topleft.VerticalAlignment = VerticalAlignment.Top;
                    _image_border_topleft.Cursor = Cursors.SizeNWSE;
                    _base_grid.Children.Add(_image_border_topleft);

                    _image_border_topright = new Rectangle();
                    _image_border_topright.HorizontalAlignment = HorizontalAlignment.Right;
                    _image_border_topright.VerticalAlignment = VerticalAlignment.Top;
                    _image_border_topright.Cursor = Cursors.SizeNESW;
                    _base_grid.Children.Add(_image_border_topright);

                    _image_border_bottomleft = new Rectangle();
                    _image_border_bottomleft.HorizontalAlignment = HorizontalAlignment.Left;
                    _image_border_bottomleft.VerticalAlignment = VerticalAlignment.Bottom;
                    _image_border_bottomleft.Cursor = Cursors.SizeNESW;
                    _base_grid.Children.Add(_image_border_bottomleft);

                    _image_border_bottomright = new Rectangle();
                    _image_border_bottomright.HorizontalAlignment = HorizontalAlignment.Right;
                    _image_border_bottomright.VerticalAlignment = VerticalAlignment.Bottom;
                    _image_border_bottomright.Cursor = Cursors.SizeNWSE;
                    _base_grid.Children.Add(_image_border_bottomright);

                    _rect_border_topleft = new Rectangle();
                    _rect_border_topleft.Fill = _resizer_border_fill;
                    _rect_border_topleft.HorizontalAlignment = HorizontalAlignment.Left;
                    _rect_border_topleft.VerticalAlignment = VerticalAlignment.Top;
                    _rect_border_topleft.Cursor = Cursors.SizeNWSE;
                    _rect_border_topleft.Margin = new Thickness(_resizergrip_thickness / 2);
                    _base_grid.Children.Add(_rect_border_topleft);

                    _rect_border_topright = new Rectangle();
                    _rect_border_topright.Fill = _resizer_border_fill;
                    _rect_border_topright.HorizontalAlignment = HorizontalAlignment.Right;
                    _rect_border_topright.VerticalAlignment = VerticalAlignment.Top;
                    _rect_border_topright.Cursor = Cursors.SizeNESW;
                    _rect_border_topright.Margin = new Thickness(_resizergrip_thickness / 2);
                    _base_grid.Children.Add(_rect_border_topright);

                    _rect_border_bottomleft = new Rectangle();
                    _rect_border_bottomleft.Fill = _resizer_border_fill;
                    _rect_border_bottomleft.HorizontalAlignment = HorizontalAlignment.Left;
                    _rect_border_bottomleft.VerticalAlignment = VerticalAlignment.Bottom;
                    _rect_border_bottomleft.Cursor = Cursors.SizeNESW;
                    _rect_border_bottomleft.Margin = new Thickness(_resizergrip_thickness / 2);
                    _base_grid.Children.Add(_rect_border_bottomleft);

                    _rect_border_bottomright = new Rectangle();
                    _rect_border_bottomright.Fill = _resizer_border_fill;
                    _rect_border_bottomright.HorizontalAlignment = HorizontalAlignment.Right;
                    _rect_border_bottomright.VerticalAlignment = VerticalAlignment.Bottom;
                    _rect_border_bottomright.Cursor = Cursors.SizeNWSE;
                    _rect_border_bottomright.Margin = new Thickness(_resizergrip_thickness / 2);
                    _base_grid.Children.Add(_rect_border_bottomright);

                    _image_border_topleft.Width = _image_border_topleft.Height = _resizergrip_thickness;
                    _image_border_topright.Width = _image_border_topright.Height = _resizergrip_thickness;
                    _image_border_bottomleft.Width = _image_border_bottomleft.Height = _resizergrip_thickness;
                    _image_border_bottomright.Width = _image_border_bottomright.Height = _resizergrip_thickness;

                    _rect_border_topleft.Width = _rect_border_topleft.Height = _resizergrip_thickness;
                    _rect_border_topright.Width = _rect_border_topright.Height = _resizergrip_thickness;
                    _rect_border_bottomleft.Width = _rect_border_bottomleft.Height = _resizergrip_thickness;
                    _rect_border_bottomright.Width = _rect_border_bottomright.Height = _resizergrip_thickness;

                    _window_resizer = new WindowResizer(_base_window);
                    _window_resizer.addResizerRight(_rect_border_right);
                    _window_resizer.addResizerLeft(_rect_border_left);
                    _window_resizer.addResizerUp(_rect_border_top);
                    _window_resizer.addResizerDown(_rect_border_bottom);
                    _window_resizer.addResizerLeftUp(_rect_border_topleft);
                    _window_resizer.addResizerRightUp(_rect_border_topright);
                    _window_resizer.addResizerLeftDown(_rect_border_bottomleft);
                    _window_resizer.addResizerRightDown(_rect_border_bottomright);
                    _window_resizer.WindowSizeUpdated += new EventHandler(_window_resizer_WindowSizeUpdated);
                    _window_resizer.AllowResize = AllowResize;

                    _window_resizer.WindowResizeStarted += new EventHandler(_window_resizer_WindowResizeStarted);
                    _window_resizer.WindowResizeComplete += new EventHandler(_window_resizer_WindowResizeComplete);
                    #endregion
                }

                _with_transparent_window_border = true;
                WithTransparentWindowBorder = false;

                _base_window.SizeChanged += new SizeChangedEventHandler(_base_window_SizeChanged);
                _base_window.StateChanged += new EventHandler(CustomWindow_StateChanged);
                _base_window.SourceInitialized += new EventHandler(CustomWindow_SourceInitialized);
                _base_window.Activated += new EventHandler(CustomWindow_Activated);
                _base_window.Deactivated += new EventHandler(CustomWindow_Deactivated);
                _base_window.Closing += new CancelEventHandler(_base_window_Closing);

                object saved_content = Content;
                Content = _base_grid;
                _main_content.Content = saved_content;

                #region Bounded Window Overay
                if (AppMode == AppModes.WpfApp && _base_window != null)
                {
                    DoAttachBoundedWindowApp();
                }
                #endregion
            }

            void _reserved_canvas_StylusMove(object sender, StylusEventArgs e)
            {
                e.Handled = true;
            }

            void _reserved_canvas_StylusUp(object sender, StylusEventArgs e)
            {
                e.Handled = true;
            }

            void _reserved_canvas_StylusDown(object sender, StylusDownEventArgs e)
            {
                e.Handled = true;
            }

            void _reserved_canvas_MouseWheel(object sender, MouseWheelEventArgs e)
            {
                e.Handled = true;
            }

            void _reserved_canvas_MouseMove(object sender, MouseEventArgs e)
            {
                e.Handled = true;
            }

            void _reserved_canvas_MouseUp(object sender, MouseButtonEventArgs e)
            {
                e.Handled = true;
            }

            void _reserved_canvas_MouseDown(object sender, MouseButtonEventArgs e)
            {
                e.Handled = true;
            }

            void _window_resizer_WindowResizeComplete(object sender, EventArgs e)
            {
                if (OverlayManager != null)
                    OverlayManager.StopCanvasMouseCapture();
            }

            void _window_resizer_WindowResizeStarted(object sender, EventArgs e)
            {
                if (OverlayManager != null)
                    OverlayManager.BeginCanvasMouseCapture();
            }

            private void InitWindowHandler()
            {
                if (_base_window != null && _base_window.IsLoaded == false)
                    _base_window.Loaded += new RoutedEventHandler(_base_window_Loaded);
            }

            private static void OnAllowResizeChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
            {
                if (d is BaseFrameBase<B, S, T, U, O, SU> && e.NewValue is bool)
                {
                    BaseFrameBase<B, S, T, U, O, SU> base_frame = (BaseFrameBase<B, S, T, U, O, SU>)d;
                    if (base_frame._window_resizer != null)
                        base_frame._window_resizer.AllowResize = (bool)e.NewValue;
                }

            }


            #region TitleBar
            private void InitTitleBar()
            {
                _titlebar = new U();
                _titlebar.BaseFrame = this;
                _titlebar.Height = 30;
                //_titlebar.Height = 0;
                _titlebar.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
                _titlebar.VerticalAlignment = System.Windows.VerticalAlignment.Top;

                if (AppMode == AppModes.WpfApp)
                {
                    _titlebar.MouseLeftButtonDown += new MouseButtonEventHandler(_titlebar_MouseLeftButtonDown);
                    _titlebar.FullScreenClick += new EventHandler(_titlebar_FullScreenClick);
                    _titlebar.MaximizeClick += new EventHandler(_titlebar_MaximizeClick);
                    _titlebar.CloseClick += new EventHandler(_titlebar_CloseClick);
                    _titlebar.MinimizeClick += new EventHandler(_titlebar_MinimizeClick);
                    _titlebar.MouseDoubleClick += new MouseButtonEventHandler(_titlebar_MouseDoubleClick);
                }

                _titlebar.SizeChanged += new SizeChangedEventHandler(_titlebar_SizeChanged);

                Binding b_border = new Binding("WindowBorderBrush");
                b_border.Source = this;
                _titlebar.SetBinding(BorderBrushProperty, b_border);

                Binding b_foreground = new Binding("DefaultForegroundBrush");
                b_foreground.Source = this;
                _titlebar.SetBinding(ForegroundProperty, b_foreground);

                if (AppMode == AppModes.Xbap)
                {
                    Binding b_bg = new Binding("TitleBarBrush");
                    b_bg.Source = this;
                    _titlebar.SetBinding(BackgroundProperty, b_bg);
                }
            }
            #endregion

            #region TaskBar
            private void InitTaskBar()
            {
                //Type generic_typ = typeof (T);
                //Type inner_typ = typeof (S);

                
                //System.Type specificType = generic_typ.MakeGenericType(new System.Type[] { inner_typ });
                //_taskbar = (T)Activator.CreateInstance(specificType);

                _taskbar = new T();

                _taskbar.BaseFrame = this;
                _taskbar.BarHeight = 70;
                //_taskbar.BarHeight = 0;
                _taskbar.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
                _taskbar.VerticalAlignment = System.Windows.VerticalAlignment.Bottom;

                _taskbar.SizeChanged += new SizeChangedEventHandler(_taskbar_SizeChanged);

                Binding b_bg = new Binding("TaskBarBrush");
                b_bg.Source = this;
                _taskbar.BaseBorder.SetBinding(BackgroundProperty, b_bg);

                Binding b_border = new Binding("WindowBorderBrush");
                b_border.Source = this;
                _taskbar.SetBinding(BorderBrushProperty, b_border);
            }
            #endregion

            #region Window Border + Border Resizing
            private void InitWindowBorder()
            {
                _window_border = new Border();
                _window_border.SetBinding(MarginProperty, new Binding("ResizerBorderMargin") { Source = this });
                //_window_border.Background = Brushes.White;
                _window_border.BorderThickness = new Thickness(1);
                _window_border.HorizontalAlignment = HorizontalAlignment.Stretch;
                _window_border.VerticalAlignment = VerticalAlignment.Stretch;
                _window_border.Child = _main_content_base;

                Binding b_border = new Binding("WindowBorderBrush");
                b_border.Source = this;
                _window_border.SetBinding(Border.BorderBrushProperty, b_border);
            }

            protected virtual void SetWindowBorderEffect()
            {
                _rect_border_left.Visibility = Visibility.Visible;
                _rect_border_right.Visibility = Visibility.Visible;
                _rect_border_top.Visibility = Visibility.Visible;
                _rect_border_bottom.Visibility = Visibility.Visible;

                _rect_border_topleft.Visibility = Visibility.Visible;
                _rect_border_topright.Visibility = Visibility.Visible;
                _rect_border_bottomleft.Visibility = Visibility.Visible;
                _rect_border_bottomright.Visibility = Visibility.Visible;

                ResizerBorderMargin = _saved_border_margin;
            }

            protected virtual void UnSetWindowBorderEffect()
            {
                _rect_border_left.Visibility = Visibility.Collapsed;
                _rect_border_right.Visibility = Visibility.Collapsed;
                _rect_border_top.Visibility = Visibility.Collapsed;
                _rect_border_bottom.Visibility = Visibility.Collapsed;

                _rect_border_topleft.Visibility = Visibility.Collapsed;
                _rect_border_topright.Visibility = Visibility.Collapsed;
                _rect_border_bottomleft.Visibility = Visibility.Collapsed;
                _rect_border_bottomright.Visibility = Visibility.Collapsed;

                _saved_border_margin = ResizerBorderMargin;
                //if (_maximized_window_mode == MaximizedWindowMode.TaskBarVisible)
                ResizerBorderMargin = new Thickness(0);
                //else
                //    ResizerBorderMargin = new Thickness(7);
            }

            private Brush GetImgBrush(string path, Rect tile_rect)
            {
                ImageBrush img_brush = new ImageBrush(GetImgSource(path));
                img_brush.TileMode = TileMode.Tile;
                img_brush.Viewport = tile_rect;
                img_brush.ViewportUnits = BrushMappingMode.Absolute;
                img_brush.Freeze();
                return img_brush;
            }

            private Brush GetImgBrush(string path)
            {
                ImageBrush img_brush = new ImageBrush(GetImgSource(path));
                img_brush.Freeze();
                return img_brush;
            }

            private ImageSource GetImgSource(string path)
            {
                try
                {
                    FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read);
                    byte[] b = new byte[fs.Length];
                    fs.Read(b, 0, b.Length);
                    fs.Close();
                    fs.Dispose();
                    MemoryStream ms = new MemoryStream(b);

                    PngBitmapDecoder decoder = new PngBitmapDecoder(ms,
                                                                    BitmapCreateOptions.PreservePixelFormat,
                                                                    BitmapCacheOption.OnLoad);
                    ImageSource bitmap = decoder.Frames[0];
                    bitmap.Freeze();
                    ms.Dispose();
                    return bitmap;
                }
                catch
                {
                    return null;
                }
            }

            void _titlebar_MinimizeClick(object sender, EventArgs e)
            {
                BaseWindow.WindowState = WindowState.Minimized;
            }

            void _titlebar_CloseClick(object sender, EventArgs e)
            {
                if (CanClose())
                    BaseWindow.Close();
            }

            public void SetMaximizedCheck(bool is_checked)
            {
                _titlebar.SetMaximizedCheck(is_checked);
            }

            public void SeFullscreenCheck(bool is_checked)
            {
                _titlebar.SeFullscreenCheck(is_checked);
            }

            void _titlebar_FullScreenClick(object sender, EventArgs e)
            {
                ToggleButton b = sender as ToggleButton;
                if (b == null)
                    return;

                if (b.IsChecked == false && BaseWindow.WindowState == WindowState.Normal)
                    return;

                if (b.IsChecked == true && BaseWindow.WindowState == WindowState.Maximized)
                    return;

                _maximized_window_mode = MaximizedWindowMode.FullScreen;
                RemoveWindowHook();

                if (BaseWindow.WindowState == WindowState.Normal)
                {
                    UnSetWindowBorderEffect();
                    BaseWindow.WindowState = WindowState.Maximized;
                    if (BaseWindow is MultiFrameWindow)
                        ((MultiFrameWindow)BaseWindow).SyncMaximizedButtonState(this, true);
                }
                else
                {
                    SetWindowBorderEffect();
                    BaseWindow.WindowState = WindowState.Normal;
                    if (BaseWindow is MultiFrameWindow)
                        ((MultiFrameWindow)BaseWindow).SyncMaximizedButtonState(this, false);
                }
            }

            void _titlebar_MaximizeClick(object sender, EventArgs e)
            {
                ToggleButton b = sender as ToggleButton;
                if (b == null)
                    return;

                if (b.IsChecked == false && BaseWindow.WindowState == WindowState.Normal)
                    return;

                if (b.IsChecked == true && BaseWindow.WindowState == WindowState.Maximized)
                    return;

                _maximized_window_mode = MaximizedWindowMode.TaskBarVisible;
                AddWindowHook();

                if (BaseWindow.WindowState == WindowState.Normal)
                {
                    UnSetWindowBorderEffect();
                    BaseWindow.WindowState = WindowState.Maximized;
                    if (BaseWindow is MultiFrameWindow)
                        ((MultiFrameWindow)BaseWindow).SyncMaximizedButtonState(this, true);
                }
                else
                {
                    SetWindowBorderEffect();
                    BaseWindow.WindowState = WindowState.Normal;
                    if (BaseWindow is MultiFrameWindow)
                        ((MultiFrameWindow)BaseWindow).SyncMaximizedButtonState(this, false);
                }
            }

            void _titlebar_MouseDoubleClick(object sender, MouseButtonEventArgs e)
            {
                _titlebar.MaximizeButton.IsChecked = !_titlebar.MaximizeButton.IsChecked;
            }
            #endregion

            #region Bonded Window Overlay
            public void AttachBoundedWindowApp(string path_to_bounded_window_app, string pipe_comm_name)
            {
                _pipe_comm_name = pipe_comm_name;
                _path_to_bounded_window_app = path_to_bounded_window_app;
                if (!IsInitialized)
                    return;
                DoAttachBoundedWindowApp();
            }

            private void DoAttachBoundedWindowApp()
            {
                if (string.IsNullOrEmpty(_path_to_bounded_window_app))
                    return;

                _bound_window_process = new Process();
                _bound_window_process.StartInfo.UseShellExecute = false;
                _bound_window_process.StartInfo.FileName = _path_to_bounded_window_app;
                _bound_window_process.StartInfo.CreateNoWindow = false;
                _bound_window_process.Exited += new EventHandler(_bound_window_process_Exited);
                _bound_window_process.Disposed += new EventHandler(_bound_window_process_Disposed);
                _bound_window_process.Start();

                _window_comm_sender = new WindowCommSender(_pipe_comm_name);

                Application.Current.Exit += new ExitEventHandler(Current_Exit);

            }


            void _bound_window_process_Disposed(object sender, EventArgs e)
            {
            }

            void _bound_window_process_Exited(object sender, EventArgs e)
            {
            }
            #endregion

            public void DisableClicksMainArea()
            {
                _main_content_base.IsHitTestVisible = false;
            }
            public void EnableClicksMainArea()
            {
                _main_content_base.IsHitTestVisible = true;
            }

            public void WriteDebugInfo(string msg)
            {
#if DEBUG
                if (_debug_writer == null)
                    return;

                _debug_writer.WriteInfo(msg);
#endif
            }

            #region Maximize/Fullscreen with WindowStyle=None

        protected abstract void RemoveWindowHook();
        protected abstract void AddWindowHook();

        
            #endregion

            #region Event Handler
            void _base_window_Loaded(object sender, RoutedEventArgs e)
            {
                _base_window.Loaded -= _base_window_Loaded;
                _titlebar.CheckInitialWindowState();
            }

            void _titlebar_SizeChanged(object sender, SizeChangedEventArgs e)
            {
                SetMainContentBaseHeight();
            }

            void _taskbar_SizeChanged(object sender, SizeChangedEventArgs e)
            {
                SetMainContentBaseHeight();
            }

            void CustomWindow_Activated(object sender, EventArgs e)
            {
                Binding b_bg = new Binding("TitleBarBrush");
                b_bg.Source = this;
                _titlebar.SetBinding(BackgroundProperty, b_bg);
            }


            void CustomWindow_Deactivated(object sender, EventArgs e)
            {
                Binding b_bg = new Binding("TitleBarInActiveBrush");
                b_bg.Source = this;
                _titlebar.SetBinding(BackgroundProperty, b_bg);
            }

            void CustomWindow_SourceInitialized(object sender, EventArgs e)
            {
                if (AppMode == AppModes.WpfApp)
                {
                    System.IntPtr handle = (new WindowInteropHelper(_base_window)).Handle;
                    //HwndSource.FromHwnd(handle).AddHook(new HwndSourceHook(WindowProc));
                }
            }


            void _base_window_Closing(object sender, CancelEventArgs e)
            {
                DisposeBoundWindow();
            }

            void Current_Exit(object sender, ExitEventArgs e)
            {
                DisposeBoundWindow();
            }

            void DisposeBoundWindow()
            {
                if (_bound_window_process != null)
                {
                    _bound_window_process.CloseMainWindow();
                    _bound_window_process.Dispose();
                    _bound_window_process = null;
                }
            }

            void CustomWindow_StateChanged(object sender, EventArgs e)
            {
                if (BaseWindow.WindowState == WindowState.Normal)
                {
                    SetWindowBorderEffect();
                }
            }

            void _base_window_SizeChanged(object sender, SizeChangedEventArgs e)
            {
                if (_window_comm_sender != null)
                {
                    double left = 0;
                    double top = 0;
                    if (BaseWindow.WindowState == WindowState.Normal)
                    {
                        left = BaseWindow.Left;
                        top = BaseWindow.Top;
                    }
                    _window_comm_sender.Proxy.SetWindowDimension(left, top, BaseWindow.ActualWidth, BaseWindow.ActualHeight);
                }
            }

            void CustomWindow_Loaded(object sender, RoutedEventArgs e)
            {
                if (_base_window == null)
                {
                    AppMode = AppModes.WpfApp;
                    if (_base_window == null)
                        _base_window = Application.Current.MainWindow;

                    if (_base_window.ToString().ToLower().IndexOf("browser") >= 0)
                        AppMode = AppModes.Xbap;

                    InternalInit();
                    Init();
                }

                //_base_window = UIHelper.TryFindParent<Window>(this);
                _base_page = UIHelper.TryFindParent<Page>(this);
            }

            void BaseGrid_SizeChanged(object sender, SizeChangedEventArgs e)
            {
                if (_window_border.ActualWidth == 0)
                    return;

                if (e != null && AppMode == AppModes.WpfApp)
                {
                    _rect_border_left.Height = e.NewSize.Height - _resizergrip_thickness * 2;
                    _rect_border_right.Height = e.NewSize.Height - _resizergrip_thickness * 2;
                    _rect_border_top.Width = e.NewSize.Width - _resizergrip_thickness * 2;
                    _rect_border_bottom.Width = e.NewSize.Width - _resizergrip_thickness * 2;
                }

                _titlebar.MaxWidth = _titlebar.MinWidth = _titlebar.Width = _window_border.ActualWidth - (_window_border.BorderThickness.Left + _window_border.BorderThickness.Right);
                _taskbar.MaxWidth = _taskbar.MinWidth = _taskbar.Width = _window_border.ActualWidth - (_window_border.BorderThickness.Left + _window_border.BorderThickness.Right);

                _overlay_manager.ParentHeight = _window_border.ActualHeight - (_window_border.BorderThickness.Top + _window_border.BorderThickness.Bottom);
                _overlay_manager.ParentWidth = _window_border.ActualWidth - (_window_border.BorderThickness.Left + _window_border.BorderThickness.Right);
                _overlay_manager.Height = _window_border.ActualHeight - (_window_border.BorderThickness.Top + _window_border.BorderThickness.Bottom);
                _overlay_manager.Width = _window_border.ActualWidth - (_window_border.BorderThickness.Left + _window_border.BorderThickness.Right);

                _overlay_grid.Height = _window_border.ActualHeight - (_window_border.BorderThickness.Top + _window_border.BorderThickness.Bottom);
                _overlay_grid.Width = _window_border.ActualWidth - (_window_border.BorderThickness.Left + _window_border.BorderThickness.Right);

                _tooltip_panel.Height = _window_border.ActualHeight - (_window_border.BorderThickness.Top + _window_border.BorderThickness.Bottom);
                _tooltip_panel.Width = _window_border.ActualWidth - (_window_border.BorderThickness.Left + _window_border.BorderThickness.Right);

                _reserved_canvas.Height = _window_border.ActualHeight - (_window_border.BorderThickness.Top + _window_border.BorderThickness.Bottom);
                _reserved_canvas.Width = _window_border.ActualWidth - (_window_border.BorderThickness.Left + _window_border.BorderThickness.Right);

                SetMainContentBaseHeight();
            }


            private void SetMainContentBaseHeight()
            {
                double new_height = _window_border.ActualHeight - (_taskbar.ActualHeight + _titlebar.ActualHeight);
                if (new_height < MinHeightMainContent)
                    new_height = MinHeightMainContent;
                _main_content_base.Height = new_height;
                _main_content_base.Margin = new Thickness(0, _titlebar.ActualHeight, 0, _taskbar.ActualHeight);
            }


            #region Make Window Moveable

            void _window_resizer_WindowSizeUpdated(object sender, EventArgs e)
            {
                if (_window_comm_sender != null)
                    _window_comm_sender.Proxy.SetWindowDimension(BaseWindow.Left, BaseWindow.Top, BaseWindow.Width, BaseWindow.Height);
            }


            void _titlebar_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
            {
                if (e.ClickCount > 1)
                    return;

                User32GetCursorPos(out _window_move_last_point);

                if (BaseWindow.WindowState == WindowState.Maximized)
                {
                    double old_width = ActualWidth;
                    Point cur_pos = e.GetPosition(this);
                    _titlebar.MaximizeButton.IsChecked = false;
                    double new_width = ActualWidth;

                    BaseWindow.Left = _window_move_last_point.x - cur_pos.X * new_width / old_width;
                    BaseWindow.Top = _window_move_last_point.y - cur_pos.Y * new_width / old_width;

                    if (_window_comm_sender != null)
                        _window_comm_sender.Proxy.SetWindowDimension(BaseWindow.Left, BaseWindow.Top, BaseWindow.ActualWidth, BaseWindow.ActualHeight);
                }


                QueuedBackgroundWorker bg_worker = new QueuedBackgroundWorker("window move");
                bg_worker.DoWork += bg_worker_DoWork;
                bg_worker.RunWorkerCompleted += bg_worker_RunWorkerCompleted;
                bg_worker.RunWorkerAsync();
            }

            void bg_worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
            {
                QueuedBackgroundWorker bg_worker = (QueuedBackgroundWorker)sender;
                bg_worker.DoWork -= bg_worker_DoWork;
                bg_worker.RunWorkerCompleted -= bg_worker_RunWorkerCompleted;
            }

            void bg_worker_DoWork(object sender, DoWorkEventArgs e)
            {
                while (true)
                {
                    Thread.Sleep(20);
                    Dispatcher.Invoke(DispatcherPriority.Normal, (NoParas)delegate
                    {
                        POINT point_curr_cursor_pos = new POINT();
                        User32GetCursorPos(out point_curr_cursor_pos);

                        double rel_move_x = point_curr_cursor_pos.x - _window_move_last_point.x;
                        double rel_move_y = point_curr_cursor_pos.y - _window_move_last_point.y;

                        _window_move_last_point = point_curr_cursor_pos;

                        BaseWindow.Left += rel_move_x;
                        BaseWindow.Top += rel_move_y;

                        if (_window_comm_sender != null)
                            _window_comm_sender.Proxy.SetWindowDimension(BaseWindow.Left, BaseWindow.Top, BaseWindow.ActualWidth, BaseWindow.ActualHeight);

                    });

                    if (User32GetKeyState(0x01) >= 0)
                    {
                        return;
                    }
                }
            }
            #endregion
            #region External Call 
            protected abstract short User32GetKeyState(int keyCode);
            protected abstract bool User32GetCursorPos(out POINT lpPoint);
            #endregion
            #endregion

            #region Dependency Properties

            public Thickness ResizerBorderMargin
            {
                get { return (Thickness)GetValue(ResizerBorderMarginProperty); }
                set { SetValue(ResizerBorderMarginProperty, value); }
            }
            public static readonly DependencyProperty ResizerBorderMarginProperty = DependencyProperty.Register("ResizerBorderMargin", typeof(Thickness), typeof(BaseFrameBase<B, S, T, U, O, SU>), new UIPropertyMetadata(new Thickness(0)));

            public Brush TitleBarBrush
            {
                get { return (Brush)GetValue(TitleBarBrushProperty); }
                set { SetValue(TitleBarBrushProperty, value); }
            }
            public static readonly DependencyProperty TitleBarBrushProperty = DependencyProperty.Register("TitleBarBrush", typeof(Brush), typeof(BaseFrameBase<B, S, T, U, O, SU>), new UIPropertyMetadata(Brushes.LightGray));

            public Brush TitleBarInActiveBrush
            {
                get { return (Brush)GetValue(TitleBarInActiveBrushProperty); }
                set { SetValue(TitleBarInActiveBrushProperty, value); }
            }
            public static readonly DependencyProperty TitleBarInActiveBrushProperty = DependencyProperty.Register("TitleBarInActiveBrush", typeof(Brush), typeof(BaseFrameBase<B, S, T, U, O, SU>), new UIPropertyMetadata(Brushes.LightGray));

            public Brush TaskBarBrush
            {
                get { return (Brush)GetValue(TaskBarBrushProperty); }
                set { SetValue(TaskBarBrushProperty, value); }
            }
            public static readonly DependencyProperty TaskBarBrushProperty = DependencyProperty.Register("TaskBarBrush", typeof(Brush), typeof(BaseFrameBase<B, S, T, U, O, SU>), new UIPropertyMetadata(Brushes.LightGray));

            public Brush MainAreaBrush
            {
                get { return (Brush)GetValue(MainAreaBrushProperty); }
                set { SetValue(MainAreaBrushProperty, value); }
            }
            public static readonly DependencyProperty MainAreaBrushProperty = DependencyProperty.Register("MainAreaBrush", typeof(Brush), typeof(BaseFrameBase<B, S, T, U, O, SU>), new UIPropertyMetadata(Brushes.LightGray));


            public Brush WindowBorderBrush
            {
                get { return (Brush)GetValue(WindowBorderBrushProperty); }
                set { SetValue(WindowBorderBrushProperty, value); }
            }
            public static readonly DependencyProperty WindowBorderBrushProperty = DependencyProperty.Register("WindowBorderBrush", typeof(Brush), typeof(BaseFrameBase<B, S, T, U, O, SU>), new UIPropertyMetadata(Brushes.LightGray));

            public Brush DefaultForegroundBrush
            {
                get { return (Brush)GetValue(DefaultForegroundBrushProperty); }
                set { SetValue(DefaultForegroundBrushProperty, value); }
            }
            public static readonly DependencyProperty DefaultForegroundBrushProperty = DependencyProperty.Register("DefaultForegroundBrush", typeof(Brush), typeof(BaseFrameBase<B, S, T, U, O, SU>), new UIPropertyMetadata(Brushes.LightGray));


            public Brush HooverBrush
            {
                get { return (Brush)GetValue(HooverBrushProperty); }
                set { SetValue(HooverBrushProperty, value); }
            }
            public static readonly DependencyProperty HooverBrushProperty = DependencyProperty.Register("HooverBrush", typeof(Brush), typeof(BaseFrameBase<B, S, T, U, O, SU>), new UIPropertyMetadata(Brushes.LightGray));


            public Brush HooverStrokeBrush
            {
                get { return (Brush)GetValue(HooverStrokeBrushProperty); }
                set { SetValue(HooverStrokeBrushProperty, value); }
            }
            public static readonly DependencyProperty HooverStrokeBrushProperty = DependencyProperty.Register("HooverStrokeBrush", typeof(Brush), typeof(BaseFrameBase<B, S, T, U, O, SU>), new UIPropertyMetadata(Brushes.LightGray));


            public double HooverStrokeThickness
            {
                get { return (double)GetValue(HooverStrokeThicknessProperty); }
                set { SetValue(HooverStrokeThicknessProperty, value); }
            }
            public static readonly DependencyProperty HooverStrokeThicknessProperty = DependencyProperty.Register("HooverStrokeThickness", typeof(double), typeof(BaseFrameBase<B, S, T, U, O, SU>), new UIPropertyMetadata((double)1));

            public Brush SubMenuPanelBackground
            {
                get { return (Brush)GetValue(SubMenuPanelBackgroundProperty); }
                set { SetValue(SubMenuPanelBackgroundProperty, value); }
            }
            public static readonly DependencyProperty SubMenuPanelBackgroundProperty = DependencyProperty.Register("SubMenuPanelBackground", typeof(Brush), typeof(BaseFrameBase<B, S, T, U, O, SU>), new UIPropertyMetadata(Brushes.White));

            public bool AllowResize
            {
                get { return (bool)GetValue(AllowResizeProperty); }
                set { SetValue(AllowResizeProperty, value); }
            }

            // Using a DependencyProperty as the backing store for AllowResize.  This enables animation, styling, binding, etc...
            public static readonly DependencyProperty AllowResizeProperty =
                DependencyProperty.Register("AllowResize", typeof(bool), typeof(BaseFrameBase<B, S, T, U, O, SU>), new UIPropertyMetadata(true, new PropertyChangedCallback(BaseFrameBase<B, S, T, U, O, SU>.OnAllowResizeChanged)));

         #endregion

            #region Disable Mouse Clicks
            public void EnableMouseClicks()
            {
                _reserved_canvas.Background = null;
            }

            public void DisableMouseClicks()
            {
                _reserved_canvas.Background = Brushes.Transparent;

                // alle hover states in der taskbar reseten
                if (_taskbar != null)
                {
                    List<S> panels = _taskbar.GetSubMenuPanels();
                    foreach (S panel in panels)
                        panel.ResetHoverElementState();
                    panels.Clear();
                }
            }

            public bool AreMouseClicksDisabled
            {
                get
                {
                    return _reserved_canvas.Background != null;
                }
            }
            #endregion

            #region App Cursor
            public void SetFixedCursor(Cursor cursor)
            {
                _fixed_cursor = cursor;
                Cursor = cursor;
            }

            public void SetAppCursor(Cursor cursor)
            {
                if (_fixed_cursor != null)
                    Cursor = _fixed_cursor;
                else
                {
                    if (cursor == Cursors.Wait)
                    {
                        ShowAnimatedCursor();
                    }
                    Cursor = cursor;
                }
            }

            public void SetDefaultAppCursor()
            {
                HideAnimatedCursor();
                SetAppCursor(DefaultCursor);
                _fixed_cursor = null;
            }

            private void ShowAnimatedCursor()
            {
                _animated_cursor = GetAnimatedCursor();
                if (_animated_cursor == null)
                    return;

                if (!_reserved_canvas.Children.Contains(_animated_cursor))
                    _reserved_canvas.Children.Add(_animated_cursor);

                Point curr_pos = Mouse.GetPosition(this);

                Canvas.SetLeft(_animated_cursor, curr_pos.X - 30);
                Canvas.SetTop(_animated_cursor, curr_pos.Y - 30);
            }

            protected virtual UIElement GetAnimatedCursor()
            {
                return null;
            }

            private void HideAnimatedCursor()
            {
                if (_animated_cursor == null)
                    return;

                _reserved_canvas.Children.Remove(_animated_cursor);
                _animated_cursor = null;
            }


            protected override void OnMouseMove(MouseEventArgs e)
            {
                if (_animated_cursor != null)
                {
                    Point curr_pos = e.GetPosition(this);

                    Canvas.SetLeft(_animated_cursor, curr_pos.X - 30);
                    Canvas.SetTop(_animated_cursor, curr_pos.Y - 30);
                }
            }
            #endregion

            public override void OnApplyTemplate()
            {
                // wenn als standard WindowsState in der App WindowsState.Maximized angegeben wird
                // und der InitialWindowMode auf TaskbarVisible steht, dann sofort nach dem erstellen
                // des Fensters den WindowHook aktivieren damit die Taskbar sichtbar bleibt
                if (InitialWindowMode == InitialWindowModeEnum.TaskBarVisible)
                    AddWindowHook();
            }

            #region Properties

        public Border WindowBorder
        {
            get { return _window_border; }
        }

        public double ActualTitleBarHeight
        {
            get { return _titlebar.ActualHeight; }
        }

        public double ActualTaskBarBaseBorderHeight
        {
            get { return _taskbar.BaseBorder.ActualHeight; }
        }

        public virtual InitialWindowModeEnum InitialWindowMode
            {
                get
                {
                    return InitialWindowModeEnum.Normal;
                }
            }

            public double ResizerGripThickness
            {
                get
                {
                    return _resizergrip_thickness;
                }
                set
                {
                    try
                    {
                        _resizergrip_thickness = value;

                        if (_with_transparent_window_border)
                        {
                            _rect_border_topleft.Width = _rect_border_topleft.Height = _resizergrip_thickness;
                            _rect_border_topright.Width = _rect_border_topright.Height = _resizergrip_thickness;
                            _rect_border_bottomleft.Width = _rect_border_bottomleft.Height = _resizergrip_thickness;
                            _rect_border_bottomright.Width = _rect_border_bottomright.Height = _resizergrip_thickness;

                            _rect_border_topleft.Margin = new Thickness(_resizergrip_thickness / 2);
                            _rect_border_topright.Margin = new Thickness(_resizergrip_thickness / 2);
                            _rect_border_bottomleft.Margin = new Thickness(_resizergrip_thickness / 2);
                            _rect_border_bottomright.Margin = new Thickness(_resizergrip_thickness / 2);
                        }
                        else
                        {
                            _rect_border_topleft.Width = _rect_border_topleft.Height = _resizergrip_thickness * 2;
                            _rect_border_topright.Width = _rect_border_topright.Height = _resizergrip_thickness * 2;
                            _rect_border_bottomleft.Width = _rect_border_bottomleft.Height = _resizergrip_thickness * 2;
                            _rect_border_bottomright.Width = _rect_border_bottomright.Height = _resizergrip_thickness * 2;

                            _rect_border_topleft.Margin = new Thickness(0);
                            _rect_border_topright.Margin = new Thickness(0);
                            _rect_border_bottomleft.Margin = new Thickness(0);
                            _rect_border_bottomright.Margin = new Thickness(0);
                        }

                        _rect_border_left.Width = _resizergrip_thickness;
                        _rect_border_right.Width = _resizergrip_thickness;
                        _rect_border_top.Height = _resizergrip_thickness;
                        _rect_border_bottom.Height = _resizergrip_thickness;

                        BaseGrid_SizeChanged(null, null);

                    }
                    catch
                    {
                    }
                }
            }

            /// <summary>
            /// Wenn true dann werden Images für den Border geladen - ausserdem wird allowstransparency auf true gesetzt
            /// das ganze schaut kewl aus kostet es aber massig wpf render performance, für anspruchsvolle main contents
            /// sollte dieser wert auf false gesetzt werden
            /// </summary>
            public bool WithTransparentWindowBorder
            {
                set
                {
                    if (AppMode == AppModes.Xbap)
                        return;

                    if (_with_transparent_window_border == value)
                        return;
                    _with_transparent_window_border = value;

                    if (value)
                    {
                        BaseWindow.WindowStyle = WindowStyle.None;
                        BaseWindow.ResizeMode = ResizeMode.NoResize;
                        try
                        {
                            BaseWindow.AllowsTransparency = true;
                        }
                        catch { }
                        Background = Brushes.Transparent;

                        ResizerGripThickness = 10;
                        ResizerBorderMargin = new Thickness(9);

                        _rect_border_right.Fill = GetImgBrush(PathBorderImages + "border_right.png", new Rect(0, 0, 10, 1));
                        _rect_border_top.Fill = GetImgBrush(PathBorderImages + "border_top.png", new Rect(0, 0, 1, 10));
                        _rect_border_bottom.Fill = GetImgBrush(PathBorderImages + "border_Bottom.png", new Rect(0, 0, 1, 10));
                        _rect_border_left.Fill = GetImgBrush(PathBorderImages + "border_left.png", new Rect(0, 0, 10, 1));

                        _image_border_topleft.Fill = GetImgBrush(PathBorderImages + "border_topleft.png");
                        _image_border_topright.Fill = GetImgBrush(PathBorderImages + "border_topright.png");
                        _image_border_bottomleft.Fill = GetImgBrush(PathBorderImages + "border_bottomleft.png");
                        _image_border_bottomright.Fill = GetImgBrush(PathBorderImages + "border_bottomright.png");

                        _image_border_topleft.Visibility = Visibility.Visible;
                        _image_border_topright.Visibility = Visibility.Visible;
                        _image_border_bottomleft.Visibility = Visibility.Visible;
                        _image_border_bottomright.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        BaseWindow.WindowStyle = WindowStyle.None;
                        BaseWindow.ResizeMode = ResizeMode.NoResize;
                        try
                        {
                            BaseWindow.AllowsTransparency = false;
                        }
                        catch { }
                        Background = Brushes.Transparent;

                        ResizerGripThickness = 5;
                        ResizerBorderMargin = new Thickness(0);

                        _rect_border_topleft.Fill = Brushes.Transparent;
                        _rect_border_topright.Fill = Brushes.Transparent;
                        _rect_border_bottomleft.Fill = Brushes.Transparent;
                        _rect_border_bottomright.Fill = Brushes.Transparent;

                        _rect_border_left.Fill = Brushes.Transparent;
                        _rect_border_right.Fill = Brushes.Transparent;
                        _rect_border_top.Fill = Brushes.Transparent;
                        _rect_border_bottom.Fill = Brushes.Transparent;

                        _image_border_topleft.Visibility = Visibility.Collapsed;
                        _image_border_topright.Visibility = Visibility.Collapsed;
                        _image_border_bottomleft.Visibility = Visibility.Collapsed;
                        _image_border_bottomright.Visibility = Visibility.Collapsed;
                    }
                }
            }

            protected virtual string PathBorderImages
            {
                get
                {
                    return "Resources\\Graphics\\WindowManagement\\";
                }
            }

            public virtual int MinHeightMainContent
            {
                get { return 50; }
            }

        public double ActualTaskBarHeight
        {
            get { return _taskbar.ActualHeight; }
        }


        public Brush AdornerActiveRenderBrush
            {
                get
                {
                    if (_adorner_active_render_brush == null)
                    {
                        _adorner_active_render_brush = HooverBrush;
                        _adorner_active_render_brush.Freeze();
                    }
                    return _adorner_active_render_brush;
                }
            }

            public Pen AdornerActiveRenderPen
            {
                get
                {
                    if (_adorner_active_render_pen == null)
                    {
                        if (HooverStrokeThickness == 0)
                            return null;
                        _adorner_active_render_pen = new Pen(HooverStrokeBrush, HooverStrokeThickness);
                        _adorner_active_render_pen.Freeze();
                    }
                    return _adorner_active_render_pen;
                }
            }

            public O OverlayManager
            {
                get
                {
                    return _overlay_manager;
                }
            }

            public Grid MainContentBaseGrid
            {
                get
                {
                    return _main_content_base;
                }
            }


            public U TitleBar
            {
                get
                {
                    return _titlebar;
                }
            }
            public T TaskBar
            {
                get
                {
                    return _taskbar;
                }
            }
            public TooltipPanel TooltipPanel
            {
                get
                {
                    return _tooltip_panel;
                }
            }
            public WindowResizer WindowResizer
            {
                get
                {
                    return _window_resizer;
                }
            }
            public Window BaseWindow
            {
                get
                {
                    return _base_window;
                }
            }
            public Page BasePage
            {
                get
                {
                    return _base_page;
                }
            }
            #endregion

            #region Attributes
            protected delegate void NoParas();

            protected MaximizedWindowMode _maximized_window_mode = MaximizedWindowMode.TaskBarVisible;

            protected T _taskbar;
            protected U _titlebar;

            /// <summary>
            /// dies ist das haupt grid
            /// hier liegen die eigenltichen applikations inhalte drinnen
            /// </summary>
            protected ContentControl _main_content;
            /// <summary>
            /// hiermit kann der style des windows definiert werden
            /// </summary>
            protected Border _window_border;

            private POINT _window_move_last_point;
            private double _resizergrip_thickness = 10;
            private Brush _resizer_border_fill = Brushes.Transparent;

            private Rectangle _rect_border_left;
            private Rectangle _rect_border_right;
            private Rectangle _rect_border_top;
            private Rectangle _rect_border_bottom;

            private Rectangle _rect_border_topleft;
            private Rectangle _rect_border_topright;
            private Rectangle _rect_border_bottomleft;
            private Rectangle _rect_border_bottomright;

            private Rectangle _image_border_topleft;
            private Rectangle _image_border_topright;
            private Rectangle _image_border_bottomleft;
            private Rectangle _image_border_bottomright;

            protected WindowResizer _window_resizer;
            protected Grid _main_content_base;
            protected Grid _overlay_grid;
            protected Grid _base_grid;

            protected Canvas _touch_canvas_interceptor;

            protected O _overlay_manager;
            protected TooltipPanel _tooltip_panel;

            private bool _with_transparent_window_border;

            protected Cursor _fixed_cursor;
            public Cursor DefaultCursor;
            private UIElement _animated_cursor;

            protected Canvas _reserved_canvas;

            protected Brush _adorner_active_render_brush;
            protected Pen _adorner_active_render_pen;

            protected Thickness _saved_border_margin;

            //public const int MINHEIGHT_MAINCONTENT = 50;

            public string[] StartupArgs;

            public AppModes AppMode;
            protected Window _base_window;
            protected Page _base_page;
            protected TextBlockDebugWriter _debug_writer;

            protected bool _added_window_hook = false;
            protected object _change_hook_monitor = new object();

            #region Bounded Window Overlay
            protected string _path_to_bounded_window_app;
            protected Process _bound_window_process;
            protected WindowCommSender _window_comm_sender;
            protected string _pipe_comm_name;
            #endregion

            public event EventHandler ResizerBorderMarginChanged;
            private ReportInitStatusDelegate _report_init_status = null;
            #endregion

        }

}
