﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace BankLib.Data
{
    public class BuchungsHelper
    {
        #region Buchungs Status Email Body
        public static string GetBuchungsStatusEmailBody(BuchungsStatusInfo last_status, BuchungsStatusInfo curr_status)
        {
            string msg = "";

            msg += "        <div>" +
                   "            <table>" +
                   "                <tr>" +
                   "                    <th style=\"width:150px;text-align:left\">Konto Gesamt<br><span style=\"font-weight: lighter;font-size: xx-small\">per " + DateTime.Now + "</span></th>" +
                   "                    <th style=\"width:100px;font-weight: lighter;\">+/- last update</th>" +
                   "                    <th style=\"width:100px;\">Aktuell</th>" +
                   "                </tr>";

            float summe_aktuell = 0;
            float summe_last_update = 0;
            foreach (Konto konto in Konten.AlleKonten)
            {
                float aktuell = Konten.GetGiroKontoStand(konto.Owner);
                float stand_last_update = -(last_status.LastKtoStand[konto] - aktuell);

                msg += "                <tr>" +
                       "                    <td>"+konto.Owner+"</td>" +
                       "                    <td style=\"" + GetRedColor(stand_last_update) + "text-align:right;\">" + stand_last_update.ToString("C2", CultureInfo.CreateSpecificCulture("de-DE")).Replace(" ", "&nbsp;") + "</td>" +
                       "                    <td style=\"" + GetRedColor(aktuell) + "text-align:right;font-weight:bold\">" + aktuell.ToString("C2", CultureInfo.CreateSpecificCulture("de-DE")).Replace(" ", "&nbsp;") + GetKtoStandFehlerMsg(konto) +"</td>" +
                       "                </tr>";

                summe_aktuell += aktuell;
                summe_last_update += stand_last_update;
            }

            msg += "                <tr>" +
                   "                    <td style=\"border-top:2px black solid;border-bottom:2px black double\">Summe</td>" +
                   "                    <td style=\"" + GetRedColor(summe_last_update) + "text-align:right;border-top:2px black solid;border-bottom:2px black double\">" + summe_last_update.ToString("C2", CultureInfo.CreateSpecificCulture("de-DE")).Replace(" ", "&nbsp;") + "</td>" +
                   "                    <td style=\"" + GetRedColor(summe_aktuell) + "text-align:right;border-top:2px black solid;border-bottom:2px black double;font-weight:bold\">" + summe_aktuell.ToString("C2", CultureInfo.CreateSpecificCulture("de-DE")).Replace(" ", "&nbsp;") + "</td>" +
                   "                </tr>";

            msg += "            </table>" +
                   "        </div>      " +
                   "            <br/>";







            msg += "        <div>" +
                   "            <table>" +
                   "                <tr>" +
                   "                    <th style=\"width:150px;text-align:left\">Konto " + DateTime.Now.ToString("MMM yyyy") + "</th>" +
                   "                    <th style=\"width:100px;font-weight: lighter;\">reine&nbsp;Ausgabe</th>" +
                   "                    <th style=\"width:100px;\">Gesamt</th>" +
                   "                </tr>";

            float summe_monat = 0;
            float summe_reine_ausgaben = 0;
            foreach (Konto konto in Konten.AlleKonten)
            {
                float aktuell = Konten.GetGiroKontoStand(konto.Owner);
                float aktuell_monat = Konten.GetGiroKontoStandMonat(konto.Owner, DateTime.Now.Month, DateTime.Now.Year);
                float stand_last_update = last_status.LastKtoStand[konto] - aktuell;
                float reine_ausgaben = Buchungen.GetAusgaben(konto, DateTime.Now.Month, DateTime.Now.Year);

                msg += "                <tr>" +
                       "                    <td>" + konto.Owner + "</td>" +
                       "                    <td style=\"" + GetRedColor(reine_ausgaben) + "text-align:right\">" + reine_ausgaben.ToString("C2", CultureInfo.CreateSpecificCulture("de-DE")).Replace(" ", "&nbsp;") + "</td>" +
                       "                    <td style=\"" + GetRedColor(aktuell_monat) + "text-align:right;font-weight:bold\">" + aktuell_monat.ToString("C2", CultureInfo.CreateSpecificCulture("de-DE")).Replace(" ", "&nbsp;") + "</td>" +
                       "                </tr>";

                summe_aktuell += aktuell;
                summe_last_update += stand_last_update;
                summe_monat += aktuell_monat;
                summe_reine_ausgaben += reine_ausgaben;
            }

            msg += "                <tr>" +
                   "                    <td style=\"border-top:2px black solid;border-bottom:2px black double\">Summe</td>" +
                   "                    <td style=\"" + GetRedColor(summe_reine_ausgaben) + "text-align:right;border-top:2px black solid;border-bottom:2px black double\">" + summe_reine_ausgaben.ToString("C2", CultureInfo.CreateSpecificCulture("de-DE")).Replace(" ", "&nbsp;") + "</td>" +
                   "                    <td style=\"" + GetRedColor(summe_monat) + "text-align:right;border-top:2px black solid;border-bottom:2px black double;font-weight:bold\">" + summe_monat.ToString("C2", CultureInfo.CreateSpecificCulture("de-DE")).Replace(" ", "&nbsp;") + "</td>" +
                   "                </tr>";

            msg += "            </table>" +
                   "        </div>      " +
                   "            <br/>";




















            List<Buchung> new_buchungen = Buchungen.GetNewBuchungen(last_status.LastBuchungIndex).Where(b=>b.BuchungsDatum > DateTime.Now.AddDays(-10)).ToList();
            if (new_buchungen.Count > 0)
            {
                msg += "            <p><u><b>Buchungszeilen</b></u></p>" +
                       "            <table  style=\"width: 920px;\">" +
                       "                <tr>" +
                       "                    <th style=\"width:100px;\">Buchungs Datum</th>" +
                       "                    <th style=\"width:100px;\">Valuta<br>Datum</th>" +
                       "                    <th style=\"width:280px;\">Buchungs Typ</th>" +
                       "                    <th style=\"width:320px;\">Beschreibung</th>" +
                       "                    <th style=\"width:130px;\">Betrag</th>" +
                       "                    <th style=\"width:100px;\">Konto</th>" +
                       "                </tr>";

                for (int i = new_buchungen.Count - 1; i >= 0; i--)
                {
                    Buchung b = new_buchungen[i];
                    msg += "                <tr>" +
                           "                    <td style=\"text-align:center\">" + b.BuchungsDatum.ToShortDateString() +
                           "</td>" +
                           "                    <td style=\"text-align:center\">" + b.ValutaDatum.ToShortDateString() +
                           "</td>" +
                           "                    <td style=\"text-align:left;padding-left: 10px\">" + b.BuchungsType +
                           "</td>" +
                           "                    <td style=\"text-align:left;padding-left: 10px\">" + b.UmsatzType +
                           "</td>" +
                           "                    <td style=\"" + GetRedColor(b.Betrag) +
                           "text-align:right;padding-right:10px\">" +
                           b.Betrag.ToString("C2", CultureInfo.CreateSpecificCulture("de-DE")).Replace(" ", "&nbsp;") +
                           "</td>" +
                           "                    <td style=\"text-align:center\">" + b.Konto.Owner + "</td>" +
                           "                </tr>";
                }
                msg += "            </table>";
                msg += "        </div><br><br>";
            }














            msg += "        <p><u><b>Monatsübersicht</b></u></p>";
            msg += "        <div>" +
                   "            <table>" +
                   "                <tr>" +
                   "                    <th style=\"width:150px;text-align:left\">Konto</th>";

            const int month_back = 10;
            DateTime date_now = DateTime.Now;
            for (int i = 0; i < month_back; i++)
            {
                msg += "                    <th style=\"width:100px;font-size: small;text-align:right\">" + date_now.ToString("MMM yyyy").Replace(" ", "&nbsp;") + "</th>";
                date_now = date_now.AddMonths(-1);
            }
            msg += "                </tr>";


            float[] summe_ausgaben = new float[month_back];
            float[] summe_einnahmen = new float[month_back];

            foreach (Konto konto in Konten.AlleKonten)
            {
                msg += "                <tr>" +
                       "                    <td>" + konto.Owner + "<br><span style=\"font-weight: lighter;font-size: xx-small\">reine&nbsp;Ausgaben</span></td>";
                date_now = DateTime.Now;
                for (int i = 0; i < month_back; i++)
                {
                    float ausgaben = Buchungen.GetAusgaben(konto, date_now.Month, date_now.Year);

                    msg += "                    <td style=\"" + GetRedColor(ausgaben) + "text-align:right\">" + ausgaben.ToString("C2", CultureInfo.CreateSpecificCulture("de-DE")).Replace(" ", "&nbsp;") + "</td>";
                    summe_ausgaben[i] += ausgaben;
                    date_now = date_now.AddMonths(-1);
                }
                msg += "                </tr>";
            }

            foreach (Konto konto in Konten.AlleKonten)
            {
                msg += "                <tr>" +
                       "                    <td>" + konto.Owner + "<br><span style=\"font-weight: lighter;font-size: xx-small\">reine&nbsp;Einnahmen</span></td>";
                date_now = DateTime.Now;
                for (int i = 0; i < month_back; i++)
                {
                    float einnahmen = Buchungen.GetEinnahmen(konto, date_now.Month, date_now.Year);

                    msg += "                    <td style=\"" + GetRedColor(einnahmen) + "text-align:right\">" + einnahmen.ToString("C2", CultureInfo.CreateSpecificCulture("de-DE")).Replace(" ", "&nbsp;") + "</td>";
                    summe_einnahmen[i] += einnahmen;
                    date_now = date_now.AddMonths(-1);
                }
                msg += "                </tr>";
            }


            msg += "                <tr>" +
                   "                    <td style=\"border-top:2px black solid;\">Summe<br><span style=\"font-weight: lighter;font-size: xx-small\">reine&nbsp;Ausgabe</span></td>";
            for (int i = 0; i < month_back; i++)
            {
                msg += "                    <td style=\"" + GetRedColor(summe_ausgaben[i]) + "text-align:right;border-top:2px black solid;\">" + summe_ausgaben[i].ToString("C2", CultureInfo.CreateSpecificCulture("de-DE")).Replace(" ", "&nbsp;") + "</td>";
            }
            msg += "                </tr>";



            msg += "                <tr>" +
                   "                    <td style=\"border-bottom:2px black solid\">Summe<br><span style=\"font-weight: lighter;font-size: xx-small\">reine&nbsp;Einnahmen</span></td>";
            for (int i = 0; i < month_back; i++)
            {
                msg += "                    <td style=\"" + GetRedColor(summe_einnahmen[i]) + "text-align:right;border-bottom:2px black solid\">" + summe_einnahmen[i].ToString("C2", CultureInfo.CreateSpecificCulture("de-DE")).Replace(" ", "&nbsp;") + "</td>";
            }
            msg += "                </tr>";


            msg += "                <tr>" +
                   "                    <td style=\"border-bottom:2px black double\">+/-</td>";
            for (int i = 0; i < month_back; i++)
            {
                msg += "                    <td style=\"" + GetRedColor(summe_einnahmen[i] + summe_ausgaben[i]) + "text-align:right;border-bottom:2px black double\">" + (summe_einnahmen[i] + summe_ausgaben[i]).ToString("C2", CultureInfo.CreateSpecificCulture("de-DE")).Replace(" ", "&nbsp;") + "</td>";
            }
            msg += "                </tr>";



            msg += "            </table>" +
                   "        </div>      " +
                   "        <div>" +
                   "            <br/>";




            return msg;
        }

        private static string GetKtoStandFehlerMsg(Konto konto)
        {
            if (konto.GiroKtoStandWeb == null)
                return "";

            float kto_stand = Konten.GetGiroKontoStand(konto.Owner);
            if ((float)Math.Round(kto_stand,2) != konto.GiroKtoStandWeb.Value)
            {
                return "<br/>Im Web gibt es einen anderen Giro Kto Stand?&nbsp;" + konto.GiroKtoStandWeb.Value;
            }

            return "";
        }

        private static string GetRedColor(float betrag)
        {
            if (betrag < 0)
                return "color:red;";
            else
                return "";
        }
        #endregion
    }
}
