﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BankLib.Data;

namespace BankLib.Gruppe
{
    public class BgZuoIndexOf : BgZuo
    {
        internal override void WriteToStream(BinaryWriter writer)
        {
            BinaryHelper.WriteString(writer, "BgZuoIndexOf");
            base.WriteToStream(writer);
            BinaryHelper.WriteString(writer, BuchungsType);
            BinaryHelper.WriteString(writer, UmsatzType);
            BinaryHelper.WriteString(writer, _search_string_raw);
        }

        internal override void LoadFromStream(BinaryReader reader)
        {
            base.LoadFromStream(reader);
            BuchungsType = BinaryHelper.ReadString(reader);
            UmsatzType = BinaryHelper.ReadString(reader);
            SearchString = BinaryHelper.ReadString(reader);
        }

        protected override bool CheckZuo(Buchung b)
        {
            if (BuchungsType == b.BuchungsType)
                if(_search_strings != null)
                    foreach (string s in _search_strings)
                    {
                        if(b.UmsatzType.IndexOf(s, StringComparison.Ordinal) >= 0)
                            return true;
                    }
            return false;
        }

        public override string ToString()
        {
            return String.Format("[BgZuoIndexOf] BuchungsTyp [{0}] UmatzTyp [{1}] SearchString [{2}]", BuchungsType, UmsatzType,_search_string_raw);
        }


        
        public string SearchString {
            set
            {
                _search_string_raw = value;
                string[] sarr = _search_string_raw.Split(";".ToCharArray());
                _search_strings = sarr.Where(o => o != "").ToArray();
            }
        }

        public string SearchStringRaw
        {
            get { return _search_string_raw; }
        }
        public string BuchungsType { get; set; }
        public string UmsatzType { get; set; }
        private string[] _search_strings;
        private string _search_string_raw;
    }
}
