﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using Windows7.Multitouch.WPF;
using Windows7.Multitouch.Manipulation;

namespace Logicx.WpfUtility.UiManipulation
{
    public class TouchInertiaProcessManipulation : UiManipulator
    {
        public TouchInertiaProcessManipulation(UIElement ui_element, Canvas canvas, UIElement event_base, EnabledAxes enabled_axes) : this(ui_element, canvas, event_base, enabled_axes, true) { }
        public TouchInertiaProcessManipulation(UIElement ui_element, Canvas canvas, UIElement event_base, EnabledAxes enabled_axes, bool initialize_canvas_position) : base(ui_element, canvas, event_base, enabled_axes, initialize_canvas_position) { }
        
        public override void Register()
        {
            TransformGroup transform_group = new TransformGroup();
            _translate_transform = new TranslateTransform();
            _translate_transform.X = 0;
            _translate_transform.Y = 0;
            _scale_transform = new ScaleTransform();
            _scale_transform.ScaleX = 1;
            _scale_transform.ScaleY = 1;
            _rotate_transform = new RotateTransform();
            _rotate_transform.Angle = 0;
            transform_group.Children.Add(_scale_transform);
            transform_group.Children.Add(_rotate_transform);
            transform_group.Children.Add(_translate_transform);
            _ui_element.RenderTransform = transform_group;
            _ui_element.RenderTransformOrigin = new Point(0.5, 0.5);

            _processor = new SmoothInertiaManipulationProcessor();
            _processor.ManipulationDelta += ProcessManipulationDelta;
            _processor.ManipulationCompleted += ProcessManipulationCompleted;
            _processor.InertiaCompleted += ProcessInertiaCompleted;
            _processor.SmoothTranslation = 0.2f;
            _processor.SmoothRotation = 0.08f;
            _processor.SmoothScale = 0.2f;
            _processor.TranslationDamping = 0.94f;

            _ui_element.StylusDown += StylusDown;
        }


        public override void DeRegister()
        {
            _ui_element.StylusDown -= StylusDown;
            if (_processor != null)
            {
                _processor.CompleteManipulation();
                _processor.ManipulationDelta -= ProcessManipulationDelta;
                _processor.ManipulationCompleted -= ProcessManipulationCompleted;
                _processor.InertiaCompleted -= ProcessInertiaCompleted;
                _processor.DeRegister();
                _processor = null;
            }

            _translate_transform = null;
            _scale_transform = null;
            _rotate_transform = null;
        }

        private void RegisterManipulationEvents()
        {
            if (_events_registered)
                return;

            SetCanvasBackground();
            _event_base.StylusUp += StylusUp;
            _event_base.StylusMove += StylusMove;
            _event_base.StylusLeave += StylusLeave;
            _processor.Register();
            _events_registered = true;
        }

        private void DeRegisterManipulationEvents()
        {
            if (!_events_registered)
                return;

            UnsetCanvasBackground();
            _event_base.StylusUp -= StylusUp;
            _event_base.StylusMove -= StylusMove;
            _event_base.StylusLeave -= StylusLeave;

            _events_registered = false;
        }

        protected void RaisUiManipulationInertiaCompleted(UIElement ui_element)
        {
            _inertia_completed = true;
            _processor.DeRegister();

            if (UiManipulationInertiaCompleted != null)
            {
                Point ui_pos_next = QueryInertiaEndPoint();
                UiManipulationInertiaCompleted(this, new UiManipulationEventArgs(ui_element, ui_pos_next));
            }
        }

        protected virtual Point QueryInertiaEndPoint()
        {
            Point ui_pos_next = new Point(Canvas.GetLeft(_ui_element), Canvas.GetTop(_ui_element));
            return ui_pos_next;
        }

        public virtual void StylusDown(object sender, StylusDownEventArgs e)
        {
            e.Handled = true;

            if (!_events_registered)
            {
                RegisterManipulationEvents();
                RaisUiManipulationStarted(_ui_element);
                _my_stylus_ids = new List<int>();
                _my_stylus_ids.Add(e.StylusDevice.Id);
                _drag_offset = e.GetPosition(_ui_element);
            }
            else
            {
                if (_my_stylus_ids.Count == 2)
                    return;
                _my_stylus_ids.Add(e.StylusDevice.Id);
            }

            Point pos = e.GetPosition(_canvas);
            _inertia_completed = false;
            _processor.ProcessDown((uint)e.StylusDevice.Id, pos.ToDrawingPointF());
            e.Handled = true;
        }

        public virtual void StylusUp(object sender, StylusEventArgs e)
        {
            if (!_events_registered)
                return;

            if (!_my_stylus_ids.Contains(e.StylusDevice.Id))
                return;

            //e.Handled = true;

            _my_stylus_ids.Remove(e.StylusDevice.Id);

            Point pos = e.GetPosition(_canvas);
            _processor.ProcessUp((uint)e.StylusDevice.Id, pos.ToDrawingPointF());

            e.Handled = true;

            if (_my_stylus_ids.Count > 0)
                return;

            DeRegisterManipulationEvents();
            
        }

        public virtual void StylusMove(object sender, StylusEventArgs e)
        {
            if (!_my_stylus_ids.Contains(e.StylusDevice.Id))
                return;

            Point pos = e.GetPosition(_canvas);
            _processor.ProcessMove((uint)e.StylusDevice.Id, pos.ToDrawingPointF());

            e.Handled = true;
        }

        public virtual void StylusLeave(object sender, StylusEventArgs e)
        {
            e.Handled = true;

            StylusUp(sender, e);
        }

        void ProcessManipulationCompleted(object sender, ManipulationCompletedEventArgs e)
        {
            RaiseUiManipulationCompleted(_ui_element);
        }

        void ProcessInertiaCompleted(object sender, ManipulationCompletedEventArgs e)
        {
            RaisUiManipulationInertiaCompleted(_ui_element);
        }

        public virtual void ProcessManipulationDelta(object sender, ManipulationDeltaEventArgs e)
        {
            double left = Canvas.GetLeft(_ui_element);
            double top = Canvas.GetTop(_ui_element);

            if (double.IsNaN(left))
                left = 0;
            if (double.IsNaN(top))
                top = 0;

            if (_enabled_axes == EnabledAxes.XAxis || _enabled_axes == EnabledAxes.All)
                Canvas.SetLeft(_ui_element, left + e.TranslationDelta.Width);
            if (_enabled_axes == EnabledAxes.YAxis || _enabled_axes == EnabledAxes.All)
                Canvas.SetTop(_ui_element, top + e.TranslationDelta.Height);

            if (_with_rotation)
            {
                _rotate_transform.Angle += e.RotationDelta*180/Math.PI;
            }

            if (_with_scaling)
            {
                _scale_transform.ScaleX *= e.ScaleDelta;
                _scale_transform.ScaleY *= e.ScaleDelta;
            }
        }


        protected ScaleTransform ScaleTransform
        {
            get { return _scale_transform; }
        }

        protected RotateTransform RotateTransform
        {
            get { return _rotate_transform; }
        }

        protected TranslateTransform TranslateTransform
        {
            get { return _translate_transform; }
        }

        public bool EnableInertiaAnimation
        {
            get { return _enable_inertia_animation; }
            set
            {
                if(_enable_inertia_animation != value)
                {
                    _enable_inertia_animation = value;
                    if(_processor != null)
                    {
                        _processor.EnableInertiaAnimation = _enable_inertia_animation;
                    }
                }
            }
        }
        protected Point DragOffset
        {
            get { return _drag_offset; }
        }

        #region Attributes
        private SmoothInertiaManipulationProcessor _processor;
        private TranslateTransform _translate_transform;
        private ScaleTransform _scale_transform;
        private RotateTransform _rotate_transform;

        private Point _drag_offset;

        protected List<int> _my_stylus_ids;
        private bool _events_registered;

        protected bool _with_scaling = true;
        protected bool _with_rotation = true;

        private bool _enable_inertia_animation = true;
        protected bool _inertia_completed = false;
        #endregion

        public event EventHandler<UiManipulationEventArgs> UiManipulationInertiaCompleted;
    }
}
