﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MathLib;

namespace Logicx.WpfUtility.PieMenu
{
    /// <summary>
    /// Interaction logic for PieMenu.xaml
    /// </summary>
    public partial class PieMenu : UserControl
    {
        protected enum ShowMenuAnimationStyle
        {
            NoAnimation,
            ForwardAnimation,
            BackwardAnimation
        }

        public PieMenu()
        {
            InitializeComponent();
        }

        public void ResetMenu()
        {
            if(_parent_menu_slices != null)
                _parent_menu_slices.Clear();

            if (_root_menu_slice == null)
                return;

            ShowMenuSlice(_root_menu_slice, ShowMenuAnimationStyle.NoAnimation);
        }

        public void SetMenu(List<PieMenuEntry> menu_entries, double outer_radius, double inner_radius, Color inner_ellipse_color, Color stroke_inner_ellipse, Color outer_ellipse_color, double font_size, SolidColorBrush font_brush, System.Windows.Media.Effects.BitmapEffect font_effect, bool close_on_outerellipse_click)
        {
            _close_on_outer_ellipse_click = close_on_outerellipse_click;

            SetMenu(menu_entries, outer_radius, inner_radius, font_size, font_brush, PieMenuSlice.DefaultFontStrokeBrush, PieMenuSlice.DefaultFontStrokeThickness, font_effect);
            _root_menu_slice.inner_ellipse.Stroke = new SolidColorBrush(stroke_inner_ellipse);
            _root_menu_slice.inner_ellipse.Fill = new SolidColorBrush(inner_ellipse_color);
            _root_menu_slice.outer_ellipse.Fill = new SolidColorBrush(outer_ellipse_color);

            _root_menu_slice.PieFontBitmapEffect = font_effect;
            _root_menu_slice.PieFontBrush = font_brush;
            _root_menu_slice.PieFontSize = font_size;
        }

        public void SetMenu(List<PieMenuEntry> menu_entries, double outer_radius, double inner_radius)
        {
            SetMenu( menu_entries, outer_radius, inner_radius, PieMenuSlice.DefaultFontSize, PieMenuSlice.DefaultFontBrush, PieMenuSlice.DefaultFontStrokeBrush, PieMenuSlice.DefaultFontStrokeThickness, null);
        }

        public void SetMenu(List<PieMenuEntry> menu_entries, double outer_radius, double inner_radius, double font_size, Brush font_brush, Brush font_stroke_brush, float font_stroke_thickness, System.Windows.Media.Effects.BitmapEffect font_effect)
        {
            _root_menu_slice = null;
            _menu_slices = new Dictionary<PieMenuEntry, PieMenuSlice>();
            _parent_menu_slices = new Stack<PieMenuSlice>();
            _menu_entries = menu_entries;

            _inner_radius = inner_radius;
            _outer_radius = outer_radius;

            PieFontBitmapEffect = font_effect;
            PieFontBrush = font_brush;
            PieFontSize = font_size;

            PieMenuSlice menu_slice = CreateMenuSlice(menu_entries, font_size, font_brush, font_stroke_brush, font_stroke_thickness, font_effect);
            _root_menu_slice = menu_slice;

            ShowMenuSlice(_root_menu_slice, ShowMenuAnimationStyle.NoAnimation);
        }


        protected PieMenuSlice CreateMenuSlice(List<PieMenuEntry> menu_entries, double font_size, Brush font_brush, System.Windows.Media.Effects.BitmapEffect font_effect)
        {
            return CreateMenuSlice(menu_entries, font_size, font_brush, null, 0, font_effect);
        }

        protected PieMenuSlice CreateMenuSlice(List<PieMenuEntry> menu_entries, double font_size, Brush font_brush, Brush font_stroke_brush, float font_stroke_thickness, System.Windows.Media.Effects.BitmapEffect font_effect)
        {
            PieMenuSlice menu_slice = new PieMenuSlice(_close_on_outer_ellipse_click);
            menu_slice.SetMenuDimensions(_outer_radius, _inner_radius);
            menu_slice.PieFontBitmapEffect = font_effect;
            menu_slice.PieFontBrush = font_brush;
            menu_slice.PieFontStrokeBrush = font_stroke_brush;
            menu_slice.PieFontStrokeThickness = font_stroke_thickness;
            menu_slice.PieFontSize = font_size;

            //add menuentries
            menu_slice.MenuEntries = menu_entries;

            //register for interaction handlers
            menu_slice.BackClick += new EventHandler(menu_slice_BackClick);
            menu_slice.ButtonClick += new EventHandler<PieMenuSlice.ButtonClickEventArgs>(menu_slice_ButtonClick);
            menu_slice.EnterSubMenuClick += new EventHandler<PieMenuSlice.EnterSubMenuClickEventArgs>(menu_slice_EnterSubMenuClick);

            return menu_slice;
        }


        private void ShowMenuSlice(PieMenuSlice menu_slice, ShowMenuAnimationStyle anim_style)
        {
            _parent_menu_slices.Push(menu_slice);
            if (anim_style == ShowMenuAnimationStyle.NoAnimation)
            {
                base_grid.Children.Clear();
                base_grid.Children.Add(menu_slice);
                menu_slice.ResetAnimations();
            }
            else if(anim_style == ShowMenuAnimationStyle.BackwardAnimation)
            {
                base_grid.Children.Clear();
                base_grid.Children.Add(menu_slice);
                menu_slice.StartBackwardAnimation();
            }
            else if(anim_style == ShowMenuAnimationStyle.ForwardAnimation)
            {
                base_grid.Children.Clear();
                base_grid.Children.Add(menu_slice);
                menu_slice.StartForwardAnimation();
            }
            else
            {
                throw new Exception("Animation Type unkown");
            }
        }

        public void HideMenu()
        {
            Visibility = Visibility.Hidden;
            ResetMenu();

            if (MenuHidden != null)
                MenuHidden(this, new EventArgs());
        }

        #region Find Methods
        public PieMenuEntry FindPieMenuEntryByNameAndKey(string name, string key)
        {
            if (_menu_entries == null)
                return null;

            PieMenuEntry entry = FindPieMenuEntryByKeyAndName(_menu_entries, key, name);

            if (entry == null)
                return null;

            return entry;
        }

        public PieMenuEntry FindPieMenuEntryByKey(string entry_key)
        {
            if (_menu_entries == null)
                return null;

            PieMenuEntry entry = FindPieMenuEntryByKey(_menu_entries, entry_key);

            if (entry == null)
                return null;

            return entry;
        }

        public PieMenuEntry FindPieMenuEntry(string entry_name)
        {
            if (_menu_entries == null)
                return null;

            PieMenuEntry entry = FindPieMenuEntry(_menu_entries, entry_name);

            if (entry == null)
                return null;

            return entry;
        }

        private PieMenuEntry FindPieMenuEntry(List<PieMenuEntry> entries, string entry_name)
        {
            for (int i = 0; i < entries.Count; i++)
            {
                if (entries[i].Name == entry_name)
                    return entries[i];
                if (entries[i].SubEntries != null)
                {
                    PieMenuEntry entry = FindPieMenuEntry(entries[i].SubEntries, entry_name);
                    if (entry != null)
                        return entry;
                }
            }
            return null;
        }


        private PieMenuEntry FindPieMenuEntryByKey(List<PieMenuEntry> entries, string entry_key)
        {
            for (int i = 0; i < entries.Count; i++)
            {
                if (entries[i].Key == entry_key)
                    return entries[i];
                if (entries[i].SubEntries != null)
                {
                    PieMenuEntry entry = FindPieMenuEntryByKey(entries[i].SubEntries, entry_key);
                    if (entry != null)
                        return entry;
                }
            }
            return null;
        }

        private PieMenuEntry FindPieMenuEntryByKeyAndName(List<PieMenuEntry> entries, string entry_key, string name)
        {
            for (int i = 0; i < entries.Count; i++)
            {
                if (string.Equals(entries[i].Key, entry_key) && string.Equals(entries[i].Name, name))
                    return entries[i];
                if (entries[i].SubEntries != null)
                {
                    PieMenuEntry entry = FindPieMenuEntryByKeyAndName(entries[i].SubEntries, entry_key, name);
                    if (entry != null)
                        return entry;
                }
            }
            return null;
        }
        #endregion

        #region Eventhandlers
        void menu_slice_ButtonClick(object sender, PieMenuSlice.ButtonClickEventArgs e)
        {
            if (e.PieMenuEntry.HidesMenuOnClick)
                HideMenu();
        }

        private void menu_slice_EnterSubMenuClick(object sender, PieMenuSlice.EnterSubMenuClickEventArgs e)
        {
            if (!_menu_slices.ContainsKey(e.PieMenuEntry))
                _menu_slices.Add(e.PieMenuEntry, CreateMenuSlice(e.PieMenuEntry.SubEntries, PieFontSize, PieFontBrush, PieFontStrokeBrush, PieFontStrokeThickness, PieFontBitmapEffect));

            ShowMenuSlice(_menu_slices[e.PieMenuEntry], ShowMenuAnimationStyle.ForwardAnimation);
        }

        private void menu_slice_BackClick(object sender, EventArgs e)
        {
            if (_parent_menu_slices.Count < 2)
            {
                HideMenu();
                return;
            }

            _parent_menu_slices.Pop();
            PieMenuSlice menu_slice = _parent_menu_slices.Pop();

            if (menu_slice == null)
                return;

            ShowMenuSlice(menu_slice, ShowMenuAnimationStyle.BackwardAnimation);
        }
        #endregion

        #region Properties
        public Vector2d CenterPostion
        {
            get
            {
                return new Vector2d(_outer_radius, _outer_radius);
            }
        }
        public double PieFontSize
        {
            set; get;
        }
        public Brush PieFontBrush
        {
            set;
            get;
        }
        public Brush PieFontStrokeBrush
        {
            set;
            get;
        }
        public float PieFontStrokeThickness
        { 
            get;
            set;
        }
        public BitmapEffect PieFontBitmapEffect
        {
            set;
            get;
        }
        public List<PieMenuEntry> PieMenuEntries
        {
            get
            {
                return _menu_entries;
            }
            set
            {
                _menu_entries = value;                
            }
        }
        #endregion

        #region Attributes

        protected bool _close_on_outer_ellipse_click;
        protected List<PieMenuEntry> _menu_entries;

        protected PieMenuSlice _root_menu_slice;
        protected Dictionary<PieMenuEntry, PieMenuSlice> _menu_slices = new Dictionary<PieMenuEntry, PieMenuSlice>();
        protected Stack<PieMenuSlice> _parent_menu_slices = new Stack<PieMenuSlice>();

        protected double _inner_radius = 30;
        protected double _outer_radius = 150;

        public event EventHandler MenuHidden;

        private int? _move_stylus_id = null;
        #endregion


    }
}
