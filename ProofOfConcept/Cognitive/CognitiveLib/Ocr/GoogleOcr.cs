﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using CognitiveLib.ApiService;
using Google.Cloud.Vision.V1;

namespace CognitiveLib.Ocr
{
    public class GoogleOcr
    {
        public static void AssertGoogleAuthentication()
        {
            if (string.IsNullOrEmpty(Environment.GetEnvironmentVariable("GOOGLE_APPLICATION_CREDENTIALS")))
                throw new ExternalException("environment variable GOOGLE_APPLICATION_CREDENTIALS not set");
        }

        public static string DetectText(string path_to_file)
        {
            AssertGoogleAuthentication();

            // Load an image from a local file.
            var image = Image.FromFile(path_to_file);
            var client = ImageAnnotatorClient.Create();
            var response = client.DetectText(image);
            string text = "";
            foreach (var annotation in response)
            {
                if (annotation.Description != null)
                    text += annotation.Description;
            }
            return text;
        }

        public static string DetectDocumentText(string path_to_file)
        {
            AssertGoogleAuthentication();

            // Load an image from a local file.
            var image = Image.FromFile(path_to_file);
            var client = ImageAnnotatorClient.Create();
            var response = client.DetectDocumentText(image);
            string text = "";
            foreach (var page in response.Pages)
            {
                foreach (var block in page.Blocks)
                {
                    text += "\n"; 
                    foreach (var paragraph in block.Paragraphs)
                    {
                        text += string.Join(" ", paragraph.Words.Select(w=> string.Join( "", w.Symbols.Select(s => s.Text) )));
                    }
                }
            }
            return text;
        }
    }
}
