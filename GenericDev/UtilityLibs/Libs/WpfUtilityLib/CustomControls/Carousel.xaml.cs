﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Media.Animation;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using MathLib;


namespace Logicx.WpfUtility.CustomControls {
    /// <summary>
    /// Interaktionslogik für Carousel.xaml
    /// </summary>
    public partial class Carousel : UserControl {

        public Carousel(int p_Radius) {
            _radius = p_Radius;
            InitializeComponent();
        }

        public Carousel() {
            InitializeComponent();
            //initElements();
        }


        private void calcBorders() {
            Vector2d l_vec = new Vector2d(_radius, 0);
            l_vec.RotateCounterClockwise(Vector2d.DegToRad(_visibleDegree/2));
            _topBorder = l_vec.Y;
            l_vec.RotateCounterClockwise(Vector2d.DegToRad(_visibleDegree*-1));
            _bottomBorder = l_vec.Y;
            _xBorder = l_vec.X;
            //visibleBorders
            l_vec = new Vector2d(_radius, 0);
            l_vec.RotateCounterClockwise(Vector2d.DegToRad((_visibleDegree / 2)+_scaleAngle/2));
            _visibleTopBorder = l_vec.Y;
            l_vec.RotateCounterClockwise(Vector2d.DegToRad((_visibleDegree+2*_scaleAngle/2) * -1));
            _visibleBottomBorder = l_vec.Y;
            _visibleXBorder = l_vec.X;

        }

        private void LayoutStartPositions() {
            int l_elementCount = GetCarItems(MainCanvas.Children);
            UIElementCollection l_col = MainCanvas.Children;
            _scaleAngle = 0;
            if (_allElemVisible) {// Beschränkung des sichtbaren Bereichs
                if (_elemSpaceConstant > 0) {
                    _scaleAngle = _elemSpaceConstant;
                }
                else {
                    _scaleAngle = _visibleDegree / (l_elementCount - 1);
                }
            }
            else { // Homogene Verteilung am Kreis
                _scaleAngle = 360 / l_elementCount;
            }
            calcBorders();
            Vector2f l_vec = new Vector2f(0, _radius * -1);
            int l_x = 1;
            foreach (UIElement l_elem in MainCanvas.Children) {
                try {
                    TransformGroup tg = new TransformGroup();
                    //tg.Children.Add(new TranslateTransform((MainCanvas.Width+_elemWidth/2)*-1,(MainCanvas.Height+_elemHeigth/2)*-1));
                    if (_rotateElems) {
                        tg.Children.Add(new TranslateTransform(0, _radius * -1));
                        float w = (_scaleAngle * l_x) - _scaleAngle;
                        if (w > 0.0) {
                            tg.Children.Add(new RotateTransform((_scaleAngle * l_x) - _scaleAngle));
                            l_elem.RenderTransform = tg;
                        }
                    }
                    else {
                        //l_vec.X = _radius;
                        //l_vec.Y = 0;
                        float w = (_scaleAngle * l_x) - _scaleAngle;
                        if (_allElemVisible) {
                            w += 270 + (180 - _visibleDegree) / 2;
                            //w += 270;
                        }
                        //l_vec.RotateCounterClockwise(MathLib.Vector2f.DegToRad(w));
                        ((CarouselItem)l_elem).Angle = w;
                        float w_rad = Vector2f.DegToRad(w);
                        double x = (_scaleOutFactor * _radius * Math.Cos(w_rad))+ _xLayoutOffset;
                        double y = (_scaleOutFactor * _radius * Math.Sin(w_rad))+ _yLayoutOffset;
                        ((CarouselItem)l_elem).ActPos = new Point(x, y);
                        Canvas.SetLeft(l_elem, x);
                        Canvas.SetTop(l_elem, y);
                    }
                    l_x++;
                }
                catch (Exception ex) {
                    Console.WriteLine(ex.Message);
                }
            }
            //MainCanvas.ClipToBounds = true;
        }

        /*private void LoadCenterIcon() {
            try {
                BitmapImage centerImageBMP = new BitmapImage();
                centerImageBMP.BeginInit();
                centerImageBMP.UriSource = new Uri(_startFolder + @"\Center.png");
                centerImageBMP.EndInit();
                _centerImage.Source = centerImageBMP;
                _centerImage.Height = centerImageBMP.Height;
                _centerImage.Width = centerImageBMP.Width;

                MainCanvas.Children.Add(_centerImage);
                float w = 0;
                if (_allElemVisible) {
                    w += 270 + (180 - _visibleDegree) / 2;
                }
                float w_rad = Vector2f.DegToRad(w);
                double x = (_scaleOutFactor * _radius * Math.Cos(w_rad));
                //double y = (_scaleOutFactor * _radius * Math.Sin(w_rad));
                Canvas.SetTop(_centerImage, _centerImage.Height/2);
                Canvas.SetLeft(_centerImage, x+_centerImage.Width/2);
            }
            catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
        }*/


        private int GetCarItems(UIElementCollection p_col) {
            int i = 0;
            foreach (UIElement l_elem in MainCanvas.Children) {
                if (l_elem is CarouselItem) {
                    i++;
                }
            }
            return i;
        }

        private void initElements() {
            string[] l_subDirs = Directory.GetDirectories(_startFolder);
            MainCanvas.Children.Clear();
            //MainCanvas.Children.RemoveRange(0, MainCanvas.Children.Count);
            int l_size = l_subDirs.Count();

            foreach(string s in l_subDirs)
            {
                if (s.IndexOf(".svn") < 0)
                    MainCanvas.Children.Add(CreateItem(s));
            }

            string[] curFiles = Directory.GetFiles(_startFolder);
            foreach (string s in curFiles)
                MainCanvas.Children.Add(CreateItem(s));
           

            LayoutStartPositions();
            RotateCarousel(1);
            RotateCarousel(-1);
        }

        private UIElement CreateItem(string path) {
            BitmapImage myBitmapImage = new BitmapImage();
            CarouselItem l_item = new CarouselItem(this);

            try {
                
                string mydir = path.Substring(path.LastIndexOf("\\")+1);
                string iconpath = path + "\\" + mydir + ".png";
                if(File.Exists(iconpath))
                {
                    myBitmapImage.BeginInit();
                    myBitmapImage.UriSource = new Uri(path + "\\" + mydir + ".png" );
                    myBitmapImage.EndInit();
                    l_item.Icon.Source = myBitmapImage;

                }
                
                l_item.Path = path;
                l_item.Icon.Width = _elemWidth;
                l_item.Icon.Height = _elemHeigth;
                l_item.Scale = 1;
                l_item.MouseMove += new MouseEventHandler(OnMouseMove);
                l_item.MouseWheel += new MouseWheelEventHandler(OnMouseWheel);
                string path_name = path.Substring(path.LastIndexOf("\\") + 1);
                l_item.Itemname = path_name.Substring(path_name.IndexOf("-") + 1);
                //if (path.Equals("C:\\Logicx\\Projekte\\Kunden\\511-Ipoint\\5-Impl\\FileDb\\1-BI\\2-Aktuelles")) {
                if (path.EndsWith("FileDb\\1-BI\\2-Aktuelles")) {
                    l_item.Target="Page4";
                }
                if (path.Equals("C:\\Logicx\\Projekte\\Kunden\\511-Ipoint\\5-Impl\\FileDb\\1-BI\\6-Flächenstruktur")) {
                    l_item.Target = "Page5";
                }
                return l_item;
            }
            catch (Exception ex) {
                return null;
            }

        }

        public void SetTarget(object newcontent)
        {
            if(newcontent is BitmapImage)
            {
                //_target = newcontent;
            }
        }

        public void OnCarouselClick(object sender, MouseButtonEventArgs e) {
            _lastMousePos = e.GetPosition(MainCanvas);
            if (sender is CarouselItem) {
                _target = ((CarouselItem)sender).Target;
                ItemSelected(sender, e);
                if (e.ClickCount == 2) {
                    //moveToCenter((CarouselItem)sender);
                }
                
            }
        }



        public void OnMouseWheel(object sender, MouseWheelEventArgs e) {
            int val = e.Delta;
            RotateCarousel(val > 0 ? (float)_mouseWheelSensitivity * -1 : (float)_mouseWheelSensitivity);
        }

        public void OnMouseMove(object sender, MouseEventArgs e) {
            if (e.LeftButton == MouseButtonState.Pressed) {
                Point actPos = e.GetPosition(MainCanvas);

                Vector l_hy = new Vector(_lastMousePos.X, _lastMousePos.Y);
                double l_hyLen = l_hy.Length;
                double alpha = Math.Asin(_lastMousePos.Y / l_hyLen);
                double alpha1 = Math.Asin(actPos.Y / l_hyLen);
                float l_deltaDeg = (float)Vector2d.RadToDeg((float)(alpha1 - alpha));
                if (l_deltaDeg < 5) {
                    RotateCarousel(l_deltaDeg);
                }
                _lastMousePos = actPos;
            }
        }

        /*private void RotateCarousel(float value) {
            if (value.Equals(float.NaN)) return;
            Vector2f l_vec = new Vector2f();
            //Point point;
            float l_actAlpha;
            foreach (UIElement l_elem in MainCanvas.Children) {
                if (l_elem is CarouselItem) {
                    l_vec.X = (float)Canvas.GetLeft(l_elem);
                    l_vec.Y = (float)Canvas.GetTop(l_elem);
                    //((CarouselItem)l_elem).ActPos = new Point(l_vec.X, l_vec.Y);
                    double l_scale = GetScale(l_vec.Y);
                    ((CarouselItem)l_elem).Scale = (float)l_scale;
                    TransformGroup tg = new TransformGroup();
                    tg.Children.Add(new ScaleTransform(l_scale, l_scale, _elemWidth / 2, _elemHeigth / 2));
                    //tg.Children.Add(new TranslateTransform(l_vec.X , l_vec.Y));
                    l_elem.RenderTransform = tg;
                    l_actAlpha = ((CarouselItem)l_elem).Angle;
                    if (l_actAlpha > 360) {
                        l_actAlpha -= 360;
                    }
                    if (_allElemVisible) {
                        if (value < 0) {
                            bool rotateUp = true;
                        }
                    }
                    float l_actAlphaRad = Vector2f.DegToRad((float)(value + l_actAlpha));
                    Canvas.SetLeft(l_elem, _radius * Math.Cos(l_actAlphaRad));
                    Canvas.SetTop(l_elem, _radius * Math.Sin(l_actAlphaRad));
                    ((CarouselItem)l_elem).Angle = l_actAlpha + value;
                }
            }
        }*/

        private double GetMaxYCoordinate(bool updirection) {
            double l_yMax = 0;
            foreach (UIElement l_elem in MainCanvas.Children) {
                if (updirection) {
                    l_yMax = (Canvas.GetTop(l_elem) > l_yMax ? Canvas.GetTop(l_elem) : l_yMax);
                }else {
                    l_yMax = (Canvas.GetTop(l_elem) < l_yMax ? Canvas.GetTop(l_elem) : l_yMax); 
                    
                }
            }
            return l_yMax;
        }

        private void RotateCarousel(float value) {
            int elemPos=-1;
            if (value.Equals(float.NaN)) return;
            if (value == 0.0) return;
            Vector2f l_vec = new Vector2f();
            //Point point;
            float l_actAlpha;
            //CheckBorders
            if (_stopAtBorder) {
                double yMax = GetMaxYCoordinate(value > 0);
                if (value < 0) {
                    if (yMax <= _topBorder) return;
                }
                else {
                    if (yMax >= _bottomBorder) return;
                }
            }
            foreach (UIElement l_elem in MainCanvas.Children) {
                elemPos++;
                if (l_elem is CarouselItem) {
                    l_vec.X = (float)Canvas.GetLeft(l_elem);
                    l_vec.Y = (float)Canvas.GetTop(l_elem);
                    //((CarouselItem)l_elem).ActPos = new Point(l_vec.X, l_vec.Y);
                    double l_scale = GetScale(l_vec.Y);
                    ((CarouselItem)l_elem).Scale = (float)l_scale;
                    //TransformGroup tg = new TransformGroup();
                    //tg.Children.Add(new ScaleTransform(l_scale, l_scale, _elemWidth / 2, _elemHeigth / 2));
                    //tg.Children.Add(new TranslateTransform(l_vec.X , l_vec.Y));
                    l_elem.RenderTransform = new ScaleTransform(l_scale, l_scale, _elemWidth / 2, _elemHeigth / 2);
                    l_actAlpha = ((CarouselItem)l_elem).Angle;
                    if (l_actAlpha > 360) {
                        l_actAlpha -= 360;
                    }
                    float l_actAlphaRad = Vector2f.DegToRad((float)(value + l_actAlpha));
                    Canvas.SetLeft(l_elem, (_radius * Math.Cos(l_actAlphaRad))+_xLayoutOffset);
                    Canvas.SetTop(l_elem, (_radius * Math.Sin(l_actAlphaRad))+_yLayoutOffset);
                    ((CarouselItem)l_elem).Angle = l_actAlpha + value;
                    ((CarouselItem)l_elem).ActPos = new Point(Canvas.GetLeft(l_elem), Canvas.GetTop(l_elem));
                    if (_allElemVisible && !_stopAtBorder) {
                        Point l_rotPoint;
                        if (value < 0) { //UP
                            if ((Canvas.GetTop(l_elem) <=( _visibleTopBorder+_yLayoutOffset))){//&&
                                //(Canvas.GetLeft(l_elem) <_visibleXBorder)){
                                string itemName = ((CarouselItem)l_elem).Itemname;
                                double l_x = Canvas.GetLeft(l_elem);
                                double l_y = Canvas.GetTop(l_elem);
                                l_rotPoint = CalcInvisibleRotation(elemPos,true);
                                Canvas.SetTop(l_elem, l_rotPoint.Y+_yLayoutOffset);
                                Canvas.SetLeft(l_elem, l_rotPoint.X+_xLayoutOffset);
                                ((CarouselItem)l_elem).ActPos = l_rotPoint;
                                Vector l_hy = new Vector(l_rotPoint.X, l_rotPoint.Y);
                                double l_hyLen = l_hy.Length;
                                double alpha1 = Vector2d.RadToDeg(Math.Asin(l_rotPoint.Y / l_hyLen));
                                ((CarouselItem)l_elem).Angle = (float)alpha1;

                            }
                        }
                        else {
                            if ((Canvas.GetTop(l_elem) >= (_visibleBottomBorder+_yLayoutOffset))){// &&
                                //(Canvas.GetLeft(l_elem) < _visibleXBorder)) {
                                string itemName = ((CarouselItem)l_elem).Itemname;
                                double l_x = Canvas.GetLeft(l_elem);
                                double l_y = Canvas.GetTop(l_elem);
                                l_rotPoint = CalcInvisibleRotation(elemPos, false);
                                Canvas.SetTop(l_elem, l_rotPoint.Y + _yLayoutOffset);
                                Canvas.SetLeft(l_elem, l_rotPoint.X + _xLayoutOffset);
                                ((CarouselItem)l_elem).ActPos = l_rotPoint;
                                Vector l_hy = new Vector(l_rotPoint.X, l_rotPoint.Y);
                                double l_hyLen = l_hy.Length;
                                double alpha1 = Vector2d.RadToDeg(Math.Asin(l_rotPoint.Y / l_hyLen));
                                ((CarouselItem)l_elem).Angle = (float)alpha1;

                            }
                        }
                    }

                }
            }
        }

        private Point GetNeighbourPos(int elemPos, bool upRotation) {
            int l_elementCount = MainCanvas.Children.Count;
            CarouselItem l_neighbourItem=null;
            int idx=elemPos;
            bool found =false;
            while (!found) {
                //idx = (upRotation ? idx-- : idx++);
                if (upRotation) {
                    idx--;
                }
                else {
                    idx++;
                }
                if (idx < 0) { idx = l_elementCount - 1; }
                if (idx == (l_elementCount)) {idx=0;}
                if (MainCanvas.Children[idx] is CarouselItem) {
                    found=true;
                    l_neighbourItem = (CarouselItem) MainCanvas.Children[idx];
                }
            }
            return new Point(Canvas.GetLeft(l_neighbourItem),Canvas.GetTop(l_neighbourItem));
        }

        private Point CalcInvisibleRotation(int elemPos,bool upRotation) {
            Point l_nbPosPoint = GetNeighbourPos(elemPos, upRotation);
            Vector2d l_vec = new Vector2d(l_nbPosPoint.X, l_nbPosPoint.Y);
            double l_scaleAngle=_scaleAngle;
            if (upRotation) {
                l_scaleAngle = _scaleAngle * -1;
            }
            l_vec.RotateCounterClockwise(Vector2d.DegToRad(l_scaleAngle));
            return new Point(l_vec.X, l_vec.Y);
        }

        /*private void RotateCarouselTranslate(float value) {
            if (value.Equals(float.NaN)) return;
            Vector2f l_vec = new Vector2f();
            //Point point;
            float l_actAlpha;
            foreach (UIElement l_elem in MainCanvas.Children) {
                if (l_elem is CarouselItem) {
                    l_vec.X = (float)Canvas.GetLeft(l_elem);
                    l_vec.Y = (float)Canvas.GetTop(l_elem);
                    
                    //((CarouselItem)l_elem).ActPos = new Point(l_vec.X, l_vec.Y);
                    double l_scale = GetScale(l_vec.Y);
                    ((CarouselItem)l_elem).Scale = (float)l_scale;
                    l_actAlpha = ((CarouselItem)l_elem).Angle;
                    if (l_actAlpha > 360) {
                        l_actAlpha -= 360;
                    }
                    if (_allElemVisible) {
                        if (value < 0) {
                            bool rotateUp = true;
                        }
                    }
                    float l_actAlphaRad = Vector2f.DegToRad((float)(value + l_actAlpha));
                    l_vec.RotateCounterClockwise(l_actAlphaRad);
                    TransformGroup tg = new TransformGroup();
                    tg.Children.Add(new ScaleTransform(l_scale, l_scale, _elemWidth / 2, _elemHeigth / 2));
                    tg.Children.Add(new TranslateTransform(l_vec.X , l_vec.Y));
                    l_elem.RenderTransform = tg;
                    //Canvas.SetLeft(l_elem, _radius * Math.Cos(l_actAlphaRad));
                    //Canvas.SetTop(l_elem, _radius * Math.Sin(l_actAlphaRad));
                    ((CarouselItem)l_elem).Angle = l_actAlpha + value;
                }
            }
        }*/


        private double GetScale(float p_y) {
            double l_y = Math.Abs(p_y);
            if (l_y > _radius) l_y = _radius;
            double x3 = ((_radius - l_y) * _zoomFactor + (l_y - 0) * _scaleOutFactor) / (_radius - 0);
            return x3;
        }

        private void moveToCenter(CarouselItem elem) {
            CarouselItem l_item = (CarouselItem)elem;
            //float l_angle = l_item.Angle * -1;
            float l_angle = l_item.Angle;
            int i=0;
            TransformGroup centerTransform = new TransformGroup();

            TranslateTransform moveX = new TranslateTransform();
            TranslateTransform moveY = new TranslateTransform();
            centerTransform.Children.Add(moveX);
            centerTransform.Children.Add(moveY);
            //elem.RenderTransform = centerTransform;

            // Create a DoubleAnimationUsingKeyFrames
            // to animate the transform.
            Vector2f l_vec = new Vector2f();
            l_vec.X = (float)Canvas.GetLeft(elem);
            l_vec.Y = (float)Canvas.GetTop(elem);
            l_vec.Rotate(Vector2f.DegToRad(l_angle));

            


            DoubleAnimationUsingKeyFrames xAnim =  new DoubleAnimationUsingKeyFrames();
            xAnim.KeyFrames.Add(new LinearDoubleKeyFrame(l_vec.X-Canvas.GetLeft(elem), KeyTime.FromTimeSpan(TimeSpan.FromSeconds(1))));
            DoubleAnimationUsingKeyFrames yAnim =  new DoubleAnimationUsingKeyFrames();
            yAnim.KeyFrames.Add(new LinearDoubleKeyFrame(l_vec.Y-Canvas.GetTop(elem), KeyTime.FromTimeSpan(TimeSpan.FromSeconds(1))));
            moveX.BeginAnimation(TranslateTransform.XProperty, xAnim);
            moveY.BeginAnimation(TranslateTransform.YProperty, yAnim);
            ArcSegment l_arc = new ArcSegment();
            PathGeometry animationPath = new PathGeometry();
            PathFigure pFigure = new PathFigure();
            NameScope.SetNameScope(this, new NameScope());

            foreach (UIElement l_elem in MainCanvas.Children) {
                if (l_elem is CarouselItem) {
                    /*try {
                        pFigure.Segments.Remove(l_arc);
                        animationPath.Figures.Remove(pFigure);
                    }
                    catch (Exception ex) {
                        
                    }
                    string name = ((CarouselItem)l_elem).Path;
                    name=name.Substring(name.LastIndexOf("-")+1);
                    this.RegisterName(name, l_elem);
                    l_vec.X = (float)Canvas.GetLeft(l_elem);
                    l_vec.Y = (float)Canvas.GetTop(l_elem);
                    pFigure.StartPoint = new Point(l_vec.X, l_vec.Y);
                    l_vec.RotateCounterClockwise(Vector2f.DegToRad(l_angle));
                    l_arc = new ArcSegment(new Point(l_vec.X, l_vec.Y), new Size(_radius, _radius), 90, false, SweepDirection.Clockwise, false);
                    pFigure.Segments.Add(l_arc);
                    animationPath.Figures.Add(pFigure);

                    DoubleAnimationUsingPath pathAnimX = new DoubleAnimationUsingPath();
                    pathAnimX.PathGeometry = animationPath;
                    pathAnimX.Duration = TimeSpan.FromSeconds(1);
                    pathAnimX.Source = PathAnimationSource.X;
                    Storyboard.SetTargetProperty(pathAnimX,new PropertyPath(TranslateTransform.XProperty));
                    Storyboard.SetTargetName(pathAnimX,name);

                    DoubleAnimationUsingPath pathAnimY = new DoubleAnimationUsingPath();
                    pathAnimY.PathGeometry = animationPath;
                    pathAnimY.Duration = TimeSpan.FromSeconds(1);
                    pathAnimY.Source = PathAnimationSource.Y;
                    Storyboard.SetTargetProperty(pathAnimY,new PropertyPath(TranslateTransform.YProperty));
                    Storyboard.SetTargetName(pathAnimY, name);


                    Storyboard pathAnimationStoryboard = new Storyboard();
                    pathAnimationStoryboard.Children.Add(pathAnimX);
                    pathAnimationStoryboard.Children.Add(pathAnimY);
                    Button l_but = new Button();

                    pathAnimationStoryboard.Begin(l_but);
                    */
                    
                    xAnim.KeyFrames.RemoveAt(xAnim.KeyFrames.Count - 1);
                    yAnim.KeyFrames.RemoveAt(yAnim.KeyFrames.Count - 1);
                    xAnim.KeyFrames.Add(new LinearDoubleKeyFrame(l_vec.X - Canvas.GetLeft(l_elem), KeyTime.FromTimeSpan(TimeSpan.FromSeconds(1))));
                    yAnim.KeyFrames.Add(new LinearDoubleKeyFrame(l_vec.Y - Canvas.GetTop(l_elem), KeyTime.FromTimeSpan(TimeSpan.FromSeconds(1))));
                    l_elem.RenderTransform = centerTransform;
                    moveX.BeginAnimation(TranslateTransform.XProperty, xAnim);
                    moveY.BeginAnimation(TranslateTransform.YProperty, yAnim);
                    
                }
            }

            
            //RotateCarouselTranslate(l_angle);
        }

        #region setter
        public int Radius {
            get { return _radius; }
            set { _radius = value; }
        }

        public string StartFolder {
            get { return _startFolder; }
            set {
                _startFolder = value;
                initElements();
            }
        }

        public string Target {
            get { return _target; }
        }

        public int ElemSpaceConstant {
            set { _elemSpaceConstant = value; }
        }

        public bool StopAtBorder {
            get { return _stopAtBorder; }
            set { _stopAtBorder = value; }
        }

        #endregion

        #region Attributes
        //string _startFolder = @"F:\Projekte\Kunden\511-Ipoint\5-Impl\FileDb\1-BI";
        string _startFolder;
        int _elemSpaceConstant = 0; // in Degree !!
        //string _startFolder = @"F:\Projekte\Kunden\511-Ipoint\5-Impl\FileDb\2-Stab Bereichsentwicklung\1-Vorstellung";
        //int _elemSpaceConstant = 25; // in Degree !!
        //int _centerPos;
        double _scaleOutFactor = 0.9;
        double _zoomFactor = 1.5;
        // bool _mouseDown=false;
        Point _lastMousePos = new Point();
        int _elemWidth = 100;
        int _elemHeigth = 100;
        int _radius = 400;
        float _mouseWheelSensitivity = (float)1.7;
        bool _allElemVisible = true;
        int _visibleDegree = 120;
        float _scaleAngle;
        bool _rotateElems = false;
        //bool _debugVisible = false;
        //Image _centerImage = new Image();
        public event EventHandler ItemSelected;
        public string _target;
        bool _stopAtBorder=true;
        double _topBorder;
        double _bottomBorder;
        double _xBorder;
        double _visibleTopBorder;
        double _visibleBottomBorder;
        double _visibleXBorder;
        double _yLayoutOffset = 0;
        double _xLayoutOffset = 0;
        #endregion

    }
}
