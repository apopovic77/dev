﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GeneticProgrammingLib.FindSentence
{
    public class FindSentenceGeneticAlgorithm : GeneticAlgorithm<SentenceDna,char>
    {
        public FindSentenceGeneticAlgorithm(string target_sentence)
        {
            FindSentence.SentenceDna.GeneticTarget = new SentenceDna(target_sentence);
            IntervalMsec = 0;
        }
    }
}
