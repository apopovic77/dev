﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LinqLib.Linq2Sql.Notification
{
    public class SqlChangeNotificationEventArgs : EventArgs
    {
        public SqlChangeNotificationEventArgs(SqlChangeNotificationMeta meta)
        {
            NotificationMeta = meta;
        }

        public SqlChangeNotificationMeta NotificationMeta
        {
            get;
            private set;
        }
    }
}
