﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

namespace LinqLib.Linq2Sql.Progress
{
    public class LinqOutputProgressParser : TextWriter
    {
        public LinqOutputProgressParser(double start_percent, int max_steps, int inserts, int updates, int deletes)
        {
            _start_percent = start_percent;
            _max_steps = max_steps;
            _inserts = inserts;
            _updates = updates;
            _deletes = deletes;
        }

        public LinqOutputProgressParser(double start_percent, int max_steps, DataContext dc)
        {
            _start_percent = start_percent;
            _max_steps = max_steps;
            ChangeSet cs = dc.GetChangeSet();
            _inserts = cs.Inserts.Count;
            _updates = cs.Updates.Count;
            _deletes = cs.Deletes.Count;
            _data_context = dc;
        }

        public LinqOutputProgressParser(DataContext dc)
        {
            _start_percent = 0;
            ChangeSet cs = dc.GetChangeSet();
            _inserts = cs.Inserts.Count;
            _updates = cs.Updates.Count;
            _deletes = cs.Deletes.Count;
            _max_steps = _inserts + _updates + _deletes;
            _data_context = dc;
        }

        public void StartParsing()
        {
            StartParsing(_data_context);
        }

        public void StartParsing(DataContext dc)
        {
            if (dc != null && !object.Equals(dc.Log, this))
            {
                _inserts_catched = 0;
                _updates_catched = 0;
                _deletes_catched = 0;

                _data_context = dc;
                _old_writer = _data_context.Log;

                _data_context.Log = this;
            }
        }

        public void StopParsing()
        {
            if (_data_context != null && object.Equals( _data_context.Log, this))
            {
                _data_context.Log = _old_writer;
            }
        }

        public override void WriteLine(string value)
        {
            if(!string.IsNullOrEmpty(value))
            {
                _submission_history.AppendLine(value);

                if(!value.StartsWith("--"))
                {
                    if(value.StartsWith("INSERT INTO "))
                    {
                        _last_catch = value;
                        _inserts_catched++;
                        RecalculatePercentage();
                        OnUpdatePercentageChanged();
                    }
                    else if(value.StartsWith("UPDATE "))
                    {
                        _last_catch = value;
                        _updates_catched++;
                        RecalculatePercentage();
                        OnUpdatePercentageChanged();
                    }
                    else if (value.StartsWith("DELETE "))
                    {
                        _last_catch = value;
                        _deletes_catched++;
                        RecalculatePercentage();
                        OnUpdatePercentageChanged();
                    }
                }
            }
        }

        private void RecalculatePercentage()
        {
            int work_done = _inserts_catched + _updates_catched + _deletes_catched;
            double percentTotal = 100.0 - _start_percent;
            int maxSteps = (_max_steps == 0 ? 1 : _max_steps);
            double stepPercent = percentTotal / maxSteps;

            _current_percent_done = work_done*stepPercent + _start_percent;
        }

        public override Encoding Encoding
        {
            get { return Encoding.ASCII; }
        }

        public double CurrentPercentDone
        {
            get { return _current_percent_done; }
        }

        public string LastCatch
        {
            get { return _last_catch; }
        }

        public int InsertsDone
        {
            get { return _inserts_catched; }
        }
        public int UpdatesDone
        {
            get { return _updates_catched; }
        }

        public int DeletesDone
        {
            get { return _deletes_catched; }
        }

        public string SubmissionHistory
        {
            get
            {
                return _submission_history.ToString();
            }
        }

        private void OnUpdatePercentageChanged()
        {
            if (UpdatePercentageChanged != null)
            {
                LinqProgressEventArgs e = new LinqProgressEventArgs(_current_percent_done, _inserts, _inserts_catched, _updates, _updates_catched,
                                                                    _deletes, _deletes_catched, _last_catch);

                UpdatePercentageChanged(this, e);
            }


        }

        public event EventHandler<LinqProgressEventArgs> UpdatePercentageChanged;

        private int _inserts_catched = 0;
        private int _updates_catched = 0;
        private int _deletes_catched = 0;

        private DataContext _data_context = null;
        private TextWriter _old_writer = null;
        private double _start_percent = 0;
        private int _max_steps = 0;
        private double _current_percent_done = 0;
        private string _last_catch = "";
        private int _inserts = 0;
        private int _updates = 0;
        private int _deletes = 0;

        private StringBuilder _submission_history = new StringBuilder();

    }
}
